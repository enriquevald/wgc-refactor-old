//------------------------------------------------------------------------------
// Copyright © 2007-2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Interfaces.cs
// 
//   DESCRIPTION : File than implement the following classes:
//      1. PrintVoucher
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-SEP-2010 MBF    Everything moved to WSI.Common, except printing logic.
// 04-APR-2012 JAB    Changes to move operations to obtain and view tickets to the project WSI.Common
// 10-AUG-2012 ACC    Use HtmlPrinter class for printing html vouchers 
// 17-AUG-2015 DHA    Modified print method using voucher parameters configuration
// 10-APR-2017 DHA    Bug 26735: WTA-350: Wait before printing next voucher
// 08-MAY-2017 DHA    Bug 26735: WTA-350: Wait before printing next voucher
// 28-AUG-2017 EOR    Bug 29474: WIGOS-4701 [Ticket #8360] Culiacán - Pago en disputa al realizar una recarga
// 23-FEB-2018 DHA    Bug 31718:WIGOS-8183 [Ticket #12442] Tickets de fondos disponibles terminales
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.ComponentModel;
using Microsoft.Win32;
using System.Collections;
using System.Threading;

namespace WSI.Cashier
{
  public static class VoucherPrint
  {
    #region Public

    //------------------------------------------------------------------------------
    // PURPOSE: Generates and return a file name with the vouchers concatenated
    //          Only for loading in browser preview, not for printing.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - VouchersList: List of Voucher objects
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String with the file name
    // 
    //   NOTES:
    //      - Do not use this for printing!
    //    
    public static string GenerateFileForPreview(ArrayList VouchersList)
    {
      String _doc;
      Int32 _count_row;

      // Generate HTML from concatenate vouchers
      _doc = "<table>\n";
      _count_row = 1;
      foreach (Voucher _voucher in VouchersList)
      {
        _doc += "<tr><td>\n" + _voucher.VoucherHTML + "</td></tr>\n";
        if (_count_row < VouchersList.Count)
        {
          _doc += "<tr><td>\n<hr color='" + CashierStyle.Colors.HexConverter(CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_E3E7E9]) + "' noshade size='12'>\n</td></tr>\n";
        }
        _count_row++;
      }
      _doc += "</table>";

      // Save to file
      return VoucherManager.SaveHTMLVoucherOnFile("REPRINT", _doc);

    } // GenerateFileForPreview

    //------------------------------------------------------------------------------
    // PURPOSE: Prints the voucher list
    //
    //  PARAMS:
    //      - INPUT:
    //          - VouchersList: List of Voucher objects
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //      - Calls the Print function for each voucher
    //  

    public static void Print(ArrayList VouchersList)
    {
      PrintParameters _print_parameters = new PrintParameters();

      Print(VouchersList, ref _print_parameters, null);
    }

    public static void Print(ArrayList VouchersList, Form Form)
    {
      PrintParameters _print_parameters = new PrintParameters();

      Print(VouchersList, ref _print_parameters, Form);
    }
    public static void Print(ArrayList VouchersList, ref PrintParameters PrintParameters, Form Form)
    {
      Int32 _idx_voucher;
      Boolean _has_remaind_copies;
      Int32 _lks_witek_printer_wait;
      Voucher _voucher;
      Boolean _first_printing;
      Int32 _tick0;

      _idx_voucher = 0;
      _has_remaind_copies = true;
      _first_printing = true;

      if (PrintParameters == null)
      {
        PrintParameters = new PrintParameters();
      }

      try
      {
        PrintParameters.m_printing = true;
        PrintParameters.m_total = VouchersList.Count;
        PrintParameters.m_current = 0;

        Int32.TryParse(Environment.GetEnvironmentVariable("LKS_WITEK_WAIT_PRINTER"), out _lks_witek_printer_wait);

        while (_has_remaind_copies)
        {
          _has_remaind_copies = false;

          for (_idx_voucher = 0; _idx_voucher < VouchersList.Count; _idx_voucher++)
          {
            _voucher = ((Voucher)VouchersList[_idx_voucher]);

            while (PrintParameters.m_pause)
            {
              Thread.Sleep(10);
              Application.DoEvents();

              // Cancel while in pause mode
              if (PrintParameters.m_cancel)
              {
                return;
              }
            }

            if (_voucher.NumPendingCopies > 0)
            {
              // Don't show message before the first voucher printing            
              if (_lks_witek_printer_wait > 0 && !_first_printing)
              {
                frm_message.Show(Resource.String("STR_APP_GEN_MSG_WARNING_CONTINUE_PRINTIN_VOCUHER"),
                               Resource.String("STR_APP_GEN_MSG_WARNING"),
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error,
                               Form == null ? Cashier.MainForm : Form);
              }

              _first_printing = false;

              // First print is not a copy
              Print(_voucher, _voucher.NumPendingCopies != _voucher.m_voucher_params.PrintCopy);

              PrintParameters.m_current++;
              _voucher.NumPendingCopies--;

              _tick0 = Misc.GetTickCount();
              while (Misc.GetElapsedTicks(_tick0) < 500)
              {
                Thread.Sleep(10);
                Application.DoEvents();

                // Cancel while in printer wait time
                if (PrintParameters.m_cancel)
                {
                  return;
                }
              }
            }

            // Do another iteration
            if (_voucher.NumPendingCopies > 0)
            {
              _has_remaind_copies = true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }
      finally
      {
        PrintParameters.m_printing = false;
        PrintParameters.m_pause = false;
        PrintParameters.m_cancel = false;

      }
    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Prints the voucher
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherInstance: Voucher
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //      - Saves the voucher on the file and prints
    // 
    public static void Print(Voucher VoucherInstance)
    {
      ArrayList _vouchers;

      _vouchers = new ArrayList();
      _vouchers.Add(VoucherInstance);

      Print(_vouchers);
    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Prints the voucher
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherInstance: Voucher
    //          - IsCopy
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //      - Saves the voucher on the file and prints
    // 
    public static void Print(Voucher VoucherInstance, Boolean IsCopy)
    {
      WebBrowser _browser_for_printing;
      String _str_is_copy;
      String _orig_voucher_html;
      String _doc;
      ////////Boolean _navigated;
      ////////Boolean _printed;
      ////////Int32 _num_tries;
      ////////Int32 _print_time;
      ////////Int32 _tick0;

      Program.print_voucher = VoucherInstance.PrintVoucher;

      // DHA 22-DEC-2014: Replace OperationId key, in case the voucher isn't saved and can't not be update OperationId value
      VoucherInstance.VoucherHTML = VoucherInstance.VoucherHTML.Replace("@VOUCHER_OPERATION_ID<br>", "");

      if (!Program.print_voucher)
      {
        return;
      }

      // Reset browser margins
      Voucher.PageSetup();

      _orig_voucher_html = VoucherInstance.VoucherHTML;

      _browser_for_printing = null;

      try
      {
        if (IsCopy)
        {
          _str_is_copy = Misc.ReadGeneralParams("Cashier.Voucher", "ReprintText");

          if (!String.IsNullOrEmpty(_str_is_copy))
          {
            _doc = "<table>\n<tr><td>\n" + VoucherInstance.VoucherHTML + "</td></tr>";
            _doc += "<tr><td align='center'><br/><b>" + _str_is_copy + "</b></td></tr>\n</table>";

            VoucherInstance.VoucherHTML = _doc;
          }
        }

        // Adjust font and voucher size
        VoucherInstance.VoucherHTML = VoucherManager.FontSetup(VoucherInstance.VoucherHTML);

        // ACC 10-AUG-2012 Use HtmlPrinter class for printing html vouchers 
        HtmlPrinter.AddHtml(VoucherInstance.VoucherHTML);


        ////////////////_navigated = false;
        ////////////////_num_tries = 0;
        ////////////////while (!_navigated && _num_tries < 3)
        ////////////////{
        ////////////////  _num_tries++;

        ////////////////  try
        ////////////////  {
        ////////////////    if (_browser_for_printing != null)
        ////////////////    {
        ////////////////      _browser_for_printing.Dispose();
        ////////////////      _browser_for_printing = null;
        ////////////////    }
        ////////////////    _browser_for_printing = new WebBrowser();

        ////////////////    VoucherInstance.GetFileName();

        ////////////////    // Load the document.
        ////////////////    _browser_for_printing.Navigate(VoucherInstance.VoucherFileName);

        ////////////////    _navigated = true;
        ////////////////  }
        ////////////////  catch (Exception _ex)
        ////////////////  {
        ////////////////    Log.Error(" Can't navigate HTML document. Try num. " + _num_tries + ".");
        ////////////////    Log.Exception(_ex);
        ////////////////    System.Threading.Thread.Sleep(500);
        ////////////////  }
        ////////////////}

        ////////////////if (_navigated)
        ////////////////{
        ////////////////  // Get parameter EstimatedPrintTime from GeneralParms: in ms.
        ////////////////  if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "EstimatedPrintTime"), out _print_time))
        ////////////////  {
        ////////////////    _print_time = 0;
        ////////////////  }

        ////////////////  _printed = false;
        ////////////////  _num_tries = 0;
        ////////////////  while (!_printed && _num_tries < 3)
        ////////////////  {
        ////////////////    _num_tries++;

        ////////////////    try
        ////////////////    {
        ////////////////      // Wait until the document is completely loaded.
        ////////////////      while (_browser_for_printing.ReadyState != WebBrowserReadyState.Complete)
        ////////////////      {
        ////////////////        System.Threading.Thread.Sleep(10);
        ////////////////        Application.DoEvents();
        ////////////////      }

        ////////////////      // RCI & AJQ 13-JUL-2011: Don't use WaitNotTooBusy(). The printer can work wrong and we can stay sleeping inside forever.
        ////////////////      //// RCI 12-JUL-2011: Wait for the printer to be not too busy.
        ////////////////      //Printer.WaitNotTooBusy();

        ////////////////      // Print the document when is fully loaded.
        ////////////////      _browser_for_printing.Print();

        ////////////////      // RCI & AJQ 13-JUL-2011: Sleep again as before without knowing the number of jobs in the printer queue.
        ////////////////      // Sleep to see message, and allow to pause/continue while printing.
        ////////////////      _tick0 = Misc.GetTickCount();
        ////////////////      while (Misc.GetElapsedTicks(_tick0) < _print_time)
        ////////////////      {
        ////////////////        System.Threading.Thread.Sleep(10);
        ////////////////        Application.DoEvents();
        ////////////////      }

        ////////////////      _printed = true;
        ////////////////    }
        ////////////////    catch (Exception _ex)
        ////////////////    {
        ////////////////      Log.Error(" Can't print HTML document. Try num. " + _num_tries + ".");
        ////////////////      Log.Exception(_ex);
        ////////////////      System.Threading.Thread.Sleep(500);
        ////////////////    }
        ////////////////  }
        ////////////////}
      }
      finally
      {
        if (_browser_for_printing != null)
        {
          // Can't dispose now, THE error ocurrs if it does. The system will dispose it when it can.
          //_browser_for_printing.Dispose();
          _browser_for_printing = null;
        }

        VoucherInstance.VoucherHTML = _orig_voucher_html;
      }
    } // Print

    //EOR 28-AUG-2017
    public static void Print(ArrayList VouchersList, Boolean IsBackground)
    {
      BackgroundWorker _bw;

      if (IsBackground)
      {
        m_vouchers_list = VouchersList;
        _bw = new BackgroundWorker();
        _bw.DoWork += new DoWorkEventHandler(PrintVouchers);
        _bw.RunWorkerAsync();
      }
      else
      {
        m_vouchers_list = null;
        Print(VouchersList);
      }
    }

    #endregion // Public
    
    #region private

    //EOR 28-AUG-2017
    private static ArrayList m_vouchers_list;

    //EOR 28-AUG-2017
    private static void PrintVouchers(object o, DoWorkEventArgs e)
    {
      if (m_vouchers_list != null)
      {
        Print(m_vouchers_list);
      }
    }

    #endregion

    
    

  }
}