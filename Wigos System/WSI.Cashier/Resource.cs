//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Resource.cs
// 
//   DESCRIPTION: Class that offers culture associated methods.
// 
//        AUTHOR: AJQ
// 
// CREATION DATE: 26-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 226-AUG-2007 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Drawing;


namespace WSI.Cashier
{
  static class Resource
  {
    static Assembly asm;
    public static ResourceManager rm;
    static CultureInfo short_culture;
    public static CultureInfo culture;
    public static CompareInfo compare;

    /// <summary>
    /// Initialization
    /// </summary>
    public static void Init ()
    {
      Init (CultureInfo.CurrentCulture.Name);
    }

    /// <summary>
    /// Initialization
    /// </summary>
    /// <param name="CultureName">Culture Name</param>
    public static void Init (String CultureName)
    {
      WSI.Common.Resource.Init (CultureName);
      
      asm = Assembly.GetExecutingAssembly ();
      rm = new ResourceManager ("WSI.Cashier.Resources.Resource", System.Reflection.Assembly.GetExecutingAssembly ());

      // Create application cultures
      try
      {
        culture = new CultureInfo(CultureName);
      }
      catch
      {
        CultureName = "en-US";
        culture = new CultureInfo(CultureName);
      }

      short_culture = new CultureInfo (culture.TwoLetterISOLanguageName);

      // Get culture comparison class
      compare = CompareInfo.GetCompareInfo(CultureName);  
    }

    /// <summary>
    /// Returns a resource string from the resource files.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static String String (String Name)
    {
      Boolean _found;

      return String(Name, out _found);
    }

    public static String String(String Name, out Boolean Found)
    {
      return WSI.Common.Resource.String(Name, out Found);
    }

    /// <summary>
    /// Returns a resource string from the resource files without using the parameter Cashier.Voucher.Mode.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static String StringNoVoucherMode(String Name)
    {
      Boolean _found;

      return StringNoVoucherMode(Name, out _found);
    }

    public static String StringNoVoucherMode(String Name, out Boolean Found)
    {
      return WSI.Common.Resource.StringNoVoucherMode(Name, out Found);
    }

    /// <summary>
    /// Parametrized string.
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static String String (String Name, params Object[] list)
    {
      Boolean _found;

      return String(Name, out _found, list);
    }

    public static String String(String Name, out Boolean Found, params Object[] list)
    {
      return WSI.Common.Resource.String(Name, out Found, list);
    }


    /// <summary>
    /// Returns a resource string from the resource files.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static Bitmap Image(String ImageName)
    {
      Bitmap image;

      image = null;

      // Try to get image on localized country-subculture
      image = (Bitmap)rm.GetObject(ImageName, culture);

      if (image == null)
      {
        // Try to get image on localized country culture
        image = (Bitmap)rm.GetObject(ImageName, short_culture);
      }

      // Try to get image on default resource 
      if (image == null)
      {
        // Try to get image on localized country culture
        image = (Bitmap)rm.GetObject(ImageName);
      }

      return image;
    }
   
  }
}
