﻿namespace WSI.Cashier
{
  partial class frm_reception_scan_document
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.lbl_expiration_date = new WSI.Cashier.Controls.uc_label();
      this.uc_dt_expiration = new WSI.Cashier.uc_datetime();
      this.lbl_document_type = new WSI.Cashier.Controls.uc_label();
      this.cb_document_type = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.btn_scan = new WSI.Cashier.Controls.uc_round_button();
      this.btn_rotate = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pb_scanned_photo = new System.Windows.Forms.PictureBox();
      this.btn_configure = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_scanned_photo)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.pnl_data.Controls.Add(this.btn_configure);
      this.pnl_data.Controls.Add(this.pb_scanned_photo);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.lbl_expiration_date);
      this.pnl_data.Controls.Add(this.btn_rotate);
      this.pnl_data.Controls.Add(this.uc_dt_expiration);
      this.pnl_data.Controls.Add(this.btn_scan);
      this.pnl_data.Controls.Add(this.lbl_document_type);
      this.pnl_data.Controls.Add(this.cb_document_type);
      this.pnl_data.Size = new System.Drawing.Size(812, 549);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(483, 314);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Padding = new System.Windows.Forms.Padding(3);
      this.lbl_msg_blink.Size = new System.Drawing.Size(314, 39);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 1022;
      this.lbl_msg_blink.Text = "xError Message";
      this.lbl_msg_blink.Visible = false;
      // 
      // lbl_expiration_date
      // 
      this.lbl_expiration_date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_expiration_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_expiration_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_expiration_date.Location = new System.Drawing.Point(486, 241);
      this.lbl_expiration_date.Name = "lbl_expiration_date";
      this.lbl_expiration_date.Size = new System.Drawing.Size(311, 23);
      this.lbl_expiration_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_expiration_date.TabIndex = 1021;
      this.lbl_expiration_date.Text = "xDocument expiration date";
      // 
      // uc_dt_expiration
      // 
      this.uc_dt_expiration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.uc_dt_expiration.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_expiration.DateText = "";
      this.uc_dt_expiration.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration.DateTextWidth = 0;
      this.uc_dt_expiration.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_expiration.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_expiration.FormatWidth = 135;
      this.uc_dt_expiration.Invalid = false;
      this.uc_dt_expiration.Location = new System.Drawing.Point(483, 264);
      this.uc_dt_expiration.Name = "uc_dt_expiration";
      this.uc_dt_expiration.Size = new System.Drawing.Size(324, 51);
      this.uc_dt_expiration.TabIndex = 1020;
      this.uc_dt_expiration.Tag = "ExpirationDate";
      this.uc_dt_expiration.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      // 
      // lbl_document_type
      // 
      this.lbl_document_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_document_type.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_document_type.Location = new System.Drawing.Point(486, 13);
      this.lbl_document_type.Name = "lbl_document_type";
      this.lbl_document_type.Size = new System.Drawing.Size(319, 23);
      this.lbl_document_type.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_document_type.TabIndex = 1018;
      this.lbl_document_type.Text = "xDocument type to scan";
      // 
      // cb_document_type
      // 
      this.cb_document_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cb_document_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_document_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_document_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_document_type.CornerRadius = 5;
      this.cb_document_type.DataSource = null;
      this.cb_document_type.DisplayMember = "";
      this.cb_document_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_document_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_document_type.DropDownWidth = 314;
      this.cb_document_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_document_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_document_type.FormattingEnabled = true;
      this.cb_document_type.FormatValidator = null;
      this.cb_document_type.IsDroppedDown = false;
      this.cb_document_type.ItemHeight = 40;
      this.cb_document_type.Location = new System.Drawing.Point(486, 36);
      this.cb_document_type.MaxDropDownItems = 8;
      this.cb_document_type.Name = "cb_document_type";
      this.cb_document_type.OnFocusOpenListBox = true;
      this.cb_document_type.SelectedIndex = -1;
      this.cb_document_type.SelectedItem = null;
      this.cb_document_type.SelectedValue = null;
      this.cb_document_type.SelectionLength = 0;
      this.cb_document_type.SelectionStart = 0;
      this.cb_document_type.Size = new System.Drawing.Size(314, 40);
      this.cb_document_type.Sorted = false;
      this.cb_document_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_document_type.TabIndex = 1019;
      this.cb_document_type.TabStop = false;
      this.cb_document_type.ValueMember = "";
      // 
      // btn_scan
      // 
      this.btn_scan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_scan.FlatAppearance.BorderSize = 0;
      this.btn_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_scan.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_scan.Image = null;
      this.btn_scan.IsSelected = false;
      this.btn_scan.Location = new System.Drawing.Point(486, 82);
      this.btn_scan.Name = "btn_scan";
      this.btn_scan.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_scan.Size = new System.Drawing.Size(314, 48);
      this.btn_scan.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_scan.TabIndex = 1014;
      this.btn_scan.Text = "XSCAN";
      this.btn_scan.UseVisualStyleBackColor = false;
      this.btn_scan.Click += new System.EventHandler(this.btn_scan_Click);
      // 
      // btn_rotate
      // 
      this.btn_rotate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_rotate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_rotate.FlatAppearance.BorderSize = 0;
      this.btn_rotate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_rotate.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_rotate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_rotate.Image = null;
      this.btn_rotate.IsSelected = false;
      this.btn_rotate.Location = new System.Drawing.Point(486, 136);
      this.btn_rotate.Name = "btn_rotate";
      this.btn_rotate.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_rotate.Size = new System.Drawing.Size(314, 48);
      this.btn_rotate.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_rotate.TabIndex = 1015;
      this.btn_rotate.Text = "XROTATE IMAGE";
      this.btn_rotate.UseVisualStyleBackColor = false;
      this.btn_rotate.Click += new System.EventHandler(this.btn_rotate_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(634, 489);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(166, 48);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 4;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(462, 489);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(166, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      // 
      // pb_scanned_photo
      // 
      this.pb_scanned_photo.Cursor = System.Windows.Forms.Cursors.Default;
      this.pb_scanned_photo.Location = new System.Drawing.Point(14, 13);
      this.pb_scanned_photo.Name = "pb_scanned_photo";
      this.pb_scanned_photo.Size = new System.Drawing.Size(466, 470);
      this.pb_scanned_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_scanned_photo.TabIndex = 1023;
      this.pb_scanned_photo.TabStop = false;
      this.pb_scanned_photo.Click += new System.EventHandler(this.pb_scanned_photo_Click);
      // 
      // btn_configure
      // 
      this.btn_configure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_configure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_configure.FlatAppearance.BorderSize = 0;
      this.btn_configure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_configure.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_configure.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_configure.Image = null;
      this.btn_configure.IsSelected = false;
      this.btn_configure.Location = new System.Drawing.Point(487, 190);
      this.btn_configure.Name = "btn_configure";
      this.btn_configure.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_configure.Size = new System.Drawing.Size(314, 48);
      this.btn_configure.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_configure.TabIndex = 1024;
      this.btn_configure.Text = "XCONFIGURE";
      this.btn_configure.UseVisualStyleBackColor = false;
      this.btn_configure.Click += new System.EventHandler(this.btn_configure_Click);
      // 
      // frm_reception_scan_document
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.MediumTurquoise;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(812, 604);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_reception_scan_document";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "xScanDocument";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_reception_scan_document_FormClosed);
      this.Shown += new System.EventHandler(this.frm_reception_scan_document_Shown);
      this.pnl_data.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_scanned_photo)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_label lbl_msg_blink;
    private Controls.uc_label lbl_expiration_date;
    private Controls.uc_round_button btn_rotate;
    private uc_datetime uc_dt_expiration;
    private Controls.uc_round_button btn_scan;
    private Controls.uc_label lbl_document_type;
    private WSI.Cashier.Controls.uc_round_auto_combobox cb_document_type;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.PictureBox pb_scanned_photo;
    private Controls.uc_round_button btn_configure;

  }
}