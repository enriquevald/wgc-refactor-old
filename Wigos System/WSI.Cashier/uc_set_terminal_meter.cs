//------------------------------------------------------------------------------
// Copyright � 2007-2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_set_terminal_meter.cs
// 
//   DESCRIPTION: Implements the class uc_set_terminal_meter (Set Terminal meter value)
// 
//        AUTHOR: Daniel Dub� Spielvogel
// 
// CREATION DATE: 21-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2015 DDS    Inital Draft.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier;
using System.Globalization;
using System.Threading;
using System.Xml;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
   
    public partial class uc_set_terminal_meter : UserControl, IDisposable
    {

        #region Members

        private String m_meter_id;
        private DataTable m_actions;
        private string m_selected_action;
        private string[] m_clear_values;
        private string m_no_action_value;
        private TerminalMeter m_terminal_meter;

        #endregion

        #region Constants

        private const Int32 PCD_METER_MAX_VALUE = 1000000000;
        public const String DisplayMember = "DisplayMember";
        public const String ValueMember = "ValueMember";

        #endregion // Constants

        #region Delegates & Events

        public event EventHandler Changed;
        public event EventHandler MaxValue;

        private void OnChange()
        {
            if (Changed != null)
                Changed(this, EventArgs.Empty);
        }
        private void OnMaxValue()
        {
            if (MaxValue != null)
            {
                MaxValue(this, EventArgs.Empty);
            }
        }

        private void txt_value_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!Char.IsDigit(e.KeyChar) &&  // It's not a number
                        e.KeyChar != (Char)8);  // It's not backspace
        }

        private void txt_value_TextChanged(object sender, EventArgs e)
        {
            this.OnChange();

            Int64 _current_value;
            Int64.TryParse(this.Value, out _current_value);
            if (_current_value > this.MaxMeterValue)
            {
                this.OnMaxValue();
            }
        }
        private void cb_action_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txt_value.ReadOnly = false;

            this.SelectedAction = this.cb_action.SelectedValue as String;
            if (this.cb_action.SelectedIndex > -1)
                this.OnChange();

            if (this.m_clear_values == null)
            {
                return;
            }

            if (Array.IndexOf(this.m_clear_values, this.cb_action.SelectedValue) >= 0)
            {
                this.txt_value.Text = String.Empty;
                this.txt_value.ReadOnly = true;
            }
        }

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Int64 MaxMeterValue
        {
            get
            {
                return this.m_terminal_meter != null && this.m_terminal_meter.MeterMaxValue > 0 ?
                  this.m_terminal_meter.MeterMaxValue :
                  PCD_METER_MAX_VALUE;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public String MaxValueMsg
        {
            get { return Resource.String("STR_PCD_MAX_ALLOWED_VALUE", this.MaxMeterValue.ToString("N0")); }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Boolean HasSelectedAction
        {
            get { return this.SelectedAction != null && this.SelectedAction != this.NoActionValue; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public String NoActionValue
        {
            get { return this.m_no_action_value; }
            set { this.m_no_action_value = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataTable Actions
        {
            get { return this.m_actions; }
            set { this.m_actions = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string SelectedAction
        {
            get { return this.m_selected_action; }
            set { this.m_selected_action = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Code
        {
            get { return this.m_meter_id; }
            set { this.m_meter_id = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Point? PadNumberLocation
        {
            get { return this.txt_value.PadNumberLocation; }
            set { this.txt_value.PadNumberLocation = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FormStartPosition PadNumberStartPosition
        {
            get { return this.txt_value.PadNumberStartPosition; }
            set { this.txt_value.PadNumberStartPosition = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PadNumberHorizontalPosition PadNumberHorizontalPosition
        {
            get { return this.txt_value.PadNumberHorizontalPosition; }
            set { this.txt_value.PadNumberHorizontalPosition = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PadNumberVerticalPosition PadNumberVerticalPosition
        {
            get { return this.txt_value.PadNumberVerticalPosition; }
            set { this.txt_value.PadNumberVerticalPosition = value; }
        }


        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Byte SelectedActionToEnum
        {
            get
            {
                switch (this.m_selected_action)
                {
                    case "SET":
                        return (Byte)TerminalMeterAction.ENUM_PCD_UPDATE_METERS_ACTION_SET;
                    case "FIX":
                        return (Byte)TerminalMeterAction.ENUM_PCD_UPDATE_METERS_ACTION_FIX;
                    case "RESET":
                        return (Byte)TerminalMeterAction.ENUM_PCD_UPDATE_METERS_ACTION_RESET;
                    default:
                        throw new InvalidEnumArgumentException(String.Format("'{0}' is no a correct value", this.m_meter_id));

                }
            }

        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string[] ClearTextBoxValues
        {
            get { return this.m_clear_values; }
            set
            {
                this.m_clear_values = value;
                if (this.m_clear_values != null && this.m_clear_values.Length > 1)
                    this.txt_value.ReadOnly = true;
            }
        }

        [Category("Custom control")]
        public String Description
        {
            get { return this.lbl_description.Text.Replace(":", String.Empty); }
            set { this.lbl_description.Text = String.Format("{0}:", value); }
        }

        [Category("Custom control")]
        public String Value
        {
            get { return this.txt_value.Text; }
            set { this.txt_value.Text = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TerminalMeter TerminalMeter
        {
            get { return this.m_terminal_meter; }
            set
            {
                this.m_terminal_meter = value;

                this.Code = this.m_terminal_meter.ToString();
                this.Value = this.m_terminal_meter.MeterValue.ToString();

            }
        }

        [Category("Custom control")]
        public Int32 DescriptionWidth
        {
            get { return this.lbl_description.Width; }
            set { this.lbl_description.Width = value; }
        }

        [Category("Custom control")]
        public Point DescriptionLocation
        {
            get { return this.lbl_description.Location; }
            set { this.lbl_description.Location = value; }
        }

        [Category("Custom control")]
        public Int32 ActionsWidth
        {
            get { return this.cb_action.Width; }
            set { this.cb_action.Width = value; }
        }

        [Category("Custom control")]
        public Point ActionsLocation
        {
            get { return this.cb_action.Location; }
            set { this.cb_action.Location = value; }
        }

        [Category("Custom control")]
        public Int32 ValueWidth
        {
            get { return this.txt_value.Width; }
            set { this.txt_value.Width = value; }
        }

        [Category("Custom control")]
        public Point ValueLocation
        {
            get { return this.txt_value.Location; }
            set { this.txt_value.Location = value; }
        }
        [Category("Custom control")]
        [DefaultValue(false)]
        public Boolean ReadOnly
        {
            get { return this.txt_value.ReadOnly; }
            set
            {
                this.txt_value.ReadOnly = value;
                this.cb_action.Enabled = !value;
            }
        }
        //[Category("Custom control")]
        //[DefaultValue(false)]
        //public Font Font
        //{
        //    get { return this.txt_value.Font; }
        //    set
        //    {
        //        this.txt_value.Font = value;
        //        this.lbl_description.Font = value;
        //    }
        //}
        //[Category("Custom control")]
        //[DefaultValue(false)]
        //public Int32 FontSize
        //{
        //    get { return this.txt_value.Size; }
        //    set
        //    {
        //        this.txt_value.Font = value;
        //        this.lbl_description.Font = value;
        //    }
        //}

        #endregion

        #region Public

        //------------------------------------------------------------------------------
        // PURPOSE : Constructor.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        public uc_set_terminal_meter()
        {
          // ATB 16-FEB-2017
          Misc.WriteLog("[UC LOAD] uc_set_terminal_meter", Log.Type.Message);

            InitializeComponent();
            this.txt_value.PadNumberAmountInputType = GENERIC_AMOUNT_INPUT_TYPE.METER;
        }

        public void InitializeControlValues()
        {
            this.cb_action.ValueMember = ValueMember;
            this.cb_action.DisplayMember = DisplayMember;
            this.cb_action.DataSource = this.Actions;
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Creates one meter Xml item
        // 
        //  PARAMS:
        //      - INPUT: a uc_set_pcd_meter object
        //
        //      - OUTPUT: 
        //
        // RETURNS: One Meter Xml Item
        // 
        public string GetMeterAsXml(Boolean WithAction = true)
        {
            if (WithAction)
                return TerminalMetersInfo.GetMeterXml(this.Code, this.Value, this.SelectedActionToEnum);
            else
                return TerminalMetersInfo.GetMeterXml(this.Code, this.Value, null);

        }

        //------------------------------------------------------------------------------
        // PURPOSE: Set meter values from Xml item
        // 
        //  PARAMS:
        //      - INPUT: Xml Reader whene reader.NodeType == XmlNodeType.Element
        //
        //      - OUTPUT: 
        //
        // RETURNS: One Meter Xml Item
        // 
        public void SetMeterValues(XmlTextReader Reader)
        {
            while (Reader.MoveToNextAttribute())
            {
                switch (Reader.Name.ToUpper())
                {
                    case "CODE":
                        this.Code = Reader.Value;
                        break;
                    case "VALUE":
                        Int32 _value;

                        if (Int32.TryParse(Reader.Value, out _value) && _value == TerminalMetersInfo.NOT_READED_METER_VALUE)
                            this.Value = String.Empty;
                        else
                            this.Value = Reader.Value;

                        break;
                    case "ACTION":
                        this.SelectedAction = Reader.Value;
                        break;
                    case "DESC":
                        this.Description = Reader.Value;
                        break;
                    default:
                        break;
                }
            }
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Set's default action and hides combo
        // 
        //  PARAMS:
        //      - INPUT: SET, FIX, RESET
        //
        //      - OUTPUT: 
        //
        // RETURNS: One Meter Xml Item
        // 
        public void SetAction(string Action)
        {
            this.SelectedAction = Action;
            this.cb_action.Width = 0;
            this.cb_action.Visible = false;
        }

        #endregion

        #region IDisposable Members

        //------------------------------------------------------------------------------
        // PURPOSE : Clean Resources.
        //
        //  PARAMS :
        //      - INPUT :
        //
        //      - OUTPUT :
        //
        // RETURNS :
        //
        void IDisposable.Dispose()
        {
            this.Dispose();
        }

        #endregion

    }

}
