﻿namespace WSI.Cashier
{
  partial class frm_pinpad_config_edit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cb_type = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.cb_port = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.chk_enabled = new WSI.Cashier.Controls.uc_checkBox();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.chk_enabled);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.cb_port);
      this.pnl_data.Controls.Add(this.cb_type);
      this.pnl_data.Size = new System.Drawing.Size(422, 160);
      // 
      // cb_type
      // 
      this.cb_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cb_type.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_type.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_type.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_type.CornerRadius = 5;
      this.cb_type.DataSource = null;
      this.cb_type.DisplayMember = "";
      this.cb_type.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_type.DropDownWidth = 194;
      this.cb_type.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_type.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_type.FormattingEnabled = false;
      this.cb_type.FormatValidator = null;
      this.cb_type.IsDroppedDown = false;
      this.cb_type.ItemHeight = 40;
      this.cb_type.Location = new System.Drawing.Point(25, 20);
      this.cb_type.MaxDropDownItems = 8;
      this.cb_type.Name = "cb_type";
      this.cb_type.OnFocusOpenListBox = true;
      this.cb_type.SelectedIndex = -1;
      this.cb_type.SelectedItem = null;
      this.cb_type.SelectedValue = null;
      this.cb_type.SelectionLength = 0;
      this.cb_type.SelectionStart = 0;
      this.cb_type.Size = new System.Drawing.Size(194, 40);
      this.cb_type.Sorted = false;
      this.cb_type.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_type.TabIndex = 0;
      this.cb_type.TabStop = false;
      this.cb_type.ValueMember = "";
      // 
      // cb_port
      // 
      this.cb_port.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cb_port.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_port.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_port.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_port.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_port.CornerRadius = 5;
      this.cb_port.DataSource = null;
      this.cb_port.DisplayMember = "";
      this.cb_port.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.cb_port.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_port.DropDownWidth = 194;
      this.cb_port.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_port.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_port.FormattingEnabled = false;
      this.cb_port.FormatValidator = null;
      this.cb_port.IsDroppedDown = false;
      this.cb_port.ItemHeight = 40;
      this.cb_port.Location = new System.Drawing.Point(25, 66);
      this.cb_port.MaxDropDownItems = 8;
      this.cb_port.Name = "cb_port";
      this.cb_port.OnFocusOpenListBox = true;
      this.cb_port.SelectedIndex = -1;
      this.cb_port.SelectedItem = null;
      this.cb_port.SelectedValue = null;
      this.cb_port.SelectionLength = 0;
      this.cb_port.SelectionStart = 0;
      this.cb_port.Size = new System.Drawing.Size(194, 40);
      this.cb_port.Sorted = false;
      this.cb_port.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_port.TabIndex = 0;
      this.cb_port.TabStop = false;
      this.cb_port.ValueMember = "";
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(247, 77);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 51);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 0;
      this.btn_cancel.Text = "CANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(247, 20);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(155, 51);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 0;
      this.btn_ok.Text = "OK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // chk_enabled
      // 
      this.chk_enabled.BackColor = System.Drawing.Color.Transparent;
      this.chk_enabled.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.chk_enabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.chk_enabled.Location = new System.Drawing.Point(25, 114);
      this.chk_enabled.MinimumSize = new System.Drawing.Size(212, 30);
      this.chk_enabled.Name = "chk_enabled";
      this.chk_enabled.Padding = new System.Windows.Forms.Padding(5);
      this.chk_enabled.Size = new System.Drawing.Size(212, 30);
      this.chk_enabled.TabIndex = 0;
      this.chk_enabled.Text = "xEnabled";
      this.chk_enabled.UseVisualStyleBackColor = true;
      // 
      // frm_pinpad_config_edit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(422, 215);
      this.Name = "frm_pinpad_config_edit";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "frm_pinpad_config_edit";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Controls.uc_round_auto_combobox cb_port;
    private Controls.uc_round_auto_combobox cb_type;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_checkBox chk_enabled;
  }
}