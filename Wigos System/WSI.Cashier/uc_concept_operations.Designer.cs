namespace WSI.Cashier
{
  partial class uc_concept_operations
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_concept_operations));
      this.pnl_container_0 = new System.Windows.Forms.Panel();
      this.pnl_container_2 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.pnl_container_3 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.pnl_container_1 = new System.Windows.Forms.Panel();
      this.pnl_container_4 = new System.Windows.Forms.Panel();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.pnl_container_left = new System.Windows.Forms.Panel();
      this.pnl_container_right = new System.Windows.Forms.Panel();
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.uc_title = new WSI.Cashier.Controls.uc_label();
      this.pnl_container_2.SuspendLayout();
      this.pnl_container_3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.pnl_container_left.SuspendLayout();
      this.pnl_container_right.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container_0
      // 
      this.pnl_container_0.AutoSize = true;
      this.pnl_container_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_0.Location = new System.Drawing.Point(0, -1);
      this.pnl_container_0.Name = "pnl_container_0";
      this.pnl_container_0.Size = new System.Drawing.Size(411, 66);
      this.pnl_container_0.TabIndex = 58;
      // 
      // pnl_container_2
      // 
      this.pnl_container_2.AutoSize = true;
      this.pnl_container_2.Controls.Add(this.panel2);
      this.pnl_container_2.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_container_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_2.Location = new System.Drawing.Point(0, 0);
      this.pnl_container_2.Name = "pnl_container_2";
      this.pnl_container_2.Size = new System.Drawing.Size(411, 245);
      this.pnl_container_2.TabIndex = 58;
      // 
      // panel2
      // 
      this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.panel2.Location = new System.Drawing.Point(447, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(441, 242);
      this.panel2.TabIndex = 58;
      // 
      // pnl_container_3
      // 
      this.pnl_container_3.AutoSize = true;
      this.pnl_container_3.Controls.Add(this.panel3);
      this.pnl_container_3.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_container_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_3.Location = new System.Drawing.Point(0, 0);
      this.pnl_container_3.Name = "pnl_container_3";
      this.pnl_container_3.Size = new System.Drawing.Size(411, 245);
      this.pnl_container_3.TabIndex = 58;
      // 
      // panel3
      // 
      this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.panel3.Location = new System.Drawing.Point(447, 0);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(441, 242);
      this.panel3.TabIndex = 58;
      // 
      // pnl_container_1
      // 
      this.pnl_container_1.AutoSize = true;
      this.pnl_container_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_1.Location = new System.Drawing.Point(0, -1);
      this.pnl_container_1.Name = "pnl_container_1";
      this.pnl_container_1.Size = new System.Drawing.Size(411, 66);
      this.pnl_container_1.TabIndex = 58;
      // 
      // pnl_container_4
      // 
      this.pnl_container_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.pnl_container_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_4.Location = new System.Drawing.Point(21, 636);
      this.pnl_container_4.Name = "pnl_container_4";
      this.pnl_container_4.Size = new System.Drawing.Size(844, 60);
      this.pnl_container_4.TabIndex = 58;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(10, 5);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(50, 50);
      this.pictureBox1.TabIndex = 60;
      this.pictureBox1.TabStop = false;
      // 
      // pnl_container_left
      // 
      this.pnl_container_left.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pnl_container_left.Controls.Add(this.pnl_container_0);
      this.pnl_container_left.Controls.Add(this.pnl_container_2);
      this.pnl_container_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_left.Location = new System.Drawing.Point(22, 74);
      this.pnl_container_left.Name = "pnl_container_left";
      this.pnl_container_left.Size = new System.Drawing.Size(411, 551);
      this.pnl_container_left.TabIndex = 58;
      // 
      // pnl_container_right
      // 
      this.pnl_container_right.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.pnl_container_right.Controls.Add(this.pnl_container_1);
      this.pnl_container_right.Controls.Add(this.pnl_container_3);
      this.pnl_container_right.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pnl_container_right.Location = new System.Drawing.Point(454, 74);
      this.pnl_container_right.Name = "pnl_container_right";
      this.pnl_container_right.Size = new System.Drawing.Size(411, 551);
      this.pnl_container_right.TabIndex = 58;
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(905, 1);
      this.lbl_separator_1.TabIndex = 88;
      // 
      // uc_title
      // 
      this.uc_title.AutoSize = true;
      this.uc_title.BackColor = System.Drawing.Color.Transparent;
      this.uc_title.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_title.Location = new System.Drawing.Point(65, 20);
      this.uc_title.Name = "uc_title";
      this.uc_title.Size = new System.Drawing.Size(51, 22);
      this.uc_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.uc_title.TabIndex = 103;
      this.uc_title.Text = "TITLE";
      // 
      // uc_concept_operations
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.uc_title);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.pnl_container_right);
      this.Controls.Add(this.pnl_container_left);
      this.Controls.Add(this.pnl_container_4);
      this.Controls.Add(this.pictureBox1);
      this.Name = "uc_concept_operations";
      this.Size = new System.Drawing.Size(902, 714);
      this.pnl_container_2.ResumeLayout(false);
      this.pnl_container_3.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.pnl_container_left.ResumeLayout(false);
      this.pnl_container_left.PerformLayout();
      this.pnl_container_right.ResumeLayout(false);
      this.pnl_container_right.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnl_container_0;
    private System.Windows.Forms.Panel pnl_container_2;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel pnl_container_3;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel pnl_container_1;
    private System.Windows.Forms.Panel pnl_container_4;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Panel pnl_container_left;
    private System.Windows.Forms.Panel pnl_container_right;
    private System.Windows.Forms.Label lbl_separator_1;
    private Controls.uc_label uc_title;
  }
}
