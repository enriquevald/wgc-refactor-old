namespace WSI.Cashier
{
  partial class uc_database_cfg
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_db_id = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_db_id = new WSI.Cashier.Controls.uc_label();
      this.btn_edit = new WSI.Cashier.Controls.uc_round_button();
      this.pb_db_config = new System.Windows.Forms.PictureBox();
      this.txt_db_secondary = new WSI.Cashier.Controls.uc_round_textbox();
      this.txt_db_primary = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_secondary = new WSI.Cashier.Controls.uc_label();
      this.lbl_primary = new WSI.Cashier.Controls.uc_label();
      this.btn_save = new WSI.Cashier.Controls.uc_round_button();
      ((System.ComponentModel.ISupportInitialize)(this.pb_db_config)).BeginInit();
      this.SuspendLayout();
      // 
      // txt_db_id
      // 
      this.txt_db_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_db_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_db_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_db_id.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_db_id.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_db_id.CornerRadius = 5;
      this.txt_db_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_db_id.Location = new System.Drawing.Point(168, 109);
      this.txt_db_id.MaxLength = 3;
      this.txt_db_id.Multiline = false;
      this.txt_db_id.Name = "txt_db_id";
      this.txt_db_id.PasswordChar = '\0';
      this.txt_db_id.ReadOnly = false;
      this.txt_db_id.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_db_id.SelectedText = "";
      this.txt_db_id.SelectionLength = 0;
      this.txt_db_id.SelectionStart = 0;
      this.txt_db_id.Size = new System.Drawing.Size(50, 40);
      this.txt_db_id.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_db_id.TabIndex = 5;
      this.txt_db_id.TabStop = false;
      this.txt_db_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_db_id.UseSystemPasswordChar = false;
      this.txt_db_id.WaterMark = null;
      this.txt_db_id.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_db_id
      // 
      this.lbl_db_id.BackColor = System.Drawing.Color.Transparent;
      this.lbl_db_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_db_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_db_id.Location = new System.Drawing.Point(80, 117);
      this.lbl_db_id.Name = "lbl_db_id";
      this.lbl_db_id.Size = new System.Drawing.Size(70, 25);
      this.lbl_db_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_db_id.TabIndex = 4;
      this.lbl_db_id.Text = "xId";
      this.lbl_db_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_edit
      // 
      this.btn_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_edit.FlatAppearance.BorderSize = 0;
      this.btn_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_edit.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_edit.Image = null;
      this.btn_edit.IsSelected = false;
      this.btn_edit.Location = new System.Drawing.Point(304, 151);
      this.btn_edit.Name = "btn_edit";
      this.btn_edit.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_edit.Size = new System.Drawing.Size(115, 50);
      this.btn_edit.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_edit.TabIndex = 6;
      this.btn_edit.Text = "XEDIT";
      this.btn_edit.UseVisualStyleBackColor = false;
      this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
      // 
      // pb_db_config
      // 
      this.pb_db_config.Location = new System.Drawing.Point(9, 9);
      this.pb_db_config.Name = "pb_db_config";
      this.pb_db_config.Size = new System.Drawing.Size(50, 50);
      this.pb_db_config.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pb_db_config.TabIndex = 101;
      this.pb_db_config.TabStop = false;
      // 
      // txt_db_secondary
      // 
      this.txt_db_secondary.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_db_secondary.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_db_secondary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_db_secondary.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_db_secondary.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_db_secondary.CornerRadius = 5;
      this.txt_db_secondary.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_db_secondary.Location = new System.Drawing.Point(168, 63);
      this.txt_db_secondary.MaxLength = 20;
      this.txt_db_secondary.Multiline = false;
      this.txt_db_secondary.Name = "txt_db_secondary";
      this.txt_db_secondary.PasswordChar = '\0';
      this.txt_db_secondary.ReadOnly = false;
      this.txt_db_secondary.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_db_secondary.SelectedText = "";
      this.txt_db_secondary.SelectionLength = 0;
      this.txt_db_secondary.SelectionStart = 0;
      this.txt_db_secondary.Size = new System.Drawing.Size(171, 40);
      this.txt_db_secondary.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_db_secondary.TabIndex = 3;
      this.txt_db_secondary.TabStop = false;
      this.txt_db_secondary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_db_secondary.UseSystemPasswordChar = false;
      this.txt_db_secondary.WaterMark = null;
      this.txt_db_secondary.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // txt_db_primary
      // 
      this.txt_db_primary.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_db_primary.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_db_primary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_db_primary.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_db_primary.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_db_primary.CornerRadius = 5;
      this.txt_db_primary.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_db_primary.Location = new System.Drawing.Point(168, 16);
      this.txt_db_primary.MaxLength = 20;
      this.txt_db_primary.Multiline = false;
      this.txt_db_primary.Name = "txt_db_primary";
      this.txt_db_primary.PasswordChar = '\0';
      this.txt_db_primary.ReadOnly = false;
      this.txt_db_primary.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_db_primary.SelectedText = "";
      this.txt_db_primary.SelectionLength = 0;
      this.txt_db_primary.SelectionStart = 0;
      this.txt_db_primary.Size = new System.Drawing.Size(171, 40);
      this.txt_db_primary.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_db_primary.TabIndex = 1;
      this.txt_db_primary.TabStop = false;
      this.txt_db_primary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_db_primary.UseSystemPasswordChar = false;
      this.txt_db_primary.WaterMark = null;
      this.txt_db_primary.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_secondary
      // 
      this.lbl_secondary.BackColor = System.Drawing.Color.Transparent;
      this.lbl_secondary.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_secondary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_secondary.Location = new System.Drawing.Point(40, 71);
      this.lbl_secondary.Name = "lbl_secondary";
      this.lbl_secondary.Size = new System.Drawing.Size(110, 25);
      this.lbl_secondary.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_secondary.TabIndex = 2;
      this.lbl_secondary.Text = "xSecundario";
      this.lbl_secondary.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_primary
      // 
      this.lbl_primary.BackColor = System.Drawing.Color.Transparent;
      this.lbl_primary.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_primary.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_primary.Location = new System.Drawing.Point(28, 24);
      this.lbl_primary.Name = "lbl_primary";
      this.lbl_primary.Size = new System.Drawing.Size(122, 25);
      this.lbl_primary.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_primary.TabIndex = 0;
      this.lbl_primary.Text = "xPrimario";
      this.lbl_primary.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // btn_save
      // 
      this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_save.FlatAppearance.BorderSize = 0;
      this.btn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_save.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_save.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_save.Image = null;
      this.btn_save.IsSelected = false;
      this.btn_save.Location = new System.Drawing.Point(304, 151);
      this.btn_save.Name = "btn_save";
      this.btn_save.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_save.Size = new System.Drawing.Size(115, 50);
      this.btn_save.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_save.TabIndex = 94;
      this.btn_save.Text = "XSAVE";
      this.btn_save.UseVisualStyleBackColor = false;
      this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
      // 
      // uc_database_cfg
      // 
      this.BackColor = System.Drawing.Color.LightGray;
      this.Controls.Add(this.txt_db_id);
      this.Controls.Add(this.lbl_db_id);
      this.Controls.Add(this.txt_db_primary);
      this.Controls.Add(this.btn_edit);
      this.Controls.Add(this.btn_save);
      this.Controls.Add(this.pb_db_config);
      this.Controls.Add(this.lbl_primary);
      this.Controls.Add(this.txt_db_secondary);
      this.Controls.Add(this.lbl_secondary);
      this.Name = "uc_database_cfg";
      this.Size = new System.Drawing.Size(447, 215);
      ((System.ComponentModel.ISupportInitialize)(this.pb_db_config)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_save;
    private WSI.Cashier.Controls.uc_round_textbox txt_db_secondary;
    private WSI.Cashier.Controls.uc_round_textbox txt_db_primary;
    private WSI.Cashier.Controls.uc_label lbl_secondary;
    private WSI.Cashier.Controls.uc_label lbl_primary;
    private System.Windows.Forms.PictureBox pb_db_config;
    private WSI.Cashier.Controls.uc_round_textbox txt_db_id;
    private WSI.Cashier.Controls.uc_label lbl_db_id;
    private WSI.Cashier.Controls.uc_round_button btn_edit;
  }
}
