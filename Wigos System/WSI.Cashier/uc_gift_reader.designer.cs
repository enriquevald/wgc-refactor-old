namespace WSI.Cashier
{
  partial class uc_gift_reader
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.tmr_clear = new System.Windows.Forms.Timer(this.components);
      this.pic_logo = new System.Windows.Forms.PictureBox();
      this.txt_voucher_number = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_message = new WSI.Cashier.Controls.uc_label();
      this.lbl_title = new WSI.Cashier.Controls.uc_label();
      ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).BeginInit();
      this.SuspendLayout();
      // 
      // tmr_clear
      // 
      this.tmr_clear.Interval = 250;
      this.tmr_clear.Tick += new System.EventHandler(this.tmr_clear_Tick);
      // 
      // pic_logo
      // 
      this.pic_logo.Location = new System.Drawing.Point(3, 3);
      this.pic_logo.Name = "pic_logo";
      this.pic_logo.Size = new System.Drawing.Size(50, 50);
      this.pic_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pic_logo.TabIndex = 1;
      this.pic_logo.TabStop = false;
      this.pic_logo.Click += new System.EventHandler(this.pic_logo_Click);
      // 
      // txt_voucher_number
      // 
      this.txt_voucher_number.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_voucher_number.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_voucher_number.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_voucher_number.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_voucher_number.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_voucher_number.CornerRadius = 5;
      this.txt_voucher_number.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_voucher_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.txt_voucher_number.Location = new System.Drawing.Point(353, 9);
      this.txt_voucher_number.MaxLength = 32767;
      this.txt_voucher_number.Multiline = false;
      this.txt_voucher_number.Name = "txt_voucher_number";
      this.txt_voucher_number.PasswordChar = '\0';
      this.txt_voucher_number.ReadOnly = false;
      this.txt_voucher_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_voucher_number.SelectedText = "";
      this.txt_voucher_number.SelectionLength = 0;
      this.txt_voucher_number.SelectionStart = 0;
      this.txt_voucher_number.Size = new System.Drawing.Size(265, 40);
      this.txt_voucher_number.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_voucher_number.TabIndex = 1;
      this.txt_voucher_number.Text = "12345678901234567890";
      this.txt_voucher_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_voucher_number.UseSystemPasswordChar = false;
      this.txt_voucher_number.WaterMark = null;
      this.txt_voucher_number.TextChanged += new System.EventHandler(this.txt_track_number_TextChanged);
      this.txt_voucher_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_track_number_KeyPress);
      // 
      // lbl_message
      // 
      this.lbl_message.AutoSize = true;
      this.lbl_message.BackColor = System.Drawing.Color.Transparent;
      this.lbl_message.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_message.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_message.Location = new System.Drawing.Point(632, 20);
      this.lbl_message.Name = "lbl_message";
      this.lbl_message.Size = new System.Drawing.Size(213, 18);
      this.lbl_message.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_message.TabIndex = 2;
      this.lbl_message.Text = "xN�mero de regalo no v�lido";
      this.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_title
      // 
      this.lbl_title.AutoSize = true;
      this.lbl_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_title.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_title.Location = new System.Drawing.Point(74, 20);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(146, 18);
      this.lbl_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_title.TabIndex = 0;
      this.lbl_title.Text = "xInput Gift Number";
      // 
      // uc_gift_reader
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.txt_voucher_number);
      this.Controls.Add(this.lbl_message);
      this.Controls.Add(this.lbl_title);
      this.Controls.Add(this.pic_logo);
      this.Name = "uc_gift_reader";
      this.Size = new System.Drawing.Size(880, 58);
      this.VisibleChanged += new System.EventHandler(this.uc_gift_reader_VisibleChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pic_logo;
    private System.Windows.Forms.Timer tmr_clear;
    private Controls.uc_label lbl_title;
    private Controls.uc_label lbl_message;
    private Controls.uc_round_textbox txt_voucher_number;
  }
}
