﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_mobile_bank_user_info : UserControl
  {
    public const Int32 USER_ID_EMPTY = 0;

    #region "Attributes"
    private MobileBankUserInfoData m_mobilebank_userinfo_data { get; set; }
    #endregion

    #region "Constructor"
    public uc_mobile_bank_user_info(MobileBankUserInfoData MobileBankUserData)
    {
      m_mobilebank_userinfo_data = MobileBankUserData;

      if (m_mobilebank_userinfo_data == null)
      {
        m_mobilebank_userinfo_data = new MobileBankUserInfoData();
      }

      InitializeComponent();
      InitializeControlResources();

      Show(MobileBankUserData);
    }

    public uc_mobile_bank_user_info()
      : this(null)
    {
    }
    #endregion

    #region "Public methods"
    public void Show(MobileBankUserInfoData MobileBankUserData)
    {
      m_mobilebank_userinfo_data = MobileBankUserData;

      if (m_mobilebank_userinfo_data == null)
      {
        m_mobilebank_userinfo_data = new MobileBankUserInfoData();
      }

      ShowMobileBankData();
    }

    public void InitializeControlResources()
    {
      //Labels
      lbl_username_title.Text = Resource.String("STR_SESSION_USER") + ":";
      lbl_track_data_title.Text = Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":";
      lbl_holder_name_title.Text = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME") + ":";
    }
    #endregion

    #region "Private methods"
    private void ShowMobileBankData()
    {
      ShowUserName();

      ShowMobileBankTrackNumber();

      ShowUserHolderName();

      ShowUserPhoto();
    }

    /// <summary>
    /// Is a valid mobile bank id
    /// </summary>
    /// <returns></returns>
    private Boolean IsAValidMobileBank()
    {
      return ((m_mobilebank_userinfo_data != null) && (m_mobilebank_userinfo_data.UserId != 0));
    }

    /// <summary>
    /// Show username
    /// </summary>
    private void ShowUserName()
    {
      lbl_username_value.Text = m_mobilebank_userinfo_data.UserName;
      lbl_username_value.Visible = IsAValidMobileBank();
    }

    /// <summary>
    /// Show mobile bank track data
    /// </summary>
    private void ShowMobileBankTrackNumber()
    {
      String _visible_track_number;

      _visible_track_number = (m_mobilebank_userinfo_data.IsVirtualCard) ? MobileBankConfig.GetVirtualCardTrackdataToShow() : m_mobilebank_userinfo_data.MobileExternalTrackData;

      lbl_track_data_value.Text = _visible_track_number;
      lbl_track_data_value.Visible = IsAValidMobileBank();
    }

    /// <summary>
    /// Show user holder name
    /// </summary>
    private void ShowUserHolderName()
    {
      String _holder_name = m_mobilebank_userinfo_data.UserHolderName;

      lbl_holder_name_value.Text = _holder_name;
      lbl_holder_name_value.Visible = IsAValidMobileBank();
    }

    /// <summary>
    /// Show user photo
    /// </summary>
    private void ShowUserPhoto()
    {
      //User does not have photo
      pb_user.Image = Resources.ResourceImages.anonymous_user;
      pb_user.Visible = true;
    }

    /// <summary>
    /// Toogle the apparence of control depending on EnabledValue
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="EnabledValue"></param>
    private void ToggleApparenceCtrl(Control Ctrl, Boolean EnabledValue)
    {
      if (!Ctrl.Equals(this))
      {
        Ctrl.Enabled = EnabledValue;
      }

      foreach (Control _control in Ctrl.Controls)
      {
        ToggleApparenceCtrl(_control, EnabledValue);
      }
    }
    #endregion
  }
}
