using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_win_loss_statement 
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_request = new WSI.Cashier.Controls.uc_round_button();
      this.urp_container = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_request_year = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_name = new WSI.Cashier.Controls.uc_label();
      this.cb_request_year = new WSI.Cashier.Controls.uc_round_combobox();
      this.lbl_holder_name_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_period_year_to = new WSI.Cashier.Controls.uc_label();
      this.lblTo = new WSI.Cashier.Controls.uc_label();
      this.lbl_from = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_status_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_status = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_period_year_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_period_year_from = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_datetime_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_request_datetime = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_account = new WSI.Cashier.Controls.uc_label();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.urp_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_request);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.urp_container);
      this.pnl_data.Size = new System.Drawing.Size(618, 417);
      this.pnl_data.TabIndex = 0;
      // 
      // btn_request
      // 
      this.btn_request.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_request.FlatAppearance.BorderSize = 0;
      this.btn_request.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_request.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_request.Image = null;
      this.btn_request.IsSelected = false;
      this.btn_request.Location = new System.Drawing.Point(445, 340);
      this.btn_request.Name = "btn_request";
      this.btn_request.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_request.Size = new System.Drawing.Size(155, 60);
      this.btn_request.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_request.TabIndex = 1;
      this.btn_request.Text = "XSOLICITAR";
      this.btn_request.UseVisualStyleBackColor = false;
      // 
      // urp_container
      // 
      this.urp_container.BackColor = System.Drawing.Color.Transparent;
      this.urp_container.BorderColor = System.Drawing.Color.Empty;
      this.urp_container.Controls.Add(this.lbl_request_year);
      this.urp_container.Controls.Add(this.lbl_holder_name);
      this.urp_container.Controls.Add(this.cb_request_year);
      this.urp_container.Controls.Add(this.lbl_holder_name_prefix);
      this.urp_container.Controls.Add(this.lbl_request_period_year_to);
      this.urp_container.Controls.Add(this.lblTo);
      this.urp_container.Controls.Add(this.lbl_from);
      this.urp_container.Controls.Add(this.lbl_request_status_prefix);
      this.urp_container.Controls.Add(this.lbl_request_status);
      this.urp_container.Controls.Add(this.lbl_request_period_year_prefix);
      this.urp_container.Controls.Add(this.lbl_request_period_year_from);
      this.urp_container.Controls.Add(this.lbl_request_datetime_prefix);
      this.urp_container.Controls.Add(this.lbl_request_datetime);
      this.urp_container.Controls.Add(this.lbl_account_prefix);
      this.urp_container.Controls.Add(this.lbl_account);
      this.urp_container.CornerRadius = 10;
      this.urp_container.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.urp_container.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.urp_container.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.urp_container.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.urp_container.HeaderHeight = 35;
      this.urp_container.HeaderSubText = null;
      this.urp_container.HeaderText = null;
      this.urp_container.Location = new System.Drawing.Point(18, 17);
      this.urp_container.Name = "urp_container";
      this.urp_container.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.urp_container.Size = new System.Drawing.Size(581, 307);
      this.urp_container.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.urp_container.TabIndex = 2;
      // 
      // lbl_request_year
      // 
      this.lbl_request_year.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_year.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_year.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_request_year.Location = new System.Drawing.Point(88, 109);
      this.lbl_request_year.Name = "lbl_request_year";
      this.lbl_request_year.Size = new System.Drawing.Size(157, 23);
      this.lbl_request_year.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_request_year.TabIndex = 4;
      this.lbl_request_year.Text = "A�o solicitado:";
      this.lbl_request_year.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_holder_name
      // 
      this.lbl_holder_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_name.Location = new System.Drawing.Point(266, 77);
      this.lbl_holder_name.Name = "lbl_holder_name";
      this.lbl_holder_name.Size = new System.Drawing.Size(216, 20);
      this.lbl_holder_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_name.TabIndex = 3;
      this.lbl_holder_name.Text = "XXXXXXXXXXXXXXXXXXXX";
      this.lbl_holder_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // cb_request_year
      // 
      this.cb_request_year.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_request_year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_request_year.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_request_year.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_request_year.CornerRadius = 5;
      this.cb_request_year.DataSource = null;
      this.cb_request_year.DisplayMember = "";
      this.cb_request_year.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_request_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_request_year.DropDownWidth = 216;
      this.cb_request_year.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_request_year.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_request_year.FormattingEnabled = true;
      this.cb_request_year.IsDroppedDown = false;
      this.cb_request_year.ItemHeight = 40;
      this.cb_request_year.Location = new System.Drawing.Point(266, 104);
      this.cb_request_year.MaxDropDownItems = 8;
      this.cb_request_year.Name = "cb_request_year";
      this.cb_request_year.OnFocusOpenListBox = true;
      this.cb_request_year.SelectedIndex = -1;
      this.cb_request_year.SelectedItem = null;
      this.cb_request_year.SelectedValue = null;
      this.cb_request_year.SelectionLength = 0;
      this.cb_request_year.SelectionStart = 0;
      this.cb_request_year.Size = new System.Drawing.Size(216, 40);
      this.cb_request_year.Sorted = false;
      this.cb_request_year.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_request_year.TabIndex = 5;
      this.cb_request_year.TabStop = false;
      this.cb_request_year.ValueMember = "";
      // 
      // lbl_holder_name_prefix
      // 
      this.lbl_holder_name_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_name_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_name_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_holder_name_prefix.Location = new System.Drawing.Point(88, 76);
      this.lbl_holder_name_prefix.Name = "lbl_holder_name_prefix";
      this.lbl_holder_name_prefix.Size = new System.Drawing.Size(157, 23);
      this.lbl_holder_name_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_holder_name_prefix.TabIndex = 2;
      this.lbl_holder_name_prefix.Text = "Nombre:";
      this.lbl_holder_name_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_request_period_year_to
      // 
      this.lbl_request_period_year_to.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_period_year_to.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_period_year_to.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_request_period_year_to.Location = new System.Drawing.Point(266, 227);
      this.lbl_request_period_year_to.Name = "lbl_request_period_year_to";
      this.lbl_request_period_year_to.Size = new System.Drawing.Size(216, 20);
      this.lbl_request_period_year_to.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_request_period_year_to.TabIndex = 12;
      this.lbl_request_period_year_to.Text = "01/01/1901";
      this.lbl_request_period_year_to.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblTo
      // 
      this.lblTo.BackColor = System.Drawing.Color.Transparent;
      this.lblTo.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lblTo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lblTo.Location = new System.Drawing.Point(88, 226);
      this.lblTo.Name = "lblTo";
      this.lblTo.Size = new System.Drawing.Size(157, 23);
      this.lblTo.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lblTo.TabIndex = 11;
      this.lblTo.Text = "Hasta fecha:";
      this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_from
      // 
      this.lbl_from.BackColor = System.Drawing.Color.Transparent;
      this.lbl_from.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_from.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_from.Location = new System.Drawing.Point(88, 203);
      this.lbl_from.Name = "lbl_from";
      this.lbl_from.Size = new System.Drawing.Size(157, 23);
      this.lbl_from.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_from.TabIndex = 9;
      this.lbl_from.Text = "Desde fecha:";
      this.lbl_from.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_request_status_prefix
      // 
      this.lbl_request_status_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_status_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_status_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_request_status_prefix.Location = new System.Drawing.Point(88, 256);
      this.lbl_request_status_prefix.Name = "lbl_request_status_prefix";
      this.lbl_request_status_prefix.Size = new System.Drawing.Size(157, 23);
      this.lbl_request_status_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_request_status_prefix.TabIndex = 13;
      this.lbl_request_status_prefix.Text = "Estado solicitud:";
      this.lbl_request_status_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_request_status
      // 
      this.lbl_request_status.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_status.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_request_status.Location = new System.Drawing.Point(266, 257);
      this.lbl_request_status.Name = "lbl_request_status";
      this.lbl_request_status.Size = new System.Drawing.Size(216, 20);
      this.lbl_request_status.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_request_status.TabIndex = 14;
      this.lbl_request_status.Text = "Pdte";
      this.lbl_request_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_request_period_year_prefix
      // 
      this.lbl_request_period_year_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_period_year_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_period_year_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_request_period_year_prefix.Location = new System.Drawing.Point(88, 178);
      this.lbl_request_period_year_prefix.Name = "lbl_request_period_year_prefix";
      this.lbl_request_period_year_prefix.Size = new System.Drawing.Size(157, 23);
      this.lbl_request_period_year_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_request_period_year_prefix.TabIndex = 8;
      this.lbl_request_period_year_prefix.Text = "Periodo solicitud";
      this.lbl_request_period_year_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_request_period_year_from
      // 
      this.lbl_request_period_year_from.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_period_year_from.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_period_year_from.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_request_period_year_from.Location = new System.Drawing.Point(266, 204);
      this.lbl_request_period_year_from.Name = "lbl_request_period_year_from";
      this.lbl_request_period_year_from.Size = new System.Drawing.Size(216, 20);
      this.lbl_request_period_year_from.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_request_period_year_from.TabIndex = 10;
      this.lbl_request_period_year_from.Text = "01/01/1901";
      this.lbl_request_period_year_from.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_request_datetime_prefix
      // 
      this.lbl_request_datetime_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_datetime_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_datetime_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_request_datetime_prefix.Location = new System.Drawing.Point(88, 140);
      this.lbl_request_datetime_prefix.Name = "lbl_request_datetime_prefix";
      this.lbl_request_datetime_prefix.Size = new System.Drawing.Size(157, 23);
      this.lbl_request_datetime_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_request_datetime_prefix.TabIndex = 6;
      this.lbl_request_datetime_prefix.Text = "Fecha solicitud:";
      this.lbl_request_datetime_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_request_datetime
      // 
      this.lbl_request_datetime.BackColor = System.Drawing.Color.Transparent;
      this.lbl_request_datetime.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_request_datetime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_request_datetime.Location = new System.Drawing.Point(266, 141);
      this.lbl_request_datetime.Name = "lbl_request_datetime";
      this.lbl_request_datetime.Size = new System.Drawing.Size(216, 20);
      this.lbl_request_datetime.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_request_datetime.TabIndex = 7;
      this.lbl_request_datetime.Text = "01/01/1901 ";
      this.lbl_request_datetime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_prefix
      // 
      this.lbl_account_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_prefix.Location = new System.Drawing.Point(88, 52);
      this.lbl_account_prefix.Name = "lbl_account_prefix";
      this.lbl_account_prefix.Size = new System.Drawing.Size(157, 23);
      this.lbl_account_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_prefix.TabIndex = 0;
      this.lbl_account_prefix.Text = "Cuenta:";
      this.lbl_account_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account
      // 
      this.lbl_account.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account.Location = new System.Drawing.Point(266, 53);
      this.lbl_account.Name = "lbl_account";
      this.lbl_account.Size = new System.Drawing.Size(216, 20);
      this.lbl_account.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_account.TabIndex = 1;
      this.lbl_account.Text = "XXXXXXXXXXXXXXXXXXXX";
      this.lbl_account.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_close
      // 
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(275, 340);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 0;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // frm_win_loss_statement
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(618, 472);
      this.Name = "frm_win_loss_statement";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "WinLossStatement";
      this.pnl_data.ResumeLayout(false);
      this.urp_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_round_button btn_request;
    private uc_round_button btn_close;
    private uc_round_panel urp_container;
    private uc_label lbl_request_year;
    private uc_label lbl_holder_name;
    private uc_round_combobox cb_request_year;
    private uc_label lbl_holder_name_prefix;
    private uc_label lbl_request_period_year_to;
    private uc_label lblTo;
    private uc_label lbl_from;
    private uc_label lbl_request_status_prefix;
    private uc_label lbl_request_status;
    private uc_label lbl_request_period_year_prefix;
    private uc_label lbl_request_period_year_from;
    private uc_label lbl_request_datetime_prefix;
    private uc_label lbl_request_datetime;
    private uc_label lbl_account_prefix;
    private uc_label lbl_account;


  }
}