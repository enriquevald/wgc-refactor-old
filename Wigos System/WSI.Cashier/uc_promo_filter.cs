//------------------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_promo_filter.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-DEC-2013 JPJ    The textbox could be edited, it has been changed into read only.
// 12-MAR-2014 LEM    Fixed Bug WIGOSTITO-1137: Multiple errors.
// 12-MAR-2014 LEM    Fixed Bug WIGOSTITO-1139: Disable lock promotions for TITO.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_promo_filter : UserControl
  {
    private Font m_font_filter_credit_checked = null;
    private Font m_font_filter_credit_unchecked = null;
    private List<CheckBox> m_ls_chk_filter = null;
    private Int32 m_last_count_checked;
    private Int32 m_count_enabled;
    private Boolean m_virtual_mode;
    private Boolean m_is_tito_mode;
    public String m_filter;

    public uc_promo_filter()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_promo_filter", Log.Type.Message);

      InitializeComponent();
      m_is_tito_mode = Common.TITO.Utils.IsTitoMode();
    }

    public Boolean VirtualMode
    {
      get
      {
        return m_virtual_mode;
      }
      set
      {
        m_virtual_mode = (value && m_is_tito_mode);
      }
    }

    public Boolean ShowHeadLabel
    {
      get
      {
        return lbl_sort_by.Visible;
      }
      set
      {
        lbl_sort_by.Visible = value;
      }
    }
    
    public delegate void FilterChangeEvent(object sender);
    public event FilterChangeEvent FilterChange;

    private void uc_promo_filter_Load(object sender, EventArgs e)
    {
      if (m_ls_chk_filter == null)
      {
        m_ls_chk_filter = new List<CheckBox>();

        m_ls_chk_filter.Add(chk_nr);
        m_ls_chk_filter.Add(chk_ca);
        m_ls_chk_filter.Add(chk_re);
        m_ls_chk_filter.Add(chk_pt);
      }

      m_last_count_checked = m_ls_chk_filter.Count;
      m_count_enabled = m_last_count_checked;

      if (m_font_filter_credit_checked == null)
      {
        Font _font = (Font)chk_nr.Font.Clone();
        m_font_filter_credit_checked = new Font(_font.FontFamily, (float)10.0, FontStyle.Bold);
      }

      if (m_font_filter_credit_unchecked == null)
      {
        Font _font = (Font)chk_nr.Font.Clone();
        m_font_filter_credit_unchecked = new Font(_font.FontFamily, (float)8.25, FontStyle.Regular);
      }

      foreach (CheckBox _chk in m_ls_chk_filter)
      {

        _chk.CheckedChanged -= CheckedFilterGrid;
        _chk.Checked = true;
        _chk.CheckedChanged += new EventHandler(CheckedFilterGrid);
        FilterCreditChecked(_chk);

        switch (_chk.Name)
        {
          case "chk_nr":
            lbl_non_redeemable.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_022");
            break;
          case "chk_ca":
            lbl_lock.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_023");
            break;
          case "chk_re":
            lbl_redeemable.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_024");
            break;
          case "chk_pt":
            lbl_points.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_025");
            break;

          default:
            break;
        }
        _chk.Visible = true;
      }

      lbl_sort_by.Text = Resource.String("STR_FRM_AMOUNT_INPUT_PROMO_034");
      m_filter = "";

      if (m_is_tito_mode)
      {
        chk_ca.Enabled = false;
        chk_ca.CheckedChanged -= new EventHandler(CheckedFilterGrid);
        chk_ca.Checked = false;
        chk_ca.Font = m_font_filter_credit_unchecked;
        chk_ca.TextAlign = ContentAlignment.BottomRight;
        m_last_count_checked--;
        m_count_enabled--;
        m_filter += (!String.IsNullOrEmpty(m_filter) ? " AND " : "") + String.Format(" SORT <> {0} ", (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR2);
      }

      if (m_virtual_mode || GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
      {
        chk_pt.Enabled = false;
        chk_pt.CheckedChanged -= new EventHandler(CheckedFilterGrid);
        chk_pt.Checked = false;
        chk_pt.Font = m_font_filter_credit_unchecked;
        chk_pt.TextAlign = ContentAlignment.BottomRight;
        m_last_count_checked--;
        m_count_enabled--;
        m_filter += (!String.IsNullOrEmpty(m_filter) ? " AND " : "") + String.Format(" SORT <> {0}", (Int32)ACCOUNT_PROMO_CREDIT_TYPE.POINT);
      }

      if (FilterChange != null)
      {
        FilterChange(this);
      }
    }

    private void CheckedFilterGrid(object sender, EventArgs e)
    {
      String _filter;
      ACCOUNT_PROMO_CREDIT_TYPE[] CreditType;

      if (m_last_count_checked == m_count_enabled)
      {
        foreach (CheckBox _chk in m_ls_chk_filter)
        {
          if (!_chk.Equals(sender) && _chk.Enabled)
          {
            _chk.CheckedChanged -= new EventHandler(CheckedFilterGrid);
            _chk.Checked = false;
            _chk.CheckedChanged += new EventHandler(CheckedFilterGrid);
            FilterCreditUnChecked(_chk);
          }
        }
        FilterCreditChecked((CheckBox)sender);
        ((CheckBox)sender).CheckedChanged -= new EventHandler(CheckedFilterGrid);
        ((CheckBox)sender).Checked = true;
        ((CheckBox)sender).CheckedChanged += new EventHandler(CheckedFilterGrid);
        m_last_count_checked = 0;
      }
      else
      {
        m_last_count_checked = 0;
        foreach (CheckBox _chk in m_ls_chk_filter)
        {
          if (_chk.Checked)
          {
            FilterCreditChecked(_chk);
            m_last_count_checked++;
          }
          else
          {
            FilterCreditUnChecked(_chk);
          }
        }
        if (m_last_count_checked == 0)
        {
          foreach (CheckBox _chk in m_ls_chk_filter)
          {
            if (_chk.Enabled)
            {
              FilterCreditChecked(_chk);
            }
          }
        }
      }

      _filter = "";
      CreditType = null;

      if (chk_nr.Checked)
      {
        Array.Resize(ref CreditType, 1);
        CreditType.SetValue(ACCOUNT_PROMO_CREDIT_TYPE.NR1, 0);
      }

      if (chk_ca.Checked)
      {
        Array.Resize(ref CreditType, 2);
        //
        // 2 IS WONLOCK!!!!
        //
        CreditType.SetValue(ACCOUNT_PROMO_CREDIT_TYPE.NR2, 1);
        //
        // ACCOUNT_PROMO_CREDIT_TYPE.NR2 IS WONLOCK!!!!
        //
      }

      if (chk_re.Checked)
      {
        Array.Resize(ref CreditType, 3);
        CreditType.SetValue(ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE, 2);
      }

      if (chk_pt.Checked)
      {
        Array.Resize(ref CreditType, 4);
        CreditType.SetValue(ACCOUNT_PROMO_CREDIT_TYPE.POINT, 3);
      }

      if (CreditType != null)
      {
        foreach (ACCOUNT_PROMO_CREDIT_TYPE _credit_type in CreditType)
        {
          _filter += String.Format("SORT = {0} OR ", (Int32)_credit_type);
        }
        _filter += "SORT = -1";
      }

      if (String.IsNullOrEmpty(_filter))
      {
        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
        {
          _filter = String.Format("SORT <> {0}", (Int32)ACCOUNT_PROMO_CREDIT_TYPE.POINT);
        }
        if (Common.TITO.Utils.IsTitoMode())
        {
          if (String.IsNullOrEmpty(_filter) && !chk_pt.Enabled)
          {
            _filter += String.Format("SORT <> {0} ", (Int32)ACCOUNT_PROMO_CREDIT_TYPE.POINT);
          }
          if (!String.IsNullOrEmpty(_filter))
          {
            _filter += " AND ";
          }
          _filter += String.Format("SORT <> {0} ", (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR2);
        }
      }
      m_filter = _filter;
      FilterChange(this);
      pnl_info_promos.Focus();
    }

    private void FilterCreditChecked(CheckBox CheckBoxFilter)
    {
      if (CheckBoxFilter.Name.Equals("chk_pt") && GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
      {
        return;
      }

      switch (CheckBoxFilter.Name)
      {
        case "chk_nr":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoNR));
          break;
        case "chk_ca":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoLock));
          break;
        case "chk_re":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoReedemable));
          break;
        case "chk_pt":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoPoints));
          break;
        default:
          break;
      }

      CheckBoxFilter.Font = m_font_filter_credit_checked;
      CheckBoxFilter.TextAlign = ContentAlignment.MiddleCenter;
    }

    private void FilterCreditUnChecked(CheckBox CheckBoxFilter)
    {
      if (CheckBoxFilter.Name.Equals("chk_pt") && GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
      {
        return;
      }

      switch (CheckBoxFilter.Name)
      {
        case "chk_nr":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoNR_Circle));
          break;
        case "chk_ca":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoLock_Circle));
          break;
        case "chk_re":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoReedemable_Circle));
          break;
        case "chk_pt":
          CheckBoxFilter.Image = ((Image)(Resources.ResourceImages.promoPoints_Circle));
          break;
        default:
          break;
      }

           
      CheckBoxFilter.Font = m_font_filter_credit_unchecked;
      CheckBoxFilter.TextAlign = ContentAlignment.BottomRight;
    }
  }
}
