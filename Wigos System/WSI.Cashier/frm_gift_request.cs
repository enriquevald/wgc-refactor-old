//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_gift_request.cs
// 
//   DESCRIPTION : Implements the following classes:
//                 1. frm_gift_request
//
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-SEP-2010 TJG    Gift request
// 29-OCT-2010 TJG    New gift: Points per draw numbers
// 25-MAY-2012 SSC    Moved CardData, PlaySession and PlayerTrackingData classes to WSI.Common
// 29-MAY-2012 SSC    Moved LoyaltyProgram, Gift and GiftInstance classes to WSI.Common
// 06-FEB-2013 QMP    Check PointsStatus value to allow/deny gift request
// 12-MAY-2013 JMA    Added gift redeem limit functionallity
// 03-JUL-2013 RRR    Moddified view parameters for Window Mode
// 21-AUG-2013 JMM    Fixed Bug WIG-137: Gifts request screen doesn't generate activity.
// 02-JUL-2014 AMF    Personal Card Replacement for points
// 12-NOV-2015 SGB    Backlog Item WIG-5859: Change designer
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
// 29-MAR-2016 ESE    Bug 10718: New Cashier: label singular/plural in gifts
// 24-APR-2017 RGR    Fixed Bug 25322: Error in Credit Exchange
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Globalization;

namespace WSI.Cashier
{
  public partial class frm_gift_request : Controls.frm_base
  {
    #region Enum

    public enum INPUT_MODE
    {
      GIFT = 1,
      POINTS = 2,
    }

    #endregion

    #region Constants
    #endregion

    #region Structures
    #endregion

    #region Attributes

    private frm_yesno form_yes_no;

    private static String m_decimal_str = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    private static Boolean m_cancel = true;
    private static GiftInstanceCashier m_gift_instance;
    private static Boolean m_enter_points_amount = false;
    private static INPUT_MODE m_input_mode = INPUT_MODE.GIFT;
    private static string[] m_voucher_account_info_points;
    private static Int32 m_initial_points;
    private static Int32 m_subtract_points;
    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_gift_request(GiftCashier GiftToRequest, CardData CardToUse)
    {
      InitializeComponent();

      InitControls();

      InitializeControlResources();

      m_gift_instance = new GiftInstanceCashier(GiftToRequest, CardToUse);
      m_gift_instance.IsCashier = true;

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      InitGiftRequest();
    }

    public frm_gift_request(Int32 SubtractPoints, CardData CardToUse)
    {
      Int32 _final_points;

      m_input_mode = INPUT_MODE.POINTS;

      InitializeComponent();

      InitControls();

      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.6;

      InitGiftRequest();

      _final_points = (Int32)CardToUse.PlayerTracking.TruncatedPoints - SubtractPoints;
      lbl_acc_starting_points.Text = CardToUse.PlayerTracking.TruncatedPoints.ToString(Gift.FORMAT_POINTS);
      lbl_acc_gift_points.Text = SubtractPoints.ToString(Gift.FORMAT_POINTS);
      lbl_acc_final_points.Text = _final_points.ToString(Gift.FORMAT_POINTS);

      lbl_acc_gift_points_prefix.Text = Resource.String("STR_FRM_GIFT_REQUEST_CARD") + ":";      // "Tarjeta"
      lbl_gift_limits.Visible = false;

      m_voucher_account_info_points = CardToUse.VoucherAccountInfo();
      m_initial_points = (Int32)CardToUse.PlayerTracking.TruncatedPoints;
      m_subtract_points = SubtractPoints;

      PreviewVoucher();
    }

    #endregion

    #region Buttons

    private void btn_num_1_Click(object sender, EventArgs e)
    {
      AddNumber("1");
    }

    private void btn_num_2_Click(object sender, EventArgs e)
    {
      AddNumber("2");
    }

    private void btn_num_3_Click(object sender, EventArgs e)
    {
      AddNumber("3");
    }

    private void btn_num_4_Click(object sender, EventArgs e)
    {
      AddNumber("4");
    }

    private void btn_num_5_Click(object sender, EventArgs e)
    {
      AddNumber("5");
    }

    private void btn_num_6_Click(object sender, EventArgs e)
    {
      AddNumber("6");
    }

    private void btn_num_7_Click(object sender, EventArgs e)
    {
      AddNumber("7");
    }

    private void btn_num_8_Click(object sender, EventArgs e)
    {
      AddNumber("8");
    }

    private void btn_num_9_Click(object sender, EventArgs e)
    {
      AddNumber("9");
    }

    private void btn_num_10_Click(object sender, EventArgs e)
    {
      AddNumber("0");
    }

    private void btn_num_dot_Click(object sender, EventArgs e)
    {
      // Ignore decimal separator  

      //if ( ! m_enter_points_amount )
      //{
      //  return;
      //}
      ///
      //// Only one decimal separator is allowed
      //if (txt_points_amount.Text.IndexOf(m_decimal_str) == -1)
      //{
      //  txt_points_amount.Text = txt_points_amount.Text + m_decimal_str;
      //}

      //if ( m_enter_points_amount )
      //{
      //  RefreshBalance (false);  // TakeAllAccountPoints = false
      //}

      btn_intro.Focus();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      m_cancel = true;

      Misc.WriteLog("[FORM CLOSE] frm_gift_request (cancel)", Log.Type.Message);
      this.Close();
    }

    private void btn_back_Click(object sender, EventArgs e)
    {
      if (!m_enter_points_amount)
      {
        return;
      }

      if (txt_points_amount.Text.Length > 0)
      {
        txt_points_amount.Text = txt_points_amount.Text.Substring(0, txt_points_amount.Text.Length - 1);
      }

      if (m_enter_points_amount)
      {
        RefreshBalance(false);     // TakeAllAccountPoints = false
      }

      btn_intro.Focus();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the ok button, which confirms all shown data
    //           and closes the screen.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_ok_Click(object sender, EventArgs e)
    {
      // RCI 16-MAY-2011: Don't allow to request more than one gift per click.
      btn_ok.Enabled = false;

      {
        CardData _card_data = new CardData();

        // Get the latest Card data
        if (!CardData.DB_CardGetAllData(m_gift_instance.RelatedCard.AccountId, _card_data))
        {
          Log.Error("frm_gift_request.btn_ok_Click: DB_GetCardAllData. Error reading card.");
        }
        else
        {
          m_gift_instance.RelatedCard = _card_data;
        }
      }

      // QMP 06-FEB-2013: Allow/deny gift request depending on PointsStatus
      if (m_gift_instance.RelatedCard.PointsStatus == ACCOUNT_POINTS_STATUS.REDEEM_NOT_ALLOWED)
      {
        m_gift_instance.Message(GiftInstance.GIFT_INSTANCE_MSG.PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS, this.ParentForm);

        m_cancel = true;

        Misc.WriteLog("[FORM CLOSE] frm_gift_request (ok - pointstatus = not allowed)", Log.Type.Message);
        this.Close();

        return;
      }

      // Check again that the gift can be delivered (stock issue)
      if (!m_gift_instance.CheckGiftRequestCashier(this.ParentForm))
      {
        m_cancel = true;

        Misc.WriteLog("[FORM CLOSE] frm_gift_request (ok - not m_gift_instance.checkgiftrequestcashier)", Log.Type.Message);
        this.Close();

        return;
      }

      GiftInstance.GIFT_INSTANCE_MSG _err_msg;

      if (!m_gift_instance.Request(out _err_msg))
      {
        // Process the selected gift request
        m_gift_instance.Message(_err_msg, this.ParentForm);

        this.Focus();
      }
      else
      {
        // Show the printed voucher containing draw tickets (a kind of informative pop-up)
        if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS)
        {
          // TODO: AJQ

          //frm_draw_voucher _draw_management = new frm_draw_voucher (m_gift_instance.DrawTicket);
          //frm_yesno _shader = new frm_yesno ();

          //_shader.Opacity = 0.6;
          //_shader.Show (this);

          //if ( !_draw_management.IsDisposed )
          //{
          //  _draw_management.ShowDialog ();

          //  _draw_management.Hide ();
          //  _draw_management.Dispose ();
          //}

          //// RCI 03-NOV-2010: Hide and Dispose the shader.
          //_shader.Hide ();
          //_shader.Dispose ();
        }
      }

      m_cancel = false;

      Misc.WriteLog("[FORM CLOSE] frm_gift_request (ok)", Log.Type.Message);
      this.Close();
    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Handle the click on the intro button, which confirms the amount on the
    //           numeric pad.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void btn_intro_Click(object sender, EventArgs e)
    {
      RefreshBalance(false);   // TakeAllAccountPoints = false

      // Check before proceeding
      //    - Amount of points selected is enough to convert to NRC or to RC

      //    - Amount of points selected is enough to convert to NRC or to RC
      switch (m_gift_instance.RelatedGift.Type)
      {
        // RCI 02-DEC-2013: For CREDITS gifts, do not check NotRedeemableCredits < 1, check NumUnits < 1 (like DRAW_NUMBERS).
        //                  Must accept for exemple, NotRedeemableCredits = $0.20.
        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
        case GIFT_TYPE.REDEEMABLE_CREDIT:
        case GIFT_TYPE.DRAW_NUMBERS:
          if (Math.Truncate(Decimal.Parse(txt_points_amount.Text) / m_gift_instance.RelatedGift.Points) > m_gift_instance.NumAvailable)
          {
            m_gift_instance.Message(GiftInstance.GIFT_INSTANCE_MSG.REDEEM_LIMIT_EXCEEDED, this.ParentForm);

            txt_points_amount.Focus();
            btn_ok.Enabled = false;

            return;
          }
          if (m_gift_instance.NumUnits < 1)
          {
            m_gift_instance.Message(GiftInstance.GIFT_INSTANCE_MSG.NOT_ENOUGH_POINTS_TO_CONVERT_NRC, this.ParentForm);

            txt_points_amount.Focus();
            btn_ok.Enabled = false;

            return;
          }
          break;

        default:
          break;
      }

      // RCI 21-JUN-2011: Check the gift can be delivered.
      //     For DRAW_NUMBERS type, make the Preview Voucher to show real spent points. Spent points can be changed by draw limits.
      if (!m_gift_instance.CheckGiftRequestCashier(this.ParentForm))
      {
        m_cancel = true;
        btn_ok.Enabled = false;

        return;
      }

      // Generate voucher in preview mode
      PreviewVoucher();

      btn_ok.Enabled = true;
      btn_ok.Focus();
    } // btn_intro_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Generate a preview version of the voucher to be generated.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private void PreviewVoucher()
    {
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;
      String _voucher_file_name;
      ArrayList _voucher_list;
      Int32 _gift_expiration_days;

      _sql_conn = null;
      _sql_trx = null;

      _voucher_file_name = "";
      web_browser.Navigate(_voucher_file_name);

      try
      {
        // Get Sql Connection 
        _sql_conn = WSI.Common.WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        switch (m_input_mode)
        {
          case INPUT_MODE.GIFT:
          default:
            // Gift voucher 
            //    - OperationId, GiftInstanceId and SiteId are not required when Previewing the voucher
            _gift_expiration_days = Convert.ToInt32(WSI.Common.Misc.ReadGeneralParams("PlayerTracking", "GiftExpirationDays"));

            _voucher_list = VoucherBuilder.GiftRequest(0,                                                            // OperationId
                                                        m_gift_instance.RelatedCard.VoucherAccountInfo(),
                                                        m_gift_instance.RelatedCard.PlayerTracking.TruncatedPoints,   // Initial Points Balance
                                                        m_gift_instance.SpentPoints,                                  // Spent Points
                                                        m_gift_instance.RelatedGift.Type,                             // Gift Type
                                                        m_gift_instance.RelatedGift.GiftName,                         // Gift Name
                                                        "",                                                           // Gift Number
                                                        m_gift_instance.NotRedeemableCredits,                         // Not Redeemable Credits
                                                        m_gift_instance.NumUnits,                                     // Number of gift units
                                                        WGDB.Now.AddDays(_gift_expiration_days),
                                                        PrintMode.Preview,
                                                        _sql_trx);

            _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);

            break;
          case INPUT_MODE.POINTS:
            // Card Replacement voucher 
            _voucher_list = VoucherBuilder.PointToCardReplacement(0,                              // OperationId
                                                                  m_voucher_account_info_points,  // Account Info
                                                                  m_initial_points,               // Initial Points Balance
                                                                  m_subtract_points,              // Spent Points
                                                                  PrintMode.Preview,
                                                                  _sql_trx);

            _voucher_file_name = VoucherPrint.GenerateFileForPreview(_voucher_list);

            break;
        }
      }
      catch (Exception _ex)
      {
        _sql_trx.Rollback();
        Log.Exception(_ex);
      }

      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

      web_browser.Navigate(_voucher_file_name);
    } // PreviewVoucher

    #endregion Buttons

    #region Public Methods

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="CaptionText"></param>
    /// <param name="Amount"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>

    public Boolean Show(Form ParentForm)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_gifts_request", Log.Type.Message);
      Boolean _show_dialog;

      switch (m_input_mode)
      {
        case INPUT_MODE.GIFT:
        default:
          lbl_gift_name.Text = "";
          lbl_gift_value.Text = "";
          txt_points_amount.Text = "";
          lbl_acc_starting_points.Text = "";
          lbl_acc_gift_points.Text = "";
          lbl_acc_final_points.Text = "";

          m_enter_points_amount = (m_gift_instance.RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
                                 || m_gift_instance.RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT
                                 || m_gift_instance.RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS);

          // At this point, NumUnits = 0, so it's not necessary in Check.
          // We need it when the user requests the gift (in the btn_intro_Click). NumUnits will be set in RefreshBalance() after this initial check.
          m_gift_instance.NumUnits = 0;

          if (!m_gift_instance.CheckGiftRequestCashier(this.ParentForm))
          {
            m_cancel = true;

            this.Close();

            return false;
          }

          RefreshBalance(true);    // TakeAllAccountPoints = true

          gp_digits_box.Enabled = m_enter_points_amount;

          _show_dialog = true;
          if (!m_enter_points_amount)
          {
            btn_intro_Click(null, null);

            _show_dialog = btn_ok.Enabled;
          }

          if (_show_dialog)
          {
            form_yes_no.Show();
            this.ShowDialog(form_yes_no);
            form_yes_no.Hide();
          }

          if (m_cancel)
          {
            return false;
          }

          break;
        case INPUT_MODE.POINTS:
          gp_digits_box.Enabled = false;
          gp_digits_box.Visible = false;
          gp_gift.Enabled = false;
          gp_gift.Visible = false;
          gp_account.Location = new System.Drawing.Point(7, 2);

          form_yes_no.Show();
          this.ShowDialog(form_yes_no);
          form_yes_no.Hide();

          if (m_cancel)
          {
            return false;
          }

          break;
      }

      return true;
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Add event handler to scan and configure buttons
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private void InitControls()
    {
      EventLastAction.AddEventLastAction(this.Controls);

      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      this.btn_intro.Click += new System.EventHandler(this.btn_intro_Click);

      this.btn_num_1.Click += new System.EventHandler(this.btn_num_1_Click);
      this.btn_num_2.Click += new System.EventHandler(this.btn_num_2_Click);
      this.btn_num_3.Click += new System.EventHandler(this.btn_num_3_Click);
      this.btn_num_4.Click += new System.EventHandler(this.btn_num_4_Click);
      this.btn_num_5.Click += new System.EventHandler(this.btn_num_5_Click);
      this.btn_num_6.Click += new System.EventHandler(this.btn_num_6_Click);
      this.btn_num_7.Click += new System.EventHandler(this.btn_num_7_Click);
      this.btn_num_8.Click += new System.EventHandler(this.btn_num_8_Click);
      this.btn_num_9.Click += new System.EventHandler(this.btn_num_9_Click);
      this.btn_num_10.Click += new System.EventHandler(this.btn_num_10_Click);
      this.btn_num_dot.Click += new System.EventHandler(this.btn_num_dot_Click);

      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
    }

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings
      //    - Labels
      this.FormTitle = Resource.String("STR_FRM_GIFT_REQUEST_001");          // "Gift Request"

      gp_gift.HeaderText = Resource.String("STR_FRM_GIFT_REQUEST_002");                         // "Selected Gift"

      gp_account.HeaderText = Resource.String("STR_FRM_GIFT_REQUEST_004");                      // "Cuenta"
      lbl_acc_starting_points_prefix.Text = Resource.String("STR_FRM_GIFT_REQUEST_005") + ":";  // "Balance Inicial"
      lbl_acc_gift_points_prefix.Text = Resource.String("STR_FRM_GIFT_REQUEST_006") + ":";      // "Regalo"
      lbl_acc_final_points_prefix.Text = Resource.String("STR_FRM_GIFT_REQUEST_007") + ":";     // "Balance Final"
      lbl_acc_starting_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");  // "points"
      lbl_acc_gift_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");      // "points"
      lbl_acc_final_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");     // "points"

      lbl_info.Text = Resource.String("STR_FRM_GIFT_REQUEST_003");                   // "Points To Convert"

      //    - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //   - Textbox
      gp_digits_box.HeaderBackColor = WSI.Cashier.CashierStyle.Colors.m_colors_dic[CashierStyle.Colors.ENUM_COLORS.HEX_373B3F];
      gp_digits_box.HeaderHeight = 55;

    } // InitializeControlResources

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void InitGiftRequest()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, 0);
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Get label to display on gift redeem view
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //  
    // RETURNS :
    //          - String with display label
    //   NOTES :

    private String GetGiftRedeemLimitMsg()
    {
      String _result;

      if (m_gift_instance.NumAvailable > 0 && (m_gift_instance.NumAvailable < Int32.MaxValue))
      {
        if (m_gift_instance.NumUnits == 1)
        {
          //Show message in singular
          _result = Resource.String(("STR_FRM_GIFT_REQUEST_013_SINGULAR"), m_gift_instance.NumUnits, m_gift_instance.NumAvailable);
        }
        else
        {
          //Show message in pluar
          _result = Resource.String(("STR_FRM_GIFT_REQUEST_013"), m_gift_instance.NumUnits, m_gift_instance.NumAvailable);
        }
      }
      else
      {
        if (m_gift_instance.NumUnits == 1)
        {
          //Show message in singular          
          _result = Resource.String(("STR_FRM_GIFT_REQUEST_014_SINGULAR"), m_gift_instance.NumUnits, m_gift_instance.NumAvailable);
        }
        else
        {
          //Show message in pluar
          _result = Resource.String(("STR_FRM_GIFT_REQUEST_014"), m_gift_instance.NumUnits, m_gift_instance.NumAvailable);
        }
      }

      return _result;
    } // GetGiftRedeemLimitMsg

    private void RefreshBalance(Boolean TakeAllAccountPoints)
    {
      Decimal _acc_points_before;
      Decimal _acc_points_after;
      Decimal _points_to_spend;
      string[] _message_params = { "", "", "", "", "", "" };

      btn_ok.Enabled = false;
      //web_browser.Url = null;

      // Get the maximum number of times that this gift can be redeemed

      _acc_points_before = m_gift_instance.RelatedCard.PlayerTracking.TruncatedPoints;

      if (TakeAllAccountPoints)
      {
        _points_to_spend = _acc_points_before;
      }
      else
      {
        Decimal _input_amount;

        _input_amount = 0;

        // Check the amount entered
        if (txt_points_amount.Text != "")
        {
          try
          {
            _input_amount = Decimal.Parse(txt_points_amount.Text);
          }
          catch
          {
            _input_amount = 0;
          }
        }

        if (_input_amount < 0)
        {
          _input_amount = 0;
        }

        _points_to_spend = _input_amount;
      }

      m_gift_instance.DB_GetNumOfAvailableGifts();

      m_gift_instance.GiftBalance(_points_to_spend);

      // When the user introduces a value, check NumAvailables. TakeAllAccountPoints is false when this happens.
      if (!TakeAllAccountPoints)
      {
        if (m_gift_instance.NumAvailable > 0 && Math.Truncate(_points_to_spend / m_gift_instance.RelatedGift.Points) > m_gift_instance.NumAvailable)
        {
          // Number requested is bigger than number available for redeem 
          m_gift_instance.NumUnits = 0;
          m_gift_instance.SpentPoints = 0;
          m_gift_instance.NotRedeemableCredits = 0;
        }
      }

      lbl_gift_limits.Text = GetGiftRedeemLimitMsg();

      lbl_gift_name.Text = m_gift_instance.RelatedGift.GiftName;

      // @p0 puntos
      _message_params[0] = m_gift_instance.RelatedGift.Points.ToString(Gift.FORMAT_POINTS);

      //Set the gift value in singular/plurar
      if (m_gift_instance.RelatedGift.Points == 1)
      {
        lbl_gift_value.Text = Resource.String(("STR_FRM_GIFT_REQUEST_015"), _message_params); // "point"
      }
      else
      {
        lbl_gift_value.Text = Resource.String(("STR_FRM_GIFT_REQUEST_011"), _message_params); // "points"
      }

      if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.OBJECT)
      {
        // Gift = Object

        lbl_gift_items.Text = "";

        txt_points_amount.Text = "";
      }
      else if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT)
      {
        // Gift = NRC

        // @p0 cr�ditos no redimibles
        _message_params[0] = m_gift_instance.NotRedeemableCredits.ToString();
        if (decimal.Parse(m_gift_instance.NotRedeemableCredits.ToStringWithoutSymbols()) == 1)
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_009_SINGULAR"), _message_params); //cr�dito no redimible
        }
        else
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_009"), _message_params); //cr�ditos no redimibles
        }

        if (TakeAllAccountPoints)
        {
          txt_points_amount.Text = m_gift_instance.SpentPoints.ToString("###0");
        }
      }
      else if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.REDEEMABLE_CREDIT)
      {
        // Gift = Redeem Credits

        // @p0 cr�ditos redimibles
        _message_params[0] = m_gift_instance.NotRedeemableCredits.ToString();
        if (decimal.Parse(m_gift_instance.NotRedeemableCredits.ToStringWithoutSymbols()) == 1)
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_012_SINGULAR"), _message_params);// @p0 cr�dito redimible
        }
        else
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_012"), _message_params);// @p0 cr�ditos redimibles
        }

        if (TakeAllAccountPoints)
        {
          txt_points_amount.Text = m_gift_instance.SpentPoints.ToString("###0");
        }
      }
      else if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.DRAW_NUMBERS)
      {
        // Gift = Draw Numbers

        // @p0 n�meros
        _message_params[0] = m_gift_instance.NumUnits.ToString();
        if (m_gift_instance.NumUnits == 1)
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_010_SINGULAR"), _message_params);// @p0 n�mero
        }
        else
        {
          lbl_gift_items.Text = Resource.String(("STR_FRM_GIFT_REQUEST_010"), _message_params);// @p0 n�meros
        }

        if (TakeAllAccountPoints)
        {
          txt_points_amount.Text = m_gift_instance.SpentPoints.ToString("###0");
        }
      }
      else if (m_gift_instance.RelatedGift.Type == GIFT_TYPE.SERVICES)
      {
        // Gift = Services

        lbl_gift_items.Text = "";

        txt_points_amount.Text = "";
      }

      _acc_points_after = _acc_points_before - m_gift_instance.SpentPoints;

      //Set the spent point in singular/plural
      if (m_gift_instance.SpentPoints == 1)
      {
        lbl_acc_gift_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_016");      // "point"
      }
      else
      {
        lbl_acc_gift_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");      // "points"
      }

      //Set the starting points in singular/plural
      if (_acc_points_before == 1)
      {
        lbl_acc_starting_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_016");  // "point"
      }
      else
      {
        lbl_acc_starting_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");  // "points"
      }

      //Set the final points in singular/plural
      if (_acc_points_after == 1)
      {
        lbl_acc_final_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_016");     // "point"
      }
      else
      {
        lbl_acc_final_points_suffix.Text = Resource.String("STR_FRM_GIFT_REQUEST_008");     // "points"
      }

      lbl_acc_starting_points.Text = _acc_points_before.ToString(Gift.FORMAT_POINTS);
      lbl_acc_gift_points.Text = m_gift_instance.SpentPoints.ToString(Gift.FORMAT_POINTS);
      lbl_acc_final_points.Text = _acc_points_after.ToString(Gift.FORMAT_POINTS);

      return;
    } // RefreshBalance

    /// <summary>
    /// Check and Add digit to amount
    /// </summary>
    /// <param name="NumberStr"></param>
    private void AddNumber(String NumberStr)
    {
      String aux_str;
      String tentative_number;
      Currency input_amount;
      int dec_symbol_pos;

      if (!m_enter_points_amount)
      {
        return;
      }

      if (txt_points_amount.Text.Length == txt_points_amount.SelectionLength)
      {
        txt_points_amount.Text = "";
      }

      // Prevent exceeding maximum number of decimals (2)
      dec_symbol_pos = txt_points_amount.Text.IndexOf(m_decimal_str);
      if (dec_symbol_pos > -1)
      {
        aux_str = txt_points_amount.Text.Substring(dec_symbol_pos);
        if (aux_str.Length > 2)
        {
          return;
        }
      }

      // Prevent exceeding maximum amount length
      if (txt_points_amount.Text.Length >= Cashier.MAX_ALLOWED_AMOUNT_LENGTH)
      {
        return;
      }

      tentative_number = txt_points_amount.Text + NumberStr;

      try
      {
        input_amount = Decimal.Parse(tentative_number);
        if (input_amount > Cashier.MAX_ALLOWED_AMOUNT)
        {
          return;
        }
      }
      catch
      {
        input_amount = 0;

        return;
      }

      // Remove unneeded leading zeros
      // Unless we are entering a decimal value, there must not be any leading 0
      if (dec_symbol_pos == -1)
      {
        txt_points_amount.Text = txt_points_amount.Text.TrimStart('0') + NumberStr;
      }
      else
      {
        // Accept new symbol/digit 
        txt_points_amount.Text = txt_points_amount.Text + NumberStr;
      }

      if (m_enter_points_amount)
      {
        RefreshBalance(false);  // TakeAllAccountPoints = false
      }

      btn_intro.Focus();
    }

    private void frm_gift_request_Shown(object sender, EventArgs e)
    {
      this.txt_points_amount.Focus();
    }

    private void frm_gift_request_KeyPress(object sender, KeyPressEventArgs e)
    {
      String aux_str;
      char c;

      c = e.KeyChar;

      if (c >= '0' && c <= '9')
      {
        aux_str = "" + c;
        AddNumber(aux_str);

        e.Handled = true;
      }

      if (c == '\r')
      {
        btn_intro_Click(null, null);

        e.Handled = true;
      }

      if (c == '\b')
      {
        btn_back_Click(null, null);

        e.Handled = true;
      }

      if (c == '.')
      {
        btn_num_dot_Click(null, null);

        e.Handled = true;
      }

      e.Handled = false;
    }

    private void splitContainer1_KeyPress(object sender, KeyPressEventArgs e)
    {
      frm_gift_request_KeyPress(sender, e);
    }

    private void frm_gift_request_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
    {
      char c;

      c = Convert.ToChar(e.KeyCode);
      if (c == '\r')
      {
        e.IsInputKey = true;
      }
    }


  }

    #endregion
}