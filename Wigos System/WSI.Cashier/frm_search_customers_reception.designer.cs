﻿using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  partial class FrmSearchCustomersReception
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {

      if (m_camera_module != null)
      {
        m_camera_module.Dispose();
        m_camera_module = null;
      }
      if(IDScanner!=null)
        IDScanner.DetatchFromAllAvailable(null);

      EventLastAction.RemoveEventLastAction(Controls);
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearchCustomersReception));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn_visits_report = new WSI.Cashier.Controls.uc_round_button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.btn_escanear_id = new WSI.Cashier.Controls.uc_round_button_long_press();
      this.btn_add_customer = new WSI.Cashier.Controls.uc_round_button();
      this.btn_clear = new WSI.Cashier.Controls.uc_round_button();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.uc_pnl_top = new WSI.Cashier.Controls.uc_round_panel();
      this.img_ticket_scan = new System.Windows.Forms.PictureBox();
      this.uc_voucher_search = new WSI.Cashier.Controls.uc_checkBox();
      this.txt_track_data = new WSI.Cashier.NumericTextBox();
      this.lbl_track_data = new WSI.Cashier.Controls.uc_label();
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.uc_pnl_medium = new WSI.Cashier.Controls.uc_round_panel();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.tlp_name = new System.Windows.Forms.TableLayoutPanel();
      this.txt_name = new WSI.Cashier.CharacterTextBox();
      this.lbl_name = new WSI.Cashier.Controls.uc_label();
      this.tlp_name2 = new System.Windows.Forms.TableLayoutPanel();
      this.txt_name2 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name2 = new WSI.Cashier.Controls.uc_label();
      this.tlp_surname = new System.Windows.Forms.TableLayoutPanel();
      this.txt_lastname1 = new WSI.Cashier.CharacterTextBox();
      this.lbl_lastname1 = new WSI.Cashier.Controls.uc_label();
      this.tlp_surname2 = new System.Windows.Forms.TableLayoutPanel();
      this.txt_lastname2 = new WSI.Cashier.CharacterTextBox();
      this.lbl_lastname2 = new WSI.Cashier.Controls.uc_label();
      this.tlp_sex = new System.Windows.Forms.TableLayoutPanel();
      this.uc_lbl_sex = new WSI.Cashier.Controls.uc_label();
      this.uc_cb_gender = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.tlp_document = new System.Windows.Forms.TableLayoutPanel();
      this.lbl_id = new WSI.Cashier.Controls.uc_label();
      this.txt_id = new WSI.Cashier.Controls.uc_round_textbox();
      this.tlp_dob = new System.Windows.Forms.TableLayoutPanel();
      this.uc_lbl_birth_date = new WSI.Cashier.Controls.uc_label();
      this.uc_dt_birth_date = new WSI.Cashier.uc_datetime();
      this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.characterTextBox1 = new WSI.Cashier.CharacterTextBox();
      this.uc_label1 = new WSI.Cashier.Controls.uc_label();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.characterTextBox2 = new WSI.Cashier.CharacterTextBox();
      this.uc_label2 = new WSI.Cashier.Controls.uc_label();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.characterTextBox3 = new WSI.Cashier.CharacterTextBox();
      this.uc_label3 = new WSI.Cashier.Controls.uc_label();
      this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
      this.uc_label4 = new WSI.Cashier.Controls.uc_label();
      this.uc_round_auto_combobox1 = new WSI.Cashier.Controls.uc_round_auto_combobox();
      this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
      this.uc_label5 = new WSI.Cashier.Controls.uc_label();
      this.uc_round_textbox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
      this.uc_datetime1 = new WSI.Cashier.uc_datetime();
      this.uc_label6 = new WSI.Cashier.Controls.uc_label();
      this.lbl_fullname2 = new WSI.Cashier.Controls.uc_label();
      this.lbl_fullname = new WSI.Cashier.Controls.uc_label();
      this.pnl_message = new System.Windows.Forms.Panel();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.uc_grid_entries = new WSI.Cashier.Controls.uc_DataGridView();
      this.uc_pnl_grid = new WSI.Cashier.Controls.uc_round_panel();
      this.pnl_grid_container = new System.Windows.Forms.Panel();
      this.pnl_data.SuspendLayout();
      this.panel1.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.uc_pnl_top.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.img_ticket_scan)).BeginInit();
      this.uc_pnl_medium.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.tlp_name.SuspendLayout();
      this.tlp_name2.SuspendLayout();
      this.tlp_surname.SuspendLayout();
      this.tlp_surname2.SuspendLayout();
      this.tlp_sex.SuspendLayout();
      this.tlp_document.SuspendLayout();
      this.tlp_dob.SuspendLayout();
      this.flowLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.tableLayoutPanel4.SuspendLayout();
      this.tableLayoutPanel5.SuspendLayout();
      this.tableLayoutPanel6.SuspendLayout();
      this.tableLayoutPanel7.SuspendLayout();
      this.pnl_message.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.uc_grid_entries)).BeginInit();
      this.uc_pnl_grid.SuspendLayout();
      this.pnl_grid_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.BackColor = System.Drawing.Color.WhiteSmoke;
      this.pnl_data.Controls.Add(this.pnl_grid_container);
      this.pnl_data.Controls.Add(this.pnl_message);
      this.pnl_data.Controls.Add(this.uc_pnl_medium);
      this.pnl_data.Controls.Add(this.uc_pnl_top);
      this.pnl_data.Controls.Add(this.panel1);
      this.pnl_data.Size = new System.Drawing.Size(1024, 658);
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.panel1.Controls.Add(this.btn_visits_report);
      this.panel1.Controls.Add(this.tableLayoutPanel1);
      this.panel1.Controls.Add(this.btn_cancel);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(138, 658);
      this.panel1.TabIndex = 1058;
      // 
      // btn_visits_report
      // 
      this.btn_visits_report.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_visits_report.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_visits_report.FlatAppearance.BorderSize = 0;
      this.btn_visits_report.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_visits_report.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_visits_report.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_visits_report.Image = null;
      this.btn_visits_report.IsSelected = false;
      this.btn_visits_report.Location = new System.Drawing.Point(8, 520);
      this.btn_visits_report.Name = "btn_visits_report";
      this.btn_visits_report.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_visits_report.Size = new System.Drawing.Size(121, 60);
      this.btn_visits_report.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_visits_report.TabIndex = 15;
      this.btn_visits_report.TabStop = false;
      this.btn_visits_report.Text = "XCLEAR";
      this.btn_visits_report.UseVisualStyleBackColor = false;
      this.btn_visits_report.Click += new System.EventHandler(this.btn_visits_report_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.btn_escanear_id, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.btn_add_customer, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.btn_clear, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.btn_keyboard, 0, 3);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 6);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 5;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(127, 264);
      this.tableLayoutPanel1.TabIndex = 14;
      // 
      // btn_escanear_id
      // 
      this.btn_escanear_id.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_escanear_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_escanear_id.Enabled = false;
      this.btn_escanear_id.FlatAppearance.BorderSize = 0;
      this.btn_escanear_id.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_escanear_id.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_escanear_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_escanear_id.Image = null;
      this.btn_escanear_id.IsSelected = false;
      this.btn_escanear_id.Location = new System.Drawing.Point(3, 3);
      this.btn_escanear_id.Name = "btn_escanear_id";
      this.btn_escanear_id.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_escanear_id.Size = new System.Drawing.Size(121, 60);
      this.btn_escanear_id.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_escanear_id.TabIndex = 9;
      this.btn_escanear_id.TabStop = false;
      this.btn_escanear_id.Text = "XESCANEAR ID";
      this.btn_escanear_id.UseVisualStyleBackColor = false;
      this.btn_escanear_id.Visible = false;
      this.btn_escanear_id.LongClick += new System.EventHandler(this.btn_escanear_id_LongClick);
      this.btn_escanear_id.Click += new System.EventHandler(this.btn_escanear_id_Click);
      // 
      // btn_add_customer
      // 
      this.btn_add_customer.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.btn_add_customer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_add_customer.FlatAppearance.BorderSize = 0;
      this.btn_add_customer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_add_customer.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_add_customer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_add_customer.Image = null;
      this.btn_add_customer.IsSelected = false;
      this.btn_add_customer.Location = new System.Drawing.Point(3, 69);
      this.btn_add_customer.Name = "btn_add_customer";
      this.btn_add_customer.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_add_customer.Size = new System.Drawing.Size(121, 60);
      this.btn_add_customer.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_add_customer.TabIndex = 11;
      this.btn_add_customer.TabStop = false;
      this.btn_add_customer.Text = "XADD CUSTOMER";
      this.btn_add_customer.UseVisualStyleBackColor = false;
      this.btn_add_customer.Click += new System.EventHandler(this.btn_add_customer_Click);
      // 
      // btn_clear
      // 
      this.btn_clear.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_clear.FlatAppearance.BorderSize = 0;
      this.btn_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_clear.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_clear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_clear.Image = null;
      this.btn_clear.IsSelected = false;
      this.btn_clear.Location = new System.Drawing.Point(3, 135);
      this.btn_clear.Name = "btn_clear";
      this.btn_clear.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_clear.Size = new System.Drawing.Size(121, 60);
      this.btn_clear.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_clear.TabIndex = 10;
      this.btn_clear.TabStop = false;
      this.btn_clear.Text = "XCLEAR";
      this.btn_clear.UseVisualStyleBackColor = false;
      this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.IsSelected = false;
      this.btn_keyboard.Location = new System.Drawing.Point(26, 201);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_keyboard.Size = new System.Drawing.Size(75, 55);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 8;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(8, 586);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(121, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 12;
      this.btn_cancel.TabStop = false;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.Transparent;
      this.btn_ok.CornerRadius = 0;
      this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(114)))), ((int)(((byte)(121)))));
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = global::WSI.Cashier.Properties.Resources.btnSearch;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(787, 54);
      this.btn_ok.Margin = new System.Windows.Forms.Padding(0);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.Empty;
      this.btn_ok.Size = new System.Drawing.Size(55, 55);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.NONE;
      this.btn_ok.TabIndex = 1;
      this.btn_ok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // uc_pnl_top
      // 
      this.uc_pnl_top.BackColor = System.Drawing.Color.WhiteSmoke;
      this.uc_pnl_top.BorderColor = System.Drawing.Color.Empty;
      this.uc_pnl_top.Controls.Add(this.img_ticket_scan);
      this.uc_pnl_top.Controls.Add(this.btn_ok);
      this.uc_pnl_top.Controls.Add(this.uc_voucher_search);
      this.uc_pnl_top.Controls.Add(this.txt_track_data);
      this.uc_pnl_top.Controls.Add(this.lbl_track_data);
      this.uc_pnl_top.Controls.Add(this.uc_card_reader1);
      this.uc_pnl_top.CornerRadius = 1;
      this.uc_pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
      this.uc_pnl_top.HeaderBackColor = System.Drawing.Color.Empty;
      this.uc_pnl_top.HeaderForeColor = System.Drawing.Color.Empty;
      this.uc_pnl_top.HeaderHeight = 0;
      this.uc_pnl_top.HeaderSubText = null;
      this.uc_pnl_top.HeaderText = null;
      this.uc_pnl_top.Location = new System.Drawing.Point(138, 0);
      this.uc_pnl_top.Name = "uc_pnl_top";
      this.uc_pnl_top.PanelBackColor = System.Drawing.Color.Empty;
      this.uc_pnl_top.Size = new System.Drawing.Size(886, 124);
      this.uc_pnl_top.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.uc_pnl_top.TabIndex = 1059;
      this.uc_pnl_top.Paint += new System.Windows.Forms.PaintEventHandler(this.uc_pnl_top_Paint);
      // 
      // img_ticket_scan
      // 
      this.img_ticket_scan.Image = ((System.Drawing.Image)(resources.GetObject("img_ticket_scan.Image")));
      this.img_ticket_scan.Location = new System.Drawing.Point(45, 67);
      this.img_ticket_scan.Name = "img_ticket_scan";
      this.img_ticket_scan.Size = new System.Drawing.Size(35, 35);
      this.img_ticket_scan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.img_ticket_scan.TabIndex = 7;
      this.img_ticket_scan.TabStop = false;
      // 
      // uc_voucher_search
      // 
      this.uc_voucher_search.Appearance = System.Windows.Forms.Appearance.Button;
      this.uc_voucher_search.BackColor = System.Drawing.Color.Transparent;
      this.uc_voucher_search.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_voucher_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_voucher_search.Location = new System.Drawing.Point(259, 18);
      this.uc_voucher_search.MinimumSize = new System.Drawing.Size(215, 30);
      this.uc_voucher_search.Name = "uc_voucher_search";
      this.uc_voucher_search.Padding = new System.Windows.Forms.Padding(5);
      this.uc_voucher_search.Size = new System.Drawing.Size(215, 30);
      this.uc_voucher_search.TabIndex = 1028;
      this.uc_voucher_search.TabStop = false;
      this.uc_voucher_search.Text = "x_VoucherSearch";
      this.uc_voucher_search.UseVisualStyleBackColor = false;
      this.uc_voucher_search.CheckedChanged += new System.EventHandler(this.uc_voucher_search_CheckedChanged);
      // 
      // txt_track_data
      // 
      this.txt_track_data.AllowSpace = false;
      this.txt_track_data.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_track_data.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_track_data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_track_data.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_track_data.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_track_data.CornerRadius = 5;
      this.txt_track_data.FillWithCeros = false;
      this.txt_track_data.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_track_data.Location = new System.Drawing.Point(259, 64);
      this.txt_track_data.MaxLength = 50;
      this.txt_track_data.Multiline = false;
      this.txt_track_data.Name = "txt_track_data";
      this.txt_track_data.PasswordChar = '●';
      this.txt_track_data.ReadOnly = false;
      this.txt_track_data.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_track_data.SelectedText = "";
      this.txt_track_data.SelectionLength = 0;
      this.txt_track_data.SelectionStart = 0;
      this.txt_track_data.Size = new System.Drawing.Size(226, 40);
      this.txt_track_data.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_track_data.TabIndex = 1;
      this.txt_track_data.TabStop = false;
      this.txt_track_data.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_track_data.UseSystemPasswordChar = true;
      this.txt_track_data.WaterMark = null;
      this.txt_track_data.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_track_data.TextChanged += new System.EventHandler(this.txt_track_data_TextChanged);
      this.txt_track_data.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_track_data.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // lbl_track_data
      // 
      this.lbl_track_data.BackColor = System.Drawing.Color.Transparent;
      this.lbl_track_data.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_track_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_track_data.Location = new System.Drawing.Point(130, 67);
      this.lbl_track_data.Name = "lbl_track_data";
      this.lbl_track_data.Size = new System.Drawing.Size(105, 37);
      this.lbl_track_data.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_track_data.TabIndex = 1027;
      this.lbl_track_data.Text = "XINSERT CARD";
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(32, 54);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(800, 60);
      this.uc_card_reader1.TabIndex = 1029;
      this.uc_card_reader1.OnTrackNumberReadEvent += new WSI.Cashier.uc_card_reader.TrackNumberReadEventHandler(this.uc_card_reader1_OnTrackNumberReadEvent);
      this.uc_card_reader1.Load += new System.EventHandler(this.uc_card_reader1_Load);
      // 
      // uc_pnl_medium
      // 
      this.uc_pnl_medium.BackColor = System.Drawing.Color.WhiteSmoke;
      this.uc_pnl_medium.BorderColor = System.Drawing.Color.Empty;
      this.uc_pnl_medium.Controls.Add(this.flowLayoutPanel1);
      this.uc_pnl_medium.Controls.Add(this.lbl_fullname2);
      this.uc_pnl_medium.Controls.Add(this.lbl_fullname);
      this.uc_pnl_medium.CornerRadius = 1;
      this.uc_pnl_medium.Dock = System.Windows.Forms.DockStyle.Top;
      this.uc_pnl_medium.HeaderBackColor = System.Drawing.Color.Empty;
      this.uc_pnl_medium.HeaderForeColor = System.Drawing.Color.Empty;
      this.uc_pnl_medium.HeaderHeight = 0;
      this.uc_pnl_medium.HeaderSubText = null;
      this.uc_pnl_medium.HeaderText = null;
      this.uc_pnl_medium.Location = new System.Drawing.Point(138, 124);
      this.uc_pnl_medium.Name = "uc_pnl_medium";
      this.uc_pnl_medium.PanelBackColor = System.Drawing.Color.Empty;
      this.uc_pnl_medium.Size = new System.Drawing.Size(886, 213);
      this.uc_pnl_medium.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NONE;
      this.uc_pnl_medium.TabIndex = 1060;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.Controls.Add(this.tlp_name);
      this.flowLayoutPanel1.Controls.Add(this.tlp_name2);
      this.flowLayoutPanel1.Controls.Add(this.tlp_surname);
      this.flowLayoutPanel1.Controls.Add(this.tlp_surname2);
      this.flowLayoutPanel1.Controls.Add(this.tlp_sex);
      this.flowLayoutPanel1.Controls.Add(this.tlp_document);
      this.flowLayoutPanel1.Controls.Add(this.tlp_dob);
      this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
      this.flowLayoutPanel1.Location = new System.Drawing.Point(30, 7);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(824, 148);
      this.flowLayoutPanel1.TabIndex = 1079;
      // 
      // tlp_name
      // 
      this.tlp_name.ColumnCount = 1;
      this.tlp_name.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_name.Controls.Add(this.txt_name, 0, 1);
      this.tlp_name.Controls.Add(this.lbl_name, 0, 0);
      this.tlp_name.Location = new System.Drawing.Point(3, 3);
      this.tlp_name.Name = "tlp_name";
      this.tlp_name.RowCount = 2;
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_name.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
      this.tlp_name.Size = new System.Drawing.Size(200, 68);
      this.tlp_name.TabIndex = 0;
      // 
      // txt_name
      // 
      this.txt_name.AllowSpace = false;
      this.txt_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name.CornerRadius = 5;
      this.txt_name.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name.Location = new System.Drawing.Point(3, 25);
      this.txt_name.MaxLength = 50;
      this.txt_name.Multiline = false;
      this.txt_name.Name = "txt_name";
      this.txt_name.PasswordChar = '\0';
      this.txt_name.ReadOnly = false;
      this.txt_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name.SelectedText = "";
      this.txt_name.SelectionLength = 0;
      this.txt_name.SelectionStart = 0;
      this.txt_name.Size = new System.Drawing.Size(185, 40);
      this.txt_name.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name.TabIndex = 0;
      this.txt_name.TabStop = false;
      this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name.UseSystemPasswordChar = false;
      this.txt_name.WaterMark = null;
      this.txt_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name.TextChanged += new System.EventHandler(this.txt_fullname_TextChanged);
      this.txt_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_name.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // lbl_name
      // 
      this.lbl_name.AutoSize = true;
      this.lbl_name.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name.Location = new System.Drawing.Point(3, 0);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(58, 19);
      this.lbl_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name.TabIndex = 1068;
      this.lbl_name.Text = "xName";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_name2
      // 
      this.tlp_name2.ColumnCount = 1;
      this.tlp_name2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_name2.Controls.Add(this.txt_name2, 0, 1);
      this.tlp_name2.Controls.Add(this.lbl_name2, 0, 0);
      this.tlp_name2.Location = new System.Drawing.Point(209, 3);
      this.tlp_name2.Name = "tlp_name2";
      this.tlp_name2.RowCount = 2;
      this.tlp_name2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tlp_name2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
      this.tlp_name2.Size = new System.Drawing.Size(200, 68);
      this.tlp_name2.TabIndex = 1;
      // 
      // txt_name2
      // 
      this.txt_name2.AllowSpace = false;
      this.txt_name2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_name2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_name2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_name2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_name2.CornerRadius = 5;
      this.txt_name2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_name2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_name2.Location = new System.Drawing.Point(3, 25);
      this.txt_name2.MaxLength = 50;
      this.txt_name2.Multiline = false;
      this.txt_name2.Name = "txt_name2";
      this.txt_name2.PasswordChar = '\0';
      this.txt_name2.ReadOnly = false;
      this.txt_name2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_name2.SelectedText = "";
      this.txt_name2.SelectionLength = 0;
      this.txt_name2.SelectionStart = 0;
      this.txt_name2.Size = new System.Drawing.Size(185, 40);
      this.txt_name2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_name2.TabIndex = 0;
      this.txt_name2.TabStop = false;
      this.txt_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_name2.UseSystemPasswordChar = false;
      this.txt_name2.WaterMark = null;
      this.txt_name2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_name2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_name2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // lbl_name2
      // 
      this.lbl_name2.AutoSize = true;
      this.lbl_name2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name2.Location = new System.Drawing.Point(3, 0);
      this.lbl_name2.Name = "lbl_name2";
      this.lbl_name2.Size = new System.Drawing.Size(66, 19);
      this.lbl_name2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name2.TabIndex = 1068;
      this.lbl_name2.Text = "xName2";
      this.lbl_name2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_surname
      // 
      this.tlp_surname.ColumnCount = 1;
      this.tlp_surname.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_surname.Controls.Add(this.txt_lastname1, 0, 1);
      this.tlp_surname.Controls.Add(this.lbl_lastname1, 0, 0);
      this.tlp_surname.Location = new System.Drawing.Point(415, 3);
      this.tlp_surname.Name = "tlp_surname";
      this.tlp_surname.RowCount = 2;
      this.tlp_surname.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.82353F));
      this.tlp_surname.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.17647F));
      this.tlp_surname.Size = new System.Drawing.Size(200, 68);
      this.tlp_surname.TabIndex = 2;
      // 
      // txt_lastname1
      // 
      this.txt_lastname1.AllowSpace = false;
      this.txt_lastname1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_lastname1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_lastname1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_lastname1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_lastname1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_lastname1.CornerRadius = 5;
      this.txt_lastname1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_lastname1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_lastname1.Location = new System.Drawing.Point(3, 25);
      this.txt_lastname1.MaxLength = 50;
      this.txt_lastname1.Multiline = false;
      this.txt_lastname1.Name = "txt_lastname1";
      this.txt_lastname1.PasswordChar = '\0';
      this.txt_lastname1.ReadOnly = false;
      this.txt_lastname1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_lastname1.SelectedText = "";
      this.txt_lastname1.SelectionLength = 0;
      this.txt_lastname1.SelectionStart = 0;
      this.txt_lastname1.Size = new System.Drawing.Size(185, 40);
      this.txt_lastname1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_lastname1.TabIndex = 0;
      this.txt_lastname1.TabStop = false;
      this.txt_lastname1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_lastname1.UseSystemPasswordChar = false;
      this.txt_lastname1.WaterMark = null;
      this.txt_lastname1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_lastname1.TextChanged += new System.EventHandler(this.txt_fullname_TextChanged);
      this.txt_lastname1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_lastname1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // lbl_lastname1
      // 
      this.lbl_lastname1.AutoSize = true;
      this.lbl_lastname1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_lastname1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_lastname1.Location = new System.Drawing.Point(3, 0);
      this.lbl_lastname1.Name = "lbl_lastname1";
      this.lbl_lastname1.Size = new System.Drawing.Size(93, 19);
      this.lbl_lastname1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_lastname1.TabIndex = 1070;
      this.lbl_lastname1.Text = "xLastname1";
      this.lbl_lastname1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_surname2
      // 
      this.tlp_surname2.ColumnCount = 1;
      this.tlp_surname2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_surname2.Controls.Add(this.txt_lastname2, 0, 1);
      this.tlp_surname2.Controls.Add(this.lbl_lastname2, 0, 0);
      this.tlp_surname2.Location = new System.Drawing.Point(621, 3);
      this.tlp_surname2.Name = "tlp_surname2";
      this.tlp_surname2.RowCount = 2;
      this.tlp_surname2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.34328F));
      this.tlp_surname2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.65672F));
      this.tlp_surname2.Size = new System.Drawing.Size(200, 68);
      this.tlp_surname2.TabIndex = 3;
      // 
      // txt_lastname2
      // 
      this.txt_lastname2.AllowSpace = false;
      this.txt_lastname2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_lastname2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_lastname2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_lastname2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_lastname2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_lastname2.CornerRadius = 5;
      this.txt_lastname2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_lastname2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.txt_lastname2.Location = new System.Drawing.Point(3, 24);
      this.txt_lastname2.MaxLength = 50;
      this.txt_lastname2.Multiline = false;
      this.txt_lastname2.Name = "txt_lastname2";
      this.txt_lastname2.PasswordChar = '\0';
      this.txt_lastname2.ReadOnly = false;
      this.txt_lastname2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_lastname2.SelectedText = "";
      this.txt_lastname2.SelectionLength = 0;
      this.txt_lastname2.SelectionStart = 0;
      this.txt_lastname2.Size = new System.Drawing.Size(185, 40);
      this.txt_lastname2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_lastname2.TabIndex = 0;
      this.txt_lastname2.TabStop = false;
      this.txt_lastname2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_lastname2.UseSystemPasswordChar = false;
      this.txt_lastname2.WaterMark = null;
      this.txt_lastname2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_lastname2.TextChanged += new System.EventHandler(this.txt_fullname_TextChanged);
      this.txt_lastname2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_lastname2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // lbl_lastname2
      // 
      this.lbl_lastname2.AutoSize = true;
      this.lbl_lastname2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_lastname2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_lastname2.Location = new System.Drawing.Point(3, 0);
      this.lbl_lastname2.Name = "lbl_lastname2";
      this.lbl_lastname2.Size = new System.Drawing.Size(93, 19);
      this.lbl_lastname2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_lastname2.TabIndex = 1071;
      this.lbl_lastname2.Text = "xLastname2";
      this.lbl_lastname2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tlp_sex
      // 
      this.tlp_sex.ColumnCount = 1;
      this.tlp_sex.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_sex.Controls.Add(this.uc_lbl_sex, 0, 0);
      this.tlp_sex.Controls.Add(this.uc_cb_gender, 0, 1);
      this.tlp_sex.Location = new System.Drawing.Point(3, 77);
      this.tlp_sex.Name = "tlp_sex";
      this.tlp_sex.RowCount = 2;
      this.tlp_sex.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
      this.tlp_sex.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.64706F));
      this.tlp_sex.Size = new System.Drawing.Size(200, 68);
      this.tlp_sex.TabIndex = 4;
      // 
      // uc_lbl_sex
      // 
      this.uc_lbl_sex.AutoSize = true;
      this.uc_lbl_sex.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_lbl_sex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_lbl_sex.Location = new System.Drawing.Point(3, 0);
      this.uc_lbl_sex.Name = "uc_lbl_sex";
      this.uc_lbl_sex.Size = new System.Drawing.Size(41, 19);
      this.uc_lbl_sex.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_lbl_sex.TabIndex = 1072;
      this.uc_lbl_sex.Text = "xSex";
      this.uc_lbl_sex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_cb_gender
      // 
      this.uc_cb_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.uc_cb_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_cb_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_cb_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_cb_gender.CornerRadius = 5;
      this.uc_cb_gender.DataSource = null;
      this.uc_cb_gender.DisplayMember = "";
      this.uc_cb_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.uc_cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.uc_cb_gender.DropDownWidth = 185;
      this.uc_cb_gender.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_cb_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.uc_cb_gender.FormattingEnabled = false;
      this.uc_cb_gender.FormatValidator = null;
      this.uc_cb_gender.IsDroppedDown = false;
      this.uc_cb_gender.ItemHeight = 40;
      this.uc_cb_gender.Location = new System.Drawing.Point(3, 24);
      this.uc_cb_gender.MaxDropDownItems = 8;
      this.uc_cb_gender.Name = "uc_cb_gender";
      this.uc_cb_gender.OnFocusOpenListBox = true;
      this.uc_cb_gender.SelectedIndex = -1;
      this.uc_cb_gender.SelectedItem = null;
      this.uc_cb_gender.SelectedValue = null;
      this.uc_cb_gender.SelectionLength = 0;
      this.uc_cb_gender.SelectionStart = 0;
      this.uc_cb_gender.Size = new System.Drawing.Size(185, 40);
      this.uc_cb_gender.Sorted = false;
      this.uc_cb_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.uc_cb_gender.TabIndex = 0;
      this.uc_cb_gender.TabStop = false;
      this.uc_cb_gender.ValueMember = "";
      this.uc_cb_gender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.uc_cb_gender.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // tlp_document
      // 
      this.tlp_document.ColumnCount = 1;
      this.tlp_document.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_document.Controls.Add(this.lbl_id, 0, 0);
      this.tlp_document.Controls.Add(this.txt_id, 0, 1);
      this.tlp_document.Location = new System.Drawing.Point(209, 77);
      this.tlp_document.Name = "tlp_document";
      this.tlp_document.RowCount = 2;
      this.tlp_document.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.82353F));
      this.tlp_document.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.17647F));
      this.tlp_document.Size = new System.Drawing.Size(200, 68);
      this.tlp_document.TabIndex = 5;
      // 
      // lbl_id
      // 
      this.lbl_id.AutoSize = true;
      this.lbl_id.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_id.Location = new System.Drawing.Point(3, 0);
      this.lbl_id.Name = "lbl_id";
      this.lbl_id.Size = new System.Drawing.Size(89, 19);
      this.lbl_id.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_id.TabIndex = 1058;
      this.lbl_id.Text = "xDocument";
      this.lbl_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_id
      // 
      this.txt_id.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_id.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_id.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_id.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_id.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_id.CornerRadius = 5;
      this.txt_id.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_id.Location = new System.Drawing.Point(3, 25);
      this.txt_id.MaxLength = 50;
      this.txt_id.Multiline = false;
      this.txt_id.Name = "txt_id";
      this.txt_id.PasswordChar = '\0';
      this.txt_id.ReadOnly = false;
      this.txt_id.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.txt_id.SelectedText = "";
      this.txt_id.SelectionLength = 0;
      this.txt_id.SelectionStart = 0;
      this.txt_id.Size = new System.Drawing.Size(185, 40);
      this.txt_id.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.txt_id.TabIndex = 0;
      this.txt_id.TabStop = false;
      this.txt_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_id.UseSystemPasswordChar = false;
      this.txt_id.WaterMark = null;
      this.txt_id.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      this.txt_id.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.txt_id.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // tlp_dob
      // 
      this.tlp_dob.ColumnCount = 1;
      this.tlp_dob.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tlp_dob.Controls.Add(this.uc_lbl_birth_date, 0, 0);
      this.tlp_dob.Controls.Add(this.uc_dt_birth_date, 0, 1);
      this.tlp_dob.Location = new System.Drawing.Point(415, 77);
      this.tlp_dob.Name = "tlp_dob";
      this.tlp_dob.RowCount = 2;
      this.tlp_dob.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
      this.tlp_dob.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.64706F));
      this.tlp_dob.Size = new System.Drawing.Size(342, 68);
      this.tlp_dob.TabIndex = 6;
      // 
      // uc_lbl_birth_date
      // 
      this.uc_lbl_birth_date.AutoSize = true;
      this.uc_lbl_birth_date.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_lbl_birth_date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_lbl_birth_date.Location = new System.Drawing.Point(3, 0);
      this.uc_lbl_birth_date.Name = "uc_lbl_birth_date";
      this.uc_lbl_birth_date.Size = new System.Drawing.Size(83, 19);
      this.uc_lbl_birth_date.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_lbl_birth_date.TabIndex = 1064;
      this.uc_lbl_birth_date.Text = "xBirthDate";
      this.uc_lbl_birth_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_dt_birth_date
      // 
      this.uc_dt_birth_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_dt_birth_date.DateText = "";
      this.uc_dt_birth_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth_date.DateTextWidth = 0;
      this.uc_dt_birth_date.DateValue = new System.DateTime(((long)(0)));
      this.uc_dt_birth_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth_date.FormatWidth = 39;
      this.uc_dt_birth_date.Invalid = false;
      this.uc_dt_birth_date.Location = new System.Drawing.Point(3, 24);
      this.uc_dt_birth_date.Name = "uc_dt_birth_date";
      this.uc_dt_birth_date.Size = new System.Drawing.Size(226, 41);
      this.uc_dt_birth_date.TabIndex = 0;
      this.uc_dt_birth_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_dt_birth_date.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_track_data_KeyDown);
      this.uc_dt_birth_date.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_id_KeyUp);
      // 
      // flowLayoutPanel2
      // 
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel2);
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel3);
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel4);
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel5);
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel6);
      this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel7);
      this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 151);
      this.flowLayoutPanel2.Name = "flowLayoutPanel2";
      this.flowLayoutPanel2.Size = new System.Drawing.Size(824, 148);
      this.flowLayoutPanel2.TabIndex = 1080;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Controls.Add(this.characterTextBox1, 0, 1);
      this.tableLayoutPanel2.Controls.Add(this.uc_label1, 0, 0);
      this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 68);
      this.tableLayoutPanel2.TabIndex = 1075;
      // 
      // characterTextBox1
      // 
      this.characterTextBox1.AllowSpace = false;
      this.characterTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.characterTextBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.characterTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox1.CornerRadius = 5;
      this.characterTextBox1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.characterTextBox1.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox1.Location = new System.Drawing.Point(3, 25);
      this.characterTextBox1.MaxLength = 50;
      this.characterTextBox1.Multiline = false;
      this.characterTextBox1.Name = "characterTextBox1";
      this.characterTextBox1.PasswordChar = '\0';
      this.characterTextBox1.ReadOnly = false;
      this.characterTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox1.SelectedText = "";
      this.characterTextBox1.SelectionLength = 0;
      this.characterTextBox1.SelectionStart = 0;
      this.characterTextBox1.Size = new System.Drawing.Size(185, 40);
      this.characterTextBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.characterTextBox1.TabIndex = 2;
      this.characterTextBox1.TabStop = false;
      this.characterTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox1.UseSystemPasswordChar = false;
      this.characterTextBox1.WaterMark = null;
      this.characterTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_label1
      // 
      this.uc_label1.AutoSize = true;
      this.uc_label1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label1.Location = new System.Drawing.Point(3, 0);
      this.uc_label1.Name = "uc_label1";
      this.uc_label1.Size = new System.Drawing.Size(58, 19);
      this.uc_label1.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label1.TabIndex = 1068;
      this.uc_label1.Text = "xName";
      this.uc_label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.ColumnCount = 1;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel3.Controls.Add(this.characterTextBox2, 0, 1);
      this.tableLayoutPanel3.Controls.Add(this.uc_label2, 0, 0);
      this.tableLayoutPanel3.Location = new System.Drawing.Point(209, 3);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 2;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.82353F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.17647F));
      this.tableLayoutPanel3.Size = new System.Drawing.Size(200, 68);
      this.tableLayoutPanel3.TabIndex = 1073;
      // 
      // characterTextBox2
      // 
      this.characterTextBox2.AllowSpace = false;
      this.characterTextBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.characterTextBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.characterTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox2.CornerRadius = 5;
      this.characterTextBox2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.characterTextBox2.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox2.Location = new System.Drawing.Point(3, 25);
      this.characterTextBox2.MaxLength = 50;
      this.characterTextBox2.Multiline = false;
      this.characterTextBox2.Name = "characterTextBox2";
      this.characterTextBox2.PasswordChar = '\0';
      this.characterTextBox2.ReadOnly = false;
      this.characterTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox2.SelectedText = "";
      this.characterTextBox2.SelectionLength = 0;
      this.characterTextBox2.SelectionStart = 0;
      this.characterTextBox2.Size = new System.Drawing.Size(185, 40);
      this.characterTextBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.characterTextBox2.TabIndex = 3;
      this.characterTextBox2.TabStop = false;
      this.characterTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox2.UseSystemPasswordChar = false;
      this.characterTextBox2.WaterMark = null;
      this.characterTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_label2
      // 
      this.uc_label2.AutoSize = true;
      this.uc_label2.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label2.Location = new System.Drawing.Point(3, 0);
      this.uc_label2.Name = "uc_label2";
      this.uc_label2.Size = new System.Drawing.Size(93, 19);
      this.uc_label2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label2.TabIndex = 1070;
      this.uc_label2.Text = "xLastname1";
      this.uc_label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.ColumnCount = 1;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel4.Controls.Add(this.characterTextBox3, 0, 1);
      this.tableLayoutPanel4.Controls.Add(this.uc_label3, 0, 0);
      this.tableLayoutPanel4.Location = new System.Drawing.Point(415, 3);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 2;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.34328F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.65672F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 68);
      this.tableLayoutPanel4.TabIndex = 1074;
      // 
      // characterTextBox3
      // 
      this.characterTextBox3.AllowSpace = false;
      this.characterTextBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.characterTextBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.characterTextBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.characterTextBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.characterTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.characterTextBox3.CornerRadius = 5;
      this.characterTextBox3.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.characterTextBox3.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.characterTextBox3.Location = new System.Drawing.Point(3, 24);
      this.characterTextBox3.MaxLength = 50;
      this.characterTextBox3.Multiline = false;
      this.characterTextBox3.Name = "characterTextBox3";
      this.characterTextBox3.PasswordChar = '\0';
      this.characterTextBox3.ReadOnly = false;
      this.characterTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.characterTextBox3.SelectedText = "";
      this.characterTextBox3.SelectionLength = 0;
      this.characterTextBox3.SelectionStart = 0;
      this.characterTextBox3.Size = new System.Drawing.Size(185, 40);
      this.characterTextBox3.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.characterTextBox3.TabIndex = 4;
      this.characterTextBox3.TabStop = false;
      this.characterTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.characterTextBox3.UseSystemPasswordChar = false;
      this.characterTextBox3.WaterMark = null;
      this.characterTextBox3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // uc_label3
      // 
      this.uc_label3.AutoSize = true;
      this.uc_label3.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label3.Location = new System.Drawing.Point(3, 0);
      this.uc_label3.Name = "uc_label3";
      this.uc_label3.Size = new System.Drawing.Size(93, 19);
      this.uc_label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label3.TabIndex = 1071;
      this.uc_label3.Text = "xLastname2";
      this.uc_label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tableLayoutPanel5
      // 
      this.tableLayoutPanel5.ColumnCount = 1;
      this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel5.Controls.Add(this.uc_label4, 0, 0);
      this.tableLayoutPanel5.Controls.Add(this.uc_round_auto_combobox1, 0, 1);
      this.tableLayoutPanel5.Location = new System.Drawing.Point(621, 3);
      this.tableLayoutPanel5.Name = "tableLayoutPanel5";
      this.tableLayoutPanel5.RowCount = 2;
      this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
      this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.64706F));
      this.tableLayoutPanel5.Size = new System.Drawing.Size(200, 68);
      this.tableLayoutPanel5.TabIndex = 1076;
      // 
      // uc_label4
      // 
      this.uc_label4.AutoSize = true;
      this.uc_label4.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label4.Location = new System.Drawing.Point(3, 0);
      this.uc_label4.Name = "uc_label4";
      this.uc_label4.Size = new System.Drawing.Size(41, 19);
      this.uc_label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label4.TabIndex = 1072;
      this.uc_label4.Text = "xSex";
      this.uc_label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_round_auto_combobox1
      // 
      this.uc_round_auto_combobox1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.uc_round_auto_combobox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_round_auto_combobox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_round_auto_combobox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_round_auto_combobox1.CornerRadius = 5;
      this.uc_round_auto_combobox1.DataSource = null;
      this.uc_round_auto_combobox1.DisplayMember = "";
      this.uc_round_auto_combobox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.uc_round_auto_combobox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.uc_round_auto_combobox1.DropDownWidth = 185;
      this.uc_round_auto_combobox1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_auto_combobox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.uc_round_auto_combobox1.FormattingEnabled = false;
      this.uc_round_auto_combobox1.FormatValidator = null;
      this.uc_round_auto_combobox1.IsDroppedDown = false;
      this.uc_round_auto_combobox1.ItemHeight = 40;
      this.uc_round_auto_combobox1.Location = new System.Drawing.Point(3, 24);
      this.uc_round_auto_combobox1.MaxDropDownItems = 8;
      this.uc_round_auto_combobox1.Name = "uc_round_auto_combobox1";
      this.uc_round_auto_combobox1.OnFocusOpenListBox = true;
      this.uc_round_auto_combobox1.SelectedIndex = -1;
      this.uc_round_auto_combobox1.SelectedItem = null;
      this.uc_round_auto_combobox1.SelectedValue = null;
      this.uc_round_auto_combobox1.SelectionLength = 0;
      this.uc_round_auto_combobox1.SelectionStart = 0;
      this.uc_round_auto_combobox1.Size = new System.Drawing.Size(185, 40);
      this.uc_round_auto_combobox1.Sorted = false;
      this.uc_round_auto_combobox1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.uc_round_auto_combobox1.TabIndex = 5;
      this.uc_round_auto_combobox1.TabStop = false;
      this.uc_round_auto_combobox1.ValueMember = "";
      // 
      // tableLayoutPanel6
      // 
      this.tableLayoutPanel6.ColumnCount = 1;
      this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel6.Controls.Add(this.uc_label5, 0, 0);
      this.tableLayoutPanel6.Controls.Add(this.uc_round_textbox1, 0, 1);
      this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 77);
      this.tableLayoutPanel6.Name = "tableLayoutPanel6";
      this.tableLayoutPanel6.RowCount = 2;
      this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.82353F));
      this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.17647F));
      this.tableLayoutPanel6.Size = new System.Drawing.Size(200, 68);
      this.tableLayoutPanel6.TabIndex = 1077;
      // 
      // uc_label5
      // 
      this.uc_label5.AutoSize = true;
      this.uc_label5.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label5.Location = new System.Drawing.Point(3, 0);
      this.uc_label5.Name = "uc_label5";
      this.uc_label5.Size = new System.Drawing.Size(89, 19);
      this.uc_label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label5.TabIndex = 1058;
      this.uc_label5.Text = "xDocument";
      this.uc_label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // uc_round_textbox1
      // 
      this.uc_round_textbox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.uc_round_textbox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.uc_round_textbox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_round_textbox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.uc_round_textbox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.uc_round_textbox1.CornerRadius = 5;
      this.uc_round_textbox1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_round_textbox1.Location = new System.Drawing.Point(3, 25);
      this.uc_round_textbox1.MaxLength = 50;
      this.uc_round_textbox1.Multiline = false;
      this.uc_round_textbox1.Name = "uc_round_textbox1";
      this.uc_round_textbox1.PasswordChar = '\0';
      this.uc_round_textbox1.ReadOnly = false;
      this.uc_round_textbox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.uc_round_textbox1.SelectedText = "";
      this.uc_round_textbox1.SelectionLength = 0;
      this.uc_round_textbox1.SelectionStart = 0;
      this.uc_round_textbox1.Size = new System.Drawing.Size(185, 40);
      this.uc_round_textbox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.uc_round_textbox1.TabIndex = 6;
      this.uc_round_textbox1.TabStop = false;
      this.uc_round_textbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.uc_round_textbox1.UseSystemPasswordChar = false;
      this.uc_round_textbox1.WaterMark = null;
      this.uc_round_textbox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // tableLayoutPanel7
      // 
      this.tableLayoutPanel7.ColumnCount = 1;
      this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel7.Controls.Add(this.uc_datetime1, 0, 1);
      this.tableLayoutPanel7.Controls.Add(this.uc_label6, 0, 0);
      this.tableLayoutPanel7.Location = new System.Drawing.Point(209, 77);
      this.tableLayoutPanel7.Name = "tableLayoutPanel7";
      this.tableLayoutPanel7.RowCount = 2;
      this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.35294F));
      this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.64706F));
      this.tableLayoutPanel7.Size = new System.Drawing.Size(342, 68);
      this.tableLayoutPanel7.TabIndex = 1078;
      // 
      // uc_datetime1
      // 
      this.uc_datetime1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
      this.uc_datetime1.DateText = "";
      this.uc_datetime1.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime1.DateTextWidth = 0;
      this.uc_datetime1.DateValue = new System.DateTime(((long)(0)));
      this.uc_datetime1.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_datetime1.FormatWidth = 0;
      this.uc_datetime1.Invalid = false;
      this.uc_datetime1.Location = new System.Drawing.Point(3, 24);
      this.uc_datetime1.Name = "uc_datetime1";
      this.uc_datetime1.Size = new System.Drawing.Size(240, 41);
      this.uc_datetime1.TabIndex = 7;
      this.uc_datetime1.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      // 
      // uc_label6
      // 
      this.uc_label6.AutoSize = true;
      this.uc_label6.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.uc_label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_label6.Location = new System.Drawing.Point(3, 0);
      this.uc_label6.Name = "uc_label6";
      this.uc_label6.Size = new System.Drawing.Size(83, 19);
      this.uc_label6.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.uc_label6.TabIndex = 1064;
      this.uc_label6.Text = "xBirthDate";
      this.uc_label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_fullname2
      // 
      this.lbl_fullname2.BackColor = System.Drawing.Color.WhiteSmoke;
      this.lbl_fullname2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fullname2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_fullname2.Location = new System.Drawing.Point(36, 176);
      this.lbl_fullname2.Name = "lbl_fullname2";
      this.lbl_fullname2.Size = new System.Drawing.Size(687, 37);
      this.lbl_fullname2.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_fullname2.TabIndex = 1048;
      this.lbl_fullname2.Text = "xFullName";
      this.lbl_fullname2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_fullname
      // 
      this.lbl_fullname.AutoSize = true;
      this.lbl_fullname.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_fullname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_fullname.Location = new System.Drawing.Point(36, 158);
      this.lbl_fullname.Name = "lbl_fullname";
      this.lbl_fullname.Size = new System.Drawing.Size(87, 19);
      this.lbl_fullname.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_fullname.TabIndex = 1047;
      this.lbl_fullname.Text = "xFull Name";
      this.lbl_fullname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnl_message
      // 
      this.pnl_message.BackColor = System.Drawing.Color.WhiteSmoke;
      this.pnl_message.Controls.Add(this.lbl_msg_blink);
      this.pnl_message.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_message.Location = new System.Drawing.Point(138, 337);
      this.pnl_message.Name = "pnl_message";
      this.pnl_message.Size = new System.Drawing.Size(886, 37);
      this.pnl_message.TabIndex = 1061;
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.lbl_msg_blink.BackColor = System.Drawing.Color.WhiteSmoke;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(142, 9);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(581, 18);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 1049;
      this.lbl_msg_blink.Text = "xAT LEAST ONE FIELD MUST BE PROVIDED";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lbl_msg_blink.Visible = false;
      // 
      // uc_grid_entries
      // 
      this.uc_grid_entries.AllowUserToAddRows = false;
      this.uc_grid_entries.AllowUserToDeleteRows = false;
      this.uc_grid_entries.AllowUserToResizeColumns = false;
      this.uc_grid_entries.AllowUserToResizeRows = false;
      this.uc_grid_entries.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.uc_grid_entries.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.uc_grid_entries.ColumnHeaderHeight = 35;
      this.uc_grid_entries.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.uc_grid_entries.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.uc_grid_entries.ColumnHeadersHeight = 35;
      this.uc_grid_entries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.uc_grid_entries.CornerRadius = 10;
      this.uc_grid_entries.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.uc_grid_entries.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.uc_grid_entries.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.uc_grid_entries.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.uc_grid_entries.DefaultCellStyle = dataGridViewCellStyle2;
      this.uc_grid_entries.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.uc_grid_entries.EnableHeadersVisualStyles = false;
      this.uc_grid_entries.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_grid_entries.GridColor = System.Drawing.Color.LightGray;
      this.uc_grid_entries.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.uc_grid_entries.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.uc_grid_entries.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_grid_entries.HeaderImages = null;
      this.uc_grid_entries.Location = new System.Drawing.Point(0, 36);
      this.uc_grid_entries.MultiSelect = false;
      this.uc_grid_entries.Name = "uc_grid_entries";
      this.uc_grid_entries.ReadOnly = true;
      this.uc_grid_entries.RowHeadersVisible = false;
      this.uc_grid_entries.RowHeadersWidth = 20;
      this.uc_grid_entries.RowTemplateHeight = 35;
      this.uc_grid_entries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.uc_grid_entries.Size = new System.Drawing.Size(870, 232);
      this.uc_grid_entries.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITHOUT_ROUND_BORDERS;
      this.uc_grid_entries.TabIndex = 6;
      this.uc_grid_entries.TabStop = false;
      this.uc_grid_entries.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.uc_grid_entries_CellClick);
      this.uc_grid_entries.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.uc_grid_entries_CellFormatting);
      this.uc_grid_entries.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.uc_grid_entries_CellToolTipTextNeeded);
      this.uc_grid_entries.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.uc_grid_entries_ColumnAdded);
      // 
      // uc_pnl_grid
      // 
      this.uc_pnl_grid.BackColor = System.Drawing.Color.Transparent;
      this.uc_pnl_grid.BorderColor = System.Drawing.Color.Empty;
      this.uc_pnl_grid.Controls.Add(this.uc_grid_entries);
      this.uc_pnl_grid.CornerRadius = 10;
      this.uc_pnl_grid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_pnl_grid.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.uc_pnl_grid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_pnl_grid.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.uc_pnl_grid.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_pnl_grid.HeaderHeight = 35;
      this.uc_pnl_grid.HeaderSubText = null;
      this.uc_pnl_grid.HeaderText = "XLAST ENTRIES";
      this.uc_pnl_grid.Location = new System.Drawing.Point(8, 8);
      this.uc_pnl_grid.Name = "uc_pnl_grid";
      this.uc_pnl_grid.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.uc_pnl_grid.Size = new System.Drawing.Size(870, 268);
      this.uc_pnl_grid.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.uc_pnl_grid.TabIndex = 1063;
      // 
      // pnl_grid_container
      // 
      this.pnl_grid_container.BackColor = System.Drawing.Color.WhiteSmoke;
      this.pnl_grid_container.Controls.Add(this.uc_pnl_grid);
      this.pnl_grid_container.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_grid_container.Location = new System.Drawing.Point(138, 374);
      this.pnl_grid_container.Name = "pnl_grid_container";
      this.pnl_grid_container.Padding = new System.Windows.Forms.Padding(8);
      this.pnl_grid_container.Size = new System.Drawing.Size(886, 284);
      this.pnl_grid_container.TabIndex = 1062;
      // 
      // FrmSearchCustomersReception
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1024, 713);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmSearchCustomersReception";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "frm_search_customers_reception";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_search_customers_reception_FormClosing);
      this.Shown += new System.EventHandler(this.FrmSearchCustomersReception_Shown);
      this.pnl_data.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.uc_pnl_top.ResumeLayout(false);
      this.uc_pnl_top.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.img_ticket_scan)).EndInit();
      this.uc_pnl_medium.ResumeLayout(false);
      this.uc_pnl_medium.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.tlp_name.ResumeLayout(false);
      this.tlp_name.PerformLayout();
      this.tlp_name2.ResumeLayout(false);
      this.tlp_name2.PerformLayout();
      this.tlp_surname.ResumeLayout(false);
      this.tlp_surname.PerformLayout();
      this.tlp_surname2.ResumeLayout(false);
      this.tlp_surname2.PerformLayout();
      this.tlp_sex.ResumeLayout(false);
      this.tlp_sex.PerformLayout();
      this.tlp_document.ResumeLayout(false);
      this.tlp_document.PerformLayout();
      this.tlp_dob.ResumeLayout(false);
      this.tlp_dob.PerformLayout();
      this.flowLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.PerformLayout();
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tableLayoutPanel3.PerformLayout();
      this.tableLayoutPanel4.ResumeLayout(false);
      this.tableLayoutPanel4.PerformLayout();
      this.tableLayoutPanel5.ResumeLayout(false);
      this.tableLayoutPanel5.PerformLayout();
      this.tableLayoutPanel6.ResumeLayout(false);
      this.tableLayoutPanel6.PerformLayout();
      this.tableLayoutPanel7.ResumeLayout(false);
      this.tableLayoutPanel7.PerformLayout();
      this.pnl_message.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.uc_grid_entries)).EndInit();
      this.uc_pnl_grid.ResumeLayout(false);
      this.pnl_grid_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private Controls.uc_round_button_long_press btn_escanear_id;
    private Controls.uc_round_button btn_clear;
    private Controls.uc_round_button btn_add_customer;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_button btn_keyboard;
    private Controls.uc_round_panel uc_pnl_top;
    private NumericTextBox txt_track_data;
    private Controls.uc_label lbl_track_data;
    private System.Windows.Forms.Panel pnl_message;
    private Controls.uc_label lbl_msg_blink;
    private Controls.uc_round_panel uc_pnl_medium;
    private uc_round_textbox txt_id;
    private Controls.uc_round_auto_combobox uc_cb_gender;
    private Controls.uc_label lbl_fullname2;
    private Controls.uc_label lbl_fullname;
    private Controls.uc_label uc_lbl_sex;
    private CharacterTextBox txt_name;
    private Controls.uc_label lbl_name;
    private CharacterTextBox txt_lastname1;
    private Controls.uc_label lbl_lastname1;
    private Controls.uc_label lbl_lastname2;
    private CharacterTextBox txt_lastname2;
    private Controls.uc_label uc_lbl_birth_date;
    private Controls.uc_label lbl_id;
    private Controls.uc_DataGridView uc_grid_entries;
    private Controls.uc_round_panel uc_pnl_grid;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private Controls.uc_checkBox uc_voucher_search;
    private System.Windows.Forms.TableLayoutPanel tlp_dob;
    private System.Windows.Forms.TableLayoutPanel tlp_document;
    private System.Windows.Forms.TableLayoutPanel tlp_sex;
    private System.Windows.Forms.TableLayoutPanel tlp_name;
    private System.Windows.Forms.TableLayoutPanel tlp_surname2;
    private System.Windows.Forms.TableLayoutPanel tlp_surname;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.TableLayoutPanel tlp_name2;
    private CharacterTextBox txt_name2;
    private uc_label lbl_name2;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private CharacterTextBox characterTextBox1;
    private uc_label uc_label1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private CharacterTextBox characterTextBox2;
    private uc_label uc_label2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private CharacterTextBox characterTextBox3;
    private uc_label uc_label3;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    private uc_label uc_label4;
    private uc_round_auto_combobox uc_round_auto_combobox1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
    private uc_label uc_label5;
    private uc_round_textbox uc_round_textbox1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
    private uc_datetime uc_datetime1;
    private uc_label uc_label6;
    private uc_datetime uc_dt_birth_date;
    private uc_card_reader uc_card_reader1;
    private System.Windows.Forms.PictureBox img_ticket_scan;
    private System.Windows.Forms.Panel pnl_grid_container;
    private uc_round_button btn_ok;
    private uc_round_button btn_visits_report;
  }
}
