using WSI.Cashier.Controls;
namespace WSI.Cashier
{
  partial class frm_balance_mismatch
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btn_close_session = new WSI.Cashier.Controls.uc_round_button();
      this.txt_comment = new WSI.Cashier.Controls.uc_round_textbox();
      this.lbl_comment = new WSI.Cashier.Controls.uc_label();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.dgv_close_options = new WSI.Cashier.Controls.uc_DataGridView();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_close_options)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_close_session);
      this.pnl_data.Controls.Add(this.txt_comment);
      this.pnl_data.Controls.Add(this.lbl_comment);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.dgv_close_options);
      this.pnl_data.Size = new System.Drawing.Size(588, 382);
      // 
      // btn_close_session
      // 
      this.btn_close_session.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close_session.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_close_session.FlatAppearance.BorderSize = 0;
      this.btn_close_session.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close_session.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close_session.Image = null;
      this.btn_close_session.IsSelected = false;
      this.btn_close_session.Location = new System.Drawing.Point(430, 320);
      this.btn_close_session.Name = "btn_close_session";
      this.btn_close_session.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close_session.Size = new System.Drawing.Size(128, 48);
      this.btn_close_session.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_close_session.TabIndex = 30;
      this.btn_close_session.Text = "XCLOSE SESSION";
      this.btn_close_session.UseVisualStyleBackColor = false;
      this.btn_close_session.Click += new System.EventHandler(this.btn_close_session_Click);
      // 
      // txt_comment
      // 
      this.txt_comment.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.txt_comment.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.txt_comment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.txt_comment.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.txt_comment.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.txt_comment.CornerRadius = 5;
      this.txt_comment.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.txt_comment.Location = new System.Drawing.Point(26, 48);
      this.txt_comment.MaxLength = 255;
      this.txt_comment.Multiline = true;
      this.txt_comment.Name = "txt_comment";
      this.txt_comment.PasswordChar = '\0';
      this.txt_comment.ReadOnly = false;
      this.txt_comment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_comment.SelectedText = "";
      this.txt_comment.SelectionLength = 0;
      this.txt_comment.SelectionStart = 0;
      this.txt_comment.Size = new System.Drawing.Size(532, 100);
      this.txt_comment.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.MULTILINE;
      this.txt_comment.TabIndex = 27;
      this.txt_comment.TabStop = false;
      this.txt_comment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.txt_comment.UseSystemPasswordChar = false;
      this.txt_comment.WaterMark = null;
      this.txt_comment.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
      // 
      // lbl_comment
      // 
      this.lbl_comment.AutoSize = true;
      this.lbl_comment.BackColor = System.Drawing.Color.Transparent;
      this.lbl_comment.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_comment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_comment.Location = new System.Drawing.Point(26, 15);
      this.lbl_comment.Name = "lbl_comment";
      this.lbl_comment.Size = new System.Drawing.Size(86, 19);
      this.lbl_comment.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_comment.TabIndex = 26;
      this.lbl_comment.Text = "xComment";
      this.lbl_comment.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(296, 320);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(128, 48);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 29;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // dgv_close_options
      // 
      this.dgv_close_options.AllowUserToAddRows = false;
      this.dgv_close_options.AllowUserToDeleteRows = false;
      this.dgv_close_options.AllowUserToResizeColumns = false;
      this.dgv_close_options.AllowUserToResizeRows = false;
      this.dgv_close_options.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgv_close_options.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgv_close_options.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
      this.dgv_close_options.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgv_close_options.ColumnHeaderHeight = 35;
      this.dgv_close_options.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_close_options.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_close_options.ColumnHeadersHeight = 35;
      this.dgv_close_options.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgv_close_options.CornerRadius = 10;
      this.dgv_close_options.DefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.dgv_close_options.DefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.dgv_close_options.DefaultCellSelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.dgv_close_options.DefaultCellSelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgv_close_options.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_close_options.EnableHeadersVisualStyles = false;
      this.dgv_close_options.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.dgv_close_options.GridColor = System.Drawing.Color.LightGray;
      this.dgv_close_options.HeaderBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgv_close_options.HeaderDefaultCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.dgv_close_options.HeaderDefaultCellForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      // pendiente de subir el cambio del uc_datagridview.....
      //this.dgv_close_options.HeaderImages = null;
      this.dgv_close_options.Location = new System.Drawing.Point(26, 166);
      this.dgv_close_options.MultiSelect = false;
      this.dgv_close_options.Name = "dgv_close_options";
      this.dgv_close_options.ReadOnly = true;
      this.dgv_close_options.RowHeadersVisible = false;
      this.dgv_close_options.RowHeadersWidth = 20;
      this.dgv_close_options.RowTemplate.Height = 35;
      this.dgv_close_options.RowTemplateHeight = 35;
      this.dgv_close_options.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.dgv_close_options.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_close_options.Size = new System.Drawing.Size(532, 337);
      this.dgv_close_options.Style = WSI.Cashier.Controls.uc_DataGridView.DataGridViewStyle.WITH_TOP_ROUND_BORDERS;
      this.dgv_close_options.TabIndex = 28;
      this.dgv_close_options.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_close_options_CellClick);
      // 
      // frm_balance_mismatch
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(588, 437);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_balance_mismatch";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Message";
      this.Load += new System.EventHandler(this.frm_balance_mismatch_Load);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_close_options)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private uc_round_button btn_close_session;
    private uc_round_textbox txt_comment;
    private uc_label lbl_comment;
    private uc_round_button btn_cancel;
    private uc_DataGridView dgv_close_options;



  }
}