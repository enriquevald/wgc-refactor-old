﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_gaming_tables_win_loss.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Rafa Arjona
// 
// CREATION DATE: 24-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-MAY-2017 RAB    First release (PBI 27485: WIGOS-983 Win / loss - Introduce the WinLoss - SELECTION SCREEN)
// 25-MAY-2017 RAB    Bug 27683: WIGOS-2360 Win/Loss. It is allowed to enter amount value in the minut 10 after hour
// 30-MAY-2017 JML    PBI 27486: WIGOS-1227 Win / loss - Introduce the WinLoss - EDITION SCREEN - ACCUMULATED
// 25-MAY-2017 RAB    Bug 27683: WIGOS-2360 Win/Loss. It is allowed to enter amount value in the minut 10 after hour
// 31-MAY-2017 RAB    PBI 27810: WIGOS-2423 Win - loss - Change request
// 02-JUN-2017 RAB    Bug 27848: WIGOS-2546 - Win/Loss. Warning message in spanish language is cut when it is not possible to edit the register
// 02-JUN-2017 RAB    Bug 27849: WIGOS-2545 - Win/Loss. Exception is displayed when when is selected Edit the first time after open the form
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Cashier.Controls;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class frm_gaming_tables_win_loss : frm_base
  {
    #region Constants

    private const Int32 GRID_MAX_COLUMNS = 3;

    //Constants: Grid columns of visible grid
    private const Int32 GRID_COLUMN_WIN_LOSS_ID = 0;
    private const Int32 GRID_COLUMN_WIN_LOSS_ISO_CODE = 1;
    private const Int32 GRID_COLUMN_WIN_LOSS_TIME_SLOT = 2;
    private const Int32 GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED = 3;
    private const Int32 GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED = 4;
    private const Int32 GRID_COLUMN_WIN_LOSS_AMOUNT = 5;
    private const Int32 GRID_COLUMN_WIN_LOSS_LAST_UDPATE = 6;    

    //Constants: Grid columns width
    private const Int32 GRID_COLUMN_WIN_LOSS_GRID_TIME_SLOT_WIDTH = 110;
    private const Int32 GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_WIDTH = 100;
    private const Int32 GRID_COLUMN_WIN_LOSS_AMOUNT_WIDTH = 198;

    //Constants: WinLossData (frm_gaming_tables_win_loss) datatable
    private const Int32 SQL_COLUMN_WIN_LOSS_ID = 0;
    private const Int32 SQL_COLUMN_WIN_LOSS_ISO_CODE = 1;
    private const Int32 SQL_COLUMN_WIN_LOSS_TIME_SLOT = 2;
    private const Int32 SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED = 3;
    private const Int32 SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED = 4;
    private const Int32 SQL_COLUMN_WIN_LOSS_AMOUNT = 5;  
    private const Int32 SQL_COLUMN_WIN_LOSS_LAST_UPDATE = 6;

    //Constants: Default value when the value is null
    private const String UNINFORMED_STRING = "---";
    private const String FORMAT_DB_DATE_TIME = "yyyy/MM/dd HH:mm:ss";

        
    #endregion

    #region Constructor

    /// <summary>
    /// Class constructor frm_gaming_tables_win_loss
    /// </summary>
    public frm_gaming_tables_win_loss()
    {
      InitializeComponent();
    } //frm_gaming_tables_win_loss

    #endregion

    #region Members

    //Static members
    static Form m_parent;
    static Int64 m_gaming_table_session_id;
    static Int32 m_margin_to_edit;

    //Constants from frm_amount_input
    private Int32 m_sql_column_cage_amount_denomination;
    private Int32 m_sql_column_cage_amount_quantity;
    private Int32 m_sql_column_cage_amount_total;
    private Int32 m_sql_column_cage_amount_chip_id;

    //Dictionary that saves the isocode with its corresponding win/loss id. For example: ('MXN', 21), ('USD', 34)...
    Dictionary<String, Int64> m_iso_code_associated_to_win_loss_id;

    #endregion

    #region Public methods

    /// <summary>
    /// Init and Show the form
    /// </summary>
    /// <param name="Parent">Parent form of uc_gt_player_tracking.cs</param>
    /// <param name="GamingTableSessionId">Current gaming table session id</param>
    static public void Show(Form Parent, Int64 GamingTableSessionId)
    {  
      frm_gaming_tables_win_loss _gaming_tables_win_loss;
      _gaming_tables_win_loss = new frm_gaming_tables_win_loss();

      m_parent = Parent;
      m_gaming_table_session_id = GamingTableSessionId;
      m_margin_to_edit = GamingTableBusinessLogic.GetGTWinLossMinutes();      

      _gaming_tables_win_loss.Init();

      using (frm_yesno form_yes_no = new frm_yesno())
      {
        form_yes_no.Opacity = 0.6;
        form_yes_no.Show();
        _gaming_tables_win_loss.ShowDialog(form_yes_no);
      }

      if (m_parent != null)
      {
        m_parent.Focus();
      }

      _gaming_tables_win_loss.Dispose();
      _gaming_tables_win_loss = null;
    } // Show

    #endregion

    #region Private methods

    /// <summary>
    /// Init variables
    /// </summary>
    private void Init()
    {
      this.FormTitle = String.Format(Resource.String("STR_GAMING_TABLE_WINLOSS_FORM_TITLE"), Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS"));
      pnl_win_loss_last_report_hour.HeaderText = Resource.String("STR_GAMING_TABLE_WINLOSS_LAST_REPORTED_HOUR");
      btn_win_loss_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      btn_win_loss_edit.Text = Resource.String("STR_DB_EDIT");

      lbl_not_reported_hour_error.Text = Resource.String("STR_GAMING_TABLE_WINLOSS_NOT_REPORTED");
      lbl_not_reported_hour_error.Visible = true;
      lbl_win_loss_last_reported_hour_time_slot.Text = UNINFORMED_STRING;
      lbl_win_loss_last_reported_hour_date.Text = UNINFORMED_STRING;
      lbl_win_loss_last_reported_hour_amount.Text = UNINFORMED_STRING;

      Location = new Point(1024 / 2 - this.Width / 2, 768 / 2 - this.Height / 2 - 90);
      pnl_win_loss_error.Visible = false; //The error message is not displayed by default

      if(GetTotalGamingTableWinLoss())
      {
        //Apply grid style and visualize the rows that correspond
        GamingTableWinLossGridStyle();
      }
    } // Init

    /// <summary>
    /// Obtaining the total rows to be displayed in the selection grid
    /// </summary>
    /// <returns>True you got it correctly, false found errors</returns>
    private Boolean GetTotalGamingTableWinLoss()
    {
      StringBuilder _sb;
      DataTable _win_loss_data;
      DataView _dv_win_loss_data_result;
      String _time_slot;
      Currency _win_loss_amount;

      _sb = new StringBuilder();
      _time_slot = String.Empty;

      _win_loss_data = new DataTable();
      _win_loss_data.Columns.Add("ID", typeof(Int64));
      _win_loss_data.Columns.Add("ISO_CODE", typeof(String));
      _win_loss_data.Columns.Add("TIME_SLOT", typeof(String));
      _win_loss_data.Columns.Add("DATETIME_HOUR_FORMATED", typeof(String));
      _win_loss_data.Columns.Add("DATETIME_HOUR_UNFORMATED", typeof(DateTime));
      _win_loss_data.Columns.Add("WIN_LOSS_AMOUNT", typeof(String));      
      _win_loss_data.Columns.Add("LAST_UPDATE", typeof(DateTime));

      _sb.AppendLine("    SELECT   GTWL_ID                          AS ID                         ");
      _sb.AppendLine("           , GTWL_ISO_CODE                    AS ISO_CODE                   ");
      _sb.AppendLine("           , GTWL_DATETIME_HOUR               AS DATETIME_HOUR_UNFORMATED   ");
      _sb.AppendLine("           , GTWL_WIN_LOSS_AMOUNT             AS WIN_LOSS_AMOUNT            ");
      _sb.AppendLine("           , GTWL_LAST_UPDATE                 AS LAST_UPDATE                ");
      _sb.AppendLine("      FROM   GAMING_TABLES_WIN_LOSS                                         ");
      _sb.AppendLine("     WHERE   GTWL_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId          ");
      _sb.AppendLine("  ORDER BY   GTWL_DATETIME_HOUR ASC                                         ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = m_gaming_table_session_id;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(_win_loss_data);
            }
          }
        }       

        foreach (DataRow _current_row in _win_loss_data.Rows)
        {
          _time_slot = Convert.ToDateTime(_current_row[SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED]).ToString("HH:mm") + " - " + Convert.ToDateTime(_current_row[SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED]).AddHours(1).ToString("HH:mm");
          _current_row[SQL_COLUMN_WIN_LOSS_TIME_SLOT] = _time_slot;
          _current_row[SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED] = Convert.ToDateTime(_current_row[SQL_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED]).ToShortDateString();

          if (_current_row[SQL_COLUMN_WIN_LOSS_AMOUNT] == DBNull.Value)
          {
            _current_row[SQL_COLUMN_WIN_LOSS_AMOUNT] = UNINFORMED_STRING;
          }
          else
          {
            _win_loss_amount = (Currency)Convert.ToDecimal(_current_row[SQL_COLUMN_WIN_LOSS_AMOUNT]);
            _current_row[SQL_COLUMN_WIN_LOSS_AMOUNT] = _win_loss_amount;
          }
        }

        //Get last reported hour
        GetLastReportedHour(_win_loss_data);
        //We updated the editing margin to 60 minutes plus the margin per general parameter
        Int32 _minutes_margin_to_show;
        _minutes_margin_to_show = -(60 + m_margin_to_edit);

        _dv_win_loss_data_result = new DataView(_win_loss_data);
        //We show the largest rows above the margin set above. We show the row that can be edited, and if applicable, the future row.
        _dv_win_loss_data_result.RowFilter = "DATETIME_HOUR_UNFORMATED >= '" + WGDB.Now.AddMinutes(_minutes_margin_to_show - 1).ToString(FORMAT_DB_DATE_TIME) + "'";
        dgv_win_loss.DataSource = _dv_win_loss_data_result;

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        return false;
      }
      catch (Exception _ex)
      {
        Log.Warning("Exception throw: Error reading win/loss gaming table information. GamingTableSessionId: " + m_gaming_table_session_id.ToString());
        Log.Exception(_ex);

        return false;
      }      
    } // GetTotalGamingTableWinLoss
    
    /// <summary>
    /// Get last reported hour
    /// </summary>
    /// <param name="WinLossData">Win loss datatable</param>
    private void GetLastReportedHour(DataTable WinLossData)
    {
      DataTable _dt_win_loss_data;
      DataView _dv_win_loss_data;
      
      _dv_win_loss_data = new DataView(WinLossData);
      //We discard the rows that have as '---' amount
      _dv_win_loss_data.RowFilter = "WIN_LOSS_AMOUNT <> '" + UNINFORMED_STRING + "' AND DATETIME_HOUR_UNFORMATED <= '" + WGDB.Now.AddMinutes(-(60 + m_margin_to_edit) - 1).ToString(FORMAT_DB_DATE_TIME) + "'";
      _dt_win_loss_data = _dv_win_loss_data.ToTable();

      //If there are no rows in the datatable, it means that no time has been reported. Show UNFORMATED_STRING
      if (_dt_win_loss_data.Rows.Count == 0)
      {
        lbl_not_reported_hour_error.Visible = true;
        lbl_win_loss_last_reported_hour_time_slot.Text = UNINFORMED_STRING;
        lbl_win_loss_last_reported_hour_date.Text = UNINFORMED_STRING;
        lbl_win_loss_last_reported_hour_amount.Text = UNINFORMED_STRING;
      }
      else
      {
        //We sorted the dataview down. Thus, we show the first row the last modified hour.
        _dv_win_loss_data.Sort = "DATETIME_HOUR_UNFORMATED DESC";
        _dt_win_loss_data = _dv_win_loss_data.ToTable();

        foreach (DataRow _current_row in _dt_win_loss_data.Rows)
        {
          //We show the modified time whenever the editing margin has passed
          if (Convert.ToDateTime(_current_row[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED]).AddMinutes(60 + m_margin_to_edit) > WGDB.Now.AddMinutes(m_margin_to_edit))
          {
            lbl_not_reported_hour_error.Visible = false;
            lbl_win_loss_last_reported_hour_time_slot.Text = _current_row[GRID_COLUMN_WIN_LOSS_TIME_SLOT].ToString();
            lbl_win_loss_last_reported_hour_date.Text = _current_row[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].ToString();
            lbl_win_loss_last_reported_hour_amount.Text = _current_row[GRID_COLUMN_WIN_LOSS_AMOUNT].ToString();

            return;
          }
        }

        //If you have not found anything, show the data in the first row
        lbl_not_reported_hour_error.Visible = false;
        lbl_win_loss_last_reported_hour_time_slot.Text = _dt_win_loss_data.Rows[0][GRID_COLUMN_WIN_LOSS_TIME_SLOT].ToString();
        lbl_win_loss_last_reported_hour_date.Text = _dt_win_loss_data.Rows[0][GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].ToString();
        lbl_win_loss_last_reported_hour_amount.Text = _dt_win_loss_data.Rows[0][GRID_COLUMN_WIN_LOSS_AMOUNT].ToString();
      }
    } // GetLastReportedHour

    /// <summary>
    /// Define grid style
    /// </summary>
    private void GamingTableWinLossGridStyle()
    {
      String _date_format;
      _date_format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_TIME_SLOT].Width = GRID_COLUMN_WIN_LOSS_GRID_TIME_SLOT_WIDTH;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_TIME_SLOT].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_TIME_SLOT].HeaderText = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DATE");
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_TIME_SLOT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].Width = GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_WIDTH;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].HeaderText = String.Empty;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].DefaultCellStyle.Format = _date_format;

      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_AMOUNT].Width = GRID_COLUMN_WIN_LOSS_AMOUNT_WIDTH;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_AMOUNT].SortMode = DataGridViewColumnSortMode.NotSortable;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_AMOUNT].HeaderText = String.Empty;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_AMOUNT].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_ID].Visible = false;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED].Visible = false;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_ISO_CODE].Visible = false;
      dgv_win_loss.Columns[GRID_COLUMN_WIN_LOSS_LAST_UDPATE].Visible = false;
    } // GamingTableWinLossGridStyle
    
    /// <summary>
    /// Decide whether the selected row is editable
    /// </summary>
    /// <returns>True you got it correctly, false found errors</returns>
    private Boolean IsSelectedRowEditable()
    {
      DateTime _selected_datetime;

      if(dgv_win_loss.Rows.Count > 0)
      {
        _selected_datetime = Convert.ToDateTime(dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED].Value);

        //The row is editable as long as it is within the margin of the edit margin
        if (_selected_datetime.AddHours(1) >= WGDB.Now.AddMinutes(-m_margin_to_edit - 1) && _selected_datetime.AddHours(1) < WGDB.Now.AddMinutes(m_margin_to_edit))
        {
          return true;
        }
      }

      return false;
    } // IsSelectedRowEditable

    /// <summary>
    /// We get the ids corresponding to their isocode. The data are obtained from database
    /// </summary>
    /// <param name="DateTimeHour">Date time hour selected</param>
    /// <param name="DbTrx">Db Transaction</param>
    /// <returns>True you got it correctly, false found errors</returns>
    private Boolean GetGamingTablesWinLossIds(DateTime DateTimeHour, DB_TRX DbTrx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      m_iso_code_associated_to_win_loss_id = new Dictionary<String, Int64>();

      _sb.AppendLine("   SELECT   GTWL_ID                                                 ");
      _sb.AppendLine("          , GTWL_ISO_CODE                                           ");
      _sb.AppendLine("     FROM   GAMING_TABLES_WIN_LOSS                                  ");
      _sb.AppendLine("    WHERE   GTWL_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId   ");
      _sb.AppendLine("      AND   GTWL_DATETIME_HOUR           = @pDateTimeHour           ");
      _sb.AppendLine(" ORDER BY   GTWL_ID DESC                                            ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), DbTrx.SqlTransaction.Connection, DbTrx.SqlTransaction))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = m_gaming_table_session_id;
          _cmd.Parameters.Add("@pDateTimeHour", SqlDbType.DateTime).Value = DateTimeHour;

          using (SqlDataReader _reader = DbTrx.ExecuteReader(_cmd))
          {
            while (_reader.Read())
            {
              m_iso_code_associated_to_win_loss_id.Add(_reader.GetString(SQL_COLUMN_WIN_LOSS_ISO_CODE), _reader.GetInt64(SQL_COLUMN_WIN_LOSS_ID));
            }
          }
        }
        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }

    /// <summary>
    /// Insert values to GAMING_TABLES_WIN_LOSS and GAMING_TABLES_WIN_LOSS_DETAIL tables
    /// </summary>
    /// <param name="CageAmounts">Cage amounts from frm_amount_input</param>
    /// <param name="WinLossMode">Actual win/loss mode</param>
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <returns>True you got it correctly, false found errors</returns>
    private Boolean InsertCageAmountsToWinLossTable(CageDenominationsItems.CageDenominationsDictionary CageAmounts, GamingTableBusinessLogic.GT_WIN_LOSS_MODE WinLossMode, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _current_cage_amount in CageAmounts)
      {
        if (_current_cage_amount.Key.Type == CageCurrencyType.ChipsRedimible && m_iso_code_associated_to_win_loss_id.ContainsKey(_current_cage_amount.Key.ISOCode))
        {
          //We first remove, regardless of the win/loss mode, the details of the modified win/loss
          _sb = new StringBuilder();
          _sb.AppendLine(" DELETE GAMING_TABLES_WIN_LOSS_DETAIL                       ");
          _sb.AppendLine("  WHERE GTWLD_ID = @pWinLossId                              ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pWinLossId", SqlDbType.BigInt).Value = (Int64)m_iso_code_associated_to_win_loss_id[_current_cage_amount.Key.ISOCode];
            if (_sql_cmd.ExecuteNonQuery() == -1)
            {
              return false;
            }
          }

          _sb = new StringBuilder();
          _sb.AppendLine(" UPDATE GAMING_TABLES_WIN_LOSS                              ");
          _sb.AppendLine(" SET    GTWL_ISO_CODE           = @pIsoCode                 ");
          _sb.AppendLine("      , GTWL_CAGE_CURRENCY_TYPE = @pCageCurrencyType        ");
          _sb.AppendLine("      , GTWL_WIN_LOSS_AMOUNT    = @pWinLossAmount           ");
          _sb.AppendLine("      , GTWL_USER_ID            = @pUserId                  ");
          _sb.AppendLine("      , GTWL_LAST_UPDATE        = @pLastUpdate              ");
          _sb.AppendLine(" WHERE GTWL_ID                  = @pWinLossId               ");
          _sb.AppendLine("   AND GTWL_ISO_CODE            = @pIsoCode                 ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = _current_cage_amount.Key.ISOCode;
            _sql_cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).Value = (Int32)CageCurrencyType.ChipsRedimible;
            _sql_cmd.Parameters.Add("@pWinLossAmount", SqlDbType.Money).Value = CageAmounts[_current_cage_amount.Key].TotalAmount;
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = Cashier.UserId;
            _sql_cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = WGDB.Now;
            _sql_cmd.Parameters.Add("@pWinLossId", SqlDbType.BigInt).Value = (Int64)m_iso_code_associated_to_win_loss_id[_current_cage_amount.Key.ISOCode];

            if (_sql_cmd.ExecuteNonQuery() <= 0)
            {
              return false;
            }
          }

          if (WinLossMode == GamingTableBusinessLogic.GT_WIN_LOSS_MODE.BY_DENOMINATION)
          {
            for (int _idx = 0; _idx < CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows.Count; _idx++)
            {
              if (CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_quantity] != DBNull.Value)
              {
                if (Convert.ToInt32(CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_quantity]) != 0)
                {
                  _sb = new StringBuilder();

                  _sb.AppendLine("    INSERT INTO GAMING_TABLES_WIN_LOSS_DETAIL         ");
                  _sb.AppendLine("             ( GTWLD_ID                               ");
                  _sb.AppendLine("             , GTWLD_CHIP_ID                          ");
                  _sb.AppendLine("             , GTWLD_DENOMINATION                     ");
                  _sb.AppendLine("             , GTWLD_QUANTITY                         ");
                  _sb.AppendLine("             , GTWLD_WIN_LOSS_DETAIL_AMOUNT)          ");
                  _sb.AppendLine("        VALUES                                        ");
                  _sb.AppendLine("             ( @pWinLossId                            ");
                  _sb.AppendLine("             , @pChipId                               ");
                  _sb.AppendLine("             , @pChipDenomination                     ");
                  _sb.AppendLine("             , @pChipQuatity                          ");
                  _sb.AppendLine("             , @pWinLossDetailAmount)                 ");

                  using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
                  {
                    _sql_cmd.Parameters.Add("@pWinLossId", SqlDbType.BigInt).Value = (Int64)m_iso_code_associated_to_win_loss_id[_current_cage_amount.Key.ISOCode];
                    _sql_cmd.Parameters.Add("@pChipId", SqlDbType.BigInt).Value = (Int64)CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_chip_id];
                    _sql_cmd.Parameters.Add("@pChipDenomination", SqlDbType.Money).Value = Convert.ToDecimal(CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_denomination]);
                    _sql_cmd.Parameters.Add("@pChipQuatity", SqlDbType.Int).Value = Convert.ToInt32(CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_quantity]);
                    _sql_cmd.Parameters.Add("@pWinLossDetailAmount", SqlDbType.Money).Value = Convert.ToDecimal(CageAmounts[_current_cage_amount.Key].ItemAmounts.Rows[_idx].ItemArray[m_sql_column_cage_amount_total]);

                    if (_sql_cmd.ExecuteNonQuery() <= 0)
                    {
                      return false;
                    }
                  }
                }
              }
            }
          }
        }
      }

      return true;
    } // InsertCageAmountsToWinLossTable

    #endregion

    #region Events

    private void btn_win_loss_edit_Click(object sender, EventArgs e)
    {
      try
      {
        if (dgv_win_loss.Rows.Count > 0)
        {
          if (IsSelectedRowEditable())
          {
            String _form_title;
            CageDenominationsItems.CageDenominationsDictionary _cage_amounts;
            GamingTableBusinessLogic.GT_WIN_LOSS_MODE _win_loss_mode;
            frm_amount_input _amount_input;

            _form_title = String.Format("{0}: {1} {2}", Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_WINLOSS")
                                                             , dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_TIME_SLOT].Value.ToString()
                                                             , dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_FORMATED].Value.ToString());

            _win_loss_mode = GamingTableBusinessLogic.GT_WIN_LOSS_MODE.NONE;
            _amount_input = new frm_amount_input();

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!GetGamingTablesWinLossIds(Convert.ToDateTime(dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED].Value), _db_trx))
              {
                _db_trx.Rollback();
                return;
              }
            }

            using (frm_yesno form_yes_no = new frm_yesno())
            {
              form_yes_no.Opacity = 0.6;
              form_yes_no.Show();
              _amount_input.ShowWinLossPublic(_form_title, VoucherTypes.WinLoss, form_yes_no, new List<Int64>(m_iso_code_associated_to_win_loss_id.Values), out _cage_amounts, out _win_loss_mode);
            }
            if (_amount_input.DialogResult != DialogResult.OK)
            {
              return;
            }

            using (DB_TRX _db_trx = new DB_TRX())
            {
              //Save the constants of the frm_amount_input columns to local variables
              m_sql_column_cage_amount_denomination = _amount_input.sql_column_cage_amount_denomination;
              m_sql_column_cage_amount_quantity = _amount_input.sql_column_cage_amount_quantity;
              m_sql_column_cage_amount_total = _amount_input.sql_column_cage_amount_total_amount;
              m_sql_column_cage_amount_chip_id = _amount_input.sql_column_cage_amount_chip_id;

              if (!InsertCageAmountsToWinLossTable(_cage_amounts, _win_loss_mode, _db_trx.SqlTransaction))
              {
                _db_trx.Rollback();
                return;
              }
              _db_trx.Commit();
            }

            //Refresh the grid with the new values ​​obtained from database
            GetTotalGamingTableWinLoss(); 
          }
          else
          {
            ShowNotEditableError(Resource.String("STR_GAMING_TABLE_WINLOSS_EDIT_ERROR"));
          }
        }
        else
        {
          //Refresh the grid with the new values ​​obtained from database
          GetTotalGamingTableWinLoss();
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        return;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return;
      }
    } // btn_win_loss_edit_Click

    /// <summary>
    /// Show the error message received by parameter when the selected row is not editable
    /// </summary>
    /// <param name="ErrorStr">Error message</param>
    private void ShowNotEditableError(String ErrorStr)
    {
      String _time_from;
      String _time_to;

      _time_from = Convert.ToDateTime(dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED].Value).AddMinutes(60 - m_margin_to_edit).ToString("HH:mm");
      _time_to = Convert.ToDateTime(dgv_win_loss.SelectedRows[0].Cells[GRID_COLUMN_WIN_LOSS_DATETIME_HOUR_UNFORMATED].Value).AddMinutes(60 + m_margin_to_edit).ToString("HH:mm");

      lbl_win_loss_error.Text = String.Format(ErrorStr, _time_from, _time_to);
      pnl_win_loss_error.Visible = false;
      System.Threading.Thread.Sleep(100);
      pnl_win_loss_error.Visible = true;

    } // ShowNotEditableError

    private void dgv_win_loss_SelectionChanged(object sender, EventArgs e)
    {
      pnl_win_loss_error.Visible = false;
    } // dgv_win_loss_SelectionChanged

    #endregion
  }
}