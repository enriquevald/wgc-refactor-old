﻿namespace WSI.Cashier
{
  partial class frm_new_card_prompt
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_card_reader1 = new WSI.Cashier.uc_card_reader();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.uc_card_reader1);
      this.pnl_data.Size = new System.Drawing.Size(627, 111);
      // 
      // uc_card_reader1
      // 
      this.uc_card_reader1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.uc_card_reader1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_card_reader1.ErrorMessageBelow = false;
      this.uc_card_reader1.InvalidCardTextVisible = false;
      this.uc_card_reader1.Location = new System.Drawing.Point(0, 0);
      this.uc_card_reader1.Margin = new System.Windows.Forms.Padding(5);
      this.uc_card_reader1.MaximumSize = new System.Drawing.Size(1600, 163);
      this.uc_card_reader1.MinimumSize = new System.Drawing.Size(1600, 109);
      this.uc_card_reader1.Name = "uc_card_reader1";
      this.uc_card_reader1.Size = new System.Drawing.Size(1600, 111);
      this.uc_card_reader1.TabIndex = 1;
      this.uc_card_reader1.OnTrackNumberReadEvent += new WSI.Cashier.uc_card_reader.TrackNumberReadEventHandler(this.uc_card_reader1_OnTrackNumberReadEvent);
      // 
      // frm_new_card_prompt
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(627, 166);
      this.ControlBox = false;
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.Name = "frm_new_card_prompt";
      this.Text = "frm_new_card_prompt";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_card_reader uc_card_reader1;

  }
}