//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_generic_string.cs
// 
//   DESCRIPTION: frm_generic_string
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 15-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-FEB-2016 FGB    First version.
// 16-FEB-2016 FGB    Added the window title parameter.
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_generic_string_input : frm_base
  {
    #region Constants
    private const int CATALOG_Y_POSITION_SIZE_BALANCE = 160;
    #endregion

    #region Attributes
    private frm_yesno m_form_yes_no;
    private String m_windows_title;
    private String m_label_text;
    private int m_max_text_length;
    private String m_generic_string;
    private Boolean m_ok;
    private Form m_parent;
    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_generic_string_input(String WindowTitle, String LabelText, int MaxTextLength)
    {
      if (!String.IsNullOrEmpty(WindowTitle))
      {
        m_windows_title = WindowTitle;
      }
      else
      {
        m_windows_title = Resource.String("STR_FRM_GENERIC_STRING_TITLE");      
      }

      m_label_text = LabelText;
      m_max_text_length = MaxTextLength;

      InitializeComponent();

      InitializeControlResources();

      Init();
    }

    /// <summary>
    /// Constructor without the WindowTitle parameter
    /// </summary>
    /// <param name="LabelText"></param>
    /// <param name="MaxTextLength"></param>
    public frm_generic_string_input(String LabelText, int MaxTextLength)
      : this(String.Empty, LabelText, MaxTextLength)
    {
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize screen logic
    /// </summary>
    private void Init()
    {
      // Center screen
      this.Location = new Point(1024 / 2 - this.Width / 2, CATALOG_Y_POSITION_SIZE_BALANCE);

      // Show keyboard if Automatic OSK it's enabled
      //WSIKeyboard.Toggle();
      m_generic_string = String.Empty;
      m_ok = false;
      m_form_yes_no = new frm_yesno();
   
      // Set focus
      this.txt_reason.Select();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize control resources (NLS strings, images, etc).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public void InitializeControlResources()
    {
      // - Title
      this.FormTitle = m_windows_title;

      // - Show label
      lbl_description.Text = m_label_text;
      lbl_description.Visible = true;
      lbl_error_string_empty.Text = Resource.String("STR_FRM_GENERIC_STRING_TEXT_REQUIRED");
      lbl_error_string_empty.Visible = false;

      // - Lenght
      txt_reason.MaxLength = m_max_text_length;
    
      btn_accept.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
    } // InitializeControlResources

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Init and Show the form.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ParentForm
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void Show(Form ParentForm, out Boolean IsOK, out String GenericString)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_generic_string_input", Log.Type.Message);
      m_parent = ParentForm;

      try
      {
        m_form_yes_no.Show(ParentForm);
        this.ShowDialog(m_form_yes_no);
        m_form_yes_no.Hide();
        IsOK = m_ok;
        GenericString = m_generic_string;

        Misc.WriteLog("[FORM CLOSE] frm_generic_string_input", Log.Type.Message);
        this.Close();
      }
      catch (Exception _ex)
      {
        IsOK = false;
        GenericString = String.Empty;
        Log.Exception(_ex);
        m_form_yes_no.Hide();
      }
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    new public void Dispose()
    {
      base.Dispose();
    }

    #endregion
    
    #region ButtonHandling

    private void CloseForm()
    {
      // Hide keyboard and close
      WSIKeyboard.Hide();

      Misc.WriteLog("[FORM CLOSE] frm_generic_string_inut (closeform)", Log.Type.Message);
      this.Close();
    }

    private void btn_accept_Click(object sender, EventArgs e)
    {
      if (this.txt_reason.Text.Length == 0)
      {
        m_ok = false;
        m_generic_string = String.Empty;
        lbl_error_string_empty.Visible = true;
        //frm_message.Show(Resource.String("STR_FRM_GENERIC_STRING_TEXT_EMPTY"), "", MessageBoxButtons.OK, Images.CashierImage.Warning, Cashier.MainForm);

        return;
      }

      lbl_error_string_empty.Visible = false;
      m_generic_string = this.txt_reason.Text;
      m_ok = true;

      CloseForm();
    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      lbl_error_string_empty.Visible = false;
      m_generic_string = string.Empty;
      m_ok = false;

      CloseForm();
    }

    private void btn_keyboard_Click(object sender, EventArgs e)
    {
      // show keyboard (if hidden)
      WSIKeyboard.ForceToggle();
    }

    #endregion

  }
}
