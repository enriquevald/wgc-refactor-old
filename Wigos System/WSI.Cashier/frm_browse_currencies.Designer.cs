namespace WSI.Cashier
{
  partial class frm_browse_currencies
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_browse_currencies_title = new WSI.Cashier.Controls.uc_label();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.pnl_container);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Dock = System.Windows.Forms.DockStyle.None;
      this.pnl_data.Size = new System.Drawing.Size(336, 250);
      // 
      // lbl_browse_currencies_title
      // 
      this.lbl_browse_currencies_title.BackColor = System.Drawing.Color.Transparent;
      this.lbl_browse_currencies_title.Font = new System.Drawing.Font("Open Sans Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_browse_currencies_title.ForeColor = System.Drawing.Color.White;
      this.lbl_browse_currencies_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_browse_currencies_title.Name = "lbl_browse_currencies_title";
      this.lbl_browse_currencies_title.Size = new System.Drawing.Size(349, 18);
      this.lbl_browse_currencies_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.lbl_browse_currencies_title.TabIndex = 2;
      this.lbl_browse_currencies_title.Text = "xCurrencies Exchange";
      // 
      // pnl_container
      // 
      this.pnl_container.AutoScroll = true;
      this.pnl_container.Location = new System.Drawing.Point(4, 3);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(332, 178);
      this.pnl_container.TabIndex = 24;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.IsSelected = false;
      this.btn_cancel.Location = new System.Drawing.Point(177, 187);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 23;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // frm_browse_currencies
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(339, 310);
      this.Name = "frm_browse_currencies";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_browse_currencies";
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    //private System.Windows.Forms.SplitContainer splitContainer1;
    private WSI.Cashier.Controls.uc_label lbl_browse_currencies_title;
    private WSI.Cashier.Controls.uc_round_button btn_cancel;
    private System.Windows.Forms.Panel pnl_container;

  }
}