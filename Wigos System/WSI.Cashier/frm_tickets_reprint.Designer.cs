namespace WSI.Cashier
{
  partial class frm_tickets_reprint
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.lbl_tickets_reprint_title = new System.Windows.Forms.Label();
      this.gb_search = new System.Windows.Forms.GroupBox();
      this.lbl_ticket_number = new System.Windows.Forms.Label();
      this.txt_floorID = new System.Windows.Forms.TextBox();
      this.lbl_terminal = new System.Windows.Forms.Label();
      this.lbl_floor_id = new System.Windows.Forms.Label();
      this.btn_ticket_search = new System.Windows.Forms.Button();
      this.btn_print_selected = new System.Windows.Forms.Button();
      this.gb_last_operations = new System.Windows.Forms.GroupBox();
      this.dgv_last_operations = new System.Windows.Forms.DataGridView();
      this.btn_close = new System.Windows.Forms.Button();
      this.txt_ticket_number = new WSI.Cashier.NumericTextBox();
      this.uc_prov_terminals = new WSI.Cashier.uc_terminal_filter();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.gb_search.SuspendLayout();
      this.gb_last_operations.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).BeginInit();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackColor = System.Drawing.Color.CornflowerBlue;
      this.splitContainer1.Panel1.Controls.Add(this.lbl_tickets_reprint_title);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.BackColor = System.Drawing.Color.MediumTurquoise;
      this.splitContainer1.Panel2.Controls.Add(this.gb_search);
      this.splitContainer1.Panel2.Controls.Add(this.btn_print_selected);
      this.splitContainer1.Panel2.Controls.Add(this.gb_last_operations);
      this.splitContainer1.Panel2.Controls.Add(this.btn_close);
      this.splitContainer1.Size = new System.Drawing.Size(806, 749);
      this.splitContainer1.SplitterDistance = 25;
      this.splitContainer1.TabIndex = 1;
      // 
      // lbl_tickets_reprint_title
      // 
      this.lbl_tickets_reprint_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_tickets_reprint_title.ForeColor = System.Drawing.Color.White;
      this.lbl_tickets_reprint_title.Location = new System.Drawing.Point(3, 5);
      this.lbl_tickets_reprint_title.Name = "lbl_tickets_reprint_title";
      this.lbl_tickets_reprint_title.Size = new System.Drawing.Size(512, 16);
      this.lbl_tickets_reprint_title.TabIndex = 2;
      this.lbl_tickets_reprint_title.Text = "xTicketsReprint";
      // 
      // gb_search
      // 
      this.gb_search.Controls.Add(this.txt_ticket_number);
      this.gb_search.Controls.Add(this.uc_prov_terminals);
      this.gb_search.Controls.Add(this.lbl_ticket_number);
      this.gb_search.Controls.Add(this.txt_floorID);
      this.gb_search.Controls.Add(this.lbl_terminal);
      this.gb_search.Controls.Add(this.lbl_floor_id);
      this.gb_search.Controls.Add(this.btn_ticket_search);
      this.gb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.gb_search.Location = new System.Drawing.Point(6, 3);
      this.gb_search.Name = "gb_search";
      this.gb_search.Size = new System.Drawing.Size(689, 317);
      this.gb_search.TabIndex = 0;
      this.gb_search.TabStop = false;
      this.gb_search.Text = "xSource";
      // 
      // lbl_ticket_number
      // 
      this.lbl_ticket_number.AutoSize = true;
      this.lbl_ticket_number.Location = new System.Drawing.Point(10, 278);
      this.lbl_ticket_number.Name = "lbl_ticket_number";
      this.lbl_ticket_number.Size = new System.Drawing.Size(114, 20);
      this.lbl_ticket_number.TabIndex = 106;
      this.lbl_ticket_number.Text = "xTicketNumber";
      this.lbl_ticket_number.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // txt_floorID
      // 
      this.txt_floorID.Location = new System.Drawing.Point(120, 243);
      this.txt_floorID.Name = "txt_floorID";
      this.txt_floorID.Size = new System.Drawing.Size(282, 26);
      this.txt_floorID.TabIndex = 105;
      // 
      // lbl_terminal
      // 
      this.lbl_terminal.AutoSize = true;
      this.lbl_terminal.Location = new System.Drawing.Point(25, 30);
      this.lbl_terminal.Name = "lbl_terminal";
      this.lbl_terminal.Size = new System.Drawing.Size(76, 20);
      this.lbl_terminal.TabIndex = 103;
      this.lbl_terminal.Text = "xTerminal";
      this.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lbl_floor_id
      // 
      this.lbl_floor_id.AutoSize = true;
      this.lbl_floor_id.Location = new System.Drawing.Point(10, 246);
      this.lbl_floor_id.Name = "lbl_floor_id";
      this.lbl_floor_id.Size = new System.Drawing.Size(66, 20);
      this.lbl_floor_id.TabIndex = 101;
      this.lbl_floor_id.Text = "xFloorId";
      this.lbl_floor_id.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // btn_ticket_search
      // 
      this.btn_ticket_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ticket_search.BackColor = System.Drawing.Color.PaleTurquoise;
      this.btn_ticket_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_ticket_search.Location = new System.Drawing.Point(523, 250);
      this.btn_ticket_search.Name = "btn_ticket_search";
      this.btn_ticket_search.Size = new System.Drawing.Size(137, 47);
      this.btn_ticket_search.TabIndex = 9;
      this.btn_ticket_search.Text = "xBuscar";
      this.btn_ticket_search.UseVisualStyleBackColor = false;
      this.btn_ticket_search.Click += new System.EventHandler(this.btn_ticket_search_Click);
      // 
      // btn_print_selected
      // 
      this.btn_print_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_print_selected.BackColor = System.Drawing.Color.PaleTurquoise;
      this.btn_print_selected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_print_selected.Location = new System.Drawing.Point(319, 659);
      this.btn_print_selected.Name = "btn_print_selected";
      this.btn_print_selected.Size = new System.Drawing.Size(128, 48);
      this.btn_print_selected.TabIndex = 0;
      this.btn_print_selected.Text = "xPrintSelected";
      this.btn_print_selected.UseVisualStyleBackColor = false;
      this.btn_print_selected.Click += new System.EventHandler(this.btn_print_selected_Click);
      // 
      // gb_last_operations
      // 
      this.gb_last_operations.Controls.Add(this.dgv_last_operations);
      this.gb_last_operations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.gb_last_operations.Location = new System.Drawing.Point(6, 326);
      this.gb_last_operations.Name = "gb_last_operations";
      this.gb_last_operations.Size = new System.Drawing.Size(792, 321);
      this.gb_last_operations.TabIndex = 95;
      this.gb_last_operations.TabStop = false;
      this.gb_last_operations.Text = "xLastOperations";
      // 
      // dgv_last_operations
      // 
      this.dgv_last_operations.AllowUserToAddRows = false;
      this.dgv_last_operations.AllowUserToDeleteRows = false;
      this.dgv_last_operations.AllowUserToResizeColumns = false;
      this.dgv_last_operations.AllowUserToResizeRows = false;
      this.dgv_last_operations.BorderStyle = System.Windows.Forms.BorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.Color.PaleTurquoise;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      this.dgv_last_operations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgv_last_operations.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgv_last_operations.GridColor = System.Drawing.Color.LightGray;
      this.dgv_last_operations.Location = new System.Drawing.Point(3, 22);
      this.dgv_last_operations.MultiSelect = false;
      this.dgv_last_operations.Name = "dgv_last_operations";
      this.dgv_last_operations.ReadOnly = true;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.PaleTurquoise;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgv_last_operations.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
      this.dgv_last_operations.RowHeadersVisible = false;
      this.dgv_last_operations.RowHeadersWidth = 20;
      this.dgv_last_operations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgv_last_operations.Size = new System.Drawing.Size(786, 296);
      this.dgv_last_operations.TabIndex = 0;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.PaleTurquoise;
      this.btn_close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btn_close.Location = new System.Drawing.Point(655, 659);
      this.btn_close.Name = "btn_close";
      this.btn_close.Size = new System.Drawing.Size(128, 48);
      this.btn_close.TabIndex = 1;
      this.btn_close.Text = "xClose/Cancel";
      this.btn_close.UseVisualStyleBackColor = false;
      this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
      // 
      // txt_ticket_number
      // 
      this.txt_ticket_number.AllowSpace = false;
      this.txt_ticket_number.FillWithCeros = false;
      this.txt_ticket_number.Location = new System.Drawing.Point(120, 275);
      this.txt_ticket_number.Name = "txt_ticket_number";
      this.txt_ticket_number.Size = new System.Drawing.Size(282, 26);
      this.txt_ticket_number.TabIndex = 108;
      this.txt_ticket_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // uc_prov_terminals
      // 
      this.uc_prov_terminals.BackColor = System.Drawing.Color.MediumTurquoise;
      this.uc_prov_terminals.Location = new System.Drawing.Point(14, 27);
      this.uc_prov_terminals.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.uc_prov_terminals.Name = "uc_prov_terminals";
      this.uc_prov_terminals.Size = new System.Drawing.Size(657, 204);
      this.uc_prov_terminals.TabIndex = 96;
      // 
      // frm_tickets_reprint
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(806, 749);
      this.Controls.Add(this.splitContainer1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_tickets_reprint";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Vouchers Reprint";
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.gb_search.ResumeLayout(false);
      this.gb_search.PerformLayout();
      this.gb_last_operations.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgv_last_operations)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Label lbl_tickets_reprint_title;
    private System.Windows.Forms.Button btn_print_selected;
    private System.Windows.Forms.GroupBox gb_last_operations;
    private System.Windows.Forms.DataGridView dgv_last_operations;
    private System.Windows.Forms.Button btn_close;
    private System.Windows.Forms.Button btn_ticket_search;
    private System.Windows.Forms.GroupBox gb_search;
    private System.Windows.Forms.Label lbl_floor_id;
    private System.Windows.Forms.Label lbl_terminal;
    private System.Windows.Forms.TextBox txt_floorID;
    private System.Windows.Forms.Label lbl_ticket_number;
    private uc_terminal_filter uc_prov_terminals;
    private NumericTextBox txt_ticket_number;

  }
}