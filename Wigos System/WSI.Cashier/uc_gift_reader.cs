//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : uc_gift_reader.cs
// 
//   DESCRIPTION : Implements the uc_gift_reader user control for card track data 
//                 input (uc_gift_reader)
//
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-SEP-2010 TJG     First release.
// 26-SEP-2014 DRV     Added KeyboardMessageEvent.
// 09-NOV-2015 ECP     Backlog Item 5791 New Cashier Design
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

namespace WSI.Cashier
{
  public partial class uc_gift_reader : UserControl
  {
    public delegate void VoucherNumberReadEventHandler(String ExternalCode, ref Boolean IsValid);
    public delegate void VoucherNumberReadingHandler();

    public event VoucherNumberReadEventHandler OnVoucherTrackNumberReadEvent;
    public event VoucherNumberReadingHandler OnVoucherTrackNumberReadingEvent;

    private const Int32 GIFT_VOUCHER_NUMBER_LENGTH = 20;

    #region Class Atributes

    private Int32 m_tick_last_change = 0;
    private Boolean is_kbd_msg_filter_event = false;
    private String received_track_data;
    ////private Boolean m_all_data_entered = false;

    #endregion

    #region Constructor

    public uc_gift_reader()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[UC LOAD] uc_gift_reader", Log.Type.Message);

      InitializeComponent();

      InitializeControlResources();

      if (!this.DesignMode)
      {
        tmr_clear.Enabled = true;
        ClearVoucherNumber();
        txt_voucher_number.MaxLength = GIFT_VOUCHER_NUMBER_LENGTH;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources. (NLS string, images, etc.)
    /// </summary>
    public void InitializeControlResources()
    {
      // NLS Strings
      lbl_title.Text = Resource.String("STR_UC_GIFT_READER_001");
      lbl_message.Text = Resource.String("STR_UC_GIFT_READER_002");
      
      pic_logo.Image = Resources.ResourceImages.gift_header;
    } // InitializeControlResources

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void pic_logo_Click(object sender, EventArgs e)
    {
      ClearVoucherNumber();
      txt_voucher_number.Focus();
    } // pic_logo_Click

    /// <summary>
    /// Clear input for track number actions.
    /// </summary>
    private void ClearVoucherNumber()
    {
      m_tick_last_change = 0;
      ////m_all_data_entered = false;
      txt_voucher_number.Text = "";
      lbl_message.Visible = false;

    } // ClearVoucherNumber

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_TextChanged(object sender, EventArgs e)
    {
      String _track_number;

      // Variables initialization
      m_tick_last_change = 0;

      if (is_kbd_msg_filter_event)
      {
        _track_number = received_track_data;
      }
      else
      {
        _track_number = txt_voucher_number.Text;
      }

      if (_track_number.Length >= GIFT_VOUCHER_NUMBER_LENGTH)
      {
        ////m_all_data_entered = true;
      }
      else
      {
        ////m_all_data_entered = false;

        if (_track_number.Length > 0)
        {
          if (OnVoucherTrackNumberReadingEvent != null)
          {
            OnVoucherTrackNumberReadingEvent();
          }

          lbl_message.Visible = !Barcode.IsValid(_track_number);
        }
      }
    } // txt_track_number_TextChanged

    /// <summary>
    /// Key pressed management.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_track_number_KeyPress(object sender, KeyPressEventArgs e)
    {
      String _track_number;
      Boolean _is_valid;

      m_tick_last_change = 0;
      
      if (is_kbd_msg_filter_event)
      {
        _track_number = received_track_data;
        is_kbd_msg_filter_event = false;
      }
      else
      {
        _track_number = txt_voucher_number.Text;
      }

      switch (e.KeyChar)
      {
        case '\r':
          e.Handled = true;

          if (_track_number.Length >= GIFT_VOUCHER_NUMBER_LENGTH)
          {
            Barcode _barcode;

            _is_valid = Barcode.TryParse(_track_number, out _barcode);
            if (_is_valid)
            {
              if (_barcode.Type != BarcodeType.Gift)
              {
                _is_valid = false;
              }
            }

            if (_is_valid)
            {
              tmr_clear.Enabled = false;
              if (OnVoucherTrackNumberReadEvent != null)
              {
                OnVoucherTrackNumberReadEvent(_track_number, ref _is_valid);
              }
              tmr_clear.Enabled = true;
            }

            if (!_is_valid)
            {
              txt_voucher_number.SelectAll();

              lbl_message.Visible = true;
            }
            else
            {
              ClearVoucherNumber();
            }
          }
          break;

        default:
          break;
      } // switch
    } // txt_track_number_KeyPress

    /// <summary>
    /// Timer to clear card input, if key is not pressed in five seconds.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void tmr_clear_Tick(object sender, EventArgs e)
    {
      m_tick_last_change += 1;
      if (m_tick_last_change > 20) // 5 seconds
      {
        ClearVoucherNumber();
        m_tick_last_change = 0;
      }
    }

    private void GiftReceivedEvent(KbdMsgEvent e, Object data)
    {
      Barcode _barcode;
      KeyPressEventArgs _event_args;

      if (e == KbdMsgEvent.EventBarcode)
      {
        _barcode = (Barcode)data;
        received_track_data = _barcode.ExternalCode;

        is_kbd_msg_filter_event = true;
        txt_track_number_TextChanged(this, new EventArgs());
        _event_args = new KeyPressEventArgs('\r');
        txt_track_number_KeyPress(this, _event_args);
      }
    }

    private void uc_gift_reader_VisibleChanged(object sender, EventArgs e)
    {
      if (this.Visible)
      {
        KeyboardMessageFilter.RegisterHandler(this, BarcodeType.Gift, GiftReceivedEvent);
      }
    } // tmr_clear_Tick
    
    #endregion
  }
}
