namespace WSI.Cashier
{
  partial class frm_withholding
  {  
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.label3 = new WSI.Cashier.Controls.uc_label();
      this.label4 = new WSI.Cashier.Controls.uc_label();
      this.textBox1 = new WSI.Cashier.Controls.uc_round_textbox();
      this.comboBox1 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label5 = new WSI.Cashier.Controls.uc_label();
      this.comboBox2 = new WSI.Cashier.Controls.uc_round_combobox();
      this.label6 = new WSI.Cashier.Controls.uc_label();
      this.label7 = new WSI.Cashier.Controls.uc_label();
      this.label8 = new WSI.Cashier.Controls.uc_label();
      this.label9 = new WSI.Cashier.Controls.uc_label();
      this.textBox2 = new WSI.Cashier.Controls.uc_round_textbox();
      this.gb_principal = new WSI.Cashier.Controls.uc_round_panel();
      this.ef_holder_id_01 = new WSI.Cashier.CharacterTextBox();
      this.lbl_expected_rfc = new WSI.Cashier.Controls.uc_label();
      this.cb_gender = new WSI.Cashier.Controls.uc_round_combobox();
      this.ef_holder_id_02 = new WSI.Cashier.CharacterTextBox();
      this.lbl_holder_id_01 = new WSI.Cashier.Controls.uc_label();
      this.lbl_gender = new WSI.Cashier.Controls.uc_label();
      this.lbl_holder_id_02 = new WSI.Cashier.Controls.uc_label();
      this.lbl_name_01 = new WSI.Cashier.Controls.uc_label();
      this.lbl_name_03 = new WSI.Cashier.Controls.uc_label();
      this.ef_name_03 = new WSI.Cashier.CharacterTextBox();
      this.ef_name_02 = new WSI.Cashier.CharacterTextBox();
      this.ef_name_01 = new WSI.Cashier.CharacterTextBox();
      this.lbl_name_02 = new WSI.Cashier.Controls.uc_label();
      this.uc_birth_date = new WSI.Cashier.uc_datetime();
      this.btn_cancel = new WSI.Cashier.Controls.uc_round_button();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.lbl_msg_blink = new WSI.Cashier.Controls.uc_label();
      this.btn_keyboard = new WSI.Cashier.Controls.uc_round_button();
      this.numericTextBox1 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox2 = new WSI.Cashier.NumericTextBox();
      this.numericTextBox3 = new WSI.Cashier.NumericTextBox();
      this.pnl_data.SuspendLayout();
      this.gb_principal.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.BackColor = System.Drawing.Color.Transparent;
      this.pnl_data.Controls.Add(this.gb_principal);
      this.pnl_data.Controls.Add(this.btn_cancel);
      this.pnl_data.Controls.Add(this.btn_ok);
      this.pnl_data.Controls.Add(this.lbl_msg_blink);
      this.pnl_data.Controls.Add(this.btn_keyboard);
      this.pnl_data.Size = new System.Drawing.Size(770, 458);
      this.pnl_data.TabIndex = 0;
      // 
      // timer1
      // 
      this.timer1.Interval = 4000;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // label3
      // 
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(365, 130);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 38);
      this.label3.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label3.TabIndex = 0;
      this.label3.Text = "/";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.Color.Transparent;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(6, 77);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(144, 38);
      this.label4.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label4.TabIndex = 0;
      this.label4.Text = "xC�digo Elector";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox1
      // 
      this.textBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox1.BackColor = System.Drawing.Color.White;
      this.textBox1.BorderColor = System.Drawing.Color.Empty;
      this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox1.CornerRadius = 0;
      this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox1.Location = new System.Drawing.Point(156, 76);
      this.textBox1.MaxLength = 20;
      this.textBox1.Multiline = false;
      this.textBox1.Name = "textBox1";
      this.textBox1.PasswordChar = '\0';
      this.textBox1.ReadOnly = false;
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox1.SelectedText = "";
      this.textBox1.SelectionLength = 0;
      this.textBox1.SelectionStart = 0;
      this.textBox1.Size = new System.Drawing.Size(510, 38);
      this.textBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox1.TabIndex = 0;
      this.textBox1.TabStop = false;
      this.textBox1.Text = "AAAAAAAAAAAAAAAAAAAAAAAAAB";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox1.UseSystemPasswordChar = false;
      this.textBox1.WaterMark = null;
      // 
      // comboBox1
      // 
      this.comboBox1.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox1.BackColor = System.Drawing.Color.White;
      this.comboBox1.BorderColor = System.Drawing.Color.White;
      this.comboBox1.CornerRadius = 5;
      this.comboBox1.DataSource = null;
      this.comboBox1.DisplayMember = "";
      this.comboBox1.DrawMode = System.Windows.Forms.DrawMode.Normal;
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.DropDownWidth = 174;
      this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.IsDroppedDown = false;
      this.comboBox1.ItemHeight = 21;
      this.comboBox1.Location = new System.Drawing.Point(156, 238);
      this.comboBox1.MaxDropDownItems = 8;
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.SelectedIndex = -1;
      this.comboBox1.SelectedItem = null;
      this.comboBox1.SelectedValue = null;
      this.comboBox1.SelectionLength = 0;
      this.comboBox1.SelectionStart = 0;
      this.comboBox1.Size = new System.Drawing.Size(174, 50);
      this.comboBox1.Sorted = false;
      this.comboBox1.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox1.TabIndex = 0;
      this.comboBox1.TabStop = false;
      this.comboBox1.ValueMember = "";
      // 
      // label5
      // 
      this.label5.BackColor = System.Drawing.Color.Transparent;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(6, 238);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(144, 38);
      this.label5.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label5.TabIndex = 0;
      this.label5.Text = "xEstado Civil";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // comboBox2
      // 
      this.comboBox2.ArrowColor = System.Drawing.Color.Empty;
      this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.comboBox2.BackColor = System.Drawing.Color.White;
      this.comboBox2.BorderColor = System.Drawing.Color.White;
      this.comboBox2.CornerRadius = 5;
      this.comboBox2.DataSource = null;
      this.comboBox2.DisplayMember = "";
      this.comboBox2.DrawMode = System.Windows.Forms.DrawMode.Normal;
      this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox2.DropDownWidth = 149;
      this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.comboBox2.FormattingEnabled = true;
      this.comboBox2.IsDroppedDown = false;
      this.comboBox2.ItemHeight = 21;
      this.comboBox2.Location = new System.Drawing.Point(156, 184);
      this.comboBox2.MaxDropDownItems = 8;
      this.comboBox2.Name = "comboBox2";
      this.comboBox2.SelectedIndex = -1;
      this.comboBox2.SelectedItem = null;
      this.comboBox2.SelectedValue = null;
      this.comboBox2.SelectionLength = 0;
      this.comboBox2.SelectionStart = 0;
      this.comboBox2.Size = new System.Drawing.Size(149, 50);
      this.comboBox2.Sorted = false;
      this.comboBox2.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.NONE;
      this.comboBox2.TabIndex = 0;
      this.comboBox2.TabStop = false;
      this.comboBox2.ValueMember = "";
      // 
      // label6
      // 
      this.label6.BackColor = System.Drawing.Color.Transparent;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.Location = new System.Drawing.Point(6, 184);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(144, 38);
      this.label6.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label6.TabIndex = 0;
      this.label6.Text = "xGenero";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.Color.Transparent;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label7.Location = new System.Drawing.Point(248, 130);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(19, 38);
      this.label7.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label7.TabIndex = 0;
      this.label7.Text = "/";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label8
      // 
      this.label8.BackColor = System.Drawing.Color.Transparent;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.Location = new System.Drawing.Point(6, 131);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(144, 38);
      this.label8.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label8.TabIndex = 0;
      this.label8.Text = "xNacimiento";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.BackColor = System.Drawing.Color.Transparent;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label9.Location = new System.Drawing.Point(6, 23);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(144, 38);
      this.label9.Style = WSI.Cashier.Controls.uc_label.LabelStyle.NONE;
      this.label9.TabIndex = 0;
      this.label9.Text = "xName";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // textBox2
      // 
      this.textBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.textBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.textBox2.BackColor = System.Drawing.Color.White;
      this.textBox2.BorderColor = System.Drawing.Color.Empty;
      this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.textBox2.CornerRadius = 0;
      this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox2.Location = new System.Drawing.Point(156, 22);
      this.textBox2.MaxLength = 50;
      this.textBox2.Multiline = false;
      this.textBox2.Name = "textBox2";
      this.textBox2.PasswordChar = '\0';
      this.textBox2.ReadOnly = false;
      this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.textBox2.SelectedText = "";
      this.textBox2.SelectionLength = 0;
      this.textBox2.SelectionStart = 0;
      this.textBox2.Size = new System.Drawing.Size(510, 38);
      this.textBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.textBox2.TabIndex = 0;
      this.textBox2.TabStop = false;
      this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.textBox2.UseSystemPasswordChar = false;
      this.textBox2.WaterMark = null;
      // 
      // gb_principal
      // 
      this.gb_principal.BackColor = System.Drawing.Color.Transparent;
      this.gb_principal.Controls.Add(this.ef_holder_id_01);
      this.gb_principal.Controls.Add(this.lbl_expected_rfc);
      this.gb_principal.Controls.Add(this.cb_gender);
      this.gb_principal.Controls.Add(this.ef_holder_id_02);
      this.gb_principal.Controls.Add(this.lbl_holder_id_01);
      this.gb_principal.Controls.Add(this.lbl_gender);
      this.gb_principal.Controls.Add(this.lbl_holder_id_02);
      this.gb_principal.Controls.Add(this.lbl_name_01);
      this.gb_principal.Controls.Add(this.lbl_name_03);
      this.gb_principal.Controls.Add(this.ef_name_03);
      this.gb_principal.Controls.Add(this.ef_name_02);
      this.gb_principal.Controls.Add(this.ef_name_01);
      this.gb_principal.Controls.Add(this.lbl_name_02);
      this.gb_principal.Controls.Add(this.uc_birth_date);
      this.gb_principal.CornerRadius = 10;
      this.gb_principal.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_principal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_principal.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_principal.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_principal.HeaderHeight = 35;
      this.gb_principal.HeaderSubText = null;
      this.gb_principal.HeaderText = "DATOS";
      this.gb_principal.Location = new System.Drawing.Point(12, 18);
      this.gb_principal.Name = "gb_principal";
      this.gb_principal.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_principal.Size = new System.Drawing.Size(744, 351);
      this.gb_principal.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_principal.TabIndex = 0;
      // 
      // ef_holder_id_01
      // 
      this.ef_holder_id_01.AllowSpace = true;
      this.ef_holder_id_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_holder_id_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_holder_id_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_holder_id_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_holder_id_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_holder_id_01.CornerRadius = 5;
      this.ef_holder_id_01.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_holder_id_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_holder_id_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_holder_id_01.Location = new System.Drawing.Point(146, 56);
      this.ef_holder_id_01.MaxLength = 13;
      this.ef_holder_id_01.Multiline = false;
      this.ef_holder_id_01.Name = "ef_holder_id_01";
      this.ef_holder_id_01.PasswordChar = '\0';
      this.ef_holder_id_01.ReadOnly = false;
      this.ef_holder_id_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_holder_id_01.SelectedText = "";
      this.ef_holder_id_01.SelectionLength = 0;
      this.ef_holder_id_01.SelectionStart = 0;
      this.ef_holder_id_01.Size = new System.Drawing.Size(293, 40);
      this.ef_holder_id_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_holder_id_01.TabIndex = 1;
      this.ef_holder_id_01.TabStop = false;
      this.ef_holder_id_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_holder_id_01.UseSystemPasswordChar = false;
      this.ef_holder_id_01.WaterMark = null;
      this.ef_holder_id_01.TextChanged += new System.EventHandler(this.DataChanged);
      // 
      // lbl_expected_rfc
      // 
      this.lbl_expected_rfc.AutoSize = true;
      this.lbl_expected_rfc.BackColor = System.Drawing.Color.Transparent;
      this.lbl_expected_rfc.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_expected_rfc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_expected_rfc.Location = new System.Drawing.Point(440, 68);
      this.lbl_expected_rfc.Name = "lbl_expected_rfc";
      this.lbl_expected_rfc.Size = new System.Drawing.Size(123, 18);
      this.lbl_expected_rfc.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_expected_rfc.TabIndex = 2;
      this.lbl_expected_rfc.Text = "xMESSAGE LINE";
      this.lbl_expected_rfc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cb_gender
      // 
      this.cb_gender.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.cb_gender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.cb_gender.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.cb_gender.CornerRadius = 5;
      this.cb_gender.DataSource = null;
      this.cb_gender.DisplayMember = "";
      this.cb_gender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
      this.cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_gender.DropDownWidth = 95;
      this.cb_gender.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.cb_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(73)))), ((int)(((byte)(84)))));
      this.cb_gender.FormattingEnabled = false;
      this.cb_gender.IsDroppedDown = false;
      this.cb_gender.ItemHeight = 40;
      this.cb_gender.Location = new System.Drawing.Point(626, 289);
      this.cb_gender.MaxDropDownItems = 8;
      this.cb_gender.Name = "cb_gender";
      this.cb_gender.SelectedIndex = -1;
      this.cb_gender.SelectedItem = null;
      this.cb_gender.SelectedValue = null;
      this.cb_gender.SelectionLength = 0;
      this.cb_gender.SelectionStart = 0;
      this.cb_gender.Size = new System.Drawing.Size(95, 40);
      this.cb_gender.Sorted = false;
      this.cb_gender.Style = WSI.Cashier.Controls.uc_round_combobox.RoundComboBoxStyle.BASIC;
      this.cb_gender.TabIndex = 13;
      this.cb_gender.TabStop = false;
      this.cb_gender.ValueMember = "";
      // 
      // ef_holder_id_02
      // 
      this.ef_holder_id_02.AllowSpace = true;
      this.ef_holder_id_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_holder_id_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_holder_id_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_holder_id_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_holder_id_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_holder_id_02.CornerRadius = 5;
      this.ef_holder_id_02.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_holder_id_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_holder_id_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.ExtendedMode;
      this.ef_holder_id_02.Location = new System.Drawing.Point(146, 102);
      this.ef_holder_id_02.MaxLength = 20;
      this.ef_holder_id_02.Multiline = false;
      this.ef_holder_id_02.Name = "ef_holder_id_02";
      this.ef_holder_id_02.PasswordChar = '\0';
      this.ef_holder_id_02.ReadOnly = false;
      this.ef_holder_id_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_holder_id_02.SelectedText = "";
      this.ef_holder_id_02.SelectionLength = 0;
      this.ef_holder_id_02.SelectionStart = 0;
      this.ef_holder_id_02.Size = new System.Drawing.Size(293, 40);
      this.ef_holder_id_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_holder_id_02.TabIndex = 4;
      this.ef_holder_id_02.TabStop = false;
      this.ef_holder_id_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_holder_id_02.UseSystemPasswordChar = false;
      this.ef_holder_id_02.WaterMark = null;
      // 
      // lbl_holder_id_01
      // 
      this.lbl_holder_id_01.AutoSize = true;
      this.lbl_holder_id_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_id_01.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_id_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_id_01.Location = new System.Drawing.Point(96, 68);
      this.lbl_holder_id_01.Name = "lbl_holder_id_01";
      this.lbl_holder_id_01.Size = new System.Drawing.Size(45, 18);
      this.lbl_holder_id_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_id_01.TabIndex = 0;
      this.lbl_holder_id_01.Text = "xRFC";
      this.lbl_holder_id_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_gender
      // 
      this.lbl_gender.AutoSize = true;
      this.lbl_gender.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gender.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gender.Location = new System.Drawing.Point(539, 300);
      this.lbl_gender.Name = "lbl_gender";
      this.lbl_gender.Size = new System.Drawing.Size(69, 18);
      this.lbl_gender.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_gender.TabIndex = 12;
      this.lbl_gender.Text = "xGender";
      this.lbl_gender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_holder_id_02
      // 
      this.lbl_holder_id_02.AutoSize = true;
      this.lbl_holder_id_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_holder_id_02.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_holder_id_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_holder_id_02.Location = new System.Drawing.Point(84, 114);
      this.lbl_holder_id_02.Name = "lbl_holder_id_02";
      this.lbl_holder_id_02.Size = new System.Drawing.Size(56, 18);
      this.lbl_holder_id_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_holder_id_02.TabIndex = 3;
      this.lbl_holder_id_02.Text = "xCURP";
      this.lbl_holder_id_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_name_01
      // 
      this.lbl_name_01.AutoSize = true;
      this.lbl_name_01.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_01.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_01.Location = new System.Drawing.Point(57, 160);
      this.lbl_name_01.Name = "lbl_name_01";
      this.lbl_name_01.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.lbl_name_01.Size = new System.Drawing.Size(82, 18);
      this.lbl_name_01.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name_01.TabIndex = 5;
      this.lbl_name_01.Text = "xFam N 01";
      this.lbl_name_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_name_03
      // 
      this.lbl_name_03.AutoSize = true;
      this.lbl_name_03.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_03.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_03.Location = new System.Drawing.Point(71, 250);
      this.lbl_name_03.Name = "lbl_name_03";
      this.lbl_name_03.Size = new System.Drawing.Size(63, 18);
      this.lbl_name_03.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name_03.TabIndex = 9;
      this.lbl_name_03.Text = "xFirst N";
      this.lbl_name_03.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // ef_name_03
      // 
      this.ef_name_03.AllowSpace = true;
      this.ef_name_03.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_03.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_03.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_03.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_03.CornerRadius = 5;
      this.ef_name_03.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_03.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_name_03.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_03.Location = new System.Drawing.Point(146, 240);
      this.ef_name_03.MaxLength = 50;
      this.ef_name_03.Multiline = false;
      this.ef_name_03.Name = "ef_name_03";
      this.ef_name_03.PasswordChar = '\0';
      this.ef_name_03.ReadOnly = false;
      this.ef_name_03.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_03.SelectedText = "";
      this.ef_name_03.SelectionLength = 0;
      this.ef_name_03.SelectionStart = 0;
      this.ef_name_03.Size = new System.Drawing.Size(576, 40);
      this.ef_name_03.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_name_03.TabIndex = 10;
      this.ef_name_03.TabStop = false;
      this.ef_name_03.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_03.UseSystemPasswordChar = false;
      this.ef_name_03.WaterMark = null;
      this.ef_name_03.TextChanged += new System.EventHandler(this.DataChanged);
      // 
      // ef_name_02
      // 
      this.ef_name_02.AllowSpace = true;
      this.ef_name_02.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_02.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_02.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_02.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_02.CornerRadius = 5;
      this.ef_name_02.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_name_02.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_02.Location = new System.Drawing.Point(146, 194);
      this.ef_name_02.MaxLength = 50;
      this.ef_name_02.Multiline = false;
      this.ef_name_02.Name = "ef_name_02";
      this.ef_name_02.PasswordChar = '\0';
      this.ef_name_02.ReadOnly = false;
      this.ef_name_02.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_02.SelectedText = "";
      this.ef_name_02.SelectionLength = 0;
      this.ef_name_02.SelectionStart = 0;
      this.ef_name_02.Size = new System.Drawing.Size(576, 40);
      this.ef_name_02.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_name_02.TabIndex = 8;
      this.ef_name_02.TabStop = false;
      this.ef_name_02.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_02.UseSystemPasswordChar = false;
      this.ef_name_02.WaterMark = null;
      this.ef_name_02.TextChanged += new System.EventHandler(this.DataChanged);
      // 
      // ef_name_01
      // 
      this.ef_name_01.AllowSpace = true;
      this.ef_name_01.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.ef_name_01.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.ef_name_01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ef_name_01.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(137)))), ((int)(((byte)(137)))));
      this.ef_name_01.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.ef_name_01.CornerRadius = 5;
      this.ef_name_01.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.ef_name_01.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
      this.ef_name_01.FormatMode = WSI.Cashier.CharacterTextBox.ENUM_FORMAT_MODE.BaseMode;
      this.ef_name_01.Location = new System.Drawing.Point(146, 148);
      this.ef_name_01.MaxLength = 50;
      this.ef_name_01.Multiline = false;
      this.ef_name_01.Name = "ef_name_01";
      this.ef_name_01.PasswordChar = '\0';
      this.ef_name_01.ReadOnly = false;
      this.ef_name_01.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.ef_name_01.SelectedText = "";
      this.ef_name_01.SelectionLength = 0;
      this.ef_name_01.SelectionStart = 0;
      this.ef_name_01.Size = new System.Drawing.Size(576, 40);
      this.ef_name_01.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.BASIC;
      this.ef_name_01.TabIndex = 6;
      this.ef_name_01.TabStop = false;
      this.ef_name_01.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
      this.ef_name_01.UseSystemPasswordChar = false;
      this.ef_name_01.WaterMark = null;
      this.ef_name_01.TextChanged += new System.EventHandler(this.DataChanged);
      // 
      // lbl_name_02
      // 
      this.lbl_name_02.AutoSize = true;
      this.lbl_name_02.BackColor = System.Drawing.Color.Transparent;
      this.lbl_name_02.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_name_02.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_name_02.Location = new System.Drawing.Point(57, 204);
      this.lbl_name_02.Name = "lbl_name_02";
      this.lbl_name_02.Size = new System.Drawing.Size(84, 18);
      this.lbl_name_02.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_name_02.TabIndex = 7;
      this.lbl_name_02.Text = "xFam N 02";
      this.lbl_name_02.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_birth_date
      // 
      this.uc_birth_date.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
      this.uc_birth_date.DateText = "xBirthDate";
      this.uc_birth_date.DateTextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_birth_date.DateTextWidth = 162;
      this.uc_birth_date.DateValue = new System.DateTime(((long)(0)));
      this.uc_birth_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_birth_date.FormatFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
      this.uc_birth_date.FormatWidth = 127;
      this.uc_birth_date.Location = new System.Drawing.Point(1, 291);
      this.uc_birth_date.Name = "uc_birth_date";
      this.uc_birth_date.Size = new System.Drawing.Size(476, 40);
      this.uc_birth_date.TabIndex = 11;
      this.uc_birth_date.TabStop = false;
      this.uc_birth_date.ValuesFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      // 
      // btn_cancel
      // 
      this.btn_cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_cancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_cancel.FlatAppearance.BorderSize = 0;
      this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_cancel.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_cancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_cancel.Image = null;
      this.btn_cancel.Location = new System.Drawing.Point(430, 383);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(155, 60);
      this.btn_cancel.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "XCANCEL";
      this.btn_cancel.UseVisualStyleBackColor = false;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.Location = new System.Drawing.Point(601, 383);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(155, 60);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 4;
      this.btn_ok.Text = "XOK";
      this.btn_ok.UseVisualStyleBackColor = false;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // lbl_msg_blink
      // 
      this.lbl_msg_blink.AutoSize = true;
      this.lbl_msg_blink.BackColor = System.Drawing.Color.Transparent;
      this.lbl_msg_blink.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_msg_blink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.lbl_msg_blink.Location = new System.Drawing.Point(99, 388);
      this.lbl_msg_blink.Name = "lbl_msg_blink";
      this.lbl_msg_blink.Size = new System.Drawing.Size(123, 18);
      this.lbl_msg_blink.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_RED;
      this.lbl_msg_blink.TabIndex = 2;
      this.lbl_msg_blink.Text = "xMESSAGE LINE";
      this.lbl_msg_blink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btn_keyboard
      // 
      this.btn_keyboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(50)))));
      this.btn_keyboard.FlatAppearance.BorderSize = 0;
      this.btn_keyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_keyboard.Font = new System.Drawing.Font("Montserrat", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_keyboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_keyboard.Image = global::WSI.Cashier.Properties.Resources.keyboard;
      this.btn_keyboard.Location = new System.Drawing.Point(13, 382);
      this.btn_keyboard.Name = "btn_keyboard";
      this.btn_keyboard.Size = new System.Drawing.Size(75, 55);
      this.btn_keyboard.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.TERTIARY;
      this.btn_keyboard.TabIndex = 1;
      this.btn_keyboard.TabStop = false;
      this.btn_keyboard.UseVisualStyleBackColor = false;
      this.btn_keyboard.Click += new System.EventHandler(this.btn_keyboard_Click);
      // 
      // numericTextBox1
      // 
      this.numericTextBox1.AllowSpace = false;
      this.numericTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox1.BackColor = System.Drawing.Color.White;
      this.numericTextBox1.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox1.CornerRadius = 0;
      this.numericTextBox1.FillWithCeros = false;
      this.numericTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox1.Location = new System.Drawing.Point(390, 130);
      this.numericTextBox1.MaxLength = 4;
      this.numericTextBox1.Multiline = false;
      this.numericTextBox1.Name = "numericTextBox1";
      this.numericTextBox1.PasswordChar = '\0';
      this.numericTextBox1.ReadOnly = false;
      this.numericTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox1.SelectedText = "";
      this.numericTextBox1.SelectionLength = 0;
      this.numericTextBox1.SelectionStart = 0;
      this.numericTextBox1.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox1.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox1.TabIndex = 0;
      this.numericTextBox1.TabStop = false;
      this.numericTextBox1.Text = "1984";
      this.numericTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox1.UseSystemPasswordChar = false;
      this.numericTextBox1.WaterMark = null;
      // 
      // numericTextBox2
      // 
      this.numericTextBox2.AllowSpace = false;
      this.numericTextBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox2.BackColor = System.Drawing.Color.White;
      this.numericTextBox2.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox2.CornerRadius = 0;
      this.numericTextBox2.FillWithCeros = false;
      this.numericTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox2.Location = new System.Drawing.Point(156, 130);
      this.numericTextBox2.MaxLength = 2;
      this.numericTextBox2.Multiline = false;
      this.numericTextBox2.Name = "numericTextBox2";
      this.numericTextBox2.PasswordChar = '\0';
      this.numericTextBox2.ReadOnly = false;
      this.numericTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox2.SelectedText = "";
      this.numericTextBox2.SelectionLength = 0;
      this.numericTextBox2.SelectionStart = 0;
      this.numericTextBox2.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox2.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox2.TabIndex = 0;
      this.numericTextBox2.TabStop = false;
      this.numericTextBox2.Text = "06";
      this.numericTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox2.UseSystemPasswordChar = false;
      this.numericTextBox2.WaterMark = null;
      // 
      // numericTextBox3
      // 
      this.numericTextBox3.AllowSpace = false;
      this.numericTextBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
      this.numericTextBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
      this.numericTextBox3.BackColor = System.Drawing.Color.White;
      this.numericTextBox3.BorderColor = System.Drawing.Color.Empty;
      this.numericTextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
      this.numericTextBox3.CornerRadius = 0;
      this.numericTextBox3.FillWithCeros = false;
      this.numericTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.numericTextBox3.Location = new System.Drawing.Point(273, 130);
      this.numericTextBox3.MaxLength = 2;
      this.numericTextBox3.Multiline = false;
      this.numericTextBox3.Name = "numericTextBox3";
      this.numericTextBox3.PasswordChar = '\0';
      this.numericTextBox3.ReadOnly = false;
      this.numericTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.numericTextBox3.SelectedText = "";
      this.numericTextBox3.SelectionLength = 0;
      this.numericTextBox3.SelectionStart = 0;
      this.numericTextBox3.Size = new System.Drawing.Size(86, 38);
      this.numericTextBox3.Style = WSI.Cashier.Controls.uc_round_textbox.RoundTextBoxStyle.NONE;
      this.numericTextBox3.TabIndex = 0;
      this.numericTextBox3.TabStop = false;
      this.numericTextBox3.Text = "11";
      this.numericTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericTextBox3.UseSystemPasswordChar = false;
      this.numericTextBox3.WaterMark = null;
      // 
      // frm_withholding
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(231)))), ((int)(((byte)(233)))));
      this.ClientSize = new System.Drawing.Size(770, 513);
      this.ControlBox = false;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_withholding";
      this.ShowIcon = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "frm_mb_account_user_edit";
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      this.gb_principal.ResumeLayout(false);
      this.gb_principal.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    //private System.Windows.Forms.SplitContainer splitContainer1;
    private Controls.uc_round_button btn_keyboard;
    internal System.Windows.Forms.Timer timer1;
    private Controls.uc_label label3;
    private Controls.uc_label label4;
    private Controls.uc_round_textbox textBox1;
    private Controls.uc_round_combobox comboBox1;
    private Controls.uc_label label5;
    private NumericTextBox numericTextBox1;
    private Controls.uc_round_combobox comboBox2;
    private NumericTextBox numericTextBox2;
    private Controls.uc_label label6;
    private Controls.uc_label label7;
    private Controls.uc_label label8;
    private NumericTextBox numericTextBox3;
    private Controls.uc_label label9;
    private Controls.uc_round_textbox textBox2;
    private Controls.uc_label lbl_msg_blink;
    private Controls.uc_round_button btn_ok;
    private Controls.uc_round_button btn_cancel;
    private Controls.uc_round_panel gb_principal;
    private CharacterTextBox ef_holder_id_01;
    private Controls.uc_label lbl_expected_rfc;
    private Controls.uc_round_combobox cb_gender;
    private CharacterTextBox ef_holder_id_02;
    private Controls.uc_label lbl_holder_id_01;
    private Controls.uc_label lbl_gender;
    private Controls.uc_label lbl_holder_id_02;
    private Controls.uc_label lbl_name_01;
    private Controls.uc_label lbl_name_03;
    private CharacterTextBox ef_name_03;
    private CharacterTextBox ef_name_02;
    private CharacterTextBox ef_name_01;
    private Controls.uc_label lbl_name_02;
    private uc_datetime uc_birth_date;
  }
}