namespace WSI.Cashier
{
  partial class uc_gift_delivery
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.lbl_separator_1 = new System.Windows.Forms.Label();
      this.web_browser = new System.Windows.Forms.WebBrowser();
      this.btn_ok = new WSI.Cashier.Controls.uc_round_button();
      this.gb_gift = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_gift_name = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_value = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_value_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_date_delivery = new WSI.Cashier.Controls.uc_label();
      this.lbl_date_delivery_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_date_request = new WSI.Cashier.Controls.uc_label();
      this.lbl_date_request_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_instance_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_gift_instance_number_prefix = new WSI.Cashier.Controls.uc_label();
      this.gb_account = new WSI.Cashier.Controls.uc_round_panel();
      this.lbl_account_client = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_client_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_card_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_card_number_prefix = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_number = new WSI.Cashier.Controls.uc_label();
      this.lbl_account_number_prefix = new WSI.Cashier.Controls.uc_label();
      this.uc_gift_reader = new WSI.Cashier.uc_gift_reader();
      this.gb_gift.SuspendLayout();
      this.gb_account.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Interval = 500;
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // lbl_separator_1
      // 
      this.lbl_separator_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.lbl_separator_1.BackColor = System.Drawing.Color.Black;
      this.lbl_separator_1.Location = new System.Drawing.Point(-3, 60);
      this.lbl_separator_1.Name = "lbl_separator_1";
      this.lbl_separator_1.Size = new System.Drawing.Size(885, 1);
      this.lbl_separator_1.TabIndex = 87;
      // 
      // web_browser
      // 
      this.web_browser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.web_browser.Location = new System.Drawing.Point(547, 75);
      this.web_browser.MinimumSize = new System.Drawing.Size(20, 20);
      this.web_browser.Name = "web_browser";
      this.web_browser.Size = new System.Drawing.Size(320, 353);
      this.web_browser.TabIndex = 3;
      // 
      // btn_ok
      // 
      this.btn_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_ok.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
      this.btn_ok.FlatAppearance.BorderSize = 0;
      this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_ok.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_ok.Image = null;
      this.btn_ok.IsSelected = false;
      this.btn_ok.Location = new System.Drawing.Point(726, 443);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_ok.Size = new System.Drawing.Size(141, 57);
      this.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_ok.TabIndex = 4;
      this.btn_ok.Text = "ENTREGAR";
      this.btn_ok.UseVisualStyleBackColor = false;
      // 
      // gb_gift
      // 
      this.gb_gift.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_gift.BackColor = System.Drawing.Color.Transparent;
      this.gb_gift.BorderColor = System.Drawing.Color.Empty;
      this.gb_gift.Controls.Add(this.lbl_gift_name);
      this.gb_gift.Controls.Add(this.lbl_gift_value);
      this.gb_gift.Controls.Add(this.lbl_gift_value_prefix);
      this.gb_gift.Controls.Add(this.lbl_date_delivery);
      this.gb_gift.Controls.Add(this.lbl_date_delivery_prefix);
      this.gb_gift.Controls.Add(this.lbl_date_request);
      this.gb_gift.Controls.Add(this.lbl_date_request_prefix);
      this.gb_gift.Controls.Add(this.lbl_gift_instance_number);
      this.gb_gift.Controls.Add(this.lbl_gift_instance_number_prefix);
      this.gb_gift.CornerRadius = 10;
      this.gb_gift.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_gift.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_gift.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gift.HeaderHeight = 35;
      this.gb_gift.HeaderSubText = null;
      this.gb_gift.HeaderText = "XGIFT";
      this.gb_gift.Location = new System.Drawing.Point(30, 223);
      this.gb_gift.Name = "gb_gift";
      this.gb_gift.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_gift.Size = new System.Drawing.Size(472, 208);
      this.gb_gift.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_gift.TabIndex = 2;
      // 
      // lbl_gift_name
      // 
      this.lbl_gift_name.AutoSize = true;
      this.lbl_gift_name.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_name.Font = new System.Drawing.Font("Open Sans Semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_gift_name.Location = new System.Drawing.Point(159, 44);
      this.lbl_gift_name.Name = "lbl_gift_name";
      this.lbl_gift_name.Size = new System.Drawing.Size(97, 22);
      this.lbl_gift_name.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLUE;
      this.lbl_gift_name.TabIndex = 8;
      this.lbl_gift_name.Text = "xGift Name";
      this.lbl_gift_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lbl_gift_value
      // 
      this.lbl_gift_value.AutoSize = true;
      this.lbl_gift_value.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_value.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_value.Location = new System.Drawing.Point(159, 178);
      this.lbl_gift_value.Name = "lbl_gift_value";
      this.lbl_gift_value.Size = new System.Drawing.Size(84, 19);
      this.lbl_gift_value.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_gift_value.TabIndex = 7;
      this.lbl_gift_value.Text = "xGift Value";
      this.lbl_gift_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_gift_value_prefix
      // 
      this.lbl_gift_value_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_value_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_value_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_gift_value_prefix.Location = new System.Drawing.Point(18, 178);
      this.lbl_gift_value_prefix.Name = "lbl_gift_value_prefix";
      this.lbl_gift_value_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_gift_value_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_gift_value_prefix.TabIndex = 6;
      this.lbl_gift_value_prefix.Text = "xPuntos:";
      this.lbl_gift_value_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_date_delivery
      // 
      this.lbl_date_delivery.AutoSize = true;
      this.lbl_date_delivery.BackColor = System.Drawing.Color.Transparent;
      this.lbl_date_delivery.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date_delivery.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_date_delivery.Location = new System.Drawing.Point(159, 147);
      this.lbl_date_delivery.Name = "lbl_date_delivery";
      this.lbl_date_delivery.Size = new System.Drawing.Size(91, 19);
      this.lbl_date_delivery.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_date_delivery.TabIndex = 5;
      this.lbl_date_delivery.Text = "x99/99/9999";
      this.lbl_date_delivery.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_date_delivery_prefix
      // 
      this.lbl_date_delivery_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_date_delivery_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date_delivery_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_date_delivery_prefix.Location = new System.Drawing.Point(18, 147);
      this.lbl_date_delivery_prefix.Name = "lbl_date_delivery_prefix";
      this.lbl_date_delivery_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_date_delivery_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_date_delivery_prefix.TabIndex = 4;
      this.lbl_date_delivery_prefix.Text = "xEntregado:";
      this.lbl_date_delivery_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_date_request
      // 
      this.lbl_date_request.AutoSize = true;
      this.lbl_date_request.BackColor = System.Drawing.Color.Transparent;
      this.lbl_date_request.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date_request.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_date_request.Location = new System.Drawing.Point(159, 116);
      this.lbl_date_request.Name = "lbl_date_request";
      this.lbl_date_request.Size = new System.Drawing.Size(91, 19);
      this.lbl_date_request.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_date_request.TabIndex = 3;
      this.lbl_date_request.Text = "x99/99/9999";
      this.lbl_date_request.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_date_request_prefix
      // 
      this.lbl_date_request_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_date_request_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_date_request_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_date_request_prefix.Location = new System.Drawing.Point(18, 116);
      this.lbl_date_request_prefix.Name = "lbl_date_request_prefix";
      this.lbl_date_request_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_date_request_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_date_request_prefix.TabIndex = 2;
      this.lbl_date_request_prefix.Text = "xEmitido:";
      this.lbl_date_request_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_gift_instance_number
      // 
      this.lbl_gift_instance_number.AutoSize = true;
      this.lbl_gift_instance_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_instance_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_instance_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_gift_instance_number.Location = new System.Drawing.Point(159, 83);
      this.lbl_gift_instance_number.Name = "lbl_gift_instance_number";
      this.lbl_gift_instance_number.Size = new System.Drawing.Size(169, 19);
      this.lbl_gift_instance_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_gift_instance_number.TabIndex = 1;
      this.lbl_gift_instance_number.Text = "01234567890123456789";
      this.lbl_gift_instance_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_gift_instance_number_prefix
      // 
      this.lbl_gift_instance_number_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_gift_instance_number_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_gift_instance_number_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_gift_instance_number_prefix.Location = new System.Drawing.Point(18, 83);
      this.lbl_gift_instance_number_prefix.Name = "lbl_gift_instance_number_prefix";
      this.lbl_gift_instance_number_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_gift_instance_number_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_gift_instance_number_prefix.TabIndex = 0;
      this.lbl_gift_instance_number_prefix.Text = "xGift Number:";
      this.lbl_gift_instance_number_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // gb_account
      // 
      this.gb_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_account.BackColor = System.Drawing.Color.Transparent;
      this.gb_account.BorderColor = System.Drawing.Color.Empty;
      this.gb_account.Controls.Add(this.lbl_account_client);
      this.gb_account.Controls.Add(this.lbl_account_client_prefix);
      this.gb_account.Controls.Add(this.lbl_card_number);
      this.gb_account.Controls.Add(this.lbl_card_number_prefix);
      this.gb_account.Controls.Add(this.lbl_account_number);
      this.gb_account.Controls.Add(this.lbl_account_number_prefix);
      this.gb_account.CornerRadius = 10;
      this.gb_account.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.gb_account.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(88)))), ((int)(((byte)(95)))));
      this.gb_account.HeaderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.HeaderHeight = 35;
      this.gb_account.HeaderSubText = null;
      this.gb_account.HeaderText = "XACCOUNT";
      this.gb_account.Location = new System.Drawing.Point(30, 75);
      this.gb_account.Name = "gb_account";
      this.gb_account.PanelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.gb_account.Size = new System.Drawing.Size(472, 132);
      this.gb_account.Style = WSI.Cashier.Controls.uc_round_panel.RoundPanelStyle.NORMAL;
      this.gb_account.TabIndex = 1;
      // 
      // lbl_account_client
      // 
      this.lbl_account_client.AutoSize = true;
      this.lbl_account_client.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_client.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_client.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_client.Location = new System.Drawing.Point(159, 104);
      this.lbl_account_client.Name = "lbl_account_client";
      this.lbl_account_client.Size = new System.Drawing.Size(57, 19);
      this.lbl_account_client.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_account_client.TabIndex = 116;
      this.lbl_account_client.Text = "xName";
      this.lbl_account_client.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_client_prefix
      // 
      this.lbl_account_client_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_client_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_client_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_client_prefix.Location = new System.Drawing.Point(18, 104);
      this.lbl_account_client_prefix.Name = "lbl_account_client_prefix";
      this.lbl_account_client_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_account_client_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_client_prefix.TabIndex = 115;
      this.lbl_account_client_prefix.Text = "xClient:";
      this.lbl_account_client_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_card_number
      // 
      this.lbl_card_number.AutoSize = true;
      this.lbl_card_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_card_number.Location = new System.Drawing.Point(159, 74);
      this.lbl_card_number.Name = "lbl_card_number";
      this.lbl_card_number.Size = new System.Drawing.Size(177, 19);
      this.lbl_card_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_card_number.TabIndex = 114;
      this.lbl_card_number.Text = "x13245678901234567890";
      this.lbl_card_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_card_number_prefix
      // 
      this.lbl_card_number_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_card_number_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_card_number_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_card_number_prefix.Location = new System.Drawing.Point(18, 74);
      this.lbl_card_number_prefix.Name = "lbl_card_number_prefix";
      this.lbl_card_number_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_card_number_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_card_number_prefix.TabIndex = 113;
      this.lbl_card_number_prefix.Text = "xCard:";
      this.lbl_card_number_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_account_number
      // 
      this.lbl_account_number.AutoSize = true;
      this.lbl_account_number.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_number.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_number.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_account_number.Location = new System.Drawing.Point(159, 44);
      this.lbl_account_number.Name = "lbl_account_number";
      this.lbl_account_number.Size = new System.Drawing.Size(81, 19);
      this.lbl_account_number.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK;
      this.lbl_account_number.TabIndex = 112;
      this.lbl_account_number.Text = "x12345678";
      this.lbl_account_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_account_number_prefix
      // 
      this.lbl_account_number_prefix.BackColor = System.Drawing.Color.Transparent;
      this.lbl_account_number_prefix.Font = new System.Drawing.Font("Open Sans Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_account_number_prefix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(141)))), ((int)(((byte)(193)))));
      this.lbl_account_number_prefix.Location = new System.Drawing.Point(18, 44);
      this.lbl_account_number_prefix.Name = "lbl_account_number_prefix";
      this.lbl_account_number_prefix.Size = new System.Drawing.Size(117, 18);
      this.lbl_account_number_prefix.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_SECONDARY_14PX_BLUE;
      this.lbl_account_number_prefix.TabIndex = 111;
      this.lbl_account_number_prefix.Text = "xAccount:";
      this.lbl_account_number_prefix.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // uc_gift_reader
      // 
      this.uc_gift_reader.Location = new System.Drawing.Point(0, 0);
      this.uc_gift_reader.MaximumSize = new System.Drawing.Size(878, 59);
      this.uc_gift_reader.MinimumSize = new System.Drawing.Size(878, 59);
      this.uc_gift_reader.Name = "uc_gift_reader";
      this.uc_gift_reader.Size = new System.Drawing.Size(878, 59);
      this.uc_gift_reader.TabIndex = 0;
      // 
      // uc_gift_delivery
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.btn_ok);
      this.Controls.Add(this.gb_gift);
      this.Controls.Add(this.gb_account);
      this.Controls.Add(this.lbl_separator_1);
      this.Controls.Add(this.uc_gift_reader);
      this.Controls.Add(this.web_browser);
      this.Name = "uc_gift_delivery";
      this.Size = new System.Drawing.Size(884, 535);
      this.gb_gift.ResumeLayout(false);
      this.gb_gift.PerformLayout();
      this.gb_account.ResumeLayout(false);
      this.gb_account.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Label lbl_separator_1;
    public System.Windows.Forms.WebBrowser web_browser;
    private uc_gift_reader uc_gift_reader;
    private Controls.uc_round_panel gb_account;
    private Controls.uc_label lbl_account_number_prefix;
    private Controls.uc_label lbl_account_number;
    private Controls.uc_label lbl_card_number;
    private Controls.uc_label lbl_card_number_prefix;
    private Controls.uc_label lbl_account_client;
    private Controls.uc_label lbl_account_client_prefix;
    private Controls.uc_round_panel gb_gift;
    private Controls.uc_label lbl_date_delivery;
    private Controls.uc_label lbl_date_delivery_prefix;
    private Controls.uc_label lbl_date_request;
    private Controls.uc_label lbl_date_request_prefix;
    private Controls.uc_label lbl_gift_instance_number;
    private Controls.uc_label lbl_gift_instance_number_prefix;
    private Controls.uc_label lbl_gift_value;
    private Controls.uc_label lbl_gift_value_prefix;
    private Controls.uc_label lbl_gift_name;
    private Controls.uc_round_button btn_ok;


  }
}
