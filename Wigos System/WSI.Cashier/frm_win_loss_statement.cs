//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_win_loss_statements.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: FJC
// 
// CREATION DATE: 23-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2015 FJC     First release.
// 12-NOV-2015 RGR     Change style.
// 14-MAR-2016 FOS     Fixed Bug 9357: New design corrections
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Cashier.Controls;

namespace WSI.Cashier
{
  public partial class frm_win_loss_statement : frm_base
  {

    #region 'Members'

    private Int64 _account_id;
    private String _holder_name;
    #endregion

    #region 'Constructor'

    /// <summary>
    /// Constructor
    /// </summary>
    public frm_win_loss_statement(CardData _card_data)
    {

      this._account_id = _card_data.AccountId;
      this._holder_name = _card_data.PlayerTracking.HolderName;

      InitializeComponent();

      InitControls();

      InitializeControlResources();

      FillComboboxRequestYear();

      //Acount Id
      this.lbl_account.Text = this._account_id.ToString();
      this.lbl_holder_name.Text = this._holder_name;
      cb_request_year.SelectedIndex = 0;
    }

    #endregion

    #region 'Private Methods'

    /// <summary>
    /// Call for fill ComboBox request year and next step call LoadDataWinLossStatement
    /// </summary>
    private void FillComboboxRequestYear()
    {
      Int32 _NumberPrevYear;
      Int32 _year;

      _NumberPrevYear = GeneralParam.GetInt32("WinLossStatement", "NumberOfPreviousYear");

      cb_request_year.Items.Clear();

      for (int i = 0; i < _NumberPrevYear; i++)
      {
        _year = WGDB.Now.Year - i - 1;
        cb_request_year.Items.Add(_year.ToString());
      }

    

    }

    /// <summary>
    /// Call for load WinLossStatement data to Form
    /// </summary>
    private void LoadDataWinLossStatement()
    {
      WinLossStatement.WIN_LOSS_STATEMENT _winloss_statement_param_ret;
      Boolean _enabled_button;

      _enabled_button = true;
      _winloss_statement_param_ret = new WinLossStatement.WIN_LOSS_STATEMENT();
      _winloss_statement_param_ret.AccountId = this._account_id;
      _winloss_statement_param_ret.HolderName = this._holder_name;
      _winloss_statement_param_ret.RequestYear = Convert.ToInt32(cb_request_year.SelectedItem.ToString());

      using (DB_TRX _db_trx = new DB_TRX())
      {

        if (WinLossStatement.GetWinLossStatement(ref _winloss_statement_param_ret,
                                                 _db_trx.SqlTransaction))
        {

          if (_winloss_statement_param_ret.Status != WinLossStatement.WinLossStatementStatus.NotExists)
          {
            // Request DateTime
            this.lbl_request_datetime.Text = WinLossStatement.DateTimeCustomFormatWithOutSeconds(_winloss_statement_param_ret.RequestDate);

            // Period (From - To)
            this.lbl_request_period_year_from.Text = WinLossStatement.DateTimeCustomFormatWithOutSeconds(_winloss_statement_param_ret.DateFrom);
            this.lbl_request_period_year_to.Text = WinLossStatement.DateTimeCustomFormatWithOutSeconds(_winloss_statement_param_ret.DateTo.AddSeconds(-1));
          }

          // Request Status
          switch (_winloss_statement_param_ret.Status)
          {
            case WinLossStatement.WinLossStatementStatus.Pending:
              this.lbl_request_status.Text = Resource.String("STR_WIN_LOSS_STATEMENT_STATUS_1");
              _enabled_button = false;

              break;
            case WinLossStatement.WinLossStatementStatus.InProgress:
              this.lbl_request_status.Text = Resource.String("STR_WIN_LOSS_STATEMENT_STATUS_2");
              _enabled_button = false;

              break;
            case WinLossStatement.WinLossStatementStatus.Ready:
              if (_winloss_statement_param_ret.StatusPrint == WinLossStatement.WinLossStatementStatusPrint.Printed)
              {
                this.lbl_request_status.Text = Resource.String("STR_WIN_LOSS_STATEMENT_STATUS_4");
              }
              else
              {
                _enabled_button = false;
                this.lbl_request_status.Text = Resource.String("STR_WIN_LOSS_STATEMENT_STATUS_5");
              }
              break;
            case WinLossStatement.WinLossStatementStatus.NotExists:
              this.lbl_request_status.Text = Resource.String("STR_WIN_LOSS_STATEMENT_STATUS_6");

              lbl_request_datetime.Text = "--";
              lbl_request_period_year_from.Text = "--";
              lbl_request_period_year_to.Text = "--";
              break;
          }
        }
      }

      //Button enbaled (I don't like...)
      this.btn_request.Enabled = _enabled_button;

    } // LoadDataWinLossStatement

    /// <summary>
    /// Initialize NLS strings
    /// </summary>
    private void InitializeControlResources()
    {
      //pb_blocked.Image = WSI.Cashier.Images.Get(Images.CashierImage.Locked);

      // - NLS Strings:

      //   - Labels
      this.FormTitle = Resource.String("STR_WIN_LOSS_STATEMENT_FORM_TITTLE");               
      this.urp_container.HeaderText = Resource.String("STR_WIN_LOSS_STATEMENT_FORM_TITTLE");
      this.lbl_account_prefix.Text = Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ":";
      this.lbl_holder_name_prefix.Text = Resource.String("STR_BANK_TRANSACTION_BANK_HOLDER_NAME") + ":";
      this.lbl_request_year.Text = Resource.String("STR_WIN_LOSS_STATEMENT_REQUEST_YEAR");
      this.lbl_request_datetime_prefix.Text = Resource.String("STR_WIN_LOSS_STATEMENT_REQUEST_DATE");
      this.lbl_from.Text = Resource.String("STR_WIN_LOSS_STATEMENT_DATE_FROM");
      this.lblTo.Text = Resource.String("STR_WIN_LOSS_STATEMENT_DATE_TO");
      this.lbl_request_status_prefix.Text = Resource.String("STR_WIN_LOSS_STATEMENT_REQUEST_STATUS");
      this.lbl_request_period_year_prefix.Text = Resource.String("STR_WIN_LOSS_STATEMENT_REQUEST_PERIOD");

      //   - Buttons
      this.btn_close.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CLOSE");
      this.btn_request.Text = Resource.String("STR_WIN_LOSS_STATEMENT_REQUEST");

    } //InitializeControlResources

    /// <summary>
    /// Initialize Events && text
    /// </summary>
    private void InitControls()
    {
      //Text Initialize
      this.lbl_request_datetime.Text = String.Empty;
      this.lbl_request_period_year_from.Text = String.Empty;
      this.lbl_request_period_year_to.Text = String.Empty;
      this.lbl_request_status.Text = String.Empty;

      //Events
      this.btn_request.Click += new EventHandler(this.btn_request_click);
      this.cb_request_year.SelectedIndexChanged += new EventHandler(this.cb_request_year_SelectedIndexChanged);
    } //InitControls

    #endregion

    #region 'Events'

    /// <summary>
    /// 
    /// </summary>
    private void btn_exit_Click(object sender, EventArgs e)
    {

      Misc.WriteLog("[FORM CLOSE] frm_win_loss_statement (exit)", Log.Type.Message);
      this.Close();
    } //btn_exit_Click

    /// <summary>
    /// 
    /// </summary>
    private void btn_request_click(object sender, EventArgs e)
    {
      WinLossStatement.WIN_LOSS_STATEMENT _statement;
      Boolean _process_ok;
      Int64 OperationId;

      _statement = new WinLossStatement.WIN_LOSS_STATEMENT();
      _process_ok = false;
      _statement.AccountId = this._account_id;
      _statement.RequestYear = Convert.ToInt32(cb_request_year.SelectedItem.ToString());
      _statement.Status = WinLossStatement.WinLossStatementStatus.Pending;
      _statement.StatusPrev = WinLossStatement.WinLossStatementStatus.Ready;
      _statement.StatusPrint = WinLossStatement.WinLossStatementStatusPrint.NotPrinted;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (WSI.Common.WinLossStatement.InsertWinLossStatement(_statement,
                                                               _db_trx.SqlTransaction))
        {
          if (WSI.Common.WinLossStatement.InsertAccountOperationMovements(_statement, Cashier.CashierSessionInfo(), out OperationId, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();
            _process_ok = true;
          }
        }
      }



     

      if (_process_ok)
      {
        //Refresh Data
        LoadDataWinLossStatement();
      }
      else
      {
        //TODO: Show Message Error
      }
    } // btn_request_click

    /// <summary>
    /// 
    /// </summary>
    public new void Show()
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_win_loss_statement", Log.Type.Message);

      //Refresh Data
      this.cb_request_year.IsDroppedDown = false;

      this.ShowDialog();
    }

    private void  cb_request_year_SelectedIndexChanged(object sender, EventArgs e)
    {
      LoadDataWinLossStatement();
    }
    #endregion
  }
}