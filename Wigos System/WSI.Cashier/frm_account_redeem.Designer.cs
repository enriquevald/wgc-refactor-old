namespace WSI.Cashier
{
  partial class frm_account_redeem
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_card_balance = new WSI.Cashier.uc_card_balance();
      this.btn_close = new WSI.Cashier.Controls.uc_round_button();
      this.btn_credit_redeem_partial = new WSI.Cashier.Controls.uc_round_button();
      this.btn_credit_redeem_total = new WSI.Cashier.Controls.uc_round_button();
      this.btn_prize_payout = new WSI.Cashier.Controls.uc_round_button();
      this.pnl_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.btn_prize_payout);
      this.pnl_data.Controls.Add(this.uc_card_balance);
      this.pnl_data.Controls.Add(this.btn_close);
      this.pnl_data.Controls.Add(this.btn_credit_redeem_partial);
      this.pnl_data.Controls.Add(this.btn_credit_redeem_total);
      this.pnl_data.Size = new System.Drawing.Size(498, 410);
      // 
      // uc_card_balance
      // 
      this.uc_card_balance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.uc_card_balance.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.uc_card_balance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_card_balance.Location = new System.Drawing.Point(11, 3);
      this.uc_card_balance.Name = "uc_card_balance";
      this.uc_card_balance.Size = new System.Drawing.Size(310, 395);
      this.uc_card_balance.TabIndex = 0;
      // 
      // btn_close
      // 
      this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(72)))), ((int)(((byte)(83)))));
      this.btn_close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_close.FlatAppearance.BorderSize = 0;
      this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_close.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_close.Image = null;
      this.btn_close.IsSelected = false;
      this.btn_close.Location = new System.Drawing.Point(333, 338);
      this.btn_close.Name = "btn_close";
      this.btn_close.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_close.Size = new System.Drawing.Size(155, 60);
      this.btn_close.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.SECONDARY;
      this.btn_close.TabIndex = 3;
      this.btn_close.Text = "XCLOSE";
      this.btn_close.UseVisualStyleBackColor = false;
      // 
      // btn_credit_redeem_partial
      // 
      this.btn_credit_redeem_partial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_credit_redeem_partial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_credit_redeem_partial.FlatAppearance.BorderSize = 0;
      this.btn_credit_redeem_partial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_credit_redeem_partial.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_credit_redeem_partial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_credit_redeem_partial.Image = null;
      this.btn_credit_redeem_partial.IsSelected = false;
      this.btn_credit_redeem_partial.Location = new System.Drawing.Point(358, 10);
      this.btn_credit_redeem_partial.Name = "btn_credit_redeem_partial";
      this.btn_credit_redeem_partial.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_credit_redeem_partial.Size = new System.Drawing.Size(130, 50);
      this.btn_credit_redeem_partial.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_credit_redeem_partial.TabIndex = 1;
      this.btn_credit_redeem_partial.Text = "XPARTIAL REDEEM";
      this.btn_credit_redeem_partial.UseVisualStyleBackColor = false;
      // 
      // btn_credit_redeem_total
      // 
      this.btn_credit_redeem_total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_credit_redeem_total.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_credit_redeem_total.FlatAppearance.BorderSize = 0;
      this.btn_credit_redeem_total.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_credit_redeem_total.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_credit_redeem_total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_credit_redeem_total.Image = null;
      this.btn_credit_redeem_total.IsSelected = false;
      this.btn_credit_redeem_total.Location = new System.Drawing.Point(358, 68);
      this.btn_credit_redeem_total.Name = "btn_credit_redeem_total";
      this.btn_credit_redeem_total.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_credit_redeem_total.Size = new System.Drawing.Size(130, 50);
      this.btn_credit_redeem_total.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_credit_redeem_total.TabIndex = 2;
      this.btn_credit_redeem_total.Text = "XTOTAL REDEEM";
      this.btn_credit_redeem_total.UseVisualStyleBackColor = false;
      // 
      // btn_prize_payout
      // 
      this.btn_prize_payout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_prize_payout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(131)))), ((int)(((byte)(161)))));
      this.btn_prize_payout.FlatAppearance.BorderSize = 0;
      this.btn_prize_payout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_prize_payout.Font = new System.Drawing.Font("Open Sans Semibold", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
      this.btn_prize_payout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
      this.btn_prize_payout.Image = null;
      this.btn_prize_payout.IsSelected = false;
      this.btn_prize_payout.Location = new System.Drawing.Point(358, 126);
      this.btn_prize_payout.Name = "btn_prize_payout";
      this.btn_prize_payout.SelectedColor = System.Drawing.Color.LightSteelBlue;
      this.btn_prize_payout.Size = new System.Drawing.Size(130, 50);
      this.btn_prize_payout.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
      this.btn_prize_payout.TabIndex = 4;
      this.btn_prize_payout.Text = "XPRIZE PAYOUT";
      this.btn_prize_payout.UseVisualStyleBackColor = false;
      this.btn_prize_payout.Click += new System.EventHandler(this.btn_prize_payout_Click);
      // 
      // frm_account_redeem
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_close;
      this.ClientSize = new System.Drawing.Size(498, 465);
      this.Name = "frm_account_redeem";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Style = WSI.Cashier.Controls.frm_base.BaseFormStyle.PRINCIPAL;
      this.Text = "Account Points";
      this.Shown += new System.EventHandler(this.OnShown);
      this.pnl_data.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private WSI.Cashier.Controls.uc_round_button btn_close;
    private WSI.Cashier.Controls.uc_round_button btn_credit_redeem_partial;
    private WSI.Cashier.Controls.uc_round_button btn_credit_redeem_total;
    private uc_card_balance uc_card_balance;
    private Controls.uc_round_button btn_prize_payout;
  }
}