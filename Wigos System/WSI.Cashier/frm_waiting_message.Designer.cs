﻿namespace WSI.Cashier
{
  partial class frm_waiting_message
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tm_close = new System.Windows.Forms.Timer(this.components);
      this.pb_CircularProgressBar = new System.Windows.Forms.PictureBox();
      this.lbl_message = new WSI.Cashier.Controls.uc_label();
      this.pnl_data.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_CircularProgressBar)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_data
      // 
      this.pnl_data.Controls.Add(this.lbl_message);
      this.pnl_data.Controls.Add(this.pb_CircularProgressBar);
      this.pnl_data.Size = new System.Drawing.Size(503, 117);
      // 
      // tm_close
      // 
      this.tm_close.Interval = 1000;
      this.tm_close.Tick += new System.EventHandler(this.tm_close_Tick);
      // 
      // pb_CircularProgressBar
      // 
      this.pb_CircularProgressBar.Image = global::WSI.Cashier.Properties.Resources.CircularProgressBar;
      this.pb_CircularProgressBar.Location = new System.Drawing.Point(27, 22);
      this.pb_CircularProgressBar.Name = "pb_CircularProgressBar";
      this.pb_CircularProgressBar.Size = new System.Drawing.Size(68, 68);
      this.pb_CircularProgressBar.TabIndex = 14;
      this.pb_CircularProgressBar.TabStop = false;
      // 
      // lbl_message
      // 
      this.lbl_message.AutoSize = true;
      this.lbl_message.Font = new System.Drawing.Font("Montserrat", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
      this.lbl_message.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
      this.lbl_message.Location = new System.Drawing.Point(123, 45);
      this.lbl_message.Name = "lbl_message";
      this.lbl_message.Size = new System.Drawing.Size(78, 18);
      this.lbl_message.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TEXT_INFO_14PX_BLACK_BOLD;
      this.lbl_message.TabIndex = 15;
      this.lbl_message.Text = "xMessage";
      // 
      // frm_waiting_message
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.ClientSize = new System.Drawing.Size(503, 172);
      this.Name = "frm_waiting_message";
      this.ShowCloseButton = false;
      this.Text = "xForm";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
      this.pnl_data.ResumeLayout(false);
      this.pnl_data.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_CircularProgressBar)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer tm_close;
    private System.Windows.Forms.PictureBox pb_CircularProgressBar;
    private WSI.Cashier.Controls.uc_label lbl_message;
  }
}