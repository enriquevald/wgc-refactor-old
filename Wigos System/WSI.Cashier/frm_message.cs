//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_message.cs
// 
//   DESCRIPTION: Implements the generic message dialog. (frm_message)
//
//        AUTHOR: Armando Alva
// 
// CREATION DATE: 22-AUG-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-AUG-2007 AAC     First release.
// 28-MAR-2012 MPO     User session: New type of message box -> ShowTimerWithCancel. 
// 13-APR-2012 JCM     Show dialog for select account type (anonymous or personalized). Is a customized YesNoCancel.
//                     Added a third Custom Button.
// 03-APR-2012 JCM     Fixed Bug #265: If disconnect data cable, "DISCONNECT" message appear down some forms
// 18-JUL-2014 SMN     If TitoMode is true and ELP is enabled, create new accounts is not allowed
// 04-MAY-2015 JBC     Fixed Bug WIG-2277: Countdown label had incorret size.
// 10-FEB-2016 DDS     Fixed Bug 9179:Incorrect Message (Yes / No / Cancel)
// 18-JUL-2016 DHA     Fixed Bug 15665: remove images on the devolution message
// 18-JUL-2016 FJC     PBI 15447:Visitas / Recepci�n: MEJORAS - Cajero - Mejorar mensaje de error "...documentos expirados o lista de prohibidos..."
// 07-SEP-2016 DHA     Product Backlog Item 15912: resize for differents windows size
// 17-FEB-2017 DPC     Bug 24736:Cashier: Con an�nimos no permitidos se ve el bot�n al crear una cuenta nueva
// 21-FEB-2017 SA      Bug 24869:Cajero: No es posible cancelar la creaci�n de un registro de entrada nuevo desde Recepci�n
// 23-MAY-2017 DHA     PBI 27487: WIGOS-83 MES21 - Sales selection
// 18-SEP-2017 DPC     WIGOS-5268: Cashier & GUI - Icons - Implement
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.Utilities.Extensions;

namespace WSI.Cashier
{
  public partial class frm_message : Controls.frm_base_icon
  {
    public enum MessageStyle
    {
      NONE = 0,
      ONE_BUTTON = 1,
      TWO_BUTTON = 2,
      THREE_BUTTON = 3,
      FOUR_BUTTON = 4,
      FIVE_BUTTON = 5,
    }

    #region Variables

    private Color m_transparent_back_color;
    private Int32 m_form_width;
    private Controls.uc_label.LabelStyle m_label_style;
    private Color m_form_back_color;
    private Color m_label_back_color;
    private Int32 m_label_height;
    private MessageStyle m_form_style;
    private Size m_image_size;
    private ContentAlignment m_message_text_align;

    #endregion

    #region Propierties

    public Color TransparentBackColor
    {
      get
      {
        return m_transparent_back_color;
      }
      set
      {
        this.m_transparent_back_color = value;
      }
    }

    public Int32 FormWidth
    {
      get
      {
        return m_form_width;
      }
      set
      {
        this.m_form_width = value;
      }
    }

    public Controls.uc_label.LabelStyle LabelStyle
    {
      get
      {
        return m_label_style;
      }
      set
      {
        this.m_label_style = value;
      }
    }

    public Color LabelBackColor
    {
      get
      {
        return m_label_back_color;
      }
      set
      {
        this.m_label_back_color = value;
      }
    }

    public Color FormBackColor
    {
      get
      {
        return m_form_back_color;
      }
      set
      {
        this.m_form_back_color = value;
      }
    }

    public Int32 LabelHeight
    {
      get
      {
        return m_label_height;
      }
      set
      {
        this.m_label_height = value;
      }
    }

    public MessageStyle FormStyle
    {
      get
      {
        return m_form_style;
      }
      set
      {
        this.m_form_style = value;
      }
    }

    public Size ImageSize
    {
      get
      {
        return m_image_size;
      }
      set
      {
        this.m_image_size = value;
      }
    }

    public ContentAlignment MessageTextAlign
    {
      get
      {
        return m_message_text_align;
      }
      set
      {
        this.m_message_text_align = value;
      }
    }


    #endregion

    #region Attributes

    private frm_yesno form_yes_no;
    private DialogResult dialog_result;
    static private frm_message wgc_message_with_timer;
    static private Boolean result_none = false;
    Boolean with_timer = false;
    Form parent_timer;
    int countdown_interval;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor method.
    /// 1. Initialize controls.
    /// 2. Initialize control�s resources. (NLS, Images, etc)
    /// </summary>
    public frm_message()
    {
      m_message_text_align = ContentAlignment.TopCenter;

      InitializeComponent();
      InitializeControlResources();

      form_yes_no = new frm_yesno();
      form_yes_no.Opacity = 0.8;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize control resources (NLS strings, images, etc)
    /// </summary>
    private void InitializeControlResources()
    {
      // - NLS Strings:
      //   - Title: External set
      lbl_message_title.Text = "";

      //   - Labels

      //   - Buttons
      btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
      btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      // - Images:

    } // InitializeControlResources

    /// <summary>
    /// Button Ok actions:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ok_Click(object sender, EventArgs e)
    {
      Hide();
      form_yes_no.Hide();

      if (with_timer)
      {
        this.tm_close.Enabled = false;

        if (wgc_message_with_timer != null)
          wgc_message_with_timer.dialog_result = DialogResult.OK;

        if (parent_timer != null)
        {
          parent_timer.Focus();
        }

        Dispose();
      }
      else
      {
        dialog_result = DialogResult.OK;
      }
    }

    /// <summary>
    /// Button Cancel actions:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      if (with_timer)
      {
        this.tm_close.Enabled = false;
      }

      this.dialog_result = DialogResult.Cancel;

      Hide();

      form_yes_no.Hide();
    }
    /// <summary>
    /// Button Custom actions:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_custom_Click(object sender, EventArgs e)
    {

      this.dialog_result = DialogResult.No;

      Hide();

      form_yes_no.Hide();
    }

    /// <summary>
    /// Initialize the generic dialog depending on the settings.
    /// </summary>
    /// <param name="ButtonType"></param>
    /// <param name="ImageId"></param>
    /// <param name="Caption"></param>
    /// <param name="Text"></param>
    private void Init(MessageBoxButtons ButtonType, Images.CashierImage ImageId, String Caption, String Text,
                       String TextCountdown, int CountdownInterval, string CountdownTimeUnit)
    {

      lbl_message_title.Text = Caption.ToUpper();
      lbl_text.Text = Text;
      if (with_timer)
      {
        lbl_text_countdown.Text = TextCountdown;
        lbl_time_countdown.Text = CountdownInterval.ToString();
        lbl_text_countdown_time_unit.Text = CountdownTimeUnit;
        countdown_interval = CountdownInterval;
      }
      else
      {
        lbl_text_countdown.Visible = false;
        lbl_time_countdown.Visible = false;
        lbl_text_countdown_time_unit.Visible = false;
      }

      icon_box.Image = Images.Get32x32(ImageId);

      FormStyle = MessageStyle.TWO_BUTTON;

      switch (ButtonType)
      {
        case MessageBoxButtons.OK:
          FormStyle = MessageStyle.ONE_BUTTON;
          btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
          btn_cancel.Visible = false;

          break;

        case MessageBoxButtons.YesNo:
          FormStyle = MessageStyle.THREE_BUTTON;
          if (String.IsNullOrEmpty(btn_ok.Text))
          {
            btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_YES");
          }

          if (String.IsNullOrEmpty(btn_cancel.Text))
          {
            btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
          }


          break;

        case MessageBoxButtons.YesNoCancel:
          FormStyle = MessageStyle.THREE_BUTTON;
          TopMost = true;
          btn_custom.Visible = true;
          btn_cancel.Visible = true;
          btn_ok.Visible = true;

          if (String.IsNullOrEmpty(btn_ok.Text))
          {
            btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_YES");
          }

          if (String.IsNullOrEmpty(btn_cancel.Text))
          {
            btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
          }

          if (String.IsNullOrEmpty(btn_custom.Text))
          {
            btn_custom.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_NO");
          }

          //This MessageBoxButtons used and customized by ShowAccountSelect
          //But i need a dialog with yes no cancel...
          // Yes / No  / Cancel text are only applied if text boxes are empty
          break;

        case MessageBoxButtons.AbortRetryIgnore:
          FormStyle = MessageStyle.THREE_BUTTON;
          TopMost = true;
          btn_cancel.Visible = false;
          btn_ok.Visible = false;
          btn_custom.Visible = false;
          break;

        default:
          FormStyle = MessageStyle.TWO_BUTTON;
          // Button type not implemented
          btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");
          btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

          break;
      }

    } // Init

    protected override void InitLayout()
    {

      base.InitLayout();

      AdjustControls();

    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      AdjustControls();
    }

    private void AdjustControls()
    {

      Int32 _label_height;
      Int32 _panel_height;
      Int32 _panel_width;

      CashierStyle.ControlStyle.InitStyle(this, FormStyle);

      form_yes_no.BackColor = TransparentBackColor;

      this.Width = FormWidth;
      this.BackColor = FormBackColor;

      this.panel0.Width = this.Width;
      this.panel0.BackColor = LabelBackColor;
      this.panel1.Width = this.Width;
      this.panel2.Width = this.Width;

      this.lbl_message_title.Style = WSI.Cashier.Controls.uc_label.LabelStyle.POPUP_HEADER;
      if (this.icon_box.Visible)
      {
        this.lbl_message_title.TextAlign = ContentAlignment.TopCenter;
        _panel_height = 50;
      }
      else
      {
        this.lbl_message_title.TextAlign = ContentAlignment.MiddleCenter;
        _panel_height = 10;
      }
      this.lbl_message_title.Height = LabelHeight;

      this.lbl_text.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_time_countdown.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text_countdown_time_unit.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;
      this.lbl_text_countdown.Style = WSI.Cashier.Controls.uc_label.LabelStyle.TITLE_PRINCIPAL_16PX_BLACK;

      // Revisar WindowManager
      this.Location = new Point(WindowManager.GetFormCenterLocation(form_yes_no).X - (this.Width / 2), WindowManager.GetFormCenterLocation(form_yes_no).Y - this.Height / 2);

      this.icon_box.Size = ImageSize;

      this.panel0.Height = LabelHeight;

      this.icon_box.Location = new Point(((this.Width / 2) - (this.icon_box.Width / 2)), (((this.lbl_message_title.Height - 1) / 2) + (this.icon_box.Height / 2)));

      _panel_width = this.panel1.Width - 3 * (this.panel1.Margin.Left + this.panel1.Margin.Right);

      this.lbl_text.Location = new Point(5, _panel_height);
      this.lbl_text.MaximumSize = new Size(_panel_width, 2000);
      //this.lbl_text.Width = this.Width - 10;
      this.lbl_text.TextAlign = ContentAlignment.MiddleCenter;
      _label_height = this.lbl_text.Height + 10;
      this.lbl_text.AutoSize = false;
      this.lbl_text.Size = new Size(_panel_width, _label_height);
      this.lbl_text.TextAlign = MessageTextAlign;
      if (this.lbl_text.Visible)
      {
        _panel_height = _panel_height + this.lbl_text.Height;
      }

      this.lbl_time_countdown.Location = new Point(this.Width / 2 - 20, _panel_height);
      this.lbl_time_countdown.Width = (this.Width / 2) - 10;
      this.lbl_time_countdown.TextAlign = ContentAlignment.MiddleCenter;

      this.lbl_text_countdown_time_unit.Location = new Point(this.Width / 2 + 20, _panel_height);
      this.lbl_text_countdown_time_unit.Width = (this.Width / 2) - 10;
      this.lbl_text_countdown_time_unit.TextAlign = ContentAlignment.MiddleCenter;
      if (this.lbl_time_countdown.Visible || this.lbl_text_countdown_time_unit.Visible)
      {
        _panel_height = _panel_height + Math.Max(this.lbl_time_countdown.Height, this.lbl_text_countdown_time_unit.Height) + 10;
      }

      this.lbl_text_countdown.Location = new Point(5, this.lbl_text_countdown_time_unit.Location.Y + this.lbl_text_countdown_time_unit.Height + 10);
      this.lbl_text_countdown.Width = this.Width - 10;
      this.lbl_text_countdown.TextAlign = ContentAlignment.MiddleCenter;
      if (this.lbl_text_countdown.Visible)
      {
        _panel_height = _panel_height + this.lbl_text_countdown.Height + 10;
      }

      this.panel1.Height = _panel_height;

      this.panel2.Location = new Point(0, this.lbl_text_countdown.Location.Y + this.lbl_text_countdown.Height + 10);
      this.panel2.Width = this.Width;

      this.btn_custom.Height = 50;
      this.btn_ok.Height = 50;
      this.btn_cancel.Height = 50;

      switch (FormStyle)
      {
        case MessageStyle.ONE_BUTTON:

          this.panel2.Height = 70;
          this.btn_custom.Width = 420;
          this.btn_ok.Width = 420;
          this.btn_cancel.Width = 420;
          btn_custom.Location = new Point((this.panel2.Width - this.btn_custom.Width) / 2, 10);
          btn_cancel.Location = new Point((this.panel2.Width - this.btn_cancel.Width) / 2, 10);
          btn_ok.Location = new Point((this.panel2.Width - this.btn_ok.Width) / 2, 10);
          break;

        case MessageStyle.TWO_BUTTON:

          this.panel2.Height = 70;
          this.btn_custom.Width = 205;
          this.btn_ok.Width = 205;
          this.btn_cancel.Width = 205;
          btn_custom.Location = new Point((this.panel2.Width / 2 - this.btn_custom.Width) / 2, 10);
          btn_cancel.Location = new Point((this.panel2.Width / 2 - this.btn_cancel.Width) / 3 * 2, 10);
          btn_ok.Location = new Point(this.panel2.Width / 2 + (this.panel2.Width / 2 - this.btn_ok.Width) / 3, 10);
          break;

        case MessageStyle.THREE_BUTTON:

          this.panel2.Height = 190;
          this.btn_custom.Width = 420;
          this.btn_ok.Width = 420;
          this.btn_cancel.Width = 420;
          btn_custom.Location = new Point((this.panel2.Width - this.btn_custom.Width) / 2, 10);
          btn_cancel.Location = new Point((this.panel2.Width - this.btn_ok.Width) / 2, 130);
          btn_ok.Location = new Point((this.panel2.Width - this.btn_cancel.Width) / 2, 70);
          break;

        default:

          FormStyle = MessageStyle.TWO_BUTTON;
          // Button type not implemented
          btn_cancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
          btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_OK");

          break;
      }
      this.panel0.Location = new Point(0, 0);
      this.panel1.Location = new Point(0, this.panel0.Height);
      this.panel2.Location = new Point(0, this.panel0.Height + this.panel1.Height);
      this.Height = this.panel0.Height + this.panel1.Height + this.panel2.Height;

    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="ButtonType"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    static public DialogResult Show(string Text, string Caption, MessageBoxButtons ButtonType, Form Parent)
    {
      return Show(Text, Caption, ButtonType, MessageBoxIcon.Information, Parent);
    }

    /// <summary>
    /// Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="ButtonType"></param>
    /// <param name="IconType"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    static public DialogResult Show(string Text, string Caption, MessageBoxButtons ButtonType, MessageBoxIcon IconType, Form Parent)
    {
      return Show(Text, Caption, ButtonType, IconType, Parent, true);
    }

    /// <summary>
    /// Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="ButtonType"></param>
    /// <param name="IconType"></param>
    /// <param name="Parent"></param>
    /// <param name="ShowYesNo"></param>
    /// <returns></returns>
    static public DialogResult Show(string Text, string Caption, MessageBoxButtons ButtonType, MessageBoxIcon IconType, Form Parent, Boolean ShowYesNo)
    {
      Images.CashierImage image_id;

      switch (IconType)
      {
        case MessageBoxIcon.Error:
          image_id = Images.CashierImage.Error;
          break;
        case MessageBoxIcon.Information:
          image_id = Images.CashierImage.Information;
          break;
        case MessageBoxIcon.Question:
          image_id = Images.CashierImage.Question;
          break;
        case MessageBoxIcon.Warning:
          image_id = Images.CashierImage.Warning;
          break;
        default:
          image_id = Images.CashierImage.Warning;
          break;
      }

      return Show(Text, Caption, ButtonType, image_id, Parent, ShowYesNo);
    }

    /// <summary>
    /// Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="ButtonType"></param>
    /// <param name="ImageId"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    static public DialogResult Show(string Text, string Caption, MessageBoxButtons ButtonType, Images.CashierImage ImageId, Form Parent, string TextForCustomButton = "")
    {
      return Show(Text, Caption, ButtonType, ImageId, Parent, true, TextForCustomButton);
    }

    static private DialogResult Show(frm_message FormMessage, string Text, string Caption, MessageBoxButtons ButtonType, Images.CashierImage ImageId,
                                Form Parent, Boolean ShowYesNo)
    {
      // ATB 16-FEB-2017
      Misc.WriteLog("[FORM LOAD] frm_message", Log.Type.Message);

      frm_message wgc_message = FormMessage;
      DialogResult dialog_result;

      wgc_message.Init(ButtonType, ImageId, Caption, Text, "", -1, "");

      if (ShowYesNo)
      {
        wgc_message.form_yes_no.Show(Parent);
      }


      wgc_message.ShowDialog(wgc_message.form_yes_no);
      if (ShowYesNo)
      {
        wgc_message.form_yes_no.Hide();
      }

      if (Parent != null)
      {
        Parent.Focus();
      }
      dialog_result = wgc_message.dialog_result;

      wgc_message.Dispose();
      wgc_message = null;

      return dialog_result;
    } // Show

    static public DialogResult Show(string Text, string Caption, MessageBoxButtons ButtonType, Images.CashierImage ImageId,
                                    Form Parent, Boolean ShowYesNo, string TextForCustomButton = "", string TextForOkButton = "")
    {

      DialogResult _res;
      using (frm_message _frm = new frm_message())
      {
        if (!TextForCustomButton.IsNullOrWhiteSpace())
        {
          _frm.btn_custom.Text = TextForCustomButton;
        }
        _res = Show(_frm, Text, Caption, ButtonType, ImageId,
                                     Parent, ShowYesNo);
      }
      return _res;
    } // Show

    /// <summary>
    ///  Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="TodayButtonMessage"></param>
    /// <param name="TomorrowButtonMessage"></param>
    /// <param name="Parent"></param>
    /// <returns></returns>
    static public DialogResult ShowGamingDaySelector(string Text, string Caption, string TodayButtonMessage, string TomorrowButtonMessage, Form Parent)
    {
      DialogResult _res;
      Point _p_btn_ok;
      Point _p_btn_cancel;
      Point _p_btn_custom;
      Point _p_lbl_text;
      Size _panel1_size;

      using (frm_message _frm = new frm_message())
      {
        _frm.Init(MessageBoxButtons.OKCancel, Images.CashierImage.Warning, Caption, Text, "", -1, "");

        /*
         BUTTONS EDITION
         */
        //new locations
        _p_btn_ok = new Point(201, 0);
        _p_btn_cancel = new Point(283, 55);
        _p_btn_custom = new Point(83, 0);
        _p_lbl_text = new Point(63, 25);
        _panel1_size = new Size(419, 110);

        //undocking from pane
        _frm.btn_cancel.Dock = DockStyle.None;
        _frm.btn_ok.Dock = DockStyle.None;
        _frm.btn_custom.Dock = DockStyle.None;

        //unanchor textbox
        _frm.lbl_text.Anchor = AnchorStyles.None;

        //redim size panel
        _frm.panel2.Size = _panel1_size;

        //relocation buttons & label
        _frm.btn_cancel.Location = _p_btn_cancel;
        _frm.btn_ok.Location = _p_btn_ok;
        _frm.btn_custom.Location = _p_btn_custom;
        _frm.lbl_text.Location = _p_lbl_text;

        _frm.btn_custom.Visible = true;
        //---------------

        _frm.btn_ok.Text = TomorrowButtonMessage;
        _frm.btn_custom.Text = TodayButtonMessage;
        _frm.btn_custom.Dock = DockStyle.None;

        _frm.form_yes_no.Show(Parent);
        _frm.ShowDialog(_frm.form_yes_no);
        _frm.form_yes_no.Hide();

        if (Parent != null)
        {
          Parent.Focus();
        }

        _res = _frm.dialog_result;

        _frm.Dispose();
      }

      return _res;
    } // Show

    /// <summary>
    /// Show dialog for select Holder Account type.
    /// </summary>
    /// <param name="TrackData"></param>
    /// <param name="Parent"></param>
    /// <returns name="ACCOUNT_USER_TYPE"></returns>
    static public ACCOUNT_USER_TYPE ShowAccountSelect(string TrackData, Form Parent)
    {
      String _text;
      String _caption;
      String _rsc_cancel;
      String _rsc_custom;
      String _rsc_anonymous;
      frm_message frm = new frm_message();
      DialogResult _result;

      _caption = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITLE");

      // MPO&LEM 18/12/2013: In TITOMode only show confirmation message because not exist anonymous accounts
      if (WSI.Common.TITO.Utils.IsTitoMode() && !WSI.Common.Misc.IsMico2Mode())
      {
        // SMN 18-JUL-2014: If
        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
        {
          _text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITO_AND_ELP", TrackData + "\n\n");
          _text = _text.Replace("\\r\\n", "\r\n");

          Show(frm, _text, _caption, MessageBoxButtons.OK,
                          Images.CashierImage.Warning, Parent, true);

          _result = DialogResult.None;
        }
        else
        {
          _text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST_TITO", TrackData + "\n\n");
          _text = _text.Replace("\\r\\n", "\r\n");

          _result = Show(frm, _text, _caption, MessageBoxButtons.OKCancel,
                          Images.CashierImage.Warning, Parent, true);
        }
      }
      else
      {
        _text = Resource.String("STR_UC_CARD_USER_MSG_CARD_NOT_EXIST", TrackData + "\n\n");
        _text = _text.Replace("\\r\\n", "\r\n");
        _rsc_cancel = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        _rsc_custom = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_CARD_MODE_PERSONAL");

        //images  and txt of buttons
        //Personal
        frm.btn_ok.Text = _rsc_custom;

        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
        {
          frm.btn_ok.Enabled = false;
        }

        //Cancel
        frm.btn_cancel.Text = _rsc_cancel;

        //images  and txt of buttons
        //Anonymous
        _rsc_anonymous = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS");

        frm.btn_custom.Text = _rsc_anonymous;

        _result = Show(frm, _text, _caption,
          GeneralParam.GetBoolean("Cashier", "AllowedAnonymous", true)
            ? MessageBoxButtons.YesNoCancel
            : MessageBoxButtons.YesNo, Images.CashierImage.Warning, Parent, true);

      }
      switch (_result)
      {
        case DialogResult.OK:
          return ACCOUNT_USER_TYPE.PERSONAL;

        case DialogResult.No:
          return ACCOUNT_USER_TYPE.ANONYMOUS;

        case DialogResult.Cancel:
        default:
          return ACCOUNT_USER_TYPE.NONE;
      }

    } //ShowAccountSelect

    /// <summary>
    /// Show dialog for select devolution mode.
    /// </summary>
    /// <param name="TrackData"></param>
    /// <param name="Parent"></param>
    /// <returns name="ACCOUNT_USER_TYPE"></returns>
    static public DialogResult ShowRefundSelect(Form Parent, Currency RefundAmount, Boolean CashEnabled)
    {
      String _text;
      String _caption;
      String _rsc_cash;
      String _rsc_error;
      String _rsc_ticket;

      frm_message frm = new frm_message();
      Int16 _letf_position;

      _caption = Resource.String("STR_REFUND");
      _text = Resource.String("STR_REFUND_ANSWER", Currency.Format(RefundAmount, CurrencyExchange.GetNationalCurrency()));

      _rsc_cash = Resource.String("STR_REFUND_CASH");
      _rsc_ticket = Resource.String("STR_REFUND_TICKET");
      _rsc_error = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");

      //images  and txt of buttons
      _letf_position = 20;

      // Ticket
      frm.btn_ok.Text = _rsc_ticket;
      frm.btn_ok.Dock = DockStyle.None;
      frm.btn_ok.Left -= _letf_position;

      // Cash
      frm.btn_custom.Text = _rsc_cash;
      frm.btn_custom.Visible = true;
      frm.btn_custom.Enabled = CashEnabled;
      frm.btn_custom.Dock = DockStyle.None;
      frm.btn_custom.Left -= _letf_position;

      // Cancel
      frm.btn_cancel.Text = _rsc_error;
      frm.btn_cancel.Visible = true;

      return Show(frm, _text, _caption, MessageBoxButtons.YesNoCancel,
                     Images.CashierImage.Warning, Parent, true);
    }

    /// <summary>
    /// Show dialog for Reception module.
    /// </summary>
    /// <param name="TrackData"></param>
    /// <param name="Parent"></param>
    /// <returns name="ACCOUNT_USER_TYPE"></returns>
    static public DialogResult ShowReceptionAllowEntry(Form Parent, string MessageText, MessageBoxButtons MessageButtons)
    {
      String _text;
      String _caption;
      DialogResult _res;

      _caption = Resource.String("STR_APP_GEN_MSG_WARNINGS");
      _text = MessageText;

      using (frm_message _frm = new frm_message())
      {
        _frm.MessageTextAlign = ContentAlignment.TopLeft;
        _frm.Init(MessageButtons, Images.CashierImage.Warning, _caption, _text, "", -1, "");
        if (MessageButtons == MessageBoxButtons.OKCancel)
        {
          _frm.btn_ok.Style = WSI.Cashier.Controls.uc_round_button.RoundButonStyle.PRINCIPAL;
          _frm.btn_ok.Text = Resource.String("STR_FRM_VOUCHERS_REPRINT_022");
        }
        else
        {
          _frm.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        }

        _frm.form_yes_no.Show(Parent);
        _frm.ShowDialog(_frm.form_yes_no);
        _frm.form_yes_no.Hide();

        if (Parent != null)
        {
          Parent.Focus();
        }
        _res = _frm.dialog_result;
        _frm.Dispose();
      }

      return _res;
    }


    static public CHIPS_SALE_MODE ShowChipsSaleModeSelect(Form Parent)
    {
      String _text;
      String _caption;
      String _rsc_cancel;
      String _rsc_custom;
      String _rsc_anonymous;
      frm_message frm = new frm_message();
      DialogResult _result;

      _caption = Resource.String("STR_CHIPS_SALE_SELECCTION_MODE_TITLE");

      // MICO2
      if (WSI.Common.TITO.Utils.IsTitoMode() && WSI.Common.Misc.IsMico2Mode())
      {
        return CHIPS_SALE_MODE.FROM_ACCOUNT;
      }
      else if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        return CHIPS_SALE_MODE.INTEGRATED_RECHARGE;
      }
      else
      {
        _text = Resource.String("STR_CHIPS_SALE_SELECCTION_MODE_MESSAGE"); ;
        
        _rsc_cancel = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        _rsc_custom = Resource.String("STR_CHIPS_SALE_SELECCTION_MODE_RECHARGE");

        //images  and txt of buttons
        //Personal
        frm.btn_ok.Text = _rsc_custom;

        //Cancel
        frm.btn_cancel.Text = _rsc_cancel;

        //Anonymous
        _rsc_anonymous = Resource.String("STR_CHIPS_SALE_SELECCTION_MODE_ACCOUNT_BALANCE");

        frm.btn_custom.Text = _rsc_anonymous;

        _result = Show(frm, _text, _caption, MessageBoxButtons.YesNoCancel, Images.CashierImage.Warning, Parent, true);

      }
      switch (_result)
      {
        case DialogResult.OK:
          return CHIPS_SALE_MODE.INTEGRATED_RECHARGE;

        case DialogResult.No:
          return CHIPS_SALE_MODE.FROM_ACCOUNT;

        case DialogResult.Cancel:
        default:
          return CHIPS_SALE_MODE.NONE;
      }

    } //ShowAccountSelect


    /// <summary>
    /// Show dialog function.
    /// </summary>
    /// <param name="Text"></param>
    /// <param name="Caption"></param>
    /// <param name="ButtonType"></param>
    /// <param name="ImageId"></param>
    /// <param name="Parent"></param>
    /// <param name="ShowYesNo"></param>
    /// <param name="WithTimer"></param>
    /// <returns></returns>
    static public DialogResult Show(string Text, string Caption, string TextCountdown, int CountdownInterval, string CountdownTimeUnit,
                                    MessageBoxButtons ButtonType, MessageBoxIcon IconType, Form Parent, Boolean ShowYesNo, Boolean WithTimer)
    {
      Images.CashierImage image_id;

      wgc_message_with_timer = new frm_message();
      wgc_message_with_timer.tm_close.Enabled = true;
      wgc_message_with_timer.with_timer = WithTimer;
      wgc_message_with_timer.countdown_interval = CountdownInterval;
      wgc_message_with_timer.parent_timer = Parent;

      switch (IconType)
      {
        case MessageBoxIcon.Error:
          image_id = Images.CashierImage.Error;
          break;
        case MessageBoxIcon.Information:
          image_id = Images.CashierImage.Information;
          break;
        case MessageBoxIcon.Question:
          image_id = Images.CashierImage.Question;
          break;
        case MessageBoxIcon.Warning:
          image_id = Images.CashierImage.Warning;
          break;
        default:
          image_id = Images.CashierImage.Warning;
          break;
      }

      wgc_message_with_timer.Init(ButtonType, image_id, Caption, Text,
                                  TextCountdown, CountdownInterval, CountdownTimeUnit);

      if (ShowYesNo)
      {
        wgc_message_with_timer.form_yes_no.Show(Parent);
      }

      //wgc_message_with_timer.Show(wgc_message_with_timer.form_yes_no);
      //Application.DoEvents();

      wgc_message_with_timer.ShowDialog(wgc_message_with_timer.form_yes_no);

      //return DialogResult.OK;
      return wgc_message_with_timer.dialog_result;
    } // Show

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - Text:
    //           - Caption:
    //           - TextCountdown:
    //           - CountdownInterval:
    //           - CountdownTimeUnit:
    //           - IconType:
    //           - Parent:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    static public DialogResult ShowTimerWithCancel(string Text, string Caption, string TextCountdown, int CountdownInterval, string CountdownTimeUnit, MessageBoxIcon IconType, Form Parent)
    {
      Images.CashierImage image_id;

      wgc_message_with_timer = new frm_message();
      wgc_message_with_timer.tm_close.Enabled = true;
      wgc_message_with_timer.with_timer = true;
      wgc_message_with_timer.countdown_interval = CountdownInterval;
      wgc_message_with_timer.parent_timer = Parent;

      switch (IconType)
      {
        case MessageBoxIcon.Error:
          image_id = Images.CashierImage.Error;
          break;
        case MessageBoxIcon.Information:
          image_id = Images.CashierImage.Information;
          break;
        case MessageBoxIcon.Question:
          image_id = Images.CashierImage.Question;
          break;
        case MessageBoxIcon.Warning:
          image_id = Images.CashierImage.Warning;
          break;
        default:
          image_id = Images.CashierImage.Warning;
          break;
      }

      wgc_message_with_timer.form_yes_no.Show(Parent);
      wgc_message_with_timer.dialog_result = DialogResult.None;
      wgc_message_with_timer.Init(MessageBoxButtons.OK, image_id, Caption, Text,
                                  TextCountdown, CountdownInterval, CountdownTimeUnit);

      wgc_message_with_timer.btn_ok.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
      result_none = false;

      wgc_message_with_timer.ShowDialog(wgc_message_with_timer.form_yes_no);

      if (result_none)
      {
        wgc_message_with_timer.dialog_result = DialogResult.None;
      }

      if (wgc_message_with_timer.dialog_result == DialogResult.OK)
      {
        return DialogResult.Cancel;
      }
      else
      {
        return DialogResult.None;
      }

    } // Show


    //------------------------------------------------------------------------------
    // PURPOSE : Dispose yes_no form.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    new public void Dispose()
    {
      form_yes_no.Dispose();
      form_yes_no = null;

      base.Dispose();
    }

    #endregion

    private void tm_close_Tick(object sender, EventArgs e)
    {
      if (with_timer)
      {

        //this.tm_close.Enabled = false;
        //Hide();
        //form_yes_no.Hide();

        //if (parent_timer != null)
        //{
        //  parent_timer.Focus();
        //}
        //Dispose();

        if (countdown_interval == 0)
        {
          this.tm_close.Enabled = false;
          result_none = true;
          wgc_message_with_timer.dialog_result = DialogResult.OK;
          this.btn_ok_Click(btn_ok, null);
        }
        else
        {
          countdown_interval--;
          this.lbl_time_countdown.Text = countdown_interval.ToString();
          //On top timer message over a disconnected messaage or other forms
          this.TopMost = true;
        }
      }
    }


  }
}