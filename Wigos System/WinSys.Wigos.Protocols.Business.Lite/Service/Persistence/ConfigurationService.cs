﻿using System.Collections.Generic;
using WinSys.Wigos.Protocols.Business.Lite.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Lite.Repositories;
using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Lite.Service.Persistence
{
    // TODO: Refactor. Basic replica from WinSys.Wigos.Gsa.S2S.Business but in .NET 2.0 to integrate with Wigos

    /// <summary>
    /// Logic for ConfigurationService
    /// </summary>
    public class ConfigurationService
    {
        private readonly SequenceRepository sequenceRepository;

        public ConfigurationService(SequenceRepository sequenceRepository)
        {
            this.sequenceRepository = sequenceRepository;
        }

        #region IConfigurationService

        public int GetConfigurationIdCurrentSequence()
        {
            return (int)this.sequenceRepository.GetCurrentValue(SequenceId.S2SGsaConfigurationId);
        }

        public void CheckAndSetConfigurationIdIncrement(IEnumerable<GeneralParamItem> generalParamValuesToSave)
        {
            if (S2S.Gsa.Configuration.Lite.GeneralParams.Gsa.IsConfigurationIdAutoIncreaseable)
            {
                bool hasChanges = false;

                foreach (GeneralParamItem item in generalParamValuesToSave)
                {
                    if (item.Value != GeneralParam.GetString(item.GroupKey, item.SubjectKey))
                    {
                        hasChanges = true;
                        break;
                    }
                }

                if (hasChanges)
                {
                    this.GetNextConfigurationIdSequence();
                }
            }
        }

        #endregion
		
        private int GetNextConfigurationIdSequence()
        {
            return (int)this.sequenceRepository.GetNextValue(SequenceId.S2SGsaConfigurationId);
        }
    }
}