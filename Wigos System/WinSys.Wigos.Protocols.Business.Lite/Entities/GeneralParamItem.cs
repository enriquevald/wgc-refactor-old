﻿namespace WinSys.Wigos.Protocols.Business.Lite.Entities
{
	// TODO: Refactor. Basic replica from WinSys.Wigos.Gsa.S2S.Business but in .NET 2.0 to integrate with Wigos

	public class GeneralParamItem
	{
		public string GroupKey { get; set; }
		public string SubjectKey { get; set; }
		public string Value { get; set; }
	}
}