﻿using Microsoft.Practices.Unity;
using WinSys.Wigos.Architecture.Enums;
using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.Business.Validations;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Adapters;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Bootstrapper
{
	public static class Unity
	{
		public static void Configure(IUnityContainer container)
		{
			Unity.ConfigureBusiness(container);
			Unity.ConfigurePersistence(container);

			IConfigurationService configurationService = container.Resolve<IConfigurationService>();

			container
				.RegisterType<ISyntaxValidator, SyntaxValidator>()
				.RegisterType<IS2SMessageContentValidator, S2SMessageContentValidator>()
				.RegisterType<IIssueVoucherValidator, IssueVoucherValidator>()
				.RegisterType<ICommitVoucherValidator, CommitVoucherValidator>()
				.RegisterType<IXmlSerializerService, XmlSerializerService>()
				.RegisterType<IS2SMessageBuilder, S2SMessageBuilder>();

			container
				.RegisterType<ICommandFactory<CommandInfo>, CommandFactory>()
				.RegisterType<IErrorMessageFactory, ErrorMessageFactory>()
				.RegisterType<IServiceClientFactory, ServiceClientFactory>();

			container
				.RegisterType<ILogService, LogService>(new InjectionConstructor(new InjectionParameter(typeof(LogLevel), Configuration.GeneralParams.Gsa.LogLevel)))
				.RegisterType<ILogCommandService, LogCommandService>();

			container
				.RegisterType<IAdapter<BusinessExceptionTicketItem, string>, BusinessTicketClassToErrorCodeAdapter>()
				.RegisterType<IClientTypeToTitoTerminaltypeAdapter, ClientTypeToTitoTerminalTypeAdapter>();

			container
				.RegisterType<IProtocolManager, ProtocolManager>()
				.RegisterType<IProtocolState, ProtocolState>(new ContainerControlledLifetimeManager());

			container
				.RegisterType<IS2SGsaService, S2SGsaService>(new ContainerControlledLifetimeManager());
		}

		// TODO: Implement. New bootstrapper from integration layer to expand business bootstrapper
		private static void ConfigureBusiness(IUnityContainer container)
		{
			container
				.RegisterType<IRedeemTicketValidator, RedeemTicketValidator>()
				.RegisterType<ICommitTicketValidator, CommitTicketValidator>()
				.RegisterType<ITicketIdValidator, TicketIdValidator>()
				.RegisterType<ITicketNotExpiredValidator, TicketNotExpiredValidator>()
				.RegisterType<ITicketNotDiscardedValidator, TicketNotDiscardedValidator>()
				.RegisterType<ITicketNotRedeemedValidator, TicketNotRedeemedValidator>()
				.RegisterType<ITicketCanBeRedeemInPendingPrintValidator, TicketCanBeRedeemInPendingPrintValidator>()
				.RegisterType<ITicketHasValidStatusToRedeem, TicketHasValidStatusToRedeem>();

			container
				.RegisterType<ITicketService, TicketService>()
				.RegisterType<IConfigurationService, ConfigurationService>();
		}

		// TODO: Implement. New bootstrapper from business layer to expand persistence bootstrapper
		private static void ConfigurePersistence(IUnityContainer container)
		{
			container
				.RegisterType<ISequenceRepository, SequenceRepository>(new ContainerControlledLifetimeManager());
		}
	}
}