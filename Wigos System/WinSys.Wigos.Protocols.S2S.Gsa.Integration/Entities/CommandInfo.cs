﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities
{
	public class CommandInfo
	{
		public s2sHeader Header { get; set; }
		public object Command { get; set; }
	}
}