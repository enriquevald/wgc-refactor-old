﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities
{
	public class State
	{
		public string PropertyId { get; set; }
		public string FromSystem { get; set; }
		public string ToSystem { get; set; }
		public string RemoteEndpoint { get; set; }
		public long SessionId { get; set; }
		public long InboundCommandId { get; set; }
	}
}