﻿using System;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders
{
	/// <summary>
	/// Implements a S2sAck builder
	/// </summary>
	public class S2SAckBuilder : IS2SAckBuilder
	{
		private string fromSystem;
		private string toSystem;
		private long messageId;
		private DateTime dateTimeSent;
		private string errorCode;
		private string errorText;

		public void SetFromSystem(string fromSystem)
		{
			this.fromSystem = fromSystem;
		}

		public void SetToSystem(string toSystem)
		{
			this.toSystem = toSystem;
		}

		public void SetMessageId(long messageId)
		{
			this.messageId = messageId;
		}

		public void SetDateTimeSent(DateTime dateTimeSent)
		{
			this.dateTimeSent = dateTimeSent;
		}

		public void SetErrorCode(string errorCode)
		{
			this.errorCode = errorCode;
		}

		public void SetErrorText(string errorText)
		{
			this.errorText = errorText;
		}

		/// <summary>
		/// Performs de construction of the object 
		/// </summary>
		/// <returns>Builded object generated</returns>
		public s2sAck Build()
		{
			return new s2sAck
			{
				fromSystem = this.fromSystem,
				toSystem = this.toSystem,
				messageId = this.messageId,
				dateTimeSent = this.dateTimeSent,
				errorCode = this.errorCode,
				errorText = this.errorText
			};
		}

		/// <summary>
		/// Reset current build status to perform a build from an initial status
		/// </summary>
		public void Reset()
		{
			this.fromSystem = null;
			this.toSystem = null;
			this.messageId = 0;
			this.dateTimeSent = DateTime.MinValue;
			this.errorCode = null;
			this.errorText = null;
		}
	}
}