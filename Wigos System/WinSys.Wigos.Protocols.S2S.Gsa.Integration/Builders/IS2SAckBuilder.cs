﻿using System;

using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders
{
	/// <summary>
	/// Implements a S2sAck builder
	/// </summary>
	public interface IS2SAckBuilder : IBuilder<s2sAck>
	{
		void SetFromSystem(string fromSystem);
		void SetToSystem(string toSystem);
		void SetMessageId(long messageId);
		void SetDateTimeSent(DateTime dateTimeSent);
		void SetErrorCode(string errorCode);
		void SetErrorText(string errorText);
	}
}