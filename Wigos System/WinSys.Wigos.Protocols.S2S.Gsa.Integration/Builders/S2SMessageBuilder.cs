﻿using System.Collections.Generic;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders
{
	/// <summary>
	/// Implements a S2sMessage builder
	/// </summary>
	public class S2SMessageBuilder : IS2SMessageBuilder
	{
		private List<object> items;

		public S2SMessageBuilder()
		{
			this.items = new List<object>();
		}

		public void Add(s2sAck item)
		{
			this.items.Add(item);
		}

		public void Add(s2sBody item)
		{
			this.items.Add(item);
		}

		public void Add(s2sHeader item)
		{
			this.items.Add(item);
		}

		/// <summary>
		/// Performs de construction of the object 
		/// </summary>
		/// <returns>Builded object generated</returns>
		public s2sMessage Build()
		{
			return new s2sMessage
			{
				Items = this.items.ToArray()
			};
		}

		/// <summary>
		/// Reset current build status to perform a build from an initial status
		/// </summary>
		public void Reset()
		{
			this.items.Clear();
		}
	}
}