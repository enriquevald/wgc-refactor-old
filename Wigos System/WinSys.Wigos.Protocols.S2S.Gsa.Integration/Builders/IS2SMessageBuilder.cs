﻿using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders
{
	/// <summary>
	/// Implements a S2sMessage builder
	/// </summary>
	public interface IS2SMessageBuilder : IBuilder<s2sMessage>
	{
		/// <summary>
		/// Adds a s2sAck item
		/// </summary>
		/// <param name="item">s2sAck item to add</param>
		void Add(s2sAck item);

		/// <summary>
		/// Adds a s2sBody item
		/// </summary>
		/// <param name="item">s2sBody item to add</param>
		void Add(s2sBody item);

		/// <summary>
		/// Adds a s2sHeader item
		/// </summary>
		/// <param name="item">s2sHeader item to add</param>
		void Add(s2sHeader item);
	}
}