﻿using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Adapters
{
	public interface IClientTypeToTitoTerminaltypeAdapter : IAdapter<clientType, TITO_TERMINAL_TYPE>, IAdapter<TITO_TERMINAL_TYPE, clientType>
	{
	}
}
