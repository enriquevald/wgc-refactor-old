﻿using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Adapters
{
	/// <summary>
	/// Implements BusinessTicketClass to S2S GSA ErrorCode adapter
	/// </summary>
	public class BusinessTicketClassToErrorCodeAdapter : IAdapter<BusinessExceptionTicketItem, string>
	{
		public string Adapt(BusinessExceptionTicketItem source)
		{
			string result = null;

			switch (source)
			{
				case BusinessExceptionTicketItem.InvalidTicketIdentifier:
					result = VoucherClass.InvalidVoucherIdentifier;
					break;
				case BusinessExceptionTicketItem.TicketHasBeenRedeemed:
					result = VoucherClass.VoucherHasBeenRedeemed;
					break;
				case BusinessExceptionTicketItem.TicketHasBeenVoided:
					result = VoucherClass.VoucherHasBeenVoided;
					break;
				case BusinessExceptionTicketItem.TicketHasExpired:
					result = VoucherClass.VoucherHasExpired;
					break;
				case BusinessExceptionTicketItem.TicketRedemptionVoidPending:
					result = VoucherClass.VoucherRedemptionVoidPending;
					break;
				case BusinessExceptionTicketItem.InvalidTicketAction:
					result = VoucherClass.InvalidVoucherAction;
					break;
			}

			return result;
		}
	}
}