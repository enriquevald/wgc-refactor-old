﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Adapters
{
	/// <summary>
	/// Implements ClientType to TitoTerminalType adapter and viceversa
	/// </summary>
	public class ClientTypeToTitoTerminalTypeAdapter : IClientTypeToTitoTerminaltypeAdapter
	{
		public TITO_TERMINAL_TYPE Adapt(clientType source)
		{
			TITO_TERMINAL_TYPE result;

			switch (source)
			{
				case clientType.cashier:
					result = TITO_TERMINAL_TYPE.CASHIER;
					break;
				case clientType.egm:
					result = TITO_TERMINAL_TYPE.TERMINAL;
					break;
				case clientType.kiosk:
					result = TITO_TERMINAL_TYPE.COUNTR;
					break;
				default:
					result = TITO_TERMINAL_TYPE.TERMINAL;
					break;
			}

			return result;
		}

		public clientType Adapt(TITO_TERMINAL_TYPE source)
		{
			clientType result;

			switch (source)
			{
				case TITO_TERMINAL_TYPE.CASHIER:
					result = clientType.cashier;
					break;
				case TITO_TERMINAL_TYPE.COUNTR:
					result = clientType.kiosk;
					break;
				default:
					result = clientType.egm;
					break;
			}

			return result;
		}
	}
}