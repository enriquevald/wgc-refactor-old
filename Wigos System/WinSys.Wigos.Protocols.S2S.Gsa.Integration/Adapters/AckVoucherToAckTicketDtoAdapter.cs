﻿using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Adapters
{
	/// <summary>
	/// Implements a AckVoucher to AckTicketDto adapter
	/// </summary>
	public class AckVoucherToAckTicketDtoAdapter : IAdapter<ackVoucher, AckTicketDto>
	{
		private readonly IClientTypeToTitoTerminaltypeAdapter clientTypeToTitoTerminalTypeAdapter;

		public AckVoucherToAckTicketDtoAdapter(IClientTypeToTitoTerminaltypeAdapter clientTypeToTitoTerminalTypeAdapter)
		{
			this.clientTypeToTitoTerminalTypeAdapter = clientTypeToTitoTerminalTypeAdapter;
		}

		public AckTicketDto Adapt(ackVoucher source)
		{
			return new AckTicketDto
			       {
				       ClientId = source.clientId,
					   //TODO: Refactor. FINISH the adapters. The imply Dto changes with high implication
				       //TerminalType = this.clientTypeToTitoTerminalTypeAdapter.Adapt(source.clientType)
			       };
		}
	}
}