﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of AuthorizeVoucherCommand
	/// </summary>
	public class AuthorizeVoucherCommand : ICommand
	{
		private readonly authorizeVoucher authorizeVoucherData;

		private readonly voucher voucherData;

		public AuthorizeVoucherCommand(voucher voucherData)
		{
			this.voucherData = voucherData;

			this.authorizeVoucherData = (authorizeVoucher)voucherData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action required. Really we don't have to receive this command but is necessary recognize it for tests only
		}

		#endregion
	}
}