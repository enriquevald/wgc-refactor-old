﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommsOffLineCommand
	/// </summary>
	public class CommsOffLineCommand : ICommand
	{
		private readonly commsOffLine commsOffLineData;

		private readonly IProtocolState protocolState;
		private readonly s2sHeader requestHeader;
		private readonly communications communicationsData;

		public CommsOffLineCommand(
			IProtocolState protocolState,
			s2sHeader requestHeader,
			communications communicationsData)
		{
			this.protocolState = protocolState;
			this.requestHeader = requestHeader;
			this.communicationsData = communicationsData;

			this.commsOffLineData = (commsOffLine)communicationsData.Item;
		}

		#region ICommand

		public void Execute()
		{
			this.protocolState.RemoveRemoteState(this.requestHeader.fromSystem);
		}

		#endregion	
	}
}