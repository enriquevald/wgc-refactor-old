﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of ValidationIdListCommand
	/// </summary>
	public class ValidationIdListCommand : ICommand
	{
		private readonly validationIdList validationIdListData;

		private readonly voucher voucherData;

		public ValidationIdListCommand(voucher voucherData)
		{
			this.voucherData = voucherData;

			this.validationIdListData = (validationIdList)voucherData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action required. Really we don't have to receive this command but is necessary recognize it for tests only
		}

		#endregion	
	}
}