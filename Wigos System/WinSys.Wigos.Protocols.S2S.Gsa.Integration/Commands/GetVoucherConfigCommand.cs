﻿using System;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of GetVoucherConfigCommand
	/// </summary>
	public class GetVoucherConfigCommand : ICommandWithReturnValue<object>
	{
		private voucher resultVoucher;

		private readonly getVoucherConfig getVoucherConfigData;

		private readonly ITicketService ticketService;
		private readonly voucher voucherData;

		public GetVoucherConfigCommand(
			ITicketService ticketService,
			voucher voucherData)
		{
			this.ticketService = ticketService;
			this.voucherData = voucherData;

			this.getVoucherConfigData = (getVoucherConfig)voucherData.Item;
			this.resultVoucher = null;
		}

		#region IReturnCommand

		public void Execute()
		{
			TicketConfigurationDto voucherConfigDto = this.ticketService.GetTicketConfiguration(new GetTicketConfigDto
			                                                                  {
																				  ClientId = this.getVoucherConfigData.clientId,
																				  ClientType = (ClientType)this.getVoucherConfigData.clientType
			                                                                  });
			this.resultVoucher = new voucher
			                     {
									 sessionTypeSpecified = true,
									 sessionType = sessionType.response,
									 dateTime = DateTime.Now,
				                     Item = new voucherConfig
				                            {
												clientId = voucherConfigDto.ClientId,
												clientType = (clientType)voucherConfigDto.ClientType,
												configurationId = voucherConfigDto.ConfigurationId,
												expireCage = voucherConfigDto.ExpireCage,
												expireEGM = voucherConfigDto.ExpireEgm,
												expirePrint = (expirePrint)voucherConfigDto.ExpirePrint,
												propLine1 = this.GetMaxString(voucherConfigDto.AddressLine1, 40),
												propLine2 = this.GetMaxString(voucherConfigDto.AddressLine2, 40),
												propName = this.GetMaxString(voucherConfigDto.Location, 40),
												roundTime = voucherConfigDto.RoundTime,
												roundUp = voucherConfigDto.IsRoundUp,
												titleCash = this.GetMaxString(voucherConfigDto.TitleCash, 16),
												titleLargeWin = this.GetMaxString(voucherConfigDto.TitleJackpot, 16),
												titleNonCash = this.GetMaxString(voucherConfigDto.TitleNonCash, 16),
												titlePromo = this.GetMaxString(voucherConfigDto.TitlePromo, 16)
				                            },
				                     propertyId = this.voucherData.propertyId
			                     };
		}

		public object GetValue()
		{
			return this.resultVoucher;
		}

		#endregion

		private string GetMaxString(string str, int maxLength)
		{
			string result = null;

			if (str != null)
			{
				if (maxLength > 0)
				{
					result = str.Length <= maxLength ? str : str.Substring(0, maxLength - 1);
				}
				else
				{
					result = string.Empty;
				}
			}

			return result;
		}
	}
}