﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of KeepAliveCommand
	/// </summary>
	public class KeepAliveCommand : ICommand
	{
		private readonly keepAlive keepAliveData;

		private readonly communications communicationsData;

		public KeepAliveCommand(communications communicationsData)
		{
			this.communicationsData = communicationsData;

			this.keepAliveData = (keepAlive)this.communicationsData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action for this command
		}

		#endregion	
	}
}