﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommsOnLineCommand
	/// </summary>
	public class CommsOnLineCommand : ICommand
	{
		private readonly communications communicationsData;

		private readonly IProtocolState protocolState;
		private readonly s2sHeader requestHeader;
		private readonly commsOnLine commsOnLineData;

		public CommsOnLineCommand(
			IProtocolState protocolState,
			s2sHeader requestHeader,
			communications communicationsData)
		{
			this.protocolState = protocolState;
			this.requestHeader = requestHeader;
			this.communicationsData = communicationsData;

			this.commsOnLineData = (commsOnLine)communicationsData.Item;
		}
		
		#region ICommand

		public void Execute()
		{
			this.protocolState.SetRemoteState(
				this.requestHeader.fromSystem, 
				new State
				{
					PropertyId = communicationsData.propertyId,
					FromSystem = this.requestHeader.fromSystem,
					ToSystem = this.requestHeader.toSystem,
					RemoteEndpoint = this.commsOnLineData.s2sLocation,
					SessionId = -1,
					InboundCommandId = -1
				});
		}

		#endregion
	}
}