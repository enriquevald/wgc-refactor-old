﻿using System;
using System.Resources;
using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of IssueVoucherCommand
	/// </summary>
	public class IssueVoucherCommand : ICommandWithReturnValue<object>
	{
		private voucher resultVoucher;

		private readonly issueVoucher issueVoucherData;
		private readonly voucher voucherData;

		private readonly IIssueVoucherValidator issueVoucherValidator;
		private readonly IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter;
		private readonly ITicketService ticketService;
		private readonly ResourceManager errorResourceManager;

		public IssueVoucherCommand(
			IIssueVoucherValidator issueVoucherValidator,
			ITicketService ticketService,
			IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter,
			voucher voucherData)
		{
			this.issueVoucherValidator = issueVoucherValidator;
			this.ticketService = ticketService;
			this.businessVoucherClassToErrorCodeAdapter = businessVoucherClassToErrorCodeAdapter;
			this.voucherData = voucherData;

			this.issueVoucherData = (issueVoucher)voucherData.Item;

			this.errorResourceManager = new ResourceManager(typeof(TypedResources.S2SErrors));

			this.resultVoucher = null;
		}

		#region IReturnCommand

		public void Execute()
		{
			this.resultVoucher = new voucher
			                     {
				                     sessionTypeSpecified = true,
				                     sessionType = sessionType.response,
				                     dateTime = DateTime.Now,
				                     propertyId = this.voucherData.propertyId
			                     };
			
			try
			{
				this.issueVoucherValidator.Validate(this.issueVoucherData);

				AckTicketDto ackVoucher = this.ticketService.IssueTicket(this.ReadIssueVoucherRequestToDto());
				if (ackVoucher != null)
				{
					this.resultVoucher.Item = new ackVoucher
					                          {
						                          clientId = ackVoucher.ClientId,
						                          clientType = (clientType)ackVoucher.TerminalType,
						                          configurationId = ackVoucher.ConfigurationId,
						                          transactionId = ackVoucher.TransactionId
					                          };
				}
			}
			catch (Exception ex)
			{
				BusinessException businessException = ex as BusinessException;
				if (businessException != null)
				{
					this.resultVoucher.errorCode = this.businessVoucherClassToErrorCodeAdapter.Adapt((BusinessExceptionTicketItem)businessException.SourceItem);
					this.resultVoucher.errorText = this.errorResourceManager.GetString(this.resultVoucher.errorCode);
				}
				else
				{
					throw;
				}
			}
		}

		private IssueTicketDto ReadIssueVoucherRequestToDto()
		{
			return new IssueTicketDto()
			{
				CageDateTime = this.issueVoucherData.cageDateTime,
				CardId = this.issueVoucherData.cardId,
				CardRestrict = (CardRestrict)this.issueVoucherData.cardRestrict,
				ClientId = this.issueVoucherData.clientId,
				ClientType = (ClientType)this.issueVoucherData.clientType,
				CreditType = (CreditType)this.issueVoucherData.creditType,
				CurrencyId = this.issueVoucherData.currencyId,
				EgmDateTime = this.issueVoucherData.egmDateTime,
				IsJackpot = this.issueVoucherData.largeWin,
				PoolId = this.issueVoucherData.poolId,
				TransactionId = this.issueVoucherData.transactionId,
				TransferAction = (int)this.issueVoucherData.transferAction,
				TransferAmount = this.issueVoucherData.transferAmount,
				TransferDateTime = this.issueVoucherData.transferDateTime,
				TransferException = this.issueVoucherData.transferException,
				TransferSequence = this.issueVoucherData.transferSequence,
				TicketAmount = this.issueVoucherData.voucherAmount,
				ValidationNumber = this.issueVoucherData.voucherId,
				ExpireDateTime = this.issueVoucherData.expireDateTime
			};
		}

		public object GetValue()
		{
			return this.resultVoucher;
		}

		#endregion
	}
}