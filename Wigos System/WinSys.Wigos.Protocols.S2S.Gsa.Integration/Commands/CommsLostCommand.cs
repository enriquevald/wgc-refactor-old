﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommsLostCommand
	/// </summary>
	public class CommsLostCommand : ICommand
	{
		private readonly communications communicationsData;

		private readonly commsLost commsLostData;

		public CommsLostCommand(communications communicationsData)
		{
			this.communicationsData = communicationsData;

			this.commsLostData = (commsLost)communicationsData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action required.
		}

		#endregion
	}
}