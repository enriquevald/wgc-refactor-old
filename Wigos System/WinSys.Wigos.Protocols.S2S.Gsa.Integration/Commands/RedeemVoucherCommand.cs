﻿using System;
using System.Resources;

using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of IssueVoucherCommand
	/// </summary>
	public class RedeemVoucherCommand : ICommandWithReturnValue<object>
	{
		private voucher resultVoucher;

		private readonly ResourceManager errorResourceManager;

		private readonly redeemVoucher redeemVoucherData;

		private readonly ITicketService ticketService;
		private readonly IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter;
		private readonly voucher voucherData;
		
		public RedeemVoucherCommand(
			ITicketService ticketService, 
			IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter,
			voucher voucherData)
		{
			this.ticketService = ticketService;
			this.businessVoucherClassToErrorCodeAdapter = businessVoucherClassToErrorCodeAdapter;
			this.voucherData = voucherData;

			this.redeemVoucherData = (redeemVoucher)voucherData.Item;

			this.errorResourceManager = new ResourceManager(typeof(TypedResources.S2SErrors));
		}

		#region IReturnCommand

		public void Execute()
		{
			RedeemTicketDto ticketIn =
				new RedeemTicketDto
				{
					CardId = this.redeemVoucherData.cardId, 
					ClientId = this.redeemVoucherData.clientId, 
					ClientType = (ClientType)this.redeemVoucherData.clientType, 
					TransactionId = this.redeemVoucherData.transactionId, 
					ValidationNumber = this.redeemVoucherData.voucherId
				};

			AuthorizeTicketDto authorizeVoucher = null;
			this.resultVoucher = new voucher
			                     {
				                     sessionTypeSpecified = true,
				                     sessionType = sessionType.response,
									 dateTime = DateTime.Now,
				                     propertyId = this.voucherData.propertyId
			                     };
			try
			{
				authorizeVoucher = this.ticketService.ReddemTicket(ticketIn);
			}
			catch (Exception ex)
			{
				BusinessException businessException = ex as BusinessException;
				if (businessException != null)
				{
					this.resultVoucher.errorCode = this.businessVoucherClassToErrorCodeAdapter.Adapt((BusinessExceptionTicketItem)businessException.SourceItem);
					this.resultVoucher.errorText = this.errorResourceManager.GetString(this.resultVoucher.errorCode);
				}
				else
				{
					throw;
				}
			}

			if (authorizeVoucher != null)
			{
				this.resultVoucher.Item = new authorizeVoucher
				                          {
					                          clientId = authorizeVoucher.ClientId,
					                          clientType = (clientType)authorizeVoucher.ClientType,
					                          cageDateTime = authorizeVoucher.CageDateTime,
					                          cardId = authorizeVoucher.CardId,
					                          cardRestrict = (cardRestrict)authorizeVoucher.CardRestrict,
					                          creditType = (creditTypes)authorizeVoucher.CreditType,
					                          currencyId = authorizeVoucher.CurrencyId,
					                          egmDateTime = authorizeVoucher.EgmDateTime,
					                          largeWin = authorizeVoucher.IsJackpot,
					                          poolId = authorizeVoucher.PoolId,
					                          transactionId = authorizeVoucher.TransactionId,
					                          voucherAmount = authorizeVoucher.TicketAmount,
					                          voucherId = authorizeVoucher.ValidationNumber,
				                          };
			}
		}

		public object GetValue()
		{
			return this.resultVoucher;
		}

		#endregion
	}
}