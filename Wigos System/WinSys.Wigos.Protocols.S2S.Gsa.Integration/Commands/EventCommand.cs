﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of EventCommand
	/// </summary>
	public class EventCommand : ICommand
	{
		private readonly event1 eventData;

		private readonly voucher voucherData;

		public EventCommand(voucher voucherData)
		{
			this.voucherData = voucherData;

			this.eventData = (event1)voucherData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action required.
		}

		#endregion	
	}
}