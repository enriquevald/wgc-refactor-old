﻿using System;

using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of GetValidationIdsCommand
	/// </summary>
	public class GetValidationIdsCommand : ICommandWithReturnValue<object>
	{
		private readonly getValidationIds getValidationIdsData;
		private ValidationNumbersDto valIdList;

		private readonly ITicketService ticketService;

		private readonly IConfigurationService configurationService;	// TODO: Refactor. Do this get configurationId in VoucherConfig service!!!
		private readonly voucher voucherData;

		public GetValidationIdsCommand(
			ITicketService ticketService,
			IConfigurationService configurationService,
			voucher voucherData)
		{
			this.ticketService = ticketService;
			this.configurationService = configurationService;
			this.voucherData = voucherData;

			this.getValidationIdsData = (getValidationIds)voucherData.Item;
		}

		#region IReturnCommand

		public void Execute()
		{
			this.valIdList = this.ticketService.GetValidationNumbers(this.getValidationIdsData.clientId);
		}

		// TODO: Refactor. Move logic actions to VoucherService. See RedeemVoucherCommand as an example
		public object GetValue()
		{
			voucher returnVoucher = new voucher();
			validationIdList vil;

			validationIdListValidationIdItem[] viListValidationiItem = new validationIdListValidationIdItem[this.valIdList.ValidationNumbers.Count];
			for (int i = 0; i < this.valIdList.ValidationNumbers.Count; i++)
			{
				viListValidationiItem[i] = new validationIdListValidationIdItem { validationId = this.valIdList.ValidationNumbers[i] };
			}

			vil = new validationIdList()
			{
				clientType = this.getValidationIdsData.clientType,
				clientId = this.getValidationIdsData.clientId,
				configurationId = this.configurationService.GetConfigurationIdCurrentSequence(),
				validationIdItem = viListValidationiItem,
				seedDateTime = this.valIdList.SeedDateTime,
				seedValue1 = this.valIdList.Seed1,
				seedValue2 = this.valIdList.Seed2
			};

			returnVoucher = new voucher()
			{
				propertyId = this.voucherData.propertyId,
				dateTime = DateTime.Now,
				sessionTypeSpecified = true,
				sessionType = sessionType.response,
				sessionId = this.voucherData.sessionId,
				Item = vil
			};

			return returnVoucher;
		}

		#endregion
	}
}