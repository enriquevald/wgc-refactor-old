﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommsErrorCommand
	/// </summary>
	public class CommsErrorCommand : ICommand
	{
		private readonly commsError commsErrorData;

		private readonly communications communicationsData;

		public CommsErrorCommand(
			communications communicationsData)
		{
			this.communicationsData = communicationsData;

			this.commsErrorData = (commsError)communicationsData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action for this command. Ignore.
		}

		#endregion	
	}
}