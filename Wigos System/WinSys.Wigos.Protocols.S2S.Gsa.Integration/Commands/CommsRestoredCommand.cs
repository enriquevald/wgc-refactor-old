﻿using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommsRestoredCommand
	/// </summary>
	public class CommsRestoredCommand : ICommand
	{
		private readonly communications communicationsData;

		private readonly commsRestored commsRestoredData;

		public CommsRestoredCommand(communications communicationsData)
		{
			this.communicationsData = communicationsData;

			this.commsRestoredData = (commsRestored)communicationsData.Item;
		}

		#region ICommand

		public void Execute()
		{
			// No action required.
		}

		#endregion
	}
}