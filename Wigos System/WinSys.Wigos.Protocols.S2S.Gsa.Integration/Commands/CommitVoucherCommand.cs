﻿using System;
using System.Resources;
using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands
{
	/// <summary>
	/// Logic of CommitVoucherCommand
	/// </summary>
	public class CommitVoucherCommand : ICommandWithReturnValue<object>
	{
		private voucher resultVoucher;

		private readonly ResourceManager errorResourceManager;

		private readonly commitVoucher commitVoucherData;

		private readonly ICommitVoucherValidator commitVoucherValidator;
		private readonly ITicketService ticketService;
		private readonly IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter;
		private readonly voucher voucherData;

		public CommitVoucherCommand(
			ICommitVoucherValidator commitVoucherValidator,
			ITicketService ticketService,
			IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter,
			voucher voucherData)
		{
			this.commitVoucherValidator = commitVoucherValidator;
			this.ticketService = ticketService;
			this.businessVoucherClassToErrorCodeAdapter = businessVoucherClassToErrorCodeAdapter;
			this.voucherData = voucherData;

			this.commitVoucherData = (commitVoucher)voucherData.Item;

			this.errorResourceManager = new ResourceManager(typeof(TypedResources.S2SErrors));

			this.resultVoucher = null;
		}

		#region IReturnCommand

		public void Execute()
		{
			this.resultVoucher = new voucher
			                     {
				                     sessionTypeSpecified = true,
				                     sessionType = sessionType.response,
				                     dateTime = DateTime.Now,
				                     propertyId = this.voucherData.propertyId
			                     };
		
			try
			{
				this.commitVoucherValidator.Validate(this.commitVoucherData);

				CommitTicketDto commitVoucher =
					new CommitTicketDto
					{
						CardId = this.commitVoucherData.cardId,
						ClientId = this.commitVoucherData.clientId,
						ClientType = (ClientType)this.commitVoucherData.clientType,
						TransactionId = this.commitVoucherData.transactionId,
						ValidationNumber = this.commitVoucherData.voucherId
					};
				
				AckTicketDto ackVoucher = this.ticketService.CommitTicket(commitVoucher);
				if (ackVoucher != null)
				{
					this.resultVoucher.Item = new ackVoucher
					                          {
						                          clientId = ackVoucher.ClientId,
						                          clientType = (clientType)ackVoucher.TerminalType,
						                          configurationId = ackVoucher.ConfigurationId,
						                          transactionId = ackVoucher.TransactionId
					                          };
				}
			}
			catch (Exception ex)
			{
				BusinessException businessException = ex as BusinessException;
				if (businessException != null)
				{
					this.resultVoucher.errorCode = this.businessVoucherClassToErrorCodeAdapter.Adapt((BusinessExceptionTicketItem)businessException.SourceItem);
					this.resultVoucher.errorText = this.errorResourceManager.GetString(this.resultVoucher.errorCode);
				}
				else
				{
					throw;
				}
			}
		}

		public object GetValue()
		{
			return this.resultVoucher;
		}

		#endregion
	}
}