﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services
{
	/// <summary>
	/// Logic for XmlSerializerService
	/// </summary>
	public class XmlSerializerService : IXmlSerializerService
	{
		private const string XmlNamespace = @"http://www.gamingstandards.com/s2s/schemas/v1.2.6/";
		private const string XmlPrefix = "s2s";

		#region IXmlSerializer

		/// <summary>
		/// Serialize a specified data object
		/// </summary>
		/// <param name="type">Data type to serialize</param>
		/// <param name="data">Data to serialize</param>
		/// <returns>Serialized data</returns>
		/// <remarks>SyntaxException is thrown in case of any serialization error</remarks>
		public string Serialize(Type type, object data)
		{
			string result;

			try
			{
				using (MemoryStream memoryStream = new MemoryStream())
				using (XmlWriter xmlWriter = new XmlTextWriter(memoryStream, Encoding.UTF8))
				{
					XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces();
					xmlSerializerNamespaces.Add(XmlSerializerService.XmlPrefix, XmlSerializerService.XmlNamespace);

					XmlSerializer xmlSerializer = new XmlSerializer(type);
					xmlSerializer.Serialize(xmlWriter, data, xmlSerializerNamespaces);
					using (StreamReader streamReader = new StreamReader(memoryStream))
					{
						memoryStream.Position = 0;
						result = streamReader.ReadToEnd();
					}
				}
			}
			catch (Exception ex)
			{
				throw new SyntaxException(ex.Message, ex);
			}

			return result;
		}

		/// <summary>
		/// Serialize a specified data object
		/// </summary>
		/// <typeparam name="T">Data type to serialize</typeparam>
		/// <param name="data">Data to serialize</param>
		/// <returns>Serialized data</returns>
		/// <remarks>SyntaxException is thrown in case of any serialization error</remarks>
		public string Serialize<T>(T data)
		{
			return this.Serialize(data.GetType(), data);
		}

		/// <summary>
		/// Deserialize a specified data
		/// </summary>
		/// <param name="type">Data type to deserialize</param>
		/// <param name="data">Data to deserialize</param>
		/// <returns>Typed data object</returns>
		/// <remarks>SyntaxException is thrown in case of any deserialization error</remarks>
		public object Deserialize(Type type, string data)
		{
			object result;

			try
			{
				XmlSerializer xmlSerializer = new XmlSerializer(type);
				using (StringReader stringReader = new StringReader(data))
				{
					result = xmlSerializer.Deserialize(XmlReader.Create(stringReader));
				}
			}
			catch (Exception ex)
			{
				throw new SyntaxException(ex.Message, ex);
			}

			return result;
		}

		/// <summary>
		/// Deserialize a specified data
		/// </summary>
		/// <typeparam name="T">Data type to deserialize</typeparam>
		/// <param name="data">Data to deserialize</param>
		/// <returns>typed data object</returns>
		/// <remarks>SyntaxException is thrown in case of any deserialization error</remarks>
		public T Deserialize<T>(string data)
		{
			return (T)this.Deserialize(typeof(T), data);
		}

		#endregion
	}
}