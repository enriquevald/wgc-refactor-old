﻿using System.Collections.Generic;
using System.Linq;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Interfaces;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6
{
	public partial class communications : IName
	{
		public string Name
		{
			get 
			{
				string subCommand = this.Item != null ? "." + this.Item.GetType().Name : string.Empty; 
				return this.GetType().Name + subCommand;
			}
		}
	}
	
	public partial class voucher : IVoucherIds, IName
	{
		public IEnumerable<string> VoucherIds
		{
			get
			{
				IEnumerable<string> result = null;

				IVoucherId internalVoucherId = this.Item as IVoucherId;
				if (internalVoucherId != null
					&& !string.IsNullOrEmpty(internalVoucherId.VoucherId))
				{
					result = new List<string> { internalVoucherId.VoucherId };
				}
				else
				{
					IVoucherIds internalVoucherIds = this.Item as IVoucherIds;
					if (internalVoucherIds != null)
					{
						result = internalVoucherIds.VoucherIds;
					}
				}

				return result ?? new List<string>();
			}
		}

		public string Name
		{
			get
			{
				string subCommand = this.Item != null ? "." + this.Item.GetType().Name : string.Empty; 
				return this.GetType().Name + subCommand;
			}
		}
	}

	public partial class validationIdList : IVoucherIds
	{
		#region IVoucherIds

		public IEnumerable<string> VoucherIds
		{
			get
			{
				return Enumerable.Where<validationIdListValidationIdItem>(this.validationIdItem, i => !string.IsNullOrEmpty(i.VoucherId))
					.Select(i => i.VoucherId);
			}
		}

		#endregion
	}

	public partial class validationIdListValidationIdItem : IVoucherId
	{
		#region IVoucherId

		public string VoucherId
		{
			get
			{
				return this.validationId;
			}
		}

		#endregion
	}

	public partial class issueVoucher : IVoucherId
	{
		#region IVoucherId

		public string VoucherId
		{
			get
			{
				return this.voucherId;
			}
		}

		#endregion
	}

	public partial class redeemVoucher : IVoucherId
	{
		#region IVoucherId

		public string VoucherId
		{
			get
			{
				return this.voucherId;
			}
		}

		#endregion
	}

	public partial class commitVoucher : IVoucherId
	{
		#region IVoucherId

		public string VoucherId
		{
			get
			{
				return this.voucherId;
			}
		}

		#endregion
	}
}