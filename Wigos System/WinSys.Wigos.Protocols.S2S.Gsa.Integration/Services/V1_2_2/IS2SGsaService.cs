using System.ServiceModel;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2
{
	[ServiceContract]
	public interface IS2SGsaService : S2SMessagePostPortType
	{
	}
}