using System;
using System.ServiceModel;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2
{
	/// <summary>
	/// GSA S2S service layer
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class S2SGsaService : IS2SGsaService
	{
		private readonly IProtocolManager protocolManager;
		private readonly ILogService logService;

		public S2SGsaService(
			IProtocolManager protocolManager,
			ILogService logService)
		{
			this.protocolManager = protocolManager;
			this.logService = logService;
		}

		public S2SMessagePostOperationResponse S2SMessagePostOperation(S2SMessagePostOperationRequest request)
		{
			this.logService.Information(string.Format("Received S2SMessagePostOperationRequest: '{0}'", request));

			S2SMessagePostOperationResponse response = null;
			try
			{
				response = new S2SMessagePostOperationResponse
				{
					s2sResponse = new ResponseType
					{
						response = this.protocolManager.Receive(request.s2sRequest.request)
					}
				};
			}
			catch (Exception ex)
			{
				this.logService.Exception(ex);
			}

			return response;
		}

		public S2SMessagePostOperationResponse SendS2SGZipPayloadMessage(SendS2SGZipPayloadMessageRequest request)
		{
			throw new NotImplementedException();
		}

		public getTransportOptionsResponse getTransportOptions(getTransportOptionsRequest request)
		{
			throw new NotImplementedException();
		}
	}
}