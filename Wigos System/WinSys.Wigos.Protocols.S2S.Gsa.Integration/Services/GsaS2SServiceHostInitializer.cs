﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services
{
	/// <summary>
	/// GSA S2S service listener for general host manager
	/// </summary>
	public class GsaS2SServiceHostInitializer : IDisposable
	{
		private bool isDisposed;	// Track whether Dispose has been called.

		private ServiceHost gsaS2sServiceHost;

		private readonly IS2SGsaService gsaS2sService;

		private readonly ILogService logService;

		public GsaS2SServiceHostInitializer(
			IS2SGsaService gsaS2sService,
			ILogService logService)
		{
			this.gsaS2sService = gsaS2sService;
			this.logService = logService;

			this.isDisposed = false;

			try
			{
				this.logService.Information("Starting GsaS2sService...");
				this.CreateAndInitializeHostListener();
			}
			catch (Exception ex)
			{
				this.logService.Exception(ex);
			}
		}

		private void CreateAndInitializeHostListener()
		{
			Uri baseAddress = this.GetServiceUrl();

			this.logService.Information(string.Format("GsaS2sService Start. Uri: '{0}'.", baseAddress));

			this.gsaS2sServiceHost = new ServiceHost(
				this.gsaS2sService,
				baseAddress);

			ServiceMetadataBehavior serviceMetadataBehavior = new ServiceMetadataBehavior();
			serviceMetadataBehavior.HttpGetEnabled = true;
			serviceMetadataBehavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
			this.gsaS2sServiceHost.Description.Behaviors.Add(serviceMetadataBehavior);

			this.gsaS2sServiceHost.AddServiceEndpoint(
				typeof(IS2SGsaService),
				new BasicHttpBinding(),
				baseAddress);
			this.gsaS2sServiceHost.AddServiceEndpoint(
				ServiceMetadataBehavior.MexContractName,
				MetadataExchangeBindings.CreateMexHttpBinding(),
				"mex");

			this.gsaS2sServiceHost.Faulted += this.OnServiceHostFaulted;
			this.gsaS2sServiceHost.Closed += this.OnServiceHostClosed;

			this.gsaS2sServiceHost.Open();

			this.logService.Information("S2S GsaS2sService: Started");
		}

		private Uri GetServiceUrl()
		{
			return new Uri(Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.Ip + ':' + Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.Port);
		}

		private void OnServiceHostFaulted(object sender, EventArgs eventArgs)
		{
			this.logService.Information("GsaS2sService: Faulted");

			this.gsaS2sServiceHost.Faulted -= this.OnServiceHostFaulted;
			this.gsaS2sServiceHost = null;
			this.CreateAndInitializeHostListener();
		}

		private void OnServiceHostClosed(object sender, EventArgs eventArgs)
		{
			this.logService.Information("GsaS2sService: Closed");

			this.gsaS2sServiceHost.Faulted -= this.OnServiceHostClosed;
			this.gsaS2sServiceHost = null;
			this.CreateAndInitializeHostListener();
		}

		// Implement IDisposable.
		// Do not make this method virtual.
		// A derived class should not be able to override this method.
		public void Dispose()
		{
			this.Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool isDisposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If isDisposing equals false, the method has been called by the
		// runtime from inside the finalizer and you should not reference
		// other objects. Only unmanaged resources can be disposed.
		protected virtual void Dispose(bool isDisposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.isDisposed)
			{
				// If disposing equals true, dispose all managed
				// and unmanaged resources.
				if (isDisposing)
				{
					// Dispose managed resources.
					if (this.gsaS2sServiceHost.State != CommunicationState.Closed)
					{
						this.gsaS2sServiceHost.Faulted -= this.OnServiceHostFaulted;
						this.gsaS2sServiceHost.Closed -= this.OnServiceHostClosed;

						this.gsaS2sServiceHost.Close();
					}
				}

				// Call the appropriate methods to clean up
				// unmanaged resources here.
				// If disposing is false,
				// only the following code is executed.

				// Note disposing has been done.
				this.isDisposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~GsaS2SServiceHostInitializer()
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			this.Dispose(false);
		}
	}
}