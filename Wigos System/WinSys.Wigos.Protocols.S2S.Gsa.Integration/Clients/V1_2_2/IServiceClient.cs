﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2
{
	public interface IServiceClient : IS2SGsaService
	{
		s2sMessage S2SMessagePostOperation(s2sMessage request);
	}
}