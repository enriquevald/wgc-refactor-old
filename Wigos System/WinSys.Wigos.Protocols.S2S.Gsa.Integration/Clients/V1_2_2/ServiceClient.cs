﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2
{
	/// <summary>
	/// Implementation of centralized ServiceClient
	/// </summary>
	public class ServiceClient : IServiceClient
	{
		private bool IsInTypedMode 
		{
			get
			{
				return this.xmlSerializerService != null && this.sequenceRepository != null && this.protocolState != null && this.logService != null;
			}
		}

		private readonly IXmlSerializerService xmlSerializerService;
		private readonly ISequenceRepository sequenceRepository;
		private readonly IProtocolState protocolState;
		private readonly ILogService logService;
		private readonly ServiceReference.S2SGsaServiceClient serviceClient;

		public ServiceClient(
			Binding binding,
			EndpointAddress remoteAddress)
		{
			this.xmlSerializerService = null;
			this.sequenceRepository = null;
			this.protocolState = null;
			this.serviceClient = new ServiceReference.S2SGsaServiceClient(binding, remoteAddress);
		}

		public ServiceClient(
			IXmlSerializerService xmlSerializerService,
			ISequenceRepository sequenceRepository,
			IProtocolState protocolState,
			ILogService logService,
			Binding binding,
			EndpointAddress remoteAddress)
		{
			this.xmlSerializerService = xmlSerializerService;
			this.sequenceRepository = sequenceRepository;
			this.protocolState = protocolState;
			this.logService = logService;
			this.serviceClient = new ServiceReference.S2SGsaServiceClient(binding, remoteAddress);
		}

		// TODO: Refactor. This is an externar responsability and must move out of this class the logic of message data settings
		public s2sMessage S2SMessagePostOperation(s2sMessage request)
		{
			S2SMessagePostOperationResponse clientResponse = null;

			if (this.IsInTypedMode)
			{
				s2sHeader header = request.Items
					.OfType<s2sHeader>()
					.First();
				s2sBody body = request.Items
					.OfType<s2sBody>()
					.First();
				RemoteStateActionDelegate sendOperation = (State state) =>
				{
					header.messageId = this.sequenceRepository.GetNextValue(SequenceId.S2SGsaOutBoundMessageId);
					foreach (baseClass command in body.Items)
					{
						command.commandId = this.sequenceRepository.GetNextValue(SequenceId.S2SGsaOutBoundCommandId);
					}

					this.logService.Information(string.Format("ServiceClient Request to '{0}': {1}", state.RemoteEndpoint, this.xmlSerializerService.Serialize(request)));

					S2SMessagePostOperationRequest clientRequest =
						new S2SMessagePostOperationRequest
						{
							s2sRequest = new RequestType { request = this.xmlSerializerService.Serialize(request) }
						};
					clientResponse = this.S2SMessagePostOperation(clientRequest);
				};
				this.protocolState.RemoteStateAction(header.toSystem, sendOperation);
			}
			else
			{
				throw new ArgumentException("This method needs injected parameters");
			}

			return this.xmlSerializerService.Deserialize<s2sMessage>(clientResponse.s2sResponse.response);
		}

		public S2SMessagePostOperationResponse S2SMessagePostOperation(S2SMessagePostOperationRequest request)
		{
			ServiceReference.RequestType serviceRequest = new ServiceReference.RequestType { request = request.s2sRequest.request };
			ServiceReference.ResponseType serviceResponse = this.serviceClient.S2SMessagePostOperation(serviceRequest);

			return new S2SMessagePostOperationResponse { s2sResponse = new ResponseType { response = serviceResponse.response } };
		}

		public S2SMessagePostOperationResponse SendS2SGZipPayloadMessage(SendS2SGZipPayloadMessageRequest request)
		{
			throw new NotImplementedException();
		}

		public getTransportOptionsResponse getTransportOptions(getTransportOptionsRequest request)
		{
			throw new NotImplementedException();
		}
	}
}