﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Implements the protocol message reception method
	/// </summary>
	public interface IReceive
	{
		/// <summary>
		/// Manage the reception of the request
		/// </summary>
		/// <param name="request">Request to receive</param>
		/// <return>Response to the request</return>
		string Receive(string request);
	}
}