﻿using System;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	// TODO: Refactor. The base class may be inyected instead of being inherited, but Helio decided that it was inheritable.

	/// <summary>
	/// Base management for GSA S2S protocol
	/// </summary>
	public abstract class BaseProtocolManager : IProtocolManager
	{
		protected readonly ILogService logService;

		public BaseProtocolManager(ILogService logService)
		{
			this.logService = logService;
		}

		#region IGsaS2sProtocolManager

		/// <summary>
		/// Manage the reception of the request
		/// </summary>
		/// <param name="request">Request to receive</param>
		public string Receive(string request)
		{
			string response;

			try
			{
				response = this.DoReceive(request);
			}
			catch (Exception ex)
			{
				this.logService.Information("* Receive Exception");
				this.logService.Exception(ex);
				response = this.SendAck(ex);
			}

			return response;
		}

		/// <summary>
		/// Manage the process of the request
		/// </summary>
		/// <param name="request">Request to process</param>
		public void Process(s2sMessage request)
		{
			try
			{
				this.DoProcess(request);
			}
			catch (Exception ex)
			{
				this.logService.Information("* Process Exception");
				this.logService.Exception(ex);
				this.DoProcessError(request, ex);
			}
		}

		/// <summary>
		/// Send a specified request
		/// </summary>
		/// <param name="request">Request to send</param>
		public void Send(s2sMessage request)
		{
			try
			{
				this.DoSend(request);
			}
			catch (Exception ex)
			{
				this.logService.Information("* Send Exception");
				this.logService.Exception(ex);
				this.DoProcessError(request, ex);
			}
		}

		#endregion

		protected abstract string DoReceive(string request);
		protected abstract void DoProcess(s2sMessage request);
		protected abstract void DoProcessError(s2sMessage request, Exception ex);
		protected abstract void DoSend(s2sMessage request);
		protected abstract string SendAck(s2sMessage sourceRequest);
		protected abstract string SendAck(Exception ex);
	}
}