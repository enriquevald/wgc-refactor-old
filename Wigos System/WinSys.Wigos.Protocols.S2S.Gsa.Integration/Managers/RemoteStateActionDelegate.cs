﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Delegate to perform actions under lock context to the specified system
	/// </summary>
	/// <param name="state">Correspondent system state</param>
	public delegate void RemoteStateActionDelegate(State state);
}