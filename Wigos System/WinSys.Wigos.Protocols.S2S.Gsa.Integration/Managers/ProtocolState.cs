﻿using System.Collections.Generic;

using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Logic of ProtocolState to maintenance Configuration
	/// </summary>
	public class ProtocolState : IProtocolState
	{
		private const string AddressSeparatorToken = ":";

		private readonly string localEndpoint;
		private readonly Dictionary<string, State> remoteStatesDictionary;

		public string LocalEndpoint
		{
			get
			{
				return this.localEndpoint;
			}
		}

		public ProtocolState()
		{
			this.remoteStatesDictionary = new Dictionary<string, State>();

			this.localEndpoint = Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.Ip + ProtocolState.AddressSeparatorToken + Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.Port;
		}

		public IEnumerable<string> GetAllRemotes()
		{
			IEnumerable<string> result;
			lock (this.remoteStatesDictionary)
			{
				result = this.remoteStatesDictionary.Keys;
			}

			return result;
		}

		public bool RemoteStateAction(string remoteSystem, RemoteStateActionDelegate remoteStateActionDelegate)
		{
			bool isFound = false;
			if (!string.IsNullOrEmpty(remoteSystem))
			{
				State remoteState = null;

				lock (this.remoteStatesDictionary)
				{
					if (this.remoteStatesDictionary.ContainsKey(remoteSystem))
					{
						isFound = true;
						remoteState = this.remoteStatesDictionary[remoteSystem];
					}
				}

				if (isFound)
				{
					lock (remoteState)
					{
						remoteStateActionDelegate(remoteState);
					}
				}
			}

			return isFound;
		}

		public void SetRemoteState(string remoteSystem, State state)
		{
			lock (this.remoteStatesDictionary)
			{
				if (this.remoteStatesDictionary.ContainsKey(remoteSystem))
				{
					lock (this.remoteStatesDictionary[remoteSystem])
					{
						this.remoteStatesDictionary[remoteSystem] = state;
					}
				}
				else
				{
					this.remoteStatesDictionary.Add(remoteSystem, state);
				}
			}
		}

		public void RemoveRemoteState(string remoteSystem)
		{
			lock (this.remoteStatesDictionary)
			{
				if (this.remoteStatesDictionary.ContainsKey(remoteSystem))
				{
					lock (this.remoteStatesDictionary[remoteSystem])
					{
						this.remoteStatesDictionary.Remove(remoteSystem);
					}
				}
			}
		}	
	}
}