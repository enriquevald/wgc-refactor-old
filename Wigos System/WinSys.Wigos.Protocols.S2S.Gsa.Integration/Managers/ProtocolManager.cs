﻿using System;
using System.Linq;
using System.Xml.Linq;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	// TODO: Refactor. Each protocol method or action must be implemented in separated logical class (+ KeepAlive, global s2SMessageFactory call to s2SMessageBuilder).

	/// <summary>
	/// Management of the actions to be carried out of the GSA S2S protocol 
	/// </summary>
	public class ProtocolManager : BaseProtocolManager, IDisposable
	{
		private bool isDisposed;	// Track whether Dispose has been called.

		private readonly object keepAliveTaskLock;
		private System.Threading.Tasks.Task keepAliveTask;

		private readonly ISyntaxValidator syntaxValidator;
		private readonly IS2SMessageContentValidator s2SMessageContentValidator;
		private readonly IErrorMessageFactory errorMessageFactory;
		private readonly IXmlSerializerService xmlSerializerService;
		private readonly ICommandFactory<CommandInfo> commandFactory;
		private readonly IServiceClientFactory serviceClientFactory;
		private readonly IProtocolState protocolState;
		private readonly ILogCommandService logCommandService;

		public ProtocolManager(
			ISyntaxValidator syntaxValidator,
			IS2SMessageContentValidator s2SMessageContentValidator,
			IErrorMessageFactory errorMessageFactory,
			IXmlSerializerService xmlSerializerService,
			ICommandFactory<CommandInfo> commandFactory,
			IServiceClientFactory serviceClientFactory,
			IProtocolState protocolState,
			ILogCommandService logCommandService,
			ILogService logService)
			: base(logService)
		{
			this.syntaxValidator = syntaxValidator;
			this.s2SMessageContentValidator = s2SMessageContentValidator;
			this.errorMessageFactory = errorMessageFactory;
			this.xmlSerializerService = xmlSerializerService;
			this.commandFactory = commandFactory;
			this.serviceClientFactory = serviceClientFactory;
			this.protocolState = protocolState;
			this.logCommandService = logCommandService;

			this.isDisposed = false;
			this.keepAliveTask = null;
			this.keepAliveTaskLock = new object();

			this.InitializeKeepAliveTask();
		}

		/// <summary>
		/// Initialize in safe mode the task for send keepAlive commands
		/// </summary>
		private void InitializeKeepAliveTask()
		{
			lock (this.keepAliveTaskLock)
			{
				if (this.keepAliveTask == null)
				{
					this.keepAliveTask = System.Threading.Tasks.Task.Factory.StartNew(this.KeepAliveTask);
				}
			}
		}

		/// <summary>
		/// Performs the send of keepAlive commands for the registered remote hosts
		/// </summary>
		private void KeepAliveTask()
		{
			string remoteEndPoint = null;
			RemoteStateActionDelegate getRemoteEndPoint = (State state) => { remoteEndPoint = state.RemoteEndpoint; };

			while (true)
			{
				foreach (string remote in this.protocolState.GetAllRemotes())
				{
					this.protocolState.RemoteStateAction(remote, getRemoteEndPoint);

					if (!string.IsNullOrEmpty(remoteEndPoint))
					{
						keepAlive keepAlive = null;
						try
						{
							s2sMessage keepAliveMessage = this.CreateKeepAliveMessage(remote);
							keepAlive = (keepAlive)keepAliveMessage.Items.OfType<s2sBody>().First().Items.OfType<communications>().First().Item;
							this.logCommandService.Information(keepAlive, "KeepAliveTask");
							this.logService.Information(this.xmlSerializerService.Serialize(keepAliveMessage));

							this.serviceClientFactory.Create(remote).S2SMessagePostOperation(keepAliveMessage);
						}
						catch (Exception ex)
						{
							if (keepAlive != null)
							{
								this.logCommandService.Exception(keepAlive, "KeepAliveTask", ex);
							}
							else
							{
								this.logService.Exception("KeepAliveTask", ex);
							}
						}
					}
				}

				System.Threading.Thread.Sleep(Configuration.GeneralParams.Gsa.KeepAliveCommandInterval);
			}
		}

		/// <summary>
		/// Creates keepAlive message for a specific remote host
		/// </summary>
		/// <param name="toSystem">Remote host to which the message will be sent</param>
		/// <returns>Created keepAlive message</returns>
		private s2sMessage CreateKeepAliveMessage(string toSystem)
		{
			IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
			communications communicationsData = new communications
												{
													sessionTypeSpecified = false,
													dateTime = DateTime.Now,
													Item = new keepAlive()
												};
			s2sHeader header = new s2sHeader();
			RemoteStateActionDelegate setHeaderInfo = (State state) =>
			{
				communicationsData.propertyId = state.PropertyId;
				header.fromSystem = state.ToSystem;
				header.toSystem = state.FromSystem;
			};
			this.protocolState.RemoteStateAction(
				toSystem,
				setHeaderInfo);
			header.dateTimeSent = DateTime.Now;

			s2SMessageBuilder.Add(header);
			s2SMessageBuilder.Add(new s2sBody { Items = new object[] { communicationsData } });

			return s2SMessageBuilder.Build();
		}

		/// <summary>
		/// Performs the send of commsOffLine command to all registered remote hosts
		/// </summary>
		private void CommsOffLineMessageSender()
		{
			string remoteEndPoint = null;
			RemoteStateActionDelegate getRemoteEndPoint = (State state) => { remoteEndPoint = state.RemoteEndpoint; };

			foreach (string remote in this.protocolState.GetAllRemotes())
			{
				this.protocolState.RemoteStateAction(remote, getRemoteEndPoint);

				if (!string.IsNullOrEmpty(remoteEndPoint))
				{
					commsOffLine commsOffLine = null;
					try
					{
						s2sMessage commsOffLineMessage = this.CreateCommsOffLineMessage(remote);
						commsOffLine = (commsOffLine)commsOffLineMessage.Items.OfType<s2sBody>().First().Items.OfType<communications>().First().Item;
						this.logCommandService.Information(commsOffLine, "CommsOffLineMessageSender");
						this.logService.Information(this.xmlSerializerService.Serialize(commsOffLineMessage));

						this.serviceClientFactory.Create(remote).S2SMessagePostOperation(commsOffLineMessage);
					}
					catch (Exception ex)
					{
						if (commsOffLine != null)
						{
							this.logCommandService.Exception(commsOffLine, "CommsOffLineMessageSender", ex);
						}
						else
						{
							this.logService.Exception("CommsOffLineMessageSender", ex);
						}
					}
				}
			}
		}

		/// <summary>
		/// Creates commsOffLine message for a specific remote host
		/// </summary>
		/// <param name="toSystem">Remote host to which the message will be sent</param>
		/// <returns>Created keepAlive message</returns>
		private s2sMessage CreateCommsOffLineMessage(string toSystem)
		{
			IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
			communications communicationsData = new communications
												{
													sessionTypeSpecified = false,
													dateTime = DateTime.Now,
													Item = new commsOffLine()
												};
			s2sHeader header = new s2sHeader();
			RemoteStateActionDelegate setHeaderInfo = (State state) =>
			{
				communicationsData.propertyId = state.PropertyId;
				header.fromSystem = toSystem;
				header.toSystem = state.FromSystem;
			};
			this.protocolState.RemoteStateAction(
				toSystem,
				setHeaderInfo);
			header.dateTimeSent = DateTime.Now;

			s2SMessageBuilder.Add(header);
			s2SMessageBuilder.Add(new s2sBody { Items = new object[] { communicationsData } });

			return s2SMessageBuilder.Build();
		}

		#region BaseProtocolManager

		protected override string DoReceive(string request)
		{
			this.logService.Information("ProtocolManager.Receive - Request:" + request);
			this.syntaxValidator.Validate(this.ParseRequest(request));
			s2sMessage typedRequest = this.xmlSerializerService.Deserialize<s2sMessage>(request);
			this.s2SMessageContentValidator.Validate(typedRequest);

			s2sBody requestCommands = typedRequest.Items
				.OfType<s2sBody>()
				.First();
			foreach (object command in requestCommands.Items)
			{
				this.logCommandService.Information(command, "ProtocolManager.Receive");
			}

			string response = this.SendAck(typedRequest);

			System.Threading.Tasks.Task.Factory.StartNew(() => this.Process(this.xmlSerializerService.Deserialize<s2sMessage>(request)));

			return response;
		}

		protected override void DoProcess(s2sMessage request)
		{
			s2sHeader requestHeader = request.Items
				.OfType<s2sHeader>()
				.First();
			s2sBody requestBody = request.Items
				.OfType<s2sBody>()
				.First();
			foreach (object requestBodyItem in requestBody.Items)
			{
				if (this.MustProcessCommand(
					requestHeader.fromSystem,
					(baseClass)requestBodyItem))
				{
					ICommand command = this.commandFactory.Create(new CommandInfo { Header = requestHeader, Command = requestBodyItem });

					if (command != null)
					{
						this.logCommandService.Information(requestBodyItem, "ProtocolManager.Process");

						try
						{
							command.Execute();
						}
						catch (Exception ex)
						{
							this.logCommandService.Exception(requestBodyItem, "ProtocolManager.Process", ex);
							throw;
						}

						ICommandWithReturnValue<object> commandWithReturnValue = command as ICommandWithReturnValue<object>;
						if (commandWithReturnValue != null)
						{
							GetValidationIdsCommand getValidationIdsCommand = command as GetValidationIdsCommand;
							if (getValidationIdsCommand != null)
							{
								this.logCommandService.Information(requestBodyItem, "ProtocolManager.Process.Returned");
							}

							IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();

							s2sHeader header = new s2sHeader();
							RemoteStateActionDelegate setHeaderInfo = (State state) =>
							{
								header.fromSystem = state.ToSystem;
								header.toSystem = state.FromSystem;
							};
							this.protocolState.RemoteStateAction(requestHeader.fromSystem, setHeaderInfo);
							header.dateTimeSent = DateTime.Now;
							s2SMessageBuilder.Add(header);

							s2sBody body = new s2sBody { Items = new[] { commandWithReturnValue.GetValue() } };
							this.ParseClientIdFromReturnedCommands(body.Items);
							s2SMessageBuilder.Add(body);

							RemoteStateActionDelegate setCommandData = (State state) =>
							{
								foreach (baseClass item in body.Items)
								{
									item.sessionIdSpecified = true;
									item.sessionId = ((baseClass)requestBodyItem).sessionId;
									item.dateTime = DateTime.Now;
								}
							};
							this.protocolState.RemoteStateAction(requestHeader.fromSystem, setCommandData);

							this.Send(s2SMessageBuilder.Build());
						}
					}
					else
					{
						throw new CommsException(requestBodyItem, MessageLevel.CommandNotSupported);
					}
				}
				else
				{
					this.logCommandService.Warning(requestBodyItem, "ProtocolManager.Process ~ Was ignored");
				}
			}
		}

		protected override void DoProcessError(s2sMessage request, Exception ex)
		{
			this.DoSend(this.errorMessageFactory.Create(new CommsException(
				sourceContainer: request,
				sourceItem: ex,
				message: ex.Message,
				innerException: ex)));
		}

		protected override void DoSend(s2sMessage request)
		{
			System.Threading.Tasks.Task.Factory.StartNew(() => this.RetrySenderTask(request));
		}

		protected override string SendAck(s2sMessage sourceRequest)
		{
			s2sHeader requestHeader = sourceRequest.Items
				.OfType<s2sHeader>()
				.First();
			IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
			IS2SAckBuilder s2SAckBuilder = new S2SAckBuilder();
			s2SAckBuilder.SetFromSystem(requestHeader.toSystem);
			s2SAckBuilder.SetToSystem(requestHeader.fromSystem);
			s2SAckBuilder.SetMessageId(requestHeader.messageId);
			s2SAckBuilder.SetDateTimeSent(DateTime.Now);
			s2SMessageBuilder.Add(s2SAckBuilder.Build());

			string ackMessage = this.xmlSerializerService.Serialize(s2SMessageBuilder.Build());

			this.logService.Information("ProtocolManager.SendAck" + this.xmlSerializerService.Serialize(ackMessage));
			s2sBody requestBody = sourceRequest.Items
				.OfType<s2sBody>()
				.First();
			foreach (object command in requestBody.Items)
			{
				this.logCommandService.Information(command, "ProtocolManager.SendAck");
			}

			return ackMessage;
		}

		protected override string SendAck(Exception ex)
		{
			this.logService.Exception("ProtocolManager.SendAck", ex);

			return this.xmlSerializerService.Serialize(this.errorMessageFactory.Create(ex));
		}

		#endregion

		/// <summary>
		/// Determines if the specified command must be or not processed
		/// </summary>
		/// <param name="remoteSystem">Source host of the message</param>
		/// <param name="commandData">Command to analyze</param>
		/// <returns>true if the command must be processed, else false</returns>
		private bool MustProcessCommand(string remoteSystem, baseClass commandData)
		{
			return commandData is communications
			       || (this.CheckAndSetCommandId(remoteSystem, commandData) && commandData.dateTime.AddMilliseconds(commandData.timeToLive) >= DateTime.Now);
		}

		/// <summary>
		/// Check if can set the current commandId value for the source host, and set it
		/// </summary>
		/// <param name="remoteSystem">Source host of the message</param>
		/// <param name="commandData">Command to analyze</param>
		/// <returns>true if the commandId could be processed (remoteSystem found), else false</returns>
		/// <remarks>When the returned value is false, determines that the system must not process messages from unknown hosts</remarks>
		private bool CheckAndSetCommandId(string remoteSystem, baseClass commandData)
		{
			bool isOk = false;

			RemoteStateActionDelegate checkAndSetInboundCommandId = (State state) =>
			{
				if (state.InboundCommandId < commandData.commandId)
				{
					state.InboundCommandId = commandData.commandId;
					isOk = true;
				}
			};

			this.protocolState.RemoteStateAction(
				remoteSystem,
				checkAndSetInboundCommandId);

			return isOk;
		}

		/// <summary>
		/// Performs the management of message sending retries
		/// </summary>
		/// <param name="request">Message to send</param>
		private void RetrySenderTask(s2sMessage request)
		{
			this.logService.Information("ProtocolManager.Send - Request:" + this.xmlSerializerService.Serialize(request));

			s2sHeader requestHeader = request.Items
				.OfType<s2sHeader>()
				.First();
			s2sBody requestBody = request.Items
				.OfType<s2sBody>()
				.First();

			bool mustSend = true;
			while (mustSend)
			{
				long retryCounter = this.GetRetryCounter(requestBody);
				foreach (object command in requestBody.Items)
				{
					this.logCommandService.Information(command, string.Format("ProtocolManager.Send - Retry: {0}", retryCounter));
				}

				s2sMessage response;
				try
				{
					requestHeader.dateTimeSent = DateTime.Now;
					response = this.serviceClientFactory.Create(requestHeader.toSystem).S2SMessagePostOperation(request);
				}
				catch (Exception ex)
				{
					response = null;
					foreach (object command in requestBody.Items)
					{
						this.logCommandService.Exception(command, string.Format("ProtocolManager.Send - Retry: {0}", retryCounter), ex);
					}
				}

				if (response != null)
				{
					mustSend = false;
					s2sAck responseAck = response.Items
						.OfType<s2sAck>()
						.FirstOrDefault();
					if (responseAck == null)
					{
						foreach (object command in requestBody.Items)
						{
							this.logCommandService.Error(command, "ProtocolManager.Send - No Ack received");
						}

						this.DoSend(this.errorMessageFactory.Create(new CommsException(request, new CommsException(null, MessageLevel.IncompleteOrMalformedXml))));
					}
					else
					{
						this.logService.Information("ProtocolManager.Send.Ack: " + this.xmlSerializerService.Serialize(response));
						if (responseAck.messageId != requestHeader.messageId)
						{
							foreach (object command in requestBody.Items)
							{
								this.logCommandService.Error(
									command, 
									string.Format(
										"ProtocolManager.Send.Ack - MessageId does not mach: Header messageId={0}, Ack messageId={1}.}", 
										requestHeader.messageId,
										responseAck.messageId));
							}

							this.DoSend(this.errorMessageFactory.Create(
								new CommsException(
									request,
									new CommsException(
										responseAck,
										MessageLevel.InvalidMessageIdentifier))));
						}
						else if (!string.IsNullOrEmpty(responseAck.errorCode) 
						         && responseAck.errorCode != MessageLevel.MessageProcessingServicesUnavailable)
						{
							foreach (object command in requestBody.Items)
							{
								this.logCommandService.Error(command, string.Format("ProtocolManager.Send.Ack - ErrorCode: {0}", responseAck.errorCode));
							}
						}
						else
						{
							foreach (object command in requestBody.Items)
							{
								this.logCommandService.Information(command, "ProtocolManager.Send.Ack");
							}
						}
					}
				}

				if (mustSend)
				{
					if (retryCounter < int.MaxValue)
					{
						this.IncreaseRetryCounter(requestBody);
						System.Threading.Thread.Sleep(Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.SendRetryInterval);
						requestHeader.dateTimeSent = DateTime.Now;
						RemoteStateActionDelegate setMessageInfo = (State state) =>
						{
							foreach (baseClass command in requestBody.Items)
							{
								command.dateTime = DateTime.Now;
							}
						};
						this.protocolState.RemoteStateAction(
							requestHeader.fromSystem,
							setMessageInfo);
					}
					else
					{
						mustSend = false;
					}
				}
			}
		}

		/// <summary>
		/// Performs the increase of retry counter for all of the commands contained in the specified body
		/// </summary>
		/// <param name="body">Body to analyze</param>
		private void IncreaseRetryCounter(s2sBody body)
		{
			foreach (baseClass item in body.Items)
			{
				if (item.sessionRetrySpecified)
				{
					item.sessionRetry++;
				}
				else
				{
					item.sessionRetrySpecified = true;
					item.sessionRetry = 1;
				}
			}
		}

		/// <summary>
		/// Get the current retry counter for a specified body message
		/// </summary>
		/// <param name="body">Body to analyze</param>
		/// <returns>Current retry counter</returns>
		private long GetRetryCounter(s2sBody body)
		{
			long result = 0;

			baseClass item = body.Items
				.OfType<baseClass>()
				.FirstOrDefault();
			if (item != null)
			{
				result = item.sessionRetry;
			}

			return result;
		}

		/// <summary>
		/// Parse the specified serialized request, to a XML formatted document
		/// </summary>
		/// <param name="request">Request to parse</param>
		/// <returns>Formatted XML document</returns>
		private XDocument ParseRequest(string request)
		{
			XDocument result;

			try
			{
				result = XDocument.Parse(request);
			}
			catch (Exception ex)
			{
				throw new SyntaxException(MessageLevel.IncompleteOrMalformedXml, ex);
			}

			return result;
		}

		private void ParseClientIdFromReturnedCommands(object[] commands)
		{
			foreach (object command in commands)
			{
				voucher voucherCommand = command as voucher;
				if (voucherCommand != null)
				{
					voucherConfig voucherConfigCommand = voucherCommand.Item as voucherConfig;
					if (voucherConfigCommand != null)
					{
						voucherConfigCommand.clientId = this.ParseMACIdToS2SNormalization(voucherConfigCommand.clientId);
					}
					else
					{
						validationIdList validationIdListCommand = voucherCommand.Item as validationIdList;
						if (validationIdListCommand != null)
						{
							validationIdListCommand.clientId = this.ParseMACIdToS2SNormalization(validationIdListCommand.clientId);
						}
						else
						{
							ackVoucher ackVoucherCommand = voucherCommand.Item as ackVoucher;
							if (ackVoucherCommand != null)
							{
								ackVoucherCommand.clientId = this.ParseMACIdToS2SNormalization(ackVoucherCommand.clientId);
							}
							else
							{
								authorizeVoucher authorizeVoucherCommand = voucherCommand.Item as authorizeVoucher;
								if (authorizeVoucherCommand != null)
								{
									authorizeVoucherCommand.clientId = this.ParseMACIdToS2SNormalization(authorizeVoucherCommand.clientId);
								}
							}
						}
					}
				}
			}
		}

		private string ParseMACIdToS2SNormalization(string externalTerminalId)
		{
			string result = externalTerminalId;

			const string macString = "MAC ";
			const string separatorToken = "_";

			if (externalTerminalId.ToUpper().StartsWith(macString))
			{
				result = externalTerminalId.Replace(" ", separatorToken);
			}

			return result;
		}

		#region IDisposable Members

		public void Dispose(Boolean isDisposing)
		{
			if (!this.isDisposed)
			{
				if (!isDisposing)
				{
					this.CommsOffLineMessageSender();
				}
				// Note disposing has been done.
				this.isDisposed = true;
			}
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		~ProtocolManager()
		{
			this.Dispose(false);
		}
		#endregion
	}
}