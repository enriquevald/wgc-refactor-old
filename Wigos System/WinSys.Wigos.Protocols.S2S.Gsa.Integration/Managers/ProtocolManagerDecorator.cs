﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	public abstract class ProtocolManagerDecorator : IProtocolManager
	{
		protected readonly IProtocolManager Component;

		protected ProtocolManagerDecorator(IProtocolManager component)
		{
			this.Component = component;
		}

		#region GsaS2SProtocolManagerDecorator

		public abstract string Receive(string request);

		public abstract void Process(s2sMessage request);

		public abstract void Send(s2sMessage request);

		#endregion
	}
}