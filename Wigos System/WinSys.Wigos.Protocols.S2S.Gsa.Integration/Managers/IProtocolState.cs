﻿using System.Collections.Generic;

using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Implements ProtocolState
	/// </summary>
	public interface IProtocolState
	{
		/// <summary>
		/// Execute actions under thread safe state
		/// </summary>
		/// <param name="remoteSystem">system to check</param>
		/// <param name="remoteStateActionDelegate">delegate to perform actions in remote state</param>
		/// <returns>True if remote founded and therefore excecuted</returns>
		bool RemoteStateAction(string remoteSystem, RemoteStateActionDelegate remoteStateActionDelegate);

		/// <summary>
		/// Get and return a list with all of remote systems added into internal dictionary
		/// </summary>
		/// <returns>List of all remote systems</returns>
		IEnumerable<string> GetAllRemotes();

		/// <summary>
		/// Set a specific system state to the internal dictionary
		/// </summary>
		/// <param name="remoteSystem">system to set state</param>
		/// <param name="state">state to set</param>
		/// <remarks>If it does not exist, a new one will be created. If remote already added, current state are replaced by the specified</remarks>
		void SetRemoteState(string remoteSystem, State state);

		/// <summary>
		/// Get the string of local address url
		/// </summary>
		string LocalEndpoint { get; }

		/// <summary>
		/// Removes a existent system from internal dictionary
		/// </summary>
		/// <param name="remoteSystem">system to remove</param>
		void RemoveRemoteState(string remoteSystem);
	}
}