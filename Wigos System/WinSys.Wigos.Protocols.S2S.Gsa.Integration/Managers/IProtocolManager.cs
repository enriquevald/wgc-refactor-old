﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Implements methods for the management of GSA S2S protocol 
	/// </summary>
	public interface IProtocolManager : IReceive, IProcess, ISend
	{
	}
}