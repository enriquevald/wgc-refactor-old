﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Implements the method of sending protocol messages
	/// </summary>
	public interface ISend
	{
		/// <summary>
		/// Send a specified request
		/// </summary>
		/// <param name="request">Request to send</param>
		void Send(s2sMessage request);
	}
}