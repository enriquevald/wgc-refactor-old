﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers
{
	/// <summary>
	/// Implements the protocol message processing method
	/// </summary>
	public interface IProcess
	{
		/// <summary>
		/// Manage the process of the request
		/// </summary>
		/// <param name="request">Request to process</param>
		void Process(s2sMessage request);
	}
}