﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference.IS2SGsaService")]
    public interface IS2SGsaService {
        
        // CODEGEN: Generating message contract since the operation S2SMessagePostOperation is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebsite/services/SlotSystemHost", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/S2SMessagePostOperationResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse S2SMessagePostOperation(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebsite/services/SlotSystemHost", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/S2SMessagePostOperationResponse")]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> S2SMessagePostOperationAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest request);
        
        // CODEGEN: Generating message contract since the operation SendS2SGZipPayloadMessage is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebSite/services/SendS2SGZipPayloadMessage" +
            "", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/SendS2SGZipPayloadMessageResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse SendS2SGZipPayloadMessage(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebSite/services/SendS2SGZipPayloadMessage" +
            "", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/SendS2SGZipPayloadMessageResponse")]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> SendS2SGZipPayloadMessageAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest request);
        
        // CODEGEN: Generating message contract since the operation getTransportOptions is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebSite/services/getTransportOptions", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/getTransportOptionsResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse getTransportOptions(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.gamingstandards.com:8080/S2SWebSite/services/getTransportOptions", ReplyAction="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl/S2SMessagePos" +
            "tPortType/getTransportOptionsResponse")]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse> getTransportOptionsAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2634.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl")]
    public partial class RequestType : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string requestField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string request {
            get {
                return this.requestField;
            }
            set {
                this.requestField = value;
                this.RaisePropertyChanged("request");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2634.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl")]
    public partial class TransportOptions : object, System.ComponentModel.INotifyPropertyChanged {
        
        private TransportOptionType optionField;
        
        private TransportOptionType[] option1Field;
        
        private string versionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public TransportOptionType option {
            get {
                return this.optionField;
            }
            set {
                this.optionField = value;
                this.RaisePropertyChanged("option");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("option", Order=1)]
        public TransportOptionType[] option1 {
            get {
                return this.option1Field;
            }
            set {
                this.option1Field = value;
                this.RaisePropertyChanged("option1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.RaisePropertyChanged("version");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2634.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl")]
    public enum TransportOptionType {
        
        /// <remarks/>
        NO_GZIP,
        
        /// <remarks/>
        GZIP_PAYLOAD,
        
        /// <remarks/>
        GZIP_IN_HTTP_STACK,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2634.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl")]
    public partial class ResponseType : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string responseField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string response {
            get {
                return this.responseField;
            }
            set {
                this.responseField = value;
                this.RaisePropertyChanged("response");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class S2SMessagePostOperationRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl", Order=0)]
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.RequestType s2sRequest;
        
        public S2SMessagePostOperationRequest() {
        }
        
        public S2SMessagePostOperationRequest(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.RequestType s2sRequest) {
            this.s2sRequest = s2sRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class S2SMessagePostOperationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl", Order=0)]
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.ResponseType s2sResponse;
        
        public S2SMessagePostOperationResponse() {
        }
        
        public S2SMessagePostOperationResponse(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.ResponseType s2sResponse) {
            this.s2sResponse = s2sResponse;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SendS2SGZipPayloadMessageRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] s2sGZipPayloadRequest;
        
        public SendS2SGZipPayloadMessageRequest() {
        }
        
        public SendS2SGZipPayloadMessageRequest(byte[] s2sGZipPayloadRequest) {
            this.s2sGZipPayloadRequest = s2sGZipPayloadRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getTransportOptionsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl", Order=0)]
        public string s2sTransportReqVersion;
        
        public getTransportOptionsRequest() {
        }
        
        public getTransportOptionsRequest(string s2sTransportReqVersion) {
            this.s2sTransportReqVersion = s2sTransportReqVersion;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getTransportOptionsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.gamingstandards.com/s2s/wsdl/v1.1/S2SMessageService.wsdl", Order=0)]
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.TransportOptions s2sTransportResp;
        
        public getTransportOptionsResponse() {
        }
        
        public getTransportOptionsResponse(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.TransportOptions s2sTransportResp) {
            this.s2sTransportResp = s2sTransportResp;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IS2SGsaServiceChannel : WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class S2SGsaServiceClient : System.ServiceModel.ClientBase<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService>, WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService {
        
        public S2SGsaServiceClient() {
        }
        
        public S2SGsaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public S2SGsaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public S2SGsaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public S2SGsaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.S2SMessagePostOperation(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest request) {
            return base.Channel.S2SMessagePostOperation(request);
        }
        
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.ResponseType S2SMessagePostOperation(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.RequestType s2sRequest) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest();
            inValue.s2sRequest = s2sRequest;
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse retVal = ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).S2SMessagePostOperation(inValue);
            return retVal.s2sResponse;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.S2SMessagePostOperationAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest request) {
            return base.Channel.S2SMessagePostOperationAsync(request);
        }
        
        public System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> S2SMessagePostOperationAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.RequestType s2sRequest) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationRequest();
            inValue.s2sRequest = s2sRequest;
            return ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).S2SMessagePostOperationAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.SendS2SGZipPayloadMessage(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest request) {
            return base.Channel.SendS2SGZipPayloadMessage(request);
        }
        
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.ResponseType SendS2SGZipPayloadMessage(byte[] s2sGZipPayloadRequest) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest();
            inValue.s2sGZipPayloadRequest = s2sGZipPayloadRequest;
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse retVal = ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).SendS2SGZipPayloadMessage(inValue);
            return retVal.s2sResponse;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.SendS2SGZipPayloadMessageAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest request) {
            return base.Channel.SendS2SGZipPayloadMessageAsync(request);
        }
        
        public System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.S2SMessagePostOperationResponse> SendS2SGZipPayloadMessageAsync(byte[] s2sGZipPayloadRequest) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.SendS2SGZipPayloadMessageRequest();
            inValue.s2sGZipPayloadRequest = s2sGZipPayloadRequest;
            return ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).SendS2SGZipPayloadMessageAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.getTransportOptions(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest request) {
            return base.Channel.getTransportOptions(request);
        }
        
        public WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.TransportOptions getTransportOptions(string s2sTransportReqVersion) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest();
            inValue.s2sTransportReqVersion = s2sTransportReqVersion;
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse retVal = ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).getTransportOptions(inValue);
            return retVal.s2sTransportResp;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse> WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService.getTransportOptionsAsync(WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest request) {
            return base.Channel.getTransportOptionsAsync(request);
        }
        
        public System.Threading.Tasks.Task<WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsResponse> getTransportOptionsAsync(string s2sTransportReqVersion) {
            WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest inValue = new WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.getTransportOptionsRequest();
            inValue.s2sTransportReqVersion = s2sTransportReqVersion;
            return ((WinSys.Wigos.Protocols.S2S.Gsa.Integration.ServiceReference.IS2SGsaService)(this)).getTransportOptionsAsync(inValue);
        }
    }
}
