﻿using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories
{
	/// <summary>
	/// Implements ServiceClientFactory
	/// </summary>
	public interface IServiceClientFactory
	{
		IServiceClient Create(string remoteSystem);
	}
}