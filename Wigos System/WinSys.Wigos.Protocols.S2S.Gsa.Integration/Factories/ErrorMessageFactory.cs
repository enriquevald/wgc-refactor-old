﻿using System;
using System.Linq;
using System.Resources;

using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.TypedResources.Classes;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories
{
	/// <summary>
	/// Logic of ErrorFactory
	/// </summary>
	public class ErrorMessageFactory : IErrorMessageFactory
	{
		private readonly ResourceManager errorResourceManager;

		private readonly IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter;

		public ErrorMessageFactory(IAdapter<BusinessExceptionTicketItem, string> businessVoucherClassToErrorCodeAdapter)
		{
			this.businessVoucherClassToErrorCodeAdapter = businessVoucherClassToErrorCodeAdapter;

			this.errorResourceManager = new ResourceManager(typeof(TypedResources.S2SErrors));
		}

		#region IErrorFactory

		/// <summary>
		/// Generate a specified s2sAck message based on passed exception
		/// </summary>
		/// <param name="exception">Exception to parse</param>
		/// <returns>Filled s2sMessage with return command type. According to the exception, it will return more or less completed object</returns>
		/// <remarks>
		/// The following exceptions are recognized:
		///		* SyntaxException
		///		* ContentException
		///		* Exception as generic system errors
		/// 
		/// Note: Every not recognized exception are treated like a generic
		/// </remarks>
		public s2sMessage Create(Exception exception)
		{
			IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
			IS2SAckBuilder s2SAckBuilder = new S2SAckBuilder();
			s2SAckBuilder.SetDateTimeSent(DateTime.Now);

			SyntaxException syntaxException = exception as SyntaxException;
			if (syntaxException != null)
			{
				s2SAckBuilder.SetErrorCode(MessageLevel.MessageProcessingServicesUnavailable);
				s2SAckBuilder.SetErrorText(this.errorResourceManager.GetString(MessageLevel.IncompleteOrMalformedXml));

				s2SMessageBuilder.Add(s2SAckBuilder.Build());
			}
			else
			{
				ContentException contentException = exception as ContentException;
				if (contentException != null)
				{
					s2sHeader s2SHeader = contentException.SourceContainer as s2sHeader;
					if (s2SHeader != null)
					{
						s2SAckBuilder.SetToSystem(s2SHeader.fromSystem);
						s2SAckBuilder.SetFromSystem(s2SHeader.toSystem);
						s2SAckBuilder.SetMessageId(s2SHeader.messageId);

						if (contentException.SourceItem == S2SHeader.ToSystem)
						{
							s2SAckBuilder.SetErrorCode(MessageLevel.InvalidMessageRecipientSpecified);
							s2SAckBuilder.SetErrorText(this.errorResourceManager.GetString(MessageLevel.InvalidMessageRecipientSpecified));
						}
						else if (contentException.SourceItem == S2SHeader.FromSystem)
						{
							s2SAckBuilder.SetErrorCode(MessageLevel.InvalidMessageOriginatorSpecified);
							s2SAckBuilder.SetErrorText(this.errorResourceManager.GetString(MessageLevel.InvalidMessageOriginatorSpecified));
						}

						s2SMessageBuilder.Add(s2SAckBuilder.Build());
					}
				}
				else
				{
					CommsException commsException = exception as CommsException;
					if (commsException != null)
					{
						s2sMessage s2SMessage = commsException.SourceContainer as s2sMessage;
						s2sHeader s2SMessageHeader = null;
						if (s2SMessage != null)
						{
							s2SMessageHeader = s2SMessage.Items
								.OfType<s2sHeader>()
								.FirstOrDefault();
						}

						string errorCode;
						CommsException commsExceptionItem = commsException.SourceItem as CommsException;
						if (commsExceptionItem != null)
						{
							errorCode = MessageLevel.CommandProcessingServicesUnavailable;
							string includedErrorCode = commsExceptionItem.SourceItem as string;
							if (includedErrorCode != null)
							{
								errorCode = includedErrorCode;
							}
						}
						else
						{
							BusinessException businessException = commsException.SourceItem as BusinessException;
							errorCode = businessException != null ? this.GetBusinessErrorCode(businessException) : MessageLevel.MessageProcessingServicesUnavailable;
						}

						s2SMessage = this.CreateCommsErrorMessage(s2SMessageHeader, errorCode);

						s2SMessageBuilder.Add(s2SMessage.Items.OfType<s2sHeader>().First());
						s2SMessageBuilder.Add(s2SMessage.Items.OfType<s2sBody>().First());
					}
					else
					{
						s2SAckBuilder.SetErrorCode(MessageLevel.MessageProcessingServicesUnavailable);
						s2SAckBuilder.SetErrorText(this.errorResourceManager.GetString(MessageLevel.MessageProcessingServicesUnavailable));

						s2SMessageBuilder.Add(s2SAckBuilder.Build());
					}
				}
			}

			return s2SMessageBuilder.Build();
		}

		#endregion

		private s2sMessage CreateCommsErrorMessage(s2sHeader sourceRequestHeader, string errorcode)
		{
			IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
			communications communicationsData = new communications
			{
				sessionType = sessionType.response,
				sessionTypeSpecified = true,
				dateTime = DateTime.Now,
				
				Item = new commsError
				{
					channelType = S2SCommunications.CommsErrorChannelType,
					toSystem = this.GetFromSystemValue(sourceRequestHeader),
					fromSystem = this.GetToSystemValue(sourceRequestHeader),
					commandInError = 0,
					errorCode = errorcode,
					errorText = this.errorResourceManager.GetString(errorcode)
				}
			};
			s2sHeader requestHeader = new s2sHeader 
			                          {
				                          toSystem = this.GetFromSystemValue(sourceRequestHeader),
				                          fromSystem = this.GetToSystemValue(sourceRequestHeader),
				                          dateTimeSent = DateTime.Now
			                          };
			s2SMessageBuilder.Add(requestHeader);
			s2SMessageBuilder.Add(
				new s2sBody
				{
					Items = new object[] { communicationsData }
				});

			return s2SMessageBuilder.Build();
		}

		private string GetToSystemValue(s2sHeader sourceRequestHeader)
		{
			string result = S2SCommunications.CommsErrorToFromSystemIndecipherable;

			if (sourceRequestHeader != null 
			    && !string.IsNullOrEmpty(sourceRequestHeader.toSystem))
			{
				result = sourceRequestHeader.toSystem;
			}

			return result;
		}

		private string GetFromSystemValue(s2sHeader sourceRequestHeader)
		{
			string result = S2SCommunications.CommsErrorToFromSystemIndecipherable;

			if (sourceRequestHeader != null 
			    && !string.IsNullOrEmpty(sourceRequestHeader.fromSystem))
			{
				result = sourceRequestHeader.fromSystem;
			}

			return result;
		}

		private string GetBusinessErrorCode(BusinessException businessException)
		{
			string result = null;

			switch (businessException.SourceContainer)
			{
				case BusinessExceptionContainer.Ticket:
					result = this.businessVoucherClassToErrorCodeAdapter.Adapt((BusinessExceptionTicketItem)businessException.SourceItem);
					break;
				case BusinessExceptionContainer.Message:
					result = this.GetBusinessMessageErrorCode((BusinessExceptionMessageItem)businessException.SourceItem);
					break;
			}

			return result;
		}


		// TODO: Refactor. Make a separated converter class like BusinessVoucherClassToErrorConverter
		private string GetBusinessMessageErrorCode(BusinessExceptionMessageItem messageError)
		{
			string result = null;

			switch (messageError)
			{
				case BusinessExceptionMessageItem.MessageProcessingServicesUnavailable:
					result = MessageLevel.MessageProcessingServicesUnavailable;
					break;
			}

			return result;
		}
	}
}