﻿using Microsoft.Practices.Unity;

using WinSys.Wigos.Architecture.Patterns;
using WinSys.Wigos.Architecture.Patterns.Command;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Commands;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories
{
	/// <summary>
	/// Logic of CommandFactory
	/// </summary>
	public class CommandFactory : ICommandFactory<CommandInfo>
	{
		private readonly IUnityContainer container;
		private readonly IProtocolState protocolState;

		public CommandFactory(
			IUnityContainer container,
			IProtocolState protocolManager)
		{
			this.container = container;
			this.protocolState = protocolManager;
		}

		#region ICommandFactory

		public ICommand Create(CommandInfo type)
		{
			ICommand result = this.CreateVoucherCommand(type.Command as voucher);

			if (result == null)
			{
				result = this.CreateCommunicationsCommand(type.Header, type.Command as communications);
			}

			return result;
		}

		#endregion

		private ICommand CreateVoucherCommand(voucher voucherData)
		{
			ICommand result = null;

			if (voucherData != null && voucherData.Item != null)
			{
				if (voucherData.Item is getValidationIds)
				{
					result = new GetValidationIdsCommand(
						this.container.Resolve<ITicketService>(),
						this.container.Resolve<IConfigurationService>(), // TODO: Refactor. Remove from here. See GetValidationIdCommand.
						voucherData);
				}
				else if (voucherData.Item is issueVoucher)
				{
					result = new IssueVoucherCommand(
						this.container.Resolve<IIssueVoucherValidator>(),
						this.container.Resolve<ITicketService>(),
						this.container.Resolve<IAdapter<BusinessExceptionTicketItem, string>>(),
						voucherData);
				}
				else if (voucherData.Item is redeemVoucher)
				{
					result = new RedeemVoucherCommand(
						this.container.Resolve<ITicketService>(),
						this.container.Resolve<IAdapter<BusinessExceptionTicketItem, string>>(),
						voucherData);
				}
				else if (voucherData.Item is commitVoucher)
				{
					result = new CommitVoucherCommand(
						this.container.Resolve<ICommitVoucherValidator>(),
						this.container.Resolve<ITicketService>(),
						this.container.Resolve<IAdapter<BusinessExceptionTicketItem, string>>(),
						voucherData);
				}
				else if (voucherData.Item is getVoucherConfig)
				{
					result = new GetVoucherConfigCommand(
						this.container.Resolve<ITicketService>(), 
						voucherData);
				}
				else if (voucherData.Item is voucherConfig)
				{
					result = new VoucherConfigCommand(voucherData);
				}
				else if (voucherData.Item is validationIdList)
				{
					result = new ValidationIdListCommand(voucherData);
				}
				else if (voucherData.Item is ackVoucher)
				{
					result = new AckVoucherCommand(voucherData);
				}
				else if (voucherData.Item is authorizeVoucher)
				{
					result = new AuthorizeVoucherCommand(voucherData);
				}
				else if (voucherData.Item is event1)
				{
					result = new EventCommand(voucherData);
				}
			}

			return result;
		}

		private ICommand CreateCommunicationsCommand(s2sHeader header, communications communicationsData)
		{
			ICommand result = null;

			if (communicationsData != null && communicationsData.Item != null)
			{
				if (communicationsData.Item is keepAlive)
				{
					result = new KeepAliveCommand(communicationsData);
				}
				else if (communicationsData.Item is commsError)
				{
					result = new CommsErrorCommand(communicationsData);
				}
				else if (communicationsData.Item is commsOnLine)
				{
					result = new CommsOnLineCommand(
					this.protocolState,
					header,
					communicationsData);
				}
				else if (communicationsData.Item is commsOffLine)
				{
					result = new CommsOffLineCommand(
					this.protocolState,
					header,
					communicationsData);
				}
				else if (communicationsData.Item is commsRestored)
				{
					result = new CommsRestoredCommand(communicationsData);
				}
				else if (communicationsData.Item is commsLost)
				{
					result = new CommsLostCommand(communicationsData);
				}
			}

			return result;
		}
	}
}