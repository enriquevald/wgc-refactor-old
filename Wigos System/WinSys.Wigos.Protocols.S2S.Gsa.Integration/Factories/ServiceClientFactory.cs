﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

using Microsoft.Practices.Unity;

using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Bootstrapper;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories
{
	/// <summary>
	/// Implementation of ServiceClientFactory
	/// </summary>
	public class ServiceClientFactory : IServiceClientFactory
	{
		private const string HttpsScheme = "https";

		private readonly IUnityContainer container;

		private readonly IProtocolState protocolState;

		public ServiceClientFactory(IProtocolState protocolState)
		{
			this.protocolState = protocolState;

			this.container = new UnityContainer();
			Unity.Configure(this.container);
		}

		public IServiceClient Create(string remoteSystem)
		{
			string remoteAddress = null;
			RemoteStateActionDelegate getRemoteEndPoint = (State state) => { remoteAddress = state.RemoteEndpoint; };
			this.protocolState.RemoteStateAction(
				remoteSystem,
				getRemoteEndPoint);

			EndpointAddress remoteEndPointAddress = new EndpointAddress(remoteAddress);
			Binding binding;
			if (this.IsHttpsEndPoint(remoteAddress))
			{
				binding = new BasicHttpsBinding();
			}
			else
			{
				binding = new BasicHttpBinding();
			}
			binding.CloseTimeout = new TimeSpan(0, 0, 0, 0, Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.ClientConnectionTimeout);

			return new ServiceClient(
				this.container.Resolve<IXmlSerializerService>(),
				this.container.Resolve<ISequenceRepository>(),
				this.protocolState,
				this.container.Resolve<ILogService>(),
				binding,
				remoteEndPointAddress);
		}

		private bool IsHttpsEndPoint(string url)
		{
			return url != null && url.ToLower().StartsWith(ServiceClientFactory.HttpsScheme);
		}
	}
}