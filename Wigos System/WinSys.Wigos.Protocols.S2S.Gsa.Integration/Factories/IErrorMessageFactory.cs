﻿using System;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories
{
	/// <summary>
	/// Implements ErrorFactory
	/// </summary>
	public interface IErrorMessageFactory
	{
		/// <summary>
		/// Generate a specified s2sAck message based on passed exception
		/// </summary>
		/// <param name="exception">Source exception</param>
		/// <returns>Filled s2sMessage with return command type. According to the exception, it will return more or less completed object</returns>
		s2sMessage Create(Exception exception);
	}
}