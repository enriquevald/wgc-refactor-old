﻿using System;
using System.Runtime.Serialization;

using WinSys.Wigos.Architecture.Exceptions;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions
{
	public class CommsException : ContainerWithItemException<object, object>
	{
		public CommsException()
			: base()
		{
		}

		public CommsException(
			object sourceContainer,
			object sourceItem)
			: base(sourceContainer, sourceItem)
		{
		}

		public CommsException(string message)
			: base(message)
		{
		}

		public CommsException(
			object sourceContainer,
			object sourceItem,
			string message)
			: base(sourceContainer, sourceItem, message)
		{
		}

		public CommsException(
			string message,
			Exception innerException)
			: base(message, innerException)
		{
		}

		public CommsException(
			object sourceContainer,
			object sourceItem,
			string message,
			Exception innerException)
			: base(sourceContainer, sourceItem, message, innerException)
		{
		}

		public CommsException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context)
		{
		}
	}
}