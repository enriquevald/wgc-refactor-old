﻿using System;
using System.Runtime.Serialization;
using System.Xml.Linq;

using WinSys.Wigos.Architecture.Exceptions;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions
{
	public class SyntaxException : ContainerWithItemException<XObject, XObject>
	{
		public SyntaxException() 
			: base()
		{
		}

		public SyntaxException(
			XObject sourceContainer,
			XObject sourceItem)
			: base(sourceContainer, sourceItem)
		{
		}

		public SyntaxException(string message) 
			: base(message)
		{
		}

		public SyntaxException(
			XObject sourceContainer,
			XObject sourceItem,
			string message) 
			: base(sourceContainer, sourceItem, message)
		{
		}

		public SyntaxException(
			string message,
			Exception innerException) 
			: base(message, innerException)
		{
		}

		public SyntaxException(
			XObject sourceContainer,
			XObject sourceItem,
			string message,
			Exception innerException) 
			: base(sourceContainer, sourceItem, message, innerException)
		{
		}

		public SyntaxException(
			SerializationInfo info,
			StreamingContext context) 
			: base(info, context)
		{
		}
	}
}