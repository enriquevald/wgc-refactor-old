﻿using System;
using System.Runtime.Serialization;

using WinSys.Wigos.Architecture.Exceptions;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions
{
	public class ContentException : ContainerWithItemException<object, string>
	{
		public ContentException()
			: base()
		{
		}

		public ContentException(
			object sourceContainer,
			string sourceItem)
			: base(sourceContainer, sourceItem)
		{
		}

		public ContentException(string message)
			: base(message)
		{
		}

		public ContentException(
			object sourceContainer,
			string sourceItem,
			string message)
			: base(sourceContainer, sourceItem, message)
		{
		}

		public ContentException(
			string message,
			Exception innerException)
			: base(message, innerException)
		{
		}

		public ContentException(
			object sourceContainer,
			string sourceItem,
			string message,
			Exception innerException)
			: base(sourceContainer, sourceItem, message, innerException)
		{
		}

		public ContentException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context)
		{
		}
	}
}