﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants
{
	// TODO: Refactor. Move the apropiate class to respective independent file and class in "*.Constants.Errors" namespace
	//public static class S2S_BN_Constants
	//{
	//	public const string InvalidControllerIdentifierSpecified = "BN01";
	//	public const string InvalidPoolIdentifierSpecified = "BN02";
	//	public const string InvalidBonusMeter = "BN03";
	//	public const string InvalidBonusPaymentMethod = "BN04";
	//	public const string BonusConfigurationCouldNotBeSet = "BN05";
	//	public const string TransactionIdentifierIsNotPending = "BN06";
	//	public const string BonusSubscriptionCouldNotBeSet = "BN07";
	//	public const string InvalidDateRangeSpecifiedForReport = "BN08";
	//}

	//public static class S2S_CM_Constants
	//{
	//	public const string CompRecordNotFound = "CM01";
	//	public const string HostIdSourceIdMismatch = "CM02";
	//	public const string CompCanNotBeIssuedDueToStopCodes = "CM03";
	//	public const string AmountRequestedExceedsAvailableBalance = "CM04";
	//	public const string CompHasBeenRedeemed = "CM05";
	//	public const string CompHasBeenVoided = "CM06";
	//	public const string EmployeeCanNotAuthorizeCompsForLocation = "CM07";
	//	public const string InvalidCompStatus = "CM08";
	//	public const string InvalidTransactionActionCode = "CM09";
	//	public const string PinRequired = "CM10";
	//	public const string PinInvalid = "CM11";
	//	public const string ForeignExchangeDataExpired = "CM12";
	//	public const string IncorrectForeignExchangeData = "CM13";
	//}

	//public static class S2S_CN_Constants
	//{
	//	public const string InvalidVipProgramType = "CN01";
	//	public const string InvalidMachineFloorLocation = "CN02";
	//	public const string InvalidCalculationType = "CN03";
	//	public const string InvalidAwardBasis = "CN04";
	//}

	//public static class S2S_FC_Constants
	//{
	//	public const string FillCreditRecordNotFound = "FC01";
	//	public const string InvalidFillBankIdentifier = "FC02";
	//	public const string FillBankIsNotOpen = "FC03";
	//	public const string FillBankHasRolledToTheNextGamingDay = "FC04";
	//	public const string NoInventoryRequested = "FC05";
	//	public const string ChipSetNotAvailableFromFillBank = "FC06";
	//	public const string FillCreditHasBeenAcknowledged = "FC07";
	//	public const string FillCreditHasBeenConsummated = "FC08";
	//	public const string FillCreditHasBeenVoided = "FC09";
	//	public const string IncorrectLocationForFillCredit = "FC10";
	//	public const string IncorrectFillBankForFillCredit = "FC11";
	//	public const string InvalidFillCreditStatus = "FC12";
	//	public const string InvalidFillCreditType = "FC13";
	//	public const string InvalidFillCreditPriority = "FC14";
	//	public const string InvalidTransactionActionCode = "FC15";
	//}

	//public static class S2S_FT_Constants
	//{
	//	public const string InvalidGamingDate = "FT01";
	//	public const string InvalidReferenceIdentifier = "FT02";
	//	public const string InvalidFinancialTransactionFlowDirection = "FT03";
	//	public const string Textdescribingerrorcondition = "FT99";
	//}

	//public static class S2S_GA_Constants
	//{
	//	public const string VerificationIdUnknown = "GA01";
	//	public const string ComponentIdUnknown = "GA02";
	//	public const string AlgorithmNotSupported = "GA03";
	//	public const string SeedInvalidOrNotSupported = "GA04";
	//	public const string SaltNotSupported = "GA05";
	//	public const string OffsetInvalidOrNotSupported = "GA06";
	//	public const string VerificationRejectedQueueFull = "GA07";
	//	public const string TransactionIdMismatch = "GA08";
	//	public const string VerificationRejected = "GA09";
	//	public const string MissingSeed = "GA10";
	//}

	//public static class S2S_HP_Constants
	//{
	//	public const string InvalidHandpayIdentifier = "HP01";
	//	public const string InvalidKeyoffType = "HP02";
	//	public const string InvalidHandpayType = "HP03";
	//	public const string InvalidIdentificationType = "HP04";
	//	public const string InvalidEmployeeIdentifier = "HP05";
	//	public const string IncorrectPinForEmployee = "HP06";
	//	public const string InactiveEmployee = "HP07";
	//	public const string EmployeeNotAuthorizedForSpecifiedJobCode = "HP08";
	//	public const string HandpayAlreadyDisbursedOrCancelled = "HP09";
	//	public const string LastWagerAmountRequired = "HP10";
	//	public const string MaximumBetValueRequired = "HP11";
	//	public const string WinningCombinationRequired = "HP12";
	//	public const string InvalidDisbursementIdentifier = "HP13";
	//	public const string RemoteKeyoffNotAllowed = "HP14";
	//	public const string IncorrectHandpayRequestAmount = "HP15";
	//	public const string HandpayIdentifierIsNotCurrentlyPending = "HP16";
	//	public const string DisbursementIdentifierIsNotCurrentlyPending = "HP17";
	//}

	//public static class S2S_JP_Constants
	//{
	//	public const string JackpotRecordNotFound = "JP01";
	//	public const string JackpotAmountDoesNotEqualDetail = "JP02";
	//	public const string LocationNotAssignedToControllerLevel = "JP03";
	//	public const string JackpotHasBeenRedeemed = "JP04";
	//	public const string JackpotHasBeenVoided = "JP05";
	//	public const string InvalidJackpotStatus = "JP06";
	//	public const string InvalidTransactionActionCode = "JP07";
	//}

	//public static class S2S_MK_Constants
	//{
	//	public const string MarkerRecordNotFound = "MK01";
	//	public const string VoucherRecordNotFound = "MK02";
	//	public const string TransferRecordNotFound = "MK03";
	//	public const string CpvRecordNotFound = "MK04";
	//	public const string MarkerTotalDoesNotEqualDetail = "MK05";
	//	public const string VoucherTotalDoesNotEqualDetail = "MK06";
	//	public const string MarkerCanNotBeIssuedDueToStopCodes = "MK07";
	//	public const string VoucherCanNotBeIssuedDueToStopCodes = "MK08";
	//	public const string AmountRequestedExceedsAvailableBalance = "MK09";
	//	public const string InsufficientBankInformationOnFile = "MK10";
	//	public const string InsufficientIdentificationInformationOnFile = "MK11";
	//	public const string CallCreditNoSupervisorOk = "MK12";
	//	public const string CheckBankHasRolledToTheNextGamingDay = "MK13";
	//	public const string InvalidMarkerTypeForRedemption = "MK14";
	//	public const string MarkerNotIssuedToPatron = "MK15";
	//	public const string MarkerNotLocatedInPit = "MK16";
	//	public const string MarkerHasBeenVoided = "MK17";
	//	public const string MarkerHasBeenRedeemed = "MK18";
	//	public const string PatronStopCodeNoSupervisorOk = "MK19";
	//	public const string MarkerWasIssuedAsPartOfARedemption = "MK20";
	//	public const string MarkerIsNoLongerAtItsIssuanceLocation = "MK21";
	//	public const string VoucherHasBeenVoided = "MK22";
	//	public const string VoucherIsNoLongerAtItsIssuanceLocation = "MK23";
	//	public const string VoucherIsNotPatronsMostRecentRedemption = "MK24";
	//	public const string InvalidCheckBankIdentifier = "MK25";
	//	public const string CheckBankIsNotOpen = "MK26";
	//	public const string CpvHasBeenRedeemed = "MK27";
	//	public const string CpvHasBeenVoided = "MK28";
	//	public const string CpvNotIssuedToPatron = "MK29";
	//	public const string CpvChipSetNotAvailableAtLocation = "MK30";
	//	public const string NoSupervisorOk = "MK31";
	//	public const string InvalidMarkerStatus = "MK32";
	//	public const string InvalidMarkerType = "MK33";
	//	public const string InvalidVoucherStatus = "MK34";
	//	public const string InvalidVoucherType = "MK35";
	//	public const string InvalidTransferStatus = "MK36";
	//	public const string InvalidTransactionActionCode = "MK37";
	//}

	//public static class S2S_OC_Constants
	//{
	//	public const string LocationIsOpen = "OC01";
	//	public const string LocationIsClosed = "OC02";
	//	public const string LocationIsLockedNoSupervisorOk = "OC03";
	//	public const string IncorrectGameCodeForLocation = "OC04";
	//	public const string LocationCanNotBeOpenedForDateShift = "OC05";
	//	public const string LocationIsNotOpenForDateShift = "OC06";
	//	public const string DateShiftOutOfSequenceNoSupervisorOk = "OC07";
	//	public const string ReOpenRequestedNoSupervisorOk = "OC08";
	//	public const string VarianceFromCloseNoSupervisorOk = "OC09";
	//	public const string UnacknowledgedFillCredits = "OC10";
	//	public const string UntransferredMarkersRedemptions = "OC11";
	//	public const string LargeChangeInWinLossNoSupervisorOk = "OC12";
	//	public const string InvalidHeadCount = "OC13";
	//	public const string InvalidTransactionActionCode = "OC14";
	//}

	//public static class S2S_PC_Constants
	//{
	//	public const string InvalidCountdownBasis = "PC01";
	//	public const string InvalidCardTrackSpecified = "PC02";
	//	public const string InvalidCountdownType = "PC03";
	//	public const string InvalidCountdownDirection = "PC04";
	//	public const string InvalidPlayerCardType = "PC05";
	//	public const string InvalidPlayerCardStatus = "PC06";
	//	public const string MessageNotDisplayed = "PC07";
	//	public const string InvalidRpnExpression = "PC08";
	//}
	//public static class S2S_PG_Constants
	//{
	//	public const string InvalidProgressiveIdSpecified = "PG01";
	//	public const string InvalidProgressiveLevelSpecified = "PG02";
	//	public const string InvalidProgressiveMeter = "PG03";
	//	public const string InvalidWinLevelForPaytable = "PG04";
	//	public const string InvalidNumberOfCreditsSpecified = "PG05";
	//	public const string InvalidProgressivePaymentMethod = "PG06";
	//	public const string ProgressiveConfigurationCouldNotBeSet = "PG07";
	//	public const string TransactionIdentifierIsNotPending = "PG08";
	//	public const string ProgressiveSubscriptionCouldNotBeSet = "PG09";
	//	public const string InvalidDateRangeSpecifiedForReport = "PG10";
	//}

	//public static class S2S_PI_Constants
	//{
	//	public const string PatronRecordNotFound = "PI01";
	//	public const string InvalidPatronRank = "PI02";
	//	public const string InvalidCtrStatus = "PI03";
	//	public const string InvalidGender = "PI04";
	//	public const string InvalidMaritalStatus = "PI05";
	//	public const string InvalidAddressMailCode = "PI06";
	//	public const string InvalidEMailSendCode = "PI07";
	//	public const string InvalidStopCodeAction = "PI08";
	//	public const string UnableToAddPatron = "PI09";
	//	public const string UnableToDeletePatron = "PI10";
	//	public const string UnableToUpdatePatron = "PI11";
	//	public const string AddPatronCommandNotSupported = "PI12";
	//	public const string DeletePatronCommandNotSupported = "PI13";
	//	public const string UpdatePatronCommandNotSupported = "PI14";
	//	public const string ClientNotAuthorizedToAddPatron = "PI15";
	//	public const string ClientNotAuthorizedToDeletePatron = "PI16";
	//	public const string ClientNotAuthorizedToUpdatePatron = "PI17";
	//	public const string SearchQueryParameterNotSupported = "PI18";
	//	public const string UnableToReturnRequestedResults = "PI19";
	//	public const string SearchTimedOut = "PI20";
	//	public const string SystemWasBusy = "PI21";
	//	public const string RedemptionLocationMustBeSpecified = "PI22";
	//	public const string CompItemMustBeSpecified = "PI23";
	//}

	//public static class S2S_PR_Constants
	//{
	//	public const string PlayerRatingRecordNotFound = "PR01";
	//	public const string HostIdSourceIdMismatch = "PR02";
	//	public const string InvalidRatingType = "PR03";
	//	public const string InvalidRatingStatus = "PR04";
	//	public const string InvalidTableLineNumber = "PR05";
	//	public const string InvalidTableSeatNumber = "PR06";
	//	public const string TotalBuyInDoesNotEqualDetail = "PR07";
	//	public const string GamesPlayedDoesNotCrossCalculate = "PR08";
	//	public const string AverageBetDoesNotCrossCalculate = "PR09";
	//	public const string TurnoverDoesNotCrossCalculate = "PR10";
	//	public const string ActualWinDoesNotEqualDetail = "PR11";
	//	public const string TheoreticalWinDoesNotCrossCalculate = "PR12";
	//	public const string InvalidTransactionActionCode = "PR13";
	//	public const string LocationNotOpenForGamingDateShift = "PR14";
	//	public const string IncorrectGameCodeForLocation = "PR15";
	//	public const string IncorrectCalculationCodeForLocation = "PR16";
	//	public const string IncorrectSkillCodeForLocation = "PR17";

	//}
	//public static class S2S_SSCIX_Constants
	//{
	//	public const string InvalidClientType = "S2S_CIX002";
	//	public const string ClientTypeNotSupported = "S2S_CIX003";
	//	public const string InvalidDeviceClass = "S2S_CIX004";
	//	public const string InvalidDeviceIdentifier = "S2S_CIX005";
	//	public const string ClientIdentifierAlreadyAssigned = "S2S_CIX101";
	//	public const string ClientCannotBeAdded = "S2S_CIX102";
	//	public const string ClientCannotBeModified = "S2S_CIX103";
	//	public const string ClientCannotBeDeleted = "S2S_CIX104";
	//	public const string ClientDataSetNotSupported = "S2S_CIX105";
	//	public const string ClientDataSetCannotBeAdded = "S2S_CIX106";
	//	public const string ClientDataSetCannotBeModified = "S2S_CIX107";
	//	public const string ClientTypeNotSupportedByCashierWorkstation = "S2S_CIX108";
	//	public const string InvalidClientIdentifier = "S2S_CIX201";
	//	public const string InvalidClientIdentifierForProperty = "S2S_CIX202";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_CIX999";
	//}

	//public static class S2S_SSCUX_Constants
	//{
	//	public const string RetentionPeriodExpiredInformationUpdatesDiscarded = "S2S_CUX001";
	//	public const string SubscriptionCannotBeAccepted = "S2S_CUX002";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_CUX999";
	//}

	//public static class S2S_SSEIX_Constants
	//{
	//	public const string InvalidAddressIdentifierforEmployee = "S2S_EIX001";
	//	public const string InvalidPhoneIdentifierforEmployee = "S2S_EIX002";
	//	public const string InvalidEmailIdentifierEmployee = "S2S_EIX003";
	//	public const string InvalidIdentificationIdentifierForEmployee = "S2S_EIX004";
	//	public const string InvalidImageIdentifierForEmployee = "S2S_EIX005";
	//	public const string InvalidCommentIdentifierForEmployee = "S2S_EIX006";
	//	public const string InvalidLinkForEmployee = "S2S_EIX007";
	//	public const string InvalidLinkType = "S2S_EIX008";
	//	public const string EmployeeIdentifierAlreadyAssigned = "S2S_EIX101";
	//	public const string EmployeeCannotBeAdded = "S2S_EIX102";
	//	public const string EmployeeCannotBeModified = "S2S_EIX103";
	//	public const string EmployeeCannotBeDeleted = "S2S_EIX104";
	//	public const string EmployeeDataSetNotSupported = "S2S_EIX105";
	//	public const string EmployeeDataSetCannotBeAdded = "S2S_EIX106";
	//	public const string EmployeeDataSetCannotBeModified = "S2S_EIX107";
	//	public const string InvalidEmployeeSearchOption = "S2S_EIX122";
	//	public const string InvalidEmployeeSearchValue = "S2S_EIX123";
	//	public const string EmployeeCardAssignedtoAnotherEmployee = "S2S_EIX141";
	//	public const string EmployeeCardCannotBeAdded = "S2S_EIX142";
	//	public const string EmployeeCardCannotBeModified = "S2S_EIX143";
	//	public const string EmployeeCardCannotBeDeleted = "S2S_EIX144";
	//	public const string PinAlreadyAssignedToEmployee = "S2S_EIX161";
	//	public const string PinCannotBeAddedToEmployee = "S2S_EIX162";
	//	public const string PinCannotBeModifiedForEmployee = "S2S_EIX163";
	//	public const string PinCannotBeDeletedForEmployee = "S2S_EIX164";
	//	public const string IncorrectPinforEmployee = "S2S_EIX165";
	//	public const string NotEnoughPinsForEmployee = "S2S_EIX166";
	//	public const string InvalidEmployeeIdentifier = "S2S_EIX201";
	//	public const string InvalidEmployeeIdentifierForProperty = "S2S_EIX202";
	//	public const string InvalidEmployeeCard = "S2S_EIX203";
	//	public const string InvalidEmployeeCardForProperty = "S2S_EIX204";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_EIX999";
	//}

	//public static class S2S_SSEVX_Constants
	//{
	//	public const string EventsforEndClientTypeNotSupported = "S2S_EVX001";
	//	public const string InvalidEndClientIdentifier = "S2S_EVX002";
	//	public const string ClientNotSubscribedToEvent = "S2S_EVX003";
	//	public const string RetentionPeriodExpiredEventInformationDiscarded = "S2S_EVX004";
	//	public const string InvalidMeterForm = "S2S_EVX005";
	//	public const string MeterFormNotSupported = "S2S_EVX006";
	//	public const string MetersforEndClientTypeNotSupported = "S2S_EVX007";
	//	public const string InvalidEndClientIdentifier2 = "S2S_EVX008";
	//	public const string MeterFormCannotBeChanged = "S2S_EVX009";
	//	public const string ClientNotSubscribedtoClass = "S2S_EVX010";
	//	public const string ClientNotSubscribedtoDevice = "S2S_EVX011";
	//	public const string ClientNotSubscribedtoMeter = "S2S_EVX012";
	//	public const string InvalidMeterType = "S2S_EVX013";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_EVX999";
	//}

	//public static class S2S_SSGBX_Constants
	//{
	//	public const string InvalidLanguageCode = "S2S_GBX001";
	//	public const string InvalidCountryCode = "S2S_GBX002";
	//	public const string InvalidStateProvinceCode = "S2S_GBX003";
	//	public const string InvalidPostalCode = "S2S_GBX004";
	//	public const string InvalidPostalCodeExtension = "S2S_GBX005";
	//	public const string InvalidCurrencyCode = "S2S_GBX006";
	//	public const string InvalidLocaleCode = "S2S_GBX007";
	//	public const string InvalidUnitofMeasureCode = "S2S_GBX008";
	//	public const string InvalidManufacturerCode = "S2S_GBX009";
	//	public const string InvalidDayofWeek = "S2S_GBX010";
	//	public const string InvalidMaritalState = "S2S_GBX021";
	//	public const string InvalidGenderType = "S2S_GBX022";
	//	public const string InvalidAddressState = "S2S_GBX023";
	//	public const string InvalidMailingAddressType = "S2S_GBX024";
	//	public const string InvalidEmailAddressType = "S2S_GBX025";
	//	public const string InvalidPhoneNumberType = "S2S_GBX026";
	//	public const string InvalidIdentificationType = "S2S_GBX031";
	//	public const string InvalidImageType = "S2S_GBX032";
	//	public const string InvalidImageEncoding = "S2S_GBX033";
	//	public const string InvalidIdReaderType = "S2S_GBX034";
	//	public const string InvalidSecurityAlgorithm = "S2S_GBX035";
	//	public const string SecurityAlgorithmNotSupported = "S2S_GBX036";
	//	public const string IncorrectSecurityKeyIdentifierForClient = "S2S_GBX037";
	//	public const string SecurityKeyIdentifierHasExpired = "S2S_GBX038";
	//	public const string InvalidPinValidationIdentifier = "S2S_GBX039";
	//	public const string InvalidIdEncodingMethod = "S2S_GBX040";
	//	public const string IdReaderTypeNotSupported = "S2S_GBX041";
	//	public const string SecurityAlgorithmNotSupportedForIdReaderType = "S2S_GBX042";
	//	public const string PinProtectionRequired = "S2S_GBX044";
	//	public const string InvalidSubscriptionIdentifier = "S2S_GBX051";
	//	public const string InvalidSubscriptionDeliveryMethod = "S2S_GBX052";
	//	public const string InvalidSubscriptionLocation = "S2S_GBX053";
	//	public const string InvalidCurrencyType = "S2S_GBX054";
	//}

	//public static class S2S_SSIPX_Constants
	//{
	//	public const string InvalidPinOption = "S2S_IPX001";
	//	public const string InvalidPreCommitOption = "S2S_IPX002";
	//	public const string InvalidRestrictionPeriod = "S2S_IPX003";
	//	public const string InvalidInformedPlayerAction = "S2S_IPX004";
	//	public const string PlayerNotEnrolledinInformedPlayerProgram = "S2S_IPX005";
	//	public const string InvalidExclusionIdentifier = "S2S_IPX006";
	//	public const string InvalidExclusionSource = "S2S_IPX007";
	//	public const string InvalidSessionIdentifier = "S2S_IPX008";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_IPX999";
	//}

	//public static class S2S_SSPCX_Constants
	//{
	//	public const string InvalidGamingDate = "S2S_PCX001";
	//	public const string InvalidReferenceIdentifier = "S2S_PCX002";
	//	public const string InvalidPlayerCompIdentifier = "S2S_PCX003";
	//	public const string CompAmountExceedsBalanceAvailableForAccount = "S2S_PCX004";
	//	public const string CompAmountExceedsBalanceAvailableForCompRedemptionLocation = "S2S_PCX005";
	//	public const string CompAmountExceedsBalanceAvailableForCompItem = "S2S_PCX006";
	//	public const string IncorrectForeignExchangeData = "S2S_PCX007";
	//	public const string ForeignExchangeDataExpired = "S2S_PCX008";
	//	public const string CompHasBeenVoided = "S2S_PCX009";
	//	public const string CompCannotBeRedeemed = "S2S_PCX010";
	//	public const string CompCannotBeVoided = "S2S_PCX011";
	//	public const string CompActionsRestricted = "S2S_PCX012";
	//	public const string InvalidPlayerCompStatus = "S2S_PCX013";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_PCX999";
	//}

	//public static class S2S_SSPIX_Constants
	//{
	//	public const string InvalidAddressIdentifierforPlayer = "S2S_PIX001";
	//	public const string InvalidPhoneIdentifierforPlayer = "S2S_PIX002";
	//	public const string InvalidEmailIdentifierPlayer = "S2S_PIX003";
	//	public const string InvalidIdentificationIdentifierforPlayer = "S2S_PIX004";
	//	public const string InvalidImageIdentifierforPlayer = "S2S_PIX005";
	//	public const string InvalidCommentIdentifierforPlayer = "S2S_PIX006";
	//	public const string InvalidStopTypeforPlayer = "S2S_PIX007";
	//	public const string InvalidTagforPlayer = "S2S_PIX008";
	//	public const string InvalidAttributeforPlayer = "S2S_PIX009";
	//	public const string InvalidRelationshipforPlayer = "S2S_PIX010";
	//	public const string InvalidLinkforPlayer = "S2S_PIX011";
	//	public const string InvalidClubforPlayer = "S2S_PIX012";
	//	public const string InvalidGroupforPlayer = "S2S_PIX013";
	//	public const string InvalidAccountforPlayer = "S2S_PIX014";
	//	public const string InvalidJunketforPlayer = "S2S_PIX015";
	//	public const string InvalidDiscountProgramInstanceforPlayer = "S2S_PIX016";
	//	public const string DiscountProgramEndDateTimeMustBeSpecified = "S2S_PIX017";
	//	public const string InvalidStopCodeType = "S2S_PIX018";
	//	public const string InvalidStopCodeAction = "S2S_PIX019";
	//	public const string InvalidEmployeeRelationshipType = "S2S_PIX020";
	//	public const string InvalidPlayerLinkType = "S2S_PIX021";
	//	public const string PlayerIsBarredFromProperty = "S2S_PIX022";
	//	public const string PlayerIsExcludedFromProperty = "S2S_PIX023";
	//	public const string PlayerIsProhibitedFromPlay = "S2S_PIX024";
	//	public const string PlayerSignatureVerificationRequired = "S2S_PIX025";
	//	public const string PlayerCreditSuspended = "S2S_PIX026";
	//	public const string PlayerCreditRequiresVerification = "S2S_PIX027";
	//	public const string PlayerHasReturnedChecks = "S2S_PIX028";
	//	public const string PlayerHasWriteOffs = "S2S_PIX029";
	//	public const string PlayerMailReturned = "S2S_PIX030";
	//	public const string PlayerEmailUndeliverable = "S2S_PIX031";
	//	public const string PlayerPhoneDisconnected = "S2S_PIX032";
	//	public const string PlayerIdentifierAlreadyAssigned = "S2S_PIX101";
	//	public const string PlayerCannotBeAdded = "S2S_PIX102";
	//	public const string PlayerCannotBeModified = "S2S_PIX103";
	//	public const string PlayerCannotBeDeleted = "S2S_PIX104";
	//	public const string PlayerDataSetNotSupported = "S2S_PIX105";
	//	public const string PlayerDataSetCannotBeAdded = "S2S_PIX106";
	//	public const string PlayerDataSetCannotBeModified = "S2S_PIX107";
	//	public const string InvalidPlayerSearchOption = "S2S_PIX122";
	//	public const string InvalidPlayerSearchValue = "S2S_PIX123";
	//	public const string PlayerCardAssignedtoAnotherPlayer = "S2S_PIX141";
	//	public const string PlayerCardCannotBeAdded = "S2S_PIX142";
	//	public const string PlayerCardCannotBeModified = "S2S_PIX143";
	//	public const string PlayerCardCannotBeDeleted = "S2S_PIX144";
	//	public const string PlayerPinAlreadyAssignedtoPlayer = "S2S_PIX161";
	//	public const string PlayerPinCannotBeAdded = "S2S_PIX162";
	//	public const string PlayerPinCannotBeModified = "S2S_PIX163";
	//	public const string PlayerPinCannotBeDeleted = "S2S_PIX164";
	//	public const string IncorrectPinforPlayer = "S2S_PIX165";
	//	public const string NotEnoughPinsforPlayer = "S2S_PIX166";
	//	public const string CompRedemptionLocationMustBeSpecified = "S2S_PIX181";
	//	public const string CompItemMustBeSpecified = "S2S_PIX182";
	//	public const string InvalidPlayerIdentifier = "S2S_PIX201";
	//	public const string InvalidPlayerIdentifierForProperty = "S2S_PIX202";
	//	public const string InvalidPlayerCard = "S2S_PIX203";
	//	public const string InvalidPlayerCardForProperty = "S2S_PIX204";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_PIX999";
	//}

	//public static class S2S_SSRIX_Constants
	//{
	//	public const string InvalidGeographicAreaIdentifierForProperty = "S2S_RIX002";
	//	public const string InvalidPlayerAccountIdentifierForProperty = "S2S_RIX003";
	//	public const string InvalidPlayerTagIdentifierForProperty = "S2S_RIX004";
	//	public const string InvalidPlayerAttributeIdentifierForProperty = "S2S_RIX005";
	//	public const string InvalidPlayerClubIdentifierForProperty = "S2S_RIX006";
	//	public const string InvalidPlayerGroupIdentifierForProperty = "S2S_RIX007";
	//	public const string InvalidDepartmentIdentifierForProperty = "S2S_RIX008";
	//	public const string InvalidEmployeeJobCodeIdentifierForProperty = "S2S_RIX009";
	//	public const string InvalidCompRedemptionLocationIdentifierForProperty = "S2S_RIX010";
	//	public const string InvalidCompItemIdentifierForProperty = "S2S_RIX011";
	//	public const string InvalidPhysicalAreaIdentifierForProperty = "S2S_RIX012";
	//	public const string InvalidPhysicalZoneIdentifierForProperty = "S2S_RIX013";
	//	public const string InvalidPhysicalSectionIdentifierForProperty = "S2S_RIX014";
	//	public const string InvalidGameClassificationIdentifierForProperty = "S2S_RIX015";
	//	public const string InvalidGameThemeIdentifierForProperty = "S2S_RIX016";
	//	public const string InvalidGamePaytableIdentifierForProperty = "S2S_RIX017";
	//	public const string InvalidJunketIdentifierForProperty = "S2S_RIX019";
	//	public const string InvalidJunketRepIdentifierForProperty = "S2S_RIX020";
	//	public const string InvalidDiscountProgramIdentifierForProperty = "S2S_RIX021";
	//	public const string InvalidFloorLocationIdentifierForProperty = "S2S_RIX022";
	//	public const string InvalidMultiPlayerGroupIdentifierForProperty = "S2S_RIX023";
	//	public const string InvalidMachineIdentifierForProperty = "S2S_RIX024";
	//	public const string InvalidKioskIdentifierForProperty = "S2S_RIX025";
	//	public const string InvalidStationIdentifierForProperty = "S2S_RIX026";
	//	public const string InvalidDiscountProgramInstanceForProperty = "S2S_RIX027";
	//	public const string InvalidCompPurposeIdentifierForProperty = "S2S_RIX028";
	//	public const string InvalidChipSetIdentifierForProperty = "S2S_RIX030";
	//	public const string InvalidShiftIdentifierForProperty = "S2S_RIX031";
	//	public const string InvalidCeqIdentifierForProperty = "S2S_RIX032";
	//	public const string InvalidPlayerRatingCalculationIdentifierForProperty = "S2S_RIX033";
	//	public const string InvalidPlayerRatingSkillForCalculation = "S2S_RIX034";
	//	public const string InvalidPlayerRatingAwardForCalculation = "S2S_RIX035";
	//	public const string InvalidLocationIdentifierForProperty = "S2S_RIX036";
	//	public const string InvalidChipSetForLocation = "S2S_RIX037";
	//	public const string InvalidPlayerAccountType = "S2S_RIX038";
	//	public const string InvalidCreditType = "S2S_RIX039";
	//	public const string InvalidCompRedemptionLocationType = "S2S_RIX040";
	//	public const string InvalidCompItemType = "S2S_RIX041";
	//	public const string InvalidDiscountProgramType = "S2S_RIX042";
	//	public const string InvalidDiscountProgramState = "S2S_RIX043";
	//	public const string InvalidZoneType = "S2S_RIX044";
	//	public const string InvalidJunketBadgeIdentifier = "S2S_RIX045";
	//	public const string InvalidPropertyType = "S2S_RIX046";
	//	public const string InvalidBrandIdentifier = "S2S_RIX047";
	//	public const string InvalidCapabilityType = "S2S_RIX048";
	//	public const string InvalidPropertyAddressType = "S2S_RIX049";
	//	public const string InvalidRevenueClassification = "S2S_RIX050";
	//	public const string InvalidAccountingIdentifier = "S2S_RIX051";
	//	public const string InvalidWorldMarketIdentifier = "S2S_RIX052";
	//	public const string InvalidCountryMarketIdentifier = "S2S_RIX053";
	//	public const string InvalidStateProvinceMarketIdentifier = "S2S_RIX054";
	//	public const string InvalidCityMarketIdentifier = "S2S_RIX055";
	//	public const string InvalidDistrictMarketIdentifier = "S2S_RIX056";
	//	public const string InvalidGameType = "S2S_RIX057";
	//	public const string InvalidPlayerRatingAwardBasis = "S2S_RIX058";
	//	public const string InvalidLocationType = "S2S_RIX059";
	//	public const string InvalidSignIdentifierForProperty = "S2S_RIX060";
	//	public const string InvalidCabinetStyle = "S2S_RIX061";
	//	public const string PropertyIdentifierAlreadyAssigned = "S2S_RIX101";
	//	public const string PropertyCannotBeAdded = "S2S_RIX102";
	//	public const string PropertyCannotBeModified = "S2S_RIX103";
	//	public const string PropertyCannotBeDeleted = "S2S_RIX104";
	//	public const string PropertyDataSetNotSupported = "S2S_RIX105";
	//	public const string PropertyDataSetCannotBeAdded = "S2S_RIX106";
	//	public const string PropertyDataSetCannotBeModified = "S2S_RIX107";
	//	public const string InvalidgetPropertyInfoRequest = "S2S_RIX108";
	//	public const string InvalidPropertyIdentifier = "S2S_RIX201";
	//	public const string IncludeAppropriateTextDescribingTheErrorCondition = "S2S_RIX999";
	//}

	//public static class S2S_TF_Constants
	//{
	//	public const string InvalidGamingDate = "TF01";
	//	public const string InvalidReferenceIdentifier = "TF02";
	//	public const string InvalidTaxpayerIdentifier = "TF03";
	//	public const string InvalidW2GFilingType = "TF04";
	//	public const string InvalidWagerFilingType = "TF05";
	//	public const string Invalid1042SFilingType = "TF06";
	//	public const string Invalid1099MiscType = "TF07";
	//	public const string InvalidTaxFormType = "TF08";
	//	public const string TaxFormHasBeenVoided = "TF09";
	//	public const string IncorrectTaxFormDetail = "TF10";
	//	public const string InsufficientPlayerIdentificationInformation = "TF11";
	//	public const string InvalidTaxFormIdentifier = "TF12";
	//	public const string TaxFormHasBeenFiled = "TF13";
	//	public const string IncludeAppropriateTextDescribingErrorCondition = "TF99";
	//}

	//public static class S2S_WT_Constants
	//{
	//	public const string TransferDeniedByHost = "WT01";
	//	public const string InvalidIdentificationTypeSpecified = "WT02";
	//	public const string InvalidPlayerIdentificationSpecified = "WT03";
	//	public const string InvalidAccountIdSpecified = "WT04";
	//	public const string InvalidTransferActionSpecified = "WT05";
	//	public const string IncorrectCreditTypeForAccount = "WT06";
	//	public const string IncorrectPoolIdForAccount = "WT07";
	//	public const string InsufficientFundsAvailable = "WT08";
	//	public const string DuplicateTransactionId = "WT09";
	//	public const string AlgorithmNotSupported = "WT10";
	//	public const string InvalidKeyPairIdentifier = "WT11";
	//	public const string KeyPairIdentifierHasExpired = "WT12";
	//	public const string ForeignExchangeDataExpired = "WT13";
	//	public const string IncorrectForeignExchangeData = "WT14";
	//}
}