﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors
{
	public static class MessageLevel
	{
		public const string InvalidMessageRecipientSpecified = "MS01";
		public const string InvalidMessageOriginatorSpecified = "MS02";
		public const string InvalidMessageIdentifier = "MS03";
		public const string InvalidDataTypeEncountered = "MS04";
		public const string IncompleteOrMalformedXml = "MS05";
		public const string InvalidSessionIdentifier = "MS06";
		public const string InvalidSessionType = "MS07";
		public const string ClassNotSupported = "MS09";
		public const string CommandNotSupported = "MS10";
		public const string CommandContainedSyntaxOrSemanticErrors = "MS11";
		public const string MiscellaneousError = "MS12";
		public const string UnknownClient = "MS13";
		public const string ClientDisabled = "MS14";
		public const string MessageProcessingServicesUnavailable = "MS15";
		public const string CommandProcessingServicesUnavailable = "MS16";
		public const string UnknownClassEncountered = "MS17";
		public const string UnknownCommandEncountered = "MS18";
		public const string InboundMessageTooLarge = "MS19";
		public const string OutboundMessageTooLarge = "MS20";
		public const string InboundQueueFull = "MS21";
		public const string InboundCommandTooLarge = "MS22";
		public const string OutboundCommandTooLarge = "MS23";
		public const string CommandsNotInOrder = "MS24";
		public const string MiscellaneousError2 = "MS29";
	}
}