﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors
{
	public static class VoucherClass
	{
		public const string InvalidVoucherAction = "VC01";
		public const string InvalidVoucherIdentifier = "VC02";
		public const string InvalidTransactionIdentifier = "VC03";
		public const string VoucherHasBeenRedeemed = "VC04";
		public const string VoucherHasBeenVoided = "VC05";
		public const string VoucherRedemptionVoidPending = "VC06";
		public const string VoucherHasExpired = "VC07";
		public const string PlayerCardRequiredButNotSupplied = "VC08";
		public const string IncorrectPlayerCard = "VC09";
		public const string DuplicateTransactionId = "VC10";
		public const string LargeWinCannotBeRedeemedAtThisClient = "VC11";
		public const string InvalidDateRangeSpecifiedForReport = "VC12";
		public const string UnsupportedTransferProtocol = "VC13";
		public const string InvalidClientTypeSpecified = "VC14";
	}
}