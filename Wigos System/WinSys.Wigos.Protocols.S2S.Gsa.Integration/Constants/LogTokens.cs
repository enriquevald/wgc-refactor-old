﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants
{
	public static class LogTokens
	{
		public const string ApplicationLogTemplate = "{0} {1}";
		
		public const string GsaS2S = "[GSA][S2S]";
	}
}