﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants
{
	public static class S2SCommunications
	{
		public const string CommsErrorChannelType = "S2S_pointToPoint";
		public const string CommsErrorToFromSystemIndecipherable = "S2S_indecipherable";
	}
}