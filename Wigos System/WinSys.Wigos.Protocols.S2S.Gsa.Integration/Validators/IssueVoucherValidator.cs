﻿using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators
{
	/// <summary>
	/// Implements a validator to check voucher amount and transfer amount are equal
	/// </summary>
	public class IssueVoucherValidator : IIssueVoucherValidator
	{
		/// <inheritdoc />
		///  <summary>
		///  Validate voucher data for issue action
		///  </summary>
		///  <param name="item">Ticket to validate</param>
		///  <remarks>
		///  Exceptions:
		/// 		* BussinessException is thrown in case of any validation error
		///  </remarks>
		public void Validate(issueVoucher item)
		{
			if (item.transferAction != voucherAction.issued)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.InvalidTicketAction);
			}

			if (item.voucherAmount != item.transferAmount)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.InvalidTicketAction);
			}
		}
	}
}
