﻿using System.Collections;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Xml.Linq;
using System.Xml.Schema;
using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators
{
	/// <summary>
	/// Implements a syntax validator for a S2S Gsa request message
	/// </summary>
	public class SyntaxValidator : ISyntaxValidator
	{
		private readonly XmlSchemaSet xmlSchemaSet;

		public SyntaxValidator()
		{
			this.xmlSchemaSet = new XmlSchemaSet();

			ResourceManager resourceManager = new ResourceManager(typeof(SchemasV1_2_6));
			ResourceSet resourceSet = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
			IDictionaryEnumerator resourceEnumerator = resourceSet.GetEnumerator();
			while (resourceEnumerator.MoveNext())
			{
				using (StringReader stringReader = new StringReader((string)resourceEnumerator.Value))
				{
					this.xmlSchemaSet.Add(XmlSchema.Read(stringReader, null));
				}
			}
			this.xmlSchemaSet.Compile();
		}

		#region ISyntaxValidator

		/// <summary>
		/// Validate the syntax of the specified item
		/// </summary>
		/// <param name="item">Item to validate</param>
		/// <remarks>SyntaxException is thrown in case of any validation error</remarks>
		public void Validate(XDocument item)
		{
			item.Validate(
				this.xmlSchemaSet,
				(obj, eventArgs) =>
				{
					throw new SyntaxException(
						item, 
						(XObject)obj, 
						eventArgs.Message, 
						eventArgs.Exception);
				});
		}

		#endregion
	}
}