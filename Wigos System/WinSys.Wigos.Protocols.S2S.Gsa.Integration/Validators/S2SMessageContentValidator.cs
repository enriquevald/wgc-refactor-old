﻿using System.Linq;
using System.Resources;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Constants.Errors;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Entities;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.TypedResources.Classes;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators
{
	/// <summary>
	/// Implements a validator for a S2S Gsa Message envelop
	/// </summary>
	public class S2SMessageContentValidator : IS2SMessageContentValidator
	{
		private readonly ResourceManager errorResourceManager;

		private readonly IProtocolState protocolState;

		public S2SMessageContentValidator(IProtocolState protocolState)
		{
			this.protocolState = protocolState;

			this.errorResourceManager = new ResourceManager(typeof(Protocols.S2S.Gsa.Integration.TypedResources.S2SErrors));
		}

		#region IS2SMessageValidator

		/// <summary>
		/// Validate the content of the specified item
		/// </summary>
		/// <param name="item">Item to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* SyntaxException is thrown in case of any construction error (basic SyntaxException)
		///		* ContentException is thrown in case of any content validation error
		/// </remarks>
		public void Validate(s2sMessage item)
		{
			s2sHeader s2SHeader = item.Items
				.OfType<s2sHeader>()
				.FirstOrDefault();
			
			if (s2SHeader != null)
			{
				string fromSystem = null;
				string toSystem = null;
				RemoteStateActionDelegate getSystemFromTo = (State state) =>
				{
					fromSystem = state.FromSystem;
					toSystem = state.ToSystem;
				};
				this.protocolState.RemoteStateAction(s2SHeader.fromSystem, getSystemFromTo);

				if (string.IsNullOrEmpty(s2SHeader.toSystem) 
				    || (!string.IsNullOrEmpty(toSystem) && toSystem != s2SHeader.toSystem))
				{
					throw new ContentException(s2SHeader, S2SHeader.ToSystem);
				}
				if (string.IsNullOrEmpty(s2SHeader.fromSystem)
				    || (!string.IsNullOrEmpty(fromSystem) && fromSystem != s2SHeader.fromSystem))
				{
					throw new ContentException(s2SHeader, S2SHeader.FromSystem);
				}
			}
			else
			{
				throw new SyntaxException(this.errorResourceManager.GetString(MessageLevel.IncompleteOrMalformedXml));
			}
		}

		#endregion
	}
}