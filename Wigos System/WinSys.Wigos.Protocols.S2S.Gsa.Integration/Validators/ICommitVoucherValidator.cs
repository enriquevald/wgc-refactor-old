﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.Validators
{
	/// <summary>
	/// Implements a validator for a S2S Gsa CommitVoucher command
	/// </summary>
	public interface ICommitVoucherValidator : IValidator<commitVoucher>
	{
	}
}