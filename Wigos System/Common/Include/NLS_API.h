//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: NLS_API.h
//
//   DESCRIPTION: Constants, types, variables definitions and prototypes for NLS_API.cpp
//
//                 This module is SHARED in LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKT & LKAS), 
//                 ANY change must be checked twice by compiling LKT
//                 and LKAS DLLs
//
//        AUTHOR: Toni Jord�
//
// CREATION DATE: 18-MAR-2002
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-MAR-2002 TJG    First release.
//
//------------------------------------------------------------------------------

#ifndef __NLS_API_H
#define __NLS_API_H

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------
#define NLS_API_VERSION                 0

// Maximum length of each string on the string table
#define NLS_MSG_STRING_MAX_LENGTH       255

// Maximum length of each parameter
#define NLS_MSG_PARAM_MAX_LENGTH        40

// Private Messages
// Debug message with 5 parameters, if you use less, 
// the rest of parameters use _T(" ")
#define NLS_PRIVATE_MESSAGE_DEBUG       65001

// Return codes
#define NLS_STATUS_OK                   0
#define NLS_STATUS_ERROR                1
#define NLS_STATUS_DLL_NOT_LOADED       2
#define NLS_STATUS_STRING_NOT_EXIST     3

// Mask for NLS dll files
#define NLS_DLL_MASK                    "LKAS_NLS_???.dll"

// Languages
#define NLS_LANGUAGE_NEUTRAL            LANG_NEUTRAL
#define NLS_LANGUAGE_SPANISH            LANG_SPANISH
#define NLS_LANGUAGE_ENGLISH            LANG_ENGLISH
#define NLS_LANGUAGE_ITALIAN            LANG_ITALIAN
#define NLS_LANGUAGE_KOREAN             LANG_KOREAN
#define NLS_LANGUAGE_CZECH              LANG_CZECH

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the NLS_API_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// NLS_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef NLS_API_EXPORTS
#define NLS_API __declspec(dllexport)
#else
#define NLS_API __declspec(dllimport)
#endif

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
NLS_API WORD WINAPI NLS_SetLanguage (USHORT LanguageCode);

NLS_API WORD WINAPI NLS_GetLanguage (void);

NLS_API BYTE WINAPI NLS_GetFontCharset ();

NLS_API WORD WINAPI NLS_GetString (const WORD   StringId,
                                   TCHAR        * ResultString,
                                   const int    ResultStringLength,
                                   const TCHAR  * Param1 = NULL,
                                   const TCHAR  * Param2 = NULL,
                                   const TCHAR  * Param3 = NULL,
                                   const TCHAR  * Param4 = NULL,
                                   const TCHAR  * Param5 = NULL);

NLS_API WORD WINAPI NLS_GetNumLanguages (WORD * pNumLangs);

NLS_API WORD WINAPI NLS_GetLanguageId (WORD Index);

NLS_API WORD WINAPI NLS_SetAvailableLanguages (WORD     NumLangs, 
                                               WORD     * pLangIdList);

#endif // __NLS_API_H