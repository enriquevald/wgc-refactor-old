//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: CommonConstantsCAT.h
//
//   DESCRIPTION: Common constants
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked by compiling LKC, LKAS
//                 and LKT DLLs
//
//        AUTHOR: Nir Ingbir
//
// CREATION DATE: 04-FEB-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2003 NIR    Initial release.
//
//------------------------------------------------------------------------------
#ifndef __COMMON_CONSTANTS_CAT_H
#define __COMMON_CONSTANTS_CAT_H

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------
// General 
#define COMMON_MAX_GAMES                                 30  // Number of supported games 
                                                             // in LYKOS
// Device Codes 
#define COMMON_DEVICE_CODE_NO_DEVICE                     0
#define COMMON_DEVICE_CODE_PRINTER                       1
#define COMMON_DEVICE_CODE_NOTE_ACCEPTOR                 2
#define COMMON_DEVICE_CODE_SMARTCARD_READER             3
#define COMMON_DEVICE_CODE_CUSTOMER_DISPLAY              4
#define COMMON_DEVICE_CODE_COIN_ACCEPTOR                 5
#define COMMON_DEVICE_CODE_UPPER_DISPLAY                 6
#define COMMON_DEVICE_CODE_BAR_CODE_READER               7
//#define COMMON_DEVICE_CODE_COMMUNICATIONS                8
#define COMMON_DEVICE_CODE_UPS                           9
#define COMMON_DEVICE_CODE_LKAS_SOFTWARE_VERSION         10
#define COMMON_DEVICE_CODE_LKT_SOFTWARE_VERSION          11
#define COMMON_DEVICE_CODE_FLASH_1                       12
#define COMMON_DEVICE_CODE_FLASH_2                       13
#define COMMON_DEVICE_CODE_KIOSK                         14
#define COMMON_DEVICE_CODE_MODULE_IO                     15
#define COMMON_DEVICE_CODE_AGENCY_CONNECTION             16

// Generic Device Status 
#define COMMON_DEVICE_STATUS_UNKNOWN                    0
#define COMMON_DEVICE_STATUS_OK                         1
#define COMMON_DEVICE_STATUS_ERROR                      2
// AJQ 20-DEC-2005, Not Installed Status
#define COMMON_DEVICE_STATUS_NOT_INSTALLED              100

// XPS 18-MAY-2004
// Note: All specific driver status code start of last generic device status code

// Printer status
#define COMMON_DEVICE_STATUS_PRINTER_OFF                3
#define COMMON_DEVICE_STATUS_PRINTER_READY              4
#define COMMON_DEVICE_STATUS_PRINTER_NOT_READY          5
#define COMMON_DEVICE_STATUS_PRINTER_PAPER_OUT          6
#define COMMON_DEVICE_STATUS_PRINTER_PAPER_LOW          7
#define COMMON_DEVICE_STATUS_PRINTER_OFFLINE            8

// Bill acceptor status
#define COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_JAM          3
#define COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR     4
#define COMMON_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL 5

// Ups status
#define COMMON_DEVICE_STATUS_UPS_NO_AC                  3
#define COMMON_DEVICE_STATUS_UPS_BATTERY_LOW            4
#define COMMON_DEVICE_STATUS_UPS_OVERLOAD               5
#define COMMON_DEVICE_STATUS_UPS_OFF                    6
#define COMMON_DEVICE_STATUS_UPS_BATTERY_FAIL           7

// Smartcard status
#define COMMON_DEVICE_STATUS_SMARTCARD_REMOVED          3
#define COMMON_DEVICE_STATUS_SMARTCARD_INSERTED         4

// software status
#define COMMON_DEVICE_STATUS_VERSION_ERROR              3
#define COMMON_DEVICE_STATUS_DL_ERROR                   4

// Kiosk status
#define COMMON_DEVICE_STATUS_KIOSK_ACTIVE               3
#define COMMON_DEVICE_STATUS_KIOSK_NOT_ACTIVE           4

// Agency connection
#define COMMON_DEVICE_STATUS_CONNECTED                  3
#define COMMON_DEVICE_STATUS_NOT_CONNECTED              4

// Coin acceptor status
#define COMMON_DEVICE_STATUS_COIN_ACCEPTOR_JAM          3
#define COMMON_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR     4
#define COMMON_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL 5

#define COMMON_KIOSK_DOOR_1                             1
#define COMMON_KIOSK_DOOR_2                             2
#define COMMON_KIOSK_DOOR_3                             3
#define COMMON_KIOSK_DOOR_4                             4
#define COMMON_KIOSK_DOOR_5                             5

#define COMMON_PRIORITY_OK                              0
#define COMMON_PRIORITY_WARNING                         1
#define COMMON_PRIORITY_ERROR                           2

// Terminal Params constants
#define COMMON_KIOSK_MODE_MUSIC_ALWAYS                  1
#define COMMON_KIOSK_MODE_MUSIC_NEVER                   2
#define COMMON_KIOSK_MODE_MUSIC_PERIODIC                3
#define COMMON_KIOSK_AUTOPLAY_ENABLED                   1
#define COMMON_KIOSK_AUTOPLAY_DISABLED                  0

#define COMMON_SMARTCARD_TYPE_SINGLE_USE                1
#define COMMON_SMARTCARD_TYPE_PURSE                     2

// Settings constants
#define COMMON_MAX_CURRENCY_SYMBOL                      5   // The same of TRX_MAX_CURRENCY_SYMBOL in LKT Protocol
#define COMMON_MAX_DATE_FORMAT                          10  // The same of TRX_MAX_DATE_FORMAT in LKT Protocol
#define COMMON_MAX_TIME_FORMAT                          10  // The same of TRX_MAX_TIME_FORMAT in LKT Protocol

#define COMMON_CURRENCY_PREFIX                          0
#define COMMON_CURRENCY_SUFFIX                          1
#define COMMON_CURRENCY_PREFIX_SEP                      2
#define COMMON_CURRENCY_SUFFIX_SEP                      3

#define COMMON_ACCEPTOR_MAX_CHANNELS                    16
#define COMMON_NUM_NOTE_ACCEPTOR_CHANNELS               COMMON_ACCEPTOR_MAX_CHANNELS
#define COMMON_NUM_COIN_ACCEPTOR_CHANNELS               COMMON_ACCEPTOR_MAX_CHANNELS

// Games constants
#define COMMON_GAME_NAME_LENGTH                         20
#define COMMON_GAME_AUX_NAME_LENGTH                     40
#define COMMON_MAX_PRICE_LEVELS_PER_GAME                 5
#define COMMON_MAX_PRIZE_CATEGORIES                     20

// Kiosk constants
#define COMMON_KIOSK_SERIAL_NUMBER_LENGTH               20
#define COMMON_MAX_DEVICES                              16
#define COMMON_KIOSK_MAX_DEVICES                        COMMON_MAX_DEVICES 
#define COMMON_TRANSPORT_ADDRESS_LENGTH                 16
#define COMMON_KIOSK_MAX_BOOKS                          10000  // Number of supported books

// Tickets
#define COMMON_TICKET_PRIZE_DATA_LEN                    16
#define COMMON_NUM_PRIZE_TYPES                          5
#define COMMON_PRIZE_TYPE_NO_PRIZE                      0
#define COMMON_PRIZE_TYPE_NON_REFUNDABLE_PRIZE          1
#define COMMON_PRIZE_TYPE_MINOR_PRIZE                   2
#define COMMON_PRIZE_TYPE_MAJOR_PRIZE                   3
#define COMMON_PRIZE_TYPE_JACKPOT_CONTRIBUTION          4
#define COMMON_PRIZE_TYPE_JACKPOT_PRIZE                 5

// Agency constants 
#define COMMON_AGENCY_NAME_LENGTH                       20
#define COMMON_AGENCY_ADRESS_LENGTH                     80
#define COMMON_AGENCY_ZIP_CODE_LENGTH                   10
#define COMMON_AGENCY_PHONE_NUMBER_LENGTH               11

// User constants
#define COMMON_USER_NAME_LENGTH                         20

// Software Download
#define COMMON_MAX_SW_FILE_NAME_LENGTH                  40

#define COMMON_LOGGER_PARAMETER_SIZE                    80

#define LKAS_SEQUENCE_MULTIPLIER                        10000000000000L

#define COMMON_LEN_ENCRIPTED_RESULT_STRING              26

#define COMMON_UPPER_DISPLAY_LINE_SIZE                  50
#define COMMON_MAX_UPPER_DISPLAY_MESSAGE_LINES          2
#define COMMON_MAX_UPPER_DISPLAY_MESSAGES               5

#define COMMON_BOOK_NOT_ELIGIBLE_TO_JACKPOT             0
#define COMMON_BOOK_ELIGIBLE_TO_JACKPOT                 1

#define COMMON_MAX_BOOK_SALE_EVENTS                     8
// Software download
#define SW_DOWNLOAD_VERSION_LKAS                        0
#define SW_DOWNLOAD_VERSION_LKT                         1

#define COMMON_LKAS_MODULE_NAME                         _T("LKAS")
#define COMMON_LKT_MODULE_NAME                          _T("LKT_TERMINAL")

// Terminal Id
#define COMMON_TERMINAL_ID_LKAS                         0
#define COMMON_TERMINAL_ID_ALL                          999

#define COMMON_TERMINAL_STATUS_OK                       1
#define COMMON_TERMINAL_STATUS_ERROR                    2
#define COMMON_TERMINAL_STATUS_WARNING                  3
#define COMMON_TERMINAL_STATUS_NOT_ACTIVE               4
#define COMMON_TERMINAL_STATUS_UNKNOWN                  5

#define COMMON_MAX_KIOSK_LIST                           128 
#define COMMON_MAX_TERMINAL_LIST                        (1 + COMMON_MAX_KIOSK_LIST) // LKAS + LKTs

#define COMMON_EVENT_TYPE_OPERATION                     1
#define COMMON_EVENT_TYPE_DEVICE_STATUS                 2

#define COMMON_MAX_EVENTS                               COMMON_KIOSK_MAX_DEVICES

// Operations
#define COMMON_OPERATION_CODE_NO_OPERATION              0
#define COMMON_OPERATION_CODE_START                     1
#define COMMON_OPERATION_CODE_SHUTDOWN                  2
#define COMMON_OPERATION_CODE_RESTART                   3
#define COMMON_OPERATION_CODE_TUNE_SCREEN               4
#define COMMON_OPERATION_CODE_LAUNCH_EXPLORER           5
#define COMMON_OPERATION_CODE_ASSIGN_KIOSK              6  // Operation data -> kiosk id
#define COMMON_OPERATION_CODE_DEASSIGN_KIOSK            7  // Operation data -> kiosk id
#define COMMON_OPERATION_CODE_UNLOCK_DOOR               8
#define COMMON_OPERATION_CODE_LOGIN                     9  // Operation data -> user id
#define COMMON_OPERATION_CODE_LOGOUT                    10 // Operation data -> user id
#define COMMON_OPERATION_CODE_DISPLAY_SETTINGS          11
#define COMMON_OPERATION_CODE_DOOR_OPENED               12 // Operation data -> door id
#define COMMON_OPERATION_CODE_DOOR_CLOSED               13 // Operation data -> door id
#define COMMON_OPERATION_CODE_BLOCK_KIOSK               14
#define COMMON_OPERATION_CODE_UNBLOCK_KIOSK             15

// Online report
#define COMMON_NUM_ONLINE_REP_EVENTS                    4
#define COMMON_ONLINE_REP_EV_IDX_SALES                  0
#define COMMON_ONLINE_REP_EV_IDX_EVENTS                 1
#define COMMON_ONLINE_REP_EV_IDX_DEVICES                2
#define COMMON_ONLINE_REP_EV_IDX_VERSIONS               3

// Books in agency
#define COMMON_MAX_BOOKS_IN_AGENCY                      10000

#define COMMON_AG_BOOK_STATUS_UNKNOWN       0
#define COMMON_AG_BOOK_STATUS_ERROR         1
#define COMMON_AG_BOOK_STATUS_UNOPENED      2
#define COMMON_AG_BOOK_STATUS_OPENED        3
#define COMMON_AG_BOOK_STATUS_SOLD          4
#define COMMON_AG_BOOK_STATUS_LOCKED        5
#define COMMON_AG_BOOK_STATUS_RESERVED_6    6
#define COMMON_AG_BOOK_STATUS_RESERVED_7    7
#define COMMON_AG_BOOK_STATUS_RESERVED_8    8
#define COMMON_AG_BOOK_STATUS_RESERVED_9    9
#define COMMON_MAX_AG_BOOK_STATUS           10

#define COMMON_MAX_COMPONENTS               100 // VERSION_MAX_COMPONENTS

#define COMMON_MAX_LKAS_FUNCTIONS           100

#define COMMON_MAX_BCAST_JACKPOTS           5

#define COMMON_MAX_RANDOM_NUMBERS           1000

// Game types
typedef enum
{
    GAME_TYPE_UNKNOWN          = 0
  , GAME_TYPE_PREDRAWN         = 1
  , GAME_TYPE_RANDOM           = 2

} ENUM_GAME_TYPE;


enum ENUM_TERMINAL_TYPE
{
  TERMINAL_TYPE_LKAS = 0,
  TERMINAL_TYPE_LKT  = 1,
};

#define COMMON_PLAY_ITEM_MAX_BOOKS                5
#define COMMON_PLAY_ITEM_RAW_SIZE                 64


#define COMMON_MAX_METER_M_ITEMS                       20

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------
typedef enum
{
  LKAS_FUNC_NONE                  = 0,
  LKAS_FUNC_LOGIN                 = 1,
  LKAS_FUNC_LOGOUT                = 2,
  LKAS_FUNC_CHANGE_PWD            = 3,
  LKAS_FUNC_CASH                  = 4,
  LKAS_FUNC_RESERVED_05           = 5,
  LKAS_FUNC_RESERVED_06           = 6,
  LKAS_FUNC_RESERVED_07           = 7,
  LKAS_FUNC_RESERVED_08           = 8,
  LKAS_FUNC_RESERVED_09           = 9,
  LKAS_FUNC_RESERVED_10           = 10,

} ENUM_LKAS_FUNCTION_ID;  // COMMON_MAX_LKAS_FUNCTIONS

typedef enum
{
  LKAS_SEQUENCE_NONE                    = 0,
  LKAS_SEQUENCE_COLLECT_ID              = 1,
  LKAS_SEQUENCE_SMARTCARD_ID            = 2,
  LKAS_SEQUENCE_VOUCHER_ID              = 3,
  LKAS_SEQUENCE_EVENT_ID                = 4,
  LKAS_SEQUENCE_VOUCHER_HISTORY_ID      = 5,
  LKAS_SEQUENCE_PLAY_HISTORY_ID         = 6,
  LKAS_SEQUENCE_H_METER_ID              = 7,  
  LKAS_SEQUENCE_SESSION_ID              = 8,
  LKAS_SEQUENCE_MONEY_ID                = 9,
  LKAS_SEQUENCE_BOOK_ID                 = 10,
  LKAS_SEQUENCE_F_METER_ID              = 11,
  LKAS_SEQUENCE_LKT_METER_ID            = 12,
  LKAS_SEQUENCE_RESERVED_13             = 13,
  LKAS_SEQUENCE_RESERVED_14             = 14,
  LKAS_SEQUENCE_RESERVED_15             = 15,
  LKAS_SEQUENCE_RESERVED_16             = 16
  
} ENUM_LKAS_SEQUENCE_ID;

typedef enum
{
  SHUTDOWN_AUTOMATIC    = 0,
  SHUTDOWN_MANUAL       = 1,
  SHUTDOWN_POWER_FAIL   = 2,
  SHUTDOWN_LKC_REQUEST  = 3,
  REBOOT_LKC_REQUEST    = 4,

} ENUM_SHUTDOWN_TYPE;


typedef enum
{
  MONEY_TYPE_UNKNOWN    = 0,
  MONEY_TYPE_BANK_NOTE  = 1,
  MONEY_TYPE_SMARTCARD  = 2,
  MONEY_TYPE_COIN       = 3,
  MONEY_TYPE_VOUCHER    = 4,

} ENUM_MONEY_TYPE;

typedef enum
{
  MONEY_INOUT_UNKNOWN  = 0,
  MONEY_INOUT_IN       = 1,
  MONEY_INOUT_OUT      = 2,


} ENUM_MONEY_INOUT;


typedef enum
{
  MONEY_SOURCE_UNKNOWN       = 0,

  MONEY_SOURCE_CREDIT_LKAS   = 1,
  MONEY_SOURCE_VOUCHER       = 2,
  MONEY_SOURCE_SMARTCARD     = 3,
  MONEY_SOURCE_CREDIT_LKT    = 4,

  MONEY_SOURCE_NOTE_ACCEPTOR = 5,
  MONEY_SOURCE_COIN_ACCEPTOR = 6,

  MONEY_SOURCE_GAME_PRIZE    = 7,

} ENUM_MONEY_SOURCE;

typedef enum
{
  MONEY_TARGET_UNKNOWN     = 0,

  MONEY_TARGET_CREDIT_LKAS = 1,
  MONEY_TARGET_VOUCHER     = 2,
  MONEY_TARGET_SMARTCARD   = 3,
  MONEY_TARGET_CREDIT_LKT  = 4,
  MONEY_TARGET_GAME_PLAY   = 5,

  MONEY_TARGET_COIN_HOPPER = 10,

} ENUM_MONEY_TARGET;



typedef enum
{
  REDEEM_TO_LKAS_CREDIT = 1,
  REDEEM_TO_VOUCHER     = 2,
  REDEEM_TO_CARD        = 3,
  REDEEM_TO_LKT_CREDIT  = 4,

} ENUM_REDEEM_MODE;

#define COMMON_MAC_ADDRESS_SIZE                         8
#define COMMON_SERIAL_NUMBER_MAX_LENGTH                20



typedef enum
{
  SYSTEM_MODE_UNKNOWN = 0,
  SYSTEM_MODE_LYKOS   = 1,
  SYSTEM_MODE_WINGS   = 2,

} ENUM_SYSTEM_MODE;

typedef enum
{
  RNG_MODE_LOCAL    = 0,
  RNG_MODE_REMOTE   = 1,

} ENUM_RNG_MODE;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

#endif // __COMMON_CONSTANTS_CAT_H
