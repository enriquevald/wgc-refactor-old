//-----------------------------------------------------------------------------
// Copyright � 2003 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: CommonTypesCAT.h
//
//   DESCRIPTION: Common constants and types.
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked by compiling LKC, LKAS
//                 and LKT DLLs
//
//        AUTHOR: Toni Jord�
//
// CREATION DATE: 12-FEB-2003
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-FEB-2003 TJG    Initial release.
//
//------------------------------------------------------------------------------

#ifndef __COMMON_TYPES_CAT_H
#define __COMMON_TYPES_CAT_H

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------
typedef TCHAR TYPE_LOGGER_PARAM [COMMON_LOGGER_PARAMETER_SIZE + 1];

typedef TCHAR TYPE_COMMON_UPPER_DISPLAY_LINE [COMMON_UPPER_DISPLAY_LINE_SIZE + 1];

typedef struct
{
  WORD                            display_time;   // seconds
  WORD                            num_lines;
  TYPE_COMMON_UPPER_DISPLAY_LINE  line [COMMON_MAX_UPPER_DISPLAY_MESSAGE_LINES];

} TYPE_COMMON_UPPER_DISPLAY_MESSAGE;

typedef struct
{
  WORD                               num_messages;
  TYPE_COMMON_UPPER_DISPLAY_MESSAGE  message [COMMON_MAX_UPPER_DISPLAY_MESSAGES];

} TYPE_COMMON_UPPER_DISPLAY_LIST;

typedef struct
{
  u_long  on;
  u_long  time;
  u_long  interval;

} TYPE_COMMON_TCP_KEEP_ALIVE;

typedef struct
{
  TCHAR   data_1 [COMMON_TRANSPORT_ADDRESS_LENGTH + 1];
  TCHAR   data_2 [COMMON_TRANSPORT_ADDRESS_LENGTH + 1];

} TYPE_COMMON_LKC_ADDR;

typedef struct
{
  DWORD                line_type;
  DWORD                num_addr;
  TYPE_COMMON_LKC_ADDR addr_list [COMMON_MAX_LKC_ADDRESSES];

} TYPE_COMMON_LKC_ADDR_LIST;

typedef struct
{
  DWORD           date;
  WORD            last_ticket_id;

} TYPE_COMMON_BOOK_SALE_EVENT;

typedef struct
{
  TYPE_COMMON_BOOK_SALE_EVENT  event [COMMON_MAX_BOOK_SALE_EVENTS];

} TYPE_COMMON_BOOK_SALES_HISTORY;

// Events & devices structures

// XPS 14-MAY-2004
typedef struct
{
  BYTE  device_code;   
  BYTE  status; 
  BYTE  priority;

} TYPE_COMMON_DEVICE_STATUS;

typedef struct
{
  DWORD           date;
  WORD            terminal_id;     // In LKT case

  TYPE_COMMON_DEVICE_STATUS device_status;

} TYPE_COMMON_CURRENT_DEVICE_STATUS;

typedef struct
{
  DWORD   local_datetime;
  
  BYTE    type;         // COMMON_EVENT_TYPE_OPERATION
                        // COMMON_EVENT_TYPE_DEVICE_STATUS
  union
  {
    TYPE_COMMON_DEVICE_STATUS device_status;

    struct
    {
      BYTE    code;         // Operation or device code
      DWORD   data;         // Reserved data

    } operation;
  };

} TYPE_COMMON_EVENT;

typedef struct
{
  DWORD64            lkc_event_id;

  DWORD              terminal_internal_id;
  WORD               terminal_external_id;  // COMMON_TERMINAL_ID_LKAS
                                            // COMMON_TERMINAL_ID_ALL
                                            // # kiosk
  TYPE_COMMON_EVENT  event;

} TYPE_COMMON_HISTORICAL_EVENT;

typedef struct
{
  // PK
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;
  
} TYPE_COMMON_BOOK_KEY;

typedef struct
{
  WORD            game_id;
  DWORD           series_id;
  DWORD           book_id;
  WORD            ticket_id;
  
} TYPE_COMMON_TICKET_KEY;

typedef TYPE_COMMON_TICKET_KEY  TYPE_COMMON_PRIZE_VOUCHER_ID;

typedef struct
{
  DWORD64   x100_gross;      // Gross amount
  DWORD64   x100_net;        // Net amount
  DWORD64   x100_taxes;      // Taxes amount
  WORD      x100_pct_taxes;  // %Taxes 
  WORD      expiration_days; // Expiration Date

} TYPE_COMMON_PRIZE;

typedef struct
{
  DWORD64     voucher_audit_id;
  DWORD	    date;
  WORD	    terminal_id;
  BYTE	    type;
  union 
  {
    DWORD64                       cash_voucher_id;
    TYPE_COMMON_PRIZE_VOUCHER_ID  prize_voucher_id;
  };
  
  BYTE	    event;
  double      amount;

} TYPE_COMMON_HISTORICAL_VOUCHER;

// AJQ 27-APR-2004
typedef struct
{
  BYTE  mode;                           // reserved_00, DB_AGENCY_MODE_ONLINE  or DB_AGENCY_MODE_BATCH
  BYTE  sales_enabled;                  // reserved_01
  BYTE  auto_logout_enabled;            // reserved_02
  BYTE  pay_major_prize_allowed;        // reserved_03
  BYTE  confirmation_voucher_required;  // reserved_04
  BYTE  reserved_05;
  BYTE  reserved_06;
  BYTE  reserved_07;
  BYTE  reserved_08;
  BYTE  reserved_09;
  BYTE  reserved_10;
  BYTE  reserved_11;
  BYTE  reserved_12;
  BYTE  reserved_13;
  BYTE  reserved_14;
  BYTE  reserved_15;

} TYPE_COMMON_AGENCY_FLAGS;

// ACC & AJQ 25-OCT-2004
typedef BYTE  TYPE_COMMON_LKAS_FUNCTION_LIST [COMMON_MAX_LKAS_FUNCTIONS];  // != 0  --> ONLINE

typedef struct 
{
  TYPE_COMMON_LKAS_FUNCTION_LIST    function_list;

} TYPE_COMMON_ONLINE_PARAMS;

// AJQ 12-MAY-2004
typedef struct 
{
  TYPE_COMMON_BOOK_KEY  book_key;
  BYTE                  status;

} TYPE_COMMON_BOOK_STATUS;

// ACC 28-MAY-2004
// Versions
typedef struct
{
  DWORD  client;
  DWORD  build;

} TYPE_COMMON_VERSION_BASE;

typedef struct
{
  TYPE_COMMON_VERSION_BASE found;
  TYPE_COMMON_VERSION_BASE expected;

} TYPE_COMMON_VER_COMP;

typedef struct
{
  DWORD                     control_block;
  TYPE_COMMON_VERSION_BASE  installed_lkas_version;
  TYPE_COMMON_VERSION_BASE  installed_lkt_version;
  
  WORD                      num_lkas_components;
  TYPE_COMMON_VER_COMP      lkas_components [COMMON_MAX_COMPONENTS];

  WORD                      num_lkt_components;
  TYPE_COMMON_VER_COMP      lkt_components [COMMON_MAX_COMPONENTS];
  
} TYPE_COMMON_LKAS_LKT_VERSIONS;

typedef struct
{
  // Prize Plan Id
  WORD                    game_id;
  BYTE                    price_level_id;
  float                   ticket_price;

  float                   jackpot_contribution_per_unit;  // [0,1]
  DWORD                   reserved0;

  WORD      major_expiration;   
  BYTE      jackpot_flag;

  WORD      num_categories;
  struct
  {
    BYTE    type;
    double  gross_amount;
    double  net_amount;

  } category [COMMON_MAX_PRIZE_CATEGORIES];

} TYPE_COMMON_PRIZE_PLAN;

typedef struct
{
  BYTE  apply_limit;
  BYTE  show_counter;
  WORD  reset_interval;
  float cash_limit_per_game;

} TYPE_COMMON_GAME_COUNT;

typedef struct
{
  BYTE    cash;
  BYTE    coins_and_notes;
  BYTE    prize [COMMON_NUM_PRIZE_TYPES];  // ENUM_PRIZE_REDEEM

} TYPE_COMMON_REDEEM;

typedef struct
{
  BYTE  code;     
  WORD  model;         
  BYTE  port_type;     
  WORD  port_number;   

  BYTE  reserved[2];

} TYPE_COMMON_CUSTOM_DEVICE;

typedef struct
{
  WORD                      num_devices;         
  TYPE_COMMON_CUSTOM_DEVICE device [COMMON_MAX_DEVICES];     

} TYPE_COMMON_CUSTOM_DEVICE_LIST;




typedef struct
{
  DWORD     utc_date_time;      // Date time when jackpot was awarded
  DWORD64   amount_x100;        // Jackpot Amount multipled by 100.

  DWORD     agency_ext_id;
  DWORD     terminal_ext_id;

  TCHAR     agency_name [COMMON_AGENCY_NAME_LENGTH + 1];

} TYPE_COMMON_JACKPOT_BCAST_ITEM;   

typedef struct
{
  DWORD   control_block;
  DWORD   utc_date_time;            // Current date time (LKC)
  WORD    notification_period;      // Estimated number of seconds till next notification

  struct
  {
    WORD    num_jackpots;                       
    DWORD64 jackpot_x100 [COMMON_MAX_BCAST_JACKPOTS];   // Jackpot Amount multipled by 100.

  } current;  

  struct
  {
    WORD                            num_jackpots;        
    TYPE_COMMON_JACKPOT_BCAST_ITEM  jackpot [COMMON_MAX_BCAST_JACKPOTS];

  } history;                        

  BYTE  reserved [64];             

} TYPE_COMMON_JACKPOT_BCAST_LKC_DATA;     


// MAC address
typedef struct
{
  BYTE      data [COMMON_MAC_ADDRESS_SIZE];
  DWORD     physical_address_length;

} TYPE_COMMON_MAC_ADDRESS;

typedef struct
{
  TCHAR     string [COMMON_SERIAL_NUMBER_MAX_LENGTH + 1];

} TYPE_COMMON_SERIAL_NUMBER;

typedef struct
{
  BYTE  mode;                           // reserved_00, DB_AGENCY_MODE_ONLINE  or DB_AGENCY_MODE_BATCH
  BYTE  sales_enabled;                  // reserved_01
  BYTE  auto_logout_enabled;            // reserved_02
  BYTE  pay_major_prize_allowed;        // reserved_03
  BYTE  confirmation_voucher_required;  // reserved_04
  BYTE  display_player_time;            // reserved_05
  BYTE  purse_card;
  BYTE  system_mode;                    // SYSTEM_MODE: 0: Lykos, 1: Wings
  BYTE  rng_mode;
  BYTE  reserved_09;
  BYTE  reserved_10;
  BYTE  reserved_11;
  BYTE  reserved_12;
  BYTE  reserved_13;
  BYTE  reserved_14;
  BYTE  reserved_15;

} TYPE_COMMON_FLAGS;

typedef struct
{
  struct
  {
    FLOAT contrib_min_bet_amount;
    FLOAT contrib_percent;
    BYTE  award_flag;
    FLOAT award_min_bet_amount;
  
  } jackpot;

} TYPE_COMMON_RANDOM_GAME_DATA;

//
////// METERS /////////////////////////////////////
//

typedef struct
{
  double  amount;

} TYPE_COMMON_METER_AMOUNT;

typedef struct
{
  double  amount;
  DWORD   count;

} TYPE_COMMON_METER_AMOUNT_COUNT;

typedef struct
{
  // Total = CashIn + PrizeWon + NotRedeemable + Jackpot
  TYPE_COMMON_METER_AMOUNT  total;		
  TYPE_COMMON_METER_AMOUNT  cash_in;
  TYPE_COMMON_METER_AMOUNT  prizes_won;
  TYPE_COMMON_METER_AMOUNT  not_redeemable;

} TYPE_COMMON_METER_CREDIT;

typedef struct
{
  TYPE_COMMON_METER_AMOUNT        		cash_in;
  TYPE_COMMON_METER_AMOUNT        		cash_out;
  TYPE_COMMON_METER_AMOUNT_COUNT  		played;
  TYPE_COMMON_METER_AMOUNT_COUNT  		won;
  TYPE_COMMON_METER_AMOUNT_COUNT  		jackpots;
  TYPE_COMMON_METER_AMOUNT			      jackpot_contribution;
  TYPE_COMMON_METER_AMOUNT        		weighted_payback_percent;
  DWORD                               game_count;
  BYTE                                reserved [56];
  
} TYPE_COMMON_METER_K;

typedef struct
{
  TYPE_COMMON_METER_AMOUNT        		cash_in;
  TYPE_COMMON_METER_AMOUNT        		cash_out;
  TYPE_COMMON_METER_AMOUNT_COUNT  		played;
  TYPE_COMMON_METER_AMOUNT_COUNT  		won;
  TYPE_COMMON_METER_AMOUNT_COUNT  		jackpots;
  TYPE_COMMON_METER_AMOUNT			      jackpot_contribution;
  TYPE_COMMON_METER_AMOUNT        		weighted_payback_percent;

  TYPE_COMMON_METER_AMOUNT        		tax_01;
  TYPE_COMMON_METER_AMOUNT        		tax_02;

  BYTE                                reserved [44];
  
} TYPE_COMMON_METER_F;

typedef struct
{
  BYTE                            type;	      // MONEY_TYPE_XX
  float                           face_value;	// Face Value (0 for types other than �Coin�, �Note�)
  TYPE_COMMON_METER_AMOUNT_COUNT  in;
  TYPE_COMMON_METER_AMOUNT_COUNT  out;

} TYPE_COMMON_METER_M_ITEM;

typedef struct
{
  WORD                      num_meters;
  TYPE_COMMON_METER_M_ITEM  meter [COMMON_MAX_METER_M_ITEMS];

} TYPE_COMMON_METER_M;


typedef struct
{
  TYPE_COMMON_METER_AMOUNT        		cash_in;
  TYPE_COMMON_METER_AMOUNT        		cash_out;

  TYPE_COMMON_METER_AMOUNT_COUNT  		played;
  TYPE_COMMON_METER_AMOUNT_COUNT  		won;
  TYPE_COMMON_METER_AMOUNT_COUNT  		cashed;

  TYPE_COMMON_METER_CREDIT            credit;

} TYPE_COMMON_METER_P;

typedef struct
{
  DWORD                             utc_created;
  DWORD                             utc_modified;

  WORD                           		game_id;
  TYPE_COMMON_METER_AMOUNT_COUNT  	played;
  TYPE_COMMON_METER_AMOUNT_COUNT  	won;
  TYPE_COMMON_METER_AMOUNT_COUNT  	jackpots;
  TYPE_COMMON_METER_AMOUNT			    jackpot_contribution;
  TYPE_COMMON_METER_AMOUNT        	weighted_payback_percent;

} TYPE_COMMON_METER_G;

typedef struct
{
  DWORD                          		utc_hour;

  WORD                           		game_id;
  TYPE_COMMON_METER_AMOUNT_COUNT  	played;
  TYPE_COMMON_METER_AMOUNT_COUNT  	won;
  TYPE_COMMON_METER_AMOUNT_COUNT  	jackpots;
  TYPE_COMMON_METER_AMOUNT			    jackpot_contribution;
  TYPE_COMMON_METER_AMOUNT        	weighted_payback_percent;

} TYPE_COMMON_METER_H;

typedef struct
{
  WORD                              num_numbers;
  DWORD                             number[COMMON_MAX_RANDOM_NUMBERS];

}TYPE_COMMON_RANDOM_NUMBERS;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
// Macro to calculate array size
#define Common_ArraySize(_array)        ( sizeof (_array) / sizeof (_array[0]) )

// Macro to calculate TCHAR string size
#define Common_StringSize(_str)         Common_ArraySize (_str)

#endif // __COMMON_TYPES_CAT_H
