﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadUndo.cs
//  
//   DESCRIPTION: Class that implement methods to Reverse PinPad by thread.
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 27-JUN-2016
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-JUN-2016 ETP     First release.
// 06-SEP-2016 ETP     Fixed Bug 17128: Error enable PinPad
// 06-SEP-2016 ETP     Fixed Bug 18287: Error pinpad WCP thread
// 06-OCT-2016 FAV     Fixed Bug 18280: Televisa: Add 'Canceled' status for TPV transactions
// 22-OCT-2016 ETP     Fixed Bug 19702: General Param rename.
//------------------------------------------------------------------------------

using Banorte;
using Banorte.PinPad;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.PinPad;
using WSI.PinPad;

namespace WSI.PinPad.Service
{
  public static class PinPadUndo
  {
    # region " const "

    private const String EXCEPTION_WCP_PIN_PAD_UNDO_TASK = "Exception into class WCP_BucketsUpdate.";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    private const Int32 WAIT_MANUAL_RESET_EVENT_MS = 1000;
    private const String RESPONSE_MESSAGE_LANGUAGE = "ES";

    #endregion " const "

    private static Vx820Segura m_pinpad_vx820;

    #region " public methods "
    public static void Init()
    {
      try
      {
        m_pinpad_vx820 = new Vx820Segura(RESPONSE_MESSAGE_LANGUAGE);

        Thread _thread;
        _thread = new Thread(UndoPinPadThread);
        _thread.Name = "PinPadUndoThread";

        _thread.Start();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_PIN_PAD_UNDO_TASK + "Init" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
    } // Init


    /// <summary>
    /// Undo PinPad Thread
    /// </summary>
    static private void UndoPinPadThread()
    {
      Int32 _wait_hint = 0;
      Int32 _max_retry = 0;
      Int32 _num_cancel_transaction_pending;


      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);


         if (!Services.IsPrincipal("WCP"))
         {
           _wait_hint = 1000;

           continue;
         }

         _wait_hint = GeneralParam.GetInt32("PinPad", "UndoOperation.RetryInterval", 10000); //milisegundos
         _max_retry = GeneralParam.GetInt32("PinPad", "UndoOperation.MaxRetries", 20);

          // Process all pending sessions

          DataTable _dt_undo_pending_cancel;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Get Pending sessions from [pending_play_sessions_to_player_tracking]
            if (!GetPinPadPendingCancel(out _dt_undo_pending_cancel, _db_trx.SqlTransaction))
            {
              continue;
            }
            _num_cancel_transaction_pending = _dt_undo_pending_cancel.Rows.Count;

            // Not exists pending cancelations
            if (_num_cancel_transaction_pending == 0)
            {
              continue;
            }

            // Process transacction foreach pending cancel.
            foreach (DataRow _dr in _dt_undo_pending_cancel.Rows)
            {
              ProcessUndoPinPadOperation(_dr, _db_trx.SqlTransaction);
            }

            //Delete all transactions that has reached max limit
            if (!DeleteMaxRetryUndoPinPad(_max_retry, _db_trx.SqlTransaction))
            {
              continue;
            }

            _db_trx.Commit();

            Thread.Sleep(200);

          } // while
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          //Log.Warning(String.Format(EXCEPTION_WCP_BUCKETS_UPDATE_TASK + "BucketsUpdateThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        }
      }
    } // UndoPinPadThread

    #endregion " Public Methods"

    #region " Private Methods"

    /// <summary>
    /// Get PinPad Pending Cancel
    /// </summary>
    /// <param name="_data"></param>
    /// <param name="SqlTrans"></param>
    /// <returns></returns>
    private static Boolean GetPinPadPendingCancel(out DataTable _data, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _data = new DataTable();

      try
      {
        _sb.AppendLine("   SELECT TOP 10 UPPT_CONTROL_NUMBER,    ");
        _sb.AppendLine("                 UPPT_PINPADMODEL,       ");
        _sb.AppendLine("                 UPPT_TERMINAL_ID        ");
        _sb.AppendLine("     FROM    UNDO_PIN_PAD_TRANSACTION    ");
        _sb.AppendLine("    WHERE    UPPT_LAST_UPDATE < @pNow    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_data);
            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_PIN_PAD_UNDO_TASK + "GetPinPadPendingCancel" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } //GetPinPadPendingCancel

    /// <summary>
    /// Process Undo PinPad Operation
    /// </summary>
    /// <param name="_dr"></param>
    /// <param name="SQLTrans"></param>
    /// <returns></returns>
    private static Boolean ProcessUndoPinPadOperation(DataRow _dr, SqlTransaction SQLTrans)
    {
      String _control_number;
      String _terminal_id;
      PinPadType _pinpad_type;
      PinPadResponse _PinPadResponse;
      Boolean _response;

      _control_number = _dr["UPPT_CONTROL_NUMBER"].ToString();
      _pinpad_type = (PinPadType)Int32.Parse(_dr["UPPT_PINPADMODEL"].ToString());
      _terminal_id = _dr["UPPT_TERMINAL_ID"].ToString();


      if (Reverse(_control_number, _terminal_id , out _PinPadResponse))
      {
        _response = PinPadTransactions.DeleteFromUndoPinPad(_control_number, SQLTrans);
      }
      else
      {
        _response = UpdateUndoPinPad(_control_number, _pinpad_type, SQLTrans);
      }
      //_pinpad.ReleaseDevice();

      return _response;
    } //ProcessUndoPinPadOperation






    private static Boolean DeleteMaxRetryUndoPinPad(Int32 MaxRetry, SqlTransaction SQLTrans)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("DELETE FROM  UNDO_PIN_PAD_TRANSACTION             ");
        _sb.AppendLine("      WHERE  UPPT_CANCEL_RETRIES  >= @pMaxRetry    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTrans.Connection, SQLTrans))
        {
          _cmd.Parameters.Add("@pMaxRetry", SqlDbType.Int).Value = MaxRetry;

          _cmd.ExecuteNonQuery();
          return true;
        }
      }
      catch (Exception Ex)
      {
        Log.Error("DeleteFromUndoPinPad not saved Control Number");
        Log.Exception(Ex);
      }


      return false;
    } //DeleteMaxRetryUndoPinPad
    private static Boolean UpdateUndoPinPad(String ControlNumber, PinPadType PinPadModel, SqlTransaction SQLTrans)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("UPDATE  UNDO_PIN_PAD_TRANSACTION                       ");
        _sb.AppendLine("   SET  UPPT_CANCEL_RETRIES = UPPT_CANCEL_RETRIES + 1   ");
        _sb.AppendLine(" WHERE  UPPT_CONTROL_NUMBER    =  @pControlNumber      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTrans.Connection, SQLTrans))
        {
          _cmd.Parameters.Add("@pControlNumber", SqlDbType.NVarChar).Value = ControlNumber;
          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception Ex)
      {
        Log.Error("UpdateUndoPinPad not saved Control Number = " + ControlNumber);
        Log.Exception(Ex);
      }

      return false;
    } //DeleteFromUndoPinPad


    private static Boolean Reverse(String ControlNumber, String TerminalId, out PinPadResponse ResponseOut)
    {
      Hashtable _input_parameters = new Hashtable();
      Hashtable _output_parameters = new Hashtable();
      String _declinada_offline;
      String _codigo_banorte;

      ResponseOut = new PinPadResponse();
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;
      PinPadConfig _pinpad_config = new PinPadConfig("001");

      try
      {
        _input_parameters.Add(_pinpad_config.Atributes["CMD_TRANS"], "REVERSAL");
        _input_parameters.Add(_pinpad_config.Atributes["CONTROL_NUMBER"], ControlNumber);
        _input_parameters.Add(_pinpad_config.Atributes["MERCHANT_ID"], _pinpad_config.MerchandId);
        _input_parameters.Add(_pinpad_config.Atributes["USER"], _pinpad_config.User);
        _input_parameters.Add(_pinpad_config.Atributes["PASSWORD"], _pinpad_config.Password);
        _input_parameters.Add(_pinpad_config.Atributes["TERMINAL_ID"], TerminalId);
        _input_parameters.Add(_pinpad_config.Atributes["MODE"], _pinpad_config.ModeOperation);
        _input_parameters.Add(_pinpad_config.Atributes["RESPONSE_LANGUAGE"], _pinpad_config.Language);
        _input_parameters.Add(_pinpad_config.Atributes["BANORTE_URL"], _pinpad_config.URL);
        _input_parameters.Add(_pinpad_config.Atributes["TRANS_TIMEOUT"], "30");

        ConectorBanorte.sendTransaction(_input_parameters, _output_parameters);

        _declinada_offline = (String)_output_parameters[_pinpad_config.Atributes["CHIP_DECLINED"]];
        _codigo_banorte = (String)_output_parameters[_pinpad_config.Atributes["PAYW_RESULT"]];

        if (String.Equals(_codigo_banorte, "A"))
        {
          ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
          return true;
        }
        else
        {
          ResponseOut.ResponseMessage = ConvertSpanishTextToUTF8((String)_output_parameters[_pinpad_config.Atributes["TEXT"]]);
          if (ResponseOut.ResponseMessage.Contains("has been previously reversed") || ResponseOut.ResponseMessage.Contains("does not exist"))
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
            return true;
          }
          if (ResponseOut.ResponseMessage.Contains("does not exist") || ResponseOut.ResponseMessage.Contains("no existe"))
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
            return true;
          }

          ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    private static string ConvertSpanishTextToUTF8(string Text)
    {
      try
      {
        if (RESPONSE_MESSAGE_LANGUAGE == "ES")
        {
          byte[] bytes = Encoding.Default.GetBytes(Text);
          return Encoding.UTF8.GetString(bytes);
        }
      }
      catch
      {
      }

      return Text;
    }
    #endregion " Private Methods "
  } // PinPadUndo



}
