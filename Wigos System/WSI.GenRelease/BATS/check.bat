@echo Off

set OPERATION=%1
set TFSDIR=%2
set TFSUSER=%3
set TFSPASS=%4
set COMMENT=%5

set TFSEXE="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\TF.exe"
set TFSSRV="http://vstfs:8080/tfs/Gaming Products"
set TFSWORKSPACE="WSIGEN"
set HDDDIR="C:\Temp"

if not exist %HDDDIR% (
	mkdir %HDDDIR%
)

cd %HDDDIR%

echo %OPERATION%
if "%OPERATION%"=="IN" ( 
	echo "Check In %TFSDIR%"
  %TFSEXE% checkin %TFSDIR% /recursive /noprompt /comment:"%COMMENT%" /override:"Automated Build Process" /Login:%TFSUSER%,%TFSPASS%
) 
if "%OPERATION%"=="OUT" ( 
	echo "Check out %TFSDIR%"
  REM %TFSEXE% get %TFSDIR% /Login:%TFSUSER%,%TFSPASS%
  %TFSEXE% checkout %TFSDIR% /lock:checkin /Login:%TFSUSER%,%TFSPASS%
)

echo "END"

REM 2s. PAUSED
ping 127.0.0.1 -n 3 > nul