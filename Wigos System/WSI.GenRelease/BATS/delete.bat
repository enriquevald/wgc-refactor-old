@ECHO Off

IF EXIST "C:\Temp\" (

	IF EXIST "C:\Temp\Client_00\" (
		RMDIR "C:\Temp\Client_00\" /S /Q
	)
	
	IF EXIST "C:\Temp\W32\" (
		RMDIR "C:\Temp\W32\" /S /Q
	)
	
	IF EXIST "C:\Temp\Wigos System" (
		RMDIR "C:\Temp\Wigos System" /S /Q
	)
  
	IF EXIST "C:\Temp\License" (
		RMDIR "C:\Temp\License" /S /Q
	)
  
	IF EXIST "C:\Temp\QA" (
		RMDIR "C:\Temp\QA" /S /Q
	)
)