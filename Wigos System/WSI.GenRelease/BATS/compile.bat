REM cd "C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\"
REM 
REM 
REM IF %2 == "CLIENT" (
REM 
REM 	devenv %1 /rebuild "Release"
REM 	devenv %1 /rebuild "Debug"
REM )
REM   
REM IF %2 == "WGC" (
REM   devenv %1 /rebuild "Release|WIN32"
REM   REM	devenv %1 /rebuild "Debug|WIN32"
REM ) 
REM   
REM IF %2 == "ELSE" (
REM 	devenv %1 /rebuild "Debug|Any CPU"
REM 	devenv %1 /rebuild "Release|Any CPU"
REM )

ECHO OFF
cd "C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\"


set PATHCOMPILE=%1
set COMPILEMODE=%2

devenv %PATHCOMPILE% /rebuild %COMPILEMODE%

