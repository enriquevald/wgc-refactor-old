﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.GenRelease.Class
{

  class Users
  {

    private static IList<String> m_users;

    private static Boolean InitializeUsers()
    {
      Boolean _status;

      _status = false;

      try
      {
        m_users = new List<String> { };

        m_users.Add("Raul");
        m_users.Add("M.Piedra");
        m_users.Add("Y.Nunez");
        m_users.Add("D.Lasdiez");
        m_users.Add("Versionador");

        _status = true;
      }
      catch
      {
        _status = false;
      }

      return _status;
    }

    public static Boolean GetPermissionUser(String UserName)
    {
      Boolean _status;

      _status = false;

      if (InitializeUsers())
      {
        if (m_users.Contains(UserName))
        {
          _status = true;
        }
      }

      return _status;
    }

  }

}
