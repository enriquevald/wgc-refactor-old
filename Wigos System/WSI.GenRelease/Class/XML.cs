﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace WSI.GenRelease
{
  class XML
  {

    #region "Class"

    public XML(String Root, Boolean Encryption = false)
    {
      m_root_directory = Root;
      m_encryption = Encryption;
    }

    #endregion

    #region "Members"

    public String m_root_directory { get; set; }
    public Boolean m_encryption { get; set; }

    #endregion

    #region "Public Functions"

    public String ReturnNode(String Node)
    {
      String _node;

      _node = String.Empty;

      try
      {
        if (!String.IsNullOrEmpty(m_root_directory))
        {
          XmlDocument _document = new XmlDocument();
          _document.Load(m_root_directory);

          if (_document.GetElementsByTagName(Node).Count > 0)
          {
            XmlNode _element = _document.GetElementsByTagName(Node)[0];
            _node = _element.InnerText;

            if (m_encryption && _node != String.Empty)
            {
              _node = Crypto.DecryptString(_node);
            }

            _element = null;
            _document = null;
            GC.Collect();
          }
        }
      }
      catch (Exception)
      {
        _node = String.Empty;
      }

      return _node;
    }

    public Boolean WriteNode(String Node, String Text)
    {
      Boolean _ok;

      _ok = true;

      try
      {
        if (m_encryption)
        {
          Text = Crypto.EncryptString(Text);
        }

        XmlDocument _document = new XmlDocument();
        _document.Load(m_root_directory);

        if (_document.GetElementsByTagName(Node).Count > 0)
        {
          XmlNode _element = _document.GetElementsByTagName(Node)[0];
          _element.InnerText = Text;
          _document.Save(m_root_directory);

          _element = null;
          _document = null;
          GC.Collect();
        }
      }
      catch (Exception)
      {
        _ok = false;
      }

      return _ok;
    }

    public void CreateConfigXML()
    {
      if (!File.Exists(m_root_directory))
      {
        StringBuilder _sb = new StringBuilder();

        _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        _sb.AppendLine("<configuration>");
        _sb.AppendLine("<path_origin></path_origin>");
        _sb.AppendLine("<path_destiny></path_destiny>");
        _sb.AppendLine("<path_sprint></path_sprint>");
        _sb.AppendLine("<group></group>");
        _sb.AppendLine("<user></user>");
        _sb.AppendLine("</configuration>");

        XmlDocument _document = new XmlDocument();
        _document.LoadXml(_sb.ToString());
        XmlWriterSettings _config = new XmlWriterSettings();
        _config.Indent = true;

        using (XmlWriter _writer = XmlWriter.Create(m_root_directory, _config))
        {
          _document.Save(_writer);
        }

        _config = null;
        _document = null;
        GC.Collect();
      }
    }

    #endregion

  }
}
