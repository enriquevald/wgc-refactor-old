﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;

namespace WSI.GenRelease
{
  class Crypto
  {
    
    public static String EncryptString(String ToEncrypt)
    {
      Byte[] keyArray;
      Byte[] _string_to_encrypt = UTF8Encoding.UTF8.GetBytes(ToEncrypt);

      AppSettingsReader config = new AppSettingsReader();
      MD5CryptoServiceProvider hash = new MD5CryptoServiceProvider();
      keyArray = hash.ComputeHash(UTF8Encoding.UTF8.GetBytes("$WinSystems2014!"));
      hash.Clear();

      TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
      tdes.Key = keyArray;
      tdes.Mode = CipherMode.ECB;
      tdes.Padding = PaddingMode.PKCS7;

      ICryptoTransform cTransform = tdes.CreateEncryptor();

      Byte[] _final = cTransform.TransformFinalBlock(_string_to_encrypt, 0, _string_to_encrypt.Length);
      tdes.Clear();

      return Convert.ToBase64String(_final, 0, _final.Length);
    }

    public static String DecryptString(String ToDecrypt)
    {
      Byte[] keyArray;
      Byte[] _string_to_decrypt = Convert.FromBase64String(ToDecrypt);

      AppSettingsReader config = new AppSettingsReader();
      MD5CryptoServiceProvider hash = new MD5CryptoServiceProvider();
      keyArray = hash.ComputeHash(UTF8Encoding.UTF8.GetBytes("$WinSystems2014!"));
      hash.Clear();

      TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
      tdes.Key = keyArray;
      tdes.Mode = CipherMode.ECB;
      tdes.Padding = PaddingMode.PKCS7;

      ICryptoTransform cTransform = tdes.CreateDecryptor();

      Byte[] _final = cTransform.TransformFinalBlock(_string_to_decrypt, 0, _string_to_decrypt.Length);
      tdes.Clear();

      return UTF8Encoding.UTF8.GetString(_final);
    }
    
  }
}
