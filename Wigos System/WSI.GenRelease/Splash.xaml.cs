﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Threading;

namespace WSI.GenRelease
{
  /// <summary>
  /// Interaction logic for Splash.xaml
  /// </summary>
  public partial class Splash : Window
  {

    public Splash()
    {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      Storyboard _sb;
      DoubleAnimation _da;

      _sb = new Storyboard();
      _da = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromSeconds(2))) { AutoReverse = false };

      Storyboard.SetTargetProperty(_da, new PropertyPath(OpacityProperty));
      _da.Completed += new EventHandler(AnimationFinished);
      _sb.Children.Clear();
      _sb.Children.Add(_da);
      this.Opacity = 0;

      Show();
      _sb.Begin(this);
    }

    private void AnimationFinished(object sender, EventArgs e)
    {
      Thread.Sleep(2000);
      this.WindowState = System.Windows.WindowState.Minimized;
      this.Hide();

      MainWindow _frm = new MainWindow();
      _frm.Focus();
      _frm.Show();

      _frm = null;

      this.Close();
    }

  }

}
