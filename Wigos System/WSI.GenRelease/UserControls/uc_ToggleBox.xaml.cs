﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace WSI.GenRelease.UserControls
{

  public partial class uc_ToggleBox : UserControl
  {

    #region Builder

    public uc_ToggleBox()
    {
      InitializeComponent();
      InitiValues();
    }

    #endregion

    #region Members

    private Boolean m_status;
    private String m_name;

    #endregion

    #region Properties

    public Boolean IsChecked
    {
      get { return m_status; }
      set { m_status = value; }
    }

    public String LabelContent
    {
      get { return m_name; }
      set { m_name = value; SetContentLabel(); }
    }

    #endregion

    #region Form events

    private void gContent_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      if (IsChecked)
      {
        AnimationRightToLeft();
      }
      else
      {
        AnimationLeftToRight();
      }
      IsChecked = !IsChecked;
    }

    #endregion

    #region Functions

    private void AnimationLeftToRight()
    {
      ThicknessAnimationUsingKeyFrames _taukf = new ThicknessAnimationUsingKeyFrames();
      _taukf.BeginTime = TimeSpan.FromSeconds(0.0);
      _taukf.Duration = TimeSpan.FromSeconds(0.5);
      Storyboard.SetTargetProperty(_taukf, new PropertyPath(MarginProperty));
      Storyboard.SetTargetName(_taukf, "bPos");

      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(0, 0, 0, 0), KeyTime.Uniform));
      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(18, 0, 0, 0), KeyTime.Uniform));
      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(36, 0, 0, 0), KeyTime.Uniform));

      ColorAnimation _ca = new ColorAnimation();
      _ca.From = Colors.AliceBlue;
      _ca.To = Colors.LightGreen;
      Storyboard.SetTargetProperty(_ca, new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));
      Storyboard.SetTargetName(_ca, "bContent");
      _ca.Duration = TimeSpan.FromSeconds(0.5);
      _ca.AutoReverse = false;

      ColorAnimation _ca2 = new ColorAnimation();
      _ca2.From = Colors.AliceBlue;
      _ca2.To = Colors.LightGreen;
      Storyboard.SetTargetProperty(_ca2, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
      Storyboard.SetTargetName(_ca2, "bPos");
      _ca2.Duration = TimeSpan.FromSeconds(0.5);
      _ca2.AutoReverse = false;

      Storyboard _sb = new Storyboard();
      _sb.Children.Clear();
      _sb.Children.Add(_ca);
      _sb.Children.Add(_ca2);
      _sb.Children.Add(_taukf);
      _sb.Begin(this);
    }

    private void AnimationRightToLeft()
    {
      ThicknessAnimationUsingKeyFrames _taukf = new ThicknessAnimationUsingKeyFrames();
      _taukf.BeginTime = TimeSpan.FromSeconds(0.0);
      _taukf.Duration = TimeSpan.FromSeconds(0.5);
      Storyboard.SetTargetProperty(_taukf, new PropertyPath(MarginProperty));
      Storyboard.SetTargetName(_taukf, "bPos");

      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(36, 0, 0, 0), KeyTime.Uniform));
      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(18, 0, 0, 0), KeyTime.Uniform));
      _taukf.KeyFrames.Add(new SplineThicknessKeyFrame(new Thickness(0, 0, 0, 0), KeyTime.Uniform));

      ColorAnimation _ca = new ColorAnimation();
      _ca.From = Colors.LightGreen;
      _ca.To = Colors.AliceBlue;
      Storyboard.SetTargetProperty(_ca, new PropertyPath("(Border.Background).(SolidColorBrush.Color)"));
      Storyboard.SetTargetName(_ca, "bContent");
      _ca.Duration = TimeSpan.FromSeconds(0.5);
      _ca.AutoReverse = false;

      ColorAnimation _ca2 = new ColorAnimation();
      _ca2.From = Colors.LightGreen;
      _ca2.To = Colors.AliceBlue;
      Storyboard.SetTargetProperty(_ca2, new PropertyPath("(Border.BorderBrush).(SolidColorBrush.Color)"));
      Storyboard.SetTargetName(_ca2, "bPos");
      _ca2.Duration = TimeSpan.FromSeconds(0.5);
      _ca2.AutoReverse = false;

      Storyboard _sb = new Storyboard();
      _sb.Children.Clear();
      _sb.Children.Add(_ca);
      _sb.Children.Add(_ca2);
      _sb.Children.Add(_taukf);
      _sb.Begin(this);
    }

    private void InitiValues()
    {
      IsChecked = false;
      LabelContent = String.Empty;
    }

    private void SetContentLabel()
    {
      lbl_Title.Content = LabelContent;
    }

    #endregion

  }
}
