﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Diagnostics;
using WSI.GenRelease.Class;
using System.Windows.Media.Animation;
using WSI.GenRelease.UserControls;

namespace WSI.GenRelease
{

  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {

    #region Class

    public MainWindow()
    {
      InitializeComponent();
    }

    #endregion

    #region Constants

    private const String CONFIG_NAME_FILE = "config.xml";
    private const String PATH_TEMPORAL = "C:\\Temp\\";
    private const String PATH_WIGOS_TEMP = PATH_TEMPORAL + "Wigos System\\";
    private const String PATH_CASHIER_TEMP = PATH_TEMPORAL + "Wigos System\\WSI.Cashier\\";
    private const String PATH_WIGOS_RUNTIME = PATH_WIGOS_TEMP + "Runtime\\";
    private const String PATH_WGC_RELEASE = PATH_TEMPORAL + "W32\\Release\\WGC\\";
    private const String PATH_VISUAL_STUDIO = "C:\\Program Files (x86)\\Microsoft Visual Studio 12.0\\Common7\\IDE\\";

    #endregion

    #region Members

    private enum ERROR_UI
    {
      OK = 0,
      ANY_APPLICATION = 1,
      EMPTY = 2,
      EXCEPTION = 3,
      DIRECTORY_NOT_EXIST = 4,
      GROUP_EMPTY = 5,
      SPRINT_PATH_EMPTY = 6,
      BRANCH_EMPTY = 7,
      LABEL_EMPTY = 8,
    }

    private enum APPLICATION
    {
      GUI = 0,
      KERNEL = 1,
      WC2 = 2,
      WCP = 3,
      MS = 4,
      WWPC = 5,
      WWPS = 6,
      PSA_CLIENT = 7,
      AFIP_CLIENT = 8,

      RELEASE_GUI = 10,
      RELEASE_KERNEL = 11,
      RELEASE_WC2 = 12,
      RELEASE_WCP = 13,
      RELEASE_MS = 14,
      RELEASE_WWPC = 15,
      RELEASE_WWPS = 16,
      RELEASE_PSA_CLIENT = 17,
      RELEASE_AFIP_CLIENT = 18,

      SPRINT = 20,

    }

    private enum STEPS
    {
      GUI = 10,
      CASHIER = 6,
      MS = 15,
      WCP = 10,
      WC2 = 10,
      WWPC = 10,
      WWPS = 10,
      PSA_CLIENT = 10,
      AFIP_CLIENT = 10,
    }

    private enum TYPE_VERSION
    {
      RELEASE = 0,
      SPRINT = 1,
    }

    private IList<String> m_files = new List<String>();

    private String m_date;
    private String m_label_ss;
    private Int32 m_steps;
    private Int32 m_steps_taken;
    private ERROR_UI m_global_status;
    private TYPE_VERSION m_version_mode;
    private Boolean m_change_user;

    private DateTime m_start;
    private DateTime m_end;

    private delegate void CallWriteLog(String Text);
    private delegate void UpdateProgress(Int32 Value);
    private delegate void EnableControls(Boolean Value);

    // DELEGATE FOR READ CHECKBOX
    private delegate Boolean ReadToggleBox(uc_ToggleBox Check);

    // DELEGATE FOR READ TEXTBOX
    private delegate String ReadTextBox(TextBox Text);

    private volatile Boolean m_abort;
    private Thread m_thread;

    #endregion

    #region Properties

    public String User { get; set; }
    public String Password { get; set; }

    #endregion

    #region Form events

    private void txt_Group_PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
      if (!char.IsDigit(e.Text, e.Text.Length - 1))
      {
        e.Handled = true;
      }
      else
      {
        Int32 _number = Convert.ToInt32(e.Text);

        if (!(_number >= 1 && _number <= 4))
        {
          e.Handled = true;
        }
      }
    }

    private void btn_Source_Directory_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.FolderBrowserDialog _fbd_Finder = new System.Windows.Forms.FolderBrowserDialog();

      if (!String.IsNullOrEmpty(txt_Source_Directory.Text))
      {
        if (System.IO.Directory.Exists(txt_Source_Directory.Text))
        {
          _fbd_Finder.SelectedPath = txt_Source_Directory.Text;
        }
      }

      if (_fbd_Finder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        String _path = _fbd_Finder.SelectedPath;

        FindApplications(_path);
      }
    }

    private void btn_Destiny_Directory_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.FolderBrowserDialog _fbd_Finder = new System.Windows.Forms.FolderBrowserDialog();

      if (!String.IsNullOrEmpty(txt_Destiny_Directory.Text))
      {
        if (System.IO.Directory.Exists(txt_Destiny_Directory.Text))
        {
          _fbd_Finder.SelectedPath = txt_Destiny_Directory.Text;
        }
      }

      if (_fbd_Finder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        String _release_label = "Release YYYYMMDD HHMM -- MS";
        _release_label = _release_label.Replace("YYYYMMDD HHMM", m_date);

        txt_Destiny_Directory.Text = _fbd_Finder.SelectedPath + "\\" + _release_label;

        // GUI
        if ((Boolean)tb_GUI.IsChecked && !String.IsNullOrEmpty(txt_Version_GUI.Text))
        {
          txt_Destiny_GUI.Text = BuildLabelRelease(APPLICATION.RELEASE_GUI);
        }

        // CASHIER
        if ((Boolean)tb_CASHIER.IsChecked && !String.IsNullOrEmpty(txt_Version_CASHIER.Text))
        {
          txt_Destiny_CASHIER.Text = BuildLabelRelease(APPLICATION.RELEASE_KERNEL);
        }

        // WC2
        if ((Boolean)tb_WC2.IsChecked && !String.IsNullOrEmpty(txt_Version_WC2.Text))
        {
          txt_Destiny_WC2.Text = BuildLabelRelease(APPLICATION.RELEASE_WC2);
        }

        // WCP
        if ((Boolean)tb_WCP.IsChecked && !String.IsNullOrEmpty(txt_Version_WCP.Text))
        {
          txt_Destiny_WCP.Text = BuildLabelRelease(APPLICATION.RELEASE_WCP);
        }

        // MS
        if ((Boolean)tb_MS.IsChecked && !String.IsNullOrEmpty(txt_Version_MS.Text))
        {
          txt_Destiny_MS.Text = BuildLabelRelease(APPLICATION.RELEASE_MS);
        }

        // WWPC
        if ((Boolean)tb_WWPC.IsChecked && !String.IsNullOrEmpty(txt_Version_WWPC.Text))
        {
          txt_Destiny_WWPC.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPC);
        }

        // WWPS
        if ((Boolean)tb_WWPS.IsChecked && !String.IsNullOrEmpty(txt_Version_WWPS.Text))
        {
          txt_Destiny_WWPS.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPS);
        }

        // PSA
        if ((Boolean)tb_PSA.IsChecked && !String.IsNullOrEmpty(txt_Version_PSA.Text))
        {
          txt_Destiny_PSA.Text = BuildLabelRelease(APPLICATION.RELEASE_PSA_CLIENT);
        }

        // AFIP
        if ((Boolean)tb_AFIP.IsChecked && !String.IsNullOrEmpty(txt_Version_AFIP.Text))
        {
          txt_Destiny_AFIP.Text = BuildLabelRelease(APPLICATION.RELEASE_AFIP_CLIENT);
        }
      }
    }

    private void btn_Start_Click(object sender, RoutedEventArgs e)
    {
      switch (CheckChecks())
      {
        case ERROR_UI.EMPTY:
          ShowMessage("Error", "Contenido vació en algún textbox.", MessageBoxButton.OK);
          break;
        case ERROR_UI.EXCEPTION:
          ShowMessage("Error", "Excepción no controlada.", MessageBoxButton.OK);
          break;
        case ERROR_UI.ANY_APPLICATION:
          ShowMessage("Error", "No se ha seleccionado ninguna aplicación.", MessageBoxButton.OK);
          break;
        case ERROR_UI.GROUP_EMPTY:
          ShowMessage("Error", "Debe indicar un equipo de trabajo.", MessageBoxButton.OK);
          break;
        case ERROR_UI.SPRINT_PATH_EMPTY:
          ShowMessage("Error", "Se debe especificar la ruta para el sprint.", MessageBoxButton.OK);
          break;
        case ERROR_UI.BRANCH_EMPTY:
          ShowMessage("Error", "Se debe especificar la rama.", MessageBoxButton.OK);
          break;
        case ERROR_UI.LABEL_EMPTY:
          ShowMessage("Error", "Se debe especificar la label.", MessageBoxButton.OK);
          break;
        case ERROR_UI.OK:
          InitValues();
          SaveDirectories();
          m_thread = new Thread(new ThreadStart(ExecuteApplications));
          m_thread.Start();
          break;
      }
    }

    private void btn_Stop_Click(object sender, RoutedEventArgs e)
    {
      m_thread.Abort();
      m_abort = true;
      WriteLog("Proceso abortado: " + DateTime.Now);
      EnableButtons(true);
    }

    private void bnt_Close_Click(object sender, RoutedEventArgs e)
    {
      AnimationBlurStart();
      if (ShowMessage("Información", "¿Desea cerrar el aplicativo?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        this.Close();
      }
      else
      {
        AnimationBlurFinished();
      }
    }

    private void bnt_Min_Click(object sender, RoutedEventArgs e)
    {
      this.WindowState = System.Windows.WindowState.Minimized;
    }

    private void bnt_Close_MouseEnter(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Hand;
    }

    private void bnt_Close_MouseLeave(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Arrow;
    }

    private void bnt_Min_MouseEnter(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Hand;
    }

    private void bnt_Min_MouseLeave(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Arrow;
    }

    private void menu_Release_Click(object sender, RoutedEventArgs e)
    {
      ChangeVersionMode(TYPE_VERSION.RELEASE);
    }

    private void menu_Sprint_Click(object sender, RoutedEventArgs e)
    {
      ChangeVersionMode(TYPE_VERSION.SPRINT);
    }

    private void menu_ChangeUser_Click(object sender, RoutedEventArgs e)
    {
      m_change_user = true;
      LoadUser();
      ChangeVersionMode(m_version_mode);
    }

    private void btn_Sprint_Directory_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.FolderBrowserDialog _fbd_Finder = new System.Windows.Forms.FolderBrowserDialog();

      if (!String.IsNullOrEmpty(txt_Sprint_Source.Text))
      {
        if (System.IO.Directory.Exists(txt_Sprint_Source.Text))
        {
          _fbd_Finder.SelectedPath = txt_Sprint_Source.Text;
        }
      }

      if (_fbd_Finder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        String _path = _fbd_Finder.SelectedPath;

        if (!System.IO.Directory.Exists(_path + "\\Wigos System"))
        {
          txt_Sprint_Source.Text = String.Empty;
          ShowMessage("Error", "El directorio indicado, no contiene la esctructura de ficheros correcta", MessageBoxButton.OK);
        }
        else
        {
          txt_Sprint_Source.Text = _path;
        }
      }
    }

    private void Window_MouseDown(object sender, MouseButtonEventArgs e)
    {
      this.DragMove();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      m_change_user = true;
      LoadUser();
      ChangeVersionMode(TYPE_VERSION.SPRINT);
      WriteLog("--- Apertura del aplicativo: " + DateTime.Now.ToString());
      m_date = GetDate();
      SetToggleButtonNames();
      EnableButtons(true);
      LoadDirectories();
    }

    private void Window_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Escape)
      {
        AnimationBlurStart();
        if (ShowMessage("Información", "¿Desea cerrar el aplicativo?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
        {
          this.Close();
        }
        else
        {
          AnimationBlurFinished();
        }
      }
    }

    #endregion

    #region Form animations

    private void AnimationBlurStart()
    {
      var _storyboard = new Storyboard();
      var _doubleanimation = new DoubleAnimation(0.0, 10.0, new Duration(TimeSpan.FromSeconds(1))) { AutoReverse = false };
      Storyboard.SetTargetProperty(_doubleanimation, new PropertyPath(BlurEffect.RadiusProperty));
      Storyboard.SetTargetName(_doubleanimation, "blurEffect");
      _storyboard.Children.Clear();
      _storyboard.Children.Add(_doubleanimation);

      Show();
      _storyboard.Begin(this);
    }

    private void AnimationBlurFinished()
    {
      var _storyboard = new Storyboard();
      var _doubleanimation = new DoubleAnimation(10.0, 0.0, new Duration(TimeSpan.FromSeconds(1))) { AutoReverse = false };
      Storyboard.SetTargetProperty(_doubleanimation, new PropertyPath(BlurEffect.RadiusProperty));
      Storyboard.SetTargetName(_doubleanimation, "blurEffect");
      _storyboard.Children.Clear();
      _storyboard.Children.Add(_doubleanimation);

      Show();
      _storyboard.Begin(this);
    }

    #endregion

    #region Private Functions

    private void LoadDirectories()
    {
      XML _doc;
      String _origin;
      String _destiny;
      String _sprint;
      String _group;
      String _release_label;

      _doc = new XML(CONFIG_NAME_FILE, true);
      _origin = _doc.ReturnNode("path_origin");
      _destiny = _doc.ReturnNode("path_destiny");
      _sprint = _doc.ReturnNode("path_sprint");
      _group = _doc.ReturnNode("group");

      if (!String.IsNullOrEmpty(_origin) &&
          !String.IsNullOrEmpty(_destiny) &&
          !String.IsNullOrEmpty(_sprint) &&
          !String.IsNullOrEmpty(_group))
      {
        _release_label = "Release YYYYMMDD HHMM -- MS";
        _release_label = _release_label.Replace("YYYYMMDD HHMM", m_date);

        txt_Source_Directory.Text = _origin;
        txt_Destiny_Directory.Text = _destiny + _release_label;
        txt_Sprint_Source.Text = _sprint;
        txt_Group.Text = _group;

        FindApplications(_origin);
      }
    }

    private void SaveDirectories()
    {
      XML _doc;

      _doc = new XML(CONFIG_NAME_FILE, true);

      _doc.CreateConfigXML();
      _doc.WriteNode("path_origin", txt_Source_Directory.Text);
      _doc.WriteNode("path_destiny", txt_Destiny_Directory.Text.Substring(0, txt_Destiny_Directory.Text.LastIndexOf('\\') + 1));
      _doc.WriteNode("path_sprint", txt_Sprint_Source.Text);
      _doc.WriteNode("group", txt_Group.Text);
    }

    private void UserPrivileges()
    {
      mi_Release.IsEnabled = Users.GetPermissionUser(User);
    }

    private void SetToggleButtonNames()
    {
      tb_MS.LabelContent = "Multi Site";
      tb_GUI.LabelContent = "GUI";
      tb_CASHIER.LabelContent = "Cashier";
      tb_PSA.LabelContent = "PSA Client";
      tb_WC2.LabelContent = "WC2";
      tb_WCP.LabelContent = "WCP";
      tb_WWPC.LabelContent = "WWP Center";
      tb_WWPS.LabelContent = "WWP Site";
      tb_AFIP.LabelContent = "AFIP Client";
    }

    private Int32 CalculateProgress()
    {
      m_steps_taken++;
      return ((Int32)100 / m_steps) * m_steps_taken;
    }

    private void InitValues()
    {
      m_steps_taken = 0;
      m_abort = false;
      m_global_status = ERROR_UI.OK;
      CreateReleaseFolder();
      EnableButtons(false);
    }

    private void LoadUser()
    {
      lbl_User.Content = User;

      if (m_change_user)
      {
        ShowUserLogin();
        LoadUser();
      }
    }

    private void ShowUserLogin()
    {
      AnimationBlurStart();

      Login _frm_login = new Login();

      while (!_frm_login.m_Exit)
      {
        if (!String.IsNullOrEmpty(User))
        {
          _frm_login.User = User;
        }
        else
        {
          _frm_login.User = Environment.UserName;
        }
        _frm_login.Topmost = true;
        _frm_login.ShowDialog();
      }

      User = _frm_login.User;
      Password = Crypto.EncryptString(_frm_login.m_Password);

      m_change_user = String.IsNullOrEmpty(User) || String.IsNullOrEmpty(Password);

      _frm_login.Close();

      AnimationBlurFinished();
    }

    private void CreateReleaseFolder()
    {
      try
      {
        if (!System.IO.Directory.Exists(txt_Destiny_Directory.Text))
        {
          System.IO.Directory.CreateDirectory(txt_Destiny_Directory.Text);
        }

        if (!System.IO.Directory.Exists(txt_Destiny_Directory.Text + "\\MULTISITE"))
        {
          System.IO.Directory.CreateDirectory(txt_Destiny_Directory.Text + "\\MULTISITE");
        }

        if (!System.IO.Directory.Exists(txt_Destiny_Directory.Text + "\\SITE"))
        {
          System.IO.Directory.CreateDirectory(txt_Destiny_Directory.Text + "\\SITE");
        }

        if (!System.IO.Directory.Exists(txt_Destiny_Directory.Text + "\\SORTEADOR"))
        {
          System.IO.Directory.CreateDirectory(txt_Destiny_Directory.Text + "\\SORTEADOR");
        }
      }
      catch (Exception _ex)
      {
        System.Windows.MessageBox.Show(_ex.Message);
      }
    }

    private void ExecuteApplications()
    {
      DeleteTempFiles();

      // Download solutions from repository
      if (m_version_mode == TYPE_VERSION.RELEASE)
      {
        BuildLabel("Release YYYYMMDD HHMM -- MS");

        m_global_status = DownloadCode(PATH_WIGOS_TEMP.TrimEnd('\\'), "$/Wigos Project/" + ReadText(txt_branch));
        //m_global_status = DownloadCode(PATH_TEMPORAL + "\"Client_00\"", "\"$/Wigos Dev/Client_00\"");
      }
      else if (m_version_mode == TYPE_VERSION.SPRINT)
      {
        // Copy solutions from specific path
        m_global_status = CopyFiles(APPLICATION.SPRINT);
      }

      if (ReadCheck(tb_GUI) || ReadCheck(tb_MS))
      {
        // m_global_status = CopyFiles(APPLICATION.GUI);

        // Compile Solution WGC GUI.sln
        // m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC GUI.sln"), "Debug|Any CPU");
        // m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC GUI.sln"), "Release|Any CPU");
      }

      // Compile Solution WGC.sln       
      m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC.sln"), "Debug|WIN32");
      m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC.sln"), "Release|WIN32");

      m_global_status = CopyFiles(APPLICATION.KERNEL);

      if (ReadCheck(tb_WCP))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          m_global_status = CheckFiles(APPLICATION.WCP, "OUT");
          m_global_status = ModifyFile(APPLICATION.WCP);
          m_global_status = CheckFiles(APPLICATION.WCP, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.WCP);
        }
      }

      if (ReadCheck(tb_WC2))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          CheckFiles(APPLICATION.WC2, "OUT");
          m_global_status = ModifyFile(APPLICATION.WC2);
          CheckFiles(APPLICATION.WC2, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.WC2);
        }
      }

      if (ReadCheck(tb_WWPC))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          m_global_status = CheckFiles(APPLICATION.WWPC, "OUT");
          m_global_status = ModifyFile(APPLICATION.WWPC);
          m_global_status = CheckFiles(APPLICATION.WWPC, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.WWPC);
        }
      }

      if (ReadCheck(tb_WWPS))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          m_global_status = CheckFiles(APPLICATION.WWPS, "OUT");
          m_global_status = ModifyFile(APPLICATION.WWPS);
          m_global_status = CheckFiles(APPLICATION.WWPS, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.WWPS);
        }
      }

      if (ReadCheck(tb_PSA))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          m_global_status = CheckFiles(APPLICATION.PSA_CLIENT, "OUT");
          m_global_status = ModifyFile(APPLICATION.PSA_CLIENT);
          m_global_status = CheckFiles(APPLICATION.PSA_CLIENT, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.PSA_CLIENT);
        }
      }

      if (ReadCheck(tb_AFIP))
      {
        if (m_version_mode == TYPE_VERSION.RELEASE)
        {
          m_global_status = CheckFiles(APPLICATION.AFIP_CLIENT, "OUT");
          m_global_status = ModifyFile(APPLICATION.AFIP_CLIENT);
          m_global_status = CheckFiles(APPLICATION.AFIP_CLIENT, "IN");
        }
        else if (m_version_mode == TYPE_VERSION.SPRINT)
        {
          m_global_status = ModifyFile(APPLICATION.AFIP_CLIENT);
        }
      }

      // Compile Solution WGC Kernel.sln
      m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC Kernel.sln"), "Debug|Any CPU");
      m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC Kernel.sln"), "Release|Any CPU");

      if (ReadCheck(tb_GUI) || ReadCheck(tb_MS))
      {
        m_global_status = CopyFiles(APPLICATION.GUI);

        // Compile Solution WGC Client00.sln
        m_global_status = CompileCode(System.IO.Path.Combine(PATH_TEMPORAL, "Client_00\\WGC Client_00.sln"), "Release");

        // Compile Solution WGC GUI.sln
        m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC GUI.sln"), "Debug|Any CPU");
        m_global_status = CompileCode(System.IO.Path.Combine(PATH_WIGOS_TEMP, "WGC GUI.sln"), "Release|Any CPU");

        m_global_status = CopyFiles(APPLICATION.RELEASE_GUI);
        m_global_status = MakeDIST(APPLICATION.GUI);
        m_global_status = BuildLKCPaket(APPLICATION.GUI);
        m_global_status = MoveLKCPaket(APPLICATION.GUI);

        if (ReadCheck(tb_MS))
        {
          m_global_status = DataDuplicate();
          m_global_status = MakeDIST(APPLICATION.MS);
          m_global_status = BuildLKCPaket(APPLICATION.MS);
          m_global_status = MoveLKCPaket(APPLICATION.MS);
          m_global_status = RenameManifest(APPLICATION.MS);
        }
      }

      if (ReadCheck(tb_CASHIER))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_KERNEL);
        m_global_status = MakeDIST(APPLICATION.KERNEL);
        m_global_status = BuildLKCPaket(APPLICATION.KERNEL);
        m_global_status = MoveLKCPaket(APPLICATION.KERNEL);
      }

      if (ReadCheck(tb_WCP))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_WCP);
        m_global_status = BuildLKCPaket(APPLICATION.WCP);
        m_global_status = MoveLKCPaket(APPLICATION.WCP);
      }

      if (ReadCheck(tb_WC2))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_WC2);
        m_global_status = BuildLKCPaket(APPLICATION.WC2);
        m_global_status = MoveLKCPaket(APPLICATION.WC2);
      }

      if (ReadCheck(tb_WWPC))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_WWPC);
        m_global_status = BuildLKCPaket(APPLICATION.WWPC);
        m_global_status = MoveLKCPaket(APPLICATION.WWPC);
      }

      if (ReadCheck(tb_WWPS))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_WWPS);
        m_global_status = BuildLKCPaket(APPLICATION.WWPS);
        m_global_status = MoveLKCPaket(APPLICATION.WWPS);
      }

      if (ReadCheck(tb_PSA))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_PSA_CLIENT);
        m_global_status = BuildLKCPaket(APPLICATION.PSA_CLIENT);
        m_global_status = MoveLKCPaket(APPLICATION.PSA_CLIENT);
      }

      if (ReadCheck(tb_AFIP))
      {
        m_global_status = CopyFiles(APPLICATION.RELEASE_AFIP_CLIENT);
        m_global_status = BuildLKCPaket(APPLICATION.AFIP_CLIENT);
        m_global_status = MoveLKCPaket(APPLICATION.AFIP_CLIENT);
      }

      // General check out!
      if (m_version_mode == TYPE_VERSION.RELEASE)
      {
        /* m_global_status = CheckFiles(APPLICATION.KERNEL, "OUT");
         m_global_status = ModifyFile(APPLICATION.KERNEL);
         m_global_status = CheckFiles(APPLICATION.KERNEL, "IN");*/
      }

      DeleteTempFiles();

      UpdateProgressValue(100);

      WriteLog("\n");
      WriteLog("--- Ejecución finalizada: " + DateTime.Now);

      EnableButtons(true);
    }

    private ERROR_UI ModifyFile(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        UpdateProgressValue(CalculateProgress());

        String _line = String.Empty;
        String _file = String.Empty;
        String _string_to_find = "private const string VERSION =";
        String _version = String.Empty;
        Boolean _exit;
        Encoding _encoding;

        switch (Applications)
        {
          case APPLICATION.WCP:
            _file = PATH_WIGOS_TEMP + "WSI.Service\\WCP_Service.cs";
            _version = ReadText(txt_Version_WCP);
            break;

          case APPLICATION.WC2:
            _file = PATH_WIGOS_TEMP + "WSI.WC2_Service\\WC2_Service.cs";
            _version = ReadText(txt_Version_WC2);
            break;

          case APPLICATION.WWPC:
            _file = PATH_WIGOS_TEMP + "WSI.WWP_Server\\WWP_CenterService.cs";
            _version = ReadText(txt_Version_WWPC);
            break;

          case APPLICATION.WWPS:
            _file = PATH_WIGOS_TEMP + "WSI.WWP_Client\\WWP_SiteService.cs";
            _version = ReadText(txt_Version_WWPS);
            break;

          case APPLICATION.PSA_CLIENT:
            _file = PATH_WIGOS_TEMP + "WSI.PSA_Client\\PSA_ClientService.cs";
            _version = ReadText(txt_Version_PSA);
            break;

          case APPLICATION.AFIP_CLIENT:
            _file = PATH_WIGOS_TEMP + "WSI.AFIP_Client\\AFIP_ClientService.cs";
            _version = ReadText(txt_Version_AFIP);
            break;

          case APPLICATION.KERNEL:

            _string_to_find = "NOTHING TO FIND!!! :( ";
            _file = PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\AdobeReader.cs";
            var lines = System.IO.File.ReadAllLines(_file);
            lines[0] = lines[0];
            System.IO.File.WriteAllLines(_file, lines, Encoding.GetEncoding(1250));

            break;
        }

        using (System.IO.StreamReader _sr = new System.IO.StreamReader(_file))
        {
          _exit = false;
          _encoding = _sr.CurrentEncoding;
          while (!(_sr.EndOfStream) && !(_exit))
          {
            _line = _sr.ReadLine();

            if (!(String.IsNullOrEmpty(_line)) && (_line.Contains(_string_to_find)))
            {
              _exit = true;
            }
          }
        }

        if (!(String.IsNullOrEmpty(_line)))
        {
          // NORMAL
          System.IO.File.SetAttributes(_file, System.IO.FileAttributes.Normal);

          String texto = System.IO.File.ReadAllText(_file, Encoding.GetEncoding(1250));
          texto = texto.Replace(_line, _line.Split('=')[0] + "= \"18." + _version + "\";");
          System.IO.File.WriteAllText(_file, texto, Encoding.GetEncoding(1250));

          // READ ONLY
          System.IO.File.SetAttributes(_file, System.IO.FileAttributes.ReadOnly);
        }

      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private ERROR_UI CheckFiles(APPLICATION Applications, String Action)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        UpdateProgressValue(CalculateProgress());

        using (Process _process = new Process())
        {
          ProcessStartInfo _info = new ProcessStartInfo();

          _info.WindowStyle = ProcessWindowStyle.Normal;
          _info.FileName = "BATS\\check.bat";

          switch (Applications)
          {
            case APPLICATION.WCP:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.Service/WCP_Service.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " WCP_VersionUpdated";
              break;
            case APPLICATION.WC2:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.WC2_Service/WC2_Service.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " WC2_VersionUpdated";
              break;
            case APPLICATION.WWPC:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.WWP_Server/WWP_CenterService.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " WWPC_VersionUpdated";
              break;
            case APPLICATION.WWPS:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.WWP_Client/WWP_SiteService.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " WWPS_VersionUpdated";
              break;
            case APPLICATION.PSA_CLIENT:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.PSA_Client/PSA_ClientService.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " PSA_CLIENT_VersionUpdated";
              break;
            case APPLICATION.AFIP_CLIENT:
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WSI.AFIP_Client/AFIP_ClientService.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + " AFIP_CLIENT_VersionUpdated";
              break;
            case APPLICATION.KERNEL: // General check in
              String _release_label = "Release YYYYMMDD HHMM";
              _release_label = _release_label.Replace("YYYYMMDD HHMM", m_date);
              _release_label = _release_label.Replace(" ", "_");
              _info.Arguments = Action + " \"$/Wigos Project/" + ReadText(txt_branch) + "/Wigos System/WGC/Kernel/WSI.Common/AdobeReader.cs\" " + User + " " + Crypto.DecryptString(Password) + " " + _release_label;
              break;

          }

          _process.StartInfo = _info;
          _process.Start();
          _process.WaitForExit();
        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private ERROR_UI DeleteFile(String File)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        UpdateProgressValue(CalculateProgress());

        if (System.IO.File.Exists(File))
        {
          System.IO.File.SetAttributes(File, System.IO.FileAttributes.Normal);
          System.IO.File.Delete(File);
        }
      }
      catch (Exception)
      {
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private ERROR_UI DataDuplicate()
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        UpdateProgressValue(CalculateProgress());

        String _multisite_path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS) + "\\Bin\\";
        String _gui_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\Bin\\";

        if (!System.IO.Directory.Exists(_multisite_path)) { System.IO.Directory.CreateDirectory(_multisite_path); }
        if (System.IO.Directory.Exists(_gui_path))
        {
          CopyDirectories(_gui_path, _multisite_path, true);

          if (System.IO.File.Exists(_multisite_path + "WigosGUI.exe"))
          {
            System.IO.File.Copy(_multisite_path + "WigosGUI.exe", _multisite_path + "WigosMultiSiteGUI.exe");
            System.IO.File.Delete(_multisite_path + "WigosGUI.exe");
          }

          foreach (String _file in System.IO.Directory.GetFiles(_multisite_path))
          {
            if (_file.Contains(".DIST"))
            {
              System.IO.File.SetAttributes(_file, System.IO.FileAttributes.Normal);
              System.IO.File.Delete(_file);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private ERROR_UI CopyFiles(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;
      String _file_copy;

      _status = ERROR_UI.OK;

      StartLog("\nCopiando ficheros.");

      try
      {
        UpdateProgressValue(CalculateProgress());

        switch (Applications)
        {
          case APPLICATION.KERNEL:
            if (!System.IO.Directory.Exists(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\"))
            {
              System.IO.Directory.CreateDirectory(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\");
            }

            if (!System.IO.Directory.Exists(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release\\"))
            {
              System.IO.Directory.CreateDirectory(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release\\");
            }

            m_files = new List<String>();
            m_files.Add(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll");
            m_files.Add(PATH_WIGOS_RUNTIME + "POSPrinter.ocx");

            string[] _files = System.IO.Directory.GetFiles(PATH_WIGOS_RUNTIME + "IDCardScanner");

            foreach (String _file in _files)
            {
              m_files.Add(_file);
            }

            //m_files.Add(PATH_WIGOS_RUNTIME + "IDCardScanner\\Interop.SCANWLib.dll");
            //m_files.Add(PATH_WIGOS_RUNTIME + "IDCardScanner\\Interop.SCANWEXLib.dll");
            //m_files.Add(PATH_WIGOS_RUNTIME + "IDCardScanner\\WSI.IDCardScanner.dll");
            //m_files.Add(PATH_WIGOS_RUNTIME + "IDCardScanner\\ACCUANT_SDK_SETUP.bat");
            //m_files.Add(PATH_WIGOS_RUNTIME + "IDCardScanner\\ACCUANT_SDK_SETUP.exe");

            foreach (String _file in m_files)
            {
              _file_copy = _file.Replace("IDCardScanner\\", "");
              _file_copy = _file_copy.Replace(PATH_WIGOS_RUNTIME, PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\");

              if (System.IO.File.Exists(_file_copy))
              {
                System.IO.File.SetAttributes(_file_copy, System.IO.FileAttributes.Normal);
              }

              System.IO.File.Copy(_file, _file_copy, true);
              System.IO.File.Copy(_file, _file_copy.Replace("Debug", "Release"), true);
            }


            CopyRuntimeVS(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug");
            CopyRuntimeVS(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release");

            break;
          case APPLICATION.GUI:

            if (System.IO.Directory.Exists(PATH_WIGOS_RUNTIME))
            {
              if (!System.IO.Directory.Exists(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug"))
              {
                System.IO.Directory.CreateDirectory(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\");
              }

              if (!System.IO.Directory.Exists(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release"))
              {
                System.IO.Directory.CreateDirectory(PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release\\");
              }

              m_files = new List<String>();

              // RUNTIME
              m_files.Add(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "AxInterop.MSChart20Lib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "AxInterop.MSFlexGridLib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "Interop.MSChart20Lib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "POSPrinter.ocx");
              m_files.Add(PATH_WIGOS_RUNTIME + "Interop.MSFlexGridLib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "mschrt20.ocx");
              m_files.Add(PATH_WIGOS_RUNTIME + "MSFLXGRD.OCX");
              m_files.Add(PATH_WIGOS_RUNTIME + "vsFlex8\\AxInterop.VSFlex8.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "vsFlex8\\Interop.VSFlex8.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "vsFlex8\\vsFlex8.ocx");
              m_files.Add(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll");
              m_files.Add(PATH_WIGOS_RUNTIME + "POSPrinter.ocx");


              foreach (String _file in m_files)
              {
                if (System.IO.File.Exists(_file))
                {
                  System.IO.File.SetAttributes(_file, System.IO.FileAttributes.Normal);
                }
              }

              // DEBUG
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\Interop.POSPRINTERLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "POSPrinter.ocx", PATH_TEMPORAL + "W32\\Debug\\WGC\\POSPrinter.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "AxInterop.MSChart20Lib.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\AxInterop.MSChart20Lib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "AxInterop.MSFlexGridLib.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\AxInterop.MSFlexGridLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.MSChart20Lib.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\Interop.MSChart20Lib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.MSFlexGridLib.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\Interop.MSFlexGridLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "mschrt20.ocx", PATH_TEMPORAL + "W32\\Debug\\WGC\\mschrt20.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "MSFLXGRD.OCX", PATH_TEMPORAL + "W32\\Debug\\WGC\\MSFLXGRD.OCX", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\AxInterop.VSFlex8.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\AxInterop.VSFlex8.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\Interop.VSFlex8.dll", PATH_TEMPORAL + "W32\\Debug\\WGC\\Interop.VSFlex8.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\vsFlex8.ocx", PATH_TEMPORAL + "W32\\Debug\\WGC\\vsFlex8.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll", PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\Interop.POSPRINTERLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "POSPrinter.ocx", PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Debug\\POSPrinter.ocx", true);

              CopyRuntimeVS(PATH_TEMPORAL + "W32\\Debug\\WGC");

              //RELEASE           PATH_WIGOS_RUNTIME + 
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll", PATH_WGC_RELEASE + "Interop.POSPRINTERLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "POSPrinter.ocx", PATH_WGC_RELEASE + "POSPrinter.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "AxInterop.MSChart20Lib.dll", PATH_WGC_RELEASE + "AxInterop.MSChart20Lib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "AxInterop.MSFlexGridLib.dll", PATH_WGC_RELEASE + "AxInterop.MSFlexGridLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.MSChart20Lib.dll", PATH_WGC_RELEASE + "Interop.MSChart20Lib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.MSFlexGridLib.dll", PATH_WGC_RELEASE + "Interop.MSFlexGridLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "mschrt20.ocx", PATH_WGC_RELEASE + "mschrt20.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "MSFLXGRD.OCX", PATH_WGC_RELEASE + "MSFLXGRD.OCX", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\AxInterop.VSFlex8.dll", PATH_WGC_RELEASE + "AxInterop.VSFlex8.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\Interop.VSFlex8.dll", PATH_WGC_RELEASE + "Interop.VSFlex8.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "vsFlex8\\vsFlex8.ocx", PATH_WGC_RELEASE + "vsFlex8.ocx", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "Interop.POSPRINTERLib.dll", PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release\\Interop.POSPRINTERLib.dll", true);
              System.IO.File.Copy(PATH_WIGOS_RUNTIME + "POSPrinter.ocx", PATH_WIGOS_TEMP + "WGC\\Kernel\\WSI.Common\\bin\\Release\\POSPrinter.ocx", true);
              CopyRuntimeVS(PATH_WGC_RELEASE);
            }
            else
            {
              _status = ERROR_UI.DIRECTORY_NOT_EXIST;
            }

            break;

          case APPLICATION.RELEASE_GUI:

            if (System.IO.Directory.Exists(ReadText(txt_Source_GUI)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_GUI), "Bin");
              String _file_target;
              String _src_dir = PATH_WGC_RELEASE;
              String _src_file;

              CopyDirectories(ReadText(txt_Source_GUI), _dir_target.Replace("Bin", ""), true);

              m_files = new List<string>();
              m_files.Clear();

              m_files.Add(PATH_WGC_RELEASE + "WigosGUI.exe");
              m_files.Add(PATH_WGC_RELEASE + "GUI_Controls.dll");
              m_files.Add(PATH_WGC_RELEASE + "GUI_Reports.dll");
              m_files.Add(PATH_WGC_RELEASE + "GUI_CommonMisc.dll");
              m_files.Add(PATH_WGC_RELEASE + "GUI_CommonOperations.dll");
              m_files.Add(PATH_WGC_RELEASE + "NLS_009.dll");
              m_files.Add(PATH_WGC_RELEASE + "NLS_010.dll");
              m_files.Add(PATH_WGC_RELEASE + "NLS_012.dll");
              m_files.Add(PATH_WGC_RELEASE + "en-US_WSI.Common.resources.dll");
              m_files.Add(PATH_WGC_RELEASE + "es_WSI.Common.resources.dll");
              m_files.Add(PATH_WGC_RELEASE + "fr_WSI.Common.resources.dll");
              m_files.Add(PATH_WGC_RELEASE + "es-MX_WSI.Common.resources.dll");
              m_files.Add(PATH_WGC_RELEASE + "WSI.Common.dll");
              m_files.Add(PATH_WGC_RELEASE + "CommonBase.dll");
              if (System.IO.File.Exists(PATH_WGC_RELEASE + "ThoughtWorks.QRCode.dll"))
                m_files.Add(PATH_WGC_RELEASE + "ThoughtWorks.QRCode.dll");
              if (System.IO.File.Exists(PATH_WGC_RELEASE + "WSI.Entrances.BlackList.Model.dll"))
                m_files.Add(PATH_WGC_RELEASE + "WSI.Entrances.BlackList.Model.dll");
              if (System.IO.File.Exists(PATH_WGC_RELEASE + "WigosModel.dll"))
                m_files.Add(PATH_WGC_RELEASE + "WigosModel.dll");
              if (System.IO.File.Exists(PATH_WGC_RELEASE + "WigosCommon.dll"))
                m_files.Add(PATH_WGC_RELEASE + "WigosCommon.dll");
              if (System.IO.File.Exists(PATH_WGC_RELEASE + "WSI.Entrances.BlackList.Model.dll"))
                m_files.Add(PATH_WGC_RELEASE + "WSI.Entrances.BlackList.Model.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: GUI - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }
            else
            {
              _status = ERROR_UI.DIRECTORY_NOT_EXIST;
            }

            break;

          case APPLICATION.RELEASE_KERNEL:

            if (System.IO.Directory.Exists(ReadText(txt_Source_CASHIER)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_CASHIER), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.Cashier\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_CASHIER), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.Cashier.exe");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              m_files.Add("CommonBase.dll");
              m_files.Add("ThoughtWorks.QRCode.dll");
              m_files.Add("CameraConnectorDirectShow.dll");
              m_files.Add("DirectShowLib-2005.dll");
              m_files.Add("WSI.IDCamera.dll");
              m_files.Add("WSI.IDCamera.Model.dll");
              m_files.Add("WSI.IDScanner.dll");
              m_files.Add("WSI.IDScanner.Model.dll");
              m_files.Add("WSI.Entrances.BlackList.Model.dll");
              m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");
              m_files.Add("IDScannerConnectorICarIDBoxBasic.dll");
              m_files.Add("IDScannerConnectorSnapShell.dll");
              m_files.Add("ICSharpCode.SharpZipLib.dll");
              m_files.Add("Interop.ICARCOMLib.dll");
              m_files.Add("WSI.PinPad.dll");
              m_files.Add("WSI.PinPad.Banorte.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

              m_files.Clear();

              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.Cashier.exe");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\en-US_WSI.Common.resources.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\es_WSI.Common.resources.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\es-MX_WSI.Common.resources.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.Common.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\CommonBase.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\ThoughtWorks.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\CameraConnectorDirectShow.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\DirectShowLib-2005.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.IDCamera.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.IDCamera.Model.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.Entrances.BlackList.Model.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WigosModel.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WigosCommon.dll");
              m_files.Add(PATH_WIGOS_TEMP + "WSI.Cashier\\bin\\Release\\WSI.Entrances.BlackList.Model.dll");

              foreach (String _file in m_files)
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: CASHIER - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_WCP:

            if (System.IO.Directory.Exists(ReadText(txt_Source_WCP)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_WCP), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.Service\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_WCP), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.Service.exe");
              m_files.Add("CommonBase.dll");
              m_files.Add("WSI.CommonService.dll");
              m_files.Add("WSI.WCP.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              m_files.Add("LCD_GameGateway.dll");
              m_files.Add("ThoughtWorks.QRCode.dll");
              m_files.Add("WSI.Entrances.BlackList.Model.dll");
              m_files.Add("WSI.Entrances.BlackList.dll");
              m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");
              m_files.Add("BlackListConnectorInternal.dll");
              m_files.Add("BlackListConnectorLocal.dll");
              m_files.Add("CamaraOcupacion.dll");
              m_files.Add("WSI.PinPad.dll");
              m_files.Add("WSI.PinPad.Banorte.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: WCP - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_WC2:

            if (System.IO.Directory.Exists(ReadText(txt_Source_WC2)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_WC2), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.WC2_Service\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_WC2), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.WC2_Service.exe");
              m_files.Add("CommonBase.dll");
              m_files.Add("WSI.CommonService.dll");
              m_files.Add("WSI.WC2.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              //m_files.Add("ThoughtWorks.QRCode.dll");
              //m_files.Add("WSI.Entrances.BlackList.Model.dll");
              //m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: WC2 - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_WWPC:
            if (System.IO.Directory.Exists(ReadText(txt_Source_WWPC)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "MULTISITE", ReadText(txt_Destiny_WWPC), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.WWP_Server\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_WWPC), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.WWP_CenterService.exe");
              m_files.Add("WSI.WWP_CenterService.XmlSerializers.dll");
              m_files.Add("WSI.CommonService.dll");
              m_files.Add("CommonBase.dll");
              m_files.Add("WSI.WWP.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              //m_files.Add("ThoughtWorks.QRCode.dll");
              //m_files.Add("WSI.Entrances.BlackList.Model.dll");
              //m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: WWPC - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_WWPS:
            if (System.IO.Directory.Exists(ReadText(txt_Source_WWPS)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_WWPS), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.WWP_Client\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_WWPS), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.WWP_SiteService.exe");
              m_files.Add("CommonBase.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              m_files.Add("WSI.CommonService.dll");
              m_files.Add("WSI.WWP.dll");
              //m_files.Add("ThoughtWorks.QRCode.dll");
              //m_files.Add("WSI.Entrances.BlackList.Model.dll");
              //m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: WWPS - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_PSA_CLIENT:
            if (System.IO.Directory.Exists(ReadText(txt_Source_PSA)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_PSA), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.PSA_Client\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_PSA), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("WSI.CommonService.dll");
              m_files.Add("WSI.PSA_Client.exe");
              m_files.Add("WSI.PSA_Client.XmlSerializers.dll");
              m_files.Add("CommonBase.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              //m_files.Add("ThoughtWorks.QRCode.dll");
              //m_files.Add("WSI.Entrances.BlackList.Model.dll");
              //m_files.Add("WigosModel.dll");
              m_files.Add("WigosCommon.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: PSA - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.RELEASE_AFIP_CLIENT:
            if (System.IO.Directory.Exists(ReadText(txt_Source_AFIP)))
            {
              String _dir_target = System.IO.Path.Combine(ReadText(txt_Destiny_Directory), "SITE", ReadText(txt_Destiny_AFIP), "Bin");
              String _file_target;
              String _src_dir = System.IO.Path.Combine(PATH_WIGOS_TEMP, "WSI.AFIP_Client\\bin\\Release");
              String _src_file;

              CopyDirectories(ReadText(txt_Source_AFIP), _dir_target.Replace("Bin", ""), true);

              m_files = new List<String>();
              m_files.Clear();

              m_files.Add("es_WSI.Common.resources.dll");
              m_files.Add("WSI.AFIP_Client.exe");
              m_files.Add("WSI.CommonService.dll");
              m_files.Add("en-US_WSI.Common.resources.dll");
              m_files.Add("es-MX_WSI.Common.resources.dll");
              m_files.Add("WSI.Common.dll");
              m_files.Add("Interop.Microsoft.Office.Interop.Excel.dll");
              m_files.Add("WSI.Entrances.BlackList.Model.dll");
              //m_files.Add("ThoughtWorks.QRCode.dll");
              //m_files.Add("WigosModel.dll");

              foreach (String _file in m_files)
              {
                _src_file = System.IO.Path.Combine(_src_dir, _file);
                _file_target = System.IO.Path.Combine(_dir_target, _file);

                if (System.IO.File.Exists(_file_target))
                {
                  System.IO.File.SetAttributes(_file_target, System.IO.FileAttributes.Normal);
                  System.IO.File.Delete(_file_target);
                }

                if (System.IO.File.Exists(_src_file))
                {
                  System.IO.File.Copy(_src_file, _file_target, true);
                }
                else
                {
                  WriteLog("\n\tWARNING!!!: AFIP - " + _src_file.ToString() + " - not found");
                }
              }

              CopyRuntimeVS(_dir_target);
            }

            break;

          case APPLICATION.SPRINT:
            String _sprint_path = ReadText(txt_Sprint_Source);
            String _wigos = System.IO.Path.Combine(_sprint_path, "Wigos System");
            String _client = System.IO.Path.Combine(_sprint_path, "Client_00");

            if (System.IO.Directory.Exists(_wigos) && System.IO.Directory.Exists(_client))
            {
              if (System.IO.Directory.Exists(PATH_WIGOS_TEMP))
              {
                CleanDirectory(PATH_WIGOS_TEMP);
              }
              if (System.IO.Directory.Exists(PATH_TEMPORAL + "Client_00"))
              {
                CleanDirectory(PATH_TEMPORAL + "\"Client_00\"");
              }

              System.IO.Directory.CreateDirectory(PATH_WIGOS_TEMP);
              System.IO.Directory.CreateDirectory(PATH_TEMPORAL + "Client_00");

              CopyDirectories(_client, PATH_TEMPORAL + "Client_00", true);
              CopyDirectories(_wigos, PATH_WIGOS_TEMP, true);

            }
            else
            {
              _status = ERROR_UI.DIRECTORY_NOT_EXIST;
            }

            break;

        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          //ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
          MessageBox.Show(_ex.Message, "Error", MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI CopyRuntimeVS(string _PATH)
    {

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        System.IO.File.Copy(System.IO.Path.Combine(PATH_WIGOS_RUNTIME, "VS2013\\mfc120u.dll"), System.IO.Path.Combine(_PATH, "mfc120u.dll"), true);
        System.IO.File.Copy(System.IO.Path.Combine(PATH_WIGOS_RUNTIME + "VS2013\\mfcm120u.dll"), System.IO.Path.Combine(_PATH, "mfcm120u.dll"), true);
        System.IO.File.Copy(System.IO.Path.Combine(PATH_WIGOS_RUNTIME + "VS2013\\msvcp120.dll"), System.IO.Path.Combine(_PATH, "msvcp120.dll"), true);
        System.IO.File.Copy(System.IO.Path.Combine(PATH_WIGOS_RUNTIME + "VS2013\\msvcr120.dll"), System.IO.Path.Combine(_PATH, "msvcr120.dll"), true);
        System.IO.File.Copy(System.IO.Path.Combine(PATH_WIGOS_RUNTIME + "VS2013\\vccorlib120.dll"), System.IO.Path.Combine(_PATH, "vccorlib120.dll"), true);
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }


    private String BuildLabelRelease(APPLICATION Applications)
    {
      String _label = String.Empty;

      switch (Applications)
      {
        case APPLICATION.RELEASE_GUI:
          _label = "WSI.WigosGUI_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_GUI);
          break;
        case APPLICATION.RELEASE_KERNEL:
          _label = "WSI.Cashier_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_CASHIER);
          break;
        case APPLICATION.RELEASE_WC2:
          _label = "WSI.WC2_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_WC2);
          break;
        case APPLICATION.RELEASE_WCP:
          _label = "WSI.WCP_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_WCP);
          break;
        case APPLICATION.RELEASE_MS:
          _label = "WSI.WigosMultiSiteGUI_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_MS);
          break;
        case APPLICATION.RELEASE_WWPS:
          _label = "WSI.WWP_Site_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_WWPS);
          break;
        case APPLICATION.RELEASE_WWPC:
          _label = "WSI.WWP_Center_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_WWPC);
          break;
        case APPLICATION.RELEASE_PSA_CLIENT:
          _label = "WSI.PSA_Client_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_PSA);
          break;
        case APPLICATION.RELEASE_AFIP_CLIENT:
          _label = "WSI.AFIP_Client_" + m_date + " Version_18";
          if (m_version_mode == TYPE_VERSION.SPRINT)
          {
            _label += "." + ReadText(txt_Group);
          }
          _label += "_" + ReadText(txt_Version_AFIP);
          break;
      }

      return _label;
    }

    private String GetDate()
    {
      String m_date = string.Empty;

      m_date = System.DateTime.Now.ToString("yyyyMMdd HH");

      if (DateTime.Now.Minute < 15)
      {
        m_date += "00";
      }
      else if (DateTime.Now.Minute < 30)
      {
        m_date += "15";
      }
      else if (DateTime.Now.Minute < 45)
      {
        m_date += "30";
      }
      else
      {
        m_date += "45";
      }

      return m_date;
    }

    private void BuildLabel(String LabelSS)
    {
      if (m_abort) { return; }

      if (String.IsNullOrEmpty(ReadText(txt_label)))
      {
        return;
      }

      StartLog("\nCreación etiqueta en SourceSafe:");

      //m_label_ss = String.Empty;
      //m_label_ss = LabelSS.Replace("YYYYMMDD HHMM", m_date);
      //m_label_ss = "\"" + m_label_ss + "\"";

      DoLabel(ReadText(txt_label));

      UpdateProgressValue(CalculateProgress());

      EndLog();
    }

    private ERROR_UI DoLabel(String Label)
    {
      ERROR_UI _status;

      try
      {
        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();

        _info.WindowStyle = ProcessWindowStyle.Hidden;
        _info.FileName = "TFS\\labelTFS.bat";
        _info.Arguments = Label + " " + User + " " + Crypto.DecryptString(Password) + " \"" + "$/Wigos Project/" + ReadText(txt_branch) + "\"";

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();

        _status = ERROR_UI.OK;
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private ERROR_UI CheckChecks()
    {
      ERROR_UI _status;

      _status = ERROR_UI.OK;

      try
      {
        if (!((Boolean)tb_CASHIER.IsChecked) &&
            !((Boolean)tb_GUI.IsChecked) &&
            !((Boolean)tb_WC2.IsChecked) &&
            !((Boolean)tb_WCP.IsChecked) &&
            !((Boolean)tb_MS.IsChecked) &&
            !((Boolean)tb_WWPC.IsChecked) &&
            !((Boolean)tb_WWPS.IsChecked) &&
            !((Boolean)tb_PSA.IsChecked) &&
            !((Boolean)tb_AFIP.IsChecked))
        {
          _status = ERROR_UI.ANY_APPLICATION;
        }
        else
        {
          m_steps = 2;

          // MS
          if ((Boolean)tb_MS.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_MS.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_MS.Text)) ||
                (string.IsNullOrEmpty(txt_Source_MS.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = (Boolean)tb_GUI.IsChecked ? m_steps + (Int32)STEPS.MS : m_steps + ((Int32)STEPS.MS - (Int32)STEPS.GUI);
          }

          // CASHIER
          if ((Boolean)tb_CASHIER.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_CASHIER.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_CASHIER.Text)) ||
                (string.IsNullOrEmpty(txt_Source_CASHIER.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.CASHIER;
          }

          // GUI
          if ((Boolean)tb_GUI.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_GUI.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_GUI.Text)) ||
                (string.IsNullOrEmpty(txt_Source_GUI.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.GUI;
          }

          // WC2
          if ((Boolean)tb_WC2.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_WC2.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_WC2.Text)) ||
                (string.IsNullOrEmpty(txt_Source_WC2.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.WC2;
          }

          // WCP
          if ((Boolean)tb_WCP.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_WCP.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_WCP.Text)) ||
                (string.IsNullOrEmpty(txt_Source_WCP.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.WCP;
          }

          // WWP C
          if ((Boolean)tb_WWPC.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_WWPC.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_WWPC.Text)) ||
                (string.IsNullOrEmpty(txt_Source_WWPC.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.WWPC;
          }

          // WWP S
          if ((Boolean)tb_WWPS.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_WWPS.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_WWPS.Text)) ||
                (string.IsNullOrEmpty(txt_Source_WWPS.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.WWPS;
          }

          // PSA
          if ((Boolean)tb_PSA.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_PSA.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_PSA.Text)) ||
                (string.IsNullOrEmpty(txt_Source_PSA.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.PSA_CLIENT;
          }

          // AFIP
          if ((Boolean)tb_AFIP.IsChecked)
          {
            if ((string.IsNullOrEmpty(txt_Version_AFIP.Text)) ||
                (string.IsNullOrEmpty(txt_Destiny_AFIP.Text)) ||
                (string.IsNullOrEmpty(txt_Source_AFIP.Text)))
            {
              _status = ERROR_UI.EMPTY;
            }
            m_steps = m_steps + (Int32)STEPS.AFIP_CLIENT;
          }

          // GROUP NUMBER
          if (m_version_mode == TYPE_VERSION.SPRINT && String.IsNullOrEmpty(txt_Group.Text))
          {
            _status = ERROR_UI.GROUP_EMPTY;
          }

          // SPRINT VERSION
          if (m_version_mode == TYPE_VERSION.SPRINT && String.IsNullOrEmpty(txt_Sprint_Source.Text))
          {
            _status = ERROR_UI.EMPTY;
          }

          // GROUP NUMBER
          if (m_version_mode == TYPE_VERSION.RELEASE && String.IsNullOrEmpty(txt_branch.Text))
          {
            _status = ERROR_UI.BRANCH_EMPTY;
          }

          // GROUP NUMBER
          if (m_version_mode == TYPE_VERSION.RELEASE && String.IsNullOrEmpty(txt_label.Text))
          {
            _status = ERROR_UI.LABEL_EMPTY;
          }

        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      return _status;
    }

    private void CleanDirectory(String CleanPath)
    {
      try
      {
        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();

        _info.FileName = "cmd.exe";
        _info.Arguments = "/C rmdir " + CleanPath + " /S /Q";
        _info.WindowStyle = ProcessWindowStyle.Hidden;

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();

      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
      }

    }

    private ERROR_UI DownloadCode(String SavingPath, String SourceSafePath)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nDescargando código de TFS <" + SourceSafePath + ">");

      try
      {
        UpdateProgressValue(CalculateProgress());

        System.IO.Directory.CreateDirectory(SavingPath.Replace("\"", String.Empty));

        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();
        String _arguments = "\"" + SavingPath + "\"" + " " + "\"" + SourceSafePath + "\""; // +" " + m_label_ss;

        _info.FileName = "BATS\\source.bat";
        _info.Arguments = _arguments + " " + User + " " + Crypto.DecryptString(Password);
        //_info.WindowStyle = ProcessWindowStyle.Hidden;

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI CompileCode(String PathCompile, String CompileMode)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nCompilando el proyecto: <" + PathCompile + "> \nEn:<" + CompileMode + ">");

      try
      {
        UpdateProgressValue(CalculateProgress());

        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();

        String _arguments = "\"" + PathCompile + "\"" + " " + "\"" + CompileMode + "\"";

        _info.FileName = "BATS\\compile.bat";
        _info.Arguments = _arguments;
        _info.WindowStyle = ProcessWindowStyle.Hidden;

        //_info.WorkingDirectory = PATH_VISUAL_STUDIO;
        //_info.FileName = "devenv";

        //_info.Arguments ="\"" + PathCompile + "\"" + " /Rebuild " + "\"" + CompileMode + "\"";

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI MakeDIST(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nGenerando fichero dist");

      try
      {
        UpdateProgressValue(CalculateProgress());

        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();

        _info.FileName = "VersionGenerator\\GUI_VersionGenerator.exe";

        String[] _files = { };
        String _path = String.Empty;

        if (Applications == APPLICATION.GUI)
        {
          if (System.IO.File.Exists("WIGOS_GUI.DISTCFG"))
          {
            System.IO.File.Delete("WIGOS_GUI.DISTCFG");
          }

          System.IO.File.Copy("WIGOS_GUI_S.DISTCFG", "WIGOS_GUI.DISTCFG");

          if (System.IO.Directory.Exists(ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\BIN\\"))
          {
            _files = System.IO.Directory.GetFiles(ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\BIN\\");
          }

          _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\BIN";
          _path = "\"" + _path + "\"";
          _info.Arguments = "WIGOS_GUI.DISTCFG " + _path + " " + ReadText(txt_Version_GUI);
        }
        else if (Applications == APPLICATION.KERNEL)
        {
          if (System.IO.Directory.Exists(ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER) + "\\BIN\\"))
          {
            _files = System.IO.Directory.GetFiles(ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER) + "\\BIN\\");
          }

          _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER) + "\\BIN";
          _path = "\"" + _path + "\"";
          _info.Arguments = "WSI_CASHIER.DISTCFG " + _path + " " + ReadText(txt_Version_CASHIER);
        }
        else if (Applications == APPLICATION.MS)
        {
          if (System.IO.File.Exists("WIGOS_GUI.DISTCFG"))
          {
            System.IO.File.Delete("WIGOS_GUI.DISTCFG");
          }

          System.IO.File.Copy("WIGOS_GUI_MS.DISTCFG", "WIGOS_GUI.DISTCFG");

          _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS) + "\\BIN";
          _path = "\"" + _path + "\"";
          _info.Arguments = "WIGOS_GUI.DISTCFG " + _path + " " + ReadText(txt_Version_MS);
        }

        if (_files.Length > 0)
        {
          foreach (String _file in _files)
          {
            if (_file.Contains("DIST") && (_file.Contains("GUI") || _file.Contains("CASHIER")))
            {
              System.IO.File.SetAttributes(_file, System.IO.FileAttributes.Normal);
              System.IO.File.Delete(_file);
            }
          }
        }

        _info.WindowStyle = ProcessWindowStyle.Hidden;
        _info.UseShellExecute = false;
        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI BuildLKCPaket(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nGenerando paquete LKC");

      try
      {
        UpdateProgressValue(CalculateProgress());

        String _path;

        Process _process = new Process();
        ProcessStartInfo _info = new ProcessStartInfo();

        _info.FileName = "BATS\\pack.bat";
        switch (Applications)
        {
          case APPLICATION.GUI:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\BIN\\";
            _info.Arguments = "2 18 " + ReadText(txt_Version_GUI) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.KERNEL:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER) + "\\BIN\\";
            _info.Arguments = "0 18 " + ReadText(txt_Version_CASHIER) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.WCP:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WCP) + "\\BIN\\";
            _info.Arguments = "3 18 " + ReadText(txt_Version_WCP) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.WC2:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WC2) + "\\BIN\\";
            _info.Arguments = "4 18 " + ReadText(txt_Version_WC2) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.MS:
            _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS) + "\\BIN\\";
            _info.Arguments = "12 18 " + ReadText(txt_Version_MS) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.WWPC:
            _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_WWPC) + "\\BIN\\";
            _info.Arguments = "7 18 " + ReadText(txt_Version_WWPC) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.WWPS:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WWPS) + "\\BIN\\";
            _info.Arguments = "6 18 " + ReadText(txt_Version_WWPS) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.PSA_CLIENT:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_PSA) + "\\BIN\\";
            _info.Arguments = "22 18 " + ReadText(txt_Version_PSA) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
          case APPLICATION.AFIP_CLIENT:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_AFIP) + "\\BIN\\";
            _info.Arguments = "24 18 " + ReadText(txt_Version_AFIP) + " \"" + System.IO.Directory.GetCurrentDirectory() + "\\LKC\\Packer.exe\"" + " \"" + _path + "\"";
            break;
        }

        _info.WindowStyle = ProcessWindowStyle.Hidden;

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI MoveLKCPaket(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nCopiando paquete LKC");

      try
      {
        UpdateProgressValue(CalculateProgress());

        String _path = String.Empty;
        String _previous_path = String.Empty;

        switch (Applications)
        {
          case APPLICATION.GUI:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_GUI);
            break;
          case APPLICATION.KERNEL:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_CASHIER);
            break;
          case APPLICATION.MS:
            _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS);
            break;
          case APPLICATION.WCP:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WCP) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WCP);
            break;
          case APPLICATION.WC2:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WC2) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WC2);
            break;
          case APPLICATION.WWPC:
            _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_WWPC) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_WWPC);
            break;
          case APPLICATION.WWPS:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WWPS) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_WWPS);
            break;
          case APPLICATION.PSA_CLIENT:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_PSA) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_PSA);
            break;
          case APPLICATION.AFIP_CLIENT:
            _path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_AFIP) + "\\BIN\\";
            _previous_path = ReadText(txt_Destiny_Directory) + "\\SITE\\" + ReadText(txt_Destiny_AFIP);
            break;
        }


        if (System.IO.Directory.Exists(_path))
        {
          if (!System.IO.Directory.Exists(_previous_path + "\\Packet"))
          {
            System.IO.Directory.CreateDirectory(_previous_path + "\\Packet");
          }

          foreach (String _folders in System.IO.Directory.GetFiles(_path))
          {
            if (_folders.Contains("TSV_") || _folders.Contains(".TSV") || _folders.Contains("TSV"))
            {
              System.IO.File.SetAttributes(_folders, System.IO.FileAttributes.Normal);
              if (!_folders.Contains("LOG"))
              {
                System.IO.File.Copy(_folders, _previous_path + "\\Packet\\" + _folders.Split('\\')[_folders.Split('\\').Length - 1]);
              }
              System.IO.File.Delete(_folders);
            }

            if (_folders.Contains("Packer.exe") || _folders.Contains("Compress"))
            {
              System.IO.File.SetAttributes(_folders, System.IO.FileAttributes.Normal);
              System.IO.File.Delete(_folders);
            }
          }

        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private ERROR_UI RenameManifest(APPLICATION Applications)
    {
      if (m_abort || m_global_status != ERROR_UI.OK) { EnableButtons(true); return m_global_status; }

      ERROR_UI _status;

      _status = ERROR_UI.OK;

      StartLog("\nRenombrando ficheros necesarios");

      try
      {
        String _path;
        String _contains_file;
        String _rename_file;

        UpdateProgressValue(CalculateProgress());

        _path = String.Empty;
        _contains_file = String.Empty;
        _rename_file = String.Empty;

        switch (Applications)
        {
          case APPLICATION.MS:
            _path = ReadText(txt_Destiny_Directory) + "\\MULTISITE\\" + ReadText(txt_Destiny_MS) + "\\BIN\\";
            _contains_file = "WigosGUI.exe.manifest";
            _rename_file = _path + "WigosMultiSiteGUI.exe.manifest";
            break;
        }

        foreach (String _folders in System.IO.Directory.GetFiles(_path))
        {
          if (_folders.Contains(_contains_file))
          {
            System.IO.File.SetAttributes(_folders, System.IO.FileAttributes.Normal);
            System.IO.File.Move(_folders, _rename_file);
          }
        }
      }
      catch (Exception _ex)
      {
        if (!m_abort)
        {
          ShowMessage("Error", _ex.Message, MessageBoxButton.OK);
        }
        _status = ERROR_UI.EXCEPTION;
      }

      EndLog();

      return _status;
    }

    private static void CopyDirectories(string SourcePath, string DestinyPath, bool CopySubFolders)
    {
      System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(SourcePath);
      System.IO.DirectoryInfo[] dirs = dir.GetDirectories();

      if (!System.IO.Directory.Exists(DestinyPath))
      {
        System.IO.Directory.CreateDirectory(DestinyPath);
      }

      System.IO.FileInfo[] files = dir.GetFiles();
      foreach (System.IO.FileInfo file in files)
      {
        String temppath = System.IO.Path.Combine(DestinyPath, file.Name);
        if (System.IO.File.Exists(temppath)) System.IO.File.SetAttributes(temppath, System.IO.FileAttributes.Normal);
        file.CopyTo(temppath, true);
      }

      if (CopySubFolders)
      {
        foreach (System.IO.DirectoryInfo subdir in dirs)
        {
          if (subdir.Name != "Packet")
          {
            string temppath = System.IO.Path.Combine(DestinyPath, subdir.Name);
            CopyDirectories(subdir.FullName, temppath, CopySubFolders);
          }
        }
      }
    }

    private void WriteLog(String Log)
    {
      if (!lb_Log.Dispatcher.CheckAccess())
      {
        CallWriteLog _delegate = new CallWriteLog(WriteLog);
        this.Dispatcher.BeginInvoke(_delegate, new object[] { Log });
      }
      else
      {
        lb_Log.Items.Insert(0, Log);
        lb_Log.SelectedIndex = lb_Log.Items.Count - 1;
      }
    }

    private void UpdateProgressValue(Int32 Value)
    {
      if (!pgb_Status.Dispatcher.CheckAccess())
      {
        UpdateProgress _delegate = new UpdateProgress(UpdateProgressValue);
        this.Dispatcher.BeginInvoke(_delegate, new object[] { Value });
      }
      else
      {
        if (Value > (Int32)pgb_Status.Maximum)
        {
          pgb_Status.Value = pgb_Status.Maximum;
        }
        else
        {
          pgb_Status.Value = Value;
        }
      }
    }

    private void DeleteTempFiles()
    {
      using (Process _process = new Process())
      {
        ProcessStartInfo _info = new ProcessStartInfo();

        _info.FileName = "BATS\\delete.bat";
        _info.WindowStyle = ProcessWindowStyle.Hidden;

        _process.StartInfo = _info;
        _process.Start();
        _process.WaitForExit();
      }
    }

    private void StartLog(String Start)
    {
      WriteLog("\n");
      //WriteLog(Start);
      m_start = DateTime.Now;
      WriteLog(Start + "\n\tInicio: " + m_start.ToString());

    }

    private void EndLog()
    {
      m_end = DateTime.Now;
      WriteLog("\tFinalización: " + m_end.ToString() + "\n\tTiempo requerido: " + (m_end - m_start));
      //WriteLog("\tTiempo requerido: " + (m_end - m_start));
    }

    private void EnableButtons(Boolean Status)
    {
      if (!btn_Start.Dispatcher.CheckAccess() || !btn_Stop.Dispatcher.CheckAccess())
      {
        EnableControls _delegate = new EnableControls(EnableButtons);
        this.Dispatcher.BeginInvoke(_delegate, new object[] { Status });
      }
      else
      {
        btn_Start.IsEnabled = Status;
        btn_Stop.IsEnabled = !Status;
      }
    }

    private Boolean ReadCheck(uc_ToggleBox Check)
    {
      Boolean _check;

      if (!Check.Dispatcher.CheckAccess())
      {
        _check = (Boolean)Check.Dispatcher.Invoke(new Func<Boolean>(() => ReadCheck(Check)));
      }
      else
      {
        _check = (Boolean)Check.IsChecked;
      }

      return _check;
    }

    private String ReadText(TextBox Text)
    {
      String _text;

      if (!Text.Dispatcher.CheckAccess())
      {
        _text = (String)Text.Dispatcher.Invoke(new Func<String>(() => ReadText(Text)));
      }
      else
      {
        _text = Text.Text;
      }

      return _text;
    }

    private void ChangeVersionMode(TYPE_VERSION Mode)
    {
      switch (Mode)
      {
        case TYPE_VERSION.RELEASE:
          mi_Release.IsChecked = true;
          mi_Sprint.IsChecked = false;
          m_version_mode = TYPE_VERSION.RELEASE;
          lbl_Sprint_Source.Visibility = System.Windows.Visibility.Collapsed;
          txt_Sprint_Source.Visibility = System.Windows.Visibility.Collapsed;
          btn_Sprint_Directory.Visibility = System.Windows.Visibility.Collapsed;
          txt_branch.Visibility = System.Windows.Visibility.Visible;
          lbl_branch.Visibility = System.Windows.Visibility.Visible;
          txt_label.Visibility = System.Windows.Visibility.Visible;
          lbl_label.Visibility = System.Windows.Visibility.Visible;
          ShowTeam(Visibility.Hidden);
          break;
        case TYPE_VERSION.SPRINT:
          mi_Release.IsChecked = false;
          mi_Sprint.IsChecked = true;
          m_version_mode = TYPE_VERSION.SPRINT;
          lbl_Sprint_Source.Visibility = System.Windows.Visibility.Visible;
          txt_Sprint_Source.Visibility = System.Windows.Visibility.Visible;
          btn_Sprint_Directory.Visibility = System.Windows.Visibility.Visible;
          txt_branch.Visibility = System.Windows.Visibility.Collapsed;
          lbl_branch.Visibility = System.Windows.Visibility.Collapsed;
          txt_label.Visibility = System.Windows.Visibility.Collapsed;
          lbl_label.Visibility = System.Windows.Visibility.Collapsed;
          ShowTeam(Visibility.Visible);
          break;
      }

      UserPrivileges();
    }

    private void ShowTeam(Visibility Status)
    {
      lbl_Group.Visibility = Status;
      txt_Group.Visibility = Status;
    }

    private MessageBoxResult ShowMessage(String Header, String Text, MessageBoxButton Buttons)
    {
      AnimationBlurStart();

      MessageBoxResult _result = MessageBoxResult.Cancel;

      if (this.Dispatcher.CheckAccess())
      {
        Messages _msg = new Messages();
        _msg.Topmost = true;
        _msg.LoadValues(Header, Text, Buttons);
        _msg.ShowDialog();
        _result = _msg.m_result;
        _msg = null;
      }
      else
      {
        Dispatcher.Invoke(new Action(() => { ShowMessage(Header, Text, Buttons); }));
      }

      AnimationBlurFinished();
      return _result;
    }

    private void FindApplications(String Path)
    {
      txt_Source_Directory.Text = Path;
      if (System.IO.Directory.Exists(Path + "\\SITE"))
      {
        String[] _directories = System.IO.Directory.GetDirectories(Path + "\\SITE");

        foreach (String _directory in _directories)
        {
          if (_directory.Contains("WSI.WigosGUI_"))
          {
            txt_Source_GUI.Text = _directory;
          }
          else if (_directory.Contains("WSI.Cashier_"))
          {
            txt_Source_CASHIER.Text = _directory;
          }
          else if (_directory.Contains("WSI.WC2_"))
          {
            txt_Source_WC2.Text = _directory;
          }
          else if (_directory.Contains("WSI.WCP_"))
          {
            txt_Source_WCP.Text = _directory;
          }
          else if (_directory.Contains("WSI.WWP_Site_"))
          {
            txt_Source_WWPS.Text = _directory;
          }
          else if (_directory.Contains("WSI.PSA_Client_"))
          {
            txt_Source_PSA.Text = _directory;
          }
          else if (_directory.Contains("WSI.AFIP_Client_"))
          {
            txt_Source_AFIP.Text = _directory;
          }
        }
      }

      if (System.IO.Directory.Exists(Path + "\\MULTISITE"))
      {
        String[] _directories = System.IO.Directory.GetDirectories(Path + "\\MULTISITE");

        foreach (String _directory in _directories)
        {
          if (_directory.Contains("WSI.WigosMultiSiteGUI_"))
          {
            txt_Source_MS.Text = _directory;
          }
          else if (_directory.Contains("WSI.WWP_Center_"))
          {
            txt_Source_WWPC.Text = _directory;
          }
        }
      }
    }

    private void GenericTextBoxTextChanged(object sender, TextChangedEventArgs e)
    {
      TextBox _txt;

      _txt = (TextBox)sender;

      switch (_txt.Name)
      {
        case "txt_Version_MS":
          if (tb_MS.IsChecked)
          {
            txt_Destiny_MS.Text = BuildLabelRelease(APPLICATION.RELEASE_MS);
          }
          break;
        case "txt_Version_GUI":
          if (tb_GUI.IsChecked)
          {
            txt_Destiny_GUI.Text = BuildLabelRelease(APPLICATION.RELEASE_GUI);
          }
          break;
        case "txt_Version_CASHIER":
          if (tb_CASHIER.IsChecked)
          {
            txt_Destiny_CASHIER.Text = BuildLabelRelease(APPLICATION.RELEASE_KERNEL);
          }
          break;
        case "txt_Version_WCP":
          if (tb_WCP.IsChecked)
          {
            txt_Destiny_WCP.Text = BuildLabelRelease(APPLICATION.RELEASE_WCP);
          }
          break;
        case "txt_Version_WC2":
          if (tb_WC2.IsChecked)
          {
            txt_Destiny_WC2.Text = BuildLabelRelease(APPLICATION.RELEASE_WC2);
          }
          break;
        case "txt_Version_WWPC":
          if (tb_WWPC.IsChecked)
          {
            txt_Destiny_WWPC.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPC);
          }
          break;
        case "txt_Version_WWPS":
          if (tb_WWPS.IsChecked)
          {
            txt_Destiny_WWPS.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPS);
          }
          break;
        case "txt_Version_PSA":
          if (tb_PSA.IsChecked)
          {
            txt_Destiny_PSA.Text = BuildLabelRelease(APPLICATION.RELEASE_PSA_CLIENT);
          }
          break;
        case "txt_Version_AFIP":
          if (tb_AFIP.IsChecked)
          {
            txt_Destiny_AFIP.Text = BuildLabelRelease(APPLICATION.RELEASE_AFIP_CLIENT);
          }
          break;
        case "txt_Group":
          if (!String.IsNullOrEmpty(txt_Version_MS.Text) && (tb_MS.IsChecked))
          {
            txt_Destiny_MS.Text = BuildLabelRelease(APPLICATION.RELEASE_MS);
          }

          if (!String.IsNullOrEmpty(txt_Version_GUI.Text) && (tb_GUI.IsChecked))
          {
            txt_Destiny_GUI.Text = BuildLabelRelease(APPLICATION.RELEASE_GUI);
          }

          if (!String.IsNullOrEmpty(txt_Version_CASHIER.Text) && (tb_CASHIER.IsChecked))
          {
            txt_Destiny_CASHIER.Text = BuildLabelRelease(APPLICATION.RELEASE_KERNEL);
          }

          if (!String.IsNullOrEmpty(txt_Version_WCP.Text) && (tb_WCP.IsChecked))
          {
            txt_Destiny_WCP.Text = BuildLabelRelease(APPLICATION.RELEASE_WCP);
          }

          if (!String.IsNullOrEmpty(txt_Version_WC2.Text) && (tb_WC2.IsChecked))
          {
            txt_Destiny_WC2.Text = BuildLabelRelease(APPLICATION.RELEASE_WC2);
          }

          if (!String.IsNullOrEmpty(txt_Version_WWPC.Text) && (tb_WWPC.IsChecked))
          {
            txt_Destiny_WWPC.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPC);
          }

          if (!String.IsNullOrEmpty(txt_Version_WWPS.Text) && (tb_WWPS.IsChecked))
          {
            txt_Destiny_WWPS.Text = BuildLabelRelease(APPLICATION.RELEASE_WWPS);
          }

          if (!String.IsNullOrEmpty(txt_Version_PSA.Text) && (tb_PSA.IsChecked))
          {
            txt_Destiny_PSA.Text = BuildLabelRelease(APPLICATION.RELEASE_PSA_CLIENT);
          }

          if (!String.IsNullOrEmpty(txt_Version_AFIP.Text) && (tb_AFIP.IsChecked))
          {
            txt_Destiny_AFIP.Text = BuildLabelRelease(APPLICATION.RELEASE_AFIP_CLIENT);
          }
          break;
      }
    }

    #endregion

  }

}
