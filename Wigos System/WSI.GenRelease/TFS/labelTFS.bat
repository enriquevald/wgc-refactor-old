@echo Off

set LABEL=%1
set TFSUSER=%2
set TFSPASS=%3
set TFSBRANCH=%4

set TFSEXE="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\TF.exe"
set TFSSRV="http://vstfs:8080/tfs/Gaming Products"

%TFSEXE% label -server:%TFSSRV% %LABEL% %TFSBRANCH% -login:%TFSUSER%,%TFSPASS%

REM 3s. PAUSED
ping 127.0.0.1 -n 3 > nul


 