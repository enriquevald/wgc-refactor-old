@echo Off

set OPERATION=%1
set TFSDIR=%2
set TFSUSER=%3
set TFSPASS=%4

set TFSEXE="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\TF.exe"
set TFSSRV="http://vstfs:8080/tfs/Gaming Products"
set TFSWORKSPACE="TFS"
set HDDDIR="C:\TFS\Ficheros_SS\"

if not exist %HDDDIR% (
	mkdir %HDDDIR%
)

cd %HDDDIR%


:_OUT
%TFSEXE% workspace /new %TFSWORKSPACE% /collection:%TFSSRV% /Login:%TFSUSER%,%TFSPASS% /noprompt 
%TFSEXE% workfold /map %TFSDIR% %HDDDIR% /workspace:%TFSWORKSPACE% /Login:%TFSUSER%,%TFSPASS%
%TFSEXE% get %TFSDIR% /Login:%TFSUSER%,%TFSPASS%
%TFSEXE% checkout %TFSDIR% /lock:checkin /Login:%TFSUSER%,%TFSPASS%
goto :_END

:_IN
%TFSEXE% checkin %TFSDIR% /recursive /noprompt /comment:"Cambio de version" /override:"Automated Build Process" /Login:%TFSUSER%,%TFSPASS% 
%TFSEXE% workfold /unmap %HDDDIR% /Login:%TFSUSER%,%TFSPASS% /noprompt 
%TFSEXE% workspace /delete %TFSWORKSPACE% /Login:%TFSUSER%,%TFSPASS% /noprompt 
goto :_END

:_END
echo "END"
