@echo Off

set HDDDIR=%1
set TFSDIR=%2
set TFSLABEL=%3
set TFSUSER=%4
set TFSPASS=%5
set TFSEXE="C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\TF.exe"
set TFSSRV="http://vstfs:8080/tfs/Gaming Products"
set TFSWORKSPACE="WSIGEN"

cd "C:\Temp\"


REM CREATE THE WORKSPACE
%TFSEXE% workspace -new %TFSWORKSPACE% -noprompt -server:%TFSSRV% -Login:%TFSUSER%,%TFSPASS%
REM CREATE THE WORK FOLDER
%TFSEXE% workfold -map %TFSDIR% %HDDDIR% -workspace:%TFSWORKSPACE% -server:%TFSSRV% -Login:%TFSUSER%,%TFSPASS%
REM UNMAP THE DEFAULT FOLDER
%TFSEXE% workfold /unmap "C:\Temp" -Login:%TFSUSER%,%TFSPASS%
REM GET THE INFORMATION
%TFSEXE% get -force -recursive -noprompt
REM UNMAP THE WORK FOLDER
%TFSEXE% workfold /unmap %TFSDIR% -Login:%TFSUSER%,%TFSPASS%
REM DELETE THE WORKSPACE
%TFSEXE% workspace -delete %TFSWORKSPACE% -noprompt -server:%TFSSRV% -Login:%TFSUSER%,%TFSPASS%

