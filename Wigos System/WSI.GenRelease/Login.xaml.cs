﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WSI.GenRelease
{

  /// <summary>
  /// Interaction logic for Login.xaml
  /// </summary>
  public partial class Login : Window
  {

    #region Builder

    public Login()
    {
      InitializeComponent();

      m_Exit = false;
    }

    #endregion

    #region Members

    private String m_user;

    #endregion

    #region Properties

    public String User
    {
      get
      {
        return m_user;
      }
      set
      {
        m_user = value;
        txt_User.Text = value;
      }
    }
    public String m_Password { get; set; }
    public Boolean m_Exit { get; set; }

    #endregion

    #region Events

    private void btn_Accept_Click(object sender, RoutedEventArgs e)
    {
      if (Validation())
      {
        User = txt_User.Text.Trim().ToString();
        m_Password = txt_Password.Password.Trim().ToString();
        m_Exit = true;
        this.Close();
      }
    }

    private void btn_Cancel_Click(object sender, RoutedEventArgs e)
    {
      User = String.Empty;
      m_Password = String.Empty;

      m_Exit = true;
      this.Close();
    }

    private void txt_User_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
      {
        if (!String.IsNullOrEmpty(txt_Password.Password))
        {
          btn_Accept_Click(null, null);
        }
        else
        {
          txt_Password.Focus();
        }
      }
    }

    private void txt_Password_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Enter)
      {
        if (!String.IsNullOrEmpty(txt_User.Text))
        {
          btn_Accept_Click(null, null);
        }
        else
        {
          txt_User.Focus();
        }
      }
    }

    #endregion

    #region Functions

    private Boolean Validation()
    {
      return String.IsNullOrEmpty(txt_User.Text) ? false : String.IsNullOrEmpty(txt_Password.Password) ? false : true;
    }

    #endregion

  }

}
