﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WSI.GenRelease.Class
{
  /// <summary>
  /// Interaction logic for Messages.xaml
  /// </summary>
  public partial class Messages : Window
  {

    #region Builder

    public Messages()
    {
      InitializeComponent();
    }

    #endregion

    #region Members

    public MessageBoxResult m_result;

    #endregion

    #region Form events

    private void btn_OK_MouseEnter(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Hand;
    }

    private void btn_OK_MouseLeave(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Arrow;
    }

    private void btn_KO_MouseEnter(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Hand;
    }

    private void btn_KO_MouseLeave(object sender, MouseEventArgs e)
    {
      this.Cursor = Cursors.Arrow;
    }

    private void btn_OK_Click(object sender, RoutedEventArgs e)
    {
      m_result = MessageBoxResult.Yes;
      this.Close();
    }

    private void btn_KO_Click(object sender, RoutedEventArgs e)
    {
      m_result = MessageBoxResult.No;
      this.Close();
    }

    #endregion

    #region Functions

    public void LoadValues(String Header, String Body, MessageBoxButton Buttons)
    {
      lblHeader.Content = Header;
      tbText.Text = Body;

      switch (Buttons)
      {
        case MessageBoxButton.OK:
          btn_OK.Visibility = System.Windows.Visibility.Visible;
          btn_OK.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
          btn_KO.Visibility = System.Windows.Visibility.Collapsed;
          break;
        case MessageBoxButton.OKCancel:
        case MessageBoxButton.YesNo:
        case MessageBoxButton.YesNoCancel:
          btn_OK.Visibility = System.Windows.Visibility.Visible;
          btn_KO.Visibility = System.Windows.Visibility.Visible;
          break;
      }
    }

    #endregion

  }
}
