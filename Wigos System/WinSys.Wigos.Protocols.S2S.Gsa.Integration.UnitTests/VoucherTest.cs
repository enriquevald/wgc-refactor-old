﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;

using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using WigosHubCommon;

using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Bootstrapper;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;

using WSI.Common;

using ILogger = WigosHubCommon.ILogger;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.UnitTests
{
	[TestClass]
	public class VoucherTest : CommonService	// TODO: Refactor. Review database connection and log dependencies from CommonService
	{
		private const string SystemAUrl = "http://localhost:8890";
		private const string SystemBUrl = "http://localhost:8891";

		private const string ConfigFileName = "WSI.Testing.Configuration";

		private S2SMessagePostOperationResponse response;
		private IUnityContainer containerA;
		private IUnityContainer containerB;
		private IS2SGsaService serviceA;
		private IS2SGsaService serviceB;
		private IUnityContainer gsaS2sContainer;
		private ServiceHost gsaS2sServiceHostA;
		private ServiceHost gsaS2sServiceHostB;
		private IS2SGsaService gsaS2sService;
		private IServiceClient serviceClientA;
		private IServiceClient serviceClientB;

		public VoucherTest()
		{
			this.containerA = new UnityContainer();
			this.containerB = new UnityContainer();

			Unity.Configure(this.containerA);
			Unity.Configure(this.containerB);

			this.InitializeWigosSubSystems();
			this.InitializeGsaS2S();
		}

		private void InitializeWigosSubSystems()
		{
			Log.AddListener(new SimpleLogger(this.m_log_prefix));

			if (this.m_console)
			{
				Log.AddListener(new WSI.Common.ConsoleLogger());
				Console.CancelKeyPress += this.ConsoleCancelEventHandler;
			}

			if (!ConfigurationFile.Init(VoucherTest.ConfigFileName, string.Empty))
			{
				Log.Error(" Reading application configuration settings. Application stopped");
				return;
			}

			WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
			WGDB.SetApplication(this.m_service_name, this.m_version);
			WGDB.ConnectAs("WGROOT");

			while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
			{
				Log.Warning("Waiting for database connection ...");

				System.Threading.Thread.Sleep(5000);
			}

			Resource.Init();

			IPEndPoint _service_ip_endpoint;

			this.m_ip_address = ConfigurationFile.GetSetting("LocalIP");
			if (String.IsNullOrEmpty(this.m_ip_address))
			{
				this.m_ip_address = IPAddress.Any.ToString();

				Log.Error(" No Discovery Service IP address is available. Application stopped");

				return;
			}

			Alarm.Register(AlarmSourceCode.Service, 0, this.m_alarm_source, AlarmCode.Service_Started);

			this.OnServiceBeforeInit();

			if (this.m_stand_by)
			{
				Alarm.Register(AlarmSourceCode.Service, 0, this.m_alarm_source, AlarmCode.Service_StandBy);
			}
		}

		private void InitializeGsaS2S()
		{
			this.serviceClientA = new ServiceClient(new BasicHttpBinding(), new EndpointAddress(VoucherTest.SystemBUrl));
			this.serviceClientB = new ServiceClient(new BasicHttpBinding(), new EndpointAddress(VoucherTest.SystemAUrl));

			this.serviceA = this.containerA.Resolve<IS2SGsaService>();
			this.serviceB = this.containerB.Resolve<IS2SGsaService>();

			this.gsaS2sServiceHostA = new ServiceHost(this.serviceA, new Uri(VoucherTest.SystemAUrl));
			this.gsaS2sServiceHostB = new ServiceHost(this.serviceB, new Uri(VoucherTest.SystemBUrl));
			this.CreateAndInitializeHostListener(ref this.gsaS2sServiceHostA);
			this.CreateAndInitializeHostListener(ref this.gsaS2sServiceHostB);
		}

		[TestInitialize]
		public void Initialize()
		{
		}

		[TestMethod]
		public void GetVoucherIdListTestMethod()
		{
			string getVoucherIdListRequestMessage = @"<s2s:s2sMessage xmlns:xs=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:s2s=""http://www.gamingstandards.com/s2s/schemas/v1.2.6/""><s2s:s2sHeader s2s:fromSystem=""PariMAX"" s2s:toSystem=""WINSystems"" s2s:messageId=""7"" s2s:dateTimeSent=""2018-04-10T12:00:19.781""/><s2s:s2sBody><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2018-04-10T12:03:48.965"" s2s:commandId=""4146"" s2s:sessionId=""2"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""9""><s2s:getValidationIds s2s:clientType=""egm"" s2s:clientId=""AMT_OKGA-81-001""/></s2s:voucher><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2018-04-10T12:03:48.965"" s2s:commandId=""4147"" s2s:sessionId=""3"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""7""><s2s:getValidationIds s2s:clientType=""egm"" s2s:clientId=""AMT_OKGA-81-001""/></s2s:voucher></s2s:s2sBody></s2s:s2sMessage>";

			this.response = this.serviceA.S2SMessagePostOperation(new S2SMessagePostOperationRequest
			{
				s2sRequest = new RequestType
				{
					request = getVoucherIdListRequestMessage
				}
			});
			System.Diagnostics.Debugger.Log(1, System.Diagnostics.Debugger.DefaultCategory, string.Format("Response: '{0}'.", this.response.s2sResponse.response));
			System.Threading.Thread.Sleep(1000);
		}

		[TestMethod]
		public void IssueVoucherTestMethod()
		{
			string sVoucherId = this.GenerateNumber();
			string issueVoucherRequestMessage = @"<?xml version = ""1.0"" encoding = ""UTF-8"" standalone = ""yes""?><s2s:s2sMessage xmlns:s2s = ""http://www.gamingstandards.com/s2s/schemas/v1.2.6/""><s2s:s2sHeader s2s:fromSystem = ""http://localhost:8890"" s2s:toSystem = ""http://localhost:9990"" s2s:messageId = ""648885"" s2s:dateTimeSent = ""2007-03-01T00:00:04.148-06:00""/><s2s:s2sBody><s2s:voucher s2s:propertyId = ""1"" s2s:dateTime = ""2007-03-01T00:00:04.148-06:00"" s2s:commandId = ""328895"" s2s:sessionId = ""199289"" s2s:sessionType = ""request"" s2s:timeToLive = ""30000"" s2s:sessionRetry = ""0""><s2s:issueVoucher s2s:clientId = ""VEN_010000000000000000000E59FC35"" s2s:clientType = ""egm"" s2s:voucherId = """ + sVoucherId + @""" s2s:voucherAmount = ""4000"" s2s:currencyId = ""USD"" s2s:largeWin = ""false"" s2s:transferAction = ""issued"" s2s:transferAmount = ""4000"" s2s:transferException = ""00"" s2s:transferDateTime = ""2007-03-01T00:01:02.000-06:00"" s2s:transferSequence = ""23"" s2s:creditType = ""cashable"" s2s:poolId = ""0"" s2s:cardId = """" s2s:cardRestrict = ""noCard"" s2s:egmDateTime = ""2007-03-10T07:21:02.000-06:00"" s2s:cageDateTime = ""2007-03-10T07:21:02.000-06:00"" s2s:transactionId = ""2917"" /></s2s:voucher></s2s:s2sBody></s2s:s2sMessage>";

			this.response = this.serviceA.S2SMessagePostOperation(new S2SMessagePostOperationRequest
			{
				s2sRequest = new RequestType
				{
					request = issueVoucherRequestMessage
				}
			});
			System.Diagnostics.Debugger.Log(1, System.Diagnostics.Debugger.DefaultCategory, string.Format("Response: '{0}'.", this.response.s2sResponse.response));
			System.Threading.Thread.Sleep(1000);
		}

		[TestMethod]
		public void AmtoteForcedBugTestMethod()
		{
			string sVoucherId = this.GenerateNumber();
			string amtoteForcedBugRequestMessage = @"<s2s:s2sMessage xmlns:xs=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:s2s=""http://www.gamingstandards.com/s2s/schemas/v1.2.6/""><s2s:s2sHeader s2s:fromSystem=""PariMAX"" s2s:toSystem=""Wigos"" s2s:messageId=""23"" s2s:dateTimeSent=""2020-05-17T15:36:14.594""/><s2s:s2sBody><s2s:communications s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.322"" s2s:commandId=""53306""><s2s:commsOnLine s2s:s2sVersion=""1.2.6"" s2s:s2sLocation=""http://localhost:8891""/></s2s:communications><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.422"" s2s:commandId=""53307"" s2s:sessionId=""3232"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""0""><s2s:issueVoucher s2s:clientType=""egm"" s2s:clientId=""AMT_PARMCMTY1-1-011"" s2s:voucherId=""" + sVoucherId + @""" s2s:voucherAmount=""25075"" s2s:currencyId=""USD"" s2s:largeWin=""false"" s2s:creditType=""cashable"" s2s:poolId=""0"" s2s:cardId=""0"" s2s:cardRestrict=""anyCard"" s2s:egmDateTime=""2020-06-16T19:34:10"" s2s:cageDateTime=""2020-06-16T19:34:10"" s2s:transferAction=""issued"" s2s:transferAmount=""25075"" s2s:transferException=""0"" s2s:transferDateTime=""2020-05-16T19:34:10"" s2s:transferSequence=""0"" s2s:transactionId=""333""/></s2s:voucher><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.438"" s2s:commandId=""53308"" s2s:sessionId=""3233"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""0""><s2s:redeemVoucher s2s:clientType=""egm"" s2s:clientId=""AMT_PARMCMTY1-1-011"" s2s:voucherId=""" + sVoucherId + @""" s2s:cardId=""0"" s2s:transactionId=""334""/></s2s:voucher><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.453"" s2s:commandId=""53309"" s2s:sessionId=""3234"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""0""><s2s:issueVoucher s2s:clientType=""egm"" s2s:clientId=""AMT_PARMCMTY1-1-011"" s2s:voucherId=""" + sVoucherId + @""" s2s:voucherAmount=""25075"" s2s:currencyId=""USD"" s2s:largeWin=""false"" s2s:creditType=""cashable"" s2s:poolId=""0"" s2s:cardId=""0"" s2s:cardRestrict=""anyCard"" s2s:egmDateTime=""2020-06-16T19:34:10"" s2s:cageDateTime=""2020-06-16T19:34:10"" s2s:transferAction=""issued"" s2s:transferAmount=""25075"" s2s:transferException=""0"" s2s:transferDateTime=""2020-05-16T19:34:10"" s2s:transferSequence=""0"" s2s:transactionId=""335""/></s2s:voucher><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.469"" s2s:commandId=""53310"" s2s:sessionId=""3235"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""0""><s2s:issueVoucher s2s:clientType=""egm"" s2s:clientId=""AMT_PARMCMTY1-1-011"" s2s:voucherId=""" + sVoucherId + @""" s2s:voucherAmount=""25075"" s2s:currencyId=""USD"" s2s:largeWin=""false"" s2s:creditType=""cashable"" s2s:poolId=""0"" s2s:cardId=""0"" s2s:cardRestrict=""anyCard"" s2s:egmDateTime=""2020-06-16T19:34:10"" s2s:cageDateTime=""2020-06-16T19:34:10"" s2s:transferAction=""issued"" s2s:transferAmount=""25075"" s2s:transferException=""0"" s2s:transferDateTime=""2020-05-16T19:34:10"" s2s:transferSequence=""0"" s2s:transactionId=""337""/></s2s:voucher><s2s:communications s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:14.485"" s2s:commandId=""53311""><s2s:commsOffLine/></s2s:communications><s2s:voucher s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:15.202"" s2s:commandId=""53312"" s2s:sessionId=""3236"" s2s:sessionType=""request"" s2s:timeToLive=""30000"" s2s:sessionRetry=""0""><s2s:issueVoucher s2s:clientType=""egm"" s2s:clientId=""AMT_PARMCMTY1-1-011"" s2s:voucherId=""" + sVoucherId + @""" s2s:voucherAmount=""25075"" s2s:currencyId=""USD"" s2s:largeWin=""false"" s2s:creditType=""cashable"" s2s:poolId=""0"" s2s:cardId=""0"" s2s:cardRestrict=""anyCard"" s2s:egmDateTime=""2020-06-16T19:34:10"" s2s:cageDateTime=""2020-06-16T19:34:10"" s2s:transferAction=""issued"" s2s:transferAmount=""25075"" s2s:transferException=""0"" s2s:transferDateTime=""2020-05-16T19:34:10"" s2s:transferSequence=""0"" s2s:transactionId=""338""/></s2s:voucher><s2s:communications s2s:propertyId=""456"" s2s:dateTime=""2020-05-17T15:36:15.452"" s2s:commandId=""53314""><s2s:commsOnLine s2s:s2sVersion=""1.2.6"" s2s:s2sLocation=""http://localhost:8891""/></s2s:communications></s2s:s2sBody></s2s:s2sMessage>";

			this.response = this.serviceA.S2SMessagePostOperation(new S2SMessagePostOperationRequest
			{
				s2sRequest = new RequestType
				{
					request = amtoteForcedBugRequestMessage
				}
			});
			System.Diagnostics.Debugger.Log(1, System.Diagnostics.Debugger.DefaultCategory, string.Format("Response: '{0}'.", this.response.s2sResponse.response));
			System.Threading.Thread.Sleep(1000);
		}

		private void CreateAndInitializeHostListener(ref ServiceHost gsaS2sServiceHost)
		{
			Log.Message(string.Format("GsaS2sService Start. Uri: '{0}'.", gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri));

			ServiceMetadataBehavior serviceMetadataBehavior = new ServiceMetadataBehavior()
			{
				HttpGetEnabled = true
			};
			serviceMetadataBehavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;

			gsaS2sServiceHost.Description.Behaviors.Add(serviceMetadataBehavior);
			gsaS2sServiceHost.AddServiceEndpoint(typeof(IS2SGsaService), new BasicHttpBinding(), gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri);
			gsaS2sServiceHost.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexHttpBinding(), "mex");
			gsaS2sServiceHost.Faulted += this.OnServiceHostFaulted;
			gsaS2sServiceHost.Closed += this.OnServiceHostClosed;
			gsaS2sServiceHost.Open();

			Log.Message(string.Format("S2S GsaS2sService: Started on '{0}'.", gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri));
		}

		private void OnServiceHostClosed(object sender, EventArgs e)
		{
			Log.Message("S2S GsaS2sService: Closed");
		}

		private void OnServiceHostFaulted(object sender, EventArgs e)
		{
			Log.Message("S2S GsaS2sService: Faulted");
		}

		private void ConsoleCancelEventHandler(Object Sender, ConsoleCancelEventArgs Event)
		{
			Event.Cancel = (Event.SpecialKey == ConsoleSpecialKey.ControlC);
			RequestOrderlyShutdown("CTRL-C");
		}

		private string GenerateNumber()
		{
			Random random = new Random();
			string r = string.Empty;
			int i;
			for (i = 1; i < 19; i++)
			{
				r += random.Next(0, 9).ToString();
			}
			return r;
		}
	}

	// TODO: Refactor. Remove from here
	public class WigosLogger : WigosHubCommon.ILogger
	{
		public void Log(LogLevel Level, string Message)
		{
			if (Level >= this.LogLevel)
			{
				WSI.Common.Log.Message(Message);
			}
		}

		public void Exception(Exception _ex)
		{
			WSI.Common.Log.Exception(_ex);
		}

		public void Chain(WigosHubCommon.ILogger SubLogger)
		{
			throw new NotImplementedException();
		}

		public LogLevel LogLevel { get; set; }

		public ILogger Parent { get; set; }
	}

}