﻿using System;
using WigosHubCommon;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.UnitTests
{
	public class WigosLogger : ILogger
	{
		public void Log(LogLevel Level, string Message)
		{
			if (Level >= this.LogLevel)
			{
				WSI.Common.Log.Message(Message);
			}
		}

		public void Exception(Exception _ex)
		{
			WSI.Common.Log.Exception(_ex);
		}

		public void Chain(WigosHubCommon.ILogger SubLogger)
		{
			throw new NotImplementedException();
		}

		public LogLevel LogLevel { get; set; }

		public ILogger Parent { get; set; }
	}
}
