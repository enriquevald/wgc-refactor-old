﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Bootstrapper;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Managers;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.UnitTests
{
	[TestClass]
	public partial class TicketTests
	{
		private const int WaitInMillisecondsByCommand = 1000;

		private readonly IUnityContainer container;
		private readonly IXmlSerializerService xmlSerializerService;

		private Mock<IProtocolManager> protocolManagerMocked;
		private List<s2sMessage> originatorInboundMessages;
		private Action<s2sMessage> originatorInboundMessageReceiver;

		public TicketTests()
		{
			this.container = new UnityContainer();
			Unity.Configure(this.container);
			this.originatorContainer = new UnityContainer();
			Unity.Configure(this.originatorContainer);
			this.receiverContainer = new UnityContainer();
			Unity.Configure(this.receiverContainer);

			this.xmlSerializerService = this.container.Resolve<IXmlSerializerService>();
			this.originatorInboundMessages = new List<s2sMessage>();

			this.InitializeOriginatorMockedService();
			this.InitializeWigosSubSystems();
			this.InitializeGsaS2S();

			this.dateTimeForTest = DateTime.Now.AddYears(1);
		}

		[TestInitialize]
		public void Initialize()
		{
			this.protocolManagerMocked.ResetCalls();
			this.originatorInboundMessages.Clear();
			this.originatorInboundMessageReceiver = null;
		}

		/// <summary>
		/// Check that the sending of an GetValidationIds command is as expected
		/// </summary>
		[TestMethod]
		[TestCategory("VoucherClassTests")]
		public void GetValidationIdsIsAsExpected()
		{
			object[] commandsToSend = { this.CreateGetValidationIdsCommand(TicketTests.TestingTerminalId) };
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);
			this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[0]);

			s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[0]);
			voucher voucher = messageBody.Items[0] as voucher;
			validationIdList validationIdList = voucher != null ? voucher.Item as validationIdList : null;

			Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
			Assert.AreNotEqual(null, validationIdList, "Received command is not a validationIdList");
			Assert.AreNotEqual(0, validationIdList.validationIdItem.Length, "Received validationIdList command has not any validationId");
			Assert.AreEqual(validationIdList.validationIdItem.Length, validationIdList.validationIdItem.Select(i => i.validationId).Distinct().Count(), "Received validationIdList command has some repeated validationId");
		}

		/// <summary>
		/// Check that the sending of an IssueVoucher command is as expected
		/// </summary>
		[TestMethod]
		[TestCategory("VoucherClassTests")]
		public void IssueVoucherAsExpected()
		{
			string voucherId = this.GenerateRandomNumber();
			object[] commandsToSend = { this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId) };
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);
			this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[0]);

			s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[0]);
			voucher voucher = messageBody.Items[0] as voucher;
			ackVoucher ackVoucher = voucher != null ? voucher.Item as ackVoucher : null;

			Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
			Assert.IsTrue(string.IsNullOrEmpty(voucher.errorCode) && string.IsNullOrEmpty(voucher.errorText), "Received voucher command has a returned error");
			Assert.AreNotEqual(null, ackVoucher, "Received command is not an ackVoucher");
		}

		/// <summary>
		/// Check that the sending of an IssueVoucher command is as expected
		/// </summary>
		[TestMethod]
		[TestCategory("VoucherClassTests")]
		public void AmtoteForcedBugTestMethod()
		{
			string voucherId = this.GenerateRandomNumber();
			object[] commandsToSend = 
			{
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId),
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId), 
				this.CreateCommitVoucherCommand(voucherId, TicketTests.TestingTerminalId), 
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId), 
				this.CreateCommitVoucherCommand(voucherId, TicketTests.TestingTerminalId)
			};
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);

			int issueCommandsReceived = 0;
			int redeemCommandsReceived = 0;
			int commitCommandsReceived = 0;


			bool isFirstCommitVoucherExecuted = false;

			for (int commandIndex = 0; commandIndex < this.originatorInboundMessages.Count; commandIndex++)
			{
				this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[commandIndex]);

				s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[commandIndex]);
				voucher voucher = messageBody.Items[0] as voucher;
				ackVoucher ackVoucher = voucher.Item as ackVoucher;
				authorizeVoucher authorizeVoucher = voucher.Item as authorizeVoucher;

				voucher senderVoucher = commandsToSend[commandIndex] as voucher;
				issueVoucher senderIssueVoucher = senderVoucher.Item as issueVoucher;
				redeemVoucher senderRedeemVoucher = senderVoucher.Item as redeemVoucher;
				commitVoucher senderCommitVoucher = senderVoucher.Item as commitVoucher;

				Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
				bool isVoucherWithError = !string.IsNullOrEmpty(voucher.errorCode) || !string.IsNullOrEmpty(voucher.errorText);
				bool hasCorrespondingCoupleSentReceivedCommand =
					((senderIssueVoucher != null || senderCommitVoucher != null) && (ackVoucher != null || isVoucherWithError))
					|| (senderRedeemVoucher != null && (authorizeVoucher != null || isVoucherWithError));
				Assert.IsTrue(hasCorrespondingCoupleSentReceivedCommand, "Received command are not corresponding with the sent one");

				if (senderIssueVoucher != null)
				{
					Assert.IsTrue(!isVoucherWithError, string.Format("Received unexpected error in an IssueVoucher command. CommandIndex: {0}", commandIndex));
					issueCommandsReceived++;
				}
				else if (senderRedeemVoucher != null)
				{
					if (issueCommandsReceived == 1 && commitCommandsReceived == 0)
					{
						Assert.IsTrue(!isVoucherWithError, string.Format("Received unexpected error in an RedeemVoucher command. CommandIndex: {0}", commandIndex));
					}
					else
					{
						Assert.IsTrue(isVoucherWithError, string.Format("Received unexpected authorizeVoucher in a RedeemVoucher command. CommandIndex: {0}", commandIndex));
					}
					redeemCommandsReceived++;
				}
				else if (commitCommandsReceived != null)
				{
					if (issueCommandsReceived == 1 && redeemCommandsReceived >= 1 && commitCommandsReceived == 0)
					{
						Assert.IsTrue(!isVoucherWithError, string.Format("Received unexpected error in a commitVoucher command. CommandIndex: {0}", commandIndex));
					}
					else
					{
						Assert.IsTrue(isVoucherWithError, string.Format("Received unexpected ackVoucher in a commitVoucher command. CommandIndex: {0}", commandIndex));
					}
					commitCommandsReceived++;
				}
			}

			int numberOfSameValidationIdsInDataBase;
			using (DB_TRX dataBase = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM tickets WHERE ti_validation_number = " + voucherId, dataBase.SqlTransaction.Connection, dataBase.SqlTransaction))
			{
				numberOfSameValidationIdsInDataBase = (int)sqlCommand.ExecuteScalar();
			}

			Assert.AreEqual(commandsToSend.Count(i => i is voucher && ((voucher)i).Item is issueVoucher), numberOfSameValidationIdsInDataBase, "Unexpected number of validation numbers in the database");
		}

		/// <summary>
		/// Check that the sending of an IssueVoucher command is as expected
		/// </summary>
		[TestMethod]
		[TestCategory("VoucherClassTests")]
		public void NoMoreThanOneValidationVoucherForIssue()
		{
			string voucherId = this.GenerateRandomNumber();
			object[] commandsToSend = 
			{
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId)
			};
			issueVoucher lastIssueVoucher = ((issueVoucher)((voucher)commandsToSend[commandsToSend.Length - 2]).Item);
			lastIssueVoucher.voucherAmount += 100;
			lastIssueVoucher.transferAmount += 100;
			
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);

			for (int commandIndex = 0; commandIndex < this.originatorInboundMessages.Count; commandIndex++)
			{
				this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[commandIndex]);

				s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[commandIndex]);
				voucher voucher = messageBody.Items[0] as voucher;
				Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
				bool isVoucherWithError = !string.IsNullOrEmpty(voucher.errorCode) || !string.IsNullOrEmpty(voucher.errorText);

				ackVoucher ackVoucher = voucher.Item as ackVoucher;
				if (commandIndex != commandsToSend.Length - 1)
				{
					Assert.IsTrue(
						!isVoucherWithError && voucher.Item is ackVoucher,
						commandIndex == 0
							? "Received unexpected error in first IssueVoucher command"
							: "Received unexpected error in secondary IssueVoucher commands");
				}
				else
				{
					Assert.IsTrue(isVoucherWithError, "Received unexpected voucher without error RedeemVoucher command");
				}
			}

			int numberOfSameValidationIdsInDataBase;
			using (DB_TRX dataBase = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM tickets WHERE ti_validation_number = " + voucherId, dataBase.SqlTransaction.Connection, dataBase.SqlTransaction))
			{
				numberOfSameValidationIdsInDataBase = (int)sqlCommand.ExecuteScalar();
			}

			Assert.AreEqual(3, numberOfSameValidationIdsInDataBase, "Unexpected number of validation numbers in the database");
		}

		/// <summary>
		/// Check that the sending goldpath flow for Issue, Redeem and Commit commands are Ok
		/// </summary>
		[TestMethod]
		[TestCategory("VoucherClassTests")]
		public void IssueRedeemCommitFlowAsExpected()
		{
			string voucherId = this.GenerateRandomNumber();
			object[] commandsToSend = 
			{
				this.CreateIssueVoucherCommand(voucherId, TicketTests.TestingTerminalId, DateTime.Now),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId),
				this.CreateCommitVoucherCommand(voucherId, TicketTests.TestingTerminalId)
			};		
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);

			for (int commandIndex = 0; commandIndex < this.originatorInboundMessages.Count; commandIndex++)
			{
				this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[commandIndex]);

				s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[commandIndex]);
				voucher voucher = messageBody.Items[0] as voucher;
				Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
				bool isVoucherWithError = !string.IsNullOrEmpty(voucher.errorCode) || !string.IsNullOrEmpty(voucher.errorText) || voucher.Item == null;

				Assert.IsTrue(!isVoucherWithError, string.Format("Received unexpected error in commandIndex = {0}", commandIndex));
			}

			int numberOfSameValidationIdsInDataBase;
			using (DB_TRX dataBase = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM tickets WHERE ti_validation_number = " + voucherId, dataBase.SqlTransaction.Connection, dataBase.SqlTransaction))
			{
				numberOfSameValidationIdsInDataBase = (int)sqlCommand.ExecuteScalar();
			}

			Assert.AreEqual(1, numberOfSameValidationIdsInDataBase, "Unexpected number of validation numbers in the database");
		}

		/// <summary>
		/// Check that the sending goldpath flow for Issue, Redeem and Commit commands are Ok with a returned normalized ClientId
		/// </summary>
		/// <remarks>MANUAL TEST</remarks>
		[TestMethod]
		[Ignore]
		[TestCategory("VoucherClassTests")]
		public void IssueRedeemCommitFlowWithNormalizedClientIdInRedeemAndCommit()
		{
			string voucherId = "707677401041560366";	// This ticket must have a valid status to redeem and test flow and created terminal Id is invalid for S2S in DB
			object[] commandsToSend = 
			{
				//this.CreateIssueVoucherCommand(voucherId, notNormalizedClientId, DateTime.Now),
				this.CreateRedeemVoucherCommand(voucherId, TicketTests.TestingTerminalId),
				this.CreateCommitVoucherCommand(voucherId, TicketTests.TestingTerminalId)
			};
			s2sMessage response = this.originatorServiceClient.S2SMessagePostOperation(this.CreateS2SMessage(commandsToSend).Build());
			Log.Message(this.xmlSerializerService.Serialize(response));

			this.CheckResponseAsserts(response);

			this.WaitForExecutionOfAllCommandsOrMaxDelay(commandsToSend.Length);
			this.AssertReceivedCommands(commandsToSend.Length);

			for (int commandIndex = 0; commandIndex < this.originatorInboundMessages.Count; commandIndex++)
			{
				this.CheckCommonInboundMessageAsserts(this.originatorInboundMessages[commandIndex]);

				s2sBody messageBody = this.GetMessageBody(this.originatorInboundMessages[commandIndex]);
				voucher voucher = messageBody.Items[0] as voucher;
				Assert.AreNotEqual(null, voucher, "Received command is not a voucher");
				bool isVoucherWithError = !string.IsNullOrEmpty(voucher.errorCode) || !string.IsNullOrEmpty(voucher.errorText) || voucher.Item == null;

				Assert.IsTrue(!isVoucherWithError, string.Format("Received unexpected error in commandIndex = {0}", commandIndex));

				ackVoucher ackVoucher = voucher.Item as ackVoucher;
				if (ackVoucher != null)
				{
					Assert.IsFalse(ackVoucher.clientId.Contains(" "), string.Format("Received not normalized ClientId in ackVoucher commandIndex = {0}", commandIndex));
				}
				else
				{
					authorizeVoucher authorizeVoucher = voucher.Item as authorizeVoucher;
					if (authorizeVoucher != null)
					{
						Assert.IsFalse(authorizeVoucher.clientId.Contains(" "), string.Format("Received not normalized ClientId in authorizeVoucher commandIndex = {0}", commandIndex));
					}
				}
			}

			int numberOfSameValidationIdsInDataBase;
			using (DB_TRX dataBase = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM tickets WHERE ti_validation_number = " + voucherId, dataBase.SqlTransaction.Connection, dataBase.SqlTransaction))
			{
				numberOfSameValidationIdsInDataBase = (int)sqlCommand.ExecuteScalar();
			}

			Assert.AreEqual(1, numberOfSameValidationIdsInDataBase, "Unexpected number of validation numbers in the database");
		}

		private void InitializeOriginatorMockedService()
		{
			this.protocolManagerMocked = new Mock<IProtocolManager>();

			this.protocolManagerMocked.Setup(m => m.Receive(It.IsAny<string>()))
				.Returns(
					(string request) =>
					{
						s2sMessage message = this.xmlSerializerService.Deserialize<s2sMessage>(request);

						this.originatorInboundMessages.Add(message);
						if (this.originatorInboundMessageReceiver != null)
						{
							this.originatorInboundMessageReceiver(message);
						}

						s2sHeader requestHeader = message.Items.OfType<s2sHeader>().First();

						IS2SMessageBuilder s2SMessageBuilder = new S2SMessageBuilder();
						IS2SAckBuilder s2SAckBuilder = new S2SAckBuilder();

						s2SAckBuilder.SetFromSystem(requestHeader.toSystem);
						s2SAckBuilder.SetToSystem(requestHeader.fromSystem);
						s2SAckBuilder.SetDateTimeSent(DateTime.Now);
						s2SAckBuilder.SetMessageId(requestHeader.messageId);

						s2SMessageBuilder.Add(s2SAckBuilder.Build());
						return this.xmlSerializerService.Serialize(s2SMessageBuilder.Build());
					});

			ILogService logService = this.originatorContainer.Resolve<ILogService>();
			this.originatorService = new Services.V1_2_2.S2SGsaService(protocolManagerMocked.Object, logService);
		}

		private void WaitForExecutionOfAllCommandsOrMaxDelay(int numberOfCommandsToWait)
		{
			Stopwatch chronometer = new Stopwatch();
			chronometer.Start();

			while (this.originatorInboundMessages.Count < numberOfCommandsToWait || chronometer.ElapsedMilliseconds < (TicketTests.WaitInMillisecondsByCommand * numberOfCommandsToWait))
			{
				// Do nothing while waiting
			}

			chronometer.Stop();
		}

		private void CheckResponseAsserts(s2sMessage response)
		{
			Assert.AreNotEqual(null, response, "No response message received");
			Assert.IsTrue(response.Items != null && response.Items.Length == 1 && response.Items[0] is s2sAck, "No valid s2sAck command response received");
		}

		private void CheckCommonInboundMessageAsserts(s2sMessage message)
		{
			Assert.AreNotEqual(null, message, "No message received");
			Assert.AreNotEqual(null, this.GetMessageHeader(message), "No header in received message");
			s2sBody messageBody = this.GetMessageBody(message);
			Assert.AreNotEqual(null, messageBody, "No body in received message");
			Assert.AreNotEqual(null, messageBody.Items, "Body received has not commands");
			Assert.AreNotEqual(0, messageBody.Items.Length, "Body received has not commands");
		}

		private void AssertReceivedCommands(int numberOfCommandsExpected)
		{
			this.protocolManagerMocked.Verify(m => m.Receive(It.IsAny<string>()), Times.Exactly(numberOfCommandsExpected), "Number of commands received was wrong");
		}
	}
}