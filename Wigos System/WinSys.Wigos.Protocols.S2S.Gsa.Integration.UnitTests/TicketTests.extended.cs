﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using Microsoft.Practices.Unity;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Builders;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Clients.V1_2_2;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Factories;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.ClassesV1_2_6;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Integration.UnitTests
{
	public partial class TicketTests : CommonService
	{
		private const string OriginatorSystemUrl = "http://localhost:8890";
		private const string ReceiverSystemUrl = "http://localhost:8891";
		private const string OriginatorSystemIdentifier = "OriginatorSystem";
		private const string ReceiverSystemIdentifier = "ReceiverSystem";
		private const string ConfigFileName = "WSI.Testing.Configuration";
		private const string CurrencyId = "USD";
		private const string TestingTerminalId = "AMT_TEST-12-345";

		private IUnityContainer originatorContainer;
		private IUnityContainer receiverContainer;
		private IS2SGsaService originatorService;
		private IS2SGsaService receiverService;
		private ServiceHost gsaS2sOriginatorServiceHost;
		private ServiceHost gsaS2sReceiverServiceHost;
		private IServiceClient originatorServiceClient;
		private DateTime dateTimeForTest;
		private Managers.IProtocolState originatorProtocolState;
		private Managers.IProtocolState receiverProtocolState;

		private void InitializeWigosSubSystems()
		{
			Log.AddListener(new SimpleLogger(this.m_log_prefix));

			if (this.m_console)
			{
				Log.AddListener(new WSI.Common.ConsoleLogger());
				Console.CancelKeyPress += this.ConsoleCancelEventHandler;
			}

			if (!ConfigurationFile.Init(TicketTests.ConfigFileName, string.Empty))
			{
				Log.Error(" Reading application configuration settings. Application stopped");
				return;
			}

			WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
			WGDB.SetApplication(this.m_service_name, this.m_version);
			WGDB.ConnectAs("WGROOT");

			while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
			{
				Log.Warning("Waiting for database connection ...");

				System.Threading.Thread.Sleep(5000);
			}

			Resource.Init();

			this.m_ip_address = ConfigurationFile.GetSetting("LocalIP");
			if (String.IsNullOrEmpty(this.m_ip_address))
			{
				this.m_ip_address = IPAddress.Any.ToString();

				Log.Error(" No Discovery Service IP address is available. Application stopped");

				return;
			}

			Alarm.Register(AlarmSourceCode.Service, 0, this.m_alarm_source, AlarmCode.Service_Started);

			this.OnServiceBeforeInit();

			if (this.m_stand_by)
			{
				Alarm.Register(AlarmSourceCode.Service, 0, this.m_alarm_source, AlarmCode.Service_StandBy);
			}
		}

		private void InitializeGsaS2S()
		{
			this.originatorProtocolState = this.originatorContainer.Resolve<Managers.IProtocolState>();
			this.receiverProtocolState = this.receiverContainer.Resolve<Managers.IProtocolState>();
			
			this.receiverService = this.receiverContainer.Resolve<IS2SGsaService>();

			this.gsaS2sOriginatorServiceHost = new ServiceHost(this.originatorService, new Uri(TicketTests.OriginatorSystemUrl));
			this.gsaS2sReceiverServiceHost = new ServiceHost(this.receiverService, new Uri(TicketTests.ReceiverSystemUrl));
			
			this.CreateAndInitializeHostListener(ref this.gsaS2sOriginatorServiceHost);
			this.CreateAndInitializeHostListener(ref this.gsaS2sReceiverServiceHost);

			this.ServicesHandShacking();
			
			this.originatorServiceClient = this.originatorContainer.Resolve<IServiceClientFactory>().Create(TicketTests.ReceiverSystemIdentifier);
		}

		private void ServicesHandShacking()
		{
			this.originatorProtocolState.SetRemoteState(
				TicketTests.ReceiverSystemIdentifier,
				new Entities.State
				{
					RemoteEndpoint = TicketTests.ReceiverSystemUrl,
					FromSystem = TicketTests.ReceiverSystemIdentifier,
					ToSystem = TicketTests.OriginatorSystemIdentifier
				}
			);

			this.receiverProtocolState.SetRemoteState(
				TicketTests.OriginatorSystemIdentifier,
				new Entities.State
				{
					RemoteEndpoint = TicketTests.OriginatorSystemUrl,
					FromSystem = TicketTests.OriginatorSystemIdentifier,
					ToSystem = TicketTests.ReceiverSystemIdentifier
				}
			);
		}

		private string GenerateRandomNumber()
		{
			Random random = new Random();
			string result = string.Empty;

			for (int i = 1; i < 19; i++)
			{
				result += random.Next(0, 9).ToString();
			}

			return result;
		}

		/* Class Objects */
		private communications CreateCommunicationsCommand()
		{
			return new communications
			{
				sessionTypeSpecified = false,
				dateTime = dateTimeForTest,
				propertyId = "042"
			};
		
		}
		private voucher CreateVoucherCommand() 
		{
			return new voucher
			 {
				 propertyId = "042",
				 dateTime = dateTimeForTest,
				 timeToLiveSpecified = true,
				 timeToLive = 30000,
				 sessionTypeSpecified = true,
				 sessionType = sessionType.request
			 };
		}
		
		/* Communications Commands */
		private communications CreateCommsOnLineCommand(string locationUri)
		{
			communications commsOnLineCommand = this.CreateCommunicationsCommand();
			commsOnLineCommand.Item = new commsOnLine
			{
				s2sVersion = "1.2.6",
				s2sLocation = locationUri
			};

			return commsOnLineCommand;
		}

		private communications CreateCommsOffLineCommand(string locationUri)
		{
			communications commsOffLineCommand = this.CreateCommunicationsCommand();
			commsOffLineCommand.Item = new commsOffLine();

			return commsOffLineCommand;
		}

		private communications CreateCommsRestoredCommand(string locationUri)
		{
			communications commsRestoredCommand = this.CreateCommunicationsCommand();
			commsRestoredCommand.Item = new commsRestored();

			return commsRestoredCommand;
		}

		private communications CreateCommsLostCommand(string locationUri)
		{
			communications commsLostCommand = this.CreateCommunicationsCommand();
			commsLostCommand.Item = new commsOffLine();

			return commsLostCommand;
		}
		
		/* Voucher Commands */
		private voucher CreateIssueVoucherCommand(string validationId, string clientId, DateTime? transferDateTime = null) 
		{
			voucher issueVoucherCommand = this.CreateVoucherCommand();

			DateTime voucherTransferDateTime = transferDateTime.HasValue ? transferDateTime.Value : this.dateTimeForTest;

			issueVoucherCommand.Item = new issueVoucher
			{
				cageDateTime = this.dateTimeForTest,
				cardId = string.Empty,
				cardRestrict = cardRestrict.anyCard,
				clientId = clientId,
				clientType = clientType.egm,
				creditType = creditTypes.cashable,
				currencyId = TicketTests.CurrencyId,
				egmDateTime = this.dateTimeForTest,
				expireCredits = false,
				expireDateTime = this.dateTimeForTest,
				largeWin = false,
				transferAction = voucherAction.issued,
				transferAmount = 123456,
				transferDateTime = voucherTransferDateTime,
				voucherAmount = 123456,
				voucherId = validationId
			};

			return issueVoucherCommand;
		}

		private voucher CreateRedeemVoucherCommand(string validationId, string clientId) 
		{
			voucher redeemVoucherCommand = this.CreateVoucherCommand();
			redeemVoucherCommand.Item = new redeemVoucher 
			{
				cardId = string.Empty,
				clientId = clientId,
				clientType = clientType.egm,
				voucherId = validationId
			};

			return redeemVoucherCommand;
		}

		private voucher CreateCommitVoucherCommand(string validationId, string clientId) 
		{
			voucher commitVoucherCommand = this.CreateVoucherCommand();
			commitVoucherCommand.Item = new commitVoucher 
			{
				cageDateTime = this.dateTimeForTest,
				cardId = string.Empty,
				cardRestrict = cardRestrict.anyCard,
				clientId = clientId,
				clientType = clientType.egm,
				currencyId = CurrencyId,
				egmDateTime = this.dateTimeForTest,
				creditType = creditTypes.cashable,
				expireCredits=false,
				expireDateTime = this.dateTimeForTest,
				largeWin = false,
				transferAction = voucherAction.redeemed,
				transferAmount = 123456,
				transferDateTime = this.dateTimeForTest,
				voucherAmount = 123456,
				voucherId = validationId
			};

			return commitVoucherCommand;
		}

		private voucher CreateGetValidationIdsCommand(string clientId) 
		{
			voucher getValidationIdsCommand = this.CreateVoucherCommand();

			getValidationIdsCommand.Item = new getValidationIds
			{
				clientId = clientId,
				clientType = clientType.egm
			};

			return getValidationIdsCommand;
		}

		private voucher CreateGetVoucherConfigCommand(string clientId)
		{
			voucher getVoucherConfigCommand = this.CreateVoucherCommand();

			getVoucherConfigCommand.Item = new getVoucherConfig
			{
				clientId = clientId,
				clientType = clientType.egm
			};

			return getVoucherConfigCommand;
		}

		private IS2SMessageBuilder CreateS2SMessage(object[] commandsToSend)
		{
			IS2SMessageBuilder request = new S2SMessageBuilder();

			request.Add(new s2sHeader
			            {
				            dateTimeSent = this.dateTimeForTest,
				            fromSystem = TicketTests.OriginatorSystemIdentifier,
				            toSystem = TicketTests.ReceiverSystemIdentifier
			            });

			request.Add(new s2sBody
			            {
				            Items = commandsToSend
			            });

			return request;
		}
		
		private s2sHeader GetMessageHeader(s2sMessage message)
		{
			return message.Items.OfType<s2sHeader>().FirstOrDefault();
		}

		private s2sBody GetMessageBody(s2sMessage message)
		{
			return message.Items.OfType<s2sBody>().FirstOrDefault();
		}

		private s2sAck GetMessageAck(s2sMessage message)
		{
			return message.Items.OfType<s2sAck>().FirstOrDefault();
		}

		private void CreateAndInitializeHostListener(ref ServiceHost gsaS2sServiceHost)
		{
			Log.Message(string.Format("GsaS2sService Start. Uri: '{0}'.", gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri));
			ServiceMetadataBehavior serviceMetadataBehavior = new ServiceMetadataBehavior()
			{
				HttpGetEnabled = true
			};
			serviceMetadataBehavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
			gsaS2sServiceHost.Description.Behaviors.Add(serviceMetadataBehavior);
			gsaS2sServiceHost.Description.Behaviors.Find<ServiceBehaviorAttribute>().InstanceContextMode = InstanceContextMode.Single;
			gsaS2sServiceHost.AddServiceEndpoint(typeof(IS2SGsaService), new BasicHttpBinding(), gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri);
			gsaS2sServiceHost.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexHttpBinding(), "mex");
			gsaS2sServiceHost.Faulted += this.OnServiceHostFaulted;
			gsaS2sServiceHost.Closed += this.OnServiceHostClosed;
			gsaS2sServiceHost.Open();
			Log.Message(string.Format("S2S GsaS2sService: Started on '{0}'.", gsaS2sServiceHost.BaseAddresses[0].AbsoluteUri));
		}

		private void OnServiceHostClosed(object sender, EventArgs e)
		{
			Log.Message("S2S GsaS2sService: Closed");
		}

		private void OnServiceHostFaulted(object sender, EventArgs e)
		{
			Log.Message("S2S GsaS2sService: Faulted");
		}

		private void ConsoleCancelEventHandler(Object Sender, ConsoleCancelEventArgs Event)
		{
			Event.Cancel = (Event.SpecialKey == ConsoleSpecialKey.ControlC);
			RequestOrderlyShutdown("CTRL-C");
		}
	}
}
