//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SqlProcedures.cs
// DESCRIPTION:   
// AUTHOR:        Sergi Mart�nez
// CREATION DATE: 21-OCT-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 21-OCT-2013  SMN    Initial version
// 17-FEB-2014  RCI    Not used anymore
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  // RCI 17-FEB-2014: Not used anymore.

  //public static class SqlMovementsProcedures
  //{
  //  [SqlProcedure]
  //  public static void SP_ICM(Int64 pOperationId,
  //                            Int64 pAccountId,
  //                            String pCardTrackData,
  //                            Int32 pType,
  //                            Decimal pInitialBalance,
  //                            Decimal pAddAmount,
  //                            Decimal pSubAmount,
  //                            Decimal pFinalBalance,
  //                            Decimal pBalanceIncrement,
  //                            String pDetails,
  //                            String pCurrencyIsoCode,
  //                            Decimal pAuxAmount,
  //                            Int64 pCashierSessionId,
  //                            Int64 pTerminalId,
  //                            String pTerminalName,
  //                            Int64 pAuthorizedByUserId,
  //                            String pAuthorizedByUserName,
  //                            Decimal pCurrencyDenomination,
  //                            Int64 pMovementId,
  //                            Int64 pGamingTableSessionId)
  //  {

  //    StringBuilder _sb;
  //    // Int32 _num_rows_inserted;
  //    SqlParameter _param_iso_code;
  //    SqlParameter _param_currency_type;
  //    SqlParameter _param_increment;
  //    SqlParameter _param_currency_increment;
  //    SqlParameter _param_in_cashier_sessions;
  //    SqlParameter _param_in_cashier_sessions_by_currency;
  //    String _national_currency;
  //    CASHIER_MOVEMENT _movement;

  //    Int64 _first_account_id;
  //    String _track_data;

  //    using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
  //    {
  //      _conn.Open();

  //      using (SqlTransaction _trx = _conn.BeginTransaction())
  //      {
  //        _trx.Save("SP_ICM");

  //        try
  //        {

  //          _first_account_id = 0;
  //          _track_data = "";
  //          //foreach (DataRow _row in m_table.Rows)
  //          //{

  //          if (pAccountId != 0 && String.IsNullOrEmpty(pCardTrackData))
  //          {

  //            //if (_row.IsNull("CM_ACCOUNT_ID"))
  //            //{
  //            //  continue;
  //            //}

  //            //if (!_row.IsNull("CM_CARD_TRACK_DATA"))
  //            //{
  //            //  continue;
  //            //}

  //            if (_first_account_id == 0)
  //            {
  //              _first_account_id = pAccountId;
  //              _track_data = AccountMovementsTable.ExternalTrackData(_first_account_id, _trx);
  //              pCardTrackData = _track_data;
  //            }
  //            else
  //            {
  //              if (_first_account_id == pAccountId)
  //              {
  //                pCardTrackData = _track_data;
  //              }
  //            }
  //          }
  //          //}

  //          //Update Balance
  //          //DLL 21-AUG-2013: Control register exists in CASHIER_SESSIONS_BY_CURRENCY
  //          _sb = new StringBuilder();
  //          _sb.AppendLine(" IF @InCashierSessions = 1  ");
  //          _sb.AppendLine(" BEGIN ");
  //          _sb.AppendLine("   UPDATE   CASHIER_SESSIONS  ");
  //          _sb.AppendLine("      SET   CS_BALANCE     = CS_BALANCE + (@pBalanceIncrement) ");
  //          _sb.AppendLine("   OUTPUT   DELETED.CS_BALANCE ");
  //          _sb.AppendLine("          , INSERTED.CS_BALANCE  ");
  //          _sb.AppendLine("    WHERE   CS_SESSION_ID  =   @pSessionId ");
  //          _sb.AppendLine("      AND   CS_STATUS     IN ( @pStatusOpen ");
  //          _sb.AppendLine("                             , @pStatusOpenPending ");
  //          _sb.AppendLine("                             , @pStatusPending ) ");
  //          _sb.AppendLine("     END ");
  //          _sb.AppendLine(" IF @InCashierSessionsByCurrency = 1 ");
  //          _sb.AppendLine(" BEGIN  ");
  //          _sb.AppendLine("   IF EXISTS ( SELECT   1  ");
  //          _sb.AppendLine("                 FROM   CASHIER_SESSIONS_BY_CURRENCY ");
  //          _sb.AppendLine("                WHERE   CSC_SESSION_ID = @pSessionId ");
  //          _sb.AppendLine("                  AND   CSC_ISO_CODE   = @pIsoCode ");
  //          _sb.AppendLine("                  AND   CSC_TYPE       = @pType) ");
  //          _sb.AppendLine("       UPDATE   CASHIER_SESSIONS_BY_CURRENCY ");
  //          _sb.AppendLine("          SET   CSC_BALANCE     = CSC_BALANCE + (@pBalanceCurrencyIncrement) ");
  //          _sb.AppendLine("       OUTPUT   DELETED.CSC_BALANCE ");
  //          _sb.AppendLine("              , INSERTED.CSC_BALANCE ");
  //          _sb.AppendLine("        WHERE   CSC_SESSION_ID         = @pSessionId ");
  //          _sb.AppendLine("          AND   CSC_ISO_CODE           = @pIsoCode ");
  //          _sb.AppendLine("          AND   CSC_TYPE               = @pType ");
  //          _sb.AppendLine("          AND   EXISTS ( ");
  //          _sb.AppendLine("                        SELECT CS_SESSION_ID ");
  //          _sb.AppendLine("                          FROM CASHIER_SESSIONS ");
  //          _sb.AppendLine("                         WHERE CS_SESSION_ID  =   @pSessionId ");
  //          _sb.AppendLine("                           AND CS_STATUS     IN ( @pStatusOpen ");
  //          _sb.AppendLine("                                                , @pStatusOpenPending ");
  //          _sb.AppendLine("                                                , @pStatusPending ) ");
  //          _sb.AppendLine("                         ) ");
  //          _sb.AppendLine("  ELSE ");
  //          _sb.AppendLine("     INSERT INTO   CASHIER_SESSIONS_BY_CURRENCY ");
  //          _sb.AppendLine("                 ( CSC_SESSION_ID ");
  //          _sb.AppendLine("                 , CSC_ISO_CODE ");
  //          _sb.AppendLine("                 , CSC_TYPE ");
  //          _sb.AppendLine("                 , CSC_BALANCE ");
  //          _sb.AppendLine("                 , CSC_COLLECTED  ");
  //          _sb.AppendLine("                 ) ");
  //          _sb.AppendLine("          OUTPUT   0.00 ");
  //          _sb.AppendLine("                 , INSERTED.CSC_BALANCE ");
  //          _sb.AppendLine("          VALUES    ");
  //          _sb.AppendLine("                 ( @pSessionId ");
  //          _sb.AppendLine("                 , @pIsoCode ");
  //          _sb.AppendLine("                 , @pType ");
  //          _sb.AppendLine("                 , @pBalanceCurrencyIncrement ");
  //          _sb.AppendLine("                 , 0.00 ");
  //          _sb.AppendLine("                 ) ");
  //          _sb.AppendLine("   END ");

  //          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
  //          {
  //            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = pCashierSessionId;
  //            _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
  //            _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
  //            _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;
  //            _param_iso_code = _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar);
  //            _param_currency_type = _sql_cmd.Parameters.Add("@pType", SqlDbType.Int);
  //            _param_increment = _sql_cmd.Parameters.Add("@pBalanceIncrement", SqlDbType.Money);
  //            _param_currency_increment = _sql_cmd.Parameters.Add("@pBalanceCurrencyIncrement", SqlDbType.Money);
  //            _param_in_cashier_sessions = _sql_cmd.Parameters.Add("@InCashierSessions", SqlDbType.Bit);
  //            _param_in_cashier_sessions_by_currency = _sql_cmd.Parameters.Add("@InCashierSessionsByCurrency", SqlDbType.Bit);

  //            _national_currency = MultiPromos.ReadGeneralParams("RegionalOptions", "CurrencyISOCode", _trx);

  //            // set default values
  //            _param_iso_code.Value = pCurrencyIsoCode;
  //            _param_currency_type.Value = (Int32)CurrencyExchangeType.CURRENCY;
  //            _param_in_cashier_sessions.Value = 1;
  //            _param_in_cashier_sessions_by_currency.Value = 0;
  //            _param_currency_increment.Value = 0;

  //            _movement = (CASHIER_MOVEMENT)pType;
  //            switch (_movement)
  //            {
  //              case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
  //              case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
  //                _param_increment.Value = -pAddAmount;
  //                break;

  //              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
  //              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
  //              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
  //              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
  //              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
  //              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
  //                _param_increment.Value = -pFinalBalance;
  //                if (_movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1 || _movement == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1 ||
  //                    _movement == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1)
  //                {
  //                  _param_currency_increment.Value = pInitialBalance;
  //                }
  //                _param_in_cashier_sessions_by_currency.Value = 1;

  //                if (!String.IsNullOrEmpty(pCurrencyIsoCode) && pCurrencyIsoCode == _national_currency)
  //                {
  //                  switch (_movement)
  //                  {
  //                    case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
  //                    case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
  //                    case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
  //                      _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;
  //                      break;
  //                    case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
  //                    case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
  //                    case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
  //                      _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
  //                      break;
  //                  }
  //                }
  //                break;

  //              case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
  //                _param_in_cashier_sessions_by_currency.Value = 1;
  //                _param_in_cashier_sessions.Value = 0;
  //                _param_increment.Value = 0;
  //                _param_currency_increment.Value = -pSubAmount;
  //                _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
  //                break;

  //              case CASHIER_MOVEMENT.OPEN_SESSION:
  //                _param_increment.Value = pBalanceIncrement;

  //                if (!String.IsNullOrEmpty(pCurrencyIsoCode))
  //                {
  //                  _param_increment.Value = 0;
  //                  _param_currency_increment.Value = pBalanceIncrement;

  //                  _param_in_cashier_sessions_by_currency.Value = 1;
  //                  _param_in_cashier_sessions.Value = 0;
  //                }
  //                break;

  //              case CASHIER_MOVEMENT.FILLER_IN:
  //              case CASHIER_MOVEMENT.FILLER_OUT:
  //              case CASHIER_MOVEMENT.CLOSE_SESSION:
  //              case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN:
  //                _param_currency_increment.Value = pBalanceIncrement;
  //                _param_increment.Value = pBalanceIncrement;
  //                if (!String.IsNullOrEmpty(pCurrencyIsoCode))
  //                {
  //                  _param_increment.Value = 0;
  //                  _param_in_cashier_sessions_by_currency.Value = 1;
  //                  _param_in_cashier_sessions.Value = 0;
  //                }
  //                break;

  //              case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
  //                pInitialBalance = pAddAmount;
  //                pFinalBalance = pAddAmount;
  //                pBalanceIncrement = 0;
  //                pAddAmount = 0;
  //                break;

  //              // 20-SEP-2013 DLL
  //              case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
  //                _param_currency_increment.Value = pBalanceIncrement;
  //                _param_increment.Value = 0;
  //                _param_in_cashier_sessions_by_currency.Value = 1;
  //                _param_in_cashier_sessions.Value = 0;
  //                _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;
  //                break;

  //              // 20-SEP-2013 DLL
  //              case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
  //                _param_currency_increment.Value = pBalanceIncrement;
  //                _param_increment.Value = 0;
  //                _param_in_cashier_sessions_by_currency.Value = 1;
  //                _param_in_cashier_sessions.Value = 0;
  //                _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
  //                break;

  //              case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
  //                _param_in_cashier_sessions_by_currency.Value = 1;
  //                _param_in_cashier_sessions.Value = 1;
  //                _param_increment.Value = 0;
  //                _param_currency_increment.Value = -pSubAmount;
  //                // TODO: Change to CurrencyExchangeType.CASINOCHIP when Boveda is ready to change it.
  //                _param_currency_type.Value = (Int32)CurrencyExchangeType.CURRENCY;
  //                break;

  //              case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
  //                _param_in_cashier_sessions_by_currency.Value = 1;
  //                _param_in_cashier_sessions.Value = 1;
  //                _param_increment.Value = 0;
  //                _param_currency_increment.Value = pAddAmount;
  //                // TODO: Change to CurrencyExchangeType.CASINOCHIP when Boveda is ready to change it.
  //                _param_currency_type.Value = (Int32)CurrencyExchangeType.CURRENCY;
  //                break;

  //              case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
  //                _param_currency_increment.Value = 0;
  //                _param_increment.Value = 0;
  //                if (!String.IsNullOrEmpty(pCurrencyIsoCode))
  //                {
  //                  _param_in_cashier_sessions_by_currency.Value = 1;
  //                  _param_in_cashier_sessions.Value = 0;
  //                }
  //                break;

  //              default:
  //                _param_currency_increment.Value = pBalanceIncrement;
  //                _param_increment.Value = pBalanceIncrement;
  //                break;
  //            }

  //            if (_movement != CASHIER_MOVEMENT.CAGE_FILLER_IN &&
  //                _movement != CASHIER_MOVEMENT.CAGE_CLOSE_SESSION &&
  //                _movement != CASHIER_MOVEMENT.CAGE_FILLER_OUT)
  //            {
  //              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
  //              {
  //                if (!_reader.Read())
  //                {

  //                  throw new Exception();
  //                }

  //                switch (_movement)
  //                {
  //                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
  //                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
  //                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
  //                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
  //                  case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
  //                  case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
  //                  case CASHIER_MOVEMENT.FILLER_OUT_CHECK:

  //                    break;
  //                  default:
  //                    pInitialBalance = _reader.GetDecimal(0);
  //                    pFinalBalance = _reader.GetDecimal(1);
  //                    break;
  //                }

  //              } // using SqlDataReader
  //            } // if
  //          } // using SqlCommand

  //          // Insert the movement
  //          _sb.Length = 0;
  //          _sb.AppendLine(" DECLARE   @_cm_movement_id as BIGINT");
  //          _sb.AppendLine(" DECLARE   @_trackdata AS NVARCHAR(50) ");
  //          _sb.AppendLine(" SET       @_trackdata = '' ");
  //          _sb.AppendLine(" IF @pCardData IS NULL AND @pAccountId IS NOT NULL ");
  //          _sb.AppendLine("   SELECT  @_trackdata = dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId ");
  //          _sb.AppendLine(" IF @_trackdata = '' ");
  //          _sb.AppendLine("   SET     @_trackdata = @pCardData ");

  //          _sb.AppendLine(" INSERT INTO   CASHIER_MOVEMENTS      ");
  //          _sb.AppendLine("             ( CM_SESSION_ID          ");
  //          _sb.AppendLine("             , CM_CASHIER_ID          ");
  //          _sb.AppendLine("             , CM_USER_ID             ");
  //          _sb.AppendLine("             , CM_TYPE                ");
  //          _sb.AppendLine("             , CM_INITIAL_BALANCE     ");
  //          _sb.AppendLine("             , CM_ADD_AMOUNT          ");
  //          _sb.AppendLine("             , CM_SUB_AMOUNT          ");
  //          _sb.AppendLine("             , CM_FINAL_BALANCE       ");
  //          _sb.AppendLine("             , CM_USER_NAME           ");
  //          _sb.AppendLine("             , CM_CASHIER_NAME        ");
  //          _sb.AppendLine("             , CM_CARD_TRACK_DATA     ");
  //          _sb.AppendLine("             , CM_ACCOUNT_ID          ");
  //          _sb.AppendLine("             , CM_OPERATION_ID        ");
  //          _sb.AppendLine("             , CM_DETAILS             ");
  //          _sb.AppendLine("             , CM_CURRENCY_ISO_CODE   ");
  //          _sb.AppendLine("             , CM_AUX_AMOUNT          ");
  //          _sb.AppendLine("             , CM_CURRENCY_DENOMINATION ");
  //          _sb.AppendLine("             , CM_GAMING_TABLE_SESSION_ID ");
  //          _sb.AppendLine("             )                        ");
  //          _sb.AppendLine("   VALUES    ( @pSessionId            ");
  //          _sb.AppendLine("             , @pCashierId            ");
  //          _sb.AppendLine("             , @pUserId               ");
  //          _sb.AppendLine("             , @pType                 ");
  //          _sb.AppendLine("             , @pInitialBalance       ");
  //          _sb.AppendLine("             , @pAddAmount            ");
  //          _sb.AppendLine("             , @pSubAmmount           ");
  //          _sb.AppendLine("             , @pFinalBalance         ");
  //          _sb.AppendLine("             , @pUserName             ");
  //          _sb.AppendLine("             , @pCashierName          ");
  //          _sb.AppendLine("             , @_trackdata             ");
  //          _sb.AppendLine("             , @pAccountId            ");
  //          _sb.AppendLine("             , @pOperationId          ");
  //          _sb.AppendLine("             , @pDetails              ");
  //          _sb.AppendLine("             , @pCurrencyCode         ");
  //          _sb.AppendLine("             , @pAuxAmount            ");
  //          _sb.AppendLine("             , @pCurrencyDenom        ");
  //          _sb.AppendLine("             , @pGamingTableSessionId ");
  //          _sb.AppendLine("             )                        ");
  //          _sb.AppendLine("        SET   @_cm_movement_id = SCOPE_IDENTITY() ");
  //          _sb.AppendLine("IF @pType >= @pCageFirstValue AND @pType <= @pCageLastValue");
  //          _sb.AppendLine("BEGIN");
  //          _sb.AppendLine("  INSERT INTO   CAGE_CASHIER_MOVEMENT_RELATION ");
  //          _sb.AppendLine("       VALUES   (");
  //          _sb.AppendLine("                @pExternMovementId");
  //          _sb.AppendLine("                , @_cm_movement_id");
  //          _sb.AppendLine("                )");
  //          _sb.AppendLine("END");
  //          _sb.AppendLine(" BEGIN TRY");
  //          _sb.AppendLine("      EXEC dbo.CashierMovementsHistory @pSessionId, @pType, @pSubType, @pInitialBalance, @pAddAmount, @pSubAmmount, @pFinalBalance, @pCurrencyCode, @pAuxAmount, @pDate, @pCurrencyDenom");
  //          _sb.AppendLine(" END TRY ");
  //          _sb.AppendLine(" BEGIN CATCH ");
  //          _sb.AppendLine("    DECLARE   @ErrorMessage NVARCHAR(4000); ");
  //          _sb.AppendLine("    DECLARE   @ErrorSeverity INT; ");
  //          _sb.AppendLine("    DECLARE   @ErrorState INT; ");
  //          _sb.AppendLine("    SELECT  ");
  //          _sb.AppendLine("        @ErrorMessage = ERROR_MESSAGE(), ");
  //          _sb.AppendLine("        @ErrorSeverity = ERROR_SEVERITY(), ");
  //          _sb.AppendLine("        @ErrorState = ERROR_STATE(); ");
  //          _sb.AppendLine("    RAISERROR (@ErrorMessage, ");
  //          _sb.AppendLine("               @ErrorSeverity, ");
  //          _sb.AppendLine("               @ErrorState);");
  //          _sb.AppendLine(" END CATCH");

  //          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
  //          {
  //            _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = pOperationId;

  //            if (pAccountId == 0)
  //            {
  //              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = DBNull.Value;
  //            }
  //            else
  //            {
  //              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = pAccountId;
  //            }

  //            _sql_cmd.Parameters.Add("@pCardData", SqlDbType.NVarChar).Value = pCardTrackData;
  //            _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = pType;
  //            _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = pInitialBalance;
  //            _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).Value = pAddAmount;
  //            _sql_cmd.Parameters.Add("@pSubAmmount", SqlDbType.Money).Value = pSubAmount;
  //            _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = pFinalBalance;
  //            _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).Value = pDetails;
  //            _sql_cmd.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).Value = pCurrencyIsoCode;
  //            _sql_cmd.Parameters.Add("@pAuxAmount", SqlDbType.Money).Value = pAuxAmount;
  //            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = pCashierSessionId;
  //            _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = pTerminalId;
  //            _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = pTerminalName;
  //            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = pAuthorizedByUserId;
  //            _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50).Value = pAuthorizedByUserName;
  //            _sql_cmd.Parameters.Add("@pSubType", SqlDbType.Int).Value = 0;
  //            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = System.DateTime.Now;

  //            if (pCurrencyDenomination == 0)
  //            {
  //              _sql_cmd.Parameters.Add("@pCurrencyDenom", SqlDbType.Money).Value = DBNull.Value;
  //            }
  //            else
  //            {
  //              _sql_cmd.Parameters.Add("@pCurrencyDenom", SqlDbType.Money).Value = pCurrencyDenomination;
  //            }             
              
  //            //FBA 22-NOV-2013 - Cage
  //            _sql_cmd.Parameters.Add("@pCageFirstValue", SqlDbType.Int).Value = WSI.Common.CASHIER_MOVEMENT.CAGE_OPEN_SESSION;
  //            _sql_cmd.Parameters.Add("@pCageLastValue", SqlDbType.Int).Value = WSI.Common.CASHIER_MOVEMENT.CAGE_LAST_MOVEMENT;
  //            _sql_cmd.Parameters.Add("@pExternMovementId", SqlDbType.BigInt).Value = pMovementId;

  //            //07-GEN-2014 - Gaming Tables
  //            if (pGamingTableSessionId == 0)
  //            {
  //              _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = DBNull.Value;
  //            }
  //            else
  //            {
  //              _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = pGamingTableSessionId;
  //            }

  //            _sql_cmd.UpdatedRowSource = UpdateRowSource.None;

  //            _sql_cmd.ExecuteNonQuery();


  //          }
  //        }
  //        catch (Exception _ex)
  //        {
  //          try
  //          {
  //            _trx.Rollback("SP_ICM");
  //          }
  //          catch { }

  //          throw _ex;
  //        }
  //        finally
  //        {
  //          _trx.Commit();
  //        }
  //      } // using
  //    } // using
  //  } // SP_ICM
  //} // class SqlMovementsProcedures
}
