//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SqlProcedures.cs
// DESCRIPTION:   
// AUTHOR:        Ra�l Cervera
// CREATION DATE: 23-JUL-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 23-JUL-2012  RCI    Initial version
// 15-MAR-2013  RCI    Corrected the way the Rollback(SavePoint)/Commit is done. Have to do always a Commit at the end.
// 19-JAN-2016  JRC   Product Backlog Item 7909: Multiple buckets. Waking up the thread
// 20-AGO-2016  PDM    PBI 16148:WebService FBM - Adaptar l�gica a los m�todos de FBM
// 23-DIC-2016  FJC    BUG 20326:FBM: "ReportEvent" no esta mostrando ninguna informaci�n en el log
// 08-JUN-2017 XGJ    WIGOS-2752 FBM. Create two new events (Handpay - Cancel Credits, Handpay - Jackpot)
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class SqlProcedures
  {

    [SqlProcedure]
    public static void Trx_3GS_StartCardSession(String VendorId, String SerialNumber, Int32 MachineNumber,
                                                String ExternalTrackData,
                                                out Int64 PlaySessionId,
                                                out Decimal PlayableBalance,
                                                out Int32 StatusCode,
                                                out String StatusText,
                                                out String StatusError)
    {
      MultiPromos.StartSessionOutput _output;

      StatusCode = 4;
      StatusText = "Access Denied";
      StatusError = "";
      PlaySessionId = 0;
      PlayableBalance = 0;

      try
      {
        using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
        {
          _conn.Open();

          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_StartCardSession");

            try
            {
              if (!T3GS.DB_CheckFBMSession(_trx))
              {
                StatusError = "FBM session closed!";
              }
              else
              {
                _output = T3GS.Start(VendorId, SerialNumber, MachineNumber, ExternalTrackData, _trx);

                T3GS.Translate(_output, out StatusCode, out StatusText, out StatusError);

                if (_output.StatusCode == MultiPromos.StartSessionStatus.AccountInSession)
                {
                  PlaySessionId = _output.PlaySessionId;
                }

                if (_output.StatusCode == MultiPromos.StartSessionStatus.Ok)
                {
                  PlaySessionId = _output.PlaySessionId;

                  if (_output.PlayableBalance.GetOnlyReserved)
                  {
                    PlayableBalance = _output.PlayableBalance.Reserved;
                  }
                  else
                  {
                    PlayableBalance = _output.PlayableBalance.TotalBalance;
                  }

                  StatusCode = 0;
                  StatusText = "Bienvenido " + _output.HolderName;
                }
              }
            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_StartCardSession");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        StatusCode = 4;
        StatusText = "Access Denied";
        StatusError = "Trx_3GS_StartCardSession failed!, Exception: " + _ex.Message;
      }
    } // Trx_3GS_StartCardSession


    [SqlProcedure]
    public static void Trx_3GS_UpdateCardSession(String VendorId, String SerialNumberDummy, Int32 MachineNumberDummy,
                                                 String ExternalTrackDataDummy,
                                                 Int64 PlaySessionId,
                                                 Decimal PlayedAmount, Decimal WonAmount, Int32 PlayedCount, Int32 WonCount,
                                                 Decimal FinalBalance,
                                                 Decimal BillIn,
                                                 out Int32 StatusCode,
                                                 out String StatusText,
                                                 out String StatusError)
    {
      MultiPromos.EndSessionOutput _output;
      MultiPromos.PlayedWonMeters _meters;

      StatusCode = 4;
      StatusText = "Access Denied";
      StatusError = "";

      try
      {
        using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
        {
          _conn.Open();

          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_UpdateCardSession");

            try
            {
              _meters = new MultiPromos.PlayedWonMeters();
              _meters.PlayedCount = PlayedCount;
              _meters.PlayedAmount = PlayedAmount;
              _meters.WonCount = WonCount;
              _meters.WonAmount = WonAmount;
              _meters.JackpotAmount = 0;
              _meters.BillInAmount = BillIn;

              _output = T3GS.Update(VendorId, SerialNumberDummy, MachineNumberDummy, ExternalTrackDataDummy, PlaySessionId, _meters, FinalBalance, _trx);

              T3GS.Translate(_output, out StatusCode, out StatusText, out StatusError);
            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_UpdateCardSession");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        StatusCode = 4;
        StatusText = "Access Denied";
        StatusError = "Trx_3GS_UpdateCardSession failed!, Exception: " + _ex.Message;
      }
    } // Trx_3GS_UpdateCardSession

    [SqlProcedure]
    public static void Trx_3GS_EndCardSession(String VendorId, String SerialNumberDummy, Int32 MachineNumberDummy,
                                              String ExternalTrackDataDummy,
                                              Int64 PlaySessionId,
                                              Decimal PlayedAmount, Decimal WonAmount, Int32 PlayedCount, Int32 WonCount,
                                              Decimal FinalBalance,
                                              Decimal BillIn,
                                              out Int32 StatusCode,
                                              out String StatusText,
                                              out String StatusError)
    {
      MultiPromos.EndSessionOutput _output;
      MultiPromos.PlayedWonMeters _meters;

      StatusCode = 4;
      StatusText = "Access Denied";
      StatusError = "";

      try
      {
        using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
        {
          _conn.Open();

          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_EndCardSession");

            try
            {
              _meters = new MultiPromos.PlayedWonMeters();
              _meters.PlayedCount = PlayedCount;
              _meters.PlayedAmount = PlayedAmount;
              _meters.WonCount = WonCount;
              _meters.WonAmount = WonAmount;
              _meters.JackpotAmount = 0;
              _meters.BillInAmount = BillIn;

              _output = T3GS.End(VendorId, SerialNumberDummy, MachineNumberDummy, ExternalTrackDataDummy, PlaySessionId, _meters, FinalBalance, _trx);

              T3GS.Translate(_output, out StatusCode, out StatusText, out StatusError);
            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_EndCardSession");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
#if !SQL_BUSINESS_LOGIC
                      WCP_BucketsUpdate.SetDataAvailable(); /* wakes up the thread*/
#endif
            }
          }
        }
      }
      catch (Exception _ex)
      {
        StatusCode = 4;
        StatusText = "Access Denied";
        StatusError = "Trx_3GS_EndCardSession failed!, Exception: " + _ex.Message;
      }
    } // Trx_3GS_EndCardSession


    [SqlProcedure]
    public static void Trx_3GS_AccountStatus(String ExternalTrackData,
                                             String VendorId, Int32 MachineNumberDummy,
                                             out Decimal Balance,
                                             out Int32 StatusCode,
                                             out String StatusText,
                                             out String StatusError)
    {
      MultiPromos.StartSessionOutput _output;

      Balance = 0;
      StatusCode = 4;
      StatusText = "Access Denied";
      StatusError = "";

      try
      {
        using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
        {
          _conn.Open();

          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_AccountStatus");

            try
            {
              _output = T3GS.AccountStatus(ExternalTrackData, VendorId, MachineNumberDummy, _trx);

                Balance = _output.FinBalance.TotalBalance;

              T3GS.Translate(_output, out StatusCode, out StatusText, out StatusError);

            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_AccountStatus");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        StatusCode = 4;
        StatusText = "Access Denied";
        StatusError = "Trx_3GS_AccountStatus failed!, Exception: " + _ex.Message;
      }
    } // Trx_3GS_AccountStatus

    [SqlProcedure]
    public static void Trx_3GS_SendEvent(String ExternalTrackData,
                                         String VendorId, String SerialNumber, Int32 MachineNumber,
                                         Int64 EventId, Decimal Amount,
                                         out Int32 StatusCode,
                                         out String StatusText,
                                         out String StatusError)
    {
      MultiPromos.StartSessionOutput _output;
      Int64 _event_id;
      Boolean _is_hp;
      Int32 _is_listener_enabled;

      StatusCode = 4;
      StatusText = "Access Denied";
      StatusError = "";
      _is_hp = false;
      
      try
      {
        using (SqlConnection _conn = new SqlConnection(@"context connection=true"))
        {
          _conn.Open();

          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_SendEvent");

            Int32.TryParse(MultiPromos.ReadGeneralParams("WS2S", "Listener.Enabled", _trx), out _is_listener_enabled);      

            try
            {
              _output = T3GS.SendEvent(ExternalTrackData, VendorId, SerialNumber, MachineNumber, EventId, Amount, _trx);

              _event_id = EventId;

              if ((EventId == 10 || EventId == 11) && (_is_listener_enabled == 1))
              {
                _is_hp = true;
              }

              // FBM: Send 1 like a handpay jackpot
              if ((VendorId == "FBM" || EventId == 1) && (_is_listener_enabled == 1))
              {
                _is_hp = true;
                _event_id = 11;
              }

              if (_is_hp)
              {
                _output = T3GS.Insert_Handpay(VendorId, SerialNumber, MachineNumber, Amount, _event_id, _trx);
              }

              T3GS.Translate(_output, out StatusCode, out StatusText, out StatusError);

              if (_output.StatusCode == MultiPromos.StartSessionStatus.Ok)
              {
                StatusCode = (Int32)(EventId + 100);
              }
            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_SendEvent");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        StatusCode = 4;
        StatusText = "Access Denied";
        StatusError = "Trx_3GS_SendEvent failed!, Exception: " + _ex.Message;
      }
    } // Trx_3GS_SendEvent

  } // SqlProcedures

} // WSI.Common
