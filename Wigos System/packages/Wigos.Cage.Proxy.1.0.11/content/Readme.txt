﻿Project Name: Wigos.Cage.Proxy
Description:  This project is the entry point to Wigos.Cage module.
              This proxy includes all Feature methods that could be consumed for the application or UI.
              
Structure of the Project:
- Base: A common code that support the proxy implementations.
- Use cases (Example: Movements, Sessions, etc.)
    - Features: Here you must include all use cases/features with the interfaces and implementations 
                that you want to expose to the application or UI.
- Bootstrapper.cs:  This code has the installation of the complete module. Create or populate the application container (IoC), 
                    the Automapper profiles, etc... It also register all the type mapping for Unity container that need the Wigos.Cage module

Nuget package:  The Wigos.Cage.Proxy could be consumed by Nuget package manager. For installation open the Package Manager and 
                select the WinSystem package source and use: PM> Install-Package Wigos.Cage.Proxy
