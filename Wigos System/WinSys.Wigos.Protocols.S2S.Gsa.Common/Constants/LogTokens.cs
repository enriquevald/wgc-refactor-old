﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Constants
{
	public static class LogTokens
	{		
		public const string Gsa = "GSA";
		public const string S2S = "S2S";
		public const string Exception = "EXCEPTION";
		public const string Command = "Command";
		public const string VoucherId = "VoucherId";
	}
}