﻿using System;
using WinSys.Wigos.Architecture.Builders;
using WinSys.Wigos.Architecture.Enums;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Constants;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Services
{
	/// <summary>
	/// Implements a log service
	/// </summary>
	public class LogService : ILogService
	{
		public LogService(LogLevel logLevel)
		{
			this.LogLevel = logLevel;
		}

		#region ILogService

		public LogLevel LogLevel { get; private set; }

		public void Information(string message)
		{
			if (this.LogLevel <= LogLevel.Information)
			{
				Log.Message(this.LogFormatter(message));
			}
		}

		public void Warning(string message)
		{
			if (this.LogLevel <= LogLevel.Warning)
			{
				Log.Warning(this.LogFormatter(message));
			}
		}

		public void Error(string message)
		{
			if (this.LogLevel <= LogLevel.Error)
			{
				Log.Error(this.LogFormatter(message));
			}
		}

		public void Exception(Exception exception)
		{
			if (this.LogLevel <= LogLevel.Critical)
			{
				this.Error(this.LogFormatter(this.ExceptionFormatter(exception)));
				Log.Exception(exception);
			}
		}

		public void Exception(string message, Exception exception)
		{
			if (this.LogLevel <= LogLevel.Critical)
			{
				this.Error(this.LogFormatter(this.ExceptionFormatter(message, exception)));
				Log.Exception(exception);
			}
		}

		#endregion

		private string LogFormatter(string message)
		{
			ITokenStringBuilder tokenStringBuilder = new TokenStringBuilder();
			tokenStringBuilder.AddSingleToken(LogTokens.S2S);
			tokenStringBuilder.AddSingleToken(LogTokens.Gsa);

			return tokenStringBuilder.Build() + message;
		}

		private string ExceptionFormatter(Exception exception)
		{
			string exceptionMessage = exception != null
			                          && !string.IsNullOrEmpty(exception.Message) ? exception.Message : null;

			ITokenStringBuilder tokenStringBuilder = new TokenStringBuilder();
			tokenStringBuilder.AddSingleToken(LogTokens.Exception);

			return tokenStringBuilder.Build() + exceptionMessage;
		}

		private string ExceptionFormatter(string message, Exception exception)
		{
			string formattedMessage = message ?? string.Empty;

			return formattedMessage + this.ExceptionFormatter(exception);;
		}
	}
}