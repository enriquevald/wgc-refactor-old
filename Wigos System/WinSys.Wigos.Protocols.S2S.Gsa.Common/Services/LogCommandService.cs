﻿using System;
using System.Collections.Generic;
using System.Linq;
using WinSys.Wigos.Architecture.Builders;
using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Constants;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Interfaces;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Services
{
	/// <summary>
	/// Implements log for commands service
	/// </summary>
	public class LogCommandService : ILogCommandService
	{
		private delegate void LogMethod(string message);

		private readonly ILogService logService;

		public LogCommandService(ILogService logService)
		{
			this.logService = logService;
		}

		public void Information(object command, string message)
		{
			this.LogFormatted(this.logService.Information, command, message);
		}

		public void Warning(object command, string message)
		{
			this.LogFormatted(this.logService.Warning, command, message);
		}

		public void Error(object command, string message)
		{
			this.LogFormatted(this.logService.Error, command, message);
		}

		public void Exception(object command, Exception exception)
		{
			string exceptionMessage = this.ExceptionFormatter(exception);
			this.LogFormatted(this.logService.Error, command, exceptionMessage);
			this.logService.Exception(exception);
		}

		public void Exception(object command, string message, Exception exception)
		{
			string exceptionMessage = this.ExceptionFormatter(message, exception);
			this.LogFormatted(this.logService.Error, command, exceptionMessage);
			this.logService.Exception(message, exception);
		}

		private void LogFormatted(LogMethod logMethod, object command, string message)
		{
			foreach (string formattedMessage in this.GetFormattedMessageByVoucherId(command, message))
			{
				logMethod(formattedMessage);
			}
		}

		private IEnumerable<string> GetFormattedMessageByVoucherId(object command, string message)
		{
			IEnumerable<string> result;

			string commandName = command is IName ? ((IName)command).Name : command.GetType().Name;
			ITokenStringBuilder tokenStringBuilder = new TokenStringBuilder();
			IVoucherIds voucherIdsCommand = command as IVoucherIds;

			if (voucherIdsCommand != null 
			    && voucherIdsCommand.VoucherIds.Any())
			{
				List<string> logMessages = new List<string>();
				
				foreach (string voucherId in voucherIdsCommand.VoucherIds)
				{
					tokenStringBuilder.AddCommaSeparatedKeyValuePairToken(new KeyValuePair<string, string>(LogTokens.Command, commandName));
					tokenStringBuilder.AddCommaSeparatedKeyValuePairToken(new KeyValuePair<string, string>(LogTokens.VoucherId, voucherId));
					logMessages.Add(tokenStringBuilder.Build() + message);
					tokenStringBuilder.Reset();
				}

				result = logMessages;
			}
			else
			{
				tokenStringBuilder.AddCommaSeparatedKeyValuePairToken(new KeyValuePair<string, string>(LogTokens.Command, commandName));
				result = new List<string> { tokenStringBuilder.Build() + message};
			}

			return result;
		}

		private string ExceptionFormatter(Exception exception)
		{
			string exceptionMessage = exception != null
			                          && !string.IsNullOrEmpty(exception.Message) ? exception.Message : null;

			ITokenStringBuilder tokenStringBuilder = new TokenStringBuilder();
			tokenStringBuilder.AddSingleToken(LogTokens.Exception);

			return tokenStringBuilder.Build() + exceptionMessage;
		}

		private string ExceptionFormatter(string message, Exception exception)
		{
			string formattedMessage = message ?? string.Empty;

			return formattedMessage + this.ExceptionFormatter(exception);;
		}
	}
}