﻿using System.Collections.Generic;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Interfaces
{
	/// <summary>
	/// Implements a readable List of IVoucherId property
	/// </summary>
	public interface IVoucherIds
	{
		IEnumerable<string> VoucherIds { get; }
	}
}