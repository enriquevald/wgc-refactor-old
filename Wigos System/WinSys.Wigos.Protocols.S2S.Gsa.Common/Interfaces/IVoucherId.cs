﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Interfaces
{
	/// <summary>
	/// Implements a readable VoucherId property
	/// </summary>
	public interface IVoucherId
	{
		string VoucherId { get; }
	}
}