﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Common.Interfaces
{
	/// <summary>
	/// Implements a descriptive name
	/// </summary>
	public interface IName
	{
		string Name { get; }
	}
}