//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Interface_Pattern.cs
// 
//   DESCRIPTION: Interface_Pattern class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 21-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-NOV-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using WSI.WC2;
using WSI.Common;

namespace WSI.WC2.WC2_Protocol
{

  [StructLayout(LayoutKind.Sequential)]
  public struct WC2_PatternInfo
  {
    public int order;
    public Int64 min;
    public Int64 max;
    public int num_positions;
    public Int64 position_list;
  }

  public interface WC2_IMsgGetPatternsReply
  {
    void GetNumPatterns(out UInt16 NumPatterns);
    void GetPattern(UInt16 PatternIndex, out WC2_PatternInfo Pattern);
  }

  public class WC2_GetInterface_MsgGetPatternsReply : WC2_IMsgGetPatternsReply
  {
    private WC2_MsgGetPatternsReply reply;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Response"></param>
    public WC2_GetInterface_MsgGetPatternsReply(WC2_Message Response)
    {
      reply = (WC2_MsgGetPatternsReply)Response.MsgContent;
    }

    #region WC2_IMsgGetPattern Members

    /// <summary>
    ///  Returns the number of patterns in the pattern list
    /// </summary>
    /// <param name="NumPatterns"></param>
    void WC2_IMsgGetPatternsReply.GetNumPatterns(out ushort NumPatterns)
    {
      NumPatterns = (ushort)reply.NumPatterns;
    }

    /// <summary>
    /// Returns a single pattern, having a Pattern Index
    /// </summary>
    /// <param name="PatternIndex"></param>
    /// <param name="Pattern"></param>
    void WC2_IMsgGetPatternsReply.GetPattern(ushort PatternIndex, out WC2_PatternInfo Pattern)
    {
      WC2_Pattern pattern;
      WC2_PatternInfo pattern_info;

      pattern = (WC2_Pattern)reply.Patterns[PatternIndex];
      pattern_info = new WC2_PatternInfo();
      pattern_info.order = pattern.Order;
      pattern_info.min = pattern.Min;
      pattern_info.max = pattern.Max;
      pattern_info.num_positions = pattern.NumPositions;
      pattern_info.position_list = Convert.ToInt64(pattern.PositionListProtocol,16);

      Pattern = pattern_info;
    }

    #endregion
  }
}
