using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using WSI.WC2;
using WSI.Common;

namespace WSI.WC2.WC2_Protocol
{
  [StructLayout(LayoutKind.Sequential)]
  public struct WC2_SiteJackpotInfo
  {
    public int index;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String name;
    public Double amount;
    public Double minimum_bet_amount;
  }

  public struct WC2_SiteJackpotInfoHistory
  {
    public int index;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String name;
    public Double amount;
    public long date;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String player_name;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String terminal_name;
  }

  public struct WC2_SiteJackpotInfoMessage
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 255)]
    public String message;
  }

  public struct WC2_SiteJackpotParameters
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String mode;
    public int block_interval;
    public int recent_history_interval;
    public int animation_interval_01;
    public int animation_interval_02;
    public Double min_amount;
  }

  public interface WC2_IMsgSiteJackpotInfoReply
  {
    void GetNumJackpots (out UInt16 NumJackpots);
    void SiteJackpot(UInt16 JackpotIndex, out WC2_SiteJackpotInfo JackpotInfo);
    void GetNumJackpotsHistory(out UInt16 NumJackpotsHistory);
    void SiteJackpotHistory(UInt16 JackpotHistoryIndex, out WC2_SiteJackpotInfoHistory JackpotHistory);
    void GetNumJackpotMessages(out UInt16 NumJackpotMessages);
    void SiteJackpotMessage(UInt16 MessageIndex, out WC2_SiteJackpotInfoMessage Message);
    void SiteJackpotParameters (out WC2_SiteJackpotParameters JackpotPatameters);
  }
  
  public class WC2_GetInterface_MsgSiteJackpotInfoReply : WC2_IMsgSiteJackpotInfoReply
  {
    private WC2_MsgSiteJackpotInfoReply reply;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Response"></param>
    public WC2_GetInterface_MsgSiteJackpotInfoReply(WC2_Message Response)
    {
      reply = (WC2_MsgSiteJackpotInfoReply) Response.MsgContent;
    }
    
#region WC2_IMsgSiteJackpotInfo Members

    void WC2_IMsgSiteJackpotInfoReply.GetNumJackpots(out ushort NumJackpots)
    {
      NumJackpots = (ushort) reply.NumJackpots;
    }

    void WC2_IMsgSiteJackpotInfoReply.SiteJackpot(ushort JackpotIndex, out WC2_SiteJackpotInfo JackpotInfo)
    {
      WC2_Jackpot jackpot;
      WC2_SiteJackpotInfo jackpot_info;
      
      jackpot = (WC2_Jackpot) reply.Jackpots[JackpotIndex];
      jackpot_info = new WC2_SiteJackpotInfo();
      jackpot_info.index  = jackpot.Index;
      jackpot_info.name   = jackpot.Name;
      jackpot_info.amount = (double)jackpot.Amount;
      jackpot_info.minimum_bet_amount = (double)jackpot.MinimumBetAmount;
      
      JackpotInfo = jackpot_info;
    }

    void WC2_IMsgSiteJackpotInfoReply.GetNumJackpotsHistory(out ushort NumJackpotsHistory)
    {
      NumJackpotsHistory = (ushort)reply.NumJackpotsHistory;
    }

    void WC2_IMsgSiteJackpotInfoReply.SiteJackpotHistory(ushort JackpotHistoryIndex, out WC2_SiteJackpotInfoHistory JackpotInfoHistory)
    {
      WC2_Jackpot_History jackpot_history;
      WC2_SiteJackpotInfoHistory jackpot_info_history;

      jackpot_history = (WC2_Jackpot_History)reply.JackpotsHistory[JackpotHistoryIndex];
      jackpot_info_history = new WC2_SiteJackpotInfoHistory();
      jackpot_info_history.index = jackpot_history.Index;
      jackpot_info_history.name = jackpot_history.Name;
      jackpot_info_history.amount = (double)jackpot_history.Amount;
      jackpot_info_history.date = jackpot_history.Date.ToFileTime();
      jackpot_info_history.player_name = jackpot_history.PlayerName;
      jackpot_info_history.terminal_name = jackpot_history.TerminalName;

      JackpotInfoHistory = jackpot_info_history;
    }

    void WC2_IMsgSiteJackpotInfoReply.GetNumJackpotMessages(out ushort NumMessages)
    {
      NumMessages = (ushort)reply.NumMessages;
    }

    void WC2_IMsgSiteJackpotInfoReply.SiteJackpotMessage(ushort MessageIndex, out WC2_SiteJackpotInfoMessage Message)
    {
      WC2_SiteJackpotInfoMessage _msg;
      
      _msg = new WC2_SiteJackpotInfoMessage();
      _msg.message = (string)reply.Messages[MessageIndex]; 
      Message = _msg;
    }

    void WC2_IMsgSiteJackpotInfoReply.SiteJackpotParameters (out WC2_SiteJackpotParameters JackpotParameters)
    {
      WC2_SiteJackpotParameters jackpot_parameters;

      jackpot_parameters = new WC2_SiteJackpotParameters ();
      jackpot_parameters.mode = reply.Mode;
      jackpot_parameters.block_interval = reply.BlockInterval;
      jackpot_parameters.recent_history_interval = reply.RecentHistoryInterval;
      jackpot_parameters.animation_interval_01 = reply.AnimationInterval01;
      jackpot_parameters.animation_interval_02 = reply.AnimationInterval02;
      jackpot_parameters.min_amount = (double) reply.MinBlockAmount;

      JackpotParameters = jackpot_parameters;
    }

#endregion
  }
}
