using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using WSI.WC2;
using WSI.Common;

namespace WSI.WC2.WC2_Protocol
{

  [StructLayout(LayoutKind.Sequential)]
  public struct WC2_JackpotInfo
  {
    public int jackpot_index;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String name;
    public Double amount;
    public Double minimum_bet_amount;
  }

  public struct WC2_JackpotInfoHistory
  {

    public int index;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String name;
    public Double amount;
    public long date;
  }

  public struct WC2_JackpotInfoMessage
  {

    [MarshalAs(UnmanagedType.BStr, SizeConst = 255)]
    public String message;
  }

  public struct WC2_JackpotParameters
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 40)]
    public String mode;
    public int block_interval;
    public int recent_history_interval;
    public int animation_interval;
    public Double min_amount;
  }

  public interface WC2_IMsgGetJackpotInfoReply
  {
    void GetNumJackpots (out UInt16 NumJackpots);
    void GetJackpot(UInt16 JackpotIndex, out WC2_JackpotInfo JackpotInfo);
    void GetNumJackpotsHistory(out UInt16 NumJackpotsHistory);
    void GetJackpotHistory(UInt16 JackpotHistoryIndex, out WC2_JackpotInfoHistory JackpotHistory);
    void GetNumJackpotMessages(out UInt16 NumJackpotMessages);
    void GetJackpotMessage(UInt16 MessageIndex, out WC2_JackpotInfoMessage Message);
    void GetJackpotParameters(out WC2_JackpotParameters JackpotPatameters);
  }
  
  public class WC2_GetInterface_MsgGetJackpotInfoReply : WC2_IMsgGetJackpotInfoReply
  {
    private WC2_MsgGetJackpotInfoReply reply;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Response"></param>
    public WC2_GetInterface_MsgGetJackpotInfoReply(WC2_Message Response)
    {
      reply = (WC2_MsgGetJackpotInfoReply)Response.MsgContent;
    }
    

    #region WC2_IMsgGetJackpotInfo Members

    void WC2_IMsgGetJackpotInfoReply.GetNumJackpots(out ushort NumJackpots)
    {
      NumJackpots = (ushort) reply.NumJackpots;
    }

    void WC2_IMsgGetJackpotInfoReply.GetJackpot(ushort JackpotIndex, out WC2_JackpotInfo JackpotInfo)
    {
      WC2_Jackpot jackpot;
      WC2_JackpotInfo jackpot_info;
      
      jackpot = (WC2_Jackpot) reply.Jackpots[JackpotIndex];
      jackpot_info = new WC2_JackpotInfo();
      jackpot_info.jackpot_index  = jackpot.Index;
      jackpot_info.name   = jackpot.Name;
      jackpot_info.amount = (double)jackpot.Amount;
      jackpot_info.minimum_bet_amount = (double)jackpot.MinimumBetAmount;
      
      JackpotInfo = jackpot_info;
    }



    void WC2_IMsgGetJackpotInfoReply.GetNumJackpotsHistory(out ushort NumJackpotsHistory)
    {
      NumJackpotsHistory = (ushort)reply.NumJackpotsHistory;
    }

    void WC2_IMsgGetJackpotInfoReply.GetJackpotHistory(ushort JackpotHistoryIndex, out WC2_JackpotInfoHistory JackpotInfoHistory)
    {
      WC2_Jackpot_History jackpot_history;
      WC2_JackpotInfoHistory jackpot_info_history;

      jackpot_history = (WC2_Jackpot_History)reply.JackpotsHistory[JackpotHistoryIndex];
      jackpot_info_history = new WC2_JackpotInfoHistory();
      jackpot_info_history.index = jackpot_history.Index;
      jackpot_info_history.name = jackpot_history.Name;
      jackpot_info_history.amount = (double)jackpot_history.Amount;
      jackpot_info_history.date = jackpot_history.Date.ToFileTime();

      JackpotInfoHistory = jackpot_info_history;
    }


    void WC2_IMsgGetJackpotInfoReply.GetNumJackpotMessages(out ushort NumMessages)
    {
      NumMessages = (ushort)reply.NumMessages;
    }

    void WC2_IMsgGetJackpotInfoReply.GetJackpotMessage(ushort MessageIndex, out WC2_JackpotInfoMessage Message)
    {
      WC2_JackpotInfoMessage _msg;
      
      _msg = new WC2_JackpotInfoMessage();
      _msg.message = (string)reply.Messages[MessageIndex]; 
      Message = _msg;
    }


    void WC2_IMsgGetJackpotInfoReply.GetJackpotParameters(out WC2_JackpotParameters JackpotParameters)
    {
      WC2_JackpotParameters jackpot_parameters;

      jackpot_parameters = new WC2_JackpotParameters();
      jackpot_parameters.mode = reply.Mode;
      jackpot_parameters.block_interval = reply.BlockInterval;
      jackpot_parameters.recent_history_interval = reply.RecentHistoryInterval;
      jackpot_parameters.animation_interval = reply.AnimationInterval;
      jackpot_parameters.min_amount = (double)reply.MinBlockAmount;

      

      JackpotParameters = jackpot_parameters;
    }



    #endregion
  }
}
