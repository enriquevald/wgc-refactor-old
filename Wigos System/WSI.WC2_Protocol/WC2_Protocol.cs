//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_Protocol.cs
// 
//   DESCRIPTION: WC2 Protocol Dll - Shared between Wigos Bingo Server and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 12-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 02-MAY-2016 ETP    Fixed bug 14082: Fixed connection errors with windows update.
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WC2;
using WSI.Common;

namespace WSI.WC2.WC2_Protocol
{
  public delegate void WC2_LoggerWriteCallback([In, MarshalAs(UnmanagedType.LPStr, SizeConst = 255)] String Function, [In, MarshalAs(UnmanagedType.LPStr, SizeConst = 255)] String Value);

  [StructLayout(LayoutKind.Sequential)]
  public struct WC2_Parameters
  {
    public int client_id;
    public int build_id;
    public int num_locations;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String location1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String location2;

    public int keep_alive_interval;
    public int connection_timeout;
    public int query_jackpot_interval;
  }



  public interface WC2_IDraw
  {
    void GetDrawId([Out, MarshalAs(UnmanagedType.I8)] out UInt64 DrawId);
    void GetNumNumbers(out UInt16 NumNumber);
    void GetNumber(UInt16 Index, out UInt16 Number);
  }
  public interface WC2_ICardPrize
  {
    void GetWonCredits(out UInt32 WonCredits);
    void GetNumPositions(out UInt16 NumPositions);
    void GetPosition(UInt16 Index, out UInt16 Position);
    void GetResultIndex(out UInt16 ResultIndex);
  }




  public interface WC2_IMessage
  {
    void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId);
    void GetMessageType([Out, MarshalAs(UnmanagedType.U4)] out Int32 MessageType);
    void GetResponseCode([Out, MarshalAs(UnmanagedType.U2)] out UInt16 ResponseCode);
  }

  public interface WC2_ICard
  {
    void GetId([Out, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] out String CardId);
    void GetNumNumbers(out UInt16 NumNumbers);
    void GetNumber(UInt16 Index, out UInt16 Number);
  }

  public interface WC2_ICardList
  {
    void GetNumCards([Out, MarshalAs(UnmanagedType.U2)] out UInt16 NumCards);
    void GetCard(UInt16 CardIndex, out WC2_ICard Card);
  }

  public interface WC2_IMsgPlayReply
  {
    void GetJackpot(out WC2_JackpotInfo JackpotInfo);
    void GetCardList(out WC2_ICardList CardList);
    void GetDraw(out WC2_IDraw Draw);
    void GetPrize(out WC2_ICardPrize CardPrize);
  }

  public interface WC2_IMsgGetCardsReply
  {
    void GetCardList(out WC2_ICardList CardList);
  }

  public class WC2_Response_MsgGetCardsReply : WC2_IMsgGetCardsReply
  {
    WC2_Message wc2_message;

    public WC2_Response_MsgGetCardsReply(WC2_Message Message)
    {
      wc2_message = Message;
    }

    public void GetCardList(out WC2_ICardList CardList)
    {
      CardList = (WC2_ICardList)new CardList(wc2_message);
    }
  }


  public class WC2_Response_MsgPlayReply : WC2_IMsgPlayReply
  {
    private class Draw : WC2_IDraw
    {
      private UInt64 m_draw_id;
      private int[] m_numbers;

      #region WC2_IDraw Members

      void WC2_IDraw.GetDrawId(out ulong DrawId)
      {
        DrawId = m_draw_id;
      }

      void WC2_IDraw.GetNumNumbers(out ushort NumNumber)
      {
        NumNumber = (ushort)m_numbers.Length;
      }

      void WC2_IDraw.GetNumber(ushort Index, out ushort Number)
      {
        Number = (ushort)m_numbers[Index];
      }

      public Draw(long DrawId, int[] Numbers)
      {
        m_draw_id = (ulong)DrawId;
        m_numbers = Numbers;
      }
      #endregion
    }
    private class Prize : WC2_ICardPrize
    {
      private UInt16 m_result_index;
      private UInt32 m_won_credits;
      private int[] m_positions;


      #region WC2_ICardPrize Members

      void WC2_ICardPrize.GetWonCredits(out uint WonCredits)
      {
        WonCredits = m_won_credits;
      }

      void WC2_ICardPrize.GetNumPositions(out ushort NumPositions)
      {
        NumPositions = (ushort)m_positions.Length;
      }

      void WC2_ICardPrize.GetPosition(ushort Index, out ushort Position)
      {
        Position = (ushort)m_positions[Index];
      }

      void WC2_ICardPrize.GetResultIndex(out UInt16 ResultIndex)
      {
        ResultIndex = m_result_index;
      }


      public Prize(UInt16 ResultIndex, UInt32 WonCredits, int[] Positions)
      {
        m_result_index = ResultIndex;
        m_won_credits = WonCredits;
        m_positions = Positions;
      }
      #endregion
    }

    WC2_Message wc2_message;

    public WC2_Response_MsgPlayReply(WC2_Message Message)
    {
      wc2_message = Message;
    }

    public void GetCardList(out WC2_ICardList CardList)
    {
      CardList = (WC2_ICardList)new CardList(wc2_message);
    }

    #region WC2_IMsgPlayReply Members

    void WC2_IMsgPlayReply.GetJackpot(out WC2_JackpotInfo JackpotInfo)
    {
      WC2_Jackpot jackpot;
      WC2_JackpotInfo jackpot_info;
      WC2_MsgPlayReply reply;

      reply = (WC2_MsgPlayReply)wc2_message.MsgContent;
      jackpot_info = new WC2_JackpotInfo();


      if (reply.HasJackpotPrize)
      {
        jackpot = reply.JackpotData;
        jackpot_info.jackpot_index = jackpot.Index;
        jackpot_info.name = jackpot.Name;
        jackpot_info.amount = (double)jackpot.Amount;
        jackpot_info.minimum_bet_amount = (double)jackpot.MinimumBetAmount;
      }
      else
      {
        jackpot_info.jackpot_index = 0;
        jackpot_info.name = "";
        jackpot_info.amount = 0;
        jackpot_info.minimum_bet_amount = 0;
      }

      JackpotInfo = jackpot_info;
    }

    void WC2_IMsgPlayReply.GetCardList(out WC2_ICardList CardList)
    {
      CardList = (WC2_ICardList)new CardList(wc2_message);
    }

    #endregion

    #region WC2_IMsgPlayReply Members


    void WC2_IMsgPlayReply.GetDraw(out WC2_IDraw Draw)
    {
      WC2_MsgPlayReply reply;
      WC2_Response_MsgPlayReply.Draw draw;

      reply = (WC2_MsgPlayReply)wc2_message.MsgContent;

      draw = new Draw(reply.DrawId, reply.Draw.GetBalls());

      Draw = draw;
    }

    void WC2_IMsgPlayReply.GetPrize(out WC2_ICardPrize CardPrize)
    {
      WC2_MsgPlayReply reply;
      WC2_Response_MsgPlayReply.Prize prize;
      WC2_Prize wc2_prize;
      WC2_CardPrize wc2_card_prize;


      reply = (WC2_MsgPlayReply)wc2_message.MsgContent;

      wc2_prize = (WC2_Prize)reply.Prizes[0];
      wc2_card_prize = (WC2_CardPrize)wc2_prize.CardPrizes[0];

      prize = new Prize((UInt16)wc2_card_prize.ResultIndex, (UInt32)wc2_card_prize.PrizeCredits, wc2_card_prize.GetPositions());

      CardPrize = prize;
    }

    #endregion
  }


  public class WC2_CardEnvelope : WC2_ICard
  {
    WC2_Card m_card;
    int[] numbers;

    public WC2_CardEnvelope(WC2_Card Card)
    {
      m_card = Card;
      numbers = m_card.GetNumbers();
    }

    #region WC2_ICard Members

    void WC2_ICard.GetId(out string CardId)
    {
      CardId = m_card.CardId;
    }

    void WC2_ICard.GetNumNumbers(out ushort NumNumbers)
    {
      NumNumbers = (ushort)numbers.Length;
    }

    void WC2_ICard.GetNumber(ushort Index, out ushort Number)
    {
      Number = (ushort)numbers[Index];
    }

    #endregion
  }
  public class CardList : WC2_ICardList
  {
    private ArrayList m_cardlist = new ArrayList();

    public CardList(WC2_Message Message)
    {
      switch (Message.MsgHeader.MsgType)
      {
        case WC2_MsgTypes.WC2_MsgGetCardsReply:
          {
            WC2_MsgGetCardsReply reply;
            reply = (WC2_MsgGetCardsReply)Message.MsgContent;
            foreach (WC2_Card wc2_card in reply.Cards)
            {
              m_cardlist.Add(new WC2_CardEnvelope(wc2_card));
            }
          }
          break;

        case WC2_MsgTypes.WC2_MsgPlayReply:
          {
            WC2_MsgPlayReply reply;
            reply = (WC2_MsgPlayReply)Message.MsgContent;
            foreach (WC2_Card wc2_card in reply.Cards)
            {
              m_cardlist.Add(new WC2_CardEnvelope(wc2_card));
            }
          }
          break;

        default:
          break;
      }
    }

    #region WC2_ICardList Members

    void WC2_ICardList.GetNumCards(out ushort NumCards)
    {
      NumCards = (ushort)m_cardlist.Count;
    }

    void WC2_ICardList.GetCard(ushort CardIndex, out WC2_ICard Card)
    {
      Card = (WC2_CardEnvelope)m_cardlist[CardIndex];
    }

    #endregion
  }


  /// <summary>
  /// Interface for WC2 Protocol. 
  /// </summary>
  public interface WC2_IProtocol
  {

    // Public Requests from Kiosk protocol

    // Basic operations
     void Start ([In, MarshalAs (UnmanagedType.BStr, SizeConst = 50)] String TerminalName, IntPtr LoggerCallback);
    void Stop ();

    void GetIsConnected ([Out, MarshalAs (UnmanagedType.Bool)] out Boolean IsConnected);

    void GetSequenceId ([Out, MarshalAs (UnmanagedType.I8)] out Int64 SequenceId);
    void GetTransactionId ([Out, MarshalAs (UnmanagedType.I8)] out Int64 TransactionId);

    void WaitForMessage (out WC2_IMessage ReceivedMessage);
    void Send (WC2_IMessage MessageToSend);

    void GetInterface_MsgGetCardsReply (WC2_IMessage ReceivedMessage, out WC2_IMsgGetCardsReply Interface);
    void GetInterface_MsgGetJackpotInfoReply(WC2_IMessage ReceivedMessage, out WC2_IMsgGetJackpotInfoReply Interface);
    void GetInterface_MsgSiteJackpotInfoReply (WC2_IMessage ReceivedMessage, out WC2_IMsgSiteJackpotInfoReply Interface);
    void GetInterface_Parameters (WC2_IMessage ReceivedMessage, out WC2_Parameters Parameters);
    void GetInterface_MsgPlayReply (WC2_IMessage ReceivedMessage, out WC2_IMsgPlayReply Interface);
    void GetInterface_MsgGetPatternsReply (WC2_IMessage ReceivedMessage, out WC2_IMsgGetPatternsReply Interface);

    void NewRequest_GetCards (out WC2_IMessage Request, long SequenceId, int NumCards);
    void NewRequest_GetJackpotInfo (out WC2_IMessage Request, long SequenceId, ushort GameId, int Denomination, long BetCredits);
    void NewRequest_SiteJackpotInfo (out WC2_IMessage Request, long SequenceId, ushort GameId, int Denomination, long BetCredits);
    void NewRequest_GetParameters (out WC2_IMessage Request, long SequenceId, int ClientId, int BuildId);
    void NewRequest_CancelPlay (out WC2_IMessage Request, long SequenceId, long TransactionId);
    void NewRequest_GetPatterns (out WC2_IMessage Request, long SequenceId);

    void NewRequest_Play(out WC2_IMessage Request,
                         long SequenceId, long TransactionId, ushort GameId,
                         [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                         long Denomination, long BetCredits, long WonCredits);

  } // WC2_IProtocol

  /// <summary>
  /// Protocol Class
  /// </summary>
  public class WC2_Protocol_Class : WC2_IProtocol
  {


    #region Constants

    private const Int32 WC2_TIMEOUT = 30000;  // AJQ 19-ABR-2010, TCP is connected, so we wait a bit more ...

    #endregion

    #region Enums

    public enum WC2_ResponseMsgType
    {
      None = 0,
      GetCards = 1,
      Play = 2,
      CancelPlay = 3,
    }

    private enum WC2_PROTOCOL_STATUS
    {
      UNKNOWN,
      DISCONNECTED,
      ENROLLING,
      GETTING_PARAMETERS,
      CONNECTED,
    }
    #endregion

    #region Members

    private WaitableQueue m_queue_send;         // Queue to send messages.
    private WaitableQueue m_queue_receive;      // Queue for all responses.
    private WaitableQueue m_queue_responses;  // Queue for responses to Terminal. (Kiosk)

    private TCPClientSSL m_ssl_client;


    // Status
    private WC2_PROTOCOL_STATUS m_status = WC2_PROTOCOL_STATUS.UNKNOWN;
    private long m_status_tick = 0;
    private AutoResetEvent m_status_changed = new AutoResetEvent(true);

    private String m_terminal_name = "";
    private Int64 m_terminal_session_id = 0;
    private Int64 m_sequence_id = 0;
    private Int64 m_transaction_id = 0;

    private int m_keep_alive_interval     = 30000;
    private int m_connection_timeout      = 60000;
    private int m_tick_last_msg_sent      = Misc.GetTickCount();
    private int m_tick_last_msg_received  = Misc.GetTickCount();

    private Thread m_wc2_main_thread;

    private StringBuilder m_sb = new StringBuilder(128 * 1024);

    private WC2_Card m_wc2_card;

    private WC2_LoggerWriteCallback m_logger_callback;

    #endregion // Members

    #region Private Methods


    private void LoggerWrite(String Function, String Value)
    {
      if (m_logger_callback != null)
      {
        m_logger_callback(Function.Substring(0, Math.Min(Function.Length, 240)), Value.Substring(0, Math.Min(Value.Length, 240)));
      }
    }

    private void KeepAliveWhileConnected()
    {
      long _elapsed_ticks;
      WC2_Message _wc2_request;
      String _xml_request;
      Byte[] _raw_request;

      while (Status == WC2_PROTOCOL_STATUS.CONNECTED)
      {
        try
        {
          if (WaitStatusChanged(1000) != WC2_PROTOCOL_STATUS.CONNECTED)
          {
            return;
          }
          if (!m_ssl_client.IsConnected)
          {
            SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }
          // Check Last Received Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_received);
          if (_elapsed_ticks > m_connection_timeout)
          {
            SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          // Check Last Sent Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_sent);
          if (_elapsed_ticks <= m_keep_alive_interval)
          {
            // A few time ago a message was sent: continue waiting
            continue;
          }

          _wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgKeepAlive);
          _wc2_request.MsgHeader.TerminalId = m_terminal_name;
          _wc2_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
          this.GetSequenceId(out _wc2_request.MsgHeader.SequenceId);

          _xml_request = _wc2_request.ToXml();
          _raw_request = Encoding.UTF8.GetBytes(_xml_request);

          if (!m_ssl_client.Send(_raw_request))
          {
            SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          m_tick_last_msg_sent = Misc.GetTickCount();
        }
        catch (Exception _ex)
        {
          Log.Exception (_ex);

          SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);

          return;
        }
      }
    } // KeepAliveWhileConnected


    /// <summary>
    /// WC2_Protocol Constructor
    /// </summary>
    public WC2_Protocol_Class()
    {
      ////// TEMP: Logger
      ////Log.AddListener(new SimpleLogger("WC2_Protocol_Logger"));

      // Queues
      m_queue_send = new WaitableQueue();         // Send to WC2 Server
      m_queue_receive = new WaitableQueue();      // Received from WC2 Server
      m_queue_responses = new WaitableQueue();  // Queue where kiosk remain blocked waiting for a response

      m_ssl_client = new TCPClientSSL(m_queue_receive);

      m_wc2_main_thread = new Thread(new ThreadStart(MainThread));
      m_wc2_main_thread.Name = "WC2 Main Thread";

    } // WC2_Protocol


    /// <summary>
    /// Manage kiosk requests and periodically messages requests.
    /// </summary>
    private void MainThread()
    {
      WC2_PROTOCOL_STATUS _wc2_status;

      Log.Message("Main Thread Started");

      while (true)
      {
        try
        {
          _wc2_status = WaitStatusChanged(1000);

          switch (_wc2_status)
          {
            default:
            case WC2_PROTOCOL_STATUS.UNKNOWN:
              {
                SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);
              }
              break;

            case WC2_PROTOCOL_STATUS.CONNECTED:
              {
                KeepAliveWhileConnected();
              }
              break;

            case WC2_PROTOCOL_STATUS.DISCONNECTED:
              {
                WC2_QueryService _wc2_query_service;
                IPEndPoint _wc2_service_ipe;
                Boolean _disconnect;
                Boolean _enrolled;
                Boolean _msg_ok;
                int _wait_hint;

                _disconnect = true;
                _enrolled = false;
                _msg_ok = false;
                _wait_hint = 0;

                while (!_enrolled)
                {
                  Thread.Sleep(_wait_hint);
                  _wait_hint = 2000;

                  _wc2_service_ipe = null;
                  _wc2_query_service = null;

                  try
                  {
                    Log.Message("Query Service ...");

                    // Ask for WC2_Service's IP endpoint to the WC2_DirectoryService 
                    _wc2_query_service = new WC2_QueryService();
                    _wc2_service_ipe = _wc2_query_service.QueryService(5000);
                    if (_wc2_service_ipe == null)
                    {
                      continue;
                    }

                    Log.Message("WC2 Service found at: " + _wc2_service_ipe.ToString());

                    // Connect to the WC2_Service
                    m_ssl_client = new TCPClientSSL(m_queue_receive);
                    m_ssl_client.Connect(_wc2_service_ipe);
                    if (!m_ssl_client.IsConnected)
                    {
                      m_ssl_client.Disconnect();
                      m_ssl_client = null;

                      continue;
                    }

                    Log.Message("Connected to:" + _wc2_service_ipe.ToString());
                    // AJQ 01-AUG-2014, Connected but on any error or not authorized response wait more time 
                    _wait_hint = 10000;

                    _msg_ok = false;

                    // Enroll
                    WC2_Message wc2_request;
                    WC2_Message wc2_response;
                    String xml_request;
                    Byte[] request;
                    IAsyncResult result;

                    wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgEnrollTerminal);
                    wc2_request.MsgHeader.TerminalId = m_terminal_name;

                    xml_request = wc2_request.ToXml();
                    request = Encoding.UTF8.GetBytes(xml_request);
                    if (!m_ssl_client.Send(request))
                    {
                      continue;
                    }


                    string xml = "";

                    xml = ReciveSyncMessage();
                    wc2_response = WC2_Message.CreateMessage(xml);
                    Log.Message("Enroll ResponseCode: " + wc2_response.MsgHeader.ResponseCode.ToString());
                    if (wc2_response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
                    {
                      WC2_MsgEnrollTerminalReply _wc2_msg_reply;

                      _wc2_msg_reply = (WC2_MsgEnrollTerminalReply)wc2_response.MsgContent;
                      _msg_ok = true;

                      lock (this)
                      {
                        m_terminal_session_id = wc2_response.MsgHeader.TerminalSessionId;
                        m_sequence_id = Math.Max(m_sequence_id, 1 + _wc2_msg_reply.LastSequenceId);
                        m_transaction_id = Math.Max(m_transaction_id, 1 + _wc2_msg_reply.LastTransactionId);
                      }
                    }

                    if (!_msg_ok)
                    {
                      continue;
                    }

                    _msg_ok = false;

                    // Get Parameters
                    wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgGetParameters);
                    lock (this)
                    {
                      wc2_request.MsgHeader.TerminalId = m_terminal_name;
                      wc2_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
                      this.GetSequenceId(out wc2_request.MsgHeader.SequenceId);
                    }

                    xml_request = wc2_request.ToXml();
                    request = Encoding.UTF8.GetBytes(xml_request);
                    result = m_ssl_client.BeginSend(request, new AsyncCallback(SendCallback), m_ssl_client);

                  
                    // Response received
                    string xml2 = "";

                    // Enrolled
                    xml2 = ReciveSyncMessage(); ;
                    wc2_response = WC2_Message.CreateMessage(xml2);
                    Log.Message("GetParameters ResponseCode: " + wc2_response.MsgHeader.ResponseCode.ToString());
                    if (wc2_response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
                    {
                      WC2_MsgGetParametersReply wc2_msg_reply;

                      wc2_msg_reply = (WC2_MsgGetParametersReply)wc2_response.MsgContent;
                      _msg_ok = true;

                      m_keep_alive_interval = 1000 * wc2_msg_reply.KeepAliveInterval;
                      m_connection_timeout = 1000 * wc2_msg_reply.ConnectionTimeout;
                    }


                    if (!_msg_ok)
                    {
                      continue;
                    }

                    // Start receiving data
                    m_ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), m_ssl_client);


                    _enrolled   = true;
                    _disconnect = false;

                    SetStatus(WC2_PROTOCOL_STATUS.CONNECTED);
                  }
                  catch (Exception ex)
                  {
                    Log.Exception(ex);
                  }
                  finally
                  {
                    _wc2_query_service = null;

                    if (_disconnect)
                    {
                      if (m_ssl_client != null)
                      {
                        m_ssl_client.Disconnect();
                      }
                    }
                  }
                }
              }
              break;
          };
        }
        catch
        {
          try
          {
            m_ssl_client.Disconnect();
          }
          catch
          {}
          SetStatus(WC2_PROTOCOL_STATUS.UNKNOWN);
        }
      }
    } // MainThread


    #endregion

    #region Public Methods

    #endregion

    #region Public Messages

    /// <summary>
    /// Enroll Terminal: LKT Request.
    /// </summary>
    void WC2_IProtocol.Start([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TerminalName, IntPtr LoggerCallback)
    {
      m_terminal_name = TerminalName;

      if (!LoggerCallback.Equals(IntPtr.Zero))
      {
        try
        {
          m_logger_callback = (WC2_LoggerWriteCallback)Marshal.GetDelegateForFunctionPointer(LoggerCallback, Type.GetType("WSI.WC2.WC2_Protocol.WC2_LoggerWriteCallback"));
        }
        catch (Exception e)
        {
          Log.Message(e.Message);
        }
      }

      m_wc2_main_thread.Start();
      
    } // StartProtocol


    /// <summary>
    /// 
    /// </summary>
    void WC2_IProtocol.Stop()
    {
      // Do nothing ...
      Log.Message("WC2 STOP!!!");
      Log.Message("");

    } // Stop

    public void GetIsConnected(out bool IsConnected)
    {
      IsConnected = (m_status == WC2_PROTOCOL_STATUS.CONNECTED);
    }

    public void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId)
    {
      lock (this)
      {
        SequenceId = m_sequence_id++;
      }
    } // GetSequenceId

    public void GetTransactionId([Out, MarshalAs(UnmanagedType.I8)] out Int64 TransactionId)
    {
      lock (this)
      {
        TransactionId = m_transaction_id++;
      }
    } // GetTransactionId


    private void SendCallback(IAsyncResult Result)
    {
      try
      {
        TCPClientSSL _ssl_client;

        m_tick_last_msg_sent = Misc.GetTickCount();

        _ssl_client = (TCPClientSSL)Result.AsyncState;
        _ssl_client.EndSend(Result);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    } // SendCallback

    private void ReceiveCallback(IAsyncResult Result)
    {
      m_tick_last_msg_received = Misc.GetTickCount();
    } // ReceiveCallback

    private String ReciveSyncMessage()
    {
      StringBuilder _sb;
      String _message;
      Int32 _tick_start_receiving;
      IAsyncResult _result;
      Boolean _msg_ok = false;
      byte[] _raw_response;

      _sb = new StringBuilder();

      _tick_start_receiving = Misc.GetTickCount();

      try
      {


        while (Misc.GetElapsedTicks(_tick_start_receiving) < WC2_TIMEOUT)
        {
          _result = m_ssl_client.BeginReceive(new AsyncCallback(ReceiveCallback), m_ssl_client);
          if (_result.AsyncWaitHandle.WaitOne(WC2_TIMEOUT, false))
          {
            // Enrolled
            _raw_response = m_ssl_client.EndReceive(_result);
            _sb.Append(Encoding.UTF8.GetString(_raw_response));

            if (_sb.ToString().EndsWith("</WC2_Message>"))
            {
              //Message completed
              _msg_ok = true;
              break;
            }
          }
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_msg_ok)
      {
        _message = _sb.ToString();
      }
      else
      {
        _message = String.Empty;
        Log.Error("ReciveSyncMessage: Message not Correct");
      }

      return _message;
    }


    private void EnqueueReceivedMessage(IAsyncResult Result)
    {
      Boolean _error;
      Boolean _disconnect;
      TCPClientSSL _ssl_client;
      Byte[] _raw_response;
      String _xml_response;
      WC2_Message _wc2_response;
      Boolean _enqueue_message;
      String _str_error;


      // Default: Error occurred -> Disconnect
      _error = true;
      _disconnect = true;
      _wc2_response = null;
      _xml_response = null;
      _str_error = "Unknown";
      _ssl_client = null;

      try
      {
        m_tick_last_msg_received = Misc.GetTickCount();

        _ssl_client = (TCPClientSSL)Result.AsyncState;

        if (_ssl_client == null)
        {
          _str_error = "SSLClient is null";
          return;
        }

        _raw_response = _ssl_client.EndReceive(Result);
        if (_raw_response == null)
        {
          _str_error = "Raw is null";
          return;
        }

        if (_raw_response.Length == 0)
        {
          _str_error = "Raw is empty";
          return;
        }

        String _xml_partial;
        int _idx0;
        int _idx1;

        _disconnect = false;

        _str_error = "Parsing ...";
        _xml_partial = Encoding.UTF8.GetString(_raw_response);
        m_sb.Append(_xml_partial);


        while (true)
        {
          _xml_response = null;

          if (m_sb.Length < "<WC2_Message></WC2_Message>".Length)
          {
            break;
          }

          _xml_partial = m_sb.ToString();
          _idx0 = _xml_partial.IndexOf("<WC2_Message>");
          if (_idx0 >= 0)
          {
            _idx1 = _xml_partial.IndexOf("</WC2_Message>");

            if (_idx1 > 0)
            {
              _idx1 = _idx1 + "</WC2_Message>".Length;

              _xml_response = _xml_partial.Substring(_idx0, _idx1 - _idx0);
              _xml_partial = _xml_partial.Substring(_idx1);
              m_sb.Length = 0;
              m_sb.Append(_xml_partial);
            }
          }

          if (String.IsNullOrEmpty(_xml_response))
          {
            break;
          }

          _str_error = "CreateMessage(Xml)";
          _wc2_response = WC2_Message.CreateMessage(_xml_response);

          if (_wc2_response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID)
          {
            _str_error = "SESSION NOT VALID";
            _disconnect = true;
            m_sb.Length = 0; // Delete any pending received data to be processed 

            return;
          }

          _str_error = "EnqueueMessage ...";
          _enqueue_message = true;

          if (_enqueue_message
           && _wc2_response.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgKeepAliveReply)
          {
            m_queue_responses.Enqueue(_wc2_response);
          }

        } // while

        _error = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        LoggerWrite("EnqueueReceivedMessage", "Exception: " + _ex.Message);

        if (_wc2_response != null)
        {
          LoggerWrite("EnqueueReceivedMessage", "Exception In " + _wc2_response.MsgHeader.MsgType.ToString());
        }

        _error = true;
      }
      finally
      {
        if (_error)
        {
          try
          {
            LoggerWrite("EnqueueReceivedMessage", "Error: " + _str_error);

            if (_wc2_response != null)
            {
              LoggerWrite("EnqueueReceivedMessage", " MessageType: " + _wc2_response.MsgHeader.MsgType.ToString());
            }
            else if (_xml_response != null)
            {
              int _beginning_tag;
              int _ending_tag;

              _beginning_tag = _xml_response.IndexOf("<MsgType>");
              _ending_tag = _xml_response.IndexOf("</MsgType>");

              if (_beginning_tag > 0 && _ending_tag > 0)
              {
                LoggerWrite("EnqueueReceivedMessage", "MessageType: " + _xml_response.Substring(_beginning_tag, _ending_tag - _beginning_tag));
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
            LoggerWrite("EnqueueReceivedMessage", "Exception finally: " + _ex.Message);
          }
        }

        try
        {
          if (!_disconnect)
          {
            _str_error = "BeginReceive ...";
            _ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), _ssl_client);
          }
        }
        catch (Exception _ex)
        {
          LoggerWrite("EnqueueReceivedMessage", "BeginReceive Failed! Ex:" + _ex.Message);
          _disconnect = true;
        }

        if (_disconnect)
        {
          SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);
        }
      }
    } // EnqueueReceivedMessage

    private void EnqueueReceivedMessage2(IAsyncResult Result)
    {
      Boolean _disconnect;
      TCPClientSSL _ssl_client;
      Byte[] _raw_response;
      String _xml_response;
      WC2_Message _wc2_response;

      // Default: Error occurred -> Disconnect
      _disconnect = true;

      try
      {
        m_tick_last_msg_received = Misc.GetTickCount();

        _ssl_client = (TCPClientSSL)Result.AsyncState;
        _raw_response = _ssl_client.EndReceive(Result);

        if ( _raw_response == null )
        {
          return;
        }
        if ( _raw_response.Length == 0 )
        {
          return;
        }

        _xml_response = Encoding.UTF8.GetString(_raw_response);
        _wc2_response = WC2_Message.CreateMessage(_xml_response);

        if (_wc2_response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID)
        {
          return;
        }
        if (_wc2_response.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgKeepAliveReply)
        {
          m_queue_responses.Enqueue(_wc2_response);
        }
        _ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), _ssl_client);
        _disconnect = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if ( _disconnect )
        {
          try
          {
            SetStatus (WC2_PROTOCOL_STATUS.DISCONNECTED);
          }
          catch (Exception _ex)
          {
            Log.Exception (_ex);
          }
        }
      }
    } // EnqueueReceivedMessage


    public void Send (WC2_IMessage MessageToSend)
    {
      WC2_Message _wc2_request;
      WC2_MessageEnvelope _envelope;
      String _xml_request;
      Byte[] _raw_request;
      Boolean _disconnect;

      if (Status != WC2_PROTOCOL_STATUS.CONNECTED)
      {
        return;
      }

      // Default: Error occurred -> Disconnect
      _disconnect = true;

      try
      {
        _envelope = (WC2_MessageEnvelope)MessageToSend;
        _wc2_request = _envelope.Message;

        lock (this)
        {
          _wc2_request.MsgHeader.TerminalId = m_terminal_name;
          _wc2_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
        }

        _xml_request = _wc2_request.ToXml();
        _raw_request = Encoding.UTF8.GetBytes(_xml_request);

        Log.Message("" + _wc2_request.MsgHeader.SequenceId.ToString("000000") + " " + _wc2_request.MsgHeader.MsgType.ToString() + ": " + _wc2_request.MsgHeader.ResponseCode.ToString());

        //m_ssl_client.BeginSend(_raw_request, new AsyncCallback(SendCallback), m_ssl_client);
        m_tick_last_msg_sent = Misc.GetTickCount();
        m_ssl_client.Send(_raw_request);
        m_tick_last_msg_sent = Misc.GetTickCount();

        _disconnect = false;
      }
      catch (Exception _ex)
      {
        Log.Exception (_ex);
      }
      finally
      {
        if ( _disconnect )
        {
          try
          {
            SetStatus(WC2_PROTOCOL_STATUS.DISCONNECTED);
          }
          catch
          {}
        }
      }
    } // Send

    /// <summary>
    /// Function called by Kiosk to obtain a response to a pending request.
    /// </summary>
    public void WaitForMessage(out WC2_IMessage ReceivedMessage)
    {
      WC2_Message wc2_message;

      ReceivedMessage = null;

      try
      {
        wc2_message = (WC2_Message)m_queue_responses.Dequeue();

        Log.Message("    " + wc2_message.MsgHeader.SequenceId.ToString("000000") + " " + wc2_message.MsgHeader.MsgType.ToString() + ": " + wc2_message.MsgHeader.ResponseCode.ToString());

        ReceivedMessage = WC2_MessageEnvelope.CreateEnvelope(wc2_message);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

    } // Response_GetMsgType

    private WC2_PROTOCOL_STATUS WaitStatusChanged(int MillisecondsTimeout)
    {
      m_status_changed.WaitOne(MillisecondsTimeout, false);

      return m_status;
    }

    private WC2_PROTOCOL_STATUS SetStatus(WC2_PROTOCOL_STATUS NewStatus)
    {
      WC2_PROTOCOL_STATUS _old_status;

      lock (this)
      {
        _old_status = m_status;

        if (_old_status != NewStatus)
        {
          m_status = NewStatus;
          m_status_tick = Environment.TickCount;
          m_status_changed.Set();

          if (NewStatus == WC2_PROTOCOL_STATUS.DISCONNECTED)
          {
            try
            {
              m_ssl_client.Disconnect();
            }
            catch
            {}
          }
          Log.Message("WC2_PROTOCOL_STATUS changed: " + _old_status.ToString() + " --> " + m_status.ToString() );
        }
      }

      return _old_status;
    }

    private WC2_PROTOCOL_STATUS Status
    {
      get
      {
        lock (this)
        {
          return m_status;
        }
      }
    }


    void WC2_IProtocol.NewRequest_GetCards(out WC2_IMessage Request, long SequenceId, int NumCards)
    {
      WC2_Message wc2_request;
      WC2_MsgGetCards wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgGetCards);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgGetCards)wc2_request.MsgContent;
      wc2_content.NumCards = NumCards;
      wc2_content.CardType = "Card5x5";

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }

    void WC2_IProtocol.NewRequest_GetJackpotInfo(out WC2_IMessage Request, long SequenceId, ushort GameId, int Denomination, long BetCredits)
    {
      WC2_Message wc2_request;
      WC2_MsgGetJackpotInfo wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgGetJackpotInfo);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgGetJackpotInfo)wc2_request.MsgContent;
      wc2_content.Denomination = Denomination;
      wc2_content.GameId = "Game_" + GameId.ToString();
      wc2_content.TerminalId = m_terminal_name;
      wc2_content.TotalBetCredits = BetCredits;

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }

    void WC2_IProtocol.NewRequest_SiteJackpotInfo (out WC2_IMessage Request, long SequenceId, ushort GameId, int Denomination, long BetCredits)
    {
      WC2_Message wc2_request;
      WC2_MsgSiteJackpotInfo wc2_content;

      wc2_request = WC2_Message.CreateMessage (WC2_MsgTypes.WC2_MsgSiteJackpotInfo);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgSiteJackpotInfo) wc2_request.MsgContent;
      wc2_content.Denomination = Denomination;
      wc2_content.GameId = "Game_" + GameId.ToString ();
      wc2_content.TerminalId = m_terminal_name;
      wc2_content.TotalBetCredits = BetCredits;

      Request = WC2_MessageEnvelope.CreateEnvelope (wc2_request);
    }

    void WC2_IProtocol.NewRequest_GetParameters (out WC2_IMessage Request, long SequenceId, int ClientId, int BuildId)
    {
      WC2_Message wc2_request;
      WC2_MsgGetParameters wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgGetParameters);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgGetParameters)wc2_request.MsgContent;
      wc2_content.ClientId = ClientId;
      wc2_content.BuildId = BuildId;

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }

    void WC2_IProtocol.NewRequest_GetPatterns(out WC2_IMessage Request, long SequenceId)
    {
      WC2_Message wc2_request;
      WC2_MsgGetPatterns wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgGetPatterns);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgGetPatterns)wc2_request.MsgContent;

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }

    void WC2_IProtocol.NewRequest_CancelPlay(out WC2_IMessage Request, long SequenceId, long TransactionId)
    {
      WC2_Message wc2_request;
      WC2_MsgCancelPlay wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgCancelPlay);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgCancelPlay)wc2_request.MsgContent;
      wc2_content.TransactionId = TransactionId;

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }


    void WC2_IProtocol.NewRequest_Play(out WC2_IMessage Request,
                                       long SequenceId, long TransactionId, ushort GameId,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                       long Denomination, long BetCredits, long WonCredits)
    {
      WC2_Message wc2_request;
      WC2_MsgPlay wc2_content;

      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgPlay);
      wc2_request.MsgHeader.SequenceId = SequenceId;
      wc2_content = (WC2_MsgPlay)wc2_request.MsgContent;
      wc2_content.TransactionId = TransactionId;
      wc2_content.GameId = GameName;
      wc2_content.JackpotBoundCredits = BetCredits;
      wc2_content.TotalPlayedCredits = BetCredits;
      wc2_content.WonCredits = WonCredits;
      wc2_content.Denomination = (int)Denomination;

      // Add the card
      wc2_content.NumCards = 1;
      wc2_content.Cards.Add(m_wc2_card);

      Request = WC2_MessageEnvelope.CreateEnvelope(wc2_request);
    }


    void WC2_IProtocol.GetInterface_MsgGetCardsReply(WC2_IMessage ReceivedMessage, out WC2_IMsgGetCardsReply Interface)
    {
      WC2_MessageEnvelope msg;
      WC2_MsgGetCardsReply reply;

      msg = (WC2_MessageEnvelope)ReceivedMessage;
      reply = (WC2_MsgGetCardsReply) msg.Message.MsgContent;

      m_wc2_card = (WC2_Card) reply.Cards[0];

      Interface = (WC2_IMsgGetCardsReply)new WC2_Response_MsgGetCardsReply(msg.Message);
    }

    void WC2_IProtocol.GetInterface_MsgGetJackpotInfoReply(WC2_IMessage ReceivedMessage, out WC2_IMsgGetJackpotInfoReply Interface)
    {
      WC2_MessageEnvelope msg;

      msg = (WC2_MessageEnvelope)ReceivedMessage;

      Interface = (WC2_IMsgGetJackpotInfoReply)new WC2_GetInterface_MsgGetJackpotInfoReply(msg.Message);
    }

    void WC2_IProtocol.GetInterface_MsgSiteJackpotInfoReply (WC2_IMessage ReceivedMessage, out WC2_IMsgSiteJackpotInfoReply Interface)
    {
      WC2_MessageEnvelope msg;

      msg = (WC2_MessageEnvelope) ReceivedMessage;

      Interface = (WC2_IMsgSiteJackpotInfoReply) new WC2_GetInterface_MsgSiteJackpotInfoReply (msg.Message);
    }

    void WC2_IProtocol.GetInterface_MsgGetPatternsReply(WC2_IMessage ReceivedMessage, out WC2_IMsgGetPatternsReply Interface)
    {
      WC2_MessageEnvelope msg;

      msg = (WC2_MessageEnvelope)ReceivedMessage;

      Interface = (WC2_IMsgGetPatternsReply)new WC2_GetInterface_MsgGetPatternsReply(msg.Message);
    }


    #endregion // Public Messages

    #region WC2_IProtocol Members



    void WC2_IProtocol.GetInterface_Parameters(WC2_IMessage ReceivedMessage, out WC2_Parameters Parameters)
    {
      WC2_MessageEnvelope msg;
      WC2_Message wc2_response;
      WC2_MsgGetParametersReply wc2_msg_reply;
      WC2_Parameters wc2_parameters;

      msg = (WC2_MessageEnvelope)ReceivedMessage;
      wc2_response = msg.Message;
      wc2_msg_reply = (WC2_MsgGetParametersReply)wc2_response.MsgContent;

      wc2_parameters = new WC2_Parameters();

      wc2_parameters.build_id = wc2_msg_reply.BuildId;
      wc2_parameters.client_id = wc2_msg_reply.ClientId;
      wc2_parameters.num_locations = 0;
      wc2_parameters.location1 = "";
      wc2_parameters.location2 = "";
      wc2_parameters.num_locations = Math.Min(2, wc2_msg_reply.NumLocations);
      if (wc2_parameters.num_locations > 0)
      {
        wc2_parameters.location1 = (String)wc2_msg_reply.Locations[0];
        if (wc2_parameters.num_locations > 1)
        {
          wc2_parameters.location2 = (String)wc2_msg_reply.Locations[1];
        }
      }

      wc2_parameters.keep_alive_interval = 1000 * wc2_msg_reply.KeepAliveInterval;
      wc2_parameters.query_jackpot_interval = 1000 * wc2_msg_reply.JackpotRequestInterval;
      wc2_parameters.connection_timeout = 1000 * wc2_msg_reply.ConnectionTimeout;

      Parameters = wc2_parameters;
    }



    #endregion

    #region WC2_IProtocol Members


    void WC2_IProtocol.GetInterface_MsgPlayReply(WC2_IMessage ReceivedMessage, out WC2_IMsgPlayReply Interface)
    {
      WC2_MessageEnvelope wc2_envelope;
      WC2_Message wc2_message;
      WC2_MsgPlayReply wc2_reply;
      WC2_Response_MsgPlayReply wc2_response;

      wc2_envelope = (WC2_MessageEnvelope)ReceivedMessage;
      wc2_message = wc2_envelope.Message;
      wc2_reply   = (WC2_MsgPlayReply) wc2_message.MsgContent;
      wc2_response = new WC2_Response_MsgPlayReply(wc2_message);

      m_wc2_card = (WC2_Card)wc2_reply.Cards[0];

      Interface = wc2_response;
    }

    #endregion
  }


  public class WC2_MessageEnvelope : WC2_IMessage
  {
    WC2_Message m_message;

    #region WC2_IMessage Members

    void WC2_IMessage.GetSequenceId(out long SequenceId)
    {
      SequenceId = Message.MsgHeader.SequenceId;
    }

    void WC2_IMessage.GetMessageType(out int MessageType)
    {
      MessageType = (int)Message.MsgHeader.MsgType;
    }

    void WC2_IMessage.GetResponseCode(out UInt16 ResponseCode)
    {
      ResponseCode = (UInt16)Message.MsgHeader.ResponseCode;
    }


    #endregion

    public static WC2_MessageEnvelope CreateEnvelope(WC2_Message Message)
    {
      WC2_MessageEnvelope msg;

      msg = new WC2_MessageEnvelope();

      msg.m_message = Message;

      return msg;
    }


    public WC2_Message Message
    {
      get { return m_message; }
    }
  }


}


