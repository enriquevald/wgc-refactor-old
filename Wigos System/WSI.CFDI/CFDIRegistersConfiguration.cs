﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.CFDI.oServicioReten;

namespace WSI.CFDI
{
  public class CFDIRegistersConfiguration
  {

    #region " Members "

    private static String m_user;
    private static String m_password;
    private static Int32 m_max_retrys;
    private static String m_site;

    #endregion 

    #region " Properties "

    public String User
    {
      get { return m_user; }
      set { m_user = value; }
    } // User

    public String Password
    {
      get { return m_password; }
      set { m_password = value; }
    } // Password
    
    public Int32 MaxRetrys
    {
      get { return m_max_retrys; }
      set { m_max_retrys = value; }
    } // MaxRetrys

    public String Site
        {
      get { return Site; }
      set { Site = value; }
    } // Site

    #endregion 

    public CFDIRegistersConfiguration
    {
    }
  }
}
