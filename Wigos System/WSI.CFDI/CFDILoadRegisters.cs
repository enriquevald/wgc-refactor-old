﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: CFDILoadRegisters.cs
// 
//      DESCRIPTION: Load Registers for CFDI
// 
//           AUTHOR: Pablo Molina
// 
//    CREATION DATE: 13-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-OCT-2016 PM     First release.
// 25-OCT-2016 JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
// 17-NOV-2016 AMF    Bug 20692:CFDI: No suben al CFDI los premios obtenidos al realizar una compra de fichas
// 22-NOV-2016 JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.CFDI
{
  public class CFDILoadRegisters
  {
    #region " Constants "

    private const Int32 CFDI_MAX_OPERATIONS = 1000;
    private const String CFDI_DATE_FORMAT = "yyyyMMdd";

    #endregion

    #region " Members "

    public static GeneralParam.Dictionary m_general_param;

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Account operation to CFDI registers
    /// </summary>
    /// <returns></returns>
    public static Boolean AccountOperationToCFDIRegisters()
    {
      DataTable _dt_sales_and_payments;
      String _operations_ids;
      Int64 _last_id;
      DateTime _first_date;
      Int32 _record_inserted;

      _operations_ids = "";
      _dt_sales_and_payments = null;
      _record_inserted = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Load General Params dictionary
          RefreshGeneralParams(_db_trx.SqlTransaction);

          _first_date = GetFirstDate();
          _last_id = GetLastOperationId();

          // Get operation ids pending to process
          _operations_ids = GetUnprocessedOperationIds(_db_trx.SqlTransaction, _last_id, _first_date);

          Log.Message("CFDI Service: Loading registers...");

          if (String.IsNullOrEmpty(_operations_ids.Trim()))
          {
            Log.Message("CFDI Service: No registers to load.");

            return true;
          }

          // Get data for pending registers by OperationID
          if (!SharedBL02Common.GetSalesAndPaymentsForCFDI(CFDI_MAX_OPERATIONS
                                                          , _operations_ids
                                                          , _db_trx.SqlTransaction
                                                          , out _dt_sales_and_payments))
          {
            Log.Message("CFDI Service: Error on AccountOperationToCFDIRegisters. No loaded registers.");

            return false;
          }

          // Insert pending operations in CFDI_REGISTERS
          foreach (DataRow _dt in _dt_sales_and_payments.Select("Premio > 0", " IdOperacion ASC"))
          {
            if (SaveCFDIRegister(_db_trx.SqlTransaction, _dt))
            {
              _record_inserted++;
            }
          }

          // Save last item loaded in GeneralParam 'CFDI.LastOperationId' 
          _last_id = Convert.ToInt32(_dt_sales_and_payments.Compute("MAX(IdOperacion)", String.Empty));
          UpdateLastOperationIdObtained(_db_trx.SqlTransaction, _last_id);

          // Add log
          if (_record_inserted > 0)
          {
            Log.Message(String.Format("CFDI Service: {0} register{1} loaded", _record_inserted, (_record_inserted > 1) ? "s" : String.Empty));
          }
          else
          {
            Log.Message("CFDI Service: No registers to load.");
          }

          _db_trx.Commit();

          return true;

        }
      }
      catch (Exception Ex)
      {
        Log.Error("CFDI Service: Error on AccountOperationToCFDIRegisters");

        Log.Exception(Ex);
      }

      return false;
    } // AccountOperationToCFDIRegisters

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Get unprocessed operations Id's
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="LastId"></param>
    /// <param name="FirstDate"></param>
    /// <returns></returns>
    private static String GetUnprocessedOperationIds(SqlTransaction SqlTrx, Int64 LastId, DateTime FirstDate)
    {
      StringBuilder _sb;
      String _results;

      _results = String.Empty;
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("  SELECT  AO_OPERATION_ID               ");
        _sb.AppendLine("    FROM  ACCOUNT_OPERATIONS            ");
        _sb.AppendLine("   WHERE  AO_OPERATION_ID >  @LastId    ");
        _sb.AppendLine("     AND  AO_DATETIME     >  @FirstDate ");
        _sb.AppendLine("     AND  AO_DATETIME     <= @Delay     ");
        _sb.AppendLine("     AND  AO_CODE         IN  (2, 16)   "); // OperationCode.CASH_OUT = 2  &  CHIPS_PURCHASE_WITH_CASH_OUT = 16

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@LastId", SqlDbType.Int).Value = LastId;
          _sql_command.Parameters.Add("@FirstDate", SqlDbType.DateTime).Value = FirstDate;
          _sql_command.Parameters.Add("@Delay", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(GetDelay());

          using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              if (String.IsNullOrEmpty(_results))
              {
                _results = _sql_reader[0].ToString();
              }
              else
              {
                _results = _results + ", " + _sql_reader[0].ToString();
              }
            }
          }
        }

        return _results;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return null;
    } // GetUnprocessedOperationIds

    /// <summary>
    /// Save the data in cfdi_registers
    /// </summary>
    /// <param name="DRow"></param>
    private static Boolean SaveCFDIRegister(SqlTransaction SqlTrx, DataRow DRow)
    {
      StringBuilder _sb;
      SqlXml _xml;
      String _result;

      _result = String.Empty;
      _sb = new StringBuilder();

      _sb.AppendLine("  IF NOT EXISTS (SELECT * FROM CFDI_REGISTERS WHERE CR_OPERATION_ID = @cr_operation_id)         ");
      _sb.AppendLine("  BEGIN                                                                                         ");
      _sb.AppendLine("    INSERT INTO CFDI_REGISTERS (CR_OPERATION_ID , CR_STATUS , CR_INSERTED , CR_XML_REQUEST  )   ");
      _sb.AppendLine("                        VALUES (@cr_operation_id, @cr_status, @cr_inserted, @cr_xml_request )   ");
      _sb.AppendLine("  END                                                                                           ");

      try
      {
        _xml = ConvertDataRowToXmlString(DRow, ref _result);

        if (_xml == null)
        {
          Log.Message(String.Format("CFDI Service: Error inserting CFDI register. Error Building XML:\n    {0}", _result));

          return false;
        }

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@cr_xml_request", SqlDbType.Xml).Value = _xml;
          _sql_command.Parameters.Add("@cr_operation_id", SqlDbType.Int).Value = DRow[0];
          _sql_command.Parameters.Add("@cr_inserted", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_command.Parameters.Add("@cr_status", SqlDbType.Int).Value = CFDIThread.CFDIStatus.Pending;

          return (_sql_command.ExecuteNonQuery() > 0);
        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;
    } // SaveCFDIRegister

    /// <summary>
    /// Convert DataRow to Xml String
    /// </summary>
    /// <param name="DRow"></param>
    /// <param name="Message"></param>
    /// <returns></returns>
    private static SqlXml ConvertDataRowToXmlString(DataRow DRow, ref String Message)
    {
      CFDIRegister _register;
      XmlSerializer _xml_serializer;
      MemoryStream _strm;
      StringWriter _sww;

      _strm = new MemoryStream();
      _sww = new StringWriter();

      try
      {
        _register = new CFDIRegister();

        _register.IdOperacion = Convert.ToString(DRow["IdOperacion"]);
        _register.FechaHora = Convert.ToString(DRow["FechaHora"]);
        _register.NoCuenta = Convert.ToString(DRow["NoCuenta"]);
        _register.TipoOperacion = Convert.ToString(DRow["TipoOperacion"]);
        _register.TotalRecarga = Convert.ToString(DRow["TotalRecarga"]);
        _register.EmpresaA = Convert.ToString(DRow["EmpresaA"]);
        _register.EmpresaB = Convert.ToString(DRow["EmpresaB"]);
        _register.Impuesto1 = Convert.ToString(DRow["Impuesto1"]);
        _register.Impuesto2 = Convert.ToString(DRow["Impuesto2"]);
        _register.TotalRetiro = Convert.ToString(DRow["TotalRetiro"]);
        _register.Devolucion = Convert.ToString(DRow["Devolucion"]);
        _register.Premio = Convert.ToString(DRow["Premio"]);
        _register.ISR1 = Convert.ToString(DRow["ISR1"]);
        _register.ISR2 = Convert.ToString(DRow["ISR2"]);
        _register.RetiroEnEfectivo = Convert.ToString(DRow["RetiroEnEfectivo"]);
        _register.RetiroEnCheque = Convert.ToString(DRow["RetiroEnCheque"]);
        _register.RFC = Convert.ToString(DRow["RFC"]);
        _register.CURP = Convert.ToString(DRow["CURP"]);
        _register.Nombre = Convert.ToString(DRow["Nombre"]);
        _register.Domicilio = Convert.ToString(DRow["Domicilio"]);
        _register.Localidad = Convert.ToString(DRow["Localidad"]);
        _register.CodigoPostal = Convert.ToString(DRow["CodigoPostal"]);
        _register.ConstanciaRFC = Convert.ToString(DRow["Constancia.RFC"]);
        _register.ConstanciaCURP = Convert.ToString(DRow["Constancia.CURP"]);
        _register.ConstanciaNombre = Convert.ToString(DRow["Constancia.Nombre"]);
        _register.ConstanciaDomicilio = Convert.ToString(DRow["Constancia.Domicilio"]);
        _register.ConstanciaLocalidad = Convert.ToString(DRow["Constancia.Localidad"]);
        _register.ConstanciaCodigoPostal = Convert.ToString(DRow["Constancia.CodigoPostal"]);
        _register.Constancia = Convert.ToString(DRow["Constancia"]);
        _register.DocumentID = Convert.ToString(DRow["DocumentID"]);
        _register.RedondeoPorRetencion = Convert.ToString(DRow["RedondeoPorRetencion"]);
        _register.CargoPorServicio = Convert.ToString(DRow["CargoPorServicio"]);
        _register.RedondeoPorDecimales = Convert.ToString(DRow["RedondeoPorDecimales"]);
        _register.SesionCaja = Convert.ToString(DRow["SesionCaja"]);
        _register.PremioEspecie = Convert.ToString(DRow["PremioEspecie"]);
        _register.CashierName = Convert.ToString(DRow["CashierName"]);
        _register.PremioCaducado = Convert.ToString(DRow["PremioCaducado"]);
        _register.VoucherId = Convert.ToString(DRow["VoucherId"]);
        _register.Folio = Convert.ToString(DRow["Folio"]);
        _register.SaldoPromocional = Convert.ToString(DRow["SaldoPromocional"]);
        _register.ClaveTipoPago = Convert.ToString(DRow["ClaveTipoPago"]);
        _register.FormaPago = Convert.ToString(DRow["FormaPago"]);
        _register.RecargaPorPuntos = Convert.ToString(DRow["RecargaPorPuntos"]);
        _register.Fecha = Convert.ToDateTime(DRow["Fecha"]).ToString(CultureInfo.InvariantCulture);
        _register.TarjetaCliente = Convert.ToString(DRow["TarjetaCliente"]);
        _register.Nombres = Convert.ToString(DRow["Nombres"]);
        _register.Apellidos = Convert.ToString(DRow["Apellidos"]);
        _register.Shift_Id = Convert.ToString(DRow["Shift_Id"]);
        _register.Cajera_Id = Convert.ToString(DRow["Cajera_Id"]);
        _register.CajaNombres = Convert.ToString(DRow["CajaNombres"]);
        _register.CajaApellidos = Convert.ToString(DRow["CajaApellidos"]);
        _register.Caja_Id = Convert.ToString(DRow["Caja_Id"]);
        _register.Caja_Name = Convert.ToString(DRow["Caja_Name"]);
        _register.ClaveTipoPagoRecarga = Convert.ToString(DRow["ClaveTipoPagoRecarga"]);
        _register.TipoOperacionDescripcion = Convert.ToString(DRow["TipoOperacionDescripcion"]);
        _register.Folio2 = Convert.ToString(DRow["Folio2"]);
        _register.TotalRecargaSinImpuestos = Convert.ToString(DRow["TotalRecargaSinImpuestos"]);
        _register.BalanceAFichas = Convert.ToString(DRow["BalanceAFichas"]);
        _register.CageCurrencyType = Convert.ToString(DRow["CageCurrencyType"]);
        _register.CurrencyIsoCode = Convert.ToString(DRow["CurrencyIsoCode"]);
        _register.PurchaseCurrency = Convert.ToString(DRow["PurchaseCurrency"]);
        _register.SalesCurrency = Convert.ToString(DRow["SalesCurrency"]);
        _register.Nacionalidad = Convert.ToString(DRow["Nacionalidad"]);
        _register.EntidadFederativa = (Convert.ToInt32(DRow["EntidadFederativa"]) == -1) ? String.Empty : Convert.ToString(DRow["EntidadFederativa"]);
        _register.Email = Convert.ToString(DRow["Email"]);
        _register.UserType = Convert.ToString(DRow["UserType"]);

        _xml_serializer = new XmlSerializer(typeof(CFDIRegister));

        using (XmlWriter writer = XmlWriter.Create(_sww))
        {
          _xml_serializer.Serialize(_strm, _register);
        }

        return new SqlXml(_strm);
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return null;
    } // ConvertDataRowToXmlString

    /// <summary>
    /// Update last operation id loaded
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="LastId"></param>
    private static void UpdateLastOperationIdObtained(SqlTransaction SqlTrx, Int64 LastId)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE  GENERAL_PARAMS                        ");
      _sb.AppendLine("     SET  GP_KEY_VALUE    = @pLastOperationId   ");
      _sb.AppendLine("   WHERE  GP_GROUP_KEY    = 'CFDI'              ");
      _sb.AppendLine("     AND  GP_SUBJECT_KEY  = 'LastOperationId'   ");

      try
      {
        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pLastOperationId", SqlDbType.Int).Value = LastId;

          _sql_command.ExecuteNonQuery();
        }
      }
      catch (Exception)
      {
        return;
      }
    } // UpdateLastOperationIdObtained

    /// <summary>
    /// First Date
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="FirstDate"></param>
    private static void UpdateFirstDate(String FirstDate)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE  GENERAL_PARAMS                  ");
      _sb.AppendLine("     SET  GP_KEY_VALUE    = @pFirstDate   ");
      _sb.AppendLine("   WHERE  GP_GROUP_KEY    = 'CFDI'        ");
      _sb.AppendLine("     AND  GP_SUBJECT_KEY  = 'FirstDate'   ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pFirstDate", SqlDbType.VarChar).Value = FirstDate;

            _sql_command.ExecuteNonQuery();
          }

          // Refresh m_general_params
          RefreshGeneralParams(_db_trx.SqlTransaction);

          _db_trx.Commit();
        }
      }
      catch (Exception)
      {
        return;
      }
    } // UpdateFirstDate

    #region " General Params "

    /// <summary>
    /// Refresh GP data
    /// </summary>
    /// <param name="SqlTrx"></param>
    private static void RefreshGeneralParams(SqlTransaction SqlTrx)
    {
      m_general_param = GeneralParam.Dictionary.Create(SqlTrx, "CFDI");
    } // RefreshGeneralParams

    /// <summary>
    /// Get GP CFDI.LastOperationId
    /// </summary>
    /// <returns></returns>
    private static Int64 GetLastOperationId()
    {
      return m_general_param.GetInt64("CFDI", "LastOperationId", 0);
    } // GetLastOperationId

    /// <summary>
    /// Get GP CFDI.FirstDate
    /// </summary>
    /// <returns></returns>
    private static DateTime GetFirstDate()
    {
      DateTime _date;

      // Default value
      _date = WGDB.Now;

      // Check if GP is null
      if (String.IsNullOrEmpty(m_general_param.GetString("CFDI", "FirstDate", String.Empty)))
      {
        // Update General Param with default value
        UpdateFirstDate(_date.ToString(CFDI_DATE_FORMAT));
      }

      _date = DateTime.ParseExact(m_general_param.GetString("CFDI", "FirstDate", _date.ToString(CFDI_DATE_FORMAT))
                                  , CFDI_DATE_FORMAT
                                  , CultureInfo.InvariantCulture
                                  , DateTimeStyles.None);

      return _date;
    } // GetFirstDate

    /// <summary>
    /// Get GP CFDI.GetDelay
    /// </summary>
    /// <returns></returns>
    private static Int64 GetDelay()
    {
      return m_general_param.GetInt64("CFDI", "Scheduling.Delay", 30);
    } // GetDelay

    #endregion

    #endregion

  } // End CFDILoadRegisters
}
