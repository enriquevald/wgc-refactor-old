﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: CFDI.cs
// 
//      DESCRIPTION: CFDI
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 05-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-OCT-2016 JBP    First release.
// 25-OCT-2016 JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
// 22-NOV-2016 JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
//------------------------------------------------------------------------------
using System;
using System.Threading;
using WSI.Common;

namespace WSI.CFDI
{
  public class CFDIThread
  {
    #region " Enums "

    public enum CFDIThreadType
    {
      Load = 0,
      Process = 1
    }

    public enum CFDIStatus
    {
      Pending = 0,
      Send = 1,
      Retries = 2,
      Error = 3
    }

    #endregion

    #region " Members "

    private static DateTime m_datetime_load;
    private static DateTime m_datetime_process;

    private static Int32 m_wait_hint_load;
    private static Int32 m_wait_hint_process;

    #endregion 

    #region " Public Methods "

    /// <summary>
    /// Initializes CFDI threads
    /// </summary>
    public static void Init()
    {
      Thread _thread_cfdi;

      _thread_cfdi = new Thread(CDFIMainThread);
      _thread_cfdi.Name = "CDFIMainThread";
      _thread_cfdi.Start();
    }

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Thread CFDI 
    /// </summary>
    private static void CDFIMainThread()
    {
      Int32 _wait_hint;

      InitializeTimes();

      _wait_hint = 1000; // 1 sec

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          // Check if functionality is enabled
          if (!IsCFDIEnabled() || !IsCFDIActive())
          {
            _wait_hint = 1 * 60 * 60 * 1000; // 1 Hour

            continue;
          }

          // Check if service is principal
          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1 * 15 * 1000; //15 seg

            continue;
          }

          // Check if is time to load registers
          if (TimeToExecute(CFDIThreadType.Load))
          {
            LoadRegisters();
          }

          // Check if is time to process registers
          if (TimeToExecute(CFDIThreadType.Process))
          {
            ProcessRegisters();            
          }

          _wait_hint = 1000; // 1 sec
        } 
        catch (Exception _ex)
        {
          _wait_hint = 1 * 60 * 1000; // 60 seg

          Log.Exception(_ex);
        }
      } // while(true)

    } // CDFIMainThread

    /// <summary>
    /// Load registers method
    /// </summary>
    private static void LoadRegisters()
    {
      // Reset load times
      m_datetime_load  = WGDB.Now;
      m_wait_hint_load = GetScheduling(CFDIThreadType.Load);

      // Load account operation info into CFDI_REGISTERS table
      if (!CFDILoadRegisters.AccountOperationToCFDIRegisters())
      {
        m_wait_hint_load = 1 * 60 * 1000; // 60 seg
      }   

    } // LoadRegisters

    /// <summary>
    /// Process registers method
    /// </summary>
    private static void ProcessRegisters()
    {
      // Reset process times
      m_datetime_process  = WGDB.Now;
      m_wait_hint_process = GetScheduling(CFDIThreadType.Process);

      // Process all registers: Send data to WS
      if (!CFDIProcessRegisters.ProcessRegistersData())
      {
        m_wait_hint_process = 1 * 60 * 1000; // 60 seg
      }

    } // ProcessRegisters

    /// <summary>
    /// Checks if is time to execute load or process method
    /// </summary>
    /// <param name="Type"></param>
    /// <returns></returns>
    private static Boolean TimeToExecute(CFDIThreadType Type)
    {
      switch(Type)
      {
        case CFDIThreadType.Load:
        {
          return m_datetime_load.AddMilliseconds(m_wait_hint_load) < WGDB.Now;
        }

        case CFDIThreadType.Process:
        {
          return m_datetime_process.AddMilliseconds(m_wait_hint_process) < WGDB.Now;
        }
      }

      return false;
    } // TimeToExecute

    /// <summary>
    /// Initialize method times
    /// </summary>
    private static void InitializeTimes()
    {
      // Initial Times
      m_datetime_load     = WGDB.Now;
      m_datetime_process  = WGDB.Now;

      // Initial delay      
      m_wait_hint_load    = 1 *  5 * 1000;  //  5 segs
      m_wait_hint_process = 1 * 30 * 1000;  // 30 segs

    } // InitializeTimes

    #region " General Params "

    /// <summary>
    /// Get GP CFDI.Enabled
    /// </summary>
    /// <returns></returns>
    private static Boolean IsCFDIEnabled()
    {
      return Misc.IsCDFIEnabled();
    } // IsCFDIEnabled

    /// <summary>
    /// Get GP CFDI.Active
    /// </summary>
    /// <returns></returns>
    private static Boolean IsCFDIActive()
    {
      return Misc.IsCDFIActive();
    } // IsCFDIActive

    /// <summary>
    /// Get GP CFDI.Uri
    /// </summary>
    /// <returns></returns>
    private static Int32 GetScheduling(CFDIThreadType Type)
    {
      switch(Type)
      {
        case CFDIThreadType.Load:
        {
          return GeneralParam.GetInt32("CFDI", "Scheduling.Load", 60) * 60 * 1000; // (Minutos * 60 * 1000) -> MiliSegundos
        }
        
        case CFDIThreadType.Process:
        {
          return GeneralParam.GetInt32("CFDI", "Scheduling.Process", 60) * 60 * 1000; // (Minutos * 60 * 1000) -> MiliSegundos
        }
      }

      // Default
      return 15 * 60 * 1000; // 15 segs

    } // GetScheduling

    /// <summary>
    /// Set TEST MODE
    /// </summary>
    /// <returns></returns>
    public static Boolean IsTestMode()
    {
      return false;
    } // IsTestMode

    #endregion

    #endregion

  } // End CDFI
}
