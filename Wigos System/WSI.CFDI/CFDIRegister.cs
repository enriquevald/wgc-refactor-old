﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CFDIRegister.cs
//// 
////      DESCRIPTION: CFDIRegister
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 10-OCT-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 10-OCT-2016 JBP    First release.
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WSI.CFDI
{
  public class CFDIRegister
  {
    [XmlElement("Nacionalidad")]
    public string Nacionalidad { get; set; }

    [XmlElement("EntidadFederativa")]
    public string EntidadFederativa { get; set; }

    [XmlElement("IdOperacion")]
    public string IdOperacion { get; set; }

    [XmlElement("FechaHora")]
    public string FechaHora { get; set; }

    [XmlElement("NoCuenta")]
    public string NoCuenta { get; set; }

    [XmlElement("TipoOperacion")]
    public string TipoOperacion { get; set; }

    [XmlElement("TotalRecarga")]
    public string TotalRecarga { get; set; }

    [XmlElement("EmpresaA")]
    public string EmpresaA { get; set; }

    [XmlElement("EmpresaB")]
    public string EmpresaB { get; set; }

    [XmlElement("Impuesto1")]
    public string Impuesto1 { get; set; }

    [XmlElement("Impuesto2")]
    public string Impuesto2 { get; set; }

    [XmlElement("TotalRetiro")]
    public string TotalRetiro { get; set; }

    [XmlElement("Devolucion")]
    public string Devolucion { get; set; }

    [XmlElement("Premio")]
    public string Premio { get; set; }

    [XmlElement("ISR1")]
    public string ISR1 { get; set; }

    [XmlElement("ISR2")]
    public string ISR2 { get; set; }

    [XmlElement("RetiroEnEfectivo")]
    public string RetiroEnEfectivo { get; set; }

    [XmlElement("RetiroEnCheque")]
    public string RetiroEnCheque { get; set; }

    [XmlElement("RFC")]
    public string RFC { get; set; }

    [XmlElement("CURP")]
    public string CURP { get; set; }

    [XmlElement("Nombre")]
    public string Nombre { get; set; }

    [XmlElement("Domicilio")]
    public string Domicilio { get; set; }

    [XmlElement("Localidad")]
    public string Localidad { get; set; }

    [XmlElement("CodigoPostal")]
    public string CodigoPostal { get; set; }

    [XmlElement("Constancia.RFC")]
    public string ConstanciaRFC { get; set; }

    [XmlElement("Constancia.CURP")]
    public string ConstanciaCURP { get; set; }

    [XmlElement("Constancia.Nombre")]
    public string ConstanciaNombre { get; set; }

    [XmlElement("Constancia.Domicilio")]
    public string ConstanciaDomicilio { get; set; }

    [XmlElement("Constancia.Localidad")]
    public string ConstanciaLocalidad { get; set; }

    [XmlElement("Constancia.CodigoPostal")]
    public string ConstanciaCodigoPostal { get; set; }

    [XmlElement("Constancia")]
    public string Constancia { get; set; }

    [XmlElement("DocumentID")]
    public string DocumentID { get; set; }

    [XmlElement("RedondeoPorRetencion")]
    public string RedondeoPorRetencion { get; set; }

    [XmlElement("CargoPorServicio")]
    public string CargoPorServicio { get; set; }

    [XmlElement("RedondeoPorDecimales")]
    public string RedondeoPorDecimales { get; set; }

    [XmlElement("SesionCaja")]
    public string SesionCaja { get; set; }

    [XmlElement("PremioEspecie")]
    public string PremioEspecie { get; set; }

    [XmlElement("CashierName")]
    public string CashierName { get; set; }

    [XmlElement("PremioCaducado")]
    public string PremioCaducado { get; set; }

    [XmlElement("VoucherId")]
    public string VoucherId { get; set; }

    [XmlElement("Folio")]
    public string Folio { get; set; }

    [XmlElement("SaldoPromocional")]
    public string SaldoPromocional { get; set; }

    [XmlElement("ClaveTipoPago")]
    public string ClaveTipoPago { get; set; }

    [XmlElement("FormaPago")]
    public string FormaPago { get; set; }

    [XmlElement("RecargaPorPuntos")]
    public string RecargaPorPuntos { get; set; }

    [XmlElement("Fecha")]
    public string Fecha { get; set; }

    [XmlElement("TarjetaCliente")]
    public string TarjetaCliente { get; set; }

    [XmlElement("Nombres")]
    public string Nombres { get; set; }

    [XmlElement("Apellidos")]
    public string Apellidos { get; set; }

    [XmlElement("Shift_Id")]
    public string Shift_Id { get; set; }

    [XmlElement("Cajera_Id")]
    public string Cajera_Id { get; set; }

    [XmlElement("CajaNombres")]
    public string CajaNombres { get; set; }

    [XmlElement("CajaApellidos")]
    public string CajaApellidos { get; set; }

    [XmlElement("Caja_Id")]
    public string Caja_Id { get; set; }

    [XmlElement("Caja_Name")]
    public string Caja_Name { get; set; }

    [XmlElement("ClaveTipoPagoRecarga")]
    public string ClaveTipoPagoRecarga { get; set; }

    [XmlElement("TipoOperacionDescripcion")]
    public string TipoOperacionDescripcion { get; set; }

    [XmlElement("Folio2")]
    public string Folio2 { get; set; }

    [XmlElement("TotalRecargaSinImpuestos")]
    public string TotalRecargaSinImpuestos { get; set; }

    [XmlElement("BalanceAFichas")]
    public string BalanceAFichas { get; set; }

    [XmlElement("FichasABalance")]
    public string FichasABalance { get; set; }

    [XmlElement("CageCurrencyType")]
    public string CageCurrencyType { get; set; }

    [XmlElement("CurrencyIsoCode")]
    public string CurrencyIsoCode { get; set; }

    [XmlElement("PurchaseCurrency")]
    public string PurchaseCurrency { get; set; }

    [XmlElement("SalesCurrency")]
    public string SalesCurrency { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("UserType")]
    public string UserType { get; set; }
  }
}
