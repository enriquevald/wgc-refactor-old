﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: CFDI.cs
// 
//      DESCRIPTION: CFDI
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 05-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-OCT-2016 JBP    First release.
// 25-OCT-2016 JBP    PBI 19598:Constancias - Mejoras REVIEW Sprint 31
// 22-NOV-2016 JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using WSI.CFDI.oServicioReten;
using WSI.Common;

namespace WSI.CFDI
{
  public class CFDIProcessRegisters
  {
    #region " Enums "

    public enum TaxType
    {
      ISR = 1,
      IVA = 2,
      IEPS = 3
    }

    #endregion

    #region " Constants "

    private const string NACIONALITY_MX = "MX";
    private const string WS_RESULT_OK = "Correcto";
    private const string CFDI_DATA_INFO_FORMAT = "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};{21}";

    // SQl
    private const int SQL_COLUMN_XML = 0;
    private const int SQL_COLUMN_OPERATION_ID = 1;
    private const int SQL_COLUMN_RETRIES = 2;

    #endregion

    #region " Members "

    private static String m_user;
    private static String m_password;
    private static Int32 m_max_retries;
    private static String m_site;
    private static Int64 m_num_success;
    private static Int64 m_num_errors;
    private static Ws_CertConstRetencionesSoapClient m_service;

    #endregion
    
    #region " Public Methods "

    /// <summary>
    /// Process data
    /// </summary>
    /// <returns></returns>
    public static Boolean ProcessRegistersData()
    {
      DataTable _dt_constancies;
      String _space;
      
      try
      {
        _dt_constancies = new DataTable();

        // Initialize
        Initializaztion();

        // Get registers
        if (!SelectRegisters(out _dt_constancies))
        {
          Log.Message("CFDI Service: No registers to send");

          return true;
        }
        
        // Send registers
        SendRegisters(_dt_constancies);

        // Log info: Results.
        _space = "                      ";
        Log.Message(String.Format("CFDI Service Results:\n" +
                                    _space + "   - Success = {0}\n" +
                                    _space + "   - Error   = {1}\n" +
                                    _space + "   - Total   = {2}", m_num_success, m_num_errors, (m_num_success + m_num_errors)));

        return true;
        
      }
      catch(Exception ex)
      {
        Log.Error("CFDI Service: Error on ProcessRegistersData.");

        Log.Exception(ex);
      }

      return false;

    } // ProcessRegistersData

    /// <summary>
    /// Send registers to Web Service CFDI
    /// </summary>
    /// <param name="DtConstancias"></param>
    /// <returns></returns>
    private static void SendRegisters(DataTable DtConstancias)
    {
      String _xml;
      Int64 _operation_id;
      Int32 _retries;
      String _ws_result;
      String _data_info;

      try
      {
        // Log info: Start sending message
        Log.Message("CFDI Service: Sending registers...");

        // Process registers
        foreach (DataRow _row in DtConstancias.Rows)
        {
          // Check if service is principal
          if (!Services.IsPrincipal("WCP"))
          {
            continue;
          }

          // Get request data from SQL query
          _xml          = (String)_row[SQL_COLUMN_XML];
          _operation_id = (Int64)_row[SQL_COLUMN_OPERATION_ID];
          _retries       = (_row.IsNull(SQL_COLUMN_RETRIES)) ? 0 : (Int32)_row[SQL_COLUMN_RETRIES];

          // Format xml to data info result
          if (!FormatXMLToDataInfo(_xml, out _data_info))
          {
            m_num_errors++;

            continue;
          }

          // Send request to WS
          _ws_result = m_service.RetenPremios(m_user, m_password, _data_info);

          // Send request 
          if (_ws_result == WS_RESULT_OK)
          {
            UpdateRegister_Success(_operation_id, _retries, _data_info);
          }
          else
          {
            UpdateRegister_Error(_operation_id, _retries, _data_info, _ws_result);
          }
        }
      }
      catch(Exception _ex)
      {
        Log.Error("CFDI Service: Error on ProcessRegistersData.");

        Log.Exception(_ex);
      }
    } // SendRegisters

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Initialize global variables
    /// </summary>
    private static void Initializaztion()
    {      
      // Initialize counters
      m_num_success = 0;
      m_num_errors  = 0;

      // Get Credentials
      m_user        = GetUser();
      m_password    = GetPassword();
      m_site        = GetSite();
      m_max_retries  = GetRetries();

      // Initialize Service
      InitializeCFDIService();

    } // Initializaztion
    
    /// <summary>
    /// Initialize CFDI service
    /// </summary>
    /// <returns></returns>
    private static void InitializeCFDIService()
    {
      String _url;
      EndpointAddress _endpoint;

      _url = GetServiceUri();
      _endpoint = new EndpointAddress(_url);

      m_service = new Ws_CertConstRetencionesSoapClient(new BasicHttpBinding(), _endpoint);

    } // InitializeCFDIService

    /// <summary>
    /// Format XML to Data info
    /// </summary>
    /// <param name="Xml"></param>
    /// <returns></returns>
    private static Boolean FormatXMLToDataInfo(String Xml, out String Result)
    {     
      CFDIRegister _register;
      XmlSerializer _serializer;

      _register = new CFDIRegister();
      _serializer = new XmlSerializer(typeof(CFDIRegister));
      
      // Deserialize XML to CFDIRegister
      if (!String.IsNullOrEmpty(Xml))
      {
        using (TextReader _reader = new StringReader(Xml))
        {
          _register = (CFDIRegister)_serializer.Deserialize(_reader);
        }
      }

      // Test mode
      if (CFDIThread.IsTestMode())
      {
        return SetDataInfo_TEST(_register, out Result);
      }
      
      // Format CFDIRegister to WS Data required
      if (!SetDataInfoFromXML(_register, out Result))
      {
        return false;
      }

      return true;
    } // FormatXMLToDataInfo

    /// <summary>
    /// Set data info to string format (ONLY TEST)
    /// </summary>
    /// <param name="Register"></param>
    /// <param name="Result"></param>
    /// <returns></returns>
    private static Boolean SetDataInfo_TEST(CFDIRegister Register, out String Result)
    {
      CFDIRegister _register;
      String _entidad_federativa;
      Random _rnd;

      _register = new CFDIRegister();

      _rnd = new Random(Environment.TickCount);
      _entidad_federativa = _rnd.Next(1, 32).ToString().PadLeft(2, '0');

      // Fake Data
      _register.Nacionalidad       = NACIONALITY_MX;
      _register.RFC                = (String.IsNullOrEmpty(Register.RFC))     ? "JICF780517KX4"                 : Register.RFC;
      _register.Nombre             = (String.IsNullOrEmpty(Register.Nombre))  ? "CAGE A SEGUNDO CAGE A CAGE A"  : Register.Nombre;
      _register.CURP               = Register.CURP;
      _register.Fecha              = (String.IsNullOrEmpty(Register.Fecha))   ? "22/06/2016 9:00:00"            : Register.Fecha;
      _register.ISR1               = (String.IsNullOrEmpty(Register.ISR1))    ? "6.0000"                        : Register.ISR1;
      _register.ISR2               = (String.IsNullOrEmpty(Register.ISR2))    ? "6.0000"                        : Register.ISR2;
      _register.Premio             = (String.IsNullOrEmpty(Register.Premio))  ? "57000.0000"                    : Register.Premio;
      _register.EntidadFederativa  = _entidad_federativa;

      // Set result data
      if (!SetDataInfoFromXML(_register, out Result))
      {
        return false;
      }

      //Log.Message("SetDataInfo TEST: " + Result);

      return true;
    } // SetDataInfo_TEST

    /// <summary>
    /// Set data info to string format
    /// </summary>
    /// <param name="Register"></param>
    /// <returns></returns>
    private static Boolean SetDataInfoFromXML(CFDIRegister Register, out String Result)
    {
      DateTime _date;
      String _mes;
      String _ano;
      Decimal _cobrado;
      String _tax_type;
      Decimal _isr1;
      Decimal _isr2;
      String _name;
      String _nationality;
      String _rfc;
      String _entidad_federativa;

      Result = string.Empty;
      _entidad_federativa = String.Empty;

      // Format ISR1
      //if ( !AmountStringToDecimal(Register.ISR1, out _isr1) )
      if (!Decimal.TryParse(Register.ISR1, out _isr1))
      {
        Log.Message(String.Format("CFDI Service: Incorrect 'ISR1' of register with 'IdOperacion' = {0}.", Register.IdOperacion));

        return false;
      }
      
      // Format ISR2
      //if ( !AmountStringToDecimal(Register.ISR2, out _isr2) )
      if (!Decimal.TryParse(Register.ISR2, out _isr2))
      {
        Log.Message(String.Format("CFDI Service: Incorrect 'ISR2' of register with 'IdOperacion' = {0}.", Register.IdOperacion));

        return false;
      }

      // Format Date      
      if (!DateTime.TryParseExact(Register.Fecha, "MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out _date))
      {
        Log.Message(String.Format("CFDI Service: Incorrect 'Fecha' of register with 'IdOperacion' = {0}.", Register.IdOperacion));

        return false;
      }

      // Set data
      _mes        = _date.Month.ToString();
      _ano        = _date.Year.ToString();
      _cobrado    = (_isr1 + _isr2);
      _tax_type   = ((int)TaxType.ISR).ToString().PadLeft(2, '0');

      // Get/Set special data.
      GetSpecialHolderData(Register, out _nationality, out _name, out _rfc, out _entidad_federativa);

      //----------------------------------------------------------------------------------------------------------------
      //-----------------------              DATA                           --  PDF:Manual-WS-RetencionesPremios.pdf  --
      Result =  String.Format ( CFDI_DATA_INFO_FORMAT                       // #STRING CHAIN FORMAT                   --
                              , _nationality                                // Nacionalidad                           --
                              , _rfc                                        // RFC                                    --
                              , _name                                       // Razón Social o Nombre                  --
                              , Register.CURP                               // CURP                                   --
                              , String.Empty                                // Número de Registro ID                  --
                              , _mes                                        // Inicio de periodo (mes inicio)         --
                              , _mes                                        // Fin de periodo (mes final              --
                              , _ano                                        // Ejercicio. (Solo de 2004 a 2024)       --
                              , AmountToInvariantCulture(Register.Premio)   // Base Retención                         --
                              , _tax_type                                   // Tipo de Impuesto                       --
                              , AmountToInvariantCulture(_cobrado)          // Monto Retenido                         --
                              , "PAGO DEFINITIVO"                           // Tipo Pago de la Retención              --
                              , AmountToInvariantCulture(Register.Premio)   // Total de la Operación                  --
                              , AmountToInvariantCulture(Register.Premio)   // Total Gravado                          --
                              , "0.00"                                      // Total Exento                           --
                              , AmountToInvariantCulture(_cobrado)          // Total Retenido                         --
                              , _entidad_federativa                         // Entidad Federativa                     --
                              , AmountToInvariantCulture(Register.Premio)   // Total de Pago                          --
                              , AmountToInvariantCulture(Register.Premio)   // Total de Pago Gravado                  --
                              , "0.00"                                      // Total de Pago Exento                   --
                              , m_site                                      // Cadena o número que identifica la sala --
                              , Register.Email);                            // Email                                  --
      //----------------------------------------------------------------------------------------------------------------

      return true;

    } // SetDataInfoFromXML

    /// <summary>
    /// Special Data manager
    /// </summary>
    /// <param name="Register"></param>
    /// <param name="Nationality"></param>
    /// <param name="Name"></param>
    /// <param name="RFC"></param>
    /// <param name="EntidadFederativa"></param>
    private static void GetSpecialHolderData(CFDIRegister Register, out String Nationality, out String Name, out String RFC, out String EntidadFederativa)
    {
      // Default values
      Nationality       = Register.Nacionalidad;
      Name              = Register.Nombre;
      RFC               = Register.RFC;
      EntidadFederativa = Register.EntidadFederativa;


      // Anonymous
      if (Register.UserType == "0") 
      {
        Name  = " ";
        RFC   = GetDefaultRFC_Anonymous();

        if (String.IsNullOrEmpty(Register.EntidadFederativa))
        {
          EntidadFederativa = GetDefaultEF_Anonymous();
        }
      }
      // Foreing
      else if (Register.Nacionalidad != NACIONALITY_MX)
      {
        // Set default RFC if RFC is null or empty
        if (String.IsNullOrEmpty(RFC))
        {
          RFC = GetDefaultRFC_Foreign();
        }

        if (String.IsNullOrEmpty(Register.EntidadFederativa))
        {
          EntidadFederativa = GetDefaultEF_Foreign();
        }
      }

      // Set default RFC value
      if (String.IsNullOrEmpty(RFC))
      {
        RFC = GetDefaultRFC_Anonymous();
      }

      // Format Entidad federativa
      if (!String.IsNullOrEmpty(EntidadFederativa))
      {
        EntidadFederativa = EntidadFederativa.PadLeft(2, '0');
      }

    } // GetSpecialHolderData

    /// <summary>
    /// Select registers from CFDI_REGISTERS
    /// </summary>
    /// <param name="Constancias"></param>
    /// <returns></returns>
    private static Boolean SelectRegisters(out DataTable Constancias)
    {
      String _select;

      Constancias = new DataTable();

      // Get registers to send
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _select = GetSelectQuery();

        using (SqlCommand _sql_cmd = new SqlCommand(_select, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Constancias);
          }
        } // SqlCommand
      }

      return (Constancias.Rows.Count > 0);
    } // SelectRegisters

    /// <summary>
    /// Update register with success result
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Retries"></param>
    /// <param name="ReportedData"></param>
    private static void UpdateRegister_Success(Int64 OperationId, Int32 Retries, String ReportedData)
    {
      UpdateRegisterInternal(OperationId, CFDIThread.CFDIStatus.Send, Retries, ReportedData, 0, String.Empty);
    } // UpdateRegister_Success

    /// <summary>
    /// Update register with error result
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Retries"></param>
    /// <param name="ReportedData"></param>
    /// <param name="Result"></param>
    private static void UpdateRegister_Error(Int64 OperationId, Int32 Retries, String ReportedData, String Result)
    {
      CFDIThread.CFDIStatus _status;
      Int32 _error_code;
      String _error_message;

      // Set status
      _status = (++Retries >= m_max_retries) ? CFDIThread.CFDIStatus.Error : CFDIThread.CFDIStatus.Retries;

      // Format error
      FormatErrorCode(Result, out _error_code, out _error_message);

      // Update register
      UpdateRegisterInternal(OperationId, _status, Retries, ReportedData, _error_code, _error_message);

      Log.Message(String.Format("CFDI Service: Error on send register with OperationId = {0} - Error Message: {1} ", OperationId, _error_message));

    } // UpdateRegister_Error

    /// <summary>
    /// Update register
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Status"></param>
    /// <param name="Retries"></param>
    /// <param name="ReportedData"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="ErrorMessage"></param>
    private static void UpdateRegisterInternal(Int64 OperationId, CFDIThread.CFDIStatus Status, Int32 Retries, String ReportedData, Int32 ErrorCode, String ErrorMessage)
    {
      String _update;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _update = GetUpdateQuery();

        using (SqlCommand _sql_cmd = new SqlCommand(_update, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pStatus"        , SqlDbType.Int).Value      = (Int32)Status;
          _sql_cmd.Parameters.Add("@pRetries"       , SqlDbType.Int).Value      = Retries;
          _sql_cmd.Parameters.Add("@pErrorCode"     , SqlDbType.Int).Value      = ErrorCode;
          _sql_cmd.Parameters.Add("@pErrorMessage"  , SqlDbType.VarChar).Value  = ErrorMessage;
          _sql_cmd.Parameters.Add("@pOperationId"   , SqlDbType.BigInt).Value   = OperationId;
          _sql_cmd.Parameters.Add("@pReportedData"  , SqlDbType.Text).Value     = ReportedData;
          _sql_cmd.Parameters.Add("@pUpdated"       , SqlDbType.DateTime).Value = WGDB.Now;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("CFDI Service: Error on UpdateRegisterInternal. OperationId: " + OperationId.ToString());
          }

          _db_trx.Commit();

        } // SqlCommand
      }

      // Update log counters
      if (Status == CFDIThread.CFDIStatus.Send)
      {
        m_num_success++;
      }
      else
      {
        m_num_errors++;
      }
    } // UpdateRegisterInternal
    
    /// <summary>
    /// Process result for obtain error code and error message
    /// </summary>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private static void FormatErrorCode(String Error, out Int32 _error_code, out String _error_message)
    {
      String[] _error;

      // Default values
      _error_code = 9999;
      _error_message = "Undefined error";

      if (String.IsNullOrEmpty(Error))
      {
        return;
      }

      _error = Error.Split(' ');

      if (_error.Length == 0)
      {
        return;
      }

      // Set Error code & error message
      if (Int32.TryParse(_error[0], out _error_code))
      {
        _error_message = Error.Replace(_error[0], "").Trim();
      }

    } // FormatErrorCode

    /// <summary>
    /// Get SQL Update query
    /// </summary>
    /// <returns></returns>
    private static String GetUpdateQuery()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Update constancies registers
      _sb.Length = 0;
      _sb.AppendLine("  UPDATE	  CFDI_REGISTERS                      ");
      _sb.AppendLine("     SET   	CR_STATUS         = @pStatus        ");
      _sb.AppendLine("        	, CR_RETRIES        = @pRetries       ");
      _sb.AppendLine("        	, CR_ERROR_CODE     = @pErrorCode     ");
      _sb.AppendLine("        	, CR_ERROR_MESSAGE  = @pErrorMessage  ");
      _sb.AppendLine("        	, CR_REPORTED_DATA  = @pReportedData  ");
      _sb.AppendLine("        	, CR_UPDATED        = @pUpdated       ");
      _sb.AppendLine("   WHERE    CR_OPERATION_ID   = @pOperationId   ");

      return _sb.ToString();

    } // GetUpdateQuery

    /// <summary>
    /// Get SQL Select query
    /// </summary>
    /// <returns></returns>
    private static String GetSelectQuery()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      // Select constancies registers
      _sb.Length = 0;
      _sb.AppendLine("  SELECT	  CR_XML_REQUEST                ");
      _sb.AppendLine("        	, CR_OPERATION_ID               ");
      _sb.AppendLine("        	, CR_RETRIES                    ");
      _sb.AppendLine("    FROM    CFDI_REGISTERS                ");
      _sb.AppendLine("   WHERE    CR_STATUS = 0   -- Pendiente  ");
      _sb.AppendLine("  	  OR    CR_STATUS = 2   -- Retry      ");

      return _sb.ToString();

    } // GetSelectQuery
    
    /// <summary>
    /// Convert amount to String with invariant culture
    /// </summary>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private static String AmountToInvariantCulture(String Amount)
    {
      Decimal _amount;

      if (!Decimal.TryParse (Amount, out _amount) )
      {
        Log.Message(String.Format("CFDI Service: Error on AmountToInvariantCulture. Can't convert string amount '{0}' to decimal.", Amount));

        return Amount;
      }

      return AmountToInvariantCulture(_amount);
    }
    private static String AmountToInvariantCulture(Decimal Amount)
    {
      return Amount.ToString("0.00", CultureInfo.InvariantCulture);
    } // AmountToInvariantCulture
    
    #region " General Params "

    /// <summary>
    /// Get GP CFDI.User
    /// </summary>
    /// <returns></returns>
    private static String GetUser()
    {
      return GeneralParam.GetString("CFDI", "User");
    } // GetUser

    /// <summary>
    /// Get GP CFDI.Password
    /// </summary>
    /// <returns></returns>
    private static String GetPassword()
    {
      return GeneralParam.GetString("CFDI", "Password");
    } // GetPassword 

    /// <summary>
    /// Get GP CFDI.Retries
    /// </summary>
    /// <returns></returns>
    private static Int32 GetRetries()
    {
      return GeneralParam.GetInt32("CFDI", "Retries", 5);
    } // GetRetries

    /// <summary>
    /// Get GP CFDI.Uri
    /// </summary>
    /// <returns></returns>
    private static String GetServiceUri()
    {
      return GeneralParam.GetString("CFDI", "Uri", "http://201.99.72.63/appConstancias/Ws_CertConstRetenciones.asmx");
    } // GetServiceUri
    
    /// <summary>
    /// Get GP CFDI.Site
    /// </summary>
    /// <returns></returns>
    private static String GetSite()
    {
      return GeneralParam.GetString("CFDI", "Site", String.Empty);
    } // GetSite

    /// <summary>
    /// Get GP CFDI.Anonymous.RFC
    /// </summary>
    /// <returns></returns>
    private static String GetDefaultRFC_Anonymous()
    {
      return GeneralParam.GetString("CFDI", "Anonymous.RFC", String.Empty);
    } // GetDefaultRFC_Anonymous

    /// <summary>
    /// Get GP CFDI.Anonymous.EntidadFederativa
    /// </summary>
    /// <returns></returns>
    private static String GetDefaultEF_Anonymous()
    {
      return GeneralParam.GetString("CFDI", "Anonymous.EntidadFederativa", String.Empty);
    } // GetDefaultEF_Anonymous

    /// <summary>
    /// Get GP CFDI.Foreing.RFC
    /// </summary>
    /// <returns></returns>
    private static String GetDefaultRFC_Foreign()
    {
      return GeneralParam.GetString("CFDI", "Foreign.RFC", String.Empty);
    } // GetDefaultRFC_Foreign

    /// <summary>
    /// Get GP CFDI.Foreing.EntidadFederativa
    /// </summary>
    /// <returns></returns>
    private static String GetDefaultEF_Foreign()
    {
      return GeneralParam.GetString("CFDI", "Foreign.EntidadFederativa", String.Empty);
    } // GetDefaultEF_Foreign

    #endregion

    #endregion

  } // End CFDIProcessRegisters
}
