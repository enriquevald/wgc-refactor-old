//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Program.cs
// 
//   DESCRIPTION : Generate excel ReportsTool
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 AMF    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WSI.ReportsTool_Cage
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new frm_generate());
    }
  }
}