using System.Windows.Forms;
namespace WSI.ReportsTool_Cage
{
  partial class frm_login
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_username_name = new System.Windows.Forms.TextBox();
      this.txt_username_password = new System.Windows.Forms.TextBox();
      this.btn_ok = new System.Windows.Forms.Button();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.lbl_username_name = new System.Windows.Forms.Label();
      this.lbl_username_password = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // txt_username_name
      // 
      this.txt_username_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_username_name.Font = new System.Drawing.Font("Arial", 14.25F);
      this.txt_username_name.Location = new System.Drawing.Point(95, 20);
      this.txt_username_name.MaxLength = 15;
      this.txt_username_name.Name = "txt_username_name";
      this.txt_username_name.Size = new System.Drawing.Size(251, 29);
      this.txt_username_name.TabIndex = 0;
      // 
      // txt_username_password
      // 
      this.txt_username_password.Font = new System.Drawing.Font("Arial", 14.25F);
      this.txt_username_password.Location = new System.Drawing.Point(95, 64);
      this.txt_username_password.MaxLength = 22;
      this.txt_username_password.Name = "txt_username_password";
      this.txt_username_password.PasswordChar = '*';
      this.txt_username_password.Size = new System.Drawing.Size(251, 29);
      this.txt_username_password.TabIndex = 1;
      // 
      // btn_ok
      // 
      this.btn_ok.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_ok.Location = new System.Drawing.Point(393, 49);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(90, 30);
      this.btn_ok.TabIndex = 2;
      this.btn_ok.Text = "Aceptar";
      this.btn_ok.UseVisualStyleBackColor = true;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_cancel.Location = new System.Drawing.Point(393, 85);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(90, 30);
      this.btn_cancel.TabIndex = 3;
      this.btn_cancel.Text = "Cancelar";
      this.btn_cancel.UseVisualStyleBackColor = true;
      // 
      // lbl_username_name
      // 
      this.lbl_username_name.AutoSize = true;
      this.lbl_username_name.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_username_name.Location = new System.Drawing.Point(10, 26);
      this.lbl_username_name.Name = "lbl_username_name";
      this.lbl_username_name.Size = new System.Drawing.Size(74, 22);
      this.lbl_username_name.TabIndex = 4;
      this.lbl_username_name.Text = "Usuario";
      // 
      // lbl_username_password
      // 
      this.lbl_username_password.AutoSize = true;
      this.lbl_username_password.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_username_password.Location = new System.Drawing.Point(10, 67);
      this.lbl_username_password.Name = "lbl_username_password";
      this.lbl_username_password.Size = new System.Drawing.Size(58, 22);
      this.lbl_username_password.TabIndex = 5;
      this.lbl_username_password.Text = "Clave";
      // 
      // frm_login
      // 
      this.AcceptButton = this.btn_ok;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ClientSize = new System.Drawing.Size(488, 127);
      this.Controls.Add(this.lbl_username_password);
      this.Controls.Add(this.lbl_username_name);
      this.Controls.Add(this.btn_cancel);
      this.Controls.Add(this.btn_ok);
      this.Controls.Add(this.txt_username_password);
      this.Controls.Add(this.txt_username_name);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "frm_login";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Reports Tool - Login de usuario";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_login_FormClosed);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txt_username_name;
    private System.Windows.Forms.TextBox txt_username_password;
    private System.Windows.Forms.Button btn_ok;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.Label lbl_username_name;
    private System.Windows.Forms.Label lbl_username_password;
  }
}