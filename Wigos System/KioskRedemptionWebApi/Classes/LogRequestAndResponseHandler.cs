﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LogRequestAndResponseHandler.cs
// 
//   DESCRIPTION: LogRequestAndResponseHandler class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 23-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-AUG-2017 DPC    First version.
// 19-OCT-2017  ETP    WIGOS-5286 WRKP TITO - Service
//------------------------------------------------------------------------------

using KioskRedemption.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using KioskRedemption.Classes.Response;
using KioskRedemption.Classes.Request;
using System.IO;

namespace KioskRedemptionWebApi.Classes
{
  public class LogRequestAndResponseHandler : DelegatingHandler
  {
    /// <summary>
    /// Create a log with request and response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="CancellationToken"></param>
    /// <returns></returns>
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage Request, CancellationToken CancellationToken)
    {
      Int32 _tick;
      Int32 _kiosk_id;
      String _request;
      String _response;
      LogRequest _log_request;
      LogResponse _log_response;
      HttpResponseMessage _result;
      Dictionary<String, String> _header_request;
      Dictionary<String, String> _header_response;

      _kiosk_id = -1;
      _response = String.Empty;
      _tick = Environment.TickCount;
      _log_request = new LogRequest();
      _log_response = new LogResponse();
      _header_request = new Dictionary<String, String>();
      _header_response = new Dictionary<String, String>();

      //Get body request
      _request = await Request.Content.ReadAsStringAsync();

      //Get headers request
      foreach (var _header in Request.Headers)
      {
        foreach (String _value in _header.Value)
        {
          if (_header_request.ContainsKey(_header.Key.ToString().ToLower()))
            _header_request[_header.Key.ToString().ToLower()] += String.Format(",{0}", _value);
          else
            _header_request.Add(_header.Key.ToString().ToLower(), _value);
        }
      }

      //Make the request
      _result = await base.SendAsync(Request, CancellationToken);

      if (_result.Content != null)
      {
        //Get the body response
        _response = await _result.Content.ReadAsStringAsync();
      }

      //Get headers response
      foreach (var _header in _result.Headers)
      {
        foreach (String _value in _header.Value)
        {
          if (_header_request.ContainsKey(_header.Key.ToString().ToLower()))
            _header_request[_header.Key.ToString().ToLower()] += String.Format(",{0}",_value);          
          else
            _header_request.Add(_header.Key.ToString().ToLower(), _value);
        }
      }

      //Get Kiosk redemption id
      if (_header_request.ContainsKey("kioskid"))
      {
        Int32.TryParse(_header_request["kioskid"], out _kiosk_id);

        _kiosk_id = (_kiosk_id == 0 ? -1 : _kiosk_id);
      }

      //Make the request log object
      _log_request.Url = Request.RequestUri.AbsoluteUri;
      _log_request.Headers = _header_request;
      _log_request.Method = Request.Method.ToString();
      _log_request.Body = JsonConvert.DeserializeObject(_request);

      //Make the response log object
      _log_response.HttpCode = (Int32)_result.StatusCode;
      _log_response.Headers = _header_response;
      _log_response.Body = JsonConvert.DeserializeObject(_response);

      //Save log
      BusinessLogic.ManageLog(JsonConvert.SerializeObject(_log_request), JsonConvert.SerializeObject(_log_response), _kiosk_id, Environment.TickCount - _tick);

      return _result;
    } // SendAsync
  } // LogRequestAndResponseHandler
}
