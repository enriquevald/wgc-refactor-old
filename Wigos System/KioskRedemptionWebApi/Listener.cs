﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Listener.cs
// 
//   DESCRIPTION: Listener class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 23-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using KioskRedemptionWebApi.Classes;
using Microsoft.Web.Http.Routing;
using Microsoft.Web.Http.Versioning;
using Newtonsoft.Json;
using System;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Http.SelfHost;
using WSI.Common;

namespace KioskRedemptionWebApi
{
  public class Listener
  {
    /// <summary>
    /// Listener for wake up the WebApi
    /// </summary>
    public Listener()
    {
      String _uri;
      HttpSelfHostServer _server; 
      HttpSelfHostConfiguration _config;

      try
      {
      _uri = GeneralParam.GetString("CountR", "WebApi.Uri", "http://0.0.0.0:8082");

        _config = new HttpSelfHostConfiguration(_uri);

        _config.AddApiVersioning(options =>
        {
          options.ApiVersionReader = new QueryStringApiVersionReader();
          options.AssumeDefaultVersionWhenUnspecified = true;
          options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
        });

        var constraintResolver = new DefaultInlineConstraintResolver();
        constraintResolver.ConstraintMap.Add("apiVersion", typeof(ApiVersionRouteConstraint));
            
        _config.MapHttpAttributeRoutes(constraintResolver);

      _config.Routes.MapHttpRoute(
          "API Default", "api/{controller}/{action}/{id}",
            new
            {
              id = RouteParameter.Optional
      });

      _config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings();

      _config.MessageHandlers.Add(new LogRequestAndResponseHandler());

      _server = new HttpSelfHostServer(_config);

      _server.OpenAsync().Wait();

        Log.Message("Kiosk redemption WebApi Start. Uri: " + _uri);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }
  } // Listener
} 
