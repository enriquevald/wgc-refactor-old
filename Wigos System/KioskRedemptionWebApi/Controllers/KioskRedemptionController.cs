﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: KioskRedemptionController.cs
// 
//   DESCRIPTION: KioskRedemptionController class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 23-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using KioskRedemption.Classes.Request;
using KioskRedemption.Classes.Response;
using System;
using System.Net;
using System.Web.Http;
using WSI.Common;
using WSI.Common.CountR;

namespace KioskRedemptionWebApi.Controllers
{
  public class KioskRedemptionController : BaseController
  {

    #region " v1 "

    #region " GET "

    /// <summary>
    /// Get ticket
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpGet]
    [Route("v1/ticket/{id}")]
    public IHttpActionResult GetTicket(String Id)
    {
      TicketDataReponse _response;
      HttpStatusCode _status;

      try
      {
        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.GetRedemptionTicketData(Id, out _response, out _status);
        
        return CreateResponse(_status, _response);
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // GetTicket

    #endregion " GET "

    #region " POST "

    /// <summary>
    /// Pay the ticket
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("v1/payment")]
    public IHttpActionResult Payment(PaymentRequest Request)
    {
      PaymentResponse _response;
      HttpStatusCode _status;

      try
      {
        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.PaymentTicket(Request, out _response, out _status);

        return CreateResponse(_status,_response);

      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // Payment

    /// <summary>
    /// Create a ticket
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("v1/ticket")]
    public IHttpActionResult CreateTicket(CreateTicketRequest Request)
    {
      CreateTicketResponse _response;
      HttpStatusCode _status;

      try
      {
        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.CreateTicket(Request, out _response, out _status);

        return CreateResponse(_status, _response);
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // CreateTicket

    /// <summary>
    /// Create event
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("v1/event")]
    public IHttpActionResult SendEvent(EventRequest Request)
    {
      EventResponse _response;
      HttpStatusCode _status;

      try
      {
        _response = new EventResponse();

        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.SendEvent(Request, out _response, out _status);

        return CreateResponse(_status, _response);
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // Event

    #endregion " POST "

    #region " PUT "

    /// <summary>
    /// Get kiosk status
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    [HttpPut]
    [Route("v1/kioskstatus")]
    public IHttpActionResult KioskStatus(KioskStatusRequest Request)
    {
      KioskStatusResponse _response;
      HttpStatusCode _status;

      try
      {
        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.GetRedemptionKioskStatus(Request, out _response, out _status);
        
        return CreateResponse(_status, _response);
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // KioskStatus

    /// <summary>
    /// Transaction action
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpPut]
    [Route("v1/transaction")]
    public IHttpActionResult Transaction(TransactionRequest Request)
    {
      TransactionResponse _response;
      HttpStatusCode _status;

      try
      {
        if (!CheckHeaderRequest() || Request == null)
        {
          return CreateResponse(HttpStatusCode.BadRequest);
        }

        this.BusinessLogic.Transaction(Request, out _response, out _status);

        return CreateResponse(_status, _response);
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.ToString());
      }

      return CreateResponse(HttpStatusCode.InternalServerError);
    } // Transaction

    #endregion " PUT "

    #endregion " v1 "

  }
} // KioskRedemptionController
