﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BaseController.cs
// 
//   DESCRIPTION: BaseController class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 23-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Web.Http;
using System.Web.Http.Results;
using KioskRedemption.Business;
using System.Collections.Generic;

namespace KioskRedemptionWebApi.Controllers
{
  public class BaseController : ApiController
  {

    #region " Members "

    private BusinessLogic m_business_logic;

    #endregion " Members "

    #region " Properties "

    public BusinessLogic BusinessLogic
    {
      get
      {
        if (m_business_logic == null)
        {
          m_business_logic = new BusinessLogic()
          {
            KioskId = this.KioskId,
            Provider = this.Provider,
            IP = this.IP
          };          
        }

        return m_business_logic;
      }
    }
    public String IP
    {
      get
      {
        return GetClientIp(Request);
      }
    }
    public Int32 KioskId
    {
      get
      {
        if (GetHeaderValue("KioskId") != null)
        {
          Int32.Parse(Request.Headers.GetValues("KioskId").FirstOrDefault());
        }

        return Int32.Parse(Request.Headers.GetValues("KioskId").FirstOrDefault());
      }
    }
    public String Provider
    {
      get
      {
        return Request.Headers.GetValues("Provider").FirstOrDefault();
      }
    }
    public String RequestId
    {
      get
      {
        return Request.Headers.GetValues("RequestId").FirstOrDefault();
      }
    }

    #endregion " Properties "

    #region " Public methods "
    
    /// <summary>
    /// Create response to client
    /// </summary>
    /// <param name="StatusCode"></param>
    /// <param name="Body"></param>
    /// <returns></returns>
    public ResponseMessageResult CreateResponse(HttpStatusCode StatusCode, Object Body = null)
    {
      HttpResponseMessage _response;

      try
      {
        _response = new HttpResponseMessage();

        if (StatusCode == HttpStatusCode.OK && Body != null)
        {
            _response = Request.CreateResponse(StatusCode, Body);
        }
        else
        {
          _response = Request.CreateResponse(StatusCode);
        }       

        _response.Headers.Add("RequestId", this.RequestId);

        return new ResponseMessageResult(_response);
      }
      catch (Exception _ex)
      {
        throw _ex;
      }
    } // CreateResponse

    /// <summary>
    /// Get ip from client
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public string GetClientIp(HttpRequestMessage Request)
    {
      RemoteEndpointMessageProperty _remote;

      if (Request.Properties[RemoteEndpointMessageProperty.Name] != null)
      {
        _remote = Request.Properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

        if (_remote != null)
        {
          return _remote.Address;
        }
      }

      return String.Empty;
    } // GetClientIp

    /// <summary>
    /// Check headers request
    /// </summary>
    /// <returns></returns>
    public Boolean CheckHeaderRequest()
    {
      if (GetHeaderValue("KioskId") == null || GetHeaderValue("KioskId") == String.Empty || !GetHeaderValue("KioskId").All(char.IsNumber))
      {
        return false;
      }

      if (GetHeaderValue("Provider") == null || GetHeaderValue("Provider") == String.Empty)
      {
        return false;
      }

      if (GetHeaderValue("RequestId") == null || GetHeaderValue("RequestId") == String.Empty)
      {
        return false;
      }
      
      return true;
    } // CheckHeaderRequest

    /// <summary>
    /// Get header, if don't exist return null
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public String GetHeaderValue(String name)
    {
      IEnumerable<String> values;
      
      if (Request.Headers.TryGetValues(name, out values))
      {
        return values.FirstOrDefault();
      }

      return null;
    } // GetHeaderValue

    #endregion " Public methods "

  } // BaseController
}