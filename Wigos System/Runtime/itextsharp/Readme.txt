File:

	* itextsharp.dll: Library for create and manipulate pdf and also it use for fill fields (version 4.0.4).


Used in WGC Kernel/WSI.Common/TemplatePDF.cs

The Version 5.0.0 and up is licensed under the AGPL (strong copyleft).
The Version 4.1.6 and previous are still licensed under the MPL/LGPL (weak copyleft).
So for a commercial product you'll almost certainly want to stick with 4.1.6 or previous.




