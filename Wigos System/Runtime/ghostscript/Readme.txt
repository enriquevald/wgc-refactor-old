Files in the directory:

	* gsdll32.dll  --> Library ghostscript.
	* gswin32c.exe --> Ghostscript: Ghostscript is an interpreter for PostScript™ and Portable Document Format (PDF) files.
	* gsprint.exe  --> gsprint: It Prints pdf's (and ps) under several parameters.

For more information you view WSI Kernel/WSI.Common/GhostScript.cs

Documentation ghostscript: http://ghostscript.com/doc/current/Readme.htm
Documentation gsprint: http://pages.cs.wisc.edu/~ghost/gsview/gsprint.htm.

--------------------
------GSPRINT-------
--------------------

GSPRINT uses Ghostscript to print a PostScript file to a Windows printer. Using Ghostscript alone you can print using a number of inbuilt printer drivers for specific printers (e.g. djet500, ljet4), or you can use the driver mswinpr2 which talks to the Windows printer driver. The mswinpr2 driver (which I wrote) is very slow for printers that claim to support colour. GSPRINT tells Ghostscript to produce a bitmap, and then sends this to the Windows printer driver more efficiently than the old mswinpr2.
GSPRINT requires AFPL Ghostscript 6.50 or later.

--------------------
Usage
--------------------

Typical usage would be

    gsprint golfer.ps
    gsprint -colour colorcir.ps 

You may need to configure gsprint with a gsprint.cfg file. See the configuration section below.

Command line options are:

    -help
        Print usage summary
    -mono
        Render in monochrome as 1bit/pixel
    -grey or -gray
        Render in greyscale as 8bits/pixel
    -colour or -color
        Render in colour as 24bits/pixel
    -query
        Show printer setup dialog
    -noquery
        Don't show printer setup dialog
    -printer "name"
        Print to the specified printer
    -noprinter
        Use default printer
    -port "name"
        Print to the specified printer port, instead of the default for the printer
    -ghostscript "name"
        Path and filename of command line Ghostscript
    -config "name"
        Read options from this file, one argument per line.
    -odd
        Print only odd pages
    -even
        Print only even pages
    -all
        Print all pages
    -from NN
        First page to print is NN
    -to NN
        Last page to print is NN
    -twoup
        Two pages per sheet
    -portrait
        Portrait orientation
    -landscape
        Landscape orientation
    -duplex_vertical
        Duplex (for long edge binding)
    -duplex_horizontal
        Duplex (for short edge binding)
    -copies NN
        Print NN copies (if supported by Windows printer driver)
    "filename"
        The PostScript/PDF file to print
    -
        Tell Ghostscript to read PostScript from standard input

Defaults are -mono -noquery -noprinter -all
Unrecognised options are passed on to Ghostscript.
The filename to print must be the last option.

If neither -query nor -printer are used (the default), the default printer will be used. This is equivalent to -noquery and -noprinter.

-all overrides -odd, -even, -from and -to.
DSC comments are ignored.
-odd, -even, -from and -to operate by rendering every page but only sending the selected pages to the printer.
Printing one page out of a 200 page document using "-from 100 -to 100" will result in a long wait before and after the page is printed.
If using "-from 10 -odd", the first page printed will be 11.

The last occurence of -mono, -grey, -colour is used.

Page size and margins cannot be set from the command line. These are obtained from the printer driver. To change the page size, change your printer settings.

Ghostscript options that must not be used are:

  -r  
  -g  
  -sDEVICE=  
  -sOutputFile=  
  -sPAPERSIZE=

These are either used by, or interfere with gsprint.

Possible future options are:

 -twoup
 -scale NN.NNN
 -papersize A4

--------------------
Configuration
--------------------

If you installed AFPL Ghostscript 6.50 or later using the self installer, gsprint will automatically use the latest copy of Ghostscript on your system. Otherwise you will need to specify -ghostscript name.

Before reading any command line options, gsprint.exe attempts to read options from gsprint.cfg in the same directory as gsprint.exe. In this file you should place the most common options. For example:

   -mono
   -ghostscript  
   "c:\aladdin\gs6.01\bin\gswin32c.exe"
   -I"c:\aladdin\gs6.01\lib;c:\aladdin\fonts"

With a configuration file like this, you can print a monochrome PostScript file with

    gsprint golfer.ps 

or a colour file with

    gsprint -colour colorcir.ps 

Config file options must be one per line. Other configuration files may be used with the command line option "-config filename.cfg"

--------------------
Licence
--------------------

GSPRINT is Copyright (C) 1999-2004 Ghostgum Software Pty Ltd.

GSPRINT is part of GSview. GSview is distributed with the Aladdin Free Public Licence. This licence is contained in the file LICENCE . See the GSview Readme for more details.

--------------------
Availability
--------------------

After installing GSview, gsprint.exe should be in the same directory as the GSview executable.

To use GSPRINT you must have AFPL Ghostscript 6.50 or later, This is available from:

    http://www.cs.wisc.edu/~ghost/ 