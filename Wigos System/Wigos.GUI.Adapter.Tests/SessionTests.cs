﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wigos.Cage.Proxy.Sessions;
using Wigos.Cage.Features.Dto.Sessions;
using Moq;
using Wigos.GUI.Adapter.Sessions.Commands;
using Wigos.Cage.Features.Sessions;
using Wigos.Cage.Proxy;
using Unity;

namespace Wigos.GUI.Adapter.Tests
{
  [TestClass]
  public class SessionTests
  {
    private SessionInputDto sessionDto;
    private Mock<ISessionProxy> sessionProxyMock;


    [TestInitialize]
    public void Initialize()
    {
      sessionProxyMock = new Mock<ISessionProxy>();

       var container = new UnityContainer();

       Bootstrapper.Initialize(container);
    }

    [TestMethod]
    public void OpenSession_Given_ProxyExists_And_Result_Is_SUCCESS_When_ExecuteProxy_Result_Is_SUCCESS()
    {

    }

    //[TestMethod]
    //public void OpenSession_Given_ProxyExists_And_Result_Is_SUCCESS_When_ExecuteProxy_Result_Is_SUCCESS()
    //{
    //  //Arrange      
    //  sessionProxyMock.Setup(m => m.OpenSession(It.IsAny<SessionInputDto>()))
    //    .Returns(new Core.Entity.Result(Core.Entity.Result.StatusResult.Success, null));
      
    //  //Act
    //  var adapter = new OpenSessionCommandHandler(sessionProxyMock.Object);
    //  var sessionCommand = new OpenSessionCommand("SESSION NAME", DateTime.Now);
    //  var result = adapter.Execute(sessionCommand);

    //  //Assert
    //  Assert.AreEqual(Core.Entity.Result.StatusResult.Success, result.Status, "The result must be successfully");
    //}

    //[TestMethod]    
    //public void OpenSession_Given_ProxyExists_And_Result_Is_FAILURE_When_ExecuteProxy_Result_Is_FAILURE()
    //{
    //  //Arrange      
    //  sessionProxyMock.Setup(m => m.OpenSession(It.IsAny<SessionInputDto>()))
    //    .Returns(new Core.Entity.Result(Core.Entity.Result.StatusResult.Failure, null));

    //  //Act
    //  var adapter = new OpenSessionCommandHandler(sessionProxyMock.Object);
    //  var sessionCommand = new OpenSessionCommand("SESSION NAME", DateTime.Now);
    //  var result = adapter.Execute(sessionCommand);

    //  //Assert
    //  Assert.AreEqual(Core.Entity.Result.StatusResult.Failure, result.Status, "The result must be successfully");
    //}

    //[TestMethod]
    //public void OpenSession_Given_ProxyExists_And_Result_Is_FAILURE_When_ExecuteProxy_Result_Is_FAILURE_And_Return_Errors()
    //{
    //  //Arrange   
    //  var errors = new List<Core.Entity.Error>()
    //  {
    //    new Core.Entity.Error(1, "Error description 1"),
    //    new Core.Entity.Error(2, "Error description 2")
    //  };

    //  sessionProxyMock.Setup(m => m.OpenSession(It.IsAny<SessionInputDto>()))
    //    .Returns(new Core.Entity.Result(Core.Entity.Result.StatusResult.Failure, errors));

    //  //Act
    //  var adapter = new OpenSessionCommandHandler(sessionProxyMock.Object);
    //  var sessionCommand = new OpenSessionCommand("SESSION NAME", DateTime.Now);
    //  var result = adapter.Execute(sessionCommand);
    //  var listError = result.GetError();

    //  //Assert
    //  Assert.AreEqual(Core.Entity.Result.StatusResult.Failure, result.Status, "The result must be successfully");
    //  Assert.IsNotNull(listError, "When result is Failure you must received a not null list of error(s)");
    //  Assert.IsTrue(listError.Count() == 2, "When result is Failure you must received 2 error(s)");
    //}

    //[TestMethod]
    //public void TestInt()
    //{
    //  var container = new UnityContainer();
    //  Bootstrapper.Initialize(container);

    //  ISessionProxy sessionProxy = container.Resolve<SessionProxy>();

    //  var adapter = new OpenSessionCommandHandler(sessionProxy);
    //  var sessionCommand = new OpenSessionCommand("SESSION NAME TEST FROM GUI", DateTime.Now);
    //  var result = adapter.Execute(sessionCommand);
    //}
  }
}
