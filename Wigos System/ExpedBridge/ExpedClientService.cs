﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExpedClientService.cs
// 
//   DESCRIPTION: Class used by ServiceHost 
//                  this class is used to instanciate the main webservice 
//                  for the Exped Validation proxy service
//                Class holds its own serializers for types used
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 15-NOV-2016 FGB    BUG 20636: Exped: Faltan informar campos al WS
// 06-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 07-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
//------------------------------------------------------------------------------
//
using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Xml.Serialization;
using WSI.Common.ExternalPaymentAndSaleValidation;
using ExpedBridge.ExpedWS;

namespace ExpedBridge
{
  public class ExpedClientService
  {
    #region "Constructor"
    public ExpedClientService()
    {
      System.Net.ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
    }
    #endregion

    #region "Public Methods"
    /// <summary>
    /// Request auhtorization for an AccountId
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    /// **NOTE** it is called by Reflection by ExpedProxyHost in the WCP_Service
    public ExpedClientResponse GetAuthorization(ExpedClientRequest Request)
    {
      ExpedServiceContractClient _exped_proxy;
      SolicitudAutorizacion _request;
      Autorizacion _response;
      ExpedClientResponse _authorization_result;

      //Generate client response object
      _authorization_result = GenerateClientResponse();

      //Get WS
      _exped_proxy = GetExpedService(Request);

      try
      {
        try
        {
          _exped_proxy.Open();

          //Set Authorization values
          _request = new SolicitudAutorizacion();
          _request.CuentaWin = Request.AccountId;

          //Get reply from Exped WS
          _response = _exped_proxy.SolicitarAutorizacion(_request);
        }
        finally
        {
          _exped_proxy.Close();
        }

        if (_response.TipoAutorizacion == TipoAutorizacion.TrxAutorizada)
        {
          _authorization_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK;
          _authorization_result.CodeAuthorization = _response.Codigo;
        }
        else
        {
          _authorization_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
          _authorization_result.ErrorDescription = "Cuenta no autorizada"; //TODO:ZZZZ - (FGB): 2016-10-04: Check if correct
        }
      }
      catch (FaultException<SolicitudAutorizacion> _fe)
      {
        //Normally it arrives here when the Account is unkown for the Exped Service
        _authorization_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_FAULT;
        _authorization_result.ErrorDescription = _fe.Message;
      }
      catch (Exception _ex)
      {
        _authorization_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
        _authorization_result.ErrorDescription = _ex.Message;
      }

      return _authorization_result;
    }

    /// <summary>
    /// Registers transaction for an operation
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    /// **NOTE** it is called by Reflection by ExpedProxyHost in the WCP_Service
    public ExpedClientResponse RegisterTransactionRecharge(ExpedClientRequest Request)
    {
      ExpedServiceContractClient _exped_proxy;
      TransaccionRecarga _request;
      TransaccionRegistrada _response;
      ExpedClientResponse _registration_result;

      //Get client response object
      _registration_result = GenerateClientResponse();

      //Get WS
      _exped_proxy = GetExpedService(Request);

      try
      {
        try
        {
          _exped_proxy.Open();

          //Map request fields to transaction fields
          _request = RequestToTransactionRecharge(Request);
          //Get reply from Exped WS
          _response = _exped_proxy.RegistrarRecarga(_request);
        }
        finally
        {
          _exped_proxy.Close();
        }

        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK;
        _registration_result.CodeAuthorization = _response.Codigo;
        _registration_result.TransactionId = _response.TransaccionId;
      }
      catch (FaultException<ExpedWS.FaultContract> _fe)
      {
        //Normally it arrives here when the Account is unkown for the Exped Service
        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_FAULT;
        _registration_result.ErrorDescription = _fe.Detail.Mensaje;
        _registration_result.ErrorCode = _fe.Detail.CodigoError;
      }
      catch (Exception _ex)
      {
        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
        _registration_result.ErrorDescription = _ex.Message;
      }

      return _registration_result;
    }

    public ExpedClientResponse RegisterTransactionRefund(ExpedClientRequest Request)
    {
      ExpedServiceContractClient _exped_proxy;
      TransaccionReintegro _request;
      TransaccionRegistrada _response;
      ExpedClientResponse _registration_result;

      //Get client response object
      _registration_result = GenerateClientResponse();

      //Get WS
      _exped_proxy = GetExpedService(Request);

      try
      {
        try
        {
          _exped_proxy.Open();

          //Map request fields to transaction fields
          _request = RequestToTransactionRefund(Request);
          //Get reply from Exped WS
          _response = _exped_proxy.RegistrarReintegro(_request);
        }
        finally
        {
          _exped_proxy.Close();
        }

        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK;
        _registration_result.CodeAuthorization = _response.Codigo;
        _registration_result.TransactionId = _response.TransaccionId;
      }
      catch (FaultException<ExpedWS.FaultContract> _fe)
      {
        //Normally it arrives here when the Account is unkown for the Exped Service
        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_FAULT;
        _registration_result.ErrorDescription = _fe.Detail.Mensaje;
        _registration_result.ErrorCode = _fe.Detail.CodigoError;
      }
      catch (Exception _ex)
      {
        _registration_result.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
        _registration_result.ErrorDescription = _ex.Message;
      }

      return _registration_result;
    }
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Maps request fields to transaction fields
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private TransaccionRecarga RequestToTransactionRecharge(ExpedClientRequest Request)
    {
      TransaccionRecarga _request;

      //Set Authorization values
      _request = new TransaccionRecarga();
      _request.NroTransaccion = Request.OperationId;
      _request.CuentaWin = Request.AccountId;
      _request.Nombre = Request.AccountName;
      _request.Codigo = Request.AuthorizationCode;
      _request.MontoA = Request.NetAmountSplitA;
      _request.MontoB = Request.NetAmountSplitB;
      _request.FechaHora = Request.OperationDate;
      _request.Sala = Request.SiteId;
      _request.TipoLineaNegocio = Request.BusinessLineType;
      _request.HtmlTicket = Request.HtmlTicket;
      _request.NroTicket = Request.TicketNumber;

      return _request;
    }

    private TransaccionReintegro RequestToTransactionRefund(ExpedClientRequest Request)
    {
      TransaccionReintegro _request;

      //Set Authorization values
      _request = new TransaccionReintegro();
      _request.NroTransaccion = Request.OperationId;
      _request.CuentaWin = Request.AccountId;
      _request.Nombre = Request.AccountName;
      _request.Codigo = Request.AuthorizationCode;
      _request.MontoTotal = Request.NetAmount;
      _request.FechaHora = Request.OperationDate;
      _request.Sala = Request.SiteId;
      _request.TipoLineaNegocio = Request.BusinessLineType;
      _request.HtmlTicket = Request.HtmlTicket;
      _request.NroTicket = Request.TicketNumber;
      _request.NroCheque = Request.CheckNumber;
      _request.MontoCheque = Request.CheckAmount ?? 0;

      return _request;
    }

    /// <summary>
    /// Returns an ExpedServiceContractClient with the connection values
    /// </summary>
    /// <returns></returns>
    private ExpedServiceContractClient GetExpedService(ExpedClientRequest Request)
    {
      String _channel_address;
      WSHttpBinding _channel_binding;
      ExpedServiceContractClient _exped_proxy;

      _channel_binding = GenerateChannelBinding(Request);
      _channel_address = Request.ServerAddress;

      _exped_proxy = new ExpedServiceContractClient(_channel_binding, new EndpointAddress(_channel_address));

      _exped_proxy.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(Request.User, Request.Password, Request.Domain);

      return _exped_proxy;
    }

    /// <summary>
    /// Generates the Channel binding
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private WSHttpBinding GenerateChannelBinding(ExpedClientRequest Request)
    {
      WSHttpBinding _channel_binding;
      _channel_binding = new WSHttpBinding(SecurityMode.Transport);

      _channel_binding.OpenTimeout = TimeSpan.FromMilliseconds(Request.TimeOutInMilliseconds);
      _channel_binding.SendTimeout = TimeSpan.FromMilliseconds(Request.TimeOutInMilliseconds);

      _channel_binding.ReceiveTimeout = TimeSpan.FromMilliseconds(Request.TimeOutInMilliseconds);
      _channel_binding.OpenTimeout = TimeSpan.FromMilliseconds(Request.TimeOutInMilliseconds);

      return _channel_binding;
    }

    /// <summary>
    /// Generates the client response
    /// </summary>
    /// <returns></returns>
    private ExpedClientResponse GenerateClientResponse()
    {
      ExpedClientResponse _response;

      _response = new ExpedClientResponse();
      _response.OperationResult = EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
      _response.CodeAuthorization = String.Empty;
      _response.ErrorCode = String.Empty;
      _response.ErrorDescription = String.Empty;

      return _response;
    }

    /// <summary>
    /// Validates any certification
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="certificate"></param>
    /// <param name="chain"></param>
    /// <param name="sslPolicyErrors"></param>
    /// <returns></returns>
    private Boolean ValidateRemoteCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      // trust any certificate!!!
      return true;
    }
    #endregion
  }

  #region "Serializable Objects"
  /// <summary>
  /// Class that contains the info for a Request 
  /// </summary>
  [Serializable, XmlRoot(ElementName = "ExpedClientRequest")]
  public class ExpedClientRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "ServerAddress")]
    public String ServerAddress { get; set; }

    [XmlElementAttribute(ElementName = "Domain")]
    public String Domain { get; set; }

    [XmlElementAttribute(ElementName = "User")]
    public String User { get; set; }

    [XmlElementAttribute(ElementName = "Password")]
    public String Password { get; set; }

    [XmlElementAttribute(ElementName = "TimeOutInMilliseconds")]
    public Int32 TimeOutInMilliseconds { get; set; }

    [XmlElementAttribute(ElementName = "OperationId")]
    public Int64 OperationId { get; set; }

    [XmlElementAttribute(ElementName = "AccountId")]
    public Int64 AccountId { get; set; }

    [XmlElementAttribute(ElementName = "NetAmount")]
    public Decimal NetAmount;

    [XmlElementAttribute(ElementName = "NetAmountSplitA")]
    public Decimal NetAmountSplitA;

    [XmlElementAttribute(ElementName = "NetAmountSplitB")]
    public Decimal NetAmountSplitB;
    
    [XmlElementAttribute(ElementName = "BusinessLineType")]
    public ExpedBridge.ExpedWS.TipoLineaNegocio BusinessLineType;

    [XmlElementAttribute(ElementName = "SiteId")]
    public Int32 SiteId;

    [XmlElementAttribute(ElementName = "AuthorizationCode")]
    public String AuthorizationCode;

    [XmlElementAttribute(ElementName = "OperationDate")]
    public DateTime OperationDate;

    [XmlElementAttribute(ElementName = "AccountName")]
    public String AccountName;

    [XmlElementAttribute(ElementName = "TicketNumber")]
    public Int64 TicketNumber;

    [XmlElementAttribute(ElementName = "HtmlTicket")]
    public String HtmlTicket;

    [XmlElementAttribute(ElementName = "CheckNumber")]
    public String CheckNumber;

    [XmlElementAttribute(ElementName = "CheckAmount")]
    public Decimal? CheckAmount;
  }

  /// Class that contains the info of a Response
  [Serializable, XmlRoot(ElementName = "ExpedClientResponse")]
  public class ExpedClientResponse
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "CodeAuthorization")]
    public String CodeAuthorization { get; set; }

    [XmlElementAttribute(ElementName = "TransactionId")]
    public String TransactionId { get; set; }

    [XmlElementAttribute(ElementName = "ErrorCode")]
    public String ErrorCode { get; set; }

    [XmlElementAttribute(ElementName = "ErrorDescription")]
    public String ErrorDescription { get; set; }

    [XmlElementAttribute(ElementName = "OperationResult")]
    public EnumExternalPaymentAndSaleValidationOperationResult OperationResult { get; set; }
  }
  #endregion
}