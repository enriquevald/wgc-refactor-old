//------------------------------------------------------------------------------
// Copyright (c) 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: UDPServer.cs
// 
//   DESCRIPTION: Abstract UDP Server
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 12-SEP-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-SEP-2008 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using WSI.Common;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Net.Sockets;

namespace WSI.Common
{
  /// <summary>
  /// Abstract UDP Server
  /// </summary>
  public class DiscoveryService
  {
    private delegate void XmlMsgReceivedHandler(DiscoveryService Sender, IPEndPoint Remote, String XmlRequest, ref String XmlResponse);

    #region MEMBERS
    // ------------------------------------------------------------------------- MEMBERS
    private Encoding m_encoding = Encoding.UTF8;
    private int m_udp_port = 0;
    private Thread m_thread = null;

    private ManualResetEvent m_ev_stop = new ManualResetEvent(false);
    private ManualResetEvent m_ev_run = new ManualResetEvent(true);


    private UdpClient m_udp_client = null;
    private IAsyncResult m_iar_recv = null;
    private IPAddress m_ip_filter = null;


    private XmlMsgReceivedHandler m_xml_msg_handler;

    private XmlMsgReceivedHandler XmlMsgHandler
    {
      get { return m_xml_msg_handler; }
      set { m_xml_msg_handler = value; }
    }


    #endregion // MEMBERS

    #region PUBLIC METHODS
    // ------------------------------------------------------------------------- PUBLIC METHODS



    /// <summary>
    /// Creates a new instance of a UPD server that will be listening on the given port number.
    /// </summary>
    /// <param name="PortNumber">Port Number where to listen</param>
    public DiscoveryService(int PortNumber)
    {
      string _str_ip_filter;
      IPAddress _ip_filter;

      m_udp_port = PortNumber;
      m_ip_filter = null;

      _str_ip_filter = Environment.GetEnvironmentVariable("DIRECTORY_SERVICE_IP_FILTER");
      if (!String.IsNullOrEmpty(_str_ip_filter))
      {
        if (IPAddress.TryParse(_str_ip_filter, out _ip_filter) == true)
        {
          m_ip_filter = _ip_filter;

          Log.Message("IP FILTER: " + _ip_filter.ToString());
        }
      }

      

    } // UdpServer

    private Boolean WaitForIncomingData(IAsyncResult IAR)
    {
      WaitHandle[] _handles;

      _handles = new WaitHandle[2];
      _handles[0] = m_ev_stop;
      _handles[1] = IAR.AsyncWaitHandle;

      return (WaitHandle.WaitAny(_handles) == 1);
    }
    private Boolean WaitForContinue()
    {
      WaitHandle[] _handles;

      _handles = new WaitHandle[2];
      _handles[0] = m_ev_stop;
      _handles[1] = m_ev_run;
      return (WaitHandle.WaitAny(_handles) == 1);
    }
    private Boolean Sleep(int Timeout)
    {
      return !m_ev_stop.WaitOne(Timeout);
    }

    protected Boolean StopRequested
    {
      get
      {
        return m_ev_stop.WaitOne(0);
      }
    }
    protected Boolean Paused
    {
      get
      {
        return !m_ev_run.WaitOne(0);
      }
    }

    public void Pause()
    {
      m_ev_run.Reset();
    }

    public void Continue()
    {
      m_ev_run.Set();
    }

    public void Start()
    {
      m_thread = new Thread(new ThreadStart(this.UDP_ListenerThread));
      m_thread.Start();
    }

    public void Stop()
    {
      // Request pause ...
      Pause();
      // Signal 'Stop event'
      m_ev_stop.Set();

    } // Stop








    /// <summary>
    /// Starts a thread that listen the UDP information.
    /// </summary>
    private void UDP_ListenerThread()
    {
      int _wait_hint;
      int _tick_log;

      _wait_hint = 0;
      _tick_log = Misc.GetTickCount();

      // Create Listener ...
      while (!StopRequested)
      {
        this.Sleep(_wait_hint);
        _wait_hint = 30000;

        try
        {
          m_udp_client = new UdpClient(m_udp_port);
          m_udp_client.EnableBroadcast = true;
          m_iar_recv = null;
          //
          // Receive loop ...
          //
          while (!StopRequested)
          {
            m_iar_recv = m_udp_client.BeginReceive(null, null);
            if (WaitForIncomingData(m_iar_recv))
            {
              ProcessIncomingData(m_iar_recv);
            }
            m_iar_recv = null;
          }
        }
        catch (SocketException _se)
        {
          if (_se.ErrorCode == 10048)
          {
            if (Misc.GetElapsedTicks(_tick_log) >= 5 * 60 * 1000)
            {
              Log.Message("UDP Port " + m_udp_port + " already in use.");
            }
            continue;
          }
          else
          {
            Log.Exception(_se);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (m_udp_client != null)
          {
            try { m_udp_client.Close(); }
            catch { };
          }
        }
      } // while (!StopRequested )
    }

    #endregion // PUBLIC METHODS

    #region PRIVATE METHODS
    // ------------------------------------------------------------------------- PRIVATE METHODS

    /// <summary>
    /// Callback, called when data is received
    /// </summary>
    /// <param name="AsyncResult"></param>
    private void ProcessIncomingData(IAsyncResult AsyncResult)
    {
      IPEndPoint _remote_ep;
      Byte[] _udp_packet;
      IPAddress _ip_filter;
      IPAddress _ip_remote;

      _remote_ep = null;
      _udp_packet = null;
      _ip_filter = m_ip_filter;

      try
      {
        _udp_packet = m_udp_client.EndReceive(AsyncResult, ref _remote_ep);
        if (Paused)
        {
          return;
        }
        if (_udp_packet == null)
        {
          return;
        }
        if (_udp_packet.Length <= 0)
        {
          return;
        }
        if (_remote_ep == null)
        {
          return;
        }
        // AJQ 16-OCT-2008, Ignore messages comming from Loopback IP address: 
        //   This happens when the terminal has no IP and sends a message indicating that its IP is the Loopback one.
        //   The service must avoid sending a message to its Loopback!!
        _ip_remote = _remote_ep.Address;
        if (IPAddress.IsLoopback(_ip_remote))
        {
          return;
        }

        if (m_ip_filter != null)
        {
          byte[] _bytes_ip_remote;
          byte[] _bytes_ip_filter;
          IPAddress _ip_filtered;
          int _idx_byte;

          _bytes_ip_remote = _ip_remote.GetAddressBytes();
          _bytes_ip_filter = _ip_filter.GetAddressBytes();

          for (_idx_byte = 0; _idx_byte < _bytes_ip_remote.Length; _idx_byte++)
          {
            _bytes_ip_filter[_idx_byte] = (byte)(_bytes_ip_filter[_idx_byte] & _bytes_ip_remote[_idx_byte]);
          }
          _ip_filtered = new IPAddress(_bytes_ip_filter);
          if (!_ip_remote.Equals(_ip_filtered))
          {
            return;
          }
        }

        if (XmlMsgHandler != null)
        {
          String _xml_request;
          String _xml_response;

          _xml_request = this.m_encoding.GetString(_udp_packet);
          if (String.IsNullOrEmpty(_xml_request))
          {
            return;
          }

          _xml_response = null;
          XmlMsgHandler(this, _remote_ep, _xml_request, ref _xml_response);
          if (String.IsNullOrEmpty(_xml_response))
          {
            return;
          }
          _udp_packet = this.m_encoding.GetBytes(_xml_response);
          if (_udp_packet.Length == 0)
          {
            return;
          }
          // Write data to the network
          m_udp_client.Send(_udp_packet, _udp_packet.Length, _remote_ep);
        }
      }
      catch (SocketException _soex)
      {
        switch (_soex.ErrorCode)
        {
          case 10065: // WSAEHOSTUNREACH
            break;

          default:
            Log.Exception(_soex);
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // DataReceivedCallback

    #endregion // PRIVATE METHODS


  } // class UdpServer

}
// namespace WSI.Common
