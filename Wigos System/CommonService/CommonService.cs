//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WWP Client to run as a service
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 17-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAR-2011 AJQ    First release.
// 23-JUL-2014 RRR    Added ELP Log
//-------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using WSI.Common;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace WSI.Common
{
  public partial class CommonService : ServiceBase
  {
    protected ManualResetEvent m_ev_stop    = new ManualResetEvent(false);
    protected ManualResetEvent m_ev_stopped = new ManualResetEvent(false);
    protected ManualResetEvent m_ev_run = new ManualResetEvent(true);
    
    protected Boolean m_console = false;
    protected Boolean m_verbose = false;

    protected Thread m_main_thread = null;
    private   Thread m_shutdown_thread = null;
    private   String m_shutdown_reason = "";
    protected String m_working_folder = null;
    protected String[] m_args = null;

    protected TimeSpan m_tick_stop_requested;

    protected String m_service_name = "Unknown";
    protected String m_version      = "CC.BBB";
    protected String m_log_prefix   = "LOG";
    protected String m_protocol_name = "PPP";
    protected ENUM_TSV_PACKET m_tsv_packet = ENUM_TSV_PACKET.TSV_PACKET_NOT_SET;
    protected String m_old_config_file   = "";
    protected String m_alarm_source = @"Machine\\Unknown";
    
    protected Boolean    m_check_license = true;
    protected int        m_default_port = 0;
    protected Boolean    m_stand_by = false;
    protected LicenseStatus m_license_status = LicenseStatus.NOT_CHECKED;
    protected string     m_ip_address = "";
    
    protected void DisableLicenseChecking ()
    {
      m_check_license = false;
    } // DisableLicenseChecking

    public WaitHandle StopEvent
    {
      get
      {
        return m_ev_stop;
      }
    } // StopEvent

    public CommonService()
    {
      //
      // ServiceBase attributes
      //
      this.AutoLog = true;
      this.CanHandlePowerEvent = true;
      this.CanPauseAndContinue = true;
      this.CanShutdown = true;
      this.CanStop = true;
      //
      // CommonService attributes
      //
      m_console = false;
      m_verbose = false;
    } // CommonService

    protected bool Paused
    {
      get
      {
        return !m_ev_run.WaitOne(0);
      }
    } // Paused

    protected bool StopRequested
    {
      get
      {
        return m_ev_stop.WaitOne(0);
      }
    } // StopRequested

    protected override void OnContinue()
    {
      Log.Message("SERVICE RESUMED ...");
      base.OnContinue();
      m_ev_run.Set();
    } // OnContinue

    protected override void OnPause()
    {
      base.OnPause();
      m_ev_run.Reset();
      Log.Message("SERVICE PAUSED ...");
    } // OnPause

    protected override void OnSessionChange(SessionChangeDescription changeDescription)
    {
      base.OnSessionChange(changeDescription);
      Log.Message(" * SessionChangeDescription: " + changeDescription.ToString() + ", SessionId: " + changeDescription.SessionId.ToString());
    } // OnSessionChange

    protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
    {
      bool _rc;

      _rc = base.OnPowerEvent(powerStatus);

      switch (powerStatus)
      { 
        case PowerBroadcastStatus.BatteryLow:
          Alarm.Register(AlarmSourceCode.System, 0, m_alarm_source, AlarmCode.SystemPowerStatus_BatteryLow);
          break;

        case PowerBroadcastStatus.PowerStatusChange:
          Alarm.Register(AlarmSourceCode.System, 0, m_alarm_source, AlarmCode.SystemPowerStatus_PowerStatusChange);
          break;
        
        case PowerBroadcastStatus.ResumeCritical:
          Alarm.Register(AlarmSourceCode.System, 0, m_alarm_source, AlarmCode.SystemPowerStatus_ResumeCritical);
          break;

        default:
          Log.Message(" * PowerBroadcastStatus: " + powerStatus.ToString());
          break;
      }

      return _rc;
    } // OnPowerEvent

    protected override void OnStop()
    {
      RequestOrderlyShutdown ("STOP");
      
      //m_ev_stopped.WaitOne();

    } // OnStop

    protected override void OnShutdown()
    {
      RequestOrderlyShutdown("SHUTDOWN");
    }

    protected virtual void OnServiceStopped()
    {
      // Do noting
    } // OnServiceStopped

    protected void RequestOrderlyShutdown (String Reason)
    {
      if ( m_shutdown_thread != null )
      {
        return;
      }
      m_shutdown_thread = new Thread(OrderlyShutdwonThread);
      m_shutdown_reason = Reason;
      m_shutdown_thread.Start();
    }
    
    private void OrderlyShutdwonThread ()
    {
      int _tick;
      int _tick_task;
      int _tick_log;
      int _pending_tasks;
      int _prev_pending;
      Int32 _queued_tasks;
      Int32 _running_tasks;

      // Tick Stop requested
      _tick = Misc.GetTickCount();
      _tick_log = _tick;

      Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Stopping);

      Log.Message("STOP REQUESTED: " + m_shutdown_reason + " ----------------------------------------------------");
      // Set the Stop event
      m_ev_stop.Set();
      
      WorkerPool.RequestStop ();

      if (m_main_thread.Join(0))
      {
        ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
      }
      
      _prev_pending = int.MaxValue;
      _tick_task = Misc.GetTickCount();
      while (!WorkerPool.CanShutdown(out _queued_tasks, out _running_tasks))
      {
        _pending_tasks = _queued_tasks + _running_tasks;
        if (_pending_tasks < _prev_pending)
        {
          _prev_pending = _pending_tasks;
          _tick_task = Misc.GetTickCount();
        }

        if (Misc.GetElapsedTicks(_tick_task) >= 10000)
        {
          if ( _pending_tasks > 0 )
          {
            Log.Message("  - Remaining tasks: Queued/Running = " + _queued_tasks.ToString() + "/" + _running_tasks.ToString() + ", aborting ...");
            _tick_log = Misc.GetTickCount();
          }
        
          break;
        }
        
        if ( Misc.GetElapsedTicks(_tick_log) >= 1000 )
        {
          Log.Message("  - Remaining tasks: Queued/Running = " + _queued_tasks.ToString() + "/" + _running_tasks.ToString() + ", stopping ...");
          _tick_log = Misc.GetTickCount();
        }

        if (m_main_thread.Join(0))
        {
          ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
        }
        Thread.Sleep(100);
      }


      if (!m_main_thread.Join(10000))
      {
        try { m_main_thread.Abort (); }
        catch {}
      }

      ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPED");
      Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Stopped);

      OnServiceStopped();

      Log.Message("Stopped.");
      Log.Close();

      m_ev_stopped.Set();
      
      Environment.Exit(0);
    }
    

    protected override void OnStart(string[] Args)
    {
      // Command line arguments
      m_args = Args;

      // Start the Main Thread
      m_main_thread = new Thread(CommonServiceMainRoutine);
      m_main_thread.Name = "Service Main Thread";
      m_main_thread.Start();
      
      if (!m_console)
      {
        return;
      }

      //
      // Console Loop waiting for Keystrokes
      //
      while (!this.StopRequested)
      {
        ConsoleKeyInfo _cki;

        _cki = Console.ReadKey(true);

        if (_cki.Modifiers == ConsoleModifiers.Control
            && _cki.Key == ConsoleKey.P)
        {
          if (this.Paused)
          {
            // Resume service ...
            this.OnContinue();
          }
          else
          {
            // Pause service
            this.OnPause();
          }
        }
      } // while (!this.StopRequested)
    } // OnStart

    protected virtual void OnServiceRunning(IPEndPoint _service_ip_endpoint)
    {
      throw new Exception("The method or operation is not implemented.");
    } // OnServiceRunning

    protected virtual void OnServiceBeforeInit()
    {
      // Do noting
    } // OnServiceBeforeInit

    protected virtual void SetParameters ()
    {
      throw new Exception("Not implemented!");
    } // SetParameters

    private string CurrentVersion
    {
      get
      {
        Assembly _assembly;
        Version _version;

        _assembly = Assembly.GetEntryAssembly ();
        _version = _assembly.GetName().Version;
        
        return _version.Major.ToString("00") + "." + _version.Minor.ToString("000");
      }
    } // CurrentVersion
    
    protected void CommonServiceMainRoutine()
    {
      Assembly _assembly;
      int _idx_module;
      String _env;
      String _env_value;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        // The Service will be executed on the Assembly (.exe) folder.
        _assembly = Assembly.GetEntryAssembly ();
        _idx_module = _assembly.Location.IndexOf(_assembly.ManifestModule.Name);
        m_working_folder = _assembly.Location.Substring(0, _idx_module);

        // Change the Working Directory
        Directory.SetCurrentDirectory(m_working_folder);

        m_version = CurrentVersion;
        SetParameters();
        m_alarm_source = Environment.MachineName + @"\\" + m_service_name;

        // Start a Simple Logger
        Log.AddListener(new SimpleLogger(m_log_prefix));

        if (m_console)
        {
          // Start a Console Logger
          Log.AddListener(new ConsoleLogger());

          // Catch the "Ctrl-C"
          Console.CancelKeyPress += new ConsoleCancelEventHandler(this.ConsoleCancelEventHandler);
        }

        // Command Line
        String _args;
     
        _args = "";
        foreach (String _arg in m_args)
        {
          if (String.IsNullOrEmpty(_args))
          {
            _args = _arg.Trim();
          }
          else
          {
            _args += " " + _arg.Trim();
          }
        }
        if (String.IsNullOrEmpty(_args))
        {
          _args = "<none>";
        }

        _sb.AppendLine("Starting " + m_service_name.Trim() + " -----------------------------------------------------------------------");
        _sb.AppendLine("");
        _sb.AppendLine("- Version:   " + m_version.Trim());
        _sb.AppendLine("");
        _sb.AppendLine("- Arguments: " + _args);
        _sb.AppendLine(" ");
        _sb.AppendLine("- Environment: ");
        _env = m_log_prefix.Trim();
        _env += "_NETLOG_ENABLED";
        _env = _env.ToUpper();
        _env_value = Environment.GetEnvironmentVariable(_env);
        if (_env_value != null)
        {
          NetLog.AddListener(new NetLogger());
        }
        if (String.IsNullOrEmpty(_env_value))
        {
          _env_value = "Not defined";
        }
        else
        {
          _env_value = "Defined";
        }
        _sb.AppendLine("  - " + _env + ": " + _env_value);
        _sb.AppendLine(" ");
        Log.Message(_sb.ToString());
      
        // Configuration default values 
        StringBuilder _xml;
        _xml = new StringBuilder();
        _xml.AppendLine("<SiteConfig>");
        _xml.AppendLine("    <DBPrincipal>192.168.200.1</DBPrincipal>");
        _xml.AppendLine("    <DBMirror>192.168.200.2</DBMirror>");
        _xml.AppendLine("    <DBId>0</DBId>");
        _xml.AppendLine("    <Server>");
        _xml.AppendLine("        <LocalIP>192.168.1.13</LocalIP>");
        _xml.AppendLine("        <CJ>");
        _xml.AppendLine("            <VendorID>WIN12</VendorID>");
        _xml.AppendLine("            <VendorIP>192.168.200.1</VendorIP>");
        _xml.AppendLine("            <RemoteIP>10.1.1.112</RemoteIP>");
        _xml.AppendLine("        </CJ>");
        _xml.AppendLine("    </Server>");
        _xml.AppendLine("</SiteConfig>");

        if (!ConfigurationFile.Init(m_old_config_file, _xml.ToString()))
        {
          Log.Error(" Reading application configuration settings. Application stopped");

          return;
        }

        // Connect to DB
        WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
        WGDB.SetApplication(m_service_name, m_version);
        WGDB.ConnectAs("WGROOT");

        while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
        {
          Log.Warning("Waiting for database connection ...");
      
          Thread.Sleep(5000);
        }

        // Init resources
        WSI.Common.Resource.Init();

        // Check for new version
        AutoUpdate ();

        Misc.StartLicenseChecker(OnLicenseStatusChanged);

        IPEndPoint _service_ip_endpoint;

        // Get Local IpAddress
        m_ip_address = ConfigurationFile.GetSetting("LocalIP");
        if (String.IsNullOrEmpty(m_ip_address))
        {
          m_ip_address = IPAddress.Any.ToString();
        
          // TODO: ...
          Log.Error(" No Discovery Service IP address is available. Application stopped");

          return;
        }
      
        // WWP IP EndPoint
        _service_ip_endpoint = new IPEndPoint(IPAddress.Parse(m_ip_address), m_default_port);

        Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Started);

        OnServiceBeforeInit ();

        if (m_stand_by)
        {
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_StandBy);
        }

        if ( ServiceStatus.InitService(m_ev_stop, m_protocol_name, m_ip_address, m_stand_by, m_version) )
        {
          OnServiceRunning(_service_ip_endpoint);
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_Running);
          ServiceStatus.RunningService(m_ev_stop, m_protocol_name, m_ip_address, m_stand_by, m_version);
        }
        ServiceStatus.SetServiceStatus(m_protocol_name, m_ip_address, m_stand_by, m_version, "STOPPING");
      }
      catch (FormatException _format_ex)
      {
        Log.Error("Bad IP Address format. Please fix it in the cfg file and restart the service");
        Log.Message("-------------------------------------");
        Log.Exception(_format_ex);
        Log.Message("-------------------------------------");
      }
      
    } // CommonServiceMainRoutine

    void OnLicenseStatusChanged(LicenseStatus LicenseStatus)
    {
      m_license_status = LicenseStatus;
      switch (m_license_status)
      {
        case LicenseStatus.VALID:
          return;

        case LicenseStatus.VALID_EXPIRES_SOON:
          Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_LicenseWillExpireSoon);
          return;

        case LicenseStatus.NOT_AUTHORIZED:
        case LicenseStatus.INVALID:
        case LicenseStatus.EXPIRED:
        case LicenseStatus.NOT_CHECKED:
        default:
        break;
      }

      // License NOT VALID
      m_check_license = m_check_license && !Debugger.IsAttached;
      if (m_check_license)
      {
        Alarm.Register(AlarmSourceCode.Service, 0, m_alarm_source, AlarmCode.Service_LicenseExpired);
        
        RequestOrderlyShutdown("LICENSE");
      }
    } // OnLicenseStatusChanged

    LicenseStatus WaitTillLicenceChecked()
    {
      while ( m_license_status == LicenseStatus.NOT_CHECKED )
      {
        if ( m_ev_stop.WaitOne(100) )
        {
          break;
        }
      }
      
      return m_license_status;
    } // WaitTillLicenceChecked
    
    #region CONSOLE MODE

    //------------------------------------------------------------------------------
    // PURPOSE: Simulte "STOP SERVICE" when Ctrl-C is received
    //
    // PARAMS:
    //    - INPUT:
    //        - Sender
    //
    //    - INPUT/OUTPUT:
    //        - Event
    //
    // RETURNS:
    //
    private void ConsoleCancelEventHandler(Object Sender, ConsoleCancelEventArgs Event)
    {
      Event.Cancel = (Event.SpecialKey == ConsoleSpecialKey.ControlC);
      RequestOrderlyShutdown("CTRL-C");
    } // ConsoleCancelEventHandler

    //------------------------------------------------------------------------------
    // PURPOSE: Simulte "START SERVICE" when service started with "-console"
    //
    // PARAMS:
    //    - INPUT:
    //        - Args
    //
    // RETURNS:
    //
    public void Run(string[] Args)
    {
      foreach (String _arg in Args)
      {
        if (_arg == "-verbose")
        {
          m_verbose = true;
          m_console = true;
        }
        if (_arg == "-console")
        {
          m_console = true;
        }
        if (_arg == "-restart")
        {
          ServiceController _sc;

          _sc = new ServiceController(m_service_name);
          _sc.Stop();
          _sc.Start();

          return;
        }
      }

      if ( Debugger.IsAttached )
      {
        m_console = true;
      }

      if (m_console)
      {
        this.OnStart(Args);
      }
      else
      {
        ServiceBase.Run(this);
      }
    } // Run

    #endregion

  } // CommonService

} // WSI.Common
