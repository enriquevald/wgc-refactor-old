//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WC2 to run as a service
// 
//        AUTHOR: Agust� Poch (Header only)
// 
// CREATION DATE: 28-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2009 APB    First release (Header).
// 30-SEP-2015 JMM    Fixed Bug 4803: WC2 locks tables & hangs itself
// 02-MAY-2017 AMF    WIGOS-1106: Jackpots Contribution - Amounts distribution to Jackpot
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Net;
using WSI.Common;
using WSI.WC2;

public partial class WC2_Service : CommonService
{
  private const string VERSION = "18.313";
  private const string SVC_NAME = "WC2_Service";
  private const string PROTOCOL_NAME = "WC2";
  private const int SERVICE_PORT = 13001;
  private const Boolean STAND_BY = true;
  private const string LOG_PREFIX = "WC2";
  private const string CONFIG_FILE = "WSI.WC2_Configuration";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE;

  public static void Main(String[] Args)
  {
    WC2_Service _this_service;

    _this_service = new WC2_Service();
    _this_service.Run(Args);
  } // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
    OperationVoucherParams.LoadData = true;
  } // SetParameters

  protected override void OnServiceBeforeInit()
  {
    // RCI 10-JAN-2011: Start Mailing processing
    Mailing.Start();
  } // OnServiceBeforeInit

  /// <summary>
  /// Starts the service
  /// </summary>
  /// <param name="ipe"></param>
  protected override void OnServiceRunning(IPEndPoint Ipe)
  {
    WC2_DirectoryService _ds;
    IXmlSink _executor;
    WC2_Server _server;

    // Start Cache
    WC2_DbCache.Init();
    WC2_DbCache.Start();

    // Start WC2 Client
    WC2_Client.Init();
    WC2_Client.Start();

    // Start WC2 Pending Enrolls Queue
    WC2_PendingEnrollsQueue.Init();
    WC2_PendingEnrollsQueue.Start();

    // Start WC2 Pending Plays Queue
    WC2_PendingPlaysQueue.Init();
    WC2_PendingPlaysQueue.Start();

    // Init BatchUpdate 
    WSI.Common.BatchUpdate.TerminalSession.Init("WC2",
                                                (Int32)WC2_SessionStatus.Opened,
                                                (Int32)WC2_SessionStatus.Abandoned,
                                                (Int32)WC2_SessionStatus.Disconnected,
                                                (Int32)WC2_SessionStatus.Timeout);

    // Generate WC2 cards.
    WC2_BusinessLogic.DB_WC2_CheckCards();

    // Generate WC2 patterns.
    WC2_BusinessLogic.DB_WC2_CheckPatterns();

    // Save all patterns in memory.
    WC2_Patterns.p_list = WC2_BusinessLogic.WC2_GetPatterns();

    // Update terminal list of jackpot configuration
    GroupsService.UpdateJackpotTerminalsFromProviders();

    WC2_JackpotManager.Init();
    WC2_JackpotManager.Start();

    WC2_SiteJackpotManager.Start();

    // AMF 02/05/2017: Start Jackpots
    WC2_Jackpots.Init();

    _executor = WC2_Client.GetInstance();
    _server = new WC2_Server(Ipe);
    _server.Sink = _executor;
    WSI.WC2.WC2.Server = _server;
    _server.Start();

    //Operations Schedule Service
    OperationsScheduleService.Init(m_service_name);

    // Start directory Service
    _ds = new WC2_DirectoryService(Ipe);

    Log.Message("WC2_Service listening on: " + Ipe.ToString());
    Log.Message("Directory Service says:   " + Ipe.ToString());
    Log.Message("");
    Log.Message("");

  } // OnServiceRunning

} // WC2_Service


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under system account
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WC2_Service";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WC2_Service";
  }
}
