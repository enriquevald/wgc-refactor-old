﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wigos.Core.Entity;
using Wigos.GUI.Adapter.Common.Base;

namespace Wigos.GUI.Adapter.WGC.Sessions
{
  public class OpenSessionTranslator : ITranslator<Result, bool>
  {
    public bool Translate(Result source)
    {
      return source.Status == Result.StatusResult.Success;
    }
  }
}
