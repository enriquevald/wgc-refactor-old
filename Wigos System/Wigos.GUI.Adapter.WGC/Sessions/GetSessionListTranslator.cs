﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wigos.Cage.Features.Dto.Sessions;
using Wigos.Core.Entity;
using Wigos.GUI.Adapter.Common.Base;
using Wigos.GUI.Adapter.WGC.Helpers;

namespace Wigos.GUI.Adapter.WGC.Sessions
{
  public class GetSessionListTranslator : ITranslator<IEnumerable<SessionOutputDto>, DataTable>
  {
    public DataTable Translate(IEnumerable<SessionOutputDto> list)
    {
      return list.ToDataTable<SessionOutputDto>();
    }
  }
}
