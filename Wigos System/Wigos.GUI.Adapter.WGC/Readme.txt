﻿Project Name: Wigos.Cage.Feature.Dto
Description:  This project include all the Data Transfer Objects (DTO) for the input/output messages. 
              All the requests to Wigos.Cage module must be done using an input DTO 
              and all the responses from the Wigos.Cage module must be done using an output DTO       
              
Structure of the Project:
- Use cases (Example: Movements, Sessions, etc.)
    - For each one, you must include the input and output DTO