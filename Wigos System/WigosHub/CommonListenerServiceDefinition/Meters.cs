using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CommonListenerServiceDefinition
{
  [CollectionDataContract]
  public class Meters : List<Meter>
  {
  }
}