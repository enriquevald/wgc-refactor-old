using System.Runtime.Serialization;

namespace CommonListenerServiceDefinition
{
  [DataContract]
  public class CommonResult
  {
    [DataMember]
    public int StatusCode { get; set; }
    [DataMember]
    public string StatusText { get; set; }
    [DataMember]
    public bool IsError { get; set; }
    [DataMember]
    public string LogMessage { get; set; }
  }
}