using System.Runtime.Serialization;

namespace CommonListenerServiceDefinition
{
  [DataContract]
  public class CommonSessionStartResult : CommonResult
  {
    [DataMember]
    public decimal Balance { get; set; }
    [DataMember]
    public long SessionId { get; set; }
    [DataMember]
    public decimal AmountCashable { get; set; }
    [DataMember]
    public decimal AmountRestricted { get; set; }
    [DataMember]
    public decimal AmountNonRestricted { get; set; }

  }
}