using System.ServiceModel;

namespace CommonListenerServiceDefinition
{
  [ServiceContract]
  public interface ICommonListenerServiceDefinition
  {
    [OperationContract]
    CommonAccountResult WS2S_AccountStatus(string VendorId,
                                           string Trackdata);

    [OperationContract]
    CommonSessionStartResult WS2S_StartCardSession(string VendorId,
      string SerialNumber,
      int MachineNumber,
      long VendorSessionId,
      string TrackData);

    [OperationContract]
    CommonResult WS2S_EndCardSession(string VendorId,
      long SessionId,
      decimal AmountPlayed,
      decimal AmountWon,
      int GamesPlayed,
      int GamesWon,
      decimal BillIn,
      decimal CurrentBalance,
      decimal AmountCashable,
      decimal AmountRestricted,
      decimal AmountNonRestricted);

    [OperationContract]
    CommonResult WS2S_UpdateCardSession(string VendorId,
      long SessionId,
      decimal AmountPlayed,
      decimal AmountWon,
      int GamesPlayed,
      int GamesWon,
      decimal BillIn,
      decimal CurrentBalance,
      decimal AmountCashable,
      decimal AmountRestricted,
      decimal AmountNonRestricted);

    [OperationContract]
    CommonResult WS2S_ReportEvent(
      string VendorId,
      string SerialNumber,
      int MachineNumber,
      int EventId,
      decimal Amount,
      long SessionId);

    [OperationContract]
    CommonAccountDetailsResult WS2S_AccountInfo(string VendorId,
      string TrackData);

    [OperationContract]
    CommonCardResult WS2S_CardInfo(string TrackData);


    [OperationContract]
    CommonResult WS2S_ReportMeters(
      string VendorId,
      string SerialNumber,
      int? MachineNumber,
      Meters XmlMeters);
  }
}