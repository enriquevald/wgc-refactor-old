using System.Runtime.Serialization;

namespace CommonListenerServiceDefinition
{
  [DataContract]
  public class Meter
  {
    [DataMember]
    public int Code { get; set; }
    [DataMember]
    public long Value { get; set; }
  }
}