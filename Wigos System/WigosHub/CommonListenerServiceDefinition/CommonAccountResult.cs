using System;
using System.Runtime.Serialization;

namespace CommonListenerServiceDefinition
{
  [DataContract]
  public class CommonAccountResult : CommonResult
  {
    [DataMember]
    public decimal Balance { get; set; }
    [DataMember]
    public string VID { get; set; }
  }
  [DataContract]
  public class CommonAccountDetailsResult : CommonResult
  {
    [DataMember]
    public long AccountNumber { get; set; }
    [DataMember]
    public string Name { get; set; }
    [DataMember]
    public string FirstSurname { get; set; }
    [DataMember]
    public string SecondSurname { get; set; }
    [DataMember]
    public string Gender { get; set; }
    [DataMember]
    public string BirthDate { get; set; }
    //[DataMember]
    //public string BirthStateCode { get; set; }
    //[DataMember]
    //public string BirthState { get; set; }
    //[DataMember]
    //public string Curp { get; set; }
    [DataMember]
    public string Email { get; set; }
    [DataMember]
    public string AuxEmail { get; set; }
    [DataMember]
    public string MobileNumber { get; set; }
    [DataMember]
    public string FixedPhone { get; set; }
    [DataMember]
    public string ClientTypeCode { get; set; }
    [DataMember]
    public string ClientType { get; set; }
    [DataMember]
    public string FiscalCode { get; set; }
    //[DataMember]
    //public string RFC { get; set; }
    //[DataMember]
    //public string NationalityCode { get; set; }
    [DataMember]
    public string Nationality { get; set; }
    [DataMember]
    public string Street { get; set; }
    [DataMember]
    public string HouseNumber { get; set; }
    //[DataMember]
    //public string DoorNumber { get; set; }
    [DataMember]
    public string Colony { get; set; }
    //[DataMember]
    //public string CountyCode { get; set; }
    //[DataMember]
    //public string Country { get; set; }
    //[DataMember]
    //public string StateCode { get; set; }
    [DataMember]
    public string State { get; set; }
    //[DataMember]
    //public string CityCode { get; set; }
    [DataMember]
    public string City { get; set; }
    //[DataMember]
    //public string IssuingAuthority { get; set; }
    [DataMember]
    public string DocumentType { get; set; }
    [DataMember]
    public string DocumentNumber { get; set; }
    [DataMember]
    public string Document2Type { get; set; }
    [DataMember]
    public string Document2Number { get; set; }
    [DataMember]
    public string Document3Type { get; set; }
    [DataMember]
    public string Document3Number { get; set; }
    [DataMember]
    public string LevelCode { get; set; }
    [DataMember]
    public string Level { get; set; }
    [DataMember]
    public string Points { get; set; }
    [DataMember]
    public string CityCode { get; set; }
    [DataMember]
    public string DocumentTypeCode { get; set; }
  }


  [DataContract]
  public class CommonCardResult : CommonResult
  {
    [DataMember]
    public int CardIndex { get; set; }
    [DataMember]
    public Int64 CustomerId { get; set; }
    [DataMember]
    public int AccountType { get; set; }
    [DataMember]
    public int CardStatus { get; set; }
  }

}