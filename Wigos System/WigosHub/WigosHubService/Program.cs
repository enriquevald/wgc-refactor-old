﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using Common;
using SessionCommon;

using EventInterchange;
using EventCommon;
using MeterInterchange;
using MeterCommon;
using AccountCommon;
using Microsoft.Practices.Unity;
using WigosHubCommon;

namespace WigosHubService
{
  class Program
  {
    public Program()
    {
      if (ConfigurationManager.AppSettings.HasKeys())
      {
        if (ConfigurationManager.AppSettings.AllKeys.Contains("WigosHub.ServiceInstanceName"))
          ServiceName = ConfigurationManager.AppSettings.Get("WigosHub.ServiceInstanceName");
        if (ConfigurationManager.AppSettings.AllKeys.Contains("LogLevel"))
          m_log_level = (LogLevel)int.Parse(ConfigurationManager.AppSettings.Get("LogLevel"));
      }

    }
    public static string ServiceName = "Default Instance Hub";
    public static LogLevel m_log_level = LogLevel.Error;
    private static ILogger Logger;
    public class Service : ServiceBase
    {

      protected override void OnStart(string[] Args)
      {
        if (!Start()) throw new Exception("error starting " + ServiceName);
      }

      protected override void OnStop()
      {
        Program.Stop();
      }
    }



    private static void Main()
    {
      if (ConfigurationManager.AppSettings.HasKeys())
      {
        if (ConfigurationManager.AppSettings.AllKeys.Contains("WigosHub.ServiceInstanceName"))
          ServiceName = ConfigurationManager.AppSettings.Get("WigosHub.ServiceInstanceName");
        if (ConfigurationManager.AppSettings.AllKeys.Contains("LogLevel"))
          m_log_level = (LogLevel)int.Parse(ConfigurationManager.AppSettings.Get("LogLevel"));
      }

      Logger = new ConsoleLogger {LogLevel = m_log_level};
      if (!Environment.UserInteractive)
      {
        Logger.Log(LogLevel.Info, "Starting as service"); 
        using (var _service = new Service())
          ServiceBase.Run(_service);
      }
      else
      {
        Logger.Log(LogLevel.Info, "Starting in interactive mode"); 
        Console.WriteLine(@"Press any key to stop " + ServiceName + "...");
        if (!Start())
        {
          Console.WriteLine("Error starting service " + ServiceName);
        }
        Console.ReadKey(true);
        Stop();
      }

    }

    private static void Stop()
    {
    }

    private static bool Start()
    {
      Logger.Log(LogLevel.Info, "Encrypt Configuration file if needed..."); 
      EncryptConnectionString( Process.GetCurrentProcess().MainModule.FileName);
      Logger.Log(LogLevel.Info, "Register interchange routines..."); 
      RegisterEGMInterchanges();
      Logger.Log(LogLevel.Info, "Register listeners..."); 
      IoC.LoadAssemblies("Listeners");
      Logger.Log(LogLevel.Info, "Register dispatchers...");
      if (!IoC.LoadAssemblies("Dispatchers"))
      {
        Logger.Log(LogLevel.Info, "No dispatchers found, registering consumer...");
        IoC.LoadAssemblies("Consumer");
      }
      Logger.Log(LogLevel.Info, "Start listeners..."); 
      IoC.Container.ResolveAll<IWHUBListener>();
      Logger.Log(LogLevel.Info, "Waiting for events..."); 
      return true;
    }

    public static void EncryptConnectionString(string FileName)
    {
      try
      {
        // Open the configuration file and retrieve the connectionStrings section.
        var _configuration = ConfigurationManager.OpenExeConfiguration(FileName);
        ConnectionStringsSection _config_section =
          _configuration.GetSection("connectionStrings") as ConnectionStringsSection;
        if (_config_section != null && ((!(_config_section.ElementInformation.IsLocked)) && (!(_config_section.SectionInformation.IsLocked))))
        {
          _config_section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
          _config_section.SectionInformation.ForceSave = true;
          _configuration.Save();
        }
      }
      catch (Exception _ex)
      {
        Console.Write(_ex.Message);
      }
    }

   private static void RegisterEGMInterchanges()
    {
      IoC.Container.RegisterInstance(Logger, new ContainerControlledLifetimeManager());
      IoC.RegisterType<IFileSystem, FileSystem>();
      IoC.RegisterType<IFileSystemDirectory, FileSystemDirectory>();

      IoC.RegisterType<IInterchangeSession, SessionInterchange.SessionInterchange>();
      IoC.RegisterType<IInterchangeEvent, InterchangeEvent>();
      IoC.RegisterType<IInterchangeMeter, InterchangeMeter>();
      IoC.RegisterType<IInterchangeAccount, AccountInterchange.AccountInterchange>();
    }
  }
}
