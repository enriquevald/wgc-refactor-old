﻿using System.Collections.Generic;

namespace Common
{
  public interface IWHUBModule 
  {
    Dictionary<string, string> Query();
    void Kill();
  }
}