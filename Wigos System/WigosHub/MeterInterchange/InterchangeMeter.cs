﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using IEGMCommon;
using MeterCommon;

namespace MeterInterchange
{
  public class InterchangeMeter : IInterchangeMeter
  {
    private readonly IEGMConsumer[] m_egm_consumer;

    public InterchangeMeter(IWHUBProcessor[] EGMConsumer)
    {
      m_egm_consumer = EGMConsumer.OfType<IEGMConsumer>().ToArray();
    }

    public GenericResult Report(string VendorId, string SerialNumber, int? MachineNumber, Dictionary<int, long> Meters)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.ReportMeters(VendorId,  SerialNumber,  MachineNumber,  Meters);
    }
  }
}
