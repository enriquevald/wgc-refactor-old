﻿using System.Collections.Generic;
using System.ServiceModel;

namespace HubStatusListenerServiceDefinition
{
  [ServiceContract]
  public interface IHubStatusListenerServiceDefinition
  {
    [OperationContract]
    List<string> LoadedModules();
    [OperationContract]
    Dictionary<string, string> QueryModule(string ModuleName);
    [OperationContract]
    bool PushUpdatePart(byte[] File, string Crc32, int Part);
    [OperationContract]
    bool DoUpdate(string Crc32);
    [OperationContract]
    bool CancelUpdate();
  }
}
