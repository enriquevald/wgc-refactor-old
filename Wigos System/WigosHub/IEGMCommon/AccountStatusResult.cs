using System.Data;

namespace IEGMCommon
{
  public class AccountStatusResult : GenericResult
  {
    private const int SP_RESPONSE_PARAM3 = 2;
    private const int SP_RESPONSE_PARAM4 = 3;
    public override void Fill(IDataReader DataReader)
    {
      base.Fill(DataReader);
      if (StatusCode != 0) return;
      Balance = DataReader.GetDecimal(SP_RESPONSE_PARAM3);
      VID = DataReader.GetString(SP_RESPONSE_PARAM4);
    }

    public decimal Balance { get; set; }
    public string VID { get; set; }
  }
}