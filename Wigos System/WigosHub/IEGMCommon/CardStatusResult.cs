using System.Data;

namespace IEGMCommon
{
  public class CardStatusResult : GenericResult
  {
    private const int SP_RESPONSE_PARAM3 = 2;
    private const int SP_RESPONSE_PARAM4 = 3;
    public override void Fill(IDataReader DataReader)
    {
      base.Fill(DataReader);
      if (StatusCode != 0) return;
      //CustomerId = DataReader.GetString(SP_RESPONSE_PARAM3);
      //VID = DataReader.GetString(SP_RESPONSE_PARAM4);
    }

    public string CustomerId { get; set; }
    public int CardIndex { get; set; }
    public int AccountType { get; set; }
    public int CardStatus { get; set; }
    public string VID { get; set; }
  }
}