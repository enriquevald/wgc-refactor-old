using System.Collections.Generic;
using Common;

namespace IEGMCommon
{
  public interface IEGMConsumer:IWHUBConsumer
  {
    SessionStartResult StartSession(string VendorId,
      string SerialNumber,
      int MachineNumber,
      long VendorSessionId,
      string TrackData);

    GenericResult EndSession(string VendorId,
      long SessionId,
      decimal AmountPlayed,
      decimal AmountWon,
      int GamesPlayed,
      int GamesWon,
      decimal BillIn,
      decimal CurrentBalance);

    GenericResult UpdateSession(string VendorId,
      long SessionId,
      decimal AmountPlayed,
      decimal AmountWon,
      int GamesPlayed,
      int GamesWon,
      decimal BillIn,
      decimal CurrentBalance);

    CardDetailsResult CardInfo(string TrackData);

    AccountStatusResult AccountGetInfo(string VendorId, string Trackdata);

    AccountDetailsResult AccountGetDetails(string VendorId,string Trackdata);

    GenericResult ReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount,
      long SessionId);

    GenericResult ReportMeters(string VendorId, string SerialNumber, int? MachineNumber, Dictionary<int, long> Meters);
  }
}