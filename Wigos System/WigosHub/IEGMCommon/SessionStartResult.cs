using System.Data;

namespace IEGMCommon
{
  public class SessionStartResult : GenericResult
  {
    private const int SP_RESPONSE_PARAM3 = 2;
    private const int SP_RESPONSE_PARAM4 = 3;
    public override void Fill(IDataReader DataReader)
    {
      base.Fill(DataReader);
      if (StatusCode == 0 || StatusCode == 2)
      {
        SessionId = DataReader.GetInt64(SP_RESPONSE_PARAM3);
        Balance = DataReader.GetDecimal(SP_RESPONSE_PARAM4);
      }
    }

    public long SessionId { get; set; }
    public decimal Balance { get; set; }
    public decimal AmountCashable { get; set; }
    public decimal AmountRestricted { get; set; }
    public decimal AmountNonRestricted { get; set; } 
  }
}