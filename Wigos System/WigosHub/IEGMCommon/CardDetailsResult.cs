using System;
using System.Data;

namespace IEGMCommon
{
  public class CardDetailsResult : GenericResult
  {
    public override void Fill(IDataReader DataReader)
    {
      base.Fill(DataReader);
      try
      {
        //CustomerId = DataReader.GetString(DataReader.GetOrdinal("CustomerId"));
        CustomerId = DataReader.GetInt64(DataReader.GetOrdinal("CustomerId"));
      }
      catch
      {
        //swallow
      }
      try
      {
        CardIndex = DataReader.GetInt32(DataReader.GetOrdinal("CardIndex"));
      }
      catch
      {
        //swallow
      }
      try
      {

        AccountType = DataReader.GetInt32(DataReader.GetOrdinal("AccountType"));
      }
      catch
      {
        //swallow
      }
      try
      {
        CardStatus = DataReader.GetInt32(DataReader.GetOrdinal("CardStatus"));
      }
      catch
      {
        //swallow
      }
      //Balance = DataReader.GetDecimal(SP_RESPONSE_PARAM3);
      //VID = DataReader.GetString(SP_RESPONSE_PARAM4);
    }


    public Int64 CustomerId { get; set; }
    public int CardIndex { get; set; }
    public int AccountType { get; set; }
    public int CardStatus { get; set; }
  }
}