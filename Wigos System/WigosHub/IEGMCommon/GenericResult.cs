﻿using System.Data;

namespace IEGMCommon
{
  #region " Result Classes "

  public interface IGenericResult
  {
    int StatusCode { get; set; }

    string StatusText { get; set; }

    string LogMessage { get; set; }


    bool IsError { get; set; }
  }

  public class GenericResult : IGenericResult
  {
    private const int SP_RESPONSE_STATUS = 0;
    private const int SP_RESPONSE_TEXT = 1;
    public virtual void Fill(IDataReader DataReader)
    {
      StatusCode = DataReader.GetInt32(SP_RESPONSE_STATUS);
      StatusText = DataReader.GetString(SP_RESPONSE_TEXT);
      IsError = (StatusCode != 0);
    }
  
    public int StatusCode { get; set; }    
    public string StatusText { get; set; }
    public override string ToString()
    {
      return string.Format("{0}:{1}", StatusCode, StatusText);
    }


    public string LogMessage { get; set; }


    public bool IsError { get; set; }
  }

  #endregion

}
