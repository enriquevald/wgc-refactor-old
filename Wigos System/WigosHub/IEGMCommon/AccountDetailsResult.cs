using System.Data;

namespace IEGMCommon
{
  public class AccountDetailsResult : GenericResult
  {
    public override void Fill(IDataReader DataReader)
    {
      base.Fill(DataReader);
      if (StatusCode != 0) return;
      AccountNumber = DataReader.GetInt64(DataReader.GetOrdinal("AccountNumber"));
      Name = DataReader.GetString(DataReader.GetOrdinal("Name"));
      FirstSurname = DataReader.GetString(DataReader.GetOrdinal("FirstSurname"));
      SecondSurname = DataReader.GetString(DataReader.GetOrdinal("SecondSurname"));
      Gender = DataReader.GetString(DataReader.GetOrdinal("Gender"));
      BirthDate = DataReader.GetString(DataReader.GetOrdinal("BirthDate"));
      //Curp = DataReader.GetString(DataReader.GetOrdinal("Curp"));
      Email = DataReader.GetString(DataReader.GetOrdinal("Email"));
      AuxEmail = DataReader.GetString(DataReader.GetOrdinal("AuxEmail"));
      MobileNumber = DataReader.GetString(DataReader.GetOrdinal("MobileNumber"));
      FixedPhone = DataReader.GetString(DataReader.GetOrdinal("FixedPhone"));
      ClientTypeCode = DataReader.GetString(DataReader.GetOrdinal("ClientTypeCode"));
      ClientType = DataReader.GetString(DataReader.GetOrdinal("ClientType"));
      FiscalCode = DataReader.GetString(DataReader.GetOrdinal("FiscalCode"));
      //RFC = DataReader.GetString(DataReader.GetOrdinal("RFC"));
      Nationality = DataReader.GetString(DataReader.GetOrdinal("Nationality"));
      Street = DataReader.GetString(DataReader.GetOrdinal("Street"));
      HouseNumber = DataReader.GetString(DataReader.GetOrdinal("HouseNumber"));
      //DoorNumber = DataReader.GetString(DataReader.GetOrdinal("DoorNumber"));
      Colony = DataReader.GetString(DataReader.GetOrdinal("Colony"));
      //CountyCode = DataReader.GetString(DataReader.GetOrdinal("CountyCode"));
      //Country = DataReader.GetString(DataReader.GetOrdinal("Country"));
      //StateCode = DataReader.GetString(DataReader.GetOrdinal("StateCode"));
      State = DataReader.GetString(DataReader.GetOrdinal("State"));
      //CityCode = DataReader.GetString(DataReader.GetOrdinal("CityCode"));
      City = DataReader.GetString(DataReader.GetOrdinal("City"));
      DocumentType = DataReader.GetString(DataReader.GetOrdinal("DocumentType"));
      DocumentNumber = DataReader.GetString(DataReader.GetOrdinal("DocumentNumber"));
      LevelCode = DataReader.GetString(DataReader.GetOrdinal("LevelCode"));
      Level = DataReader.GetString(DataReader.GetOrdinal("Level"));
      Points = DataReader.GetString(DataReader.GetOrdinal("Points"));
      Document2Type = DataReader.GetString(DataReader.GetOrdinal("Document2Type"));
      Document2Number = DataReader.GetString(DataReader.GetOrdinal("Document2Number"));
      Document3Type = DataReader.GetString(DataReader.GetOrdinal("Document3Type"));
      Document3Number = DataReader.GetString(DataReader.GetOrdinal("Document3Number"));
      //Balance = DataReader.GetDecimal(SP_RESPONSE_PARAM3);
      //VID = DataReader.GetString(SP_RESPONSE_PARAM4);
      CityCode = DataReader.GetString(DataReader.GetOrdinal("CityCode"));
      DocumentTypeCode = DataReader.GetString(DataReader.GetOrdinal("DocumentTypeCode"));
    }


    public long AccountNumber { get; set; }
    public string Name { get; set; }
    public string FirstSurname { get; set; }
    public string SecondSurname { get; set; }
    public string Gender { get; set; }
    public string BirthDate { get; set; }
    //public string BirthStateCode { get; set; }
    //public string BirthState { get; set; }
    //public string Curp { get; set; }
    public string Email { get; set; }
    public string AuxEmail { get; set; }
    public string MobileNumber { get; set; }
    public string FixedPhone { get; set; }
    public string ClientTypeCode { get; set; }
    public string ClientType { get; set; }
    public string FiscalCode { get; set; }
    //public string RFC { get; set; }
    //public string NationalityCode { get; set; }
    public string Nationality { get; set; }
    public string Street { get; set; }
    public string HouseNumber { get; set; }
    //public string DoorNumber { get; set; }
    public string Colony { get; set; }
    //public string CountyCode { get; set; }
    //public string Country { get; set; }
    //public string StateCode { get; set; }
    public string State { get; set; }
    //public string CityCode { get; set; }
    public string City { get; set; }
    public string DocumentType { get; set; }
    public string DocumentNumber { get; set; }
    public string LevelCode { get; set; }
    public string Level { get; set; }
    public string Points { get; set; }
    public string Document2Type { get; set; }
    public string Document2Number { get; set; }
    public string Document3Type { get; set; }
    public string Document3Number { get; set; }
    public string DocumentTypeCode { get; set; }
    public string CityCode { get; set; }
  }
}