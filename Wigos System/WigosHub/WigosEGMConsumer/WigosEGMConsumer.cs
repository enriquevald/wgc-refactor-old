﻿//-----------------------------------------------------------------------------
// Copyright (c) 2016 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME: WigosEGMConsumerService.cs
//
//   DESCRIPTION: 
//                
//
//        AUTHOR: Scott Adamson
//
// CREATION DATE: 18-JUL-2016
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-JUL-2016 SJA    First release.
// 25-JUL-2015 JBP    Implementation of stored procedure calls
// 20-AGO-2016 PDM    PBI 16148:WebService FBM - Adaptar lógica a los métodos de FBM
// 19-DEC-2016 JBP    Task 21699:FBM: Mejorar LOG
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Configuration;
#if WigosHub
using System.Configuration;
#endif
using Common;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Xml;
using IEGMCommon;
using Microsoft.Practices.Unity;
using WigosHubCommon;
#if !WigosHub
using WSI.Common;
#endif
using ILogger = WigosHubCommon.ILogger;


namespace WigosEGMConsumer
{
  public class WigosEGMConsumer : IEGMConsumer
  {
    #region " Constants "

    private const string LOG_MESSAGE_FORMAT = "Call {0} <{1}>, <{2}>";

    #endregion 

    private readonly ILogger m_logger;
#if WigosHub
    private readonly string m_con_str;
#endif
    #region " Public Functions "

    /// <summary>
    /// Start session
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="VendorSessionId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SessionStartResult</returns>
    public SessionStartResult StartSession(string VendorId, string SerialNumber, int MachineNumber, long VendorSessionId,
      string TrackData)
    {
      return
        ExecuteSP<SessionStartResult>(
          GetSPStartSessionCommand(VendorId, SerialNumber, MachineNumber, VendorSessionId,
            TrackData)
          );
    }


    public AccountDetailsResult AccountGetDetails(string VendorId, string TrackData)
    {
      return
       ExecuteSP<AccountDetailsResult>(
         GetSPAccountDetailsCommand("S2S_AccountInfo", VendorId,TrackData)
         );
    }


    /// <summary>
    /// Update session
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="BillIn"></param>
    /// <param name="CurrentBalance"></param>
    /// <returns>GenericResult</returns>
    public GenericResult UpdateSession(string VendorId
      , long SessionId
      , decimal AmountPlayed
      , decimal AmountWon
      , int GamesPlayed
      , int GamesWon
      , decimal BillIn
      , decimal CurrentBalance)
    {
      return
        ExecuteSP<GenericResult>(
          GetSPSessionCommand("S2S_UpdateCardSession", VendorId, SessionId, AmountPlayed,
            AmountWon, GamesPlayed, GamesWon,
            CurrentBalance, BillIn)
          );

    }

    /// <summary>
    /// End session
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="BillIn"></param>
    /// <param name="CurrentBalance"></param>
    /// <returns>GenericResult</returns>
    public GenericResult EndSession(string VendorId
      , long SessionId
      , decimal AmountPlayed
      , decimal AmountWon
      , int GamesPlayed
      , int GamesWon
      , decimal BillIn
      , decimal CurrentBalance)
    {
      return
        ExecuteSP<GenericResult>(
          GetSPSessionCommand("S2S_EndCardSession", VendorId, SessionId, AmountPlayed, AmountWon,
            GamesPlayed, GamesWon,
            CurrentBalance, BillIn)
          );
    }

    public CardDetailsResult CardInfo(string TrackData)
    {
      return
        ExecuteSP<CardDetailsResult>(
          GetSPGetCardInfo(TrackData)
          );
    }

    /// <summary>
    /// Account get info
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="TrackData"></param>
    /// <returns>AccountStatusResult</returns>
    public AccountStatusResult AccountGetInfo(string VendorId, string TrackData)
    {
      return ExecuteSP<AccountStatusResult>(GetSPAccountGetInfo(VendorId, TrackData));
    }

    /// <summary>
    /// Report event
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="EventId"></param>
    /// <param name="Amount"></param>
    /// <param name="SessionId"></param>
    /// <returns>GenericResult</returns>
    public GenericResult ReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId,
      decimal Amount, long SessionId)
    {
      return ExecuteSP<GenericResult>(
        GetSPReportEvent(VendorId, SerialNumber, MachineNumber, EventId, Amount, SessionId)
        );
    }

    /// <summary>
    /// Report meters
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="Meters"></param>
    /// <returns>GenericResult</returns>
    public GenericResult ReportMeters(string VendorId, string SerialNumber, int? MachineNumber,
      Dictionary<int, long> Meters)
    {
      return ExecuteSP<GenericResult>(
        GetSPReportMeters(VendorId, SerialNumber, MachineNumber, Meters)
        );
    }

    #endregion


    #region " Private Functions "

    #region " Stored Procedure Commands "

    /// <summary>
    /// Stored procedure command of 'StartSession'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="VendorSessionId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SqlCommand</returns>
    private IDbCommand GetSPStartSessionCommand(string VendorId, string SerialNumber, int MachineNumber,
      long VendorSessionId, string TrackData)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = "S2S_SessionStart",
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pTrackData", DbType.String, TrackData));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pSerialNumber", DbType.String, SerialNumber));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pMachineNumber", DbType.Int32, MachineNumber));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorId", DbType.String, VendorId));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorSessionId", DbType.Int64, VendorSessionId));

      return _sql_command;
    }

    private IDataParameter CreateParameter(IDbCommand Cmd, string ParamName, DbType DBType, object Value)
    {
      IDataParameter _param = Cmd.CreateParameter();

      _param.DbType = DBType;
      _param.ParameterName = ParamName;
      _param.Value = Value;

      return _param;

    }

    private IDbCommand GetSPAccountDetailsCommand(string ProcedureName
      , string VendorId, string TrackData)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = ProcedureName,
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@TrackData", DbType.String, TrackData));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@VendorId", DbType.String, VendorId));
      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'UpdateSession' or 'EndSession'
    /// </summary>
    /// <param name="ProcedureName"></param>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="CreditBalance"></param>
    /// <param name="Billin"></param>
    /// <returns>SqlCommand</returns>
    private IDbCommand GetSPSessionCommand(string ProcedureName
      , string VendorId
      , long SessionId
      , decimal AmountPlayed
      , decimal AmountWon
      , int GamesPlayed
      , int GamesWon
      , decimal CreditBalance
      , decimal Billin)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = ProcedureName,
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorId", DbType.String, VendorId));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pSessionId", DbType.Int64, SessionId));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pAmountPlayed", DbType.Currency, AmountPlayed));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pAmountWon", DbType.Currency, AmountWon));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pGamesPlayed", DbType.Int32, GamesPlayed));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pGamesWon", DbType.Int32, GamesWon));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pBillIn", DbType.Currency, Billin));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pCurrentBalance", DbType.Currency, CreditBalance));

      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'AccountGetInfo'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SqlCommand</returns>
    private IDbCommand GetSPAccountGetInfo(string VendorId, string TrackData)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = "S2S_AccountStatus",
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pTrackdata", DbType.String, TrackData));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorId", DbType.String, VendorId));

      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'SendEvent'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="EventId"></param>
    /// <param name="Amount"></param>
    /// <param name="SessionID"></param>
    /// <returns>SqlCommand</returns>
    private IDbCommand GetSPReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId,
      decimal Amount, long SessionID)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = "s2s_ReportEvent",
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorId", DbType.String, VendorId));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pSerialNumber", DbType.String, SerialNumber));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pMachineNumber", DbType.Int32, MachineNumber));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pEventID", DbType.Int64, EventId));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pAmount", DbType.Currency, Amount));
      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pSessionId", DbType.Int64, SessionID));

      return _sql_command;
    }

    /// <summary>
    /// Get Card Info
    /// </summary>
    /// <param name="TrackData"></param>
    /// <returns></returns>
    private IDbCommand GetSPGetCardInfo(string TrackData)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText= "S2S_CardInfo",
        CommandType = CommandType.StoredProcedure
      };

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@Trackdata", DbType.String, TrackData));

      return _sql_command;
    }


    /// <summary>
    /// Stored procedure command of 'ReportMeters'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="Meters"></param>
    /// <returns>SqlCommand</returns>
    private IDbCommand GetSPReportMeters(string VendorId, string SerialNumber, int? MachineNumber,
      Dictionary<int, long> Meters)
    {
      IDbCommand _sql_command;

      _sql_command = new SqlCommand
      {
        CommandText = "s2s_ReportMeters",
        CommandType = CommandType.StoredProcedure
      };


      if (!string.IsNullOrEmpty(VendorId) && !string.IsNullOrEmpty(SerialNumber))
      {
        _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pVendorId", DbType.String, VendorId));
        _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pSerialNumber", DbType.String, SerialNumber));
      }
      else if (MachineNumber.HasValue)
      {
        _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pMachineNumber", DbType.Int32, MachineNumber));
      }
      else
      {
        m_logger.Log(LogLevel.Exception, "insufficient identifier parameters passed to GetSPReportMeters");
        throw new Exception("insufficient parameters passed to GetSPReportMeters");
      }

      _sql_command.Parameters.Add(CreateParameter(_sql_command, "@pXmlMeters", DbType.Xml,
        BuildMetersXml(Meters).InnerXml));
      m_logger.Log(LogLevel.Info, "Created procedure s2s_ReportMeters");

      return _sql_command;
    }

    private XmlDocument BuildMetersXml(Dictionary<int, long> Meters)
    {
      XmlDocument _xml_dom;

      _xml_dom = new XmlDocument();
      XmlElement _meters = _xml_dom.CreateElement("Meters");

      foreach (var _meter_data in Meters)
      {
        XmlElement _meter = _xml_dom.CreateElement("Meter");
        XmlAttribute _attr_id = _xml_dom.CreateAttribute("Id");
        _attr_id.Value = _meter_data.Key.ToString(CultureInfo.InvariantCulture);
        XmlAttribute _attr_value = _xml_dom.CreateAttribute("Value");
        _attr_value.Value = _meter_data.Value.ToString(CultureInfo.InvariantCulture);
        _meter.Attributes.Append(_attr_id);
        _meter.Attributes.Append(_attr_value);
        _meters.AppendChild(_meter);
      }
      _xml_dom.AppendChild(_meters);

      return _xml_dom;
    }

    #endregion

    #region " Stored Procedure Executions "

    /// <summary>
    /// Execute procedure
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <returns>AccountStatusResult</returns>
    private T ExecuteSP<T>(IDbCommand SqlCmd) where T : GenericResult, new()
    {
      T _result;
      _result = new T
      {
        StatusCode = (Int32)MultiPromos.EndSessionStatus.FatalError,
        StatusText = "ERROR",
        IsError = true
      };

      try
      {
#if WigosHub
        using (
          IDbConnection _con =
            new SqlConnection(m_con_str))
        {
          _con.Open();
          using (var _trx = _con.BeginTransaction())
          {
            SqlCmd.Connection = _con;
            SqlCmd.Transaction = _trx;
            using (IDataReader _dr = SqlCmd.ExecuteReader())
            {
              if (_dr.Read())
              {
                _result.Fill(_dr);
              }
            }
            _trx.Commit();
          }
        }
#else
          using (var _trx = new DB_TRX())
          {
            SqlCmd.Connection = _trx.SqlTransaction.Connection;
            SqlCmd.Transaction = _trx.SqlTransaction;
            using (IDataReader _dr = SqlCmd.ExecuteReader())
            {
              if (_dr.Read())
              {
                _result.Fill(_dr);
              }
            }
            _trx.Commit();
          }
#endif
      }
      catch (Exception _ex)
      {
        _result.StatusText = "!!! " + _ex.Message;
      }
      finally
      {
        _result.LogMessage = GetLogMessage(_result.StatusCode, _result.StatusText, SqlCmd);
      }       

      return _result;
    }

    #endregion
    
    /// <summary>
    /// Get log message
    /// </summary>
    /// <param name="Result"></param>
    /// <param name="SqlCmd"></param>
    /// <returns></returns>
    private String GetLogMessage(int StatusCode, String StatusText, IDbCommand SqlCmd)
    {
      String _log_message;
      String _param_values;
      String _result_value;

      _log_message = String.Empty;
      _param_values = String.Empty;

      _result_value = String.Format("result = {0} : {1}", StatusCode, StatusText);

      // Get param values
      foreach (SqlParameter _param in SqlCmd.Parameters)
      {
        if (!String.IsNullOrEmpty(_param_values))
        {
          _param_values += ",";
        }

        _param_values += _param.Value;
      }

      return String.Format(LOG_MESSAGE_FORMAT, SqlCmd.CommandText, _param_values, _result_value);
    }

    #endregion




    public WigosEGMConsumer(ILogger Logger)
    {
      m_logger = Logger;
#if WigosHub
      m_con_str =
        string.Format(ConfigurationManager.ConnectionStrings["DBConnection"].ToString(),
          ConfigurationManager.AppSettings["DBPrincipal"], ConfigurationManager.AppSettings["DBMirror"],
          int.Parse(ConfigurationManager.AppSettings["DBId"]));
#endif

    }

    public static void Init(IUnityContainer Container)
    {
      Container.RegisterType<IWHUBProcessor, WigosEGMConsumer>(
        "WigosEGMConsumer", new ContainerControlledLifetimeManager()
        );
    }

    public Dictionary<string, string> Query()
    {
      Assembly _asm = Assembly.GetExecutingAssembly();
      AssemblyName _asm_info = _asm.GetName();
      return new Dictionary<string, string>()
      {
        {
          "ModuleState", "Loaded"
        },
        {
          "Name", _asm_info.Name
        },
        {
          "Location", _asm.Location
        },
        {
          "Version", _asm_info.Version.ToString(4)
        }
      };
    }


    public void Kill()
    {
      //nothing to do
    }


  }
}
