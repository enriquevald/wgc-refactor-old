﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SendEvent')
	DROP PROCEDURE zsp_SendEvent
GO

--------------------------------------------------------------------------------
-- PURPOSE: Send Event
-- 
--  PARAMS:
--      - INPUT:
--         @AccountID       varchar(24)
--         @VendorId        varchar(16)
--         @SerialNumber    varchar(30)
--         @MachineNumber   int
--         @EventID         bigint
--         @Amount          money
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
CREATE PROCEDURE [dbo].[zsp_SendEvent]
  @AccountID       varchar(24),
  @VendorId        varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int,
  @EventID         bigint, 
  @Amount          money
WITH EXECUTE AS OWNER
AS
BEGIN

  BEGIN TRANSACTION
  
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)

  SET @status_code = 4
  SET @status_text = 'Access Denied'
  SET @error_text  = ''

  BEGIN TRY    

    EXECUTE dbo.Trx_3GS_SendEvent @AccountID,
                                  @VendorId, @SerialNumber, @MachineNumber,                                       
                                  @EventID, @Amount,
                                  @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

    GOTO LABEL_AUDIT  

  END TRY

  BEGIN CATCH

    SET @status_code = 4;
    SET @status_text = 'Access Denied';
    SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))

  END CATCH
    
LABEL_ERROR:

    ROLLBACK TRANSACTION
    BEGIN TRANSACTION

LABEL_AUDIT: 
   
  DECLARE @input AS nvarchar(MAX)
  DECLARE @output AS nvarchar(MAX)
            
  SET @input = '@AccountID='     + @AccountID
             +';@VendorId='      + @VendorId
             +';@SerialNumber='  + CAST (@SerialNumber AS nvarchar)
             +';@MachineNumber=' + CAST (@MachineNumber AS nvarchar)
             +';@EventID='       + CAST (@EventID AS nvarchar)
             +';@Amount='        + CAST (@Amount AS nvarchar)               

  IF @error_text <> ''
    SET @error_text = ';Details='    + @error_text

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              + @error_text;
         
  EXECUTE dbo.zsp_Audit 'zsp_SendEvent', @AccountID, @VendorId , @SerialNumber, @MachineNumber, NULL,  
                                         @status_code, NULL, 1, @input, @output
                        
  COMMIT TRANSACTION
     
  SELECT @status_code AS StatusCode, @status_text AS StatusText
  
END -- zsp_SendEvent