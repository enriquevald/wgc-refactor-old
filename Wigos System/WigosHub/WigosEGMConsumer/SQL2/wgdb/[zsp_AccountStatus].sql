﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_AccountStatus')
	DROP PROCEDURE zsp_AccountStatus
GO
CREATE PROCEDURE [dbo].[zsp_AccountStatus]
  @AccountID       varchar(24),
  @VendorId        varchar(16),
  @MachineNumber   int
WITH EXECUTE AS OWNER
AS
BEGIN

  BEGIN TRANSACTION

  DECLARE @balance      money
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  
  SET @balance      = 0
  SET @status_code  = 4
  SET @status_text  = 'Access Denied'
  SET @error_text   = ''
  
  BEGIN TRY

    EXECUTE dbo.Trx_3GS_AccountStatus @AccountID,
                                      @VendorId, @MachineNumber,                                       
                                      @balance OUTPUT,
                                      @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
    GOTO LABEL_AUDIT

  END TRY

  BEGIN CATCH

    SET @status_code = 4;
    SET @status_text = 'Access Denied';
    SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))

  END CATCH
     
LABEL_ERROR:
  SET @balance = 0

  ROLLBACK TRANSACTION
  BEGIN TRANSACTION

LABEL_AUDIT:

  DECLARE @input  AS nvarchar(MAX)
  DECLARE @output AS nvarchar(MAX)

  SET @input = '@AccountID='     + @AccountID
             +';@VendorId='      + @VendorId
             +';@MachineNumber=' + CAST (@MachineNumber AS NVARCHAR)         

  IF @error_text <> ''
    SET @error_text = ';Details='    + @error_text

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              +';AcctBalance='   + CAST (@balance AS NVARCHAR)
              +';VID=0'
              + @error_text

  EXECUTE dbo.zsp_Audit 'zsp_AccountStatus', @AccountID, @VendorId, NULL, @MachineNumber, NULL,
                                             @status_code, NULL, 1, @input, @output

  COMMIT TRANSACTION
  
  SELECT @status_code AS StatusCode, @status_text AS StatusText, @balance AS Balance, '0' AS VID

END -- zsp_AccountStatus