﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionStart')
	DROP PROCEDURE zsp_SessionStart
GO
/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- PURPOSE: Start play session
--
--  PARAMS:
--      - INPUT:
--         @AccountID        varchar(24)
--         @VendorId        varchar(16)
--         @SerialNumber    varchar(30)
--         @MachineNumber   int
--         @CurrentJackpot  money = 0
--         @VendorSessionID bigint = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--      SessionID
--      Balance
--
--   NOTES:
--
CREATE PROCEDURE [dbo].[zsp_SessionStart]
  @AccountID       varchar(24),
  @VendorId        varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int,
  @CurrentJackpot  money = 0,
  @VendorSessionID bigint = 0
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @session_id   bigint
  DECLARE @balance      money
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''
    SET @session_id   = 0
    SET @balance      = 0

    BEGIN TRY

      SET @_try = @_try + 1

      EXECUTE dbo.Trx_3GS_StartCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @session_id OUTPUT, @balance OUTPUT,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      SET @session_id = 0
      SET @balance    = 0
  
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='       + @AccountID
             +';@VendorId='        + @VendorId
             +';@SerialNumber='    + CAST (@SerialNumber    AS NVARCHAR)
             +';@MachineNumber='   + CAST (@MachineNumber   AS NVARCHAR)
             +';@CurrentJackpot='  + CAST (@CurrentJackpot  AS NVARCHAR)
             +';@VendorSessionID=' + CAST (@VendorSessionID AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text

  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              +';SessionID='       + CAST(@session_id       AS NVARCHAR)
              +';Balance='         + CAST(@balance          AS NVARCHAR)
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionStart', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                            @session_id, @status_code, @balance, 1,
                                            @input, @output

  COMMIT TRANSACTION

  SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart