﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_ReportMeters')
	DROP PROCEDURE zsp_ReportMeters
GO
CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16) = null,
  @pSerialNumber varchar(30) = null,
  @pMachineNumber int= null,
  @pXmlMeters Xml
 
AS

  declare @terminal_id int = 0
  declare @status_code int = 0
  declare @status_text varchar(254)
  declare @exception_info varchar(254)=''
  select @terminal_id =t3gs_terminal_id from TERMINALS_3GS where (t3gs_machine_number = @pMachineNumber) or (t3gs_vendor_id = @pVendorId and t3gs_serial_number = @pSerialNumber)
  if(@terminal_id<1)
    set @status_code = 1
  else
    begin
      begin transaction
      begin try

        DECLARE @SummaryOfChanges TABLE(Change VARCHAR(20));  

        MERGE INTO terminal_sas_meters AS Target 
          USING (select @terminal_id as terminal_id, meters.meter.value('(@Id)[1]', 'int') as metercode, 
                    meters.meter.value('(@Value)[1]', 'bigint') as metervalue, 
                        case meters.meter.value('(@Id)[1]', 'int')
                          when 66 then 5
                          when 67 then 10
                          when 68 then 20
                          when 70 then 50
                          when 71 then 100
                          when 72 then 200
                          when 74 then 500
                          when 75 then 1000
                          else 0.000
                        end as denom
                      FROM @pXmlMeters.nodes('Meters/Meter') meters(meter))
              AS Source (terminal_id, metercode, metervalue, denom)
            ON Target.tsm_terminal_id = Source.terminal_id AND Target.tsm_meter_code = Source.metercode AND Target.tsm_denomination = Source.denom
          WHEN MATCHED THEN
            UPDATE SET Target.tsm_meter_value = Source.metervalue, Target.tsm_last_reported = GETDATE()
          WHEN NOT MATCHED BY Target THEN 
            INSERT ([tsm_terminal_id], [tsm_meter_code],  [tsm_game_id], [tsm_denomination],[tsm_wcp_sequence_id],
              [tsm_last_reported], [tsm_last_modified], [tsm_meter_value], [tsm_meter_max_value], [tsm_delta_value],
              [tsm_raw_delta_value], [tsm_delta_updating], [tsm_sas_accounting_denom])
            VALUES (terminal_id, metercode, 0, denom, 0,
              getdate(), null, metervalue, 9999999999, 0,
              0, 0, null)
          OUTPUT $action into @SummaryOfChanges;

        set @status_code=0
        commit transaction 
      end try
      begin catch
        rollback transaction
        set @exception_info= ' (ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                       + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                       + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                       + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                       + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                       + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
                       + ')'
        set @status_code=4
      end catch
     end

  set @status_text = case @status_code 
                       when 0 then 'OK'
                       when 1 then 'Numero de terminal invalido'
                       when 4 then 'Acceso denegado'
                       else 'Unknown' 
                     end
  
  DECLARE @input AS nvarchar(MAX)

  set @input = CAST( @pXmlMeters as nvarchar(max));
  DECLARE @output AS nvarchar(MAX)

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              + @exception_info;
         
  EXECUTE dbo.zsp_Audit 'zsp_ReportMeters', '', @pVendorId , @pSerialNumber, @terminal_id, NULL,  
                                         @status_code, NULL, 1, @input , @output


  select @status_code as StatusCode, @status_text  as StatusText