IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_SessionStart')
	DROP PROCEDURE S2S_SessionStart
GO

CREATE PROCEDURE S2S_SessionStart
  @pTrackData        varchar(24),
  @pVendorID        varchar(16),
  @pSerialNumber    varchar(30),
  @pMachineNumber   int,
  @pVendorSessionID bigint
AS
BEGIN

  DECLARE @CurrentJackPot money
  SET @CurrentJackPot = 0

  EXECUTE zsp_SessionStart @pTrackData, @pVendorID, @pSerialNumber, @pMachineNumber, @CurrentJackPot, @pVendorSessionID
    
END

	