﻿ IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_ReportMeters')
	DROP PROCEDURE S2S_ReportMeters
GO
CREATE PROCEDURE [dbo].[S2S_ReportMeters]
@pVendorId NvarChar(50)=null,
@pSerialNumber NvarChar(50)= null,
@pMachineNumber int= null,
@pXmlMeters Xml
AS
execute zsp_ReportMeters @pVendorId=@pVendorId ,
                      @pSerialNumber=@pSerialNumber ,
                      @pMachineNumber=@pMachineNumber ,
                      @pXmlMeters=@pXmlMeters
