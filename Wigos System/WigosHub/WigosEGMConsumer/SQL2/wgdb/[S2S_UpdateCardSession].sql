﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_UpdateCardSession')
	DROP PROCEDURE S2S_UpdateCardSession
GO
CREATE PROCEDURE S2S_UpdateCardSession
	@pVendorId			varchar(16),
	@pSessionId			bigint,
	@pAmountPlayed		money,
	@pAmountWon			money,
	@pGamesPlayed		int,
	@pGamesWon			int,
	@pBillIn			money,
	@pCurrentBalance	money
AS
BEGIN

	DECLARE @TrackData	varchar(24)
	DECLARE @SerialNumber varchar(30)
	DECLARE @MachineNumber int  
	SET @MachineNumber = 0

	select @TrackData = dbo.TrackDataToExternal(a.ac_track_data), @SerialNumber = t.te_serial_number from play_sessions p
	inner join terminals t on p.ps_terminal_id= t.te_terminal_id
	inner join accounts a on a.ac_account_id=p.ps_account_id
	where p.ps_play_session_id = @pSessionId

	-- @AccountId
	--,@VendorId	OK
	--,@SerialNumber
	--,@MachineNumber	OK
	--,@SessionId	OK
	--,@AmountPlayed	OK
	--,@AmountWon	OK
	--,@GamesPlayed	OK
	--,@GamesWon	OK
	--,@CreditBalance	@pCurrentBalance??
	--,@CurrentJackpot

  EXECUTE zsp_SessionUpdate  @TrackData, @pVendorId, @SerialNumber, @MachineNumber,@pSessionId,@pAmountPlayed,@pAmountWon,@pGamesPlayed, @pGamesWon,@pCurrentBalance,0,@pBillIn

    
END