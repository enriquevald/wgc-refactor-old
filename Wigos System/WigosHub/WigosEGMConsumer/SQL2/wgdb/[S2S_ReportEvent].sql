﻿ IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_ReportEvent')
	DROP PROCEDURE S2S_ReportEvent
GO
CREATE PROCEDURE [dbo].[S2S_ReportEvent]
  @pVendorId varchar(16),
  @pSerialNumber varchar(30),
  @pMachineNumber int,
  @pEventId int,
  @pAmount money,
  @pSessionId bigint
AS
  declare @AccountId varchar(24)

  select @AccountId = dbo.TrackDataToExternal(a.ac_track_data) 
    from play_sessions p
      inner join accounts a on a.ac_account_id=p.ps_account_id
    where p.ps_play_session_id = @pSessionId
  
  execute zsp_SendEvent @AccountId,
      @pVendorId,
      @pSerialNumber,
      @pMachineNumber,
      @pEventId,
      @pAmount