﻿CREATE PROCEDURE [dbo].[zsp_SendEvent]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pEventID         bigint, 
	@pPayoutAmt       money
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SendEvent  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber  
                                        , @pEventID        
                                        , @pPayoutAmt  
END