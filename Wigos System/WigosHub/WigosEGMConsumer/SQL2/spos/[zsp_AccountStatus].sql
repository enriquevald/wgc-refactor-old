﻿CREATE PROCEDURE [dbo].[zsp_AccountStatus]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pMachineNumber   int
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_AccountStatus  @pAccountID
                                                                   , @pVendorId
                                                                   , @pMachineNumber
END