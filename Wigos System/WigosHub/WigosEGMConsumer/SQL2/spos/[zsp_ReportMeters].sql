﻿CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16),
  @pSerialNumber varchar(30),
  @pMachineNumber int,
  @pXmlMeters xml
  
AS
    execute vscage.wgdb_000.dbo.zsp_ReportMeters 
      @pVendorId ,
      @pSerialNumber ,
      @pMachineNumber ,
      @pXmlMeters
