﻿CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@pAccountID 	    varchar(24),
	@pVendorId 	      varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pCurrentJackpot  money = 0,
	@pVendorSessionId bigint = 0,
	@pGameTitle       varchar(50) = '',
	@pDummy           int = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionStart  @pAccountID
                                       , @pVendorId
                                       , @pSerialNumber
                                       , @pMachineNumber
                                       , @pCurrentJackpot
                                       , @pVendorSessionId
END