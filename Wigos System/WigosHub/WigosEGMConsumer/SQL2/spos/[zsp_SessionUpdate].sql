USE [sPOS]
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionUpdate')
       DROP PROCEDURE zsp_SessionUpdate
GO

/****** Object:  StoredProcedure [dbo].[zsp_SessionUpdate]    Script Date: 16/08/2016 11:33:30 a ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId      bigint,
  @pCreditsPlayed  money,
  @pCreditsWon     money,
  @pGamesPlayed    int,
  @pGamesWon        int,
  @pCreditBalance   money,
  @pCurrentJackpot  money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionUpdate  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber
                                        , @pSessionId 
                                        , @pCreditsPlayed
                                        , @pCreditsWon 
                                        , @pGamesPlayed 
                                        , @pGamesWon  
                                        , @pCreditBalance
                                        , @pCurrentJackpot
										, 0
END
