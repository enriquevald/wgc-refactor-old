using System;
using CommonListenerServiceDefinition;
using IEGMCommon;

namespace WS2S_Listener
{
  public static class ExtentionMethods
  {
    public static CommonResult ToCommonResult(this IGenericResult Result)
    {
      return new CommonResult() { StatusCode = Result.StatusCode, StatusText = Result.StatusText };
    }

    public static CommonResult ToCommonResult(this GenericResult Result)
    {
      return new CommonResult(){StatusCode = Result.StatusCode, StatusText = Result.StatusText};
    }
    public static CommonSessionStartResult ToCommonResult(this SessionStartResult Result)
    {
      return new CommonSessionStartResult() { StatusCode = Result.StatusCode, StatusText = Result.StatusText, SessionId = Result.SessionId, Balance = Result.Balance };
    }

    public static CommonSessionStartResult ToCommonSessionStartResult(this IGenericResult Result)
    {
      SessionStartResult ar = Result as SessionStartResult;
      if (ar != null)
      {
        //DPC --> PROVISIONAL!!!!!
        ar.AmountCashable = 0;
        ar.AmountRestricted = 0;
        ar.AmountNonRestricted = 0;

        return new CommonSessionStartResult()
        {
          StatusCode = ar.StatusCode,
          StatusText = ar.StatusText,
          SessionId = ar.SessionId,
          Balance = ar.Balance,
          IsError = ar.IsError,
          LogMessage = ar.LogMessage,          
          AmountCashable = ar.AmountCashable,
          AmountRestricted = ar.AmountRestricted,
          AmountNonRestricted = ar.AmountNonRestricted
        };
      }
      throw new Exception();

    }

    public static CommonAccountResult ToCommonAccountResult(this AccountStatusResult Result)
    {
      return new CommonAccountResult() { StatusCode = Result.StatusCode, Balance= Result.Balance, StatusText = Result.StatusText, VID = Result.VID};
    }
    public static CommonAccountResult ToCommonAccountResult(this IGenericResult Result)
    {
      AccountStatusResult ar = Result as AccountStatusResult;
      if (ar != null)
      {
        return new CommonAccountResult()
        {
          StatusCode = ar.StatusCode,
          Balance = ar.Balance,
          StatusText = Result.StatusText,
          VID = ar.VID,
          IsError = ar.IsError,
          LogMessage = ar.LogMessage
        };
      }
      throw new Exception();
    }
    
    public static CommonCardResult ToCommonCardResult(this IGenericResult Result)
    {
      IEGMCommon.CardDetailsResult ar = Result as IEGMCommon.CardDetailsResult;
      if (ar != null)
      {
        return new CommonCardResult()
        {
          CardIndex = ar.CardIndex,
          CustomerId = ar.CustomerId,
          AccountType = ar.AccountType,
          CardStatus = ar.CardStatus,
          IsError = ar.IsError,
          LogMessage = ar.LogMessage
        };
      }
      throw new Exception();
    }

    public static CommonAccountDetailsResult ToCommonAccountDetailsResult(this IGenericResult Result)
    {
      AccountDetailsResult ad = Result as AccountDetailsResult;
      if (ad != null)
      {
        return new CommonAccountDetailsResult()
        {
          StatusCode = ad.StatusCode,
          StatusText = Result.StatusText,
          IsError = ad.IsError,
          LogMessage = ad.LogMessage,
          AccountNumber = ad.AccountNumber,
          Name = ad.Name,
          FirstSurname = ad.FirstSurname,
          SecondSurname = ad.SecondSurname,
          Gender = ad.Gender,
          BirthDate = ad.BirthDate,
          //Curp = ad.Curp,
          Email = ad.Email,
          AuxEmail = ad.AuxEmail,
          MobileNumber = ad.MobileNumber,
          FixedPhone = ad.FixedPhone,
          ClientTypeCode = ad.ClientTypeCode,
          ClientType = ad.ClientType,
          FiscalCode = ad.FiscalCode,
          //RFC = ad.RFC,
          Nationality = ad.Nationality,
          Street = ad.Street,
          HouseNumber = ad.HouseNumber,
          Colony = ad.Colony,
          State = ad.State,
          City = ad.City,
          DocumentType = ad.DocumentType,
          DocumentNumber = ad.DocumentNumber,
          Document2Type = ad.Document2Type,
          Document2Number = ad.Document2Number,
          Document3Type = ad.Document3Type,
          Document3Number = ad.Document3Number,
          LevelCode = ad.LevelCode,
          Level = ad.Level,
          Points= ad.Points,
          CityCode = ad.CityCode,
          DocumentTypeCode = ad.DocumentTypeCode

        };
      }
      throw new Exception();
    }
  }
}