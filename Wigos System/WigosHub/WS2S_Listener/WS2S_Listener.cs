﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using Common;
using CommonListenerServiceDefinition;
using IEGMCommon;
using Microsoft.Practices.Unity;
using WigosHubCommon;
#if !WigosHub
using WSI.Common;
#endif
using ILogger = WigosHubCommon.ILogger;


namespace WS2S_Listener
{
  public class WS2S_Listener : IEGMListener
  {
    private ICommonListenerServiceDefinition m_service_listener;
    private readonly ILogger m_logger;
    private ServiceHost m_host_listener;

    public WS2S_Listener(ICommonListenerServiceDefinition ServiceListener, ILogger Logger )
    {
      m_service_listener = ServiceListener;
      m_logger = Logger;
      m_logger.Log(LogLevel.Info, "Starting WS2S_Listener...");
      OpenHostListener();

    }


    private void OpenHostListener()
    {
#if WigosHub
      Uri _base_address = new Uri(ConfigurationManager.AppSettings["WS2S_Listener.HostAddress"]);
#else
      Uri _base_address = new Uri(GeneralParam.GetString("WS2S", "Listener.HostAddress", "http://0.0.0.0:7787"));
#endif
      m_logger.Log(LogLevel.Info, string.Format("WS2S_LISTENER:Starting at {0}",_base_address));
      m_host_listener = new ServiceHost(m_service_listener, _base_address);

      ServiceMetadataBehavior _smb = m_host_listener.Description.Behaviors.Find<ServiceMetadataBehavior>() ??
                                     new ServiceMetadataBehavior();
      // If not, add one
      _smb.HttpGetEnabled = true;
      _smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
      m_host_listener.Description.Behaviors.Add(_smb);

      m_host_listener.AddServiceEndpoint(
        ServiceMetadataBehavior.MexContractName,
        MetadataExchangeBindings.CreateMexHttpBinding(),
        "mex"
        );


      BasicHttpBinding _binding = new BasicHttpBinding();

      m_host_listener.AddServiceEndpoint(typeof (ICommonListenerServiceDefinition), _binding, _base_address);


      m_host_listener.Faulted += host_listener_Faulted;
      m_host_listener.Closed += host_listener_Closed;

      m_host_listener.Open();
      m_logger.Log(LogLevel.Info, string.Format("WS2S_LISTENER:Started"));


    }

    private void host_listener_Faulted(object Sender, EventArgs E)
    {
      m_logger.Log(LogLevel.Warning, string.Format("WS2S_LISTENER:Faulted"));
      m_host_listener.Faulted -= host_listener_Faulted;
      m_host_listener = null;
      OpenHostListener();
    } //host_Faulted

    private void host_listener_Closed(object Sender, EventArgs E)
    {
      m_logger.Log(LogLevel.Warning, string.Format("WS2S_LISTENER:Closed"));
      m_host_listener.Faulted -= host_listener_Closed;
      m_host_listener = null;
      OpenHostListener();
    } //host_Faulted

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    } //Dispose

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing) return;
      if (m_host_listener != null)
        if (m_host_listener.State != CommunicationState.Closed)
        {
          m_host_listener.Faulted -= host_listener_Faulted;
          m_host_listener.Closed -= host_listener_Closed;

          m_host_listener.Close();
                }
      m_host_listener = null;

    } //Dispose

    public static void Init(IUnityContainer Container)
    {
      Container.RegisterType<IWHUBListener, WS2S_Listener>("WS2S_Listener", new ContainerControlledLifetimeManager());
      Container.RegisterType<ICommonListenerServiceDefinition, CommonListenerService>();
    }


    public Dictionary<string, string> Query()
    {
      m_logger.Log(LogLevel.Info, string.Format("WS2S_LISTENER:Queried"));

      return new Dictionary<string, string>()
      {
        {
          "ModuleState", "Loaded"
        },
        {
          "Version", "2"
        },
        {
          "WebServiceAddress",
          m_host_listener != null && m_host_listener.BaseAddresses.Any()
            ? m_host_listener.BaseAddresses.First().ToString()
            : "Not Running"
        },
        {
          "WebServiceState",
          m_host_listener != null ? m_host_listener.State.ToString() : "Unknown"
        }
      };
    }


    public void Kill()
    {
      m_logger.Log(LogLevel.Info, string.Format("WS2S_LISTENER:Killing"));
      if (m_host_listener != null)
        if (m_host_listener.State != CommunicationState.Closed)
        {
          m_host_listener.Faulted -= host_listener_Faulted;
          m_host_listener.Closed -= host_listener_Closed;
          m_host_listener.Close();
        }
      m_host_listener = null;
      m_service_listener = null;
      m_logger.Log(LogLevel.Info, string.Format("WS2S_LISTENER:Killed"));
    }
  }
}
