﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using AccountCommon;
using CommonListenerServiceDefinition;
using EventCommon;
using IEGMCommon;
using MeterCommon;
using SessionCommon;
using WigosHubCommon;
using WSI.Common;

namespace WS2S_Listener
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
  public class CommonListenerService : ICommonListenerServiceDefinition
  {
    #region " Constants "

    private const int LOG_MODE_ALL = 0;
    private const int LOG_MODE_ERROR = 1;
    private const int LOG_MODE_ELAPSED = 2;

    #endregion

    private readonly IInterchangeSession m_session;
    private readonly IInterchangeEvent m_event;
    private readonly IInterchangeMeter m_meter;
    private readonly IInterchangeAccount m_account;
    private readonly WigosHubCommon.ILogger m_logger;
    
    public CommonListenerService(IInterchangeSession Session, IInterchangeEvent Event, IInterchangeMeter Meter,
      IInterchangeAccount Account, WigosHubCommon.ILogger Logger)
    {
      m_session = Session;
      m_event = Event;
      m_meter = Meter;
      m_account = Account;
      m_logger = Logger;
    }
    public CommonAccountDetailsResult WS2S_AccountInfo(string VendorId, string TrackData)
    {
      if (!GetAccountInfoEnabled())
      {
        CommonAccountDetailsResult _return;
        _return = new CommonAccountDetailsResult();

        _return.StatusCode = 0;
        _return.StatusText = "Disabled method";

        return _return;
      }

      IGenericResult v = Exec(() => m_account.GetDetails(VendorId, TrackData));
      return v.ToCommonAccountDetailsResult();
    }
    public CommonSessionStartResult WS2S_StartCardSession(string VendorId, string SerialNumber, int MachineNumber,
      long VendorSessionId, string TrackData)
    {
      IGenericResult v = Exec(() => m_session.Start(VendorId, SerialNumber, MachineNumber, VendorSessionId, TrackData));
      return v.ToCommonSessionStartResult();
    }

    public CommonResult WS2S_EndCardSession(string VendorId, long SessionId, decimal AmountPlayed, decimal AmountWon, int GamesPlayed,
                                            int GamesWon, decimal BillIn, decimal CurrentBalance, decimal AmountCashable,
                                            decimal AmountRestricted, decimal AmountNonRestricted)
    {
      IGenericResult v = Exec(() =>
        m_session.End(VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon, BillIn, CurrentBalance));
      return v.ToCommonResult();
    }

    public CommonResult WS2S_UpdateCardSession(string VendorId, long SessionId, decimal AmountPlayed, decimal AmountWon,
                                               int GamesPlayed, int GamesWon, decimal BillIn, decimal CurrentBalance,
                                               decimal AmountCashable, decimal AmountRestricted, decimal AmountNonRestricted)
    {
      IGenericResult v = Exec(() => m_session.Update(VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon, BillIn, CurrentBalance));
      return v.ToCommonResult();
    }

    public CommonResult WS2S_ReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount, long SessionId)
    {
      IGenericResult v = Exec(() => m_event.Report(VendorId, SerialNumber, MachineNumber, EventId, Amount, SessionId));
      return v.ToCommonResult();
    }

    public CommonResult WS2S_ReportMeters(string VendorId, string SerialNumber, int? MachineNumber, Meters XmlMeters)
    {

      IGenericResult v = Exec(() =>
      {
        var meters = XmlMeters.ToDictionary(_x => _x.Code, _y => _y.Value);
        return m_meter.Report(VendorId, SerialNumber, MachineNumber, meters);
      });

      return v.ToCommonResult();
    }

    public CommonAccountResult WS2S_AccountStatus(string VendorId, string Trackdata)
    {
      IGenericResult v = Exec(() => m_account.GetInfo(VendorId, Trackdata));
      return v.ToCommonAccountResult();
    }

    public CommonCardResult WS2S_CardInfo(string Trackdata)
    {
      IGenericResult v = Exec(() => m_account.GetCardDetails(Trackdata));
      return v.ToCommonCardResult();
    }


    private IGenericResult Exec(Func<GenericResult> Func)
    {
      Int64 _tick;

      _tick = Environment.TickCount;
      IGenericResult v = Func.Invoke();
      _tick = Environment.TickCount - _tick;

      // Write log
      this.WriteLog(v.IsError, v.LogMessage, _tick);

      return v;
    }

    /// <summary>
    /// Write log
    /// </summary>
    /// <param name="IsError"></param>
    /// <param name="LogMessage"></param>
    /// <param name="Tick"></param>
    private void WriteLog(Boolean IsError, String LogMessage, Int64 Tick)
    {
      String _log_elapsed;
      Int32 _log_mode;
      Boolean _is_log_elapsed;

      _log_elapsed = String.Empty;
      _log_mode = this.GetLogMode();

      _is_log_elapsed = (_log_mode != LOG_MODE_ERROR && Tick > GetTimeElapsedWarning());

      if (_log_mode == LOG_MODE_ALL || IsError || _is_log_elapsed)
      {
        if (_is_log_elapsed)
        {
          _log_elapsed = String.Format(", Elapsed = {0}", Tick);
        }

        m_logger.Log(LogLevel.Info, LogMessage + _log_elapsed);
      }
    } // WriteLog

    /// <summary>
    /// Get GP Log mode:
    ///   - 0 = All       |   LOG_MODE_ALL
    ///   - 1 = Error     |   LOG_MODE_ERROR
    ///   - 2 = Elapsed   |   LOG_MODE_ELAPSED
    /// </summary>
    /// <returns></returns>
    private Int32 GetLogMode()
    {
      return GeneralParam.GetInt32("WS2S", "Listener.Log.Mode", 0);
    } // GetLogMode

    /// <summary>
    /// Get time elapsed warnign time
    /// </summary>
    /// <returns></returns>
    private Int32 GetTimeElapsedWarning()
    {
      return GeneralParam.GetInt32("WS2S", "Listener.Warning.TimeElapsed", 100);
    } // GetTimeElapsedWarning

    /// <summary>
    /// Get if AccountInfo methos is enabled
    /// </summary>
    /// <returns></returns>
    private Boolean GetAccountInfoEnabled()
    {
      return GeneralParam.GetBoolean("WS2S", "Listener.AccountInfo.Enabled", false);
    } // GetAccountInfoEnabled
  }
}
