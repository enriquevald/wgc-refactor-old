﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using HubStatusListenerServiceDefinition;
using WigosHubCommon;
using System.Security.Cryptography;
using Ionic.Zip;

namespace HubStatusListener
{
  // Copyright (c) Damien Guard.  All rights reserved.
  // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. 
  // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
  // Originally published at http://damieng.com/blog/2006/08/08/calculating_crc32_in_c_and_net



  /// <summary>
  /// Implements a 32-bit CRC hash algorithm compatible with Zip etc.
  /// </summary>
  /// <remarks>
  /// Crc32 should only be used for backward compatibility with older file formats
  /// and algorithms. It is not secure enough for new applications.
  /// If you need to call multiple times for the same data either use the HashAlgorithm
  /// interface or remember that the result of one Compute call needs to be ~ (XOR) before
  /// being passed in as the seed for the next Compute call.
  /// </remarks>
  public sealed class Crc32 : HashAlgorithm
  {
    public const UInt32 DefaultPolynomial = 0xedb88320u;
    public const UInt32 DefaultSeed = 0xffffffffu;

    private static UInt32[] DefaultTable;

    private readonly UInt32 m_seed;
    private readonly UInt32[] m_table;
    private UInt32 m_hash;

    public Crc32()
      : this(DefaultPolynomial, DefaultSeed)
    {
    }

    public Crc32(UInt32 Polynomial, UInt32 Seed)
    {
      m_table = InitializeTable(Polynomial);
      m_seed = m_hash = Seed;
    }

    public override void Initialize()
    {
      m_hash = m_seed;
    }

    protected override void HashCore(byte[] Array, int IbStart, int CbSize)
    {
      m_hash = CalculateHash(m_table, m_hash, Array, IbStart, CbSize);
    }

    protected override byte[] HashFinal()
    {
      var _hash_buffer = UInt32ToBigEndianBytes(~m_hash);
      HashValue = _hash_buffer;
      return _hash_buffer;
    }

    public override int HashSize
    {
      get { return 32; }
    }

    public static UInt32 Compute(byte[] Buffer)
    {
      return Compute(DefaultSeed, Buffer);
    }

    public static UInt32 Compute(UInt32 Seed, byte[] Buffer)
    {
      return Compute(DefaultPolynomial, Seed, Buffer);
    }

    public static UInt32 Compute(UInt32 Polynomial, UInt32 Seed, byte[] Buffer)
    {
      return ~CalculateHash(InitializeTable(Polynomial), Seed, Buffer, 0, Buffer.Length);
    }

    private static UInt32[] InitializeTable(UInt32 Polynomial)
    {
      if (Polynomial == DefaultPolynomial && DefaultTable != null)
        return DefaultTable;

      var _create_table = new UInt32[256];
      for (var _i = 0; _i < 256; _i++)
      {
        var _entry = (UInt32) _i;
        for (var _j = 0; _j < 8; _j++)
          if ((_entry & 1) == 1)
            _entry = (_entry >> 1) ^ Polynomial;
          else
            _entry = _entry >> 1;
        _create_table[_i] = _entry;
      }

      if (Polynomial == DefaultPolynomial)
        DefaultTable = _create_table;

      return _create_table;
    }

    private static UInt32 CalculateHash(UInt32[] Table, UInt32 Seed, IList<byte> Buffer, int Start, int Size)
    {
      var _hash = Seed;
      for (var _i = Start; _i < Start + Size; _i++)
        _hash = (_hash >> 8) ^ Table[Buffer[_i] ^ _hash & 0xff];
      return _hash;
    }

    private static byte[] UInt32ToBigEndianBytes(UInt32 Uint32)
    {
      var _result = BitConverter.GetBytes(Uint32);

      if (BitConverter.IsLittleEndian)
        Array.Reverse(_result);

      return _result;
    }
  }


  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
  public class HubStatusListenerService : IHubStatusListenerServiceDefinition
  {
    private readonly IFileSystem m_files_system;
    private readonly IFileSystemDirectory m_directory;
    private readonly ILogger m_logger;

    public HubStatusListenerService(IFileSystem FileSystem, IFileSystemDirectory Directory, ILogger Logger)
    {
      m_files_system = FileSystem;
      m_directory = Directory;
      m_logger = Logger;
    }

    public List<string> LoadedModules()
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Requested loaded modules"));

      return IoC.LoadedModules();
    }

    public Dictionary<string, string> QueryModule(string ModuleName)
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Request Query module {0}", ModuleName));

      return IoC.QueryModule(ModuleName);
    }

    public bool DoUpdate(string Crc32)
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Performing Update"));
      
      using (Stream _fsdest = m_files_system.Create("update.zip"))
      {
        using (BinaryWriter _bw = new BinaryWriter(_fsdest))
        {
          foreach (var _s in m_directory.GetFiles("update").OrderBy(X => X))
            using (Stream _fs = m_files_system.Open(_s, FileMode.Open))
            {

              using (BinaryReader _br = new BinaryReader(_fs))
              {
                _bw.Write(_br.ReadBytes((int) _fs.Length));
              }
            }
        }
      }

      Crc32 _crc32 = new Crc32();
      string _hash = String.Empty;

      using (Stream _fs = m_files_system.Open("update.zip", FileMode.Open))
        _hash = _crc32.ComputeHash(_fs).Aggregate(_hash, (_current, B) => _current + B.ToString("x2").ToLower());
      CancelUpdate();

      if (!_hash.Equals(Crc32)) return false;
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Update CRC OK"));

      using (ZipFile _zf = new ZipFile("update.zip"))
      {
        _zf.ExtractAll("extractedupdate");
      }
      m_files_system.Delete("update.zip");
      if (m_files_system.Exists("update.bat"))
      {
        m_files_system.Delete("update.bat");
      }
      using (StreamWriter _writer = m_files_system.CreateText("update.bat"))
      {
        _writer.Write("xcopy extractedupdate\\*.* /s/q/y" + Environment.NewLine);
        _writer.Write("rmdir extractedupdate /s/q" + Environment.NewLine);
      }

      ThreadPool.QueueUserWorkItem(O =>
      {
        ProcessStartInfo _info2 = new ProcessStartInfo
        {
          Arguments = "/C restart.bat \"" + System.Windows.Forms.Application.ExecutablePath + "\"",
          WindowStyle = ProcessWindowStyle.Normal,
          CreateNoWindow = true,
          FileName = "cmd.exe"
        };
        Process.Start(_info2);
        Environment.Exit(0);
      });
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Preparing to restart"));

      return true;
    }

    public bool PushUpdatePart(byte[] IncomingFile, string Crc32, int Part)
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Received Update Part {0}", Part));

      if (!m_directory.Exists("update")) m_directory.CreateDirectory("update");
      string _filename = "update\\" + Part.ToString("00000") + ".upd";
      m_files_system.WriteAllBytes(_filename, IncomingFile);
      Crc32 _crc32 = new Crc32();
      string _hash = String.Empty;
      using (Stream _fs = m_files_system.Open(_filename, FileMode.Open))
        _hash = _crc32.ComputeHash(_fs).Aggregate(_hash, (_current, B) => _current + B.ToString("x2").ToLower());
      if (!_hash.Equals(Crc32))
      {
        m_logger.Log(LogLevel.Error, string.Format("HubStatusListenerService:Update Part CRC invalid"));
        m_files_system.Delete(_filename);
        return false;
      }
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Update Part CRC Ok"));

      return true;
    }


    public bool CancelUpdate()
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Canceling update and removing uploaded parts"));
      if (m_directory.Exists("update"))
      {
        try
        {

          foreach (string _file in m_directory.GetFiles("update"))
          {
            m_files_system.Delete(_file);
          }
          m_directory.Delete("update");
        }
        catch
        {
          m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Canceling update failed could not delete parts"));
          return false;
        }
      }
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListenerService:Canceling update OK deleted uploaded parts"));

      return true;
    }
  }
}
