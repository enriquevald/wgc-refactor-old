﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using Common;
using HubStatusListenerServiceDefinition;
using Microsoft.Practices.Unity;
using WigosHubCommon;

namespace HubStatusListener
{
  public class HubStatusListener : IWHUBListener
  {
    private readonly IHubStatusListenerServiceDefinition m_service_listener;
    private readonly ILogger m_logger;
    private ServiceHost m_host_listener;

    public HubStatusListener(IHubStatusListenerServiceDefinition ServiceListener,ILogger Logger)
    {
      m_service_listener = ServiceListener;
      m_logger = Logger;
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Starting"));

      OpenHostListener();
    }

    private void OpenHostListener()
    {

      Uri _base_address = new Uri(ConfigurationManager.AppSettings["HubStatusListener.HostAddress"]);

      m_host_listener = new ServiceHost(m_service_listener, _base_address);
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Hosting at {0}",_base_address));

      var behavior = m_host_listener.Description.Behaviors.Find<ServiceDebugBehavior>();
      behavior.IncludeExceptionDetailInFaults = true;
      
      ServiceMetadataBehavior _smb = m_host_listener.Description.Behaviors.Find<ServiceMetadataBehavior>() ??
                                     new ServiceMetadataBehavior();
      // If not, add one
      _smb.HttpGetEnabled = true;
      _smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
      
      m_host_listener.Description.Behaviors.Add(_smb);

      m_host_listener.AddServiceEndpoint(
        ServiceMetadataBehavior.MexContractName,
        MetadataExchangeBindings.CreateMexHttpBinding(),
        "mex"
        );


      BasicHttpBinding _binding = new BasicHttpBinding();

      m_host_listener.AddServiceEndpoint(typeof (IHubStatusListenerServiceDefinition), _binding, _base_address);


      m_host_listener.Faulted += host_listener_Faulted;
      m_host_listener.Closed += host_listener_Closed;

      m_host_listener.Open();
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Started"));


    }

    private void host_listener_Faulted(object Sender, EventArgs E)
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Faulted"));
      m_host_listener.Faulted -= host_listener_Faulted;
      m_host_listener = null;
      OpenHostListener();
    } //host_Faulted

    private void host_listener_Closed(object Sender, EventArgs E)
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Closed"));
      m_host_listener.Faulted -= host_listener_Closed;
      m_host_listener = null;
      OpenHostListener();
    } //host_Faulted

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    } //Dispose

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing) return;
      if (m_host_listener != null)
        if (m_host_listener.State != CommunicationState.Closed)
        {
          m_host_listener.Faulted -= host_listener_Faulted;
          m_host_listener.Close();
        }
      m_host_listener = null;

    } //Dispose

    public static void Init(IUnityContainer Container)
    {
      Container.RegisterType<IWHUBListener, HubStatusListener>("HubStatusListener",
        new ContainerControlledLifetimeManager());
      Container.RegisterType<IHubStatusListenerServiceDefinition, HubStatusListenerService>();
    }

    public Dictionary<string, string> Query()
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Queried"));

      return new Dictionary<string, string>()
      {
        {
          "ModuleState", "Loaded"
        },
        {
          "Version", "1"
        },

        {
          "WebServiceAddress",
          m_host_listener != null ? m_host_listener.BaseAddresses.FirstOrDefault().ToString() : "Not Running"
        },
        {
          "WebServiceState",
          m_host_listener != null ? m_host_listener.State.ToString() : "Unknown"
        }
      };
    }


    public void Kill()
    {
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Killing"));

      /// ha ha ha you cannot kill me! i am immortal
      
      m_logger.Log(LogLevel.Info, string.Format("HubStatusListener:Cannot kill"));

    }
  }
}
