using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net.Core;
using log4net.Layout;
using log4net.Util;

namespace WigosHubCommon
{
  public class IndentationPatternConverter : PatternConverter
  {
    protected override void Convert(TextWriter writer, object state)
    {
      // do nothing - %indentation is used for indentation, so nothing should be written
    }
  }

  public class Log4NetLogger : ILogger
  {
    private ILogger m_sublogger = null;
    private LogLevel  m_log_level;

    public Log4NetLogger(Type Host)
    {
      Parent = null;
      m_sublogger = null;
      log4net.LogManager.GetLogger
        (Host);
      log4net.GlobalContext.Properties["environment"] = Environment.MachineName.ToString();

    }


    private readonly log4net.ILog m_log;
    public void Log(LogLevel Level, string Message)
    {
      log4net.GlobalContext.Properties["environment"] = Environment.MachineName.ToString();
      if ((int)Level < (int)LogLevel) return;

      switch (Level)
      {
        case LogLevel.Critical:
          m_log.Fatal(Message);
          break;
        case LogLevel.Exception:
          m_log.Error(Message);
          break;
        case LogLevel.Error:
          m_log.Error(Message);
          break;
        case LogLevel.Warning:
          m_log.Warn(Message);
          break;
        case LogLevel.Info:
          m_log.Info(Message);
          break;
      }

      if (m_sublogger != null)
      {
        m_sublogger.Log(Level, Message);
      }
    }

    public void Exception(Exception _ex)
    {
      log4net.GlobalContext.Properties["environment"] = Environment.MachineName.ToString();
      m_log.Error(_ex);

      if (m_sublogger != null)
      {
        m_sublogger.Exception(_ex);
      }
    }

    public void Chain(ILogger SubLogger)
    {
      m_sublogger.Parent = this;
      m_sublogger = SubLogger;
    }

    public LogLevel LogLevel
    {
      get { return Parent == null ? m_log_level : Parent.LogLevel; }
      set { m_log_level = value; }
    }



    public ILogger Parent { get; set; }

  }
}