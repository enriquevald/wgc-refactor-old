﻿using System;

namespace WigosHubCommon
{
  //------------------------------------------------------------------------------
  // Copyright © 2016 Win Systems International.
  //------------------------------------------------------------------------------
  // 
  //   MODULE NAME : ConsoleLogger.cs
  // 
  //   DESCRIPTION : Console logger implementation
  //
  // REVISION HISTORY:
  // 
  // Date        Author Description
  // ----------- ------ ----------------------------------------------------------
  // 13-JUN-2016 SJA    First release
  //------------------------------------------------------------------------------

  public interface ILogger
  {
    void Log(LogLevel Level, string Message);

    void Exception(Exception _ex);

    void Chain(ILogger SubLogger);

    LogLevel LogLevel { get; set; }
    ILogger Parent { get; set; }
  }
}

