﻿namespace WigosHubCommon
{
  public interface IFileSystemDirectory
  {
    bool Exists(string DirectoryName);

    string[] GetFiles(string DirectoryName);

    void Delete(string DirectoryName);

    void CreateDirectory(string DirectoryName);
  }
}