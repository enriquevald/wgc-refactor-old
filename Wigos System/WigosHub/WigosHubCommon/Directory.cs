﻿namespace WigosHubCommon
{
  public class FileSystemDirectory : IFileSystemDirectory
  {

    public bool Exists(string DirectoryName)
    {
      return System.IO.Directory.Exists(DirectoryName);
    }

    public string[] GetFiles(string DirectoryName)
    {
      return System.IO.Directory.GetFiles(DirectoryName);
    }

    public void Delete(string DirectoryName)
    {
      System.IO.Directory.Delete(DirectoryName);
    }

    public void CreateDirectory(string DirectoryName)
    {
      System.IO.Directory.CreateDirectory(DirectoryName);
    }
  }
}