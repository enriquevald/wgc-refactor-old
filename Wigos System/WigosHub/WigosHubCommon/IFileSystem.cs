﻿using System.IO;

namespace WigosHubCommon
{
  public interface IFileSystem
  {
    Stream Create(string FileName);
    Stream Open(string FileName, FileMode Mode);
    void Delete(string FileName);

    StreamWriter CreateText(string FileName);
    bool Exists(string FileName);

    void WriteAllBytes(string FileName, byte[] Buffer);
  }
}
