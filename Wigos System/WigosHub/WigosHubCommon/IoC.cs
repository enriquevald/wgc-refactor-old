﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : IoC.cs
// 
//   DESCRIPTION : IoC Container
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2016 SJA    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Common;
using Microsoft.Practices.Unity;
//using UnityLog4NetExtension.Log4Net;

namespace WigosHubCommon
{
  public static class IoC
  {

    public static IUnityContainer Container { get; private set; }

    /// <summary>
    /// IoC
    /// </summary>
    static IoC()
    {
      Container = new UnityContainer();
      //Container.AddNewExtension<Log4NetExtension>();
    } //IoC

    /// <summary>
    /// ResetIoC
    /// </summary>
    public static void ResetIoC()
    {
      Container.Dispose();
      Container = new UnityContainer();
    } //ResetIoC

    public static bool LoadAssemblies(string AsmPath, string  ModuleName = "")
    {
      var _path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)??"", AsmPath);
      if (!Directory.Exists(_path)) return false;

      List<Assembly> _assemblies = Directory.GetFiles(_path, "*.dll").Select(Assembly.LoadFile).ToList();
      if (!_assemblies.Any()) return false;
      foreach (
        var _first_or_default in
          _assemblies.Select(
            _assembly =>
              _assembly.GetTypes().FirstOrDefault(
                _x => _x.IsClass && _x.FullName.EndsWith("." + _assembly.GetName().Name))
            )
            .Where(_first_or_default => _first_or_default != null))
      {
        if (string.IsNullOrWhiteSpace(ModuleName) ||
            Path.GetFileNameWithoutExtension(_first_or_default.Assembly.GetName().Name) == ModuleName)
        {

          _first_or_default.GetMethod("Init", BindingFlags.Public | BindingFlags.Static)
            .Invoke(null, new object[] {Container});
        }
      }
      return true;
    }

    public static void RegisterType<T1, T2>(Boolean ContainerLifeTime) where T2 : T1
    {
      RegisterType<T1, T2>(ContainerLifeTime ? new ContainerControlledLifetimeManager() : null);
    }

    /// <summary>
    /// RegisterType
    /// </summary>
    /// <param name="LifetimeManager"></param>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public static void RegisterType<T1, T2>(LifetimeManager LifetimeManager = null) where T2 : T1
    {
      if (LifetimeManager != null)
        Container.RegisterType<T1, T2>(LifetimeManager);
      else
        Container.RegisterType<T1, T2>();

    } //RegisterType

    /// <summary>
    /// Resolve
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T Resolve<T>()
    {
      return Container.Resolve<T>();
    } //Resolve

    public static void RegisterType<T1, T2>(string Key,
      ContainerControlledLifetimeManager ContainerControlledLifetimeManager) where T2 : T1
    {
      Container.RegisterType<T1, T2>(Key, ContainerControlledLifetimeManager);
    }

    public static List<string> LoadedModules()
    {
      return Container.Registrations.Where(_x => !string.IsNullOrWhiteSpace(_x.Name)).Select(_x => _x.Name + " {"+  _x.MappedToType.Assembly.FullName + "}").ToList();
    }

    public static Dictionary<string, string> QueryModule(string ModuleName)
    {
      var _module = Container.Registrations.FirstOrDefault(_x => !string.IsNullOrWhiteSpace(_x.Name) && _x.Name == ModuleName);
      if (_module == null)
        return new Dictionary<string, string>() {{"Error", "Module " + ModuleName + " Not Loaded"}};
      return ((IWHUBModule) _module.LifetimeManager.GetValue()).Query();

    }
  }
}

