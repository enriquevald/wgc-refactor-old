﻿using System.IO;

namespace WigosHubCommon
{
  public class FileSystem:IFileSystem
  {
    public Stream Create(string FileName)
    {
      return File.Create(FileName);
    }

    public Stream Open(string FileName, FileMode Mode)
    {
      return File.Open(FileName, Mode);
    }

    public void Delete(string FileName)
    {
      File.Delete(FileName);
    }

    public StreamWriter CreateText(string FileName)
    {
      return File.CreateText(FileName);
    }

    public bool Exists(string FileName)
    {
      return File.Exists(FileName);
    }

    public void WriteAllBytes(string FileName, byte[] Buffer)
    {
      File.WriteAllBytes(FileName,Buffer);
    }
  }
}
