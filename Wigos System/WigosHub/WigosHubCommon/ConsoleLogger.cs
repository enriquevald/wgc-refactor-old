﻿using System;

namespace WigosHubCommon
{
  public class ConsoleLogger : ILogger
  {
    #region " Members "

    public LogLevel LogLevel
    {
      get { return Parent == null ? m_log_level : Parent.LogLevel; }
      set { m_log_level = value; }
    }

    private ILogger m_sublogger;
    private LogLevel m_log_level;

    #endregion

    #region " Constructor "

    public ConsoleLogger()
    {
      Parent = null;
      m_sublogger = null;
    }

    #endregion

    #region " Properties "

    #endregion

    #region " Public functions "


    /// <summary>
    /// Log to console
    /// </summary>
    /// <param name="Level"></param>
    /// <param name="Message"></param>
    public void Log(LogLevel Level, string Message)
    {
      if ((int)Level < (int)LogLevel) return;
      Console.WriteLine(@"({0}){1}",
        DateTime.UtcNow.TimeOfDay.ToString(@"hh\:mm\:ss", System.Globalization.CultureInfo.InvariantCulture), Message);

      if (m_sublogger != null)
      {
        m_sublogger.Log(Level, Message);
      }
    } //Log


    /// <summary>
    /// Log exception to console
    /// </summary>
    /// <param name="_ex"></param>
    public void Exception(Exception _ex)
    {
      Console.WriteLine(_ex.StackTrace);

      if (m_sublogger != null)
      {
        m_sublogger.Exception(_ex);
      }
    } //Exception

    public void Chain(ILogger SubLogger)
    {
      m_sublogger = SubLogger;
      m_sublogger.Parent = this;

    }

    #endregion



    public ILogger Parent { get; set; }
  }
}