﻿using IEGMCommon;

namespace SessionCommon
{
  public interface IInterchangeSession : IEGMAction
    {
      SessionStartResult Start(
        string VendorId,
        string SerialNumber,
        int MachineNumber,
        long VendorSessionId,
        string TrackData
        );

      GenericResult Update(
        string VendorId,
        long SessionId,
        decimal AmountPlayed,
        decimal AmountWon,
        int GamesPlayed,
        int GamesWon,
        decimal BillIn,
        decimal CurrentBalance);

      GenericResult End(
        string VendorId,
        long SessionId,
        decimal AmountPlayed,
        decimal AmountWon,
        int GamesPlayed,
        int GamesWon,
        decimal BillIn,
        decimal CurrentBalance
        );

    }
}
