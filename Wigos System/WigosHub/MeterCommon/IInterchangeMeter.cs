﻿using System.Collections.Generic;
using IEGMCommon;

namespace MeterCommon
{
  public interface IInterchangeMeter : IEGMAction
    {
      GenericResult Report(string VendorId, string SerialNumber, int? MachineNumber, Dictionary<int,long> Meters);
    }
}
