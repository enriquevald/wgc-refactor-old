﻿using System;
using System.Linq;
using Common;
using EventCommon;
using IEGMCommon;

namespace EventInterchange
{
  public class InterchangeEvent : IInterchangeEvent
  {
    private readonly IEGMConsumer[] m_egm_consumer;

    public InterchangeEvent(IWHUBProcessor[] EGMConsumer)
    {
      m_egm_consumer = EGMConsumer.OfType<IEGMConsumer>().ToArray();
    }

    public GenericResult Report(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount,
      long SessionId)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");

      return _egm_method.ReportEvent(VendorId, SerialNumber, MachineNumber, EventId, Amount, SessionId);
    }
  }
}
