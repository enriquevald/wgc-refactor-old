﻿using IEGMCommon;

namespace EventCommon
{
    public interface IInterchangeEvent:IEGMAction
    {
      GenericResult Report(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount,
      long SessionId);
    }
}
