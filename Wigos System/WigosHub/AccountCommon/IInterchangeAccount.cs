﻿using IEGMCommon;

namespace AccountCommon
{
    public interface IInterchangeAccount:IEGMAction
    {
      AccountStatusResult GetInfo(string VendorId, string Trackdata);
      AccountDetailsResult GetDetails(string VendorId, string Trackdata);
      CardDetailsResult GetCardDetails(string TrackData);
    }
}
