﻿using System;
using System.Linq;
using AccountCommon;
using Common;
using IEGMCommon;

namespace AccountInterchange
{
  public class AccountInterchange : IInterchangeAccount
  {
    private readonly IEGMConsumer[] m_egm_consumer;

    public AccountInterchange(IWHUBProcessor[] EGMConsumer)
    {

      m_egm_consumer = EGMConsumer.OfType<IEGMConsumer>().ToArray();
    }

    public AccountStatusResult GetInfo(string VendorId, string Trackdata)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.AccountGetInfo(VendorId, Trackdata);

    }


    public AccountDetailsResult GetDetails(string VendorId, string Trackdata)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.AccountGetDetails(VendorId,Trackdata);

    }

    public CardDetailsResult GetCardDetails(string Trackdata)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.CardInfo(Trackdata);

    }
  }
}
