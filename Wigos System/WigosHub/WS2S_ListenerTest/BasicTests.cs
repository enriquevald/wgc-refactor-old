﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using AccountCommon;
using Castle.Components.DictionaryAdapter.Xml;
using CommonListenerServiceDefinition;
using EventCommon;
using IEGMCommon;
using MeterCommon;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using SessionCommon;
using WS2S_Listener;
using WigosHubCommon;


namespace WS2S_ListenerTest
{
  [TestClass]
  public class BasicTests
  {
    [TestMethod]
    public void TestStartCardSession()
    {
      var _session = new Mock<IInterchangeSession>();
      _session.Setup(_x => _x.Start("WIGOS", "EGM-0000001", 0, 909090, "1234567890")).Returns(new SessionStartResult() { Balance = 100, StatusCode = 0, SessionId = 12345, StatusText = "ok" });
      _session.Setup(_x => _x.Start("WIGOS", "EGM-0000001", 0, 909090, "12")).Returns(new SessionStartResult() { Balance = 0, StatusCode = 1, SessionId = 0, StatusText = "ko" });
      _session.Setup(_x => _x.Start(null, null, 976, 909090, "1234567890")).Returns(new SessionStartResult() { Balance = 100, StatusCode = 0, SessionId = 12345, StatusText = "ok" });
      _session.Setup(_x => _x.Start(null, null, 976, 909090, "12")).Returns(new SessionStartResult() { Balance = 0, StatusCode = 1, SessionId = 0, StatusText = "ko" });

      _session.Setup(_x => _x.Start("WGOS", "EGM12345", 0, 909090, "1234567890")).Returns(new SessionStartResult() { Balance = 0, StatusCode = 1, SessionId = 0, StatusText = "ko" });
      _session.Setup(_x => _x.Start(null, null, 977, 909090, "1234567890")).Returns(new SessionStartResult() { Balance = 0, StatusCode = 1, SessionId = 0, StatusText = "ko" });


      var _event = new Mock<IInterchangeEvent>();
      var _meter = new Mock<IInterchangeMeter>();

      var _account = new Mock<IInterchangeAccount>();
      
      var _logger = new Mock<ILogger>();
      var _sut = new CommonListenerService(_session.Object, _event.Object, _meter.Object, _account.Object,
        _logger.Object);

      var _response_session_start = _sut.WS2S_StartCardSession("WIGOS", "EGM-0000001", 0, 909090, "1234567890");
      Assert.IsTrue(_response_session_start.Balance==100);
      Assert.IsTrue(_response_session_start.SessionId==12345);
      Assert.IsTrue(_response_session_start.StatusCode==0);
     
      _response_session_start = _sut.WS2S_StartCardSession("WIGOS", "EGM-0000001", 0, 909090, "12");
      Assert.IsTrue(_response_session_start.Balance == 0);
      Assert.IsTrue(_response_session_start.SessionId == 0);
      Assert.IsTrue(_response_session_start.StatusCode == 1);

      _response_session_start = _sut.WS2S_StartCardSession(null, null, 976, 909090, "1234567890");
      Assert.IsTrue(_response_session_start.Balance == 100);
      Assert.IsTrue(_response_session_start.SessionId == 12345);
      Assert.IsTrue(_response_session_start.StatusCode == 0);

      _response_session_start = _sut.WS2S_StartCardSession(null, null, 976, 909090, "12");
      Assert.IsTrue(_response_session_start.Balance == 0);
      Assert.IsTrue(_response_session_start.SessionId == 0);
      Assert.IsTrue(_response_session_start.StatusCode == 1);

      _response_session_start = _sut.WS2S_StartCardSession("WGOS", "EGM12345", 0, 909090, "1234567890");
      Assert.IsTrue(_response_session_start.Balance == 0);
      Assert.IsTrue(_response_session_start.SessionId == 0);
      Assert.IsTrue(_response_session_start.StatusCode == 1);

      _response_session_start = _sut.WS2S_StartCardSession(null, null, 977, 909090, "1234567890");
      Assert.IsTrue(_response_session_start.Balance == 0);
      Assert.IsTrue(_response_session_start.SessionId == 0);
      Assert.IsTrue(_response_session_start.StatusCode == 1);


    }

    [TestMethod]
    public void TestReportMeters()
    {
      var _session = new Mock<IInterchangeSession>();

      var _event = new Mock<IInterchangeEvent>();
      var _meter = new Mock<IInterchangeMeter>();
      _meter.Setup(_x => _x.Report("WIGOS", "EGM-0000001", null, It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 0 });
      _meter.Setup(_x => _x.Report(null, null, It.IsNotIn(976), It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 1 });
      _meter.Setup(_x => _x.Report(null, null, 976, It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 0 });
      _meter.Setup(_x => _x.Report(It.IsNotIn("WIGOS"), "EGM-0000001", null, It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 1 });
      _meter.Setup(_x => _x.Report("WIGOS", It.IsNotIn("EGM-0000001"), null, It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 1 });
      _meter.Setup(_x => _x.Report(It.IsNotIn("WIGOS"), It.IsNotIn("EGM-0000001"), null, It.IsAny<Dictionary<int, long>>())).Returns(new GenericResult() { StatusCode = 1 });

      var _account = new Mock<IInterchangeAccount>();

      var _logger = new Mock<ILogger>();
      var _sut = new CommonListenerService(_session.Object, _event.Object, _meter.Object, _account.Object,
        _logger.Object);
      var _response_meter = _sut.WS2S_ReportMeters("WIGOS", "EGM-0000001", null, new Meters() { new Meter() { Code = 1, Value = 100 } });
      Assert.IsTrue(_response_meter.StatusCode == 0);

      _response_meter = _sut.WS2S_ReportMeters(null, null, 976, new Meters() { new Meter() { Code = 1, Value = 100 } });
      Assert.IsTrue(_response_meter.StatusCode == 0);

      _response_meter = _sut.WS2S_ReportMeters("WIGS", "EGM-0000001", null, new Meters() { new Meter() { Code = 1, Value = 100 } });
      Assert.IsTrue(_response_meter.StatusCode == 1);

      _response_meter = _sut.WS2S_ReportMeters(null, null, 977, new Meters() { new Meter() { Code = 1, Value = 100 } });
      Assert.IsTrue(_response_meter.StatusCode == 1);



    }

    [TestMethod]
    public void TestAccountStatus()
    {
      var _session = new Mock<IInterchangeSession>();
      var _event = new Mock<IInterchangeEvent>();
      var _meter = new Mock<IInterchangeMeter>();

      var _account = new Mock<IInterchangeAccount>();
      _account.Setup(_x => _x.GetInfo(Moq.It.IsNotIn("WIGOS"), Moq.It.IsAny<string>()))
        .Returns(new AccountStatusResult() { StatusCode = 1, VID = default(string), Balance = default(decimal) });
      _account.Setup(_x => _x.GetInfo("WIGOS", "1234567890"))
        .Returns(new AccountStatusResult() { StatusCode = 0, VID = "WIGOS", Balance = 100 });
      _account.Setup(_x => _x.GetInfo("WIGOS", "0987654321"))
        .Returns(new AccountStatusResult() { StatusCode = 2, VID = "WIGOS", Balance = 100 });
      _account.Setup(_x => _x.GetInfo(null, null))
        .Returns(new AccountStatusResult() { StatusCode = 4, VID = default(string), Balance = default(decimal) });

      var _logger = new Mock<ILogger>();
      var _sut = new CommonListenerService(_session.Object, _event.Object, _meter.Object, _account.Object,
        _logger.Object);
      var _response_acc_status = _sut.WS2S_AccountStatus("WIGOS", "1234567890");

      Assert.IsTrue(_response_acc_status.Balance == 100);
      Assert.IsTrue(_response_acc_status.VID == "WIGOS");
      Assert.IsTrue(_response_acc_status.StatusCode == 0);

      _response_acc_status = _sut.WS2S_AccountStatus("WIGS", "1234567890");

      Assert.IsTrue(_response_acc_status.Balance == default(decimal));
      Assert.IsTrue(_response_acc_status.VID == default(string));
      Assert.IsTrue(_response_acc_status.StatusCode == 1);

      _response_acc_status = _sut.WS2S_AccountStatus("WIGOS", "0987654321");

      Assert.IsTrue(_response_acc_status.Balance == 100);
      Assert.IsTrue(_response_acc_status.VID == "WIGOS");
      Assert.IsTrue(_response_acc_status.StatusCode == 2);

      _response_acc_status = _sut.WS2S_AccountStatus(null, null);

      Assert.IsTrue(_response_acc_status.Balance == default(decimal));
      Assert.IsTrue(_response_acc_status.VID == default(string));
      Assert.IsTrue(_response_acc_status.StatusCode == 4);

    }
  }
}
