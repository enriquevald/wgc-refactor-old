﻿using System;
using System.Linq;
using Common;
using IEGMCommon;
using SessionCommon;
using WSI.WCP;
using WSI.Common;

namespace SessionInterchange
{
  public class SessionInterchange : IInterchangeSession
  {
    private readonly IEGMConsumer[] m_egm_consumer;

    public SessionInterchange(IWHUBProcessor[] EGMConsumer)
    {
      m_egm_consumer = EGMConsumer.OfType<IEGMConsumer>().ToArray();
    }

    public SessionStartResult Start(string VendorId, string SerialNumber, int MachineNumber, long VendorSessionId, string TrackData)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      SessionStartResult _result;


      if (_egm_method == null)
        throw new Exception("No consumer loaded!");

      _result = _egm_method.StartSession(VendorId, SerialNumber, MachineNumber, VendorSessionId, TrackData);

      GetOrCreateCashierSessionAndMoneyCollection(VendorId, SerialNumber, MachineNumber);

      return _result;
    }

    private Boolean GetOrCreateCashierSessionAndMoneyCollection(String VendorId, String SerialNumber, Int32 MachineNumber)
    {
      Int32 _user_id;
      String _user_name;
      Int64 _cashier_session_id;
      Int64 _money_collection_id;
      Int32 _cashier_terminal_id;
      Int32 _terminal_id;
      String _terminal_name;
      Int64 _mb_id;
      Int64 _mb_session_id;
      String _trackData;
      Int64 _virtual_account_id;

      try
      {
        if (!GeneralParam.GetBoolean("WS2S","Stacker.Enabled",false))
        {
          return true;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!Cashier.GetSystemUser(Cashier.GetGUIUserType(MB_USER_TYPE.SYS_TITO), _db_trx.SqlTransaction, out _user_id, out _user_name))
          {
            Log.Error("SessionStartResult.Start. Error getting the System User.");

            return false;
          }

          if (!WCP_PlaySessionLogic.WCP_GetTerminalData(VendorId, SerialNumber,MachineNumber, out _cashier_terminal_id, out _terminal_id, out _terminal_name, out _mb_id, _db_trx.SqlTransaction))
          {
            Log.Error("SessionStartResult.Start. Error getting the terminal data.");

            return false;
          }

          if (!WCP_PlaySessionLogic.WCP_TITO_GetOrCreateCashierSessionAndMoneyCollection(_cashier_terminal_id, _terminal_id, _terminal_name, _mb_id, _user_id, _user_name,
                                                                  out _cashier_session_id, out _money_collection_id, _db_trx.SqlTransaction))
          {
            Log.Error("SessionStartResult.Start. Error creating cashier session and money collection.");
            return false;
          }

          if (_mb_id == 0)
          {
            if (!WCP_PlaySessionLogic.WCP_TITO_CreateMobileBank(_terminal_id, out _mb_id, out _mb_session_id, out _trackData, _db_trx.SqlTransaction))
            {
              return false;
            }
          }

          if (_cashier_terminal_id == 0)
          {
            if (!WCP_PlaySessionLogic.WCP_TITO_CreateCashierTerminal(_terminal_name, _terminal_id, out _cashier_terminal_id, _db_trx.SqlTransaction))
            {
              return false;
            }
          }

          _virtual_account_id = Accounts.GetOrCreateVirtualAccount(_terminal_id, AccountType.ACCOUNT_VIRTUAL_TERMINAL, _db_trx.SqlTransaction);

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    public GenericResult Update(string VendorId, long SessionId, decimal AmountPlayed, decimal AmountWon, int GamesPlayed,
      int GamesWon, decimal BillIn, decimal CurrentBalance)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.UpdateSession(VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon,
        BillIn, CurrentBalance);
    }

    public GenericResult End(string VendorId, long SessionId, decimal AmountPlayed, decimal AmountWon, int GamesPlayed,
      int GamesWon, decimal BillIn, decimal CurrentBalance)
    {
      var _egm_method = m_egm_consumer.FirstOrDefault();
      if (_egm_method == null)
        throw new Exception("No consumer loaded!");
      return _egm_method.EndSession(VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon, BillIn,
        CurrentBalance);
    }
  }
}
