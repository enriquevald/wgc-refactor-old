﻿using System;
using HubStatusListener;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WigosHubCommon;

namespace HubStatusListenerTest
{
  [TestClass]
  public class HubStatusListenerServiceTestPushUpdate
  {

    [TestMethod]
    public void TestCancelUpdateOk()
    {
      var _delete_called = false;
      var _fsmoq = new Mock<IFileSystem>();
      var _dirmoq = new Mock<IFileSystemDirectory>();

      _dirmoq.Setup(_x => _x.Exists("update")).Returns(true);
      _dirmoq.Setup(_x => _x.Delete("update")).Callback(()=>_delete_called=true);

      var _sut = new HubStatusListenerService(_fsmoq.Object, _dirmoq.Object, new ConsoleLogger() { LogLevel = LogLevel.Info });
      Assert.IsTrue(_sut.CancelUpdate());
      Assert.IsTrue(_delete_called);
    }

    [TestMethod]
    public void TestCancelUpdateFailExceptionDeleteDir()
    {
      var _delete_called = false;
      var _fsmoq = new Mock<IFileSystem>();
      var _dirmoq = new Mock<IFileSystemDirectory>();

      _dirmoq.Setup(_x => _x.Exists("update")).Returns(true);
      _dirmoq.Setup(_x => _x.Delete("update")).Throws(new Exception());
      _fsmoq.Setup(_x => _x.Delete(It.IsAny<string>())).Throws(new Exception());

      var _sut = new HubStatusListenerService(_fsmoq.Object, _dirmoq.Object, new ConsoleLogger() { LogLevel = LogLevel.Info });

      Assert.IsTrue(!_sut.CancelUpdate());
      Assert.IsTrue(!_delete_called);
    }
    [TestMethod]
    public void TestCancelUpdateFailExceptionDeleteFile()
    {
      var _fsmoq = new Mock<IFileSystem>();
      var _dirmoq = new Mock<IFileSystemDirectory>();

      _dirmoq.Setup(_x => _x.Exists("update")).Returns(true);
      _dirmoq.Setup(_x => _x.GetFiles("update")).Returns(new[] {"File1", "File2", "File3"});
      _fsmoq.Setup(_x => _x.Delete(It.IsAny<string>())).Throws(new Exception());

      var _sut = new HubStatusListenerService(_fsmoq.Object, _dirmoq.Object, new ConsoleLogger() { LogLevel = LogLevel.Info });

      Assert.IsTrue(!_sut.CancelUpdate());
    }
  }
}
