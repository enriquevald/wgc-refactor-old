﻿using System;
using System.Collections.Generic;
using Common;
using HubStatusListener;
using IEGMCommon;
using Microsoft.Practices.Unity;
using WigosHubCommon;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WigosHubCommon;

namespace HubStatusListenerTest
{
  public class Consumer1 : IWHUBModule
  {
    public virtual Dictionary<string, string> Query()
    {
      return new Dictionary<string,string>(){{"Test","Test1"}};
    }

    public void Kill()
    {
      throw new NotImplementedException();
    }
  }
  public class Consumer2 : Consumer1
  {
    public override Dictionary<string, string> Query()
    {
      return new Dictionary<string, string>() { { "Test", "Test2" } };
    }
  }

  [TestClass]
  public class HubStatusListenerServiceTestModules
  {

    [TestMethod]
    public void TestLoadedModules()
    {
      IoC.RegisterType<IWHUBModule, Consumer1>("Consumer1", new ContainerControlledLifetimeManager());
      IoC.RegisterType<IWHUBModule, Consumer2>("Consumer2", new ContainerControlledLifetimeManager());
      var _cons1 = IoC.Container.Resolve<IWHUBModule>("Consumer1");
      var _cons2 = IoC.Container.Resolve<IWHUBModule>("Consumer2");
      var _delete_called = false;
      var _fsmoq = new Mock<IFileSystem>();
      var _dirmoq = new Mock<IFileSystemDirectory>();

      _dirmoq.Setup(_x => _x.Exists("update")).Returns(true);
      _dirmoq.Setup(_x => _x.Delete("update")).Callback(() => _delete_called = true);

      var _sut = new HubStatusListenerService(_fsmoq.Object, _dirmoq.Object,
        new ConsoleLogger() {LogLevel = LogLevel.Info});
      var _loaded_module_count = _sut.LoadedModules();
      Assert.IsTrue(_loaded_module_count.Count == 2);
      Assert.IsTrue(_sut.QueryModule("Consumer1").ContainsKey("Test") &&
                    _sut.QueryModule("Consumer1")["Test"] == "Test1");
      Assert.IsTrue(_sut.QueryModule("Consumer2").ContainsKey("Test") &&
                    _sut.QueryModule("Consumer2")["Test"] == "Test2");
    }
  }
}
