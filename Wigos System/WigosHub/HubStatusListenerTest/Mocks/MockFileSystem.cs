﻿using System;
using WigosHubCommon;

namespace HubStatusListenerTest.Mocks
{
  public class MockFileSystem:IFileSystem
  {
    public System.IO.Stream Create(string FileName)
    {
      throw new NotImplementedException();
    }

    public System.IO.Stream Open(string FileName, System.IO.FileMode Mode)
    {
      throw new NotImplementedException();
    }

    public void Delete(string FileName)
    {
      throw new NotImplementedException();
    }

    public System.IO.StreamWriter CreateText(string FileName)
    {
      throw new NotImplementedException();
    }

    public bool Exists(string FileName)
    {
      throw new NotImplementedException();
    }

    public void WriteAllBytes(string FileName, byte[] Buffer)
    {
      throw new NotImplementedException();
    }
  }
}
