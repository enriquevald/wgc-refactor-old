﻿using System;
using WigosHubCommon;

namespace HubStatusListenerTest.Mocks
{
  public class MockFileSystemDirectory : IFileSystemDirectory
  {
    public bool Exists(string DirectoryName)
    {
      throw new NotImplementedException();
    }

    public string[] GetFiles(string DirectoryName)
    {
      throw new NotImplementedException();
    }

    public void Delete(string DirectoryName)
    {
      throw new NotImplementedException();
    }

    public void CreateDirectory(string DirectoryName)
    {
      throw new NotImplementedException();
    }
  }
}
