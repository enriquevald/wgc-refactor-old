﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Listener.cs
// 
//   DESCRIPTION: Listener class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 17-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using Newtonsoft.Json;
using System;
using System.Web.Http;
using System.Web.Http.SelfHost;
using WSI.Common;
using InHouseApiWebAPI.Classes;
using System.ServiceModel.Channels;
using System.Web.Http.SelfHost.Channels;
using System.ServiceModel;
using System.Net;
using System.IdentityModel.Selectors;
using Microsoft.Web.Http.Versioning;
using System.Web.Http.Routing;
using Microsoft.Web.Http.Routing;
using System.Configuration;
using System.Web.Mvc;

namespace InHouseApiWebAPI
{
  public class Listener
  {
    /// <summary>
    /// Listener for wake up the WebApi
    /// </summary>
    public Listener()
    {
      HttpSelfHostServer _server;
      MyHttpsSelfHostConfiguration _config;
      String _uri = GetInHouseAPIUrl();

      try
      {
      
      _config = new MyHttpsSelfHostConfiguration(_uri);

      _config.AddApiVersioning(options =>
      {
        options.ApiVersionReader = new QueryStringApiVersionReader();
        options.AssumeDefaultVersionWhenUnspecified = true;
        options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
      });

      var constraintResolver = new DefaultInlineConstraintResolver();
      constraintResolver.ConstraintMap.Add("apiVersion", typeof(ApiVersionRouteConstraint));
           
      _config.MapHttpAttributeRoutes(constraintResolver);
      
      _config.Routes.MapHttpRoute(
          name: "InHouseApiWebAPIWithSite", 
          routeTemplate: "api/{controller}/{action}/{siteId=0}/{id=0}", 
          defaults: new { 
            siteId = RouteParameter.Optional, 
            id = RouteParameter.Optional 
          });

      _config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings();
      _config.MessageHandlers.Add(new LogRequestAndResponseHandler());
      if (ConfigurationManager.AppSettings.Get("InHouseAPI_Client_Authorization").ToLower().Equals("true"))
      {
      _config.MessageHandlers.Add(new CertificateAuthHandler()); 
      }         
      _server = new HttpSelfHostServer(_config);

      // Start Service.
      _server.OpenAsync().Wait();
      Log.Message(string.Format("InHouseApi WebApi Start. Uri: '{0}'.", _uri));

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Listener

    private static string GetInHouseAPIUrl()
    {
      string _uri = string.Empty;
      Uri _uri_aux;

      _uri = GeneralParam.GetString("InHouseAPI", "WebApi.Uri", "https://0.0.0.0:8886");
      
      // Protocol forced to HTTPS, it is mandatory to be able to execute it.
      _uri_aux = new Uri(_uri);
      if (!_uri_aux.Scheme.ToLower().Equals("https"))
        _uri = string.Format("https://{0}", _uri_aux.Authority);
      return _uri;
    } // GetInHouseAPIUrl
  } // Listener

  public class MyHttpsSelfHostConfiguration : HttpSelfHostConfiguration
  {
    public MyHttpsSelfHostConfiguration(string baseAddress) : base(baseAddress) { } // MyHttpsSelfHostConfiguration
    public MyHttpsSelfHostConfiguration(Uri baseAddress) : base(baseAddress) { } // MyHttpsSelfHostConfiguration

    protected override BindingParameterCollection OnConfigureBinding(HttpBinding httpBinding)
    {
      httpBinding.Security.Mode = HttpBindingSecurityMode.TransportCredentialOnly;
      httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
      return base.OnConfigureBinding(httpBinding);
    } // OnConfigureBinding

  } // MyHttpsSelfHostConfiguration : HttpSelfHostConfiguration

} // InHouseApiWebAPI
