﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RequireHttpsAttribute.cs
// 
//   DESCRIPTION: RequireHttpsAttribute class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace InHouseApiWebAPI.Filters
{
  public sealed class RequireHttpsAttribute : AuthorizationFilterAttribute
  {
    public override void OnAuthorization(HttpActionContext actionContext)
    {
      if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
      {
        actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
        {
          ReasonPhrase = "HTTPS Required"
        };
      }
      else
      {
        if (ConfigurationManager.AppSettings.Get("InHouseAPI_Client_Authorization").ToLower().Equals("true")) 
        {
          X509Certificate2 certificate = actionContext.Request.GetClientCertificate();
          if (certificate == null)
          {
            string client_thumbprint = actionContext.Request.Headers.Authorization.Parameter;

            if (string.IsNullOrEmpty(client_thumbprint))
            {
              actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
              {
                ReasonPhrase = "Request not contains the Authorization header."
              };
            }

            X509Store _store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
            _store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            X509Certificate2Collection certificates = _store.Certificates.Find(X509FindType.FindByThumbprint, client_thumbprint, false);
            if (certificates.Count > 0)
              certificate = certificates[0];
          }

          if (certificate == null)
          {
            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized)
            {
              ReasonPhrase = "Server Certificate's not found in Trusted Root Certificate Authorities (CAs) Store."
            };
          }
        }
        base.OnAuthorization(actionContext);
      }
    } // OnAuthorization
  } // RequireHttpsAttribute : AuthorizationFilterAttribute
}
