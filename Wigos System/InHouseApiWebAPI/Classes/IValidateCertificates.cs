﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IValidateCertificates.cs
// 
//   DESCRIPTION: IValidateCertificates interface
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 14-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-NOV-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace InHouseApiWebAPI.Classes
{
  public interface IValidateCertificates
  {
    bool IsValid(X509Certificate2 certificate); // IsValid
    IPrincipal GetPrincipal(X509Certificate2 certificate2); // GetPrincipal
  } // IValidateCertificates

} // InHouseApiWebAPI.Classes