﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CertificateAuthHandler.cs
// 
//   DESCRIPTION: CertificateAuthHandler class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 14-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-NOV-2017 OMC    First version.
//------------------------------------------------------------------------------

using System.Security.Cryptography.X509Certificates;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System;

namespace InHouseApiWebAPI.Classes
{
  public class CertificateAuthHandler : DelegatingHandler
  {
    public IValidateCertificates CertificateValidator { get; set; } // CertificateValidator
    
    public CertificateAuthHandler()
    {  
      CertificateValidator = new BasicCertificateValidator();
    } // CertificateAuthHandler

    protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
    {
      string client_thumbprint = string.Empty;
      X509Store _store;
      X509Certificate2 certificate = request.GetClientCertificate();
      X509Certificate2Collection certificates;
        if (certificate == null)
        {
          if (request.Headers.Authorization != null)
          {
            client_thumbprint = request.Headers.Authorization.Parameter;
          }

          if (string.IsNullOrEmpty(client_thumbprint))
          {
            return Task<HttpResponseMessage>.Factory.StartNew(
                () => request.CreateResponse(HttpStatusCode.Forbidden, "Client Certificate Not configurated in app.config.", "application/json"));
          }

          _store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
          _store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

          certificates = _store.Certificates.Find(X509FindType.FindByThumbprint, client_thumbprint, false);
          if (certificates.Count > 0)
            certificate = certificates[0];
        }

        if (certificate == null || !CertificateValidator.IsValid(certificate))
        {
          return Task<HttpResponseMessage>.Factory.StartNew(
              () => request.CreateResponse(HttpStatusCode.Unauthorized, "Client Certificate not found or not valid.", "application/json"));
        }
        Thread.CurrentPrincipal = CertificateValidator.GetPrincipal(certificate);
      return base.SendAsync(request, cancellationToken);
    } // SendAsync

  } // CertificateAuthHandler : DelegatingHandler
  
} // InHouseApiWebAPI.Classes