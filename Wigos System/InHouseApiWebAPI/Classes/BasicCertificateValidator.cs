﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BasicCertificateValidator.cs
// 
//   DESCRIPTION: BasicCertificateValidator class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 14-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-NOV-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using InHouseApi.Business;
namespace InHouseApiWebAPI.Classes
{
  public class BasicCertificateValidator : IValidateCertificates
  {
    /// <summary>
    /// Method to compare Client's Certificate Thumbprint with Server's Certificate ThumbPrint, 
    /// If Matches is valid, else it isn't.
    /// </summary>
    /// <param name="certificate">Server Certificate.</param>
    /// <returns></returns>
    public bool IsValid(X509Certificate2 certificate)
    {
      string _thumbprint;
      Boolean _is_valid = false;
      try
      {
        _thumbprint = ConfigurationManager.AppSettings.Get("InHouseAPI_ServerCertificate_Thumbprint");
      _is_valid = string.Compare(certificate.Thumbprint.ToLower(), _thumbprint).Equals(0);
      }
      catch (Exception ex)
      {
        BusinessLogic.OutputLog(string.Format("InHouseAPI :: Error Validating Client Certificate: Error: {0}.", ex.Message));  
      }
      return _is_valid;
    } // IsValid

    public IPrincipal GetPrincipal(X509Certificate2 certificate2)
    {
      return new GenericPrincipal(
          new GenericIdentity(certificate2.Subject), new[] { "User" });
    } // GetPrincipal

  } // BasicCertificateValidator : IValidateCertificates

} // InHouseApiWebAPI.Classes