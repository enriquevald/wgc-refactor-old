﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LogRequestAndResponseHandler.cs
// 
//   DESCRIPTION: LogRequestAndResponseHandler class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using InHouseApi.Business;
using InHouseApi.Classes.Request;
using InHouseApi.Classes.Response;

namespace InHouseApiWebAPI.Classes
{
  public class LogRequestAndResponseHandler : DelegatingHandler
  {
    /// <summary>
    /// Create a log with request and response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="CancellationToken"></param>
    /// <returns></returns>
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage Request, CancellationToken CancellationToken)
    {
      Int32 _tick;
      Int32 _last_event_id;
      String _request;
      String _response;
      LogRequest _log_request;
      LogResponse _log_response;
      HttpResponseMessage _result = null;
      Dictionary<String, String> _header_request;
      Dictionary<String, String> _header_response;

      try
      {

      _last_event_id = -1;
      _response = String.Empty;
      _tick = Environment.TickCount;
      _log_request = new LogRequest();
      _log_response = new LogResponse();
      _header_request = new Dictionary<String, String>();
      _header_response = new Dictionary<String, String>();
        //Get body request
        _request = await Request.Content.ReadAsStringAsync();
        //Get headers request
        foreach (var _header in Request.Headers)
        {
          foreach (String _value in _header.Value)
          {
            if (_log_request.Headers != null && _log_request.Headers.ContainsKey(_header.Key.ToString().ToLower()))
              _log_request.ModifyHeader(_header.Key.ToString().ToLower(),_value);
            else
              _log_request.AddHeaders(_header.Key.ToString().ToLower(), _value);
          }
        }

        //Make the request
        _result = await base.SendAsync(Request, CancellationToken);

        if (_result.Content != null)
        {
          //Get the body response
          _response = await _result.Content.ReadAsStringAsync();
        }

        //Get headers response
        foreach (var _header in _result.Headers)
        {
          foreach (String _value in _header.Value)
          {
            if (_log_response.Headers != null && _log_response.Headers.ContainsKey(_header.Key.ToString().ToLower()))
              _log_response.ModifyHeader(_header.Key.ToString().ToLower(), _value);
            else
              _log_response.AddHeaders(_header.Key.ToString().ToLower(), _value);
          }
        }

        if (_log_request.Headers.ContainsKey("LastEventId"))
        {
          Int32.TryParse(_log_request.Headers["LastEventId"], out _last_event_id);
          _last_event_id = (_last_event_id == 0 ? -1 : _last_event_id);
        }

        //Make the request log object
        _log_request.Url = Request.RequestUri.AbsoluteUri;
        _log_request.ClientCertificate = Request.GetClientCertificate();
        // Headers read above.
        _log_request.Method = Request.Method.ToString();
        _log_request.Body = JsonConvert.DeserializeObject(_request);

        //Make the response log object
        _log_response.HttpCode = (Int32)_result.StatusCode;
        // Headers read above.
        _log_response.Body = JsonConvert.DeserializeObject(_response);

        //Save log
        //BusinessLogic.ManageLog(JsonConvert.SerializeObject(_log_request, Formatting.Indented), JsonConvert.SerializeObject(_log_response, Formatting.Indented), _last_event_id, Environment.TickCount - _tick);
      }
      catch (Exception ex)
      {
        BusinessLogic.OutputLog(ex.Message);
      }

      return _result;
    } // SendAsync

  } // LogRequestAndResponseHandler : DelegatingHandler
} // InHouseApiWebAPI.Classes
