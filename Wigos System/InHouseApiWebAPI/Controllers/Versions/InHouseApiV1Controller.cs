﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InHouseApiController.cs
// 
//   DESCRIPTION: InHouseApiController class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using InHouseApi.Classes.Response;
using InHouseApiWebAPI.Filters;
using System;
using System.Net;
using System.Web.Http.Results;
using WSI.Common;
using System.Web.Http;
using Microsoft.Web.Http;

namespace InHouseApiWebAPI.Controllers.Versions
{
    // TODO: Refactor. Move the logical part into another class and make WebApi layer disengaged (and others more) by inyecting the logic
    //       Do de same with other external logic that are not the responsability of the class

  [ApiVersion("1.0")]
  public class InHouseApiV1Controller : BaseController
  {
        #region V1

        #region Gets

    /// <summary>
    /// Get Events
    /// </summary>
        /// <param name="eventId">Event Id to get</param>
    /// <returns></returns>
        [HttpGet]
        [RequireHttps]
        [ActionName("GetEvents")]
        [Route("InHouseApi/v{version:apiVersion}/event/{eventId}", Name = "GetEvents")]
        public ResponseMessageResult GetEvents(Int64? eventId)
    {
            ResponseMessageResult result;

      try
      {
                if (!eventId.HasValue)
                {
                    eventId = 0;
                }

                InHouseEventResponse response;
                HttpStatusCode status;

                this.BusinessLogic.GetEvents(eventId.Value, out response, out status);
                result = this.CreateResponse(status, response);
      }
            catch (Exception ex)
      {
                Log.Error(ex.ToString());
                result = this.CreateResponse(HttpStatusCode.InternalServerError);
      }

            return result;
        }

    /// <summary>
    /// Get Events
    /// </summary>
    /// <returns></returns>
        [HttpGet]
        [RequireHttps]
        [ActionName("GetEventsFromEvent0")]
        [Route("InHouseApi/v{version:apiVersion}/event", Name = "GetEventsFromEvent0")]
    public ResponseMessageResult GetEventsFromEvent0()
    {
            ResponseMessageResult result;

      try
      {
                InHouseEventResponse response;
                HttpStatusCode status;

                this.BusinessLogic.GetEvents(0, out response, out status);
                result = this.CreateResponse(status, response);
      }
            catch (Exception ex)
      {
                Log.Error(ex.ToString());
                result = this.CreateResponse(HttpStatusCode.InternalServerError);
      }

            return result;
        }

    /// <summary>
        /// Get Demographic Data
    /// </summary>
        /// <param name="customerId">Customer Id to get</param>
    /// <returns></returns>
        [HttpGet]
        [RequireHttps]
        [ActionName("GetDemographicData")]
        [Route("InHouseApi/v{version:apiVersion}/customer/{siteId}/{customerId}", Name = "GetDemographicData")]
        public ResponseMessageResult GetDemographicData(Int16 siteId, Int64? customerId)
    {
            ResponseMessageResult result;

      try
      {
                if (customerId.HasValue)
                {
                    InHouseCustomerResponse response;
                    HttpStatusCode status;

                    this.BusinessLogic.GetDemographicData(siteId, customerId.Value, out response, out status);
                    result = this.CreateResponse(status, response);
                }
        else
                {
                    result = this.CreateResponse(HttpStatusCode.BadRequest);
                }
      }
            catch (Exception ex)
      {
                Log.Error(ex.ToString());
                result = this.CreateResponse(HttpStatusCode.InternalServerError);
      }

            return result;
        }

    /// <summary>
    /// Get Customer Picture
    /// </summary>
        /// <param name="siteId">Site from where Picture will be taken.</param>
        /// <param name="customerId">Customer's Id from who photo will be returned.</param>
    /// <returns>JSON String witch contains serialized customerPicture object.</returns>
        [HttpGet]
        [RequireHttps]
        [ActionName("GetCustomerPicture")]
        [Route("InHouseApi/v{version:apiVersion}/customerPicture/{siteId}/{customerId}", Name = "GetCustomerPicture")]
        public ResponseMessageResult GetCustomerPicture(Int16 siteId, Int64 customerId)
        {
            ResponseMessageResult result;

            try
            {
                InHouseCustomerPictureResponse response;
                HttpStatusCode status = HttpStatusCode.InternalServerError;

                this.BusinessLogic.GetCustomerPicture(siteId, customerId, out response, ref status);
                result = this.CreateResponse(status, response);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                result = this.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Get Events
        /// </summary>
        /// <param name="siteId">Site Id to get</param>
        /// <returns></returns>
        [HttpGet]
        [RequireHttps]
        [ActionName("GetStatus")]
        [Route("InHouseApi/v{version:apiVersion}/status/{siteId}/", Name = "GetStatus")]
        public ResponseMessageResult GetStatus(Int16 siteId)
    {
            ResponseMessageResult result;

      try
      {
                InHouseStatusResponse response;
                HttpStatusCode status;
          
                this.BusinessLogic.GetStatus(
                    siteId, 
                    this.ControllerContext.RequestContext.Url.Request.RequestUri.Host,
                    this.ControllerContext.RequestContext.Url.Request.RequestUri.Port,
                    out response,
                    out status);
                result = this.CreateResponse(status, response);
      }
            catch (Exception ex)
      {
                Log.Error(ex.ToString());
                result = this.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return result;
      }
    
    #endregion

    #endregion
    }
}