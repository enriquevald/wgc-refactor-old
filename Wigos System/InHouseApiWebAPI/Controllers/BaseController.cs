﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BaseController.cs
// 
//   DESCRIPTION: BaseController class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 17-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using InHouseApi.Business;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Configuration;


namespace InHouseApiWebAPI.Controllers
{
  public class BaseController : ApiController
  {

    #region " Members "

    private BusinessLogic  m_business_logic;

    #endregion " Members "

    #region " Properties "

    public BusinessLogic  BusinessLogic
    {
      get
      {
        if (m_business_logic == null)
        {
          m_business_logic = new BusinessLogic()
          {
            IP = this.IP,
            MethodVersion = this.MethodVersion,
            ClientCertificate = this.ClientCertificate,
            ClientAuthorization = ClientAuthorization
          };
        }
        return m_business_logic;
      }
    }
    public String IP
    {
      get
      {
        return GetClientIp(Request);
      }
    }
    public String MethodVersion
    {
      get
      {
        return Request.RequestUri.LocalPath.Split('/')[2];
      }
    }
    public X509Certificate2 ClientCertificate { 
      get
      {
        X509Certificate2Collection certificates;
        X509Certificate2 _cert = null;
        X509Store _store;
        string client_thumbprint;
        if (ClientAuthorization)
        {
          _cert = RequestContext.ClientCertificate;                                                                 // Find certificate on: 'RequestContext.ClientCertificate'.
          if (_cert == null)
            _cert = Request.GetClientCertificate();                                                                 // Find certificate on: 'Request.GetClientCertificate()'.
          if (_cert == null)
          {
            client_thumbprint = Request.Headers.Authorization.Parameter;                                            // Find certificate on: 'Request.Headers.Authorization.Parameter'.
            _store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
            _store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
            certificates = _store.Certificates.Find(X509FindType.FindByThumbprint, client_thumbprint, false);
            if (certificates.Count > 0)
              _cert = certificates[0];
          }
        }
        return _cert;
      }
    } // ClientCertificate

    public static Boolean ClientAuthorization
    {
     get { 
        return ConfigurationManager.AppSettings.Get("InHouseAPI_Client_Authorization").ToLower().Equals("true"); 
      }      
    } // ClientAuthorization

    #endregion " Properties "

    #region " Public methods "

    /// <summary>
    /// Create response to client
    /// </summary>
    /// <param name="StatusCode"></param>
    /// <param name="Body"></param>
    /// <returns></returns>
    public ResponseMessageResult CreateResponse(HttpStatusCode StatusCode, Object Body = null)
    {
      HttpResponseMessage _response;

      try
      {
        _response = new HttpResponseMessage();

        if (StatusCode.Equals(HttpStatusCode.OK) && Body != null)
        {
          _response = Request.CreateResponse(StatusCode, Body);
        }
        else
        {
          if (Body != null)
            _response = Request.CreateResponse(StatusCode, Body);
          else
            _response = Request.CreateResponse(StatusCode);
        }
        
        return new ResponseMessageResult(_response);
      }
      catch (Exception)
      {
        throw;
      }
    } // CreateResponse

    /// <summary>
    /// Get ip from client
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static string GetClientIp(HttpRequestMessage Request)
    {
      RemoteEndpointMessageProperty _remote;

      if (Request.Properties[RemoteEndpointMessageProperty.Name] != null)
      {
        _remote = Request.Properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

        if (_remote != null)
        {
          return _remote.Address;
        }
      }

      return String.Empty;
    } // GetClientIp
    
    #endregion " Public methods "

  } // BaseController : ApiController

} // InHouseApiWebAPI.Controllers