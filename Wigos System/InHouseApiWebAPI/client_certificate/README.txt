﻿In order to be able to add client site certificates into this folder, in case InHouseAPI runs in Multisite, a name convention has been established.

The idea is to allow multiple InHouseAPI instances in different servers connected to the same database to perform redundancy of services to avoid to lose requests information in case a concret server isn't available.

The name must be formed by:
	- Site Id, that must be present on [wgdb_100].[dbo].[site_services].[ss_site_id].
	- Id,      that must be present on [wgdb_100].[dbo].[site_services].[ss_id].
Following the next pattern:
	- Site_{x}_Id_{y}.pfx.
	- For example, given SiteId 301 and Id 5, the file name will be:
		- Site_301_Id_5.pfx.
	