﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading;

namespace GameGatewaySimulator.CustomControls
{
  public class NumericTextBox : TextBox
  {
    bool isDecimal = false;
    string decimalSeparator = ".";
    int decimalPlaces = 2;

    [PropertyTab("Custom")]
    [DisplayName("IsDecimal")]
    [Category("Numeric Options")]
    [Description("Accepts decimal points")] 
    public bool IsDecimal
    {
        get
        {
          return isDecimal;
        }
        set
        {
          isDecimal = value;
        }
    }

    [PropertyTab("Custom")]
    [DisplayName("Decimal Separator")]
    [Category("Numeric Options")]
    [Description("The decimal separator - It is only used when IsDecimal is true")]
    public string DecimalSeparator
    {
      get
      {
        return decimalSeparator;
      }
      set
      {
        decimalSeparator = value;
      }
    }

    [PropertyTab("Custom")]
    [DisplayName("Decimal Places")]
    [Category("Numeric Options")]
    [Description("The decimal places - It is only used when IsDecimal is true")]
    public int DecimalPlaces
    {
      get
      {
        return decimalPlaces;
      }
      set
      {
        decimalPlaces = value;
      }
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      base.OnKeyPress(e);

      NumberFormatInfo fi = CultureInfo.CurrentCulture.NumberFormat;

      string c = e.KeyChar.ToString();
      if (char.IsDigit(c, 0))
        return;

      if ((SelectionStart == 0) && (c.Equals(fi.NegativeSign)))
        return;

      // copy/paste
      if ((((int)e.KeyChar == 22) || ((int)e.KeyChar == 3))
          && ((ModifierKeys & Keys.Control) == Keys.Control))
        return;

      if (e.KeyChar == '\b')
        return;
      
      if (this.IsDecimal && e.KeyChar.ToString().Equals(this.DecimalSeparator))
      {
        if(!this.Text.Contains(this.DecimalSeparator))
          return;
      }

      e.Handled = true;
    }

    protected override void OnLostFocus(EventArgs e)
    {
      base.OnLostFocus(e);
      
      if(string.IsNullOrEmpty(this.Text))
        return;
      
      if(this.IsDecimal)
      {
        if (string.Equals(this.Text,this.DecimalSeparator, StringComparison.InvariantCultureIgnoreCase))
          this.Text = "0";

        NumberFormatInfo nfi = new NumberFormatInfo() { NumberDecimalSeparator = this.DecimalSeparator };
        var result = decimal.Parse(this.Text, nfi);
        string aux = string.Format(".{0}", String.Concat(Enumerable.Repeat("0", this.DecimalPlaces)));
        string format = "{0:0" + aux + "}";
        this.Text = string.Format(nfi, format, result);
      }
    }

    protected override void WndProc(ref System.Windows.Forms.Message m)
    {
      const int WM_PASTE = 0x0302;
      if (m.Msg == WM_PASTE)
      {
        string text = Clipboard.GetText();
        if (string.IsNullOrEmpty(text))
          return;

        if ((text.IndexOf('+') >= 0) && (SelectionStart != 0))
          return;

        int i;
        if (!int.TryParse(text, out i)) // change this for other integer types
          return;

        if ((i < 0) && (SelectionStart != 0))
          return;
      }
      base.WndProc(ref m);
    }
  }
}
