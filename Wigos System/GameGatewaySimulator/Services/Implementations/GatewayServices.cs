﻿using log4net;
using GameGatewaySimulator.Data.Response;
using GameGatewaySimulator.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using GameGatewaySimulator.Data.Request;
using System.Net.NetworkInformation;

namespace GameGatewaySimulator.Services.Implementations
{
  public class GatewayServices : IGatewayServices
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(GatewayServices));

    private WebRequest CreateWebRequestPOST(string uriAddress, XmlDocument document)
    {
      var webRequest = WebRequest.Create(uriAddress);
      webRequest.Method = "POST";
      webRequest.ContentType = "text/xml";
      webRequest.Credentials = CredentialCache.DefaultCredentials;
      webRequest.Proxy = null;
      SetRequestBody(webRequest, document.InnerXml);
      
      return webRequest;
    }

    private void SetRequestBody(WebRequest webRequest, string body)
    {
      byte[] buffer = Encoding.UTF8.GetBytes(body);
      webRequest.ContentLength = buffer.Length;
      using (Stream requestStream = webRequest.GetRequestStream())
        requestStream.Write(buffer, 0, buffer.Length);
    }

    private WebResponseInfo CallService(string uriAddress, XmlDocument document)
    {
      try
      {
        var webRequest = CreateWebRequestPOST(uriAddress, document);
        using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
          return Read(webResponse);
      }
      catch(Exception ex)
      {
        Logger.Error(string.Format("Ocurrió un error al hacer el llamado al servicio '{0}'", uriAddress), ex);
        throw;
      }
    }

    public WebResponseInfo Login(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo Logout(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo GameCreated(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo GameStarting(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo Debit(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo Credit(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo BulkCredit(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo OfflineCredit(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo OfflineDebit(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public WebResponseInfo CustomXML(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    public bool IsConnectionAvailable(string serverUri)
    {
      try
      {
        if (!NetworkInterface.GetIsNetworkAvailable())
          return false;
        
        HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(serverUri);
        httpWebRequest.AllowAutoRedirect = false;
        httpWebRequest.Method = "HEAD";
        httpWebRequest.Timeout = 7000;
        using (var webResponse = (HttpWebResponse)httpWebRequest.GetResponse())
          return webResponse.StatusCode == HttpStatusCode.OK;

        /*
        WebClient webClient = new WebClient();
        byte[] result = webClient.DownloadData(serverUri);
        */
      }
      catch(WebException ex)
      {
        Logger.Error(string.Format("Ocurrió un error al hacer el test de conexión al '{0}'", serverUri), ex);
        return false;
      }
    }

    public WebResponseInfo Read(HttpWebResponse response)
    {
      var info = new WebResponseInfo();
      info.StatusCode = response.StatusCode;
      info.StatusDescription = response.StatusDescription;
      info.ContentEncoding = response.ContentEncoding;
      info.ContentLength = response.ContentLength;
      info.ContentType = response.ContentType;

      using (var bodyStream = response.GetResponseStream())
      using (var streamReader = new StreamReader(bodyStream, Encoding.UTF8))
      {
        info.Body = streamReader.ReadToEnd();
      }

      return info;
    }
  }
}
