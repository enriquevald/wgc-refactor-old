﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using GameGatewaySimulator.Services.Contracts;
using System.Xml;
using GameGatewaySimulator.Data.Response;
using GameGatewaySimulator.Data.Request;

namespace GameGatewaySimulator.Services.Implementations
{
  public class DummyGatewayServices : IGatewayServices
  {
    public WebResponseInfo Login(RequestMessageData request)
    {
      WebResponseInfo info = GetDefaultResponseInfo();
      string xmlTemplatePath = System.IO.Directory.GetCurrentDirectory() + @"\Content\Response\LoginResponse.xml";
      XmlDocument xmlLoginResponse = new XmlDocument();
      xmlLoginResponse.Load(xmlTemplatePath);

      XmlNode xmlNodeSessionId = xmlLoginResponse.SelectSingleNode("//sessionid");
      XmlNode xmlNodeUserId = xmlLoginResponse.SelectSingleNode("//userid");
      XmlNode xmlNodeAffiliateId = xmlLoginResponse.SelectSingleNode("//affiliateid");
      XmlNode xmlNodeNickName = xmlLoginResponse.SelectSingleNode("//nickname");
      XmlNode xmlNodeCountry = xmlLoginResponse.SelectSingleNode("//country");

      xmlNodeSessionId.InnerText = "Session Test";
      xmlNodeUserId.InnerText = "User Id Test";
      xmlNodeAffiliateId.InnerText = "Affiliate Id Test";
      xmlNodeNickName.InnerText = "NickName Test";
      xmlNodeCountry.InnerText = "Country Test";

      info.Body = xmlLoginResponse.InnerXml;

      return info;
    }

    public WebResponseInfo Logout(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo GameCreated(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo GameStarting(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo Debit(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo Credit(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo BulkCredit(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo OfflineCredit(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo OfflineDebit(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    public WebResponseInfo CustomXML(RequestMessageData request)
    {
      throw new NotImplementedException();
    }

    private WebResponseInfo GetDefaultResponseInfo()
    {
      var info = new WebResponseInfo();
      info.StatusCode = HttpStatusCode.OK;
      info.StatusDescription = "OK";
      info.ContentEncoding = "UTF-8";
      info.ContentLength = 1024;
      info.ContentType = "Content Type";

      return info;
    }

    public bool IsConnectionAvailable(string serverUri)
    {
      throw new NotImplementedException();
    }
  }
}
