﻿using GameGatewaySimulator.Data.Entities;
using GameGatewaySimulator.Services.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WSI.Common;

namespace GameGatewaySimulator.Services.Implementations
{
  public class DBServices : IDBServices
  {
    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";

    public IList<PlaySessionData> GetLastPlaySessions(PlaySessionFilter filter)
    {
      StringBuilder sb = new StringBuilder();

      List<int> playSessionStatusList = new List<int>();
      if (filter.ClosedSessions)
        playSessionStatusList.Add((int)PlaySessionStatus.Closed);

      if (filter.OpenSessions)
        playSessionStatusList.Add((int)PlaySessionStatus.Opened);

      string playSessionStatus = String.Join(",", playSessionStatusList);

      //TODO: Ver machine credit
      sb.AppendLine("SELECT TOP 10 PS.PS_PLAY_SESSION_ID ");
      sb.AppendLine("       , PS.PS_TERMINAL_ID ");
      sb.AppendLine("       , TE.TE_NAME AS TE_TERMINAL_NAME ");
      sb.AppendLine("       , PS.PS_ACCOUNT_ID ");
      sb.AppendLine("       , AC.AC_HOLDER_NAME ");
      sb.AppendLine("       , 0 AS MACHINE_CREDIT ");
      sb.AppendLine("       , AC.AC_BALANCE ");
      sb.AppendLine("       , PS.PS_STATUS ");
      sb.AppendLine("  FROM   PLAY_SESSIONS PS ");
      sb.AppendLine("     INNER JOIN   ACCOUNTS AC ON PS.PS_ACCOUNT_ID = AC.AC_ACCOUNT_ID ");
      sb.AppendLine("     INNER JOIN   TERMINALS TE ON PS.PS_TERMINAL_ID = TE.TE_TERMINAL_ID ");
      sb.AppendLine("  WHERE PS.PS_STATUS IN ( " + playSessionStatus + " )");
      if (!string.IsNullOrEmpty(filter.PlaySessionId))
        sb.AppendLine("  AND (PS.PS_PLAY_SESSION_ID = @PlaySessionId) ");
      if (!string.IsNullOrEmpty(filter.AccountId))
        sb.AppendLine("  AND (PS.PS_ACCOUNT_ID = @AccountId) ");
      if (!string.IsNullOrEmpty(filter.TerminalId))
        sb.AppendLine("  AND (PS.PS_TERMINAL_ID = @TerminalId) ");
      sb.AppendLine("  ORDER BY PS.PS_STATUS ASC, PS.PS_PLAY_SESSION_ID DESC");
      
      List<SqlParameter> parameters = new List<SqlParameter>();
      if(!string.IsNullOrEmpty( filter.PlaySessionId ))
        parameters.Add(new SqlParameter("@PlaySessionId",  filter.PlaySessionId));
      if (!string.IsNullOrEmpty(filter.AccountId))
        parameters.Add(new SqlParameter("@AccountId", filter.AccountId));
      if (!string.IsNullOrEmpty(filter.TerminalId))
        parameters.Add(new SqlParameter("@TerminalId", filter.TerminalId));

      List<PlaySessionData> playSessions = new List<PlaySessionData>();
      try
      {
        DataTable data = GetDatatable(sb.ToString(), parameters);
        foreach( DataRow row in data.Rows)
            playSessions.Add(GetPlaySessionData(row));
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        throw;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw;
      }

      return playSessions;
    }

    public IList<GameInstanceData> GetLastGameInstances(GameInstanceFilter filter)
    {
      StringBuilder sb = new StringBuilder();

      sb.AppendLine("SELECT TOP 300 GGI.GGI_GAME_INSTANCE_ID ");
      sb.AppendLine("       , GGI.GGI_GAME_ID ");
      sb.AppendLine("       , GGI.GGI_PARTNER_ID ");
      sb.AppendLine("  FROM   GAMEGATEWAY_GAME_INSTANCES GGI ");

      if (filter.ProviderId.HasValue)
        sb.AppendLine("  WHERE GGI.GGI_PARTNER_ID = @PartnerId ");
      
      sb.AppendLine("  ORDER BY GGI.GGI_GAME_INSTANCE_ID DESC");

      List<SqlParameter> parameters = new List<SqlParameter>();
      if (filter.ProviderId.HasValue)
        parameters.Add(new SqlParameter("@PartnerId", filter.ProviderId.Value));

      List<GameInstanceData> gameInstances = new List<GameInstanceData>();
      try
      {
        DataTable data = GetDatatable(sb.ToString(), parameters);
        foreach (DataRow row in data.Rows)
          gameInstances.Add(GetGameInstanceData(row));
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        throw;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw;
      }

      return gameInstances;
    }

    public IList<PlaySessionData> GetPlaySessions(int limited)
    {
      StringBuilder sb = new StringBuilder();

      //play sessions abiertas y cerradas
      List<int> playSessionStatusList = new List<int>();
      playSessionStatusList.Add((int)PlaySessionStatus.Closed);
      playSessionStatusList.Add((int)PlaySessionStatus.Opened);
      string playSessionStatus = String.Join(",", playSessionStatusList);

      //TODO: Ver machine credit
      sb.AppendLine(string.Format("SELECT TOP {0} PS.PS_PLAY_SESSION_ID ", limited));
      sb.AppendLine("       , PS.PS_TERMINAL_ID ");
      sb.AppendLine("       , TE.TE_NAME AS TE_TERMINAL_NAME ");
      sb.AppendLine("       , PS.PS_ACCOUNT_ID ");
      sb.AppendLine("       , AC.AC_HOLDER_NAME ");
      sb.AppendLine("       , 0 AS MACHINE_CREDIT ");
      sb.AppendLine("       , AC.AC_BALANCE ");
      sb.AppendLine("       , PS.PS_STATUS ");
      sb.AppendLine("  FROM   PLAY_SESSIONS PS ");
      sb.AppendLine("     INNER JOIN   ACCOUNTS AC ON PS.PS_ACCOUNT_ID = AC.AC_ACCOUNT_ID ");
      sb.AppendLine("     INNER JOIN   TERMINALS TE ON PS.PS_TERMINAL_ID = TE.TE_TERMINAL_ID ");
      sb.AppendLine("  WHERE PS.PS_STATUS IN ( " + playSessionStatus + " ) ");
      sb.AppendLine("  ORDER BY PS.PS_STATUS ASC, PS.PS_PLAY_SESSION_ID DESC");

      List<PlaySessionData> playSessions = new List<PlaySessionData>();
      try
      {
        DataTable data = GetDatatable(sb.ToString(), new List<SqlParameter>());
        foreach (DataRow row in data.Rows)
          playSessions.Add(GetPlaySessionData(row));
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        throw;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw;
      }

      return playSessions;
    }

    private PlaySessionData GetPlaySessionData( DataRow row )
    {
      PlaySessionData playSession = new PlaySessionData();

      if(!row.IsNull("PS_PLAY_SESSION_ID"))
        playSession.PlaySessionId = (long)row["PS_PLAY_SESSION_ID"];
      if (!row.IsNull("PS_TERMINAL_ID"))
        playSession.TerminalId = (int)row["PS_TERMINAL_ID"];
      if (!row.IsNull("TE_TERMINAL_NAME"))
        playSession.TerminalName = (string)row["TE_TERMINAL_NAME"];
      if (!row.IsNull("PS_ACCOUNT_ID"))
        playSession.AccountId = (long)row["PS_ACCOUNT_ID"];
      if (!row.IsNull("AC_HOLDER_NAME"))
        playSession.AccountName = (string)row["AC_HOLDER_NAME"];
      if (!row.IsNull("MACHINE_CREDIT"))
        playSession.MachineCredit = (int)row["MACHINE_CREDIT"];
      if (!row.IsNull("AC_BALANCE"))
        playSession.AccountBalance = (decimal)row["AC_BALANCE"];
      if (!row.IsNull("PS_STATUS"))
        playSession.PlaySessionStatus = (PlaySessionStatus)row["PS_STATUS"];

      return playSession;
    }

    private GameInstanceData GetGameInstanceData(DataRow row)
    {
      GameInstanceData gameInstance = new GameInstanceData();

      if (!row.IsNull("GGI_GAME_INSTANCE_ID"))
        gameInstance.GameInstanceId = (Int64)row["GGI_GAME_INSTANCE_ID"];
      if (!row.IsNull("GGI_GAME_ID"))
        gameInstance.GameId = (Int64)row["GGI_GAME_ID"];
      if (!row.IsNull("GGI_PARTNER_ID"))
        gameInstance.PartnerId = (int)row["GGI_PARTNER_ID"];
      
      return gameInstance;
    }

    private DataTable GetDatatable(string sql, List<SqlParameter> parameters)
    {
      CreateConnection();

      DataTable result = new DataTable();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          foreach (SqlParameter parameter in parameters)
              _sql_cmd.Parameters.Add(parameter);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            _sql_da.Fill(result);

          return result;          
        }
      }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Create connection
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateConnection()
    {
      if (WGDB.ConnectionState == ConnectionState.Open)
        return;

      if (!ConfigurationFile.Init("LottoRace.cfg", string.Empty))
      {
        Log.Error(" Reading application configuration settings. Application stopped");
        return;
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");

        Thread.Sleep(5000);
      }
    } // CreateConnection


    public string GetStringGeneralParam(string groupKey, string subjectKey)
    {
      CreateConnection();
      return GeneralParam.GetString(groupKey, subjectKey);
    }
  }
}
