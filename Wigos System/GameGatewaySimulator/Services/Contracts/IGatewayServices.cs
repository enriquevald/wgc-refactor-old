﻿using GameGatewaySimulator.Data.Request;
using GameGatewaySimulator.Data.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameGatewaySimulator.Services.Contracts
{
  public interface IGatewayServices
  {
    WebResponseInfo Login(RequestMessageData request);
    WebResponseInfo Logout(RequestMessageData request);
    WebResponseInfo GameCreated(RequestMessageData request);
    WebResponseInfo GameStarting(RequestMessageData request);
    WebResponseInfo Debit(RequestMessageData request);
    WebResponseInfo Credit(RequestMessageData request);
    WebResponseInfo BulkCredit(RequestMessageData request);
    WebResponseInfo OfflineCredit(RequestMessageData request);
    WebResponseInfo OfflineDebit(RequestMessageData request);
    WebResponseInfo CustomXML(RequestMessageData request);
    bool IsConnectionAvailable(string serverUri);
  }
}
