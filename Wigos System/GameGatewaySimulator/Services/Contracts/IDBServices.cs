﻿using GameGatewaySimulator.Data.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Services.Contracts
{
  public interface IDBServices
  {
    IList<PlaySessionData> GetLastPlaySessions(PlaySessionFilter filter);
    IList<PlaySessionData> GetPlaySessions(int limited);
    string GetStringGeneralParam(string groupKey, string subjectKey);
    IList<GameInstanceData> GetLastGameInstances(GameInstanceFilter filter);
  }
}
