﻿using GameGatewaySimulator.Data.Enums;
using GameGatewaySimulator.Data.Request;
using GameGatewaySimulator.Services.Contracts;
using GameGatewaySimulator.Workers.Data;
using GameGatewaySimulator.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using GameGatewaySimulator.Data.Response;
using GameGatewaySimulator.Common.Helpers;
using System.Xml.Linq;
using GameGatewaySimulator.Data.Entities;
using System.Globalization;

namespace GameGatewaySimulator.Workers
{
  public class SimulatePlayerBetsWorker
  {
    #region Properties

    private IGatewayServices GatewayServices { get; set; }
    private string ServerURI { get; set; }
    private string SessionId { get; set; }
    private string AccountId { get; set; }
    private string PartnerId { get; set; }
    private string Password { get; set; }
    private int DelayBetweenBets { get; set; }
    private decimal BetAmount { get; set; }
    private string Currency { get; set; }
    private PlayType PlayType { get; set; }
    private IList<GameInstanceData> GameInstances { get; set; }
    private IProgress<PlayerBetsReportProgressData> Progress { get; set; }
    private CancellationToken CancellationToken { get; set; }
    private NumberFormatInfo NumberFormatInfo { get; set; }

    #endregion

    #region Constants

    private const string NUMERIC_FORMAT = "{0:0.00}";
    private const string NUMERIC_DECIMAL_SEPARATOR = ".";

    #endregion

    public SimulatePlayerBetsWorker(IGatewayServices gatewayServices, IList<GameInstanceData> gameInstances, string serverURI, string sessionId, string accountId, string partnerId, string password, int delayBetweenBets, decimal betAmount, string currency, PlayType playType, IProgress<PlayerBetsReportProgressData> progress)
    {
      GatewayServices = gatewayServices;
      GameInstances = gameInstances;
      AccountId = accountId;
      SessionId = sessionId;
      PartnerId = partnerId;
      Password = password;
      DelayBetweenBets = delayBetweenBets;
      BetAmount = betAmount;
      Currency = currency;
      PlayType = playType;
      Progress = progress;
      ServerURI = serverURI;
      NumberFormatInfo = new NumberFormatInfo() { NumberDecimalSeparator = NUMERIC_DECIMAL_SEPARATOR };
    }

    public void StartSimulation(CancellationToken cancellationToken)
    {
      this.CancellationToken = cancellationToken;
      WebResponseInfo response;
      XmlDocument responseXml = new XmlDocument();
      //Setear un nuevo StartWatch para poder realizar la medición de tiempos a las llamadas a los servicios
      var stopWatch = Stopwatch.StartNew();

      while (!this.CancellationToken.IsCancellationRequested)
      {
        //Login Request
        response = DoLoginRequest(stopWatch);
        //Comprobar que el login se haya realizado correctamente para seguir con las apuestas
        responseXml.LoadXml(response.Body);
        if (responseXml.SelectSingleNode("//type").InnerText == "error")
          continue;

        //La cantidad de transacciones a realizar es random de 1 - 20
        int numberOfTransactions = RandomGenerator.GetRandom(1, 20);
        //Ejecutar todas las transacciones
        DebitRequestData debitRequestData;
        for (int i = 1; i <= numberOfTransactions; i++)
        {
          //Delay entre apuestas
          Thread.Sleep(this.DelayBetweenBets);

          //Debit Request
          debitRequestData = GetDebitRequestData();
          response = DoDebitRequest(debitRequestData, stopWatch);
          //Comprobar la respuesta del Debit. Si en el code devuelve 0 (cero) es que puedo apostar, si el code es diferente de 0 (cero) 
          //salir de la ejecución de las transacciones.
          responseXml.LoadXml(response.Body);
          if (responseXml.SelectSingleNode("//code").InnerText != "0")
            break;

          if(this.PlayType != PlayType.AllwaysLose)
          {
            bool randomWin = RandomGenerator.GetRandom(0,1) == 1 ? true : false;
            if(this.PlayType == PlayType.AllwaysWin || ( this.PlayType == PlayType.Random && randomWin))
            {
              //Bulk Credit Request
              response = DoBulkCreditRequest(debitRequestData, stopWatch);      
            }
          }
        }

        //Logout Request
        response = DoLogoutRequest(stopWatch);
      }
    }

    private WebResponseInfo DoLoginRequest(Stopwatch stopWatch)
    {
      XmlDocument requestXml = GetLoginRequestData().ToRequestXmlDocument();

      stopWatch.Restart();
      WebResponseInfo response = this.GatewayServices.Login(new RequestMessageData
      {
        Uri = this.ServerURI,
        Xml = requestXml
      });

      stopWatch.Stop();
      Progress.Report(new PlayerBetsReportProgressData
      {
        ElapsedTime = stopWatch.Elapsed,
        RequestOperationType = RequestOperationType.Login,
        SessionId = this.SessionId,
        XMLRequest = requestXml.InnerXml.Replace(Environment.NewLine, ""),
        XMLResponse = response.Body.Replace(Environment.NewLine, "")
      });

      return response;
    }

    private WebResponseInfo DoDebitRequest(DebitRequestData requestData, Stopwatch stopWatch)
    {
      XmlDocument requestXml = requestData.ToRequestXmlDocument();

      stopWatch.Restart();
      WebResponseInfo response = this.GatewayServices.Debit(new RequestMessageData
      {
        Uri = this.ServerURI,
        Xml = requestXml
      });
      
      stopWatch.Stop();
      Progress.Report(new PlayerBetsReportProgressData
      {
        ElapsedTime = stopWatch.Elapsed,
        RequestOperationType = RequestOperationType.Debit,
        SessionId = this.SessionId,
        XMLRequest = requestXml.InnerXml.Replace(Environment.NewLine, ""),
        XMLResponse = response.Body.Replace(Environment.NewLine, "")
      });

      return response;
    }

    private WebResponseInfo DoBulkCreditRequest(DebitRequestData debitRequestData, Stopwatch stopWatch)
    {
      XmlDocument requestXml = GetBulkCreditRequestData(debitRequestData).ToRequestXmlDocument();

      stopWatch.Restart();
      WebResponseInfo response = this.GatewayServices.BulkCredit(new RequestMessageData
      {
        Uri = this.ServerURI,
        Xml = requestXml
      });

      stopWatch.Stop();
      Progress.Report(new PlayerBetsReportProgressData
      {
        ElapsedTime = stopWatch.Elapsed,
        RequestOperationType = RequestOperationType.BulkCredit,
        SessionId = this.SessionId,
        XMLRequest = requestXml.InnerXml.Replace(Environment.NewLine, ""),
        XMLResponse = response.Body.Replace(Environment.NewLine, "")
      });

      return response;
    }

    private WebResponseInfo DoLogoutRequest(Stopwatch stopWatch)
    {
      XmlDocument requestXml = GetLogoutRequestData().ToRequestXmlDocument();

      stopWatch.Restart();
      WebResponseInfo response = this.GatewayServices.Logout(new RequestMessageData
      {
        Uri = this.ServerURI,
        Xml = requestXml
      });

      stopWatch.Stop();
      Progress.Report(new PlayerBetsReportProgressData
      {
        ElapsedTime = stopWatch.Elapsed,
        RequestOperationType = RequestOperationType.Logout,
        SessionId = this.SessionId,
        XMLRequest = requestXml.InnerXml.Replace(Environment.NewLine, ""),
        XMLResponse = response.Body.Replace(Environment.NewLine, "")
      });

      return response;
    }

    private LoginRequestData GetLoginRequestData()
    {
      LoginRequestData data = new LoginRequestData();
      data.SessionId = this.SessionId;
      data.PartnerId = this.PartnerId;
      data.Password = this.Password;
      return data;
    }

    private DebitRequestData GetDebitRequestData()
    {
      GameInstanceData gameInstance = this.GameInstances[RandomGenerator.GetRandom(0, this.GameInstances.Count - 1)];
      long gameInstanceId = gameInstance.GameInstanceId;
      long gameId = gameInstance.GameId;
      //La cantidad de bets a realizar es random de 1 - 5
      int numberOfBets = RandomGenerator.GetRandom(1, 5);

      DebitRequestData data = new DebitRequestData();
      data.SessionId = this.SessionId;
      data.PartnerId = this.PartnerId;
      data.Password = this.Password;
      data.GameId = gameId.ToString();
      data.GameInstanceId = gameInstanceId.ToString();
      data.NumberOfBets = numberOfBets.ToString();
      data.Amount = string.Format(this.NumberFormatInfo, NUMERIC_FORMAT, this.BetAmount * numberOfBets);
      data.Currency = this.Currency;
      data.Transaction = "1";

      DebitRequestBetData bet;
      for (int i = 1; i <= numberOfBets; i++)
      {
        bet = new DebitRequestBetData();
        bet.BetId = i.ToString();
        bet.BetValue = string.Format(this.NumberFormatInfo, NUMERIC_FORMAT, this.BetAmount);
        bet.BetRake = string.Format(this.NumberFormatInfo, NUMERIC_FORMAT, 0);
        data.Bets.Add(bet);
      }

      return data;
    }

    private BulkCreditRequestData GetBulkCreditRequestData(DebitRequestData debitRequestData)
    {
      BulkCreditRequestData data = new BulkCreditRequestData();
      data.PartnerId = debitRequestData.PartnerId;
      data.Password = debitRequestData.Password;
      data.GameId = debitRequestData.GameId;
      data.GameInstanceId = debitRequestData.GameInstanceId;
      data.Rollback = false;
      data.Currency = debitRequestData.Currency;
      data.Transaction = debitRequestData.Transaction;

      BulkCreditRequestCreditData credit;
      foreach (DebitRequestBetData bet in debitRequestData.Bets)
      {
        credit = new BulkCreditRequestCreditData();
        credit.BetId = bet.BetId;
        credit.Amount = bet.BetValue;
        credit.UserId = this.AccountId;
        credit.Jackpot = string.Format(this.NumberFormatInfo, NUMERIC_FORMAT, this.BetAmount + RandomGenerator.GetRandom(1, 500));
        data.Credits.Add(credit);
      }

      return data;
    }

    private LogoutRequestData GetLogoutRequestData()
    {
      LogoutRequestData data = new LogoutRequestData();
      data.SessionId = this.SessionId;
      data.PartnerId = this.PartnerId;
      data.Password = this.Password;
      return data;
    }
  }
}
