﻿using GameGatewaySimulator.Data.Enums;
using GameGatewaySimulator.Data.Response;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Workers.Data
{
  public class PlayerBetsReportProgressData
  {
    public string SessionId
    {
      get;
      set;
    }

    public RequestOperationType RequestOperationType
    {
      get;
      set;
    }

    private string requestOperationTypeString;
    public string RequestOperationTypeString
    {
      get
      {
        if (requestOperationTypeString != null)
          return requestOperationTypeString;

        return RequestOperationType.ToString();
      }
    }

    public TimeSpan ElapsedTime
    {
      get;
      set;
    }

    private string elapsedTimeString;
    public string ElapsedTimeString
    {
      get
      {
        if(elapsedTimeString != null)
          return elapsedTimeString;

        return ElapsedTime.ToString();
      }
    }

    public string XMLRequest
    {
      get;
      set;
    }

    public string XMLResponse
    {
      get;
      set;
    }

    private const string DELIMITER = "|||";
    public string ToFileLine()
    {
      return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", DELIMITER, this.SessionId, this.RequestOperationType.ToString(), this.ElapsedTime.ToString(), this.XMLRequest, this.XMLResponse);
    }

    public static List<PlayerBetsReportProgressData> LoadListFromFile(string path)
    {
      var progressDataList = new List<PlayerBetsReportProgressData>();

      string[] columns;
      string[] delim = new string[] { DELIMITER };

      using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      {
        using (StreamReader sr = new StreamReader(fs))
        {
          List<String> lst = new List<string>();
          while (!sr.EndOfStream)
          {
            columns = sr.ReadLine().Split(delim, StringSplitOptions.None);
            progressDataList.Add(new PlayerBetsReportProgressData
            {
              SessionId = columns[0],
              requestOperationTypeString = columns[1],
              elapsedTimeString = columns[2],
              XMLRequest = columns[3],
              XMLResponse = columns[4]
            });
          }

          return progressDataList;
        }
      }
    }
  }
}
