﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameGatewaySimulator.Extensions
{
  public static class TextBoxExtensions
  {
    public static void AppendNewLine(this TextBox textBox, string value)
    {
      if (string.IsNullOrEmpty(textBox.Text))
        textBox.Text = value;
      else
        textBox.AppendText(Environment.NewLine + value);
    }
  }
}
