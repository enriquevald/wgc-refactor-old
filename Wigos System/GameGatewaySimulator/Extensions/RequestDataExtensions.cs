﻿using GameGatewaySimulator.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameGatewaySimulator.Extensions
{
  public static class RequestDataExtensions
  {
    public static XmlDocument ToRequestXmlDocument(this LoginRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\LoginRequest.xml");

      XmlNode xmlNodeSessionId = xmlRequest.SelectSingleNode("//sessionid");
      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");

      xmlNodeSessionId.InnerText = data.SessionId;
      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this LogoutRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\LogoutRequest.xml");

      XmlNode xmlNodeSessionId = xmlRequest.SelectSingleNode("//sessionid");
      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");

      xmlNodeSessionId.InnerText = data.SessionId;
      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this DebitRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\DebitRequest.xml");
      XmlDocument xmlRequestBet = GetXMlDocument(@"\Content\Request\DebitRequestBet.xml");

      XmlNode xmlNodeSessionId = xmlRequest.SelectSingleNode("//sessionid");
      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeNumberOfBets = xmlRequest.SelectSingleNode("//numberofbets");
      XmlNode xmlNodeAmount = xmlRequest.SelectSingleNode("//amount");
      XmlNode xmlNodeCurrency = xmlRequest.SelectSingleNode("//currency");
      XmlNode xmlNodeTransaction = xmlRequest.SelectSingleNode("//transaction");

      xmlNodeSessionId.InnerText = data.SessionId;
      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeNumberOfBets.InnerText = data.NumberOfBets;
      xmlNodeAmount.InnerText = data.Amount;
      xmlNodeCurrency.InnerText = data.Currency;
      xmlNodeTransaction.InnerText = data.Transaction;

      XmlNode xmlNodeBets = xmlRequest.SelectSingleNode("//bets");
      XmlDocumentFragment bet;
      data.Bets.ForEach(x=> {
        bet = xmlRequest.CreateDocumentFragment();
        bet.InnerXml = xmlRequestBet.InnerXml;
        bet.SelectSingleNode("//betid").InnerText = x.BetId;
        bet.SelectSingleNode("//betvalue").InnerText = x.BetValue;
        bet.SelectSingleNode("//betrake").InnerText = x.BetRake;
        xmlNodeBets.AppendChild(bet);
      });

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this BulkCreditRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\BulkCreditRequest.xml");
      XmlDocument xmlRequestCredit = GetXMlDocument(@"\Content\Request\BulkCreditRequestCredit.xml");

      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeRollback = xmlRequest.SelectSingleNode("//rollback");
      XmlNode xmlNodeCurrency = xmlRequest.SelectSingleNode("//currency");
      XmlNode xmlNodeTransaction = xmlRequest.SelectSingleNode("//transaction");

      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeRollback.InnerText = data.Rollback ? "1" : "0";
      xmlNodeCurrency.InnerText = data.Currency;
      xmlNodeTransaction.InnerText = data.Transaction;

      XmlNode xmlNodeCredits = xmlRequest.SelectSingleNode("//credits");
      XmlDocumentFragment credit;
      data.Credits.ForEach(x =>
      {
        credit = xmlRequest.CreateDocumentFragment();
        credit.InnerXml = xmlRequestCredit.InnerXml;
        credit.SelectSingleNode("//userid").InnerText = x.UserId;
        credit.SelectSingleNode("//betid").InnerText = x.BetId;
        credit.SelectSingleNode("//amount").InnerText = x.Amount;
        credit.SelectSingleNode("//jackpot").InnerText = x.Jackpot;
        xmlNodeCredits.AppendChild(credit);
      });

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this OfflineCreditRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\OfflineCreditRequest.xml");
      XmlDocument xmlRequestBet = GetXMlDocument(@"\Content\Request\OfflineCreditRequestBet.xml");

      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeUserId = xmlRequest.SelectSingleNode("//userid");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeAmount = xmlRequest.SelectSingleNode("//amount");
      XmlNode xmlNodeCurrency = xmlRequest.SelectSingleNode("//currency");
      XmlNode xmlNodeTransaction = xmlRequest.SelectSingleNode("//transaction");

      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeUserId.InnerText = data.UserId;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeAmount.InnerText = data.Amount;
      xmlNodeCurrency.InnerText = data.Currency;
      xmlNodeTransaction.InnerText = data.Transaction;

      XmlNode xmlNodeBets = xmlRequest.SelectSingleNode("//bets");
      XmlDocumentFragment bet;
      data.Bets.ForEach(x =>
      {
        bet = xmlRequest.CreateDocumentFragment();
        bet.InnerXml = xmlRequestBet.InnerXml;
        bet.SelectSingleNode("//betid").InnerText = x.BetId;
        bet.SelectSingleNode("//amount").InnerText = x.BetAmount;
        bet.SelectSingleNode("//jackpot").InnerText = x.Jackpot;
        xmlNodeBets.AppendChild(bet);
      });

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this CreditRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\CreditRequest.xml");
      XmlDocument xmlRequestBet = GetXMlDocument(@"\Content\Request\CreditRequestBet.xml");

      XmlNode xmlNodeSessionId = xmlRequest.SelectSingleNode("//sessionid");
      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeAmount = xmlRequest.SelectSingleNode("//amount");
      XmlNode xmlNodeCurrency = xmlRequest.SelectSingleNode("//currency");
      XmlNode xmlNodeBonus = xmlRequest.SelectSingleNode("//bonus");
      XmlNode xmlNodeTransaction = xmlRequest.SelectSingleNode("//transaction");

      xmlNodeSessionId.InnerText = data.SessionId;
      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeAmount.InnerText = data.Amount;
      xmlNodeCurrency.InnerText = data.Currency;
      xmlNodeBonus.InnerText = data.Bonus ? "True" : "False";
      xmlNodeTransaction.InnerText = data.Transaction;

      XmlNode xmlNodeBets = xmlRequest.SelectSingleNode("//bets");
      XmlDocumentFragment bet;
      data.Bets.ForEach(x =>
      {
        bet = xmlRequest.CreateDocumentFragment();
        bet.InnerXml = xmlRequestBet.InnerXml;
        bet.SelectSingleNode("//betid").InnerText = x.BetId;
        bet.SelectSingleNode("//amount").InnerText = x.Amount;
        bet.SelectSingleNode("//jackpot").InnerText = x.Jackpot;
        xmlNodeBets.AppendChild(bet);
      });

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this OfflineDebitRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\OfflineDebitRequest.xml");
      XmlDocument xmlRequestBet = GetXMlDocument(@"\Content\Request\OfflineDebitRequestBet.xml");

      XmlNode xmlNodeUserId = xmlRequest.SelectSingleNode("//userid");
      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeAmount = xmlRequest.SelectSingleNode("//amount");
      XmlNode xmlNodeCurrency = xmlRequest.SelectSingleNode("//currency");
      XmlNode xmlNodeTransaction = xmlRequest.SelectSingleNode("//transaction");

      xmlNodeUserId.InnerText = data.UserId;
      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeAmount.InnerText = data.Amount;
      xmlNodeCurrency.InnerText = data.Currency;
      xmlNodeTransaction.InnerText = data.Transaction;

      XmlNode xmlNodeBets = xmlRequest.SelectSingleNode("//bets");
      XmlDocumentFragment bet;
      data.Bets.ForEach(x =>
      {
        bet = xmlRequest.CreateDocumentFragment();
        bet.InnerXml = xmlRequestBet.InnerXml;
        bet.SelectSingleNode("//betid").InnerText = x.BetId;
        bet.SelectSingleNode("//betvalue").InnerText = x.BetValue;
        bet.SelectSingleNode("//betrake").InnerText = x.BetRake;
        xmlNodeBets.AppendChild(bet);
      });

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this GameCreatedRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\GameCreatedRequest.xml");

      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameTitle = xmlRequest.SelectSingleNode("//gametitle");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeGameStarts = xmlRequest.SelectSingleNode("//gamestarts");
      XmlNode xmlNodeFirstPrize = xmlRequest.SelectSingleNode("//firstprize");
      XmlNode xmlNodeEntryCost = xmlRequest.SelectSingleNode("//entrycost");
      XmlNode xmlNodeGameLogo = xmlRequest.SelectSingleNode("//gamelogo");
      XmlNode xmlNodeJackpot = xmlRequest.SelectSingleNode("//jackpot");

      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameTitle.InnerText = data.GameTitle;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeGameStarts.InnerText = data.GameStarts;
      xmlNodeFirstPrize.InnerText = data.FirstPrize;
      xmlNodeEntryCost.InnerText = data.EntryCost;
      xmlNodeGameLogo.InnerText = data.GameLogo;
      xmlNodeJackpot.InnerText = data.Jackpot;

      return xmlRequest;
    }

    public static XmlDocument ToRequestXmlDocument(this GameStartingRequestData data)
    {
      XmlDocument xmlRequest = GetXMlDocument(@"\Content\Request\GameStartingRequest.xml");

      XmlNode xmlNodePartnerId = xmlRequest.SelectSingleNode("//partnerid");
      XmlNode xmlNodePassword = xmlRequest.SelectSingleNode("//password");
      XmlNode xmlNodeGameId = xmlRequest.SelectSingleNode("//gameid");
      XmlNode xmlNodeGameInstanceId = xmlRequest.SelectSingleNode("//gameinstanceid");
      XmlNode xmlNodeJackpot = xmlRequest.SelectSingleNode("//jackpot");

      xmlNodePartnerId.InnerText = data.PartnerId;
      xmlNodePassword.InnerText = data.Password;
      xmlNodeGameId.InnerText = data.GameId;
      xmlNodeGameInstanceId.InnerText = data.GameInstanceId;
      xmlNodeJackpot.InnerText = data.Jackpot;

      return xmlRequest;
    }

    private static XmlDocument GetXMlDocument(string relativePath)
    {
      string xmlTemplatePath = System.IO.Directory.GetCurrentDirectory() + relativePath;
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.Load(xmlTemplatePath);
      return xmlDocument;
    }
  }
}
