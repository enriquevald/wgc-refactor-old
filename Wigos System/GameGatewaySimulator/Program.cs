﻿using log4net;
using log4net.Config;
using GameGatewaySimulator.Common.Logging;
using GameGatewaySimulator.Forms;
using GameGatewaySimulator.Infrastructure.IoC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace GameGatewaySimulator
{
  static class Program
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      //Log configurator
      XmlConfigurator.ConfigureAndWatch(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Logging.config"));
      
      Logger.Info("Iniciando Lotto Race Simulator...");
      //Autofac
      IoCServices.RegisterDependencies();
      AssemblyLogging.LogLoadedAssemblies(Logger, "GameGatewaySimulator");

      //Simultaneous HttpWebRequest - Improve requests performance
      //By default, this limit is set to 2, which means that there will be never more than 2 simultaneous connections.
      ServicePointManager.DefaultConnectionLimit = 100;
      //ServicePointManager.Expect100Continue = false;

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      Logger.Info("Aplicación iniciada");
      Application.Run(IoCServices.Resolve<LCDSimulator>());
    }
  }
}
