﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WSI.Common;

namespace GameGatewaySimulator.Forms
{
  public partial class PlaySessionViewXML : Form
  {
    public PlaySessionViewXML(string xml, string playSessionId, string operationType)
    {
      InitializeComponent();

      this.webBrowserData.DocumentText = xml;
      lblPlaySessionId.Text = playSessionId;
      lblOperationType.Text = operationType;
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] playsessionviewxml (close)", Log.Type.Message);
      this.Close();
    }
  }
}
