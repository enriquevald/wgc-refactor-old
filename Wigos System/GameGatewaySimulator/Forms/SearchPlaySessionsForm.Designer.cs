﻿namespace GameGatewaySimulator.Forms
{
  partial class SearchPlaySessionsForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnSelectPlaySession = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.searchPlaySessions = new GameGatewaySimulator.UserControls.SearchPlaySessions();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnSelectPlaySession);
      this.panel1.Controls.Add(this.btnCancel);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 399);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(474, 48);
      this.panel1.TabIndex = 1;
      // 
      // btnSelectPlaySession
      // 
      this.btnSelectPlaySession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSelectPlaySession.Image = global::GameGatewaySimulator.Properties.Resources.select_item;
      this.btnSelectPlaySession.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSelectPlaySession.Location = new System.Drawing.Point(356, 11);
      this.btnSelectPlaySession.Name = "btnSelectPlaySession";
      this.btnSelectPlaySession.Size = new System.Drawing.Size(106, 25);
      this.btnSelectPlaySession.TabIndex = 8;
      this.btnSelectPlaySession.Text = "Seleccionar";
      this.btnSelectPlaySession.UseVisualStyleBackColor = true;
      this.btnSelectPlaySession.Click += new System.EventHandler(this.btnSelectPlaySession_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.Image = global::GameGatewaySimulator.Properties.Resources.close;
      this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnCancel.Location = new System.Drawing.Point(244, 11);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(106, 25);
      this.btnCancel.TabIndex = 9;
      this.btnCancel.Text = "Cancelar";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.searchPlaySessions);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(474, 399);
      this.panel2.TabIndex = 0;
      // 
      // searchPlaySessions
      // 
      this.searchPlaySessions.Dock = System.Windows.Forms.DockStyle.Fill;
      this.searchPlaySessions.Location = new System.Drawing.Point(0, 0);
      this.searchPlaySessions.Name = "searchPlaySessions";
      this.searchPlaySessions.Size = new System.Drawing.Size(474, 399);
      this.searchPlaySessions.TabIndex = 0;
      // 
      // SearchPlaySessionsForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(474, 447);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "SearchPlaySessionsForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Search Play Session";
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnSelectPlaySession;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel2;
    private UserControls.SearchPlaySessions searchPlaySessions;
  }
}