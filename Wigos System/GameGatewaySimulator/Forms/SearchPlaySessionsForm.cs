﻿using GameGatewaySimulator.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.Common;

namespace GameGatewaySimulator.Forms
{
  public partial class SearchPlaySessionsForm : BaseForm
  {
    private PlaySessionData selectedPlaySession;
    public PlaySessionData SelectedPlaySession
    {
      get { return selectedPlaySession; }
    }
    public SearchPlaySessionsForm()
    {
      InitializeComponent();
      searchPlaySessions.OnSelectedChanged += searchPlaySessions_OnSelectedChanged;
    }

    void searchPlaySessions_OnSelectedChanged(object sender, UserControls.SearchPlaySessionsSelectedChangedEventArgs e)
    {
      selectedPlaySession = e.PlaySession;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] searchplaysessionsform (cancel)", Log.Type.Message);
      this.Close();
      DialogResult = DialogResult.Cancel;
    }

    private void btnSelectPlaySession_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] searchplaysessionsform (selectplaysession)", Log.Type.Message);
      this.Close();
      DialogResult = DialogResult.OK;
    }
  }
}
