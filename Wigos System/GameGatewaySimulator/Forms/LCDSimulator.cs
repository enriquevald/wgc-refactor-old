﻿using GameGatewaySimulator.Services.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameGatewaySimulator.Extensions;
using GameGatewaySimulator.Data.Request;
using System.IO;
using System.Xml;
using GameGatewaySimulator.Data.Response;
using WSI.Common;
using System.Threading;
using GameGatewaySimulator.Data.Entities;
using GameGatewaySimulator.Forms.Messages;
using System.Configuration;
using GameGatewaySimulator.Properties;
using GameGatewaySimulator.Workers;
using GameGatewaySimulator.Data.Enums;
using GameGatewaySimulator.Workers.Data;
using GameGatewaySimulator.Common.Helpers;
using GameGatewaySimulator.Common.Extensions;

namespace GameGatewaySimulator.Forms
{
  public partial class LCDSimulator : BaseForm
  {
    public LCDSimulator( IGatewayServices gatewayServices, IDBServices dbServices )
    {
      GatewayServices = gatewayServices;
      DBServices = dbServices;

      InitializeComponent();
    }

    #region Properties

    private IGatewayServices GatewayServices { get; set; }
    private IDBServices DBServices { get; set; }

    #endregion

    #region Constants

    private static string GENERAL_PARAM_GATEWAY_GROUP_KEY = "GameGateway";
    private static string GENERAL_PARAM_GATEWAY_PROVIDER_SUBJECT_KEY = "Provider";
    private static string GENERAL_PARAM_GATEWAY_PARTNERID_SUBJECT_KEY = "Provider.001.PartnerId";
    private static string GENERAL_PARAM_GATEWAY_PASSWORD_SUBJECT_KEY = "Provider.001.Password";
    private static string SESSION_MASK = "{0}_{1}_{2}_{3}";

    private static string RESULTS_CONCURRENT_CONNECTIONS_TAB_PREFIX_ID = "tabPageConcurrentConnectionsByPlaySession";
    private static string RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID = "dgvConcurrentConnectionsByPlaySession";

    #endregion

    #region Variables

    private SynchronizationContext synchronizationContext;

    #endregion

    private void LCDSimulator_Load(object sender, EventArgs e)
    {
      Initialize();
    }

    private void Initialize()
    {
      synchronizationContext = SynchronizationContext.Current;
      txtServerUri.Text = ConfigurationManager.AppSettings["UriDefaultAddress"];

      dgvDebitRequestBets.AutoGenerateColumns = false;
      dgvDebitRequestBets.DataSource = DebitRequestBets;
      dgvBulkCreditRequestCredits.AutoGenerateColumns = false;
      dgvBulkCreditRequestCredits.DataSource = BulkCreditRequestCredits;
      dgvOfflineCreditRequestBets.AutoGenerateColumns = false;
      dgvOfflineCreditRequestBets.DataSource = OfflineCreditRequestBets;
      dgvCreditRequestBets.AutoGenerateColumns = false;
      dgvCreditRequestBets.DataSource = CreditRequestBets;
      dgvOfflineDebitRequestBets.AutoGenerateColumns = false;
      dgvOfflineDebitRequestBets.DataSource = OfflineDebitRequestBets;

      pbTestConnectionResult.Visible = false;
      lblTestConnectionResult.Visible = false;

      searchPlaySessionsLoginRequest.OnSelectedChanged += searchPlaySessionsLoginRequest_OnSelectedChanged;

      cboConcurrentConnectionsPlayType.DataSource = Enum.GetValues(typeof(PlayType));

      dtpGameCreatedRequestGameStarts.Value = DateTime.Now;
      txtGameCreatedRequestGameStarts.Text = DateTime.Now.ToEpoch().ToString();
     
      LoadSessionData();
    }

    PlaySessionData selectedPlaySession;
    void searchPlaySessionsLoginRequest_OnSelectedChanged(object sender, UserControls.SearchPlaySessionsSelectedChangedEventArgs e)
    {
      selectedPlaySession = e.PlaySession;
      RefreshLoginSessionId();
    }

    private void LoadSessionData()
    {
      txtLoginRequestSessionId.Text = Properties.Settings.Default.SessionId;

      if (!string.IsNullOrEmpty(Properties.Settings.Default.Provider))
        txtLoginRequestProvider.Text = Properties.Settings.Default.Provider;
      else
        txtLoginRequestProvider.Text = this.DBServices.GetStringGeneralParam(GENERAL_PARAM_GATEWAY_GROUP_KEY, GENERAL_PARAM_GATEWAY_PROVIDER_SUBJECT_KEY);

      if (!string.IsNullOrEmpty(Properties.Settings.Default.PartnerId))
        txtLoginRequestPartnerId.Text = Properties.Settings.Default.PartnerId;
      else
        txtLoginRequestPartnerId.Text = this.DBServices.GetStringGeneralParam(GENERAL_PARAM_GATEWAY_GROUP_KEY, GENERAL_PARAM_GATEWAY_PARTNERID_SUBJECT_KEY);

      if (!string.IsNullOrEmpty(Properties.Settings.Default.Password))
        txtLoginRequestPassword.Text = Properties.Settings.Default.Password;
      else
        txtLoginRequestPassword.Text = this.DBServices.GetStringGeneralParam(GENERAL_PARAM_GATEWAY_GROUP_KEY, GENERAL_PARAM_GATEWAY_PASSWORD_SUBJECT_KEY);

      CopyAuthenticationDataFromLogin();
    }

    private void LCDSimulator_FormClosed(object sender, FormClosedEventArgs e)
    {
      Properties.Settings.Default.SessionId = txtLoginRequestSessionId.Text.Trim();
      Properties.Settings.Default.Provider = txtLoginRequestProvider.Text.Trim();
      Properties.Settings.Default.PartnerId = txtLoginRequestPartnerId.Text.Trim();
      Properties.Settings.Default.Password = txtLoginRequestPassword.Text.Trim();
      Properties.Settings.Default.Save();

      Environment.Exit(0);
    }

    private async void btnTestConnection_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(gbServerSettings);
        bool isConnectionAvailable = await Task<bool>.Run(() =>
        {
          return this.GatewayServices.IsConnectionAvailable(txtServerUri.Text.Trim());
        });

        ShowConnectionStatus(isConnectionAvailable);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(gbServerSettings);
      }
    }

    private bool ValidateUri()
    {
      if (string.IsNullOrEmpty(txtServerUri.Text.Trim()))
      {
        base.ShowInformation("Debe ingresar una dirección correcta");
        txtServerUri.Focus();
        return false;
      }

      return true;
    }

    private bool ValidateXMLEditor()
    {
      if (!xmlEditorRequest.IsValidXML)
      {
        base.ShowInformation("El XML ingresado no está bien formado");
        xmlEditorRequest.Focus();
        return false;
      }

      return true;
    }

    private void ShowConnectionStatus(bool isConnectionAvailable)
    {
      pbTestConnectionResult.Visible = true;
      lblTestConnectionResult.Visible = true;

      pbTestConnectionResult.Image = isConnectionAvailable ? Resources.test_connection_succeeded : Resources.test_connection_failed;
      lblTestConnectionResult.Text = isConnectionAvailable ? "Test Connection Succeeded" : "Test Connection Failed";
    }

    private void txtServerUri_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Return)
        btnTestConnection.PerformClick();
    }

    #region Login Request

    private void btnBuild_LoginRequest_Click(object sender, EventArgs e)
    {
      BuildLoginRequestXML();
    }

    private async void btnSendLoginRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataLoginRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.Login(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetLoginRequestXML()
          });
        });

        responseDataLoginRequest.SetResponseInfo(response);
        RefreshStatusBar();
        CopyAuthenticationDataFromLogin();
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataLoginRequest);
      }
    }

    private string currentProvider = string.Empty;
    private string currentPartnerId = string.Empty;
    private string currentPassword = string.Empty;
    private void CopyAuthenticationDataFromLogin()
    {
      string sessionId = txtLoginRequestSessionId.Text.Trim();
      string partnerId = txtLoginRequestPartnerId.Text.Trim();
      string password = txtLoginRequestPassword.Text.Trim();
      string provider = txtLoginRequestProvider.Text.Trim();

      currentProvider = provider;
      currentPartnerId = partnerId;
      currentPassword = password;

      txtLogoutRequestSessionId.Text = sessionId;
      txtLogoutRequestPartnerId.Text = partnerId;
      txtLogoutRequestPassword.Text = password;

      txtDebitRequestSessionId.Text = sessionId;
      txtDebitRequestPartnerId.Text = partnerId;
      txtDebitRequestPassword.Text = password;

      txtCreditRequestSessionId.Text = sessionId;
      txtCreditRequestPartnerId.Text = partnerId;
      txtCreditRequestPassword.Text = password;

      txtBulkCreditRequestPartnerId.Text = partnerId;
      txtBulkCreditRequestPassword.Text = password;

      txtOfflineCreditRequestPartnerId.Text = partnerId;
      txtOfflineCreditRequestPassword.Text = password;
      txtOfflineCreditRequestUserId.Text = GetAccountId(sessionId);

      txtOfflineDebitRequestPartnerId.Text = partnerId;
      txtOfflineDebitRequestPassword.Text = password;
      txtOfflineDebitRequestUserId.Text = GetAccountId(sessionId);

      txtGameCreatedRequestPartnerId.Text = partnerId;
      txtGameCreatedRequestPassword.Text = password;

      txtGameStartingRequestPartnerId.Text = partnerId;
      txtGameStartingRequestPassword.Text = password;
    }

    private LoginRequestData GetLoginRequestData()
    {
      LoginRequestData data = new LoginRequestData();
      data.SessionId = txtLoginRequestSessionId.Text.Trim();
      data.PartnerId = txtLoginRequestPartnerId.Text.Trim();
      data.Password = txtLoginRequestPassword.Text.Trim();
      return data;
    }

    private void BuildLoginRequestXML()
    {
      LoginRequestData data = GetLoginRequestData();
      requestDataLoginRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private void RefreshStatusBar()
    {
      if (selectedPlaySession != null)
      {
        lblSelectedPlaySession.Text = string.Format("Play Session Id: {0} - Terminal Id: {1} - Account Id: {2} - Account Name: {3} - Machine Credit: {5} - Account Balance: {5}",
          selectedPlaySession.PlaySessionId, selectedPlaySession.TerminalId, selectedPlaySession.AccountId, selectedPlaySession.AccountName, selectedPlaySession.MachineCredit, selectedPlaySession.AccountBalance);
      } 
    }

    private XmlDocument GetLoginRequestXML()
    {
      if (requestDataLoginRequest.RequestXML == null)
        BuildLoginRequestXML();

      return requestDataLoginRequest.RequestXML;
    }

    bool fireSelectionChanged = false;
    private void dgvLoginRequestPlaySessions_SelectionChanged(object sender, EventArgs e)
    {
      if (fireSelectionChanged)
        RefreshLoginSessionId();
    }

    private void RefreshLoginSessionId()
    {
      if(selectedPlaySession != null)
      {
        txtLoginRequestSessionId.Text = GetFormattedSessionId(selectedPlaySession, txtLoginRequestProvider.Text.Trim());
      }
      else
      {
        string data = txtLoginRequestSessionId.Text.Trim();
        int index = data.LastIndexOf("_");
        if (index > -1)
          data = data.Substring(0, index);

        txtLoginRequestSessionId.Text = data += "_" + txtLoginRequestProvider.Text.Trim();
      }
    }

    private string GetFormattedSessionId(PlaySessionData playSession, string provider)
    {
      return string.Format(SESSION_MASK, playSession.PlaySessionId, playSession.AccountId, playSession.TerminalId, provider);
    }

    private string GetAccountId(string formattedSessionId)
    {
      string[] sessionSplit = formattedSessionId.Split('_');
      if (sessionSplit.Count() == 4)
        return sessionSplit[1];
      else
        return string.Empty;
    }

    private void txtLoginRequestProvider_KeyUp(object sender, KeyEventArgs e)
    {
      RefreshLoginSessionId();
    }

    #endregion
    
    #region Logout Request

    private void btnBuildLogoutRequest_Click(object sender, EventArgs e)
    {
      BuildLogoutRequestXML();
    }

    private void BuildLogoutRequestXML()
    {
      LogoutRequestData data = GetLogoutRequestData();
      requestDataLogoutRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private LogoutRequestData GetLogoutRequestData()
    {
      LogoutRequestData data = new LogoutRequestData();
      data.SessionId = txtLogoutRequestSessionId.Text.Trim();
      data.PartnerId = txtLogoutRequestPartnerId.Text.Trim();
      data.Password = txtLogoutRequestPassword.Text.Trim();
      return data;
    }

    private XmlDocument GetLogoutRequestXML()
    {
      if (requestDataLogoutRequest.RequestXML == null)
        BuildLogoutRequestXML();

      return requestDataLogoutRequest.RequestXML;
    }

    private async void btnSendLogoutRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataLogoutRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.Logout(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetLogoutRequestXML()
          });
        });

        responseDataLogoutRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataLogoutRequest);
      }
    }

    #endregion

    #region Debit Request

    private BindingList<DebitRequestBetData> debitRequestBets;
    private BindingList<DebitRequestBetData> DebitRequestBets
    {
      get
      {
        if (debitRequestBets == null)
          debitRequestBets = new BindingList<DebitRequestBetData>();

        return debitRequestBets;
      }
    }

    private void btnDebitRequestAddBet_Click(object sender, EventArgs e)
    {
      DebitRequestBets.Add(new DebitRequestBetData
      {
        BetId = txtDebitRequestBetId.Text.Trim(),
        BetValue = txtDebitRequestBetValue.Text.Trim(),
        BetRake = txtDebitRequestBetRake.Text.Trim(),
      });
    }

    private void btnBuildDebitRequest_Click(object sender, EventArgs e)
    {
      BuildDebitRequestXML();
    }

    private void BuildDebitRequestXML()
    {
      DebitRequestData data = GetDebitRequestData();
      requestDataDebitRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private DebitRequestData GetDebitRequestData()
    {
      DebitRequestData data = new DebitRequestData();
      data.SessionId = txtDebitRequestSessionId.Text.Trim();
      data.PartnerId = txtDebitRequestPartnerId.Text.Trim();
      data.Password = txtDebitRequestPassword.Text.Trim();
      data.GameId = txtDebitRequestGameId.Text.Trim();
      data.GameInstanceId = txtDebitRequestGameInstanceId.Text.Trim();
      data.NumberOfBets = txtDebitRequestNumberOfBets.Text.Trim();
      data.Amount = txtDebitRequestAmount.Text.Trim();
      data.Currency = txtDebitRequestCurrency.Text.Trim();
      data.Transaction = txtDebitRequestTransaction.Text.Trim();

      foreach(DebitRequestBetData bet in DebitRequestBets)
        data.Bets.Add(bet);

      return data;
    }

    private XmlDocument GetDebitRequestXML()
    {
      if (requestDataDebitRequest.RequestXML == null)
        BuildDebitRequestXML();

      return requestDataDebitRequest.RequestXML;
    }

    private async void btnSendDebitRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataDebitRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.Debit(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetDebitRequestXML()
          });
        });

        responseDataDebitRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataDebitRequest);
      }
    }

    private void dgvDebitRequestBets_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if(dgvDebitRequestBets.Columns[e.ColumnIndex].Name == "dgvColumnDebitBetDelete")
      {
        if (e.RowIndex == -1)
          return;

        DebitRequestBetData selectedBet = (DebitRequestBetData)dgvDebitRequestBets.Rows[e.RowIndex].DataBoundItem;
        DebitRequestBets.Remove(selectedBet);
      }
    }

    #endregion

    #region Bulk Credit Request

    private BindingList<BulkCreditRequestCreditData> bulkCreditRequestCredits;
    private BindingList<BulkCreditRequestCreditData> BulkCreditRequestCredits
    {
      get
      {
        if (bulkCreditRequestCredits == null)
          bulkCreditRequestCredits = new BindingList<BulkCreditRequestCreditData>();

        return bulkCreditRequestCredits;
      }
    }

    private void btnBulkCreditRequestAddCredit_Click(object sender, EventArgs e)
    {
      BulkCreditRequestCredits.Add(new BulkCreditRequestCreditData
      {
        UserId = txtBulkCreditRequestCreditUserId.Text.Trim(),
        BetId = txtBulkCreditRequestCreditBetId.Text.Trim(),
        Amount = txtBulkCreditRequestCreditAmount.Text.Trim(),
        Jackpot = txtBulkCreditRequestCreditJackpot.Text.Trim()
      });
    }

    private void btnBuildBulkCreditRequest_Click(object sender, EventArgs e)
    {
      BuildBulkCreditRequestXML();
    }

    private void BuildBulkCreditRequestXML()
    {
      BulkCreditRequestData data = GetBulkCreditRequestData();
      requestDataBulkCreditRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private BulkCreditRequestData GetBulkCreditRequestData()
    {
      BulkCreditRequestData data = new BulkCreditRequestData();
      data.PartnerId = txtBulkCreditRequestPartnerId.Text.Trim();
      data.Password = txtBulkCreditRequestPassword.Text.Trim();
      data.GameId = txtBulkCreditRequestGameId.Text.Trim();
      data.GameInstanceId = txtBulkCreditRequestGameInstanceId.Text.Trim();
      data.Rollback = chkBulkCreditRequestRollback.Checked;
      data.Currency = txtBulkCreditRequestCurrency.Text.Trim();
      data.Transaction = txtBulkCreditRequestTransaction.Text.Trim();

      foreach (BulkCreditRequestCreditData credit in BulkCreditRequestCredits)
        data.Credits.Add(credit);

      return data;
    }

    private XmlDocument GetBulkCreditRequestXML()
    {
      if (requestDataBulkCreditRequest.RequestXML == null)
        BuildBulkCreditRequestXML();

      return requestDataBulkCreditRequest.RequestXML;
    }

    private async void btnSendBulkCreditRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataBulkCreditRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.BulkCredit(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetBulkCreditRequestXML()
          });
        });

        responseDataBulkCreditRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataBulkCreditRequest);
      }
    }

    private void dgvBulkCreditRequestCredits_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (dgvBulkCreditRequestCredits.Columns[e.ColumnIndex].Name == "dgvColumnBulkCreditDelete")
      {
        if (e.RowIndex == -1)
          return;

        BulkCreditRequestCreditData selectedCredit = (BulkCreditRequestCreditData)dgvBulkCreditRequestCredits.Rows[e.RowIndex].DataBoundItem;
        BulkCreditRequestCredits.Remove(selectedCredit);
      }
    }

    private void btnSearchPlaySessionsBulkCredit_Click(object sender, EventArgs e)
    {
      using(SearchPlaySessionsForm playSessionsForm = new SearchPlaySessionsForm())
      {
        DialogResult result = playSessionsForm.ShowDialog(this);
        if(result == DialogResult.OK)
        {
          PlaySessionData playSession = playSessionsForm.SelectedPlaySession;
          if(playSession != null)
            txtBulkCreditRequestCreditUserId.Text = playSession.AccountId.ToString();
        }
      }
    }

    #endregion

    #region Offline Credit Request

    private BindingList<OfflineCreditRequestBetData> offlineCreditRequestBets;
    private BindingList<OfflineCreditRequestBetData> OfflineCreditRequestBets
    {
      get
      {
        if (offlineCreditRequestBets == null)
          offlineCreditRequestBets = new BindingList<OfflineCreditRequestBetData>();

        return offlineCreditRequestBets;
      }
    }

    private void btnOfflineCreditRequestAddBet_Click(object sender, EventArgs e)
    {
      offlineCreditRequestBets.Add(new OfflineCreditRequestBetData
      {
        BetId = txtOfflineCreditRequestBetId.Text.Trim(),
        BetAmount = txtOfflineCreditRequestBetAmount.Text.Trim(),
        Jackpot = txtOfflineCreditRequestBetJackpot.Text.Trim()
      });
    }

    private void btnBuildOfflineCreditRequest_Click(object sender, EventArgs e)
    {
      BuildOfflineCreditRequestXML();
    }

    private void BuildOfflineCreditRequestXML()
    {
      OfflineCreditRequestData data = GetOfflineCreditRequestData();
      requestDataOfflineCreditRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private OfflineCreditRequestData GetOfflineCreditRequestData()
    {
      OfflineCreditRequestData data = new OfflineCreditRequestData();
      data.PartnerId = txtOfflineCreditRequestPartnerId.Text.Trim();
      data.Password = txtOfflineCreditRequestPassword.Text.Trim();
      data.UserId = txtOfflineCreditRequestUserId.Text.Trim();
      data.GameId = txtOfflineCreditRequestGameId.Text.Trim();
      data.GameInstanceId = txtOfflineCreditRequestGameInstanceId.Text.Trim();
      data.Amount = txtOfflineCreditRequestAmount.Text.Trim();
      data.Currency = txtOfflineCreditRequestCurrency.Text.Trim();
      data.Transaction = txtOfflineCreditRequestTransaction.Text.Trim();

      foreach (OfflineCreditRequestBetData bet in OfflineCreditRequestBets)
        data.Bets.Add(bet);

      return data;
    }

    private XmlDocument GetOfflineCreditRequestXML()
    {
      if (requestDataOfflineCreditRequest.RequestXML == null)
        BuildOfflineCreditRequestXML();

      return requestDataOfflineCreditRequest.RequestXML;
    }

    private async void btnSendOfflineCreditRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataOfflineCreditRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.OfflineCredit(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetOfflineCreditRequestXML()
          });
        });

        responseDataOfflineCreditRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataOfflineCreditRequest);
      }
    }

    private void dgvOfflineCreditRequestBets_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (dgvOfflineCreditRequestBets.Columns[e.ColumnIndex].Name == "dgvColumnOfflineCreditBetDelete")
      {
        if (e.RowIndex == -1)
          return;

        OfflineCreditRequestBetData selectedBet = (OfflineCreditRequestBetData)dgvOfflineCreditRequestBets.Rows[e.RowIndex].DataBoundItem;
        OfflineCreditRequestBets.Remove(selectedBet);
      }
    }

    #endregion

    #region Credit Request

    private BindingList<CreditRequestBetData> creditRequestBets;
    private BindingList<CreditRequestBetData> CreditRequestBets
    {
      get
      {
        if (creditRequestBets == null)
          creditRequestBets = new BindingList<CreditRequestBetData>();

        return creditRequestBets;
      }
    }

    private void btnCreditRequestAddBet_Click(object sender, EventArgs e)
    {
      CreditRequestBets.Add(new CreditRequestBetData
      {
        BetId = txtCreditRequestBetId.Text.Trim(),
        Amount = txtCreditRequestBetAmount.Text.Trim(),
        Jackpot = txtCreditRequestBetJackpot.Text.Trim()
      });
    }

    private void btnBuildCreditRequest_Click(object sender, EventArgs e)
    {
      BuildCreditRequestXML();
    }

    private void BuildCreditRequestXML()
    {
      CreditRequestData data = GetCreditRequestData();
      requestDataCreditRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private CreditRequestData GetCreditRequestData()
    {
      CreditRequestData data = new CreditRequestData();
      data.SessionId = txtCreditRequestSessionId.Text.Trim();
      data.PartnerId = txtCreditRequestPartnerId.Text.Trim();
      data.Password = txtCreditRequestPassword.Text.Trim();
      data.GameId = txtCreditRequestGameId.Text.Trim();
      data.GameInstanceId = txtCreditRequestGameInstanceId.Text.Trim();
      data.Amount = txtCreditRequestAmount.Text.Trim();
      data.Currency = txtCreditRequestCurrency.Text.Trim();
      data.Bonus = chkCreditRequestBonus.Checked;
      data.Transaction = txtCreditRequestTransaction.Text.Trim();

      foreach (CreditRequestBetData bet in CreditRequestBets)
        data.Bets.Add(bet);

      return data;
    }

    private XmlDocument GetCreditRequestXML()
    {
      if (requestDataCreditRequest.RequestXML == null)
        BuildCreditRequestXML();

      return requestDataCreditRequest.RequestXML;
    }

    private async void btnSendCreditRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataCreditRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.Credit(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetCreditRequestXML()
          });
        });

        responseDataCreditRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataCreditRequest);
      }
    }

    private void dgvCreditRequestBets_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (dgvCreditRequestBets.Columns[e.ColumnIndex].Name == "dgvColumnCreditBetDelete")
      {
        if (e.RowIndex == -1)
          return;

        CreditRequestBetData selectedBet = (CreditRequestBetData)dgvCreditRequestBets.Rows[e.RowIndex].DataBoundItem;
        CreditRequestBets.Remove(selectedBet);
      }
    }

    #endregion

    #region Offline Debit Request

    private BindingList<OfflineDebitRequestBetData> offlineDebitRequestBets;
    private BindingList<OfflineDebitRequestBetData> OfflineDebitRequestBets
    {
      get
      {
        if (offlineDebitRequestBets == null)
          offlineDebitRequestBets = new BindingList<OfflineDebitRequestBetData>();

        return offlineDebitRequestBets;
      }
    }

    private void btnOfflineDebitRequestAddBet_Click(object sender, EventArgs e)
    {
      OfflineDebitRequestBets.Add(new OfflineDebitRequestBetData
      {
        BetId = txtOfflineDebitRequestBetId.Text.Trim(),
        BetValue = txtOfflineDebitRequestBetValue.Text.Trim(),
        BetRake = txtOfflineDebitRequestBetRake.Text.Trim()
      });
    }

    private void btnBuildOfflineDebitRequest_Click(object sender, EventArgs e)
    {
      BuildOfflineDebitRequestXML();
    }

    private void BuildOfflineDebitRequestXML()
    {
      OfflineDebitRequestData data = GetOfflineDebitRequestData();
      requestDataOfflineDebitRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private OfflineDebitRequestData GetOfflineDebitRequestData()
    {
      OfflineDebitRequestData data = new OfflineDebitRequestData();
      data.UserId = txtOfflineDebitRequestUserId.Text.Trim();
      data.PartnerId = txtOfflineDebitRequestPartnerId.Text.Trim();
      data.Password = txtOfflineDebitRequestPassword.Text.Trim();
      data.GameId = txtOfflineDebitRequestGameId.Text.Trim();
      data.GameInstanceId = txtOfflineDebitRequestGameInstanceId.Text.Trim();
      data.Amount = txtOfflineDebitRequestAmount.Text.Trim();
      data.Currency = txtOfflineDebitRequestCurrency.Text.Trim();
      data.Transaction = txtOfflineDebitRequestTransaction.Text.Trim();

      foreach (OfflineDebitRequestBetData bet in OfflineDebitRequestBets)
        data.Bets.Add(bet);

      return data;
    }

    private XmlDocument GetOfflineDebitRequestXML()
    {
      if (requestDataOfflineDebitRequest.RequestXML == null)
        BuildOfflineDebitRequestXML();

      return requestDataOfflineDebitRequest.RequestXML;
    }

    private async void btnSendOfflineDebitRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataOfflineDebitRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.OfflineDebit(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetOfflineDebitRequestXML()
          });
        });

        responseDataOfflineDebitRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataOfflineDebitRequest);
      }
    }

    private void dgvOfflineDebitRequestBets_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (dgvOfflineDebitRequestBets.Columns[e.ColumnIndex].Name == "dgvColumnOfflineDebitBetDelete")
      {
        if (e.RowIndex == -1)
          return;

        OfflineDebitRequestBetData selectedBet = (OfflineDebitRequestBetData)dgvOfflineDebitRequestBets.Rows[e.RowIndex].DataBoundItem;
        OfflineDebitRequestBets.Remove(selectedBet);
      }
    }

    #endregion

    #region Game Created Request

    private void btnBuildGameCreatedRequest_Click(object sender, EventArgs e)
    {
      BuildGameCreatedRequestXML();
    }

    private void BuildGameCreatedRequestXML()
    {
      GameCreatedRequestData data = GetGameCreatedRequestData();
      requestDataGameCreatedRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private GameCreatedRequestData GetGameCreatedRequestData()
    {
      GameCreatedRequestData data = new GameCreatedRequestData();
      data.PartnerId = txtGameCreatedRequestPartnerId.Text.Trim();
      data.Password = txtGameCreatedRequestPassword.Text.Trim();
      data.GameId = txtGameCreatedRequestGameId.Text.Trim();
      data.GameTitle = txtGameCreatedRequestGameTitle.Text.Trim();
      data.GameInstanceId = txtGameCreatedRequestGameInstanceId.Text.Trim();
      data.GameStarts = txtGameCreatedRequestGameStarts.Text.Trim();
      data.FirstPrize = txtGameCreatedRequestFirstPrize.Text.Trim();
      data.EntryCost = txtGameCreatedRequestEntryCost.Text.Trim();
      data.GameLogo = txtGameCreatedRequestGameLogo.Text.Trim();
      data.Jackpot = txtGameCreatedRequestJackpot.Text.Trim();
      return data;
    }

    private XmlDocument GetGameCreatedRequestXML()
    {
      if (requestDataGameCreatedRequest.RequestXML == null)
        BuildGameCreatedRequestXML();

      return requestDataGameCreatedRequest.RequestXML;
    }

    private async void btnSendGameCreatedRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataGameCreatedRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.GameCreated(new RequestMessageData{
            Uri = txtServerUri.Text.Trim(),
            Xml = GetGameCreatedRequestXML()
          });
        });

        responseDataGameCreatedRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataGameCreatedRequest);
      }
    }

    #endregion

    #region Game Starting Request

    private void btnBuildGameStartingRequest_Click(object sender, EventArgs e)
    {
      BuildGameStartingRequestXML();
    }

    private void BuildGameStartingRequestXML()
    {
      GameStartingRequestData data = GetGameStartingRequestData();
      requestDataGameStartingRequest.SetRequestXML(data.ToRequestXmlDocument());
    }

    private GameStartingRequestData GetGameStartingRequestData()
    {
      GameStartingRequestData data = new GameStartingRequestData();
      data.PartnerId = txtGameStartingRequestPartnerId.Text.Trim();
      data.Password = txtGameStartingRequestPassword.Text.Trim();
      data.GameId = txtGameStartingRequestGameId.Text.Trim();
      data.GameInstanceId = txtGameStartingRequestGameInstanceId.Text.Trim();
      data.Jackpot = txtGameStartingRequestJackpot.Text.Trim();
      return data;
    }

    private XmlDocument GetGameStartingRequestXML()
    {
      if (requestDataGameStartingRequest.RequestXML == null)
        BuildGameStartingRequestXML();

      return requestDataGameStartingRequest.RequestXML;
    }

    private async void btnSendGameStartingRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      try
      {
        ShowLoading(responseDataGameStartingRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.GameStarting(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = GetGameStartingRequestXML()
          });
        });

        responseDataGameStartingRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataGameStartingRequest);
      }
    }

    #endregion

    #region Free XML Request

    private void btnBuildCustomXMLRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateXMLEditor())
        return;

      BuildCustomRequestXML();
    }

    private void BuildCustomRequestXML()
    {
      requestDataCustomXMLRequest.SetRequestXML(xmlEditorRequest.XMLDocument);
    }

    private XmlDocument GetCustomRequestXML()
    {
      if (requestDataCustomXMLRequest.RequestXML == null)
        BuildCustomRequestXML();

      return requestDataCustomXMLRequest.RequestXML;
    }

    private async void btnSendCustomXMLRequest_Click(object sender, EventArgs e)
    {
      if (!ValidateUri())
        return;

      if (requestDataCustomXMLRequest.RequestXML == null && !ValidateXMLEditor())
        return;

      XmlDocument document = GetCustomRequestXML();

      try
      {
        ShowLoading(responseDataCustomXMLRequest);
        WebResponseInfo response = await Task<WebResponseInfo>.Run(() =>
        {
          return this.GatewayServices.CustomXML(new RequestMessageData
          {
            Uri = txtServerUri.Text.Trim(),
            Xml = document
          });
        });

        responseDataCustomXMLRequest.SetResponseInfo(response);
      }
      catch (Exception ex)
      {
        base.ShowError(ex);
      }
      finally
      {
        HideLoading(responseDataCustomXMLRequest);
      }
    }

    #endregion   

    #region Simulate Concurrent Connections

    Action CancelSimulation;
    private async void btnRunConcurrentConnections_Click(object sender, EventArgs e)
    {
      CancellationTokenSource cancellationTokenSource = null;
      try
      {
        InitializeSimulationData();

        if (!ValidateConcurrentData())
          return;

        //Disable Controls
        btnRunConcurrentConnections.Enabled = false;
        btnStopConcurrentConnections.Enabled = false;

        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Initializing simulation environment configuration...");
        
        //Get Simulation parameters
        string serverURI = txtServerUri.Text.Trim();
        int numberOfPlayers = Convert.ToInt32(txtConcurrentConnectionsNumberOfPlayers.Text);
        int delayBetweenBets = Convert.ToInt32(txtConcurrentConnectionsDelayBetweenBets.Text);
        decimal betAmount = Convert.ToDecimal(txtConcurrentConnectionsBetAmount.Text);
        string currency = "MXN";
        PlayType playType = (PlayType)cboConcurrentConnectionsPlayType.SelectedItem;

        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Server URI: {0}", serverURI));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("PartnerId: {0}", currentPartnerId));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Password: {0}", currentPassword));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Currency: {0}", currency));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Number of players: {0}", numberOfPlayers.ToString()));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Play type: {0}", playType.ToString()));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Bet amount: {0}", betAmount.ToString()));
        txtConcurrentConnectionsConsoleOutput.AppendNewLine(string.Format("Delay between bets: {0}", delayBetweenBets.ToString()));

        //Remove Simulation results folder
        DirectoryInfo directory = Directory.CreateDirectory(GetSimulationResultsFolder());
        directory.GetFiles().ToList().ForEach(f => f.Delete());

        //Handle cancellation
        cancellationTokenSource = new CancellationTokenSource();
        ParallelOptions parallelOptions = new ParallelOptions();
        parallelOptions.CancellationToken = cancellationTokenSource.Token;

        this.CancelSimulation = () =>
        {
          txtConcurrentConnectionsConsoleOutput.AppendNewLine("Stopping Simulation...");
          btnStopConcurrentConnections.Enabled = false;
          cancellationTokenSource.Cancel();
        };

        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Getting Play Sessions...");
        IList<PlaySessionData> playSessionsList = await Task<IList<PlaySessionData>>.Run(() =>
        {
          return GetLimitedPlaySessions(numberOfPlayers);
        });


        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Getting Game Instances...");
        IList<GameInstanceData> gameInstancesList = await Task<IList<GameInstanceData>>.Run(() =>
        {
          return GetLimitedGameInstances(currentPartnerId);
        });

        if (gameInstancesList.Count == 0)
        {
          txtConcurrentConnectionsConsoleOutput.AppendNewLine("There are not Game Instances in the Database...");
          return;
        }

        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Setting workers by Play Session...");
        var reportProgress = new Progress<PlayerBetsReportProgressData>(SimulationReportProgress);
        List<SimulatePlayerBetsWorker> playerWorkers = await Task<List<SimulatePlayerBetsWorker>>.Run(() =>
        {
          return GetPlayerBetsWorkers(reportProgress,playSessionsList, gameInstancesList, serverURI, delayBetweenBets, betAmount, currency, playType);
        });

        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Configuration Completed...");
        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Running Simulation...");
        btnStopConcurrentConnections.Enabled = true;
        await System.Threading.Tasks.Task.Factory.StartNew(() =>
          {
            Parallel.ForEach(playerWorkers, parallelOptions, worker => { 
              worker.StartSimulation(parallelOptions.CancellationToken);
              parallelOptions.CancellationToken.ThrowIfCancellationRequested();
            });
          }
        );
      }
      catch(OperationCanceledException)
      {
        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Simulation Stoped...");
      }
      catch(Exception ex)
      {
        txtConcurrentConnectionsConsoleOutput.AppendNewLine("An error ocurred in the execution of the simulation...");
        base.ShowError(ex);
      }
      finally
      {
        if(cancellationTokenSource != null)
          cancellationTokenSource.Dispose();
        txtConcurrentConnectionsConsoleOutput.AppendNewLine("Simulation Completed...");
        btnRunConcurrentConnections.Enabled = true;
        btnStopConcurrentConnections.Enabled = false;
        FileWriter.Instance.Dispose();
      }
    }

    private void InitializeSimulationData()
    {
      txtConcurrentConnectionsConsoleOutput.Text = string.Empty;

      //Remove results tabs
      int countTabPages = tabControlConcurrentConnectionsResults.TabPages.Count;
      TabPage tabPage;
      for (int i = countTabPages - 1; i > 0; i--)
      {
        tabPage = tabControlConcurrentConnectionsResults.TabPages[i];
        tabControlConcurrentConnectionsResults.TabPages.Remove(tabPage);
        tabPage.Dispose();
      }

      //Clear general results
      dgvConcurrentConnectionsGeneralResults.AutoGenerateColumns = false;
      GeneralReportDataList.Clear();
      dgvConcurrentConnectionsGeneralResults.DataSource = GeneralReportDataList;
    }

    private bool ValidateConcurrentData()
    {
      if (!ValidateUri())
        return false;

      if(!this.GatewayServices.IsConnectionAvailable(txtServerUri.Text.Trim()))
      {
        base.ShowInformation("La conexión al servidor no se encuentra disponible");
        txtServerUri.Focus();
        return false;
      }

      string numberOfPlayers = txtConcurrentConnectionsNumberOfPlayers.Text.Trim();
      if (string.IsNullOrEmpty(numberOfPlayers) || Convert.ToInt32(numberOfPlayers) == 0 )
      {
        base.ShowInformation("Debe ingresar la cantidad de jugadores simultaneos");
        txtConcurrentConnectionsNumberOfPlayers.Focus();
        return false;
      }

      string delayBetweenBets = txtConcurrentConnectionsDelayBetweenBets.Text.Trim();
      if (string.IsNullOrEmpty(delayBetweenBets) || Convert.ToInt32(delayBetweenBets) == 0)
      {
        base.ShowInformation("Debe ingresar el delay (ms) entre apuestas");
        txtConcurrentConnectionsDelayBetweenBets.Focus();
        return false;
      }

      string betAmount = txtConcurrentConnectionsBetAmount.Text.Trim();
      if (string.IsNullOrEmpty(betAmount) || Convert.ToInt32(betAmount) == 0)
      {
        base.ShowInformation("Debe ingresar el monto de la apuesta");
        txtConcurrentConnectionsBetAmount.Focus();
        return false;
      }

      return true;
    }

    private IList<PlaySessionData> GetLimitedPlaySessions(int limited)
    {
      return DBServices.GetPlaySessions(limited);
    }

    private IList<GameInstanceData> GetLimitedGameInstances(string providerId)
    {
      GameInstanceFilter filter = new GameInstanceFilter();
      int id = 0;
      if (int.TryParse(providerId, out id))
        filter.ProviderId = id;

      return DBServices.GetLastGameInstances(filter);
    }

    private List<SimulatePlayerBetsWorker> GetPlayerBetsWorkers(IProgress<PlayerBetsReportProgressData> reportProgress, IList<PlaySessionData> playSessions, IList<GameInstanceData> gameInstances, string serverURI, int delayBetweenBets, decimal betAmount, string currency, PlayType playType)
    {
      List<SimulatePlayerBetsWorker> playerWorkers = new List<SimulatePlayerBetsWorker>();
      SimulatePlayerBetsWorker playerWorker;
      string sessionId = string.Empty;
      string accountId = string.Empty;
      foreach (PlaySessionData playSession in playSessions)
      {
        sessionId = GetFormattedSessionId(playSession, currentProvider);
        accountId = GetAccountId(sessionId);
        playerWorker = new SimulatePlayerBetsWorker(GatewayServices, gameInstances, serverURI, sessionId, accountId, currentPartnerId, currentPassword, delayBetweenBets, betAmount, currency, playType, reportProgress);
        playerWorkers.Add(playerWorker);
      }

      return playerWorkers;
    }

    private BindingList<ConcurrentConnectionsGeneralReportData> generalReportDataList;
    private BindingList<ConcurrentConnectionsGeneralReportData> GeneralReportDataList
    {
      get
      {
        if (generalReportDataList == null)
          generalReportDataList = new BindingList<ConcurrentConnectionsGeneralReportData>();

        return generalReportDataList;
      }
    }

    private void SimulationReportProgress(PlayerBetsReportProgressData progressData)
    {
      string playSessionId = progressData.SessionId.Substring(0, progressData.SessionId.IndexOf("_"));
      //DataGridView grid = InsertResultsGridConcurrentConnections(playSessionId);
      //grid.Rows.Add(progressData.RequestOperationType, progressData.ElapsedTime.ToString(), progressData.XMLRequest, progressData.XMLResponse);

      //Update general results
      bool isDebit = progressData.RequestOperationType == RequestOperationType.Debit;
      bool isLogin = progressData.RequestOperationType == RequestOperationType.Login;
      bool isCredit = progressData.RequestOperationType == RequestOperationType.BulkCredit;
      ConcurrentConnectionsGeneralReportData generalReportData = GeneralReportDataList.SingleOrDefault<ConcurrentConnectionsGeneralReportData>(x => x.PlaySessionId == playSessionId);
      if(generalReportData == null)
      {
        GeneralReportDataList.Add(new ConcurrentConnectionsGeneralReportData
        {
          PlaySessionId = playSessionId,
          TotalServerCalls = 1,
          TotalBets = isDebit ? 1 : 0,
          TotalLogins = isLogin ? 1 : 0,
          TotalWins = isCredit ? 1 : 0
        });
      }
      else
      {
        if (isDebit)
          generalReportData.TotalBets++;

        if (isLogin)
          generalReportData.TotalLogins++;

        if (isCredit)
          generalReportData.TotalWins++;

        generalReportData.TotalServerCalls++;
      }

      //Write details results in files
      FileWriter.Instance.ThreadSafeWrite(progressData.ToFileLine(), string.Format("{0}.txt", GetSimulationResultsFolder() + playSessionId), true);
    }

    private string GetSimulationResultsFolder()
    {
      return System.IO.Directory.GetCurrentDirectory() + @"\Simulation Results\";
    }

    private DataGridView InsertResultsGridConcurrentConnections( string playSessionId )
    {
      string tabPageKey = RESULTS_CONCURRENT_CONNECTIONS_TAB_PREFIX_ID + playSessionId;
      var tabPage = tabControlConcurrentConnectionsResults.TabPages[tabPageKey];
      DataGridView dgvConcurrentConnectionsResultDetails;
      if (tabPage == null)
      {
        //Add Tab Page
        tabControlConcurrentConnectionsResults.TabPages.Add(tabPageKey, string.Format("PLAY SESSION ID: {0}", playSessionId));
        var currentTabPage = tabControlConcurrentConnectionsResults.TabPages[tabPageKey];
        //Add Grid
        dgvConcurrentConnectionsResultDetails = new DataGridView();
        dgvConcurrentConnectionsResultDetails.Name = RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId;
        dgvConcurrentConnectionsResultDetails.Dock = System.Windows.Forms.DockStyle.Fill;
        dgvConcurrentConnectionsResultDetails.ReadOnly = true;
        dgvConcurrentConnectionsResultDetails.AllowUserToAddRows = false;
        dgvConcurrentConnectionsResultDetails.MultiSelect = false;
        dgvConcurrentConnectionsResultDetails.RowHeadersVisible = false;
        dgvConcurrentConnectionsResultDetails.AutoGenerateColumns = false;

        DataGridViewTextBoxColumn columnOperationType = new DataGridViewTextBoxColumn();
        columnOperationType.Name = "columnOperationType" + RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId;
        columnOperationType.HeaderText = "Operation Type";
        columnOperationType.DataPropertyName = "RequestOperationTypeString";
        columnOperationType.ReadOnly = true;
        columnOperationType.Width = 130;

        DataGridViewTextBoxColumn columnElapsedTime = new DataGridViewTextBoxColumn();
        columnElapsedTime.Name = "columnElapsedTime" + RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId;
        columnElapsedTime.HeaderText = "Elapsed Time";
        columnElapsedTime.DataPropertyName = "ElapsedTimeString";
        columnElapsedTime.ReadOnly = true;
        columnElapsedTime.Width = 130;

        DataGridViewButtonColumn columnViewXmlRequest = new DataGridViewButtonColumn();
        columnViewXmlRequest.Name = "columnViewXmlRequest" + RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId;
        columnViewXmlRequest.HeaderText = "View Request";
        columnViewXmlRequest.Text = "XML Request";
        columnViewXmlRequest.UseColumnTextForButtonValue = true;
        columnViewXmlRequest.Width = 130;

        DataGridViewButtonColumn columnViewXmlResponse = new DataGridViewButtonColumn();
        columnViewXmlResponse.Name = "columnViewXmlResponse" + RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId;
        columnViewXmlResponse.HeaderText = "View Response";
        columnViewXmlResponse.Text = "XML Response";
        columnViewXmlResponse.UseColumnTextForButtonValue = true;
        columnViewXmlResponse.Width = 130;

        dgvConcurrentConnectionsResultDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            columnOperationType,
            columnElapsedTime,
            columnViewXmlRequest,
            columnViewXmlResponse
        });

        dgvConcurrentConnectionsResultDetails.CellClick += dgvConcurrentConnectionsResultDetails_CellClick;
        currentTabPage.Controls.Add(dgvConcurrentConnectionsResultDetails);
      }
      else
      {
        dgvConcurrentConnectionsResultDetails = (DataGridView) tabPage.Controls.Find(RESULTS_CONCURRENT_CONNECTIONS_GRID_PREFIX_ID + playSessionId, true)[0];
      }

      return dgvConcurrentConnectionsResultDetails;
    }

    void dgvConcurrentConnectionsResultDetails_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex == -1)
        return;

      DataGridView grid = (DataGridView)sender;
      if (grid.Columns[e.ColumnIndex].Name.StartsWith("columnViewXmlRequest"))
      {  
        PlayerBetsReportProgressData selectedProgressData = (PlayerBetsReportProgressData)grid.Rows[e.RowIndex].DataBoundItem;
        string playSessionId = selectedProgressData.SessionId.Substring(0, selectedProgressData.SessionId.IndexOf("_"));
        using (PlaySessionViewXML playSessionRequestForm = new PlaySessionViewXML(selectedProgressData.XMLRequest, playSessionId, selectedProgressData.RequestOperationTypeString))
          playSessionRequestForm.ShowDialog();
      }

      if (grid.Columns[e.ColumnIndex].Name.StartsWith("columnViewXmlResponse"))
      {
        PlayerBetsReportProgressData selectedProgressData = (PlayerBetsReportProgressData)grid.Rows[e.RowIndex].DataBoundItem;
        string playSessionId = selectedProgressData.SessionId.Substring(0, selectedProgressData.SessionId.IndexOf("_"));
        using (PlaySessionViewXML playSessionResponseForm = new PlaySessionViewXML(selectedProgressData.XMLResponse, playSessionId, selectedProgressData.RequestOperationTypeString))
          playSessionResponseForm.ShowDialog();
      }
    }

    private async void dgvConcurrentConnectionsGeneralResults_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (dgvConcurrentConnectionsGeneralResults.Columns[e.ColumnIndex].Name == "dgvColumnGeneralResultViewDetail")
      {
        if (e.RowIndex == -1)
          return;

        try
        {
          ShowLoading(dgvConcurrentConnectionsGeneralResults);
          string playSessionId = dgvConcurrentConnectionsGeneralResults.Rows[e.RowIndex].Cells[0].Value.ToString();

          List<PlayerBetsReportProgressData> progressDataList = await Task<List<PlayerBetsReportProgressData>>.Run(() =>
          {
            return PlayerBetsReportProgressData.LoadListFromFile(string.Format("{0}.txt", GetSimulationResultsFolder() + playSessionId));
          });

          DataGridView grid = InsertResultsGridConcurrentConnections(playSessionId);
          grid.DataSource = progressDataList;
          tabControlConcurrentConnectionsResults.SelectedTab = tabControlConcurrentConnectionsResults.TabPages[RESULTS_CONCURRENT_CONNECTIONS_TAB_PREFIX_ID + playSessionId];
        }
        catch (Exception ex)
        {
          base.ShowError(ex);
        }
        finally
        {
          HideLoading(dgvConcurrentConnectionsGeneralResults);
        }
      }
    }

    private void btnStopConcurrentConnections_Click(object sender, EventArgs e)
    {
      CancelSimulation();
    }

    #endregion

    private void dtpGameCreatedRequestGameStarts_ValueChanged(object sender, EventArgs e)
    {
      txtGameCreatedRequestGameStarts.Text = dtpGameCreatedRequestGameStarts.Value.ToEpoch().ToString();
    }

  }
}
