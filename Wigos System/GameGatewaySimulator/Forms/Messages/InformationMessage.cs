﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.Common;

namespace GameGatewaySimulator.Forms.Messages
{
  public partial class InformationMessage : Form
  {
    public InformationMessage(string message)
    {
      InitializeComponent();
      lblError.Text = message;
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      Misc.WriteLog("[FORM CLOSE] informationmessage (close)", Log.Type.Message);
      this.Close();
    }
  }
}
