﻿namespace GameGatewaySimulator.Forms.Messages
{
  partial class ErrorMessage
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblError = new System.Windows.Forms.Label();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.btnClose = new System.Windows.Forms.Button();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.btnDetails = new System.Windows.Forms.Button();
      this.txtErrorDetails = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // lblError
      // 
      this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.lblError.AutoSize = true;
      this.lblError.Location = new System.Drawing.Point(3, 17);
      this.lblError.Name = "lblError";
      this.lblError.Size = new System.Drawing.Size(314, 13);
      this.lblError.TabIndex = 0;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 1;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.lblError, 0, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(67, 12);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(320, 47);
      this.tableLayoutPanel1.TabIndex = 2;
      // 
      // btnClose
      // 
      this.btnClose.Image = global::GameGatewaySimulator.Properties.Resources.close;
      this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnClose.Location = new System.Drawing.Point(302, 65);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(85, 35);
      this.btnClose.TabIndex = 3;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = global::GameGatewaySimulator.Properties.Resources.error;
      this.pictureBox1.Location = new System.Drawing.Point(12, 12);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(47, 47);
      this.pictureBox1.TabIndex = 1;
      this.pictureBox1.TabStop = false;
      // 
      // btnDetails
      // 
      this.btnDetails.Image = global::GameGatewaySimulator.Properties.Resources.details;
      this.btnDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnDetails.Location = new System.Drawing.Point(12, 65);
      this.btnDetails.Name = "btnDetails";
      this.btnDetails.Size = new System.Drawing.Size(116, 35);
      this.btnDetails.TabIndex = 4;
      this.btnDetails.Text = "Show Details";
      this.btnDetails.UseVisualStyleBackColor = true;
      this.btnDetails.Click += new System.EventHandler(this.btnDetails_Click);
      // 
      // txtErrorDetails
      // 
      this.txtErrorDetails.Location = new System.Drawing.Point(12, 116);
      this.txtErrorDetails.Multiline = true;
      this.txtErrorDetails.Name = "txtErrorDetails";
      this.txtErrorDetails.ReadOnly = true;
      this.txtErrorDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtErrorDetails.Size = new System.Drawing.Size(375, 192);
      this.txtErrorDetails.TabIndex = 5;
      // 
      // ErrorMessage
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(399, 319);
      this.Controls.Add(this.txtErrorDetails);
      this.Controls.Add(this.btnDetails);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Controls.Add(this.pictureBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ErrorMessage";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Ha ocurrido un error!!";
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblError;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Button btnDetails;
    private System.Windows.Forms.TextBox txtErrorDetails;
  }
}