﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.Common;

namespace GameGatewaySimulator.Forms.Messages
{
  public partial class ErrorMessage : Form
  {
    private const int HEIGHT_WITHOUT_DETAILS = 147;
    private const int HEIGHT_WITH_DETAILS = 357;

    public ErrorMessage(string message)
    {
      InitializeComponent();
      lblError.Text = message;
      this.Size = new System.Drawing.Size(this.Size.Width, HEIGHT_WITHOUT_DETAILS);
    }

    public ErrorMessage(Exception ex) : this(ex.Message)
    {
      if (ex.InnerException != null)
        txtErrorDetails.Text = ex.InnerException.Message;
    }

    private void btnClose_Click(object sender, EventArgs e)
    {

      Misc.WriteLog("[FORM CLOSE] errormessage (close)", Log.Type.Message);
      this.Close();
    }

    private void btnDetails_Click(object sender, EventArgs e)
    {
      ToogleDetails();
    }

    private void ToogleDetails()
    {
      this.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height == HEIGHT_WITHOUT_DETAILS ? HEIGHT_WITH_DETAILS : HEIGHT_WITHOUT_DETAILS);
    }
  }
}
