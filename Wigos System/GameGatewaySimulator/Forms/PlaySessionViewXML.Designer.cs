﻿namespace GameGatewaySimulator.Forms
{
  partial class PlaySessionViewXML
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.btnClose = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.lblOperationType = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.lblPlaySessionId = new System.Windows.Forms.Label();
      this.webBrowserData = new System.Windows.Forms.WebBrowser();
      this.panel2.SuspendLayout();
      this.panel3.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.webBrowserData);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 41);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(780, 401);
      this.panel2.TabIndex = 6;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.btnClose);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(0, 442);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(780, 44);
      this.panel3.TabIndex = 5;
      // 
      // btnClose
      // 
      this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnClose.Image = global::GameGatewaySimulator.Properties.Resources.close;
      this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnClose.Location = new System.Drawing.Point(689, 6);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(85, 30);
      this.btnClose.TabIndex = 8;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.lblOperationType);
      this.panel1.Controls.Add(this.label4);
      this.panel1.Controls.Add(this.lblPlaySessionId);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(780, 41);
      this.panel1.TabIndex = 4;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(288, 8);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(166, 25);
      this.label1.TabIndex = 9;
      this.label1.Text = "Operation Type:";
      // 
      // lblOperationType
      // 
      this.lblOperationType.AutoSize = true;
      this.lblOperationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblOperationType.Location = new System.Drawing.Point(448, 8);
      this.lblOperationType.Name = "lblOperationType";
      this.lblOperationType.Size = new System.Drawing.Size(0, 25);
      this.lblOperationType.TabIndex = 10;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(7, 8);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(166, 25);
      this.label4.TabIndex = 7;
      this.label4.Text = "Play Session Id:";
      // 
      // lblPlaySessionId
      // 
      this.lblPlaySessionId.AutoSize = true;
      this.lblPlaySessionId.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblPlaySessionId.Location = new System.Drawing.Point(167, 8);
      this.lblPlaySessionId.Name = "lblPlaySessionId";
      this.lblPlaySessionId.Size = new System.Drawing.Size(0, 25);
      this.lblPlaySessionId.TabIndex = 8;
      // 
      // webBrowserData
      // 
      this.webBrowserData.AllowWebBrowserDrop = false;
      this.webBrowserData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.webBrowserData.IsWebBrowserContextMenuEnabled = false;
      this.webBrowserData.Location = new System.Drawing.Point(0, 0);
      this.webBrowserData.MinimumSize = new System.Drawing.Size(20, 20);
      this.webBrowserData.Name = "webBrowserData";
      this.webBrowserData.Size = new System.Drawing.Size(780, 401);
      this.webBrowserData.TabIndex = 0;
      this.webBrowserData.WebBrowserShortcutsEnabled = false;
      // 
      // PlaySessionViewXML
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(780, 486);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel1);
      this.MinimizeBox = false;
      this.Name = "PlaySessionViewXML";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "XML";
      this.panel2.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lblPlaySessionId;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lblOperationType;
    private System.Windows.Forms.WebBrowser webBrowserData;
  }
}