﻿namespace GameGatewaySimulator.Forms
{
  partial class LCDSimulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panelContent = new System.Windows.Forms.Panel();
      this.gbGatewaySimulator = new System.Windows.Forms.GroupBox();
      this.tabControlGateway = new System.Windows.Forms.TabControl();
      this.tabPageLoginRequest = new System.Windows.Forms.TabPage();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.searchPlaySessionsLoginRequest = new GameGatewaySimulator.UserControls.SearchPlaySessions();
      this.txtLoginRequestProvider = new System.Windows.Forms.TextBox();
      this.lblLoginRequestProvider = new System.Windows.Forms.Label();
      this.btnBuildLoginRequest = new System.Windows.Forms.Button();
      this.btnSendLoginRequest = new System.Windows.Forms.Button();
      this.txtLoginRequestPassword = new System.Windows.Forms.TextBox();
      this.lblLoginRequestPassword = new System.Windows.Forms.Label();
      this.txtLoginRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblLoginRequestPartnerId = new System.Windows.Forms.Label();
      this.txtLoginRequestSessionId = new System.Windows.Forms.TextBox();
      this.lblLoginRequestSessionId = new System.Windows.Forms.Label();
      this.splitContainer9 = new System.Windows.Forms.SplitContainer();
      this.groupBox21 = new System.Windows.Forms.GroupBox();
      this.requestDataLoginRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.responseDataLoginRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageDebitRequest = new System.Windows.Forms.TabPage();
      this.splitContainer4 = new System.Windows.Forms.SplitContainer();
      this.groupBox7 = new System.Windows.Forms.GroupBox();
      this.txtDebitRequestGameId = new System.Windows.Forms.TextBox();
      this.lblDebitRequestGameId = new System.Windows.Forms.Label();
      this.txtDebitRequestTransaction = new System.Windows.Forms.TextBox();
      this.lblDebitRequestTransaction = new System.Windows.Forms.Label();
      this.btnBuildDebitRequest = new System.Windows.Forms.Button();
      this.txtDebitRequestPassword = new System.Windows.Forms.TextBox();
      this.lblDebitRequestPassword = new System.Windows.Forms.Label();
      this.txtDebitRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblDebitRequestPartnerId = new System.Windows.Forms.Label();
      this.gbDebitRequestBets = new System.Windows.Forms.GroupBox();
      this.btnDebitRequestAddBet = new System.Windows.Forms.Button();
      this.txtDebitRequestBetRake = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblDebitRequestBetRake = new System.Windows.Forms.Label();
      this.txtDebitRequestBetValue = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblDebitRequestBetValue = new System.Windows.Forms.Label();
      this.txtDebitRequestBetId = new System.Windows.Forms.TextBox();
      this.lblDebitRequestBetId = new System.Windows.Forms.Label();
      this.dgvDebitRequestBets = new System.Windows.Forms.DataGridView();
      this.dgvColumnDebitBetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnDebitBetValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnDebitBetRake = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnDebitBetDelete = new System.Windows.Forms.DataGridViewButtonColumn();
      this.txtDebitRequestCurrency = new System.Windows.Forms.TextBox();
      this.lblDebitRequestCurrency = new System.Windows.Forms.Label();
      this.txtDebitRequestAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblDebitRequestAmount = new System.Windows.Forms.Label();
      this.btnSendDebitRequest = new System.Windows.Forms.Button();
      this.txtDebitRequestNumberOfBets = new System.Windows.Forms.TextBox();
      this.lblDebitRequestNumberOfBets = new System.Windows.Forms.Label();
      this.txtDebitRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblDebitRequestGameInstanceId = new System.Windows.Forms.Label();
      this.txtDebitRequestSessionId = new System.Windows.Forms.TextBox();
      this.lblDebitRequestSessionId = new System.Windows.Forms.Label();
      this.splitContainer12 = new System.Windows.Forms.SplitContainer();
      this.groupBox8 = new System.Windows.Forms.GroupBox();
      this.requestDataDebitRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox24 = new System.Windows.Forms.GroupBox();
      this.responseDataDebitRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageBulkCreditRequest = new System.Windows.Forms.TabPage();
      this.splitContainer6 = new System.Windows.Forms.SplitContainer();
      this.groupBox12 = new System.Windows.Forms.GroupBox();
      this.txtBulkCreditRequestGameId = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestGameId = new System.Windows.Forms.Label();
      this.txtBulkCreditRequestTransaction = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestTransaction = new System.Windows.Forms.Label();
      this.btnBuildBulkCreditRequest = new System.Windows.Forms.Button();
      this.chkBulkCreditRequestRollback = new System.Windows.Forms.CheckBox();
      this.txtBulkCreditRequestPassword = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestPassword = new System.Windows.Forms.Label();
      this.txtBulkCreditRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestPartnerId = new System.Windows.Forms.Label();
      this.groupBox13 = new System.Windows.Forms.GroupBox();
      this.txtBulkCreditRequestCreditJackpot = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblBulkCreditRequestBetJackpot = new System.Windows.Forms.Label();
      this.btnSearchPlaySessionsBulkCredit = new System.Windows.Forms.Button();
      this.btnBulkCreditRequestAddCredit = new System.Windows.Forms.Button();
      this.txtBulkCreditRequestCreditUserId = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestBetUserId = new System.Windows.Forms.Label();
      this.txtBulkCreditRequestCreditAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblBulkCreditRequestCreditAmount = new System.Windows.Forms.Label();
      this.txtBulkCreditRequestCreditBetId = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestBetId = new System.Windows.Forms.Label();
      this.dgvBulkCreditRequestCredits = new System.Windows.Forms.DataGridView();
      this.dgvColumnBulkCreditBetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnBulkCreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnBulkCreditUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnBulkCreditJackpot = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnBulkCreditDelete = new System.Windows.Forms.DataGridViewButtonColumn();
      this.txtBulkCreditRequestCurrency = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestCurrency = new System.Windows.Forms.Label();
      this.btnSendBulkCreditRequest = new System.Windows.Forms.Button();
      this.lblBulkCreditRequestRollback = new System.Windows.Forms.Label();
      this.txtBulkCreditRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblBulkCreditRequestGameInstanceId = new System.Windows.Forms.Label();
      this.splitContainer14 = new System.Windows.Forms.SplitContainer();
      this.groupBox14 = new System.Windows.Forms.GroupBox();
      this.requestDataBulkCreditRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox26 = new System.Windows.Forms.GroupBox();
      this.responseDataBulkCreditRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageLogoutRequest = new System.Windows.Forms.TabPage();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.btnBuildLogoutRequest = new System.Windows.Forms.Button();
      this.btnSendLogoutRequest = new System.Windows.Forms.Button();
      this.txtLogoutRequestPassword = new System.Windows.Forms.TextBox();
      this.lblLogoutRequestPassword = new System.Windows.Forms.Label();
      this.txtLogoutRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblLogoutRequestPartnerId = new System.Windows.Forms.Label();
      this.txtLogoutRequestSessionId = new System.Windows.Forms.TextBox();
      this.lblLogoutRequestSessionId = new System.Windows.Forms.Label();
      this.splitContainer10 = new System.Windows.Forms.SplitContainer();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.requestDataLogoutRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox22 = new System.Windows.Forms.GroupBox();
      this.responseDataLogoutRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageOfflineCreditRequest = new System.Windows.Forms.TabPage();
      this.splitContainer7 = new System.Windows.Forms.SplitContainer();
      this.groupBox15 = new System.Windows.Forms.GroupBox();
      this.txtOfflineCreditRequestGameId = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestGameId = new System.Windows.Forms.Label();
      this.txtOfflineCreditRequestTransaction = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestTransaction = new System.Windows.Forms.Label();
      this.btnBuildOfflineCreditRequest = new System.Windows.Forms.Button();
      this.txtOfflineCreditRequestPassword = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestPassword = new System.Windows.Forms.Label();
      this.txtOfflineCreditRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestPartnerId = new System.Windows.Forms.Label();
      this.groupBox16 = new System.Windows.Forms.GroupBox();
      this.txtOfflineCreditRequestBetJackpot = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineCreditRequestBetJackpot = new System.Windows.Forms.Label();
      this.btnOfflineCreditRequestAddBet = new System.Windows.Forms.Button();
      this.txtOfflineCreditRequestBetAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineCreditRequestBetAmount = new System.Windows.Forms.Label();
      this.txtOfflineCreditRequestBetId = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestBetId = new System.Windows.Forms.Label();
      this.dgvOfflineCreditRequestBets = new System.Windows.Forms.DataGridView();
      this.dgvColumnOfflineCreditBetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineCreditBetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineCreditBetJackpot = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineCreditBetDelete = new System.Windows.Forms.DataGridViewButtonColumn();
      this.txtOfflineCreditRequestCurrency = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestCurrency = new System.Windows.Forms.Label();
      this.txtOfflineCreditRequestAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineCreditRequestAmount = new System.Windows.Forms.Label();
      this.btnSendOfflineCreditRequest = new System.Windows.Forms.Button();
      this.txtOfflineCreditRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestGameInstanceId = new System.Windows.Forms.Label();
      this.txtOfflineCreditRequestUserId = new System.Windows.Forms.TextBox();
      this.lblOfflineCreditRequestUserId = new System.Windows.Forms.Label();
      this.splitContainer15 = new System.Windows.Forms.SplitContainer();
      this.groupBox17 = new System.Windows.Forms.GroupBox();
      this.requestDataOfflineCreditRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox27 = new System.Windows.Forms.GroupBox();
      this.responseDataOfflineCreditRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageCreditRequest = new System.Windows.Forms.TabPage();
      this.splitContainer5 = new System.Windows.Forms.SplitContainer();
      this.groupBox9 = new System.Windows.Forms.GroupBox();
      this.txtCreditRequestGameId = new System.Windows.Forms.TextBox();
      this.lblCreditRequestGameId = new System.Windows.Forms.Label();
      this.txtCreditRequestTransaction = new System.Windows.Forms.TextBox();
      this.lblCreditRequestTransaction = new System.Windows.Forms.Label();
      this.txtCreditRequestPassword = new System.Windows.Forms.TextBox();
      this.lblCreditRequestPassword = new System.Windows.Forms.Label();
      this.txtCreditRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblCreditRequestPartnerId = new System.Windows.Forms.Label();
      this.chkCreditRequestBonus = new System.Windows.Forms.CheckBox();
      this.lblCreditRequestBonus = new System.Windows.Forms.Label();
      this.btnBuildCreditRequest = new System.Windows.Forms.Button();
      this.groupBox10 = new System.Windows.Forms.GroupBox();
      this.txtCreditRequestBetJackpot = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblCreditRequestBetJackpot = new System.Windows.Forms.Label();
      this.btnCreditRequestAddBet = new System.Windows.Forms.Button();
      this.txtCreditRequestBetAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblCreditRequestBetAmount = new System.Windows.Forms.Label();
      this.txtCreditRequestBetId = new System.Windows.Forms.TextBox();
      this.lblCreditRequestBetId = new System.Windows.Forms.Label();
      this.dgvCreditRequestBets = new System.Windows.Forms.DataGridView();
      this.dgvColumnCreditBetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnCreditBetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnCreditBetJackpot = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnCreditBetDelete = new System.Windows.Forms.DataGridViewButtonColumn();
      this.txtCreditRequestCurrency = new System.Windows.Forms.TextBox();
      this.lblCreditRequestCurrency = new System.Windows.Forms.Label();
      this.txtCreditRequestAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblCreditRequestAmount = new System.Windows.Forms.Label();
      this.btnSendCreditRequest = new System.Windows.Forms.Button();
      this.txtCreditRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblCreditRequestGameInstanceId = new System.Windows.Forms.Label();
      this.txtCreditRequestSessionId = new System.Windows.Forms.TextBox();
      this.lblCreditRequestSessionId = new System.Windows.Forms.Label();
      this.splitContainer13 = new System.Windows.Forms.SplitContainer();
      this.groupBox11 = new System.Windows.Forms.GroupBox();
      this.requestDataCreditRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox25 = new System.Windows.Forms.GroupBox();
      this.responseDataCreditRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageOfflineDebitRequest = new System.Windows.Forms.TabPage();
      this.splitContainer8 = new System.Windows.Forms.SplitContainer();
      this.groupBox18 = new System.Windows.Forms.GroupBox();
      this.txtOfflineDebitRequestGameId = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestGameId = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestTransaction = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestTransaction = new System.Windows.Forms.Label();
      this.btnBuildOfflineDebitRequest = new System.Windows.Forms.Button();
      this.txtOfflineDebitRequestUserId = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestUserId = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestPassword = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestPassword = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestPartnerId = new System.Windows.Forms.Label();
      this.groupBox19 = new System.Windows.Forms.GroupBox();
      this.btnOfflineDebitRequestAddBet = new System.Windows.Forms.Button();
      this.txtOfflineDebitRequestBetRake = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineDebitRequestBetRake = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestBetValue = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineDebitRequestBetValue = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestBetId = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestBetId = new System.Windows.Forms.Label();
      this.dgvOfflineDebitRequestBets = new System.Windows.Forms.DataGridView();
      this.dgvColumnOfflineDebitBetId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineDebitBetValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineDebitBetRake = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnOfflineDebitBetDelete = new System.Windows.Forms.DataGridViewButtonColumn();
      this.txtOfflineDebitRequestCurrency = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestCurrency = new System.Windows.Forms.Label();
      this.txtOfflineDebitRequestAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblOfflineDebitRequestAmount = new System.Windows.Forms.Label();
      this.btnSendOfflineDebitRequest = new System.Windows.Forms.Button();
      this.txtOfflineDebitRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblOfflineDebitRequestGameInstanceId = new System.Windows.Forms.Label();
      this.splitContainer16 = new System.Windows.Forms.SplitContainer();
      this.groupBox20 = new System.Windows.Forms.GroupBox();
      this.requestDataOfflineDebitRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox28 = new System.Windows.Forms.GroupBox();
      this.responseDataOfflineDebitRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageGameCreatedRequest = new System.Windows.Forms.TabPage();
      this.splitContainer17 = new System.Windows.Forms.SplitContainer();
      this.groupBox30 = new System.Windows.Forms.GroupBox();
      this.dtpGameCreatedRequestGameStarts = new System.Windows.Forms.DateTimePicker();
      this.txtGameCreatedRequestGameTitle = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestGameTitle = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestPassword = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestPassword = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestPartnerId = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestJackpot = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblGameCreatedRequestJackpot = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestGameLogo = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestGameLogo = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestEntryCost = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblGameCreatedRequestEntryCost = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestFirstPrize = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblGameCreatedRequestFirstPrize = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestGameStarts = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.lblGameCreatedRequestGameStarts = new System.Windows.Forms.Label();
      this.btnBuildGameCreatedRequest = new System.Windows.Forms.Button();
      this.btnSendGameCreatedRequest = new System.Windows.Forms.Button();
      this.txtGameCreatedRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestGameInstanceId = new System.Windows.Forms.Label();
      this.txtGameCreatedRequestGameId = new System.Windows.Forms.TextBox();
      this.lblGameCreatedRequestGameId = new System.Windows.Forms.Label();
      this.splitContainer18 = new System.Windows.Forms.SplitContainer();
      this.groupBox31 = new System.Windows.Forms.GroupBox();
      this.requestDataGameCreatedRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox32 = new System.Windows.Forms.GroupBox();
      this.responseDataGameCreatedRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageGameStartingRequest = new System.Windows.Forms.TabPage();
      this.splitContainer19 = new System.Windows.Forms.SplitContainer();
      this.groupBox33 = new System.Windows.Forms.GroupBox();
      this.txtGameStartingRequestPassword = new System.Windows.Forms.TextBox();
      this.lblGameStartingRequestPassword = new System.Windows.Forms.Label();
      this.txtGameStartingRequestPartnerId = new System.Windows.Forms.TextBox();
      this.lblGameStartingRequestPartnerId = new System.Windows.Forms.Label();
      this.btnBuildGameStartingRequest = new System.Windows.Forms.Button();
      this.btnSendGameStartingRequest = new System.Windows.Forms.Button();
      this.txtGameStartingRequestGameInstanceId = new System.Windows.Forms.TextBox();
      this.lblGameStartingRequestGameInstanceId = new System.Windows.Forms.Label();
      this.txtGameStartingRequestGameId = new System.Windows.Forms.TextBox();
      this.lblGameStartingRequestGameId = new System.Windows.Forms.Label();
      this.splitContainer20 = new System.Windows.Forms.SplitContainer();
      this.groupBox34 = new System.Windows.Forms.GroupBox();
      this.requestDataGameStartingRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox35 = new System.Windows.Forms.GroupBox();
      this.responseDataGameStartingRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageCustomXMLRequest = new System.Windows.Forms.TabPage();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.panelFreeXML = new System.Windows.Forms.Panel();
      this.groupBoxCustomXML = new System.Windows.Forms.GroupBox();
      this.xmlEditorRequest = new GameGatewaySimulator.UserControls.XMLEditor();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnSendCustomXMLRequest = new System.Windows.Forms.Button();
      this.btnBuildCustomXMLRequest = new System.Windows.Forms.Button();
      this.splitContainer11 = new System.Windows.Forms.SplitContainer();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.requestDataCustomXMLRequest = new GameGatewaySimulator.UserControls.RequestData();
      this.groupBox23 = new System.Windows.Forms.GroupBox();
      this.responseDataCustomXMLRequest = new GameGatewaySimulator.UserControls.ResponseData();
      this.tabPageSimulateConcurrentConnections = new System.Windows.Forms.TabPage();
      this.splitContainer21 = new System.Windows.Forms.SplitContainer();
      this.gbConcurrentConnectionsParameters = new System.Windows.Forms.GroupBox();
      this.gbConcurrentConnectionsConsoleOutput = new System.Windows.Forms.GroupBox();
      this.txtConcurrentConnectionsConsoleOutput = new System.Windows.Forms.TextBox();
      this.btnStopConcurrentConnections = new System.Windows.Forms.Button();
      this.cboConcurrentConnectionsPlayType = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.txtConcurrentConnectionsDelayBetweenBets = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.txtConcurrentConnectionsNumberOfPlayers = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.btnRunConcurrentConnections = new System.Windows.Forms.Button();
      this.txtConcurrentConnectionsBetAmount = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.groupBox29 = new System.Windows.Forms.GroupBox();
      this.tabControlConcurrentConnectionsResults = new System.Windows.Forms.TabControl();
      this.tabPageConcurrentConnectionsGeneralResults = new System.Windows.Forms.TabPage();
      this.dgvConcurrentConnectionsGeneralResults = new System.Windows.Forms.DataGridView();
      this.dgvColumnGeneralResultPlaySessionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnGeneralResultTotalServerCalls = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnGeneralResultTotalLogins = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnGeneralResultTotalBets = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnGeneralResultTotalWins = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnGeneralResultViewDetail = new System.Windows.Forms.DataGridViewButtonColumn();
      this.gbServerSettings = new System.Windows.Forms.GroupBox();
      this.lblTestConnectionResult = new System.Windows.Forms.Label();
      this.pbTestConnectionResult = new System.Windows.Forms.PictureBox();
      this.btnTestConnection = new System.Windows.Forms.Button();
      this.txtServerUri = new System.Windows.Forms.TextBox();
      this.lblServerUri = new System.Windows.Forms.Label();
      this.panelBottom = new System.Windows.Forms.Panel();
      this.lblSelectedPlaySession = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.lblGameStartingRequestJackpot = new System.Windows.Forms.Label();
      this.txtGameStartingRequestJackpot = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.panelContent.SuspendLayout();
      this.gbGatewaySimulator.SuspendLayout();
      this.tabControlGateway.SuspendLayout();
      this.tabPageLoginRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
      this.splitContainer9.Panel1.SuspendLayout();
      this.splitContainer9.Panel2.SuspendLayout();
      this.splitContainer9.SuspendLayout();
      this.groupBox21.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.tabPageDebitRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
      this.splitContainer4.Panel1.SuspendLayout();
      this.splitContainer4.Panel2.SuspendLayout();
      this.splitContainer4.SuspendLayout();
      this.groupBox7.SuspendLayout();
      this.gbDebitRequestBets.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvDebitRequestBets)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
      this.splitContainer12.Panel1.SuspendLayout();
      this.splitContainer12.Panel2.SuspendLayout();
      this.splitContainer12.SuspendLayout();
      this.groupBox8.SuspendLayout();
      this.groupBox24.SuspendLayout();
      this.tabPageBulkCreditRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
      this.splitContainer6.Panel1.SuspendLayout();
      this.splitContainer6.Panel2.SuspendLayout();
      this.splitContainer6.SuspendLayout();
      this.groupBox12.SuspendLayout();
      this.groupBox13.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvBulkCreditRequestCredits)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).BeginInit();
      this.splitContainer14.Panel1.SuspendLayout();
      this.splitContainer14.Panel2.SuspendLayout();
      this.splitContainer14.SuspendLayout();
      this.groupBox14.SuspendLayout();
      this.groupBox26.SuspendLayout();
      this.tabPageLogoutRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
      this.splitContainer10.Panel1.SuspendLayout();
      this.splitContainer10.Panel2.SuspendLayout();
      this.splitContainer10.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox22.SuspendLayout();
      this.tabPageOfflineCreditRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
      this.splitContainer7.Panel1.SuspendLayout();
      this.splitContainer7.Panel2.SuspendLayout();
      this.splitContainer7.SuspendLayout();
      this.groupBox15.SuspendLayout();
      this.groupBox16.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvOfflineCreditRequestBets)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
      this.splitContainer15.Panel1.SuspendLayout();
      this.splitContainer15.Panel2.SuspendLayout();
      this.splitContainer15.SuspendLayout();
      this.groupBox17.SuspendLayout();
      this.groupBox27.SuspendLayout();
      this.tabPageCreditRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
      this.splitContainer5.Panel1.SuspendLayout();
      this.splitContainer5.Panel2.SuspendLayout();
      this.splitContainer5.SuspendLayout();
      this.groupBox9.SuspendLayout();
      this.groupBox10.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvCreditRequestBets)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).BeginInit();
      this.splitContainer13.Panel1.SuspendLayout();
      this.splitContainer13.Panel2.SuspendLayout();
      this.splitContainer13.SuspendLayout();
      this.groupBox11.SuspendLayout();
      this.groupBox25.SuspendLayout();
      this.tabPageOfflineDebitRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
      this.splitContainer8.Panel1.SuspendLayout();
      this.splitContainer8.Panel2.SuspendLayout();
      this.splitContainer8.SuspendLayout();
      this.groupBox18.SuspendLayout();
      this.groupBox19.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvOfflineDebitRequestBets)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).BeginInit();
      this.splitContainer16.Panel1.SuspendLayout();
      this.splitContainer16.Panel2.SuspendLayout();
      this.splitContainer16.SuspendLayout();
      this.groupBox20.SuspendLayout();
      this.groupBox28.SuspendLayout();
      this.tabPageGameCreatedRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).BeginInit();
      this.splitContainer17.Panel1.SuspendLayout();
      this.splitContainer17.Panel2.SuspendLayout();
      this.splitContainer17.SuspendLayout();
      this.groupBox30.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).BeginInit();
      this.splitContainer18.Panel1.SuspendLayout();
      this.splitContainer18.Panel2.SuspendLayout();
      this.splitContainer18.SuspendLayout();
      this.groupBox31.SuspendLayout();
      this.groupBox32.SuspendLayout();
      this.tabPageGameStartingRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).BeginInit();
      this.splitContainer19.Panel1.SuspendLayout();
      this.splitContainer19.Panel2.SuspendLayout();
      this.splitContainer19.SuspendLayout();
      this.groupBox33.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).BeginInit();
      this.splitContainer20.Panel1.SuspendLayout();
      this.splitContainer20.Panel2.SuspendLayout();
      this.splitContainer20.SuspendLayout();
      this.groupBox34.SuspendLayout();
      this.groupBox35.SuspendLayout();
      this.tabPageCustomXMLRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.panelFreeXML.SuspendLayout();
      this.groupBoxCustomXML.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
      this.splitContainer11.Panel1.SuspendLayout();
      this.splitContainer11.Panel2.SuspendLayout();
      this.splitContainer11.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.groupBox23.SuspendLayout();
      this.tabPageSimulateConcurrentConnections.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).BeginInit();
      this.splitContainer21.Panel1.SuspendLayout();
      this.splitContainer21.Panel2.SuspendLayout();
      this.splitContainer21.SuspendLayout();
      this.gbConcurrentConnectionsParameters.SuspendLayout();
      this.gbConcurrentConnectionsConsoleOutput.SuspendLayout();
      this.groupBox29.SuspendLayout();
      this.tabControlConcurrentConnectionsResults.SuspendLayout();
      this.tabPageConcurrentConnectionsGeneralResults.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvConcurrentConnectionsGeneralResults)).BeginInit();
      this.gbServerSettings.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbTestConnectionResult)).BeginInit();
      this.panelBottom.SuspendLayout();
      this.SuspendLayout();
      // 
      // panelContent
      // 
      this.panelContent.Controls.Add(this.gbGatewaySimulator);
      this.panelContent.Controls.Add(this.gbServerSettings);
      this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelContent.Location = new System.Drawing.Point(0, 0);
      this.panelContent.Name = "panelContent";
      this.panelContent.Size = new System.Drawing.Size(1284, 676);
      this.panelContent.TabIndex = 2;
      // 
      // gbGatewaySimulator
      // 
      this.gbGatewaySimulator.Controls.Add(this.tabControlGateway);
      this.gbGatewaySimulator.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbGatewaySimulator.Location = new System.Drawing.Point(0, 58);
      this.gbGatewaySimulator.Name = "gbGatewaySimulator";
      this.gbGatewaySimulator.Size = new System.Drawing.Size(1284, 618);
      this.gbGatewaySimulator.TabIndex = 2;
      this.gbGatewaySimulator.TabStop = false;
      this.gbGatewaySimulator.Text = "Gateway Services";
      // 
      // tabControlGateway
      // 
      this.tabControlGateway.Controls.Add(this.tabPageLoginRequest);
      this.tabControlGateway.Controls.Add(this.tabPageDebitRequest);
      this.tabControlGateway.Controls.Add(this.tabPageBulkCreditRequest);
      this.tabControlGateway.Controls.Add(this.tabPageLogoutRequest);
      this.tabControlGateway.Controls.Add(this.tabPageOfflineCreditRequest);
      this.tabControlGateway.Controls.Add(this.tabPageCreditRequest);
      this.tabControlGateway.Controls.Add(this.tabPageOfflineDebitRequest);
      this.tabControlGateway.Controls.Add(this.tabPageGameCreatedRequest);
      this.tabControlGateway.Controls.Add(this.tabPageGameStartingRequest);
      this.tabControlGateway.Controls.Add(this.tabPageCustomXMLRequest);
      this.tabControlGateway.Controls.Add(this.tabPageSimulateConcurrentConnections);
      this.tabControlGateway.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControlGateway.Location = new System.Drawing.Point(3, 16);
      this.tabControlGateway.Name = "tabControlGateway";
      this.tabControlGateway.SelectedIndex = 0;
      this.tabControlGateway.Size = new System.Drawing.Size(1278, 599);
      this.tabControlGateway.TabIndex = 0;
      // 
      // tabPageLoginRequest
      // 
      this.tabPageLoginRequest.Controls.Add(this.splitContainer1);
      this.tabPageLoginRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageLoginRequest.Name = "tabPageLoginRequest";
      this.tabPageLoginRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageLoginRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageLoginRequest.TabIndex = 0;
      this.tabPageLoginRequest.Text = "Login Request";
      this.tabPageLoginRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer1.Location = new System.Drawing.Point(3, 3);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer9);
      this.splitContainer1.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer1.SplitterDistance = 486;
      this.splitContainer1.TabIndex = 0;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.searchPlaySessionsLoginRequest);
      this.groupBox2.Controls.Add(this.txtLoginRequestProvider);
      this.groupBox2.Controls.Add(this.lblLoginRequestProvider);
      this.groupBox2.Controls.Add(this.btnBuildLoginRequest);
      this.groupBox2.Controls.Add(this.btnSendLoginRequest);
      this.groupBox2.Controls.Add(this.txtLoginRequestPassword);
      this.groupBox2.Controls.Add(this.lblLoginRequestPassword);
      this.groupBox2.Controls.Add(this.txtLoginRequestPartnerId);
      this.groupBox2.Controls.Add(this.lblLoginRequestPartnerId);
      this.groupBox2.Controls.Add(this.txtLoginRequestSessionId);
      this.groupBox2.Controls.Add(this.lblLoginRequestSessionId);
      this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox2.Location = new System.Drawing.Point(0, 0);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(486, 567);
      this.groupBox2.TabIndex = 0;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Request data";
      // 
      // searchPlaySessionsLoginRequest
      // 
      this.searchPlaySessionsLoginRequest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.searchPlaySessionsLoginRequest.Location = new System.Drawing.Point(6, 16);
      this.searchPlaySessionsLoginRequest.Name = "searchPlaySessionsLoginRequest";
      this.searchPlaySessionsLoginRequest.Size = new System.Drawing.Size(474, 397);
      this.searchPlaySessionsLoginRequest.TabIndex = 0;
      // 
      // txtLoginRequestProvider
      // 
      this.txtLoginRequestProvider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLoginRequestProvider.Location = new System.Drawing.Point(69, 445);
      this.txtLoginRequestProvider.Name = "txtLoginRequestProvider";
      this.txtLoginRequestProvider.Size = new System.Drawing.Size(411, 20);
      this.txtLoginRequestProvider.TabIndex = 2;
      this.txtLoginRequestProvider.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtLoginRequestProvider_KeyUp);
      // 
      // lblLoginRequestProvider
      // 
      this.lblLoginRequestProvider.AutoSize = true;
      this.lblLoginRequestProvider.Location = new System.Drawing.Point(7, 448);
      this.lblLoginRequestProvider.Name = "lblLoginRequestProvider";
      this.lblLoginRequestProvider.Size = new System.Drawing.Size(46, 13);
      this.lblLoginRequestProvider.TabIndex = 6;
      this.lblLoginRequestProvider.Text = "Provider";
      // 
      // btnBuildLoginRequest
      // 
      this.btnBuildLoginRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildLoginRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildLoginRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildLoginRequest.Location = new System.Drawing.Point(314, 523);
      this.btnBuildLoginRequest.Name = "btnBuildLoginRequest";
      this.btnBuildLoginRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildLoginRequest.TabIndex = 5;
      this.btnBuildLoginRequest.Text = "Build";
      this.btnBuildLoginRequest.UseVisualStyleBackColor = true;
      this.btnBuildLoginRequest.Click += new System.EventHandler(this.btnBuild_LoginRequest_Click);
      // 
      // btnSendLoginRequest
      // 
      this.btnSendLoginRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendLoginRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendLoginRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendLoginRequest.Location = new System.Drawing.Point(400, 523);
      this.btnSendLoginRequest.Name = "btnSendLoginRequest";
      this.btnSendLoginRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendLoginRequest.TabIndex = 6;
      this.btnSendLoginRequest.Text = "Send";
      this.btnSendLoginRequest.UseVisualStyleBackColor = true;
      this.btnSendLoginRequest.Click += new System.EventHandler(this.btnSendLoginRequest_Click);
      // 
      // txtLoginRequestPassword
      // 
      this.txtLoginRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLoginRequestPassword.Location = new System.Drawing.Point(69, 497);
      this.txtLoginRequestPassword.Name = "txtLoginRequestPassword";
      this.txtLoginRequestPassword.Size = new System.Drawing.Size(411, 20);
      this.txtLoginRequestPassword.TabIndex = 4;
      // 
      // lblLoginRequestPassword
      // 
      this.lblLoginRequestPassword.AutoSize = true;
      this.lblLoginRequestPassword.Location = new System.Drawing.Point(7, 500);
      this.lblLoginRequestPassword.Name = "lblLoginRequestPassword";
      this.lblLoginRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblLoginRequestPassword.TabIndex = 4;
      this.lblLoginRequestPassword.Text = "Password";
      // 
      // txtLoginRequestPartnerId
      // 
      this.txtLoginRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLoginRequestPartnerId.Location = new System.Drawing.Point(69, 471);
      this.txtLoginRequestPartnerId.Name = "txtLoginRequestPartnerId";
      this.txtLoginRequestPartnerId.Size = new System.Drawing.Size(411, 20);
      this.txtLoginRequestPartnerId.TabIndex = 3;
      // 
      // lblLoginRequestPartnerId
      // 
      this.lblLoginRequestPartnerId.AutoSize = true;
      this.lblLoginRequestPartnerId.Location = new System.Drawing.Point(7, 474);
      this.lblLoginRequestPartnerId.Name = "lblLoginRequestPartnerId";
      this.lblLoginRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblLoginRequestPartnerId.TabIndex = 2;
      this.lblLoginRequestPartnerId.Text = "Partner Id";
      // 
      // txtLoginRequestSessionId
      // 
      this.txtLoginRequestSessionId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLoginRequestSessionId.Location = new System.Drawing.Point(69, 419);
      this.txtLoginRequestSessionId.Name = "txtLoginRequestSessionId";
      this.txtLoginRequestSessionId.Size = new System.Drawing.Size(411, 20);
      this.txtLoginRequestSessionId.TabIndex = 1;
      // 
      // lblLoginRequestSessionId
      // 
      this.lblLoginRequestSessionId.AutoSize = true;
      this.lblLoginRequestSessionId.Location = new System.Drawing.Point(7, 422);
      this.lblLoginRequestSessionId.Name = "lblLoginRequestSessionId";
      this.lblLoginRequestSessionId.Size = new System.Drawing.Size(56, 13);
      this.lblLoginRequestSessionId.TabIndex = 0;
      this.lblLoginRequestSessionId.Text = "Session Id";
      // 
      // splitContainer9
      // 
      this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer9.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer9.Location = new System.Drawing.Point(0, 0);
      this.splitContainer9.Name = "splitContainer9";
      this.splitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer9.Panel1
      // 
      this.splitContainer9.Panel1.Controls.Add(this.groupBox21);
      // 
      // splitContainer9.Panel2
      // 
      this.splitContainer9.Panel2.Controls.Add(this.groupBox1);
      this.splitContainer9.Size = new System.Drawing.Size(774, 567);
      this.splitContainer9.SplitterDistance = 243;
      this.splitContainer9.TabIndex = 0;
      // 
      // groupBox21
      // 
      this.groupBox21.Controls.Add(this.requestDataLoginRequest);
      this.groupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox21.Location = new System.Drawing.Point(0, 0);
      this.groupBox21.Name = "groupBox21";
      this.groupBox21.Size = new System.Drawing.Size(774, 243);
      this.groupBox21.TabIndex = 3;
      this.groupBox21.TabStop = false;
      this.groupBox21.Text = "Request XML data";
      // 
      // requestDataLoginRequest
      // 
      this.requestDataLoginRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataLoginRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataLoginRequest.Name = "requestDataLoginRequest";
      this.requestDataLoginRequest.Size = new System.Drawing.Size(768, 224);
      this.requestDataLoginRequest.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.responseDataLoginRequest);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox1.Location = new System.Drawing.Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(774, 320);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Response data";
      // 
      // responseDataLoginRequest
      // 
      this.responseDataLoginRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataLoginRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataLoginRequest.Name = "responseDataLoginRequest";
      this.responseDataLoginRequest.Size = new System.Drawing.Size(768, 301);
      this.responseDataLoginRequest.TabIndex = 0;
      // 
      // tabPageDebitRequest
      // 
      this.tabPageDebitRequest.Controls.Add(this.splitContainer4);
      this.tabPageDebitRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageDebitRequest.Name = "tabPageDebitRequest";
      this.tabPageDebitRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageDebitRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageDebitRequest.TabIndex = 3;
      this.tabPageDebitRequest.Text = "Debit Request";
      this.tabPageDebitRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer4
      // 
      this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer4.Location = new System.Drawing.Point(3, 3);
      this.splitContainer4.Name = "splitContainer4";
      // 
      // splitContainer4.Panel1
      // 
      this.splitContainer4.Panel1.Controls.Add(this.groupBox7);
      // 
      // splitContainer4.Panel2
      // 
      this.splitContainer4.Panel2.Controls.Add(this.splitContainer12);
      this.splitContainer4.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer4.SplitterDistance = 314;
      this.splitContainer4.TabIndex = 3;
      // 
      // groupBox7
      // 
      this.groupBox7.Controls.Add(this.txtDebitRequestGameId);
      this.groupBox7.Controls.Add(this.lblDebitRequestGameId);
      this.groupBox7.Controls.Add(this.txtDebitRequestTransaction);
      this.groupBox7.Controls.Add(this.lblDebitRequestTransaction);
      this.groupBox7.Controls.Add(this.btnBuildDebitRequest);
      this.groupBox7.Controls.Add(this.txtDebitRequestPassword);
      this.groupBox7.Controls.Add(this.lblDebitRequestPassword);
      this.groupBox7.Controls.Add(this.txtDebitRequestPartnerId);
      this.groupBox7.Controls.Add(this.lblDebitRequestPartnerId);
      this.groupBox7.Controls.Add(this.gbDebitRequestBets);
      this.groupBox7.Controls.Add(this.txtDebitRequestCurrency);
      this.groupBox7.Controls.Add(this.lblDebitRequestCurrency);
      this.groupBox7.Controls.Add(this.txtDebitRequestAmount);
      this.groupBox7.Controls.Add(this.lblDebitRequestAmount);
      this.groupBox7.Controls.Add(this.btnSendDebitRequest);
      this.groupBox7.Controls.Add(this.txtDebitRequestNumberOfBets);
      this.groupBox7.Controls.Add(this.lblDebitRequestNumberOfBets);
      this.groupBox7.Controls.Add(this.txtDebitRequestGameInstanceId);
      this.groupBox7.Controls.Add(this.lblDebitRequestGameInstanceId);
      this.groupBox7.Controls.Add(this.txtDebitRequestSessionId);
      this.groupBox7.Controls.Add(this.lblDebitRequestSessionId);
      this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox7.Location = new System.Drawing.Point(0, 0);
      this.groupBox7.Name = "groupBox7";
      this.groupBox7.Size = new System.Drawing.Size(314, 567);
      this.groupBox7.TabIndex = 1;
      this.groupBox7.TabStop = false;
      this.groupBox7.Text = "Request data";
      // 
      // txtDebitRequestGameId
      // 
      this.txtDebitRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestGameId.Location = new System.Drawing.Point(100, 95);
      this.txtDebitRequestGameId.Name = "txtDebitRequestGameId";
      this.txtDebitRequestGameId.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestGameId.TabIndex = 3;
      // 
      // lblDebitRequestGameId
      // 
      this.lblDebitRequestGameId.AutoSize = true;
      this.lblDebitRequestGameId.Location = new System.Drawing.Point(7, 98);
      this.lblDebitRequestGameId.Name = "lblDebitRequestGameId";
      this.lblDebitRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblDebitRequestGameId.TabIndex = 18;
      this.lblDebitRequestGameId.Text = "Game Id";
      // 
      // txtDebitRequestTransaction
      // 
      this.txtDebitRequestTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestTransaction.Location = new System.Drawing.Point(100, 225);
      this.txtDebitRequestTransaction.Name = "txtDebitRequestTransaction";
      this.txtDebitRequestTransaction.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestTransaction.TabIndex = 8;
      // 
      // lblDebitRequestTransaction
      // 
      this.lblDebitRequestTransaction.AutoSize = true;
      this.lblDebitRequestTransaction.Location = new System.Drawing.Point(7, 228);
      this.lblDebitRequestTransaction.Name = "lblDebitRequestTransaction";
      this.lblDebitRequestTransaction.Size = new System.Drawing.Size(63, 13);
      this.lblDebitRequestTransaction.TabIndex = 16;
      this.lblDebitRequestTransaction.Text = "Transaction";
      // 
      // btnBuildDebitRequest
      // 
      this.btnBuildDebitRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildDebitRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildDebitRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildDebitRequest.Location = new System.Drawing.Point(140, 520);
      this.btnBuildDebitRequest.Name = "btnBuildDebitRequest";
      this.btnBuildDebitRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildDebitRequest.TabIndex = 10;
      this.btnBuildDebitRequest.Text = "Build";
      this.btnBuildDebitRequest.UseVisualStyleBackColor = true;
      this.btnBuildDebitRequest.Click += new System.EventHandler(this.btnBuildDebitRequest_Click);
      // 
      // txtDebitRequestPassword
      // 
      this.txtDebitRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestPassword.Location = new System.Drawing.Point(100, 69);
      this.txtDebitRequestPassword.Name = "txtDebitRequestPassword";
      this.txtDebitRequestPassword.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestPassword.TabIndex = 2;
      // 
      // lblDebitRequestPassword
      // 
      this.lblDebitRequestPassword.AutoSize = true;
      this.lblDebitRequestPassword.Location = new System.Drawing.Point(7, 72);
      this.lblDebitRequestPassword.Name = "lblDebitRequestPassword";
      this.lblDebitRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblDebitRequestPassword.TabIndex = 14;
      this.lblDebitRequestPassword.Text = "Password";
      // 
      // txtDebitRequestPartnerId
      // 
      this.txtDebitRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestPartnerId.Location = new System.Drawing.Point(100, 43);
      this.txtDebitRequestPartnerId.Name = "txtDebitRequestPartnerId";
      this.txtDebitRequestPartnerId.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestPartnerId.TabIndex = 1;
      // 
      // lblDebitRequestPartnerId
      // 
      this.lblDebitRequestPartnerId.AutoSize = true;
      this.lblDebitRequestPartnerId.Location = new System.Drawing.Point(7, 46);
      this.lblDebitRequestPartnerId.Name = "lblDebitRequestPartnerId";
      this.lblDebitRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblDebitRequestPartnerId.TabIndex = 12;
      this.lblDebitRequestPartnerId.Text = "Partner Id";
      // 
      // gbDebitRequestBets
      // 
      this.gbDebitRequestBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gbDebitRequestBets.Controls.Add(this.btnDebitRequestAddBet);
      this.gbDebitRequestBets.Controls.Add(this.txtDebitRequestBetRake);
      this.gbDebitRequestBets.Controls.Add(this.lblDebitRequestBetRake);
      this.gbDebitRequestBets.Controls.Add(this.txtDebitRequestBetValue);
      this.gbDebitRequestBets.Controls.Add(this.lblDebitRequestBetValue);
      this.gbDebitRequestBets.Controls.Add(this.txtDebitRequestBetId);
      this.gbDebitRequestBets.Controls.Add(this.lblDebitRequestBetId);
      this.gbDebitRequestBets.Controls.Add(this.dgvDebitRequestBets);
      this.gbDebitRequestBets.Location = new System.Drawing.Point(6, 251);
      this.gbDebitRequestBets.Name = "gbDebitRequestBets";
      this.gbDebitRequestBets.Size = new System.Drawing.Size(300, 263);
      this.gbDebitRequestBets.TabIndex = 9;
      this.gbDebitRequestBets.TabStop = false;
      this.gbDebitRequestBets.Text = "Bets";
      // 
      // btnDebitRequestAddBet
      // 
      this.btnDebitRequestAddBet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnDebitRequestAddBet.Image = global::GameGatewaySimulator.Properties.Resources.add;
      this.btnDebitRequestAddBet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnDebitRequestAddBet.Location = new System.Drawing.Point(204, 39);
      this.btnDebitRequestAddBet.Name = "btnDebitRequestAddBet";
      this.btnDebitRequestAddBet.Size = new System.Drawing.Size(90, 24);
      this.btnDebitRequestAddBet.TabIndex = 3;
      this.btnDebitRequestAddBet.Text = "Add bet";
      this.btnDebitRequestAddBet.UseVisualStyleBackColor = true;
      this.btnDebitRequestAddBet.Click += new System.EventHandler(this.btnDebitRequestAddBet_Click);
      // 
      // txtDebitRequestBetRake
      // 
      this.txtDebitRequestBetRake.DecimalPlaces = 2;
      this.txtDebitRequestBetRake.DecimalSeparator = ".";
      this.txtDebitRequestBetRake.IsDecimal = true;
      this.txtDebitRequestBetRake.Location = new System.Drawing.Point(66, 40);
      this.txtDebitRequestBetRake.Name = "txtDebitRequestBetRake";
      this.txtDebitRequestBetRake.Size = new System.Drawing.Size(90, 20);
      this.txtDebitRequestBetRake.TabIndex = 2;
      // 
      // lblDebitRequestBetRake
      // 
      this.lblDebitRequestBetRake.AutoSize = true;
      this.lblDebitRequestBetRake.Location = new System.Drawing.Point(8, 43);
      this.lblDebitRequestBetRake.Name = "lblDebitRequestBetRake";
      this.lblDebitRequestBetRake.Size = new System.Drawing.Size(52, 13);
      this.lblDebitRequestBetRake.TabIndex = 16;
      this.lblDebitRequestBetRake.Text = "Bet Rake";
      // 
      // txtDebitRequestBetValue
      // 
      this.txtDebitRequestBetValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestBetValue.DecimalPlaces = 2;
      this.txtDebitRequestBetValue.DecimalSeparator = ".";
      this.txtDebitRequestBetValue.IsDecimal = true;
      this.txtDebitRequestBetValue.Location = new System.Drawing.Point(204, 13);
      this.txtDebitRequestBetValue.Name = "txtDebitRequestBetValue";
      this.txtDebitRequestBetValue.Size = new System.Drawing.Size(90, 20);
      this.txtDebitRequestBetValue.TabIndex = 1;
      // 
      // lblDebitRequestBetValue
      // 
      this.lblDebitRequestBetValue.AutoSize = true;
      this.lblDebitRequestBetValue.Location = new System.Drawing.Point(145, 16);
      this.lblDebitRequestBetValue.Name = "lblDebitRequestBetValue";
      this.lblDebitRequestBetValue.Size = new System.Drawing.Size(53, 13);
      this.lblDebitRequestBetValue.TabIndex = 14;
      this.lblDebitRequestBetValue.Text = "Bet Value";
      // 
      // txtDebitRequestBetId
      // 
      this.txtDebitRequestBetId.Location = new System.Drawing.Point(49, 13);
      this.txtDebitRequestBetId.Name = "txtDebitRequestBetId";
      this.txtDebitRequestBetId.Size = new System.Drawing.Size(90, 20);
      this.txtDebitRequestBetId.TabIndex = 0;
      // 
      // lblDebitRequestBetId
      // 
      this.lblDebitRequestBetId.AutoSize = true;
      this.lblDebitRequestBetId.Location = new System.Drawing.Point(8, 16);
      this.lblDebitRequestBetId.Name = "lblDebitRequestBetId";
      this.lblDebitRequestBetId.Size = new System.Drawing.Size(35, 13);
      this.lblDebitRequestBetId.TabIndex = 12;
      this.lblDebitRequestBetId.Text = "Bet Id";
      // 
      // dgvDebitRequestBets
      // 
      this.dgvDebitRequestBets.AllowUserToAddRows = false;
      this.dgvDebitRequestBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvDebitRequestBets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvDebitRequestBets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnDebitBetId,
            this.dgvColumnDebitBetValue,
            this.dgvColumnDebitBetRake,
            this.dgvColumnDebitBetDelete});
      this.dgvDebitRequestBets.Location = new System.Drawing.Point(6, 66);
      this.dgvDebitRequestBets.MultiSelect = false;
      this.dgvDebitRequestBets.Name = "dgvDebitRequestBets";
      this.dgvDebitRequestBets.ReadOnly = true;
      this.dgvDebitRequestBets.RowHeadersVisible = false;
      this.dgvDebitRequestBets.Size = new System.Drawing.Size(288, 191);
      this.dgvDebitRequestBets.TabIndex = 4;
      this.dgvDebitRequestBets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDebitRequestBets_CellClick);
      // 
      // dgvColumnDebitBetId
      // 
      this.dgvColumnDebitBetId.DataPropertyName = "BetId";
      this.dgvColumnDebitBetId.HeaderText = "Bet Id";
      this.dgvColumnDebitBetId.Name = "dgvColumnDebitBetId";
      this.dgvColumnDebitBetId.ReadOnly = true;
      this.dgvColumnDebitBetId.Width = 75;
      // 
      // dgvColumnDebitBetValue
      // 
      this.dgvColumnDebitBetValue.DataPropertyName = "BetValue";
      this.dgvColumnDebitBetValue.HeaderText = "Bet Value";
      this.dgvColumnDebitBetValue.Name = "dgvColumnDebitBetValue";
      this.dgvColumnDebitBetValue.ReadOnly = true;
      this.dgvColumnDebitBetValue.Width = 74;
      // 
      // dgvColumnDebitBetRake
      // 
      this.dgvColumnDebitBetRake.DataPropertyName = "BetRake";
      this.dgvColumnDebitBetRake.HeaderText = "Bet Rake";
      this.dgvColumnDebitBetRake.Name = "dgvColumnDebitBetRake";
      this.dgvColumnDebitBetRake.ReadOnly = true;
      this.dgvColumnDebitBetRake.Width = 70;
      // 
      // dgvColumnDebitBetDelete
      // 
      this.dgvColumnDebitBetDelete.HeaderText = "Delete";
      this.dgvColumnDebitBetDelete.Name = "dgvColumnDebitBetDelete";
      this.dgvColumnDebitBetDelete.ReadOnly = true;
      this.dgvColumnDebitBetDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnDebitBetDelete.Text = "Delete";
      this.dgvColumnDebitBetDelete.UseColumnTextForButtonValue = true;
      this.dgvColumnDebitBetDelete.Width = 65;
      // 
      // txtDebitRequestCurrency
      // 
      this.txtDebitRequestCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestCurrency.Location = new System.Drawing.Point(100, 199);
      this.txtDebitRequestCurrency.Name = "txtDebitRequestCurrency";
      this.txtDebitRequestCurrency.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestCurrency.TabIndex = 7;
      // 
      // lblDebitRequestCurrency
      // 
      this.lblDebitRequestCurrency.AutoSize = true;
      this.lblDebitRequestCurrency.Location = new System.Drawing.Point(7, 202);
      this.lblDebitRequestCurrency.Name = "lblDebitRequestCurrency";
      this.lblDebitRequestCurrency.Size = new System.Drawing.Size(49, 13);
      this.lblDebitRequestCurrency.TabIndex = 9;
      this.lblDebitRequestCurrency.Text = "Currency";
      // 
      // txtDebitRequestAmount
      // 
      this.txtDebitRequestAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestAmount.DecimalPlaces = 2;
      this.txtDebitRequestAmount.DecimalSeparator = ".";
      this.txtDebitRequestAmount.IsDecimal = true;
      this.txtDebitRequestAmount.Location = new System.Drawing.Point(100, 173);
      this.txtDebitRequestAmount.Name = "txtDebitRequestAmount";
      this.txtDebitRequestAmount.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestAmount.TabIndex = 6;
      // 
      // lblDebitRequestAmount
      // 
      this.lblDebitRequestAmount.AutoSize = true;
      this.lblDebitRequestAmount.Location = new System.Drawing.Point(7, 176);
      this.lblDebitRequestAmount.Name = "lblDebitRequestAmount";
      this.lblDebitRequestAmount.Size = new System.Drawing.Size(43, 13);
      this.lblDebitRequestAmount.TabIndex = 7;
      this.lblDebitRequestAmount.Text = "Amount";
      // 
      // btnSendDebitRequest
      // 
      this.btnSendDebitRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendDebitRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendDebitRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendDebitRequest.Location = new System.Drawing.Point(226, 520);
      this.btnSendDebitRequest.Name = "btnSendDebitRequest";
      this.btnSendDebitRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendDebitRequest.TabIndex = 11;
      this.btnSendDebitRequest.Text = "Send";
      this.btnSendDebitRequest.UseVisualStyleBackColor = true;
      this.btnSendDebitRequest.Click += new System.EventHandler(this.btnSendDebitRequest_Click);
      // 
      // txtDebitRequestNumberOfBets
      // 
      this.txtDebitRequestNumberOfBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestNumberOfBets.Location = new System.Drawing.Point(100, 147);
      this.txtDebitRequestNumberOfBets.Name = "txtDebitRequestNumberOfBets";
      this.txtDebitRequestNumberOfBets.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestNumberOfBets.TabIndex = 5;
      // 
      // lblDebitRequestNumberOfBets
      // 
      this.lblDebitRequestNumberOfBets.AutoSize = true;
      this.lblDebitRequestNumberOfBets.Location = new System.Drawing.Point(7, 150);
      this.lblDebitRequestNumberOfBets.Name = "lblDebitRequestNumberOfBets";
      this.lblDebitRequestNumberOfBets.Size = new System.Drawing.Size(79, 13);
      this.lblDebitRequestNumberOfBets.TabIndex = 4;
      this.lblDebitRequestNumberOfBets.Text = "Number of bets";
      // 
      // txtDebitRequestGameInstanceId
      // 
      this.txtDebitRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestGameInstanceId.Location = new System.Drawing.Point(100, 121);
      this.txtDebitRequestGameInstanceId.Name = "txtDebitRequestGameInstanceId";
      this.txtDebitRequestGameInstanceId.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestGameInstanceId.TabIndex = 4;
      // 
      // lblDebitRequestGameInstanceId
      // 
      this.lblDebitRequestGameInstanceId.AutoSize = true;
      this.lblDebitRequestGameInstanceId.Location = new System.Drawing.Point(7, 124);
      this.lblDebitRequestGameInstanceId.Name = "lblDebitRequestGameInstanceId";
      this.lblDebitRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblDebitRequestGameInstanceId.TabIndex = 2;
      this.lblDebitRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // txtDebitRequestSessionId
      // 
      this.txtDebitRequestSessionId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtDebitRequestSessionId.Location = new System.Drawing.Point(100, 17);
      this.txtDebitRequestSessionId.Name = "txtDebitRequestSessionId";
      this.txtDebitRequestSessionId.Size = new System.Drawing.Size(206, 20);
      this.txtDebitRequestSessionId.TabIndex = 0;
      // 
      // lblDebitRequestSessionId
      // 
      this.lblDebitRequestSessionId.AutoSize = true;
      this.lblDebitRequestSessionId.Location = new System.Drawing.Point(7, 20);
      this.lblDebitRequestSessionId.Name = "lblDebitRequestSessionId";
      this.lblDebitRequestSessionId.Size = new System.Drawing.Size(56, 13);
      this.lblDebitRequestSessionId.TabIndex = 0;
      this.lblDebitRequestSessionId.Text = "Session Id";
      // 
      // splitContainer12
      // 
      this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer12.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer12.Location = new System.Drawing.Point(0, 0);
      this.splitContainer12.Name = "splitContainer12";
      this.splitContainer12.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer12.Panel1
      // 
      this.splitContainer12.Panel1.Controls.Add(this.groupBox8);
      // 
      // splitContainer12.Panel2
      // 
      this.splitContainer12.Panel2.Controls.Add(this.groupBox24);
      this.splitContainer12.Size = new System.Drawing.Size(946, 567);
      this.splitContainer12.SplitterDistance = 253;
      this.splitContainer12.TabIndex = 2;
      // 
      // groupBox8
      // 
      this.groupBox8.Controls.Add(this.requestDataDebitRequest);
      this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox8.Location = new System.Drawing.Point(0, 0);
      this.groupBox8.Name = "groupBox8";
      this.groupBox8.Size = new System.Drawing.Size(946, 253);
      this.groupBox8.TabIndex = 3;
      this.groupBox8.TabStop = false;
      this.groupBox8.Text = "Request XML data";
      // 
      // requestDataDebitRequest
      // 
      this.requestDataDebitRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataDebitRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataDebitRequest.Name = "requestDataDebitRequest";
      this.requestDataDebitRequest.Size = new System.Drawing.Size(940, 234);
      this.requestDataDebitRequest.TabIndex = 0;
      // 
      // groupBox24
      // 
      this.groupBox24.Controls.Add(this.responseDataDebitRequest);
      this.groupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox24.Location = new System.Drawing.Point(0, 0);
      this.groupBox24.Name = "groupBox24";
      this.groupBox24.Size = new System.Drawing.Size(946, 310);
      this.groupBox24.TabIndex = 2;
      this.groupBox24.TabStop = false;
      this.groupBox24.Text = "Response data";
      // 
      // responseDataDebitRequest
      // 
      this.responseDataDebitRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataDebitRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataDebitRequest.Name = "responseDataDebitRequest";
      this.responseDataDebitRequest.Size = new System.Drawing.Size(940, 291);
      this.responseDataDebitRequest.TabIndex = 0;
      // 
      // tabPageBulkCreditRequest
      // 
      this.tabPageBulkCreditRequest.Controls.Add(this.splitContainer6);
      this.tabPageBulkCreditRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageBulkCreditRequest.Name = "tabPageBulkCreditRequest";
      this.tabPageBulkCreditRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageBulkCreditRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageBulkCreditRequest.TabIndex = 5;
      this.tabPageBulkCreditRequest.Text = "Bulk Credit Request";
      this.tabPageBulkCreditRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer6
      // 
      this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer6.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer6.Location = new System.Drawing.Point(3, 3);
      this.splitContainer6.Name = "splitContainer6";
      // 
      // splitContainer6.Panel1
      // 
      this.splitContainer6.Panel1.Controls.Add(this.groupBox12);
      // 
      // splitContainer6.Panel2
      // 
      this.splitContainer6.Panel2.Controls.Add(this.splitContainer14);
      this.splitContainer6.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer6.SplitterDistance = 314;
      this.splitContainer6.TabIndex = 4;
      // 
      // groupBox12
      // 
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestGameId);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestGameId);
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestTransaction);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestTransaction);
      this.groupBox12.Controls.Add(this.btnBuildBulkCreditRequest);
      this.groupBox12.Controls.Add(this.chkBulkCreditRequestRollback);
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestPassword);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestPassword);
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestPartnerId);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestPartnerId);
      this.groupBox12.Controls.Add(this.groupBox13);
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestCurrency);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestCurrency);
      this.groupBox12.Controls.Add(this.btnSendBulkCreditRequest);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestRollback);
      this.groupBox12.Controls.Add(this.txtBulkCreditRequestGameInstanceId);
      this.groupBox12.Controls.Add(this.lblBulkCreditRequestGameInstanceId);
      this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox12.Location = new System.Drawing.Point(0, 0);
      this.groupBox12.Name = "groupBox12";
      this.groupBox12.Size = new System.Drawing.Size(314, 567);
      this.groupBox12.TabIndex = 1;
      this.groupBox12.TabStop = false;
      this.groupBox12.Text = "Request data";
      // 
      // txtBulkCreditRequestGameId
      // 
      this.txtBulkCreditRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestGameId.Location = new System.Drawing.Point(100, 69);
      this.txtBulkCreditRequestGameId.Name = "txtBulkCreditRequestGameId";
      this.txtBulkCreditRequestGameId.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestGameId.TabIndex = 2;
      // 
      // lblBulkCreditRequestGameId
      // 
      this.lblBulkCreditRequestGameId.AutoSize = true;
      this.lblBulkCreditRequestGameId.Location = new System.Drawing.Point(7, 72);
      this.lblBulkCreditRequestGameId.Name = "lblBulkCreditRequestGameId";
      this.lblBulkCreditRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblBulkCreditRequestGameId.TabIndex = 18;
      this.lblBulkCreditRequestGameId.Text = "Game Id";
      // 
      // txtBulkCreditRequestTransaction
      // 
      this.txtBulkCreditRequestTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestTransaction.Location = new System.Drawing.Point(100, 167);
      this.txtBulkCreditRequestTransaction.Name = "txtBulkCreditRequestTransaction";
      this.txtBulkCreditRequestTransaction.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestTransaction.TabIndex = 6;
      // 
      // lblBulkCreditRequestTransaction
      // 
      this.lblBulkCreditRequestTransaction.AutoSize = true;
      this.lblBulkCreditRequestTransaction.Location = new System.Drawing.Point(7, 170);
      this.lblBulkCreditRequestTransaction.Name = "lblBulkCreditRequestTransaction";
      this.lblBulkCreditRequestTransaction.Size = new System.Drawing.Size(63, 13);
      this.lblBulkCreditRequestTransaction.TabIndex = 16;
      this.lblBulkCreditRequestTransaction.Text = "Transaction";
      // 
      // btnBuildBulkCreditRequest
      // 
      this.btnBuildBulkCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildBulkCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildBulkCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildBulkCreditRequest.Location = new System.Drawing.Point(140, 489);
      this.btnBuildBulkCreditRequest.Name = "btnBuildBulkCreditRequest";
      this.btnBuildBulkCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildBulkCreditRequest.TabIndex = 8;
      this.btnBuildBulkCreditRequest.Text = "Build";
      this.btnBuildBulkCreditRequest.UseVisualStyleBackColor = true;
      this.btnBuildBulkCreditRequest.Click += new System.EventHandler(this.btnBuildBulkCreditRequest_Click);
      // 
      // chkBulkCreditRequestRollback
      // 
      this.chkBulkCreditRequestRollback.AutoSize = true;
      this.chkBulkCreditRequestRollback.Location = new System.Drawing.Point(100, 121);
      this.chkBulkCreditRequestRollback.Name = "chkBulkCreditRequestRollback";
      this.chkBulkCreditRequestRollback.Size = new System.Drawing.Size(15, 14);
      this.chkBulkCreditRequestRollback.TabIndex = 4;
      this.chkBulkCreditRequestRollback.UseVisualStyleBackColor = true;
      // 
      // txtBulkCreditRequestPassword
      // 
      this.txtBulkCreditRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestPassword.Location = new System.Drawing.Point(100, 43);
      this.txtBulkCreditRequestPassword.Name = "txtBulkCreditRequestPassword";
      this.txtBulkCreditRequestPassword.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestPassword.TabIndex = 1;
      // 
      // lblBulkCreditRequestPassword
      // 
      this.lblBulkCreditRequestPassword.AutoSize = true;
      this.lblBulkCreditRequestPassword.Location = new System.Drawing.Point(7, 46);
      this.lblBulkCreditRequestPassword.Name = "lblBulkCreditRequestPassword";
      this.lblBulkCreditRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblBulkCreditRequestPassword.TabIndex = 14;
      this.lblBulkCreditRequestPassword.Text = "Password";
      // 
      // txtBulkCreditRequestPartnerId
      // 
      this.txtBulkCreditRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestPartnerId.Location = new System.Drawing.Point(100, 17);
      this.txtBulkCreditRequestPartnerId.Name = "txtBulkCreditRequestPartnerId";
      this.txtBulkCreditRequestPartnerId.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestPartnerId.TabIndex = 0;
      // 
      // lblBulkCreditRequestPartnerId
      // 
      this.lblBulkCreditRequestPartnerId.AutoSize = true;
      this.lblBulkCreditRequestPartnerId.Location = new System.Drawing.Point(7, 20);
      this.lblBulkCreditRequestPartnerId.Name = "lblBulkCreditRequestPartnerId";
      this.lblBulkCreditRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblBulkCreditRequestPartnerId.TabIndex = 12;
      this.lblBulkCreditRequestPartnerId.Text = "Partner Id";
      // 
      // groupBox13
      // 
      this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox13.Controls.Add(this.txtBulkCreditRequestCreditJackpot);
      this.groupBox13.Controls.Add(this.lblBulkCreditRequestBetJackpot);
      this.groupBox13.Controls.Add(this.btnSearchPlaySessionsBulkCredit);
      this.groupBox13.Controls.Add(this.btnBulkCreditRequestAddCredit);
      this.groupBox13.Controls.Add(this.txtBulkCreditRequestCreditUserId);
      this.groupBox13.Controls.Add(this.lblBulkCreditRequestBetUserId);
      this.groupBox13.Controls.Add(this.txtBulkCreditRequestCreditAmount);
      this.groupBox13.Controls.Add(this.lblBulkCreditRequestCreditAmount);
      this.groupBox13.Controls.Add(this.txtBulkCreditRequestCreditBetId);
      this.groupBox13.Controls.Add(this.lblBulkCreditRequestBetId);
      this.groupBox13.Controls.Add(this.dgvBulkCreditRequestCredits);
      this.groupBox13.Location = new System.Drawing.Point(6, 193);
      this.groupBox13.Name = "groupBox13";
      this.groupBox13.Size = new System.Drawing.Size(300, 290);
      this.groupBox13.TabIndex = 7;
      this.groupBox13.TabStop = false;
      this.groupBox13.Text = "Credits";
      // 
      // txtBulkCreditRequestCreditJackpot
      // 
      this.txtBulkCreditRequestCreditJackpot.DecimalPlaces = 2;
      this.txtBulkCreditRequestCreditJackpot.DecimalSeparator = ".";
      this.txtBulkCreditRequestCreditJackpot.IsDecimal = true;
      this.txtBulkCreditRequestCreditJackpot.Location = new System.Drawing.Point(54, 66);
      this.txtBulkCreditRequestCreditJackpot.Name = "txtBulkCreditRequestCreditJackpot";
      this.txtBulkCreditRequestCreditJackpot.Size = new System.Drawing.Size(85, 20);
      this.txtBulkCreditRequestCreditJackpot.TabIndex = 5;
      // 
      // lblBulkCreditRequestBetJackpot
      // 
      this.lblBulkCreditRequestBetJackpot.AutoSize = true;
      this.lblBulkCreditRequestBetJackpot.Location = new System.Drawing.Point(8, 69);
      this.lblBulkCreditRequestBetJackpot.Name = "lblBulkCreditRequestBetJackpot";
      this.lblBulkCreditRequestBetJackpot.Size = new System.Drawing.Size(45, 13);
      this.lblBulkCreditRequestBetJackpot.TabIndex = 18;
      this.lblBulkCreditRequestBetJackpot.Text = "Jackpot";
      // 
      // btnSearchPlaySessionsBulkCredit
      // 
      this.btnSearchPlaySessionsBulkCredit.Image = global::GameGatewaySimulator.Properties.Resources.search;
      this.btnSearchPlaySessionsBulkCredit.Location = new System.Drawing.Point(143, 38);
      this.btnSearchPlaySessionsBulkCredit.Name = "btnSearchPlaySessionsBulkCredit";
      this.btnSearchPlaySessionsBulkCredit.Size = new System.Drawing.Size(29, 25);
      this.btnSearchPlaySessionsBulkCredit.TabIndex = 4;
      this.btnSearchPlaySessionsBulkCredit.UseVisualStyleBackColor = true;
      this.btnSearchPlaySessionsBulkCredit.Click += new System.EventHandler(this.btnSearchPlaySessionsBulkCredit_Click);
      // 
      // btnBulkCreditRequestAddCredit
      // 
      this.btnBulkCreditRequestAddCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBulkCreditRequestAddCredit.Image = global::GameGatewaySimulator.Properties.Resources.add;
      this.btnBulkCreditRequestAddCredit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBulkCreditRequestAddCredit.Location = new System.Drawing.Point(192, 65);
      this.btnBulkCreditRequestAddCredit.Name = "btnBulkCreditRequestAddCredit";
      this.btnBulkCreditRequestAddCredit.Size = new System.Drawing.Size(102, 25);
      this.btnBulkCreditRequestAddCredit.TabIndex = 6;
      this.btnBulkCreditRequestAddCredit.Text = "Add credit";
      this.btnBulkCreditRequestAddCredit.UseVisualStyleBackColor = true;
      this.btnBulkCreditRequestAddCredit.Click += new System.EventHandler(this.btnBulkCreditRequestAddCredit_Click);
      // 
      // txtBulkCreditRequestCreditUserId
      // 
      this.txtBulkCreditRequestCreditUserId.Location = new System.Drawing.Point(54, 40);
      this.txtBulkCreditRequestCreditUserId.Name = "txtBulkCreditRequestCreditUserId";
      this.txtBulkCreditRequestCreditUserId.Size = new System.Drawing.Size(85, 20);
      this.txtBulkCreditRequestCreditUserId.TabIndex = 3;
      // 
      // lblBulkCreditRequestBetUserId
      // 
      this.lblBulkCreditRequestBetUserId.AutoSize = true;
      this.lblBulkCreditRequestBetUserId.Location = new System.Drawing.Point(8, 43);
      this.lblBulkCreditRequestBetUserId.Name = "lblBulkCreditRequestBetUserId";
      this.lblBulkCreditRequestBetUserId.Size = new System.Drawing.Size(41, 13);
      this.lblBulkCreditRequestBetUserId.TabIndex = 16;
      this.lblBulkCreditRequestBetUserId.Text = "User Id";
      // 
      // txtBulkCreditRequestCreditAmount
      // 
      this.txtBulkCreditRequestCreditAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestCreditAmount.DecimalPlaces = 2;
      this.txtBulkCreditRequestCreditAmount.DecimalSeparator = ".";
      this.txtBulkCreditRequestCreditAmount.IsDecimal = true;
      this.txtBulkCreditRequestCreditAmount.Location = new System.Drawing.Point(192, 13);
      this.txtBulkCreditRequestCreditAmount.Name = "txtBulkCreditRequestCreditAmount";
      this.txtBulkCreditRequestCreditAmount.Size = new System.Drawing.Size(102, 20);
      this.txtBulkCreditRequestCreditAmount.TabIndex = 2;
      // 
      // lblBulkCreditRequestCreditAmount
      // 
      this.lblBulkCreditRequestCreditAmount.AutoSize = true;
      this.lblBulkCreditRequestCreditAmount.Location = new System.Drawing.Point(145, 16);
      this.lblBulkCreditRequestCreditAmount.Name = "lblBulkCreditRequestCreditAmount";
      this.lblBulkCreditRequestCreditAmount.Size = new System.Drawing.Size(43, 13);
      this.lblBulkCreditRequestCreditAmount.TabIndex = 14;
      this.lblBulkCreditRequestCreditAmount.Text = "Amount";
      // 
      // txtBulkCreditRequestCreditBetId
      // 
      this.txtBulkCreditRequestCreditBetId.Location = new System.Drawing.Point(54, 13);
      this.txtBulkCreditRequestCreditBetId.Name = "txtBulkCreditRequestCreditBetId";
      this.txtBulkCreditRequestCreditBetId.Size = new System.Drawing.Size(85, 20);
      this.txtBulkCreditRequestCreditBetId.TabIndex = 0;
      // 
      // lblBulkCreditRequestBetId
      // 
      this.lblBulkCreditRequestBetId.AutoSize = true;
      this.lblBulkCreditRequestBetId.Location = new System.Drawing.Point(8, 16);
      this.lblBulkCreditRequestBetId.Name = "lblBulkCreditRequestBetId";
      this.lblBulkCreditRequestBetId.Size = new System.Drawing.Size(35, 13);
      this.lblBulkCreditRequestBetId.TabIndex = 12;
      this.lblBulkCreditRequestBetId.Text = "Bet Id";
      // 
      // dgvBulkCreditRequestCredits
      // 
      this.dgvBulkCreditRequestCredits.AllowUserToAddRows = false;
      this.dgvBulkCreditRequestCredits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvBulkCreditRequestCredits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvBulkCreditRequestCredits.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnBulkCreditBetId,
            this.dgvColumnBulkCreditAmount,
            this.dgvColumnBulkCreditUserId,
            this.dgvColumnBulkCreditJackpot,
            this.dgvColumnBulkCreditDelete});
      this.dgvBulkCreditRequestCredits.Location = new System.Drawing.Point(6, 92);
      this.dgvBulkCreditRequestCredits.MultiSelect = false;
      this.dgvBulkCreditRequestCredits.Name = "dgvBulkCreditRequestCredits";
      this.dgvBulkCreditRequestCredits.ReadOnly = true;
      this.dgvBulkCreditRequestCredits.RowHeadersVisible = false;
      this.dgvBulkCreditRequestCredits.Size = new System.Drawing.Size(288, 191);
      this.dgvBulkCreditRequestCredits.TabIndex = 7;
      this.dgvBulkCreditRequestCredits.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBulkCreditRequestCredits_CellClick);
      // 
      // dgvColumnBulkCreditBetId
      // 
      this.dgvColumnBulkCreditBetId.DataPropertyName = "BetId";
      this.dgvColumnBulkCreditBetId.HeaderText = "Bet Id";
      this.dgvColumnBulkCreditBetId.Name = "dgvColumnBulkCreditBetId";
      this.dgvColumnBulkCreditBetId.ReadOnly = true;
      this.dgvColumnBulkCreditBetId.Width = 50;
      // 
      // dgvColumnBulkCreditAmount
      // 
      this.dgvColumnBulkCreditAmount.DataPropertyName = "Amount";
      this.dgvColumnBulkCreditAmount.HeaderText = "Amount";
      this.dgvColumnBulkCreditAmount.Name = "dgvColumnBulkCreditAmount";
      this.dgvColumnBulkCreditAmount.ReadOnly = true;
      this.dgvColumnBulkCreditAmount.Width = 57;
      // 
      // dgvColumnBulkCreditUserId
      // 
      this.dgvColumnBulkCreditUserId.DataPropertyName = "UserId";
      this.dgvColumnBulkCreditUserId.HeaderText = "User Id";
      this.dgvColumnBulkCreditUserId.Name = "dgvColumnBulkCreditUserId";
      this.dgvColumnBulkCreditUserId.ReadOnly = true;
      this.dgvColumnBulkCreditUserId.Width = 60;
      // 
      // dgvColumnBulkCreditJackpot
      // 
      this.dgvColumnBulkCreditJackpot.DataPropertyName = "Jackpot";
      this.dgvColumnBulkCreditJackpot.HeaderText = "Jackpot";
      this.dgvColumnBulkCreditJackpot.Name = "dgvColumnBulkCreditJackpot";
      this.dgvColumnBulkCreditJackpot.ReadOnly = true;
      this.dgvColumnBulkCreditJackpot.Width = 60;
      // 
      // dgvColumnBulkCreditDelete
      // 
      this.dgvColumnBulkCreditDelete.HeaderText = "Delete";
      this.dgvColumnBulkCreditDelete.Name = "dgvColumnBulkCreditDelete";
      this.dgvColumnBulkCreditDelete.ReadOnly = true;
      this.dgvColumnBulkCreditDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnBulkCreditDelete.Text = "Delete";
      this.dgvColumnBulkCreditDelete.ToolTipText = "Delete selected bet";
      this.dgvColumnBulkCreditDelete.UseColumnTextForButtonValue = true;
      this.dgvColumnBulkCreditDelete.Width = 58;
      // 
      // txtBulkCreditRequestCurrency
      // 
      this.txtBulkCreditRequestCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestCurrency.Location = new System.Drawing.Point(100, 141);
      this.txtBulkCreditRequestCurrency.Name = "txtBulkCreditRequestCurrency";
      this.txtBulkCreditRequestCurrency.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestCurrency.TabIndex = 5;
      // 
      // lblBulkCreditRequestCurrency
      // 
      this.lblBulkCreditRequestCurrency.AutoSize = true;
      this.lblBulkCreditRequestCurrency.Location = new System.Drawing.Point(7, 144);
      this.lblBulkCreditRequestCurrency.Name = "lblBulkCreditRequestCurrency";
      this.lblBulkCreditRequestCurrency.Size = new System.Drawing.Size(49, 13);
      this.lblBulkCreditRequestCurrency.TabIndex = 9;
      this.lblBulkCreditRequestCurrency.Text = "Currency";
      // 
      // btnSendBulkCreditRequest
      // 
      this.btnSendBulkCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendBulkCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendBulkCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendBulkCreditRequest.Location = new System.Drawing.Point(226, 489);
      this.btnSendBulkCreditRequest.Name = "btnSendBulkCreditRequest";
      this.btnSendBulkCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendBulkCreditRequest.TabIndex = 9;
      this.btnSendBulkCreditRequest.Text = "Send";
      this.btnSendBulkCreditRequest.UseVisualStyleBackColor = true;
      this.btnSendBulkCreditRequest.Click += new System.EventHandler(this.btnSendBulkCreditRequest_Click);
      // 
      // lblBulkCreditRequestRollback
      // 
      this.lblBulkCreditRequestRollback.AutoSize = true;
      this.lblBulkCreditRequestRollback.Location = new System.Drawing.Point(7, 122);
      this.lblBulkCreditRequestRollback.Name = "lblBulkCreditRequestRollback";
      this.lblBulkCreditRequestRollback.Size = new System.Drawing.Size(49, 13);
      this.lblBulkCreditRequestRollback.TabIndex = 4;
      this.lblBulkCreditRequestRollback.Text = "Rollback";
      // 
      // txtBulkCreditRequestGameInstanceId
      // 
      this.txtBulkCreditRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtBulkCreditRequestGameInstanceId.Location = new System.Drawing.Point(100, 95);
      this.txtBulkCreditRequestGameInstanceId.Name = "txtBulkCreditRequestGameInstanceId";
      this.txtBulkCreditRequestGameInstanceId.Size = new System.Drawing.Size(206, 20);
      this.txtBulkCreditRequestGameInstanceId.TabIndex = 3;
      // 
      // lblBulkCreditRequestGameInstanceId
      // 
      this.lblBulkCreditRequestGameInstanceId.AutoSize = true;
      this.lblBulkCreditRequestGameInstanceId.Location = new System.Drawing.Point(7, 98);
      this.lblBulkCreditRequestGameInstanceId.Name = "lblBulkCreditRequestGameInstanceId";
      this.lblBulkCreditRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblBulkCreditRequestGameInstanceId.TabIndex = 2;
      this.lblBulkCreditRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // splitContainer14
      // 
      this.splitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer14.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer14.Location = new System.Drawing.Point(0, 0);
      this.splitContainer14.Name = "splitContainer14";
      this.splitContainer14.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer14.Panel1
      // 
      this.splitContainer14.Panel1.Controls.Add(this.groupBox14);
      // 
      // splitContainer14.Panel2
      // 
      this.splitContainer14.Panel2.Controls.Add(this.groupBox26);
      this.splitContainer14.Size = new System.Drawing.Size(946, 567);
      this.splitContainer14.SplitterDistance = 263;
      this.splitContainer14.TabIndex = 2;
      // 
      // groupBox14
      // 
      this.groupBox14.Controls.Add(this.requestDataBulkCreditRequest);
      this.groupBox14.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox14.Location = new System.Drawing.Point(0, 0);
      this.groupBox14.Name = "groupBox14";
      this.groupBox14.Size = new System.Drawing.Size(946, 263);
      this.groupBox14.TabIndex = 3;
      this.groupBox14.TabStop = false;
      this.groupBox14.Text = "Request XML data";
      // 
      // requestDataBulkCreditRequest
      // 
      this.requestDataBulkCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataBulkCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataBulkCreditRequest.Name = "requestDataBulkCreditRequest";
      this.requestDataBulkCreditRequest.Size = new System.Drawing.Size(940, 244);
      this.requestDataBulkCreditRequest.TabIndex = 0;
      // 
      // groupBox26
      // 
      this.groupBox26.Controls.Add(this.responseDataBulkCreditRequest);
      this.groupBox26.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox26.Location = new System.Drawing.Point(0, 0);
      this.groupBox26.Name = "groupBox26";
      this.groupBox26.Size = new System.Drawing.Size(946, 300);
      this.groupBox26.TabIndex = 2;
      this.groupBox26.TabStop = false;
      this.groupBox26.Text = "Response data";
      // 
      // responseDataBulkCreditRequest
      // 
      this.responseDataBulkCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataBulkCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataBulkCreditRequest.Name = "responseDataBulkCreditRequest";
      this.responseDataBulkCreditRequest.Size = new System.Drawing.Size(940, 281);
      this.responseDataBulkCreditRequest.TabIndex = 0;
      // 
      // tabPageLogoutRequest
      // 
      this.tabPageLogoutRequest.Controls.Add(this.splitContainer2);
      this.tabPageLogoutRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageLogoutRequest.Name = "tabPageLogoutRequest";
      this.tabPageLogoutRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageLogoutRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageLogoutRequest.TabIndex = 1;
      this.tabPageLogoutRequest.Text = "Logout Request";
      this.tabPageLogoutRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer2
      // 
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer2.Location = new System.Drawing.Point(3, 3);
      this.splitContainer2.Name = "splitContainer2";
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.groupBox3);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.splitContainer10);
      this.splitContainer2.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer2.SplitterDistance = 222;
      this.splitContainer2.TabIndex = 1;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.btnBuildLogoutRequest);
      this.groupBox3.Controls.Add(this.btnSendLogoutRequest);
      this.groupBox3.Controls.Add(this.txtLogoutRequestPassword);
      this.groupBox3.Controls.Add(this.lblLogoutRequestPassword);
      this.groupBox3.Controls.Add(this.txtLogoutRequestPartnerId);
      this.groupBox3.Controls.Add(this.lblLogoutRequestPartnerId);
      this.groupBox3.Controls.Add(this.txtLogoutRequestSessionId);
      this.groupBox3.Controls.Add(this.lblLogoutRequestSessionId);
      this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox3.Location = new System.Drawing.Point(0, 0);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(222, 567);
      this.groupBox3.TabIndex = 1;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Request data";
      // 
      // btnBuildLogoutRequest
      // 
      this.btnBuildLogoutRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildLogoutRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildLogoutRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildLogoutRequest.Location = new System.Drawing.Point(49, 95);
      this.btnBuildLogoutRequest.Name = "btnBuildLogoutRequest";
      this.btnBuildLogoutRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildLogoutRequest.TabIndex = 3;
      this.btnBuildLogoutRequest.Text = "Build";
      this.btnBuildLogoutRequest.UseVisualStyleBackColor = true;
      this.btnBuildLogoutRequest.Click += new System.EventHandler(this.btnBuildLogoutRequest_Click);
      // 
      // btnSendLogoutRequest
      // 
      this.btnSendLogoutRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendLogoutRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendLogoutRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendLogoutRequest.Location = new System.Drawing.Point(135, 95);
      this.btnSendLogoutRequest.Name = "btnSendLogoutRequest";
      this.btnSendLogoutRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendLogoutRequest.TabIndex = 4;
      this.btnSendLogoutRequest.Text = "Send";
      this.btnSendLogoutRequest.UseVisualStyleBackColor = true;
      this.btnSendLogoutRequest.Click += new System.EventHandler(this.btnSendLogoutRequest_Click);
      // 
      // txtLogoutRequestPassword
      // 
      this.txtLogoutRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLogoutRequestPassword.Location = new System.Drawing.Point(69, 69);
      this.txtLogoutRequestPassword.Name = "txtLogoutRequestPassword";
      this.txtLogoutRequestPassword.Size = new System.Drawing.Size(146, 20);
      this.txtLogoutRequestPassword.TabIndex = 2;
      // 
      // lblLogoutRequestPassword
      // 
      this.lblLogoutRequestPassword.AutoSize = true;
      this.lblLogoutRequestPassword.Location = new System.Drawing.Point(7, 72);
      this.lblLogoutRequestPassword.Name = "lblLogoutRequestPassword";
      this.lblLogoutRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblLogoutRequestPassword.TabIndex = 4;
      this.lblLogoutRequestPassword.Text = "Password";
      // 
      // txtLogoutRequestPartnerId
      // 
      this.txtLogoutRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLogoutRequestPartnerId.Location = new System.Drawing.Point(69, 43);
      this.txtLogoutRequestPartnerId.Name = "txtLogoutRequestPartnerId";
      this.txtLogoutRequestPartnerId.Size = new System.Drawing.Size(146, 20);
      this.txtLogoutRequestPartnerId.TabIndex = 1;
      // 
      // lblLogoutRequestPartnerId
      // 
      this.lblLogoutRequestPartnerId.AutoSize = true;
      this.lblLogoutRequestPartnerId.Location = new System.Drawing.Point(7, 46);
      this.lblLogoutRequestPartnerId.Name = "lblLogoutRequestPartnerId";
      this.lblLogoutRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblLogoutRequestPartnerId.TabIndex = 2;
      this.lblLogoutRequestPartnerId.Text = "Partner Id";
      // 
      // txtLogoutRequestSessionId
      // 
      this.txtLogoutRequestSessionId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLogoutRequestSessionId.Location = new System.Drawing.Point(69, 17);
      this.txtLogoutRequestSessionId.Name = "txtLogoutRequestSessionId";
      this.txtLogoutRequestSessionId.Size = new System.Drawing.Size(146, 20);
      this.txtLogoutRequestSessionId.TabIndex = 0;
      // 
      // lblLogoutRequestSessionId
      // 
      this.lblLogoutRequestSessionId.AutoSize = true;
      this.lblLogoutRequestSessionId.Location = new System.Drawing.Point(7, 20);
      this.lblLogoutRequestSessionId.Name = "lblLogoutRequestSessionId";
      this.lblLogoutRequestSessionId.Size = new System.Drawing.Size(56, 13);
      this.lblLogoutRequestSessionId.TabIndex = 0;
      this.lblLogoutRequestSessionId.Text = "Session Id";
      // 
      // splitContainer10
      // 
      this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer10.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer10.Location = new System.Drawing.Point(0, 0);
      this.splitContainer10.Name = "splitContainer10";
      this.splitContainer10.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer10.Panel1
      // 
      this.splitContainer10.Panel1.Controls.Add(this.groupBox4);
      // 
      // splitContainer10.Panel2
      // 
      this.splitContainer10.Panel2.Controls.Add(this.groupBox22);
      this.splitContainer10.Size = new System.Drawing.Size(1038, 567);
      this.splitContainer10.SplitterDistance = 205;
      this.splitContainer10.TabIndex = 1;
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.requestDataLogoutRequest);
      this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox4.Location = new System.Drawing.Point(0, 0);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(1038, 205);
      this.groupBox4.TabIndex = 3;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Request XML data";
      // 
      // requestDataLogoutRequest
      // 
      this.requestDataLogoutRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataLogoutRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataLogoutRequest.Name = "requestDataLogoutRequest";
      this.requestDataLogoutRequest.Size = new System.Drawing.Size(1032, 186);
      this.requestDataLogoutRequest.TabIndex = 0;
      // 
      // groupBox22
      // 
      this.groupBox22.Controls.Add(this.responseDataLogoutRequest);
      this.groupBox22.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox22.Location = new System.Drawing.Point(0, 0);
      this.groupBox22.Name = "groupBox22";
      this.groupBox22.Size = new System.Drawing.Size(1038, 358);
      this.groupBox22.TabIndex = 2;
      this.groupBox22.TabStop = false;
      this.groupBox22.Text = "Response data";
      // 
      // responseDataLogoutRequest
      // 
      this.responseDataLogoutRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataLogoutRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataLogoutRequest.Name = "responseDataLogoutRequest";
      this.responseDataLogoutRequest.Size = new System.Drawing.Size(1032, 339);
      this.responseDataLogoutRequest.TabIndex = 0;
      // 
      // tabPageOfflineCreditRequest
      // 
      this.tabPageOfflineCreditRequest.Controls.Add(this.splitContainer7);
      this.tabPageOfflineCreditRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageOfflineCreditRequest.Name = "tabPageOfflineCreditRequest";
      this.tabPageOfflineCreditRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageOfflineCreditRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageOfflineCreditRequest.TabIndex = 6;
      this.tabPageOfflineCreditRequest.Text = "Offline Credit Request";
      this.tabPageOfflineCreditRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer7
      // 
      this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer7.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer7.Location = new System.Drawing.Point(3, 3);
      this.splitContainer7.Name = "splitContainer7";
      // 
      // splitContainer7.Panel1
      // 
      this.splitContainer7.Panel1.Controls.Add(this.groupBox15);
      // 
      // splitContainer7.Panel2
      // 
      this.splitContainer7.Panel2.Controls.Add(this.splitContainer15);
      this.splitContainer7.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer7.SplitterDistance = 314;
      this.splitContainer7.TabIndex = 5;
      // 
      // groupBox15
      // 
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestGameId);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestGameId);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestTransaction);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestTransaction);
      this.groupBox15.Controls.Add(this.btnBuildOfflineCreditRequest);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestPassword);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestPassword);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestPartnerId);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestPartnerId);
      this.groupBox15.Controls.Add(this.groupBox16);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestCurrency);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestCurrency);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestAmount);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestAmount);
      this.groupBox15.Controls.Add(this.btnSendOfflineCreditRequest);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestGameInstanceId);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestGameInstanceId);
      this.groupBox15.Controls.Add(this.txtOfflineCreditRequestUserId);
      this.groupBox15.Controls.Add(this.lblOfflineCreditRequestUserId);
      this.groupBox15.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox15.Location = new System.Drawing.Point(0, 0);
      this.groupBox15.Name = "groupBox15";
      this.groupBox15.Size = new System.Drawing.Size(314, 567);
      this.groupBox15.TabIndex = 1;
      this.groupBox15.TabStop = false;
      this.groupBox15.Text = "Request data";
      // 
      // txtOfflineCreditRequestGameId
      // 
      this.txtOfflineCreditRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestGameId.Location = new System.Drawing.Point(100, 94);
      this.txtOfflineCreditRequestGameId.Name = "txtOfflineCreditRequestGameId";
      this.txtOfflineCreditRequestGameId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestGameId.TabIndex = 3;
      // 
      // lblOfflineCreditRequestGameId
      // 
      this.lblOfflineCreditRequestGameId.AutoSize = true;
      this.lblOfflineCreditRequestGameId.Location = new System.Drawing.Point(7, 98);
      this.lblOfflineCreditRequestGameId.Name = "lblOfflineCreditRequestGameId";
      this.lblOfflineCreditRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblOfflineCreditRequestGameId.TabIndex = 19;
      this.lblOfflineCreditRequestGameId.Text = "Game Id";
      // 
      // txtOfflineCreditRequestTransaction
      // 
      this.txtOfflineCreditRequestTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestTransaction.Location = new System.Drawing.Point(100, 198);
      this.txtOfflineCreditRequestTransaction.Name = "txtOfflineCreditRequestTransaction";
      this.txtOfflineCreditRequestTransaction.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestTransaction.TabIndex = 7;
      // 
      // lblOfflineCreditRequestTransaction
      // 
      this.lblOfflineCreditRequestTransaction.AutoSize = true;
      this.lblOfflineCreditRequestTransaction.Location = new System.Drawing.Point(7, 202);
      this.lblOfflineCreditRequestTransaction.Name = "lblOfflineCreditRequestTransaction";
      this.lblOfflineCreditRequestTransaction.Size = new System.Drawing.Size(63, 13);
      this.lblOfflineCreditRequestTransaction.TabIndex = 18;
      this.lblOfflineCreditRequestTransaction.Text = "Transaction";
      // 
      // btnBuildOfflineCreditRequest
      // 
      this.btnBuildOfflineCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildOfflineCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildOfflineCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildOfflineCreditRequest.Location = new System.Drawing.Point(140, 493);
      this.btnBuildOfflineCreditRequest.Name = "btnBuildOfflineCreditRequest";
      this.btnBuildOfflineCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildOfflineCreditRequest.TabIndex = 9;
      this.btnBuildOfflineCreditRequest.Text = "Build";
      this.btnBuildOfflineCreditRequest.UseVisualStyleBackColor = true;
      this.btnBuildOfflineCreditRequest.Click += new System.EventHandler(this.btnBuildOfflineCreditRequest_Click);
      // 
      // txtOfflineCreditRequestPassword
      // 
      this.txtOfflineCreditRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestPassword.Location = new System.Drawing.Point(100, 42);
      this.txtOfflineCreditRequestPassword.Name = "txtOfflineCreditRequestPassword";
      this.txtOfflineCreditRequestPassword.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestPassword.TabIndex = 1;
      // 
      // lblOfflineCreditRequestPassword
      // 
      this.lblOfflineCreditRequestPassword.AutoSize = true;
      this.lblOfflineCreditRequestPassword.Location = new System.Drawing.Point(7, 46);
      this.lblOfflineCreditRequestPassword.Name = "lblOfflineCreditRequestPassword";
      this.lblOfflineCreditRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblOfflineCreditRequestPassword.TabIndex = 16;
      this.lblOfflineCreditRequestPassword.Text = "Password";
      // 
      // txtOfflineCreditRequestPartnerId
      // 
      this.txtOfflineCreditRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestPartnerId.Location = new System.Drawing.Point(100, 17);
      this.txtOfflineCreditRequestPartnerId.Name = "txtOfflineCreditRequestPartnerId";
      this.txtOfflineCreditRequestPartnerId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestPartnerId.TabIndex = 0;
      // 
      // lblOfflineCreditRequestPartnerId
      // 
      this.lblOfflineCreditRequestPartnerId.AutoSize = true;
      this.lblOfflineCreditRequestPartnerId.Location = new System.Drawing.Point(7, 20);
      this.lblOfflineCreditRequestPartnerId.Name = "lblOfflineCreditRequestPartnerId";
      this.lblOfflineCreditRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblOfflineCreditRequestPartnerId.TabIndex = 14;
      this.lblOfflineCreditRequestPartnerId.Text = "Partner Id";
      // 
      // groupBox16
      // 
      this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox16.Controls.Add(this.txtOfflineCreditRequestBetJackpot);
      this.groupBox16.Controls.Add(this.lblOfflineCreditRequestBetJackpot);
      this.groupBox16.Controls.Add(this.btnOfflineCreditRequestAddBet);
      this.groupBox16.Controls.Add(this.txtOfflineCreditRequestBetAmount);
      this.groupBox16.Controls.Add(this.lblOfflineCreditRequestBetAmount);
      this.groupBox16.Controls.Add(this.txtOfflineCreditRequestBetId);
      this.groupBox16.Controls.Add(this.lblOfflineCreditRequestBetId);
      this.groupBox16.Controls.Add(this.dgvOfflineCreditRequestBets);
      this.groupBox16.Location = new System.Drawing.Point(6, 224);
      this.groupBox16.Name = "groupBox16";
      this.groupBox16.Size = new System.Drawing.Size(300, 263);
      this.groupBox16.TabIndex = 8;
      this.groupBox16.TabStop = false;
      this.groupBox16.Text = "Bets";
      // 
      // txtOfflineCreditRequestBetJackpot
      // 
      this.txtOfflineCreditRequestBetJackpot.DecimalPlaces = 2;
      this.txtOfflineCreditRequestBetJackpot.DecimalSeparator = ".";
      this.txtOfflineCreditRequestBetJackpot.IsDecimal = true;
      this.txtOfflineCreditRequestBetJackpot.Location = new System.Drawing.Point(54, 40);
      this.txtOfflineCreditRequestBetJackpot.Name = "txtOfflineCreditRequestBetJackpot";
      this.txtOfflineCreditRequestBetJackpot.Size = new System.Drawing.Size(85, 20);
      this.txtOfflineCreditRequestBetJackpot.TabIndex = 2;
      // 
      // lblOfflineCreditRequestBetJackpot
      // 
      this.lblOfflineCreditRequestBetJackpot.AutoSize = true;
      this.lblOfflineCreditRequestBetJackpot.Location = new System.Drawing.Point(8, 43);
      this.lblOfflineCreditRequestBetJackpot.Name = "lblOfflineCreditRequestBetJackpot";
      this.lblOfflineCreditRequestBetJackpot.Size = new System.Drawing.Size(45, 13);
      this.lblOfflineCreditRequestBetJackpot.TabIndex = 16;
      this.lblOfflineCreditRequestBetJackpot.Text = "Jackpot";
      // 
      // btnOfflineCreditRequestAddBet
      // 
      this.btnOfflineCreditRequestAddBet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOfflineCreditRequestAddBet.Image = global::GameGatewaySimulator.Properties.Resources.add;
      this.btnOfflineCreditRequestAddBet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnOfflineCreditRequestAddBet.Location = new System.Drawing.Point(204, 39);
      this.btnOfflineCreditRequestAddBet.Name = "btnOfflineCreditRequestAddBet";
      this.btnOfflineCreditRequestAddBet.Size = new System.Drawing.Size(90, 25);
      this.btnOfflineCreditRequestAddBet.TabIndex = 3;
      this.btnOfflineCreditRequestAddBet.Text = "Add bet";
      this.btnOfflineCreditRequestAddBet.UseVisualStyleBackColor = true;
      this.btnOfflineCreditRequestAddBet.Click += new System.EventHandler(this.btnOfflineCreditRequestAddBet_Click);
      // 
      // txtOfflineCreditRequestBetAmount
      // 
      this.txtOfflineCreditRequestBetAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestBetAmount.DecimalPlaces = 2;
      this.txtOfflineCreditRequestBetAmount.DecimalSeparator = ".";
      this.txtOfflineCreditRequestBetAmount.IsDecimal = true;
      this.txtOfflineCreditRequestBetAmount.Location = new System.Drawing.Point(209, 13);
      this.txtOfflineCreditRequestBetAmount.Name = "txtOfflineCreditRequestBetAmount";
      this.txtOfflineCreditRequestBetAmount.Size = new System.Drawing.Size(85, 20);
      this.txtOfflineCreditRequestBetAmount.TabIndex = 1;
      // 
      // lblOfflineCreditRequestBetAmount
      // 
      this.lblOfflineCreditRequestBetAmount.AutoSize = true;
      this.lblOfflineCreditRequestBetAmount.Location = new System.Drawing.Point(145, 16);
      this.lblOfflineCreditRequestBetAmount.Name = "lblOfflineCreditRequestBetAmount";
      this.lblOfflineCreditRequestBetAmount.Size = new System.Drawing.Size(62, 13);
      this.lblOfflineCreditRequestBetAmount.TabIndex = 14;
      this.lblOfflineCreditRequestBetAmount.Text = "Bet Amount";
      // 
      // txtOfflineCreditRequestBetId
      // 
      this.txtOfflineCreditRequestBetId.Location = new System.Drawing.Point(54, 13);
      this.txtOfflineCreditRequestBetId.Name = "txtOfflineCreditRequestBetId";
      this.txtOfflineCreditRequestBetId.Size = new System.Drawing.Size(85, 20);
      this.txtOfflineCreditRequestBetId.TabIndex = 0;
      // 
      // lblOfflineCreditRequestBetId
      // 
      this.lblOfflineCreditRequestBetId.AutoSize = true;
      this.lblOfflineCreditRequestBetId.Location = new System.Drawing.Point(8, 16);
      this.lblOfflineCreditRequestBetId.Name = "lblOfflineCreditRequestBetId";
      this.lblOfflineCreditRequestBetId.Size = new System.Drawing.Size(35, 13);
      this.lblOfflineCreditRequestBetId.TabIndex = 12;
      this.lblOfflineCreditRequestBetId.Text = "Bet Id";
      // 
      // dgvOfflineCreditRequestBets
      // 
      this.dgvOfflineCreditRequestBets.AllowUserToAddRows = false;
      this.dgvOfflineCreditRequestBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvOfflineCreditRequestBets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvOfflineCreditRequestBets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnOfflineCreditBetId,
            this.dgvColumnOfflineCreditBetAmount,
            this.dgvColumnOfflineCreditBetJackpot,
            this.dgvColumnOfflineCreditBetDelete});
      this.dgvOfflineCreditRequestBets.Location = new System.Drawing.Point(6, 66);
      this.dgvOfflineCreditRequestBets.MultiSelect = false;
      this.dgvOfflineCreditRequestBets.Name = "dgvOfflineCreditRequestBets";
      this.dgvOfflineCreditRequestBets.ReadOnly = true;
      this.dgvOfflineCreditRequestBets.RowHeadersVisible = false;
      this.dgvOfflineCreditRequestBets.Size = new System.Drawing.Size(288, 191);
      this.dgvOfflineCreditRequestBets.TabIndex = 4;
      this.dgvOfflineCreditRequestBets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOfflineCreditRequestBets_CellClick);
      // 
      // dgvColumnOfflineCreditBetId
      // 
      this.dgvColumnOfflineCreditBetId.DataPropertyName = "BetId";
      this.dgvColumnOfflineCreditBetId.HeaderText = "Bet Id";
      this.dgvColumnOfflineCreditBetId.Name = "dgvColumnOfflineCreditBetId";
      this.dgvColumnOfflineCreditBetId.ReadOnly = true;
      this.dgvColumnOfflineCreditBetId.Width = 70;
      // 
      // dgvColumnOfflineCreditBetAmount
      // 
      this.dgvColumnOfflineCreditBetAmount.DataPropertyName = "BetAmount";
      this.dgvColumnOfflineCreditBetAmount.HeaderText = "Bet Amount";
      this.dgvColumnOfflineCreditBetAmount.Name = "dgvColumnOfflineCreditBetAmount";
      this.dgvColumnOfflineCreditBetAmount.ReadOnly = true;
      this.dgvColumnOfflineCreditBetAmount.Width = 80;
      // 
      // dgvColumnOfflineCreditBetJackpot
      // 
      this.dgvColumnOfflineCreditBetJackpot.DataPropertyName = "Jackpot";
      this.dgvColumnOfflineCreditBetJackpot.HeaderText = "Jackpot";
      this.dgvColumnOfflineCreditBetJackpot.Name = "dgvColumnOfflineCreditBetJackpot";
      this.dgvColumnOfflineCreditBetJackpot.ReadOnly = true;
      this.dgvColumnOfflineCreditBetJackpot.Width = 70;
      // 
      // dgvColumnOfflineCreditBetDelete
      // 
      this.dgvColumnOfflineCreditBetDelete.HeaderText = "Delete";
      this.dgvColumnOfflineCreditBetDelete.Name = "dgvColumnOfflineCreditBetDelete";
      this.dgvColumnOfflineCreditBetDelete.ReadOnly = true;
      this.dgvColumnOfflineCreditBetDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnOfflineCreditBetDelete.Text = "Delete";
      this.dgvColumnOfflineCreditBetDelete.ToolTipText = "Delete selected bet";
      this.dgvColumnOfflineCreditBetDelete.UseColumnTextForButtonValue = true;
      this.dgvColumnOfflineCreditBetDelete.Width = 65;
      // 
      // txtOfflineCreditRequestCurrency
      // 
      this.txtOfflineCreditRequestCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestCurrency.Location = new System.Drawing.Point(100, 172);
      this.txtOfflineCreditRequestCurrency.Name = "txtOfflineCreditRequestCurrency";
      this.txtOfflineCreditRequestCurrency.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestCurrency.TabIndex = 6;
      // 
      // lblOfflineCreditRequestCurrency
      // 
      this.lblOfflineCreditRequestCurrency.AutoSize = true;
      this.lblOfflineCreditRequestCurrency.Location = new System.Drawing.Point(7, 176);
      this.lblOfflineCreditRequestCurrency.Name = "lblOfflineCreditRequestCurrency";
      this.lblOfflineCreditRequestCurrency.Size = new System.Drawing.Size(49, 13);
      this.lblOfflineCreditRequestCurrency.TabIndex = 9;
      this.lblOfflineCreditRequestCurrency.Text = "Currency";
      // 
      // txtOfflineCreditRequestAmount
      // 
      this.txtOfflineCreditRequestAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestAmount.DecimalPlaces = 2;
      this.txtOfflineCreditRequestAmount.DecimalSeparator = ".";
      this.txtOfflineCreditRequestAmount.IsDecimal = true;
      this.txtOfflineCreditRequestAmount.Location = new System.Drawing.Point(100, 146);
      this.txtOfflineCreditRequestAmount.Name = "txtOfflineCreditRequestAmount";
      this.txtOfflineCreditRequestAmount.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestAmount.TabIndex = 5;
      // 
      // lblOfflineCreditRequestAmount
      // 
      this.lblOfflineCreditRequestAmount.AutoSize = true;
      this.lblOfflineCreditRequestAmount.Location = new System.Drawing.Point(7, 150);
      this.lblOfflineCreditRequestAmount.Name = "lblOfflineCreditRequestAmount";
      this.lblOfflineCreditRequestAmount.Size = new System.Drawing.Size(43, 13);
      this.lblOfflineCreditRequestAmount.TabIndex = 7;
      this.lblOfflineCreditRequestAmount.Text = "Amount";
      // 
      // btnSendOfflineCreditRequest
      // 
      this.btnSendOfflineCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendOfflineCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendOfflineCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendOfflineCreditRequest.Location = new System.Drawing.Point(226, 493);
      this.btnSendOfflineCreditRequest.Name = "btnSendOfflineCreditRequest";
      this.btnSendOfflineCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendOfflineCreditRequest.TabIndex = 10;
      this.btnSendOfflineCreditRequest.Text = "Send";
      this.btnSendOfflineCreditRequest.UseVisualStyleBackColor = true;
      this.btnSendOfflineCreditRequest.Click += new System.EventHandler(this.btnSendOfflineCreditRequest_Click);
      // 
      // txtOfflineCreditRequestGameInstanceId
      // 
      this.txtOfflineCreditRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestGameInstanceId.Location = new System.Drawing.Point(100, 120);
      this.txtOfflineCreditRequestGameInstanceId.Name = "txtOfflineCreditRequestGameInstanceId";
      this.txtOfflineCreditRequestGameInstanceId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestGameInstanceId.TabIndex = 4;
      // 
      // lblOfflineCreditRequestGameInstanceId
      // 
      this.lblOfflineCreditRequestGameInstanceId.AutoSize = true;
      this.lblOfflineCreditRequestGameInstanceId.Location = new System.Drawing.Point(7, 124);
      this.lblOfflineCreditRequestGameInstanceId.Name = "lblOfflineCreditRequestGameInstanceId";
      this.lblOfflineCreditRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblOfflineCreditRequestGameInstanceId.TabIndex = 2;
      this.lblOfflineCreditRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // txtOfflineCreditRequestUserId
      // 
      this.txtOfflineCreditRequestUserId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineCreditRequestUserId.Location = new System.Drawing.Point(100, 68);
      this.txtOfflineCreditRequestUserId.Name = "txtOfflineCreditRequestUserId";
      this.txtOfflineCreditRequestUserId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineCreditRequestUserId.TabIndex = 2;
      // 
      // lblOfflineCreditRequestUserId
      // 
      this.lblOfflineCreditRequestUserId.AutoSize = true;
      this.lblOfflineCreditRequestUserId.Location = new System.Drawing.Point(7, 72);
      this.lblOfflineCreditRequestUserId.Name = "lblOfflineCreditRequestUserId";
      this.lblOfflineCreditRequestUserId.Size = new System.Drawing.Size(41, 13);
      this.lblOfflineCreditRequestUserId.TabIndex = 0;
      this.lblOfflineCreditRequestUserId.Text = "User Id";
      // 
      // splitContainer15
      // 
      this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer15.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer15.Location = new System.Drawing.Point(0, 0);
      this.splitContainer15.Name = "splitContainer15";
      this.splitContainer15.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer15.Panel1
      // 
      this.splitContainer15.Panel1.Controls.Add(this.groupBox17);
      // 
      // splitContainer15.Panel2
      // 
      this.splitContainer15.Panel2.Controls.Add(this.groupBox27);
      this.splitContainer15.Size = new System.Drawing.Size(946, 567);
      this.splitContainer15.SplitterDistance = 245;
      this.splitContainer15.TabIndex = 2;
      // 
      // groupBox17
      // 
      this.groupBox17.Controls.Add(this.requestDataOfflineCreditRequest);
      this.groupBox17.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox17.Location = new System.Drawing.Point(0, 0);
      this.groupBox17.Name = "groupBox17";
      this.groupBox17.Size = new System.Drawing.Size(946, 245);
      this.groupBox17.TabIndex = 3;
      this.groupBox17.TabStop = false;
      this.groupBox17.Text = "Request XML data";
      // 
      // requestDataOfflineCreditRequest
      // 
      this.requestDataOfflineCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataOfflineCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataOfflineCreditRequest.Name = "requestDataOfflineCreditRequest";
      this.requestDataOfflineCreditRequest.Size = new System.Drawing.Size(940, 226);
      this.requestDataOfflineCreditRequest.TabIndex = 0;
      // 
      // groupBox27
      // 
      this.groupBox27.Controls.Add(this.responseDataOfflineCreditRequest);
      this.groupBox27.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox27.Location = new System.Drawing.Point(0, 0);
      this.groupBox27.Name = "groupBox27";
      this.groupBox27.Size = new System.Drawing.Size(946, 318);
      this.groupBox27.TabIndex = 2;
      this.groupBox27.TabStop = false;
      this.groupBox27.Text = "Response data";
      // 
      // responseDataOfflineCreditRequest
      // 
      this.responseDataOfflineCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataOfflineCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataOfflineCreditRequest.Name = "responseDataOfflineCreditRequest";
      this.responseDataOfflineCreditRequest.Size = new System.Drawing.Size(940, 299);
      this.responseDataOfflineCreditRequest.TabIndex = 0;
      // 
      // tabPageCreditRequest
      // 
      this.tabPageCreditRequest.Controls.Add(this.splitContainer5);
      this.tabPageCreditRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageCreditRequest.Name = "tabPageCreditRequest";
      this.tabPageCreditRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageCreditRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageCreditRequest.TabIndex = 4;
      this.tabPageCreditRequest.Text = "Credit Request";
      this.tabPageCreditRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer5
      // 
      this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer5.Location = new System.Drawing.Point(3, 3);
      this.splitContainer5.Name = "splitContainer5";
      // 
      // splitContainer5.Panel1
      // 
      this.splitContainer5.Panel1.Controls.Add(this.groupBox9);
      // 
      // splitContainer5.Panel2
      // 
      this.splitContainer5.Panel2.Controls.Add(this.splitContainer13);
      this.splitContainer5.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer5.SplitterDistance = 314;
      this.splitContainer5.TabIndex = 4;
      // 
      // groupBox9
      // 
      this.groupBox9.Controls.Add(this.txtCreditRequestGameId);
      this.groupBox9.Controls.Add(this.lblCreditRequestGameId);
      this.groupBox9.Controls.Add(this.txtCreditRequestTransaction);
      this.groupBox9.Controls.Add(this.lblCreditRequestTransaction);
      this.groupBox9.Controls.Add(this.txtCreditRequestPassword);
      this.groupBox9.Controls.Add(this.lblCreditRequestPassword);
      this.groupBox9.Controls.Add(this.txtCreditRequestPartnerId);
      this.groupBox9.Controls.Add(this.lblCreditRequestPartnerId);
      this.groupBox9.Controls.Add(this.chkCreditRequestBonus);
      this.groupBox9.Controls.Add(this.lblCreditRequestBonus);
      this.groupBox9.Controls.Add(this.btnBuildCreditRequest);
      this.groupBox9.Controls.Add(this.groupBox10);
      this.groupBox9.Controls.Add(this.txtCreditRequestCurrency);
      this.groupBox9.Controls.Add(this.lblCreditRequestCurrency);
      this.groupBox9.Controls.Add(this.txtCreditRequestAmount);
      this.groupBox9.Controls.Add(this.lblCreditRequestAmount);
      this.groupBox9.Controls.Add(this.btnSendCreditRequest);
      this.groupBox9.Controls.Add(this.txtCreditRequestGameInstanceId);
      this.groupBox9.Controls.Add(this.lblCreditRequestGameInstanceId);
      this.groupBox9.Controls.Add(this.txtCreditRequestSessionId);
      this.groupBox9.Controls.Add(this.lblCreditRequestSessionId);
      this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox9.Location = new System.Drawing.Point(0, 0);
      this.groupBox9.Name = "groupBox9";
      this.groupBox9.Size = new System.Drawing.Size(314, 567);
      this.groupBox9.TabIndex = 1;
      this.groupBox9.TabStop = false;
      this.groupBox9.Text = "Request data";
      // 
      // txtCreditRequestGameId
      // 
      this.txtCreditRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestGameId.Location = new System.Drawing.Point(100, 96);
      this.txtCreditRequestGameId.Name = "txtCreditRequestGameId";
      this.txtCreditRequestGameId.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestGameId.TabIndex = 3;
      // 
      // lblCreditRequestGameId
      // 
      this.lblCreditRequestGameId.AutoSize = true;
      this.lblCreditRequestGameId.Location = new System.Drawing.Point(7, 99);
      this.lblCreditRequestGameId.Name = "lblCreditRequestGameId";
      this.lblCreditRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblCreditRequestGameId.TabIndex = 26;
      this.lblCreditRequestGameId.Text = "Game Id";
      // 
      // txtCreditRequestTransaction
      // 
      this.txtCreditRequestTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestTransaction.Location = new System.Drawing.Point(100, 220);
      this.txtCreditRequestTransaction.Name = "txtCreditRequestTransaction";
      this.txtCreditRequestTransaction.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestTransaction.TabIndex = 8;
      // 
      // lblCreditRequestTransaction
      // 
      this.lblCreditRequestTransaction.AutoSize = true;
      this.lblCreditRequestTransaction.Location = new System.Drawing.Point(7, 223);
      this.lblCreditRequestTransaction.Name = "lblCreditRequestTransaction";
      this.lblCreditRequestTransaction.Size = new System.Drawing.Size(63, 13);
      this.lblCreditRequestTransaction.TabIndex = 24;
      this.lblCreditRequestTransaction.Text = "Transaction";
      // 
      // txtCreditRequestPassword
      // 
      this.txtCreditRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestPassword.Location = new System.Drawing.Point(100, 70);
      this.txtCreditRequestPassword.Name = "txtCreditRequestPassword";
      this.txtCreditRequestPassword.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestPassword.TabIndex = 2;
      // 
      // lblCreditRequestPassword
      // 
      this.lblCreditRequestPassword.AutoSize = true;
      this.lblCreditRequestPassword.Location = new System.Drawing.Point(7, 73);
      this.lblCreditRequestPassword.Name = "lblCreditRequestPassword";
      this.lblCreditRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblCreditRequestPassword.TabIndex = 22;
      this.lblCreditRequestPassword.Text = "Password";
      // 
      // txtCreditRequestPartnerId
      // 
      this.txtCreditRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestPartnerId.Location = new System.Drawing.Point(100, 44);
      this.txtCreditRequestPartnerId.Name = "txtCreditRequestPartnerId";
      this.txtCreditRequestPartnerId.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestPartnerId.TabIndex = 1;
      // 
      // lblCreditRequestPartnerId
      // 
      this.lblCreditRequestPartnerId.AutoSize = true;
      this.lblCreditRequestPartnerId.Location = new System.Drawing.Point(7, 47);
      this.lblCreditRequestPartnerId.Name = "lblCreditRequestPartnerId";
      this.lblCreditRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblCreditRequestPartnerId.TabIndex = 20;
      this.lblCreditRequestPartnerId.Text = "Partner Id";
      // 
      // chkCreditRequestBonus
      // 
      this.chkCreditRequestBonus.AutoSize = true;
      this.chkCreditRequestBonus.Location = new System.Drawing.Point(100, 200);
      this.chkCreditRequestBonus.Name = "chkCreditRequestBonus";
      this.chkCreditRequestBonus.Size = new System.Drawing.Size(15, 14);
      this.chkCreditRequestBonus.TabIndex = 7;
      this.chkCreditRequestBonus.UseVisualStyleBackColor = true;
      // 
      // lblCreditRequestBonus
      // 
      this.lblCreditRequestBonus.AutoSize = true;
      this.lblCreditRequestBonus.Location = new System.Drawing.Point(7, 201);
      this.lblCreditRequestBonus.Name = "lblCreditRequestBonus";
      this.lblCreditRequestBonus.Size = new System.Drawing.Size(37, 13);
      this.lblCreditRequestBonus.TabIndex = 18;
      this.lblCreditRequestBonus.Text = "Bonus";
      // 
      // btnBuildCreditRequest
      // 
      this.btnBuildCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildCreditRequest.Location = new System.Drawing.Point(140, 515);
      this.btnBuildCreditRequest.Name = "btnBuildCreditRequest";
      this.btnBuildCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildCreditRequest.TabIndex = 10;
      this.btnBuildCreditRequest.Text = "Build";
      this.btnBuildCreditRequest.UseVisualStyleBackColor = true;
      this.btnBuildCreditRequest.Click += new System.EventHandler(this.btnBuildCreditRequest_Click);
      // 
      // groupBox10
      // 
      this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox10.Controls.Add(this.txtCreditRequestBetJackpot);
      this.groupBox10.Controls.Add(this.lblCreditRequestBetJackpot);
      this.groupBox10.Controls.Add(this.btnCreditRequestAddBet);
      this.groupBox10.Controls.Add(this.txtCreditRequestBetAmount);
      this.groupBox10.Controls.Add(this.lblCreditRequestBetAmount);
      this.groupBox10.Controls.Add(this.txtCreditRequestBetId);
      this.groupBox10.Controls.Add(this.lblCreditRequestBetId);
      this.groupBox10.Controls.Add(this.dgvCreditRequestBets);
      this.groupBox10.Location = new System.Drawing.Point(6, 246);
      this.groupBox10.Name = "groupBox10";
      this.groupBox10.Size = new System.Drawing.Size(300, 263);
      this.groupBox10.TabIndex = 9;
      this.groupBox10.TabStop = false;
      this.groupBox10.Text = "Bets";
      // 
      // txtCreditRequestBetJackpot
      // 
      this.txtCreditRequestBetJackpot.DecimalPlaces = 2;
      this.txtCreditRequestBetJackpot.DecimalSeparator = ".";
      this.txtCreditRequestBetJackpot.IsDecimal = true;
      this.txtCreditRequestBetJackpot.Location = new System.Drawing.Point(54, 40);
      this.txtCreditRequestBetJackpot.Name = "txtCreditRequestBetJackpot";
      this.txtCreditRequestBetJackpot.Size = new System.Drawing.Size(85, 20);
      this.txtCreditRequestBetJackpot.TabIndex = 2;
      // 
      // lblCreditRequestBetJackpot
      // 
      this.lblCreditRequestBetJackpot.AutoSize = true;
      this.lblCreditRequestBetJackpot.Location = new System.Drawing.Point(8, 43);
      this.lblCreditRequestBetJackpot.Name = "lblCreditRequestBetJackpot";
      this.lblCreditRequestBetJackpot.Size = new System.Drawing.Size(45, 13);
      this.lblCreditRequestBetJackpot.TabIndex = 16;
      this.lblCreditRequestBetJackpot.Text = "Jackpot";
      // 
      // btnCreditRequestAddBet
      // 
      this.btnCreditRequestAddBet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCreditRequestAddBet.Image = global::GameGatewaySimulator.Properties.Resources.add;
      this.btnCreditRequestAddBet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnCreditRequestAddBet.Location = new System.Drawing.Point(204, 39);
      this.btnCreditRequestAddBet.Name = "btnCreditRequestAddBet";
      this.btnCreditRequestAddBet.Size = new System.Drawing.Size(90, 25);
      this.btnCreditRequestAddBet.TabIndex = 3;
      this.btnCreditRequestAddBet.Text = "Add bet";
      this.btnCreditRequestAddBet.UseVisualStyleBackColor = true;
      this.btnCreditRequestAddBet.Click += new System.EventHandler(this.btnCreditRequestAddBet_Click);
      // 
      // txtCreditRequestBetAmount
      // 
      this.txtCreditRequestBetAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestBetAmount.DecimalPlaces = 2;
      this.txtCreditRequestBetAmount.DecimalSeparator = ".";
      this.txtCreditRequestBetAmount.IsDecimal = true;
      this.txtCreditRequestBetAmount.Location = new System.Drawing.Point(209, 13);
      this.txtCreditRequestBetAmount.Name = "txtCreditRequestBetAmount";
      this.txtCreditRequestBetAmount.Size = new System.Drawing.Size(85, 20);
      this.txtCreditRequestBetAmount.TabIndex = 1;
      // 
      // lblCreditRequestBetAmount
      // 
      this.lblCreditRequestBetAmount.AutoSize = true;
      this.lblCreditRequestBetAmount.Location = new System.Drawing.Point(145, 16);
      this.lblCreditRequestBetAmount.Name = "lblCreditRequestBetAmount";
      this.lblCreditRequestBetAmount.Size = new System.Drawing.Size(62, 13);
      this.lblCreditRequestBetAmount.TabIndex = 14;
      this.lblCreditRequestBetAmount.Text = "Bet Amount";
      // 
      // txtCreditRequestBetId
      // 
      this.txtCreditRequestBetId.Location = new System.Drawing.Point(54, 13);
      this.txtCreditRequestBetId.Name = "txtCreditRequestBetId";
      this.txtCreditRequestBetId.Size = new System.Drawing.Size(85, 20);
      this.txtCreditRequestBetId.TabIndex = 0;
      // 
      // lblCreditRequestBetId
      // 
      this.lblCreditRequestBetId.AutoSize = true;
      this.lblCreditRequestBetId.Location = new System.Drawing.Point(8, 16);
      this.lblCreditRequestBetId.Name = "lblCreditRequestBetId";
      this.lblCreditRequestBetId.Size = new System.Drawing.Size(35, 13);
      this.lblCreditRequestBetId.TabIndex = 12;
      this.lblCreditRequestBetId.Text = "Bet Id";
      // 
      // dgvCreditRequestBets
      // 
      this.dgvCreditRequestBets.AllowUserToAddRows = false;
      this.dgvCreditRequestBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvCreditRequestBets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvCreditRequestBets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnCreditBetId,
            this.dgvColumnCreditBetAmount,
            this.dgvColumnCreditBetJackpot,
            this.dgvColumnCreditBetDelete});
      this.dgvCreditRequestBets.Location = new System.Drawing.Point(6, 66);
      this.dgvCreditRequestBets.MultiSelect = false;
      this.dgvCreditRequestBets.Name = "dgvCreditRequestBets";
      this.dgvCreditRequestBets.ReadOnly = true;
      this.dgvCreditRequestBets.RowHeadersVisible = false;
      this.dgvCreditRequestBets.Size = new System.Drawing.Size(288, 191);
      this.dgvCreditRequestBets.TabIndex = 4;
      this.dgvCreditRequestBets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCreditRequestBets_CellClick);
      // 
      // dgvColumnCreditBetId
      // 
      this.dgvColumnCreditBetId.DataPropertyName = "BetId";
      this.dgvColumnCreditBetId.HeaderText = "Bet Id";
      this.dgvColumnCreditBetId.Name = "dgvColumnCreditBetId";
      this.dgvColumnCreditBetId.ReadOnly = true;
      this.dgvColumnCreditBetId.Width = 70;
      // 
      // dgvColumnCreditBetAmount
      // 
      this.dgvColumnCreditBetAmount.DataPropertyName = "Amount";
      this.dgvColumnCreditBetAmount.HeaderText = "Bet Amount";
      this.dgvColumnCreditBetAmount.Name = "dgvColumnCreditBetAmount";
      this.dgvColumnCreditBetAmount.ReadOnly = true;
      this.dgvColumnCreditBetAmount.Width = 80;
      // 
      // dgvColumnCreditBetJackpot
      // 
      this.dgvColumnCreditBetJackpot.DataPropertyName = "Jackpot";
      this.dgvColumnCreditBetJackpot.HeaderText = "Jackpot";
      this.dgvColumnCreditBetJackpot.Name = "dgvColumnCreditBetJackpot";
      this.dgvColumnCreditBetJackpot.ReadOnly = true;
      this.dgvColumnCreditBetJackpot.Width = 70;
      // 
      // dgvColumnCreditBetDelete
      // 
      this.dgvColumnCreditBetDelete.HeaderText = "Delete";
      this.dgvColumnCreditBetDelete.Name = "dgvColumnCreditBetDelete";
      this.dgvColumnCreditBetDelete.ReadOnly = true;
      this.dgvColumnCreditBetDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnCreditBetDelete.Text = "Delete";
      this.dgvColumnCreditBetDelete.ToolTipText = "Delete selected bet";
      this.dgvColumnCreditBetDelete.UseColumnTextForButtonValue = true;
      this.dgvColumnCreditBetDelete.Width = 65;
      // 
      // txtCreditRequestCurrency
      // 
      this.txtCreditRequestCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestCurrency.Location = new System.Drawing.Point(100, 174);
      this.txtCreditRequestCurrency.Name = "txtCreditRequestCurrency";
      this.txtCreditRequestCurrency.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestCurrency.TabIndex = 6;
      // 
      // lblCreditRequestCurrency
      // 
      this.lblCreditRequestCurrency.AutoSize = true;
      this.lblCreditRequestCurrency.Location = new System.Drawing.Point(7, 177);
      this.lblCreditRequestCurrency.Name = "lblCreditRequestCurrency";
      this.lblCreditRequestCurrency.Size = new System.Drawing.Size(49, 13);
      this.lblCreditRequestCurrency.TabIndex = 9;
      this.lblCreditRequestCurrency.Text = "Currency";
      // 
      // txtCreditRequestAmount
      // 
      this.txtCreditRequestAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestAmount.DecimalPlaces = 2;
      this.txtCreditRequestAmount.DecimalSeparator = ".";
      this.txtCreditRequestAmount.IsDecimal = true;
      this.txtCreditRequestAmount.Location = new System.Drawing.Point(100, 148);
      this.txtCreditRequestAmount.Name = "txtCreditRequestAmount";
      this.txtCreditRequestAmount.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestAmount.TabIndex = 5;
      // 
      // lblCreditRequestAmount
      // 
      this.lblCreditRequestAmount.AutoSize = true;
      this.lblCreditRequestAmount.Location = new System.Drawing.Point(7, 151);
      this.lblCreditRequestAmount.Name = "lblCreditRequestAmount";
      this.lblCreditRequestAmount.Size = new System.Drawing.Size(43, 13);
      this.lblCreditRequestAmount.TabIndex = 7;
      this.lblCreditRequestAmount.Text = "Amount";
      // 
      // btnSendCreditRequest
      // 
      this.btnSendCreditRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendCreditRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendCreditRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendCreditRequest.Location = new System.Drawing.Point(226, 515);
      this.btnSendCreditRequest.Name = "btnSendCreditRequest";
      this.btnSendCreditRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendCreditRequest.TabIndex = 11;
      this.btnSendCreditRequest.Text = "Send";
      this.btnSendCreditRequest.UseVisualStyleBackColor = true;
      this.btnSendCreditRequest.Click += new System.EventHandler(this.btnSendCreditRequest_Click);
      // 
      // txtCreditRequestGameInstanceId
      // 
      this.txtCreditRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestGameInstanceId.Location = new System.Drawing.Point(100, 122);
      this.txtCreditRequestGameInstanceId.Name = "txtCreditRequestGameInstanceId";
      this.txtCreditRequestGameInstanceId.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestGameInstanceId.TabIndex = 4;
      // 
      // lblCreditRequestGameInstanceId
      // 
      this.lblCreditRequestGameInstanceId.AutoSize = true;
      this.lblCreditRequestGameInstanceId.Location = new System.Drawing.Point(7, 125);
      this.lblCreditRequestGameInstanceId.Name = "lblCreditRequestGameInstanceId";
      this.lblCreditRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblCreditRequestGameInstanceId.TabIndex = 2;
      this.lblCreditRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // txtCreditRequestSessionId
      // 
      this.txtCreditRequestSessionId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtCreditRequestSessionId.Location = new System.Drawing.Point(100, 17);
      this.txtCreditRequestSessionId.Name = "txtCreditRequestSessionId";
      this.txtCreditRequestSessionId.Size = new System.Drawing.Size(206, 20);
      this.txtCreditRequestSessionId.TabIndex = 0;
      // 
      // lblCreditRequestSessionId
      // 
      this.lblCreditRequestSessionId.AutoSize = true;
      this.lblCreditRequestSessionId.Location = new System.Drawing.Point(7, 20);
      this.lblCreditRequestSessionId.Name = "lblCreditRequestSessionId";
      this.lblCreditRequestSessionId.Size = new System.Drawing.Size(56, 13);
      this.lblCreditRequestSessionId.TabIndex = 0;
      this.lblCreditRequestSessionId.Text = "Session Id";
      // 
      // splitContainer13
      // 
      this.splitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer13.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer13.Location = new System.Drawing.Point(0, 0);
      this.splitContainer13.Name = "splitContainer13";
      this.splitContainer13.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer13.Panel1
      // 
      this.splitContainer13.Panel1.Controls.Add(this.groupBox11);
      // 
      // splitContainer13.Panel2
      // 
      this.splitContainer13.Panel2.Controls.Add(this.groupBox25);
      this.splitContainer13.Size = new System.Drawing.Size(946, 567);
      this.splitContainer13.SplitterDistance = 257;
      this.splitContainer13.TabIndex = 2;
      // 
      // groupBox11
      // 
      this.groupBox11.Controls.Add(this.requestDataCreditRequest);
      this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox11.Location = new System.Drawing.Point(0, 0);
      this.groupBox11.Name = "groupBox11";
      this.groupBox11.Size = new System.Drawing.Size(946, 257);
      this.groupBox11.TabIndex = 3;
      this.groupBox11.TabStop = false;
      this.groupBox11.Text = "Request XML data";
      // 
      // requestDataCreditRequest
      // 
      this.requestDataCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataCreditRequest.Name = "requestDataCreditRequest";
      this.requestDataCreditRequest.Size = new System.Drawing.Size(940, 238);
      this.requestDataCreditRequest.TabIndex = 0;
      // 
      // groupBox25
      // 
      this.groupBox25.Controls.Add(this.responseDataCreditRequest);
      this.groupBox25.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox25.Location = new System.Drawing.Point(0, 0);
      this.groupBox25.Name = "groupBox25";
      this.groupBox25.Size = new System.Drawing.Size(946, 306);
      this.groupBox25.TabIndex = 2;
      this.groupBox25.TabStop = false;
      this.groupBox25.Text = "Response data";
      // 
      // responseDataCreditRequest
      // 
      this.responseDataCreditRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataCreditRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataCreditRequest.Name = "responseDataCreditRequest";
      this.responseDataCreditRequest.Size = new System.Drawing.Size(940, 287);
      this.responseDataCreditRequest.TabIndex = 0;
      // 
      // tabPageOfflineDebitRequest
      // 
      this.tabPageOfflineDebitRequest.Controls.Add(this.splitContainer8);
      this.tabPageOfflineDebitRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageOfflineDebitRequest.Name = "tabPageOfflineDebitRequest";
      this.tabPageOfflineDebitRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageOfflineDebitRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageOfflineDebitRequest.TabIndex = 7;
      this.tabPageOfflineDebitRequest.Text = "Offline Debit Request";
      this.tabPageOfflineDebitRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer8
      // 
      this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer8.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer8.Location = new System.Drawing.Point(3, 3);
      this.splitContainer8.Name = "splitContainer8";
      // 
      // splitContainer8.Panel1
      // 
      this.splitContainer8.Panel1.Controls.Add(this.groupBox18);
      // 
      // splitContainer8.Panel2
      // 
      this.splitContainer8.Panel2.Controls.Add(this.splitContainer16);
      this.splitContainer8.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer8.SplitterDistance = 314;
      this.splitContainer8.TabIndex = 4;
      // 
      // groupBox18
      // 
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestGameId);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestGameId);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestTransaction);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestTransaction);
      this.groupBox18.Controls.Add(this.btnBuildOfflineDebitRequest);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestUserId);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestUserId);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestPassword);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestPassword);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestPartnerId);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestPartnerId);
      this.groupBox18.Controls.Add(this.groupBox19);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestCurrency);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestCurrency);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestAmount);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestAmount);
      this.groupBox18.Controls.Add(this.btnSendOfflineDebitRequest);
      this.groupBox18.Controls.Add(this.txtOfflineDebitRequestGameInstanceId);
      this.groupBox18.Controls.Add(this.lblOfflineDebitRequestGameInstanceId);
      this.groupBox18.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox18.Location = new System.Drawing.Point(0, 0);
      this.groupBox18.Name = "groupBox18";
      this.groupBox18.Size = new System.Drawing.Size(314, 567);
      this.groupBox18.TabIndex = 1;
      this.groupBox18.TabStop = false;
      this.groupBox18.Text = "Request data";
      // 
      // txtOfflineDebitRequestGameId
      // 
      this.txtOfflineDebitRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestGameId.Location = new System.Drawing.Point(100, 95);
      this.txtOfflineDebitRequestGameId.Name = "txtOfflineDebitRequestGameId";
      this.txtOfflineDebitRequestGameId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestGameId.TabIndex = 3;
      // 
      // lblOfflineDebitRequestGameId
      // 
      this.lblOfflineDebitRequestGameId.AutoSize = true;
      this.lblOfflineDebitRequestGameId.Location = new System.Drawing.Point(7, 99);
      this.lblOfflineDebitRequestGameId.Name = "lblOfflineDebitRequestGameId";
      this.lblOfflineDebitRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblOfflineDebitRequestGameId.TabIndex = 20;
      this.lblOfflineDebitRequestGameId.Text = "Game Id";
      // 
      // txtOfflineDebitRequestTransaction
      // 
      this.txtOfflineDebitRequestTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestTransaction.Location = new System.Drawing.Point(100, 199);
      this.txtOfflineDebitRequestTransaction.Name = "txtOfflineDebitRequestTransaction";
      this.txtOfflineDebitRequestTransaction.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestTransaction.TabIndex = 7;
      // 
      // lblOfflineDebitRequestTransaction
      // 
      this.lblOfflineDebitRequestTransaction.AutoSize = true;
      this.lblOfflineDebitRequestTransaction.Location = new System.Drawing.Point(7, 202);
      this.lblOfflineDebitRequestTransaction.Name = "lblOfflineDebitRequestTransaction";
      this.lblOfflineDebitRequestTransaction.Size = new System.Drawing.Size(63, 13);
      this.lblOfflineDebitRequestTransaction.TabIndex = 18;
      this.lblOfflineDebitRequestTransaction.Text = "Transaction";
      // 
      // btnBuildOfflineDebitRequest
      // 
      this.btnBuildOfflineDebitRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildOfflineDebitRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildOfflineDebitRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildOfflineDebitRequest.Location = new System.Drawing.Point(140, 494);
      this.btnBuildOfflineDebitRequest.Name = "btnBuildOfflineDebitRequest";
      this.btnBuildOfflineDebitRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildOfflineDebitRequest.TabIndex = 9;
      this.btnBuildOfflineDebitRequest.Text = "Build";
      this.btnBuildOfflineDebitRequest.UseVisualStyleBackColor = true;
      this.btnBuildOfflineDebitRequest.Click += new System.EventHandler(this.btnBuildOfflineDebitRequest_Click);
      // 
      // txtOfflineDebitRequestUserId
      // 
      this.txtOfflineDebitRequestUserId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestUserId.Location = new System.Drawing.Point(100, 69);
      this.txtOfflineDebitRequestUserId.Name = "txtOfflineDebitRequestUserId";
      this.txtOfflineDebitRequestUserId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestUserId.TabIndex = 2;
      // 
      // lblOfflineDebitRequestUserId
      // 
      this.lblOfflineDebitRequestUserId.AutoSize = true;
      this.lblOfflineDebitRequestUserId.Location = new System.Drawing.Point(7, 73);
      this.lblOfflineDebitRequestUserId.Name = "lblOfflineDebitRequestUserId";
      this.lblOfflineDebitRequestUserId.Size = new System.Drawing.Size(41, 13);
      this.lblOfflineDebitRequestUserId.TabIndex = 16;
      this.lblOfflineDebitRequestUserId.Text = "User Id";
      // 
      // txtOfflineDebitRequestPassword
      // 
      this.txtOfflineDebitRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestPassword.Location = new System.Drawing.Point(100, 43);
      this.txtOfflineDebitRequestPassword.Name = "txtOfflineDebitRequestPassword";
      this.txtOfflineDebitRequestPassword.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestPassword.TabIndex = 1;
      // 
      // lblOfflineDebitRequestPassword
      // 
      this.lblOfflineDebitRequestPassword.AutoSize = true;
      this.lblOfflineDebitRequestPassword.Location = new System.Drawing.Point(7, 46);
      this.lblOfflineDebitRequestPassword.Name = "lblOfflineDebitRequestPassword";
      this.lblOfflineDebitRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblOfflineDebitRequestPassword.TabIndex = 14;
      this.lblOfflineDebitRequestPassword.Text = "Password";
      // 
      // txtOfflineDebitRequestPartnerId
      // 
      this.txtOfflineDebitRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestPartnerId.Location = new System.Drawing.Point(100, 17);
      this.txtOfflineDebitRequestPartnerId.Name = "txtOfflineDebitRequestPartnerId";
      this.txtOfflineDebitRequestPartnerId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestPartnerId.TabIndex = 0;
      // 
      // lblOfflineDebitRequestPartnerId
      // 
      this.lblOfflineDebitRequestPartnerId.AutoSize = true;
      this.lblOfflineDebitRequestPartnerId.Location = new System.Drawing.Point(7, 20);
      this.lblOfflineDebitRequestPartnerId.Name = "lblOfflineDebitRequestPartnerId";
      this.lblOfflineDebitRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblOfflineDebitRequestPartnerId.TabIndex = 12;
      this.lblOfflineDebitRequestPartnerId.Text = "Partner Id";
      // 
      // groupBox19
      // 
      this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox19.Controls.Add(this.btnOfflineDebitRequestAddBet);
      this.groupBox19.Controls.Add(this.txtOfflineDebitRequestBetRake);
      this.groupBox19.Controls.Add(this.lblOfflineDebitRequestBetRake);
      this.groupBox19.Controls.Add(this.txtOfflineDebitRequestBetValue);
      this.groupBox19.Controls.Add(this.lblOfflineDebitRequestBetValue);
      this.groupBox19.Controls.Add(this.txtOfflineDebitRequestBetId);
      this.groupBox19.Controls.Add(this.lblOfflineDebitRequestBetId);
      this.groupBox19.Controls.Add(this.dgvOfflineDebitRequestBets);
      this.groupBox19.Location = new System.Drawing.Point(6, 225);
      this.groupBox19.Name = "groupBox19";
      this.groupBox19.Size = new System.Drawing.Size(300, 263);
      this.groupBox19.TabIndex = 8;
      this.groupBox19.TabStop = false;
      this.groupBox19.Text = "Bets";
      // 
      // btnOfflineDebitRequestAddBet
      // 
      this.btnOfflineDebitRequestAddBet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOfflineDebitRequestAddBet.Image = global::GameGatewaySimulator.Properties.Resources.add;
      this.btnOfflineDebitRequestAddBet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnOfflineDebitRequestAddBet.Location = new System.Drawing.Point(204, 39);
      this.btnOfflineDebitRequestAddBet.Name = "btnOfflineDebitRequestAddBet";
      this.btnOfflineDebitRequestAddBet.Size = new System.Drawing.Size(90, 25);
      this.btnOfflineDebitRequestAddBet.TabIndex = 3;
      this.btnOfflineDebitRequestAddBet.Text = "Add bet";
      this.btnOfflineDebitRequestAddBet.UseVisualStyleBackColor = true;
      this.btnOfflineDebitRequestAddBet.Click += new System.EventHandler(this.btnOfflineDebitRequestAddBet_Click);
      // 
      // txtOfflineDebitRequestBetRake
      // 
      this.txtOfflineDebitRequestBetRake.DecimalPlaces = 2;
      this.txtOfflineDebitRequestBetRake.DecimalSeparator = ".";
      this.txtOfflineDebitRequestBetRake.IsDecimal = true;
      this.txtOfflineDebitRequestBetRake.Location = new System.Drawing.Point(66, 40);
      this.txtOfflineDebitRequestBetRake.Name = "txtOfflineDebitRequestBetRake";
      this.txtOfflineDebitRequestBetRake.Size = new System.Drawing.Size(90, 20);
      this.txtOfflineDebitRequestBetRake.TabIndex = 2;
      // 
      // lblOfflineDebitRequestBetRake
      // 
      this.lblOfflineDebitRequestBetRake.AutoSize = true;
      this.lblOfflineDebitRequestBetRake.Location = new System.Drawing.Point(8, 43);
      this.lblOfflineDebitRequestBetRake.Name = "lblOfflineDebitRequestBetRake";
      this.lblOfflineDebitRequestBetRake.Size = new System.Drawing.Size(52, 13);
      this.lblOfflineDebitRequestBetRake.TabIndex = 16;
      this.lblOfflineDebitRequestBetRake.Text = "Bet Rake";
      // 
      // txtOfflineDebitRequestBetValue
      // 
      this.txtOfflineDebitRequestBetValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestBetValue.DecimalPlaces = 2;
      this.txtOfflineDebitRequestBetValue.DecimalSeparator = ".";
      this.txtOfflineDebitRequestBetValue.IsDecimal = true;
      this.txtOfflineDebitRequestBetValue.Location = new System.Drawing.Point(204, 13);
      this.txtOfflineDebitRequestBetValue.Name = "txtOfflineDebitRequestBetValue";
      this.txtOfflineDebitRequestBetValue.Size = new System.Drawing.Size(90, 20);
      this.txtOfflineDebitRequestBetValue.TabIndex = 1;
      // 
      // lblOfflineDebitRequestBetValue
      // 
      this.lblOfflineDebitRequestBetValue.AutoSize = true;
      this.lblOfflineDebitRequestBetValue.Location = new System.Drawing.Point(145, 16);
      this.lblOfflineDebitRequestBetValue.Name = "lblOfflineDebitRequestBetValue";
      this.lblOfflineDebitRequestBetValue.Size = new System.Drawing.Size(53, 13);
      this.lblOfflineDebitRequestBetValue.TabIndex = 14;
      this.lblOfflineDebitRequestBetValue.Text = "Bet Value";
      // 
      // txtOfflineDebitRequestBetId
      // 
      this.txtOfflineDebitRequestBetId.Location = new System.Drawing.Point(49, 13);
      this.txtOfflineDebitRequestBetId.Name = "txtOfflineDebitRequestBetId";
      this.txtOfflineDebitRequestBetId.Size = new System.Drawing.Size(90, 20);
      this.txtOfflineDebitRequestBetId.TabIndex = 0;
      // 
      // lblOfflineDebitRequestBetId
      // 
      this.lblOfflineDebitRequestBetId.AutoSize = true;
      this.lblOfflineDebitRequestBetId.Location = new System.Drawing.Point(8, 16);
      this.lblOfflineDebitRequestBetId.Name = "lblOfflineDebitRequestBetId";
      this.lblOfflineDebitRequestBetId.Size = new System.Drawing.Size(35, 13);
      this.lblOfflineDebitRequestBetId.TabIndex = 12;
      this.lblOfflineDebitRequestBetId.Text = "Bet Id";
      // 
      // dgvOfflineDebitRequestBets
      // 
      this.dgvOfflineDebitRequestBets.AllowUserToAddRows = false;
      this.dgvOfflineDebitRequestBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvOfflineDebitRequestBets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvOfflineDebitRequestBets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnOfflineDebitBetId,
            this.dgvColumnOfflineDebitBetValue,
            this.dgvColumnOfflineDebitBetRake,
            this.dgvColumnOfflineDebitBetDelete});
      this.dgvOfflineDebitRequestBets.Location = new System.Drawing.Point(6, 66);
      this.dgvOfflineDebitRequestBets.MultiSelect = false;
      this.dgvOfflineDebitRequestBets.Name = "dgvOfflineDebitRequestBets";
      this.dgvOfflineDebitRequestBets.ReadOnly = true;
      this.dgvOfflineDebitRequestBets.RowHeadersVisible = false;
      this.dgvOfflineDebitRequestBets.Size = new System.Drawing.Size(288, 191);
      this.dgvOfflineDebitRequestBets.TabIndex = 4;
      this.dgvOfflineDebitRequestBets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOfflineDebitRequestBets_CellClick);
      // 
      // dgvColumnOfflineDebitBetId
      // 
      this.dgvColumnOfflineDebitBetId.DataPropertyName = "BetId";
      this.dgvColumnOfflineDebitBetId.HeaderText = "Bet Id";
      this.dgvColumnOfflineDebitBetId.Name = "dgvColumnOfflineDebitBetId";
      this.dgvColumnOfflineDebitBetId.ReadOnly = true;
      this.dgvColumnOfflineDebitBetId.Width = 75;
      // 
      // dgvColumnOfflineDebitBetValue
      // 
      this.dgvColumnOfflineDebitBetValue.DataPropertyName = "BetValue";
      this.dgvColumnOfflineDebitBetValue.HeaderText = "Bet Value";
      this.dgvColumnOfflineDebitBetValue.Name = "dgvColumnOfflineDebitBetValue";
      this.dgvColumnOfflineDebitBetValue.ReadOnly = true;
      this.dgvColumnOfflineDebitBetValue.Width = 74;
      // 
      // dgvColumnOfflineDebitBetRake
      // 
      this.dgvColumnOfflineDebitBetRake.DataPropertyName = "BetRake";
      this.dgvColumnOfflineDebitBetRake.HeaderText = "Bet Rake";
      this.dgvColumnOfflineDebitBetRake.Name = "dgvColumnOfflineDebitBetRake";
      this.dgvColumnOfflineDebitBetRake.ReadOnly = true;
      this.dgvColumnOfflineDebitBetRake.Width = 70;
      // 
      // dgvColumnOfflineDebitBetDelete
      // 
      this.dgvColumnOfflineDebitBetDelete.HeaderText = "Delete";
      this.dgvColumnOfflineDebitBetDelete.Name = "dgvColumnOfflineDebitBetDelete";
      this.dgvColumnOfflineDebitBetDelete.ReadOnly = true;
      this.dgvColumnOfflineDebitBetDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnOfflineDebitBetDelete.Text = "Delete";
      this.dgvColumnOfflineDebitBetDelete.ToolTipText = "Delete selected bet";
      this.dgvColumnOfflineDebitBetDelete.UseColumnTextForButtonValue = true;
      this.dgvColumnOfflineDebitBetDelete.Width = 65;
      // 
      // txtOfflineDebitRequestCurrency
      // 
      this.txtOfflineDebitRequestCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestCurrency.Location = new System.Drawing.Point(100, 173);
      this.txtOfflineDebitRequestCurrency.Name = "txtOfflineDebitRequestCurrency";
      this.txtOfflineDebitRequestCurrency.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestCurrency.TabIndex = 6;
      // 
      // lblOfflineDebitRequestCurrency
      // 
      this.lblOfflineDebitRequestCurrency.AutoSize = true;
      this.lblOfflineDebitRequestCurrency.Location = new System.Drawing.Point(7, 176);
      this.lblOfflineDebitRequestCurrency.Name = "lblOfflineDebitRequestCurrency";
      this.lblOfflineDebitRequestCurrency.Size = new System.Drawing.Size(49, 13);
      this.lblOfflineDebitRequestCurrency.TabIndex = 9;
      this.lblOfflineDebitRequestCurrency.Text = "Currency";
      // 
      // txtOfflineDebitRequestAmount
      // 
      this.txtOfflineDebitRequestAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestAmount.DecimalPlaces = 2;
      this.txtOfflineDebitRequestAmount.DecimalSeparator = ".";
      this.txtOfflineDebitRequestAmount.IsDecimal = true;
      this.txtOfflineDebitRequestAmount.Location = new System.Drawing.Point(100, 147);
      this.txtOfflineDebitRequestAmount.Name = "txtOfflineDebitRequestAmount";
      this.txtOfflineDebitRequestAmount.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestAmount.TabIndex = 5;
      // 
      // lblOfflineDebitRequestAmount
      // 
      this.lblOfflineDebitRequestAmount.AutoSize = true;
      this.lblOfflineDebitRequestAmount.Location = new System.Drawing.Point(7, 150);
      this.lblOfflineDebitRequestAmount.Name = "lblOfflineDebitRequestAmount";
      this.lblOfflineDebitRequestAmount.Size = new System.Drawing.Size(43, 13);
      this.lblOfflineDebitRequestAmount.TabIndex = 7;
      this.lblOfflineDebitRequestAmount.Text = "Amount";
      // 
      // btnSendOfflineDebitRequest
      // 
      this.btnSendOfflineDebitRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendOfflineDebitRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendOfflineDebitRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendOfflineDebitRequest.Location = new System.Drawing.Point(226, 494);
      this.btnSendOfflineDebitRequest.Name = "btnSendOfflineDebitRequest";
      this.btnSendOfflineDebitRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendOfflineDebitRequest.TabIndex = 10;
      this.btnSendOfflineDebitRequest.Text = "Send";
      this.btnSendOfflineDebitRequest.UseVisualStyleBackColor = true;
      this.btnSendOfflineDebitRequest.Click += new System.EventHandler(this.btnSendOfflineDebitRequest_Click);
      // 
      // txtOfflineDebitRequestGameInstanceId
      // 
      this.txtOfflineDebitRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtOfflineDebitRequestGameInstanceId.Location = new System.Drawing.Point(100, 121);
      this.txtOfflineDebitRequestGameInstanceId.Name = "txtOfflineDebitRequestGameInstanceId";
      this.txtOfflineDebitRequestGameInstanceId.Size = new System.Drawing.Size(206, 20);
      this.txtOfflineDebitRequestGameInstanceId.TabIndex = 4;
      // 
      // lblOfflineDebitRequestGameInstanceId
      // 
      this.lblOfflineDebitRequestGameInstanceId.AutoSize = true;
      this.lblOfflineDebitRequestGameInstanceId.Location = new System.Drawing.Point(7, 124);
      this.lblOfflineDebitRequestGameInstanceId.Name = "lblOfflineDebitRequestGameInstanceId";
      this.lblOfflineDebitRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblOfflineDebitRequestGameInstanceId.TabIndex = 2;
      this.lblOfflineDebitRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // splitContainer16
      // 
      this.splitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer16.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer16.Location = new System.Drawing.Point(0, 0);
      this.splitContainer16.Name = "splitContainer16";
      this.splitContainer16.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer16.Panel1
      // 
      this.splitContainer16.Panel1.Controls.Add(this.groupBox20);
      // 
      // splitContainer16.Panel2
      // 
      this.splitContainer16.Panel2.Controls.Add(this.groupBox28);
      this.splitContainer16.Size = new System.Drawing.Size(946, 567);
      this.splitContainer16.SplitterDistance = 276;
      this.splitContainer16.TabIndex = 2;
      // 
      // groupBox20
      // 
      this.groupBox20.Controls.Add(this.requestDataOfflineDebitRequest);
      this.groupBox20.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox20.Location = new System.Drawing.Point(0, 0);
      this.groupBox20.Name = "groupBox20";
      this.groupBox20.Size = new System.Drawing.Size(946, 276);
      this.groupBox20.TabIndex = 3;
      this.groupBox20.TabStop = false;
      this.groupBox20.Text = "Request XML data";
      // 
      // requestDataOfflineDebitRequest
      // 
      this.requestDataOfflineDebitRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataOfflineDebitRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataOfflineDebitRequest.Name = "requestDataOfflineDebitRequest";
      this.requestDataOfflineDebitRequest.Size = new System.Drawing.Size(940, 257);
      this.requestDataOfflineDebitRequest.TabIndex = 0;
      // 
      // groupBox28
      // 
      this.groupBox28.Controls.Add(this.responseDataOfflineDebitRequest);
      this.groupBox28.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox28.Location = new System.Drawing.Point(0, 0);
      this.groupBox28.Name = "groupBox28";
      this.groupBox28.Size = new System.Drawing.Size(946, 287);
      this.groupBox28.TabIndex = 2;
      this.groupBox28.TabStop = false;
      this.groupBox28.Text = "Response data";
      // 
      // responseDataOfflineDebitRequest
      // 
      this.responseDataOfflineDebitRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataOfflineDebitRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataOfflineDebitRequest.Name = "responseDataOfflineDebitRequest";
      this.responseDataOfflineDebitRequest.Size = new System.Drawing.Size(940, 268);
      this.responseDataOfflineDebitRequest.TabIndex = 0;
      // 
      // tabPageGameCreatedRequest
      // 
      this.tabPageGameCreatedRequest.Controls.Add(this.splitContainer17);
      this.tabPageGameCreatedRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageGameCreatedRequest.Name = "tabPageGameCreatedRequest";
      this.tabPageGameCreatedRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageGameCreatedRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageGameCreatedRequest.TabIndex = 9;
      this.tabPageGameCreatedRequest.Text = "Game Created Request";
      this.tabPageGameCreatedRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer17
      // 
      this.splitContainer17.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer17.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer17.Location = new System.Drawing.Point(3, 3);
      this.splitContainer17.Name = "splitContainer17";
      // 
      // splitContainer17.Panel1
      // 
      this.splitContainer17.Panel1.Controls.Add(this.groupBox30);
      // 
      // splitContainer17.Panel2
      // 
      this.splitContainer17.Panel2.Controls.Add(this.splitContainer18);
      this.splitContainer17.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer17.SplitterDistance = 275;
      this.splitContainer17.TabIndex = 3;
      // 
      // groupBox30
      // 
      this.groupBox30.Controls.Add(this.dtpGameCreatedRequestGameStarts);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestGameTitle);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestGameTitle);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestPassword);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestPassword);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestPartnerId);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestPartnerId);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestJackpot);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestJackpot);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestGameLogo);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestGameLogo);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestEntryCost);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestEntryCost);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestFirstPrize);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestFirstPrize);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestGameStarts);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestGameStarts);
      this.groupBox30.Controls.Add(this.btnBuildGameCreatedRequest);
      this.groupBox30.Controls.Add(this.btnSendGameCreatedRequest);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestGameInstanceId);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestGameInstanceId);
      this.groupBox30.Controls.Add(this.txtGameCreatedRequestGameId);
      this.groupBox30.Controls.Add(this.lblGameCreatedRequestGameId);
      this.groupBox30.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox30.Location = new System.Drawing.Point(0, 0);
      this.groupBox30.Name = "groupBox30";
      this.groupBox30.Size = new System.Drawing.Size(275, 567);
      this.groupBox30.TabIndex = 0;
      this.groupBox30.TabStop = false;
      this.groupBox30.Text = "Request data";
      // 
      // dtpGameCreatedRequestGameStarts
      // 
      this.dtpGameCreatedRequestGameStarts.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dtpGameCreatedRequestGameStarts.Location = new System.Drawing.Point(104, 149);
      this.dtpGameCreatedRequestGameStarts.Name = "dtpGameCreatedRequestGameStarts";
      this.dtpGameCreatedRequestGameStarts.Size = new System.Drawing.Size(163, 20);
      this.dtpGameCreatedRequestGameStarts.TabIndex = 5;
      this.dtpGameCreatedRequestGameStarts.ValueChanged += new System.EventHandler(this.dtpGameCreatedRequestGameStarts_ValueChanged);
      // 
      // txtGameCreatedRequestGameTitle
      // 
      this.txtGameCreatedRequestGameTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestGameTitle.Location = new System.Drawing.Point(104, 95);
      this.txtGameCreatedRequestGameTitle.Name = "txtGameCreatedRequestGameTitle";
      this.txtGameCreatedRequestGameTitle.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestGameTitle.TabIndex = 3;
      // 
      // lblGameCreatedRequestGameTitle
      // 
      this.lblGameCreatedRequestGameTitle.AutoSize = true;
      this.lblGameCreatedRequestGameTitle.Location = new System.Drawing.Point(7, 98);
      this.lblGameCreatedRequestGameTitle.Name = "lblGameCreatedRequestGameTitle";
      this.lblGameCreatedRequestGameTitle.Size = new System.Drawing.Size(58, 13);
      this.lblGameCreatedRequestGameTitle.TabIndex = 22;
      this.lblGameCreatedRequestGameTitle.Text = "Game Title";
      // 
      // txtGameCreatedRequestPassword
      // 
      this.txtGameCreatedRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestPassword.Location = new System.Drawing.Point(104, 43);
      this.txtGameCreatedRequestPassword.Name = "txtGameCreatedRequestPassword";
      this.txtGameCreatedRequestPassword.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestPassword.TabIndex = 1;
      // 
      // lblGameCreatedRequestPassword
      // 
      this.lblGameCreatedRequestPassword.AutoSize = true;
      this.lblGameCreatedRequestPassword.Location = new System.Drawing.Point(7, 46);
      this.lblGameCreatedRequestPassword.Name = "lblGameCreatedRequestPassword";
      this.lblGameCreatedRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblGameCreatedRequestPassword.TabIndex = 21;
      this.lblGameCreatedRequestPassword.Text = "Password";
      // 
      // txtGameCreatedRequestPartnerId
      // 
      this.txtGameCreatedRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestPartnerId.Location = new System.Drawing.Point(104, 17);
      this.txtGameCreatedRequestPartnerId.Name = "txtGameCreatedRequestPartnerId";
      this.txtGameCreatedRequestPartnerId.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestPartnerId.TabIndex = 0;
      // 
      // lblGameCreatedRequestPartnerId
      // 
      this.lblGameCreatedRequestPartnerId.AutoSize = true;
      this.lblGameCreatedRequestPartnerId.Location = new System.Drawing.Point(7, 20);
      this.lblGameCreatedRequestPartnerId.Name = "lblGameCreatedRequestPartnerId";
      this.lblGameCreatedRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblGameCreatedRequestPartnerId.TabIndex = 20;
      this.lblGameCreatedRequestPartnerId.Text = "Partner Id";
      // 
      // txtGameCreatedRequestJackpot
      // 
      this.txtGameCreatedRequestJackpot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestJackpot.DecimalPlaces = 2;
      this.txtGameCreatedRequestJackpot.DecimalSeparator = ".";
      this.txtGameCreatedRequestJackpot.IsDecimal = true;
      this.txtGameCreatedRequestJackpot.Location = new System.Drawing.Point(104, 280);
      this.txtGameCreatedRequestJackpot.Name = "txtGameCreatedRequestJackpot";
      this.txtGameCreatedRequestJackpot.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestJackpot.TabIndex = 10;
      // 
      // lblGameCreatedRequestJackpot
      // 
      this.lblGameCreatedRequestJackpot.AutoSize = true;
      this.lblGameCreatedRequestJackpot.Location = new System.Drawing.Point(7, 283);
      this.lblGameCreatedRequestJackpot.Name = "lblGameCreatedRequestJackpot";
      this.lblGameCreatedRequestJackpot.Size = new System.Drawing.Size(45, 13);
      this.lblGameCreatedRequestJackpot.TabIndex = 17;
      this.lblGameCreatedRequestJackpot.Text = "Jackpot";
      // 
      // txtGameCreatedRequestGameLogo
      // 
      this.txtGameCreatedRequestGameLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestGameLogo.Location = new System.Drawing.Point(104, 254);
      this.txtGameCreatedRequestGameLogo.Name = "txtGameCreatedRequestGameLogo";
      this.txtGameCreatedRequestGameLogo.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestGameLogo.TabIndex = 9;
      // 
      // lblGameCreatedRequestGameLogo
      // 
      this.lblGameCreatedRequestGameLogo.AutoSize = true;
      this.lblGameCreatedRequestGameLogo.Location = new System.Drawing.Point(7, 257);
      this.lblGameCreatedRequestGameLogo.Name = "lblGameCreatedRequestGameLogo";
      this.lblGameCreatedRequestGameLogo.Size = new System.Drawing.Size(62, 13);
      this.lblGameCreatedRequestGameLogo.TabIndex = 15;
      this.lblGameCreatedRequestGameLogo.Text = "Game Logo";
      // 
      // txtGameCreatedRequestEntryCost
      // 
      this.txtGameCreatedRequestEntryCost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestEntryCost.DecimalPlaces = 2;
      this.txtGameCreatedRequestEntryCost.DecimalSeparator = ".";
      this.txtGameCreatedRequestEntryCost.IsDecimal = true;
      this.txtGameCreatedRequestEntryCost.Location = new System.Drawing.Point(104, 228);
      this.txtGameCreatedRequestEntryCost.Name = "txtGameCreatedRequestEntryCost";
      this.txtGameCreatedRequestEntryCost.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestEntryCost.TabIndex = 8;
      // 
      // lblGameCreatedRequestEntryCost
      // 
      this.lblGameCreatedRequestEntryCost.AutoSize = true;
      this.lblGameCreatedRequestEntryCost.Location = new System.Drawing.Point(7, 231);
      this.lblGameCreatedRequestEntryCost.Name = "lblGameCreatedRequestEntryCost";
      this.lblGameCreatedRequestEntryCost.Size = new System.Drawing.Size(55, 13);
      this.lblGameCreatedRequestEntryCost.TabIndex = 13;
      this.lblGameCreatedRequestEntryCost.Text = "Entry Cost";
      // 
      // txtGameCreatedRequestFirstPrize
      // 
      this.txtGameCreatedRequestFirstPrize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestFirstPrize.DecimalPlaces = 2;
      this.txtGameCreatedRequestFirstPrize.DecimalSeparator = ".";
      this.txtGameCreatedRequestFirstPrize.IsDecimal = true;
      this.txtGameCreatedRequestFirstPrize.Location = new System.Drawing.Point(104, 202);
      this.txtGameCreatedRequestFirstPrize.Name = "txtGameCreatedRequestFirstPrize";
      this.txtGameCreatedRequestFirstPrize.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestFirstPrize.TabIndex = 7;
      // 
      // lblGameCreatedRequestFirstPrize
      // 
      this.lblGameCreatedRequestFirstPrize.AutoSize = true;
      this.lblGameCreatedRequestFirstPrize.Location = new System.Drawing.Point(7, 205);
      this.lblGameCreatedRequestFirstPrize.Name = "lblGameCreatedRequestFirstPrize";
      this.lblGameCreatedRequestFirstPrize.Size = new System.Drawing.Size(52, 13);
      this.lblGameCreatedRequestFirstPrize.TabIndex = 11;
      this.lblGameCreatedRequestFirstPrize.Text = "First Prize";
      // 
      // txtGameCreatedRequestGameStarts
      // 
      this.txtGameCreatedRequestGameStarts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestGameStarts.DecimalPlaces = 2;
      this.txtGameCreatedRequestGameStarts.DecimalSeparator = ".";
      this.txtGameCreatedRequestGameStarts.IsDecimal = false;
      this.txtGameCreatedRequestGameStarts.Location = new System.Drawing.Point(104, 176);
      this.txtGameCreatedRequestGameStarts.Name = "txtGameCreatedRequestGameStarts";
      this.txtGameCreatedRequestGameStarts.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestGameStarts.TabIndex = 6;
      // 
      // lblGameCreatedRequestGameStarts
      // 
      this.lblGameCreatedRequestGameStarts.AutoSize = true;
      this.lblGameCreatedRequestGameStarts.Location = new System.Drawing.Point(7, 150);
      this.lblGameCreatedRequestGameStarts.Name = "lblGameCreatedRequestGameStarts";
      this.lblGameCreatedRequestGameStarts.Size = new System.Drawing.Size(65, 13);
      this.lblGameCreatedRequestGameStarts.TabIndex = 9;
      this.lblGameCreatedRequestGameStarts.Text = "Game Starts";
      // 
      // btnBuildGameCreatedRequest
      // 
      this.btnBuildGameCreatedRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildGameCreatedRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildGameCreatedRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildGameCreatedRequest.Location = new System.Drawing.Point(101, 306);
      this.btnBuildGameCreatedRequest.Name = "btnBuildGameCreatedRequest";
      this.btnBuildGameCreatedRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildGameCreatedRequest.TabIndex = 11;
      this.btnBuildGameCreatedRequest.Text = "Build";
      this.btnBuildGameCreatedRequest.UseVisualStyleBackColor = true;
      this.btnBuildGameCreatedRequest.Click += new System.EventHandler(this.btnBuildGameCreatedRequest_Click);
      // 
      // btnSendGameCreatedRequest
      // 
      this.btnSendGameCreatedRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendGameCreatedRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendGameCreatedRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendGameCreatedRequest.Location = new System.Drawing.Point(187, 306);
      this.btnSendGameCreatedRequest.Name = "btnSendGameCreatedRequest";
      this.btnSendGameCreatedRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendGameCreatedRequest.TabIndex = 12;
      this.btnSendGameCreatedRequest.Text = "Send";
      this.btnSendGameCreatedRequest.UseVisualStyleBackColor = true;
      this.btnSendGameCreatedRequest.Click += new System.EventHandler(this.btnSendGameCreatedRequest_Click);
      // 
      // txtGameCreatedRequestGameInstanceId
      // 
      this.txtGameCreatedRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestGameInstanceId.Location = new System.Drawing.Point(104, 121);
      this.txtGameCreatedRequestGameInstanceId.Name = "txtGameCreatedRequestGameInstanceId";
      this.txtGameCreatedRequestGameInstanceId.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestGameInstanceId.TabIndex = 4;
      // 
      // lblGameCreatedRequestGameInstanceId
      // 
      this.lblGameCreatedRequestGameInstanceId.AutoSize = true;
      this.lblGameCreatedRequestGameInstanceId.Location = new System.Drawing.Point(7, 124);
      this.lblGameCreatedRequestGameInstanceId.Name = "lblGameCreatedRequestGameInstanceId";
      this.lblGameCreatedRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblGameCreatedRequestGameInstanceId.TabIndex = 4;
      this.lblGameCreatedRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // txtGameCreatedRequestGameId
      // 
      this.txtGameCreatedRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameCreatedRequestGameId.Location = new System.Drawing.Point(104, 69);
      this.txtGameCreatedRequestGameId.Name = "txtGameCreatedRequestGameId";
      this.txtGameCreatedRequestGameId.Size = new System.Drawing.Size(163, 20);
      this.txtGameCreatedRequestGameId.TabIndex = 2;
      // 
      // lblGameCreatedRequestGameId
      // 
      this.lblGameCreatedRequestGameId.AutoSize = true;
      this.lblGameCreatedRequestGameId.Location = new System.Drawing.Point(7, 72);
      this.lblGameCreatedRequestGameId.Name = "lblGameCreatedRequestGameId";
      this.lblGameCreatedRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblGameCreatedRequestGameId.TabIndex = 2;
      this.lblGameCreatedRequestGameId.Text = "Game Id";
      // 
      // splitContainer18
      // 
      this.splitContainer18.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer18.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer18.Location = new System.Drawing.Point(0, 0);
      this.splitContainer18.Name = "splitContainer18";
      this.splitContainer18.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer18.Panel1
      // 
      this.splitContainer18.Panel1.Controls.Add(this.groupBox31);
      // 
      // splitContainer18.Panel2
      // 
      this.splitContainer18.Panel2.Controls.Add(this.groupBox32);
      this.splitContainer18.Size = new System.Drawing.Size(985, 567);
      this.splitContainer18.SplitterDistance = 269;
      this.splitContainer18.TabIndex = 2;
      // 
      // groupBox31
      // 
      this.groupBox31.Controls.Add(this.requestDataGameCreatedRequest);
      this.groupBox31.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox31.Location = new System.Drawing.Point(0, 0);
      this.groupBox31.Name = "groupBox31";
      this.groupBox31.Size = new System.Drawing.Size(985, 269);
      this.groupBox31.TabIndex = 3;
      this.groupBox31.TabStop = false;
      this.groupBox31.Text = "Request XML data";
      // 
      // requestDataGameCreatedRequest
      // 
      this.requestDataGameCreatedRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataGameCreatedRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataGameCreatedRequest.Name = "requestDataGameCreatedRequest";
      this.requestDataGameCreatedRequest.Size = new System.Drawing.Size(979, 250);
      this.requestDataGameCreatedRequest.TabIndex = 0;
      // 
      // groupBox32
      // 
      this.groupBox32.Controls.Add(this.responseDataGameCreatedRequest);
      this.groupBox32.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox32.Location = new System.Drawing.Point(0, 0);
      this.groupBox32.Name = "groupBox32";
      this.groupBox32.Size = new System.Drawing.Size(985, 294);
      this.groupBox32.TabIndex = 2;
      this.groupBox32.TabStop = false;
      this.groupBox32.Text = "Response data";
      // 
      // responseDataGameCreatedRequest
      // 
      this.responseDataGameCreatedRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataGameCreatedRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataGameCreatedRequest.Name = "responseDataGameCreatedRequest";
      this.responseDataGameCreatedRequest.Size = new System.Drawing.Size(979, 275);
      this.responseDataGameCreatedRequest.TabIndex = 0;
      // 
      // tabPageGameStartingRequest
      // 
      this.tabPageGameStartingRequest.Controls.Add(this.splitContainer19);
      this.tabPageGameStartingRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageGameStartingRequest.Name = "tabPageGameStartingRequest";
      this.tabPageGameStartingRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageGameStartingRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageGameStartingRequest.TabIndex = 10;
      this.tabPageGameStartingRequest.Text = "Game Starting Request";
      this.tabPageGameStartingRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer19
      // 
      this.splitContainer19.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer19.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer19.Location = new System.Drawing.Point(3, 3);
      this.splitContainer19.Name = "splitContainer19";
      // 
      // splitContainer19.Panel1
      // 
      this.splitContainer19.Panel1.Controls.Add(this.groupBox33);
      // 
      // splitContainer19.Panel2
      // 
      this.splitContainer19.Panel2.Controls.Add(this.splitContainer20);
      this.splitContainer19.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer19.SplitterDistance = 282;
      this.splitContainer19.TabIndex = 4;
      // 
      // groupBox33
      // 
      this.groupBox33.Controls.Add(this.txtGameStartingRequestJackpot);
      this.groupBox33.Controls.Add(this.lblGameStartingRequestJackpot);
      this.groupBox33.Controls.Add(this.txtGameStartingRequestPassword);
      this.groupBox33.Controls.Add(this.lblGameStartingRequestPassword);
      this.groupBox33.Controls.Add(this.txtGameStartingRequestPartnerId);
      this.groupBox33.Controls.Add(this.lblGameStartingRequestPartnerId);
      this.groupBox33.Controls.Add(this.btnBuildGameStartingRequest);
      this.groupBox33.Controls.Add(this.btnSendGameStartingRequest);
      this.groupBox33.Controls.Add(this.txtGameStartingRequestGameInstanceId);
      this.groupBox33.Controls.Add(this.lblGameStartingRequestGameInstanceId);
      this.groupBox33.Controls.Add(this.txtGameStartingRequestGameId);
      this.groupBox33.Controls.Add(this.lblGameStartingRequestGameId);
      this.groupBox33.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox33.Location = new System.Drawing.Point(0, 0);
      this.groupBox33.Name = "groupBox33";
      this.groupBox33.Size = new System.Drawing.Size(282, 567);
      this.groupBox33.TabIndex = 1;
      this.groupBox33.TabStop = false;
      this.groupBox33.Text = "Request data";
      // 
      // txtGameStartingRequestPassword
      // 
      this.txtGameStartingRequestPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameStartingRequestPassword.Location = new System.Drawing.Point(104, 43);
      this.txtGameStartingRequestPassword.Name = "txtGameStartingRequestPassword";
      this.txtGameStartingRequestPassword.Size = new System.Drawing.Size(170, 20);
      this.txtGameStartingRequestPassword.TabIndex = 2;
      // 
      // lblGameStartingRequestPassword
      // 
      this.lblGameStartingRequestPassword.AutoSize = true;
      this.lblGameStartingRequestPassword.Location = new System.Drawing.Point(7, 46);
      this.lblGameStartingRequestPassword.Name = "lblGameStartingRequestPassword";
      this.lblGameStartingRequestPassword.Size = new System.Drawing.Size(53, 13);
      this.lblGameStartingRequestPassword.TabIndex = 26;
      this.lblGameStartingRequestPassword.Text = "Password";
      // 
      // txtGameStartingRequestPartnerId
      // 
      this.txtGameStartingRequestPartnerId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameStartingRequestPartnerId.Location = new System.Drawing.Point(104, 17);
      this.txtGameStartingRequestPartnerId.Name = "txtGameStartingRequestPartnerId";
      this.txtGameStartingRequestPartnerId.Size = new System.Drawing.Size(170, 20);
      this.txtGameStartingRequestPartnerId.TabIndex = 1;
      // 
      // lblGameStartingRequestPartnerId
      // 
      this.lblGameStartingRequestPartnerId.AutoSize = true;
      this.lblGameStartingRequestPartnerId.Location = new System.Drawing.Point(7, 20);
      this.lblGameStartingRequestPartnerId.Name = "lblGameStartingRequestPartnerId";
      this.lblGameStartingRequestPartnerId.Size = new System.Drawing.Size(53, 13);
      this.lblGameStartingRequestPartnerId.TabIndex = 25;
      this.lblGameStartingRequestPartnerId.Text = "Partner Id";
      // 
      // btnBuildGameStartingRequest
      // 
      this.btnBuildGameStartingRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildGameStartingRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildGameStartingRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildGameStartingRequest.Location = new System.Drawing.Point(108, 147);
      this.btnBuildGameStartingRequest.Name = "btnBuildGameStartingRequest";
      this.btnBuildGameStartingRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildGameStartingRequest.TabIndex = 5;
      this.btnBuildGameStartingRequest.Text = "Build";
      this.btnBuildGameStartingRequest.UseVisualStyleBackColor = true;
      this.btnBuildGameStartingRequest.Click += new System.EventHandler(this.btnBuildGameStartingRequest_Click);
      // 
      // btnSendGameStartingRequest
      // 
      this.btnSendGameStartingRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendGameStartingRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendGameStartingRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendGameStartingRequest.Location = new System.Drawing.Point(194, 147);
      this.btnSendGameStartingRequest.Name = "btnSendGameStartingRequest";
      this.btnSendGameStartingRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendGameStartingRequest.TabIndex = 6;
      this.btnSendGameStartingRequest.Text = "Send";
      this.btnSendGameStartingRequest.UseVisualStyleBackColor = true;
      this.btnSendGameStartingRequest.Click += new System.EventHandler(this.btnSendGameStartingRequest_Click);
      // 
      // txtGameStartingRequestGameInstanceId
      // 
      this.txtGameStartingRequestGameInstanceId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameStartingRequestGameInstanceId.Location = new System.Drawing.Point(104, 95);
      this.txtGameStartingRequestGameInstanceId.Name = "txtGameStartingRequestGameInstanceId";
      this.txtGameStartingRequestGameInstanceId.Size = new System.Drawing.Size(170, 20);
      this.txtGameStartingRequestGameInstanceId.TabIndex = 4;
      // 
      // lblGameStartingRequestGameInstanceId
      // 
      this.lblGameStartingRequestGameInstanceId.AutoSize = true;
      this.lblGameStartingRequestGameInstanceId.Location = new System.Drawing.Point(7, 98);
      this.lblGameStartingRequestGameInstanceId.Name = "lblGameStartingRequestGameInstanceId";
      this.lblGameStartingRequestGameInstanceId.Size = new System.Drawing.Size(91, 13);
      this.lblGameStartingRequestGameInstanceId.TabIndex = 4;
      this.lblGameStartingRequestGameInstanceId.Text = "Game Instance Id";
      // 
      // txtGameStartingRequestGameId
      // 
      this.txtGameStartingRequestGameId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameStartingRequestGameId.Location = new System.Drawing.Point(104, 69);
      this.txtGameStartingRequestGameId.Name = "txtGameStartingRequestGameId";
      this.txtGameStartingRequestGameId.Size = new System.Drawing.Size(170, 20);
      this.txtGameStartingRequestGameId.TabIndex = 3;
      // 
      // lblGameStartingRequestGameId
      // 
      this.lblGameStartingRequestGameId.AutoSize = true;
      this.lblGameStartingRequestGameId.Location = new System.Drawing.Point(7, 72);
      this.lblGameStartingRequestGameId.Name = "lblGameStartingRequestGameId";
      this.lblGameStartingRequestGameId.Size = new System.Drawing.Size(47, 13);
      this.lblGameStartingRequestGameId.TabIndex = 2;
      this.lblGameStartingRequestGameId.Text = "Game Id";
      // 
      // splitContainer20
      // 
      this.splitContainer20.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer20.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer20.Location = new System.Drawing.Point(0, 0);
      this.splitContainer20.Name = "splitContainer20";
      this.splitContainer20.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer20.Panel1
      // 
      this.splitContainer20.Panel1.Controls.Add(this.groupBox34);
      // 
      // splitContainer20.Panel2
      // 
      this.splitContainer20.Panel2.Controls.Add(this.groupBox35);
      this.splitContainer20.Size = new System.Drawing.Size(978, 567);
      this.splitContainer20.SplitterDistance = 205;
      this.splitContainer20.TabIndex = 2;
      // 
      // groupBox34
      // 
      this.groupBox34.Controls.Add(this.requestDataGameStartingRequest);
      this.groupBox34.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox34.Location = new System.Drawing.Point(0, 0);
      this.groupBox34.Name = "groupBox34";
      this.groupBox34.Size = new System.Drawing.Size(978, 205);
      this.groupBox34.TabIndex = 3;
      this.groupBox34.TabStop = false;
      this.groupBox34.Text = "Request XML data";
      // 
      // requestDataGameStartingRequest
      // 
      this.requestDataGameStartingRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataGameStartingRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataGameStartingRequest.Name = "requestDataGameStartingRequest";
      this.requestDataGameStartingRequest.Size = new System.Drawing.Size(972, 186);
      this.requestDataGameStartingRequest.TabIndex = 0;
      // 
      // groupBox35
      // 
      this.groupBox35.Controls.Add(this.responseDataGameStartingRequest);
      this.groupBox35.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox35.Location = new System.Drawing.Point(0, 0);
      this.groupBox35.Name = "groupBox35";
      this.groupBox35.Size = new System.Drawing.Size(978, 358);
      this.groupBox35.TabIndex = 2;
      this.groupBox35.TabStop = false;
      this.groupBox35.Text = "Response data";
      // 
      // responseDataGameStartingRequest
      // 
      this.responseDataGameStartingRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataGameStartingRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataGameStartingRequest.Name = "responseDataGameStartingRequest";
      this.responseDataGameStartingRequest.Size = new System.Drawing.Size(972, 339);
      this.responseDataGameStartingRequest.TabIndex = 0;
      // 
      // tabPageCustomXMLRequest
      // 
      this.tabPageCustomXMLRequest.Controls.Add(this.splitContainer3);
      this.tabPageCustomXMLRequest.Location = new System.Drawing.Point(4, 22);
      this.tabPageCustomXMLRequest.Name = "tabPageCustomXMLRequest";
      this.tabPageCustomXMLRequest.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageCustomXMLRequest.Size = new System.Drawing.Size(1270, 573);
      this.tabPageCustomXMLRequest.TabIndex = 11;
      this.tabPageCustomXMLRequest.Text = "Custom XML Request";
      this.tabPageCustomXMLRequest.UseVisualStyleBackColor = true;
      // 
      // splitContainer3
      // 
      this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer3.Location = new System.Drawing.Point(3, 3);
      this.splitContainer3.Name = "splitContainer3";
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add(this.groupBox5);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add(this.splitContainer11);
      this.splitContainer3.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer3.SplitterDistance = 511;
      this.splitContainer3.TabIndex = 5;
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.panelFreeXML);
      this.groupBox5.Controls.Add(this.panel1);
      this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox5.Location = new System.Drawing.Point(0, 0);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(511, 567);
      this.groupBox5.TabIndex = 0;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Request data";
      // 
      // panelFreeXML
      // 
      this.panelFreeXML.Controls.Add(this.groupBoxCustomXML);
      this.panelFreeXML.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelFreeXML.Location = new System.Drawing.Point(3, 16);
      this.panelFreeXML.Name = "panelFreeXML";
      this.panelFreeXML.Size = new System.Drawing.Size(505, 509);
      this.panelFreeXML.TabIndex = 0;
      // 
      // groupBoxCustomXML
      // 
      this.groupBoxCustomXML.Controls.Add(this.xmlEditorRequest);
      this.groupBoxCustomXML.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBoxCustomXML.Location = new System.Drawing.Point(0, 0);
      this.groupBoxCustomXML.Name = "groupBoxCustomXML";
      this.groupBoxCustomXML.Size = new System.Drawing.Size(505, 509);
      this.groupBoxCustomXML.TabIndex = 0;
      this.groupBoxCustomXML.TabStop = false;
      this.groupBoxCustomXML.Text = "XML Request Editor";
      // 
      // xmlEditorRequest
      // 
      this.xmlEditorRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.xmlEditorRequest.Location = new System.Drawing.Point(3, 16);
      this.xmlEditorRequest.Name = "xmlEditorRequest";
      this.xmlEditorRequest.Size = new System.Drawing.Size(499, 490);
      this.xmlEditorRequest.TabIndex = 0;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnSendCustomXMLRequest);
      this.panel1.Controls.Add(this.btnBuildCustomXMLRequest);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(3, 525);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(505, 39);
      this.panel1.TabIndex = 0;
      // 
      // btnSendCustomXMLRequest
      // 
      this.btnSendCustomXMLRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSendCustomXMLRequest.Image = global::GameGatewaySimulator.Properties.Resources.send_request;
      this.btnSendCustomXMLRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSendCustomXMLRequest.Location = new System.Drawing.Point(420, 7);
      this.btnSendCustomXMLRequest.Name = "btnSendCustomXMLRequest";
      this.btnSendCustomXMLRequest.Size = new System.Drawing.Size(80, 25);
      this.btnSendCustomXMLRequest.TabIndex = 1;
      this.btnSendCustomXMLRequest.Text = "Send";
      this.btnSendCustomXMLRequest.UseVisualStyleBackColor = true;
      this.btnSendCustomXMLRequest.Click += new System.EventHandler(this.btnSendCustomXMLRequest_Click);
      // 
      // btnBuildCustomXMLRequest
      // 
      this.btnBuildCustomXMLRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnBuildCustomXMLRequest.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnBuildCustomXMLRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnBuildCustomXMLRequest.Location = new System.Drawing.Point(334, 7);
      this.btnBuildCustomXMLRequest.Name = "btnBuildCustomXMLRequest";
      this.btnBuildCustomXMLRequest.Size = new System.Drawing.Size(80, 25);
      this.btnBuildCustomXMLRequest.TabIndex = 0;
      this.btnBuildCustomXMLRequest.Text = "Build";
      this.btnBuildCustomXMLRequest.UseVisualStyleBackColor = true;
      this.btnBuildCustomXMLRequest.Click += new System.EventHandler(this.btnBuildCustomXMLRequest_Click);
      // 
      // splitContainer11
      // 
      this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer11.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer11.Location = new System.Drawing.Point(0, 0);
      this.splitContainer11.Name = "splitContainer11";
      this.splitContainer11.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer11.Panel1
      // 
      this.splitContainer11.Panel1.Controls.Add(this.groupBox6);
      // 
      // splitContainer11.Panel2
      // 
      this.splitContainer11.Panel2.Controls.Add(this.groupBox23);
      this.splitContainer11.Size = new System.Drawing.Size(749, 567);
      this.splitContainer11.SplitterDistance = 205;
      this.splitContainer11.TabIndex = 2;
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.requestDataCustomXMLRequest);
      this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox6.Location = new System.Drawing.Point(0, 0);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(749, 205);
      this.groupBox6.TabIndex = 3;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = "Request XML data";
      // 
      // requestDataCustomXMLRequest
      // 
      this.requestDataCustomXMLRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.requestDataCustomXMLRequest.Location = new System.Drawing.Point(3, 16);
      this.requestDataCustomXMLRequest.Name = "requestDataCustomXMLRequest";
      this.requestDataCustomXMLRequest.Size = new System.Drawing.Size(743, 186);
      this.requestDataCustomXMLRequest.TabIndex = 0;
      // 
      // groupBox23
      // 
      this.groupBox23.Controls.Add(this.responseDataCustomXMLRequest);
      this.groupBox23.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox23.Location = new System.Drawing.Point(0, 0);
      this.groupBox23.Name = "groupBox23";
      this.groupBox23.Size = new System.Drawing.Size(749, 358);
      this.groupBox23.TabIndex = 2;
      this.groupBox23.TabStop = false;
      this.groupBox23.Text = "Response data";
      // 
      // responseDataCustomXMLRequest
      // 
      this.responseDataCustomXMLRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.responseDataCustomXMLRequest.Location = new System.Drawing.Point(3, 16);
      this.responseDataCustomXMLRequest.Name = "responseDataCustomXMLRequest";
      this.responseDataCustomXMLRequest.Size = new System.Drawing.Size(743, 339);
      this.responseDataCustomXMLRequest.TabIndex = 0;
      // 
      // tabPageSimulateConcurrentConnections
      // 
      this.tabPageSimulateConcurrentConnections.Controls.Add(this.splitContainer21);
      this.tabPageSimulateConcurrentConnections.Location = new System.Drawing.Point(4, 22);
      this.tabPageSimulateConcurrentConnections.Name = "tabPageSimulateConcurrentConnections";
      this.tabPageSimulateConcurrentConnections.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageSimulateConcurrentConnections.Size = new System.Drawing.Size(1270, 573);
      this.tabPageSimulateConcurrentConnections.TabIndex = 8;
      this.tabPageSimulateConcurrentConnections.Text = "Simulate Concurrent Connections";
      this.tabPageSimulateConcurrentConnections.UseVisualStyleBackColor = true;
      // 
      // splitContainer21
      // 
      this.splitContainer21.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer21.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitContainer21.Location = new System.Drawing.Point(3, 3);
      this.splitContainer21.Name = "splitContainer21";
      // 
      // splitContainer21.Panel1
      // 
      this.splitContainer21.Panel1.Controls.Add(this.gbConcurrentConnectionsParameters);
      // 
      // splitContainer21.Panel2
      // 
      this.splitContainer21.Panel2.Controls.Add(this.groupBox29);
      this.splitContainer21.Size = new System.Drawing.Size(1264, 567);
      this.splitContainer21.SplitterDistance = 305;
      this.splitContainer21.TabIndex = 2;
      // 
      // gbConcurrentConnectionsParameters
      // 
      this.gbConcurrentConnectionsParameters.Controls.Add(this.gbConcurrentConnectionsConsoleOutput);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.btnStopConcurrentConnections);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.cboConcurrentConnectionsPlayType);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.label5);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.txtConcurrentConnectionsDelayBetweenBets);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.txtConcurrentConnectionsNumberOfPlayers);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.btnRunConcurrentConnections);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.txtConcurrentConnectionsBetAmount);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.label2);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.label3);
      this.gbConcurrentConnectionsParameters.Controls.Add(this.label4);
      this.gbConcurrentConnectionsParameters.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbConcurrentConnectionsParameters.Location = new System.Drawing.Point(0, 0);
      this.gbConcurrentConnectionsParameters.Name = "gbConcurrentConnectionsParameters";
      this.gbConcurrentConnectionsParameters.Size = new System.Drawing.Size(305, 567);
      this.gbConcurrentConnectionsParameters.TabIndex = 1;
      this.gbConcurrentConnectionsParameters.TabStop = false;
      this.gbConcurrentConnectionsParameters.Text = "Parameters";
      // 
      // gbConcurrentConnectionsConsoleOutput
      // 
      this.gbConcurrentConnectionsConsoleOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gbConcurrentConnectionsConsoleOutput.Controls.Add(this.txtConcurrentConnectionsConsoleOutput);
      this.gbConcurrentConnectionsConsoleOutput.Location = new System.Drawing.Point(6, 153);
      this.gbConcurrentConnectionsConsoleOutput.Name = "gbConcurrentConnectionsConsoleOutput";
      this.gbConcurrentConnectionsConsoleOutput.Size = new System.Drawing.Size(293, 408);
      this.gbConcurrentConnectionsConsoleOutput.TabIndex = 11;
      this.gbConcurrentConnectionsConsoleOutput.TabStop = false;
      this.gbConcurrentConnectionsConsoleOutput.Text = "Console Output";
      // 
      // txtConcurrentConnectionsConsoleOutput
      // 
      this.txtConcurrentConnectionsConsoleOutput.BackColor = System.Drawing.Color.Black;
      this.txtConcurrentConnectionsConsoleOutput.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtConcurrentConnectionsConsoleOutput.ForeColor = System.Drawing.Color.White;
      this.txtConcurrentConnectionsConsoleOutput.Location = new System.Drawing.Point(3, 16);
      this.txtConcurrentConnectionsConsoleOutput.Multiline = true;
      this.txtConcurrentConnectionsConsoleOutput.Name = "txtConcurrentConnectionsConsoleOutput";
      this.txtConcurrentConnectionsConsoleOutput.ReadOnly = true;
      this.txtConcurrentConnectionsConsoleOutput.Size = new System.Drawing.Size(287, 389);
      this.txtConcurrentConnectionsConsoleOutput.TabIndex = 0;
      // 
      // btnStopConcurrentConnections
      // 
      this.btnStopConcurrentConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnStopConcurrentConnections.Enabled = false;
      this.btnStopConcurrentConnections.Image = global::GameGatewaySimulator.Properties.Resources.close;
      this.btnStopConcurrentConnections.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnStopConcurrentConnections.Location = new System.Drawing.Point(218, 122);
      this.btnStopConcurrentConnections.Name = "btnStopConcurrentConnections";
      this.btnStopConcurrentConnections.Size = new System.Drawing.Size(80, 25);
      this.btnStopConcurrentConnections.TabIndex = 6;
      this.btnStopConcurrentConnections.Text = "Stop";
      this.btnStopConcurrentConnections.UseVisualStyleBackColor = true;
      this.btnStopConcurrentConnections.Click += new System.EventHandler(this.btnStopConcurrentConnections_Click);
      // 
      // cboConcurrentConnectionsPlayType
      // 
      this.cboConcurrentConnectionsPlayType.BackColor = System.Drawing.SystemColors.Window;
      this.cboConcurrentConnectionsPlayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cboConcurrentConnectionsPlayType.FormattingEnabled = true;
      this.cboConcurrentConnectionsPlayType.Location = new System.Drawing.Point(131, 95);
      this.cboConcurrentConnectionsPlayType.Name = "cboConcurrentConnectionsPlayType";
      this.cboConcurrentConnectionsPlayType.Size = new System.Drawing.Size(167, 21);
      this.cboConcurrentConnectionsPlayType.TabIndex = 3;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(7, 98);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(54, 13);
      this.label5.TabIndex = 8;
      this.label5.Text = "Play Type";
      // 
      // txtConcurrentConnectionsDelayBetweenBets
      // 
      this.txtConcurrentConnectionsDelayBetweenBets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtConcurrentConnectionsDelayBetweenBets.DecimalPlaces = 2;
      this.txtConcurrentConnectionsDelayBetweenBets.DecimalSeparator = ".";
      this.txtConcurrentConnectionsDelayBetweenBets.IsDecimal = false;
      this.txtConcurrentConnectionsDelayBetweenBets.Location = new System.Drawing.Point(131, 43);
      this.txtConcurrentConnectionsDelayBetweenBets.Name = "txtConcurrentConnectionsDelayBetweenBets";
      this.txtConcurrentConnectionsDelayBetweenBets.Size = new System.Drawing.Size(167, 20);
      this.txtConcurrentConnectionsDelayBetweenBets.TabIndex = 1;
      // 
      // txtConcurrentConnectionsNumberOfPlayers
      // 
      this.txtConcurrentConnectionsNumberOfPlayers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtConcurrentConnectionsNumberOfPlayers.DecimalPlaces = 2;
      this.txtConcurrentConnectionsNumberOfPlayers.DecimalSeparator = ".";
      this.txtConcurrentConnectionsNumberOfPlayers.IsDecimal = false;
      this.txtConcurrentConnectionsNumberOfPlayers.Location = new System.Drawing.Point(131, 17);
      this.txtConcurrentConnectionsNumberOfPlayers.Name = "txtConcurrentConnectionsNumberOfPlayers";
      this.txtConcurrentConnectionsNumberOfPlayers.Size = new System.Drawing.Size(167, 20);
      this.txtConcurrentConnectionsNumberOfPlayers.TabIndex = 0;
      // 
      // btnRunConcurrentConnections
      // 
      this.btnRunConcurrentConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnRunConcurrentConnections.Image = global::GameGatewaySimulator.Properties.Resources.build;
      this.btnRunConcurrentConnections.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnRunConcurrentConnections.Location = new System.Drawing.Point(131, 122);
      this.btnRunConcurrentConnections.Name = "btnRunConcurrentConnections";
      this.btnRunConcurrentConnections.Size = new System.Drawing.Size(80, 25);
      this.btnRunConcurrentConnections.TabIndex = 5;
      this.btnRunConcurrentConnections.Text = "Run";
      this.btnRunConcurrentConnections.UseVisualStyleBackColor = true;
      this.btnRunConcurrentConnections.Click += new System.EventHandler(this.btnRunConcurrentConnections_Click);
      // 
      // txtConcurrentConnectionsBetAmount
      // 
      this.txtConcurrentConnectionsBetAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtConcurrentConnectionsBetAmount.DecimalPlaces = 2;
      this.txtConcurrentConnectionsBetAmount.DecimalSeparator = ".";
      this.txtConcurrentConnectionsBetAmount.IsDecimal = false;
      this.txtConcurrentConnectionsBetAmount.Location = new System.Drawing.Point(131, 69);
      this.txtConcurrentConnectionsBetAmount.Name = "txtConcurrentConnectionsBetAmount";
      this.txtConcurrentConnectionsBetAmount.Size = new System.Drawing.Size(167, 20);
      this.txtConcurrentConnectionsBetAmount.TabIndex = 2;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(7, 72);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(61, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Bet amount";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(7, 46);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(123, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Delay between bets (ms)";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(7, 20);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(92, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "Number of players";
      // 
      // groupBox29
      // 
      this.groupBox29.Controls.Add(this.tabControlConcurrentConnectionsResults);
      this.groupBox29.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox29.Location = new System.Drawing.Point(0, 0);
      this.groupBox29.Name = "groupBox29";
      this.groupBox29.Size = new System.Drawing.Size(955, 567);
      this.groupBox29.TabIndex = 2;
      this.groupBox29.TabStop = false;
      this.groupBox29.Text = "Results";
      // 
      // tabControlConcurrentConnectionsResults
      // 
      this.tabControlConcurrentConnectionsResults.Controls.Add(this.tabPageConcurrentConnectionsGeneralResults);
      this.tabControlConcurrentConnectionsResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControlConcurrentConnectionsResults.Location = new System.Drawing.Point(3, 16);
      this.tabControlConcurrentConnectionsResults.Name = "tabControlConcurrentConnectionsResults";
      this.tabControlConcurrentConnectionsResults.SelectedIndex = 0;
      this.tabControlConcurrentConnectionsResults.Size = new System.Drawing.Size(949, 548);
      this.tabControlConcurrentConnectionsResults.TabIndex = 0;
      // 
      // tabPageConcurrentConnectionsGeneralResults
      // 
      this.tabPageConcurrentConnectionsGeneralResults.Controls.Add(this.dgvConcurrentConnectionsGeneralResults);
      this.tabPageConcurrentConnectionsGeneralResults.Location = new System.Drawing.Point(4, 22);
      this.tabPageConcurrentConnectionsGeneralResults.Name = "tabPageConcurrentConnectionsGeneralResults";
      this.tabPageConcurrentConnectionsGeneralResults.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageConcurrentConnectionsGeneralResults.Size = new System.Drawing.Size(941, 522);
      this.tabPageConcurrentConnectionsGeneralResults.TabIndex = 0;
      this.tabPageConcurrentConnectionsGeneralResults.Text = "General Results";
      this.tabPageConcurrentConnectionsGeneralResults.UseVisualStyleBackColor = true;
      // 
      // dgvConcurrentConnectionsGeneralResults
      // 
      this.dgvConcurrentConnectionsGeneralResults.AllowUserToAddRows = false;
      this.dgvConcurrentConnectionsGeneralResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvConcurrentConnectionsGeneralResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnGeneralResultPlaySessionId,
            this.dgvColumnGeneralResultTotalServerCalls,
            this.dgvColumnGeneralResultTotalLogins,
            this.dgvColumnGeneralResultTotalBets,
            this.dgvColumnGeneralResultTotalWins,
            this.dgvColumnGeneralResultViewDetail});
      this.dgvConcurrentConnectionsGeneralResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvConcurrentConnectionsGeneralResults.Location = new System.Drawing.Point(3, 3);
      this.dgvConcurrentConnectionsGeneralResults.MultiSelect = false;
      this.dgvConcurrentConnectionsGeneralResults.Name = "dgvConcurrentConnectionsGeneralResults";
      this.dgvConcurrentConnectionsGeneralResults.ReadOnly = true;
      this.dgvConcurrentConnectionsGeneralResults.RowHeadersVisible = false;
      this.dgvConcurrentConnectionsGeneralResults.Size = new System.Drawing.Size(935, 516);
      this.dgvConcurrentConnectionsGeneralResults.TabIndex = 5;
      this.dgvConcurrentConnectionsGeneralResults.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvConcurrentConnectionsGeneralResults_CellClick);
      // 
      // dgvColumnGeneralResultPlaySessionId
      // 
      this.dgvColumnGeneralResultPlaySessionId.DataPropertyName = "PlaySessionId";
      this.dgvColumnGeneralResultPlaySessionId.HeaderText = "Play Session Id";
      this.dgvColumnGeneralResultPlaySessionId.Name = "dgvColumnGeneralResultPlaySessionId";
      this.dgvColumnGeneralResultPlaySessionId.ReadOnly = true;
      this.dgvColumnGeneralResultPlaySessionId.Width = 120;
      // 
      // dgvColumnGeneralResultTotalServerCalls
      // 
      this.dgvColumnGeneralResultTotalServerCalls.DataPropertyName = "TotalServerCalls";
      this.dgvColumnGeneralResultTotalServerCalls.HeaderText = "Total Server Calls";
      this.dgvColumnGeneralResultTotalServerCalls.Name = "dgvColumnGeneralResultTotalServerCalls";
      this.dgvColumnGeneralResultTotalServerCalls.ReadOnly = true;
      this.dgvColumnGeneralResultTotalServerCalls.Width = 120;
      // 
      // dgvColumnGeneralResultTotalLogins
      // 
      this.dgvColumnGeneralResultTotalLogins.DataPropertyName = "TotalLogins";
      this.dgvColumnGeneralResultTotalLogins.HeaderText = "Total Logins";
      this.dgvColumnGeneralResultTotalLogins.Name = "dgvColumnGeneralResultTotalLogins";
      this.dgvColumnGeneralResultTotalLogins.ReadOnly = true;
      this.dgvColumnGeneralResultTotalLogins.Width = 120;
      // 
      // dgvColumnGeneralResultTotalBets
      // 
      this.dgvColumnGeneralResultTotalBets.DataPropertyName = "TotalBets";
      this.dgvColumnGeneralResultTotalBets.HeaderText = "Total Bets";
      this.dgvColumnGeneralResultTotalBets.Name = "dgvColumnGeneralResultTotalBets";
      this.dgvColumnGeneralResultTotalBets.ReadOnly = true;
      this.dgvColumnGeneralResultTotalBets.Width = 120;
      // 
      // dgvColumnGeneralResultTotalWins
      // 
      this.dgvColumnGeneralResultTotalWins.DataPropertyName = "TotalWins";
      this.dgvColumnGeneralResultTotalWins.HeaderText = "Total Wins";
      this.dgvColumnGeneralResultTotalWins.Name = "dgvColumnGeneralResultTotalWins";
      this.dgvColumnGeneralResultTotalWins.ReadOnly = true;
      this.dgvColumnGeneralResultTotalWins.Width = 120;
      // 
      // dgvColumnGeneralResultViewDetail
      // 
      this.dgvColumnGeneralResultViewDetail.HeaderText = "More Details";
      this.dgvColumnGeneralResultViewDetail.Name = "dgvColumnGeneralResultViewDetail";
      this.dgvColumnGeneralResultViewDetail.ReadOnly = true;
      this.dgvColumnGeneralResultViewDetail.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvColumnGeneralResultViewDetail.Text = "More Details";
      this.dgvColumnGeneralResultViewDetail.ToolTipText = "More Details";
      this.dgvColumnGeneralResultViewDetail.UseColumnTextForButtonValue = true;
      this.dgvColumnGeneralResultViewDetail.Width = 120;
      // 
      // gbServerSettings
      // 
      this.gbServerSettings.Controls.Add(this.lblTestConnectionResult);
      this.gbServerSettings.Controls.Add(this.pbTestConnectionResult);
      this.gbServerSettings.Controls.Add(this.btnTestConnection);
      this.gbServerSettings.Controls.Add(this.txtServerUri);
      this.gbServerSettings.Controls.Add(this.lblServerUri);
      this.gbServerSettings.Dock = System.Windows.Forms.DockStyle.Top;
      this.gbServerSettings.Location = new System.Drawing.Point(0, 0);
      this.gbServerSettings.Name = "gbServerSettings";
      this.gbServerSettings.Size = new System.Drawing.Size(1284, 58);
      this.gbServerSettings.TabIndex = 1;
      this.gbServerSettings.TabStop = false;
      this.gbServerSettings.Text = "Server Settings";
      // 
      // lblTestConnectionResult
      // 
      this.lblTestConnectionResult.AutoSize = true;
      this.lblTestConnectionResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblTestConnectionResult.Location = new System.Drawing.Point(576, 25);
      this.lblTestConnectionResult.Name = "lblTestConnectionResult";
      this.lblTestConnectionResult.Size = new System.Drawing.Size(163, 18);
      this.lblTestConnectionResult.TabIndex = 18;
      this.lblTestConnectionResult.Text = "Test Connection Result";
      this.lblTestConnectionResult.Visible = false;
      // 
      // pbTestConnectionResult
      // 
      this.pbTestConnectionResult.Image = global::GameGatewaySimulator.Properties.Resources.test_connection_failed;
      this.pbTestConnectionResult.Location = new System.Drawing.Point(546, 22);
      this.pbTestConnectionResult.Name = "pbTestConnectionResult";
      this.pbTestConnectionResult.Size = new System.Drawing.Size(24, 24);
      this.pbTestConnectionResult.TabIndex = 17;
      this.pbTestConnectionResult.TabStop = false;
      this.pbTestConnectionResult.Visible = false;
      // 
      // btnTestConnection
      // 
      this.btnTestConnection.Image = global::GameGatewaySimulator.Properties.Resources.test_connection;
      this.btnTestConnection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnTestConnection.Location = new System.Drawing.Point(400, 22);
      this.btnTestConnection.Name = "btnTestConnection";
      this.btnTestConnection.Size = new System.Drawing.Size(132, 24);
      this.btnTestConnection.TabIndex = 2;
      this.btnTestConnection.Text = "Test Connection";
      this.btnTestConnection.UseVisualStyleBackColor = true;
      this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
      // 
      // txtServerUri
      // 
      this.txtServerUri.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtServerUri.Location = new System.Drawing.Point(92, 22);
      this.txtServerUri.Name = "txtServerUri";
      this.txtServerUri.Size = new System.Drawing.Size(302, 24);
      this.txtServerUri.TabIndex = 1;
      this.txtServerUri.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtServerUri_KeyPress);
      // 
      // lblServerUri
      // 
      this.lblServerUri.AutoSize = true;
      this.lblServerUri.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblServerUri.Location = new System.Drawing.Point(12, 25);
      this.lblServerUri.Name = "lblServerUri";
      this.lblServerUri.Size = new System.Drawing.Size(74, 18);
      this.lblServerUri.TabIndex = 0;
      this.lblServerUri.Text = "Server Uri";
      // 
      // panelBottom
      // 
      this.panelBottom.Controls.Add(this.lblSelectedPlaySession);
      this.panelBottom.Controls.Add(this.label1);
      this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panelBottom.Location = new System.Drawing.Point(0, 676);
      this.panelBottom.Name = "panelBottom";
      this.panelBottom.Size = new System.Drawing.Size(1284, 36);
      this.panelBottom.TabIndex = 1;
      // 
      // lblSelectedPlaySession
      // 
      this.lblSelectedPlaySession.AutoSize = true;
      this.lblSelectedPlaySession.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblSelectedPlaySession.Location = new System.Drawing.Point(169, 9);
      this.lblSelectedPlaySession.Name = "lblSelectedPlaySession";
      this.lblSelectedPlaySession.Size = new System.Drawing.Size(0, 15);
      this.lblSelectedPlaySession.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(10, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(153, 15);
      this.label1.TabIndex = 0;
      this.label1.Text = "Selected Play Session:";
      // 
      // lblGameStartingRequestJackpot
      // 
      this.lblGameStartingRequestJackpot.AutoSize = true;
      this.lblGameStartingRequestJackpot.Location = new System.Drawing.Point(7, 124);
      this.lblGameStartingRequestJackpot.Name = "lblGameStartingRequestJackpot";
      this.lblGameStartingRequestJackpot.Size = new System.Drawing.Size(45, 13);
      this.lblGameStartingRequestJackpot.TabIndex = 28;
      this.lblGameStartingRequestJackpot.Text = "Jackpot";
      // 
      // txtGameStartingRequestJackpot
      // 
      this.txtGameStartingRequestJackpot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtGameStartingRequestJackpot.DecimalPlaces = 2;
      this.txtGameStartingRequestJackpot.DecimalSeparator = ".";
      this.txtGameStartingRequestJackpot.IsDecimal = true;
      this.txtGameStartingRequestJackpot.Location = new System.Drawing.Point(104, 121);
      this.txtGameStartingRequestJackpot.Name = "txtGameStartingRequestJackpot";
      this.txtGameStartingRequestJackpot.Size = new System.Drawing.Size(170, 20);
      this.txtGameStartingRequestJackpot.TabIndex = 29;
      // 
      // LCDSimulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1284, 712);
      this.Controls.Add(this.panelContent);
      this.Controls.Add(this.panelBottom);
      this.Name = "LCDSimulator";
      this.Text = "LCD Simulator";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LCDSimulator_FormClosed);
      this.Load += new System.EventHandler(this.LCDSimulator_Load);
      this.panelContent.ResumeLayout(false);
      this.gbGatewaySimulator.ResumeLayout(false);
      this.tabControlGateway.ResumeLayout(false);
      this.tabPageLoginRequest.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.splitContainer9.Panel1.ResumeLayout(false);
      this.splitContainer9.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
      this.splitContainer9.ResumeLayout(false);
      this.groupBox21.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.tabPageDebitRequest.ResumeLayout(false);
      this.splitContainer4.Panel1.ResumeLayout(false);
      this.splitContainer4.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
      this.splitContainer4.ResumeLayout(false);
      this.groupBox7.ResumeLayout(false);
      this.groupBox7.PerformLayout();
      this.gbDebitRequestBets.ResumeLayout(false);
      this.gbDebitRequestBets.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvDebitRequestBets)).EndInit();
      this.splitContainer12.Panel1.ResumeLayout(false);
      this.splitContainer12.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
      this.splitContainer12.ResumeLayout(false);
      this.groupBox8.ResumeLayout(false);
      this.groupBox24.ResumeLayout(false);
      this.tabPageBulkCreditRequest.ResumeLayout(false);
      this.splitContainer6.Panel1.ResumeLayout(false);
      this.splitContainer6.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
      this.splitContainer6.ResumeLayout(false);
      this.groupBox12.ResumeLayout(false);
      this.groupBox12.PerformLayout();
      this.groupBox13.ResumeLayout(false);
      this.groupBox13.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvBulkCreditRequestCredits)).EndInit();
      this.splitContainer14.Panel1.ResumeLayout(false);
      this.splitContainer14.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).EndInit();
      this.splitContainer14.ResumeLayout(false);
      this.groupBox14.ResumeLayout(false);
      this.groupBox26.ResumeLayout(false);
      this.tabPageLogoutRequest.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.splitContainer10.Panel1.ResumeLayout(false);
      this.splitContainer10.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
      this.splitContainer10.ResumeLayout(false);
      this.groupBox4.ResumeLayout(false);
      this.groupBox22.ResumeLayout(false);
      this.tabPageOfflineCreditRequest.ResumeLayout(false);
      this.splitContainer7.Panel1.ResumeLayout(false);
      this.splitContainer7.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
      this.splitContainer7.ResumeLayout(false);
      this.groupBox15.ResumeLayout(false);
      this.groupBox15.PerformLayout();
      this.groupBox16.ResumeLayout(false);
      this.groupBox16.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvOfflineCreditRequestBets)).EndInit();
      this.splitContainer15.Panel1.ResumeLayout(false);
      this.splitContainer15.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
      this.splitContainer15.ResumeLayout(false);
      this.groupBox17.ResumeLayout(false);
      this.groupBox27.ResumeLayout(false);
      this.tabPageCreditRequest.ResumeLayout(false);
      this.splitContainer5.Panel1.ResumeLayout(false);
      this.splitContainer5.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
      this.splitContainer5.ResumeLayout(false);
      this.groupBox9.ResumeLayout(false);
      this.groupBox9.PerformLayout();
      this.groupBox10.ResumeLayout(false);
      this.groupBox10.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvCreditRequestBets)).EndInit();
      this.splitContainer13.Panel1.ResumeLayout(false);
      this.splitContainer13.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).EndInit();
      this.splitContainer13.ResumeLayout(false);
      this.groupBox11.ResumeLayout(false);
      this.groupBox25.ResumeLayout(false);
      this.tabPageOfflineDebitRequest.ResumeLayout(false);
      this.splitContainer8.Panel1.ResumeLayout(false);
      this.splitContainer8.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
      this.splitContainer8.ResumeLayout(false);
      this.groupBox18.ResumeLayout(false);
      this.groupBox18.PerformLayout();
      this.groupBox19.ResumeLayout(false);
      this.groupBox19.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvOfflineDebitRequestBets)).EndInit();
      this.splitContainer16.Panel1.ResumeLayout(false);
      this.splitContainer16.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).EndInit();
      this.splitContainer16.ResumeLayout(false);
      this.groupBox20.ResumeLayout(false);
      this.groupBox28.ResumeLayout(false);
      this.tabPageGameCreatedRequest.ResumeLayout(false);
      this.splitContainer17.Panel1.ResumeLayout(false);
      this.splitContainer17.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).EndInit();
      this.splitContainer17.ResumeLayout(false);
      this.groupBox30.ResumeLayout(false);
      this.groupBox30.PerformLayout();
      this.splitContainer18.Panel1.ResumeLayout(false);
      this.splitContainer18.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).EndInit();
      this.splitContainer18.ResumeLayout(false);
      this.groupBox31.ResumeLayout(false);
      this.groupBox32.ResumeLayout(false);
      this.tabPageGameStartingRequest.ResumeLayout(false);
      this.splitContainer19.Panel1.ResumeLayout(false);
      this.splitContainer19.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).EndInit();
      this.splitContainer19.ResumeLayout(false);
      this.groupBox33.ResumeLayout(false);
      this.groupBox33.PerformLayout();
      this.splitContainer20.Panel1.ResumeLayout(false);
      this.splitContainer20.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).EndInit();
      this.splitContainer20.ResumeLayout(false);
      this.groupBox34.ResumeLayout(false);
      this.groupBox35.ResumeLayout(false);
      this.tabPageCustomXMLRequest.ResumeLayout(false);
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
      this.splitContainer3.ResumeLayout(false);
      this.groupBox5.ResumeLayout(false);
      this.panelFreeXML.ResumeLayout(false);
      this.groupBoxCustomXML.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.splitContainer11.Panel1.ResumeLayout(false);
      this.splitContainer11.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
      this.splitContainer11.ResumeLayout(false);
      this.groupBox6.ResumeLayout(false);
      this.groupBox23.ResumeLayout(false);
      this.tabPageSimulateConcurrentConnections.ResumeLayout(false);
      this.splitContainer21.Panel1.ResumeLayout(false);
      this.splitContainer21.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).EndInit();
      this.splitContainer21.ResumeLayout(false);
      this.gbConcurrentConnectionsParameters.ResumeLayout(false);
      this.gbConcurrentConnectionsParameters.PerformLayout();
      this.gbConcurrentConnectionsConsoleOutput.ResumeLayout(false);
      this.gbConcurrentConnectionsConsoleOutput.PerformLayout();
      this.groupBox29.ResumeLayout(false);
      this.tabControlConcurrentConnectionsResults.ResumeLayout(false);
      this.tabPageConcurrentConnectionsGeneralResults.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvConcurrentConnectionsGeneralResults)).EndInit();
      this.gbServerSettings.ResumeLayout(false);
      this.gbServerSettings.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbTestConnectionResult)).EndInit();
      this.panelBottom.ResumeLayout(false);
      this.panelBottom.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelBottom;
    private System.Windows.Forms.Panel panelContent;
    private System.Windows.Forms.Label lblSelectedPlaySession;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TabControl tabControlGateway;
    private System.Windows.Forms.TabPage tabPageLoginRequest;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button btnBuildLoginRequest;
    private System.Windows.Forms.Button btnSendLoginRequest;
    private System.Windows.Forms.TextBox txtLoginRequestPassword;
    private System.Windows.Forms.Label lblLoginRequestPassword;
    private System.Windows.Forms.TextBox txtLoginRequestPartnerId;
    private System.Windows.Forms.Label lblLoginRequestPartnerId;
    private System.Windows.Forms.TextBox txtLoginRequestSessionId;
    private System.Windows.Forms.Label lblLoginRequestSessionId;
    private System.Windows.Forms.SplitContainer splitContainer9;
    private System.Windows.Forms.GroupBox groupBox21;
    private UserControls.RequestData requestDataLoginRequest;
    private System.Windows.Forms.GroupBox groupBox1;
    private UserControls.ResponseData responseDataLoginRequest;
    private System.Windows.Forms.TabPage tabPageDebitRequest;
    private System.Windows.Forms.SplitContainer splitContainer4;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.Button btnBuildDebitRequest;
    private System.Windows.Forms.TextBox txtDebitRequestPassword;
    private System.Windows.Forms.Label lblDebitRequestPassword;
    private System.Windows.Forms.TextBox txtDebitRequestPartnerId;
    private System.Windows.Forms.Label lblDebitRequestPartnerId;
    private System.Windows.Forms.GroupBox gbDebitRequestBets;
    private System.Windows.Forms.Button btnDebitRequestAddBet;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtDebitRequestBetRake;
    private System.Windows.Forms.Label lblDebitRequestBetRake;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtDebitRequestBetValue;
    private System.Windows.Forms.Label lblDebitRequestBetValue;
    private System.Windows.Forms.TextBox txtDebitRequestBetId;
    private System.Windows.Forms.Label lblDebitRequestBetId;
    private System.Windows.Forms.DataGridView dgvDebitRequestBets;
    private System.Windows.Forms.TextBox txtDebitRequestCurrency;
    private System.Windows.Forms.Label lblDebitRequestCurrency;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtDebitRequestAmount;
    private System.Windows.Forms.Label lblDebitRequestAmount;
    private System.Windows.Forms.Button btnSendDebitRequest;
    private System.Windows.Forms.TextBox txtDebitRequestNumberOfBets;
    private System.Windows.Forms.Label lblDebitRequestNumberOfBets;
    private System.Windows.Forms.TextBox txtDebitRequestGameInstanceId;
    private System.Windows.Forms.Label lblDebitRequestGameInstanceId;
    private System.Windows.Forms.TextBox txtDebitRequestSessionId;
    private System.Windows.Forms.Label lblDebitRequestSessionId;
    private System.Windows.Forms.SplitContainer splitContainer12;
    private System.Windows.Forms.GroupBox groupBox8;
    private UserControls.RequestData requestDataDebitRequest;
    private System.Windows.Forms.GroupBox groupBox24;
    private UserControls.ResponseData responseDataDebitRequest;
    private System.Windows.Forms.TabPage tabPageBulkCreditRequest;
    private System.Windows.Forms.SplitContainer splitContainer6;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.Button btnBuildBulkCreditRequest;
    private System.Windows.Forms.CheckBox chkBulkCreditRequestRollback;
    private System.Windows.Forms.TextBox txtBulkCreditRequestPassword;
    private System.Windows.Forms.Label lblBulkCreditRequestPassword;
    private System.Windows.Forms.TextBox txtBulkCreditRequestPartnerId;
    private System.Windows.Forms.Label lblBulkCreditRequestPartnerId;
    private System.Windows.Forms.GroupBox groupBox13;
    private System.Windows.Forms.Button btnBulkCreditRequestAddCredit;
    private System.Windows.Forms.TextBox txtBulkCreditRequestCreditUserId;
    private System.Windows.Forms.Label lblBulkCreditRequestBetUserId;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtBulkCreditRequestCreditAmount;
    private System.Windows.Forms.Label lblBulkCreditRequestCreditAmount;
    private System.Windows.Forms.TextBox txtBulkCreditRequestCreditBetId;
    private System.Windows.Forms.Label lblBulkCreditRequestBetId;
    private System.Windows.Forms.DataGridView dgvBulkCreditRequestCredits;
    private System.Windows.Forms.TextBox txtBulkCreditRequestCurrency;
    private System.Windows.Forms.Label lblBulkCreditRequestCurrency;
    private System.Windows.Forms.Button btnSendBulkCreditRequest;
    private System.Windows.Forms.Label lblBulkCreditRequestRollback;
    private System.Windows.Forms.TextBox txtBulkCreditRequestGameInstanceId;
    private System.Windows.Forms.Label lblBulkCreditRequestGameInstanceId;
    private System.Windows.Forms.SplitContainer splitContainer14;
    private System.Windows.Forms.GroupBox groupBox14;
    private UserControls.RequestData requestDataBulkCreditRequest;
    private System.Windows.Forms.GroupBox groupBox26;
    private UserControls.ResponseData responseDataBulkCreditRequest;
    private System.Windows.Forms.TabPage tabPageLogoutRequest;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Button btnBuildLogoutRequest;
    private System.Windows.Forms.Button btnSendLogoutRequest;
    private System.Windows.Forms.TextBox txtLogoutRequestPassword;
    private System.Windows.Forms.Label lblLogoutRequestPassword;
    private System.Windows.Forms.TextBox txtLogoutRequestPartnerId;
    private System.Windows.Forms.Label lblLogoutRequestPartnerId;
    private System.Windows.Forms.TextBox txtLogoutRequestSessionId;
    private System.Windows.Forms.Label lblLogoutRequestSessionId;
    private System.Windows.Forms.SplitContainer splitContainer10;
    private System.Windows.Forms.GroupBox groupBox4;
    private UserControls.RequestData requestDataLogoutRequest;
    private System.Windows.Forms.GroupBox groupBox22;
    private UserControls.ResponseData responseDataLogoutRequest;
    private System.Windows.Forms.TabPage tabPageOfflineCreditRequest;
    private System.Windows.Forms.SplitContainer splitContainer7;
    private System.Windows.Forms.GroupBox groupBox15;
    private System.Windows.Forms.Button btnBuildOfflineCreditRequest;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestPassword;
    private System.Windows.Forms.Label lblOfflineCreditRequestPassword;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestPartnerId;
    private System.Windows.Forms.Label lblOfflineCreditRequestPartnerId;
    private System.Windows.Forms.GroupBox groupBox16;
    private System.Windows.Forms.Button btnOfflineCreditRequestAddBet;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineCreditRequestBetAmount;
    private System.Windows.Forms.Label lblOfflineCreditRequestBetAmount;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestBetId;
    private System.Windows.Forms.Label lblOfflineCreditRequestBetId;
    private System.Windows.Forms.DataGridView dgvOfflineCreditRequestBets;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestCurrency;
    private System.Windows.Forms.Label lblOfflineCreditRequestCurrency;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineCreditRequestAmount;
    private System.Windows.Forms.Label lblOfflineCreditRequestAmount;
    private System.Windows.Forms.Button btnSendOfflineCreditRequest;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestGameInstanceId;
    private System.Windows.Forms.Label lblOfflineCreditRequestGameInstanceId;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestUserId;
    private System.Windows.Forms.Label lblOfflineCreditRequestUserId;
    private System.Windows.Forms.SplitContainer splitContainer15;
    private System.Windows.Forms.GroupBox groupBox17;
    private UserControls.RequestData requestDataOfflineCreditRequest;
    private System.Windows.Forms.GroupBox groupBox27;
    private UserControls.ResponseData responseDataOfflineCreditRequest;
    private System.Windows.Forms.TabPage tabPageCreditRequest;
    private System.Windows.Forms.SplitContainer splitContainer5;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.Button btnBuildCreditRequest;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.Button btnCreditRequestAddBet;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtCreditRequestBetAmount;
    private System.Windows.Forms.Label lblCreditRequestBetAmount;
    private System.Windows.Forms.TextBox txtCreditRequestBetId;
    private System.Windows.Forms.Label lblCreditRequestBetId;
    private System.Windows.Forms.DataGridView dgvCreditRequestBets;
    private System.Windows.Forms.TextBox txtCreditRequestCurrency;
    private System.Windows.Forms.Label lblCreditRequestCurrency;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtCreditRequestAmount;
    private System.Windows.Forms.Label lblCreditRequestAmount;
    private System.Windows.Forms.Button btnSendCreditRequest;
    private System.Windows.Forms.TextBox txtCreditRequestGameInstanceId;
    private System.Windows.Forms.Label lblCreditRequestGameInstanceId;
    private System.Windows.Forms.TextBox txtCreditRequestSessionId;
    private System.Windows.Forms.Label lblCreditRequestSessionId;
    private System.Windows.Forms.SplitContainer splitContainer13;
    private System.Windows.Forms.GroupBox groupBox11;
    private UserControls.RequestData requestDataCreditRequest;
    private System.Windows.Forms.GroupBox groupBox25;
    private UserControls.ResponseData responseDataCreditRequest;
    private System.Windows.Forms.TabPage tabPageOfflineDebitRequest;
    private System.Windows.Forms.SplitContainer splitContainer8;
    private System.Windows.Forms.GroupBox groupBox18;
    private System.Windows.Forms.Button btnBuildOfflineDebitRequest;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestUserId;
    private System.Windows.Forms.Label lblOfflineDebitRequestUserId;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestPassword;
    private System.Windows.Forms.Label lblOfflineDebitRequestPassword;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestPartnerId;
    private System.Windows.Forms.Label lblOfflineDebitRequestPartnerId;
    private System.Windows.Forms.GroupBox groupBox19;
    private System.Windows.Forms.Button btnOfflineDebitRequestAddBet;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineDebitRequestBetRake;
    private System.Windows.Forms.Label lblOfflineDebitRequestBetRake;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineDebitRequestBetValue;
    private System.Windows.Forms.Label lblOfflineDebitRequestBetValue;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestBetId;
    private System.Windows.Forms.Label lblOfflineDebitRequestBetId;
    private System.Windows.Forms.DataGridView dgvOfflineDebitRequestBets;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestCurrency;
    private System.Windows.Forms.Label lblOfflineDebitRequestCurrency;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineDebitRequestAmount;
    private System.Windows.Forms.Label lblOfflineDebitRequestAmount;
    private System.Windows.Forms.Button btnSendOfflineDebitRequest;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestGameInstanceId;
    private System.Windows.Forms.Label lblOfflineDebitRequestGameInstanceId;
    private System.Windows.Forms.SplitContainer splitContainer16;
    private System.Windows.Forms.GroupBox groupBox20;
    private UserControls.RequestData requestDataOfflineDebitRequest;
    private System.Windows.Forms.GroupBox groupBox28;
    private UserControls.ResponseData responseDataOfflineDebitRequest;
    private System.Windows.Forms.TabPage tabPageSimulateConcurrentConnections;
    private System.Windows.Forms.CheckBox chkCreditRequestBonus;
    private System.Windows.Forms.Label lblCreditRequestBonus;
    private System.Windows.Forms.TabPage tabPageGameCreatedRequest;
    private System.Windows.Forms.SplitContainer splitContainer17;
    private System.Windows.Forms.GroupBox groupBox30;
    private System.Windows.Forms.Button btnBuildGameCreatedRequest;
    private System.Windows.Forms.Button btnSendGameCreatedRequest;
    private System.Windows.Forms.TextBox txtGameCreatedRequestGameInstanceId;
    private System.Windows.Forms.Label lblGameCreatedRequestGameInstanceId;
    private System.Windows.Forms.TextBox txtGameCreatedRequestGameId;
    private System.Windows.Forms.Label lblGameCreatedRequestGameId;
    private System.Windows.Forms.SplitContainer splitContainer18;
    private System.Windows.Forms.GroupBox groupBox31;
    private UserControls.RequestData requestDataGameCreatedRequest;
    private System.Windows.Forms.GroupBox groupBox32;
    private UserControls.ResponseData responseDataGameCreatedRequest;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtGameCreatedRequestJackpot;
    private System.Windows.Forms.Label lblGameCreatedRequestJackpot;
    private System.Windows.Forms.TextBox txtGameCreatedRequestGameLogo;
    private System.Windows.Forms.Label lblGameCreatedRequestGameLogo;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtGameCreatedRequestEntryCost;
    private System.Windows.Forms.Label lblGameCreatedRequestEntryCost;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtGameCreatedRequestFirstPrize;
    private System.Windows.Forms.Label lblGameCreatedRequestFirstPrize;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtGameCreatedRequestGameStarts;
    private System.Windows.Forms.Label lblGameCreatedRequestGameStarts;
    private System.Windows.Forms.TabPage tabPageGameStartingRequest;
    private System.Windows.Forms.SplitContainer splitContainer19;
    private System.Windows.Forms.GroupBox groupBox33;
    private System.Windows.Forms.Button btnBuildGameStartingRequest;
    private System.Windows.Forms.Button btnSendGameStartingRequest;
    private System.Windows.Forms.TextBox txtGameStartingRequestGameInstanceId;
    private System.Windows.Forms.Label lblGameStartingRequestGameInstanceId;
    private System.Windows.Forms.TextBox txtGameStartingRequestGameId;
    private System.Windows.Forms.Label lblGameStartingRequestGameId;
    private System.Windows.Forms.SplitContainer splitContainer20;
    private System.Windows.Forms.GroupBox groupBox34;
    private UserControls.RequestData requestDataGameStartingRequest;
    private System.Windows.Forms.GroupBox groupBox35;
    private UserControls.ResponseData responseDataGameStartingRequest;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnDebitBetId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnDebitBetValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnDebitBetRake;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnDebitBetDelete;
    private System.Windows.Forms.TextBox txtCreditRequestPassword;
    private System.Windows.Forms.Label lblCreditRequestPassword;
    private System.Windows.Forms.TextBox txtCreditRequestPartnerId;
    private System.Windows.Forms.Label lblCreditRequestPartnerId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineDebitBetId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineDebitBetValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineDebitBetRake;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnOfflineDebitBetDelete;
    private System.Windows.Forms.GroupBox gbServerSettings;
    private System.Windows.Forms.TextBox txtServerUri;
    private System.Windows.Forms.Label lblServerUri;
    private System.Windows.Forms.Button btnTestConnection;
    private System.Windows.Forms.GroupBox gbGatewaySimulator;
    private System.Windows.Forms.TextBox txtLoginRequestProvider;
    private System.Windows.Forms.Label lblLoginRequestProvider;
    private System.Windows.Forms.PictureBox pbTestConnectionResult;
    private System.Windows.Forms.Label lblTestConnectionResult;
    private System.Windows.Forms.TextBox txtGameCreatedRequestPassword;
    private System.Windows.Forms.Label lblGameCreatedRequestPassword;
    private System.Windows.Forms.TextBox txtGameCreatedRequestPartnerId;
    private System.Windows.Forms.Label lblGameCreatedRequestPartnerId;
    private System.Windows.Forms.TextBox txtGameStartingRequestPassword;
    private System.Windows.Forms.Label lblGameStartingRequestPassword;
    private System.Windows.Forms.TextBox txtGameStartingRequestPartnerId;
    private System.Windows.Forms.Label lblGameStartingRequestPartnerId;
    private System.Windows.Forms.TabPage tabPageCustomXMLRequest;
    private System.Windows.Forms.SplitContainer splitContainer3;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Panel panelFreeXML;
    private System.Windows.Forms.GroupBox groupBoxCustomXML;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnSendCustomXMLRequest;
    private System.Windows.Forms.Button btnBuildCustomXMLRequest;
    private System.Windows.Forms.SplitContainer splitContainer11;
    private System.Windows.Forms.GroupBox groupBox6;
    private UserControls.RequestData requestDataCustomXMLRequest;
    private System.Windows.Forms.GroupBox groupBox23;
    private UserControls.ResponseData responseDataCustomXMLRequest;
    private UserControls.XMLEditor xmlEditorRequest;
    private UserControls.SearchPlaySessions searchPlaySessionsLoginRequest;
    private System.Windows.Forms.Button btnSearchPlaySessionsBulkCredit;
    private System.Windows.Forms.SplitContainer splitContainer21;
    private System.Windows.Forms.GroupBox gbConcurrentConnectionsParameters;
    private System.Windows.Forms.Button btnRunConcurrentConnections;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtConcurrentConnectionsBetAmount;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private CustomControls.NumericTextBox txtConcurrentConnectionsNumberOfPlayers;
    private System.Windows.Forms.ComboBox cboConcurrentConnectionsPlayType;
    private System.Windows.Forms.Label label5;
    private CustomControls.NumericTextBox txtConcurrentConnectionsDelayBetweenBets;
    private System.Windows.Forms.GroupBox groupBox29;
    private System.Windows.Forms.TabControl tabControlConcurrentConnectionsResults;
    private System.Windows.Forms.TabPage tabPageConcurrentConnectionsGeneralResults;
    private System.Windows.Forms.Button btnStopConcurrentConnections;
    private System.Windows.Forms.DataGridView dgvConcurrentConnectionsGeneralResults;
    private System.Windows.Forms.GroupBox gbConcurrentConnectionsConsoleOutput;
    private System.Windows.Forms.TextBox txtConcurrentConnectionsConsoleOutput;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnGeneralResultPlaySessionId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnGeneralResultTotalServerCalls;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnGeneralResultTotalLogins;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnGeneralResultTotalBets;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnGeneralResultTotalWins;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnGeneralResultViewDetail;
    private System.Windows.Forms.TextBox txtDebitRequestTransaction;
    private System.Windows.Forms.Label lblDebitRequestTransaction;
    private System.Windows.Forms.TextBox txtCreditRequestTransaction;
    private System.Windows.Forms.Label lblCreditRequestTransaction;
    private System.Windows.Forms.TextBox txtBulkCreditRequestTransaction;
    private System.Windows.Forms.Label lblBulkCreditRequestTransaction;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestTransaction;
    private System.Windows.Forms.Label lblOfflineCreditRequestTransaction;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestTransaction;
    private System.Windows.Forms.Label lblOfflineDebitRequestTransaction;
    private System.Windows.Forms.TextBox txtGameCreatedRequestGameTitle;
    private System.Windows.Forms.Label lblGameCreatedRequestGameTitle;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtBulkCreditRequestCreditJackpot;
    private System.Windows.Forms.Label lblBulkCreditRequestBetJackpot;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtOfflineCreditRequestBetJackpot;
    private System.Windows.Forms.Label lblOfflineCreditRequestBetJackpot;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnBulkCreditBetId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnBulkCreditAmount;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnBulkCreditUserId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnBulkCreditJackpot;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnBulkCreditDelete;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineCreditBetId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineCreditBetAmount;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnOfflineCreditBetJackpot;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnOfflineCreditBetDelete;
    private GameGatewaySimulator.CustomControls.NumericTextBox txtCreditRequestBetJackpot;
    private System.Windows.Forms.Label lblCreditRequestBetJackpot;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnCreditBetId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnCreditBetAmount;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnCreditBetJackpot;
    private System.Windows.Forms.DataGridViewButtonColumn dgvColumnCreditBetDelete;
    private System.Windows.Forms.TextBox txtDebitRequestGameId;
    private System.Windows.Forms.Label lblDebitRequestGameId;
    private System.Windows.Forms.TextBox txtBulkCreditRequestGameId;
    private System.Windows.Forms.Label lblBulkCreditRequestGameId;
    private System.Windows.Forms.TextBox txtOfflineCreditRequestGameId;
    private System.Windows.Forms.Label lblOfflineCreditRequestGameId;
    private System.Windows.Forms.TextBox txtCreditRequestGameId;
    private System.Windows.Forms.Label lblCreditRequestGameId;
    private System.Windows.Forms.TextBox txtOfflineDebitRequestGameId;
    private System.Windows.Forms.Label lblOfflineDebitRequestGameId;
    private System.Windows.Forms.DateTimePicker dtpGameCreatedRequestGameStarts;
    private CustomControls.NumericTextBox txtGameStartingRequestJackpot;
    private System.Windows.Forms.Label lblGameStartingRequestJackpot;
  }
}

