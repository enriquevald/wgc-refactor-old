﻿using GameGatewaySimulator.Forms.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameGatewaySimulator.Extensions;
using GameGatewaySimulator.UserControls.Messages;

namespace GameGatewaySimulator.Forms
{
  public partial class BaseForm : Form
  {
    public BaseForm()
    {
      InitializeComponent();
    }

    private IDictionary<Control, LoadingMessage> loadingControlsDictionary = new Dictionary<Control, LoadingMessage>();

    public void ShowLoading(Control parentControl)
    {
      if (!loadingControlsDictionary.ContainsKey(parentControl))
      {
        LoadingMessage loadingMessage = new LoadingMessage();
        parentControl.Controls.Add(loadingMessage);
        loadingControlsDictionary.Add(new KeyValuePair<Control, LoadingMessage>(parentControl, loadingMessage));
        loadingMessage.Center();
        loadingMessage.Anchor = AnchorStyles.None;
        loadingMessage.BringToFront();
      }
    }

    public void HideLoading( Control parentControl)
    {
      if (loadingControlsDictionary.ContainsKey(parentControl))
      {
        LoadingMessage loadingMessage = loadingControlsDictionary[parentControl];
        parentControl.Controls.Remove(loadingMessage);
        loadingControlsDictionary.Remove(parentControl);
        loadingMessage.Dispose();
      }
    }

    public DialogResult ShowError(Exception ex)
    {
      using (ErrorMessage errorForm = new ErrorMessage(ex))
        return errorForm.ShowDialog(this);
    }

    public DialogResult ShowError(string message)
    {
      using(ErrorMessage errorForm = new ErrorMessage(message))
        return errorForm.ShowDialog(this);
    }

    public DialogResult ShowInformation(string message)
    {
      using (InformationMessage informationForm = new InformationMessage(message))
        return informationForm.ShowDialog(this);
    }
  }
}
