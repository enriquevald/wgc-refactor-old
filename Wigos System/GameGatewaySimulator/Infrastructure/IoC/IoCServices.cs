﻿using Autofac;
using log4net;
using GameGatewaySimulator.Services.Contracts;
using GameGatewaySimulator.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autofac.Features.OwnedInstances;

namespace GameGatewaySimulator.Infrastructure.IoC
{
  static class IoCServices
  {
    private static readonly ILog Logger = LogManager.GetLogger(typeof(IoCServices));
    private static IContainer container;

    public static void RegisterDependencies()
    {
      try
      {
        Logger.Info("Registrando dependencias del contenedor de IoC.");

        var builder = new ContainerBuilder();
        RegisterServices(builder);
        RegisterForms(builder);
        container = builder.Build();

        Logger.Info("Dependencias registradas");
      }
      catch (Exception ex)
      {
        Logger.Fatal("Error al registrar dependencias del contenedor de IoC.", ex);
        throw ex;
      }
    }

    public static T Resolve<T>()
    {
      return container.Resolve<T>();
    }

    private static void RegisterServices(ContainerBuilder builder)
    {
      builder.RegisterType<GatewayServices>().As<IGatewayServices>().InstancePerLifetimeScope();
      builder.RegisterType<DBServices>().As<IDBServices>().InstancePerLifetimeScope();
    }

    private static void RegisterForms(ContainerBuilder builder)
    {
      var assembly = Assembly.GetExecutingAssembly();
      builder.RegisterAssemblyTypes(assembly)
          .AssignableTo<Form>();
    }

    public static Owned<T> GetServiceInstance<T>()
    {
      return container.Resolve<Owned<T>>();
    }
  }
}
