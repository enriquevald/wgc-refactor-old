﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameGatewaySimulator.Data.Response;

namespace GameGatewaySimulator.UserControls
{
  public partial class ResponseData : UserControl
  {
    public ResponseData()
    {
      InitializeComponent();
    }

    private WebResponseInfo responseInfo;
    public WebResponseInfo ResponseInfo
    {
      get { return responseInfo; }
    }

    public void SetResponseInfo(WebResponseInfo webResponseInfo)
    {
      lblResponseTime.Text = DateTime.Now.ToLongTimeString();
      if(webResponseInfo.StatusCode == System.Net.HttpStatusCode.OK)
      {
        panelOK.BackColor = Color.Lime;
        lblResponseStatus.Text = webResponseInfo.StatusCode.ToString();
      }
      else
      {
        panelOK.BackColor = Color.Red;
        lblResponseStatus.Text = webResponseInfo.StatusCode.ToString();
      }

      txtContentHeaderResponseData.Text = webResponseInfo.GetHeaderData();
      webBrowserXmlResponseData.DocumentText = webResponseInfo.Body;
      responseInfo = webResponseInfo;
    }
  }
}
