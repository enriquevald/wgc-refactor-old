﻿namespace GameGatewaySimulator.UserControls
{
  partial class SearchPlaySessions
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox29 = new System.Windows.Forms.GroupBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.filterClosedSessionsColor = new System.Windows.Forms.Panel();
      this.filterOpenSessionsColor = new System.Windows.Forms.Panel();
      this.btnSearchPlaySessions = new System.Windows.Forms.Button();
      this.chkClosedSessionsFilter = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.chkOpenSessionsFilter = new System.Windows.Forms.CheckBox();
      this.dgvPlaySessions = new System.Windows.Forms.DataGridView();
      this.dgvColumnPlaySessionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnTerminalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnAccountId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnAccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnMachineCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dgvColumnAccountBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panelFilters = new System.Windows.Forms.Panel();
      this.panelPlaySessions = new System.Windows.Forms.Panel();
      this.txtTerminalIdFilter = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.txtAccountIdFilter = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.txtPlaySessionIdFilter = new GameGatewaySimulator.CustomControls.NumericTextBox();
      this.groupBox29.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvPlaySessions)).BeginInit();
      this.panelFilters.SuspendLayout();
      this.panelPlaySessions.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox29
      // 
      this.groupBox29.Controls.Add(this.txtTerminalIdFilter);
      this.groupBox29.Controls.Add(this.label4);
      this.groupBox29.Controls.Add(this.txtAccountIdFilter);
      this.groupBox29.Controls.Add(this.label3);
      this.groupBox29.Controls.Add(this.txtPlaySessionIdFilter);
      this.groupBox29.Controls.Add(this.filterClosedSessionsColor);
      this.groupBox29.Controls.Add(this.filterOpenSessionsColor);
      this.groupBox29.Controls.Add(this.btnSearchPlaySessions);
      this.groupBox29.Controls.Add(this.chkClosedSessionsFilter);
      this.groupBox29.Controls.Add(this.label2);
      this.groupBox29.Controls.Add(this.chkOpenSessionsFilter);
      this.groupBox29.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBox29.Location = new System.Drawing.Point(0, 0);
      this.groupBox29.Name = "groupBox29";
      this.groupBox29.Size = new System.Drawing.Size(474, 127);
      this.groupBox29.TabIndex = 1;
      this.groupBox29.TabStop = false;
      this.groupBox29.Text = "Filters";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(153, 72);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(59, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "Terminal Id";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(153, 46);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(59, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Account Id";
      // 
      // filterClosedSessionsColor
      // 
      this.filterClosedSessionsColor.BackColor = System.Drawing.Color.Red;
      this.filterClosedSessionsColor.Location = new System.Drawing.Point(4, 44);
      this.filterClosedSessionsColor.Name = "filterClosedSessionsColor";
      this.filterClosedSessionsColor.Size = new System.Drawing.Size(13, 13);
      this.filterClosedSessionsColor.TabIndex = 1;
      // 
      // filterOpenSessionsColor
      // 
      this.filterOpenSessionsColor.BackColor = System.Drawing.Color.Lime;
      this.filterOpenSessionsColor.Location = new System.Drawing.Point(4, 20);
      this.filterOpenSessionsColor.Name = "filterOpenSessionsColor";
      this.filterOpenSessionsColor.Size = new System.Drawing.Size(13, 13);
      this.filterOpenSessionsColor.TabIndex = 0;
      // 
      // btnSearchPlaySessions
      // 
      this.btnSearchPlaySessions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSearchPlaySessions.Image = global::GameGatewaySimulator.Properties.Resources.search;
      this.btnSearchPlaySessions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.btnSearchPlaySessions.Location = new System.Drawing.Point(388, 94);
      this.btnSearchPlaySessions.Name = "btnSearchPlaySessions";
      this.btnSearchPlaySessions.Size = new System.Drawing.Size(80, 25);
      this.btnSearchPlaySessions.TabIndex = 5;
      this.btnSearchPlaySessions.Text = "Search";
      this.btnSearchPlaySessions.UseVisualStyleBackColor = true;
      this.btnSearchPlaySessions.Click += new System.EventHandler(this.btnSearchPlaySessions_Click);
      // 
      // chkClosedSessionsFilter
      // 
      this.chkClosedSessionsFilter.AutoSize = true;
      this.chkClosedSessionsFilter.Location = new System.Drawing.Point(22, 43);
      this.chkClosedSessionsFilter.Name = "chkClosedSessionsFilter";
      this.chkClosedSessionsFilter.Size = new System.Drawing.Size(126, 17);
      this.chkClosedSessionsFilter.TabIndex = 1;
      this.chkClosedSessionsFilter.Text = "Closed Play Sessions";
      this.chkClosedSessionsFilter.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(153, 20);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(79, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Play Session Id";
      // 
      // chkOpenSessionsFilter
      // 
      this.chkOpenSessionsFilter.AutoSize = true;
      this.chkOpenSessionsFilter.Checked = true;
      this.chkOpenSessionsFilter.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkOpenSessionsFilter.Location = new System.Drawing.Point(22, 19);
      this.chkOpenSessionsFilter.Name = "chkOpenSessionsFilter";
      this.chkOpenSessionsFilter.Size = new System.Drawing.Size(120, 17);
      this.chkOpenSessionsFilter.TabIndex = 0;
      this.chkOpenSessionsFilter.Text = "Open Play Sessions";
      this.chkOpenSessionsFilter.UseVisualStyleBackColor = true;
      // 
      // dgvPlaySessions
      // 
      this.dgvPlaySessions.AllowUserToAddRows = false;
      this.dgvPlaySessions.AllowUserToDeleteRows = false;
      this.dgvPlaySessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvPlaySessions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumnPlaySessionId,
            this.dgvColumnTerminalId,
            this.dgvColumnAccountId,
            this.dgvColumnAccountName,
            this.dgvColumnMachineCredit,
            this.dgvColumnAccountBalance});
      this.dgvPlaySessions.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvPlaySessions.Location = new System.Drawing.Point(0, 0);
      this.dgvPlaySessions.MultiSelect = false;
      this.dgvPlaySessions.Name = "dgvPlaySessions";
      this.dgvPlaySessions.ReadOnly = true;
      this.dgvPlaySessions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvPlaySessions.Size = new System.Drawing.Size(474, 315);
      this.dgvPlaySessions.TabIndex = 2;
      this.dgvPlaySessions.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvPlaySessions_CellFormatting);
      this.dgvPlaySessions.SelectionChanged += new System.EventHandler(this.dgvPlaySessions_SelectionChanged);
      // 
      // dgvColumnPlaySessionId
      // 
      this.dgvColumnPlaySessionId.DataPropertyName = "PlaySessionId";
      this.dgvColumnPlaySessionId.HeaderText = "Play Session Id";
      this.dgvColumnPlaySessionId.Name = "dgvColumnPlaySessionId";
      this.dgvColumnPlaySessionId.ReadOnly = true;
      this.dgvColumnPlaySessionId.Width = 50;
      // 
      // dgvColumnTerminalId
      // 
      this.dgvColumnTerminalId.DataPropertyName = "TerminalId";
      this.dgvColumnTerminalId.HeaderText = "Terminal Id";
      this.dgvColumnTerminalId.Name = "dgvColumnTerminalId";
      this.dgvColumnTerminalId.ReadOnly = true;
      this.dgvColumnTerminalId.Width = 50;
      // 
      // dgvColumnAccountId
      // 
      this.dgvColumnAccountId.DataPropertyName = "AccountId";
      this.dgvColumnAccountId.HeaderText = "Account Id";
      this.dgvColumnAccountId.Name = "dgvColumnAccountId";
      this.dgvColumnAccountId.ReadOnly = true;
      this.dgvColumnAccountId.Width = 50;
      // 
      // dgvColumnAccountName
      // 
      this.dgvColumnAccountName.DataPropertyName = "AccountName";
      this.dgvColumnAccountName.HeaderText = "Account Name";
      this.dgvColumnAccountName.Name = "dgvColumnAccountName";
      this.dgvColumnAccountName.ReadOnly = true;
      this.dgvColumnAccountName.Width = 150;
      // 
      // dgvColumnMachineCredit
      // 
      this.dgvColumnMachineCredit.DataPropertyName = "MachineCredit";
      this.dgvColumnMachineCredit.HeaderText = "Machine Credit";
      this.dgvColumnMachineCredit.Name = "dgvColumnMachineCredit";
      this.dgvColumnMachineCredit.ReadOnly = true;
      this.dgvColumnMachineCredit.Width = 65;
      // 
      // dgvColumnAccountBalance
      // 
      this.dgvColumnAccountBalance.DataPropertyName = "AccountBalance";
      this.dgvColumnAccountBalance.HeaderText = "Account Balance";
      this.dgvColumnAccountBalance.Name = "dgvColumnAccountBalance";
      this.dgvColumnAccountBalance.ReadOnly = true;
      this.dgvColumnAccountBalance.Width = 65;
      // 
      // panelFilters
      // 
      this.panelFilters.Controls.Add(this.groupBox29);
      this.panelFilters.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelFilters.Location = new System.Drawing.Point(0, 0);
      this.panelFilters.Name = "panelFilters";
      this.panelFilters.Size = new System.Drawing.Size(474, 127);
      this.panelFilters.TabIndex = 3;
      // 
      // panelPlaySessions
      // 
      this.panelPlaySessions.Controls.Add(this.dgvPlaySessions);
      this.panelPlaySessions.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelPlaySessions.Location = new System.Drawing.Point(0, 127);
      this.panelPlaySessions.Name = "panelPlaySessions";
      this.panelPlaySessions.Size = new System.Drawing.Size(474, 315);
      this.panelPlaySessions.TabIndex = 4;
      // 
      // txtTerminalIdFilter
      // 
      this.txtTerminalIdFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtTerminalIdFilter.DecimalPlaces = 2;
      this.txtTerminalIdFilter.DecimalSeparator = ".";
      this.txtTerminalIdFilter.IsDecimal = false;
      this.txtTerminalIdFilter.Location = new System.Drawing.Point(232, 69);
      this.txtTerminalIdFilter.Name = "txtTerminalIdFilter";
      this.txtTerminalIdFilter.Size = new System.Drawing.Size(236, 20);
      this.txtTerminalIdFilter.TabIndex = 4;
      this.txtTerminalIdFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminalIdFilter_KeyPress);
      // 
      // txtAccountIdFilter
      // 
      this.txtAccountIdFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtAccountIdFilter.DecimalPlaces = 2;
      this.txtAccountIdFilter.DecimalSeparator = ".";
      this.txtAccountIdFilter.IsDecimal = false;
      this.txtAccountIdFilter.Location = new System.Drawing.Point(232, 43);
      this.txtAccountIdFilter.Name = "txtAccountIdFilter";
      this.txtAccountIdFilter.Size = new System.Drawing.Size(236, 20);
      this.txtAccountIdFilter.TabIndex = 3;
      this.txtAccountIdFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAccountIdFilter_KeyPress);
      // 
      // txtPlaySessionIdFilter
      // 
      this.txtPlaySessionIdFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtPlaySessionIdFilter.DecimalPlaces = 2;
      this.txtPlaySessionIdFilter.DecimalSeparator = ".";
      this.txtPlaySessionIdFilter.IsDecimal = false;
      this.txtPlaySessionIdFilter.Location = new System.Drawing.Point(232, 17);
      this.txtPlaySessionIdFilter.Name = "txtPlaySessionIdFilter";
      this.txtPlaySessionIdFilter.Size = new System.Drawing.Size(236, 20);
      this.txtPlaySessionIdFilter.TabIndex = 2;
      this.txtPlaySessionIdFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPlaySessionIdFilter_KeyPress);
      // 
      // SearchPlaySessions
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panelPlaySessions);
      this.Controls.Add(this.panelFilters);
      this.Name = "SearchPlaySessions";
      this.Size = new System.Drawing.Size(474, 442);
      this.groupBox29.ResumeLayout(false);
      this.groupBox29.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvPlaySessions)).EndInit();
      this.panelFilters.ResumeLayout(false);
      this.panelPlaySessions.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox29;
    private CustomControls.NumericTextBox txtTerminalIdFilter;
    private System.Windows.Forms.Label label4;
    private CustomControls.NumericTextBox txtAccountIdFilter;
    private System.Windows.Forms.Label label3;
    private CustomControls.NumericTextBox txtPlaySessionIdFilter;
    private System.Windows.Forms.Panel filterClosedSessionsColor;
    private System.Windows.Forms.Panel filterOpenSessionsColor;
    private System.Windows.Forms.Button btnSearchPlaySessions;
    private System.Windows.Forms.CheckBox chkClosedSessionsFilter;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox chkOpenSessionsFilter;
    private System.Windows.Forms.DataGridView dgvPlaySessions;
    private System.Windows.Forms.Panel panelFilters;
    private System.Windows.Forms.Panel panelPlaySessions;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnPlaySessionId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnTerminalId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnAccountId;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnAccountName;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnMachineCredit;
    private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnAccountBalance;
  }
}
