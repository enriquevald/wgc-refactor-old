﻿namespace GameGatewaySimulator.UserControls
{
  partial class XMLEditor
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txtXMLEditor = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // txtXMLEditor
      // 
      this.txtXMLEditor.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txtXMLEditor.Location = new System.Drawing.Point(0, 0);
      this.txtXMLEditor.Name = "txtXMLEditor";
      this.txtXMLEditor.Size = new System.Drawing.Size(263, 169);
      this.txtXMLEditor.TabIndex = 0;
      this.txtXMLEditor.Text = "";
      // 
      // XMLEditor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.txtXMLEditor);
      this.Name = "XMLEditor";
      this.Size = new System.Drawing.Size(263, 169);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.RichTextBox txtXMLEditor;
  }
}
