﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GameGatewaySimulator.UserControls
{
  public partial class RequestData : UserControl
  {
    public RequestData()
    {
      InitializeComponent();
    }

    private XmlDocument requestXML;
    public XmlDocument RequestXML
    {
      get { return requestXML; }
    }

    public void SetRequestXML(XmlDocument xml)
    {
      this.webBrowserRequestData.DocumentText = xml.InnerXml;
      requestXML = xml;
    }
  }
}
