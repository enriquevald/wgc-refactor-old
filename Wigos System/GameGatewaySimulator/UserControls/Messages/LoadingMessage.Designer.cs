﻿namespace GameGatewaySimulator.UserControls.Messages
{
  partial class LoadingMessage
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panelLoading = new System.Windows.Forms.Panel();
      this.pbLoading = new System.Windows.Forms.PictureBox();
      this.lblLoadingMessage = new System.Windows.Forms.Label();
      this.panelLoading.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
      this.SuspendLayout();
      // 
      // panelLoading
      // 
      this.panelLoading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
      this.panelLoading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panelLoading.Controls.Add(this.pbLoading);
      this.panelLoading.Controls.Add(this.lblLoadingMessage);
      this.panelLoading.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelLoading.Location = new System.Drawing.Point(0, 0);
      this.panelLoading.Name = "panelLoading";
      this.panelLoading.Size = new System.Drawing.Size(86, 33);
      this.panelLoading.TabIndex = 2;
      // 
      // pbLoading
      // 
      this.pbLoading.Image = global::GameGatewaySimulator.Properties.Resources.ajax_loader;
      this.pbLoading.Location = new System.Drawing.Point(3, 8);
      this.pbLoading.Name = "pbLoading";
      this.pbLoading.Size = new System.Drawing.Size(16, 16);
      this.pbLoading.TabIndex = 2;
      this.pbLoading.TabStop = false;
      // 
      // lblLoadingMessage
      // 
      this.lblLoadingMessage.AutoSize = true;
      this.lblLoadingMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblLoadingMessage.Location = new System.Drawing.Point(20, 9);
      this.lblLoadingMessage.Name = "lblLoadingMessage";
      this.lblLoadingMessage.Size = new System.Drawing.Size(64, 13);
      this.lblLoadingMessage.TabIndex = 0;
      this.lblLoadingMessage.Text = "Loading...";
      // 
      // LoadingMessage
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panelLoading);
      this.Name = "LoadingMessage";
      this.Size = new System.Drawing.Size(86, 33);
      this.panelLoading.ResumeLayout(false);
      this.panelLoading.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelLoading;
    private System.Windows.Forms.PictureBox pbLoading;
    private System.Windows.Forms.Label lblLoadingMessage;
  }
}
