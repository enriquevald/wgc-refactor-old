﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GameGatewaySimulator.UserControls
{
  public partial class XMLEditor : UserControl
  {
    public XMLEditor()
    {
      InitializeComponent();
    }

    public string XML
    {
      get { return txtXMLEditor.Text; }
    }

    public XmlDocument XMLDocument
    {
      get 
      {
        XmlDocument document = new XmlDocument();
        document.LoadXml(txtXMLEditor.Text);
        bool hasDec = document.FirstChild.NodeType == XmlNodeType.XmlDeclaration;
        if (!hasDec)
        { 
          XmlDeclaration declaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);
          XmlElement root = document.DocumentElement;
          document.InsertBefore(declaration, root);
        }
        return document;
      }
    }

    public bool IsValidXML
    {
      get 
      {
        try
        {
          XmlDocument document = this.XMLDocument;
          return true;
        }
        catch
        {
          return false;
        }
      }
    }
  }
}
