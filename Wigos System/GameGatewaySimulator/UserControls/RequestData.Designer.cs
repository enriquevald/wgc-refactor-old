﻿namespace GameGatewaySimulator.UserControls
{
  partial class RequestData
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.webBrowserRequestData = new System.Windows.Forms.WebBrowser();
      this.SuspendLayout();
      // 
      // webBrowserRequestData
      // 
      this.webBrowserRequestData.AllowWebBrowserDrop = false;
      this.webBrowserRequestData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.webBrowserRequestData.Location = new System.Drawing.Point(0, 0);
      this.webBrowserRequestData.Margin = new System.Windows.Forms.Padding(1);
      this.webBrowserRequestData.MinimumSize = new System.Drawing.Size(20, 20);
      this.webBrowserRequestData.Name = "webBrowserRequestData";
      this.webBrowserRequestData.Size = new System.Drawing.Size(266, 82);
      this.webBrowserRequestData.TabIndex = 0;
      this.webBrowserRequestData.WebBrowserShortcutsEnabled = false;
      // 
      // RequestData
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.webBrowserRequestData);
      this.Name = "RequestData";
      this.Size = new System.Drawing.Size(266, 82);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.WebBrowser webBrowserRequestData;



  }
}
