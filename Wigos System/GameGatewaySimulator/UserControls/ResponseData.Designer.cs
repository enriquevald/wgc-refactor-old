﻿namespace GameGatewaySimulator.UserControls
{
  partial class ResponseData
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panelOK = new System.Windows.Forms.Panel();
      this.lblResponse = new System.Windows.Forms.Label();
      this.lblResponseStatus = new System.Windows.Forms.Label();
      this.panelResponseStatus = new System.Windows.Forms.Panel();
      this.panelResponseData = new System.Windows.Forms.Panel();
      this.webBrowserXmlResponseData = new System.Windows.Forms.WebBrowser();
      this.txtContentHeaderResponseData = new System.Windows.Forms.RichTextBox();
      this.lblResponseTime = new System.Windows.Forms.Label();
      this.panelResponseStatus.SuspendLayout();
      this.panelResponseData.SuspendLayout();
      this.SuspendLayout();
      // 
      // panelOK
      // 
      this.panelOK.BackColor = System.Drawing.Color.Lime;
      this.panelOK.Location = new System.Drawing.Point(8, 9);
      this.panelOK.Name = "panelOK";
      this.panelOK.Size = new System.Drawing.Size(25, 25);
      this.panelOK.TabIndex = 0;
      // 
      // lblResponse
      // 
      this.lblResponse.AutoSize = true;
      this.lblResponse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblResponse.Location = new System.Drawing.Point(39, 9);
      this.lblResponse.Name = "lblResponse";
      this.lblResponse.Size = new System.Drawing.Size(182, 25);
      this.lblResponse.TabIndex = 1;
      this.lblResponse.Text = "Response Status:";
      // 
      // lblResponseStatus
      // 
      this.lblResponseStatus.AutoSize = true;
      this.lblResponseStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblResponseStatus.Location = new System.Drawing.Point(218, 9);
      this.lblResponseStatus.Name = "lblResponseStatus";
      this.lblResponseStatus.Size = new System.Drawing.Size(120, 25);
      this.lblResponseStatus.TabIndex = 2;
      this.lblResponseStatus.Text = "NOT SEND";
      // 
      // panelResponseStatus
      // 
      this.panelResponseStatus.Controls.Add(this.lblResponseTime);
      this.panelResponseStatus.Controls.Add(this.lblResponse);
      this.panelResponseStatus.Controls.Add(this.lblResponseStatus);
      this.panelResponseStatus.Controls.Add(this.panelOK);
      this.panelResponseStatus.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelResponseStatus.Location = new System.Drawing.Point(0, 0);
      this.panelResponseStatus.Name = "panelResponseStatus";
      this.panelResponseStatus.Size = new System.Drawing.Size(557, 44);
      this.panelResponseStatus.TabIndex = 3;
      // 
      // panelResponseData
      // 
      this.panelResponseData.Controls.Add(this.webBrowserXmlResponseData);
      this.panelResponseData.Controls.Add(this.txtContentHeaderResponseData);
      this.panelResponseData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelResponseData.Location = new System.Drawing.Point(0, 44);
      this.panelResponseData.Name = "panelResponseData";
      this.panelResponseData.Size = new System.Drawing.Size(557, 355);
      this.panelResponseData.TabIndex = 4;
      // 
      // webBrowserXmlResponseData
      // 
      this.webBrowserXmlResponseData.AllowWebBrowserDrop = false;
      this.webBrowserXmlResponseData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.webBrowserXmlResponseData.Location = new System.Drawing.Point(0, 40);
      this.webBrowserXmlResponseData.Margin = new System.Windows.Forms.Padding(1);
      this.webBrowserXmlResponseData.MinimumSize = new System.Drawing.Size(20, 20);
      this.webBrowserXmlResponseData.Name = "webBrowserXmlResponseData";
      this.webBrowserXmlResponseData.Size = new System.Drawing.Size(557, 315);
      this.webBrowserXmlResponseData.TabIndex = 2;
      this.webBrowserXmlResponseData.WebBrowserShortcutsEnabled = false;
      // 
      // txtContentHeaderResponseData
      // 
      this.txtContentHeaderResponseData.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtContentHeaderResponseData.Dock = System.Windows.Forms.DockStyle.Top;
      this.txtContentHeaderResponseData.Location = new System.Drawing.Point(0, 0);
      this.txtContentHeaderResponseData.Name = "txtContentHeaderResponseData";
      this.txtContentHeaderResponseData.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
      this.txtContentHeaderResponseData.Size = new System.Drawing.Size(557, 40);
      this.txtContentHeaderResponseData.TabIndex = 1;
      this.txtContentHeaderResponseData.Text = "";
      // 
      // lblResponseTime
      // 
      this.lblResponseTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblResponseTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblResponseTime.Location = new System.Drawing.Point(424, 9);
      this.lblResponseTime.Name = "lblResponseTime";
      this.lblResponseTime.Size = new System.Drawing.Size(122, 25);
      this.lblResponseTime.TabIndex = 3;
      this.lblResponseTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // ResponseData
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panelResponseData);
      this.Controls.Add(this.panelResponseStatus);
      this.Name = "ResponseData";
      this.Size = new System.Drawing.Size(557, 399);
      this.panelResponseStatus.ResumeLayout(false);
      this.panelResponseStatus.PerformLayout();
      this.panelResponseData.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panelOK;
    private System.Windows.Forms.Label lblResponse;
    private System.Windows.Forms.Label lblResponseStatus;
    private System.Windows.Forms.Panel panelResponseStatus;
    private System.Windows.Forms.Panel panelResponseData;
    private System.Windows.Forms.RichTextBox txtContentHeaderResponseData;
    private System.Windows.Forms.WebBrowser webBrowserXmlResponseData;
    private System.Windows.Forms.Label lblResponseTime;
  }
}
