﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameGatewaySimulator.Data.Entities;
using WSI.Common;
using GameGatewaySimulator.Services.Contracts;
using GameGatewaySimulator.Forms;
using GameGatewaySimulator.Services.Implementations;
using System.Threading;
using GameGatewaySimulator.Infrastructure.IoC;

namespace GameGatewaySimulator.UserControls
{
  public partial class SearchPlaySessions : UserControl
  {
    #region Constants

    private Color OPEN_SESSIONS_COLOR = Color.Lime;
    private Color CLOSED_SESSIONS_COLOR = Color.Red;

    #endregion

    #region Variables

    private SynchronizationContext synchronizationContext;

    #endregion

    #region Events

    public delegate void SelectedChangedEventHandler(object sender, SearchPlaySessionsSelectedChangedEventArgs e);
    public event SelectedChangedEventHandler OnSelectedChanged;

    #endregion

    public SearchPlaySessions()
    {
      InitializeComponent();
    }

    private void Initialize()
    {
      synchronizationContext = SynchronizationContext.Current;

      filterOpenSessionsColor.BackColor = OPEN_SESSIONS_COLOR;
      filterClosedSessionsColor.BackColor = CLOSED_SESSIONS_COLOR;
    }

    private void dgvPlaySessions_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (dgvPlaySessions.Columns[e.ColumnIndex].Name == "dgvColumnPlaySessionId")
      {
        PlaySessionData selectedPlaySession = (PlaySessionData)dgvPlaySessions.Rows[e.RowIndex].DataBoundItem;
        if (selectedPlaySession.PlaySessionStatus == PlaySessionStatus.Closed)
          e.CellStyle.BackColor = CLOSED_SESSIONS_COLOR;
        else
          e.CellStyle.BackColor = OPEN_SESSIONS_COLOR;
      }
    }

    bool fireSelectionChanged = false;
    private void dgvPlaySessions_SelectionChanged(object sender, EventArgs e)
    {
      if (fireSelectionChanged)
      {
        if (OnSelectedChanged != null)
        {
          PlaySessionData selectedPlaySession = (PlaySessionData)dgvPlaySessions.SelectedRows[0].DataBoundItem;
          OnSelectedChanged(this, new SearchPlaySessionsSelectedChangedEventArgs(selectedPlaySession));
        }
      }
    }

    private async void btnSearchPlaySessions_Click(object sender, EventArgs e)
    {
      BaseForm parentForm = (BaseForm)this.ParentForm as BaseForm;

      if (!ValidatePlaySessionStatus(parentForm))
        return;
      
      try
      {
        parentForm.ShowLoading(dgvPlaySessions);
        IList<PlaySessionData> list = await Task<IList<PlaySessionData>>.Run(() =>
        {
          return GetPlaySessions();
        });

        FillPlaySessions(list);
      }
      catch (Exception ex)
      {
        parentForm.ShowError(ex);
      }
      finally
      {
        parentForm.HideLoading(dgvPlaySessions);
      }
    }

    private bool ValidatePlaySessionStatus(BaseForm form)
    {
      if (!chkClosedSessionsFilter.Checked && !chkOpenSessionsFilter.Checked)
      {
        form.ShowInformation("Debe seleccionar sesiones abiertas, cerradas o ambas");
        chkOpenSessionsFilter.Focus();
        return false;
      }

      return true;
    }

    private IList<PlaySessionData> GetPlaySessions()
    {
      using (var dbServices = IoCServices.GetServiceInstance<IDBServices>())
      {
        PlaySessionFilter filter = new PlaySessionFilter();
        filter.OpenSessions = chkOpenSessionsFilter.Checked;
        filter.ClosedSessions = chkClosedSessionsFilter.Checked;
        filter.PlaySessionId = txtPlaySessionIdFilter.Text.Trim();
        filter.AccountId = txtAccountIdFilter.Text.Trim();
        filter.TerminalId = txtTerminalIdFilter.Text.Trim();

        return dbServices.Value.GetLastPlaySessions(filter);
      }
    }

    private void FillPlaySessions(IList<PlaySessionData> playSessions)
    {
      fireSelectionChanged = false;
      dgvPlaySessions.AutoGenerateColumns = false;
      dgvPlaySessions.DataSource = playSessions;
      dgvPlaySessions.ClearSelection();
      fireSelectionChanged = true;
    }

    private void txtPlaySessionIdFilter_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Return)
        btnSearchPlaySessions.PerformClick();
    }

    private void txtAccountIdFilter_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Return)
        btnSearchPlaySessions.PerformClick();
    }

    private void txtTerminalIdFilter_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == (char)Keys.Return)
        btnSearchPlaySessions.PerformClick();
    }
  }
  public class SearchPlaySessionsSelectedChangedEventArgs : EventArgs
  {
    private PlaySessionData data;
    public SearchPlaySessionsSelectedChangedEventArgs(PlaySessionData data)
    {
      this.data = data;
    }

    public PlaySessionData PlaySession { get { return data; } }
  } 
}
