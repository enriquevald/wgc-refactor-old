﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Enums
{
  public enum RequestOperationType
  {
    Login = 0,
    Debit = 1,
    BulkCredit = 2,
    Logout = 3
  }
}
