﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Enums
{
  public enum PlayType
  {
    AllwaysWin = 0,
    AllwaysLose = 1,
    Random = 3
  }
}
