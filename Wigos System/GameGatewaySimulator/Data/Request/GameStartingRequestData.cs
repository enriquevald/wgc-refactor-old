﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class GameStartingRequestData
  {
    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string GameId
    {
      get;
      set;
    }

    public string GameInstanceId
    {
      get;
      set;
    }

    public string Jackpot
    {
      get;
      set;
    }
  }
}
