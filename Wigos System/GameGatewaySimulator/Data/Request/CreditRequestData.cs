﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class CreditRequestData
  {
    public string SessionId
    {
      get;
      set;
    }

    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string GameId
    {
      get;
      set;

    }
    public string GameInstanceId
    {
      get;
      set;
    }

    public string Amount
    {
      get;
      set;
    }

    public string Currency
    {
      get;
      set;
    }

    public bool Bonus
    {
      get;
      set;
    }

    public string Transaction
    {
      get;
      set;
    }

    private List<CreditRequestBetData> bets;
    public List<CreditRequestBetData> Bets
    {
      get
      {
        if (bets == null)
          bets = new List<CreditRequestBetData>();

        return bets;
      }
    }
  }
}
