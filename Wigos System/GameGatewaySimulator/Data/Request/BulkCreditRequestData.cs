﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class BulkCreditRequestData
  {
    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string GameId
    {
      get;
      set;
    }

    public string GameInstanceId
    {
      get;
      set;
    }

    public bool Rollback
    {
      get;
      set;
    }

    public string Transaction
    {
      get;
      set;
    }

    public string Currency
    {
      get;
      set;
    }

    private List<BulkCreditRequestCreditData> credits;
    public List<BulkCreditRequestCreditData> Credits
    {
      get
      {
        if (credits == null)
          credits = new List<BulkCreditRequestCreditData>();

        return credits;
      }
    }
  }
}
