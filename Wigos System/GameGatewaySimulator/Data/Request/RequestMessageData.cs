﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameGatewaySimulator.Data.Request
{
  public class RequestMessageData
  {
    public string Uri
    {
      get;
      set;
    }

    public XmlDocument Xml
    {
      get;
      set;
    }
  }
}
