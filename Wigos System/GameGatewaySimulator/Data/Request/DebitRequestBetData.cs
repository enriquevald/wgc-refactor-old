﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class DebitRequestBetData
  {
    public string BetId
    {
      get;
      set;
    }

    public string BetValue
    {
      get;
      set;
    }

    public string BetRake
    {
      get;
      set;
    }
  }
}
