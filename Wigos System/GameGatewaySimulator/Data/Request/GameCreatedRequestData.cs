﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class GameCreatedRequestData
  {
    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string GameId
    {
      get;
      set;
    }

    public string GameTitle
    {
      get;
      set;
    }

    public string GameInstanceId
    {
      get;
      set;
    }

    public string GameStarts
    {
      get;
      set;
    }

    public string FirstPrize
    {
      get;
      set;
    }

    public string EntryCost
    {
      get;
      set;
    }

    public string GameLogo
    {
      get;
      set;
    }

    public string Jackpot
    {
      get;
      set;
    }
  }
}
