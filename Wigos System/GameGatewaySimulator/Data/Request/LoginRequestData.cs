﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class LoginRequestData
  {
    public string SessionId
    {
      get;
      set;
    }

    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }
  }
}
