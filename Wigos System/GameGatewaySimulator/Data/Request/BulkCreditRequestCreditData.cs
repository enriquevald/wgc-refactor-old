﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class BulkCreditRequestCreditData
  {
    public string BetId
    {
      get;
      set;
    }

    public string Amount
    {
      get;
      set;
    }

    public string UserId
    {
      get;
      set;
    }

    public string Jackpot
    {
      get;
      set;
    }
  }
}
