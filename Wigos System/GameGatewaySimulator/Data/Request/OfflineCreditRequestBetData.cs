﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class OfflineCreditRequestBetData
  {
    public string BetId
    {
      get;
      set;
    }

    public string BetAmount
    {
      get;
      set;
    }

    public string Jackpot
    {
      get;
      set;
    }
  }
}
