﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Request
{
  public class OfflineDebitRequestData
  {
    public string UserId
    {
      get;
      set;
    }

    public string PartnerId
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string GameId
    {
      get;
      set;
    }

    public string GameInstanceId
    {
      get;
      set;
    }

    public string Amount
    {
      get;
      set;
    }

    public string Currency
    {
      get;
      set;
    }

    public string Transaction
    {
      get;
      set;
    }

    private List<OfflineDebitRequestBetData> bets;
    public List<OfflineDebitRequestBetData> Bets
    {
      get
      {
        if (bets == null)
          bets = new List<OfflineDebitRequestBetData>();

        return bets;
      }
    }
  }
}
