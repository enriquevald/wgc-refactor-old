﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Entities
{
  public class GameInstanceFilter
  {
    public Nullable<int> ProviderId
    {
      get;
      set;
    }
  }
}
