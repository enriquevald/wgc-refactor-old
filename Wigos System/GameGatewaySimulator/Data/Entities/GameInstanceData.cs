﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Entities
{
  public class GameInstanceData
  {
    public Int64 GameInstanceId
    {
      get;
      set;
    }
    public Int64 GameId
    {
      get;
      set;
    }

    public int PartnerId
    {
      get;
      set;
    }
  }
}
