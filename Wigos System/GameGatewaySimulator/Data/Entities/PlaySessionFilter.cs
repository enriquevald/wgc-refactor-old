﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Entities
{
  public class PlaySessionFilter
  {
    public bool OpenSessions
    {
      get;
      set;
    }

    public bool ClosedSessions
    {
      get;
      set;
    }

    public string PlaySessionId
    {
      get;
      set;
    }

    public string AccountId
    {
      get;
      set;
    }

    public string TerminalId
    {
      get;
      set;
    }
  }
}
