﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;

namespace GameGatewaySimulator.Data.Entities
{
  public class PlaySessionData
  {
    public long PlaySessionId
    {
      get;
      set;
    }

    public int TerminalId
    {
      get;
      set;
    }

    public string TerminalName
    {
      get;
      set;
    }

    public long AccountId
    {
      get;
      set;
    }

    public string AccountName
    {
      get;
      set;
    }

    public int MachineCredit
    {
      get;
      set;
    }

    public decimal AccountBalance
    {
      get;
      set;
    }

    public PlaySessionStatus PlaySessionStatus
    {
      get;
      set;
    }
  }
}
