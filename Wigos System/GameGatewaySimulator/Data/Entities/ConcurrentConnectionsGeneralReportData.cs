﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Data.Entities
{
  public class ConcurrentConnectionsGeneralReportData : INotifyPropertyChanged
  {
    public string PlaySessionId
    {
      get;
      set;
    }

    private int totalBets;

    public int TotalBets
    {
      get
      {
        return totalBets;
      }
      set
      {
        if (value != totalBets)
        {
          this.totalBets = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int totalServerCalls;
    public int TotalServerCalls
    {
      get
      {
        return totalServerCalls;
      }
      set
      {
        if (value != totalServerCalls)
        {
          this.totalServerCalls = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int totalLogins;

    public int TotalLogins
    {
      get
      {
        return totalLogins;
      }
      set
      {
        if (value != totalLogins)
        {
          this.totalLogins = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int totalWins;

    public int TotalWins
    {
      get
      {
        return totalWins;
      }
      set
      {
        if (value != totalWins)
        {
          this.totalWins = value;
          NotifyPropertyChanged();
        }
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    // This method is called by the Set accessor of each property.
    // The CallerMemberName attribute that is applied to the optional propertyName
    // parameter causes the property name of the caller to be substituted as an argument.
    private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
      }
    }
  }
}
