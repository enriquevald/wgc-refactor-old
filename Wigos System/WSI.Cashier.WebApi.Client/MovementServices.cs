﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using WSI.Cashier.Client.Interfaces;
using WSI.Cashier.Client.Interfaces.Messages;
using WSI.Cashier.WebApi.Client.Base;

namespace WSI.Cashier.WebApi.Client
{
    public class MovementServices : BaseWebApiClient, IMovementServices, IDisposable
    {
      WebClient client = new WebClient();

      public MovementServices()
        : base("movements")
      {

      }
      
      public IEnumerable<Movement> GetMovements()
      {
        base.Action = "list";
        var urlRequest = base.UrlRequest;

#if DEBUD
        Trace.WriteLine(string.Format("Request: {0}", urlRequest));
#endif
        var responseJson = client.DownloadString(urlRequest);

#if DEBUD
        Trace.WriteLine(string.Format("Response: {0}", responseJson));
#endif
        var response = JsonConvert.DeserializeObject<IEnumerable<Movement>>(responseJson);
        return response;
      }

      public void Dispose()
      {
        client.Dispose();
      }
    }
}
