﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace WSI.Cashier.WebApi.Client.Base
{
  public abstract class BaseWebApiClient
  {
    public string ApplicationName
    {
      get
      {
        if (ConfigurationManager.AppSettings["api:appName"] == null)
          throw new Exception("There is not an api:appName configuration in the appSettings section");
        return ConfigurationManager.AppSettings["api:appName"];
      }
    }
    public string EndPoint
    { 
      get
      {
        if (ConfigurationManager.AppSettings["api:endPoint"] == null)
          throw new Exception("There is not an api:endPoint configuration in the appSettings section");                        
        return ConfigurationManager.AppSettings["api:endPoint"];
      }
    }
    public string EndPointPort
    {
      get
      {
        if (ConfigurationManager.AppSettings["api:endPointPort"] == null)
          throw new Exception("There is not an api:endPointPort configuration in the appSettings section");
        return ConfigurationManager.AppSettings["api:endPointPort"];
      }
    }
    public string Controller { get; set; }
    public string Action { get; set; }

    public string UrlRequest { 
      get
      {
        return string.Format("{0}:{1}/{2}/{3}/{4}",           
          EndPoint,           
          EndPointPort,           
          ApplicationName,           
          Controller,           
          Action);
      } 
    }
    
    public BaseWebApiClient(string controller)
    {
      this.Controller = controller;
    }
  }
}
