﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace WSI.Cashier.WebApi.Client.Helpers
{
  public static class MessageHandler
  {
    public static string SendPost(string urlRequest, object message)
    {
      var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlRequest);
      httpWebRequest.ContentType = "application/json";
      httpWebRequest.Method = "POST";

      using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
      {
        string json = JsonConvert.SerializeObject(message);

        streamWriter.Write(json);
        streamWriter.Flush();
        streamWriter.Close();
      }

      var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
      using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
      {
        var result = streamReader.ReadToEnd();
        return result;
      }
    }
  }
}
