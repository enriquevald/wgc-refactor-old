﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using WSI.Cashier.Client.Interfaces;
using WSI.Cashier.Client.Interfaces.Messages;
using WSI.Cashier.WebApi.Client.Base;
using WSI.Cashier.WebApi.Client.Helpers;

namespace WSI.Cashier.WebApi.Client
{
    public class SessionServices : BaseWebApiClient, ISessionServices, IDisposable
    {
      WebClient client = new WebClient();

      public SessionServices() : base("session")
      {

      }

      public bool OpenSession(string name, DateTime openDate)
      {
        base.Action = "open";
        var urlRequest = base.UrlRequest;

#if DEBUD
        Trace.WriteLine(string.Format("Request: {0}", urlRequest));
#endif
        var session = new Session()
          {
            Name = name,
            OpeningDate = openDate
          };

        var result = MessageHandler.SendPost(urlRequest, session);

        return true;
      }


      public IEnumerable<Session> GetSessions()
      {
        base.Action = "list";
        var urlRequest = base.UrlRequest;

#if DEBUD
        Trace.WriteLine(string.Format("Request: {0}", urlRequest));
#endif
        var responseJson = client.DownloadString(urlRequest);

#if DEBUD
        Trace.WriteLine(string.Format("Response: {0}", responseJson));
#endif
        var response = JsonConvert.DeserializeObject<IEnumerable<Session>>(responseJson);
        return response;
      }

      public void Dispose()
      {
        client.Dispose();
      }
    }
}
