﻿//
//  Class used by ServiceHost
//     this class is used to instanciate the main webservice for the blacklist proxy service
//
//     class holds its own serializers for types used
//
using WSI.Entrances.BlackList.Model;
using WSI.Entrances.BlackList.Model.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using WigosCommon;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Common.Entrances.Enums;

namespace WSI.Entrances.BlackList
{
  public class BlackListService
  {
    private const string CONNECTOR_NAME = "BlackListConnector";
    readonly List<IBlackListConnector> m_found_types;

    public BlackListService()
    {
      DynamicLibraryManager<IBlackListConnector> _lib_manager;


      _lib_manager = new DynamicLibraryManager<IBlackListConnector>();

      m_found_types = new List<IBlackListConnector>();

      m_found_types.AddRange(
        _lib_manager.LoadAllCompatibleTypes(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
          CONNECTOR_NAME).Values);

      m_found_types.Sort();
    }

    public BlackListServiceResult CheckDocumentsAreNotBlackListed(BlackListDocuments Documents)
    {
      BlackListServiceResult _result = new BlackListServiceResult { FoundResults = false };


      foreach (var _found_type in m_found_types)
      {
        BlackListServiceResult _this_result = _found_type.AreAnyDocumentsBlackListed(Documents.ToDictionary());
	      if (!_this_result.FoundResults) continue;
	      _result.FoundResults = true;
	      List<CustomersResult> _results_complete = new List<CustomersResult>();
	      if(_result.Results!=null)
		      foreach (var _customer_result in _result.Results)
		      {
			      var _res = _results_complete.Find(_x => _x.GetHashCode() == _customer_result.GetHashCode());
			      if (_res == null)
				      _results_complete.Add(_customer_result);
			      else
			      {
				      _res.Message = (_res.Message ?? "") + "\\" + _customer_result.Message;
				      if (!_customer_result.NotPermittedToEnter) continue;
				      _res.NotPermittedToEnter = true;
			      }
		      }
	      foreach (var _customer_result in _this_result.Results)
	      {
					var _res = _results_complete.Find(_x => _x.GetHashCode() == _customer_result.GetHashCode());
					if (_res == null)
						_results_complete.Add(_customer_result);
					else
					{
						_res.Message = (_res.Message ?? "") + "\\" + _customer_result.Message;
						_res.ListName = (_res.ListName ?? "") + "\\" + _customer_result.ListName;
						if (!_customer_result.NotPermittedToEnter) continue;
						_res.NotPermittedToEnter = true;
					}
				}
	      _result.Results = _results_complete.ToArray();
      }
      return _result;
    
    }

    public void RefreshCache(BlackListDocuments Documents)
    {

      foreach (var _found_type in m_found_types)
      {
        if ((ENUM_BLACKLIST_SOURCE)_found_type.SourceType == (ENUM_BLACKLIST_SOURCE)Documents.Source)
          _found_type.RefreshCache();
      }
    }

  }
}
