﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : BlackList.cs
// 
//   DESCRIPTION : Class used by blacklist service for checking if customer 
//                  documents are in the local black list
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-FEB-2016 SJA    Product Backlog Item 9046:Visitas / Recepción: BlackLists
// 30-MAR-2016 YNM    Fixed Bug 11212:Recepción: logs errors on cashier
//------------------------------------------------------------------------------
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities.Extensions;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;

namespace BlackListConnectorLocal
{
  public class BlackList : BlackListConnector
  {
    #region CONSTANTS

		private static string TABLE_NAME = "[blacklist_file_imported] join [blacklist_file_imported_type] on [blacklist_file_imported].[blkf_id_type] = [blacklist_file_imported_type].[bklt_id_type]";
    private static string TABLE_COLUMN_ID = "[blkf_id]";
    private static string TABLE_COLUMN_NAME = "[blkf_name]";
    private static string TABLE_COLUMN_MIDDLE_NAME = "[blkf_middle_name]";
    private static string TABLE_COLUMN_LASTNAME_1 =  "[blkf_lastname_1]";
    private static string TABLE_COLUMN_LASTNAME_2 =  "[blkf_lastname_2]";
    private static string TABLE_COLUMN_NAME_META = "[blkf_metaphone_key_name]";
    private static string TABLE_COLUMN_MIDDLE_NAME_META = "[blkf_metaphone_key_middle_name]";
    private static string TABLE_COLUMN_LASTNAME_1_META = "[blkf_metaphone_key_lastname_1]";
    private static string TABLE_COLUMN_LASTNAME_2_META = "[blkf_metaphone_key_lastname_2]";
    private static string TABLE_COLUMN_DOC_TYPE = "[blkf_document_type]";
    private static string TABLE_COLUMN_DOC = "[blkf_document]";
    private static string TABLE_COLUMN_EXCLUSION_DATE = "[bklf_exclusion_date]";
    private static string TABLE_COLUMN_REASON_TYPE = "[bklf_reason_type]";
    private static string TABLE_COLUMN_REASON_DESCRIP = "[bklf_reason_description]";
    private static string TABLE_COLUMN_EXCLUSION_DURATION = "[bklf_exclusion_duration]";
    private static string TABLE_COLUMN_ORIGIN = "[bklf_origin]";
	  private static string TABLE_COLUMN_ORIGIN_NAME = "[bklt_name]";
		private static string TABLE_COLUMN_MESSAGE = "[bklt_message]";
		private static string TABLE_COLUMN_NOT_PERMITTED_TO_ENTER = "[bklt_entrance_allowed]";

    #endregion

    #region PROPERTIES

    //private static readonly object m_locker = new object();

    //private static bool m_cache_needs_initilization = true;

    private static readonly string[] m_fields_array = new[]
      {
        TABLE_COLUMN_ID, TABLE_COLUMN_NAME,  TABLE_COLUMN_MIDDLE_NAME, TABLE_COLUMN_LASTNAME_1, TABLE_COLUMN_LASTNAME_2,
        TABLE_COLUMN_DOC_TYPE, TABLE_COLUMN_DOC, TABLE_COLUMN_EXCLUSION_DATE, TABLE_COLUMN_REASON_TYPE,
        TABLE_COLUMN_REASON_DESCRIP, TABLE_COLUMN_EXCLUSION_DURATION , TABLE_COLUMN_ORIGIN ,TABLE_COLUMN_ORIGIN_NAME, TABLE_COLUMN_MESSAGE,TABLE_COLUMN_NOT_PERMITTED_TO_ENTER
      };

    private static readonly string m_fields = string.Join(",", m_fields_array);

    //private static long m_last_id;

    //private static DataTable m_dt;

    /// <summary>
    /// Refresh thread
    /// </summary>
    //private static readonly System.Threading.Timer m_refresh_thread = new System.Threading.Timer(NeedRefresh);

    #endregion

    #region CONSTRUCTOR

    /// <summary>
    /// Constructor, calls cache initialization
    /// </summary>
    public BlackList()
    {
      //lock (m_locker)
      //{
      //  if (m_cache_needs_initilization)
      //  {
      //  InitializeCache();
      //}
      //}
      //m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
      }

    #endregion

    #region PUBLIC FUNCTIONS

    /// <summary>
    /// The calling order of this DLL
    /// </summary>
    public override int CallOrder
    {
      get { return 2; }
    }

    /// <summary>
    /// Indicates the source of data
    /// </summary>
    public override int SourceType
    {
      get { return (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_FILE; }
    }

    /// <summary>
    /// Method that checks if any of the documents are in the cache
    /// </summary>
    /// <param name="Documents"></param>
    /// <returns></returns>
    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      BlackListServiceResult _result = null;
      //if (m_dt == null || m_dt.Rows.Count == 0)
      //{
      //  return _result;
      //}

      //lock (m_locker)
      {
        //m_dt.CaseSensitive = false;

        foreach (var _document in Documents)
        {
          if (_document.Key >= (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE &&
              _document.Key < (int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER)
          {
            int _docid = _document.Key - (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE;
            if (!Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid)) continue;
              string _docnum = Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid];
            if (string.IsNullOrEmpty(_docnum)) continue;
                _docnum = GetDocWithoutLeters(_docnum);
            if (!string.IsNullOrEmpty(_docnum))
              using (DB_TRX _trx = new DB_TRX())
              {
                using (
                  SqlCommand _cmd =
                    new SqlCommand(
                      string.Format("SELECT {0} FROM {1} where " + TABLE_COLUMN_DOC_TYPE + "=" + _docid + " AND " +
                                    TABLE_COLUMN_DOC + "like '%" +
                                    EscapeLikeValue(_docnum.Trim()) + "%'", m_fields, TABLE_NAME)))
                {
                  _cmd.Transaction = _trx.SqlTransaction;
                  _cmd.Connection = _trx.SqlTransaction.Connection;
                  using (SqlDataReader _dr = _cmd.ExecuteReader())
                  {
                    _result = BuildBlackListServiceResult(_dr);
                }
              }
            }
            if (_result != null)
              break;
          }

        }
        //  if (Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE) && Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER)
        //      && int.TryParse(Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE], out _intval) && !Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER].IsNullOrWhiteSpace())
        //  {
        //    DataRow[] _v1 =
        //        m_dt.Select(TABLE_COLUMN_DOC_TYPE + "=" + _intval + " AND " +
        //                    TABLE_COLUMN_DOC + "='" + EscapeLikeValue(Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER].Trim()) + "'");
        //    if (_v1.Length > 0)
        //    {
        //      _result = BuildBlackListServiceResult(_v1);
        //  }
        //}
        string _select_string = "";
        if (_result != null) return _result;
        if (!GeneralParam.Get("BlackListService", "PhoneticSearchMode", true))
        {
          string[] _name =
          {
            Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty,
            Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty,
            Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty,
            Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty,
          };

          _select_string = (!String.IsNullOrEmpty(_name[0])
            ? TABLE_COLUMN_NAME + "='" + EscapeLikeValue(_name[0]) + "' AND "
            : "") +
                                  (!String.IsNullOrEmpty(_name[1])
                                    ? TABLE_COLUMN_MIDDLE_NAME + "='" + EscapeLikeValue(_name[1]) + "' AND "
                                    : "") +
                                  (!String.IsNullOrEmpty(_name[2])
                                    ? TABLE_COLUMN_LASTNAME_1 + "='" + EscapeLikeValue(_name[2]) + "' AND "
                                    : "") +
                                  (!String.IsNullOrEmpty(_name[3])
                                    ? TABLE_COLUMN_LASTNAME_2 + "='" + EscapeLikeValue(_name[3]) + "' AND "
                                    : "");
          if (!String.IsNullOrEmpty(_select_string))
            _select_string = _select_string.Length > 4
              ? _select_string.Remove(_select_string.Length - 4, 4)
              : _select_string;

        }
        else
        {
          _select_string =
            TABLE_COLUMN_NAME_META + "=dbo.MetaPhone('" + EscapeLikeValue(Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty) + "') AND "
            + TABLE_COLUMN_MIDDLE_NAME_META + "=dbo.MetaPhone('" +
            EscapeLikeValue(Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty) + "') AND "
            + TABLE_COLUMN_LASTNAME_1_META + "=dbo.MetaPhone('" +
            EscapeLikeValue(Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty) + "') AND "
            + TABLE_COLUMN_LASTNAME_2_META + "=dbo.MetaPhone('" +
            EscapeLikeValue(Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2)
              ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2] ?? string.Empty).Trim().ToUpperInvariant()
              : string.Empty) + "')";
        }

        if (String.IsNullOrEmpty(_select_string))
          return new BlackListServiceResult() {FoundResults = false};
        using (DB_TRX _trx = new DB_TRX())
            {
          using (
            SqlCommand _cmd =
              new SqlCommand(
                string.Format("SELECT {0} FROM {1} where " + _select_string, m_fields, TABLE_NAME)))
          {
            _cmd.Transaction = _trx.SqlTransaction;
            _cmd.Connection = _trx.SqlTransaction.Connection;
            using (SqlDataReader _dr = _cmd.ExecuteReader())
            {
              _result = BuildBlackListServiceResult(_dr);
            }
          }
        }
      }
      return _result ?? new BlackListServiceResult(){FoundResults = false};
    }

    private string GetDocWithoutLeters(string Doc)
    {
      string _doc= Doc;
      int _n;

      if(!int.TryParse(_doc.Substring(0,1), out _n)) _doc=_doc.Substring(1,_doc.Length-1);
      if(!int.TryParse(_doc.Substring(_doc.Length -1,1), out _n)) _doc=_doc.Substring(0, _doc.Length-1); 

      return _doc;
    }

    /// <summary>
    /// allows refresh cache whenever is needed
    /// </summary>
    //public override void RefreshCache()
    //{
    //  lock (m_locker)
    //  {
    //    InitializeCache();
    //  }
    //  m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
    //}

    /// <summary>
    /// Dispose class
    /// </summary>
    public override void Dispose()
    {
    }  
    #endregion

    #region PRIVATE FUNCTIONS

    /// <summary>
    /// Method to initialize cache
    /// </summary>
    //private static void InitializeCache()
    //{
    //  lock (m_locker)
    //  {
    //    m_dt = new DataTable();

    //    using (DB_TRX _trans = new DB_TRX())
    //    {
    //      using (
    //        SqlDataAdapter _rdr = new SqlDataAdapter(string.Format("SELECT {0} FROM {1}", m_fields, TABLE_NAME),
    //          _trans.SqlTransaction.Connection))
    //      {
    //        _rdr.SelectCommand.Transaction = _trans.SqlTransaction;
    //        _rdr.Fill(m_dt);
    //      }
    //      m_last_id = GetChangeMonitorValue(_trans);
    //    }
    //    m_cache_needs_initilization = false;
    //  }
    //}

    /// <summary>
    /// Periodically checks if a cache refresh is needed
    /// </summary>
    /// <param name="State"></param>
    //private static void NeedRefresh(object State)
    //{
    //  long _last_id;
    //  m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
    //  using (DB_TRX _trans = new DB_TRX())
    //  {
    //    _last_id = GetChangeMonitorValue(_trans);
    //  }
    //  if (_last_id != m_last_id)
    //    lock (m_locker)
    //    {
    //      InitializeCache();
    //    }
    //  m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
    //}

    /// <summary>
    /// Form a BlackListServiceResult object from the given data row
    /// </summary>
    /// <param name="Row"></param>
    /// <returns></returns>
    private static BlackListServiceResult BuildBlackListServiceResult(SqlDataReader Result)
    {
      var _resultlist = new List<CustomersResult>();
      BlackListServiceResult _result = null;
      while (Result.Read())
      {
      
        if (_result == null) _result = new BlackListServiceResult() {FoundResults = true};
        CustomersResult _customer = new CustomersResult
        {
          CustumerDocType =  Result.GetInt16(Array.IndexOf(m_fields_array, TABLE_COLUMN_DOC_TYPE)),
          CustumerDocNmbr =  Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_DOC)),
          CustomerFullName = Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_NAME)) + " " +
                             Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_MIDDLE_NAME)) + " " +
                             Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_LASTNAME_1)) + " " +
                             Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_LASTNAME_2)) + " ",
          BlackListSource = (int) ENUM_BLACKLIST_SOURCE.EXTERNAL_FILE,
          ExclusionDate = Result.GetDateTime(Array.IndexOf(m_fields_array, TABLE_COLUMN_EXCLUSION_DATE)),
          ReasonType =  Result.GetInt16(Array.IndexOf(m_fields_array, TABLE_COLUMN_REASON_TYPE)),
          ReasonDescription =  Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_REASON_DESCRIP)),
					AccountId = "",
          ListName = Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_ORIGIN_NAME)),
          Message =  Result.GetString(Array.IndexOf(m_fields_array, TABLE_COLUMN_MESSAGE)),
          NotPermittedToEnter =
            ! Result.GetBoolean(Array.IndexOf(m_fields_array, TABLE_COLUMN_NOT_PERMITTED_TO_ENTER)),
				};
        _resultlist.Add(_customer);
      }
      if (_resultlist.Count <= 0 || _result == null) return _result;
      _result.Results = _resultlist.ToArray();
      _result.Count = _resultlist.Count;
      return _result;
    }

    /// <summary>
    /// Gets the value in the DB that shows the cache has changed
    /// </summary>
    /// <param name="Trans"></param>
    /// <returns></returns>
    private static long GetChangeMonitorValue(DB_TRX Trans)
    {

      using (
        SqlCommand _cmd =
          new SqlCommand(string.Format("SELECT TOP(1) {0} FROM {1} ORDER BY {0} DESC", m_fields_array[0], TABLE_NAME))
        )
      {
        var _res = Trans.ExecuteScalar(_cmd);
        return Convert.ToInt64(_res);
      }

    }

    /// <summary>
    /// Validates characters on strings recived
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    private string EscapeLikeValue(string Value)
    {
      StringBuilder sb = new StringBuilder(Value.Length);
      for (int i = 0; i < Value.Length; i++)
      {
        char c = Value[i];
        switch (c)
        {
          case ']':
          case '[':
          case '%':
          case '*':
            sb.Append("[").Append(c).Append("]");
            break;
          case '\'':
            sb.Append("''");
            break;
          default:
            sb.Append(c);
            break;
        }
      }
      return sb.ToString();
    }
    #endregion

    public override void RefreshCache()
    {
      return;
    }
  }
}
