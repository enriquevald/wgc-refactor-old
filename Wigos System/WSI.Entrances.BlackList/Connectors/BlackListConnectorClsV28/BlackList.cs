﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Xml;
using BlackListConnectorClsV28.CLSV28;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;

namespace BlackListConnectorClsV28
{
    public class BlackList : BlackListConnector
  {
    #region CONSTANTS

    #endregion

    #region PROPERTIES

    #endregion

    #region CONSTRUCTOR

    #endregion

    #region PUBLIC FUNCTIONS

    /// <summary>
    /// The calling order of this DLL
    /// </summary>
    public override int CallOrder
    {
      get { return 7; }
    }

    /// <summary>
    /// Indicates the source of data
    /// </summary>
    public override int SourceType
    {
      get { return (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS; }
    }

    /// <summary>
    /// Method that checks if the given documents exist in the cache
    /// </summary>
    /// <param name="Documents"></param>
    /// <returns></returns>
    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      string _docnum = "";

      XmlDocument _xml_dom;
      XmlNode _xml_node;
      string user;
      string pass;
      
      swV28 _svc = new swV28();
      _svc.Url = GeneralParam.GetString("ClsV28", "url", @"http://127.0.0.1/Validar.asmx");
      user = GeneralParam.GetString("ClsV28", "user", "prueba");
      pass = GeneralParam.GetString("ClsV28", "pass", "bilbao2011");

      BlackListServiceResult _result = new BlackListServiceResult { FoundResults = false };
      
      try
      {
        foreach (var _document in Documents)
        {
          _docnum = "";
          CustomersResult _cr = null;
          if (_document.Key >= (int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE &&
              _document.Key < (int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER)
          {
            int _docid = _document.Key - (int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE;
            if (Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid))
            {
              _docnum = Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid];
            }

            if (string.IsNullOrWhiteSpace(_docnum))
              continue;


            _xml_dom = new XmlDocument();
            string _xmlresult = _svc.ValidarAcceso(user,pass,_docnum);
            _xml_dom.LoadXml(_xmlresult);



            _xml_node = _xml_dom.SelectSingleNode("//esProhibido");
            if (_xml_node != null && ((XmlElement)_xml_node).InnerText.ToUpperInvariant() == "S")
            {
              _cr = new CustomersResult()
              {
                BlackListSource = (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS,
                CustomerFullName = "",
                CustumerDocNmbr = "",
                ExclusionDate = DateTime.Now,
                CustumerDocType = (int)ENUM_ID_DOC_TYPES.ID_DOC_TYPE_DNI,
                ListName = "CLSv28",
                NotPermittedToEnter = true,
                Message = "Prohibido",
                ReasonDescription = "Prohibido",
                ReasonType = 3
              };
            }
            else
            {
              _xml_node = _xml_dom.SelectSingleNode("//DescError");
              if (_xml_node != null)
              {
                _cr = new CustomersResult()
                {
                  BlackListSource = (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS,
                  CustomerFullName = "",
                  CustumerDocNmbr = "",
                  ExclusionDate = DateTime.Now,
                  CustumerDocType = (int)ENUM_ID_DOC_TYPES.ID_DOC_TYPE_DNI,
                  ListName = "CLSv28",
                  NotPermittedToEnter = true,
                  Message = "Error de acceso al servicio web",
                  ReasonDescription = ((XmlElement)_xml_node).InnerText.ToUpperInvariant(),
                  ReasonType = 3
                };
              }
            }
          }
          if (_cr == null) continue;
          _result.FoundResults = true;
          _result.Count = _result.Count + 1;


          if (_result.Results == null || _result.Results.Length == 0)
          {
            _result.Results = new []
          {
                _cr
              };
          }
          else
          {
            var v = new CustomersResult[_result.Results.Length + 1];
            Array.Copy(_result.Results, v, _result.Results.Length);
            v[_result.Results.Length] = _cr;
            _result.Results = v;
          }


          }
        }
      catch
        (Exception _ex)
      {
        WSI.Common.Log.Error("Error in Blacklist ClsV28 connector accessing component");
        WSI.Common.Log.Exception(_ex);
      }
      return _result;
      
    }

    /// <summary>
    /// allows refresh cache whenever is needed
    /// </summary>
    public override void RefreshCache()
    { 

    }
    /// <summary>
    /// Dispose class
    /// </summary>
    public override void Dispose()
    {
      //clean hard objects
    }

    #endregion

    #region PRIVATE FUNCTIONS
    #endregion
  }
}
