﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common.Entrances.Enums;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;
using WSI.Common;

namespace BlacklistConnectorPaisVasco
{
  public class BlackList : BlackListConnector
    {

      public class MyMessageInspector : IClientMessageInspector
      {
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
          //MessageBox.Show(reply.ToString(), "reply");
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
          var x = request.ToString();
          //MessageBox.Show(x, "request");
          return request;
        }
      }
      public class InspectorBehavior : IEndpointBehavior
      {
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
          clientRuntime.MessageInspectors.Add(new MyMessageInspector());
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
          throw new NotImplementedException();
        }

        public void Validate(ServiceEndpoint endpoint)
        {

        }
      }
      public class SecurityHeader : MessageHeader
      {
        private readonly UsernameToken _usernameToken;

        public SecurityHeader(string username, string password)
        {
          _usernameToken = new UsernameToken(username, password);
        }

        public override string Name
        {
          get { return "Security"; }
        }

        public override string Namespace
        {
          get { return "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"; }
        }

        protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
          XmlSerializer serializer = new XmlSerializer(typeof(UsernameToken));
          serializer.Serialize(writer, _usernameToken);
        }
      }

      [XmlRoot(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
      public class UsernameToken
      {
        public UsernameToken()
        {
        }

        public UsernameToken(string username, string password)
        {
          Username = username;
          Password = new Password() { Value = password };
        }

        [XmlElement]
        public string Username { get; set; }

        [XmlElement]
        public Password Password { get; set; }
      }

      public class Password
      {
        public Password()
        {
          Type = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
        }

        [XmlAttribute]
        public string Type { get; set; }

        [XmlText]
        public string Value { get; set; }
      }

    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      BlackListServiceResult _result = new BlackListServiceResult {FoundResults = false};
      try
      {
        string _docnum;
        string _resp;
        XmlDocument _xmldoc;
        XmlNode _xml_node;
        string _account_id;
        string _name_string;
        int _id_type_id = -99;
        string _dob = "";

        _docnum = "";
        _xmldoc = new XmlDocument();

        _account_id = string.Empty;

        if (Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.ACCOUNT))
        {
          _account_id = Documents[(int) ENUM_BLACKLIST_DOC_TYPE.ACCOUNT];
        }

        string[] _name =
        {
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
        };
        _name_string = "";
        foreach (string _s in _name)
        {
          if (!string.IsNullOrEmpty(_s))
          {
            _name_string += (_name_string.Length > 0 ? " " : "") + _s;
          }
        }

        foreach (var _document in Documents)
        {
          try
          {
            CustomersResult cr = null;
            _docnum = "";
            if (_document.Key >= (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE &&
                _document.Key < (int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER)
            {
              int _docid = _document.Key - (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE;
              _id_type_id = Int32.Parse(_document.Value);
              if (Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid))
              {
                _docnum = Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid];
              }

              if (Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH))
              {
                _dob = DateTime.Parse(Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH]).ToString("yyyyMMdd");
              }
              if (string.IsNullOrWhiteSpace(_docnum))
                continue;
              //Log.Message(string.Format("Doc num {0}", _docnum));
              _resp = FindPersonInBlackList(_docnum, _id_type_id, _dob);
              if (_resp == string.Empty) continue;
              _xmldoc.LoadXml(_resp);

              //_xml_node = _xmldoc.SelectSingleNode("//prohibida");
              _xml_node = _xmldoc.SelectSingleNode("//esProhibido");
              if (_xml_node != null && ((XmlElement) _xml_node).InnerText.ToUpperInvariant() == "S")
              {
                cr = new CustomersResult()
                {
                  BlackListSource = (int) ENUM_BLACKLIST_SOURCE.EXTERNAL_WS,
                  CustomerFullName = "",
                  CustumerDocNmbr = "",
                  ExclusionDate = DateTime.Now,
                  CustumerDocType = (int) ENUM_ID_DOC_TYPES.ID_DOC_TYPE_DNI,
                  ListName = "País Vasco",
                  NotPermittedToEnter = true,
                  Message = "Prohibido",
                  ReasonDescription = "Prohibido",
                  ReasonType = 3
                };
              }
              else
              {
                _xml_node = _xmldoc.SelectSingleNode("//DescError");
                if (_xml_node == null)
                {
                  _xml_node = _xmldoc.SelectSingleNode("//idError");
                }
                if (_xml_node != null)
                {
                  cr = new CustomersResult()
                  {
                    BlackListSource = (int) ENUM_BLACKLIST_SOURCE.EXTERNAL_WS,
                    CustomerFullName = "",
                    CustumerDocNmbr = "",
                    ExclusionDate = DateTime.Now,
                    CustumerDocType = (int) ENUM_ID_DOC_TYPES.ID_DOC_TYPE_DNI,
                    ListName = "País Vasco",
                    NotPermittedToEnter = false,
                    Message = "Error de acceso al servicio web",
                    ReasonDescription = ((XmlElement) _xml_node).InnerText.ToUpperInvariant(),
                    ReasonType = (Int32)ENUM_BLACKLIST_REASON.GENERIC_ERROR //Generic Error
                  };
                }
              }

              Log.Message("BlacklistConnectorPaisVasco.BlackList " + _resp);
            }
            if (cr == null) continue;
            _result.FoundResults = true;
            _result.Count = _result.Count + 1;


            if (_result.Results == null || _result.Results.Length == 0)
            {
              _result.Results = new CustomersResult[]
              {
                cr
              };
            }
            else
            {
              var v = new CustomersResult[_result.Results.Length + 1];
              Array.Copy(_result.Results, v, _result.Results.Length);
              v[_result.Results.Length] = cr;
              _result.Results = v;
            }
          }
          catch
          {
            //Log.Message(string.Format("{0}, {1} = {2}", _document.Key, _document.Value, ex.Message));
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      //break;
      return _result;
    }

    public override int CallOrder
      {
        get { return 1; }
      }

      public override void Dispose()
      {
      }

      public override void RefreshCache()
      {
      }

      public override int SourceType
      {
        get { return  (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS; }
      }

      #region Private
      private string FindPersonInBlackList(string NumDocument, int TypeDocument, string BirthDate)
      {
        string _ip;
        string _user;
        string _psw;
       // string _file;
        XmlDocument _xml_doc;
        string _resp;
        int _idDni;
        int _idPasaporte;
        int _idcif;
        string _typeDocument;
        StringBuilder _sb;

        _sb = new StringBuilder();
        _ip = GeneralParam.GetString("BlackListService.PaisVasco", "PaisVascoIP");
        _user = GeneralParam.GetString("BlackListService.PaisVasco", "PaisVascoUser");
        _psw = GeneralParam.GetString("BlackListService.PaisVasco", "PaisVascoPsw");
        _idDni = GeneralParam.GetInt32("BlackListService.PaisVasco", "IdDNI");
        _idPasaporte = GeneralParam.GetInt32("BlackListService.PaisVasco", "IdPasaporte");
        _idcif = GeneralParam.GetInt32("BlackListService.PaisVasco", "IdOtro");
        _xml_doc = new XmlDocument();
        _xml_doc.PreserveWhitespace = true;
        _resp = string.Empty;

        if (TypeDocument == _idDni) _typeDocument = "DNI";
        else if (TypeDocument == _idPasaporte) _typeDocument = "NIE";
        else _typeDocument = "NIF";

        if (string.IsNullOrWhiteSpace(BirthDate))
        {
          BirthDate = "19700101"; // Mandatory for WS of the PaisVasco
        }

        _sb.Append("<xml-fragment>");
        _sb.Append("<usuario>");
        _sb.Append(_user);
        _sb.Append("</usuario>");
        _sb.Append("<password>");
        _sb.Append(_psw);
        _sb.Append("</password>");
        _sb.Append("<metodo>validarAcceso</metodo>");
        _sb.Append("<dni>");
        _sb.Append(NumDocument);
        _sb.Append("</dni>");
        _sb.Append("<tipoDocumento>");
        _sb.Append(_typeDocument);
        _sb.Append("</tipoDocumento>");
        _sb.Append("<fechaNacimiento>");
        _sb.Append(BirthDate);
        _sb.Append("</fechaNacimiento>");
        _sb.Append("</xml-fragment>");

        try
        {
          var client = new ServiceReference1.v33aaServicioWSPortClient(new BasicHttpBinding(),
            new EndpointAddress(_ip));
          using (new OperationContextScope(client.InnerChannel))
          {
            OperationContext.Current.OutgoingMessageHeaders.Add(
                new SecurityHeader(_user, _psw));

            _resp = client.procesarLlamada(_sb.ToString(), "");
          }
        }
        catch (TimeoutException ex)
        {
          Log.Exception(ex);
          _resp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Respuesta><metodo>validarAcceso</metodo><dni>00000000T</dni><prohibida></prohibida><errorConsulta><IdError>5</IdError><DescError>Tiempo de espera del servicio web superado</DescError></errorConsulta></Respuesta >";
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          _resp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Respuesta><metodo>validarAcceso</metodo><dni>00000000T</dni><prohibida></prohibida><errorConsulta><IdError>5</IdError><DescError>Error accediendo al servicio web</DescError></errorConsulta></Respuesta >";
        }

        return _resp;
      }
      #endregion
    }
}
