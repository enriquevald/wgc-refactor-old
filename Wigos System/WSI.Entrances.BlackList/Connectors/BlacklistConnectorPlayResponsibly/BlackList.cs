﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using BlacklistConnectorPlayResponsibly.mt.org.playresponsibly.selfbarring;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;

namespace BlacklistConnectorPlayResponsibly
{
  public class BlackList : BlackListConnector
    {
    private SelfBarringService m_barring_service;

    public BlackList()
    {
      m_barring_service = new SelfBarringService
      {
        Url =
          GeneralParam.GetString("BlackListService.PRMalta", "Url",
            "https://playresponsibly.org.mt/ws/selfbarringservice.asmx"),
        Credentials =
          new NetworkCredential(GeneralParam.GetString("BlackListService.PRMalta", "User", "sb_winsystems"),
            GeneralParam.GetString("BlackListService.PRMalta", "Password", "w1nsyst3m$"))
      };
      //m_barring_service.Credentials = new NetworkCredential("bobs","youruncle");
    }

    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      try
      {
        string _account_id = string.Empty;
        if (Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.ACCOUNT))
        {
          _account_id = Documents[(int) ENUM_BLACKLIST_DOC_TYPE.ACCOUNT];
        }

        BlackListServiceResult _result = new BlackListServiceResult {FoundResults = false};
        string[] _name =
        {
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
          Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2)
            ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2] ?? string.Empty).Trim().ToUpperInvariant()
            : string.Empty,
        };
        string _name_string = "";
        foreach (string _s in _name)
        {
          if (!string.IsNullOrEmpty(_s))
          {
            _name_string += (_name_string.Length > 0 ? " " : "") + _s;
          }
        }
        int _id_type_id;
        string _id_num = "";
        string _pass_num = "";
        string _lic_num = "";
        string _other_id = "";
        string _dob = "";
        foreach (var _document in Documents)
        {
          if (_document.Key < (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE ||
              _document.Key >= (int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER) continue;
          int _docid = _document.Key - (int) ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE;
          _id_type_id = _docid;
          if (!Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid)) continue;
          string _docnum = Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid];
          if (string.IsNullOrEmpty(_docnum)) continue;
          if (_id_type_id == GeneralParam.GetInt32("BlackListService.PRMalta", "IdPassportTypeId", 124))
          {
            _pass_num = _docnum;
          }
          else if (_id_type_id == GeneralParam.GetInt32("BlackListService.PRMalta", "IdNumberTypeId", 91))
          {
            _id_num = _docnum;
          }
          else if (_id_type_id ==
                   GeneralParam.GetInt32("BlackListService.PRMalta", "LicenceNumberTypeId", 124))
          {
            _lic_num = _docnum;
          }
          else
          {
            _other_id = _docnum;
          }
        }
        if (Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH))
        {
          _dob = DateTime.Parse(Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH]).ToString("yyyy/MM/dd");
        }
        int _occurances = 0;

        XmlNode _res;
        try
        {
          //Log.Message("JCALERO v4.0");
          //Log.Message(string.Format("_id_type_id[{4}]||'', _id_num[{0}], _pass_num[{1}], _lic_num[{2}], _other_id[{3}], ''", _id_num, _pass_num, _lic_num, _other_id, _id_type_id));

          
          if(string.IsNullOrEmpty(_id_num))
          {
            _id_num = _lic_num;
          }
          if (string.IsNullOrEmpty(_id_num))
          {
            _id_num = _pass_num;
          }
          if (string.IsNullOrEmpty(_id_num))
          {
            _id_num = _other_id;
          }

          if (string.IsNullOrEmpty(_lic_num))
          {
            _lic_num = _id_num;
          }
          if (string.IsNullOrEmpty(_lic_num))
          {
            _lic_num = _pass_num;
          }
          if (string.IsNullOrEmpty(_lic_num))
          {
            _lic_num = _other_id;
          }

          if (string.IsNullOrEmpty(_pass_num))
          {
            _pass_num = _id_num;
          }
          if (string.IsNullOrEmpty(_pass_num))
          {
            _pass_num = _lic_num;
          }
          if (string.IsNullOrEmpty(_pass_num))
          {
            _pass_num = _other_id;
          }

          _res = m_barring_service.CheckIfBarredEntry("", _id_num, _pass_num, _lic_num, _other_id, "");

          //Log.Message("JCALERO v4.1");
          //Log.Message(string.Format("'', _id_num[{0}], _pass_num[{1}], _lic_num[{2}], _other_id[{3}], ''", _id_num, _pass_num, _lic_num, _other_id));
        }
        catch (Exception _ex)
        {
          Log.Message("Error encountered in PlayResponsibly BlacklistConnector calling barring service ");
          Log.Message("------- Details -------");
          Log.Message(string.Format(
            "Call Parameters CheckIfBarredEntry(\"\", \"{0}\", \"{1}\", \"{2}\", \"{3}\", \"\")", _id_num,
            _pass_num, _lic_num, _other_id));
          Log.Exception(_ex);
          Log.Message("-----------------------");
          throw;
        }
        if (_res == null)
        {
          Log.Message("Error encountered in PlayResponsibly BlacklistConnector calling barring service ");
          Log.Message("------- Details -------");
          Log.Message(string.Format(
            "Call Parameters CheckIfBarredEntry(\"\", \"{0}\", \"{1}\", \"{2}\", \"{3}\", \"\")", _id_num,
            _pass_num, _lic_num, _other_id));
          Log.Message("No Result Xml Received");
          Log.Message("-----------------------");
          throw new Exception("No Response Received");
        }

        if (_res.Attributes != null)
        {
          foreach (XmlAttribute _attr in _res.Attributes)
          {
            if (_attr.Name == "count")
            {
              _occurances = int.Parse(_attr.Value);
            }
          }
        }

        XmlNode _res2;
        try
        {
          _res2 = m_barring_service.CheckIfBarredEntry(_name_string, "", "", "", "", _dob);
        }
        catch (Exception _ex)
        {
          Log.Message("Error encountered in PlayResponsibly BlacklistConnector calling barring service ");
          Log.Message("------- Details -------");
          Log.Message(string.Format("Call Parameters CheckIfBarredEntry(\"{0}\", \"\",\"\",\"\",\"{1}\")", _name_string,
            _dob));
          Log.Exception(_ex);
          Log.Message("-----------------------");
          throw;
        }
        if (_res2 == null)
        {
          Log.Message("Error encountered in PlayResponsibly BlacklistConnector calling barring service ");
          Log.Message("------- Details -------");
          Log.Message(string.Format("Call Parameters CheckIfBarredEntry(\"{0}\", \"\",\"\",\"\",\"{1}\")", _name_string,
            _dob));
          Log.Message("No Result Xml Received");
          Log.Message("-----------------------");
          throw new Exception("No Response Received");
        }

        if (_res2.Attributes != null)
        {
          foreach (XmlAttribute _attr in _res2.Attributes)
          {
            if (_attr.Name == "count")
            {
              _occurances += int.Parse(_attr.Value);
            }
          }
        }
        if (_occurances == 0) return _result;
        _result.Count = _occurances;
        _result.FoundResults = true;
        var _result_matches = new List<CustomersResult>();

        var _res1_list = BuildResults(Documents, _res, _name_string, _account_id);
        var _res2_list = BuildResults(Documents, _res2, _name_string, _account_id);

        _result_matches.AddRange(_res1_list);
        if (_result_matches.Count == 0)
        {
          _result_matches.AddRange(_res2_list);
        }
        _result = new BlackListServiceResult()
        {
          Count = _result_matches.Count,
          FoundResults = true,
          Results = _result_matches.ToArray()

        };



        return _result;
      }
      catch (Exception _ex)
      {
        Log.Message("Error encountered in PlayResponsibly BlacklistConnector");
        Log.Message("------- Details -------");
        Log.Exception(_ex);
        Log.Message("-----------------------");
        throw;
      }

    }

    private List<CustomersResult> BuildResults(IDictionary<int, string> Documents, XmlNode _res, string _name_string,
      string _account_id)
    {
      List<CustomersResult> _result_matches = new List<CustomersResult>();
      foreach (XmlElement _node in
        _res.ChildNodes)
      {
        DateTime _inclusiondate = DateTime.Now;
        string _identstr = Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER)
          ? Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER]
          : "";
        string _foundname = _name_string;
        try
        {
          _foundname = _node.SelectSingleNode("wholename").InnerText;
          foreach (XmlAttribute _attr in _node.Attributes)
          {
            if (_attr.Name == "registration_date")
              _inclusiondate = DateTime.Parse(_attr.Value);
          }
          foreach (XmlElement _ident in _node.SelectSingleNode("IDENTIFICATION").ChildNodes)
          {
            if (_ident.SelectSingleNode("number").InnerText != "N/A")
              _identstr += (_identstr.Length > 0 ? " / " : "") + _ident.SelectSingleNode("number").InnerText;
          }
        }
        catch
        {
          //swallow
        }

        _result_matches.Add(new CustomersResult()
        {
          AccountId = _account_id,
          BlackListSource = 2,
          CustomerFullName = _foundname,
          CustumerDocNmbr = _identstr,
          ReasonDescription = "",
          ReasonType = 3,
          CustumerDocType = -1,
          ExclusionDate = _inclusiondate,
          ListName = "Play Responsibly Self-Barring List",
          Message = "Play Responsibly Self-Barring List",
          NotPermittedToEnter = true
        }
          );
      }
      return _result_matches;
    }

    public override int CallOrder
      {
        get { return 1; }
      }

      public override void Dispose()
      {
        m_barring_service.Dispose();
        m_barring_service = null;
      }

      public override void RefreshCache()
      {
      }

      public override int SourceType
      {
        get { return  (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS; }
      }
    }
}
