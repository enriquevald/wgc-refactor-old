﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : BlackList.cs
// 
//   DESCRIPTION : Class used by blacklist service for checking if customer 
//                  is in the internal black list
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-FEB-2016 SJA    Product Backlog Item 9046:Visitas / Recepción: BlackLists
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities.Extensions;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;

namespace BlackListConnectorInternal
{
  }
    
  

