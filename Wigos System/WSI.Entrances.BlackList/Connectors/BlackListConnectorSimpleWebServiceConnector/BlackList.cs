﻿using BlackListConnectorSimpleWebServiceConnector.SimpleServiceTest;
using WSI.Entrances.BlackList.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackListConnectorSimpleWebServiceConnector
{
  public class BlackList : BlackListConnector
  {
    public override int CallOrder
    {
      get { return 4; }
    }

    public override bool AreAnyDocumentsBlackListed(IDictionary<ENUM_ID_DOC_TYPES, string> Documents)
    {
      bool _retval = false;
      SampleService _auth_service;
      try
      {
        _auth_service = new SampleService();
        _auth_service.Timeout = 1000;
        foreach (var document in Documents)
        {
          try
          {
            var v = _auth_service.login(document.Key.ToString(), document.Value);
            _retval = true;
          }
          catch { }
          if (_retval) { break; }
        }
      }
      catch (Exception _ex) 
      {
        //log exception
      }
      return _retval;
    }
  }
}
