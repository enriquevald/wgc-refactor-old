﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;
using BlacklistConnectorPlayResponsibly.Common;
using BlacklistConnectorPlayResponsibly.Entities.Request;
using BlacklistConnectorPlayResponsibly.Entities.Response;
using System.Reflection;

namespace BlacklistConnectorPlayResponsibly
{
  public class BlackList : BlackListConnector
    {
    
    private Boolean m_is_log_enabled;
    private Boolean log_yet_obtained;
    private string m_url;
    private string m_username;
    private string m_password;
    private string m_source;
    private string m_venue;
    private string m_token;
    private ServiceCaller m_service_caller;
    private string m_authentication_token;

    private Boolean IsLogEnabled { 
      get 
      {
        Boolean logReturn;
        if (!log_yet_obtained)
        {
          log_yet_obtained = true;
          m_is_log_enabled = (GeneralParam.GetInt32("BlackListService.PRMalta", "Log", 0) == 1 ? true : false);
          logReturn = m_is_log_enabled;
        }
        else
          logReturn = m_is_log_enabled;
        return logReturn;
      } 
    }
    private string Url
    {
      get
      {
        string urlReturn;
        if (!string.IsNullOrEmpty(m_url))
        {
          urlReturn = m_url;
        }
        else
        {
          m_url = GeneralParam.GetString("BlackListService.PRMalta", "Url", "http://test.api.playresponsibly.org.mt");
          urlReturn = m_url;
        }
        return urlReturn;
      }
    }
    private string Username
    {
      get
      {
        string userReturn;
        if (!string.IsNullOrEmpty(m_username))
        {
          userReturn = m_username;
        }
        else
        {
          m_username = GeneralParam.GetString("BlackListService.PRMalta", "User", "fairbet");
          userReturn = m_username;
        }
        return userReturn;
      }
    }
    private string Password
    {
      get
      {
        string pwdReturn;
        if (!string.IsNullOrEmpty(m_password))
        {
          pwdReturn = m_password;
        }
        else
        {
          m_password = GeneralParam.GetString("BlackListService.PRMalta", "Password", "Password12345!!");
          pwdReturn = m_password;
        }
        return pwdReturn;
      }
    }
    private string Source
    {
      get
      {
        string srcReturn;
        if (!string.IsNullOrEmpty(m_source))
        {
          srcReturn = m_source;
        }
        else
        {
          m_source = GeneralParam.GetString("BlackListService.PRMalta", "Source", "CASINO");
          srcReturn = m_source;
        }
        return srcReturn;
      }
    }
    private string Venue
    {
      get
      {
        string venueReturn;
        if (!string.IsNullOrEmpty(m_venue))
        {
          venueReturn = m_venue;
        }
        else
        {
          m_venue = GeneralParam.GetString("BlackListService.PRMalta", "Venue", "string");
          venueReturn = m_venue;
        }
        return venueReturn;
      }
    }
    private string Token { get { return m_token; } set {m_token = value;} }
    private ServiceCaller Service_Caller { 
      get{
        if (m_service_caller == null)
          m_service_caller = new ServiceCaller()
          {
            Url = this.Url,
            Username = this.Username,
            Password = this.Password,
            Source = this.Source,
            Venue = this.Venue
          };
        return m_service_caller;
      }
    }

    /// <summary>
    /// When instantiating class, we're implicitly checking if we're able to use service
    /// Process is conformed by:
    ///  - Objects creation (getting its defaults values or general param values).
    ///  - Perform a 'barring/authenticate' request against the service to obtain the user's token.
    ///  - Perform a 'barring/checktoken' request to validate if user's token is an authorized one to use the service.
    ///  At this point we have a constructor that's able to take decision GeneralParam based to make requests of 'barring/search'
    ///  or not in depending of we're or not authorized users.
    /// </summary>
    public BlackList()
    {
      Log.Message(string.Format(" ** Running under of BlacklistConnectorPlayResponsibly2.dll version: '{0}'. ** ", Assembly.LoadFrom("BlacklistConnectorPlayResponsibly2.dll").GetName().Version.ToString()));
      int checkTokenResponse;
      //Steps Authenticate to be able to get Token.
      Service_Caller.BarringAuthenticate(ref m_token);
      if (IsLogEnabled)
        Log.Message(string.Format("{0} - 'barring/authenticate' responds with token '{1}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, Token));
      checkTokenResponse = Service_Caller.BarringCheckToken(Source, Venue);
      if (IsLogEnabled)
        Log.Message(string.Format("{0} - 'barring/checktoken' responds with '{1}' value.", System.Reflection.MethodBase.GetCurrentMethod().Name, checkTokenResponse));
    }

    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      int _id_type_id;
      int _occurances = 0;
      string _full_name = string.Empty;
      string _account_id = string.Empty;
      string _docnum = string.Empty;
      string _id_num = string.Empty;
      string _pass_num = string.Empty;
      string _lic_num = string.Empty;
      string _other_id = string.Empty;
      SearchInputModel sim = null;
      SearchOutputModel som = null;
      BlackListServiceResult blsrResult = null;
      List<CustomersResult> _res_list = null;
      List<CustomersResult> _res_list_2 = null;
      List<CustomersResult> _result_matches = null;
      try
      {
        blsrResult = new BlackListServiceResult {FoundResults = false};
        _result_matches = new List<CustomersResult>();

        //Account ID
        if (Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT))
        {
          _account_id = Documents[(int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT];
        } 
        
        //Name Treatment
        string _first_name =  Documents.ContainsKey ((int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME) ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME] ?? string.Empty).Trim().ToUpperInvariant() : string.Empty;
        string _middle_name = Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME) ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME] ?? string.Empty).Trim().ToUpperInvariant() : string.Empty;
        string _last_name_1 = Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1) ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1] ?? string.Empty).Trim().ToUpperInvariant() : string.Empty;
        string _last_name_2 = Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2) ? (Documents[(int) ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2] ?? string.Empty).Trim().ToUpperInvariant() : string.Empty;

        //Identification Treatment
        foreach (var _document in Documents)
        {
          if (_document.Key < (int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE || _document.Key >= (int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER) continue;
          int _docid = _document.Key - (int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE;
          _id_type_id = _docid;
          if (!Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid)) continue;
          _docnum = Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docid];
          if (string.IsNullOrEmpty(_docnum)) continue;
          if (_id_type_id.Equals(GeneralParam.GetInt32("BlackListService.PRMalta", "IdPassportTypeId", 124)))
          {
            _pass_num = _docnum;
          }
          else if (_id_type_id.Equals(GeneralParam.GetInt32("BlackListService.PRMalta", "IdNumberTypeId", 91)))
          {
            _id_num = _docnum;
          }
          else if (_id_type_id.Equals(GeneralParam.GetInt32("BlackListService.PRMalta", "LicenceNumberTypeId", 124)))
          {
            _lic_num = _docnum;
          }
          else
          {
            _other_id = _docnum;
          }
        }


        _full_name = GetFullNameFormatted(_first_name, _middle_name, _last_name_1, _last_name_2);

        sim = new SearchInputModel()
        {
          dateOfBirthInput = (Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH) ? Convert.ToDateTime((Documents[(int)ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH])).ToString("yyyy-MM-dd") : DateTime.MinValue.ToString("yyyy-MM-dd")),
         identificationInput = (!string.IsNullOrEmpty(_docnum) ? _docnum : string.Empty),
          nameInput = _full_name,
         source = Source,
         venue = Venue
        };
        
        som = Service_Caller.BarringSearch(sim);
        if (som != null)
        _res_list = BuildResults(Documents, som, sim.nameInput, _account_id);
        _result_matches.AddRange(_res_list);


        if (som!=null)
        {
          _occurances += som.resultCount;
        }
        if (_occurances == 0) return new BlackListServiceResult();

        if (_result_matches.Count == 0)
        {
          _result_matches.AddRange(_res_list_2);
        }
        blsrResult = new BlackListServiceResult()
        {
          Count = _result_matches.Count,
          FoundResults = true,
          Results = _result_matches.ToArray()
        };

        if (IsLogEnabled)
          Log.Message(string.Format("{0} - 'barring/search' responds with this count '{1}' and this trafficLight '{2}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, som.resultCount, som.resultTrafficLight));

      }
      catch (Exception _ex)
      {
        Log.Message("Error encountered in PlayResponsibly BlacklistConnector");
        Log.Message("------- Details -------");
        Log.Exception(_ex);
        Log.Message("-----------------------");
        throw;
      }
      return blsrResult;
    }

    private static string GetFullNameFormatted(string _first_name, string _middle_name, string _last_name_1, string _last_name_2)
    {
      return string.Format("{0}{1}{2}{3}", 
        (!string.IsNullOrEmpty(_first_name) ? string.Concat(_first_name, " ") : string.Empty), 
        (!string.IsNullOrEmpty(_middle_name) ? string.Concat(_middle_name, " ") : string.Empty), 
        (!string.IsNullOrEmpty(_last_name_1) ? string.Concat(_last_name_1, " ") : string.Empty), 
        (!string.IsNullOrEmpty(_last_name_2) ? _last_name_2 : string.Empty)
       ).Trim();
    }

    private List<CustomersResult> BuildResults(IDictionary<int, string> Documents, SearchOutputModel som, string _name_string, string _account_id)
    {
      List<CustomersResult> _result_matches = new List<CustomersResult>();
       DateTime _inclusiondate = DateTime.Now;
        string _identstr = Documents.ContainsKey((int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER) ? Documents[(int) ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER] : "";
        string _foundname = string.Empty;
        try
        {
          _foundname = (!string.IsNullOrEmpty(som.outputName) ? som.outputName : _name_string);
          
          _inclusiondate = som.outputExpiryOfBarring; //TODO :  InclusionDate != ExpiryOfBarring Date

          _identstr = (!string.IsNullOrEmpty(som.outputIdentification) ? som.outputIdentification : string.Empty);

        }
        catch(Exception _ex)
        {
          Log.Exception(_ex);
        }

        _result_matches.Add(
          new CustomersResult()
          {
            AccountId = _account_id,
            BlackListSource = 2,
            CustomerFullName = _foundname,
            CustumerDocNmbr = _identstr,
            ReasonDescription = "",
            ReasonType = 3,
            CustumerDocType = -1,
            ExclusionDate = _inclusiondate,
            ListName = "Play Responsibly Self-Barring List",
            Message = "Play Responsibly Self-Barring List",
            NotPermittedToEnter = true
          }
        );
      return _result_matches;
    }

    public override int CallOrder
      {
        get { return 1; }
      }

      public override void Dispose()
      {
        Service_Caller.Dispose();
      }

      public override void RefreshCache()
      {
      }

      public override int SourceType
      {
        get { return  (int)ENUM_BLACKLIST_SOURCE.EXTERNAL_WS; }
      }
    }
}
