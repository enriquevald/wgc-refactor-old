﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BlacklistConnectorPlayResponsibly.Common.Validator;

namespace BlacklistConnectorPlayResponsibly.Entities.Request
{
  public class SearchInputModel
  {
    public string source { get; set; }
    
    public string venue { get; set; }

    //[JsonConverter(typeof(JSONNullDateTimeValidator))]
    public string dateOfBirthInput { get; set; }

    public string identificationInput { get; set; }

    public string nameInput { get; set; }
  }
}
