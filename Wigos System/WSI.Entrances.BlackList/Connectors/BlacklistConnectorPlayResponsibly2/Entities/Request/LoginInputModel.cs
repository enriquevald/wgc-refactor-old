﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlacklistConnectorPlayResponsibly.Entities.Request
{
  class LoginInputModel
  {
    public string source { get; set; }
    public string venue { get; set; }
    public string password { get; set; }
    public string username { get; set; }
  }
}