﻿using System;
using System.Collections.Generic;
using System.Text;


namespace BlacklistConnectorPlayResponsibly.Entities.Response
{
  public class BarringRecordOutputModel_List
  {
    public string source { get; set; }
    public string venue { get; set; }
    public string ERROR_MESSAGE { get; set; }
    public BarringRecordOutputModel[] records { get; set; }

  }
}
