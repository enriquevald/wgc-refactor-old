﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using BlacklistConnectorPlayResponsibly.Common.Validator;

namespace BlacklistConnectorPlayResponsibly.Entities.Response
{
  public class SearchOutputModel
  {
    public string source { get; set; }
    public string venue { get; set; }
    public string ERROR_MESSAGE { get; set; }

    [JsonConverter(typeof(JSONNullDateTimeValidator))]
    public DateTime outputDateOfBirth { get; set; }

    [JsonConverter(typeof(JSONNullDateTimeValidator))]
    public DateTime outputExpiryOfBarring { get; set; }
    public string outputIdentification { get; set; }
    public string outputName { get; set; }
    public string outputPhoto { get; set; }
    public string resultCode { get; set; }
    public Int16 resultCount { get; set; }
    public string resultFuzzyPoints { get; set; }
    public string resultTrafficLight { get; set; }
  }
}
