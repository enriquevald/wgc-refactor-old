﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlacklistConnectorPlayResponsibly.Entities.Response
{
    public class FullLoginOutputModel
    {
      public string source { get; set; }
      public string venue { get; set; }
      public string token { get; set; }
      public string ERROR_MESSAGE { get; set; }
      public Boolean forcePasswordChangeFlag { get; set; }
      public string licensee{ get; set; }
      public string username { get; set; }
      public string[] venues { get; set; }
    }
}
