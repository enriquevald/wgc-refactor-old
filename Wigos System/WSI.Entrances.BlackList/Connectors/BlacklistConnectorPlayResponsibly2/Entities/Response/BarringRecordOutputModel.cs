﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using BlacklistConnectorPlayResponsibly.Common.Validator;

namespace BlacklistConnectorPlayResponsibly.Entities.Response
{
  public class BarringRecordOutputModel
  {
    public string source { get; set; }
    public string venue { get; set; }
    public string applicationCode { get; set; }
    public string confirmed { get; set; }
    [JsonConverter(typeof(JSONNullDateTimeValidator))]
    public DateTime date_end { get; set; }
    [JsonConverter(typeof(JSONNullDateTimeValidator))]
    public DateTime date_of_birth { get; set; }
    [JsonConverter(typeof(JSONNullDateTimeValidator))]
    public DateTime date_start { get; set; }
    public string ERROR_MESSAGE { get; set; }
    public string fullName { get; set; }
    public string GUID { get; set; }
    public string identification { get; set; }
    public string licensee { get; set; }
    public string pk { get; set; }
    public string photoID { get; set; }
    public string photoURL { get; set; }
    public string receptionist { get; set; }
  }
}
