﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RestClient.cs
// 
//   DESCRIPTION: RestClient class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 26-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WSI.Common;

public enum HttpVerb
{
  GET,
  POST,
  PUT,
  DELETE
} // HttpVerb

namespace BlacklistConnectorPlayResponsibly.Common.RESTRequestFactory
{
  public class RestClient : IDisposable 
  {
    
    #region " Variables "    
        private Boolean m_is_log_enabled;
        private Boolean log_yet_obtained;
    #endregion

    #region " Constants "
    private const string ContentTypeApplicationJSON = "application/json";
    private const string PostDataEmpty = "";
    #endregion

    #region " Properties "

    public string AuthorizationToken { get; set; } // AuthorizationToken
    public string EndPoint { get; set; } // EndPoint
    public HttpVerb Method { get; set; } // Method
    public string ContentType { get; set; } // ContentType
    public string PostData { get; set; } // PostData

    private Boolean IsLogEnabled
    {
      get
      {
        Boolean logReturn;
        if (!log_yet_obtained)
        {
          log_yet_obtained = true;
          m_is_log_enabled = (GeneralParam.GetInt32("BlackListService.PRMalta", "Log", 0) == 1 ? true : false);
          logReturn = m_is_log_enabled;
        }
        else
          logReturn = m_is_log_enabled;
        return logReturn;
      }
    }

    #endregion

    #region " Constructors "
    public RestClient()
    {
      EndPoint = string.Empty;
      Method = HttpVerb.GET;
      ContentType = ContentTypeApplicationJSON;
      PostData = PostDataEmpty;
      AuthorizationToken = string.Empty;
    } // RestClient 

    #endregion

    #region " Public Methods "

    public string MakeRequest()
    {
      return MakeRequest(string.Empty, PostData, AuthorizationToken);
    } // MakeRequestHttpClient
    public string MakeRequest(string parameters, string postData, string authorizationToken = "")
    {
      string _responseValue = string.Empty;      
      string _thumbprint = string.Empty;
      ASCIIEncoding encoding = new ASCIIEncoding();
      HttpWebRequest _request;
      try
      {
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        byte[] data = encoding.GetBytes(postData);

        _request = (HttpWebRequest)WebRequest.Create(new System.Uri(EndPoint + parameters));
        _request.Headers.Clear();
        _request.ContentType = "application/json";
        _request.Method = (string.IsNullOrEmpty(Method.ToString()) ? "GET" : Method.ToString());
        _request.Timeout = 5 * 1000; // 5 Seconds.
        _request.KeepAlive = false;
        if(!string.IsNullOrEmpty(authorizationToken))
          _request.Headers.Add(HttpRequestHeader.Authorization, string.Format("Basic {0}",authorizationToken));
        _request.ContentLength = data.Length;
        _request.GetRequestStream();

        Stream newStream = _request.GetRequestStream();
        // Send the data.
        newStream.Write(data, 0, data.Length);
        newStream.Close();

        
        if (IsLogEnabled)
          Log.Message(string.Format("{0} - Request to URL: '{1}' with PostData: '{2}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, EndPoint, postData));

        using (WebResponse _response = _request.GetResponse())
        {
          using (Stream _stream = _response.GetResponseStream())         
          {
            using (StreamReader _reader = new StreamReader(_stream))
            {
              _responseValue = _reader.ReadLine();
            }
          }
        }
        
        if (IsLogEnabled)
          Log.Message(string.Format("{0} - Response: '{1}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, _responseValue));

      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
      }
      return _responseValue;
    } // MakeRequestHttpClient

    public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    #endregion


    #region IDisposable Members

    public void Dispose()
    {
      
    }

    #endregion
  } // RestClient

} // InHouseApi.Shared