﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BlacklistConnectorPlayResponsibly.Common.Validator
{
  public class JSONNullDateValidator : DateTimeConverterBase
  {
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      if (reader.Value == null)
        return DateTime.MinValue.ToString("yyyy-MM-dd");

      return ((DateTime)(reader.Value)).ToString("yyyy-MM-dd");
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      DateTime dateTimeValue = (DateTime)value;
      if (dateTimeValue == DateTime.MinValue)
      {
        writer.WriteNull();
        return;
      }

      writer.WriteValue(((DateTime)(value)).ToString("yyyy-MM-dd"));
    }
  }
}
