﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlacklistConnectorPlayResponsibly.Common.RESTRequestFactory;
using BlacklistConnectorPlayResponsibly.Entities.Request;
using BlacklistConnectorPlayResponsibly.Entities.Response;
using WSI.Common;

namespace BlacklistConnectorPlayResponsibly.Common
{
	public class ServiceCaller : IDisposable
	{
		Boolean IsAuthenticated { get; set; }
		private string Token { get; set; }
		public string Url { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string Venue { get; set; }
		public string Source { get; set; }
		

		public ServiceCaller() 
		{
			
		}

		#region " Public Methods "
		
		public Boolean BarringAuthenticate(ref string token) 
		{
			string requestJson;
			LoginInputModel lim = new LoginInputModel() { password = Password, username = Username, source = Source, venue = Venue };
			FullLoginOutputModel folm;
			try 
			{	        
					requestJson=Newtonsoft.Json.JsonConvert.SerializeObject(lim, Newtonsoft.Json.Formatting.Indented);
					folm = Call<FullLoginOutputModel>(string.Format("{0}/{1}", Url, "barring/authenticate"), requestJson, string.Empty);
					if(!string.IsNullOrEmpty(folm.token))
					{
						Token = folm.token;
						token = Token;
						IsAuthenticated = true;
					}
					else
					{
						IsAuthenticated = false;
					}
			}
			catch (Exception _ex)
			{

				Log.Error(_ex.Message);
			}

			return IsAuthenticated;
		}

		public Int32 BarringCheckToken(string Source, string Venue)
		{
			Int32 iResult=-1;
			string requestJson;
			aModel am;
			try 
			{	     
				am = new aModel() { source = Source, venue = Venue };
				requestJson = Newtonsoft.Json.JsonConvert.SerializeObject(am, Newtonsoft.Json.Formatting.Indented);
						
				iResult = Call<int>(string.Format("{0}/{1}", Url, "barring/checktoken"), requestJson, Token);   
		
			}
			catch (Exception _ex)
			{

				Log.Error(_ex.Message);
			}
			return iResult;
		}

		public SearchOutputModel BarringSearch(SearchInputModel sim)
		{
			SearchOutputModel som = null;
			string requestJson;
			
			try
			{
				requestJson = Newtonsoft.Json.JsonConvert.SerializeObject(sim, Newtonsoft.Json.Formatting.Indented);
				som = Call<SearchOutputModel>(string.Format("{0}/{1}", Url, "barring/search"), requestJson, Token);
			}
			catch (Exception _ex)
			{
				Log.Error(_ex.Message);
			}
			return som;
		}

		#region " Call Method "
		public T Call<T>(string endPoint, string postData, string authorizationToken)
		{
			string _json = string.Empty;
			T _result = default(T);
			try
			{
				RestClient rc = new RestClient() { EndPoint = endPoint, ContentType = "application/json", Method = HttpVerb.POST, PostData = postData, AuthorizationToken = authorizationToken };
				_json = rc.MakeRequest();
				_result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(_json);
			}
			catch (Exception _ex)
			{
				Log.Error(_ex.Message);
			}
			return (T)_result;
		}
		#endregion

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
