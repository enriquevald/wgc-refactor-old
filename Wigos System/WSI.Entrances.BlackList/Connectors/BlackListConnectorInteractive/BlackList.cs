﻿using WSI.Entrances.BlackList.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace BlackListConnectorInteractive
{
    public class BlackList: BlackListConnector
    {
      public override int  CallOrder { get { return 3; } }
      public override bool AreAnyDocumentsBlackListed(IDictionary<ENUM_ID_DOC_TYPES, string> Documents)
      {

        return MessageBox.Show("Fail these documents?", "Fail?", MessageBoxButtons.YesNo) == DialogResult.Yes;
      }
    }
}
