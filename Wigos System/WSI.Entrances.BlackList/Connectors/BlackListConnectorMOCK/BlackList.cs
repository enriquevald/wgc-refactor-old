﻿using WSI.Entrances.BlackList.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlackListConnectorMOCK
{
  public class BlackList : BlackListConnector
  {
    public override int CallOrder { get { return 2; } }
    public override bool AreAnyDocumentsBlackListed(IDictionary<ENUM_ID_DOC_TYPES, string> Documents)
    {
      Random _rnd;
      if (Documents.Values.Contains("failme:yes"))
      {
        return true;
      }
      _rnd = new Random(DateTime.Now.Second);

      return _rnd.Next(10) > 5 ? true : false;
    }
  }
}
