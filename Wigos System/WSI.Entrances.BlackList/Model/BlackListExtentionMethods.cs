﻿using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Entrances.BlackList.Model.Interfaces;
using WSI.Entrances.BlackList.Model.DTO;

namespace WSI.Entrances.BlackList.Model
{
  public static class BlackListExtentionMethods
  {
    public static Dictionary<int, string> ToDictionary(this BlackListDocuments Documents)
    {
      var _retval = new Dictionary<int, string>();
      foreach (var _d in Documents.Documents)
      {
        _retval.Add(_d.DocumentType, _d.DocumentNumber);
      }
      return _retval;
    }

    public static BlackListDocuments ToBlackListDocuments(this Dictionary<int, string> Documents)
    {
      List<BlackListDocument> _docs = new List<BlackListDocument>();
      foreach (var d in Documents)
      {
        _docs.Add(new BlackListDocument() { DocumentNumber = d.Value, DocumentType = d.Key });
      }
      return new BlackListDocuments() { Documents = _docs.ToArray() };
    }

  }
}

namespace System.Runtime.CompilerServices
{
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
  public class ExtensionAttribute : Attribute
  {
  }
}