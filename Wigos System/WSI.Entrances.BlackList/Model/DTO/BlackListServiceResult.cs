﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace WSI.Entrances.BlackList.Model.DTO
{
	[Serializable]
	[XmlRoot(ElementName = "BlackListServiceResult")]
	public class BlackListServiceResult
	{
		[XmlAttribute(AttributeName = "FoundResults")]
		public bool FoundResults { get; set; }

		[XmlElementAttribute(ElementName = "Count")]
		public int Count { get; set; }

		[XmlElementAttribute(ElementName = "Results")]
		public CustomersResult[] Results { get; set; }

	}

	[Serializable]
	[XmlRoot(ElementName = "CustomersResult")]
	public class CustomersResult : IEqualityComparer<CustomersResult>
	{
		[XmlElementAttribute(ElementName = "BlackListSource")]
		public int BlackListSource { get; set; }

		[XmlElementAttribute(ElementName = "AccoutId")]
		public String AccountId { get; set; }

		[XmlElementAttribute(ElementName = "CustomerFullName")]
		public String CustomerFullName { get; set; }

		[XmlElementAttribute(ElementName = "CustumerDocType")]
		public int CustumerDocType { get; set; }

		[XmlElementAttribute(ElementName = "CustumerDocNmbr")]
		public string CustumerDocNmbr { get; set; }

		[XmlElementAttribute(ElementName = "ReasonType")]
		public int ReasonType { get; set; }

		[XmlAttribute(AttributeName = "ReasonDescription")]
		public string ReasonDescription { get; set; }

		[XmlAttribute(AttributeName = "ExclusionDate")]
		public DateTime ExclusionDate { get; set; }

		[XmlAttribute(AttributeName = "NotPermittedToEnter")]
		public bool NotPermittedToEnter { get; set; }

		[XmlAttribute(AttributeName = "Message")]
		public string Message { get; set; }

		[XmlAttribute(AttributeName = "ListName")]
		public string ListName { get; set; }

		public bool Equals(CustomersResult X, CustomersResult Y)
		{
			return X.GetHashCode() == Y.GetHashCode();
		}

		public override int GetHashCode()
		{
			return ((AccountId ?? "0") + ":" + CustomerFullName + ":" + CustumerDocType + ":" + CustumerDocNmbr).GetHashCode();
		}

		public int GetHashCode(CustomersResult Obj)
		{
			return Obj.GetHashCode();
		}
	}
}
