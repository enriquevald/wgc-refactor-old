﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using WSI.Entrances.BlackList.Model.Interfaces;
// ReSharper disable All

namespace WSI.Entrances.BlackList.Model.DTO
{
  public enum ENUM_BLACKLIST_DOC_TYPE
  {
    DOC_TYPE = 10000,
    DOC_NUMBER = 20000,
    ACCOUNT = 2,
    FIRST_NAME = 3,
    MIDDLE_NAME = 4,
    LASTNAME_1 = 5,
    LASTNAME_2 = 6,
    DATEOFBIRTH=7

  }
  
  [Serializable, XmlInclude(typeof(BlackListDocument)), XmlRoot(ElementName = "request")]
  public class BlackListDocuments
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }
    [XmlElementAttribute(ElementName = "souce")]
    public int Source { get; set; } 
    [XmlArray(ElementName = "Documents"), XmlArrayItem(ElementName = "BlackListDocument")]
    public BlackListDocument[] Documents { get; set; }
  }

  [Serializable, XmlRoot(ElementName = "BlackListDocument")]
  public class BlackListDocument
  {
    [XmlAttribute(AttributeName = "DocumentType")]
    public int DocumentType { get; set; }

    [XmlTextAttribute()]
    public String DocumentNumber { get; set; }
  }

}
