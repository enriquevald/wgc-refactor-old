﻿using System;
using System.Collections.Generic;
using WigosModel.Interfaces;
using WSI.Entrances.BlackList.Model.DTO;

namespace WSI.Entrances.BlackList.Model.Interfaces
{
  //classes for serializing elements sent by webservice.
 
  public enum ENUM_ID_DOC_TYPES
  {
   ID_DOC_TYPE_DNI = 1,
   ID_DOC_TYPE_NAME = 2,
   ID_DOC_TYPE_NIE = 3,
   ID_DOC_TYPE_PASSPORT = 4,
  }
  
  public interface IBlackListConnector : IWigosPlugin, IComparable<IBlackListConnector> 
  {
    int CallOrder { get; }
    int SourceType { get; }
    BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents);
    void RefreshCache();
  }

  public abstract class BlackListConnector: IBlackListConnector 
  {
    public abstract int CallOrder { get; }
    public abstract int SourceType { get; }
    public abstract BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents);
    public abstract void RefreshCache();
    public abstract void Dispose();

    public int CompareTo(IBlackListConnector Other)
    { 
      if (Other.CallOrder > this.CallOrder) return -1;
      else if (Other.CallOrder < this.CallOrder) return 1;
      return 0;
    }
  }
}
