﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common;
using WSI.Entrances.BlackList.Model.Interfaces;
using WSI.Entrances.BlackList.Model;
using WSI.Entrances.BlackList.Model.DTO;

namespace BlackListProxyServiceManualTest
{
  public partial class TestForm1 : Form
  {
    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";

    public TestForm1()
    {
      InitializeComponent();
      String client_id_str;
      int client_id;
      string default_xml;
      default_xml = "<SiteConfig>" +
                      "<DBPrincipal></DBPrincipal>" +
                      "<DBMirror></DBMirror>" +
                      "<DBId>0</DBId>" +
                    "</SiteConfig>";

      // Initialize Config file
      ConfigurationFile.Init("WSI.Cashier.cfg", default_xml);
      try
      {
        // APB 12/11/2008: 'Client Identifier' is now taken from configuration file instead of from a hard-coded constant
        client_id_str = ConfigurationFile.GetSetting("DBId").Trim();
        client_id = Convert.ToInt32(client_id_str);
      } // try
      catch
      {
        client_id = 0;
      } // catch


      WGDB.Init(client_id, ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");
    }

    private void button1_Click(object sender, EventArgs e)
    {
      BlackListDocuments documents;
      WebResponseInfo response_info;
      XmlDocument response;
      XmlDocument request;

      documents = new Dictionary<int, string>() {
        {(int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER,"1111"},
        {(int)ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME,"Scott Adamson"}
      }.ToBlackListDocuments();

      documents.Type = "CheckDocumentsAreNotBlackListed";
      
      request = new XmlDocument();
      request.LoadXml(Serialize(documents));

      response_info = CheckDocuments(new RequestMessageData() { Uri = GeneralParam.GetString("BlackListService", "Uri", "http://localhost:7778/"), Xml = request });

      response = new XmlDocument();
      response.LoadXml(response_info.Body);
      MessageBox.Show(bool.Parse(response.SelectSingleNode("/boolean").InnerText)?"Call Security!":"Let him pass");

    }
    private String Serialize(Object ObjectResponse)
    {
      XmlSerializer serializer = new XmlSerializer(ObjectResponse.GetType());
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      try
      {
        using (StringWriter writer = new Utf8StringWriter())
        {
          _name_spaces.Add("", "");
          serializer.Serialize(writer, ObjectResponse, _name_spaces);

          return writer.ToString();
        }
      }
      catch (Exception)
      {
        //Log.Exception(_ex);
      }

      return String.Empty;
    } // SerializeResponse

    public class Utf8StringWriter : StringWriter
    {
      public override Encoding Encoding
      {
        get { return Encoding.UTF8; }
      }
    } // Utf8StringWriter

    public WebResponseInfo CheckDocuments(RequestMessageData request)
    {
      return CallService(request.Uri, request.Xml);
    }

    private WebRequest CreateWebRequestPOST(string uriAddress, XmlDocument document)
    {
      var webRequest = WebRequest.Create(uriAddress);
      webRequest.Method = "POST";
      webRequest.ContentType = "text/xml";
      webRequest.Credentials = CredentialCache.DefaultCredentials;
      webRequest.Proxy = null;
      SetRequestBody(webRequest, document.InnerXml);

      return webRequest;
    }
    private void SetRequestBody(WebRequest webRequest, string body)
    {
      byte[] buffer = Encoding.UTF8.GetBytes(body);
      webRequest.ContentLength = buffer.Length;
      using (Stream requestStream = webRequest.GetRequestStream())
        requestStream.Write(buffer, 0, buffer.Length);
    }
    private WebResponseInfo CallService(string uriAddress, XmlDocument document)
    {
      try
      {
        var webRequest = CreateWebRequestPOST(uriAddress, document);
        using (var webResponse = (HttpWebResponse)webRequest.GetResponse())
          return Read(webResponse);
      }
      catch (Exception)
      {
        //Logger.Error(string.Format("Ocurrió un error al hacer el llamado al servicio '{0}'", uriAddress), ex);
        throw;
      }
    }
    public WebResponseInfo Read(HttpWebResponse response)
    {
      var info = new WebResponseInfo();
      info.StatusCode = response.StatusCode;
      info.StatusDescription = response.StatusDescription;
      info.ContentEncoding = response.ContentEncoding;
      info.ContentLength = response.ContentLength;
      info.ContentType = response.ContentType;

      using (var bodyStream = response.GetResponseStream())
      using (var streamReader = new StreamReader(bodyStream, Encoding.UTF8))
      {
        info.Body = streamReader.ReadToEnd();
      }

      return info;
    }

  }
  public class RequestMessageData
  {
    public string Uri
    {
      get;
      set;
    }

    public XmlDocument Xml
    {
      get;
      set;
    }
  }


  public class WebResponseInfo
  {
    public string Body { get; set; }
    public string ContentEncoding { get; set; }
    public long ContentLength { get; set; }
    public string ContentType { get; set; }
    public HttpStatusCode StatusCode { get; set; }
    public string StatusDescription { get; set; }

    public override string ToString()
    {
      var sb = new StringBuilder();
      sb.AppendLine(GetHeaderData());
      sb.AppendLine(string.Format("\nBody: \n{0}", Body));
      return sb.ToString();
    }

    public string GetHeaderData()
    {
      var sb = new StringBuilder();
      sb.AppendLine(string.Format("StatusCode: {0} - StatusDescripton: {1}\n", StatusCode, StatusDescription));
      sb.AppendLine(string.Format("ContentType: {0} - ContentEncoding: {1} - ContentLength: {2}", ContentType, ContentEncoding, ContentLength));
      return sb.ToString();
    }
  }

}
