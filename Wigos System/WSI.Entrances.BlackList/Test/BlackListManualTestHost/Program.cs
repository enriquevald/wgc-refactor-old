﻿using System;
using System.Collections.Generic;
using System.Text;
using WigosCommon;
using WSI.Common;
using WSI.Entrances.BlackList;

namespace BlackListManualTestHost
{
  class Program
  {
    protected static String m_service_name = "Unknown";
    protected static String m_version = "CC.BBB";
    static void Main(string[] args)
    {

      String client_id_str;
      int client_id;
      string default_xml;
      default_xml = "<SiteConfig>" +
                      "<DBPrincipal></DBPrincipal>" +
                      "<DBMirror></DBMirror>" +
                      "<DBId>0</DBId>" +
                    "</SiteConfig>";

      // Initialize Config file
      ConfigurationFile.Init("WSI.Cashier.cfg", default_xml);
      try
      {
        // APB 12/11/2008: 'Client Identifier' is now taken from configuration file instead of from a hard-coded constant
        client_id_str = ConfigurationFile.GetSetting("DBId").Trim();
        client_id = Convert.ToInt32(client_id_str);
      } // try
      catch
      {
        client_id = 0;
      } // catch

      
      WGDB.Init(client_id, ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");
      using (BlackListProxyServiceHost host = new BlackListProxyServiceHost())
      {
        host.Init();

        Console.WriteLine("press any key to kill host");
        Console.ReadKey();

      }

      Environment.Exit(0);
    }
  }
}
