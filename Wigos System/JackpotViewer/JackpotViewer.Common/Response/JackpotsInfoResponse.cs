﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: JackpotsInfoResponse.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Jackpot;

namespace JackpotViewerWS.Common.Response
{
  public class JackpotsInfoResponse
  {
    public Int32 Id { get; set; }
    public String Name { get; set; }
    public Int32 Code { get; set; }
    public String IsoCode { get; set; }
    public Boolean Enabled { get; set; }
    public List<JackpotViewerItem> Jackpots { get; set; }
    public ErrorResponse Error { get; set; }
  } // JackpotsInfoResponse
}
