﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TypeBalance.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using JackpotViewerWS.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace JackpotViewerWS.Common.Response
{
  public class ErrorResponse
  {
    public ErrorCode Code { get; set; }
    public String Message { get; set; }
  } // ErrorResponse
} 
