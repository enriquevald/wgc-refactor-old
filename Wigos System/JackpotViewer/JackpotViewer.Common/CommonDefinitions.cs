﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace JackpotViewerWS.Common
{
  public enum ErrorCode
  {
    None = 0,
    NoJackpots = 1,
    Critical = 99,
    Unknown = 999
  }
}
