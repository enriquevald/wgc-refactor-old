﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: IJackpotViewerService.cs
// 
//      DESCRIPTION: JackpotViewer Service interface
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release.
//------------------------------------------------------------------------------
using JackpotViewerWS.Common.Request;
using JackpotViewerWS.Common.Response;
using System.ServiceModel;

namespace JackpotViewerWS.Common
{
  [ServiceContract]
  public interface IJackpotViewerService
  {

    #region " Operation Contracts "

    /// <summary>
    /// Get Jackpots info related to JackpotViewer WebMethod
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>JackpotsInfoResponse</returns>
    [OperationContract]
    JackpotsInfoResponse JackpotsInfo(JackpotsInfoRequest Request); // JackpotsInfo

    /// <summary>
    /// Get Jackpots awards related to JackpotViewer WebMethod
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>JackpotsAwardsResponse</returns>
    [OperationContract]
    JackpotsAwardsResponse JackpotsAwards(JackpotsAwardsRequest Request); // JackpotsAwards

    #endregion

  } // IJackpotViewerService
}
