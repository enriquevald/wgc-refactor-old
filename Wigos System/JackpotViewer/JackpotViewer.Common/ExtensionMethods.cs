﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: ExtensionMethods.cs
// 
//      DESCRIPTION: Extension Methods
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using WSI.Common;
using WSI.Common.Jackpot;
using JackpotViewerWS.Common.Response;
using System.Collections.Generic;

namespace JackpotViewerWS.Common
{
  public static class ExtensionMethods
  {

    #region " JackpotViewer "

    #region " Jackpot Info "

    /// <summary>
    /// Jackpot Viewer item list to JackpotViewerInfo response
    /// </summary>
    /// <param name="JackpotViewer"></param>
    /// <returns></returns>
    public static JackpotsInfoResponse ToJackpotInfoResponse(this JackpotViewer JackpotViewer)
    {
      if(JackpotViewer == null)
      {
        return null;
      }

      return new JackpotsInfoResponse()
      {
        Id        = JackpotViewer.Id,
        Name      = JackpotViewer.Name,
        Code      = JackpotViewer.Code,
        Enabled   = JackpotViewer.Enabled,
        IsoCode   = GetIsoCode(JackpotViewer),
        Jackpots  = GetJackpots(JackpotViewer),
        Error     = new ErrorResponse() 
        { 
          Code    = ErrorCode.None,
          Message = String.Empty
        }
      };
    } // ToJackpotInfoResponse

    #endregion 

    #region " Jackpots Last Awards "

    /// <summary>
    /// Jackpot Viewer item list to JackpotsAwards response
    /// </summary>
    /// <param name="JackpotViewer"></param>
    /// <returns></returns>
    public static JackpotsAwardsResponse ToJackpotsAwardsResponse(this JackpotViewer JackpotViewer)
    {
      if (JackpotViewer == null)
      {
        return null;
      }

      return new JackpotsAwardsResponse()
      {
        Id              = JackpotViewer.Id,
        Name            = JackpotViewer.Name,
        Code            = JackpotViewer.Code,
        Enabled         = JackpotViewer.Enabled,
        IsoCode         = GetIsoCode(JackpotViewer),
        JackpotsAwards  = GetJackpotsAwards(JackpotViewer),
        Error = new ErrorResponse()
        {
          Code = ErrorCode.None,
          Message = String.Empty
        }
      };
    } // ToJackpotInfoResponse

    #endregion

    #region " Misc "
    
    /// <summary>
    /// Get IsoCode from first Jackpot
    /// </summary>
    /// <param name="JackpotViewer"></param>
    /// <returns></returns>
    private static string GetIsoCode(JackpotViewer JackpotViewer)
    {
      if ( JackpotViewer.Jackpots != null
        && JackpotViewer.Jackpots.Items != null
        && JackpotViewer.Jackpots.Items.Count > 0)
      {
        return JackpotViewer.Jackpots.Items[0].IsoCode;
      }

      return String.Empty;
    } // GetIsoCode

    /// <summary>
    /// Get Jackpots list
    /// </summary>
    /// <param name="JackpotViewer"></param>
    /// <returns></returns>
    private static List<JackpotViewerItem> GetJackpots(JackpotViewer JackpotViewer)
    {
      if ( JackpotViewer.Jackpots != null
        && JackpotViewer.Jackpots.Items != null
        && JackpotViewer.Jackpots.Items.Count > 0)
      {
        return JackpotViewer.Jackpots.Items;
      }

      return new List<JackpotViewerItem>();
    } // GetJackpots

    /// <summary>
    /// Get Jackpots list
    /// </summary>
    /// <param name="JackpotViewer"></param>
    /// <returns></returns>
    private static List<JackpotAwardItemList> GetJackpotsAwards(JackpotViewer JackpotViewer)
    {
      if ( JackpotViewer.Jackpots != null
        && JackpotViewer.Jackpots.JackpotsAwards != null)
      {
        return JackpotViewer.JackpotAwards;
      }

      return new List<JackpotAwardItemList>();
    } // GetJackpots

    #endregion

    #endregion

  }
}