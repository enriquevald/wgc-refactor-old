﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BaseRequest.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release. 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace JackpotViewerWS.Common.Request
{
  public abstract class BaseRequest : IBaseRequest
  {
    public Int32 JackpotViewerCode { get; set; }
  }

  public interface IBaseRequest
  {
    Int32 JackpotViewerCode { get; set; }
  }
}
