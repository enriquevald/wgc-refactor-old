﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viewer.entities
{
  public class eJackpot
  {
    public Int32 JackpotId { get; set; }
    public String Name { get; set; }
    public Boolean Status { get; set; }
    public Int32 Order { get; set; }
    public Decimal Main { get; set; }
    public String IsoCode { get; set; }    
  }
}
