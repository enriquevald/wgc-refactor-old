﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viewer.entities
{
  public class eViewer
  {
    public String Name { get; set; }
    public List<eJackpot> Jackpots { get; set; }
    public List<eAlert> Alerts { get; set; }

    public eViewer()
    {
      this.Jackpots = new List<eJackpot>();
      this.Alerts = new List<eAlert>();
    }

    public String getHTML()
    {

      StringBuilder _sb;
      Boolean _close;
      Int32 _jackpot_page;

      _sb = new StringBuilder();
      _close = true;
      _jackpot_page = Int32.Parse(ConfigurationManager.AppSettings["jackpots_page"]);

      for (int i = 0; i < this.Jackpots.Count; i++)
      {

        if (i % _jackpot_page == 0)
        {
          _close = false;
          _sb.AppendLine("<div style=\"width:100%;height:100%;float:left;\">");
        }

        _sb.AppendLine("  <div class=\"col-md-6 col-sm-6 col-xs-12 jackpot\">");
        _sb.AppendLine("    <div>");
        _sb.AppendLine(String.Format("      <div id=\"spnName_{0}\" class=\"jackpotName\">{1}</div>", this.Jackpots[i].JackpotId, this.Jackpots[i].Name));
        _sb.AppendLine("    </div>");
        _sb.AppendLine("    <div class=\"amount\">");
        _sb.AppendLine("      <span class=\"simbolMoney\">$</span>");
        _sb.AppendLine(String.Format("      <div id=\"spnMonto_{0}\" class=\"odometer odometer-theme-car font-100\">0</div>", this.Jackpots[i].JackpotId));
        _sb.AppendLine("    </div>");
        _sb.AppendLine("  </div>");

        if (i % _jackpot_page == _jackpot_page - 1)
        {
          _close = true;
          _sb.AppendLine("</div>");
        }

      }

      if (!_close)
      {
        _sb.AppendLine("</div>");
      }

      return _sb.ToString();

    }

  }
}
