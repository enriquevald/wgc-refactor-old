﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viewer.entities
{
  public class eAlert
  { 
    public Int32 Type { get; set; }
    public Decimal Amount { get; set; }           
    public DateTime DateAward { get; set; }
    public String Message { get; set; }
  }
}
