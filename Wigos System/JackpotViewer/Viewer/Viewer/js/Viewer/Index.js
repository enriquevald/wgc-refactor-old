var _html = '';
var _refresh = 10 * 1000;
var _audio;
var _direction = 1;
var _page = 0;
var _jackpots_page = 6;
var _animation = 1;
var _sound_jackpot = "WSI_998_JACKPOT_AWARDED.wav";

//Page ready
$(document).ready(function () {

  Init();

  connectWSRefresh();

  setInterval(function () {
    connectWSRefresh();
  }, _refresh);

});

//Funtions
function Init() {

  var _id;

  _id = window.location.pathname.split("/");

  $('#hdnViewer').val(_id[_id.length - 1]);
  window.odometerOptions = {
    format: '(ddd).dd'
  };
  
  switch (_animation) {
    case 0:
      MoveScroll(); //Animation scroll
      break;
    case 1:
      PageScroll();  //Animation page
      break;
  }  

} // Init

function connectWSRefresh() {

  $.ajax({
    type: 'GET',
    dataType: "json",
    url: document.location.origin + '/viewer/ws/' + $('#hdnViewer').val(),
    success: function (Data, textStatus, jqXHR) {
      RefreshViewer(Data);
    },
    error: function (responseData, textStatus, errorThrown) {

    }
  });

} // connectWSRefresh

function RefreshViewer(Data) {

  var _list_jackpot = "";

  if (Data.Jackpots != null) {

    for (var i = 0; i < Data.Jackpots.length; i++) {

      _list_jackpot += Data.Jackpots[i].JackpotId;

    }

    localStorage.setItem("NumJackpots", Data.Jackpots.length);

    if (localStorage.Jackpots) {
      if (JSON.stringify(_list_jackpot) != localStorage.Jackpots) {
        localStorage.setItem("Jackpots", JSON.stringify(_list_jackpot));
        location.href = location.href;
      }
    } else {
      localStorage.setItem("Jackpots", JSON.stringify(_list_jackpot));
    }

    $("#divTitile").html(Data.Name);

    for (var i = 0; i < Data.Jackpots.length; i++) {
      if ($('#spnMonto_' + Data.Jackpots[i].JackpotId).length > 0) {
        $('#spnMonto_' + Data.Jackpots[i].JackpotId).html(Data.Jackpots[i].Main.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
        $('#spnName_' + Data.Jackpots[i].JackpotId).html(Data.Jackpots[i].Name);
      }
    }
  }

  if (Data.Alerts.length > 0) {

    _html = "";

    $("ul.alert-box").html("");

    for (var i = 0; i < Data.Alerts.length; i++) {

      _html += "<li>" + Data.Alerts[i].Message + "</li>";

      if (Data.Alerts[i].Type == 0) {
        setTimeout(AnimationJackpot(), 10);
      }
    }

    $("ul.alert-box").html(_html);

    ShowAlert(Data.Alerts.length);
  }

  window.odometerOptions = {
    format: '(ddd).dd'
  };

} // RefreshViewer

function AnimationJackpot() {

  var h
  var w;
  var pY;
  var pX;
  var div_h;
  var div_w;

  _audio = new Audio('../../Sounds/' + _sound_jackpot);
  _audio.play();

  _audio.addEventListener("ended", function () {
    _audio.currentTime = 0;
    $('#divJackpot > img').fadeOut(1000);
  });

  h = $('#divJackpot > img').height();
  //w = $('#divJackpot > img').width();
  w = $('#divJackpot').show();

  div_h = $('#divJackpot').height();
  div_w = $('#divJackpot').width();

  pY = Math.round((div_h - h) / 2) + 'px';
  pX = Math.round((div_w - w) / 2) + 'px';

  for (var i = 0; i < 10; i++) {
    $('#divJackpot > img').animate({ opacity: "1", zoom: '500%' })
    $('#divJackpot > img').animate({ opacity: "1", zoom: '0%' })
  }

  $('#divJackpot > img').animate({ opacity: "1", zoom: '500%' })

} // AnimationJackpot

function ShowAlert(Iterations) {

  $("ul.alert-box").css("top", "0px");

  for (var i = 0; i < Iterations; i++) {

    $("ul.alert-box").animate({ "top": "-=70px" }, 1000);
    $("ul.alert-box").delay(3000);

    if (i == Iterations - 1) {
      $("ul.alert-box").animate({ "top": "-=70px" }, 1000);
    }
  }

}// ShowAlert

//Scroll Animations

function MoveScroll() {

  if (localStorage.NumJackpots) {

    if (localStorage.NumJackpots > _jackpots_page) {

      if (_direction == 1 && divJackpots.scrollTop == (divJackpots.scrollHeight - divJackpots.clientHeight)) {
        _direction = -1;
      }

      if (_direction == -1 && divJackpots.scrollTop == 0) {
        _direction = 1;
      }
      scrolldelay = setTimeout(MoveScroll, 20);
    } else {
      divJackpots.scrollTop = 0;
    }
  }

  divJackpots.scrollTop += 1 * _direction;

} // MoveScroll

function PageScroll() {

  var slideW;

  slideW = $('#divJackpots').height();

  $('#divJackpots').animate({ scrollTop: slideW * _page }, 600);

  _page += 1;

  if (_page == Math.ceil(localStorage.NumJackpots / _jackpots_page)) {
    _page = 0;
  }

  setTimeout(PageScroll, 5000);

} // PageScroll
