﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viewer.entities;

namespace Viewer.Controllers
{
  public class ViewerController : Controller
  {

    #region " Const "

    private const Int32 m_enum_paid = 32;

    #endregion    

    #region " Members "

    JackpotViewerWS.JackpotViewerService m_service;

    #endregion    

    #region " Properties "

    public JackpotViewerWS.JackpotViewerService Service
    {
      get
      {
        if (m_service == null)
        {
          m_service = new JackpotViewerWS.JackpotViewerService();
          m_service.Url = ConfigurationManager.AppSettings["urlService"];
        }
        return m_service;
      }
    } // Service
    
    #endregion " Properties "

    #region " Public methods "

    public ActionResult Index()
    {
      return View();
    } // Index

    public ActionResult Get(string id)
    {

      eViewer _viewers;

      _viewers = getViewer(id);

      return View(_viewers);
    } // Get

    public ActionResult Ws(string id)
    {

      eViewer _viewers;

      try
      {
        _viewers = getViewer(id);

        return Json(_viewers, JsonRequestBehavior.AllowGet);

      }
      catch (Exception)
      {
        return Json("");
      }

    } // Ws

    #endregion " Public methods "

    #region " Private methods "

    private eViewer getViewer(String id)
    {

      eViewer _viewer;
      List<JackpotViewerWS.JackpotAwardItemList> _awards_item;
      JackpotViewerWS.JackpotsInfoRequest _request_info;
      JackpotViewerWS.JackpotsInfoResponse _response_info;
      JackpotViewerWS.JackpotsAwardsRequest _request_award;
      JackpotViewerWS.JackpotsAwardsResponse _response_award;

      try
      {
        _request_info = new JackpotViewerWS.JackpotsInfoRequest();
        _request_award = new JackpotViewerWS.JackpotsAwardsRequest();
        _viewer = new eViewer();

        _request_info.JackpotViewerCode = Int32.Parse(id);
        _request_info.JackpotViewerCodeSpecified = true;

        _response_info = Service.JackpotsInfo(_request_info);

        _viewer.Name = _response_info.Name;

        _request_award.JackpotViewerCode = Int32.Parse(id);
        _request_award.JackpotViewerCodeSpecified = true;

        _response_award = Service.JackpotsAwards(_request_award);

        if (_response_info.Jackpots.Count() == 0)
        {
          return _viewer;
        }

        _viewer.Jackpots = new List<eJackpot>(_response_info.Jackpots.Where(_jackpot => _jackpot.Enabled == true).Select(_jackpot => new eJackpot
        {
          JackpotId = _jackpot.JackpotId,
          Name = _jackpot.Name,
          Status = _jackpot.Status,
          Order = _jackpot.Order,
          Main = _jackpot.Main,
          IsoCode = _jackpot.IsoCode,
        }));

        foreach (eJackpot _jackpot in _viewer.Jackpots)
        {
          _awards_item = _response_award.JackpotsAwards.Where(_award => _award.JackpotId == _jackpot.JackpotId).ToList();

          if (_awards_item != null)
          {
            foreach (JackpotViewerWS.JackpotAwardItemList _award_item in _awards_item)
            {
              _viewer.Alerts.AddRange(_award_item.Awards.Where(_alert => (_alert.DateAward.Ticks > DateTime.Now.AddSeconds(-9).Ticks && _alert.DateAward.Ticks < DateTime.Now.AddSeconds(1).Ticks) && IsFlagActived(_alert.Status, m_enum_paid)).ToList().Select(_alert => new eAlert
              {
                Type = _alert.Type,
                Amount = _alert.Amount,
                DateAward = _alert.DateAward,
                Message = GetMessage(_alert.Type, _jackpot.Name)
              }));
            }
          }
        }

        return _viewer;
      }
      catch (Exception)
      {

      }

      return null;
    } // getViewer

    private String GetMessage(Int32 Type, String JackpotName)
    {

      String _message;

      _message = "";

      switch (Type)
      {
        case 0:
          _message = String.Format("Ha tocado el jackpot {0}", JackpotName);
          break;
        case 5:
          _message = String.Format("Pronto se dará el jackpot {0}", JackpotName);
          break;
        case 6:
        case 7:
          _message = String.Format("Ha tocado una happy del jackpot {0}", JackpotName);
          break;
      }

      return _message;
    } // GetMessage

    private Boolean IsFlagActived(Int32 CurrentBitmask, Int32 Flag)
    {
      return ((CurrentBitmask & Flag) == Flag);
    } // IsFlagActived

    #endregion " Private methods "

  }
}