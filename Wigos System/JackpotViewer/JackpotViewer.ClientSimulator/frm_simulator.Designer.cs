﻿namespace JackpotViewerWS.ClientSimulator
{
  partial class frm_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tp_jackpots_awards = new System.Windows.Forms.TabControl();
      this.tp_jackpot_info = new System.Windows.Forms.TabPage();
      this.lbl_ji_response = new System.Windows.Forms.Label();
      this.txt_ji_response = new System.Windows.Forms.TextBox();
      this.btn_ji_execute = new System.Windows.Forms.Button();
      this.txt_ji_jackpot_viewer_code = new System.Windows.Forms.TextBox();
      this.lbl_ji_jackpot_viewer_code = new System.Windows.Forms.Label();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.lbl_ja_response = new System.Windows.Forms.Label();
      this.txt_ja_response = new System.Windows.Forms.TextBox();
      this.button1 = new System.Windows.Forms.Button();
      this.txt_ja_jackpot_viewer_code = new System.Windows.Forms.TextBox();
      this.lbl_ja_jackpot_viewer_id = new System.Windows.Forms.Label();
      this.tp_jackpots_awards.SuspendLayout();
      this.tp_jackpot_info.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.SuspendLayout();
      // 
      // tp_jackpots_awards
      // 
      this.tp_jackpots_awards.Controls.Add(this.tp_jackpot_info);
      this.tp_jackpots_awards.Controls.Add(this.tabPage2);
      this.tp_jackpots_awards.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tp_jackpots_awards.Location = new System.Drawing.Point(0, 0);
      this.tp_jackpots_awards.Name = "tp_jackpots_awards";
      this.tp_jackpots_awards.SelectedIndex = 0;
      this.tp_jackpots_awards.Size = new System.Drawing.Size(712, 443);
      this.tp_jackpots_awards.TabIndex = 0;
      // 
      // tp_jackpot_info
      // 
      this.tp_jackpot_info.Controls.Add(this.lbl_ji_response);
      this.tp_jackpot_info.Controls.Add(this.txt_ji_response);
      this.tp_jackpot_info.Controls.Add(this.btn_ji_execute);
      this.tp_jackpot_info.Controls.Add(this.txt_ji_jackpot_viewer_code);
      this.tp_jackpot_info.Controls.Add(this.lbl_ji_jackpot_viewer_code);
      this.tp_jackpot_info.Location = new System.Drawing.Point(4, 22);
      this.tp_jackpot_info.Name = "tp_jackpot_info";
      this.tp_jackpot_info.Padding = new System.Windows.Forms.Padding(3);
      this.tp_jackpot_info.Size = new System.Drawing.Size(704, 417);
      this.tp_jackpot_info.TabIndex = 0;
      this.tp_jackpot_info.Text = "JackpotInfo";
      this.tp_jackpot_info.UseVisualStyleBackColor = true;
      // 
      // lbl_ji_response
      // 
      this.lbl_ji_response.AutoSize = true;
      this.lbl_ji_response.Location = new System.Drawing.Point(44, 55);
      this.lbl_ji_response.Name = "lbl_ji_response";
      this.lbl_ji_response.Size = new System.Drawing.Size(55, 13);
      this.lbl_ji_response.TabIndex = 6;
      this.lbl_ji_response.Text = "Response";
      // 
      // txt_ji_response
      // 
      this.txt_ji_response.Location = new System.Drawing.Point(105, 55);
      this.txt_ji_response.Multiline = true;
      this.txt_ji_response.Name = "txt_ji_response";
      this.txt_ji_response.ReadOnly = true;
      this.txt_ji_response.Size = new System.Drawing.Size(592, 354);
      this.txt_ji_response.TabIndex = 3;
      // 
      // btn_ji_execute
      // 
      this.btn_ji_execute.Location = new System.Drawing.Point(349, 24);
      this.btn_ji_execute.Name = "btn_ji_execute";
      this.btn_ji_execute.Size = new System.Drawing.Size(75, 23);
      this.btn_ji_execute.TabIndex = 2;
      this.btn_ji_execute.Text = "Execute";
      this.btn_ji_execute.UseVisualStyleBackColor = true;
      this.btn_ji_execute.Click += new System.EventHandler(this.btn_ji_execute_Click);
      // 
      // txt_ji_jackpot_viewer_code
      // 
      this.txt_ji_jackpot_viewer_code.Location = new System.Drawing.Point(105, 26);
      this.txt_ji_jackpot_viewer_code.Name = "txt_ji_jackpot_viewer_code";
      this.txt_ji_jackpot_viewer_code.Size = new System.Drawing.Size(238, 20);
      this.txt_ji_jackpot_viewer_code.TabIndex = 0;
      // 
      // lbl_ji_jackpot_viewer_code
      // 
      this.lbl_ji_jackpot_viewer_code.AutoSize = true;
      this.lbl_ji_jackpot_viewer_code.Location = new System.Drawing.Point(-2, 29);
      this.lbl_ji_jackpot_viewer_code.Name = "lbl_ji_jackpot_viewer_code";
      this.lbl_ji_jackpot_viewer_code.Size = new System.Drawing.Size(102, 13);
      this.lbl_ji_jackpot_viewer_code.TabIndex = 0;
      this.lbl_ji_jackpot_viewer_code.Text = "JackpotViewerCode";
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.lbl_ja_response);
      this.tabPage2.Controls.Add(this.txt_ja_response);
      this.tabPage2.Controls.Add(this.button1);
      this.tabPage2.Controls.Add(this.txt_ja_jackpot_viewer_code);
      this.tabPage2.Controls.Add(this.lbl_ja_jackpot_viewer_id);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(704, 417);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "JackpotAwards";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // lbl_ja_response
      // 
      this.lbl_ja_response.AutoSize = true;
      this.lbl_ja_response.Location = new System.Drawing.Point(44, 55);
      this.lbl_ja_response.Name = "lbl_ja_response";
      this.lbl_ja_response.Size = new System.Drawing.Size(55, 13);
      this.lbl_ja_response.TabIndex = 11;
      this.lbl_ja_response.Text = "Response";
      // 
      // txt_ja_response
      // 
      this.txt_ja_response.Location = new System.Drawing.Point(105, 55);
      this.txt_ja_response.Multiline = true;
      this.txt_ja_response.Name = "txt_ja_response";
      this.txt_ja_response.ReadOnly = true;
      this.txt_ja_response.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txt_ja_response.Size = new System.Drawing.Size(592, 354);
      this.txt_ja_response.TabIndex = 10;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(349, 24);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 9;
      this.button1.Text = "Execute";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.btn_ja_execute_Click);
      // 
      // txt_ja_jackpot_viewer_code
      // 
      this.txt_ja_jackpot_viewer_code.Location = new System.Drawing.Point(105, 26);
      this.txt_ja_jackpot_viewer_code.Name = "txt_ja_jackpot_viewer_code";
      this.txt_ja_jackpot_viewer_code.Size = new System.Drawing.Size(238, 20);
      this.txt_ja_jackpot_viewer_code.TabIndex = 7;
      // 
      // lbl_ja_jackpot_viewer_id
      // 
      this.lbl_ja_jackpot_viewer_id.AutoSize = true;
      this.lbl_ja_jackpot_viewer_id.Location = new System.Drawing.Point(-2, 29);
      this.lbl_ja_jackpot_viewer_id.Name = "lbl_ja_jackpot_viewer_id";
      this.lbl_ja_jackpot_viewer_id.Size = new System.Drawing.Size(102, 13);
      this.lbl_ja_jackpot_viewer_id.TabIndex = 8;
      this.lbl_ja_jackpot_viewer_id.Text = "JackpotViewerCode";
      // 
      // frm_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(712, 443);
      this.Controls.Add(this.tp_jackpots_awards);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_simulator";
      this.Text = "JackpotViewer WS Client Simulator";
      this.Load += new System.EventHandler(this.Form_Load);
      this.tp_jackpots_awards.ResumeLayout(false);
      this.tp_jackpot_info.ResumeLayout(false);
      this.tp_jackpot_info.PerformLayout();
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tp_jackpots_awards;
    private System.Windows.Forms.TabPage tp_jackpot_info;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Label lbl_ji_response;
    private System.Windows.Forms.TextBox txt_ji_response;
    private System.Windows.Forms.Button btn_ji_execute;
    private System.Windows.Forms.TextBox txt_ji_jackpot_viewer_code;
    private System.Windows.Forms.Label lbl_ji_jackpot_viewer_code;
    private System.Windows.Forms.Label lbl_ja_response;
    private System.Windows.Forms.TextBox txt_ja_response;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.TextBox txt_ja_jackpot_viewer_code;
    private System.Windows.Forms.Label lbl_ja_jackpot_viewer_id;
  }
}

