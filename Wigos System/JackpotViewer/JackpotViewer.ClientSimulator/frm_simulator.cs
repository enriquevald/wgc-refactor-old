﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: frm_simulator.cs
// 
//      DESCRIPTION: Winform JackpotViewerWS Simulator
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 06-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Windows.Forms;
using JackpotViewerWS.ClientSimulator.JackpotViewerService;
using System.Web.Script.Serialization;
using System.Text;

namespace JackpotViewerWS.ClientSimulator
{
  public partial class frm_simulator : Form
  {

    #region Members

    JackpotViewerService.JackpotViewerServiceClient m_service;

    #endregion

    const string NEW_LINE = "\r\n";

    #region Constructor

    public frm_simulator()
    {
      InitializeComponent();
      
      m_service = new JackpotViewerService.JackpotViewerServiceClient();
    }

    #endregion

    #region events

    /// <summary>
    /// Form load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Form_Load(object sender, EventArgs e)
    {

    } //Form_Load

    /// <summary>
    /// PlayerAwards button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ja_execute_Click(object sender, EventArgs e)
    {
      Int32 _jackpot_code;
      JackpotsAwardsRequest _request;
      JackpotsAwardsResponse _response = new JackpotsAwardsResponse();

      try
      {

        if (!Int32.TryParse(txt_ja_jackpot_viewer_code.Text.Trim(), out _jackpot_code))
        {
          txt_ja_response.Text = String.Format("Código incorrecto");
          return;
        }

        _request = new JackpotsAwardsRequest()
        {
          JackpotViewerCode = _jackpot_code
        };

        _response = m_service.JackpotsAwards(_request);

        if (_response == null)
        {
          return;
        }

        if (_response.Error.Code == ErrorCode.None)
        {
          txt_ja_response.Text = ShowDataAwardResponse(_response).ToString();
        }
        else
        {
          txt_ja_response.Text = String.Format("ErrorCode: {0} {1}", _response.Error.Code, NEW_LINE);
          txt_ja_response.Text += String.Format("ErrorMessage: {0} {1}", _response.Error.Message, NEW_LINE);
        }
      }
      catch (Exception _ex)
      {
        MessageBox.Show(_ex.Message, "Error");
      }

    } // btn_ja_execute_Click

    /// <summary>
    /// Functions to show response's data from jackpot viewer 
    /// </summary>
    private StringBuilder ShowDataAwardResponse(JackpotsAwardsResponse _response)
    {
      StringBuilder _sb = new StringBuilder();
      int _index = 1;

      try
      {
        _sb.AppendLine(string.Format("Jackpot Viewer : {0}"   , _response.Name.ToString()));
        _sb.AppendLine(string.Format("Code: {0}"              , _response.Code.ToString()));
        _sb.AppendLine(string.Format("Enabled: {0}"           , _response.Enabled.ToString()));
        _sb.AppendLine(string.Format("Id: {0}"                , _response.Id.ToString()));
        _sb.AppendLine(string.Format("IsoCode: {0}"           , _response.IsoCode.ToString()));


        foreach (JackpotAwardItemList _item in _response.JackpotsAwards)
        {
          _sb.AppendLine(string.Format(""));
          _sb.AppendLine(string.Format("  Award : {0}"        , _item.AwardId.ToString()));

          foreach (JackpotAwardItem _item_awarded in _item.Awards)
          {           
            _sb.AppendLine(string.Format("  Last Award : {0}"                                                                                         , _index));
            _sb.AppendLine((_item_awarded.AccountId == -1) ? string.Format("  AccountId: Cuenta no añadida") : string.Format("  AccountId: {0}"       , _item_awarded.AccountId.ToString()));
            _sb.AppendLine(string.Format("  Amount: {0}"                                                                                              , _item_awarded.Amount.ToString()));
            _sb.AppendLine(string.Format("  DateAward: {0}"                                                                                           , _item_awarded.DateAward.ToString()));
            _sb.AppendLine(string.Format("  Id: {0}"                                                                                                  , _item_awarded.Id.ToString()));
            _sb.AppendLine(string.Format("  JackpotId: {0}"                                                                                           , _item_awarded.JackpotId.ToString()));
            _sb.AppendLine(string.Format("  Name: {0}"                                                                                                , _item_awarded.Name.ToString()));
            _sb.AppendLine(string.Format("  Position: {0}"                                                                                            , _item_awarded.Position.ToString()));
            _sb.AppendLine((_item_awarded.TerminalId == -1) ? string.Format("  TerminalId: Terminal no asignado") : string.Format("  TerminalId: {0}" , _item_awarded.TerminalId.ToString()));
            _sb.AppendLine((_item_awarded.Type == 0) ? string.Format("  Type : Tipo no asignado") : string.Format("  Type :"                          , _item_awarded.Type.ToString()));
            _index++;
          }
        }

        return _sb;

      }
      catch(Exception ex)
      {
        _sb.Clear();
        return _sb.AppendLine(string.Format("Error : {0}", ex.Message));

      }
      
    }

    /// <summary>
    /// PlayerInfo button click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_ji_execute_Click(object sender, EventArgs e)
    {
      Int32 _jackpot_viewer_code;
      JackpotsInfoRequest _request;
      JackpotsInfoResponse _response;

      try
      {
        if (!Int32.TryParse(txt_ji_jackpot_viewer_code.Text.Trim(), out _jackpot_viewer_code))
        {
          txt_ji_response.Text = String.Format("Código incorrecto");
          return;
        }

        _request = new JackpotsInfoRequest()
        {
          JackpotViewerCode = _jackpot_viewer_code
        };

        _response = m_service.JackpotsInfo(_request);

        if (_response == null)
        {
          return;
        }

        if (_response.Error.Code == ErrorCode.None)
        {
          txt_ji_response.Text = ShowDataJackpotinfo(_response).ToString();
        }
        else
        {
          txt_ji_response.Text = String.Format("ErrorCode: {0} {1}", _response.Error.Code, NEW_LINE);
          txt_ji_response.Text += String.Format("ErrorMessage: {0} {1}", _response.Error.Message, NEW_LINE);
        }
      }
      catch (Exception _ex)
      {
        MessageBox.Show(_ex.Message, "Error");
      }

    } // btn_ji_execute_Click

    /// <summary>
    /// Function to show response's data from jackpotinfo
    /// </summary>
    /// <returns></returns>
    private StringBuilder ShowDataJackpotinfo(JackpotsInfoResponse _response)
    {
      StringBuilder _sb = new StringBuilder();

      try 
      {
        _sb.AppendLine(string.Format("Jackpot Viewer : {0}", _response.Id.ToString()));
        _sb.AppendLine(string.Format("Name: {0}", _response.Name.ToString()));
        _sb.AppendLine(string.Format("Code: {0}", _response.Code.ToString()));
        _sb.AppendLine(string.Format("IsoCode: {0}", _response.IsoCode.ToString()));
        _sb.AppendLine(string.Format("Enabled: {0}", _response.Enabled.ToString()));

        foreach (JackpotViewerItem _item in _response.Jackpots)
        {
          _sb.AppendLine(string.Format(""));
          _sb.AppendLine(string.Format("  Jackpot : {0}", _item.Name.ToString()));
          _sb.AppendLine(string.Format("  Status  : {0}", _item.Status.ToString()));
          _sb.AppendLine(string.Format("  Order   : {0}", _item.Order.ToString()));
          _sb.AppendLine(string.Format("  Main    : {0}", _item.Main.ToString()));
          _sb.AppendLine(string.Format("  Hidden  : {0}", _item.Hidden.ToString()));
          _sb.AppendLine(string.Format("  PrizeSharing : {0}", _item.PrizeSharing.ToString()));
          _sb.AppendLine(string.Format("  HappyHOur : {0}", _item.HappyHour.ToString()));
          _sb.AppendLine(string.Format("  IsoCode : {0}", _item.IsoCode.ToString()));
          _sb.AppendLine(string.Format("  IsRelated : {0}", _item.IsRelated.ToString()));

        }

        return _sb;
      }
      catch(Exception ex)
      {
        _sb.Clear();
        return _sb.AppendLine(string.Format("Error : {0}", ex.Message));
      }
    }
    #endregion

  }

}
