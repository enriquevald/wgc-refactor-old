﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Settings.cs
// 
//      DESCRIPTION: Class for 
// 
//           AUTHOR: Pablo Molina
// 
//    CREATION DATE: 02-NOV-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2016 PDM    First release.
//------------------------------------------------------------------------------

using System.Configuration;

namespace FB_ClientSimulator
{
  public class Settings
  {
    #region Properties

    public static string PosId
    {
      get
      {
        return ConfigurationManager.AppSettings["PosId"]; ;
      }
    }

    public static string Login
    {
      get
      {
        return ConfigurationManager.AppSettings["Login"]; ;
      }
    }

    public static string Password
    {
      get
      {
        return ConfigurationManager.AppSettings["Password"]; ;
      }
    }

    #endregion

  }
}
