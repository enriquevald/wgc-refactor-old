﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: JackpotsInfo.cs
// 
//      DESCRIPTION: JackpotsInfo Class for Web Service
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Net;
using JackpotViewerWS.Common.Request;
using JackpotViewerWS.Common.Response;
using JackpotViewerWS.Common;
using WSI.Common.Jackpot;
using WSI.Common;

namespace JackpotViewerWS.Service.Classes
{
  public class JackpotsInfo : Base
  {
    
    #region " Public Methods "

    /// <summary>
    /// Process Autenticate Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public JackpotsInfoResponse ProcessRequest(JackpotsInfoRequest Request)
    {
      return JackpotsInfoInternal(Request);
    } // ProcessRequest

    #endregion 

    #region "Protected Methods"
    
    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Get jackpots info internal
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    internal JackpotsInfoResponse JackpotsInfoInternal(JackpotsInfoRequest Request)
    {
      JackpotViewer _jackpot_viewer;
           
      _jackpot_viewer = new JackpotViewer();
      
      // Get Jackpots by JackpotViewerId
      if(!_jackpot_viewer.Read(Request.JackpotViewerCode))
      {
        return JackpotInfoErrorResponse(Request.JackpotViewerCode);
      }

      return _jackpot_viewer.ToJackpotInfoResponse();

    } // JackpotsInfoInternal

    /// <summary>
    /// Returns error response
    /// </summary>
    /// <returns></returns>
    internal JackpotsInfoResponse JackpotInfoErrorResponse(Int32 JackpotViewerId)
    {
      // Set error 
      Error.Code    = ErrorCode.Critical;
      Error.Message = String.Format("JackpotsInfo: Error on get Jackpots related to JackpotViewer {0}", JackpotViewerId);

      // Write error in log. 
      Log.Message(Error.Message);

      // Return error response
      return new JackpotsInfoResponse()
      {
        Error = Error
      };
    } // JackpotInfoErrorResponse

    #endregion

  }
}