﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: JackpotsAwards.cs
// 
//      DESCRIPTION: JackpotsAwards Class for Web Service
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 01-JUN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUN-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Net;
using WSI.Common;
using JackpotViewerWS.Common.Request;
using JackpotViewerWS.Common.Response;
using JackpotViewerWS.Common;
using WSI.Common.Jackpot;

namespace JackpotViewerWS.Service.Classes
{
  public class JackpotsAwards : Base
  {

    #region " Public Methods "

    /// <summary>
    /// Process Autenticate Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public JackpotsAwardsResponse ProcessRequest(JackpotsAwardsRequest Request)
    {
      return JackpotsAwardsInternal(Request);
    } // ProcessRequest

    #endregion

    #region "Protected Methods"

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <returns></returns>
    private Boolean RequestDataValidation(JackpotsAwardsRequest Request)
    {
      // Nothing to validate in this moment. 

      return true;

    } // RequestDataValidation

    #endregion

    #region " Internal Methods "

    /// <summary>
    /// Get jackpots info internal
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    internal JackpotsAwardsResponse JackpotsAwardsInternal(JackpotsAwardsRequest Request)
    {
      JackpotViewer _jackpot_viewer;

      _jackpot_viewer = new JackpotViewer();

      // Get Jackpots by JackpotViewerId
      if (!_jackpot_viewer.ReadAwards(Request.JackpotViewerCode))
      {
        return JackpotsAwardsErrorResponse(Request.JackpotViewerCode);
      }

      if (_jackpot_viewer.Jackpots == null
        || _jackpot_viewer.Jackpots.Items == null
        || _jackpot_viewer.Jackpots.Items.Count == 0)
      {
        return JackpotAwardsEmptyJackpotsResponse(Request.JackpotViewerCode);
      }

      return _jackpot_viewer.ToJackpotsAwardsResponse();

    } // JackpotsAwardsInternal

    /// <summary>
    /// Returns error response
    /// </summary>
    /// <returns></returns>
    internal JackpotsAwardsResponse JackpotsAwardsErrorResponse(Int32 JackpotViewerId)
    {
      // Set error 
      Error.Code = ErrorCode.Critical;
      Error.Message = String.Format("JackpotsAwards: Error on get Jackpots awards related to JackpotViewer {0}", JackpotViewerId);

      // Write error in log. 
      Log.Message(Error.Message);

      // Return error response
      return new JackpotsAwardsResponse()
      {
        Error = Error
      };
    } // JackpotsAwardsErrorResponse

    /// <summary>
    /// Returns empty jackpots response
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    private JackpotsAwardsResponse JackpotAwardsEmptyJackpotsResponse(Int32 JackpotViewerId)
    {
      // Set error 
      Error.Code = ErrorCode.NoJackpots;
      Error.Message = String.Format("No Jackpots related to JackpotViewer {0}", JackpotViewerId);

      // Return error response
      return new JackpotsAwardsResponse()
      {
        Error = Error
      };
    } 

    #endregion

  } // JackpotsAwards
}