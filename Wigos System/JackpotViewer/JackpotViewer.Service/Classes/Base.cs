﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: Base.cs
// 
//      DESCRIPTION: Base Class for FB Web Service
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 28-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2016 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Net;
using System.Text;
using WSI.Common;
using JackpotViewerWS.Common.Request;
using JackpotViewerWS.Common;
using System.Data.SqlClient;
using JackpotViewerWS.Common.Response;

namespace JackpotViewerWS.Service.Classes
{
  public class Base
  {
    
    #region " Members "

    private ErrorResponse m_error;

    #endregion 

    #region " Properties "

    public ErrorResponse Error
    {
      get 
      {
        if (m_error == null)
        {
          m_error = new ErrorResponse();
        }
        
        return m_error; 
      }
      set { m_error = value; }
    } // Error

    #endregion 

    #region " Protected Methods "
    
    #endregion

    #region " Private Methods "

    #endregion

  } // Base
}