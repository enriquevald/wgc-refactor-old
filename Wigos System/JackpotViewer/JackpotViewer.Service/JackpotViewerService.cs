﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: JackpotViewerService.cs
// 
//      DESCRIPTION: FB Web Service methods
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 11-NOV-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release.
//------------------------------------------------------------------------------
using JackpotViewerWS.Service.Classes;
using JackpotViewerWS.Common.Request;
using JackpotViewerWS.Common.Response;
using JackpotViewerWS.Common;
using System.ServiceModel;

namespace JackpotViewerWS.Service
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
  public class JackpotViewerService : IJackpotViewerService
  {
    /// <summary>
    /// Get Jackpots info related to JackpotViewer info method
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>JackpotsInfo</returns>
    public JackpotsInfoResponse JackpotsInfo(JackpotsInfoRequest Request)
    {
      return new JackpotsInfo().ProcessRequest(Request);
    } // JackpotsInfo

    /// <summary>
    /// Get Jackpots awards related to JackpotViewer info method
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>JackpotsAwardsRequest</returns>
    public JackpotsAwardsResponse JackpotsAwards(JackpotsAwardsRequest Request)
    {
      return  new JackpotsAwards().ProcessRequest(Request);
      
    } // JackpotsAwards

  } // JackpotViewerService
}
