﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: JackpotViewerListener.cs
// 
//      DESCRIPTION: JackpotViewer Listener
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 31-MAY-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAY-2017 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WSI.Common;
using JackpotViewerWS.Common;

namespace JackpotViewerWS.Listener
{
  public class JackpotViewerListener 
  {
    #region " Members " 

    private IJackpotViewerService m_service_listener;
    private ServiceHost m_host_listener;

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="ServiceListener"></param>
    public JackpotViewerListener(IJackpotViewerService ServiceListener)
    {
      m_service_listener = ServiceListener;
      Log.Message("Starting JackpotViewerListener...");
      OpenHostListener();
    }
    // JackpotViewerListener

    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    } //Dispose

    #endregion 

    #region " Private Methods "

    /// <summary>
    /// Open host listener
    /// </summary>
    private void OpenHostListener()
    {
      ServiceMetadataBehavior _smb;
      Uri _base_address;

      _base_address = new Uri(GeneralParam.GetString("JackpotViewer", "HostAddress", "http://0.0.0.0:7744"));

      Log.Message(String.Format("JackpotViewerListener: Starting at {0}", _base_address));

      m_host_listener = new ServiceHost(m_service_listener, _base_address);
      _smb = m_host_listener.Description.Behaviors.Find<ServiceMetadataBehavior>() ?? new ServiceMetadataBehavior();

      // If not, add one
      _smb.HttpGetEnabled = true;
      _smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
      m_host_listener.Description.Behaviors.Add(_smb);

      m_host_listener.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexHttpBinding(), "mex");
      
      BasicHttpBinding _binding = new BasicHttpBinding();

      m_host_listener.AddServiceEndpoint(typeof(IJackpotViewerService), _binding, _base_address);
      m_host_listener.Faulted += host_listener_Faulted;
      m_host_listener.Closed += host_listener_Closed;
      
      m_host_listener.Open();

      Log.Message("JackpotViewerListener: Started");

    } // OpenHostListener

    #endregion

    #region " Protected "

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="Disposing"></param>
    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing || m_host_listener == null)
      {
        return;
      }

      // Close listener
      if (m_host_listener.State != CommunicationState.Closed)
      {
        m_host_listener.Faulted -= host_listener_Faulted;
        m_host_listener.Closed -= host_listener_Closed;

        m_host_listener.Close();
      }

      m_host_listener = null;

    } //Dispose

    #endregion
    
    #region " Events "

    private void host_listener_Faulted(object Sender, EventArgs E)
    {
      Log.Message("JackpotViewerListener: Faulted");

      m_host_listener.Faulted -= host_listener_Faulted;
      m_host_listener = null;
      OpenHostListener();

    } // host_listener_Faulted

    private void host_listener_Closed(object Sender, EventArgs E)
    {
      Log.Message("JackpotViewerListener: Closed");

      m_host_listener.Faulted -= host_listener_Closed;
      m_host_listener = null;
      OpenHostListener();

    } // host_listener_Closed

    #endregion

  } // JackpotViewerListener
}
