﻿using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams
{
	/// <summary>
	/// Implements S2S site properties for general params
	/// </summary>
	public static class Site
	{
		public static string Identifier
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Site.GroupKey,
					Constants.Site.Identifier,
					Constants.Site.IdentifierDefault);
			}
		}
	}
}
