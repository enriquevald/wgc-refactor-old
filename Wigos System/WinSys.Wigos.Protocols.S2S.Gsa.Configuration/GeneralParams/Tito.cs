﻿using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams
{
	/// <summary>
	/// Implements S2S tito properties for general params
	/// </summary>
	public static class Tito
	{
		public static bool IsPendingPrintAllowToRedeem
		{
			get
			{
				return GeneralParam.GetBoolean(
					Constants.Tito.GroupKey,
					Constants.Tito.PendingPrintAllowToRedeem,
					Constants.Tito.PendingPrintAllowToRedeemDefault);
			}
		}

		public static int CashableTicketsExpirationDays
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Tito.GroupKey,
					Constants.Tito.CashableTicketsExpirationDays,
					Constants.Tito.CashableTicketsExpirationDaysDefault);
			}
		}

		public static int PromotionalTicketsRedeemableExpirationDays
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Tito.GroupKey,
					Constants.Tito.PromotionalTicketsRedeemableExpirationDays,
					Constants.Tito.CashableTicketsExpirationDaysDefault);
			}
		}

		public static int PromotionalTicketsNonRedeemableExpirationDays
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Tito.GroupKey,
					Constants.Tito.PromotionalTicketsNonRedeemableExpirationDays,
					Constants.Tito.CashableTicketsExpirationDaysDefault);
			}
		}

		public static string TicketsLocation
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.TicketsLocation,
					string.Empty);
			}
		}

		public static string TicketsAddress1
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.TicketsAddress1,
					string.Empty);
			}
		}

		public static string TicketsAddress2
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.TicketsAddress2,
					string.Empty);
			}
		}

		public static string CashableTicketsTitle
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.CashableTicketsTitle,
					string.Empty);
			}
		}

		public static string PromotionalTicketsRedeemableTitle
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.PromotionalTicketsRedeemableTitle,
					string.Empty);
			}
		}

		public static string PromotionalTicketsNonRedeemableTitle
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.PromotionalTicketsNonRedeemableTitle,
					string.Empty);
			}
		}

		public static string CashableTicketsTitleJackpot
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Tito.GroupKey,
					Constants.Tito.CashableTicketsTitleJackpot,
					string.Empty);
			}
		}

		public static int TicketsSystemId
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Tito.GroupKey,
					Constants.Tito.TicketsSystemId,
					0);
			}
		}
	}
}