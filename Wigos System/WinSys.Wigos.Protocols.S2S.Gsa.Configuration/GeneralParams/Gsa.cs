﻿using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams
{
	/// <summary>
	/// Implements S2S GSA properties for general params
	/// </summary>
	public static class Gsa
	{
		public static bool IsEnabled
		{
			get
			{
				return GeneralParam.GetBoolean(
					Constants.Gsa.GroupKey,
					Constants.Gsa.Enable,
					Constants.Gsa.EnableDefault);
			}
		}

		public static string Ip
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Gsa.GroupKey,
					Constants.Gsa.Ip,
					Constants.Gsa.IpDefault);
			}
		}

		public static string Port
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Gsa.GroupKey,
					Constants.Gsa.Port,
					Constants.Gsa.PortDefault);
			}
		}

		public static int KeepAliveCommandInterval
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.KeepAliveCommandInterval,
					Constants.Gsa.KeepAliveCommandIntervalDefault);
			}
		}

		public static int ValidationIdListSize
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.ValidationIdListSize,
					Constants.Gsa.ValidationIdListSizeDefault);
			}
		}

		public static bool IsConfigurationIdAutoIncreaseable
		{
			get
			{
				return GeneralParam.GetBoolean(
					Constants.Gsa.GroupKey,
					Constants.Gsa.ConfigurationIdAutoIncreaseable,
					Constants.Gsa.ConfigurationIdAutoIncreaseableDefault);
			}
		}

		public static int SendRetryInterval
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.SendRetryInterval,
					Constants.Gsa.SendRetryIntervalDefault);
			}
		}

		public static int ClientConnectionTimeout
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.ClientConnectionTimeout,
					Constants.Gsa.ClientConnectionTimeoutDefault);
			}
		}

		public static string ProviderName
		{
			get
			{
				return GeneralParam.GetString(
					Constants.Gsa.GroupKey,
					Constants.Gsa.ProviderName,
					Constants.Gsa.ProviderNameDefaultValue);
			}
		}

		public static int OffsetMinutesToExpireSeedValues
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.OffsetMinutesToExpireSeedValues,
					Constants.Gsa.OffsetMinutesToExpireSeedValuesDefaultValue);
			}
		}

		public static int LogLevel
		{
			get
			{
				return GeneralParam.GetInt32(
					Constants.Gsa.GroupKey,
					Constants.Gsa.LogLevel,
					Constants.Gsa.LogLevelDefaultValue);
			}
		}
	}
}