﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.Constants
{
	public static class Gsa
	{
		public const string GroupKey = "GSA";

		public const string Enable = "S2S.Enable";
		public const bool EnableDefault = false;

		public const string Ip = "S2S.Ip";
		public const string IpDefault = "localhost";

		public const string Port = "S2S.Port";
		public const string PortDefault = "8890";

		public const string KeepAliveCommandInterval = "S2S.KeepAliveCommandInterval";
		public const int KeepAliveCommandIntervalDefault = 60000;

		public const string ValidationIdListSize = "S2S.ValidationIdListSize";
		public const int ValidationIdListSizeDefault = 3;

		public const string ConfigurationIdAutoIncreaseable = "S2S.ConfigurationIdAutoIncreaseable";
		public const bool ConfigurationIdAutoIncreaseableDefault = false;

		public const string SendRetryInterval = "S2S.SendRetryInterval";
		public const int SendRetryIntervalDefault = 30000;

		public const string ClientConnectionTimeout = "S2S.ClientConnectionTimeout";
		public const int ClientConnectionTimeoutDefault = 30000;

		public const string ProviderName = "S2S.ProviderName";
		public const string ProviderNameDefaultValue = "AMTOTE";

		public const string OffsetMinutesToExpireSeedValues = "S2S.OffsetMinutesToExpireSeedValues";
		public const int OffsetMinutesToExpireSeedValuesDefaultValue = 60;

		public const string LogLevel = "S2S.LogLevel";
		public const int LogLevelDefaultValue = 3;
	}
}