﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.Constants
{
	public static class Site
	{
		public const string GroupKey = "Site";

		public const string Identifier = "Identifier";
		public const string IdentifierDefault = "S2S_all";
	}
}
