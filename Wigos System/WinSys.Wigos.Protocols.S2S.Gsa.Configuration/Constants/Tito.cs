﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.Constants
{
	public static class Tito
	{
		public const string GroupKey = "TITO";

		public const string PendingPrintAllowToRedeem = "PendingPrint.AllowToRedeem";
		public const bool PendingPrintAllowToRedeemDefault = true;

		public const string CashableTicketsExpirationDays = "CashableTickets.ExpirationDays";
		public const string PromotionalTicketsRedeemableExpirationDays = "PromotionalTickets.Redeemable.ExpirationDays";
		public const string PromotionalTicketsNonRedeemableExpirationDays = "PromotionalTickets.NonRedeemable.ExpirationDays";
		public const int CashableTicketsExpirationDaysDefault = 30;

		public const string TicketsLocation = "Tickets.Location";

		public const string TicketsAddress1 = "Tickets.Address1";

		public const string TicketsAddress2 = "Tickets.Address2";

		public const string CashableTicketsTitle = "CashableTickets.Title";

		public const string PromotionalTicketsRedeemableTitle = "PromotionalTickets.Redeemable.Title";

		public const string PromotionalTicketsNonRedeemableTitle = "PromotionalTickets.NonRedeemable.Title";

		public const string CashableTicketsTitleJackpot = "CashableTickets.Title.Jackpot";

		public const string TicketsSystemId = "TicketsSystemId";
	}
}