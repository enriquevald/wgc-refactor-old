////------------------------------------------------------------------------------
//// Copyright � 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: OccupationSensor.cs
//// 
////      DESCRIPTION: Controls de camera occupation
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 06-JUN-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-JUN-2016 AMF    First release.
//// 13-JUN-2016 JCA    Bug 14497:SmatFloor: No historifica los contadores de la c�mara en Semana/Mes/A�o
//// 21-JUN-2016 JBC    Bug 14754:SmartFloor: Error al historificar datos de la c�mara
//// 11-JUL-2016 ETP    Bug 15278:Smartfloor: Witget Ocuppancy is not working ok.
//// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;

namespace CamaraOcupacion
{
  public static class OccupationManager
  {
    #region " Public methods "

    /// <summary>
    /// Init Camara Thread
    /// </summary>
    public static void Init()
    {
      OccupationCamaraSensor _sensor;
      Thread _thread_h;

      // Occupation Camara Sensor
      _sensor = new OccupationCamaraSensor();
      _sensor.Handler += MyDelegate;
      _sensor.Init();

      // Occupation Historification
      _thread_h = new Thread(OccupationHistorificationThread);
      _thread_h.Start();

    } // Init

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Delegate for Thread
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="NewMeterIn"></param>
    /// <param name="NewMeterOut"></param>
    private static void MyDelegate(String Name, Int64 NewMeterIn, Int64 NewMeterOut)
    {
      Int32 _sensor_id;
      Int64 _old_in;
      Int64 _old_out;
      Int64 _delta_in;
      Int64 _delta_out;

      try
      {
        _old_in = 0;
        _old_out = 0;
        _delta_in = 0;
        _delta_out = 0;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Register Sensor
          using (SqlCommand _sql_cmd = new SqlCommand("", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandText = "EXEC OccupancySensorRegister @pHostName, @pType, @pMode";
            _sql_cmd.Parameters.Add("@pHostName", System.Data.SqlDbType.NVarChar, 50).Value = Name;
            _sql_cmd.Parameters.Add("@pType", System.Data.SqlDbType.Int).Value = 2;
            _sql_cmd.Parameters.Add("@pMode", System.Data.SqlDbType.Int).Value = 4;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return;
              }

              _sensor_id = _reader.GetInt32(0);

              if (!_reader.IsDBNull(5) && !_reader.IsDBNull(6))
              {
                // Compute "Delta" based on previous meters
                _old_in = _reader.GetInt64(5);
                _old_out = _reader.GetInt64(6);
                OccupationCamaraSensor.DeltaCamaraInOut(_old_in, NewMeterIn, out _delta_in, _old_out, NewMeterOut, out _delta_out);
              }
              else
              {
                // First time "new device" --> Delta = 0
              }
            }
          }

          // Register Delta
          using (SqlCommand _sql_cmd = new SqlCommand("", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandText = "EXEC OccupancySensorDelta @pSensorId, @pOldMeterIn, @pOldMeterOut, @pNewMeterIn, @pNewMeterOut, @pDeltaIn, @pDeltaOut";
            _sql_cmd.Parameters.Add("@pSensorId", System.Data.SqlDbType.Int).Value = _sensor_id;
            _sql_cmd.Parameters.Add("@pOldMeterIn", System.Data.SqlDbType.BigInt).Value = _old_in;
            _sql_cmd.Parameters.Add("@pOldMeterOut", System.Data.SqlDbType.BigInt).Value = _old_out;
            _sql_cmd.Parameters.Add("@pNewMeterIn", System.Data.SqlDbType.BigInt).Value = NewMeterIn;
            _sql_cmd.Parameters.Add("@pNewMeterOut", System.Data.SqlDbType.BigInt).Value = NewMeterOut;
            _sql_cmd.Parameters.Add("@pDeltaIn", System.Data.SqlDbType.BigInt).Value = _delta_in;
            _sql_cmd.Parameters.Add("@pDeltaOut", System.Data.SqlDbType.BigInt).Value = _delta_out;

            if (_sql_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // MyDelegate

    /// <summary>
    /// Occupation historification Thread
    /// </summary>
    private static void OccupationHistorificationThread()
    {
      while (true)
      {
        try
        {
          Thread.Sleep(30 * 1000);

          if (Services.IsPrincipal("WCP"))
          {
            OccupationHistorification();
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // OccupationHistorificationThread

    /// <summary>
    /// Occupation historification task
    /// </summary>
    private static void OccupationHistorification()
    {
      StringBuilder _sb;
      Int64 _occupancy;

      _occupancy = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   SUM(OSE_DELTA_IN) - SUM(OSE_DELTA_OUT) ");
        _sb.AppendLine("   FROM   OCCUPANCY_SENSOR ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Get occupancy
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return;
              }

              _occupancy = _reader.GetInt64(0);

              // Check value >= 0
              if (_occupancy < 0)
              {
                _occupancy = 0;
              }
            }
          }

          // Historify occupancy
          using (SqlCommand _sql_cmd = new SqlCommand("", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandText = "EXEC SP_Update_Meter_Data_Site @pTableId, @pEntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue";
            _sql_cmd.Parameters.Add("@pTableId", System.Data.SqlDbType.Char, 1).Value = 'T';
            _sql_cmd.Parameters.Add("@pEntityId", System.Data.SqlDbType.Char, 1).Value = 'S';
            _sql_cmd.Parameters.Add("@pDate", System.Data.SqlDbType.DateTime, 1).Value = WGDB.Now;
            _sql_cmd.Parameters.Add("@pId", System.Data.SqlDbType.Int).Value = GeneralParam.GetInt32("Site", "Identifier", 0);
            _sql_cmd.Parameters.Add("@pMeterId", System.Data.SqlDbType.Int).Value = 4;
            _sql_cmd.Parameters.Add("@pMeterItem", System.Data.SqlDbType.Int).Value = 0;
            _sql_cmd.Parameters.Add("@pValue", System.Data.SqlDbType.Decimal).Value = _occupancy;

            if (_sql_cmd.ExecuteNonQuery() > 0)
            {
              _db_trx.Commit();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("OccupationSensor.OccupationHistorification");
      }
    }

    #endregion " Private methods "
  }

  public class OccupationCamaraSensor
  {
    #region " Constants "

    private static Int64 CAMARA_CYCLE = 1000000;
    private IPAddress CAMARA_MULTICAST_IP = IPAddress.Parse("239.255.0.1");
    private Int32 CAMARA_MULTICAST_PORT = 30001;

    #endregion " Constants "

    #region " Members "

    public CamaraEvent m_handler = null;
    public delegate void CamaraEvent(String CamaraName, Int64 MeterIn, Int64 MeterOut);
    public CamaraEvent Handler
    {
      get
      {
        return m_handler;
      }
      set
      {
        m_handler = value;
      }
    }

    #endregion " Members "

    #region " Public methods "

    /// <summary>
    /// Init thread
    /// </summary>
    public void Init()
    {
      Thread _thread;

      if (Services.IsPrincipal("WCP"))
      {
        _thread = new Thread(new ThreadStart(ThreadOccupationCamara));
        _thread.Start();
      }
    } // Init

    #endregion " Public methods "

    #region " Private methods "

    /// <summary>
    /// Thread principal
    /// </summary>
    private void ThreadOccupationCamara()
    {
      UdpClient _udp;
      IPEndPoint _remote;
      Byte[] _buffer;
      CamaraEvent _handler;
      String _hostname;
      Int64 _in;
      Int64 _out;
      String _str;
      Boolean _ok;
      Boolean _error;


      _error = true;
      _udp = null;

      while (true)
      {
        try
        {
          if (_error)
          {
            _error = false;

            if (_udp != null)
            {
              try { _udp.Close(); }
              catch { };
              _udp = null;
            }

            _udp = new UdpClient(new IPEndPoint(IPAddress.Any, CAMARA_MULTICAST_PORT));
            _udp.JoinMulticastGroup(CAMARA_MULTICAST_IP);
          }

          _remote = new IPEndPoint(IPAddress.None, 0);
          _buffer = _udp.Receive(ref _remote);

          _ok = (_buffer.Length == 12);
          if (!_ok) continue;

          _str = Encoding.ASCII.GetString(_buffer);
          _ok = (_str.Length == 12);
          if (!_ok) continue;

          _ok = Int64.TryParse(_str.Substring(0, 6), out _in);
          if (!_ok) continue;

          _ok = Int64.TryParse(_str.Substring(6, 6), out _out);
          if (!_ok) continue;

          try
          {
            _hostname = Dns.GetHostEntry(_remote.Address).HostName;
          }
          catch
          {
            _hostname = _remote.Address.ToString();
          }

          _handler = m_handler;
          if (_handler != null)
          {
            try
            {
              _handler(_hostname, _in, _out);
            }
            catch
            {
              ;
            }
          }

          continue;
        }
        catch
        {
          _error = true;

          Thread.Sleep(10000);
        }
      }
    } // ThreadOccupationCamara

    /// <summary>
    /// Calculate Delta In & Out
    /// </summary>
    /// <param name="In0"></param>
    /// <param name="In1"></param>
    /// <param name="DeltaIn"></param>
    /// <param name="Out0"></param>
    /// <param name="Out1"></param>
    /// <param name="DeltaOut"></param>
    internal static void DeltaCamaraInOut(Int64 In0, Int64 In1, out Int64 DeltaIn, Int64 Out0, Int64 Out1, out Int64 DeltaOut)
    {
      DeltaIn = DeltaCamara(In0, In1);
      DeltaOut = DeltaCamara(Out0, Out1);
    } // DeltaCamaraInOut

    /// <summary>
    /// Calculate delta camara
    /// </summary>
    /// <param name="Meter0"></param>
    /// <param name="Meter1"></param>
    /// <returns></returns>
    private static Int64 DeltaCamara(Int64 Meter0, Int64 Meter1)
    {
      Int64 _delta;

      _delta = 0;

      try
      {
        Meter0 = (CAMARA_CYCLE + Meter0 % CAMARA_CYCLE) % CAMARA_CYCLE;
        Meter1 = (CAMARA_CYCLE + Meter1 % CAMARA_CYCLE) % CAMARA_CYCLE;
        _delta = (CAMARA_CYCLE + Meter1 - Meter0) % CAMARA_CYCLE;

        if (_delta < 0 || _delta >= 50)
        {
          _delta = 0;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _delta;
    } // DeltaCamara

    #endregion " Private methods "
  }
}
