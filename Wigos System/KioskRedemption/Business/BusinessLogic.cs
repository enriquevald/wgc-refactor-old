﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BusinessLogic.cs
// 
//   DESCRIPTION: BusinessLogic class
// 
//        AUTHOR: David Perello
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 DPC    First version.
// 02-NOV-2017 MS     Bug 30493:[WIGOS-6310] - WRKP transaction does not count decimals
// 03-NOV-2017 AMF    Bug 30545:[WIGOS-6321] WSI.Protocols log file shows errors related to BusinessLogic.DB_GetAmountTicketsPaid function
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//------------------------------------------------------------------------------

using CashIO;
using KioskRedemption.Classes.Request;
using KioskRedemption.Classes.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using WSI.Common;
using WSI.Common.CountR;
using WSI.Common.TITO;
using System.Linq;
using System.Collections;

namespace KioskRedemption.Business
{
  public class BusinessLogic
  {
    #region " Enum "
    public enum TicketStatus
    {
      TICKET_VALID = 0,
      TICKET_UNKNOWN = 1,
      TICKET_ALREADY_PAID = 2,
      TICKET_EXPIRED = 3,
      TICKET_INVALID = 4,
      TICKET_NON_REEDEMABLE = 5,
      TICKET_OUT_OF_LIMIT = 6,
      TICKET_MISMATCH = 7,
      TICKET_BE_PAID_ANOTHER_KIOSK = 8
    }

    public enum CommunicationCode
    {
      COMMUNICATION_SUCCESSFUL = 0,
      COMMUNICATION_ERROR = 1
    }

    public enum KioskRAlarmResponse
    {
      Success = 0,
      Invalid_Wigos_Event_Code = 1,
      Invalid_Severity_Code = 2,
      Missing_Custom_Alarm_Values = 3
    }

    public enum TransactionType
    {
      Payment = 0,
      CreateTiquet = 1
    }

    public enum TransactionStatus
    {
      TRANSACTION_OK = 0,
      TRANSACTION_TICKET_PAID_NO_VALID = 1,
      TRANSACTION_TICKET_NO_PAID_NO_VALID = 2,
      TRANSACTION_KIOSD_ID_MISMATCH = 3,
      TRANSACTION_TICKET_STATUS_INCORRECT = 4,
      TRANSACTION_AMOUNT_MISMATCH = 5,
      TRANSACTION_AMOUNT_NOT_MATCH_PAID_AMOUNT = 6,
      TRANSACTION_ISSUED_TICKET_NO_VALID = 7
    }

    #endregion // Enum

    #region " Properties "

    public Int32 KioskId { get; set; }
    public String IP { get; set; }
    public String Provider { get; set; }
    public String KioskName { get; set; }
    public Int64 OperationId { get; set; }

    #endregion // Properties

    #region " Public Methods "

    /// <summary>
    /// Get Redemption Kiosk Status
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    public Boolean GetRedemptionKioskStatus(KioskStatusRequest Request, out KioskStatusResponse Response, out HttpStatusCode Status)
    {
      Boolean _return;
      Response = new KioskStatusResponse();
      Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
      Status = HttpStatusCode.InternalServerError;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _return = DB_CheckRedemptionKioskAndSetActivity(_db_trx.SqlTransaction, out Status);
          Response.ResponseCode = GetResponseCode(Status, this.KioskId);
          if (_return)
          {
            _db_trx.Commit();
          }
        }

        return _return;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        Log.Message(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Error: {0} ", this.KioskId));
      }

      return false;
    } // GetRedemptionKioskStatus

    /// <summary>
    /// GetRedemptionTicketData
    /// </summary>
    /// <param name="TicketID"></param>
    /// <param name="RedemptionKioskID"></param>
    /// <param name="IPAddress"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    public Boolean GetRedemptionTicketData(String TicketID, out TicketDataReponse Response, out HttpStatusCode Status)
    {
      Int64 _tatal_payable;
      TicketStatus _ResponseCode;
      Ticket _TicketDetails = new Ticket();
      CountR _CountRDetails = new CountR();
      List<KioskRedemption.Classes.Common.Tax> _taxes;
      ACKRedeemKiosk _CountRResponse = new ACKRedeemKiosk();

      Response = new TicketDataReponse();
      Status = HttpStatusCode.InternalServerError;
      Response.ResponseCode = TicketStatus.TICKET_UNKNOWN;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!CheckRedemptionKioskTicket(TicketID, 0, false, false, out _ResponseCode, out _TicketDetails, out _CountRDetails, out Status, _db_trx.SqlTransaction))
          {
            Response.ResponseCode = _ResponseCode;

            if (_ResponseCode != TicketStatus.TICKET_NON_REEDEMABLE)
            {
              SendAlarm(AlarmCode.CountR_GetTicket_Error, GetTicketStatusAlarmDescription(_ResponseCode), AlarmSeverity.Warning);
            }

            return HttpStatusCode.OK == Status;
          }

          Response.ResponseCode = _ResponseCode;
          Response.ResponseTicketData = new TicketData();
          Response.ResponseTicketData.TicketCode = TicketID;

          Response.ResponseTicketData.Amount = (int)(_TicketDetails.Amount * 100);

          if (!SetTicketTaxes(_TicketDetails, out _taxes, out _tatal_payable))
          {
            return false;
          }

          Response.ResponseTicketData.Taxes.AddRange(_taxes);
          Response.ResponseTicketData.AmountToPay = _tatal_payable;

          switch (_TicketDetails.TicketType)
          {
            case TITO_TICKET_TYPE.CASHABLE:
              Response.ResponseTicketData.CreditType = "RE";
              break;
            case TITO_TICKET_TYPE.PROMO_REDEEM:
              Response.ResponseTicketData.CreditType = "PRE";
              break;
            case TITO_TICKET_TYPE.PROMO_NONREDEEM:
              Response.ResponseTicketData.CreditType = "PNR";
              break;
            default:
              Response.ResponseTicketData.CreditType = "UNK";
              Log.Warning(String.Format("KioskRedemtion.BusinessLogic.GetRedemptionTicketData: TicketType not defined: {0}", _TicketDetails.TicketType));
              break;
          }

        }

        Status = HttpStatusCode.OK;

        return true;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        Log.Message(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Error: {0} ", this.KioskId));
      }

      return false;
    } // GetRedemptionTicketData

    /// <summary>
    /// Create a new ticket
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RedemptionKioskId"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    public Boolean CreateTicket(CreateTicketRequest Request, out CreateTicketResponse Response, out HttpStatusCode Status)
    {
      Ticket _ticket;
      String _ticket_number;
      ACKRedeemKiosk _redeem_kiosk;
      TITO_TICKET_TYPE _ticket_type;
      Int64 _sequence_id;

      Response = new CreateTicketResponse();
      Status = HttpStatusCode.InternalServerError;

      try
      {
        _ticket_number = String.Empty;
        _ticket_type = TITO_TICKET_TYPE.CASHABLE;

        Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;

        if (Request.Amount <= 0)
        {
          Status = HttpStatusCode.OK;
          Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
          Log.Error("RedeemKiosk.BusinessLogic.CreateTicket: Ticket Amount equal or less than 0");

          return false;
        }

        switch (Request.CreditType)
        {
          case "RE":
            _ticket_type = TITO_TICKET_TYPE.CASHABLE;
            break;
          case "PNR":
            _ticket_type = TITO_TICKET_TYPE.PROMO_NONREDEEM;
            break;
          case "PRE":
            _ticket_type = TITO_TICKET_TYPE.PROMO_REDEEM;
            break;
          default:
            Status = HttpStatusCode.OK;
            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
            Log.Error(String.Format("RedeemKiosk.BusinessLogic.CreateTicket: Ticket Type not defined {0}", Request.CreditType));

            return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {

          if (!DB_CheckRedemptionKioskAndSetActivity(_db_trx.SqlTransaction, out Status))
          {
            return false;
          }
          Status = HttpStatusCode.InternalServerError;

          //Create a ticket
          if (!CountRBusinessLogic.GetValidAccountNumber(this.KioskId, out _redeem_kiosk, out _sequence_id, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (_redeem_kiosk.Ack == CountRBusinessLogic.Ack.ACK)
          {
            _ticket_number = _redeem_kiosk.ValidationNumberSeq;
            _redeem_kiosk = null;

            //Update the amount
            if (CountRBusinessLogic.ChargeAccount(this.KioskId, _ticket_number, Request.Amount, _ticket_type, _sequence_id, out _redeem_kiosk, _db_trx.SqlTransaction))
            {
              if (_redeem_kiosk.Ack == CountRBusinessLogic.Ack.ACK)
              {
                _redeem_kiosk = null;

                //Get ticket, we need the expiration and creation date
                if (GetTicket(_ticket_number, out _ticket, _db_trx.SqlTransaction))
                {
                  //Make the response
                  if (!SetResponseCreateTicket(Request, this.KioskId, _ticket, ref Response))
                  {
                    Response.ResponseCode = GetResponseCode(HttpStatusCode.InternalServerError, this.KioskId);
                  }
                  else
                  {
                    Status = HttpStatusCode.OK;
                    _db_trx.Commit();
                  }
                }
              }
            }
          }

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Response.ResponseCode = GetResponseCode(Status, this.KioskId);
      return false;
    } // CreateTicket

    /// <summary>
    /// End payment transaction
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Amount"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean EndRedemptionKioskPayment(Ticket PaidTicket, out TransactionResponse Response, out HttpStatusCode Status, SqlTransaction SqlTrx)
    {
      CountR _countr;
      CashierSessionInfo _cashier_session_info;
      CountRSession _countr_session;
      Int32 _user_id;
      String _user_name;
      CardData _card_data;
      ParamsTicketOperation _operation_params;
      CountRTransaction _countr_trans;
      ArrayList _voucher_list;
      Boolean _witholding_needed;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _final_balance;
      String _error_msg;
      Decimal _amount_ticket;
      Currency _total_taxes;
      Int64 _operation_id;

      Response = new TransactionResponse();
      Response.ResponseCode = (Int32)TicketStatus.TICKET_UNKNOWN;
      Status = HttpStatusCode.OK;
      _operation_id = this.OperationId;

      try
      {
        _countr = new CountR();

        var watch = System.Diagnostics.Stopwatch.StartNew();

        // CountR
        if (!CountR.DB_GetCountRByCode(this.KioskId, out _countr, SqlTrx))
        {
          Response.ResponseCode = (Int32)TicketStatus.TICKET_UNKNOWN;
          Log.Error("CountRBusinessLogic EndPayment: Redeem Kiosk not found");
          return false;
        }

        // CashierSessionInfo
        _cashier_session_info = new CashierSessionInfo();
        _cashier_session_info = CountRBusinessLogic.GetorOpenCountRSessions(_countr, SqlTrx, out _countr_session);
        if (_cashier_session_info == null)
        {
          Log.Error("CountRBusinessLogic EndPayment: CashierSessionInfo not found");
          Status = HttpStatusCode.InternalServerError;

          return false;
        }

        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("CountRBusinessLogic EndPayment.GetSystemUser: Error getting the user (SYS_Redemption) for the Redeem Kiosk");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }
        _cashier_session_info.AuthorizedByUserId = _user_id;
        _cashier_session_info.AuthorizedByUserName = _user_name;

        if (PaidTicket.TicketID == 0)
        {
          Response.ResponseCode = (Int32)TicketStatus.TICKET_UNKNOWN;
          return false;
        }

        switch (PaidTicket.Status)
        {
          case TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION:
          case TITO_TICKET_STATUS.PENDING_PAYMENT:
            break;

          default:
            return false; // Status Incorrect
        }


        // card Data
        _card_data = new CardData();
        if (!CardData.DB_VirtualCardGetAllData(_cashier_session_info.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          Status = HttpStatusCode.InternalServerError;
          return false;
        }

        // ParamsTicketOperation
        _operation_params = new ParamsTicketOperation();
        _card_data.MaxDevolution = Utils.MaxDevolutionAmount(PaidTicket.Amount);
        _operation_params.in_account = _card_data;
        _operation_params.session_info = _cashier_session_info;
        _operation_params.cashier_terminal_id = _cashier_session_info.TerminalId;
        _operation_params.tito_terminal_types = TITO_TERMINAL_TYPE.COUNTR;
        _operation_params.in_cash_amount = PaidTicket.Amount;
        _operation_params.in_cash_amt_0 = PaidTicket.Amount;
        _operation_params.cash_redeem_total_paid = PaidTicket.Amount;
        _operation_params.max_devolution = Utils.MaxDevolutionAmount(PaidTicket.Amount);
        _operation_params.validation_numbers = new List<VoucherValidationNumber>();
        _operation_params.out_operation_id = _operation_id;

        // Redeem ticket
        if (!CountRBusinessLogic.RedeemTicket(PaidTicket, OperationCode.CASH_OUT, ref _operation_params, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment.RedeemTicketList. Error updating ticket data on redeemption of ticket list.");

          Status = HttpStatusCode.InternalServerError;


          return false;
        }

        VoucherValidationNumber _voucher_ticket_data;

        if (PaidTicket != null)
        {
          _voucher_ticket_data = new VoucherValidationNumber();
          _voucher_ticket_data.ValidationNumber = PaidTicket.ValidationNumber;
          _voucher_ticket_data.Amount = PaidTicket.Amount;
          _voucher_ticket_data.ValidationType = PaidTicket.ValidationType;
          _voucher_ticket_data.TicketType = PaidTicket.TicketType;
          _voucher_ticket_data.CreatedTerminalName = PaidTicket.CreatedTerminalName;
          _voucher_ticket_data.CreatedPlaySessionId = PaidTicket.CreatedPlaySessionId;
          _voucher_ticket_data.CreatedAccountId = PaidTicket.CreatedAccountID;
          _voucher_ticket_data.TicketId = PaidTicket.TicketID;
          _voucher_ticket_data.CreatedTerminalType = PaidTicket.CreatedTerminalType;
          _operation_params.validation_numbers.Add(_voucher_ticket_data);
        }

        _operation_params.is_apply_tax = !Tax.ApplyTaxCollectionTicketPayment();


        if (!AccountsRedeem.DB_CardCreditRedeem(_card_data.AccountId, null, new PaymentOrder(), PaidTicket.Amount, CASH_MODE.TOTAL_REDEEM, null,
                                                CreditRedeemSourceOperationType.DEFAULT, _card_data, OperationCode.CASH_OUT, SqlTrx, _operation_params.is_apply_tax,
                                                _cashier_session_info, out _total_taxes, out _voucher_list, out _witholding_needed,
                                                out _error_msg, ref _operation_id))
        {
          Log.Error("CountRBusinessLogic EndPayment.DB_CardCreditRedeem. Error updating");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }
        this.OperationId = _operation_id;

        // Update CountR transaction
        if (!CountRTransaction.DB_GetByTicketAndStatus(PaidTicket.ValidationNumber.ToString(), CountRTransaction.TRANSACTION_STATUS.PENDING, out _countr_trans, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_GetTransactionByTicket: Error get Redeem Kiosk transaction");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }

        if (_countr_trans.Status != CountRTransaction.TRANSACTION_STATUS.PENDING)
        {
          Log.Error("CountRBusinessLogic EndPayment:  Redeem Kiosk Transaction Status Not PENDING ");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }
        if (!_countr_trans.DB_Update(SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_Insert: Error creating Redeem Kiosk transaction");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }

        if (_operation_params.is_apply_tax)
        {
          _amount_ticket = PaidTicket.Amount - _total_taxes;  //_paid_ticket.TotalRedeemAfterTaxes;
        }
        else
        {
          _amount_ticket = PaidTicket.Amount;
        }

        // Update CountR session
        if (!_countr_session.UpdateSession_Ticket(_amount_ticket, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment  DB_Insert: Error updating Redeem Kiosk session");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }

        // Update account balance
        if (!Utils.AccountBalanceIn(_card_data.AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _final_balance, SqlTrx))
        {
          Log.Error("CountRBusinessLogic EndPayment.AccountBalanceIn. Error updating");
          Status = HttpStatusCode.InternalServerError;


          return false;
        }
        if (_final_balance.TotalRedeemable > 0)
        {
          MultiPromos.AccountBalance _to_add = new MultiPromos.AccountBalance(-_final_balance.Redeemable, -_final_balance.PromoRedeemable, 0);

          if (!MultiPromos.Trx_UpdateAccountBalance(_card_data.AccountId, _to_add, out _final_balance, SqlTrx))
          {
            Log.Error("CountRBusinessLogic EndPayment.DB_CardCreditRedeem. Error updating ticket data on redeemption of ticket list.");
            Status = HttpStatusCode.InternalServerError;


            return false;
          }
        }
        Response.ResponseCode = (Int32)TicketStatus.TICKET_VALID;
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("CountRBusinessLogic EndPayment");
        Log.Exception(_ex);
        Status = HttpStatusCode.InternalServerError;
      }

      return false;
    } // EndPayment

    /// <summary>
    /// Method Payment Ticket
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    public Boolean PaymentTicket(PaymentRequest Request, out PaymentResponse Response, out HttpStatusCode Status)
    {
      Boolean _return;
      TicketStatus _ticket_status;
      Ticket _paid_ticket;

      Status = HttpStatusCode.InternalServerError;
      Response = new PaymentResponse();
      Response.ResponseCode = (Int32)BusinessLogic.TicketStatus.TICKET_UNKNOWN;

      try
      {
        this.OperationId = 0;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _return = StartRedemptionKioskPayment(Request.TicketCode, Request.Amount, out _paid_ticket, out _ticket_status, out Status, _db_trx.SqlTransaction);
          Response.ResponseCode = (Int32)_ticket_status;

          if (_return)
          {
            _db_trx.Commit();
            return true;
          }

        }
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        Log.Message(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Error: {0} ", this.KioskId));
      }

      return false;
    } //PaymentTicket

    /// <summary>
    /// End Transaction
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    public Boolean Transaction(TransactionRequest Request, out TransactionResponse Response, out HttpStatusCode Status)
    {
      Status = HttpStatusCode.InternalServerError;
      Response = new TransactionResponse();
      Response.ResponseCode = (Int32)BusinessLogic.TicketStatus.TICKET_UNKNOWN;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!DB_CheckRedemptionKioskAndSetActivity(_db_trx.SqlTransaction, out Status))
          {
            return false;
          }

          if (!checkAmounts(Request, _db_trx.SqlTransaction, ref Response, ref Status))
          {
            return false;
          }

          if (Status == HttpStatusCode.OK && Response.ResponseCode != 0)
          {
            return true;
          }
        }

        Status = HttpStatusCode.InternalServerError;

        CreateTicketEnd(Request, ref Response, ref Status);
        PaymentEnd(Request, ref Response, ref Status);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } //EndTransaction

    /// <summary>
    /// SendEvent
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    public Boolean SendEvent(EventRequest Request, out EventResponse Response, out HttpStatusCode Status)
    {
      String _alarm_description = "";
      Int32 _value;

      Response = new EventResponse();

      Response.ResponseCode = (Int32)KioskRAlarmResponse.Success;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!DB_CheckRedemptionKioskAndSetActivity(_db_trx.SqlTransaction, out Status))
        {
          return false;
        }
      }

      Status = HttpStatusCode.OK;

      // Invalid Alarm Code
      _value = (Int32)Request.WigosEventCode;
      if (!Enum.IsDefined(typeof(AlarmCode), _value))
      {
        Response.ResponseCode = (Int32)KioskRAlarmResponse.Invalid_Wigos_Event_Code;
        return false;
      }

      // Invalid Severity
      if (!Enum.IsDefined(typeof(AlarmSeverity), Request.Severity))
      {
        Response.ResponseCode = (Int32)KioskRAlarmResponse.Invalid_Severity_Code;
        return false;
      }

      // Missing EventCode for Kiosk Custom Event
      if (Request.WigosEventCode == (Int32)AlarmCode.CountR_Custom_Event)
      {
        if ((String.IsNullOrEmpty(Request.EventCode)) || (String.IsNullOrEmpty(Request.EventDescription)) || (String.IsNullOrEmpty(Request.Severity)))
        {
          Response.ResponseCode = (Int32)KioskRAlarmResponse.Missing_Custom_Alarm_Values;
          return false;
        }
      }

      // Build Alarm Description
      if (!((String.IsNullOrEmpty(Request.EventCode)) || (String.IsNullOrEmpty(Request.EventDescription))))
      {
        _alarm_description = Request.EventCode + " - " + Request.EventDescription;
      }
      else if (!String.IsNullOrEmpty(Request.EventCode))
      {
        _alarm_description = Request.EventCode;
      }
      else
      {
        _alarm_description = Request.EventDescription;
      }

      if (!SendAlarm((AlarmCode)Request.WigosEventCode, _alarm_description, AlarmSeverity.Warning))
      {
        Status = HttpStatusCode.InternalServerError;
        return false;
      }

      return true;

    } // SendEvent

    #endregion // Public Methods

    #region " Private Methods "

    /// <summary>
    /// Get Response Code
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Int32 GetResponseCode(HttpStatusCode Status, Int32 RedemptionKioskId)
    {
      Int32 _response;

      _response = (Int32)CommunicationCode.COMMUNICATION_ERROR;

      switch (Status)
      {
        case HttpStatusCode.OK:
          _response = (Int32)CommunicationCode.COMMUNICATION_SUCCESSFUL;
          break;
        case HttpStatusCode.Forbidden:
          Log.Error(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Redeem Kiosk {0} not found", RedemptionKioskId));
          break;
        case HttpStatusCode.Unauthorized:
          Log.Error(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Redeem Kiosk {0} not enabled", RedemptionKioskId));
          break;
        case HttpStatusCode.InternalServerError:
          Log.Error(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Redeem Kiosk {0} internal error", RedemptionKioskId));
          break;
        default:
          _response = -1;
          Log.Error(String.Format("KioskRedemption.BusinessLogic.GetRedemptionKioskStatus: Status {0} not defined", Status));
          break;
      }

      return _response;

    } // GetResponseCode

    /// <summary>
    /// Set response for CreateTicket petition
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RedemptionKioskId"></param>
    /// <param name="Ticket"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    private Boolean SetResponseCreateTicket(CreateTicketRequest Request, Int32 RedemptionKioskId, Ticket Ticket, ref CreateTicketResponse Response)
    {

      try
      {
        Response.ResponseCode = GetResponseCode(HttpStatusCode.OK, RedemptionKioskId);
        Response.TicketData = new CreateTicketResponseTicketData();
        Response.TicketData.TicketCode = Ticket.ValidationNumber.ToString().PadLeft(18, '0');
        Response.TicketData.RequestedAmount = Request.Amount;
        Response.TicketData.Validity = (Int32)TimeSpan.FromTicks(Ticket.ExpirationDateTime.Ticks - Ticket.CreatedDateTime.Ticks).TotalDays;

        Response.TicketData.AmountToPrint = Request.Amount;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SetResponseCreateTicket

    /// <summary>
    /// Set Ticket Taxes
    /// </summary>
    /// <param name="Ticket"></param>
    /// <param name="Taxes"></param>
    /// <param name="TotalPayable"></param>
    /// <returns></returns>
    private Boolean SetTicketTaxes(Ticket Ticket, out List<KioskRedemption.Classes.Common.Tax> Taxes, out Int64 TotalPayable)
    {
      try
      {
        Taxes = new List<KioskRedemption.Classes.Common.Tax>();
        TotalPayable = 0;

        if (Ticket.Taxes != null)
        {
          foreach (CountRTaxes _tax in Ticket.Taxes)
          {
            Taxes.Add(new KioskRedemption.Classes.Common.Tax(_tax));
          }
        }
        TotalPayable = (Int64)(Ticket.Amount * 100) - Taxes.Select(_tax => _tax.TaxValue).Sum();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Taxes = null;
      TotalPayable = 0;

      return false;
    } // SetTicketTaxes

    /// <summary>
    /// Get Ticket
    /// </summary>
    /// <param name="TicketNumber"></param>
    /// <param name="PayTicket"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetTicket(String TicketNumber, out Ticket PayTicket, SqlTransaction SqlTrx)
    {
      String _condition;
      Int64 _ticket_number;
      TITO_VALIDATION_TYPE _validation_type;

      PayTicket = new Ticket();

      try
      {
        _ticket_number = ValidationNumberManager.UnFormatValidationNumber(TicketNumber, out _validation_type);

        //Get Ticket
        _condition = " TI_VALIDATION_NUMBER = '" + _ticket_number.ToString() + "' ";
        PayTicket = CountRBusinessLogic.SelectTicketByCondition(_condition, SqlTrx);
        CountRBusinessLogic.AmountTaxes(PayTicket);

        return _ticket_number != 0;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetTicket

    /// <summary>
    /// Check Redemption Kiosk Ticket
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Amount"></param>
    /// <param name="CheckAmount"></param>
    /// <param name="CreateOperation"></param>
    /// <param name="IPAddress"></param>
    /// <param name="Response"></param>
    /// <param name="PayTicket"></param>
    /// <param name="PayCountR"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean CheckRedemptionKioskTicket(String TicketNumber, Int64 Amount, Boolean CheckAmount, Boolean CreateOperation, out TicketStatus Response, out Ticket PayTicket, out CountR PayCountR, out HttpStatusCode Status, SqlTransaction SqlTrx)
    {
      DateTime _date_limit_payment;
      CashierSessionInfo _cashier_session_info;
      CountRSession _countr_session;
      Int32 _user_id;
      String _user_name;
      CardData _card_data;
      ParamsTicketOperation _operation_params;
      CountRTransaction _countr_trans;
      Boolean _allow_pay_tickets_pending_print;
      Int64 _amount;
      Int64 _operation_id;
      Boolean _allow_to_redeem_gp;
      List<KioskRedemption.Classes.Common.Tax> _taxes;


      _operation_id = this.OperationId;
      Response = TicketStatus.TICKET_UNKNOWN;

      PayCountR = new CountR();
      PayTicket = new Ticket();
      _allow_pay_tickets_pending_print = GeneralParam.GetBoolean("TITO", "PendingPrint.AllowToRedeem", true);

      if (!DB_CheckRedemptionKioskAndSetActivity(SqlTrx, out Status))
      {
        return false;
      }
      Status = HttpStatusCode.InternalServerError;

      if (!CountR.DB_GetCountRByCode(this.KioskId, out PayCountR, SqlTrx))
      {
        Log.Error(String.Format("BusinessLogic.CheckRedemptionKioskTicket: Redeem Kiosk {0} not found", this.KioskId));
        Response = TicketStatus.TICKET_UNKNOWN;

        return false;
      }

      if (!GetTicket(TicketNumber, out PayTicket, SqlTrx))
      {
        Response = TicketStatus.TICKET_INVALID;
        Status = HttpStatusCode.OK;

        return false;
      }

      if (PayTicket.TicketID <= 0)
      {
        Response = TicketStatus.TICKET_UNKNOWN;
        Status = HttpStatusCode.OK;

        return false;
      }

      if (PayTicket.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      {
        Response = TicketStatus.TICKET_NON_REEDEMABLE;
        Status = HttpStatusCode.OK;

        return false;
      }

      // CashierSessionInfo
      _cashier_session_info = new CashierSessionInfo();
      _cashier_session_info = CountRBusinessLogic.GetorOpenCountRSessions(PayCountR, SqlTrx, out _countr_session);
      if (_cashier_session_info == null)
      {
        Log.Error("BusinessLogic.CheckRedemptionKioskTicket: CashierSessionInfo not found");
        Status = HttpStatusCode.InternalServerError;
        Response = TicketStatus.TICKET_UNKNOWN;

        return false;
      }

      switch (PayTicket.Status)
      {
        case TITO_TICKET_STATUS.EXPIRED:
          CountRBusinessLogic.CheckGracePeriod(PayTicket.ExpirationDateTime, out _date_limit_payment);
          if (_date_limit_payment < WGDB.Now)
          {
            Response = TicketStatus.TICKET_EXPIRED;
            Status = HttpStatusCode.OK;

            return false;
          } // TICKET EXPIRED

          Response = TicketStatus.TICKET_VALID;
          PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION;
          break;

        case TITO_TICKET_STATUS.PENDING_CANCEL:
          _allow_to_redeem_gp = GeneralParam.GetBoolean("TITO", "PendingCancel.AllowToRedeem", false);

          if (_allow_to_redeem_gp)
          {
            PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
            Response = TicketStatus.TICKET_VALID;
          }
          else
          {
            Status = HttpStatusCode.OK;
            Response = TicketStatus.TICKET_INVALID;
          }

          break;

        case TITO_TICKET_STATUS.VALID:
          PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
          Response = TicketStatus.TICKET_VALID;
          break;

        case TITO_TICKET_STATUS.PENDING_PRINT:
          if (_allow_pay_tickets_pending_print)
          {
            PayTicket.Status = TITO_TICKET_STATUS.PENDING_PAYMENT;
            Response = TicketStatus.TICKET_VALID;
            Log.Message("BusinessLogic.CheckRedemptionKioskTicket: Paid Ticket " + PayTicket.ValidationNumber + " status is PENDING PRINTING");
          }
          else
          {
            Log.Message("CountRBusnessLogic CheckTicket: Not Paid Ticket " + PayTicket.ValidationNumber + " status is PENDING PRINTING");
            Status = HttpStatusCode.OK;
            Response = TicketStatus.TICKET_INVALID;
          }
          break;
        case TITO_TICKET_STATUS.PENDING_PAYMENT:
        case TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION:
          if (PayTicket.LastActionTerminalID == PayCountR.Id)
          {
            Response = TicketStatus.TICKET_VALID;
          }
          else
          {
            Status = HttpStatusCode.OK;
            Response = TicketStatus.TICKET_BE_PAID_ANOTHER_KIOSK;

            return false;
          }
          break;

        case TITO_TICKET_STATUS.DISCARDED:
          Status = HttpStatusCode.OK;
          Response = TicketStatus.TICKET_INVALID;

          return false;
        default:
          Status = HttpStatusCode.OK;
          Response = TicketStatus.TICKET_ALREADY_PAID;

          return false; // Incorrect status
      }

      _amount = Decimal.ToInt64(PayTicket.Amount * 100.00m); //amount in cents
      
      // Check amount and Check ticket amount
      if (CheckAmount && Amount != _amount)
      {
        if (!SetTicketTaxes(PayTicket, out _taxes, out _amount))
        {
          return false;
        }

        if (CheckAmount && Amount != _amount)
        {
          Response = TicketStatus.TICKET_MISMATCH;
          Status = HttpStatusCode.OK;

          return false;
        }
      }

      // Check Max/Min Payment amount
      if (((PayTicket.Amount * 100) < PayCountR.MinPayment) || ((PayCountR.MaxPayment > 0) && ((PayTicket.Amount * 100) > PayCountR.MaxPayment)))
      {
        Response = TicketStatus.TICKET_OUT_OF_LIMIT;
        Status = HttpStatusCode.OK;

        return false;
      }

      // Create operation
      if (CreateOperation)
      {
        if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_REDEMPTION, SqlTrx, out _user_id, out _user_name))
        {
          Log.Message("BusinessLogic.CheckRedemptionKioskTicket: Error getting the user (SYS_COUNTR) for the CountR");
          Status = HttpStatusCode.InternalServerError;
          Response = TicketStatus.TICKET_UNKNOWN;

          return false;
        }

        _cashier_session_info.AuthorizedByUserId = _user_id;
        _cashier_session_info.AuthorizedByUserName = _user_name;

        // card Data
        _card_data = new CardData();

        if (!CardData.DB_VirtualCardGetAllData(_cashier_session_info.TerminalName, AccountType.ACCOUNT_VIRTUAL_CASHIER, _card_data))
        {
          Log.Message("BusinessLogic.CheckRedemptionKioskTicket: Error");
          Status = HttpStatusCode.InternalServerError;
          Response = TicketStatus.TICKET_UNKNOWN;

          return false;
        }

        _operation_params = new ParamsTicketOperation();
        _card_data.MaxDevolution = Utils.MaxDevolutionAmount(PayTicket.Amount);
        _operation_params.in_account = _card_data;
        _operation_params.session_info = _cashier_session_info;
        _operation_params.cashier_terminal_id = _cashier_session_info.TerminalId;
        _operation_params.tito_terminal_types = TITO_TERMINAL_TYPE.COUNTR;
        _operation_params.in_cash_amount = PayTicket.Amount;
        _operation_params.in_cash_amt_0 = PayTicket.Amount;
        _operation_params.cash_redeem_total_paid = PayTicket.Amount;
        _operation_params.max_devolution = Utils.MaxDevolutionAmount(PayTicket.Amount);
        _operation_params.validation_numbers = new List<VoucherValidationNumber>();
        _operation_params.out_operation_id = _operation_id;

        // Create Operation
        if (!Operations.DB_InsertOperation(OperationCode.CASH_OUT,
                                           _operation_params.in_account.AccountId,
                                           _cashier_session_info.CashierSessionId,
                                           0,
                                           _operation_params.out_promotion_id,
                                           0,
                                           0,
                                           0,                      // Operation Data
                                           _operation_params.AccountOperationReasonId,
                                           _operation_params.AccountOperationComment,
                                           out _operation_id, SqlTrx))
        {
          Log.Error("BusinessLogic.CheckRedemptionKioskTicket.DB_InsertOperation: Error creating operation.");

          Status = HttpStatusCode.InternalServerError;
          Response = TicketStatus.TICKET_UNKNOWN;

          return false;
        }

        // Create CountR transaction
        _countr_trans = new CountRTransaction();
        _countr_trans.PaidTicketId = PayTicket.TicketID;
        _countr_trans.OperationId = _operation_id;
        _countr_trans.PaidAmount = PayTicket.Amount;
        _countr_trans.CashPaidAmount = PayTicket.Amount; // Pending validate correct amount
        _countr_trans.CountrId = PayCountR.Id;
        _countr_trans.CashierSessionId = _cashier_session_info.CashierSessionId;

        if (!_countr_trans.DB_Insert(SqlTrx))
        {
          Log.Error("BusinessLogic.CheckRedemptionKioskTicket: Error creating CountR transaction");

          Status = HttpStatusCode.InternalServerError;
          Response = TicketStatus.TICKET_UNKNOWN;

          return false;
        }
      }

      this.OperationId = _operation_id;
      Status = HttpStatusCode.OK;

      return true;
    } // CheckRedemptionKioskTicket

    /// <summary>
    /// Start Redemption Kiosk Payment
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="TicketNumber"></param>
    /// <param name="Amount"></param>
    /// <param name="IPAddress"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean StartRedemptionKioskPayment(String TicketNumber, Int64 Amount, out Ticket PaidTicket, out TicketStatus Response, out HttpStatusCode Status, SqlTransaction SqlTrx)
    {
      CountR _countr;

      Status = HttpStatusCode.InternalServerError;
      Response = TicketStatus.TICKET_UNKNOWN;


      if (CheckRedemptionKioskTicket(
        TicketNumber, Amount, true, true, out Response, out  PaidTicket, out  _countr, out  Status, SqlTrx))
      {
        Status = HttpStatusCode.InternalServerError;
        PaidTicket.LastActionTerminalID = (Int32)_countr.Id;
        PaidTicket.LastActionTerminalName = _countr.Name;
        PaidTicket.LastActionTerminalType = (int)TITO_TERMINAL_TYPE.COUNTR;
        PaidTicket.LastActionDateTime = WGDB.Now;

        if (!CountRBusinessLogic.UpdateTicketStatus(PaidTicket, SqlTrx))
        {
          Response = TicketStatus.TICKET_UNKNOWN;
          Status = HttpStatusCode.OK;

          return false;
        }

        Response = TicketStatus.TICKET_VALID;

        Status = HttpStatusCode.OK;

        return true;
      }

      SendAlarm(AlarmCode.CountR_Payment_Error, GetTicketStatusAlarmDescription(Response), AlarmSeverity.Warning);

      return false;
    } // StartRedemptionKioskPayment

    /// <summary>
    /// DB_Check RedemptionKiosk And Set Activity
    /// </summary>

    /// <param name="SqlTrx"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Boolean DB_CheckRedemptionKioskAndSetActivity(SqlTransaction SqlTrx, out HttpStatusCode Status)
    {
      Status = HttpStatusCode.InternalServerError;

      try
      {
        //First checks if CountR is valid and is enable.
        if (!DB_CheckRedemptionKiosk(this.KioskId, this.IP, this.Provider, SqlTrx, out Status))
        {
          return false;
        }


        // Status not ok return true.
        if (Status != HttpStatusCode.OK)
        {
          return false;
        }

        //Second: Update CountR last Activity.
        if (!CountR.DB_UpdateCountRActivity(this.KioskId, SqlTrx))
        {
          Status = HttpStatusCode.InternalServerError;
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("CountR.DB_CheckCountRAndSetActivity -> Code: {0}", this.KioskId));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckCountRAndSetActivity

    /// <summary>
    /// DB_Check Redemption Kiosk
    /// Checks for Kiosk and sets Kiosk name
    /// </summary>
    /// <param name="CountRCode"></param>
    /// <param name="CountRIPAddress"></param>
    /// <param name="Provider"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Boolean DB_CheckRedemptionKiosk(Int32 CountRCode, String CountRIPAddress, String Provider, SqlTransaction SqlTrx, out HttpStatusCode Status)
    {
      StringBuilder _sb;

      Status = HttpStatusCode.InternalServerError;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CR_ENABLED                        ");
        _sb.AppendLine("        , CR_NAME                           ");
        _sb.AppendLine("        , CR_PROVIDER                       ");
        _sb.AppendLine("   FROM   COUNTR                            ");
        _sb.AppendLine("  WHERE   CR_CODE       = @pCountrCode      ");
        _sb.AppendLine("    AND   CR_IP_ADDRESS = @pCountRIPAddress ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCountrCode", SqlDbType.Int).Value = CountRCode;
          _cmd.Parameters.Add("@pCountRIPAddress", SqlDbType.NVarChar, 20).Value = CountRIPAddress;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              Status = ((Boolean)_sql_reader["CR_ENABLED"] ? HttpStatusCode.OK : HttpStatusCode.Unauthorized);
              this.KioskName = _sql_reader["CR_NAME"].ToString();

              if (Provider != (_sql_reader["CR_PROVIDER"] == DBNull.Value ? "" : (String)_sql_reader["CR_PROVIDER"]))
              {
                Status = HttpStatusCode.Forbidden;
              }
            }
            else
            {
              Status = HttpStatusCode.Forbidden;
            }

          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.DB_CheckRedemptionKiosk-> Code: {0}", CountRCode));
        Log.Exception(_ex);
      }

      return false;
    } // DB_CheckRedemptionKiosk

    /// <summary>
    /// Transaction for create ticket
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Boolean CreateTicketEnd(TransactionRequest Request, ref TransactionResponse Response, ref HttpStatusCode Status)
    {
      TicketOut _ticket;
      TITO_TICKET_STATUS _status;
      Boolean _printed;
      Int32 _kiosk_id;
      String _description;

      try
      {
        _ticket = Request.TicketOutput;

        if (_ticket == null || _ticket.TicketCode == null)
        {
          Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_SUCCESSFUL;
          Status = HttpStatusCode.OK;

          return true;
        }

        _printed = _ticket.Printed == 1;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GetTicketStatus(_ticket.TicketCode, TransactionType.CreateTiquet, out _status, out _kiosk_id, _db_trx.SqlTransaction))
          {
            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
            Status = HttpStatusCode.InternalServerError;

            return false;
          }

          if (_kiosk_id != this.KioskId)
          {
            _description = Resource.String("REDEMPTIONKIOSK_WRONG_KIOS_ID", _ticket.TicketCode, _kiosk_id);
            SendAlarm(AlarmCode.CountR_Incorrect_Created_Ticket, _description, AlarmSeverity.Error);

            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
            Status = HttpStatusCode.OK;

            return false;
          }

          if (_status != TITO_TICKET_STATUS.VALID)
          {
            _description = Resource.String("REDEMPTIONKIOSK_WRONG_STATUS_PAYMENT", _ticket.TicketCode, TitoStatusToString(_status));
            SendAlarm(AlarmCode.CountR_Incorrect_Ticket_Status, _description, AlarmSeverity.Error);

            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
            Status = HttpStatusCode.OK;

            return false;
          }

          if (!_printed)
          {
            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
            Status = HttpStatusCode.OK;

            UpdateKioskTicketStatus(_ticket.TicketCode, TITO_TICKET_STATUS.DISCARDED, _db_trx.SqlTransaction);
            _db_trx.Commit();
          }
          else
          {
            Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_SUCCESSFUL;
            Status = HttpStatusCode.OK;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.CreateTicketEnd-> Code: {0}", this.KioskId));
        Log.Exception(_ex);
      }

      return false;

    } // CreateTicketEnd

    /// <summary>
    /// Transaction for payment
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Boolean PaymentEnd(TransactionRequest Request, ref TransactionResponse Response, ref HttpStatusCode Status)
    {
      TITO_TICKET_STATUS _status;
      Boolean _paid;
      Int32 _kiosk_id;
      String _description;
      Ticket _ticket_paid;

      try
      {
        if (Request.TicketsInput == null)
        {
          return true;
        }

        foreach (TicketIn _ticket in Request.TicketsInput)
        {
          if (_ticket == null || _ticket.TicketCode == null)
          {
            continue;
          }
          _paid = _ticket.Paid == 1;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!GetTicketStatus(_ticket.TicketCode, TransactionType.Payment, out _status, out _kiosk_id, _db_trx.SqlTransaction))
            {
              Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
              Status = HttpStatusCode.OK;
              continue;
            }

            if (_kiosk_id != this.KioskId)
            {
              if (_paid)
              {
                SendAlarm(AlarmCode.CountR_Incorrect_Paid_Ticket, "", AlarmSeverity.Error);
              }
              else
              {
                SendAlarm(AlarmCode.CountR_Incorrect_Unpaid_Ticket, "", AlarmSeverity.Error);
              }
              Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
              Status = HttpStatusCode.OK;
              continue;
            }

            switch (_status)
            {
              case TITO_TICKET_STATUS.PENDING_PAYMENT:
                _status = _paid ? TITO_TICKET_STATUS.REDEEMED : TITO_TICKET_STATUS.VALID;
                break;

              case TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION:
                _status = _paid ? TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION : TITO_TICKET_STATUS.EXPIRED;
                break;

              default:
                _description = Resource.String("REDEMPTIONKIOSK_WRONG_STATUS_CREATE_TICKET", _ticket.TicketCode, TitoStatusToString(_status));
                SendAlarm(AlarmCode.CountR_Incorrect_Ticket_Status, _description, AlarmSeverity.Error);
                Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_ERROR;
                Status = HttpStatusCode.OK;
                continue;
            }

            GetTicket(_ticket.TicketCode, out _ticket_paid, _db_trx.SqlTransaction);
            EndRedemptionKioskPayment(_ticket_paid, out Response, out Status, _db_trx.SqlTransaction);

            _db_trx.Commit();
          }
        }
        if (Response.ResponseCode != (Int32)CommunicationCode.COMMUNICATION_ERROR)
        {
          Response.ResponseCode = (Int32)CommunicationCode.COMMUNICATION_SUCCESSFUL;
          Status = HttpStatusCode.OK;
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.PaymentEnd-> Code: {0}", this.KioskId));
        Log.Exception(_ex);
      }

      return false;

    } // PaymentEnd

    /// <summary>
    /// Get ticket _status and kioskid
    /// </summary>
    /// <param name="Ticket"></param>
    /// <param name="Type"></param>
    /// <param name="Status"></param>
    /// <param name="KioskId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetTicketStatus(String Ticket, TransactionType Type, out TITO_TICKET_STATUS Status, out Int32 KioskId, SqlTransaction SqlTrx)
    {
      String _str_sql;
      Status = TITO_TICKET_STATUS.DISCARDED;
      KioskId = -1;

      try
      {
        _str_sql = Type == TransactionType.Payment ? GetSqlPaymentEnd() : GetSqlCreateTiquetEnd();

        using (SqlCommand _cmd = new SqlCommand(_str_sql, SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTicketNumber", SqlDbType.BigInt).Value = Int64.Parse(Ticket);

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              Status = (TITO_TICKET_STATUS)_sql_reader["TI_STATUS"];
              KioskId = _sql_reader["CR_CODE"] == DBNull.Value ? -1 : (Int32)_sql_reader["CR_CODE"];
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.GetTicketStatus -> Code: {0}", this.KioskId));
        Log.Exception(_ex);
      }

      return false;
    } // GetTicketStatus

    /// <summary>
    /// GetSqlCreateTiquet
    /// </summary>
    /// <returns></returns>
    private String GetSqlCreateTiquetEnd()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   TI_STATUS, CR_CODE                          ");
      _sb.AppendLine("       FROM   COUNTR C                                    ");
      _sb.AppendLine(" INNER JOIN   CASHIER_TERMINALS T                         ");
      _sb.AppendLine("         ON   T.CT_COUNTR_ID = C.CR_COUNTR_ID             ");
      _sb.AppendLine(" RIGHT JOIN   TICKETS TI                                  ");
      _sb.AppendLine("         ON   TI.TI_CREATED_TERMINAL_ID = T.CT_CASHIER_ID ");
      _sb.AppendLine("      WHERE   TI_VALIDATION_NUMBER = @pTicketNumber       ");

      return _sb.ToString();
    } // GetSqlCreateTiquetEnd

    /// <summary>
    /// Get Sql Payment End
    /// </summary>
    /// <returns></returns>
    private String GetSqlPaymentEnd()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   TI_STATUS, CR_CODE                              ");
      _sb.AppendLine("       FROM   COUNTR C                                        ");
      _sb.AppendLine(" RIGHT JOIN   TICKETS TI                                      ");
      _sb.AppendLine("         ON   TI.TI_LAST_ACTION_TERMINAL_ID = C.CR_COUNTR_ID  ");
      _sb.AppendLine("      WHERE   TI_VALIDATION_NUMBER = @pTicketNumber           ");

      return _sb.ToString();
    } // GetSqlPaymentEnd

    /// <summary>
    /// Update Kiosk Ticket Status
    /// </summary>
    /// <param name="Ticket"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean UpdateKioskTicketStatus(String Ticket, TITO_TICKET_STATUS Status, SqlTransaction SqlTrx)
    {
      Ticket _ticket;
      try
      {

        if (!GetTicket(Ticket, out _ticket, SqlTrx))
        {
          return false;
        }

        _ticket.LastActionDateTime = _ticket.LastActionDateTime == DateTime.MinValue ? WGDB.Now : _ticket.LastActionDateTime;
        _ticket.Status = Status;

        if (!CountRBusinessLogic.UpdateTicketStatus(_ticket, SqlTrx))
        {
          return false;
        }

      }
      catch (Exception _ex)
      {

        Log.Error(string.Format("BusinessLogic.UpdateKioskTicketStatus -> Code: {0}", this.KioskId));
        Log.Exception(_ex);
      }

      return false;
    } // UpdateKioskTicketStatus

    /// <summary>
    /// Send alarm
    /// </summary>
    /// <param name="EventCode"></param>
    /// <param name="Description"></param>
    /// <param name="Severity"></param>
    /// <returns></returns>
    private Boolean SendAlarm(AlarmCode EventCode, String Description, AlarmSeverity Severity)
    {
      // Send Alarm

      String _alarm_source;

      _alarm_source = this.KioskId.ToString() + "@" + this.KioskName;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Alarm.Register(AlarmSourceCode.Cashier,
                               0,
                               _alarm_source,
                               (UInt32)EventCode,
                               Description,
                               Severity,
                               WGDB.Now,
                               _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();

          return true;
        }

      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
        Log.Message(String.Format("KioskRedemption.BusinessLogic.SendAlarm: Error: {0} ", this.KioskId));
      }

      return false;
    } // SendAlarm

    /// <summary>
    /// Get alarm description for the Ticket status error
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    private String GetTicketStatusAlarmDescription(TicketStatus Status)
    {
      switch (Status)
      {
        case TicketStatus.TICKET_UNKNOWN:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_UNKNOWN");

        case TicketStatus.TICKET_ALREADY_PAID:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_ALREADY_PAID");

        case TicketStatus.TICKET_EXPIRED:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_EXPIRED");

        case TicketStatus.TICKET_INVALID:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_INVALID");

        case TicketStatus.TICKET_NON_REEDEMABLE:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_NON_REEDEMABLE");

        case TicketStatus.TICKET_OUT_OF_LIMIT:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_OUT_OF_LIMIT");

        case TicketStatus.TICKET_MISMATCH:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_MISMATCH");

        case TicketStatus.TICKET_BE_PAID_ANOTHER_KIOSK:
          return Resource.String("COUNTR_TICKET_STATUS_ALARM_TICKET_BE_PAID_ANOTHER_KIOSK");

        case TicketStatus.TICKET_VALID:
        default:
          return String.Empty;
      }
    } // GetTicketStatusAlarmDescription

    /// <summary>
    /// Get sum amounts tickets are paid
    /// </summary>
    /// <param name="TicketsID"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private Boolean DB_GetAmountTicketsPaid(List<String> TicketsID, SqlTransaction SqlTrx, out Int32 Amount)
    {
      StringBuilder _sb;

      Amount = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ISNULL(SUM(TI_AMOUNT), 0) AS AMOUNT ");
        _sb.AppendLine("   FROM   TICKETS                             ");
        _sb.AppendLine("  WHERE   TI_VALIDATION_NUMBER IN ({0})       ");

        using (SqlCommand _cmd = new SqlCommand(String.Format(_sb.ToString(), String.Join(",", TicketsID.ToArray())), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              Amount = (Int32)((Decimal)_sql_reader["AMOUNT"] * 100);
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.DB_GetAmountTicketsPaid"));
        Log.Exception(_ex);
      }

      return false;
    } // DB_GetAmountTicketsPaid

    /// <summary>
    /// Check request amounts
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private Boolean checkAmounts(TransactionRequest Request, SqlTransaction SqlTrx, ref TransactionResponse Response, ref HttpStatusCode Status)
    {

      Int32 _tickets_input_amount;
      Int32 _cash_output_amount;
      List<String> _id_tickets_paid;

      try
      {

        Status = HttpStatusCode.OK;
        Response.ResponseCode = 0;
        _tickets_input_amount = 0;
        _cash_output_amount = 0;

        //Tickets In
        _id_tickets_paid = Request.TicketsInput.Where(_ticket => _ticket.Paid == 1).Select(_ticket => _ticket.TicketCode).ToList();

        if (_id_tickets_paid.Count > 0)
        {
          if (!DB_GetAmountTicketsPaid(_id_tickets_paid, SqlTrx, out _tickets_input_amount))
          {
            Status = HttpStatusCode.InternalServerError;

            return false;
          }
        }

        if (_tickets_input_amount != Request.TicketsInputValue)
        {
          Response.ResponseCode = 5;
        }

        //Tickets Out
        if (Response.ResponseCode == 0 && (Request.TicketOutput != null && Request.TicketOutput.TicketCode != null && Request.TicketOutputValue != Request.TicketOutput.Value))
        {
          Response.ResponseCode = 6;
        }

        //Validation sums
        if (Response.ResponseCode == 0)
        {
          _cash_output_amount = Request.CashOutput.Where(_cash => _cash.Quantity > 0).Select(_cash => _cash.Denomination * _cash.Quantity).Sum();

          if ((Request.AmountToPayValue + Request.CashInputValue) != (Request.TicketOutputValue + _cash_output_amount))
          {
            Response.ResponseCode = 7;
          }
        }

        //TODO HULK - Change event
        if (Response.ResponseCode != 0 && !SendAlarm(AlarmCode.CountR_Transaction_Error, GetTransactionStatusAlarmDescription((TransactionStatus)Response.ResponseCode), AlarmSeverity.Warning))
        {
          Status = HttpStatusCode.InternalServerError;

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.checkAmounts"));
        Log.Exception(_ex);
      }

      Response = null;

      return false;

    } // checkAmounts

    /// <summary>
    /// Get alarm description for the transaction status error
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    private String GetTransactionStatusAlarmDescription(TransactionStatus Status)
    {

      switch (Status)
      {
        case TransactionStatus.TRANSACTION_TICKET_PAID_NO_VALID:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_PAID_NOT_PRIOR_PAYMENT_CALL");
        case TransactionStatus.TRANSACTION_TICKET_NO_PAID_NO_VALID:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_UNPAID_NOT_PRIOR_PAYMENT_CALL");
        case TransactionStatus.TRANSACTION_KIOSD_ID_MISMATCH:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_NOT_SAME_KIOSK");
        case TransactionStatus.TRANSACTION_TICKET_STATUS_INCORRECT:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_INCORRECT_STATUS");
        case TransactionStatus.TRANSACTION_AMOUNT_MISMATCH:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_SUM_AMOUNT_MISMATCH");
        case TransactionStatus.TRANSACTION_AMOUNT_NOT_MATCH_PAID_AMOUNT:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_AMOUNT_MISMATCH");
        case TransactionStatus.TRANSACTION_ISSUED_TICKET_NO_VALID:

          return Resource.String("COUNTR_TRANSACTION_ALARM_TICKET_NOT_MATCH_CREATE");
        case TransactionStatus.TRANSACTION_OK:
        default:

          return String.Empty;
      }

    } // GetTicketStatusAlarmDescription

    #endregion " Private Methods "

    #region " Static methods "

    /// <summary>
    /// Save a log with request and response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Tick"></param>
    public static void ManageLog(String Request, String Response, Int32 KioskRedemptionId, Int32 Tick)
    {
      CashIOServer _server;

      _server = new CashIOServer()
      {
        CountRCode = KioskRedemptionId
      };

      _server.ManageLog(Request, Response, Tick);
    } // ManageLog

    /// <summary>
    /// Status to string
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    public static String TitoStatusToString(TITO_TICKET_STATUS Status)
    {
      String _str;

      _str = Resource.String(String.Format("TITO_TICKET_STATUS_{0}", (Int32)Status).ToString());

      if (_str.Contains("String"))
      {
        _str = ((Int32)Status).ToString();
        Log.Warning(String.Format("TitoStatusToString - Ticket status {0} not defined", _str));
      }

      return _str;
    } // TitoStatusToString

    #endregion " Static methods "

  } // BusinessLogic
}
