﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cash.cs
// 
//   DESCRIPTION: Cash class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 24-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-AUG-2017 ETP    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskRedemption.Classes.Common
{
  /// <summary>
  /// Cash Request Content
  /// </summary>
  public class Cash
  {
    public Int32 Denomination { get; set; }
    public String Type { get; set; }
    public Int32 Quantity { get; set; }
  } // Cash
}
