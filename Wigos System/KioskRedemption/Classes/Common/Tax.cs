﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Tax.cs
// 
//   DESCRIPTION: Tax class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 24-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common.CountR;

namespace KioskRedemption.Classes.Common
{
  public class Tax
  {
    public Int32 TaxPercent { get; set; }
    public Int32 TaxValue { get; set; }
    public String TaxText { get; set; }

    public Tax(CountRTaxes Tax)
    {
      TaxPercent = (Int32)((decimal)(Tax.Percentage) * 10000);
      TaxValue = (Int32)(Tax.Amount * 100);
      TaxText = Tax.TaxName;
    }
  }
}
