﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: KioskStatusRequest.cs
// 
//   DESCRIPTION: KioskStatusRequest class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 ETP    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using KioskRedemption.Classes.Common;
 

namespace KioskRedemption.Classes.Request
{
  /// <summary>
  /// Kiosk Status Request
  /// </summary>
  public class KioskStatusRequest
  {
    public StatusContent Status { get; set; }

    public MeterContent Meters { get; set; }

  } // KioskStatusRequest

  /// <summary>
  /// Status Content
  /// </summary>
  public class StatusContent
  {
    public List<Cash> CashContent { get; set; }
    public String BillAcceptorStatus { get; set; }
    public String CoinAcceptorStatus { get; set; }
    public String TitoPrinterStatus { get; set; }
    public String VoucherPrinter { get; set; }
    public String BarcodeReader { get; set; }
    public String PlayerCardReader { get; set; }
    public String BankCardReader { get; set; }
  } // StatusContent

  /// <summary>
  /// Meter Content
  /// </summary>
   public class MeterContent
   {
     public Meter Input { get; set; }
     public Meter Output { get; set; }
   }

  /// <summary>
  /// Meter
  /// </summary>
   public class Meter
   {
     public Credit TotalBills { get; set; }
     public Credit TotalCoins { get; set; }
     public CreditContent Tickets { get; set; }
     public CreditContent PlayerCard { get; set; }
     public CreditContent BankCard { get; set; }
     public List<Cash> CashDetail { get; set; }

   } //

  /// <summary>
   /// Credit Content
  /// </summary>
  public class CreditContent
  {
    public Credit RE { get; set; }
    public Credit PNR { get; set; }
    public Credit PRE { get; set; }
  } // CreditContent

  /// <summary>
  ///  Credit
  /// </summary>
  public class Credit
  {
    public Int32 Quantity { get; set; }
    public Int64 Value { get; set; }
  } // Credit

}
