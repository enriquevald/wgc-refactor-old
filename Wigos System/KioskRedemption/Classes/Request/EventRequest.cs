﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskRedemption.Classes.Request
{
  public class EventRequest
  {
    public String SoftwareVersion { get; set; }
    public Int32 WigosEventCode { get; set; }
    public String EventCode { get; set; }
    public String EventDescription { get; set; }
    public String Severity { get; set; }
  }
}
