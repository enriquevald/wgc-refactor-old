﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TransationRequest.cs
// 
//   DESCRIPTION: TransationRequest class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 24-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-AUG-2017 ETP    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using KioskRedemption.Classes.Common;

namespace KioskRedemption.Classes.Request
{
  /// <summary>
  /// Transation Request
  /// </summary>
  public class TransactionRequest
  {
    public String TransactionId { get; set; }
    public Int32 TicketsInputValue { get; set; }
    public Int32 CashInputValue { get; set; }
    public Int32 AmountToPayValue { get; set; }
    public Int32 CashOutputValue { get; set; }
    public Int32 TicketOutputValue { get; set; }
    public Int32 PlayerCardWithdrawalValue { get; set; }
    public Int32 PlayerCardLoadValue { get; set; }
    public Int32 BankCardWithdrawalValue { get; set; }
    public Int32 BankCardRefundValue { get; set; }
    public List<TicketIn> TicketsInput { get; set; }
    public List<Cash> CashInput { get; set; }
    public List<Cash> CashOutput { get; set; }
    public TicketOut TicketOutput { get; set; }
    public PlayerCard PlayerCardDrawal { get; set; }
    public PlayerCard PlayerCardLoad { get; set; }
    public PlayerCard BankCardPayment { get; set; }
    public PlayerCard BankCardRefund { get; set; }
  } // TransactionRequest

  /// <summary>
  /// Ticket In
  /// </summary>
  public class TicketIn
  {
    public String TicketCode { get; set; }
    public Int32 Paid { get; set; }
  } // TicketIn

  /// <summary>
  /// Ticket Out
  /// </summary>
  public class TicketOut
  {
    public Int32 Value { get; set; }
    public String TicketCode { get; set; }
    public Int32 Printed { get; set; }
  } // TicketOut

  /// <summary>
  /// Player Card
  /// </summary>
  public class PlayerCard
  {
    public String CardNumber { get; set; }
  } // PlayerCard
 
}
