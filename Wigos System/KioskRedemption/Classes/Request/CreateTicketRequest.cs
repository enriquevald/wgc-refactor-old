﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreateTicketRequest.cs
// 
//   DESCRIPTION: CreateTicketRequest class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using System;

namespace KioskRedemption.Classes.Request
{
  public class CreateTicketRequest
  {
    public String CreditType { get; set; }
    public Int32 Amount { get; set; }
  }
}
