﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskRedemption.Classes.Request
{
  public class LogRequest
  {
    public String Url { get; set; }
    public Dictionary<String, String> Headers { get; set; }
    public String Method { get; set; }
    public Object Body { get; set; }
  }
}
