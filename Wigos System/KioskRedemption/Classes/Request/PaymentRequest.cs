﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PaymentRequest.cs
// 
//   DESCRIPTION: PaymentRequest class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 22-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskRedemption.Classes.Request
{
  public class PaymentRequest
  {
    public String TicketCode { get; set; }
    public Int32 Amount { get; set; }

  }
  
}
