﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TicketDataResponse.cs
// 
//   DESCRIPTION: TicketDataResponse class
// 
//        AUTHOR: Mark Stansfield
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 MS     First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common.CountR;
using KioskRedemption.Business;
using KioskRedemption.Classes.Common;

namespace KioskRedemption.Classes.Response
{

  public class TicketDataReponse
  {
    public BusinessLogic.TicketStatus ResponseCode { get; set; }
    public TicketData ResponseTicketData { get; set; }
  }

  public class TicketData
  {
    private List<Tax> m_taxes;
    
    public String TicketCode { get; set; }
    public Int32 Amount { get; set; }
    public List<Tax> Taxes
    {
      get
      {
        if (m_taxes == null)
        {
          m_taxes = new List<Tax>();
        }

        return m_taxes;
      }
      set
      {
        m_taxes = value;
      }
    }
    public Int64 AmountToPay { get; set; }
    public String CreditType { get; set; }
  }
}
