﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CreateTicketResponse.cs
// 
//   DESCRIPTION: CreateTicketResponse class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using KioskRedemption.Classes.Common;
using System;
using System.Collections.Generic;

namespace KioskRedemption.Classes.Response
{
  public class CreateTicketResponse
  {
    public Int32 ResponseCode { get; set; }
    public CreateTicketResponseTicketData TicketData { get; set; }
  }

  public class CreateTicketResponseTicketData
  {
    List<Tax> m_taxes;

    public String TicketCode { get; set; }
    public Int64 RequestedAmount { get; set; }
    public List<Tax> Taxes
    {
      get
      {
        if (m_taxes == null)
        {
          m_taxes = new List<Tax>();
        }
        return m_taxes;
      }
      set
      {
        m_taxes = value;
      }
    }
    public Int64 AmountToPrint { get; set; }
    public Int32 Validity { get; set; }
  }
}
