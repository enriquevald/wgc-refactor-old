﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PaymentResponse.cs
// 
//   DESCRIPTION: PaymentResponse class
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 DPC    First version.
//------------------------------------------------------------------------------

using System;

namespace KioskRedemption.Classes.Response
{
  public class PaymentResponse
  {
    public Int32 ResponseCode { get; set; }
  }
}
