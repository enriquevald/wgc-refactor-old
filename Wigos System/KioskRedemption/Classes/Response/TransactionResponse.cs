﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TransactionResponse.cs
// 
//   DESCRIPTION: TransactionResponse class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 24-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-AUG-2017 ETP    First version.
//------------------------------------------------------------------------------

using System;

namespace KioskRedemption.Classes.Response
{
  /// <summary>
  /// Transaction Response
  /// </summary>
  public class TransactionResponse
  {
    public Int32 ResponseCode { get; set; }
  } // TransactionResponse
}
