﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskRedemption.Classes.Response
{
  public class LogResponse
  {
    public Int32 HttpCode { get; set; }
    public Dictionary<String, String> Headers { get; set; }
    public Object Body { get; set; }
  }
}
