﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: KioskStatusResponse.cs
// 
//   DESCRIPTION: KioskStatusResponse class
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 21-AUG-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-AUG-2017 ETP    First version.
//------------------------------------------------------------------------------

using System;

namespace KioskRedemption.Classes.Response
{
  /// <summary>
  /// Kiosk Status Response
  /// </summary>
  public class KioskStatusResponse
  {
    public Int32 ResponseCode { get; set; }
  } // KioskStatusResponse
}
