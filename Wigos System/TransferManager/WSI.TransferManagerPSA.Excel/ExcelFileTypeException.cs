﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExcelFileTypeException.cs
// 
//   DESCRIPTION: Internal exception class 
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Excel
{

  internal class ExcelFileTypeException: Exception
  {

    #region Attributes

    public enum ExceptionType
    {
      None,
      General,
      MismatchDateReport,
    }

    private ReportTabType m_session_type;
    private ExceptionType m_exception_type = ExceptionType.None;

    #endregion

    #region Constructor

    public ExcelFileTypeException(ReportTabType SessionType, string message) : base(message) 
    {
      m_session_type = SessionType;
      m_exception_type = ExceptionType.General;
    }

    public ExcelFileTypeException(ReportTabType SessionType, ExceptionType ExceptionType, string message)
      : base(message)
    {
      m_session_type = SessionType;
      m_exception_type = ExceptionType;
    }

    #endregion

    #region Properties
    public ReportTabType SessionType
    {
      get
      {
        return m_session_type;
      }
    }

    public ExceptionType GetExceptionType
    {
      get
      {
        return m_exception_type;
      }
    }
    #endregion

  }
}
