﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DailyReportExcel.cs
// 
//   DESCRIPTION: Class with information obtained of the excel file
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using LinqToExcel;
using LinqToExcel.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  public class DailyReportExcel : IReportExcel  
  {

    #region Attributes

    /// <summary>
    /// Object for read and captures information of the WorksheetDay
    /// </summary>
    private DailyReportTabExcel m_bingo_excel_worksheet_day;

    /// <summary>
    /// Object for read and captures information of the WorksheetNight
    /// </summary>
    private DailyReportTabExcel m_bingo_excel_worksheet_night;

    /// <summary>
    /// List with the customer for day sessions
    /// </summary>
    private List<DailyItem> m_list_customers_day_session;

    /// <summary>
    /// List with the customer for night sessions
    /// </summary>
    private List<DailyItem> m_list_customers_night_session;

    /// <summary>
    /// File used to proccess the information
    /// </summary>
    private SelectedFile m_selected_file;

    /// <summary>
    /// Day session information
    /// </summary>
    private DailyReport m_report_session_day;

    /// <summary>
    /// Night session information
    /// </summary>
    private DailyReport m_report_session_night;

    private GameType m_game_type;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="SelectedFile"></param>
    /// <param name="DateReport"></param>
    public DailyReportExcel(SelectedFile SelectedFile, DateTime DateReport, GameType GameType)
    {
      try
      {
        if (!SelectedFile.IsValidFilePath)
          throw new ExcelFileException(string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_FILE_PATH"), SelectedFile.FileName));

        try
        {
          m_game_type = GameType;
          m_bingo_excel_worksheet_day = new DailyReportTabExcel(SelectedFile, AppConfiguration.WorksheetDay, DateReport, ReportTabType.Daily_Session_Day);
          m_bingo_excel_worksheet_night = new DailyReportTabExcel(SelectedFile, AppConfiguration.WorksheetNight, DateReport, ReportTabType.Daily_Session_Night);
        }
        catch (ExcelFileTypeException e)
        {
          if (e.GetExceptionType == ExcelFileTypeException.ExceptionType.MismatchDateReport)
          {
            throw new ExcelFileException(e.Message);
          }
          else
          {
            if (e.SessionType == ReportTabType.Daily_Session_Day)
              throw new ExcelFileException(LanguageManager.GetValue("NLS_MESSAGE_SESSION_DAY") + ": " + e.Message);
            else if (e.SessionType == ReportTabType.Daily_Session_Night)
              throw new ExcelFileException(LanguageManager.GetValue("NLS_MESSAGE_SESSION_NIGHT") + ": " + e.Message);
            else
              throw new Exception(e.Message);
          }
        }

        m_selected_file = SelectedFile;

        BuildDaySession();
        BuildNightSession();
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Properties

    public Decimal TotalAmountSold
    {
      get
      {
        return m_report_session_day.TotalAmountSold + m_report_session_night.TotalAmountSold;
      }
    }

    public Decimal TotalAmountWin
    {
      get
      {
        return m_report_session_day.TotalAmountWin + m_report_session_night.TotalAmountWin;
      }
    }

    public Int32 TotalEvidences
    {
      get
      {
        return m_report_session_day.TotalEvidences + m_report_session_night.TotalEvidences;
      }
    }

    public decimal TotalRefunds
    {
      get 
      { 
        throw new NotImplementedException(); 
      }
    }

    public Decimal TotalAmountBalance
    {
      get
      {
        return m_report_session_day.TotalAmountBalance + m_report_session_night.TotalAmountBalance;
      }
    }

    /// <summary>
    /// Gets date of the report recorded
    /// </summary>
    public DateTime DateReport
    {
      get
      {
        return m_bingo_excel_worksheet_day.GetReportDate;  
      }
    }

    /// <summary>
    /// Selected file
    /// </summary>
    public SelectedFile SelectedFile
    {
      get
      {
        return m_selected_file;
      }
    }

    public Int32 Count
    {
      get 
      {
        return CustomerDailyItems.Count;
      }
    }

    /// <summary>
    ///  List with the customer for day and night sessions
    /// </summary>
    public List<DailyItem> CustomerDailyItems
    {
      get
      {
        List<DailyItem> _list_full = new List<DailyItem>();
        _list_full.AddRange(m_list_customers_day_session);
        _list_full.AddRange(m_list_customers_night_session);
        return _list_full;
      }
    }

    /// <summary>
    /// Gets an object with information about the day session
    /// </summary>
    public DailyReport DaySession
    {
      get
      {
        return m_report_session_day;
      }
    }

    /// <summary>
    /// Gets an object with information about the night session
    /// </summary>
    public DailyReport NightSession
    {
      get
      {
        return m_report_session_night;
      }
    }

    public GameType ReportGameType
    {
      get 
      {
        return m_game_type;
      }
    }

    #endregion

    #region Public Methods

    public object Clone()
    {
      return this.MemberwiseClone();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Get a list with information of each customer
    /// </summary>
    /// <param name="Items"></param>
    /// <returns></returns>
    private List<DailyItem> GetListByRowList(List<Row> Items,SessionType SessionType)
    {
      List<DailyItem> _list = new List<DailyItem>();

      foreach (var _row in Items)
      {
        DailyItem _item = new DailyItem()
        {
          ID = int.Parse(_row[0].ToString()),
          CustomerName = _row[1].ToString(),
          TotalCards = int.Parse(_row[5].ToString()),
          AmountSold = Utils.CurrencyStringToDecimal(_row[6].ToString(), AppConfiguration.CultureInfo),
          AmountWin = string.IsNullOrEmpty(_row[7].ToString()) ? default(decimal?) : Utils.CurrencyStringToDecimal(_row[7].ToString(), AppConfiguration.CultureInfo),
          Results = Utils.CurrencyStringToDecimal(_row[8].ToString(), AppConfiguration.CultureInfo),
          RFC = string.IsNullOrEmpty(_row[9].ToString()) ? null : _row[9].ToString(),
          Taxes = string.IsNullOrEmpty(_row[10].ToString()) ? default(decimal?) : Utils.DecimalStringToDecimal(_row[10].ToString(), AppConfiguration.CultureInfo),
          SessionType = SessionType,
        };

        _list.Add(_item);
      }

      return _list;
    }

    /// <summary>
    /// Build the day session object
    /// </summary>
    private void BuildDaySession()
    {
      m_report_session_day = new DailyReport(SessionType.Day);
      m_report_session_day.TotalCustomers = m_bingo_excel_worksheet_day.RowsWithData.Count;
      m_report_session_day.TotalCards = m_bingo_excel_worksheet_day.GetTotalCards;
      m_report_session_day.TotalAmountSold = m_bingo_excel_worksheet_day.GetTotalAmountSold;
      m_report_session_day.TotalAmountWin = m_bingo_excel_worksheet_day.GetTotalAmountWin;
      m_report_session_day.TotalAmountBalance = m_bingo_excel_worksheet_day.GetTotalBalance;
      m_report_session_day.TotalEvidences = m_bingo_excel_worksheet_day.GetTotalEvidences;

      m_list_customers_day_session = new List<DailyItem>();
      m_list_customers_day_session = GetListByRowList(m_bingo_excel_worksheet_day.RowsWithData, SessionType.Day);
      m_report_session_day.DailyItems = m_list_customers_day_session;
    }

    /// <summary>
    /// Build the night session object
    /// </summary>
    private void BuildNightSession()
    {
      m_report_session_night = new DailyReport(SessionType.Night);
      m_report_session_night.TotalCustomers = m_bingo_excel_worksheet_night.RowsWithData.Count;
      m_report_session_night.TotalCards = m_bingo_excel_worksheet_night.GetTotalCards;
      m_report_session_night.TotalAmountSold = m_bingo_excel_worksheet_night.GetTotalAmountSold;
      m_report_session_night.TotalAmountWin = m_bingo_excel_worksheet_night.GetTotalAmountWin;
      m_report_session_night.TotalAmountBalance = m_bingo_excel_worksheet_night.GetTotalBalance;
      m_report_session_night.TotalEvidences = m_bingo_excel_worksheet_night.GetTotalEvidences;

      m_list_customers_night_session = new List<DailyItem>();
      m_list_customers_night_session = GetListByRowList(m_bingo_excel_worksheet_night.RowsWithData, SessionType.Night);
      m_report_session_night.DailyItems = m_list_customers_night_session;
    }

    #endregion

  }
}
