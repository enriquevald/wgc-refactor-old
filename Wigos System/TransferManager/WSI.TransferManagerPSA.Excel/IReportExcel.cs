﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  public interface IReportExcel
  {
    DateTime DateReport { get; }

    SelectedFile SelectedFile { get; }

    Int32 Count { get; }

    Decimal TotalAmountSold { get; }

    Decimal TotalAmountWin { get; }

    Int32 TotalEvidences { get; }

    Decimal TotalRefunds { get; }

    Decimal TotalAmountBalance { get; }

    GameType ReportGameType { get; }
  }
}
