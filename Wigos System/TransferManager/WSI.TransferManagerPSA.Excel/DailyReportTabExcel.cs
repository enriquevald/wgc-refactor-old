﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DailyReportTabExcel.cs
// 
//   DESCRIPTION: Internal class that process the excel file (Format: Day session and Night session)
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
// 05-APR-2016 FAV    Rename class and change to implement an interface
//------------------------------------------------------------------------------

using LinqToExcel;
using LinqToExcel.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  internal class DailyReportTabExcel
  {

    #region Attributes

    private const int INDEX_COLUMN_CUSTOMER_NUMBER = 0;
    private const int INDEX_COLUMN_CUSTOMER_NAME = 1;
    private const int INDEX_COLUMN_TOTAL_CARDS = 5;
    private const int INDEX_COLUMN_AMOUNT_SOLD  = 6;
    private const int INDEX_COLUMN_AMOUNT_WIN = 7;
    private const int INDEX_COLUMN_RESULTS = 8;
    private const int INDEX_COLUMN_RFC = 9;
    private const int INDEX_COLUMN_TAXES = 10;

    private const int INDEX_ROW_REPORT_DATE = 0;
    private const int INDEX_COLUMN_REPORT_DATE = 8;
    private const int INDEX_REAL_ROW_HEADERS = 5;

    private const string COLUMN_NAME_INDEX_0 = "CUSTOMER #";
    private const string COLUMN_NAME_INDEX_1 = "CUSTOMER NAME";
    private const string COLUMN_NAME_INDEX_5 = "TOTAL CARDS";
    private const string COLUMN_NAME_INDEX_6 = "AMOUNT SOLD $NP";
    private const string COLUMN_NAME_INDEX_7 = "AMOUNT WIN $NP";
    private const string COLUMN_NAME_INDEX_8 = "RESULTS";
    private const string COLUMN_NAME_INDEX_9 = "RFC";
    private const string COLUMN_NAME_INDEX_10 = "TAXES";

    private ExcelQueryFactory m_excel;
    private string m_worksheet;
    private List<Row> m_all_rows;
    private List<Row> m_valid_rows;
    private Row m_row_total;
    private DateTime m_date_report;
    private ReportTabType m_session_type;
    private int m_counter_evidences = 0;
    private Boolean m_is_valid;

    #endregion

    #region Constructor

    public DailyReportTabExcel(SelectedFile SelectedFile, string WorksheetName, DateTime DateReport, ReportTabType SessionType)
    {
      try
      {
        m_is_valid = false;

        ValidateFile(SelectedFile.FilePath);
        m_excel = new ExcelQueryFactory(SelectedFile.FilePath);

        ValidateWorksheet(WorksheetName);
        m_worksheet = WorksheetName;

        m_date_report = Utils.GetDateWithoutTime(DateReport);
        m_session_type = SessionType;

        // Linq to read the excel file
        IQueryable<Row> _list = from item in m_excel.Worksheet(WorksheetName)
                                select item;

        // Gets all elements of the Excel file and Worksheet
        m_all_rows = _list.ToList();

        // Gets list of customers (with ID and Name)
        m_valid_rows = GetValidRowList(m_all_rows);

        // Gets the row with the totals
        m_row_total = GetRowTotal();

        ValidateAndThrow();

        m_is_valid = true;
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Returns all rows of the worksheet
    /// </summary>
    public List<Row> RowsAllWorksheet
    {
      get
      {
        return m_all_rows;
      }
    }

    /// <summary>
    /// Returns rows with customer information of the worksheet
    /// </summary>
    public List<Row> RowsWithData
    {
      get
      {
        return m_valid_rows;
      }
    }

    /// <summary>
    /// Returns the Date found in the worksheet
    /// </summary>
    public DateTime GetReportDate
    {
      get
      {
        string _date_excel = m_all_rows[INDEX_ROW_REPORT_DATE][INDEX_COLUMN_REPORT_DATE].Value.ToString();

        if (Utils.IsNumeric(_date_excel))
        {
          return DateTime.FromOADate(double.Parse(_date_excel));
        }
        else
        {
          return DateTime.Parse(_date_excel);
        }
      }
    }

    /// <summary>
    /// Returns the total number of cards found in the worksheet
    /// </summary>
    public int GetTotalCards
    {
      get 
      {
        return int.Parse(m_row_total[INDEX_COLUMN_TOTAL_CARDS].Value.ToString());
      }
    }

    /// <summary>
    /// Returns the total of Amount Sold found in the worksheet
    /// </summary>
    public decimal GetTotalAmountSold
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_AMOUNT_SOLD].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total of Amount Win found in the worksheet
    /// </summary>
    public decimal GetTotalAmountWin
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_AMOUNT_WIN].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total of Balance (Results columns) found in the worksheet
    /// </summary>
    public decimal GetTotalBalance
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_RESULTS].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total number of evidences (that requiere RFC and taxes)
    /// </summary>
    public int GetTotalEvidences
    {
      get
      {
        return m_counter_evidences;
      }
    }

    /// <summary>
    /// Checks if the excel file format is valid
    /// </summary>
    public bool IsValid
    {
      get
      {
        return m_is_valid;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Get the rows with the information for each customer
    /// </summary>
    /// <param name="Rows"></param>
    /// <returns></returns>
    private List<Row> GetValidRowList(List<Row> Rows)
    {
      List<Row> _list = new List<Row>();

      foreach (Row row in Rows)
      {
        if (IsValidRow(row) &&  ValidateCustomerName(row))
          _list.Add(row);
      }

      return _list;
    }

    /// <summary>
    /// Checks if the row has values and it is valid
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool IsValidRow(Row row)
    {
      try
      {
        if (Utils.IsNumeric(row[INDEX_COLUMN_CUSTOMER_NUMBER].Value.ToString()))
        {
          bool _is_null_row = int.Parse(row[INDEX_COLUMN_TOTAL_CARDS].Value.ToString()) == 0
                              && Utils.CurrencyStringToDecimal(row[INDEX_COLUMN_AMOUNT_SOLD].Value.ToString(), AppConfiguration.CultureInfo) == 0
                              && Utils.CurrencyStringToDecimal(row[INDEX_COLUMN_RESULTS].Value.ToString(), AppConfiguration.CultureInfo) == 0
                              && string.IsNullOrEmpty(row[INDEX_COLUMN_CUSTOMER_NAME].Value.ToString());

          return !_is_null_row;
        }
        else
        {
          return false;
        }
      }
      catch
      {
        return false;
      }
    }

    /// <summary>
    /// Checks if the customer row has a name
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool ValidateCustomerName(Row row)
    {
      if (string.IsNullOrEmpty(row[INDEX_COLUMN_CUSTOMER_NAME].Value.ToString()))
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_NAME_MANDATORY"), row[INDEX_COLUMN_CUSTOMER_NUMBER].ToString()));

      return true;
    }

    /// <summary>
    /// Validates structure of the data in the worksheet and throw an excel exception 
    /// if it doesn't pass the validation
    /// </summary>
    /// <returns></returns>
    private void ValidateAndThrow()
    {
      try
      {
        List<string> _header_names = GetListOfHeaderNames();

        if (_header_names.Count != 11)
        {
          Log.Error(string.Format("The Worksheet {0} doesn't have 11 colums", m_session_type.ToString()));
          throw new ExcelFileTypeException(m_session_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_NUMBER_COLUMNS"));
        }

        ValidateColumnName(_header_names[0], COLUMN_NAME_INDEX_0);
        ValidateColumnName(_header_names[1], COLUMN_NAME_INDEX_1);
        ValidateColumnName(_header_names[5], COLUMN_NAME_INDEX_5);
        ValidateColumnName(_header_names[6], COLUMN_NAME_INDEX_6);
        ValidateColumnName(_header_names[7], COLUMN_NAME_INDEX_7);
        ValidateColumnName(_header_names[8], COLUMN_NAME_INDEX_8);
        ValidateColumnName(_header_names[9], COLUMN_NAME_INDEX_9);
        ValidateColumnName(_header_names[10], COLUMN_NAME_INDEX_10);

        ValidateDate();

        ValidateResultsColumn();

        ValidateTotalForEachRow();
      }
      catch (ExcelFileTypeException ex)
      {
        Log.Error("The selected file has a format or value invalid: " + ex.Message);
        throw new ExcelFileTypeException(m_session_type, ex.Message);

      }
      catch (Exception ex)
      {
        Log.Error("The selected file has a format or value invalid. Checks the Exception please in the next line");
        Log.Exception(ex);
        throw new ExcelFileTypeException(m_session_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATA"));
      }
    }

    /// <summary>
    /// Returns a list with the header name for each column
    /// </summary>
    /// <returns></returns>
    private List<string> GetListOfHeaderNames()
    {
      List<string> _headers = new List<string>();

      var _row_headers = from c in m_excel.WorksheetRangeNoHeader("A" + INDEX_REAL_ROW_HEADERS, "Z" + INDEX_REAL_ROW_HEADERS, m_worksheet)
                         select c;

      foreach (var headerCell in _row_headers.First())
        _headers.Add(headerCell.ToString().Trim());

      return _headers;
    }

    /// <summary>
    /// Validates two column names
    /// </summary>
    /// <param name="ColumnName"></param>
    /// <param name="ColumnNameToCompare"></param>
    private void ValidateColumnName(string ColumnName, string ColumnNameToCompare)
    {
      if (ColumnName != ColumnNameToCompare)
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_COLUMN_NAME"), ColumnNameToCompare));
    }

    /// <summary>
    /// Check the calculating the total obtained for each column
    /// </summary>
    private void ValidateTotalForEachRow()
    {
      int _total_cards = m_valid_rows.Sum(x =>int.Parse(x[INDEX_COLUMN_TOTAL_CARDS].Value.ToString()));
      if (_total_cards != this.GetTotalCards)
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Total Cards"));

      decimal _amount_sold = m_valid_rows.Sum(x => Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_AMOUNT_SOLD].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_sold != this.GetTotalAmountSold)
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Total Amount Sold"));

      decimal _amount_win = m_valid_rows.Sum(x => string.IsNullOrEmpty(x[INDEX_COLUMN_AMOUNT_WIN].Value.ToString()) ? 0 : Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_AMOUNT_WIN].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_win != this.GetTotalAmountWin)
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Total Amount Win"));

    }

    /// <summary>
    /// Gets the row in the worksheet with total values
    /// </summary>
    private Row GetRowTotal()
    {
      var row_total = m_all_rows.First(x => x[0].Value.ToString().ToUpper() == "TOTAL");

      if (row_total == null)
        throw new ExcelFileTypeException(m_session_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_ROW_TOTAL"));

      return row_total;
    }

    /// <summary>
    /// Checks the 'Results' column and validates the RFC and TAXES column 
    /// </summary>
    private void ValidateResultsColumn()
    {
      m_counter_evidences = 0;

      foreach (var row in m_valid_rows)
      {
        decimal _results = Utils.CurrencyStringToDecimal(row[INDEX_COLUMN_RESULTS].Value.ToString(), AppConfiguration.CultureInfo);

        if (_results < AppConfiguration.MaxValueResults)
        {
          // If the 'Results' column has a value greater than for example 10000 then RFC and TAXES columns are mandatory.

          if (string.IsNullOrEmpty(row[INDEX_COLUMN_RFC].Value.ToString()))
            throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_RFC_MANDATORY"), row[INDEX_COLUMN_CUSTOMER_NUMBER].ToString()));

          if (string.IsNullOrEmpty(row[INDEX_COLUMN_TAXES].Value.ToString()))
            throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_TAXES_MANDATORY"), row[INDEX_COLUMN_CUSTOMER_NUMBER].ToString()));

          // Updates the number of evidences (that requiere RFC and taxes)
          m_counter_evidences++;
        }
      }
    }

    /// <summary>
    /// Validates if the file exists
    /// </summary>
    /// <param name="ExcelFileName"></param>
    private void ValidateFile(string ExcelFileName)
    {
      if (!File.Exists(ExcelFileName))
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_FILE_PATH"), ExcelFileName));
    }

    /// <summary>
    ///  Validates if worksheet exists
    /// </summary>
    /// <param name="WorksheetName"></param>
    private void ValidateWorksheet(string WorksheetName)
    {
      var _worksheet_name = m_excel.GetWorksheetNames().FirstOrDefault(x => x.ToString() == WorksheetName);
      if (_worksheet_name == null)
        throw new ExcelFileTypeException(m_session_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_WORKSHEET_NAME"), WorksheetName));
    }

    /// <summary>
    /// Validates date in the report
    /// </summary>
    private void ValidateDate()
    {
      if (m_date_report != this.GetReportDate)
        throw new ExcelFileTypeException(m_session_type, ExcelFileTypeException.ExceptionType.MismatchDateReport, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATE_REPORT"));
    }

    #endregion

  }
}
