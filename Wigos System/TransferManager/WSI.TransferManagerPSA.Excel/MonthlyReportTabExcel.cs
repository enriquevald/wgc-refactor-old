﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MonthlyReportTabExcel.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 19-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2016 FAV     First release.
// 31-MAY-2016 FAV     Fixed Bug 13425: Exception with a time with invalid format
// 31-MAY-2016 FAV     Fixed Bug 13429: The system doesn't validate the Line in the document
// 31-MAY-2016 FAV     Fixed Bug 13432: Validate an invalid date in the 'Fecha Jornada' column.
// 01-JUN-2016 FAV     Fixed Bug 13439: Validate duplicate days in the document
//------------------------------------------------------------------------------

using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  public class MonthlyReportTabExcel
  {
    #region Attributes

    private const int INDEX_COLUMN_VALUE = 1;

    private const int INDEX_COLUMN_DATE = 0;
    private const int INDEX_COLUMN_INCOME = 1;
    private const int INDEX_COLUMN_REFUND = 2;
    private const int INDEX_COLUMN_WIN = 3;
    private const int INDEX_COLUMN_NETO = 4;

    private const int INDEX_ROW_SITE_CODE = 0;
    private const int INDEX_ROW_SITE_NAME = 1;
    private const int INDEX_ROW_LINE = 2;
    private const int INDEX_ROW_TIME = 3;
    private const int INDEX_ROW_START_DATA = 5;

    private const int INDEX_REAL_ROW_HEADERS = 6;

    private const string COLUMN_NAME_INDEX_0 = "Fecha Jornada";
    private const string COLUMN_NAME_INDEX_1 = "Ingresos (Entradas)";
    private const string COLUMN_NAME_INDEX_2 = "Devoluciones (Salidas)";
    private const string COLUMN_NAME_INDEX_3 = "Premios Pagados (Salidas)";
    private const string COLUMN_NAME_INDEX_4 = "Resultado Neto Jornada";

    private ExcelQueryFactory m_excel;
    private string m_worksheet;
    private List<Row> m_all_rows;
    private List<Row> m_valid_rows;
    private Row m_row_total;
    private ReportTabType m_tab_type;
    private Int32 m_month;
    private Int32 m_year;
    private Int32 m_site_id;
    private Boolean m_is_valid;

    #endregion

    #region Constructor

    public MonthlyReportTabExcel(SelectedFile SelectedFile, String WorksheetName, Int32 Month, Int32 Year, Int32 SiteId, ReportTabType ReportTabType)
    {
      m_is_valid = false;

      try
      {
        ValidateFile(SelectedFile.FilePath);
        m_excel = new ExcelQueryFactory(SelectedFile.FilePath);

        if (ValidateWorksheet(WorksheetName, ReportTabType))
        {
          // Linq to read the excel file
          IQueryable<Row> _list = from item in m_excel.Worksheet(WorksheetName)
                                  select item;
          // Gets all elements of the Excel file and Worksheet
          m_all_rows = _list.ToList();

          m_worksheet = WorksheetName;
          m_tab_type = ReportTabType;

          ValidateLineWithTabType(WorksheetName, ReportTabType);

          m_month = Month;
          m_year = Year;
          m_site_id = SiteId;

          // Gets list of customers (with ID and Name)
          m_valid_rows = GetValidRowList(m_all_rows);

          // Gets the row with the totals
          m_row_total = GetRowWithTotals();

          ValidateAndThrow();

          m_is_valid = true;
        }
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Returns all rows of the worksheet
    /// </summary>
    public List<Row> RowsAllWorksheet
    {
      get
      {
        return m_all_rows;
      }
    }

    /// <summary>
    /// Returns rows with information of the worksheet
    /// </summary>
    public List<Row> RowsWithData
    {
      get
      {
        return m_valid_rows;
      }
    }
    
    /// <summary>
    /// Returns the site code
    /// </summary>
    public int GetSiteCode
    {
      get
      {
        return m_site_id;
      }
    }

    /// <summary>
    /// Returns the site name
    /// </summary>
    public String GetSiteName
    {
      get
      {
        return m_all_rows[INDEX_ROW_SITE_NAME][INDEX_COLUMN_VALUE].Value.ToString();
      }
    }

    /// <summary>
    /// Returns the line reported
    /// </summary>
    public String GetLineReported
    {
      get
      {
        return m_all_rows[INDEX_ROW_LINE][INDEX_COLUMN_VALUE].Value.ToString();
      }
    }

    /// <summary>
    /// Returns the time reported
    /// </summary>
    public DateTime GetTimeReported
    {
      get
      {
        decimal result;
        string _time_excel = m_all_rows[INDEX_ROW_TIME][INDEX_COLUMN_VALUE].Value.ToString();

        if (decimal.TryParse(_time_excel, out result))
        {
          decimal d = decimal.Parse(_time_excel, System.Globalization.NumberStyles.Float, new CultureInfo(AppConfiguration.CultureInfo));
          return DateTime.FromOADate((double)d);
        }
        else
        {
          return DateTime.Parse(_time_excel);
        }
      }
    }

    public Boolean IsTimeReportValid
    {
      get
      {
        try
        {
          return Utils.IsDateTime(this.GetTimeReported.ToString());
        }
        catch (Exception ex)
        { 
          Log.Exception(ex);
        }
        return false;
      }
    }

    /// <summary>
    /// Returns month of the report
    /// </summary>
    public Int32 GetMonth
    {
      get
      {
        return m_month;
      }
    }

    /// <summary>
    /// Returns year of the report
    /// </summary>
    public Int32 GetYear
    {
      get
      {
        return m_year;
      }
    }

    /// <summary>
    /// Returns the total of Amount Sold found in the worksheet
    /// </summary>
    public decimal GetTotalAmountIncome
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_INCOME].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total of refunds
    /// </summary>
    public decimal GetTotalAmountRefund
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_REFUND].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total of Amount Win found in the worksheet
    /// </summary>
    public decimal GetTotalAmountWin
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_WIN].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Returns the total of Balance (Results columns) found in the worksheet
    /// </summary>
    public decimal GetTotalAmountNeto
    {
      get
      {
        return Utils.CurrencyStringToDecimal(m_row_total[INDEX_COLUMN_NETO].Value.ToString(), AppConfiguration.CultureInfo);
      }
    }

    /// <summary>
    /// Checks if the excel file format is valid
    /// </summary>
    public bool IsValid
    {
      get
      {
        return m_is_valid;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Get the rows with the information for each customer
    /// </summary>
    /// <param name="Rows"></param>
    /// <returns></returns>
    private List<Row> GetValidRowList(List<Row> Rows)
    {
      List<Row> _list = new List<Row>();

      for (int i = INDEX_ROW_START_DATA; i < Rows.Count; ++i)
      {
        if (IsValidDateRow(Rows[i]))
        {
          _list.Add(Rows[i]);
        }
      }

      return _list;
    }

    /// <summary>
    /// Checks if the row has values and it is valid
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool IsValidDateRow(Row row)
    {
      try
      {
        DateTime _date;
        Boolean _is_a_date;
        String _date_excel = row[INDEX_COLUMN_DATE].Value.ToString();

        if (Utils.IsNumeric(_date_excel))
        {
          _is_a_date = DateTime.TryParse(DateTime.FromOADate(double.Parse(_date_excel)).ToString(), out _date);
        }
        else
        {
          _is_a_date = DateTime.TryParse(_date_excel, out _date);
        }

        if (_is_a_date)
        {
          // If is date then all columns should be numeric
          if (IsNumericField(row, INDEX_COLUMN_INCOME) && IsNumericField(row, INDEX_COLUMN_REFUND)
              && IsNumericField(row, INDEX_COLUMN_WIN) && IsNumericField(row, INDEX_COLUMN_NETO))
          {
            return true;
          }

          throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_ROW_DATE_WITH_INVALIDA_DATA"), _date.ToShortDateString()));
        }
        else
        {
          if (!string.IsNullOrEmpty(row[INDEX_COLUMN_DATE]) && row[INDEX_COLUMN_DATE] != "TOTAL")
            throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_COLUMN_WITH_INVALIDA_DATA"), COLUMN_NAME_INDEX_0));
        }
      }
      catch (ExcelFileTypeException ex)
      {
        Log.Error("The selected file has a format or value invalid: " + ex.Message);
        throw new ExcelFileTypeException(m_tab_type, ex.Message);

      }
      catch (Exception ex)
      {
        Log.Error("The selected file has a format or value invalid. Checks the Exception please in the next line");
        Log.Exception(ex);
        throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATA"));
      }

      return false;
    }

    /// <summary>
    /// It checks if the column (by index) in the row has a numeric value
    /// </summary>
    /// <param name="row"></param>
    /// <param name="ColumnIndex"></param>
    /// <returns></returns>
    private bool IsNumericField(Row row, Int32 ColumnIndex)
    { 
      try
      {
        String _value = row[ColumnIndex].Value.ToString();
    
        if (!String.IsNullOrEmpty (_value))
        {
          return Utils.IsNumeric(Utils.CurrencyStringToDecimal(_value, AppConfiguration.CultureInfo).ToString());
        }
      }
      catch
      {
      }
      return false;
    }

    /// <summary>
    /// Validates structure of the data in the worksheet and throw an excel exception 
    /// if it doesn't pass the validation
    /// </summary>
    /// <returns></returns>
    private void ValidateAndThrow()
    {
      try
      {
        List<string> _header_names = GetListOfHeaderNames();

        if (_header_names.Count != 5)
        {
          Log.Error(string.Format("The Worksheet {0} doesn't have 5 colums", m_tab_type.ToString()));
          throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_NUMBER_COLUMNS"));
        }

        ValidateColumnName(_header_names[0], COLUMN_NAME_INDEX_0);
        ValidateColumnName(_header_names[1], COLUMN_NAME_INDEX_1);
        ValidateColumnName(_header_names[2], COLUMN_NAME_INDEX_2);
        ValidateColumnName(_header_names[3], COLUMN_NAME_INDEX_3);
        ValidateColumnName(_header_names[4], COLUMN_NAME_INDEX_4);

        ValidateSiteId();

        ValidateDateForEachRow();
        ValidateDuplicateDates();

        ValidateTotalInColumns();
      }
      catch (ExcelFileTypeException ex)
      {
        Log.Error("The selected file has a format or value invalid: " + ex.Message);
        throw new ExcelFileTypeException(m_tab_type, ex.Message);

      }
      catch (Exception ex)
      {
        Log.Error("The selected file has a format or value invalid. Checks the Exception please in the next line");
        Log.Exception(ex);
        throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATA"));
      }
    }

    /// <summary>
    /// Returns a list with the header name for each column
    /// </summary>
    /// <returns></returns>
    private List<string> GetListOfHeaderNames()
    {
      List<string> _headers = new List<string>();

      var _row_headers = from c in m_excel.WorksheetRangeNoHeader("A" + INDEX_REAL_ROW_HEADERS, "Z" + INDEX_REAL_ROW_HEADERS, m_worksheet)
                         select c;

      foreach (var headerCell in _row_headers.First())
        _headers.Add(headerCell.ToString().Trim());

      return _headers;
    }

    /// <summary>
    /// Validates two column names
    /// </summary>
    /// <param name="ColumnName"></param>
    /// <param name="ColumnNameToCompare"></param>
    private void ValidateColumnName(string ColumnName, string ColumnNameToCompare)
    {
      if (ColumnName != ColumnNameToCompare)
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_COLUMN_NAME"), ColumnNameToCompare));
    }

    /// <summary>
    /// Check the calculating the total obtained for each column
    /// </summary>
    private void ValidateTotalInColumns()
    {
      decimal _amount_in = m_valid_rows.Sum(x => string.IsNullOrEmpty(x[INDEX_COLUMN_INCOME].Value.ToString()) ? 0 : Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_INCOME].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_in != this.GetTotalAmountIncome)
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Ingresos (Entradas)"));

      decimal _amount_refund = m_valid_rows.Sum(x => string.IsNullOrEmpty(x[INDEX_COLUMN_REFUND].Value.ToString()) ? 0 : Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_REFUND].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_refund != this.GetTotalAmountRefund)
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Devoluciones (Salidas)"));

      decimal _amount_paid = m_valid_rows.Sum(x => string.IsNullOrEmpty(x[INDEX_COLUMN_WIN].Value.ToString()) ? 0 : Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_WIN].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_paid != this.GetTotalAmountWin)
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Premios Pagados (Salidas)"));

      decimal _amount_neto = m_valid_rows.Sum(x => string.IsNullOrEmpty(x[INDEX_COLUMN_NETO].Value.ToString()) ? 0 : Utils.CurrencyStringToDecimal(x[INDEX_COLUMN_NETO].Value.ToString(), AppConfiguration.CultureInfo));
      if (_amount_neto != this.GetTotalAmountNeto)
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_FIELD_VALUE"), "Resultado Neto Jornada"));
    }

    /// <summary>
    /// Checks the date (Month and Year) for each row
    /// </summary>
    private void ValidateDateForEachRow()
    { 
      foreach (Row row in m_valid_rows)
      {
        DateTime _date;
        Boolean _is_a_date;
        String _date_excel = row[INDEX_COLUMN_DATE].Value.ToString();

        if (Utils.IsNumeric(_date_excel))
        {
          _is_a_date = DateTime.TryParse(DateTime.FromOADate(double.Parse(_date_excel)).ToString(), out _date);
        }
        else
        {
          _is_a_date = DateTime.TryParse(_date_excel, out _date);
        }

        if (_date.Month != m_month || _date.Year != m_year)
          throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATE_ROW"));
      }
    }

    private void ValidateDuplicateDates()
    {
      List<string> _list_dates = new List<string>();
      foreach (Row _row in m_valid_rows)
      {
        _list_dates.Add(_row[0]);
      }

      if (_list_dates.Count != _list_dates.Distinct().Count())
      {
        throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_DUPLICATE_DATES"));
      }    
    }

    private void ValidateSiteId()
    {
      if (m_site_id != int.Parse(m_all_rows[INDEX_ROW_SITE_CODE][INDEX_COLUMN_VALUE].Value.ToString()))
      {
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_SITE_ID"), m_site_id));
      }
    }

    /// <summary>
    /// Gets the row in the worksheet with total values
    /// </summary>
    private Row GetRowWithTotals()
    {
      var row_total = m_all_rows.First(x => x[0].Value.ToString().ToUpper() == "TOTAL");

      if (row_total == null)
        throw new ExcelFileTypeException(m_tab_type, LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_ROW_TOTAL"));

      return row_total;
    }

    /// <summary>
    /// Validates if the file exists
    /// </summary>
    /// <param name="ExcelFileName"></param>
    private void ValidateFile(string ExcelFileName)
    {
      if (!File.Exists(ExcelFileName))
        throw new ExcelFileTypeException(m_tab_type, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_FILE_PATH"), ExcelFileName));
    }

    /// <summary>
    ///  Validates if worksheet exists
    /// </summary>
    /// <param name="WorksheetName"></param>
    private bool ValidateWorksheet(string WorksheetName, ReportTabType ReportTabType)
    {
      var _worksheet_name = m_excel.GetWorksheetNames().FirstOrDefault(x => x.ToString() == WorksheetName);
      return (_worksheet_name != null);
    }

    private void ValidateLineWithTabType(string WorksheetName, ReportTabType ReportTabType)
    {
      if (ReportTabType == ReportTabType.Monthly_Bingo && m_all_rows[INDEX_ROW_LINE][INDEX_COLUMN_VALUE].Value.ToString() != "L005")
      {
        throw new ExcelFileTypeException(ReportTabType, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_LINE"), WorksheetName));
      }
      else if (ReportTabType == ReportTabType.Monthly_Tables && m_all_rows[INDEX_ROW_LINE][INDEX_COLUMN_VALUE].Value.ToString() != "L006")
      {
        throw new ExcelFileTypeException(ReportTabType, string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_LINE"), WorksheetName));
      }
    }

    #endregion

  }
}
