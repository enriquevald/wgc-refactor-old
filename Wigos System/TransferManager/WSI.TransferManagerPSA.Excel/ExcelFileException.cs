﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExcelFileException.cs
// 
//   DESCRIPTION: Exception class 
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Excel
{
  public class ExcelFileException: Exception
  {

    #region Constructor

    public ExcelFileException(string message) : base(message) 
    {
    }

    #endregion

  }
}
