﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MonthlyReportExcel.cs
// 
//   DESCRIPTION: Class with information obtained of the excel file
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 FAV    First release
//------------------------------------------------------------------------------

using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  public class MonthlyReportExcel : IReportExcel
  {

    #region Attributes

    private GameType m_game_type;

    #endregion

    #region Constructor

    public MonthlyReportExcel(GameType GameType)
    {
      m_game_type = GameType;
    }

    #endregion

    #region Properties
    public Int32 SiteCode { get; set; }

    public SelectedFile SelectedFile { get; set; }

    public Int32 Count
    {
      get
      {
        return MonthlyReport.MonthlyItems.Count;
      }
    }

    public MonthlyReport MonthlyReport { get; set; }

    public DateTime DateReport { get; set; }

    public decimal TotalAmountSold
    {
      get 
      {
        decimal _total = 0;
        foreach (MonthlyItem _item in this.MonthlyReport.MonthlyItems)
        {
          _total += _item.AmountIncome;
        }

        return _total;
      }
    }

    public decimal TotalAmountWin
    {
      get
      {
        decimal _total = 0;
        foreach (MonthlyItem _item in this.MonthlyReport.MonthlyItems)
        {
          _total += _item.AmountWin;
        }

        return _total;
      }
    }

    public decimal TotalRefunds
    {
      get
      {
        decimal _total = 0;
        foreach (MonthlyItem _item in this.MonthlyReport.MonthlyItems)
        {
          _total += _item.AmountRefund;
        }

        return _total;
      }
    }

    public decimal TotalAmountBalance
    {
      get
      {
        decimal _total = 0;
        foreach (MonthlyItem _item in this.MonthlyReport.MonthlyItems)
        {
          _total += _item.AmountNeto;
        }

        return _total;
      }
    }

    public int TotalEvidences
    {
      get { throw new NotImplementedException(); }
    }

    public GameType ReportGameType
    {
      get 
      {
        return m_game_type;
      }
    }

    #endregion

    #region Public Methods

    public object Clone()
    {
      return this.MemberwiseClone();
    }

    #endregion

  }
}