﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MonthlyReportExcelGroup.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 19-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2016 FAV     First release.
// 31-MAY-2016 FAV     Fixed Bug 13425: Exception with a time with invalid format
//------------------------------------------------------------------------------

using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Excel
{
  public class MonthlyReportExcelGroup : List<MonthlyReportExcel>
  {

    #region Attributes

    /// <summary>
    /// Object for read and captures information of the Bingo Worksheet
    /// </summary>
    private MonthlyReportTabExcel m_excel_worksheet_bingo;

    /// <summary>
    /// Object for read and captures information of the Tables Worksheet
    /// </summary>
    private MonthlyReportTabExcel m_excel_worksheet_tables;

    /// <summary>
    /// File used to proccess the information
    /// </summary>
    private SelectedFile m_selected_file;

    /// <summary>
    /// Month
    /// </summary>
    private Int32 m_month;

    /// <summary>
    /// Year
    /// </summary>
    private Int32 m_year;

    /// <summary>
    /// Site Id
    /// </summary>
    private Int32 m_site_id;

    private String m_site_name;

    private List<MonthlyReportExcel> m_list_bingo_monthly_report;

    private List<MonthlyReportExcel> m_list_tables_monthly_report;

    private DateTime m_time_bingo;

    private DateTime m_time_tables;

    #endregion

    #region Constructor

    public MonthlyReportExcelGroup(SelectedFile SelectedFile, Int32 Month, Int32 Year, Int32 SiteId)
    {
      try
      {
        if (!SelectedFile.IsValidFilePath)
          throw new ExcelFileException(string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_FILE_PATH"), SelectedFile.FileName));

        try
        {
          m_month = Month;
          m_year = Year;
          m_site_id = SiteId;

          // Build and validate
          m_excel_worksheet_bingo = new MonthlyReportTabExcel(SelectedFile, "Bingo", Month, Year, SiteId, ReportTabType.Monthly_Bingo);
          m_excel_worksheet_tables = new MonthlyReportTabExcel(SelectedFile, "Mesas", Month, Year, SiteId,  ReportTabType.Monthly_Tables);

          ValidateTabsInDocument();

        }
        catch (ExcelFileTypeException e)
        {
          if (e.GetExceptionType == ExcelFileTypeException.ExceptionType.MismatchDateReport)
          {
            throw new ExcelFileException(e.Message);
          }
          else
          {
            if (e.SessionType == ReportTabType.Monthly_Bingo)
              throw new ExcelFileException(LanguageManager.GetValue("NLS_TITLE_BINGO") + ": " + e.Message);
            else if  (e.SessionType == ReportTabType.Monthly_Tables)
              throw new ExcelFileException(LanguageManager.GetValue("NLS_TITLE_TABLES") + ": " + e.Message);
            else
              throw;
          }
        }

        m_selected_file = SelectedFile;

        m_list_bingo_monthly_report = new List<MonthlyReportExcel>();
        m_list_tables_monthly_report = new List<MonthlyReportExcel>();

        if (m_excel_worksheet_bingo.IsValid)
          BuildBingoReport();

        if (m_excel_worksheet_tables.IsValid)
          BuildTablesReport();
      }
      catch
      {
        throw;
      }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Month
    /// </summary>
    public Int32 Month
    {
      get
      {
        return m_month;
      }
    }

    /// <summary>
    /// Year
    /// </summary>
    public Int32 Year
    {
      get
      {
        return m_year;
      }
    }

    /// <summary>
    /// Site Id
    /// </summary>
    public Int32 SiteId
    {
      get
      {
        return m_site_id;
      }
    }

    /// <summary>
    /// Site Name
    /// </summary>
    public String SiteName
    {
      get
      {
        return m_site_name;
      }
    }

    /// <summary>
    /// Bingo MonthlyReportExcel list
    /// </summary>
    public List<MonthlyReportExcel> BingoMonthlyReportExcelList
    {
      get
      {
        return m_list_bingo_monthly_report;
      }
    }

    /// <summary>
    /// Tables MonthlyReportExcel list
    /// </summary>
    public List<MonthlyReportExcel> TablesMonthlyReportExcelList
    {
      get
      {
        return m_list_tables_monthly_report;
      }
    }

    /// <summary>
    /// Get time of the Bingo tab
    /// </summary>
    public String GetTimeBingo
    {
      get
      {
        return m_time_bingo.ToLongTimeString();
      }
    }

    /// <summary>
    /// Get time of the Tables tab
    /// </summary>
    public String GetTimeTables
    {
      get
      {
        return m_time_tables.ToLongTimeString();
      } 
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Build the bingo report
    /// </summary>
    private void BuildBingoReport()
    {
      List<MonthlyItem> _list;

      _list = GetListByRowList(m_excel_worksheet_bingo.RowsWithData, GameType.Bingo);

      if (m_excel_worksheet_bingo.IsTimeReportValid)
        m_time_bingo = m_excel_worksheet_bingo.GetTimeReported;
      else
        throw new ExcelFileTypeException(ReportTabType.Monthly_Bingo, LanguageManager.GetValue("NLS_EXCEPTION_TIME_WITH_INVALIDA_DATA"));

      m_site_name = m_excel_worksheet_bingo.GetSiteName;

      foreach (MonthlyItem _item in _list)
      {
        MonthlyReportExcel _report = new MonthlyReportExcel(GameType.Bingo);
        _report.DateReport = new DateTime(_item.Date.Year, _item.Date.Month, _item.Date.Day, m_time_bingo.Hour, m_time_bingo.Minute, m_time_bingo.Second);
        _report.SelectedFile = m_selected_file;
        _report.SiteCode = m_excel_worksheet_bingo.GetSiteCode;

        MonthlyReport _monthly_report;
        _monthly_report = new MonthlyReport(GameType.Bingo);
        _monthly_report.Line = m_excel_worksheet_bingo.GetLineReported;
        _monthly_report.SiteCode = m_excel_worksheet_bingo.GetSiteCode;
        _monthly_report.MonthlyItems = new List<MonthlyItem>() { _item };

        _report.MonthlyReport = _monthly_report;

        m_list_bingo_monthly_report.Add(_report);
        this.Add(_report);
      }
    }

    /// <summary>
    /// Build the Tables report
    /// </summary>
    private void BuildTablesReport()
    {
      List<MonthlyItem> _list;

      _list = GetListByRowList(m_excel_worksheet_tables.RowsWithData, GameType.Tables);

      if (m_excel_worksheet_tables.IsTimeReportValid)
        m_time_tables = m_excel_worksheet_tables.GetTimeReported;
      else
        throw new ExcelFileTypeException(ReportTabType.Monthly_Tables, LanguageManager.GetValue("NLS_EXCEPTION_TIME_WITH_INVALIDA_DATA"));

      if (String.IsNullOrEmpty(m_site_name))
        m_site_name = m_excel_worksheet_tables.GetSiteName;

      foreach (MonthlyItem _item in _list)
      {
        MonthlyReportExcel _report = new MonthlyReportExcel(GameType.Tables);

        _report.DateReport = new DateTime(_item.Date.Year, _item.Date.Month, _item.Date.Day, m_time_tables.Hour, m_time_tables.Minute, m_time_tables.Second);
        _report.SelectedFile = m_selected_file;
        _report.SiteCode = m_excel_worksheet_tables.GetSiteCode;

        MonthlyReport _monthly_report;
        _monthly_report = new MonthlyReport(GameType.Tables);
        _monthly_report.Line = m_excel_worksheet_tables.GetLineReported;
        _monthly_report.SiteCode = m_excel_worksheet_tables.GetSiteCode;
        _monthly_report.MonthlyItems = new List<MonthlyItem>() { _item };

        _report.MonthlyReport = _monthly_report;

        m_list_tables_monthly_report.Add(_report);
        this.Add(_report);
      }
    }

    /// <summary>
    /// Get a list with information of each customer
    /// </summary>
    /// <param name="Items"></param>
    /// <param name="GameType"></param>
    /// <returns></returns>
    private List<MonthlyItem> GetListByRowList(List<Row> Items, GameType GameType)
    {
      List<MonthlyItem> _list = new List<MonthlyItem>();

      foreach (var _row in Items)
      {
        MonthlyItem _item = new MonthlyItem()
        {
          Date = Utils.IsNumeric(_row[0].ToString()) ? DateTime.FromOADate(Double.Parse(_row[0].ToString())) : DateTime.Parse(_row[0].ToString()),
          AmountIncome = Utils.CurrencyStringToDecimal(_row[1].ToString(), AppConfiguration.CultureInfo),
          AmountRefund = Utils.CurrencyStringToDecimal(_row[2].ToString(), AppConfiguration.CultureInfo),
          AmountWin = Utils.CurrencyStringToDecimal(_row[3].ToString(), AppConfiguration.CultureInfo),
          AmountNeto = Utils.CurrencyStringToDecimal(_row[4].ToString(), AppConfiguration.CultureInfo),
          GameType = GameType,
        };

        _list.Add(_item);
      }

      return _list;
    }

    private void ValidateTabsInDocument()
    {
      if (m_excel_worksheet_bingo.IsValid && m_excel_worksheet_tables.IsValid)
      {
        if (m_excel_worksheet_bingo.GetSiteCode != m_excel_worksheet_tables.GetSiteCode)
        {
          throw new ExcelFileException(string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_VALUE_IN_TABS"),
                                       LanguageManager.GetValue("NLS_MESSAGE_SITE_CODE")));
        }

        if (m_excel_worksheet_bingo.GetSiteName != m_excel_worksheet_tables.GetSiteName)
        {
          throw new ExcelFileException(string.Format(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_VALUE_IN_TABS"),
                                       LanguageManager.GetValue("NLS_MESSAGE_SITE_NAME")));
        }
      }

      if (!m_excel_worksheet_bingo.IsValid && !m_excel_worksheet_tables.IsValid)
      {
        throw new ExcelFileException(LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_DATA"));
      }
    }

    #endregion

  }
}
