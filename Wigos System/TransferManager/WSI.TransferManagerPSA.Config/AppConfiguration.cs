﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AppConfiguration.cs
// 
//   DESCRIPTION: Class to manage the configuration file for the application
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 06-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Config
{
  public static class AppConfiguration
  {
    #region Attributes

    private const string CONFIGURATION_FILE = "BingoReportTransfer.config";

    private static Configuration m_configuration = null;
    
    #endregion

    #region Constructor

    static AppConfiguration()
    {
      try
      {
        ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = CONFIGURATION_FILE };
        m_configuration = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

        if (!File.Exists(m_configuration.FilePath))
          throw new Exception(string.Format("There is not configuration file ({0}) required by the application", CONFIGURATION_FILE));
      }
      catch
      { }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Language to the application
    /// </summary>
    public static LanguageType Language
    {
      get
      {
        string _language = GetValueSettingByName("Idioma");
        if (_language == "es")
          return LanguageType.Spanish;
        else if (_language == "en")
          return LanguageType.English;
        else
          return LanguageType.None;
      }
    }

    /// <summary>
    /// Worksheet name for day session
    /// </summary>
    public static string WorksheetDay
    {
      get
      {
        return GetValueSettingByName("WorksheetDia");
      }
    }

    /// <summary>
    /// Worksheet name for night session
    /// </summary>
    public static string WorksheetNight
    {
      get
      {
        return GetValueSettingByName("WorksheetNoche");
      }
    }

    /// <summary>
    /// Max value of the balance to send in the report  RFC and Taxes mandatorily
    /// </summary>
    public static int MaxValueResults
    {
      get
      {
        string resultsValue = GetValueSettingByName("ValorSolicitudConstancia");
        if (!Utils.IsNumeric(resultsValue))
          throw new Exception("The value for 'ValorSolicitudConstancia' key in the configuration file is not valid. It is not numeric!");

        return int.Parse(resultsValue);
      }
    }

    /// <summary>
    /// Date of the last report sent successfully
    /// </summary>
    public static DateTime? DateLastReport
    {
      get
      {
        string _date = GetValueSettingByName("FechaUltimoReporte");
        if (string.IsNullOrEmpty(_date))
          return null;

        return new DateTime(int.Parse(_date.Substring(0, 4)),
                            int.Parse(_date.Substring(4, 2)),
                            int.Parse(_date.Substring(6, 2)));
      }
      set
      {
        if (value == null)
          m_configuration.AppSettings.Settings["FechaUltimoReporte"].Value = string.Empty;
        else
          m_configuration.AppSettings.Settings["FechaUltimoReporte"].Value = value.Value.ToString("yyyyMMdd");

        m_configuration.Save();
      }
    }

    /// <summary>
    /// Directory of the last file sent successfully
    /// </summary>
    public static string DirectoryDefault
    {
      get
      {
        return GetValueSettingByName("DirectorioPorDefecto");
      }
      set
      {
        m_configuration.AppSettings.Settings["DirectorioPorDefecto"].Value = value;
        m_configuration.Save();
        ConfigurationManager.RefreshSection("appSettings");
      }
    }

    /// <summary>
    /// Establishment key 
    /// </summary>
    public static string ClaveEstablecimiento
    {
      get
      {
        return GetValueSettingByName("ClaveEstablecimiento");
      }
    }

    /// <summary>
    /// Combination key
    /// </summary>
    public static string ClaveCombinacion
    {
      get
      {
        return GetValueSettingByName("ClaveCombinacion");
      }
    }

    /// <summary>
    /// Payment type key
    /// </summary>
    public static string ClaveTipoPago
    {
      get
      {
        return GetValueSettingByName("ClaveTipoPago");
      }
    }

    /// <summary>
    /// Subline bussiness key
    /// </summary>
    public static string ClavesublineaNegocio
    {
      get
      {
        return GetValueSettingByName("ClavesublineaNegocio");
      }
    }

    /// <summary>
    /// Version
    /// </summary>
    public static string Version
    {
      get
      {
        return GetValueSettingByName("Version");
      }
    }

    /// <summary>
    /// Principal URL to connect to the PSA web service
    /// </summary>
    public static string URL_Principal_PSA
    {
      get
      {
        return GetValueSettingByName("URLPrincipalPSA");
      }
    }

    /// <summary>
    /// Secondary URL to connect to the PSA web service
    /// </summary>
    public static string URL_Secundary_PSA
    {
      get
      {
        return GetValueSettingByName("URLSecundariaPSA");
      }
    }

    public static string CultureInfo
    {
      get
      {
        string _culture = GetValueSettingByName("CultureInfo");

        if (String.IsNullOrEmpty(_culture))
          _culture = "en-US"; //default

        return _culture;
      }
    }
    #endregion

    #region Private Methods

    private static string GetValueSettingByName(string SettingName)
    {
      if (m_configuration.AppSettings.Settings[SettingName] == null)
        return null;

      return m_configuration.AppSettings.Settings[SettingName].Value.ToString();
    }

    #endregion
  }
}
