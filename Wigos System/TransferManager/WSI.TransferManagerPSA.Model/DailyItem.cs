﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DailyItem.cs
// 
//   DESCRIPTION: Class with information about each Customer in a daily detail report.
//                Look the xls file format.
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 FAV    First release. Rename class
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{
  public class DailyItem
  {
    /// <summary>
    /// CUSTOMER #
    /// </summary>
    public int ID { get; set; }

    /// <summary>
    /// CUSTOMER NAME 
    /// </summary>
    public string CustomerName { get; set; }
    
    /// <summary>
    /// TOTAL CARDS
    /// </summary>
    public int TotalCards { get; set; }
    
    /// <summary>
    /// AMOUNT SOLD $NP
    /// </summary>
    public decimal AmountSold  { get; set; }
    
    /// <summary>
    /// AMOUNT WIN $NP
    /// </summary>
    public decimal? AmountWin { get; set; }
    
    /// <summary>
    /// RESULTS
    /// </summary>
    public decimal Results { get; set; }
    
    /// <summary>
    /// RFC
    /// </summary>
    public string RFC { get; set; }
    
    /// <summary>
    /// TAXES
    /// </summary>
    public decimal? Taxes { get; set; }

    /// <summary>
    /// TAB (Day Bingo Rep / Nght Bingo Rep)
    /// </summary>
    public SessionType SessionType { get; set; }
  }
}
