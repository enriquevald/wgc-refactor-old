﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DailyReport.cs
// 
//   DESCRIPTION: Class with information about the daily detail report
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 10-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{
  /// <summary>
  /// Session Bingo class
  /// </summary>
  public class DailyReport
  {
    private SessionType m_report_session_type = SessionType.none;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="SessionType">Report session type</param>
    public DailyReport(SessionType SessionType)
    {
      m_report_session_type = SessionType;
    }

    /// <summary>
    /// Get the report session type
    /// </summary>
    public SessionType ReportSessionType 
    {
      get
      {
        return m_report_session_type;
      }
    }

    /// <summary>
    /// Number of elements(customers) for the day session.
    /// </summary>
    public int TotalCustomers{ get; set; }

    /// <summary>
    ///Returns the Total Nmber of Cards
    /// </summary>
    public int TotalCards{ get; set; }

    /// <summary>
    /// Returns the total of Amount Sold
    /// </summary>
    public decimal TotalAmountSold{ get; set; }

    /// <summary>
    /// Returns the total of Amount Win
    /// </summary>
    public decimal TotalAmountWin{ get; set; }

    /// <summary>
    /// Returns the total of Balance
    /// </summary>
    public decimal TotalAmountBalance{ get; set; }

    /// <summary>
    /// Returns the total of Evidences
    /// </summary>
    public int TotalEvidences { get; set; }

    /// <summary>
    /// List with the customer for session
    /// </summary>
    public List<DailyItem> DailyItems { get; set; }

  }
}
