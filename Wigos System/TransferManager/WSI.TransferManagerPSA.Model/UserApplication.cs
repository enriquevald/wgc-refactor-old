﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Model
{
  public static class UserApplication
  {
    #region Attributes

    private const string PATH_REGISTRY = @"Software\BingoReportTransfer\Common";

    private static string m_operator = string.Empty;
    private static string m_login = string.Empty;
    private static string m_password = string.Empty;

    #endregion

    #region Constructor

    static UserApplication()
    {
      m_operator = WinRegistry.GetKey(PATH_REGISTRY, @"Operador");
      m_login = WinRegistry.GetKey(PATH_REGISTRY, @"Login");
      m_password = WinRegistry.GetKey(PATH_REGISTRY, @"Password");
    }

    #endregion

    #region Properties

    /// <summary>
    /// Operator ID
    /// </summary>
    public static string Operator
    {
      get { return m_operator; }
    }

    /// <summary>
    /// User name
    /// </summary>
    public static string Login
    {
      get { return m_login; }
    }

    /// <summary>
    /// Password
    /// </summary>
    public static string Password
    {
      get { return m_password; }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Update user information in the object and the windows registry
    /// </summary>
    /// <param name="Operator"></param>
    /// <param name="Login"></param>
    /// <param name="Password"></param>
    /// <returns></returns>
    public static void Update(string Operator, string Login, string Password)
    {
      WinRegistry.SetKey(PATH_REGISTRY, @"Operador", Operator);
      WinRegistry.SetKey(PATH_REGISTRY, @"Login", Login);
      WinRegistry.SetKey(PATH_REGISTRY, @"Password", Password);

      m_operator = Operator;
      m_login = Login;
      m_password = Password;
    }

    /// <summary>
    /// Check if is the first time that the User is logging in the application
    /// </summary>
    /// <returns></returns>
    public static bool IsFirstLoginInTheSystem()
    {
      // Check if there is information in the registry
      return (string.IsNullOrEmpty(m_operator) && string.IsNullOrEmpty(m_login) && string.IsNullOrEmpty(m_password));
    }
    #endregion
  }
}
