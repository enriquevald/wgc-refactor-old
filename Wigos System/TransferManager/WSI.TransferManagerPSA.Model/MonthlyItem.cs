﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MonthlyItem.cs
// 
//   DESCRIPTION: Class with information about the summary for each day in a month
//                Look the xls file format.
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{
  public class MonthlyItem
  {
    /// <summary>
    /// Fecha Jornada
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Ingresos (Entradas)
    /// </summary>
    public decimal AmountIncome { get; set; }

    /// <summary>
    /// Devoluciones (Salidas)
    /// </summary>
    public decimal AmountRefund { get; set; }

    /// <summary>
    /// Premios pagados (Salidas)
    /// </summary>
    public decimal AmountWin { get; set; }

    /// <summary>
    /// Resultado Neto Jornada
    /// </summary>
    public decimal AmountNeto { get; set; }

    /// <summary>
    /// TAB (Bingo / Mesas)
    /// </summary>
    public GameType GameType { get; set; }
  }
}
