﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{

  /// <summary>
  /// Session Type. Tabs for dayly report
  /// </summary>
  public enum SessionType
  { 
    none,
    Day,
    Night,
  }

  /// <summary>
  /// Information Type. Tabs in summary report
  /// </summary>
  public enum GameType
  {
    none,
    Bingo,
    Tables,
  }
}
