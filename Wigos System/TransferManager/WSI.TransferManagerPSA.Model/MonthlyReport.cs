﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MonthlyReport.cs
// 
//   DESCRIPTION: Class with information about the monthly summary report
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 07-APR-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-APR-2016 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{
  public class MonthlyReport
  {
    private GameType m_report_game_type = GameType.none;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="GameType">Report game type</param>
    public MonthlyReport(GameType GameType)
    {
      m_report_game_type = GameType;
    }

    /// <summary>
    /// Site code (Sala)
    /// </summary>
    public int SiteCode { get; set; }

    /// <summary>
    /// Line to report
    /// </summary>
    public String Line { get; set; }

    /// <summary>
    /// Get the game type (Tab)
    /// </summary>
    public GameType ReportGameType 
    {
      get
      {
        return m_report_game_type;
      }
    }

    /// <summary>
    /// Number of elements(days) in the report
    /// </summary>
    public int TotalDays { get; set; }

    /// <summary>
    /// Returns the total of Amount Income
    /// </summary>
    public decimal TotalAmountIncome { get; set; }

    /// <summary>
    /// Returns the total of Amount Refund
    /// </summary>
    public decimal TotalAmountRefund { get; set; }

    /// <summary>
    /// Returns the total of Amount Win
    /// </summary>
    public decimal TotalAmountWin { get; set; }

    /// <summary>
    /// Returns the total of Amount Neto
    /// </summary>
    public decimal TotalAmountNeto { get; set; }

    /// <summary>
    /// List with the items for game
    /// </summary>
    public List<MonthlyItem> MonthlyItems { get; set; }
  }
}
