﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Model
{
  public class SelectedFile
  {

    #region Attributes

    private string m_file_path = string.Empty;
    private bool m_is_valid_file = false;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="FilePath">Full path of the file</param>
    public SelectedFile(string FilePath)
    {
      m_file_path = FilePath;
      m_is_valid_file = File.Exists(m_file_path);
    }

    #endregion

    #region Properties

    /// <summary>
    /// Full path of the selected file
    /// </summary>
    public string FilePath
    {
      get
      {
        return m_file_path;
      }
    }

    /// <summary>
    /// Directory of the selected file
    /// </summary>
    public string Directory
    {
      get
      {
        return Path.GetDirectoryName(m_file_path);
      }
    }

    /// <summary>
    /// File name of the selected file
    /// </summary>
    public string FileName
    {
      get
      {
        return Path.GetFileName(m_file_path);
      }
    }

    /// <summary>
    /// Return true if the file exists
    /// </summary>
    public bool IsValidFilePath
    {
      get
      {
        return m_is_valid_file;
      }
    }

    #endregion
  }
}
