﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.TransferManagerPSA.Client;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Model;
using WSI.TransferManagerPSA.Client.PSA_Refence;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Operation;
using System.Collections.Generic;

namespace WSI.TransferManagerPSA.UnitTest
{
  [TestClass]
  public class PSAClientTest
  {
    private const string CLAVE_LINEA_NEGOCIO = "L005";
    private PSAConfiguration m_psa_config;

    [TestInitialize]
    public void TestInitialize()
    {
      m_psa_config = new PSAConfiguration();
      m_psa_config.LoadData();
    }

    [TestMethod]
    public void TestOpenAndCloseSession()
    {
      PSAClient _client = new PSAClient(m_psa_config);

      Assert.IsTrue(_client.OpenSession() == ResponseCode.PSA_RC_OK, "The client cannot open connection to the PSA Service");
      Assert.IsTrue(_client.CloseSession() == ResponseCode.PSA_RC_OK, "The client cannot close connection to the PSA Service");
    }

    [TestMethod]
    public void TestRequestPendingReports()
    {
      bool _operation = false;
      List<DiaPendiente> _list = null;
      
      PSAClient _client = new PSAClient(m_psa_config);
      _client.OpenSession();
      _operation = _client.RequestPendingReports(2015, 06, out _list);
      _client.CloseSession();

      Assert.IsTrue(_list != null, "The operation to check if the report exist in the PSA has failed!");
    }

    [TestMethod]
    public void TestOperatorCatalog()
    {
      bool _operation = false;
      PSAClient _client = new PSAClient(m_psa_config);
      _client.OpenSession();


      PSA_Catalogos_Operador _cat_operador = new PSA_Catalogos_Operador();
      _cat_operador.CatalogoCombinaciones = new List<Combinaciones>();
      _cat_operador.CatalogoSublineaNegocio = new List<SubLineaNegocio>();
      _cat_operador.CatalogoTipoPago = new List<TipoPago>();
      Combinaciones _comb = new Combinaciones()
      {
        ClaveCombinacion = AppConfiguration.ClaveCombinacion,
        ClaveLineaNegocio = CLAVE_LINEA_NEGOCIO,
        Descripcion = "Linea005",
      };
      SubLineaNegocio _subl = new SubLineaNegocio()
      {
        ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio,
        ClaveLineaNegocio = CLAVE_LINEA_NEGOCIO,
        Descripcion = "Sublinea005",
      };
      TipoPago _tip = new TipoPago()
      {
        ClaveTipoPago = AppConfiguration.ClaveTipoPago,
        ClaveLineaNegocio = CLAVE_LINEA_NEGOCIO,
        Descripcion = "TipoPago005",
      };
      _cat_operador.CatalogoCombinaciones.Add(_comb);
      _cat_operador.CatalogoSublineaNegocio.Add(_subl);
      _cat_operador.CatalogoTipoPago.Add(_tip);

      _operation = _client.CatalogosOperador(_cat_operador);
      _client.CloseSession();

      Assert.IsTrue(_operation, "The operation to check if the report exist in the PSA has failed!");
    }
  }
}
