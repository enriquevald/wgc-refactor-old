﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.UnitTest
{
  [TestClass]
  public class AppConfigurationTest
  {
    [TestMethod]
    public void TestConfigurationRead()
    {
      bool _config_values =  string.IsNullOrEmpty(AppConfiguration.ClaveCombinacion)
                            || string.IsNullOrEmpty(AppConfiguration.ClaveEstablecimiento)
                            || string.IsNullOrEmpty(AppConfiguration.ClavesublineaNegocio)
                            || string.IsNullOrEmpty(AppConfiguration.ClaveTipoPago)
                            || string.IsNullOrEmpty(AppConfiguration.URL_Principal_PSA)
                            || string.IsNullOrEmpty(AppConfiguration.URL_Secundary_PSA)
                            || string.IsNullOrEmpty(AppConfiguration.Version)
                            || string.IsNullOrEmpty(AppConfiguration.WorksheetDay)
                            || string.IsNullOrEmpty(AppConfiguration.WorksheetNight)
                            || AppConfiguration.MaxValueResults < 0;
      Assert.IsTrue(_config_values, "All Settings in the configuration field should be different to empty");
    }

    [TestMethod]
    public void TestConfigurarionReadAndWrite_DirectorioDefault()
    {
      string _folder = @"C:\test_" + DateTime.Now.ToString("yyyyMMddHHmmss"); 
      AppConfiguration.DirectoryDefault = _folder;
      Assert.IsTrue( AppConfiguration.DirectoryDefault == _folder, "The test failed saving the directory path");
    }

    [TestMethod]
    public void TestConfigurationReadAndWrite_Date()
    {
      DateTime _date = Utils.GetDateWithoutTime(DateTime.Now);

      AppConfiguration.DateLastReport = _date;
      Assert.IsTrue(AppConfiguration.DateLastReport == _date, "The test failed saving the date");
    }
  }
}
