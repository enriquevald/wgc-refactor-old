﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.TransferManagerPSA.Language;

namespace WSI.TransferManagerPSA.UnitTest
{
  [TestClass]
  public class LanguageTest
  {
    [TestMethod]
    public void TestLanguageReadTextInEnglish()
    {
      LanguageManager.Initialize();

      LanguageManager.SetLanguageType = Common.LanguageType.English;
      string _text = LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_COLUMN_NAME");
      Assert.IsTrue(!string.IsNullOrEmpty(_text), "The method has returned an empty value");

    }

    [TestMethod]
    public void TestlanguageReadTextInDefaultLanguage()
    {
      LanguageManager.Initialize();

      string _text = LanguageManager.GetValue("NLS_EXCEPTION_EXCEL_INVALID_COLUMN_NAME");
      Assert.IsTrue(!string.IsNullOrEmpty(_text), "The method has returned an empty value");
    }
  }
}
