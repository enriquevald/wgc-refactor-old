﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.UnitTest
{
  [TestClass]
  public class ExcelFileTest
  {
    SelectedFile _valid_file;

    [TestInitialize]
    public void TestInitialize()
    {
      _valid_file = new SelectedFile(AppDomain.CurrentDomain.BaseDirectory + @"\08-JUN-2015.xlsx");
    } 

    [TestMethod]
    public void TestExcelReadValidFile()
    {
      DateTime _date_report = new DateTime(2015, 06, 8);
      DailyReportExcel _excel_conververt = new DailyReportExcel(_valid_file, _date_report, GameType.Bingo);
      List<DailyItem> items = _excel_conververt.CustomerDailyItems;

      Assert.IsTrue(items.Count > 0, "The GetBingoReportFromExcel method didn't return records from the Excel file");
    }

    [TestMethod]
    [ExpectedException(typeof(ExcelFileException))]
    public void TestExcelExceptionByDate()
    {
      DateTime _date_report = new DateTime(2000, 01, 01);
      DailyReportExcel _excel_conververt = new DailyReportExcel(_valid_file, _date_report, GameType.Bingo);
      List<DailyItem> items = _excel_conververt.CustomerDailyItems;
    }
  }
}
