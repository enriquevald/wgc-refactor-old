﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Language;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class frm_connection : frm_base
  {
    #region Attributes
    #endregion

    #region Constructor

    public frm_connection()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties
    #endregion

    #region Public Methods

    /// <summary>
    /// Set the value for the current progress bar
    /// </summary>
    /// <param name="Value"></param>
    public void SetValueProgress(int Value)
    {
      Helper.SetValueProgressBar(Value, progress_bar_main);
      this.Refresh();
    }

    #endregion

    #region Private Methods

    private void InitValues()
    {
      Helper.WriteTextInControl(lbl_msg_connecting, LanguageManager.GetValue("NLS_PSA_MESSAGE_CONNECTING_WITH_PSA"));
      Helper.WriteTextInControl(this, LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_PROCESS"));
    }

    #endregion

    #region Events

    private void frm_connection_Load(object sender, EventArgs e)
    {
      InitValues();
    }

    #endregion

  }
}
