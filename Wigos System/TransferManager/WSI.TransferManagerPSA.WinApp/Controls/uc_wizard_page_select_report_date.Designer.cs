﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class uc_wizard_page_select_report_date
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_report_date_selected = new System.Windows.Forms.Label();
      this.dtp_report_date = new System.Windows.Forms.DateTimePicker();
      this.lbl_title_report_date = new System.Windows.Forms.Label();
      this.panel_main = new System.Windows.Forms.Panel();
      this.pnlMonthly = new System.Windows.Forms.Panel();
      this.lbl_site_id = new System.Windows.Forms.Label();
      this.lbl_title_sala = new System.Windows.Forms.Label();
      this.cbo_sites = new System.Windows.Forms.ComboBox();
      this.lbl_title_report_month = new System.Windows.Forms.Label();
      this.dtp_report_month_year = new System.Windows.Forms.DateTimePicker();
      this.pnlDaily = new System.Windows.Forms.Panel();
      this.panel_main.SuspendLayout();
      this.pnlMonthly.SuspendLayout();
      this.pnlDaily.SuspendLayout();
      this.SuspendLayout();
      // 
      // lbl_report_date_selected
      // 
      this.lbl_report_date_selected.AutoSize = true;
      this.lbl_report_date_selected.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_report_date_selected.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_report_date_selected.Location = new System.Drawing.Point(126, 27);
      this.lbl_report_date_selected.MaximumSize = new System.Drawing.Size(291, 0);
      this.lbl_report_date_selected.Name = "lbl_report_date_selected";
      this.lbl_report_date_selected.Size = new System.Drawing.Size(175, 14);
      this.lbl_report_date_selected.TabIndex = 8;
      this.lbl_report_date_selected.Text = "lbl_report_date_selected";
      // 
      // dtp_report_date
      // 
      this.dtp_report_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.dtp_report_date.Location = new System.Drawing.Point(6, 24);
      this.dtp_report_date.Name = "dtp_report_date";
      this.dtp_report_date.Size = new System.Drawing.Size(106, 20);
      this.dtp_report_date.TabIndex = 7;
      this.dtp_report_date.ValueChanged += new System.EventHandler(this.dtp_report_date_ValueChanged);
      // 
      // lbl_title_report_date
      // 
      this.lbl_title_report_date.AutoSize = true;
      this.lbl_title_report_date.Location = new System.Drawing.Point(3, 5);
      this.lbl_title_report_date.Name = "lbl_title_report_date";
      this.lbl_title_report_date.Size = new System.Drawing.Size(280, 13);
      this.lbl_title_report_date.TabIndex = 6;
      this.lbl_title_report_date.Text = "Seleccione la fecha de reporte que desea informar al PSA";
      // 
      // panel_main
      // 
      this.panel_main.Controls.Add(this.pnlMonthly);
      this.panel_main.Controls.Add(this.pnlDaily);
      this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel_main.Location = new System.Drawing.Point(0, 0);
      this.panel_main.Name = "panel_main";
      this.panel_main.Size = new System.Drawing.Size(409, 209);
      this.panel_main.TabIndex = 9;
      // 
      // pnlMonthly
      // 
      this.pnlMonthly.Controls.Add(this.lbl_site_id);
      this.pnlMonthly.Controls.Add(this.lbl_title_sala);
      this.pnlMonthly.Controls.Add(this.cbo_sites);
      this.pnlMonthly.Controls.Add(this.lbl_title_report_month);
      this.pnlMonthly.Controls.Add(this.dtp_report_month_year);
      this.pnlMonthly.Location = new System.Drawing.Point(15, 86);
      this.pnlMonthly.Name = "pnlMonthly";
      this.pnlMonthly.Size = new System.Drawing.Size(385, 120);
      this.pnlMonthly.TabIndex = 10;
      this.pnlMonthly.Visible = false;
      // 
      // lbl_site_id
      // 
      this.lbl_site_id.AutoSize = true;
      this.lbl_site_id.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_site_id.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_site_id.Location = new System.Drawing.Point(240, 86);
      this.lbl_site_id.MaximumSize = new System.Drawing.Size(291, 0);
      this.lbl_site_id.Name = "lbl_site_id";
      this.lbl_site_id.Size = new System.Drawing.Size(84, 14);
      this.lbl_site_id.TabIndex = 11;
      this.lbl_site_id.Text = "lbl_site_id";
      // 
      // lbl_title_sala
      // 
      this.lbl_title_sala.AutoSize = true;
      this.lbl_title_sala.Location = new System.Drawing.Point(8, 65);
      this.lbl_title_sala.Name = "lbl_title_sala";
      this.lbl_title_sala.Size = new System.Drawing.Size(221, 13);
      this.lbl_title_sala.TabIndex = 10;
      this.lbl_title_sala.Text = "Seleccione la sala que desea informar al PSA";
      // 
      // cbo_sites
      // 
      this.cbo_sites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbo_sites.FormattingEnabled = true;
      this.cbo_sites.Location = new System.Drawing.Point(8, 83);
      this.cbo_sites.Name = "cbo_sites";
      this.cbo_sites.Size = new System.Drawing.Size(218, 21);
      this.cbo_sites.TabIndex = 1;
      this.cbo_sites.SelectedIndexChanged += new System.EventHandler(this.cbo_sites_SelectedIndexChanged);
      // 
      // lbl_title_report_month
      // 
      this.lbl_title_report_month.AutoSize = true;
      this.lbl_title_report_month.Location = new System.Drawing.Point(8, 5);
      this.lbl_title_report_month.Name = "lbl_title_report_month";
      this.lbl_title_report_month.Size = new System.Drawing.Size(221, 13);
      this.lbl_title_report_month.TabIndex = 6;
      this.lbl_title_report_month.Text = "Seleccione el mes que desea informar al PSA";
      // 
      // dtp_report_month_year
      // 
      this.dtp_report_month_year.CustomFormat = "MMMM yyyy";
      this.dtp_report_month_year.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dtp_report_month_year.Location = new System.Drawing.Point(8, 23);
      this.dtp_report_month_year.Name = "dtp_report_month_year";
      this.dtp_report_month_year.ShowUpDown = true;
      this.dtp_report_month_year.Size = new System.Drawing.Size(124, 20);
      this.dtp_report_month_year.TabIndex = 0;
      this.dtp_report_month_year.ValueChanged += new System.EventHandler(this.dtp_report_month_ValueChanged);
      // 
      // pnlDaily
      // 
      this.pnlDaily.Controls.Add(this.lbl_title_report_date);
      this.pnlDaily.Controls.Add(this.dtp_report_date);
      this.pnlDaily.Controls.Add(this.lbl_report_date_selected);
      this.pnlDaily.Location = new System.Drawing.Point(15, 12);
      this.pnlDaily.Name = "pnlDaily";
      this.pnlDaily.Size = new System.Drawing.Size(385, 51);
      this.pnlDaily.TabIndex = 9;
      // 
      // uc_wizard_page_select_report_date
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_main);
      this.Name = "uc_wizard_page_select_report_date";
      this.Size = new System.Drawing.Size(409, 209);
      this.Load += new System.EventHandler(this.uc_wizard_page_date_report_Load);
      this.panel_main.ResumeLayout(false);
      this.pnlMonthly.ResumeLayout(false);
      this.pnlMonthly.PerformLayout();
      this.pnlDaily.ResumeLayout(false);
      this.pnlDaily.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_report_date_selected;
    private System.Windows.Forms.DateTimePicker dtp_report_date;
    private System.Windows.Forms.Label lbl_title_report_date;
    private System.Windows.Forms.Panel panel_main;
    private System.Windows.Forms.Panel pnlDaily;
    private System.Windows.Forms.Panel pnlMonthly;
    private System.Windows.Forms.ComboBox cbo_sites;
    private System.Windows.Forms.Label lbl_title_report_month;
    private System.Windows.Forms.DateTimePicker dtp_report_month_year;
    private System.Windows.Forms.Label lbl_title_sala;
    private System.Windows.Forms.Label lbl_site_id;

  }
}
