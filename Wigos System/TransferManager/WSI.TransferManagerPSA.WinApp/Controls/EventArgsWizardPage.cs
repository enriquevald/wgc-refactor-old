﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EventArgsWizardPage.cs
// 
//   DESCRIPTION: EventArgs class to manage the current step in the Wizard
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.WinApp.Controls
{

  public class EventArgsWizardPage: EventArgs
  {
    #region Properties

    /// <summary>
    /// Operation step
    /// </summary>
    public WizardStep OperationStep { get; set; }

    #endregion
  }

}
