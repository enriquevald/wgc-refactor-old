﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class uc_search_file
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_search_file = new System.Windows.Forms.Button();
      this.txt_path_file = new System.Windows.Forms.TextBox();
      this.ofd_file_dialog = new System.Windows.Forms.OpenFileDialog();
      this.SuspendLayout();
      // 
      // btn_search_file
      // 
      this.btn_search_file.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_search_file.Location = new System.Drawing.Point(423, -1);
      this.btn_search_file.Name = "btn_search_file";
      this.btn_search_file.Size = new System.Drawing.Size(71, 24);
      this.btn_search_file.TabIndex = 6;
      this.btn_search_file.Text = "Archivo...";
      this.btn_search_file.UseVisualStyleBackColor = true;
      this.btn_search_file.Click += new System.EventHandler(this.btn_select_excel_file_Click);
      // 
      // txt_path_file
      // 
      this.txt_path_file.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txt_path_file.Location = new System.Drawing.Point(0, 1);
      this.txt_path_file.Name = "txt_path_file";
      this.txt_path_file.ReadOnly = true;
      this.txt_path_file.Size = new System.Drawing.Size(414, 20);
      this.txt_path_file.TabIndex = 5;
      // 
      // ofd_file_dialog
      // 
      this.ofd_file_dialog.FileName = "openFileDialog1";
      // 
      // uc_search_file
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btn_search_file);
      this.Controls.Add(this.txt_path_file);
      this.Name = "uc_search_file";
      this.Size = new System.Drawing.Size(510, 25);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_search_file;
    private System.Windows.Forms.TextBox txt_path_file;
    private System.Windows.Forms.OpenFileDialog ofd_file_dialog;
  }
}
