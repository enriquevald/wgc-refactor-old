﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_wizard_page_date_report.cs
// 
//   DESCRIPTION: User control to use like a wizard page to select a report date
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Common;
using System.Xml;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class uc_wizard_page_select_report_date : UserControl
  {
    #region Attributes
    public event EventHandler<DateTime> DateSelectedEvent;

    private ReportedFileType m_reported_file_type;
    #endregion

    #region Constructor

    public uc_wizard_page_select_report_date()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Get the selected date
    /// </summary>
    public DateTime SelectedDate
    {
      get
      {
        if (m_reported_file_type == Common.ReportedFileType.Daily_Detail)
          return dtp_report_date.Value;
        else if (m_reported_file_type == Common.ReportedFileType.Monthly_Summary)
          return dtp_report_month_year.Value;
        else
          throw new Exception("The ReportedFileType is not valid");
      }
    }

    /// <summary>
    /// Get the selected Site Id
    /// </summary>
    public Int32 SelectedSiteId
    {
      get
      {
        Int32 _code = 0;

        this.Invoke((MethodInvoker)delegate()
        {
          if (cbo_sites.SelectedItem != null)
            _code = int.Parse(((KeyValuePair<Int32, String>)cbo_sites.SelectedItem).Key.ToString());
        });

        return _code;
      }
    }

    public ReportedFileType ReportedFileType
    {
      get
      {
        return m_reported_file_type;
      }
      set
      {
        m_reported_file_type = value;

        if (m_reported_file_type == Common.ReportedFileType.Daily_Detail)
        {
          pnlDaily.Visible = true;
          pnlMonthly.Visible = false;
        }
        else if (m_reported_file_type == Common.ReportedFileType.Monthly_Summary)
        {
          pnlMonthly.Visible = true;
          pnlDaily.Visible = false;
          pnlMonthly.Location = pnlDaily.Location;

        }
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Set the date in the control using the current value in the config file
    /// </summary>
    public void LoadDefaultDate()
    {
      if (m_reported_file_type == Common.ReportedFileType.Daily_Detail)
      {

        if (AppConfiguration.DateLastReport == null)
        {
          // Default value for the first time when the value is null
          dtp_report_date.Value = DateTime.Now.AddDays(-1);
        }
        else
        {
          // Default value is always the next date of the last report successfully sent to PSA
          dtp_report_date.Value = AppConfiguration.DateLastReport.Value.AddDays(1);
        }
      }
      else if (m_reported_file_type == Common.ReportedFileType.Monthly_Summary)
      {
        if (AppConfiguration.DateLastReport == null)
        {
          // Default value for the first time when the value is null
          dtp_report_month_year.Value = DateTime.Now.AddMonths(-1);
        }
        else
        {
          // Default value is always the next month of the last report successfully sent to PSA
          dtp_report_month_year.Value = AppConfiguration.DateLastReport.Value.AddMonths(1);
        }
      }
      else
      {
        throw new Exception("The ReportedFileType is not valid");
      }
    }

    /// <summary>
    /// Load 'Salas' comboBox using a xml file
    /// </summary>
    public void LoadSalas()
    {
      if (m_reported_file_type != Common.ReportedFileType.Monthly_Summary)
        return;

      SortedDictionary<Int32, String> _dictionary = new SortedDictionary<Int32, String>();
      XmlDocument doc = new XmlDocument();
      doc.Load("Salas.xml");

      foreach (XmlNode node in doc.DocumentElement.ChildNodes)
      {
        _dictionary.Add(int.Parse(node.Attributes["Code"].InnerText), node.Attributes["Name"].InnerText);
      }

      cbo_sites.DataSource = new BindingSource(_dictionary, null);
      cbo_sites.DisplayMember = "Value";
      cbo_sites.ValueMember = "Key";
    }

    #endregion

    #region Private Methods

    private void InitEnvironment()
    {
      try
      {
        lbl_title_report_date.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_DATE_REPORT_TITLE_DATE");
        lbl_title_report_month.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_DATE_REPORT_TITLE_MONTH");
        lbl_title_sala.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_DATE_REPORT_TITLE_SITE");
      }
      catch
      { }
    }

    #endregion

    #region Events

    private void uc_wizard_page_date_report_Load(object sender, EventArgs e)
    {
      InitEnvironment();
    }

    private void dtp_report_date_ValueChanged(object sender, EventArgs e)
    {
      lbl_report_date_selected.Text = dtp_report_date.Value.ToLongDateString();

      if (DateSelectedEvent != null)
      {
        DateSelectedEvent(this, Utils.GetDateWithoutTime(dtp_report_date.Value));
      }
    }

    private void dtp_report_month_ValueChanged(object sender, EventArgs e)
    {
      if (DateSelectedEvent != null)
      {
        DateSelectedEvent(this, Utils.GetDateWithoutTime(dtp_report_month_year.Value));
      }
    }

    private void cbo_sites_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cbo_sites.SelectedItem != null)
      {
        lbl_site_id.Text = LanguageManager.GetValue("NLS_MESSAGE_CODE") + ": " + ((KeyValuePair<Int32, String>)cbo_sites.SelectedItem).Key.ToString();
      }
    }

    #endregion

  }
}
