﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using WSI.TransferManagerPSA.Language;
using System.Globalization;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public partial class uc_wizard_page_sending_report : UserControl
  {

    #region Constructor

    public uc_wizard_page_sending_report()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Get the current text in the Log Window
    /// </summary>
    public string TextLog
    {
      get
      {
        return txt_operation_log.Text;
      }
    }

    #endregion

    #region Public Methods
    
    /// <summary>
    /// Set a value (1 to 100) to the progress bar in the control
    /// </summary>
    /// <param name="Value"></param>
    public void SetValueProgressBar(int Value)
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          SetValueProgressBar(Value);
        });
      }
      else
      {
        pb_operation_progress.Value = Value;
        pb_operation_progress.Refresh();
      }
    }

    /// <summary>
    /// Add a new line to the text box that shows the Log information
    /// </summary>
    /// <param name="Information"></param>
    public void AddToLog(string Information)
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          AddToLog(Information);
        });
      }
      else
      {
        string _date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture );
        txt_operation_log.AppendText(_date_time + " " + Information + "\r\n") ;
      }
    }

    /// <summary>
    /// Clear text log
    /// </summary>
    public void ClearLog()
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          ClearLog();
        });
      }
      else
      {
        txt_operation_log.Clear();
      }
    }

    #endregion

  }
}
