﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Excel;
using System.Globalization;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public partial class uc_wizard_page_excel_summary : UserControl
  {

    #region Attributes

    private DailyReportExcel m_daily_report_excel;

    private MonthlyReportExcelGroup m_monthly_report_excel_group;

    private const int MAX_LINE_FOR_SCROLL = 24;
    #endregion

    #region Constructor

    public uc_wizard_page_excel_summary()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    public string SelectedFile { get; set; }

    public DailyReportExcel DailyReportExcel
    {
      get
      {
        return m_daily_report_excel;
      }
      set
      {
        m_daily_report_excel = value;
      }
    }

    public MonthlyReportExcelGroup MonthlyReportExcelGroup
    {
      get
      {
        return m_monthly_report_excel_group;
      }
      set
      {
        m_monthly_report_excel_group = value;
      }
    }

    public string GetSummaryReport
    {
      get
      {
        return txt_information.Text;
      }
    }

    #endregion

    #region Public Methods

    public void BuildSummaryReport()
    {
      if (m_daily_report_excel != null)
        txt_information.Text = GetSummaryForDailyReport();
      else if (m_monthly_report_excel_group != null)
        txt_information.Text = GetSummaryForMonthlyReport();
    }

    #endregion

    #region Private Methods

    private string GetSummaryForDailyReport()
    {
      decimal _total = 0;
      StringBuilder _summary = new StringBuilder();

      if (m_daily_report_excel != null)
      {
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_SELECTED_FILE") + " " + this.SelectedFile);
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_REPORT_DATE") + " " + m_daily_report_excel.DateReport.ToShortDateString());
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_ITEMS_TOTAL") + " " + m_daily_report_excel.CustomerDailyItems.Count);

        _total = m_daily_report_excel.TotalAmountWin;
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_INPUT_TOTAL") + " " + Utils.DecimalToCurrencyString(_total, AppConfiguration.CultureInfo));

        _total = m_daily_report_excel.TotalAmountSold;
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_OUTPUT_TOTAL") + " " + Utils.DecimalToCurrencyString(_total, AppConfiguration.CultureInfo));

        _total = m_daily_report_excel.TotalAmountBalance;
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_BALANCE_TOTAL") + " " + Utils.DecimalToCurrencyString(_total, AppConfiguration.CultureInfo));

        _summary.AppendLine();
        _summary.AppendLine(LanguageManager.GetValue("NLS_MESSAGE_SESSION_DAY").ToUpper());
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_CUSTOMER_NUMBER") + " " + m_daily_report_excel.DaySession.TotalCustomers);
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_CARD_NUMBER") + " " + m_daily_report_excel.DaySession.TotalCards);
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_AMOUNT_SOLD") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.DaySession.TotalAmountSold, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_AMOUNT_WIN") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.DaySession.TotalAmountWin, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_BALANCE_DAY") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.DaySession.TotalAmountBalance, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_RFC") + " " + m_daily_report_excel.DaySession.TotalEvidences);
        _summary.AppendLine();
        _summary.AppendLine(LanguageManager.GetValue("NLS_MESSAGE_SESSION_NIGHT").ToUpper());
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_CUSTOMER_NUMBER") + " " + m_daily_report_excel.NightSession.TotalCustomers);
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_CARD_NUMBER") + " " + m_daily_report_excel.NightSession.TotalCards);
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_AMOUNT_SOLD") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.NightSession.TotalAmountSold, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_AMOUNT_WIN") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.NightSession.TotalAmountWin, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_BALANCE_NIGHT") + " " + Utils.DecimalToCurrencyString(m_daily_report_excel.NightSession.TotalAmountBalance, AppConfiguration.CultureInfo));
        _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_RFC") + " " + m_daily_report_excel.NightSession.TotalEvidences);
      }

      return _summary.ToString();
    }

    private string GetSummaryForMonthlyReport()
    {
      StringBuilder _summary = new StringBuilder();

      if (m_monthly_report_excel_group != null)
      {
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_SELECTED_FILE") + " " + this.SelectedFile);
        _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_MONTH_REPORTED") + ": " + m_monthly_report_excel_group.Month.ToString("00") 
                                                                                                + "/" + m_monthly_report_excel_group.Year.ToString("0000"));
        _summary.AppendLine(LanguageManager.GetValue("NLS_MESSAGE_SITE_CODE") + ": " + m_monthly_report_excel_group.SiteId.ToString());
        _summary.AppendLine(LanguageManager.GetValue("NLS_MESSAGE_SITE_NAME") + ": " + m_monthly_report_excel_group.SiteName);

        if (m_monthly_report_excel_group.BingoMonthlyReportExcelList.Count > 0)
        {
          _summary.AppendLine();
          _summary.AppendLine(LanguageManager.GetValue("NLS_TITLE_BINGO").ToUpper());
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_REPORT_LINE") + ": L005");
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_IN") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.BingoMonthlyReportExcelList.Sum(x => x.TotalAmountSold), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_REFUND") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.BingoMonthlyReportExcelList.Sum(x => x.TotalRefunds), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_WIN") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.BingoMonthlyReportExcelList.Sum(x => x.TotalAmountWin), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_NETO") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.BingoMonthlyReportExcelList.Sum(x => x.TotalAmountBalance), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_WORKING_DAY_TO_REPORT") + ": " + m_monthly_report_excel_group.BingoMonthlyReportExcelList.Count);
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TIME_REPORTED") + ": " + m_monthly_report_excel_group.GetTimeBingo);
        }

        if (m_monthly_report_excel_group.TablesMonthlyReportExcelList.Count > 0)
        {
          _summary.AppendLine();
          _summary.AppendLine(LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TABLES_TITEL_LIVE").ToUpper());
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_REPORT_LINE") + ": L006");
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_IN") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.TablesMonthlyReportExcelList.Sum(x => x.TotalAmountSold), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_REFUND") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.TablesMonthlyReportExcelList.Sum(x => x.TotalRefunds), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_WIN") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.TablesMonthlyReportExcelList.Sum(x => x.TotalAmountWin), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TOTAL_MONTH_NETO") + ": "
              + Utils.DecimalToCurrencyString(m_monthly_report_excel_group.TablesMonthlyReportExcelList.Sum(x => x.TotalAmountBalance), AppConfiguration.CultureInfo));
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_WORKING_DAY_TO_REPORT") + ": " + m_monthly_report_excel_group.TablesMonthlyReportExcelList.Count);
          _summary.AppendLine("   " + LanguageManager.GetValue("NLS_EXCEL_SUMMARY_TIME_REPORTED") + ": " + m_monthly_report_excel_group.GetTimeTables);
        }
      }

      return _summary.ToString();
    }

    #endregion

  }
}
