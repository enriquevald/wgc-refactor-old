﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class uc_wizard_page_select_excel_file
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel_main = new System.Windows.Forms.Panel();
      this.uc_search_file_excel = new WSI.TransferManagerPSA.WinApp.uc_search_file();
      this.lbl_title_select_excel_file = new System.Windows.Forms.Label();
      this.lbl_validation_file = new System.Windows.Forms.Label();
      this.pb_validation_file = new System.Windows.Forms.PictureBox();
      this.pnl_validation_file = new System.Windows.Forms.Panel();
      this.panel_main.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_validation_file)).BeginInit();
      this.pnl_validation_file.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel_main
      // 
      this.panel_main.Controls.Add(this.pnl_validation_file);
      this.panel_main.Controls.Add(this.uc_search_file_excel);
      this.panel_main.Controls.Add(this.lbl_title_select_excel_file);
      this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel_main.Location = new System.Drawing.Point(0, 0);
      this.panel_main.Name = "panel_main";
      this.panel_main.Size = new System.Drawing.Size(514, 161);
      this.panel_main.TabIndex = 10;
      // 
      // uc_search_file_excel
      // 
      this.uc_search_file_excel.FilterType = WSI.TransferManagerPSA.WinApp.uc_search_file.FileFilterType.None;
      this.uc_search_file_excel.Location = new System.Drawing.Point(21, 34);
      this.uc_search_file_excel.Name = "uc_search_file_excel";
      this.uc_search_file_excel.Size = new System.Drawing.Size(483, 25);
      this.uc_search_file_excel.TabIndex = 5;
      this.uc_search_file_excel.FileSelectedEvent += new System.EventHandler<WSI.TransferManagerPSA.WinApp.EventArgsFileSelected>(this.uc_search_file_excel_FileSelectedEvent);
      // 
      // lbl_title_select_excel_file
      // 
      this.lbl_title_select_excel_file.AutoSize = true;
      this.lbl_title_select_excel_file.Location = new System.Drawing.Point(18, 18);
      this.lbl_title_select_excel_file.Name = "lbl_title_select_excel_file";
      this.lbl_title_select_excel_file.Size = new System.Drawing.Size(273, 13);
      this.lbl_title_select_excel_file.TabIndex = 4;
      this.lbl_title_select_excel_file.Text = "Seleccione el archivo de Excel que desea enviar al PSA";
      // 
      // lbl_validation_file
      // 
      this.lbl_validation_file.AutoSize = true;
      this.lbl_validation_file.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_validation_file.Location = new System.Drawing.Point(45, 10);
      this.lbl_validation_file.Name = "lbl_validation_file";
      this.lbl_validation_file.Size = new System.Drawing.Size(220, 15);
      this.lbl_validation_file.TabIndex = 13;
      this.lbl_validation_file.Text = "El archivo seleccionado es valido";
      // 
      // pb_validation_file
      // 
      this.pb_validation_file.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.StatusAnnotations_Complete_and_ok_Medium;
      this.pb_validation_file.Location = new System.Drawing.Point(7, 5);
      this.pb_validation_file.Name = "pb_validation_file";
      this.pb_validation_file.Size = new System.Drawing.Size(32, 32);
      this.pb_validation_file.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pb_validation_file.TabIndex = 14;
      this.pb_validation_file.TabStop = false;
      // 
      // pnl_validation_file
      // 
      this.pnl_validation_file.Controls.Add(this.lbl_validation_file);
      this.pnl_validation_file.Controls.Add(this.pb_validation_file);
      this.pnl_validation_file.Location = new System.Drawing.Point(21, 65);
      this.pnl_validation_file.Name = "pnl_validation_file";
      this.pnl_validation_file.Size = new System.Drawing.Size(404, 42);
      this.pnl_validation_file.TabIndex = 15;
      this.pnl_validation_file.Visible = false;
      // 
      // uc_wizard_page_excel_file
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_main);
      this.Name = "uc_wizard_page_excel_file";
      this.Size = new System.Drawing.Size(514, 161);
      this.Load += new System.EventHandler(this.uc_wizard_page_excel_file_Load);
      this.panel_main.ResumeLayout(false);
      this.panel_main.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_validation_file)).EndInit();
      this.pnl_validation_file.ResumeLayout(false);
      this.pnl_validation_file.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel_main;
    private uc_search_file uc_search_file_excel;
    private System.Windows.Forms.Label lbl_title_select_excel_file;
    private System.Windows.Forms.PictureBox pb_validation_file;
    private System.Windows.Forms.Label lbl_validation_file;
    private System.Windows.Forms.Panel pnl_validation_file;
  }
}
