﻿namespace WSI.TransferManagerPSA.WinApp.Controls
{
  partial class uc_wizard_page_excel_summary
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel_main = new System.Windows.Forms.Panel();
      this.txt_information = new System.Windows.Forms.RichTextBox();
      this.panel_main.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel_main
      // 
      this.panel_main.Controls.Add(this.txt_information);
      this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel_main.Location = new System.Drawing.Point(0, 0);
      this.panel_main.Name = "panel_main";
      this.panel_main.Size = new System.Drawing.Size(507, 330);
      this.panel_main.TabIndex = 11;
      // 
      // txt_information
      // 
      this.txt_information.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txt_information.Location = new System.Drawing.Point(3, 3);
      this.txt_information.Name = "txt_information";
      this.txt_information.ReadOnly = true;
      this.txt_information.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.txt_information.Size = new System.Drawing.Size(501, 318);
      this.txt_information.TabIndex = 0;
      this.txt_information.Text = "";
      // 
      // uc_wizard_page_excel_summary
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_main);
      this.Name = "uc_wizard_page_excel_summary";
      this.Size = new System.Drawing.Size(507, 330);
      this.panel_main.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel_main;
    private System.Windows.Forms.RichTextBox txt_information;
  }
}
