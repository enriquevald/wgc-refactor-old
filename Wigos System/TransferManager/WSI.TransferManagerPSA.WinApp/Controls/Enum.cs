﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public enum WizardStep
  {
    /// <summary>
    /// Step to select the report date
    /// </summary>
    SelectReportDate = 0,

    /// <summary>
    /// Step to select the Excel file
    /// </summary>
    SelectExcelFile = 1,

    /// <summary>
    /// Step that shows the data summary of the Excel file
    /// </summary>
    ShowExcelSummary = 2,

    /// <summary>
    /// Step to connect to the service and send the Report to the PSA service
    /// </summary>
    SendingReportToPSA = 3,

    /// <summary>
    /// Last Step that shows the result of the operation
    /// </summary>
    ShowFinalResult = 4
  }

}
