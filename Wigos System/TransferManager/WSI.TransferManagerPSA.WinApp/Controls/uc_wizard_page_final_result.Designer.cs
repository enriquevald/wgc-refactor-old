﻿namespace WSI.TransferManagerPSA.WinApp.Controls
{
  partial class uc_wizard_page_final_result
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel_main = new System.Windows.Forms.Panel();
      this.txt_operation_log = new System.Windows.Forms.RichTextBox();
      this.pb_operation_summary = new System.Windows.Forms.PictureBox();
      this.lbl_operation_result = new System.Windows.Forms.Label();
      this.panel_main.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_operation_summary)).BeginInit();
      this.SuspendLayout();
      // 
      // panel_main
      // 
      this.panel_main.Controls.Add(this.txt_operation_log);
      this.panel_main.Controls.Add(this.pb_operation_summary);
      this.panel_main.Controls.Add(this.lbl_operation_result);
      this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel_main.Location = new System.Drawing.Point(0, 0);
      this.panel_main.Name = "panel_main";
      this.panel_main.Size = new System.Drawing.Size(502, 335);
      this.panel_main.TabIndex = 11;
      // 
      // txt_operation_log
      // 
      this.txt_operation_log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txt_operation_log.Location = new System.Drawing.Point(5, 41);
      this.txt_operation_log.Name = "txt_operation_log";
      this.txt_operation_log.ReadOnly = true;
      this.txt_operation_log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.txt_operation_log.Size = new System.Drawing.Size(494, 288);
      this.txt_operation_log.TabIndex = 13;
      this.txt_operation_log.Text = "";
      // 
      // pb_operation_summary
      // 
      this.pb_operation_summary.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.StatusAnnotations_Complete_and_ok;
      this.pb_operation_summary.Location = new System.Drawing.Point(5, 4);
      this.pb_operation_summary.Name = "pb_operation_summary";
      this.pb_operation_summary.Size = new System.Drawing.Size(32, 32);
      this.pb_operation_summary.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pb_operation_summary.TabIndex = 12;
      this.pb_operation_summary.TabStop = false;
      // 
      // lbl_operation_result
      // 
      this.lbl_operation_result.AutoSize = true;
      this.lbl_operation_result.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_operation_result.Location = new System.Drawing.Point(42, 10);
      this.lbl_operation_result.Name = "lbl_operation_result";
      this.lbl_operation_result.Size = new System.Drawing.Size(381, 21);
      this.lbl_operation_result.TabIndex = 9;
      this.lbl_operation_result.Text = "Los datos se han importado satisfactoriamente....";
      // 
      // uc_wizard_page_final_result
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_main);
      this.Name = "uc_wizard_page_final_result";
      this.Size = new System.Drawing.Size(502, 335);
      this.panel_main.ResumeLayout(false);
      this.panel_main.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_operation_summary)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel_main;
    private System.Windows.Forms.PictureBox pb_operation_summary;
    private System.Windows.Forms.Label lbl_operation_result;
    private System.Windows.Forms.RichTextBox txt_operation_log;
  }
}
