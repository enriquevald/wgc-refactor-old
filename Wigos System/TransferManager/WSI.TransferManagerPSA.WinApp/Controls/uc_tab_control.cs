﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public partial class uc_tab_control : TabControl
  {
    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 0x1328 && !DesignMode) // Hide tabs by trapping the TCM_ADJUSTRECT message
        m.Result = IntPtr.Zero;
      else
        base.WndProc(ref m);
    }
  }
}
