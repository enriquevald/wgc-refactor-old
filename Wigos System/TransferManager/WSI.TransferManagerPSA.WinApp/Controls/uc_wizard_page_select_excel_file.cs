﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_wizard_page_excel_file.cs
// 
//   DESCRIPTION: User control, wizard page to select the excel fil
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Model;
using System.Threading;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class uc_wizard_page_select_excel_file : UserControl
  {
     
    #region Attributes
    public event EventHandler<EventArgsFileSelected> FileSelectedEvent;

    private SelectedFile m_selected_file;

    #endregion

    #region Constructor

    public uc_wizard_page_select_excel_file()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Date report
    /// </summary>
    public DateTime DateReport { get; set; }

    /// <summary>
    /// Source selected file
    /// </summary>
    public SelectedFile SelectedFile
    {
      get
      {
        return m_selected_file;
      }
    }
    #endregion

    #region Public Methods

    /// <summary>
    /// Clear information about the selected file
    /// </summary>
    public void Clear()
    {
      m_selected_file = null;
      uc_search_file_excel.ClearSeletedFile();
      pnl_validation_file.Visible = false;
    }

    /// <summary>
    /// Shows message and icon to indicate that the file is valid
    /// </summary>
    public void ShowMessageValidFile()
    {
      pnl_validation_file.Visible = true;
      pb_validation_file.Image = WinApp.Properties.Resources.StatusAnnotations_Complete_and_ok_Medium;
      lbl_validation_file.Text = LanguageManager.GetValue("NLS_MESSAGE_VALID_FILE");
    }

    /// <summary>
    /// Shows message and icon to indicate that the file is not valid
    /// </summary>
    public void ShowMessageInvalidFile()
    {
      pnl_validation_file.Visible = true;
      pb_validation_file.Image = WinApp.Properties.Resources.StatusAnnotations_Critical_Medium;
      lbl_validation_file.Text = LanguageManager.GetValue("NLS_MESSAGE_INVALID_FILE");
    }

    /// <summary>
    /// Gets the default directory to search files saved in the config file
    /// </summary>
    public void LoadDefaultDirectory()
    {
      // Set directory path using the current value in the config file 
      if (!string.IsNullOrEmpty(AppConfiguration.DirectoryDefault))
        uc_search_file_excel.DefaultDirectory = AppConfiguration.DirectoryDefault;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initial values to the user control
    /// </summary>
    private void InitEnvironment()
    {
      try
      {
        lbl_title_select_excel_file.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_FILE_TITLE_FILE");

        uc_search_file_excel.SetTextButtonSearch = LanguageManager.GetValue("NLS_MESSAGE_FILE") + "...";
      }
      catch
      { }

      uc_search_file_excel.FilterType = uc_search_file.FileFilterType.Excel;
    }

    #endregion

    #region Events

    /// <summary>
    /// Event raised when the user control is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_wizard_page_excel_file_Load(object sender, EventArgs e)
    {
      InitEnvironment();
    }

    /// <summary>
    /// Event raised when the file is selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_search_file_excel_FileSelectedEvent(object sender, EventArgsFileSelected e)
    {
      if (FileSelectedEvent != null)
      {
        m_selected_file = e.SelectedFile;
        FileSelectedEvent(this, e);
      }
    }

    #endregion

  }
}
