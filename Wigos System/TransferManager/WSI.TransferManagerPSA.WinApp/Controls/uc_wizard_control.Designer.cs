﻿namespace WSI.TransferManagerPSA.WinApp.Controls
{
  partial class uc_wizard_control
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.uc_tab_control_main = new WSI.TransferManagerPSA.WinApp.Controls.uc_tab_control();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.uc_wizard_page_date_report1 = new WSI.TransferManagerPSA.WinApp.uc_wizard_page_select_report_date();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.uc_wizard_page_excel_file1 = new WSI.TransferManagerPSA.WinApp.uc_wizard_page_select_excel_file();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.uc_wizard_page_excel_file_summary = new WSI.TransferManagerPSA.WinApp.Controls.uc_wizard_page_excel_summary();
      this.tabPage4 = new System.Windows.Forms.TabPage();
      this.uc_wizard_page_sending_report1 = new WSI.TransferManagerPSA.WinApp.Controls.uc_wizard_page_sending_report();
      this.tabPage5 = new System.Windows.Forms.TabPage();
      this.uc_wizard_page_result_operation1 = new WSI.TransferManagerPSA.WinApp.Controls.uc_wizard_page_final_result();
      this.btn_back = new System.Windows.Forms.Button();
      this.btn_next = new System.Windows.Forms.Button();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.gb_main = new System.Windows.Forms.GroupBox();
      this.uc_tab_control_main.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.tabPage4.SuspendLayout();
      this.tabPage5.SuspendLayout();
      this.gb_main.SuspendLayout();
      this.SuspendLayout();
      // 
      // uc_tab_control_main
      // 
      this.uc_tab_control_main.Controls.Add(this.tabPage1);
      this.uc_tab_control_main.Controls.Add(this.tabPage2);
      this.uc_tab_control_main.Controls.Add(this.tabPage3);
      this.uc_tab_control_main.Controls.Add(this.tabPage4);
      this.uc_tab_control_main.Controls.Add(this.tabPage5);
      this.uc_tab_control_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.uc_tab_control_main.Location = new System.Drawing.Point(3, 16);
      this.uc_tab_control_main.Margin = new System.Windows.Forms.Padding(0);
      this.uc_tab_control_main.Name = "uc_tab_control_main";
      this.uc_tab_control_main.SelectedIndex = 0;
      this.uc_tab_control_main.Size = new System.Drawing.Size(525, 365);
      this.uc_tab_control_main.TabIndex = 2;
      this.uc_tab_control_main.SelectedIndexChanged += new System.EventHandler(this.uc_tab_control_main_SelectedIndexChanged);
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.uc_wizard_page_date_report1);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(517, 339);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "tabPage1";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // uc_wizard_page_date_report1
      // 
      this.uc_wizard_page_date_report1.Location = new System.Drawing.Point(6, 6);
      this.uc_wizard_page_date_report1.Name = "uc_wizard_page_date_report1";
      this.uc_wizard_page_date_report1.ReportedFileType = WSI.TransferManagerPSA.Common.ReportedFileType.Daily_Detail;
      this.uc_wizard_page_date_report1.Size = new System.Drawing.Size(424, 142);
      this.uc_wizard_page_date_report1.TabIndex = 0;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.uc_wizard_page_excel_file1);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(517, 339);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "tabPage2";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // uc_wizard_page_excel_file1
      // 
      this.uc_wizard_page_excel_file1.DateReport = new System.DateTime(((long)(0)));
      this.uc_wizard_page_excel_file1.Location = new System.Drawing.Point(6, 6);
      this.uc_wizard_page_excel_file1.Name = "uc_wizard_page_excel_file1";
      this.uc_wizard_page_excel_file1.Size = new System.Drawing.Size(501, 115);
      this.uc_wizard_page_excel_file1.TabIndex = 0;
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.uc_wizard_page_excel_file_summary);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Size = new System.Drawing.Size(517, 339);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "tabPage3";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // uc_wizard_page_excel_file_summary
      // 
      this.uc_wizard_page_excel_file_summary.DailyReportExcel = null;
      this.uc_wizard_page_excel_file_summary.Location = new System.Drawing.Point(6, 7);
      this.uc_wizard_page_excel_file_summary.MonthlyReportExcelGroup = null;
      this.uc_wizard_page_excel_file_summary.Name = "uc_wizard_page_excel_file_summary";
      this.uc_wizard_page_excel_file_summary.SelectedFile = null;
      this.uc_wizard_page_excel_file_summary.Size = new System.Drawing.Size(506, 333);
      this.uc_wizard_page_excel_file_summary.TabIndex = 0;
      // 
      // tabPage4
      // 
      this.tabPage4.Controls.Add(this.uc_wizard_page_sending_report1);
      this.tabPage4.Location = new System.Drawing.Point(4, 22);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Size = new System.Drawing.Size(517, 339);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "tabPage4";
      this.tabPage4.UseVisualStyleBackColor = true;
      // 
      // uc_wizard_page_sending_report1
      // 
      this.uc_wizard_page_sending_report1.Location = new System.Drawing.Point(3, 4);
      this.uc_wizard_page_sending_report1.Name = "uc_wizard_page_sending_report1";
      this.uc_wizard_page_sending_report1.Size = new System.Drawing.Size(511, 332);
      this.uc_wizard_page_sending_report1.TabIndex = 0;
      // 
      // tabPage5
      // 
      this.tabPage5.Controls.Add(this.uc_wizard_page_result_operation1);
      this.tabPage5.Location = new System.Drawing.Point(4, 22);
      this.tabPage5.Name = "tabPage5";
      this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage5.Size = new System.Drawing.Size(517, 339);
      this.tabPage5.TabIndex = 4;
      this.tabPage5.Text = "tabPage5";
      this.tabPage5.UseVisualStyleBackColor = true;
      // 
      // uc_wizard_page_result_operation1
      // 
      this.uc_wizard_page_result_operation1.Location = new System.Drawing.Point(3, 5);
      this.uc_wizard_page_result_operation1.Name = "uc_wizard_page_result_operation1";
      this.uc_wizard_page_result_operation1.Size = new System.Drawing.Size(508, 331);
      this.uc_wizard_page_result_operation1.TabIndex = 0;
      this.uc_wizard_page_result_operation1.TextLog = "\r\n";
      // 
      // btn_back
      // 
      this.btn_back.Location = new System.Drawing.Point(273, 388);
      this.btn_back.Name = "btn_back";
      this.btn_back.Size = new System.Drawing.Size(75, 30);
      this.btn_back.TabIndex = 6;
      this.btn_back.Text = "< Atrás";
      this.btn_back.UseVisualStyleBackColor = true;
      this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
      // 
      // btn_next
      // 
      this.btn_next.Location = new System.Drawing.Point(354, 388);
      this.btn_next.Name = "btn_next";
      this.btn_next.Size = new System.Drawing.Size(75, 30);
      this.btn_next.TabIndex = 5;
      this.btn_next.Text = "Siguiente >";
      this.btn_next.UseVisualStyleBackColor = true;
      this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
      // 
      // btn_cancel
      // 
      this.btn_cancel.Location = new System.Drawing.Point(449, 388);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(75, 30);
      this.btn_cancel.TabIndex = 4;
      this.btn_cancel.Text = "Cancelar";
      this.btn_cancel.UseVisualStyleBackColor = true;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // gb_main
      // 
      this.gb_main.Controls.Add(this.uc_tab_control_main);
      this.gb_main.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.gb_main.Location = new System.Drawing.Point(0, 0);
      this.gb_main.Name = "gb_main";
      this.gb_main.Size = new System.Drawing.Size(531, 384);
      this.gb_main.TabIndex = 7;
      this.gb_main.TabStop = false;
      // 
      // uc_wizard_control
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gb_main);
      this.Controls.Add(this.btn_back);
      this.Controls.Add(this.btn_next);
      this.Controls.Add(this.btn_cancel);
      this.Margin = new System.Windows.Forms.Padding(0);
      this.Name = "uc_wizard_control";
      this.Size = new System.Drawing.Size(531, 420);
      this.Load += new System.EventHandler(this.uc_wizard_control_Load);
      this.uc_tab_control_main.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tabPage2.ResumeLayout(false);
      this.tabPage3.ResumeLayout(false);
      this.tabPage4.ResumeLayout(false);
      this.tabPage5.ResumeLayout(false);
      this.gb_main.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_tab_control uc_tab_control_main;
    private System.Windows.Forms.TabPage tabPage1;
    private uc_wizard_page_select_report_date uc_wizard_page_date_report1;
    private System.Windows.Forms.TabPage tabPage2;
    private uc_wizard_page_select_excel_file uc_wizard_page_excel_file1;
    private System.Windows.Forms.TabPage tabPage3;
    private uc_wizard_page_excel_summary uc_wizard_page_excel_file_summary;
    private System.Windows.Forms.TabPage tabPage4;
    private uc_wizard_page_sending_report uc_wizard_page_sending_report1;
    private System.Windows.Forms.Button btn_back;
    private System.Windows.Forms.Button btn_next;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.GroupBox gb_main;
    private System.Windows.Forms.TabPage tabPage5;
    private uc_wizard_page_final_result uc_wizard_page_result_operation1;
  }
}
