﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_wizard_control.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 15-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUL-2015 FAV     First release.
// 31-MAY-2016 FAV     Fixed Bug 13425: Exception with a time with invalid format
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Model;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Language;
using System.Threading;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public partial class uc_wizard_control : UserControl
  {

    #region Attributes
    
    /// <summary>
    /// Event to close or finish the Wizard
    /// </summary>
    public event EventHandler<EventArgs> CloseWizard_Event;

    /// <summary>
    /// Event that indicates a change of page in the Wizard
    /// </summary>
    public event EventHandler<EventArgsWizardPage> WizardPageChanged_Event;

    /// <summary>
    /// Event that indicate that the Wizard is in the step that send the report to the PSA server
    /// </summary>
    public event EventHandler<EventArgs> StartSendReport_Event;

    /// <summary>
    /// Event that indicate that the Wizard is in the step before send the report
    /// </summary>
    public event EventHandler<EventArgsWizardPage> ReadyToStartOperation_Event;

    private SelectedFile m_selected_file;
    
    private DailyReportExcel m_daily_report_excel;
    private MonthlyReportExcelGroup m_monthly_report_excel_group;

    private bool m_task_finished= false;
    private bool m_is_valid_file = false;
    private ReportedFileType m_reported_file_type;

    #endregion

    #region Constructor

    public uc_wizard_control()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Set a value to the progress bar [1-100]
    /// </summary>
    public int SetValueProgressOperation
    {
      set
      {
        if (value < 0 || value > 100)
          throw new Exception("The value to the operation progress bar is invalid!");
        uc_wizard_page_sending_report1.SetValueProgressBar(value);
      }
    }

    /// <summary>
    /// Get selected Date
    /// </summary>
    public DateTime SelectedDate
    {
      get 
      {
        return uc_wizard_page_date_report1.SelectedDate;
      }
    }

    /// <summary>
    /// Get selected Site Id
    /// </summary>
    public Int32 SelectedSiteId
    {
      get
      {
        return uc_wizard_page_date_report1.SelectedSiteId;
      }
    }

    /// <summary>
    /// Get selected file
    /// </summary>
    public SelectedFile SelectedFile
    { 
      get
      {
        return m_selected_file;
      }
    }

    /// <summary>
    /// Object with valid information of the Customers report
    /// </summary>
    public DailyReportExcel DailyReportExcel
    {
      get
      {
        return m_daily_report_excel;
      }
    }

    public MonthlyReportExcelGroup MonthlyReportExcelGroup
    {
      get
      {
        return m_monthly_report_excel_group;
      }
    }

    public ReportedFileType ReportedFileType
    {

      get
      {
        return m_reported_file_type;
      }
      set
      {
        m_reported_file_type = value;
        InitializeValues();
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Clear the communication Window
    /// </summary>
    public void ClearCommunicationMessage()
    {
        uc_wizard_page_sending_report1.ClearLog();
    }

    /// <summary>
    /// Add a Log message to the communication Window
    /// </summary>
    public void  AddOperationMessage (string LogMessage)
    {
        uc_wizard_page_sending_report1.AddToLog(LogMessage);
    }

    /// <summary>
    /// Shows the final page that shows the result of the operation
    /// </summary>
    /// <param name="ReportSentOK"></param>
    public void ShowPageFinalResult(bool ReportSentOK)
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          ShowPageFinalResult(ReportSentOK);
        });
      }
      else
      {
        uc_tab_control_main.SelectedIndex = (int)WizardStep.ShowFinalResult;
        LoadWizardPage(WizardStep.ShowFinalResult);
        uc_wizard_page_result_operation1.SetResult(ReportSentOK);
        uc_wizard_page_result_operation1.TextLog = uc_wizard_page_sending_report1.TextLog;
      }
    }

    /// <summary>
    /// Shows the page to send the report to the PSA service (with a progress bar)
    /// </summary>
    public void ShowPageSendReport()
    {
      uc_tab_control_main.SelectedIndex = (int)WizardStep.SendingReportToPSA;
      LoadWizardPage(WizardStep.SendingReportToPSA);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initialize the values of the wizard to start a new send
    /// </summary>
    private void InitializeValues()
    {
      try
      {
        m_selected_file = null;
        m_daily_report_excel = null;
        m_monthly_report_excel_group = null;

        uc_wizard_page_excel_file1.Clear();
        m_task_finished = false;

        uc_wizard_page_date_report1.ReportedFileType = m_reported_file_type;

        // Load the default selected day for the first page
        uc_wizard_page_date_report1.LoadDefaultDate();

        uc_wizard_page_date_report1.LoadSalas();

        // Load the default directory to search Excel files
        uc_wizard_page_excel_file1.LoadDefaultDirectory();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        throw;
      }
    }

    /// <summary>
    /// Load each wizard page with the respective environment 
    /// </summary>
    /// <param name="WizardPage"></param>
    private void LoadWizardPage(WizardStep WizardPage)
    {
      switch (WizardPage)
      {
        case WizardStep.SelectReportDate:
          LoadWizardPage_SelectReportDate();
          break;
        case WizardStep.SelectExcelFile:
          LoadWizardPage_SelectExcelFile();
          break;
        case WizardStep.ShowExcelSummary:
          LoadWizardPage_ExcelSummary();
          break;
        case WizardStep.SendingReportToPSA:
          LoadWizardPage_SendingReport();
          break;
        case WizardStep.ShowFinalResult:
          LoadWizardPage_FinalResult();
          break;
      }
    }

    /// <summary>
    /// Checks if the selected file is valid for send to PSA web services
    /// </summary>
    /// <param name="SelectedFile"></param>
    /// <param name="ShowMessage">Parameter to specify if should show a message if the file is not valid</param>
    /// <returns></returns>
    private bool CheckSelectedFile(SelectedFile SelectedFile, bool ShowMessage)
    {
      try
      {
        Cursor.Current = Cursors.WaitCursor;

        if (SelectedFile == null)
          return false;

        if (m_reported_file_type == Common.ReportedFileType.Daily_Detail)
        {
          m_daily_report_excel = new DailyReportExcel(SelectedFile, uc_wizard_page_date_report1.SelectedDate, GameType.Bingo);
        }
        else if (m_reported_file_type == Common.ReportedFileType.Monthly_Summary)
        {
          m_monthly_report_excel_group = new MonthlyReportExcelGroup(SelectedFile, uc_wizard_page_date_report1.SelectedDate.Month, 
                                                       uc_wizard_page_date_report1.SelectedDate.Year, uc_wizard_page_date_report1.SelectedSiteId);
        }
        else
        {
          throw new Exception("Invalid report type");
        }

        uc_wizard_page_excel_file1.ShowMessageValidFile();
        Log.Message(string.Format( "The selected file ({0}) is valid", SelectedFile.FileName));
        return true;
      }
      catch (ExcelFileException ex)
      {
        uc_wizard_page_excel_file1.ShowMessageInvalidFile();

        if (ShowMessage)
          Helper.ShowCustomMessageBox(MessageBoxType.Error, ex.Message);

        Log.Warning(string.Format("The selected file ({0}) is not valid", SelectedFile.FileName));
        return false;
      }
      catch
      {
        throw;
      }
      finally
      {
        Cursor.Current = Cursors.Default;
      }
    }

    /// <summary>
    /// Change to the next page
    /// </summary>
    private void ChangeToNextPage()
    {
      WizardStep current_page = (WizardStep)uc_tab_control_main.SelectedIndex;

      switch (current_page)
      {
        case WizardStep.SelectReportDate:
          uc_tab_control_main.SelectedIndex = 1;
          LoadWizardPage_SelectExcelFile();
          break;
        case WizardStep.SelectExcelFile:
          uc_tab_control_main.SelectedIndex = 2;
          LoadWizardPage_ExcelSummary();
          break;
        case WizardStep.ShowExcelSummary:
          if (ReadyToStartOperation_Event != null)
          {
            EventArgsWizardPage e = new EventArgsWizardPage();
            e.OperationStep = current_page;
            ReadyToStartOperation_Event(this, e);
          }
          break;
        case WizardStep.SendingReportToPSA:
          break;
        case WizardStep.ShowFinalResult:
          break;
      }
    }

    /// <summary>
    /// Go back to the wizard page
    /// </summary>
    private void ChangeToBackPage()
    {
      if (uc_tab_control_main.SelectedIndex > 0)
        uc_tab_control_main.SelectedIndex--;

      LoadWizardPage((WizardStep)uc_tab_control_main.SelectedIndex);
    }

    /// <summary>
    /// Load wizard page to select the report date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LoadWizardPage_SelectReportDate()
    {
      LoadNLSButtonTextDefault();
      btn_back.Enabled = false;
      btn_next.Enabled = true;
      Log.Message("The wizard page 'Report date selected' has been loaded");
    }

    /// <summary>
    /// Load wizard page to select Excel file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LoadWizardPage_SelectExcelFile()
    {
      // Sets the selected date to the user control that is going to load the excel file
      uc_wizard_page_excel_file1.DateReport = uc_wizard_page_date_report1.SelectedDate;

      Log.Message (string.Format("The report date selected by the user is [{0}]", uc_wizard_page_date_report1.SelectedDate.ToShortDateString()));

      btn_back.Enabled = true;
      m_is_valid_file = false;

      if (m_selected_file != null)
      {
        //m_is_valid_file = CheckSelectedFile(m_selected_file, true);

        m_selected_file = null;
        uc_wizard_page_excel_file1.Clear();
      }

      btn_next.Enabled = m_is_valid_file;
      Log.Message("The wizard page 'Excel file selected' has been loaded");
    }

    /// <summary>C:\TFS\Wigos\Dev\Wigos System\TransferManager\WSI.TransferManagerPSA.WinApp\Controls\EventArgsWizardPage.cs
    ///  Load wizard page to shows summary of selected file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LoadWizardPage_ExcelSummary()
    {
      if (uc_wizard_page_excel_file1.SelectedFile.IsValidFilePath)
      {
        string _summary = string.Empty;

        if (Program.ReportedType == Common.ReportedFileType.Daily_Detail)
        { 
          uc_wizard_page_excel_file_summary.SelectedFile = uc_wizard_page_excel_file1.SelectedFile.FilePath;
          uc_wizard_page_excel_file_summary.DailyReportExcel = m_daily_report_excel;
          uc_wizard_page_excel_file_summary.MonthlyReportExcelGroup = null;
        }
        else if (Program.ReportedType == Common.ReportedFileType.Monthly_Summary)
        {
          uc_wizard_page_excel_file_summary.SelectedFile = uc_wizard_page_excel_file1.SelectedFile.FilePath;
          uc_wizard_page_excel_file_summary.MonthlyReportExcelGroup = m_monthly_report_excel_group;
          uc_wizard_page_excel_file_summary.DailyReportExcel = null;
        }

        uc_wizard_page_excel_file_summary.BuildSummaryReport();

        Log.Message(uc_wizard_page_excel_file_summary.GetSummaryReport);

        btn_back.Enabled = true;
        btn_next.Enabled = true;
        Log.Message("The wizard page 'Summary Excel file' has been loaded");
      }
    }

    /// <summary>
    /// Load wizard page to send the information the the PSA
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LoadWizardPage_SendingReport()
    {
      uc_wizard_page_sending_report1.ClearLog();

      if (StartSendReport_Event != null)
        StartSendReport_Event(this, null);

      btn_back.Enabled = false;
      btn_next.Enabled = false;
      Log.Message("The wizard page 'Sending report' has been loaded");
    }

    /// <summary>
    /// Load last wizard page to shows the result of the operation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LoadWizardPage_FinalResult()
    {
      LoadNLSButtonTextLastPage();
      m_task_finished = true;
      Log.Message("The wizard page 'Final result' has been loaded");
    }

    /// <summary>
    /// Sets the text to the controls
    /// </summary>
    private void LoadNLSButtonTextDefault()
    {
      btn_next.Text = LanguageManager.GetValue("NLS_CONTROL_TEXT_NEXT") + " >";
      btn_back.Text = "< " + LanguageManager.GetValue("NLS_CONTROL_TEXT_BACK");
      btn_cancel.Text = LanguageManager.GetValue("NLS_CONTROL_TEXT_CANCEL");
    }

    /// <summary>
    /// Set the environment when the operation was successfully
    /// </summary>
    private void LoadNLSButtonTextLastPage()
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          LoadNLSButtonTextLastPage();
        });
      }
      else
      {
        btn_cancel.Text = LanguageManager.GetValue("NLS_CONTROL_TEXT_EXIT");
        btn_back.Text = "<< " + LanguageManager.GetValue("NLS_CONTROL_TEXT_START");
        btn_back.Enabled = true;
      }
    }

    #endregion

    #region Events

    /// <summary>
    /// Load control
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_wizard_control_Load(object sender, EventArgs e)
    {
      InitializeValues();

      uc_wizard_page_excel_file1.FileSelectedEvent += wizard_page_excel_file_FileSelectedEvent;
      uc_wizard_page_date_report1.DateSelectedEvent += uc_wizard_page_date_report1_DateSelectedEvent;

      LoadNLSButtonTextDefault();

      // first wizard page by default
      LoadWizardPage(WizardStep.SelectReportDate);
    }

    /// <summary>
    /// Event raised when there is a change of page in the wizard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_tab_control_main_SelectedIndexChanged(object sender, EventArgs e)
    {
      // Raise event to indicate the current wizard page
      if (WizardPageChanged_Event != null)
      {
        EventArgsWizardPage ev = new EventArgsWizardPage();
        ev.OperationStep = (WizardStep)uc_tab_control_main.SelectedIndex;
        WizardPageChanged_Event(this, ev);
      }
    }

    /// <summary>
    /// Event raised when the user selects the excel file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void wizard_page_excel_file_FileSelectedEvent(object sender, EventArgsFileSelected e)
    {
      try
      {
      Log.Message(string.Format("The selected file by the user is [{0}]", e.SelectedFile.FilePath));

      if (e.SelectedFile.IsValidFilePath)
      {
        m_selected_file = e.SelectedFile;

        // Checks if the excel is valid to go to the next page
        m_is_valid_file = CheckSelectedFile(m_selected_file, true);
        btn_next.Enabled = m_is_valid_file;
      }
      else
      {
        Helper.ShowCustomMessageBox(Common.MessageBoxType.Warning, LanguageManager.GetValue("NLS_EXCEPTION_FILE_NOT_VALID"));
      }
    }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, ex.Message);
      }
    }

    /// <summary>
    /// The event is raised the the user select a date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void uc_wizard_page_date_report1_DateSelectedEvent(object sender, DateTime e)
    {
      // Clear to check again the file with the date selected
      m_is_valid_file = false;
    }

    /// <summary>
    /// Button Back
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_back_Click(object sender, EventArgs e)
    {
      try
      {
        if (m_task_finished)
        {
          // Go the the first page to start again 
          InitializeValues();

          uc_tab_control_main.SelectedIndex = 0;
          LoadWizardPage_SelectReportDate();
        }
        else
        {
          ChangeToBackPage();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, ex.Message);
      }
    }

    /// <summary>
    /// Button Next
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_next_Click(object sender, EventArgs e)
    {
      try
      {
        ChangeToNextPage();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, ex.Message);
      }
    }

    /// <summary>
    /// Button cancel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      if (CloseWizard_Event != null)
      {
        CloseWizard_Event(this, e);
      }
    }

    #endregion

  }
}
