﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_search_file.cs
// 
//   DESCRIPTION: User control to select a file
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class uc_search_file : UserControl
  {

    #region Attributes

    /// <summary>
    /// Event that is raised when a file is selected using the dialog box
    /// </summary>
    public event EventHandler<EventArgsFileSelected> FileSelectedEvent;

    /// <summary>
    /// File types to use like a Filter in the dialog box to find a file
    /// </summary>
    public enum FileFilterType
    {
      None,
      Excel,
      Word,
      Txt,
    }


    /// <summary>
    /// Type file that is displayed in the dialog box to search files
    /// </summary>
    private FileFilterType m_type_filer = FileFilterType.None;

    /// <summary>
    /// Default directory for the dialog box to search files
    /// </summary>
    private string m_default_directory = string.Empty;

    private SelectedFile m_selected_file;

    #endregion

    #region Constructor
    public uc_search_file()
    {
      InitializeComponent();
    }
    #endregion

    #region Properties


    /// <summary>
    ///  Selected file
    /// </summary>
    public SelectedFile SelectedFile
    {
      get
      {
        return m_selected_file;
      }
    }

    /// <summary>
    /// Default directory used to show files
    /// </summary>
    public string DefaultDirectory
    {
      set
      {
        m_default_directory = value;
      }
    }

    /// <summary>
    /// File filter used to select in the file dialog
    /// </summary>
    public FileFilterType FilterType
    {
      get
      {
        return m_type_filer;
      }
      set
      {
        m_type_filer = value;
      }
    }

    /// <summary>
    /// Set the text to the search button
    /// </summary>
    public string SetTextButtonSearch
    {
      set
      {
        btn_search_file.Text = value;
      }
    }

    /// <summary>
    /// Clear information about the selected file
    /// </summary>
    public void ClearSeletedFile()
    {

      txt_path_file.Text = string.Empty;
      m_selected_file = null;
    }

    #endregion

    #region Events

    private void btn_select_excel_file_Click(object sender, EventArgs e)
    {
      switch (m_type_filer)
      {
        case FileFilterType.Excel:
          ofd_file_dialog.Filter = LanguageManager.GetValue("NLS_MESSAGE_TYPE_DOCUMENT_EXCEL") + " (*.xlsx,*.xls)|*.xlsx;*.xls";
          break;
        case FileFilterType.Word:
          ofd_file_dialog.Filter = LanguageManager.GetValue("NLS_MESSAGE_TYPE_DOCUMENT_WORD") + " (*.docx,*.doc)|*.docx;*.doc";
          break;
        case FileFilterType.Txt:
          ofd_file_dialog.Filter = LanguageManager.GetValue("NLS_MESSAGE_TYPE_DOCUMENT_TEXT") + " (*.txt)|*.txt";
          break;
        default:
          throw new Exception(string.Format("The type file ({0}) used like a filter is invalid!", m_type_filer.ToString()));
      }

      ofd_file_dialog.InitialDirectory = m_default_directory;
      ofd_file_dialog.FileName = string.Empty;

      if (DialogResult.OK == ofd_file_dialog.ShowDialog())
      {
        txt_path_file.Text = ofd_file_dialog.FileName;

        EventArgsFileSelected _event_arg = new EventArgsFileSelected(txt_path_file.Text);
        m_selected_file = _event_arg.SelectedFile;
        m_default_directory = _event_arg.SelectedFile.Directory;

        // Raise event with information of selected file
        if (FileSelectedEvent != null)
          FileSelectedEvent(this, _event_arg);

      }
    }
    #endregion
  }
}
