﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Language;

namespace WSI.TransferManagerPSA.WinApp.Controls
{
  public partial class uc_wizard_page_final_result : UserControl
  {

    #region Constructor

    public uc_wizard_page_final_result()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Get the current text in the Log Window
    /// </summary>
    public string TextLog
    {
      get
      {
        return txt_operation_log.Text;
      }
      set
      {
        txt_operation_log.Text = value;

        // Move to last line
        txt_operation_log.SelectionStart = txt_operation_log.Text.Length;
        txt_operation_log.ScrollToCaret();
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Displays information about the result of the operation
    /// </summary>
    /// <param name="OperationOK"></param>
    public void SetResult(bool OperationOK)
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          SetResult(OperationOK);
        });
      }
      else
      {
        SetResultOperation(OperationOK);
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Shows the user control according to the result of the operation
    /// </summary>
    private void SetResultOperation(bool OperationOK)
    {
      if (OperationOK)
      {
        pb_operation_summary.Image = WinApp.Properties.Resources.StatusAnnotations_Complete_and_ok;
        lbl_operation_result.Text = LanguageManager.GetValue("NLS_MESSAGE_SEND_OPERTION_OK");
      }
      else
      {
        pb_operation_summary.Image = WinApp.Properties.Resources.StatusAnnotations_Critical;
        lbl_operation_result.Text = LanguageManager.GetValue("NLS_MESSAGE_SEND_OPERTION_KO");
      }
    }

    #endregion

  }
}
