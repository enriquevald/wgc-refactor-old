﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EventArgsFileSelected.cs
// 
//   DESCRIPTION: EventArgs class to manage the 'File Selected' event in the 'uc_search_file' user control
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.WinApp
{
  public class EventArgsFileSelected: EventArgs
  {

    #region Attributes

    private SelectedFile m_selected_file;

    #endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="FilePath">Full path of the file</param>
    public EventArgsFileSelected(string FilePath)
    {
      m_selected_file = new SelectedFile(FilePath);
    }

    #endregion

    #region Properties

    /// <summary>
    /// Get file object
    /// </summary>
    public SelectedFile SelectedFile
    {
      get
      {
        return m_selected_file;
      }
    }

    #endregion

  }
}
