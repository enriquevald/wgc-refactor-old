﻿namespace WSI.TransferManagerPSA.WinApp.Controls
{
  partial class uc_wizard_page_sending_report
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel_main = new System.Windows.Forms.Panel();
      this.txt_operation_log = new System.Windows.Forms.RichTextBox();
      this.pb_operation_progress = new System.Windows.Forms.ProgressBar();
      this.panel_main.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel_main
      // 
      this.panel_main.Controls.Add(this.txt_operation_log);
      this.panel_main.Controls.Add(this.pb_operation_progress);
      this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel_main.Location = new System.Drawing.Point(0, 0);
      this.panel_main.Name = "panel_main";
      this.panel_main.Size = new System.Drawing.Size(502, 331);
      this.panel_main.TabIndex = 10;
      // 
      // txt_operation_log
      // 
      this.txt_operation_log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txt_operation_log.Location = new System.Drawing.Point(5, 13);
      this.txt_operation_log.Name = "txt_operation_log";
      this.txt_operation_log.ReadOnly = true;
      this.txt_operation_log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.txt_operation_log.Size = new System.Drawing.Size(493, 283);
      this.txt_operation_log.TabIndex = 2;
      this.txt_operation_log.Text = "";
      // 
      // pb_operation_progress
      // 
      this.pb_operation_progress.Location = new System.Drawing.Point(5, 302);
      this.pb_operation_progress.Name = "pb_operation_progress";
      this.pb_operation_progress.Size = new System.Drawing.Size(493, 23);
      this.pb_operation_progress.TabIndex = 1;
      // 
      // uc_wizard_page_sending_report
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel_main);
      this.Name = "uc_wizard_page_sending_report";
      this.Size = new System.Drawing.Size(502, 331);
      this.panel_main.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel_main;
    private System.Windows.Forms.ProgressBar pb_operation_progress;
    private System.Windows.Forms.RichTextBox txt_operation_log;
  }
}
