﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_message_box.cs
// 
//   DESCRIPTION: Message form to show a custom window by message type
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.WinApp.Properties;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class frm_message_box : frm_base
  {
    #region Attributes
    private MessageBoxType m_message_box_type;
    private MessageBoxAnswer m_answer_type = MessageBoxAnswer.None;
    private string m_message;
    #endregion

    #region Enum
    public enum MessageBoxAnswer
    {
      None,
      OK,
      Yes,
      No,
    }
    #endregion

    #region Constructor
    public frm_message_box(MessageBoxType MessageBoxType, string Message)
    {
      InitializeComponent();

      m_message_box_type = MessageBoxType;
      m_message = Message;
    }
    #endregion

    #region Properties

    public MessageBoxAnswer AnswerMessageBox
    {
      get
      {
        return m_answer_type;
      }
    }

    #endregion

    #region Private Methods
    private void SetMessageBoxEnvironment()
    {
      switch (m_message_box_type)
      {
        case MessageBoxType.Error:
          pb_icon_message.Image = Resources.StatusAnnotations_Critical;
          btn_button1.Visible = false;
          btn_button2.Visible = true;
          btn_button2.Text = "Aceptar";
          break;
        case MessageBoxType.Information:
          pb_icon_message.Image = Resources.StatusAnnotations_Information;
          btn_button1.Visible = false;
          btn_button2.Visible = true;
          btn_button2.Text = "Aceptar";
          break;
        case MessageBoxType.Question:
          pb_icon_message.Image = Resources.StatusAnnotations_Help;
          btn_button1.Visible = true;
          btn_button1.Text = "Si";
          btn_button2.Visible = true;
          btn_button2.Text = "No";
          break;
        case MessageBoxType.Warning:
          pb_icon_message.Image = Resources.StatusAnnotations_Warning;
          btn_button1.Visible = false;
          btn_button2.Visible = true;
          btn_button2.Text = "Aceptar";
          break;
      }
    }

    private void SaveMessageInLog()
    {
      try
      {
        switch (m_message_box_type)
        {
          case MessageBoxType.Error:
            Log.Error(string.Format("[Message box] = '{0}'", m_message));
            break;
          case MessageBoxType.Warning:
            Log.Warning(string.Format("[Message box] = '{0}'", m_message));
            break;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }
    #endregion

    #region Events

    private void frm_message_box_Load(object sender, EventArgs e)
    {
      SetMessageBoxEnvironment();
      lbl__message_str.Text = m_message;
      SaveMessageInLog();
    }

    private void btn_button1_Click(object sender, EventArgs e)
    {
      switch (m_message_box_type)
      {
        case MessageBoxType.Question:
          m_answer_type = MessageBoxAnswer.Yes;
          break;
      }

      this.Close();
    }

    private void btn_button2_Click(object sender, EventArgs e)
    {
      switch (m_message_box_type)
      {
        case MessageBoxType.Error:
        case MessageBoxType.Information:
        case MessageBoxType.Warning:
          m_answer_type = MessageBoxAnswer.OK;
          break;
        case MessageBoxType.Question:
          m_answer_type = MessageBoxAnswer.No;
          break;
      }

      this.Close();
    }
    #endregion

  }
}
