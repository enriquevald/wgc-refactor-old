﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_login.cs
// 
//   DESCRIPTION: Login form
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;
using WSI.TransferManagerPSA.Operation;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class frm_login : frm_base
  {

    #region Constants

    private const int LOGIN_MAX_ATTEMPTS = 3;

    #endregion

    #region Attributes

    private bool m_valid_login = false;
    private bool m_closed_by_not_connection = false;
    private int _m_login_attempts = 0;
    private OperationServicePSA m_report_PSA;
    #endregion

    #region Constructor

    public frm_login()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    public string OperatorID
    {
      get
      {
        return txt_operator_id.Text.Trim();
      }
    }

    public string User
    {
      get
      {
        return txt_user.Text.Trim();
      }
    }

    public string Password
    {
      get
      {
        return txt_password.Text.Trim();
      }
    }

    public bool IsValidLogin
    {
      get
      {
        return m_valid_login;
      }
    }

    public bool IsClosedByNotConnection
    {
      get
      {
        return m_closed_by_not_connection;
      }
    }

    #endregion

    #region Private Methods

    private void ValidateLogin()
    {
      try
      {
        _m_login_attempts++;

        m_report_PSA.CheckConnection();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }
    
    /// <summary>
    /// Validates mandatories fields
    /// </summary>
    /// <returns></returns>
    private bool ValidateMandatoriesFields()
    {
      if (txt_operator_id.Text.Trim() == string.Empty)
      {
        Helper.ShowCustomMessageBox(Common.MessageBoxType.Warning, LanguageManager.GetValue("NLS_LOGIN_WARNING_OPERATOR"));
        txt_operator_id.Focus();
        return false;
      }
      if (txt_user.Text.Trim() == string.Empty)
      {
        Helper.ShowCustomMessageBox(Common.MessageBoxType.Warning, LanguageManager.GetValue("NLS_LOGIN_WARNING_LOGIN"));
        txt_user.Focus();
        return false;
      }
      if (txt_password.Text.Trim() == string.Empty)
      {
        Helper.ShowCustomMessageBox(Common.MessageBoxType.Warning, LanguageManager.GetValue("NLS_LOGIN_WARNING_PASSWORD"));
        txt_password.Focus();
        return false;
      }

      return true;
    }

    #endregion

    #region Events

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      Log.Message("The user has cancelled and closed the Login Window");
      this.Close();
    }

    private void btn_ok_Click(object sender, EventArgs e)
    {
      if (ValidateMandatoriesFields())
      {
        btn_cancel.Enabled = false;
        btn_ok.Enabled = false;
        progress_bar_checking.Visible = true;

        m_report_PSA.Configuration.IdOperador = txt_operator_id.Text;
        m_report_PSA.Configuration.Login = txt_user.Text;
        m_report_PSA.Configuration.Password = txt_password.Text;

        Utils.StartThread(ValidateLogin);
      }
    }

    private void frm_login_Load(object sender, EventArgs e)
    {
      PSAConfiguration _configuration = new PSAConfiguration();
      _configuration.LoadData();

      m_report_PSA = new OperationServicePSA(_configuration);
      m_report_PSA.PSAConnectionProcessEvent += m_report_PSA_ConnectionProcessEvent;
    }

    /// <summary>
    /// Set the environment and set values for the connection process
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void m_report_PSA_ConnectionProcessEvent(object sender, ConnectionEventArgs e)
    {
      try
      {
        if (e.ConnectionStep == ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTED
            || e.ConnectionStep == ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTED)
        {
          Helper.SetValueProgressBar(100, progress_bar_checking);
          Log.Message("The connection with the PSA service has been successfully. The Login is Valid.");
          m_valid_login = true;
          CloseLogin();
        }
        else if (e.ConnectionStep == ConnectionStep.CONNECTION_ERROR)
        {
          if (_m_login_attempts >= LOGIN_MAX_ATTEMPTS)
          {
            ShowMessageToCloseLogin();
            CloseLogin();
          }
          else
            ShowMessageErrorConnection();
        }
        else
        {
          switch (e.ConnectionStep)
          {
            case ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTING:
              Helper.SetValueProgressBar(15 * e.AttemptNumbers, progress_bar_checking);
              break;
            case ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTING:
              Helper.SetValueProgressBar(30 * e.AttemptNumbers, progress_bar_checking);
              break;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Show Message to the user that is unable to establish a connection with the PSA service
    /// </summary>
    private void ShowMessageErrorConnection()
    {
      if (this.InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          ShowMessageErrorConnection();
        });
      }
      else
      {
        progress_bar_checking.Visible = false;
        btn_cancel.Enabled = true;
        btn_ok.Enabled = true;
        Helper.ShowCustomMessageBox(MessageBoxType.Warning, LanguageManager.GetValue("NLS_LOGIN_WARNING_LOGIN_CONNECTION"));
      }
    }

    /// <summary>
    /// Show Message to the user that was unable to establish a connection and close the Login
    /// </summary>
    private void ShowMessageToCloseLogin()
    {
      StringBuilder _message = new StringBuilder();
      _message.AppendLine(LanguageManager.GetValue("NLS_LOGIN_ERROR_LOGIN_CONNECTION"));
      _message.AppendLine(LanguageManager.GetValue("NLS_EXCEPTION_MESSAGE_CLOSE_APPLICATION"));

      m_closed_by_not_connection = true;

      if (this.InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          ShowMessageToCloseLogin();
        });
      }
      else
      {
        Helper.ShowCustomMessageBox(MessageBoxType.Error, _message.ToString());
      }
    }

    /// <summary>
    /// Close the Login Window
    /// </summary>
    private void CloseLogin()
    {
      if (this.InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          CloseLogin();
        });
      }
      else
      {
        this.Close();
      } 
    }
    #endregion

  }
}
