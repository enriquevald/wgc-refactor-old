﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;
using WSI.TransferManagerPSA.Operation;
using WSI.TransferManagerPSA.WinApp.Controls;

namespace WSI.TransferManagerPSA.WinApp
{
  public partial class frm_main : frm_base
  {

    #region Attributes

    private bool m_closed_by_not_connection = false;
    private OperationServicePSA m_psa_client = null;
    private frm_connection _frm_connection = null;

    #endregion

    #region Constructor

    public frm_main()
    {
      InitializeComponent();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Checks if the form was closed by an error with the connection
    /// </summary>
    public bool IsClosedByNotConnection
    {
      get
      {
        return m_closed_by_not_connection;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Main function that send the report to the PSA service
    /// </summary>
    private void SendReportToPSAService()
    {
      try
      {
        bool _operation = false;

        Log.Message("The application is starting the task to send the information to the PSA Service");

        uc_wizard_control1.ClearCommunicationMessage();

        if (Program.ReportedType == ReportedFileType.Daily_Detail)
        {
          _operation = m_psa_client.SendReport(uc_wizard_control1.DailyReportExcel);
        }
        else if (Program.ReportedType == ReportedFileType.Monthly_Summary)
        {
          _operation = m_psa_client.SendReport(uc_wizard_control1.MonthlyReportExcelGroup);
        }
        else
        {
          throw new Exception("El tipo de reporte para la aplicacion no es valida o no esta implementada");
        }

        if (_operation)
        {
          // Update the default values when the report was sent successfully
          AppConfiguration.DateLastReport = uc_wizard_control1.SelectedDate;
          AppConfiguration.DirectoryDefault = uc_wizard_control1.SelectedFile.Directory;
        }

        uc_wizard_control1.ShowPageFinalResult(_operation);

        if (_operation)
          Log.Message("Report sent successfully");
        else
          Log.Error("The report could not be sent");
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        uc_wizard_control1.ShowPageFinalResult(false);
        Log.Error("The report could not be sent");
      }
    }

    /// <summary>
    /// Initial values for this form
    /// </summary>
    private void InitValues()
    {
      uc_wizard_control1.ReportedFileType = Program.ReportedType;

      uc_wizard_control1.CloseWizard_Event += OnCloseWizard_Event;
      uc_wizard_control1.StartSendReport_Event += OnStartCommunication_Event;
      uc_wizard_control1.WizardPageChanged_Event += OnWizardPageChanged_Event;
      uc_wizard_control1.ReadyToStartOperation_Event += OnReadyToStartOperation;

      PSAConfiguration _configuration = new PSAConfiguration();
      _configuration.LoadData();

      m_psa_client = new OperationServicePSA(_configuration);
      m_psa_client.PSAOperationEvent += OnPSAOperationEvent;
      m_psa_client.PSAConnectionProcessEvent += OnConnectionProcessEvent;

      // Default page title
      lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_REPORT_DATE");
    }
    
    /// <summary>
    /// Write the message received in the "Log text box" about the PSA communication 
    /// </summary>
    /// <param name="PSAMessage"></param>
    private void AddMessageFromPSAToLogWindow(string PSAMessage)
    {
      if (InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          AddMessageFromPSAToLogWindow(PSAMessage);
        });
      }
      else
      {
        uc_wizard_control1.AddOperationMessage(PSAMessage);
      }
    }

    /// <summary>
    /// Checks the connection with the PSA Service
    /// </summary>
    private void CheckPSAServiceConnection()
    {
      try
      {
        _frm_connection = new frm_connection();
        m_psa_client.CheckConnection();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Checks if the report that the user tries to send already exists in the PSA server
    /// </summary>
    /// <returns></returns>
    private bool CheckIfReportExistsInThePSAService()
    {
      bool _exists_in_psa = false;

      if (Program.ReportedType == ReportedFileType.Daily_Detail)
      {
        if (!m_psa_client.CheckReportSent(uc_wizard_control1.SelectedDate, out _exists_in_psa))
          CloseApplicationByError();
      }
      else if (Program.ReportedType == ReportedFileType.Monthly_Summary)
      {
        if (!m_psa_client.CheckReportSent(uc_wizard_control1.SelectedDate, out _exists_in_psa, uc_wizard_control1.SelectedSiteId.ToString()))
          CloseApplicationByError();
      }


      return _exists_in_psa;
    }

    /// <summary>
    /// Close the application by a communication error with the PSA service
    /// </summary>
    private void CloseApplicationByError()
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.Invoke((MethodInvoker)delegate
          {
            CloseApplicationByError();
          });
        }
        else
        {
          StringBuilder _message = new StringBuilder();
          Log.Error(string.Format("It was impossible start a connection with the PSA web service."));
          _message.AppendLine(LanguageManager.GetValue("NLS_LOGIN_ERROR_LOGIN_CONNECTION"));
          _message.AppendLine(LanguageManager.GetValue("NLS_EXCEPTION_MESSAGE_CLOSE_APPLICATION"));
          Helper.ShowCustomMessageBox(MessageBoxType.Error, _message.ToString());

          m_closed_by_not_connection = true;
          this.Close();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Close the window wiht the progress bar that indicate the connection
    /// </summary>
    private void CloseWindowConnection()
    {
      _frm_connection.Close();
      _frm_connection.Dispose();
      _frm_connection = null;
    }

    /// <summary>
    /// Start the process to send the report
    /// </summary>
    private void StartSendReport()
    {
      try
      {
        bool _continue = true;

        if (CheckIfReportExistsInThePSAService())
        {
          frm_message_box _frm_request_resend = new frm_message_box(MessageBoxType.Question, LanguageManager.GetValue("NLS_MESSAGE_SEND_REPORT_AGAIN"));
          _frm_request_resend.TopMost = true;
          _frm_request_resend.ShowDialog();
          _continue = (_frm_request_resend.AnswerMessageBox == frm_message_box.MessageBoxAnswer.Yes);
        }

        if (_continue)
        {
          // Start the operation to send the Report to the PSA
          ShowPageSendReport();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Shows the page wizard to send the report
    /// </summary>
    private void ShowPageSendReport()
    {
      if (this.InvokeRequired)
      {
        this.Invoke((MethodInvoker)delegate
        {
          ShowPageSendReport();
        });
      }
      else
      {
        uc_wizard_control1.ShowPageSendReport();
      }
    }

    #endregion

    #region Events

    /// <summary>
    /// Event Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frm_main_Load(object sender, EventArgs e)
    {
      try
      {
        InitValues();

        label_version_app.Text = "Versión: " + Application.ProductVersion;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, ex.Message);

        m_closed_by_not_connection = true;
        this.Close();
      }
    }

    /// <summary>
    /// Event to close the Wizard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnCloseWizard_Event(object sender, EventArgs e)
    {
      try
      {
        this.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Event raise when the user try to close the main window
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (m_closed_by_not_connection == false)
        {
          frm_message_box _frm_question = new frm_message_box(MessageBoxType.Question, LanguageManager.GetValue("NLS_MESSAGE_CLOSE_APPLICATION"));
          _frm_question.ShowDialog();

          if (_frm_question.AnswerMessageBox == frm_message_box.MessageBoxAnswer.Yes)
            Log.Message("The user has closed the Main Window Wizard");
          else
            e.Cancel = true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Event raised when a page change is made in the Wizard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnWizardPageChanged_Event(object sender, EventArgsWizardPage e)
    {
      try
      {
        // Sets the title for each wizard page
        switch (e.OperationStep)
        {
          case WizardStep.SelectReportDate:
            lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_REPORT_DATE");
            break;
          case WizardStep.SelectExcelFile:
            lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_SELECT_FILE");
            break;
          case WizardStep.ShowExcelSummary:
            lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_EXCEL_SUMMARY");
            break;
          case WizardStep.SendingReportToPSA:
            lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_PROCESS") + "...";
            break;
          case WizardStep.ShowFinalResult:
            lbl_title_step_wizard.Text = LanguageManager.GetValue("NLS_WIZARD_PAGE_TITLE_RESULT");
            break;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// The wizard control is ready to start the communication with the PSA service
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnStartCommunication_Event(object sender, EventArgs e)
    {
      try
      {
        Utils.StartThread(SendReportToPSAService);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    private void OnPSAOperationEvent(object sender, OperationEventArgs e)
    {
      try
      {
        if (e.Type != OperationTypePSA.SET_VALUE_PROGRESS)
          AddMessageFromPSAToLogWindow(e.Message);

        if (Program.ReportedType == ReportedFileType.Daily_Detail)
        {
          switch (e.Type)
          {
            case OperationTypePSA.OPEN_SESSION:
              uc_wizard_control1.SetValueProgressOperation = 15;
              break;
            case OperationTypePSA.SEND_DATA_FASE1:
              uc_wizard_control1.SetValueProgressOperation = 30;
              break;
            case OperationTypePSA.SEND_DATA_FASE2:
              uc_wizard_control1.SetValueProgressOperation = 45;
              break;
            case OperationTypePSA.SEND_DATA_FASE3:
              uc_wizard_control1.SetValueProgressOperation = 60;
              break;
            case OperationTypePSA.SEND_DATA_FASE4:
              uc_wizard_control1.SetValueProgressOperation = 80;
              break;
            case OperationTypePSA.CLOSE_SESSION:
              uc_wizard_control1.SetValueProgressOperation = 100;
              break;
            case OperationTypePSA.ERROR:
              uc_wizard_control1.SetValueProgressOperation = 100;
              break;
          }
        }
        else if (Program.ReportedType == ReportedFileType.Monthly_Summary)
        {
          if (e.Type == OperationTypePSA.SET_VALUE_PROGRESS)
          {
            object[] _values = e.argMessage;
            uc_wizard_control1.SetValueProgressOperation = (int)(100 * (float.Parse(_values[0].ToString()) / float.Parse(_values[1].ToString())));

            if (_values[0].ToString() == _values[1].ToString())
              uc_wizard_control1.SetValueProgressOperation = 100;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Event raized when the wizard is ready to start the communication
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnReadyToStartOperation(object sender, EventArgsWizardPage e)
    {
      try
      {
        frm_message_box _frm_request_confirmation = new frm_message_box(MessageBoxType.Question,
                                                    LanguageManager.GetValue("NLS_MESSAGE_CONFIRMATION_SEND_INFORMATION"));
        _frm_request_confirmation.ShowDialog();

        if (_frm_request_confirmation.AnswerMessageBox == frm_message_box.MessageBoxAnswer.Yes)
        {
          Utils.StartThread(CheckPSAServiceConnection);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Events raised during a connection
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnConnectionProcessEvent(object sender, ConnectionEventArgs e)
    {
      try
      {
        if (e.ConnectionStep == ConnectionStep.CONNECTION_STARTING)
        {
          _frm_connection.Show();
        }
        if (e.ConnectionStep == ConnectionStep.CONNECTION_ERROR)
        {
          _frm_connection.SetValueProgress(100);
          CloseWindowConnection();
          Utils.StartThread(CloseApplicationByError);
        }
        else if (e.ConnectionStep == ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTED
                 || e.ConnectionStep == ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTED)
        {
          _frm_connection.SetValueProgress(100);
          CloseWindowConnection();
          Utils.StartThread(StartSendReport);
        }
        else
        {
          switch (e.ConnectionStep)
          {
            case ConnectionStep.CONNECTION_STARTING:
              _frm_connection.SetValueProgress(5);
              break;
            case ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTING:
              _frm_connection.SetValueProgress(15 * e.AttemptNumbers);
              break;
            case ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTING:
              _frm_connection.SetValueProgress(30 * e.AttemptNumbers);
              break;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    #endregion

  }
}
