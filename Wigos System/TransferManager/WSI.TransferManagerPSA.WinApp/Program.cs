﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Program.cs
// 
//   DESCRIPTION: Main class to start the application
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Language;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.WinApp
{
  static class Program
  {
    #region Attributes

    public static ReportedFileType ReportedType;

    #endregion

    #region Public Methods

    [STAThread]
    static void Main()
    {
      // Checks if there is not error and should continue
      bool _continue = false;

      // Check if is a valid login
      bool _valid_login = true;



      // *******************************************************************
      // Change to establish the document format that will use the program.
      // *******************************************************************
   
       ReportedType = Common.ReportedFileType.Monthly_Summary;

      // *******************************************************************
      // *******************************************************************



      try
      {
        Log.AddListener(new SimpleLogger("BRTM"));
        Log.Message("============ Starting ReportTransferManager =============");

        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault(false);

        GetLanguageDefinition();

        _continue = CheckValidAppConfiguration();

        if (_continue && IsFirstLoginInTheSystem())
        {
          _continue = ShowLoginWindow(out _valid_login);
        }

        if (_continue && _valid_login)
        {
          Log.Message("The Main Window Wizard is shown to the user");

          frm_main _main_form = new frm_main();
          Application.Run(_main_form);

          _continue = !_main_form.IsClosedByNotConnection;
          _main_form.Dispose();

        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, LanguageManager.GetValue("NLS_EXCEPTION_MAIN_APPLICATION"));
      }
      finally
      {
        if (!_continue)
          Log.Error("The application has been closed and it cannot to continue by an exception!."); 
        
        Log.Message("============ ReportTransferManager ended ============");
        Log.Close();
      }
    }

    #endregion
    
    #region Private Methods

    private static bool IsFirstLoginInTheSystem()
    {
      try
      {
        return UserApplication.IsFirstLoginInTheSystem();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, LanguageManager.GetValue("NLS_EXCEPTION_REGISTER_PERMISSION"));
        throw;
      }
    }

    private static bool ShowLoginWindow(out bool ValidLogin)
    {
      Log.Message("The Login Window is shown to the user");
      frm_login _frm_login = new frm_login();
      _frm_login.ShowDialog();
      ValidLogin = _frm_login.IsValidLogin;
      _frm_login.Dispose();

      if (ValidLogin)
        SaveUserInformationInWindowsRegister(_frm_login.OperatorID, _frm_login.User, _frm_login.Password);

      return !_frm_login.IsClosedByNotConnection;
    }

    private static void SaveUserInformationInWindowsRegister(string OperatorID, string User, string Password)
    {
      try
      {
        UserApplication.Update(OperatorID, User, Password);
        Log.Message("The login information was saved in the Windows registry");
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Helper.ShowCustomMessageBox(MessageBoxType.Error, LanguageManager.GetValue("NLS_EXCEPTION_REGISTER_PERMISSION"));
        throw;
      }
    }

    /// <summary>
    /// Gets the language configured in the application file
    /// </summary>
    private static void GetLanguageDefinition()
    {
      LanguageManager.Initialize();

      if (AppConfiguration.Language != LanguageType.None)
      {
        // The language for the system is taken of the config file
        LanguageManager.SetLanguageType = AppConfiguration.Language;

        // Configures language for the application (UI, messages and objects)
        CultureInfo culture = LanguageManager.CultureInfo;
        Thread.CurrentThread.CurrentCulture = culture;
        Thread.CurrentThread.CurrentUICulture = culture;
      }
      else
      {
        Log.Warning("It doesn't exist a language definition for the application!");
      }
    }

    /// <summary>
    /// Checks if exists a configuration file that is mandatory to the application
    /// </summary>
    /// <returns></returns>
    private static bool CheckValidAppConfiguration()
    {
      bool _invalid_config_file = false;
      string _max_value_result = string.Empty;

      try
      { _max_value_result = AppConfiguration.MaxValueResults.ToString();}
      catch {}

      _invalid_config_file = string.IsNullOrEmpty(AppConfiguration.URL_Principal_PSA.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.URL_Secundary_PSA.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.ClavesublineaNegocio.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.ClaveTipoPago.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.ClaveCombinacion.Trim())
                    || (AppConfiguration.Language == LanguageType.None)
                    || !Utils.IsNumeric(_max_value_result)
                    || string.IsNullOrEmpty(AppConfiguration.Version.Trim());

      if (Program.ReportedType == ReportedFileType.Daily_Detail)
      {
        _invalid_config_file = _invalid_config_file
                    || string.IsNullOrEmpty(AppConfiguration.ClaveEstablecimiento.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.WorksheetDay.Trim())
                    || string.IsNullOrEmpty(AppConfiguration.WorksheetNight.Trim());
      }


      if (_invalid_config_file)
      {
        StringBuilder _message = new StringBuilder();
        if (AppConfiguration.Language != LanguageType.None)
        {
          _message.AppendLine(LanguageManager.GetValue("NLS_EXCEPTION_MISSING_CONFIG_FILE"));
          _message.AppendLine(LanguageManager.GetValue("NLS_EXCEPTION_MESSAGE_CLOSE_APPLICATION"));
        }
        else
        {
          // There is not configuration about language. The application is going to show the message in spanish
          _message.AppendLine("El archivo de configuración no es valido.");
          _message.AppendLine("La aplicación no puede continuar y se va cerrar.");
        }

        Helper.ShowCustomMessageBox(MessageBoxType.Error, _message.ToString());

        return false;
      }

      return true;
    }

    #endregion

  }
}
