﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class frm_connection
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.progress_bar_main = new System.Windows.Forms.ProgressBar();
      this.lbl_msg_connecting = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // progress_bar_main
      // 
      this.progress_bar_main.Location = new System.Drawing.Point(12, 62);
      this.progress_bar_main.Name = "progress_bar_main";
      this.progress_bar_main.Size = new System.Drawing.Size(347, 23);
      this.progress_bar_main.TabIndex = 0;
      // 
      // lbl_msg_connecting
      // 
      this.lbl_msg_connecting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg_connecting.Location = new System.Drawing.Point(12, 23);
      this.lbl_msg_connecting.Name = "lbl_msg_connecting";
      this.lbl_msg_connecting.Size = new System.Drawing.Size(347, 20);
      this.lbl_msg_connecting.TabIndex = 1;
      this.lbl_msg_connecting.Text = "Conectando con el servicio PSA";
      this.lbl_msg_connecting.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // frm_connection
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(371, 93);
      this.Controls.Add(this.lbl_msg_connecting);
      this.Controls.Add(this.progress_bar_main);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_connection";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "";
      this.Load += new System.EventHandler(this.frm_connection_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ProgressBar progress_bar_main;
    private System.Windows.Forms.Label lbl_msg_connecting;
  }
}