﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class frm_message_box
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_button2 = new System.Windows.Forms.Button();
      this.gb_error = new System.Windows.Forms.GroupBox();
      this.pb_icon_message = new System.Windows.Forms.PictureBox();
      this.lbl__message_str = new System.Windows.Forms.Label();
      this.btn_button1 = new System.Windows.Forms.Button();
      this.gb_error.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_icon_message)).BeginInit();
      this.SuspendLayout();
      // 
      // btn_button2
      // 
      this.btn_button2.Location = new System.Drawing.Point(345, 165);
      this.btn_button2.Name = "btn_button2";
      this.btn_button2.Size = new System.Drawing.Size(94, 30);
      this.btn_button2.TabIndex = 1;
      this.btn_button2.Text = "Button2";
      this.btn_button2.UseVisualStyleBackColor = true;
      this.btn_button2.Click += new System.EventHandler(this.btn_button2_Click);
      // 
      // gb_error
      // 
      this.gb_error.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.gb_error.Controls.Add(this.pb_icon_message);
      this.gb_error.Controls.Add(this.lbl__message_str);
      this.gb_error.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.gb_error.Location = new System.Drawing.Point(12, 3);
      this.gb_error.Name = "gb_error";
      this.gb_error.Size = new System.Drawing.Size(427, 156);
      this.gb_error.TabIndex = 2;
      this.gb_error.TabStop = false;
      // 
      // pb_icon_message
      // 
      this.pb_icon_message.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.StatusAnnotations_Alert;
      this.pb_icon_message.Location = new System.Drawing.Point(7, 19);
      this.pb_icon_message.Name = "pb_icon_message";
      this.pb_icon_message.Size = new System.Drawing.Size(52, 52);
      this.pb_icon_message.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_icon_message.TabIndex = 2;
      this.pb_icon_message.TabStop = false;
      // 
      // lbl__message_str
      // 
      this.lbl__message_str.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl__message_str.Location = new System.Drawing.Point(65, 34);
      this.lbl__message_str.Name = "lbl__message_str";
      this.lbl__message_str.Size = new System.Drawing.Size(350, 104);
      this.lbl__message_str.TabIndex = 1;
      this.lbl__message_str.Text = "lbl_message_str";
      // 
      // btn_button1
      // 
      this.btn_button1.Location = new System.Drawing.Point(245, 165);
      this.btn_button1.Name = "btn_button1";
      this.btn_button1.Size = new System.Drawing.Size(94, 30);
      this.btn_button1.TabIndex = 3;
      this.btn_button1.Text = "Button1";
      this.btn_button1.UseVisualStyleBackColor = true;
      this.btn_button1.Click += new System.EventHandler(this.btn_button1_Click);
      // 
      // frm_message_box
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(451, 199);
      this.Controls.Add(this.btn_button1);
      this.Controls.Add(this.gb_error);
      this.Controls.Add(this.btn_button2);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_message_box";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "PSA Transfer Manager";
      this.Load += new System.EventHandler(this.frm_message_box_Load);
      this.gb_error.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pb_icon_message)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btn_button2;
    private System.Windows.Forms.GroupBox gb_error;
    private System.Windows.Forms.Label lbl__message_str;
    private System.Windows.Forms.PictureBox pb_icon_message;
    private System.Windows.Forms.Button btn_button1;

  }
}