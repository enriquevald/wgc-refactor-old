﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class frm_main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
      this.panel1 = new System.Windows.Forms.Panel();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label_version_app = new System.Windows.Forms.Label();
      this.uc_wizard_control1 = new WSI.TransferManagerPSA.WinApp.Controls.uc_wizard_control();
      this.lbl_title_step_wizard = new System.Windows.Forms.Label();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.pictureBox1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(234, 457);
      this.panel1.TabIndex = 0;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pictureBox1.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.Banner1;
      this.pictureBox1.Location = new System.Drawing.Point(0, 0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(234, 457);
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.label_version_app);
      this.panel2.Controls.Add(this.uc_wizard_control1);
      this.panel2.Controls.Add(this.lbl_title_step_wizard);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(234, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(561, 457);
      this.panel2.TabIndex = 1;
      // 
      // label_version_app
      // 
      this.label_version_app.ForeColor = System.Drawing.Color.Gray;
      this.label_version_app.Location = new System.Drawing.Point(14, 435);
      this.label_version_app.Name = "label_version_app";
      this.label_version_app.Size = new System.Drawing.Size(134, 13);
      this.label_version_app.TabIndex = 1;
      this.label_version_app.Text = "Versión: 1.0.0.0.0";
      // 
      // uc_wizard_control1
      // 
      this.uc_wizard_control1.Location = new System.Drawing.Point(17, 31);
      this.uc_wizard_control1.Margin = new System.Windows.Forms.Padding(0);
      this.uc_wizard_control1.Name = "uc_wizard_control1";
      this.uc_wizard_control1.ReportedFileType = WSI.TransferManagerPSA.Common.ReportedFileType.Daily_Detail;
      this.uc_wizard_control1.Size = new System.Drawing.Size(542, 423);
      this.uc_wizard_control1.TabIndex = 0;
      // 
      // lbl_title_step_wizard
      // 
      this.lbl_title_step_wizard.AutoSize = true;
      this.lbl_title_step_wizard.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_title_step_wizard.Location = new System.Drawing.Point(14, 13);
      this.lbl_title_step_wizard.Name = "lbl_title_step_wizard";
      this.lbl_title_step_wizard.Size = new System.Drawing.Size(160, 18);
      this.lbl_title_step_wizard.TabIndex = 4;
      this.lbl_title_step_wizard.Text = "lbl_title_step_wizard";
      // 
      // frm_main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(795, 457);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "frm_main";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "PSA Transfer Manager";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_main_FormClosing);
      this.Load += new System.EventHandler(this.frm_main_Load);
      this.panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label lbl_title_step_wizard;
    private Controls.uc_wizard_control uc_wizard_control1;
    private System.Windows.Forms.Label label_version_app;




  }
}