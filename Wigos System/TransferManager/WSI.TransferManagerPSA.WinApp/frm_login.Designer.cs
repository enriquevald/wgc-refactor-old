﻿namespace WSI.TransferManagerPSA.WinApp
{
  partial class frm_login
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lbl_operator_id = new System.Windows.Forms.Label();
      this.txt_operator_id = new System.Windows.Forms.TextBox();
      this.txt_user = new System.Windows.Forms.TextBox();
      this.lbl_user = new System.Windows.Forms.Label();
      this.txt_password = new System.Windows.Forms.TextBox();
      this.lbl_password = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pictureBox2 = new System.Windows.Forms.PictureBox();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.btn_ok = new System.Windows.Forms.Button();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.progress_bar_checking = new System.Windows.Forms.ProgressBar();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // lbl_operator_id
      // 
      this.lbl_operator_id.AutoSize = true;
      this.lbl_operator_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_operator_id.ForeColor = System.Drawing.Color.White;
      this.lbl_operator_id.Location = new System.Drawing.Point(70, 22);
      this.lbl_operator_id.Name = "lbl_operator_id";
      this.lbl_operator_id.Size = new System.Drawing.Size(89, 15);
      this.lbl_operator_id.TabIndex = 1;
      this.lbl_operator_id.Text = "ID Operador:";
      // 
      // txt_operator_id
      // 
      this.txt_operator_id.Location = new System.Drawing.Point(176, 22);
      this.txt_operator_id.MaxLength = 50;
      this.txt_operator_id.Name = "txt_operator_id";
      this.txt_operator_id.Size = new System.Drawing.Size(194, 20);
      this.txt_operator_id.TabIndex = 2;
      // 
      // txt_user
      // 
      this.txt_user.Location = new System.Drawing.Point(176, 48);
      this.txt_user.MaxLength = 50;
      this.txt_user.Name = "txt_user";
      this.txt_user.Size = new System.Drawing.Size(194, 20);
      this.txt_user.TabIndex = 5;
      // 
      // lbl_user
      // 
      this.lbl_user.AutoSize = true;
      this.lbl_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_user.ForeColor = System.Drawing.Color.White;
      this.lbl_user.Location = new System.Drawing.Point(98, 48);
      this.lbl_user.Name = "lbl_user";
      this.lbl_user.Size = new System.Drawing.Size(61, 15);
      this.lbl_user.TabIndex = 4;
      this.lbl_user.Text = "Usuario:";
      // 
      // txt_password
      // 
      this.txt_password.Location = new System.Drawing.Point(176, 74);
      this.txt_password.MaxLength = 50;
      this.txt_password.Name = "txt_password";
      this.txt_password.PasswordChar = '*';
      this.txt_password.Size = new System.Drawing.Size(194, 20);
      this.txt_password.TabIndex = 7;
      // 
      // lbl_password
      // 
      this.lbl_password.AutoSize = true;
      this.lbl_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_password.ForeColor = System.Drawing.Color.White;
      this.lbl_password.Location = new System.Drawing.Point(75, 74);
      this.lbl_password.Name = "lbl_password";
      this.lbl_password.Size = new System.Drawing.Size(84, 15);
      this.lbl_password.TabIndex = 6;
      this.lbl_password.Text = "Contraseña:";
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.SteelBlue;
      this.panel1.Controls.Add(this.progress_bar_checking);
      this.panel1.Controls.Add(this.pictureBox2);
      this.panel1.Controls.Add(this.txt_operator_id);
      this.panel1.Controls.Add(this.txt_password);
      this.panel1.Controls.Add(this.lbl_operator_id);
      this.panel1.Controls.Add(this.lbl_password);
      this.panel1.Controls.Add(this.lbl_user);
      this.panel1.Controls.Add(this.txt_user);
      this.panel1.Location = new System.Drawing.Point(12, 104);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(390, 123);
      this.panel1.TabIndex = 9;
      // 
      // pictureBox2
      // 
      this.pictureBox2.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.users;
      this.pictureBox2.Location = new System.Drawing.Point(3, 22);
      this.pictureBox2.Name = "pictureBox2";
      this.pictureBox2.Size = new System.Drawing.Size(51, 58);
      this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox2.TabIndex = 8;
      this.pictureBox2.TabStop = false;
      // 
      // btn_cancel
      // 
      this.btn_cancel.Location = new System.Drawing.Point(313, 233);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(89, 30);
      this.btn_cancel.TabIndex = 10;
      this.btn_cancel.Text = "Cancelar";
      this.btn_cancel.UseVisualStyleBackColor = true;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // btn_ok
      // 
      this.btn_ok.Location = new System.Drawing.Point(224, 233);
      this.btn_ok.Name = "btn_ok";
      this.btn_ok.Size = new System.Drawing.Size(83, 30);
      this.btn_ok.TabIndex = 11;
      this.btn_ok.Text = "Aceptar";
      this.btn_ok.UseVisualStyleBackColor = true;
      this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
      this.pictureBox1.Image = global::WSI.TransferManagerPSA.WinApp.Properties.Resources.Banner2;
      this.pictureBox1.Location = new System.Drawing.Point(0, 0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(414, 91);
      this.pictureBox1.TabIndex = 3;
      this.pictureBox1.TabStop = false;
      // 
      // progress_bar_checking
      // 
      this.progress_bar_checking.Location = new System.Drawing.Point(3, 98);
      this.progress_bar_checking.Name = "progress_bar_checking";
      this.progress_bar_checking.Size = new System.Drawing.Size(384, 23);
      this.progress_bar_checking.TabIndex = 12;
      this.progress_bar_checking.Visible = false;
      // 
      // frm_login
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(414, 267);
      this.Controls.Add(this.btn_ok);
      this.Controls.Add(this.btn_cancel);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.pictureBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_login";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Login";
      this.Load += new System.EventHandler(this.frm_login_Load);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lbl_operator_id;
    private System.Windows.Forms.TextBox txt_operator_id;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.TextBox txt_user;
    private System.Windows.Forms.Label lbl_user;
    private System.Windows.Forms.TextBox txt_password;
    private System.Windows.Forms.Label lbl_password;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.Button btn_ok;
    private System.Windows.Forms.PictureBox pictureBox2;
    private System.Windows.Forms.ProgressBar progress_bar_checking;
  }
}