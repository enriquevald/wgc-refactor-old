﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Helper.cs
// 
//   DESCRIPTION: Static class that offers general methods to the Windows application
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.WinApp
{
  public static class Helper
  {
    /// <summary>
    /// Show a custom message box
    /// </summary>
    /// <param name="messageType"></param>
    /// <param name="message"></param>
    public static void ShowCustomMessageBox(MessageBoxType messageType, string message)
    {
      frm_message_box _frm_error = new frm_message_box(messageType, message);
      _frm_error.ShowDialog();
      _frm_error.Dispose();
    }

    /// <summary>
    /// Write a Text in the Control passed like a parameter
    /// </summary>
    /// <param name="Control"></param>
    /// <param name="Text"></param>
    public static void WriteTextInControl(Control Control, string Text)
    {
      if (Control.InvokeRequired)
      {
        Control.Invoke((MethodInvoker)delegate
        {
          WriteTextInControl(Control, Text);
        });
      }
      else
      {
        Control.Text = Text;
      }
    }

    /// <summary>
    /// Set a Value to the progress bar passed like a parameter
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="Bar"></param>
    public static void SetValueProgressBar(int Value, ProgressBar Bar)
    {
      if (Value < 0 || Value > 100)
        throw new Exception("The value to the operation progress bar is invalid!");

      if (Bar.InvokeRequired)
      {
        Bar.Invoke((MethodInvoker)delegate
        {
          SetValueProgressBar(Value, Bar);
        });
      }
      else
      {
        if (Bar != null)
          Bar.Value = Value;
      }
    }
  }
}
