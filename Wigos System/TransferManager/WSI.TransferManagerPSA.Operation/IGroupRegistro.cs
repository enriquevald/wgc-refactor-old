﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Operation
{
  public interface IGroupRegistro
  {
    int Count { get; }

    string XML { get; }
  }
}
