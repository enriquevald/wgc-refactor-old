﻿////------------------------------------------------------------------------------
//// Copyright © 2012 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Registro_Linea_Constancia.cs
//// 
////      DESCRIPTION: Class defines the constancy
//// 
////           AUTHOR: Jordi vera
//// 
////    CREATION DATE: 05-OCT-2012
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 05-OCT-2012 JVV    First release.
//// 19-OCT-2012 JVV    Update line.
////------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WSI.TransferManagerPSA.Operation
{
  public class Registro_Linea_Constancia
  {
    #region Fields
    public Registro_Linea_Constancia_General General { get; set; }
    public Registro_Linea_Constancia_Jugador Jugador { get; set; }
    public Int32 ValidationErrors { get; set; }
    #endregion
  }

  public class Registro_Linea_Constancia_General
  {
    #region Fields
    public Decimal MontoPremio { get; set; }
    public Decimal ISRretenido { get; set; }
    public String NoMaquina { get; set; }
    #endregion
  }

  public class Registro_Linea_Constancia_Jugador
  {
    #region Fields
    public Registro_Linea_Constancia_Jugador_General General { get; set; }
    public Registro_Linea_Constancia_Jugador_Domicilio Domicilio { get; set; }
    #endregion
  }

  public class Registro_Linea_Constancia_Jugador_General
  {
    #region Fields
    public String rfc { get; set; }
    public String NombreJugador { get; set; }
    public String CURP { get; set; }
    public String DocumentoID { get; set; }
    public String NoDocumentoID { get; set; }
    #endregion
  }

  public class Registro_Linea_Constancia_Jugador_Domicilio
  {
    #region Fields
    public String calle { get; set; }
    public String municipio { get; set; }
    public String estado { get; set; }
    public String pais { get; set; }
    public String codigoPostal { get; set; }
    public String noExterior { get; set; }
    public String noInterior { get; set; }
    public String colonia { get; set; }
    public String localidad { get; set; }
    public String referencia { get; set; }
    #endregion
  }

  public enum ConstanciaErrores
  {
    NONE = 0,
    RFC = 1,
    CURP = 2,
    POSTALCODE = 4
  }
}