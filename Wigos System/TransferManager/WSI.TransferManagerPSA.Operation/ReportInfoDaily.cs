﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Operation
{
  public class ReportInfoDaily : IReportInfo
  {
    #region Constants

    /// <summary>
    /// Decimal value = 0
    /// </summary>
    private const Decimal EMPTY_DECIMAL = 0.00m;

    /// <summary>
    /// Record for inform the sale of cartons of a customer
    /// </summary>
    private const string REGISTRO_OPERACION_VENTA_CARTONES = "1";

    /// <summary>
    /// Record for inform the prize obtained by a customer
    /// </summary>
    private const string REGISTRO_OPERACION_PREMIO_OBTENIDO = "2";

    #endregion

    #region Attributes

    private List<GroupRegistroL005> m_group_records = null;
    
    #endregion

    #region Constructor

    #endregion

    #region Properties

    /// <summary>
    /// Get the complete list of RegistroL005
    /// </summary>
    public List<GroupRegistroL005> GroupRegistroL005
    {
      get
      {
        return m_group_records;
      }
    }

    public List<GroupRegistroL006> GroupRegistroL006
    {
      get { throw new NotImplementedException(); }
    }

    /// <summary>
    /// Get the total of elements
    /// </summary>
    public int Count
    {
      get
      {
        return m_group_records.Sum(x => x.Count);
      }
    }

    #endregion

    #region Public Methods

    public void Load(DailyReportExcel Report, int NumberPagination)
    {
      BuildList_GroupRegistroL005(Report, NumberPagination);
    }

    public void Load(MonthlyReportExcel Report, int NumberPagination)
    {
      throw new NotImplementedException();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Gets the full list of records that the application should send to the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private void BuildList_GroupRegistroL005(DailyReportExcel Report, int NumberPagination)
    {
      List<Registro_L005> _list_L005 = new List<Registro_L005>();
      int _index = 0;

      m_group_records = new List<GroupRegistroL005>();

      foreach (DailyItem _customer in Report.CustomerDailyItems)
      {
        _list_L005.Add(GetFirstRegistroL005(Report, _customer, ++_index));
        if (_index % NumberPagination == 0)
        {
          m_group_records.Add(new GroupRegistroL005(_list_L005.ToList()));
          _list_L005.Clear();
        }

        if (_customer.Results < 0)
        {
          _list_L005.Add(GetSecondRegistroL005(Report, _customer, ++_index));
          if (_index % NumberPagination == 0)
          {
            m_group_records.Add(new GroupRegistroL005(_list_L005.ToList()));
            _list_L005.Clear();
          }
        }
      }
      
      // Add the reaminder
      if (_list_L005.Count > 0)
        m_group_records.Add(new GroupRegistroL005(_list_L005));
    }

    /// <summary>
    /// Gets the L005 record that is builded using the customer information
    /// </summary>
    /// <param name="RecordNumber"></param>
    /// <param name="BingoCustomer"></param>
    /// <returns></returns>
    private Registro_L005 GetFirstRegistroL005(DailyReportExcel Report, DailyItem BingoCustomer, int RecordNumber)
    {
      try
      {
        Registro_L005 _register_L005 = new Registro_L005()
        {
          NoRegistro = RecordNumber,
          ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio,
          ClaveCombinacion = AppConfiguration.ClaveCombinacion,
          ClaveTipoPago = AppConfiguration.ClaveTipoPago,
          NoBoletos = BingoCustomer.TotalCards,  //
          NombreJuego = "Bingo",
          IdEvento = GetIDEvent(Report, BingoCustomer, REGISTRO_OPERACION_VENTA_CARTONES),
          FormaPago = "Efectivo",
          MontoPremioLinea = EMPTY_DECIMAL,
          MontoPremioSorteo = EMPTY_DECIMAL, //
          MontoPremioEspecial = EMPTY_DECIMAL,
          MontoPremioLoteria = EMPTY_DECIMAL,
          MontoRecaudado = BingoCustomer.AmountSold, //
          MontoBolsa = EMPTY_DECIMAL,
          MontoReserva = EMPTY_DECIMAL,
          FechaHora = GetDateTime(Report, BingoCustomer),
          MontoPremioNoReclamado = EMPTY_DECIMAL,
          Constancia = null,
          TipoCambio = EMPTY_DECIMAL,
          NumTransaccion = GetIDEvent(Report, BingoCustomer, REGISTRO_OPERACION_VENTA_CARTONES).Replace("-", ""),
          MontoApostado = EMPTY_DECIMAL,
          NoComprobante = "0",
          NoRegistroCaja = "0",
          SaldoInicial = EMPTY_DECIMAL,
          SaldoPromocion = EMPTY_DECIMAL,
        };

        return _register_L005;
      }
      catch
      {
        Log.Error(string.Format("Exception in GetFirstRegistroL005 with the NoRegistro {0}", RecordNumber));
        throw;
      }
    }

    /// <summary>
    /// Gets the L005 record that has information about the RFC and Taxes
    /// </summary>
    /// <param name="RecordNumber"></param>
    /// <param name="BingoCustomer"></param>
    /// <returns></returns>
    private Registro_L005 GetSecondRegistroL005(DailyReportExcel Report, DailyItem BingoCustomer, int RecordNumber)
    {
      try
      {
        Registro_Linea_Constancia _constancia = null;

        Registro_L005 _register_L005 = new Registro_L005()
        {
          NoRegistro = RecordNumber,
          ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio,
          ClaveCombinacion = AppConfiguration.ClaveCombinacion,
          ClaveTipoPago = AppConfiguration.ClaveTipoPago,
          NoBoletos = 1,  //
          NombreJuego = "Bingo",
          IdEvento = GetIDEvent(Report, BingoCustomer, REGISTRO_OPERACION_PREMIO_OBTENIDO),
          FormaPago = "Efectivo",
          MontoPremioLinea = EMPTY_DECIMAL,
          MontoPremioSorteo = -BingoCustomer.Results, //
          MontoPremioEspecial = EMPTY_DECIMAL,
          MontoPremioLoteria = EMPTY_DECIMAL,
          MontoRecaudado = EMPTY_DECIMAL, //
          MontoBolsa = EMPTY_DECIMAL,
          MontoReserva = EMPTY_DECIMAL,
          FechaHora = GetDateTime(Report, BingoCustomer),
          MontoPremioNoReclamado = EMPTY_DECIMAL,
          Constancia = _constancia,
          TipoCambio = EMPTY_DECIMAL,
          NumTransaccion = GetIDEvent(Report, BingoCustomer, REGISTRO_OPERACION_PREMIO_OBTENIDO).Replace("-", ""),
          MontoApostado = EMPTY_DECIMAL,
          NoComprobante = "0",
          NoRegistroCaja = "0",
          SaldoInicial = EMPTY_DECIMAL,
          SaldoPromocion = EMPTY_DECIMAL,
        };

        if (BingoCustomer.Results < AppConfiguration.MaxValueResults)
        {
          Registro_Linea_Constancia_General _general = new Registro_Linea_Constancia_General();
          Registro_Linea_Constancia_Jugador _jugador = new Registro_Linea_Constancia_Jugador();
          Registro_Linea_Constancia_Jugador_General _jugador_general = new Registro_Linea_Constancia_Jugador_General();

          _constancia = new Registro_Linea_Constancia();

          _general.MontoPremio = -BingoCustomer.Results;
          _general.ISRretenido = (decimal)BingoCustomer.Taxes;
          _constancia.General = _general;

          _constancia.Jugador = _jugador;
          _jugador.General = _jugador_general;
          _jugador_general.NombreJugador = BingoCustomer.CustomerName;
          _jugador_general.rfc = BingoCustomer.RFC;

          _register_L005.Constancia = _constancia;
        }

        return _register_L005;
      }
      catch
      {
        Log.Error(string.Format("Exception in GetSecondRegistroL005 with the NoRegistro {0}", RecordNumber));
        throw;
      }
    }

    /// <summary>
    /// Get ID Event for a Registro_L005
    /// </summary>
    /// <param name="Report"></param>
    /// <param name="BingoCustomer"></param>
    /// <param name="RecordNumber"></param>
    /// <returns></returns>
    private string GetIDEvent(DailyReportExcel Report, DailyItem BingoCustomer, string TipoRegistro)
    {
      StringBuilder _id_str = new StringBuilder();
      _id_str.Append(Report.DateReport.ToString("yyyyMMdd"));
      _id_str.Append("-");
      _id_str.Append(BingoCustomer.SessionType == SessionType.Night ? "2" : "1");
      _id_str.Append("-");
      _id_str.Append(BingoCustomer.ID.ToString("0000"));
      _id_str.Append("-");
      _id_str.Append(TipoRegistro);
      return _id_str.ToString();
    }

    /// <summary>
    /// Get a date time for a Registro_L005
    /// </summary>
    /// <param name="Report"></param>
    /// <param name="BingoCustomer"></param>
    /// <returns></returns>
    private DateTime GetDateTime(DailyReportExcel Report, DailyItem BingoCustomer)
    {
      if (BingoCustomer.SessionType == SessionType.Night)
      {
        return new DateTime(Report.DateReport.Year,
          Report.DateReport.Month,
          Report.DateReport.Day,
          19, 0, 0);
      }
      else if (BingoCustomer.SessionType == SessionType.Day)
      {
        return new DateTime(Report.DateReport.Year,
          Report.DateReport.Month,
          Report.DateReport.Day,
          15, 0, 0);
      }
      else
      {
        throw new Exception("The Session type for the customer is invalid!");
      }
    }

    #endregion
  }
}
