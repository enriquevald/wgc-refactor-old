﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Operation
{
  public enum OperationTypePSA
  {
    NONE,
    OPEN_SESSION,
    CLOSE_SESSION,
    SEND_DATA_FASE1,
    SEND_DATA_FASE2,
    SEND_DATA_FASE3,
    SEND_DATA_FASE4,
    QUERY_PENDING,
    SET_VALUE_PROGRESS,
    ERROR,
  }
}
