﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Operation
{
  [Serializable]
  public class Registro_L006
  {
    public Int64 NoRegistro { get; set; }
    public String ClavesublineaNegocio { get; set; }
    public String ClaveCombinacion { get; set; }
    public String ClaveTipoPago { get; set; }
    public String NombreJuego { get; set; }
    public String IdEvento { get; set; }
    public Int32 NoBoletosEmitidos { get; set; }
    public Int32 NoBoletosVendidos { get; set; }
    public Decimal MontoBoletosEmitidos { get; set; }
    public Decimal MontoBoletosVendidos { get; set; }
    public String FormaPago { get; set; }
    public DateTime FechaHora { get; set; }
    public Decimal MontoPremioPagado { get; set; }
    public Decimal MontoPremioNoReclamado { get; set; }
    public Registro_Linea_Constancia Constancia { get; set; }
    public Decimal? TipoCambio { get; set; }
    public String NumTransaccion { get; set; }
    public String NoComprobante { get; set; }
    public String NoRegistroCaja { get; set; }
    public Decimal MontoRecaudado { get; set; }
    public Decimal MontoBolsa { get; set; }
    public Decimal MontoReserva { get; set; }
    public Decimal MontoPremioLinea { get; set; }
    public Decimal MontoPremioSorteo { get; set; }
    public Decimal MontoPremioEspecial { get; set; }
    public Decimal SaldoInicial { get; set; }
    public Decimal SaldoPromocion { get; set; }

    public override string ToString()
    {
      StringBuilder _str = new StringBuilder();
      _str.AppendLine("NoRegistro: " + this.NoRegistro);
      _str.AppendLine("ClavesublineaNegocio: " + this.ClavesublineaNegocio);
      _str.AppendLine("ClaveCombinacion: " + this.ClaveCombinacion);
      _str.AppendLine("ClaveTipoPago: " + this.ClaveTipoPago);
      _str.AppendLine("NombreJuego: " + this.NombreJuego);
      _str.AppendLine("IdEvento: " + this.IdEvento);
      _str.AppendLine("NoBoletosEmitidos: " + this.NoBoletosEmitidos);
      _str.AppendLine("NoBoletosVendidos: " + this.NoBoletosVendidos);
      _str.AppendLine("MontoBoletosEmitidos: " + this.MontoBoletosEmitidos);
      _str.AppendLine("MontoBoletosVendidos: " + this.MontoBoletosVendidos);
      _str.AppendLine("FormaPago: " + this.FormaPago);
      _str.AppendLine("FechaHora: " + this.FechaHora);
      _str.AppendLine("MontoPremioPagado: " + this.MontoPremioPagado);
      _str.AppendLine("MontoPremioNoReclamado: " + this.MontoPremioNoReclamado);
      _str.AppendLine("Constancia: " + this.Constancia);
      _str.AppendLine("TipoCambio: " + this.TipoCambio);
      _str.AppendLine("NumTransaccion: " + this.NumTransaccion);
      _str.AppendLine("NoComprobante: " + this.NoComprobante);
      _str.AppendLine("NoRegistroCaja: " + this.NoRegistroCaja);
      _str.AppendLine("MontoRecaudado: " + this.MontoRecaudado);
      _str.AppendLine("MontoBolsa: " + this.MontoBolsa);
      _str.AppendLine("MontoReserva: " + this.MontoReserva);
      _str.AppendLine("MontoPremioLinea: " + this.MontoPremioLinea);
      _str.AppendLine("MontoPremioSorteo: " + this.MontoPremioSorteo);
      _str.AppendLine("MontoPremioEspecial: " + this.MontoPremioEspecial);
      _str.AppendLine("SaldoInicial: " + this.SaldoInicial);
      _str.AppendLine("SaldoPromocion: " + this.SaldoPromocion);
      return _str.ToString();
    }
  }
}
