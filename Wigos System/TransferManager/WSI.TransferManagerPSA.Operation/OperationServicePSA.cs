﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Model;
using WSI.TransferManagerPSA.Client.PSA_Refence;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Excel;
using System.Runtime.Serialization;
using System.IO;
using WSI.TransferManagerPSA.Language;
using System.Threading;
using WSI.TransferManagerPSA.Client;

namespace WSI.TransferManagerPSA.Operation
{
  public class OperationServicePSA
  {
    #region Attributes

    public event EventHandler<OperationEventArgs> PSAOperationEvent;
    public event EventHandler<ConnectionEventArgs> PSAConnectionProcessEvent;

    private string m_id_session = string.Empty;
    private string m_id_transation = string.Empty;
    private PSAClient m_service_client;
    private IPSAConfiguration m_configuration;
    private IReportInfo m_report_Info;
    private int m_id_secuencia = 0;

    #endregion

    #region Constructor

    public OperationServicePSA(IPSAConfiguration IPSAConfiguration)
    {
      m_configuration = IPSAConfiguration;

      m_service_client = new PSAClient(m_configuration);
      m_service_client.ConnectionProcessEvent += m_service_client_ConnectionProcessEvent;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Configuration
    /// </summary>
    public IPSAConfiguration Configuration
    {
      get
      {
        return m_configuration;
      }
      set
      {
        m_configuration = value;
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Checks the connection with the PSA service doing an open and close of session.
    /// </summary>
    /// <returns></returns>
    public bool CheckConnection()
    {
      try
      {
        return m_service_client.OpenSession() == ResponseCode.PSA_RC_OK
               && m_service_client.CloseSession() == ResponseCode.PSA_RC_OK;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
    }

    /// <summary>
    /// Send a Customer Report to the PSA Service
    /// </summary>
    /// <param name="DailyReport"></param>
    /// <returns></returns>
    public bool SendReport(DailyReportExcel DailyReport)
    {
      try
      {
        ResponseCode _response;
        bool _report_sent;

        SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_STARTING", OperationTypePSA.OPEN_SESSION);

        // Open connection with PSA
        SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTING", OperationTypePSA.OPEN_SESSION);
        _response = m_service_client.OpenSession(false);

        if (_response == ResponseCode.PSA_RC_OK)
        {
          m_id_session = m_service_client.IdSesion;
          SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTED", OperationTypePSA.OPEN_SESSION);

          m_report_Info = new ReportInfoDaily();
          m_report_Info.Load(DailyReport, m_service_client.Session.NumberRecordsPagination);

          // Send Report to PSA
          _report_sent = SendFase1(DailyReport) && SendFase2(DailyReport) && SendFase3(DailyReport) && SendFase4();

          CloseSession(); // always close the session

          if (_report_sent)
          {
            int _num_records = m_report_Info.Count;
            int _num_customers = DailyReport.Count;
            int _num_evidences = DailyReport.TotalEvidences;

            // The repost was sent sussesfully
            SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FINISHED", OperationTypePSA.SEND_DATA_FASE4, DailyReport.DateReport.ToShortDateString());
            SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FINISHED_SUMMARY", OperationTypePSA.SEND_DATA_FASE4,
                              _num_records, _num_customers, _num_evidences);

            Log.Message(string.Format("The Report has been sent. Records [{0}], Customers [{1}], Evidences [{2}].", _num_records, _num_customers, _num_evidences));

            return true;
          }
        }
        else
        {
          SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_ERROR", OperationTypePSA.ERROR);
        }

        // Raise error
        SendEventError(m_service_client.LastMessageError);
        SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_ERROR", OperationTypePSA.ERROR);
        return false;

      }
      catch
      {
        SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_ERROR", OperationTypePSA.ERROR);
        throw;
      }
    }

    /// <summary>
    /// Send a Monthly Report to the PSA Service
    /// </summary>
    /// <param name="MonthlyReportExcelGroup"></param>
    /// <returns></returns>
    public bool SendReport(MonthlyReportExcelGroup MonthlyReportExcelGroup)
    {
      try
      {
        ResponseCode _response;
        Int32 _sent_ok = 0;
        Int32 _counter = 0;

        // Open connection with PSA
        SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTING", OperationTypePSA.OPEN_SESSION);
        _response = m_service_client.OpenSession(false);
        if (_response == ResponseCode.PSA_RC_OK)
        {
          m_id_session = m_service_client.IdSesion;
          SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTED", OperationTypePSA.OPEN_SESSION);

          foreach (MonthlyReportExcel _report in MonthlyReportExcelGroup)
          {
            SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_STARTING", OperationTypePSA.OPEN_SESSION);
            _counter++;

            m_report_Info = new ReportInfoMonthly();
            m_report_Info.Load(_report, m_service_client.Session.NumberRecordsPagination);

            if (SendFase1(_report, MonthlyReportExcelGroup.SiteId.ToString()) && SendFase2(_report) && SendFase3(_report) && SendFase4())
            {
              _sent_ok++;

              // The repost was sent sussesfully
              SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FINISHED_LINE", OperationTypePSA.SEND_DATA_FASE4, GetBussinessLineKey(_report), _report.DateReport.ToShortDateString());
            }
            else
            {
              SendEventError(m_service_client.LastMessageError);
              SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_ERROR", OperationTypePSA.ERROR);
              break;
            }

            SendEvent("{0},{1}", OperationTypePSA.SET_VALUE_PROGRESS, _counter, MonthlyReportExcelGroup.Count);
          }

          CloseSession();

          if (MonthlyReportExcelGroup.Count == _sent_ok)
           {
             // TODO: Falta colocar la NLS correcta y log
             SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FINISHED_SUMMARY_REPORTS", OperationTypePSA.SEND_DATA_FASE4, MonthlyReportExcelGroup.Count);
             Log.Message(string.Format("The Report has been sent. Reports [{0}].", MonthlyReportExcelGroup.Count));

           }

          return (MonthlyReportExcelGroup.Count == _sent_ok);
        }
        else
        {
          SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_ERROR", OperationTypePSA.ERROR);
        }

        return false;
      }
      catch
      {
        SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_ERROR", OperationTypePSA.ERROR);
        throw;
      }
    }

    /// <summary>
    /// Checks if the report (by date) has been sent to the PSA service
    /// </summary>
    /// <param name="ReportDate"></param>
    /// <param name="StatusReportSent"></param>
    /// <returns></returns>
    public bool CheckReportSent (DateTime ReportDate, out bool StatusReportSent, String SiteId = "")
    {
      ResponseCode _response;
      List<DiaPendiente> _pending_days = null;
      String _site_id = String.IsNullOrEmpty(SiteId) ? AppConfiguration.ClaveEstablecimiento : SiteId;

      StatusReportSent = false;

      SendEvent("NLS_PSA_MESSAGE_QUERY_PENDING_STARTING", OperationTypePSA.QUERY_PENDING);

      // Open connection with PSA
      SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTING", OperationTypePSA.OPEN_SESSION);
      _response = m_service_client.OpenSession(false);

      if (_response == ResponseCode.PSA_RC_OK)
      {
        m_id_session = m_service_client.IdSesion;
        SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_STARTED", OperationTypePSA.OPEN_SESSION);

        if (m_service_client.RequestPendingReports(ReportDate.Year, ReportDate.Month, out _pending_days))
        {
          // check the report date in the list of pending to send
          var _pending_day = _pending_days.FirstOrDefault(x => x.Fecha == ReportDate.ToString("yyyy-MM-dd"));

          if (_pending_day != null)
          {
            var _establecimiento = _pending_day.EstablecimientosPendientes.FirstOrDefault(x => x.ClaveEstablecimiento == _site_id);

            if (_establecimiento != null)
            {
              StatusReportSent = (_establecimiento.ReportesPendientes.FirstOrDefault(x => x.ContextoReporte == "L005") == null);
              Log.Message(string.Format("Status Report sent = {0}", StatusReportSent.ToString()));
            }
            else
            {
              Log.Warning(string.Format("There is not a establecimiento {0}", _site_id));
            }
          }
          else
          { 
            Log.Warning(string.Format("There is not a pending day for the day {0}", ReportDate.ToString("yyyy-MM-dd")));
          }

          if (StatusReportSent)
            SendEvent("NLS_PSA_MESSAGE_QUERY_PENDING_EXISTS", OperationTypePSA.QUERY_PENDING);
          else
          {
            SendEvent("NLS_PSA_MESSAGE_QUERY_PENDING_NOT_EXISTS", OperationTypePSA.QUERY_PENDING);
          }

          // Closing the connection
          SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHING", OperationTypePSA.CLOSE_SESSION);
          _response = m_service_client.CloseSession();
          if (_response == ResponseCode.PSA_RC_OK)
          {
            SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHED", OperationTypePSA.CLOSE_SESSION);
            return true;
          }
          else
          {
            SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_ERROR", OperationTypePSA.ERROR);
          }
        }
        else
        {
          // Try to close the session because it is open
          SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHING", OperationTypePSA.CLOSE_SESSION);
          _response = m_service_client.CloseSession();
          if (_response == ResponseCode.PSA_RC_OK)
            SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHED", OperationTypePSA.CLOSE_SESSION);
          else
            SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_ERROR", OperationTypePSA.ERROR);
        }
      }
      else
      {
        SendEvent("NLS_PSA_MESSAGE_OPEN_SESSION_ERROR", OperationTypePSA.ERROR);
      }

      // Raise error
      SendEventError(m_service_client.LastMessageError);
      SendEvent("NLS_PSA_MESSAGE_QUERY_PENDING_ERROR", OperationTypePSA.ERROR);
      return false;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Call to Fase1 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private bool SendFase1(IReportExcel Report, String SiteId = "")
    {
      String _site_id = String.IsNullOrEmpty(SiteId) ? AppConfiguration.ClaveEstablecimiento : SiteId;

      PSA_ReporteFase1_Contexto _requestF1 = new PSA_ReporteFase1_Contexto()
      {
        Body = new PSA_ReporteFase1_ContextoBody()
        {
          ClaveEstablecimiento = _site_id,
          FechaReporte =  Report.DateReport.ToString("yyyy-MM-dd"),
          IdSesion = m_id_session,
          LineasReportadas = GetBussinessLineKey (Report),
          NoLineasReportadas = 1,
          Version = AppConfiguration.Version,
          SubLineasReportadas = null,
        }
      };

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_1", OperationTypePSA.SEND_DATA_FASE1);

      if (m_service_client.SendFase1(_requestF1.Body))
      {
        m_id_transation = m_service_client.IdTransaction;
        //SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_FINISHED", OperationTypePSA.SEND_DATA_FASE1, "1");
        return true;
      }

      m_id_transation = string.Empty;
      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_ERROR", OperationTypePSA.SEND_DATA_FASE1, "1");
      return false;
    }

    /// <summary>
    ///  Call to Fase2 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private bool SendFase2(IReportExcel Report)
    {
      BalanceLinea _balance = new BalanceLinea();
      _balance.BalanceLineaNegocio = new BalanceLineaNegocio();
      _balance.BalanceSublineasNegocio = new List<BalanceSubLineaNegocio>();

      decimal _input_caja = Report.TotalAmountSold;

      decimal _output_caja = 0;

      if (Report.ReportGameType == GameType.Bingo)
        _output_caja = Report.TotalAmountWin + Report.TotalRefunds;
      else if (Report.ReportGameType == GameType.Tables)
        _output_caja = Report.TotalAmountWin;


      _balance.BalanceLineaNegocio.IngresosCaja = _input_caja.ToString();
      _balance.BalanceLineaNegocio.SalidasCaja = _output_caja.ToString();
      _balance.BalanceLineaNegocio.BaliniLinea = "0"; // It is indicated in the documentation.
      _balance.BalanceLineaNegocio.BalfinLinea = (_input_caja - _output_caja).ToString();


      BalanceSubLineaNegocio _balancesublineanegocio;
      _balancesublineanegocio = new BalanceSubLineaNegocio();
      _balancesublineanegocio.ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio;
      _balancesublineanegocio.BaliniSubLinea = "0"; // It is indicated in the documentation.
      _balancesublineanegocio.BalfinSubLinea = (_input_caja - _output_caja).ToString();
      _balance.BalanceSublineasNegocio.Add(_balancesublineanegocio);

      m_id_secuencia = 1;

      int _number_records = m_report_Info.Count;

      PSA_ReporteFase2_ReferenciaLinea _requestF2 = new PSA_ReporteFase2_ReferenciaLinea()
      {
        Body = new PSA_ReporteFase2_ReferenciaLineaBody()
        {
          BalanceLinea = _balance,
          ChecksumContenido = "myChecksum",
          IdSecuencia = m_id_secuencia,
          IdSesion = m_id_session,
          IdTransaccion = m_id_transation,
          LineaReportada = GetBussinessLineKey (Report),
          NoRegistrosReporte = _number_records,
        }
      };

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_2", OperationTypePSA.SEND_DATA_FASE2);

      if (m_service_client.SendFase2(_requestF2.Body))
      {
        //SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_FINISHED", OperationTypePSA.SEND_DATA_FASE2, "2");
        return true;
      }

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_ERROR", OperationTypePSA.SEND_DATA_FASE2, "2");
      return false;
    }

    /// <summary>
    /// Call to Fase3 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private bool SendFase3(IReportExcel Report)
    {

      List<IGroupRegistro> _list = null;

      if (Report.ReportGameType == GameType.Bingo)
        _list = m_report_Info.GroupRegistroL005.ToList<IGroupRegistro>();
      else if (Report.ReportGameType == GameType.Tables)
        _list = m_report_Info.GroupRegistroL006.ToList<IGroupRegistro>();

      foreach (IGroupRegistro records in _list)
      {
        PSA_ReporteFase3_RegistrosLinea _requestF3 = new PSA_ReporteFase3_RegistrosLinea()
        {
          Body = new PSA_ReporteFase3_RegistrosLineaBody()
          {
            ChecksumContenido = "myChecksum",
            IdSecuencia = ++m_id_secuencia,
            IdSesion = m_id_session,
            IdTransaccion = m_id_transation,
            LineaReportada = GetBussinessLineKey(Report),
            ReporteRegistrosLinea = records.XML,
            TiempoSistema = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"),
          }
        };

        if (m_service_client.SendFase3(_requestF3.Body) == false)
        {
          SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_ERROR", OperationTypePSA.SEND_DATA_FASE3, "3");
          return false;
        }
        
        Log.Message("Fase3, records sent: " + records.Count);
      }

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_3", OperationTypePSA.SEND_DATA_FASE3, m_report_Info.Count);
      return true;
    }

    /// <summary>
    /// Call to Fase4 of the PSA service
    /// </summary>
    /// <returns></returns>
    private bool SendFase4()
    {
      PSA_ReporteFase4_TransaccionCompletada _requestF4 = new PSA_ReporteFase4_TransaccionCompletada()
      {
        Body = new PSA_ReporteFase4_TransaccionCompletadaBody()
        {
          IdSecuencia = ++m_id_secuencia,
          IdSesion = m_id_session,
          IdTransaccion = m_id_transation,
        }
      };

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_4", OperationTypePSA.SEND_DATA_FASE4);

      if (m_service_client.SendFase4(_requestF4.Body))
      {
        //SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_FINISHED", OperationTypePSA.SEND_DATA_FASE4, "4");
        return true;
      }

      SendEvent("NLS_PSA_MESSAGE_SEND_REPORT_FASE_ERROR", OperationTypePSA.SEND_DATA_FASE4, "4");
      return false;
    }

    /// <summary>
    /// Close the current session with the PSA
    /// </summary>
    /// <returns></returns>
    private bool CloseSession()
    {
      ResponseCode _response;

      SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHING", OperationTypePSA.CLOSE_SESSION);
      _response = m_service_client.CloseSession();

      if (_response == ResponseCode.PSA_RC_OK)
      {
        SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_FINISHED", OperationTypePSA.CLOSE_SESSION);
        return true;
      }
      else
      {
        SendEvent("NLS_PSA_MESSAGE_CLOSE_SESSION_ERROR", OperationTypePSA.ERROR);
        return false;
      }
    }

    /// <summary>
    /// Send a event with information about a task with the PSA Service
    /// </summary>
    /// <param name="IDLanguageName"></param>
    /// <param name="OperationType"></param>
    private void SendEvent(string IDLanguageName, OperationTypePSA OperationType, params object[] argMessage)
    {
      try
      {
        if (PSAOperationEvent != null)
        {
          OperationEventArgs e = new OperationEventArgs()
          {
            Message = argMessage.Count() == 0 ? LanguageManager.GetValue(IDLanguageName) : string.Format(LanguageManager.GetValue(IDLanguageName), argMessage),
            Type = OperationType,
            argMessage = argMessage
          };

          PSAOperationEvent(this, e);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Send event with the error
    /// </summary>
    /// <param name="ErrorMsg"></param>
    private void SendEventError(string ErrorMsg)
    {
      try
      {
        if (PSAOperationEvent != null && !string.IsNullOrEmpty(ErrorMsg))
        {
          OperationEventArgs e = new OperationEventArgs()
          {
            Message = ErrorMsg,
            Type = OperationTypePSA.ERROR
          };

          PSAOperationEvent(this, e);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Gets the Bussiness line key
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private String GetBussinessLineKey(IReportExcel Report)
    {
      if (Report.ReportGameType == GameType.Bingo)
        return "L005";
      else if (Report.ReportGameType == GameType.Tables)
        return "L006";
      else
        return String.Empty;
    }

    #endregion

    #region Events

    private void m_service_client_ConnectionProcessEvent(object sender, PSAConnectionEventArgs e)
    {
      if (PSAConnectionProcessEvent != null)
      {
        ConnectionEventArgs e_connection = new ConnectionEventArgs()
        {
          AttemptNumbers = e.AttemptNumbers,
          ConnectionStep = e.ConnectionStep,
        };

        PSAConnectionProcessEvent(this, e_connection);
      }
    }

    #endregion
  }
}
