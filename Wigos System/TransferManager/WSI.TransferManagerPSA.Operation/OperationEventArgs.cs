﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Operation
{
  public class OperationEventArgs:EventArgs
  {
    public string Message { get; set; }

    public OperationTypePSA Type { get; set; }

    public object[] argMessage { get; set; }
  }
}
