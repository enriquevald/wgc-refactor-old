﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Excel;

namespace WSI.TransferManagerPSA.Operation
{
  public interface IReportInfo
  {

    #region Properties

    List<GroupRegistroL005> GroupRegistroL005 { get; }

    List<GroupRegistroL006> GroupRegistroL006 { get; }

    Int32 Count { get; }

    #endregion

    #region Public Methods

    void Load(DailyReportExcel Report, int NumberPagination);

    void Load(MonthlyReportExcel Report, int NumberPagination);

    #endregion

  }
}
