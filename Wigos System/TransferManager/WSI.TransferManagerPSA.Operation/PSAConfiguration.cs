﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Client;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Operation
{
  public class PSAConfiguration : IPSAConfiguration
  {
    public string IdOperador {get;set;}

    public string Login {get;set;}

    public string Password {get;set;}

    public string URLPrincipalPSA {get;set;}

    public string URLSecundaryPSA { get; set; }

    /// <summary>
    /// Load the values saved in the windows register and the config file
    /// </summary>
    public void LoadData()
    {
      IdOperador = UserApplication.Operator;
      Login = UserApplication.Login;
      Password = UserApplication.Password;
      URLPrincipalPSA = AppConfiguration.URL_Principal_PSA;
      URLSecundaryPSA = AppConfiguration.URL_Secundary_PSA;
    }
  }
}


