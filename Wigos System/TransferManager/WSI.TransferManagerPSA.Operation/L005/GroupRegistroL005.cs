﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Operation
{
  [Serializable]
  public class GroupRegistroL005 : IGroupRegistro
  {
    #region Attributes

    List<Registro_L005> m_records = null;
    
    #endregion

    #region Constructor

    public GroupRegistroL005(List<Registro_L005> Records)
    {
      m_records = Records;
    }

    #endregion

    #region Properties

    /// <summary>
    /// List of elements Registro_L005
    /// </summary>
    public List<Registro_L005> RecordsL005
    {
      get
      {
        return m_records;
      }
    }

    /// <summary>
    /// Number of records
    /// </summary>
    public int Count
    {
      get
      {
        return m_records.Count;
      }
    }

    /// <summary>
    /// Record in XML format
    /// </summary>
    public string XML
    {
      get
      {
        string _data_XML;
        Utils.ObjectSerializableToXml(m_records, out _data_XML);
        return _data_XML;
      }
    }

    #endregion

  }
}
