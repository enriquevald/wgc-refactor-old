﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data;
using System.Text;

namespace WSI.TransferManagerPSA.Operation
{
  [Serializable]
  public class Registro_L005
  {
    public Int64 NoRegistro { get; set; }
    public String ClavesublineaNegocio { get; set; }
    public String ClaveCombinacion { get; set; }
    public String ClaveTipoPago { get; set; }
    public Int32 NoBoletos { get; set; }
    public String NombreJuego { get; set; }
    public String IdEvento { get; set; }
    public String FormaPago { get; set; }
    public Decimal MontoPremioLinea { get; set; }//
    public Decimal MontoPremioSorteo { get; set; }//
    public Decimal MontoPremioEspecial { get; set; }//
    public Decimal MontoPremioLoteria { get; set; }
    public Decimal MontoRecaudado { get; set; }//
    public Decimal MontoBolsa { get; set; }//
    public Decimal MontoReserva { get; set; }//
    public DateTime FechaHora { get; set; }//
    public Decimal MontoPremioNoReclamado { get; set; }//
    public Registro_Linea_Constancia Constancia { get; set; }//
    public Decimal? TipoCambio { get; set; }//
    public String NumTransaccion { get; set; }//
    public Decimal MontoApostado { get; set; }
    public String NoComprobante { get; set; }//
    public String NoRegistroCaja { get; set; }//
    public Decimal SaldoInicial { get; set; }//
    public Decimal SaldoPromocion { get; set; } //

    public override string ToString()
    {
      StringBuilder _str = new StringBuilder();
      _str.AppendLine("NoRegistro: " + this.NoRegistro);
      _str.AppendLine("ClavesublineaNegocio: " + this.ClavesublineaNegocio);
      _str.AppendLine("ClaveCombinacion: " + this.ClaveCombinacion);
      _str.AppendLine("ClaveTipoPago: " + this.ClaveTipoPago);
      _str.AppendLine("NoBoletos: " + this.NoBoletos);
      _str.AppendLine("NombreJuego: " + this.NombreJuego);
      _str.AppendLine("IdEvento: " + this.IdEvento);
      _str.AppendLine("FormaPago: " + this.FormaPago);
      _str.AppendLine("MontoPremioLinea: " + this.MontoPremioLinea);
      _str.AppendLine("MontoPremioSorteo: " + this.MontoPremioSorteo);
      _str.AppendLine("MontoPremioEspecial: " + this.MontoPremioEspecial);
      _str.AppendLine("MontoPremioLoteria: " + this.MontoPremioLoteria);
      _str.AppendLine("MontoRecaudado: " + this.MontoRecaudado);
      _str.AppendLine("MontoBolsa: " + this.MontoBolsa);
      _str.AppendLine("MontoReserva: " + this.MontoReserva);
      _str.AppendLine("FechaHora: " + this.FechaHora);
      _str.AppendLine("MontoPremioNoReclamado: " + this.MontoPremioNoReclamado);
      _str.AppendLine("Constancia: " + (this.Constancia == null ? "NULL" : this.Constancia.ToString()));
      _str.AppendLine("TipoCambio: " + (this.TipoCambio == null ? "NULL" : this.TipoCambio.ToString()));
      _str.AppendLine("NumTransaccion: " + this.NumTransaccion);
      _str.AppendLine("MontoApostado: " + this.MontoApostado);
      _str.AppendLine("NoComprobante: " + this.NoComprobante);
      _str.AppendLine("NoRegistroCaja: " + this.NoRegistroCaja);
      _str.AppendLine("SaldoInicial: " + this.SaldoInicial);
      _str.AppendLine("SaldoPromocion: " + this.SaldoPromocion);
      return _str.ToString();
    }
  }
}