﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;
using WSI.TransferManagerPSA.Config;
using WSI.TransferManagerPSA.Excel;
using WSI.TransferManagerPSA.Model;

namespace WSI.TransferManagerPSA.Operation
{
  public class ReportInfoMonthly : IReportInfo
  {
    #region Constants

    /// <summary>
    /// Decimal value = 0
    /// </summary>
    private const Decimal EMPTY_DECIMAL = 0.00m;

    /// <summary>
    /// Record for inform the sale of cartons of a customer
    /// </summary>
    private const string REGISTRO_OPERACION_VENTA_CARTONES = "1";

    /// <summary>
    /// Record for inform the prize obtained by a customer
    /// </summary>
    private const string REGISTRO_OPERACION_PREMIO_OBTENIDO = "2";

    #endregion

    #region Attributes

    private List<GroupRegistroL005> m_group_records_L005 = null;

    private List<GroupRegistroL006> m_group_records_L006 = null;

    #endregion

    #region Constructor

    #endregion

    #region Properties

    /// <summary>
    /// Get the complete list of RegistroL005
    /// </summary>
    public List<GroupRegistroL005> GroupRegistroL005
    {
      get
      {
        return m_group_records_L005;
      }
    }

    /// <summary>
    /// Get the complete list of RegistroL006
    /// </summary>
    public List<GroupRegistroL006> GroupRegistroL006
    {
      get
      {
        return m_group_records_L006;
      }
    }

    /// <summary>
    /// Get the total of elements
    /// </summary>
    public Int32 Count
    {
      get
      {
        int _count = 0;

        if (m_group_records_L005 != null)
          _count += m_group_records_L005.Sum(x => x.Count);

        if (m_group_records_L006 != null)
          _count += m_group_records_L006.Sum(x => x.Count);

        return _count;
      }
    }


    #endregion

    #region Public Methods

    public void Load(DailyReportExcel Report, int NumberPagination)
    {
      throw new NotImplementedException();
    }

    public void Load(MonthlyReportExcel Report, int NumberPagination)
    {
      if (Report.MonthlyReport.ReportGameType == GameType.Bingo)
        BuildList_GroupRegistroL005(Report, NumberPagination);
      else if (Report.MonthlyReport.ReportGameType == GameType.Tables)
        BuildList_GroupRegistroL006(Report, NumberPagination);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Build the full list of records for L005
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private void BuildList_GroupRegistroL005(MonthlyReportExcel Report, int NumberPagination)
    {
      List<Registro_L005> _list_L005 = new List<Registro_L005>();
      int _index = 0;

      m_group_records_L005 = new List<GroupRegistroL005>();

      foreach (MonthlyItem _customer in Report.MonthlyReport.MonthlyItems)
      {
        _list_L005.Add(GetFirstRegistroL005(Report, _customer, ++_index));
        if (_index % NumberPagination == 0)
        {
          m_group_records_L005.Add(new GroupRegistroL005(_list_L005.ToList()));
          _list_L005.Clear();
        }
      }

      // Add the reaminder
      if (_list_L005.Count > 0)
        m_group_records_L005.Add(new GroupRegistroL005(_list_L005));
    }

    /// <summary>
    /// Build the full list of records for L005
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    private void BuildList_GroupRegistroL006(MonthlyReportExcel Report, int NumberPagination)
    {
      List<Registro_L006> _list_L006 = new List<Registro_L006>();
      int _index = 0;

      m_group_records_L006 = new List<GroupRegistroL006>();

      foreach (MonthlyItem _customer in Report.MonthlyReport.MonthlyItems)
      {
        _list_L006.Add(GetFirstRegistroL006(Report, _customer, ++_index));
        if (_index % NumberPagination == 0)
        {
          m_group_records_L006.Add(new GroupRegistroL006(_list_L006.ToList()));
          _list_L006.Clear();
        }
      }

      // Add the reaminder
      if (_list_L006.Count > 0)
        m_group_records_L006.Add(new GroupRegistroL006(_list_L006));
    }

    /// <summary>
    /// Gets the L005 record that is builded using the customer information
    /// </summary>
    /// <param name="RecordNumber"></param>
    /// <param name="MonthlyItem"></param>
    /// <returns></returns>
    private Registro_L005 GetFirstRegistroL005(MonthlyReportExcel Report, MonthlyItem MonthlyItem, int RecordNumber)
    {
      try
      {
        Registro_L005 _register_L005 = new Registro_L005()
        {
          NoRegistro = RecordNumber,
          ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio,
          ClaveCombinacion = AppConfiguration.ClaveCombinacion,
          ClaveTipoPago = AppConfiguration.ClaveTipoPago,
          NoBoletos = 0,
          NombreJuego = "Bingo",
          IdEvento = "Operación caja Bingo",
          FormaPago = "Efectivo",
          MontoPremioLinea = MonthlyItem.AmountWin,
          MontoPremioSorteo = MonthlyItem.AmountWin,
          MontoPremioEspecial = EMPTY_DECIMAL,
          MontoPremioLoteria = EMPTY_DECIMAL,
          MontoRecaudado = MonthlyItem.AmountIncome,
          MontoBolsa = EMPTY_DECIMAL,
          MontoReserva = EMPTY_DECIMAL,
          FechaHora = Report.DateReport,
          MontoPremioNoReclamado = EMPTY_DECIMAL,
          Constancia = null,
          TipoCambio = EMPTY_DECIMAL,
          NumTransaccion = "1",
          MontoApostado = MonthlyItem.AmountRefund,
          NoComprobante = "1",
          NoRegistroCaja = "1",
          SaldoInicial = EMPTY_DECIMAL,
          SaldoPromocion = EMPTY_DECIMAL,
        };

        return _register_L005;
      }
      catch
      {
        Log.Error(string.Format("Exception in GetFirstRegistroL006 for the day {0}", MonthlyItem.Date.ToShortDateString()));
        throw;
      }
    }

    /// <summary>
    /// Gets the L006 record that is builded using the customer information
    /// </summary>
    /// <param name="RecordNumber"></param>
    /// <param name="MonthlyItem"></param>
    /// <returns></returns>
    private Registro_L006 GetFirstRegistroL006(MonthlyReportExcel Report, MonthlyItem MonthlyItem, int RecordNumber)
    {
      try
      {
        Registro_L006 _register_L006 = new Registro_L006()
        {
          NoRegistro = RecordNumber,
          ClavesublineaNegocio = AppConfiguration.ClavesublineaNegocio,
          ClaveCombinacion = AppConfiguration.ClaveCombinacion,
          ClaveTipoPago = AppConfiguration.ClaveTipoPago,
          NombreJuego = "Mesas",
          IdEvento = "Operación caja Mesas",
          NoBoletosEmitidos = 0,
          NoBoletosVendidos = 0,
          MontoBoletosEmitidos = EMPTY_DECIMAL,
          MontoBoletosVendidos = EMPTY_DECIMAL,
          FormaPago = "Efectivo",
          MontoPremioLinea = MonthlyItem.AmountWin,
          MontoPremioSorteo = MonthlyItem.AmountWin,
          MontoPremioEspecial = EMPTY_DECIMAL,
          MontoRecaudado = MonthlyItem.AmountIncome,
          MontoBolsa = EMPTY_DECIMAL,
          MontoReserva = EMPTY_DECIMAL,
          FechaHora = Report.DateReport,
          MontoPremioPagado = EMPTY_DECIMAL,
          MontoPremioNoReclamado = EMPTY_DECIMAL,
          Constancia = null,
          TipoCambio = EMPTY_DECIMAL,
          NumTransaccion = "1",
          NoComprobante = "1",
          NoRegistroCaja = "1",
          SaldoInicial = EMPTY_DECIMAL,
          SaldoPromocion = EMPTY_DECIMAL,
        };

        return _register_L006;
      }
      catch
      {
        Log.Error(string.Format("Exception in GetFirstRegistroL006 for the day {0}", MonthlyItem.Date.ToShortDateString()));
        throw;
      }
    }

    /// <summary>
    /// Get ID Event for a Registro_L006
    /// </summary>
    /// <param name="Report"></param>
    /// <param name="MonthlyItem"></param>
    /// <param name="RecordNumber"></param>
    /// <returns></returns>
    private string GetIDEvent(MonthlyReportExcel Report, MonthlyItem MonthlyItem)
    {
      StringBuilder _id_str = new StringBuilder();
      _id_str.Append(MonthlyItem.Date.ToString("yyyyMMdd"));
      _id_str.Append("-");
      //_id_str.Append(MonthlyItem.SessionType == SessionType.Night ? "2" : "1");
      _id_str.Append("-");
      //_id_str.Append(MonthlyItem.ID.ToString("0000"));
      _id_str.Append("-");
      //_id_str.Append(TipoRegistro);
      return _id_str.ToString();
    }

    #endregion
  }
}