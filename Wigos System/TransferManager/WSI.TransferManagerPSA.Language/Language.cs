﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LanguageManager.cs
// 
//   DESCRIPTION: Class with information obtained of the excel file
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Language
{

  public static class LanguageManager
  {

    #region Attributes

    private static ResourceManager m_res_manager = null;
    private static CultureInfo m_culture = null;
    private static LanguageType m_language_type = LanguageType.Spanish;

    #endregion

    #region Constructor


    #endregion

    #region Properties

    /// <summary>
    /// Language used to get values for resources files
    /// </summary>
    public static LanguageType SetLanguageType
    {
      set
      {
        m_language_type = value;

        switch (m_language_type)
        {
          case LanguageType.English:
            m_culture = CultureInfo.CreateSpecificCulture("en");
            break;
          case LanguageType.Spanish:
            m_culture = CultureInfo.CreateSpecificCulture("es");
            break;
          default:
            throw new Exception(string.Format("There is not culture definition for the type language: {0}", m_language_type.ToString()));
        }
      }

      get
      {
        return m_language_type;
      }
    }

    /// <summary>
    /// Returns the current Culture Info
    /// </summary>
    public static CultureInfo CultureInfo
    {
      get
      {
        return m_culture;
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Initializes the static class
    /// </summary>
    public static void Initialize()
    {
      if (m_res_manager == null)
        m_res_manager = new ResourceManager("WSI.TransferManagerPSA.Language.NLS", typeof(LanguageManager).Assembly);
    }

    /// <summary>
    /// Get the value of the resource (by Name) 
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static string GetValue(string Name)
    {
      try
      {
        return (m_res_manager == null ? string.Empty : m_res_manager.GetString(Name, m_culture));
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return "##UNDEFINED [" + Name + "]";
      }
    }

    #endregion

  }
}
