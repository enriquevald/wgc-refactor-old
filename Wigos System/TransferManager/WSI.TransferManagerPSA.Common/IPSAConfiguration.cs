﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  public interface IPSAConfiguration
  {
    string IdOperador{get; set; }
    string Login{get; set; }
    string Password{get; set; }

    string URLPrincipalPSA { get; set; }
    string URLSecundaryPSA { get; set; }
  }
}
