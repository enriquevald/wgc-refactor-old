﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  public static class WinRegistry
  {
    /// <summary>
    /// Set Name/Value pair for a SubKey
    /// </summary>
    /// <param name="SubKey"></param>
    /// <param name="Name"></param>
    /// <param name="Value"></param>
    public static void SetKey(string SubKey, string Name, string Value)
    {
      RegistryKey key;
      key = Registry.CurrentUser.CreateSubKey(SubKey);
      key.SetValue(Name, Value);
      key.Close();
    }

    /// <summary>
    /// Get Value from the Name/Value pair for a SubKey
    /// </summary>
    /// <param name="SubKey"></param>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static string GetKey(string SubKey, string Name)
    {
      RegistryKey key = Registry.CurrentUser.OpenSubKey(SubKey);

      if (key == null)
      {
        //Log.Warning(string.Format(@"No existe la ruta {0}\{1} en el registro de windows", SubKey, Name));
        return null;
      }

      return key.GetValue(Name).ToString();
    }
  }
}
