﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace WSI.TransferManagerPSA.Common
{
  public static class Misc
  {
    public static long GetElapsedTicks(int FirstTick)
    {
      return GetElapsedTicks(FirstTick, GetTickCount());
    }

    public static long GetElapsedTicks(int FirstTick, int LastTick)
    {
      long _elapsed;

      _elapsed = LastTick;
      if (LastTick < FirstTick)
      {
        _elapsed += UInt32.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= FirstTick;

      if (_elapsed < 0)
      {
        Log.Error("*** Computed elapsed time is less than zero.");
        _elapsed = 0;
      }

      return _elapsed;
    }

    public static int GetTickCount()
    {
      return Environment.TickCount;
    }

    /// <summary>
    ///  Delete log files of the log directory
    /// </summary>
    /// <param name="LogPath"></param>
    /// <param name="MaskFile"></param>
    /// <param name="NumDays"></param>
    public static void ClearHistoryLogFiles(String LogPath, String MaskFile, Int32 NumDays)
    {
      DirectoryInfo dir;

      try
      {
        dir = new DirectoryInfo(LogPath);

        // Search all files in directory
        foreach (FileInfo file_info in dir.GetFiles(MaskFile))
        {
          if (file_info.LastWriteTime.AddDays(NumDays) < DateTime.Now)
          {
            file_info.Delete();
          }
        }
      }
      catch
      {
        ;
      }
    }
  }
}
