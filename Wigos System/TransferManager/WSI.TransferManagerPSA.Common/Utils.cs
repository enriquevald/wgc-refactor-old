﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Utils.cs
// 
//   DESCRIPTION: Class with utility methods
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 01-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-JUL-2015 FAV    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  public static class Utils
  {
    /// <summary>
    /// Validates is the path is a Excel file
    /// </summary>
    /// <param name="pathFile"></param>
    /// <returns></returns>
    public static bool IsExcelFileValid(string pathFile)
    {
      try
      {
        if (!File.Exists(pathFile))
          return false;

        return (Path.GetExtension(pathFile) == ".xls" || Path.GetExtension(pathFile) == ".xlsx");
      }
      catch
      {
        return false;
      }
    }

    /// <summary>
    /// Validates if a string is a number
    /// </summary>
    /// <param name="valor"></param>
    /// <returns></returns>
    public static Boolean IsNumeric(string valor)
    {
      float result;
      return (float.TryParse(valor, out result));
    }

    /// <summary>
    /// Validates if a string is a datetime
    /// </summary>
    /// <param name="valor"></param>
    /// <returns></returns>
    public static Boolean IsDateTime(string valor)
    {
      DateTime result;
      return (DateTime.TryParse(valor, out result));
    }

    /// <summary>
    /// Get date with time to 00:00:00
    /// </summary>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static DateTime GetDateWithoutTime(DateTime Source)
    {
      return new DateTime(Source.Year, Source.Month, Source.Day, 0, 0, 0, 0);
    }

    /// <summary>
    /// Convert decimal value to currency string
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="cultureName"></param>
    /// <returns></returns>
    public static string DecimalToCurrencyString(decimal Value, string cultureName)
    {
      CultureInfo culture = CultureInfo.CreateSpecificCulture(cultureName);
      culture.NumberFormat.CurrencyNegativePattern = 1;

      return string.Format(culture, "{0:c}", Value);
    }

    /// <summary>
    /// Convert currency string value to decimal
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="cultureName"></param>
    /// <returns></returns>
    public static decimal CurrencyStringToDecimal(string Value, string cultureName)
    {
      return decimal.Parse(Value, NumberStyles.Currency, new CultureInfo(cultureName));
    }

    /// <summary>
    /// Convert decimal string value to decimal
    /// </summary>
    /// <param name="Value"></param>
    /// <param name="cultureName"></param>
    /// <returns></returns>
    public static decimal DecimalStringToDecimal(string Value, string cultureName)
    {
      return decimal.Parse(Value, NumberStyles.AllowDecimalPoint, new CultureInfo(cultureName));
    }

    /// <summary>
    /// Convert an object to a XML string
    /// </summary>
    /// <param name="XmlObject">Object to convert</param>
    /// <param name="XmlStr">XML string</param>
    /// <returns></returns>
    public static Boolean ObjectSerializableToXml(Object XmlObject, out String XmlStr)
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;
      StreamWriter _stream_writer;
      StreamReader _stream_reader;
      XmlStr = "";

      try
      {
        using (MemoryStream _memory_stream = new System.IO.MemoryStream())
        {
          _stream_writer = new System.IO.StreamWriter(_memory_stream);
          _stream_reader = new System.IO.StreamReader(_memory_stream);

          _xml_ser = new System.Xml.Serialization.XmlSerializer(XmlObject.GetType());
          _xml_ser.Serialize(_stream_writer, XmlObject);

          _memory_stream.Position = 0;
          XmlStr = _stream_reader.ReadToEnd();
        }
        return true;
      }
      catch
      {
        XmlStr = string.Empty;
        return false;
      }
    }

    public static void Sleep(int Miliseconds)
    {
      Thread.Sleep(Miliseconds);
    }

    public static void StartThread(ThreadStart target)
    {
      Thread _thread = new Thread(new ThreadStart(target));
      _thread.Name = target.ToString();
      _thread.Start();
    }

  }
}
