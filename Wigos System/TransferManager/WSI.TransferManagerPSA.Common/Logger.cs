﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  /// <summary>
  /// Logger interface
  /// </summary>
  public interface ILogger
  {
    void WriteLine(String LogMsg);
    void Close();
    void Close(String CloseMsg);
  }

  /// <summary>
  /// The common logger
  /// </summary>
  public static class Log
  {
    private static ArrayList listeners = new ArrayList();

    private static void WriteLine(String Log)
    {
      foreach (ILogger logger in listeners)
      {
        logger.WriteLine(Log);
      }
    }

    public static void Close()
    {
      foreach (ILogger logger in listeners)
      {
        logger.Close();
      }
    }

    public static void Error(String Msg)
    {
      WriteLine("Error:" + Msg);
    }

    public static void Warning(String Msg)
    {
      WriteLine("Warning:" + Msg);
    }

    public static void Warning(String ProcessName, Int32 AcceptTimeToLogger, Int32 NumItems, Int32 Tick0)
    {
      long _elapsed;
      Int32 _accept_time;
      Int32 _env_value;

      _elapsed = Misc.GetElapsedTicks(Tick0);

      _accept_time = AcceptTimeToLogger;
      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESS_TIME_LOGGER")))
      {
        if (Int32.TryParse(Environment.GetEnvironmentVariable("PROCESS_TIME_LOGGER"), out _env_value))
        {
          _accept_time = _env_value;
        }
      }

      if (_elapsed > _accept_time)
      {
        WriteLine("Warning: " + "Delayed Process Time: " + _elapsed.ToString() + ", NumItems: " + NumItems.ToString() + " ----> " + ProcessName);
      }
    }

    public static void Message(String Msg)
    {
      WriteLine(Msg);
    }

    public static void Exception(Exception Ex)
    {
      StringBuilder _sb;
      Exception _ex;
      int _nn;

      _sb = new StringBuilder();
      _sb.Append("EXCEPTION: ");
      _sb.AppendLine(Ex.Message);

      _nn = 1;
      for (_ex = Ex.InnerException; _ex != null; _ex = _ex.InnerException)
      {
        _sb.Append("- INNER #");
        _sb.Append(_nn.ToString());
        _sb.Append(": ");
        _sb.AppendLine(_ex.Message);
        _nn++;
      }
      _sb.Append("- STACK: ");
      try
      {
        _sb.AppendLine(Ex.StackTrace);
      }
      catch { }

      WriteLine(_sb.ToString());
    }

    public static void AddListener(ILogger NewLogger)
    {
      lock (listeners)
      {
        listeners.Add(NewLogger);
      }
    }
  }

  /// <summary>
  /// A simple logger
  /// </summary>
  public class SimpleLogger : ILogger
  {
    private const string LOG_FOLDER_DEFAULT = @"LogFiles\";

    private DateTime m_next_history_clear;
    private WaitableQueue m_log_queue;
    private String m_base_name;
    private Boolean m_stopping = false;
    private Boolean m_stop = false;
    private System.Threading.Thread th;

    public SimpleLogger(String BaseName)
    {
      DirectoryInfo dir;

      m_log_queue = new WaitableQueue();

      dir = new DirectoryInfo(LOG_FOLDER_DEFAULT);
      if (!dir.Exists)
      {
        dir.Create();
      }

      m_base_name = BaseName;

      Misc.ClearHistoryLogFiles(LOG_FOLDER_DEFAULT, @"*.txt", 31);
      m_next_history_clear = DateTime.Now.AddDays(1).Date;

      th = new System.Threading.Thread(SimpleLoggerThread);
      th.Start();
    }

    public void Close(String Log)
    {
      m_stopping = true;
      m_log_queue.Enqueue(Log); // Don't remove this line
      m_stop = true;
      th.Join(5000);
    }

    public void Close()
    {
      Close("");
    }

    public void WriteLine(String Log)
    {
      if (m_stopping)
      {
        return;
      }
      m_log_queue.Enqueue(Log);
    }

    public static String FormatEvent(String Event)
    {
      String _event;
      StringBuilder _sb;
      String _tab;
      String _new_line;
      String _log_msg;
      DateTime _date;

      _sb = new StringBuilder();
      _sb.AppendLine();
      _new_line = _sb.ToString();

      _date = DateTime.Now;
      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");

      _tab = "";
      while (_tab.Length < _log_msg.Length - 1)
      {
        _tab += " ";
      }
      _tab = _new_line + _tab + "| ";

      _event = Event;
      while (_event.StartsWith(_new_line))
      {
        _event = _event.Substring(_event.IndexOf(_new_line) + _new_line.Length);
      }
      while (_event.EndsWith(_new_line))
      {
        _event = _event.Substring(0, _event.LastIndexOf(_new_line));
      }

      _log_msg = _date.Year.ToString("0000") + "/" + _date.Month.ToString("00") + "/" + _date.Day.ToString("00");
      _log_msg += " " + _date.Hour.ToString("00") + ":" + _date.Minute.ToString("00") + ":" + _date.Second.ToString("00");
      _log_msg += "." + _date.Millisecond.ToString("000");
      _event = _event.Replace(_new_line, _tab);
      _log_msg += " " + _event;

      return _log_msg;
    }

    /// <summary>
    /// Thread to write into logger
    /// </summary>
    private void SimpleLoggerThread()
    {
      String _event;
      String _log_msg;
      String _log_filename;
      DateTime _date;      // Date
      DateTime _prev_date;


      _prev_date = DateTime.Now.AddDays(-1);

      while (true)
      {
        Object _item;

        if (m_stop)
        {
          if (m_log_queue.Count == 0)
          {
            break;
          }
        }

        if (m_log_queue.Dequeue(out _item, 1 * 60 * 60 * 1000)) // 1 Hour ...
        {
          _event = (String)_item;
        }
        else
        {

          _event = "...";
        }

        if (m_next_history_clear <= DateTime.Now)
        {
          Misc.ClearHistoryLogFiles(LOG_FOLDER_DEFAULT, @"*.txt", 31);
          m_next_history_clear = DateTime.Now.AddDays(1).Date;
        }

        for (int i = 0; i < 3; i++)
        {
          try
          {
            _date = DateTime.Now;
            _log_filename = LOG_FOLDER_DEFAULT + m_base_name + "_" + _date.Year.ToString("0000") + _date.Month.ToString("00") + _date.Day.ToString("00") + "_" + Environment.MachineName + ".txt";

            using (StreamWriter _log_file = new StreamWriter(_log_filename, true))
            {
              _log_msg = FormatEvent(_event);

              _log_file.WriteLine(_log_msg);
              _log_file.Close();
            }
            break;
          }
          catch
          {
            System.Threading.Thread.Sleep(1000);
          }
        }

      } // while

    } // SimpleLoggerThread
  }
}
