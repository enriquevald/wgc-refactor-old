﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  public enum ReportedFileType
  { 
    Daily_Detail,
    Monthly_Summary
  }

  public enum MessageBoxType
  {
    Error,
    Question,
    Information,
    Warning,
  }

  public enum LanguageType
  { 
    None,
    Spanish,
    English,
  }

  public enum ReportTabType
  { 
    Daily_Session_Day,
    Daily_Session_Night,
    Monthly_Bingo,
    Monthly_Tables,
  }

  public enum ConnectionStep
  {
    CONNECTION_STARTING,
    CONNECTION_PRINCIPAL_URL_CONNECTING,
    CONNECTION_PRINCIPAL_URL_CONNECTED,
    CONNECTION_PRINCIPAL_URL_ERROR,
    CONNECTION_SECONDARY_URL_CONNECTING,
    CONNECTION_SECONDARY_URL_CONNECTED,
    CONNECTION_SECONDARY_URL_ERROR,
    CONNECTION_ERROR,
  }
}
