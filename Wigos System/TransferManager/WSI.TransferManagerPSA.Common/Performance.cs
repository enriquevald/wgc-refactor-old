﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Common
{
  static public class Performance
  {
    private static Stopwatch m_stop_watch = Stopwatch.StartNew();

    public static TimeSpan GetTickCount()
    {
      return m_stop_watch.Elapsed;
    }

    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample)
    {
      return GetElapsedTicks(FirstSample, m_stop_watch.Elapsed);
    }

    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample, TimeSpan LastSample)
    {
      ulong _t0;
      ulong _t1;
      ulong _elapsed;

      _t0 = (ulong)(FirstSample.Ticks);
      _t1 = (ulong)(LastSample.Ticks);
      _elapsed = _t1;
      if (_t1 < _t0)
      {
        _elapsed += ulong.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= _t0;

      if (_elapsed > long.MaxValue)
      {
        _elapsed = long.MaxValue;
      }

      return new TimeSpan((long)_elapsed);
    }
  }
}
