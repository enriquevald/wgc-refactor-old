﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Client
{
  public class PSASession: IPSASession
  {
    public String ID { get; set; }

    public int ConnectionExpiration { get; set; }

    public int MaxNumberResendAttempts { get; set; }

    public int NumberRecordsPagination { get; set; }
  }
}
