﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Client
{
  public interface IPSASession
  {
    /// <summary>
    /// ID Session
    /// </summary>
    String ID {get; set;}

    /// <summary>
    /// Expiration of connection
    /// </summary>
    int ConnectionExpiration {get; set;}

    /// <summary>
    /// Maximum number of resend attempts
    /// </summary>
    int MaxNumberResendAttempts { get; set; }

    /// <summary>
    /// Number of records of pagination by the report
    /// </summary>
    int NumberRecordsPagination {get; set;}
  }
}
