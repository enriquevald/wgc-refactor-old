﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PSAClient.cs
// 
//   DESCRIPTION: Class to manage the calls to the PSA web service
// 
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 06-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-JUL-2015 FAV    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WSI.TransferManagerPSA.Client.PSA_Refence;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Client
{
  public class PSAClient : IPSAClient
  {

    #region Constants

    /// <summary>
    /// Time in miliseconds to recall a web method in the PSA service
    /// </summary>
    private const int PSA_SLEEP_SEND_COMMAND_AGAIN = 100;

    #endregion

    #region Attributes

    public event EventHandler<PSAConnectionEventArgs> ConnectionProcessEvent;

    private string m_id_session = string.Empty;
    private string m_id_transation = string.Empty;
    private PSAServiceClient m_service_client;
    private IPSAConfiguration m_configuration;
    private bool m_enable_conn_events = false;
    private string m_response_error_description = string.Empty;
    private PSASession m_psa_session = null;

    /// <summary>
    /// Maximum number of retries to recall a web method in the PSA Service
    /// </summary>
    private int m_max_number_resend = 0;

    #endregion

    #region Constructor

    public PSAClient(IPSAConfiguration Configuration)
    {
      m_service_client = new PSAServiceClient();
      m_configuration = Configuration;
      m_enable_conn_events = true;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Current Id to the opened session
    /// </summary>
    public string IdSesion
    {
      get
      {
        return m_id_session;
      }
    }

    /// <summary>
    /// Current Id Transaction returned by a call to SendFase1
    /// </summary>
    public string IdTransaction
    {
      get
      {
        return m_id_transation;
      }
    }

    /// <summary>
    /// PSA Session information
    /// </summary>
    public IPSASession Session
    {
      get
      {
        return m_psa_session;
      }
    }

    /// <summary>
    /// Configuration
    /// </summary>
    public IPSAConfiguration Configuration
    {
      get
      {
        return m_configuration;
      }
      set
      {
        m_configuration = value;
      }
    }

    /// <summary>
    /// Get the last error message for a session
    /// </summary>
    public string LastMessageError
    {
      get
      {
        return m_response_error_description;
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Start a Session with the PSA Service
    /// </summary>
    /// <returns></returns>
    public ResponseCode OpenSession()
    {
      return OpenSession(true);
    }

    /// <summary>
    /// Start a Session with the PSA Service. Enable or disable connection events during the operation
    /// </summary>
    /// <returns></returns>
    public ResponseCode OpenSession(bool EnableConnectionEvents)
    {
      m_response_error_description = string.Empty;
      m_enable_conn_events = EnableConnectionEvents;
      m_psa_session = null;

      // Max resend value by default. This value will be updated if there is connection
      m_max_number_resend = 3;

      PSA_InicioSesion _open_session;
      PSA_RespuestaInicioSesion _response_open_session = null;
      bool _is_connected = false;
      m_id_session = string.Empty;

      SendConnectionEvent(ConnectionStep.CONNECTION_STARTING, 1);

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          SendConnectionEvent(ConnectionStep.CONNECTION_SECONDARY_URL_ERROR, i + 1);
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not connect using the URL Secondary");
        }

        _open_session = new PSA_InicioSesion();
        _open_session.Body = new PSA_InicioSesionBody()
        {
          IdOperador = m_configuration.IdOperador,
          Login = m_configuration.Login,
          Password = m_configuration.Password,
        };

        Log.Message(string.Format("Starting connection [{0}]", i + 1));
        SetAddressToCurrentClient(m_configuration.URLPrincipalPSA);

        SendConnectionEvent(ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTING, i + 1);

        _is_connected = OpenConnection(_open_session, out _response_open_session);

        if (_is_connected && _response_open_session.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message(string.Format("The session was opened successfully"));
          m_id_session = _response_open_session.Body.IdSesion;
          SendConnectionEvent(ConnectionStep.CONNECTION_PRINCIPAL_URL_CONNECTED, i + 1);
          BuildSessionObject(_response_open_session);
          return ResponseCode.PSA_RC_OK;
        }
        else
        {
          // Try with the secundary URL
          Log.Warning(string.Format("The client could not connect using the URL Principal", i + 1));
          SendConnectionEvent(ConnectionStep.CONNECTION_PRINCIPAL_URL_ERROR, i + 1);

          SetAddressToCurrentClient(m_configuration.URLSecundaryPSA);

          SendConnectionEvent(ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTING, i + 1);
          _is_connected = OpenConnection(_open_session, out _response_open_session);

          if (_is_connected && _response_open_session.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Message(string.Format("The session was opened successfully using the PSA URL Secondary"));
            m_id_session = _response_open_session.Body.IdSesion;
            SendConnectionEvent(ConnectionStep.CONNECTION_SECONDARY_URL_CONNECTED, i + 1);
            BuildSessionObject(_response_open_session);
            return ResponseCode.PSA_RC_OK;
          }
        }
      }

      SendConnectionEvent(ConnectionStep.CONNECTION_ERROR, m_max_number_resend);
      m_id_session = string.Empty;

      if (_is_connected)
      {
        Log.Error("The client could not open the session with the PSA Service. Response: " + _response_open_session.Body.CodigoRespuesta + ", " + _response_open_session.Body.CodigoRespuestaDescripcion);
        m_response_error_description = _response_open_session.Body.CodigoRespuestaDescripcion;
        return GetResponseCode(_response_open_session.Body.CodigoRespuesta);
      }
      else
      {
        Log.Error("The client could not open the session with the PSA Service. The server didn't answer");
        return ResponseCode.PSA_NOT_RESPONSE;
      }
    }

    /// <summary>
    /// Close the current Session with the PSA Service
    /// </summary>
    /// <returns></returns>
    public ResponseCode CloseSession()
    {
      PSA_CierreSesion _close_session;
      PSA_RespuestaCierreSesion _response_close_session = null;

      if (string.IsNullOrEmpty(m_id_session))
        throw new Exception("The current session is closed or doesn't exist");

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not close the session. Starting new try");
        }

        _close_session = new PSA_CierreSesion();
        _close_session.Body = new PSA_CierreSesionBody()
        {
          IdSesion = m_id_session,
        };

        Log.Message(string.Format("Closing session with the PSA Server [{0}]", i + 1));
        _response_close_session = m_service_client.PSA_CierreSesion(_close_session);

        if (_response_close_session.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          m_id_session = string.Empty;
          m_id_transation = string.Empty;
          Log.Message("The session was closed successfully");
          return ResponseCode.PSA_RC_OK;;
        }
      }

      Log.Error("The client could not close the session with the PSA Service. Response: " + _response_close_session.Body.CodigoRespuesta + ", " + _response_close_session.Body.CodigoRespuestaDescripcion);
      m_response_error_description = _response_close_session.Body.CodigoRespuestaDescripcion;
      return GetResponseCode(_response_close_session.Body.CodigoRespuesta);
    }

    /// <summary>
    /// Call to Fase1 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    public bool SendFase1(PSA_ReporteFase1_ContextoBody Body)
    {
      PSA_RespuestaReporteFase1_Contexto _responseF1 = null;
      PSA_ReporteFase1_Contexto _requestF1 = new PSA_ReporteFase1_Contexto() {Body = Body};

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the ReporteFase1. Starting new try");
        }

        Log.Message(string.Format("Sending ReporteFase1 to the PSA Service [{0}]", i + 1));

        _responseF1 = m_service_client.PSA_ReporteFase1_Contexto(_requestF1);

        if (_responseF1.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The ReporteFase1 was sent successfully");
          m_id_transation = _responseF1.Body.IdTransaccion;
          return true;
        }
        else
        {
          Log.Error("Response Fase1: " + _responseF1.Body.CodigoRespuestaDescripcion);
        }
      }

      Log.Error("The PSA_ReporteFase1_Contexto has returned an error. Response: " + _responseF1.Body.CodigoRespuesta);
      m_response_error_description = _responseF1.Body.CodigoRespuestaDescripcion;
      m_id_transation = string.Empty;
      return false;
    }

    /// <summary>
    ///  Call to Fase2 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    public bool SendFase2(PSA_ReporteFase2_ReferenciaLineaBody Body)
    {
      PSA_RespuestaReporteFase2_ReferenciaLinea _responseF2 = null;
      PSA_ReporteFase2_ReferenciaLinea _requestF2 = new PSA_ReporteFase2_ReferenciaLinea() {Body = Body};

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the ReporteFase2. Starting new try");
        }

        Log.Message(string.Format("Sending ReporteFase2 to the PSA Service [{0}]", i + 1));

        _responseF2 = m_service_client.PSA_ReporteFase2_ReferenciaLinea(_requestF2);

        if (_responseF2.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The ReporteFase2 was sent successfully");
          return true;
        }
        else
        {
          Log.Error("Response Fase2: " + _responseF2.Body.CodigoRespuestaDescripcion);
        }
      }

      Log.Error("The PSA_ReporteFase2_ReferenciaLinea has returned an error. Response: " + _responseF2.Body.CodigoRespuesta + ", " + _responseF2.Body.CodigoRespuestaDescripcion);
      m_response_error_description = _responseF2.Body.CodigoRespuestaDescripcion;
      return false;
    }

    /// <summary>
    /// Call to Fase3 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    public bool SendFase3(PSA_ReporteFase3_RegistrosLineaBody Body)
    {
      PSA_RespuestaReporteFase3_RegistrosLinea _responseF3 = null;
      PSA_ReporteFase3_RegistrosLinea _requestF3 = new PSA_ReporteFase3_RegistrosLinea() {Body = Body};

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the ReporteFase3. Starting new try");
        }

        Log.Message(string.Format("Sending ReporteFase3 to the PSA Service [{0}]", i + 1));
        _responseF3 = m_service_client.PSA_ReporteFase3_RegistrosLinea(_requestF3);

        if (_responseF3.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The ReporteFase3 was sent successfully");
          return true;
        }
        else
        {
          Log.Error("Response Fase3: " + _responseF3.Body.CodigoRespuestaDescripcion);
        }
      }

      Log.Error("The PSA_ReporteFase3_RegistrosLinea has returned an error. Response: " + _responseF3.Body.CodigoRespuesta);
      m_response_error_description = _responseF3.Body.CodigoRespuestaDescripcion;
      return false;
    }

    /// <summary>
    /// Call to Fase4 of the PSA service
    /// </summary>
    /// <returns></returns>
    public bool SendFase4(PSA_ReporteFase4_TransaccionCompletadaBody Body)
    {
      PSA_RespuestaReporteFase4_TransaccionCompletada _responseF4 = null;
      PSA_ReporteFase4_TransaccionCompletada _requestF4 = new PSA_ReporteFase4_TransaccionCompletada(){Body = Body};

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the ReporteFase4. Starting new try");
        }

        Log.Message(string.Format("Sending ReporteFase4 to the PSA Service [{0}]", i + 1));
        _responseF4 = m_service_client.PSA_ReporteFase4_TransaccionCompletada(_requestF4);

        if (_responseF4.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The ReporteFase4 was sent successfully");
          return true;
        }
        else
        {
          Log.Error("Response Fase4: " + _responseF4.Body.CodigoRespuestaDescripcion);
        }
      }

      Log.Error("The PSA_ReporteFase4_TransaccionCompletada has returned an error. Response: " + _responseF4.Body.CodigoRespuesta);
      m_response_error_description = _responseF4.Body.CodigoRespuestaDescripcion;
      return false;
    }

    /// <summary>
    /// Checks if the report (by date) has been sent to the PSA service
    /// </summary>
    /// <param name="ReportDate"></param>
    /// <param name="StatusReportSent"></param>
    /// <returns></returns>
    public bool RequestPendingReports(int Year, int Month, out List<DiaPendiente> PendingDays)
    {
      PSA_RespuestaConsultaReportesPendientes _response = null;
      PSA_ConsultaReportesPendientes _pending_consultation;
      PendingDays = null;

      _pending_consultation = new PSA_ConsultaReportesPendientes();
      _pending_consultation.Body = new PSA_ConsultaReportesPendientesBody()
                 {
                  IdSesion = m_id_session,
                  Periodo = Year.ToString("0000") + "-" + Month.ToString("00")
                 };

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the ConsultaReportesPendientes. Starting new try");
        }

        Log.Message(string.Format("Sending ConsultaReportesPendientes to the PSA Service [{0}]", i + 1));
        _response = m_service_client.PSA_ConsultaReportesPendientes(_pending_consultation);

        if (_response.Body.PSA_Respuesta.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The ConsultaReportesPendientes was sent successfully");
          PendingDays = _response.Body.DatosConsulta;
          return true;
        }
      }

      Log.Error("The PSA_ConsultaReportesPendientes has returned an error. Response: " + _response.Body.PSA_Respuesta.CodigoRespuesta);
      m_response_error_description = _response.Body.PSA_Respuesta.CodigoRespuestaDescripcion;
      return false;
    }

    /// <summary>
    /// Check the Operator Catalog
    /// </summary>
    /// <param name="OperatorCatalog"></param>
    /// <returns></returns>
    public bool CatalogosOperador(PSA_Catalogos_Operador OperatorCatalog)
    {
      PSA_RespuestaCatalogosOperador _response = null;
      PSA_CatalogosOperador _requestCA = new PSA_CatalogosOperador()
      {
        Body = new PSA_CatalogosOperadorBody()
        {
          CatalogosRequeridosDelOperador = OperatorCatalog,
          ChecksumContenido = "myChecksum",
          IdSesion = m_id_session,
        }
      };

      for (int i = 0; i < m_max_number_resend; ++i)
      {
        if (i > 0)
        {
          Utils.Sleep(PSA_SLEEP_SEND_COMMAND_AGAIN);
          Log.Warning("The client could not send the CatalogosOperador. Starting new try");
        }

        Log.Message(string.Format("Sending CatalogosOperador to the PSA Service [{0}]", i + 1));

        _response = m_service_client.PSA_CatalogosOperador(_requestCA);

        if (_response.Body.CodigoRespuesta == ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Message("The CatalogosOperador was sent successfully");
          return true;
        }
      }

      Log.Error("The PSA_CatalogosOperador has returned an error. Response: " + _response.Body.CodigoRespuesta);
      m_response_error_description = _response.Body.CodigoRespuestaDescripcion;
      return false;
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Convert string reponse to enum response
    /// </summary>
    /// <param name="CodigoRespuesta"></param>
    /// <returns></returns>
    private ResponseCode GetResponseCode(string CodigoRespuesta)
    {
      try
      {
        return (ResponseCode)Enum.Parse(typeof(ResponseCode), CodigoRespuesta);
      }
      catch
      {
        Log.Error(string.Format("The type of error returned by the PSA Server doesn't exist in the Client. Response [{0}]", CodigoRespuesta));
        return ResponseCode.PSA_UNKNOW_EXCEPTION;
      }
    }

    /// <summary>
    /// Raise an connection event
    /// </summary>
    /// <param name="Step"></param>
    /// <param name="AttemptNumber"></param>
    private void SendConnectionEvent(ConnectionStep Step, int AttemptNumber)
    {
      if (m_enable_conn_events && ConnectionProcessEvent != null)
      {
        PSAConnectionEventArgs e = new PSAConnectionEventArgs()
        {
          ConnectionStep = Step,
          AttemptNumbers = AttemptNumber,
        };

        ConnectionProcessEvent(this, e);
      }
    }

    /// <summary>
    /// Open the connection and detect errors with the server
    /// </summary>
    private bool OpenConnection(PSA_InicioSesion StartSession, out PSA_RespuestaInicioSesion ResponseServer)
    {
      ResponseServer = null;

      try
      {
        ResponseServer = m_service_client.PSA_InicioSesion(StartSession);
        return true;
      }
      catch (EndpointNotFoundException)
      {
        Log.Error("The server [" + m_service_client.Endpoint.Address.ToString() + "] not found. Check the PSA server address or your connection.");
        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Error("Exception reported in the communication with the server [" + m_service_client.Endpoint.Address.ToString() + "]");
        return false;
      }
    }

    /// <summary>
    /// Set the address to the Client
    /// </summary>
    /// <param name="Address"></param>
    private void SetAddressToCurrentClient(string Address)
    {
      BasicHttpBinding binding = new BasicHttpBinding();

      // Set Address
      m_service_client.Endpoint.Address = new EndpointAddress(Address);

      if (Address.StartsWith("https"))
      {
        binding.Security.Mode = BasicHttpSecurityMode.Transport;

        // Necessary to jump the security certificate authorization
        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
      }
      else
      {
        binding.Security.Mode = BasicHttpSecurityMode.None;
      }

      // Set binding
      m_service_client.Endpoint.Binding = binding;
    }

    /// <summary>
    /// Build PSASession object to a opened session
    /// </summary>
    /// <param name="ResponseServer"></param>
    private void BuildSessionObject(PSA_RespuestaInicioSesion ResponseServer)
    {
      m_psa_session = new PSASession()
      {
        ConnectionExpiration = ResponseServer.Body.PSA_Param_ExpiracionConexion,
        ID = ResponseServer.Body.IdSesion,
        MaxNumberResendAttempts = ResponseServer.Body.PSA_Param_MaxNumIntentosReenvio,
        NumberRecordsPagination = ResponseServer.Body.PSA_Param_NumRegsPaginacionReporte,
      };

      // update the value of the max number
      m_max_number_resend = m_psa_session.MaxNumberResendAttempts - 1;
    }

    #endregion
    
  }
}
