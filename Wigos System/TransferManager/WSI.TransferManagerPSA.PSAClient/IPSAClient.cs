﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Client.PSA_Refence;

namespace WSI.TransferManagerPSA.Client
{
  public interface IPSAClient
  {

    #region Events

    /// <summary>
    /// Connection event raised in the step of the connection
    /// </summary>
    event EventHandler<PSAConnectionEventArgs> ConnectionProcessEvent;

    #endregion

    #region Properties

    /// <summary>
    /// Current Id to the opened session
    /// </summary>
    string IdSesion { get; }

    /// <summary>
    /// Current Id Transaction
    /// </summary>
    string IdTransaction { get; }

    #endregion

    #region Public Methods

    /// <summary>
    /// Start a Session with the PSA Service
    /// </summary>
    /// <returns></returns>
    ResponseCode OpenSession();

    /// <summary>
    /// Start a Session with the PSA Service. Enable or disable connection events during the operation
    /// </summary>
    /// <returns></returns>
    ResponseCode OpenSession(bool EnableConnectionEvents);

    /// <summary>
    /// Close the current Session with the PSA Service
    /// </summary>
    /// <returns></returns>
    ResponseCode CloseSession();

    /// <summary>
    /// Call to Fase1 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    bool SendFase1(PSA_ReporteFase1_ContextoBody Body);

    /// <summary>
    ///  Call to Fase2 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    bool SendFase2(PSA_ReporteFase2_ReferenciaLineaBody Body);

    /// <summary>
    /// Call to Fase3 of the PSA service
    /// </summary>
    /// <param name="Report"></param>
    /// <returns></returns>
    bool SendFase3(PSA_ReporteFase3_RegistrosLineaBody Body);

    /// <summary>
    /// Call to Fase4 of the PSA service
    /// </summary>
    /// <returns></returns>
    bool SendFase4(PSA_ReporteFase4_TransaccionCompletadaBody Body);

    /// <summary>
    /// Checks if the report (by date) has been sent to the PSA service
    /// </summary>
    /// <param name="ReportDate"></param>
    /// <param name="StatusReportSent"></param>
    /// <returns></returns>
    bool RequestPendingReports(int Year, int Month, out List<DiaPendiente> PendingDays);

    /// <summary>
    /// Check the Operator Catalog
    /// </summary>
    /// <param name="OperatorCatalog"></param>
    /// <returns></returns>
    bool CatalogosOperador(PSA_Catalogos_Operador OperatorCatalog);

    #endregion

  }
}
