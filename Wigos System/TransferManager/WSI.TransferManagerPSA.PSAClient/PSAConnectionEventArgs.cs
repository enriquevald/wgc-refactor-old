﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.TransferManagerPSA.Common;

namespace WSI.TransferManagerPSA.Client
{
  public class PSAConnectionEventArgs: EventArgs
  {
    public ConnectionStep ConnectionStep { get; set; }

    public int AttemptNumbers { get; set; }
  }
}
