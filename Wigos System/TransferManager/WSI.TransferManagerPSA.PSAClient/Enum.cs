﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.TransferManagerPSA.Client
{
  public enum ResponseCode
  {
    PSA_RC_OK,
    PSA_RC_SESSION_TIMEOUT,
    PSA_RC_BAD_SEQID,
    PSA_RC_BAD_MESSAGE_TYPE,
    PSA_RC_BAD_CHECKSUM,
    PSA_RC_BAD_MESSAGE_DATA,
    PSA_RC_INCOHERENCE_WITH_DB_DATA,
    PSA_RC_INTERNAL_PSA_ERROR,
    PSA_RC_BAD_LOGIN,
    PSA_RC_TOO_MANY_RETRYS,
    PSA_RC_PERIOD_EXPIRED,
    PSA_NOT_RESPONSE,
    PSA_UNKNOW_EXCEPTION
  }
}
