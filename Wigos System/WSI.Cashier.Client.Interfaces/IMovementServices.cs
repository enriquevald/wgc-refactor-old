﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.Client.Interfaces.Messages;

namespace WSI.Cashier.Client.Interfaces
{
    public interface IMovementServices
    {
      IEnumerable<Movement> GetMovements();
    }
}
