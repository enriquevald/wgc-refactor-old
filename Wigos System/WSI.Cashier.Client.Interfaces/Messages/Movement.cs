﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Cashier.Client.Interfaces.Messages
{
  public class Movement
  {
    public long Id { get; set; }
    public int? TerminalId { get; set; }
    public int? TerminalCashierId { get; set; }
    public int? UserId { get; set; }
    public int? CashierId { get; set; }
    public int Type { get; set; }
    public DateTime MovementDate { get; set; }
    public int Status { get; set; }
    public long? CashierSessionId { get; set; }
    public long? SourceTargetId { get; set; }
    public long? CageSessionId { get; set; }
    public long? RelatedMovementId { get; set; }
    public long? TargetType { get; set; }
    public int? GamingTableId { get; set; }
    public double? GamingTableVisits { get; set; }
    public long? CollectionId { get; set; }
    public int? CancellationUserId { get; set; }
    public DateTime? CancelationDate { get; set; }
    public long? RelateId { get; set; }
  }


  public class MovementTable
  {
    public Int32 CM_TYPE { get; set; }
    public string TYPE_NAME { get; set; }
    public DateTime CM_DATE { get; set; }
    public string CM_USER_NAME { get; set; }
    public string CT_NAME { get; set; }
    public int? CM_CASHIER_ID { get; set; }
    public decimal CM_INITIAL_BALANCE { get; set; }
    public string CM_SUB_AMOUNT_STR { get; set; }
    public string CM_ADD_AMOUNT_STR { get; set; }
    public decimal CM_FINAL_BALANCE { get; set; }
    public string Column1 { get; set; }
    public string COLOR_ROW { get; set; }
    public string CM_DETAILS { get; set; }
    public decimal CM_ADD_AMOUNT { get; set; }
    public decimal CM_SUB_AMOUNT { get; set; }
    public string CM_CURRENCY_ISO_CODE { get; set; }
    public int CM_UNDO_STATUS { get; set; }
    public decimal CM_CURRENCY_DENOMINATION { get; set; }
    public long CM_AUX_AMOUNT { get; set; }
    public long CM_CAGE_CURRENCY_TYPE { get; set; }

  }
}
