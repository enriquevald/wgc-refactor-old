﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Cashier.Client.Interfaces.Messages
{
  public class Session
  {
    public long Id { get; set; }
    public string Name { get; set; }
    public DateTime OpeningDate { get; set; }
    public DateTime? ClosingDate { get; set; }
    public long CashierId { get; set; }
    public long UserId { get; set; }
    public long CloseUserId { get; set; }
  }
}
