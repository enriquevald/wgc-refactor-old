﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using System.Drawing;
using System.Globalization;
using System.Net.Configuration;
using System.Security;
using System.Security.Permissions;
using System.Threading;
using WSI.IDScanner.Model;
using WSI.IDScanner.Model.Enums;
using System.Timers;
using System.Windows.Forms;
using WSI.Common;
using Timer = System.Timers.Timer;

namespace IDScannerConnectorSnapShell
{
  public class SnapShellConnector : IDScannerDeviceBase
  {

    private readonly string m_licence_snapshell;


    #region Private Properties

    public SnapShellConnector()
    {
      m_licence_snapshell = GeneralParam.GetString(String.Format("IDCardScanner.{0:00}", 01), "License");
    }


    #endregion
    public override string Name
    {
      get { return @"Snapshell"; }
    }
    public SCANWLib.IdData IDData
    {
      get { return m_reader; }
    }

    public SCANWEXLib.IdData IDDataEx
    {
      get
      {
        return m_reader_ex;
      }
    }

    public enum SlibErr
    {
      SlibErrNone = 1,
      SlibErrInvalidScanner = -1,
      SlibErrScannerGeneralFail = -2,
      SlibErrCanceledByUser = -3,
      SlibErrScannerNotFound = -4,
      SlibErrHardwareError = -5,
      SlibErrPaperFedError = -6,
      SlibErrScanabort = -7,
      SlibErrNoPaper = -8,
      SlibErrPaperJam = -9,
      SlibErrFileIoError = -10,
      SlibErrPrinterPortUsed = -11,
      SlibErrOutOfMemory = -12,
      SlibErrBadWidthParam = -2,
      SlibErrBadHeightParam = -3,
      SlibErrBadParam = -2,
      SlibLibraryAlreadyInitialized = -13,
      SlibErrDriverNotFound = -14,
      SlibErrScannerBussy = -15,
      SlibErrImageConversion = -16,
      SlibUnloadFailedBadParent = -17,
      SlibNotInitilized = -18,
      SlibLibraryAlreadyUsedByOtherApp = -19,
      SlibConflictWithInoutscanParam = -20,
      SlibConflictWithScanSizeParam = -21,
      SlibNoSupportMultipleDevices = -22,
      SlibErrCamAlreadyAssigned = -23,
      SlibErrNoFreeCamFound = -24,
      SlibErrCamNotFound = -25,
      SlibErrCamNotAssignedToThisApp = -26,
      GeneralErrPlugNotFound = -200,
      ErrScannerAlreadyInUse = -201,
      SlibErrScannerAlreadyInUse = -202,
      SlibErrCannotOpenTwainSource = -203,
      SlibErrNoTwainInstalled = -204,
      SlibErrNoNextValue = -205
    };

    public enum IDErr
    {
      IDErrNone = 1,
      IDErrStateNotSuported = -2,
      IDErrBadParam = -3,
      IDErrNoMatch = -4,
      IDErrFileOpen = -5,
      IDBadDestinationFile = -6,
      IDErrFeatureNotSupported = -7,
      IDErrCountryNotInit = -8,
      IDErrNoNextCountry = -9,
      IDErrStateNotInit = -10,
      IDErrNoNextState = -11,
      IDErrCannotDeleteDestinationImage = -12,
      IDErrCannotCopyToDestonation = -13,
      IDErrFaceImageNotFound = -14,
      // state auto-detect error
      IDErrStateNotRecognized = -15,
      IDErrUsaTemplatesNotFound = -16,
      IDErrWrongTemplateFile = -17,
      IDErrRegionNotInit = -18,
      IDErrAutoDetectNotSupported = -19,
      IDErrCompareNoText = -20,
      IDErrCompareNoBarcode = -21,
      IDErrCompareBcLibNotFound = -22,
      IDErrCompareLicenseDontMatchBcLibrary = -23,
      IDErrDmLibraryNotFound = -24,
      IDErrDmLibraryNotLoaded = -25,
      IDErrDmWmNotFound = -26,
      IDErrDmWmNotAuthenticated = -27
    };

    private SCANWLib.SLibEx m_scanner;
    private SCANWEXLib.SLibEx m_scanner_ex;
    private SCANWLib.IdData m_reader;
    private SCANWEXLib.IdData m_reader_ex;
    private bool SLIB_HasError(int ErrCode)
    {
      SlibErr _error;
      _error = (SlibErr)ErrCode;
      switch (_error)
      {
        case SlibErr.SlibErrNone:
          return false;
        case SlibErr.SlibLibraryAlreadyInitialized:
          return false;
      }
      return _error < 0;
    }
    private bool ID_HasError(int ErrCode, out String ErrMsg)
    {
      IDErr _err;

      _err = (IDErr)ErrCode;
      ErrMsg = _err.ToString() + " (" + ErrCode.ToString() + ")";

      if (_err == IDErr.IDErrNone) return false;
      if (_err >= 0) return false;


      return true;
    }

    public override ENUM_IDScannerStatus Init()
    {

      Status = ENUM_IDScannerStatus.DISCONNECTED | ENUM_IDScannerStatus.ERROR;
      try
      {
        if (!Directory.Exists(AppData)) Directory.CreateDirectory(AppData);
      }
      catch
      {
        //swallow
      }
      string[] _filePaths = Directory.GetFiles(AppData);
      foreach (string _filePath in _filePaths)
      {
        try
        {
          File.Delete(_filePath);
        }
        catch
        {
          //swallow
        }
      }
      try
      {
        if (m_scanner == null)
        {
          m_scanner = new SCANWLib.SLibExClass();
          m_reader = new SCANWLib.IdDataClass();
          m_reader_ex = new SCANWEXLib.IdDataClass();
          m_scanner_ex = new SCANWEXLib.SLibExClass();
          if (SLIB_HasError(m_scanner.InitLibrary(m_licence_snapshell)))
          {
            m_scanner = null;
            m_reader = null;
            m_reader_ex = null;
            m_scanner_ex.UnInit();
            throw new Exception();
          }
        }
        m_scanner.Resolution = 600;
        m_scanner.ScannerColorScheme = 4;
        m_scanner.ResetIntImage();
        m_reader.InitLibrary(m_licence_snapshell);
        m_check_for_button_press = new Timer { AutoReset = true, Interval = 1000, Enabled = true };
        m_check_for_button_press.Elapsed += m_check_for_button_press_Elapsed;

        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
      }
      catch (Exception)
      {
        Status = ENUM_IDScannerStatus.DISCONNECTED | ENUM_IDScannerStatus.ERROR;
      }
      return Status;
    }

    private bool SLIB_HasError(int ErrCode, out SlibErr Error, out String ErrMsg)
    {
      Error = (SlibErr)ErrCode;
      ErrMsg = Error.ToString() + " (" + ErrCode.ToString() + ")";

      if (Error == SlibErr.SlibErrNone) return false;
      if (Error == SlibErr.SlibLibraryAlreadyInitialized) return false;
      if (Error >= 0) return false;

      return true;
    }

    private void m_check_for_button_press_Elapsed(object Sender, ElapsedEventArgs E)
    {
      if (m_scanner == null || m_scanner.PressedButton <= 0)
        return;
      Scan();
    }

    private int GetState(int I, out int Angle)
    {
      Angle = -1;
      m_reader_ex.RegionSet(I);
      m_reader_ex.EnableAllStatesInRegion(I, 1);
      if (m_reader_ex.RegionAutoDetectSupport(I) == 1)
      {
        return m_reader.AutoDetectStateEx("", out Angle);
      }
      return -15;
    }

    private Timer m_check_for_button_press;

    protected override void Dispose(bool Disposing)
    {
      base.Dispose(Disposing);
      if (!Disposing)
        return;
      if (m_check_for_button_press != null)
      {
        m_check_for_button_press.Enabled = false;
        m_check_for_button_press.Elapsed -= m_check_for_button_press_Elapsed;
        m_check_for_button_press = null;
      }
      Status = ENUM_IDScannerStatus.DISCONNECTED;
      if (m_scanner == null)
      {
        return;
      }
      m_scanner = null;
      m_reader = null;
      m_reader_ex = null;
      m_scanner_ex.UnInit();
      m_scanner_ex = null;

    }

    private bool m_scanning = false;
    public override void Scan()
    {
      if (!m_scanning)
        Scan(null);
    }


    public void Scan(int[] RegionList, int? StateId = null)
    {
      m_scanning = true;
      try
      {
        var _front_filename = Path.Combine(AppData, Path.GetRandomFileName());
        Dictionary<string, object> _params_scan;
        _params_scan = new Dictionary<string, object>();

        m_check_for_button_press.Enabled = false;
        m_scanner_ex.SetExternalTriggerMode(1);
        m_scanner_ex.SetExternalOperation();
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.INUSE;
        string _err_msg;
        SlibErr _err;

        if (SLIB_HasError(m_scanner.ScanToFile(_front_filename), out _err, out _err_msg))
        {
          Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
          return;
        }
        if (File.Exists(_front_filename))
        {
          using (var _img = Image.FromFile(_front_filename).Clone() as Image)
          {
            _img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            _img.Save(_front_filename + "R");
          }
          _params_scan.Add("DOC", _front_filename + "R");
        }

        int _angle;
        int _state = 0;
        if (StateId != null)
        {
          _state = StateId.Value;
        }
        else
        {
          bool _autodetect = true;
          int[] _regions = new[] { 3, 0, 1, 2, 5, 6 };
          ;
          if (RegionList == null)
          {
            RegistryKey _key;
            _key = Registry.CurrentUser.OpenSubKey(Registrykey, true);
            string _autodetectregions;
            if (_key != null)
            {
              string _autodetectkey = (string)_key.GetValue("AutoDetect", "true");
              if (!string.IsNullOrEmpty(_autodetectkey))
              {
                _autodetect = bool.Parse(_autodetectkey);
              }
              if (!_autodetect)
              {
                _state = (int)_key.GetValue("ManualState", -1);

              }
              _autodetectregions = (string)_key.GetValue("AutoDetectRegions", "");
              _key.Close();
              if (!string.IsNullOrEmpty(_autodetectregions))
              {
                string[] _regionsorder = _autodetectregions.Split(',');
                List<int> _reglist = new List<int>();
                foreach (var _s in _regionsorder)
                {
                  var _s2 = _s.Replace("(", "").Replace(")", "").Split(':');
                  if (_s2[1] == "Checked")
                    _reglist.Add(int.Parse(_s2[0]));
                }
                if (_reglist.Count > 0)
                  _regions = _reglist.ToArray();
              }
            }
          }
          else
            _regions = RegionList;
          if (_autodetect)
            foreach (var _region in _regions)
            {
              _state = GetState(_region, out _angle);
              if (_state > 0)
                break;
            }

          if (ID_HasError(_state, out _err_msg))
          {
            Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
            SendData(_params_scan);
            return;
          }
        }
        if (ID_HasError(m_reader.ProcState("", _state), out _err_msg))
        {
          Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
          SendData(_params_scan);
          return;
        }

        DateTime _date;

        m_reader_ex.DatesFormat = 2;

        _params_scan.Add("Address", m_reader.Address);
        _params_scan.Add("Address2", m_reader.Address2);
        _params_scan.Add("Address3", m_reader.Address3);
        _params_scan.Add("Address4", m_reader.Address4);
        _params_scan.Add("Address5", m_reader.Address5);
        _params_scan.Add("Audit", m_reader.Audit);
        _params_scan.Add("City", m_reader.City);
        _params_scan.Add("Class", m_reader.Class);
        _params_scan.Add("County", m_reader.County);
        _params_scan.Add("CSC", m_reader.CSC);

        if (DateTime.TryParse(m_reader.DateOfBirth,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("DateOfBirth", _date);
        }
        _params_scan.Add("Dup_Test", m_reader.Dup_Test);
        _params_scan.Add("Endorsements", m_reader.Endorsements);

        if (DateTime.TryParse(m_reader.ExpirationDate,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("ExpirationDate", _date);
        }
        _params_scan.Add("Eyes", m_reader.Eyes);
        _params_scan.Add("Fee", m_reader.Fee);
        _params_scan.Add("Hair", m_reader.Hair);
        _params_scan.Add("Height", m_reader.Height);
        _params_scan.Add("Id", m_reader.Id);

        if (DateTime.TryParse(m_reader.IssueDate,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("IssueDate", _date);
        }
        _params_scan.Add("LastStateIndex", m_reader.LastStateIndex);
        _params_scan.Add("license", m_reader.license);
        _params_scan.Add("Name", m_reader.Name);
        _params_scan.Add("NameFirst", m_reader.NameFirst);
        _params_scan.Add("NameLast", m_reader.NameLast);
        _params_scan.Add("NameMiddle", m_reader.NameMiddle);
        _params_scan.Add("NameSuffix", m_reader.NameSuffix);
        _params_scan.Add("Restriction", m_reader.Restriction);
        _params_scan.Add("Sex", m_reader.Sex);
        _params_scan.Add("Side", m_reader.Side);
        _params_scan.Add("IsFront", IsFront(m_reader.Side));
        _params_scan.Add("SigNum", m_reader.SigNum);
        _params_scan.Add("SocialSecurity", m_reader.SocialSecurity);
        _params_scan.Add("State", m_reader.State);
        _params_scan.Add("Text1", m_reader.Text1);
        _params_scan.Add("Text2", m_reader.Text2);
        _params_scan.Add("Text3", m_reader.Text3);
        _params_scan.Add("Type", m_reader.Type);
        _params_scan.Add("Version", m_reader.Version);
        _params_scan.Add("Weight", m_reader.Weight);
        _params_scan.Add("Zip", m_reader.Zip);
        _params_scan.Add("Address6", m_reader_ex.Address6);
        _params_scan.Add("CardType", m_reader_ex.CardType);
        _params_scan.Add("CountryISO2", m_reader_ex.CountryISO2);
        _params_scan.Add("CountryISO3", m_reader_ex.CountryISO3);
        _params_scan.Add("CountryShort", m_reader_ex.CountryShort);
        if (DateTime.TryParse(m_reader_ex.DateOfBirth4,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("DateOfBirth4", _date);
        }
        _params_scan.Add("DateOfBirthLocal", m_reader_ex.DateOfBirthLocal);
        _params_scan.Add("DatesFormat", m_reader_ex.DatesFormat);
        _params_scan.Add("DobDateAuthentication", m_reader_ex.DobDateAuthentication);
        _params_scan.Add("DocType", m_reader_ex.DocType);
        if (DateTime.TryParse(m_reader_ex.ExpirationDate4,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("ExpirationDate4", _date);
        }
        _params_scan.Add("FatherName", m_reader_ex.FatherName);
        _params_scan.Add("GeneralWaterMark", m_reader_ex.GeneralWaterMark);
        _params_scan.Add("IdCountry", m_reader_ex.IdCountry);
        if (DateTime.TryParse(m_reader_ex.IssueDate4,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("IssueDate4", _date);
        }
        _params_scan.Add("IssueDateAuthentication", m_reader_ex.IssueDateAuthentication);
        _params_scan.Add("IssueDateLocal", m_reader_ex.IssueDateLocal);
        _params_scan.Add("LicenseAuthentication", m_reader_ex.LicenseAuthentication);
        _params_scan.Add("MotherName", m_reader_ex.MotherName);
        _params_scan.Add("NameFirst_NonMRZ", m_reader_ex.NameFirst_NonMRZ);
        _params_scan.Add("NameLast_NonMRZ", m_reader_ex.NameLast_NonMRZ);
        _params_scan.Add("NameLast1", m_reader_ex.NameLast1);
        _params_scan.Add("NameLast2", m_reader_ex.NameLast2);
        _params_scan.Add("NameMiddle_NonMRZ", m_reader_ex.NameMiddle_NonMRZ);
        _params_scan.Add("NameSuffix_NonMRZ", m_reader_ex.NameSuffix_NonMRZ);
        _params_scan.Add("Nationality", m_reader_ex.Nationality);
        _params_scan.Add("Original", m_reader_ex.Original);
        _params_scan.Add("PlaceOfBirth", m_reader_ex.PlaceOfBirth);
        _params_scan.Add("PlaceOfIssue", m_reader_ex.PlaceOfIssue);
        _params_scan.Add("ProcDocType", m_reader_ex.ProcDocType);
        _params_scan.Add("StateAuthentication", m_reader_ex.StateAuthentication);
        _params_scan.Add("TemplateType", m_reader_ex.TemplateType);

        _params_scan.Add("NAME", m_reader.NameFirst);
        _params_scan.Add("NAME2", m_reader.NameMiddle);
        _params_scan.Add("SURNAME", m_reader.NameLast);
        _params_scan.Add("SURNAME2", m_reader_ex.NameLast2);
        if (!string.IsNullOrEmpty(_params_scan["SURNAME2"].ToString()))
        {
          if (!string.IsNullOrEmpty(m_reader_ex.NameLast1))
            _params_scan["SURNAME"] = m_reader_ex.NameLast1;
          else if (_params_scan["SURNAME"] != null && _params_scan["SURNAME"].ToString().Trim().Contains(" "))
            _params_scan["SURNAME"] = _params_scan["SURNAME"].ToString().Split(' ')[0];
        }

        if (((_params_scan["NAME"] as string) ?? "").Trim() == "")
        {
          _params_scan["NAME"] = m_reader_ex.NameFirst_NonMRZ;
          _params_scan["NAME2"] = m_reader_ex.NameMiddle_NonMRZ;
        }

        if (((_params_scan["SURNAME"] as string) ?? "").Trim() == "")
        {
          _params_scan["SURNAME"] = m_reader_ex.NameLast1;
          _params_scan["SURNAME2"] = m_reader_ex.NameLast2;
        }


        if (((_params_scan["SURNAME"] as string) ?? "").Trim() == "")
        {
          _params_scan["SURNAME"] = m_reader_ex.NameLast_NonMRZ;
          _params_scan["SURNAME2"] = "";
        }

        if (((_params_scan["NAME"] as string) ?? "").Trim().Contains(" ") &&
          string.IsNullOrEmpty((_params_scan["NAME2"] as string) ?? ""))
        {
          var _split_name = (_params_scan["NAME"] as string ?? "").Trim().Split(' ');
          _params_scan["NAME"] = _split_name[0];
          if (_split_name.Length > 1)
            _params_scan["NAME2"] = _split_name[1];
          if (_split_name.Length > 2)
            _params_scan["SURNAME"] = _split_name[2];
          if (_split_name.Length > 3)
            _params_scan["SURNAME2"] = _split_name[3];
        }

        if (((_params_scan["SURNAME"] as string) ?? "").Trim().Contains(" ") &&
          string.IsNullOrEmpty((_params_scan["SURNAME2"] as string) ?? ""))
        {
          var _split_name = (_params_scan["SURNAME"] as string ?? "").Trim().Split(' ');
          _params_scan["SURNAME"] = _split_name[0];
          if (_split_name.Length > 1)
            _params_scan["SURNAME2"] = _split_name[1];
        }

        if (string.IsNullOrEmpty((_params_scan["SURNAME"] as string) ?? "") &&
            string.IsNullOrEmpty((_params_scan["SURNAME2"] as string) ?? ""))
        {
          var _split_name = (_params_scan["NameLast"] as string ?? "").Trim().Split(' ');
          _params_scan["SURNAME"] = _split_name[0];
          if (_split_name.Length > 1)
            _params_scan["SURNAME2"] = _split_name[1];
        }

        _params_scan.Add("ID_NUMBER", m_reader.Id);
        ENUM_DetectableIDClass _found_class;
        switch (m_reader_ex.TemplateType)
        {
          case "DL":
          case "ID/DL":
            _found_class = ENUM_DetectableIDClass.DRIVING_LICENCE;
            break;
          case "ID":
            _found_class = ENUM_DetectableIDClass.ID_CARD;
            break;
          case "Resident Permit":
            _found_class = ENUM_DetectableIDClass.RESIDENCE_CARD;
            break;
          default:
            _found_class = ENUM_DetectableIDClass.UNKNOWN;
            break;
        }
        if (_found_class != ENUM_DetectableIDClass.UNKNOWN)
          _params_scan.Add("ID_TYPE", (int)_found_class);
        GENDER _gender;
        switch (m_reader.Sex)
        {
          case "M":
            _gender = GENDER.MALE;
            break;
          case "F":
            _gender = GENDER.FEMALE;
            break;
          default:
            _gender = GENDER.UNKNOWN;
            break;
        }
        if (_gender != GENDER.UNKNOWN)
          _params_scan.Add("SEX", (int)_gender);


        if (DateTime.TryParse(m_reader.DateOfBirth,
          new DateTimeFormatInfo() { DateSeparator = "-", ShortDatePattern = "dd-MM-yyyy" }, DateTimeStyles.AssumeLocal,
          out _date))
        {
          _params_scan.Add("BIRTHDATE", _date.Date);
        }

        var _photo_filename = Path.Combine(AppData, Path.GetRandomFileName());
        if (!ID_HasError(m_reader.GetFaceImage("", _photo_filename, _state), out _err_msg))
        {
          //var _img = Image.FromFile(_photo_filename);
          _params_scan.Add("PHOTO", _photo_filename);
        }

        SendData(_params_scan);

        if (ID_HasError(m_reader.RefreshData(), out _err_msg))
        {
          Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
          return;
        } m_scanner.ResetIntImage();
        m_reader_ex.ResetIDFields();
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
        m_scanner_ex.SetExternalTrigger();
        m_scanner_ex.SetExternalTriggerMode(0);

        m_check_for_button_press.Enabled = true;
      }
      finally
      {
        m_scanning = false;
      }
    }

    private bool IsFront(string p)
    {
      return p != null && p.Equals("Front side");
    }

    private void SendData(Dictionary<string, object> _params_scan)
    {
      if (ScannerEvent != null)
        ThreadPool.QueueUserWorkItem(delegate
        { ScannerEvent(new IDScannerInfoEvent() { info = new IDScannerInfo() { IDScannerFixedFieldMapping = FixedFieldMapping, Fields = _params_scan } }); });
    }


    protected override Dictionary<ENUM_IDScannerFixedField, string> FixedFieldMapping
    {
      get
      {
        return new Dictionary<ENUM_IDScannerFixedField, string>()
          {
            {ENUM_IDScannerFixedField.DATE_OF_BIRTH, "BIRTHDATE"},
            {ENUM_IDScannerFixedField.ID_NUMBER, "ID_NUMBER"},
            {ENUM_IDScannerFixedField.ID_TYPE, "ID_TYPE"},
            {ENUM_IDScannerFixedField.NAME, "NAME"},
            {ENUM_IDScannerFixedField.NAME2, "NAME2"},
            {ENUM_IDScannerFixedField.SURNAME, "SURNAME"},
            {ENUM_IDScannerFixedField.SURNAME2, "SURNAME2"},
            {ENUM_IDScannerFixedField.PHOTO, "PHOTO"},
            {ENUM_IDScannerFixedField.SEX, "SEX"},
            {ENUM_IDScannerFixedField.BIRTHPLACE, "BIRTHPLACE"},
            {ENUM_IDScannerFixedField.ADDRESS, "Address"},
            {ENUM_IDScannerFixedField.CITY, "City"},
            {ENUM_IDScannerFixedField.COUNTY, "County"},
            {ENUM_IDScannerFixedField.COUNTRY, "CountryISO2"},
            {ENUM_IDScannerFixedField.STATE, "State"},
            {ENUM_IDScannerFixedField.NATIONALITY, "Nationality"},
            {ENUM_IDScannerFixedField.ZIP, "Zip"},
						{ENUM_IDScannerFixedField.FRONT, "DOC"},
						{ENUM_IDScannerFixedField.DOCUMENT_EXPIRY,"ExpirationDate"},
						{ENUM_IDScannerFixedField.IS_FRONT,"IsFront"}

          };
      }
    }

    public void ScanState(int StateId)
    {
      Scan(null, StateId);
    }

    public override bool InstallDriver()
    {
      if (IsDriverInstalled())
      {
        return true;
      }
      Stream _stream =
        Assembly.GetExecutingAssembly()
          .GetManifestResourceStream("IDScannerConnectorSnapShell.acuant_driver_package.zip");

      ZipFile _zf = null;
      string _extractionroot = Path.Combine(AppData, "acuant_driver_package");
      try
      {
        _zf = new ZipFile(_stream);
        foreach (ZipEntry _zip_entry in _zf)
        {
          if (!_zip_entry.IsFile)
          {
            continue; // Ignore directories
          }
          String _entry_file_name = _zip_entry.Name;

          byte[] _buffer = new byte[4096]; // 4K is optimum
          Stream _zip_stream = _zf.GetInputStream(_zip_entry);

          String _full_zip_to_path = Path.Combine(_extractionroot, _entry_file_name);
          string _directory_name = Path.GetDirectoryName(_full_zip_to_path);
          if (!string.IsNullOrEmpty(_directory_name))
            Directory.CreateDirectory(_directory_name);

          using (FileStream _stream_writer = File.Create(_full_zip_to_path))
          {
            StreamUtils.Copy(_zip_stream, _stream_writer, _buffer);
          }
        }
      }
      finally
      {
        if (_zf != null)
        {
          _zf.IsStreamOwner = true;
          _zf.Close();

        }
      }
      ProcessStartInfo _start_info = new ProcessStartInfo
      {
        WorkingDirectory = _extractionroot,
        FileName = _extractionroot + "\\install.bat",
        Arguments = String.Empty,
        CreateNoWindow = true,
        WindowStyle = ProcessWindowStyle.Hidden,
        RedirectStandardOutput = true,
        UseShellExecute = false,
      };
      try
      {
        new FileIOPermission(FileIOPermissionAccess.Read, _start_info.FileName).Demand();

        Process _proc = Process.Start(_start_info);
        if (_proc == null)
        {
          return false;
        }
        _proc.WaitForExit((60 * 1000) * 7);
        //Directory.Delete(_extractionroot, true);
      }
      catch (SecurityException)
      {
        //
      }
      return IsDriverInstalled();
    }

    public override bool IsDriverInstalled()
    {
      bool _found_driver = false;
      RegistryKey _usb_devices =
        Registry.LocalMachine.OpenSubKey(@"SYSTEM\ControlSet001\Control\Class\{36FC9E60-C465-11CF-8056-444553540000}");

      if (_usb_devices == null)
      {
        return false;
      }
      foreach (string _reg_key_names in _usb_devices.GetSubKeyNames())
      {
        RegistryKey _sub_key = null;
        try
        {
          _sub_key = _usb_devices.OpenSubKey(_reg_key_names);
        }
        catch
        {
          //swallow
        }
        if (_sub_key == null)
        {
          continue;
        }
        string _device_name = _sub_key.GetValue("DriverDesc") as string;
        if (string.IsNullOrEmpty(_device_name) || !_device_name.Contains("CSSN USB2 Camera Device Driver"))
        {
          continue;
        }
        _found_driver = true;
        break;
      }

      if (!_found_driver)
      {
        return false;
      }

      //64bit
      RegistryKey _reg_key64 = null;
      try
      {
        _reg_key64 =
          Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" + @"\");
      }
      catch
      {
        //swallow
      }
      if (_reg_key64 == null)
      {

        //32bit
        RegistryKey _reg_key32 =
          Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" + @"\");
        if (_reg_key32 == null)
        {

          return false;
        }
        string[] _sub_key_names32 = _reg_key32.GetSubKeyNames();
        foreach (string _key_name in _sub_key_names32)
        {
          RegistryKey _regkey =
            Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" + @"\" + _key_name);


          if (_regkey == null)
          {
            continue;
          }


          string _software_name = _regkey.GetValue("DisplayName") as string;
          string _software_ver = _regkey.GetValue("DisplayVersion") as string;

          if ((!string.IsNullOrEmpty(_software_name) && !string.IsNullOrEmpty(_software_ver)) &&
              (_software_name.ToUpperInvariant().Contains(@"ACUANT SDK") && _software_ver.ToUpperInvariant().Contains(@"10.13.0304")))
          {
            return true;
          }
        }
        return false;
      }
      string[] _sub_key_names64 = _reg_key64.GetSubKeyNames();
      foreach (string _key_name in _sub_key_names64)
      {
        RegistryKey _regkey =
          Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" + @"\" +
                                           _key_name);


        if (_regkey == null)
        {
          continue;
        }
        string _software_name = _regkey.GetValue("DisplayName") as string;
        string _software_ver = _regkey.GetValue("DisplayVersion") as string;

        if ((!string.IsNullOrEmpty(_software_name) && !string.IsNullOrEmpty(_software_ver)) &&
            (_software_name.ToUpperInvariant().Contains(@"ACUANT SDK") && _software_ver.ToUpperInvariant().Contains(@"10.13.0304")))
        {
          return true;
        }
      }
      return false;
    }

    public override void ShowAdvancedScan()
    {
      Thread.CurrentThread.CurrentUICulture =
        CultureInfo.GetCultureInfo(GeneralParam.GetString("Cashier", "Language", "en"));
      new AdvancedScan(this).ShowDialog();
    }

    public Dictionary<string, string> LoadConfiguration()
    {
      return new Dictionary<string, string>()
        {
          {"Resolution","600"}
        };
    }
    public bool SaveConfiguration()
    {
      return true;
    }
    private const string Registrykey = "Software\\WigosSnapShellConnectorConfig";
  }
}
