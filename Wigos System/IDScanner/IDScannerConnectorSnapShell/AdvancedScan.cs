﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.Utilities.Extensions;

namespace IDScannerConnectorSnapShell
{
  public partial class AdvancedScan : Form
  {
    internal class SnapShellRegion
    {
      public int Id { get; set; }
      public string Description { get; set; }
    }

    private static readonly BindingList<SnapShellRegion> m_bindinglist = new BindingList<SnapShellRegion>();

    private readonly SnapShellConnector m_snap_shell_connector;


    public AdvancedScan(SnapShellConnector SnapShellConnector)
    {
      m_snap_shell_connector = SnapShellConnector;
      CreateTable();
      InitializeComponent();
      chk_lst_regions.CheckOnClick = false;
      chk_lst_regions.ItemHeight = 40;
      chk_lst_regions.Font = new Font(FontFamily.GenericSansSerif, 20);
      chk_lst_regions.DataSource = m_bindinglist;
      chk_lst_regions.DisplayMember = "Description";
      chk_lst_regions.ValueMember = "Id";
      RegionsToCheckedList();

    }

    private void btn_scan_Click(object Sender, EventArgs E)
    {
      List<int> _region_list = new List<int>();
      if (chk_autodetect.Checked)
      {
        foreach (SnapShellRegion _ssr in chk_lst_regions.CheckedItems)
        {
          _region_list.Add(_ssr.Id);
        }
        m_snap_shell_connector.Scan(_region_list.ToArray());
      }
      else
      {
        if (cmb_state.SelectedItem != null)
        {
          int _i = (int) ((RowContainer) cmb_state.SelectedItem).Row.ItemArray[2];
          m_snap_shell_connector.ScanState(_i);
        }
        else
        {
          return;
        }
      }
      DialogResult = DialogResult.OK;
    }

    private void btn_move_up_Click(object Sender, EventArgs E)
    {
      var _checked_items = new List<int>();
      foreach (var _item in chk_lst_regions.CheckedItems)
      {
        _checked_items.Add(((SnapShellRegion) _item).Id);
      }
      if (chk_lst_regions.SelectedIndex == 0)
        return;
      var _v = m_bindinglist[chk_lst_regions.SelectedIndex];
      m_bindinglist.RemoveAt(chk_lst_regions.SelectedIndex);
      m_bindinglist.Insert(chk_lst_regions.SelectedIndex - 1, _v);
      chk_lst_regions.SelectedIndex = chk_lst_regions.SelectedIndex - 2;
      foreach (var _item in _checked_items)
      {
        chk_lst_regions.SetItemChecked(chk_lst_regions.Items.IndexOf((m_bindinglist.Find("Id", _item))), true);
      }


    }


    private void RegionsToCheckedList()
    {
      DataRow[] _regions;

      _regions = m_dataset.Tables["Regions"].Select("", "Region.Order Asc");
      Dictionary<int, string> _templist = new Dictionary<int, string>();
      m_bindinglist.Clear();
      foreach (DataRow _row in _regions)
      {
        if ((Boolean) _row["Region.AutoSupported"])
        {
          _templist.Add((int) _row["Region.Id"], (string) _row["Region.Name"]);
          cmb_region.Items.Add(new RowContainer(_row, "Region.Name"));
        }
        if (!(Boolean) _row["Region.AutoSupported"])
        {
          cmb_region.Items.Add(new RowContainer(_row, "Region.Name"));
        }
      }


      Microsoft.Win32.RegistryKey _key;
      _key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(Registrykey, true);
      string _autodetectregions = "";
      int _manregion = -1;
      int _mancountry = -1;
      int _manstate = -1;
      string _autodetect = "";
      if (_key != null)
      {
        _autodetectregions = (string)_key.GetValue("AutoDetectRegions", "(3:Checked),(0:Checked),(2:Checked),(1:Checked),(4:Checked),(5:Checked),(7:Checked)");
        _manregion = (int) _key.GetValue("ManualRegion", -1);
        _mancountry = (int) _key.GetValue("ManualCountry", -1);
        _manstate = (int) _key.GetValue("ManualState", -1);
        _autodetect = (string) _key.GetValue("AutoDetect", "True");
        _key.Close();
      }
      else
      {
        _autodetectregions = "(3:Checked),(0:Checked),(2:Checked),(1:Checked),(4:Checked),(5:Checked),(7:Checked)";
        _manregion = -1;
        _mancountry = -1;
        _manstate = -1;
        _autodetect = "True";
      }
      if (string.IsNullOrEmpty(_autodetectregions))
      {
        foreach (var _variable in _templist)
        {
          m_bindinglist.Add(new SnapShellRegion() {Id = _variable.Key, Description = _variable.Value});
        }
        foreach (var _v in m_dataset.Tables["Regions"].Select("Region.AutoEnabled", ""))
        {
          int _itemindex = chk_lst_regions.Items.IndexOf(m_bindinglist.Find("Id", (int) _v[0]));
          chk_lst_regions.SetItemChecked(_itemindex, true);
        }
      }
      else
      {
        string[] _regionsorder = _autodetectregions.Split(',');
        foreach (var _s in _regionsorder)
        {
          var _s2 = _s.Replace("(", "").Replace(")", "").Split(':');
          m_bindinglist.Add(new SnapShellRegion() {Id = int.Parse(_s2[0]), Description = _templist[int.Parse(_s2[0])]});
        }
        foreach (var _s in _regionsorder)
        {
          var _s2 = _s.Replace("(", "").Replace(")", "").Split(':');
          int _itemindex = chk_lst_regions.Items.IndexOf(m_bindinglist.Find("Id", int.Parse(_s2[0])));
          chk_lst_regions.SetItemChecked(_itemindex, _s2[1] == "Checked");
        }
      }
      if (!string.IsNullOrEmpty(_autodetect))
      {
        chk_autodetect.Checked = bool.Parse(_autodetect);
      }
      if (_manregion > -1)
      {
        foreach (var _item in cmb_region.Items)
        {
          if ((int) ((RowContainer) _item).Row[0] == _manregion)
          {
            cmb_region.SelectedItem = _item;
          }
        }
      }
      if (_mancountry > -1)
      {
        foreach (var _item in cmb_country.Items)
        {
          if ((int) ((RowContainer) _item).Row[1] == _mancountry)
          {
            cmb_country.SelectedItem = _item;
          }
        }
      }
      if (_manstate > -1)
      {
        bool found = false;
        foreach (var _item in cmb_state.Items)
        {
          if ((int) ((RowContainer) _item).Row[2] == _manstate)
          {
            cmb_state.SelectedItem = _item;
            found = true;
          }
        }
        if (found)
        {
          return;
        }
        foreach (var _item in cmb_country.Items)
        {
          if ((int)((RowContainer)_item).Row[1] == _manstate)
          {
            cmb_country.SelectedItem = _item;
          }
        }
      }


    }

    private void btn_move_down_Click(object Sender, EventArgs E)
    {
      var _checked_items = new List<int>();
      foreach (var _item in chk_lst_regions.CheckedItems)
      {
        _checked_items.Add(((SnapShellRegion) _item).Id);
      }

      if (chk_lst_regions.SelectedIndex + 1 == m_bindinglist.Count)
        return;
      var _v = m_bindinglist[chk_lst_regions.SelectedIndex];
      m_bindinglist.RemoveAt(chk_lst_regions.SelectedIndex);
      m_bindinglist.Insert(chk_lst_regions.SelectedIndex + 1, _v);
      chk_lst_regions.SelectedIndex = chk_lst_regions.SelectedIndex + 1;
      foreach (var _item in _checked_items)
      {
        chk_lst_regions.SetItemChecked(chk_lst_regions.Items.IndexOf((m_bindinglist.Find("Id", _item))), true);
      }

    }

    private static DataSet m_dataset;

    private void chk_autodetect_CheckedChanged(object Sender, EventArgs E)
    {
      pnl_regions.Visible = ((CheckBox) Sender).Checked;
      pnl_manual_selection.Visible = !((CheckBox) Sender).Checked;
    }

    internal class RowContainer
    {
      private readonly int m_col_index;
      private readonly DataRow m_row;

      internal RowContainer(DataRow Row, String ColName)
      {
        m_row = Row;
        m_col_index = Row.Table.Columns[ColName].Ordinal;
      }

      public DataRow Row
      {
        get { return m_row; }
      }

      public override String ToString()
      {
        if (m_row != null)
        {
          return (String) m_row[m_col_index];
        }
        else
        {
          return String.Empty;
        }
      }
    }


    private void UpdateLabelCombo(ComboBox C, Label L)
    {
      if (C.Items.Count > 0)
      {
        C.SelectedIndex = 0;
      }
      C.Enabled = (C.Items.Count > 1);
      L.Enabled = (C.Items.Count > 1);
    }

    private void CountriesToCombo(int RegionID)
    {
      DataRow[] _items;
      ComboBox _combo;
      Label _label;

      _combo = cmb_country;
      _label = lbl_country;

      try
      {
        _combo.Items.Clear();

        if (m_dataset == null)
        {
          return;
        }

        _items = m_dataset.Tables["Countries"].Select("Region.ID = " + RegionID.ToString(), "Country.Name Asc");

        foreach (DataRow _row in _items)
        {
          _combo.Items.Add(new RowContainer(_row, "Country.Name"));
        }
      }
      finally
      {
        UpdateLabelCombo(_combo, _label);
      }
    }

    private void StatesToCombo(int CountryID)
    {
      DataRow[] _items;
      ComboBox _combo;
      Label _label;

      _combo = cmb_state;
      _label = lbl_state;

      try
      {
        _combo.Items.Clear();
        if (m_dataset == null)
        {
          return;
        }
        _items = m_dataset.Tables["States"].Select("Country.ID = " + CountryID.ToString(), "State.Name Asc");

        foreach (DataRow _row in _items)
        {
          _combo.Items.Add(new RowContainer(_row, "State.Name"));
        }
      }
      finally
      {
        UpdateLabelCombo(_combo, _label);
      }
    }


    private void CreateTable()
    {
      if (m_dataset != null)
        return;
      int _r_id;
      int _c_id;
      int _s_id;
      int _order;
      String _r_name;
      String _c_name;
      String _s_name;
      String _c_iso2;
      String _c_iso3;

      DataTable _config;
      DataTable _states;
      DataTable _countries;
      DataTable _regions;
      DataSet _ds;
      int _user_region_id;


      _user_region_id = -1;

      _config = new DataTable("Config");
      _config.Columns.Add("AutoDetect", typeof (bool));
      _config.Columns.Add("StateID", typeof (int));
      DataRow _dr_c = _config.NewRow();
      _dr_c["AutoDetect"] = true;
      _dr_c["StateID"] = -1;
      _config.Rows.Add(_dr_c);

      _regions = new DataTable("Regions");
      _regions.Columns.Add("Region.ID", typeof (int));
      _regions.Columns.Add("Region.Name", typeof (string));
      _regions.Columns.Add("Region.AutoSupported", typeof (bool));
      _regions.Columns.Add("Region.AutoEnabled", typeof (bool));
      _regions.Columns.Add("Region.Order", typeof (int));

      _countries = new DataTable("Countries");
      _countries.Columns.Add("Region.ID", typeof (int));
      _countries.Columns.Add("Country.ID", typeof (int));
      _countries.Columns.Add("Country.Name", typeof (string));
      _countries.Columns.Add("Country.ISO2", typeof (string));
      _countries.Columns.Add("Country.ISO3", typeof (string));

      _states = new DataTable("States");
      _states.Columns.Add("Region.ID", typeof (int));
      _states.Columns.Add("Country.ID", typeof (int));
      _states.Columns.Add("State.ID", typeof (int));
      _states.Columns.Add("State.Name", typeof (string));
      _states.Columns.Add("State.Enabled", typeof (bool));

      _ds = new DataSet();
      _ds.Tables.Add(_config);
      _ds.Tables.Add(_regions);
      _ds.Tables.Add(_countries);
      _ds.Tables.Add(_states);
      _ds.Relations.Add("R2C", _regions.Columns["Region.ID"], _countries.Columns["Region.ID"]);
      _ds.Relations.Add("R2S", _regions.Columns["Region.ID"], _states.Columns["Region.ID"]);
      _ds.Relations.Add("C2S", _countries.Columns["Country.ID"], _states.Columns["Country.ID"]);

      _r_id = m_snap_shell_connector.IDDataEx.RegionGetFirst();
      _order = 0;
      while (_r_id >= 0)
      {
        m_snap_shell_connector.IDDataEx.RegionGetNameById(_r_id, out _r_name);

        DataRow _dr_region = _regions.NewRow();
        Boolean _auto;

        _dr_region["Region.ID"] = _r_id;
        _dr_region["Region.Name"] = _r_name;
        _auto = (_r_id != 6);
        _dr_region["Region.AutoSupported"] = _auto;
        _dr_region["Region.AutoEnabled"] = _auto;
        if (_auto)
        {
          _dr_region["Region.Order"] = _order++;
        }
        else
        {
          _dr_region["Region.Order"] = 0;
        }
        _regions.Rows.Add(_dr_region);

        _c_id = m_snap_shell_connector.IDDataEx.RegionGetFirstCountry(_r_id);
        while (_c_id >= 0)
        {
          m_snap_shell_connector.IDData.Id2Country(_c_id, out _c_name);

          m_snap_shell_connector.IDDataEx.Id2Country2(_c_id, out _c_iso2);
          if (_c_iso2.Contains("ÿÿÿÿ"))
          {
            _c_iso2 = "--";
          }
          m_snap_shell_connector.IDDataEx.Id2Country3(_c_id, out _c_iso3);
          if (_c_iso3.Contains("ÿÿÿÿ"))
          {
            _c_iso3 = "---";
          }

          if (_c_iso2 == m_user_country_iso2)
          {
            _user_region_id = _r_id;
          }

          DataRow _dr_country;
          _dr_country = _countries.NewRow();
          _dr_country["Region.ID"] = _r_id;
          _dr_country["Country.ID"] = _c_id;
          _dr_country["Country.Name"] = _c_name;
          _dr_country["Country.ISO2"] = _c_iso2;
          _dr_country["Country.ISO3"] = _c_iso3;
          _countries.Rows.Add(_dr_country);

          _s_id = m_snap_shell_connector.IDData.GetFirstStateByCountry(_c_id);
          while (_s_id >= 0)
          {
            m_snap_shell_connector.IDData.Id2State(_s_id, out _s_name);

            DataRow _dr_state = _states.NewRow();
            _dr_state["Region.ID"] = _r_id;
            _dr_state["Country.ID"] = _c_id;
            _dr_state["State.ID"] = _s_id;
            _dr_state["State.Name"] = _s_name;
            _dr_state["State.Enabled"] = true;

            _states.Rows.Add(_dr_state);

            _s_id = m_snap_shell_connector.IDData.GetNextStateByCountry(_c_id);
          }
          _c_id = m_snap_shell_connector.IDDataEx.RegionGetNextCountry(_r_id);
        }

        _r_id = m_snap_shell_connector.IDDataEx.RegionGetNext();
      }

      m_dataset = _ds;

      if (String.IsNullOrEmpty(m_user_config))
      {
        // Try using the 'Region' of the user country
        if (_user_region_id >= 0)
        {
          _order = 1;
          foreach (DataRow _dr_region in _regions.Rows)
          {
            if (_user_region_id == (int) _dr_region["Region.ID"])
            {
              _dr_region["Region.Order"] = 0;
              _dr_region["Region.AutoEnabled"] = true;
            }
            else
            {
              _dr_region["Region.Order"] = _order++;
              _dr_region["Region.AutoEnabled"] = false;
            }
          }
        }
      }
      else
      {
      }
    }

    private static readonly String m_user_config = null;
    private static readonly String m_user_country_iso2 = GeneralParam.GetString("RegionalOptions", "CountryISOCode2");

    private void cmb_region_SelectedIndexChanged(object Sender, EventArgs E)
    {
      RowContainer _parent;

      _parent = (RowContainer) cmb_region.SelectedItem;
      if (_parent == null)
        return;
      CountriesToCombo((int) _parent.Row["Region.ID"]);
    }

    private void cmb_country_SelectedIndexChanged(object Sender, EventArgs E)
    {
      RowContainer _parent;

      _parent = (RowContainer) cmb_country.SelectedItem;
      if (_parent == null)
        return;
      StatesToCombo((int) _parent.Row["Country.ID"]);
    }

    private void cmb_state_SelectedIndexChanged(object Sender, EventArgs E)
    {
      RowContainer _r1;

      _r1 = (RowContainer) cmb_state.SelectedItem;
      if (_r1 == null)
      {
        return;
      }
      m_dataset.Tables["Config"].Rows[0]["StateID"] = _r1.Row["State.ID"];

    }

    private void AdvancedScan_FormClosing(object Sender, FormClosingEventArgs E)
    {
      string _checked_states_and_order = "";
      foreach (var _item in m_bindinglist)
      {
        _checked_states_and_order += (_checked_states_and_order.Length == 0 ? "" : ",") +
                                     string.Format("({0}:{1})", _item.Id,
                                       chk_lst_regions.GetItemCheckState(chk_lst_regions.Items.IndexOf(_item)));
      }


      Microsoft.Win32.RegistryKey _reg_key;
      _reg_key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(Registrykey, true) ??
             Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Registrykey);
      if (_reg_key == null)
      {
        return;
      }
      _reg_key.SetValue("AutoDetect", chk_autodetect.Checked);
      _reg_key.SetValue("AutoDetectRegions", _checked_states_and_order);
      if (cmb_state.SelectedItem == null)
      {
        return;
      }
      _reg_key.SetValue("ManualRegion",
        cmb_state.SelectedItem == null ? -1 : ((RowContainer) cmb_state.SelectedItem).Row["Region.Id"]);
      _reg_key.SetValue("ManualCountry",
        cmb_state.SelectedItem == null ? -1 : ((RowContainer) cmb_state.SelectedItem).Row["Country.Id"]);
      _reg_key.SetValue("ManualState",
        cmb_state.SelectedItem == null ? -1 : ((RowContainer) cmb_state.SelectedItem).Row["State.Id"]);
    }

    private const string Registrykey = "Software\\WigosSnapShellConnectorConfig";

  }
}
