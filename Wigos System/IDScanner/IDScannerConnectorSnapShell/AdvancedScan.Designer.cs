﻿namespace IDScannerConnectorSnapShell
{
  partial class AdvancedScan
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancedScan));
      this.btn_scan = new System.Windows.Forms.Button();
      this.chk_lst_regions = new System.Windows.Forms.CheckedListBox();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.btn_move_up = new System.Windows.Forms.Button();
      this.btn_move_down = new System.Windows.Forms.Button();
      this.pnl_regions = new System.Windows.Forms.Panel();
      this.chk_autodetect = new System.Windows.Forms.CheckBox();
      this.pnl_manual_selection = new System.Windows.Forms.Panel();
      this.lbl_state = new System.Windows.Forms.Label();
      this.lbl_country = new System.Windows.Forms.Label();
      this.lbl_region = new System.Windows.Forms.Label();
      this.cmb_region = new System.Windows.Forms.ComboBox();
      this.cmb_state = new System.Windows.Forms.ComboBox();
      this.cmb_country = new System.Windows.Forms.ComboBox();
      this.pnl_regions.SuspendLayout();
      this.pnl_manual_selection.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_scan
      // 
      resources.ApplyResources(this.btn_scan, "btn_scan");
      this.btn_scan.Name = "btn_scan";
      this.btn_scan.UseVisualStyleBackColor = true;
      this.btn_scan.Click += new System.EventHandler(this.btn_scan_Click);
      // 
      // chk_lst_regions
      // 
      resources.ApplyResources(this.chk_lst_regions, "chk_lst_regions");
      this.chk_lst_regions.FormattingEnabled = true;
      this.chk_lst_regions.Items.AddRange(new object[] {
            resources.GetString("chk_lst_regions.Items"),
            resources.GetString("chk_lst_regions.Items1"),
            resources.GetString("chk_lst_regions.Items2"),
            resources.GetString("chk_lst_regions.Items3")});
      this.chk_lst_regions.Name = "chk_lst_regions";
      // 
      // btn_cancel
      // 
      resources.ApplyResources(this.btn_cancel, "btn_cancel");
      this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.UseVisualStyleBackColor = true;
      // 
      // btn_move_up
      // 
      resources.ApplyResources(this.btn_move_up, "btn_move_up");
      this.btn_move_up.Name = "btn_move_up";
      this.btn_move_up.UseVisualStyleBackColor = true;
      this.btn_move_up.Click += new System.EventHandler(this.btn_move_up_Click);
      // 
      // btn_move_down
      // 
      resources.ApplyResources(this.btn_move_down, "btn_move_down");
      this.btn_move_down.Name = "btn_move_down";
      this.btn_move_down.UseVisualStyleBackColor = true;
      this.btn_move_down.Click += new System.EventHandler(this.btn_move_down_Click);
      // 
      // pnl_regions
      // 
      resources.ApplyResources(this.pnl_regions, "pnl_regions");
      this.pnl_regions.Controls.Add(this.chk_lst_regions);
      this.pnl_regions.Controls.Add(this.btn_move_down);
      this.pnl_regions.Controls.Add(this.btn_move_up);
      this.pnl_regions.Name = "pnl_regions";
      // 
      // chk_autodetect
      // 
      resources.ApplyResources(this.chk_autodetect, "chk_autodetect");
      this.chk_autodetect.Checked = true;
      this.chk_autodetect.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_autodetect.Name = "chk_autodetect";
      this.chk_autodetect.UseVisualStyleBackColor = true;
      this.chk_autodetect.CheckedChanged += new System.EventHandler(this.chk_autodetect_CheckedChanged);
      // 
      // pnl_manual_selection
      // 
      resources.ApplyResources(this.pnl_manual_selection, "pnl_manual_selection");
      this.pnl_manual_selection.Controls.Add(this.lbl_state);
      this.pnl_manual_selection.Controls.Add(this.lbl_country);
      this.pnl_manual_selection.Controls.Add(this.lbl_region);
      this.pnl_manual_selection.Controls.Add(this.cmb_region);
      this.pnl_manual_selection.Controls.Add(this.cmb_state);
      this.pnl_manual_selection.Controls.Add(this.cmb_country);
      this.pnl_manual_selection.Name = "pnl_manual_selection";
      // 
      // lbl_state
      // 
      resources.ApplyResources(this.lbl_state, "lbl_state");
      this.lbl_state.Name = "lbl_state";
      // 
      // lbl_country
      // 
      resources.ApplyResources(this.lbl_country, "lbl_country");
      this.lbl_country.Name = "lbl_country";
      // 
      // lbl_region
      // 
      resources.ApplyResources(this.lbl_region, "lbl_region");
      this.lbl_region.Name = "lbl_region";
      // 
      // cmb_region
      // 
      resources.ApplyResources(this.cmb_region, "cmb_region");
      this.cmb_region.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_region.FormattingEnabled = true;
      this.cmb_region.Name = "cmb_region";
      this.cmb_region.SelectedIndexChanged += new System.EventHandler(this.cmb_region_SelectedIndexChanged);
      // 
      // cmb_state
      // 
      resources.ApplyResources(this.cmb_state, "cmb_state");
      this.cmb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_state.FormattingEnabled = true;
      this.cmb_state.Name = "cmb_state";
      this.cmb_state.SelectedIndexChanged += new System.EventHandler(this.cmb_state_SelectedIndexChanged);
      // 
      // cmb_country
      // 
      resources.ApplyResources(this.cmb_country, "cmb_country");
      this.cmb_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_country.FormattingEnabled = true;
      this.cmb_country.Name = "cmb_country";
      this.cmb_country.SelectedIndexChanged += new System.EventHandler(this.cmb_country_SelectedIndexChanged);
      // 
      // AdvancedScan
      // 
      this.AcceptButton = this.btn_scan;
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btn_cancel;
      this.ControlBox = false;
      this.Controls.Add(this.chk_autodetect);
      this.Controls.Add(this.btn_cancel);
      this.Controls.Add(this.btn_scan);
      this.Controls.Add(this.pnl_manual_selection);
      this.Controls.Add(this.pnl_regions);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "AdvancedScan";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdvancedScan_FormClosing);
      this.pnl_regions.ResumeLayout(false);
      this.pnl_manual_selection.ResumeLayout(false);
      this.pnl_manual_selection.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_scan;
    private System.Windows.Forms.CheckedListBox chk_lst_regions;
    private System.Windows.Forms.Button btn_cancel;
    private System.Windows.Forms.Button btn_move_up;
    private System.Windows.Forms.Button btn_move_down;
    private System.Windows.Forms.Panel pnl_regions;
    private System.Windows.Forms.CheckBox chk_autodetect;
    private System.Windows.Forms.Panel pnl_manual_selection;
    private System.Windows.Forms.ComboBox cmb_region;
    private System.Windows.Forms.ComboBox cmb_state;
    private System.Windows.Forms.ComboBox cmb_country;
    private System.Windows.Forms.Label lbl_state;
    private System.Windows.Forms.Label lbl_country;
    private System.Windows.Forms.Label lbl_region;
  }
}