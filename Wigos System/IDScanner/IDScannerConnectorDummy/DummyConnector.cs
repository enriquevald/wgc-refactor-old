﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using WSI.IDScanner.Model;
using WSI.IDScanner.Model.Enums;

namespace IDScannerConnectorDummy
{
    public class DummyConnector:IDScannerDeviceBase
    {
      public override string Name
      {
        get { return "DummyScanner"; }
      }

      public override ENUM_IDScannerStatus Init()
      {
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
        return Status;
      }



      public override void Scan()
      {
        if (ScannerEvent != null)
          
          ScannerEvent(new IDScannerInfoEvent()
            {
              info = new IDScannerInfo() {Fields = new Dictionary<string, object>()
                {
                  {"NAME", "DUMMY"}
                }}
            });
      }

      protected override void Dispose(bool Disposing)
      {
        base.Dispose(Disposing);
        if (!Disposing)
          return;
        Status = ENUM_IDScannerStatus.DISCONNECTED;
      }


      public override bool InstallDriver()
      {
        return true;
      }

      public override bool IsDriverInstalled()
      {
        return true;
      }

      public override void ShowAdvancedScan()
      {
        MessageBox.Show("Dummy Advanced");
        Scan();
      }


      protected override Dictionary<ENUM_IDScannerFixedField, string> FixedFieldMapping
      {
        get
        {
	        return new Dictionary<ENUM_IDScannerFixedField, string>()
	        {
		        {ENUM_IDScannerFixedField.DATE_OF_BIRTH, null},
		        {ENUM_IDScannerFixedField.ID_NUMBER, null},
		        {ENUM_IDScannerFixedField.ID_TYPE, null},
		        {ENUM_IDScannerFixedField.NAME, "NAME"},
		        {ENUM_IDScannerFixedField.NAME2, null},
		        {ENUM_IDScannerFixedField.SURNAME, null},
		        {ENUM_IDScannerFixedField.SURNAME2, null},
		        {ENUM_IDScannerFixedField.PHOTO, null},
		        {ENUM_IDScannerFixedField.SEX, null},
		        {ENUM_IDScannerFixedField.BIRTHPLACE, null},
		        {ENUM_IDScannerFixedField.ADDRESS, null},
		        {ENUM_IDScannerFixedField.CITY, null},
		        {ENUM_IDScannerFixedField.NATIONALITY, null},
		        {ENUM_IDScannerFixedField.ZIP, null},
		        {ENUM_IDScannerFixedField.FRONT, "DOC"},
		        {ENUM_IDScannerFixedField.DOCUMENT_EXPIRY, "EXPIRY"}
	        };
        }
      }
    }
}
