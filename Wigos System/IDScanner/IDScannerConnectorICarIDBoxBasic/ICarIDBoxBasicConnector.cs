﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;
using System.Drawing;
using WSI.IDScanner.Model;
using WSI.IDScanner.Model.Enums;
using System.Globalization;
using System.Security;
using System.Security.Permissions;
using System.Threading;
using WSI.Common;
using WSI.Common.Utilities.Extensions;


namespace IDScannerConnectorICarIDBoxBasic
{
  public class ICarIDBoxBasicConnector : IDScannerDeviceBase
  {
    #region Constants
    private const sbyte ConnectionProperty = 20;
    private const sbyte ConnectionValue = 24;

    public short IcarTwainDevice = 4;
    public short IcarReadFileDevice = 5;
    public short IcarHpPhotoScan1200Device = 14;
    public short IcarIcarbox240BDevice = 16;
    public short IcarBancorIbmK2SDevice = 17;
    public short IcarVirtualFolderDevice = 18;
    public short IcarIcarbox250Device = 20;
    public short IcarIcarbox250ViuDevice = 21;
    public short IcarIcarbox260Device = 22;
    public short IcarIcarbox260ViuDevice = 23;
    public short IcarIcarboxIdboxDevice = 24;
    public short IcarIcarboxIdboxViuDevice = 25;
    public short IcarIcarboxIdboxEDevice = 26;
    public short IcarIcarboxIdboxEViuDevice = 27;

    public short IcarRfidNoDevice = 0;
    public short IcarRfidPcscDevice = 1;
    public short IcarRfidAskDevice = 2;
    public short IcarRfidIdboxDevice = 3;

    public short IcarScNoDevice = 0;
    public short IcarScIdboxDevice = 1;

    public short IcarNoAutoInfoIntegration = 0;
    public short IcarAutoInfoIntegrationHorTiling = 1;
    public short IcarAutoInfoIntegrationVerTiling = 2;
    public short IcarAutoInfoNoImageIntegration = 5;

    public short IcarHorizontalTiling = 0;
    public short IcarVerticalTiling = 1;

    public short IcarCfgAutoinfomerging = 0;
    public short IcarCfgDateformat = 1;
    public short IcarCfgUppercase = 2;
    public short IcarCfgRemoveaccents = 3;
    public short IcarCfgProcessmode = 4;
    public short IcarCfgOutputdocimageres = 5;
    public short IcarCfgOutputphotoimageres = 6;
    public short IcarCfgOutputsignatureimageres = 7;
    public short IcarCfgOutputfingerprintimageres = 8;
    public short IcarCfgOutputdigitalcopyimageres = 9;
    public short IcarCfgColormodedocimage = 10;
    public short IcarCfgColormodephotoimage = 11;
    public short IcarCfgColormodesignatureimage = 12;
    public short IcarCfgColormodefingerprintimage = 13;
    public short IcarCfgColormodedigitalcopyimage = 14;
    public short IcarCfgDocimageenhancement = 15;
    public short IcarCfgPhotoimageenhancement = 16;
    public short IcarCfgSignatureimageenhancement = 17;
    public short IcarCfgFingerprintimageenhancement = 18;
    public short IcarCfgDigitalcopyimageenhancement = 19;
    public short IcarAcqDevice = 20;
    public short IcarAcqDevicename = 21;
    public short IcarAcqImagefilepath = 22;
    public short IcarAcqDirectorypath = 23;
    public short IcarAcqDelfileimageafterprocessing = 25;
    public short IcarResultDocimagepath = 26;
    public short IcarResultPhotoimagepath = 27;
    public short IcarResultSignatureimagepath = 28;
    public short IcarResultFingerprintimagepath = 29;
    public short IcarResultDigitalcopyimagepath = 30;
    public short IcarDcopyWidth = 31;
    public short IcarDcopyHeight = 32;
    public short IcarResultXmlfilepath = 33;
    public short IcarAcqTwainacquisitionsizex = 34;
    public short IcarAcqTwainacquisitionsizey = 35;
    public short IcarAcqTwainfullduplexenabled = 36;
    public short IcarAcqTwainsidestoscan = 37;
    public short IcarAcqTwainresolution = 38;
    public short IcarAcqTwaincolormode = 39;
    public short IcarCfgOutputdocuvimageres = 40;
    public short IcarCfgOutputdocirimageres = 41;
    public short IcarCfgColormodedocuvimage = 42;
    public short IcarCfgColormodedocirimage = 43;
    public short IcarCfgDocuvimageenhancement = 44;
    public short IcarCfgDocirimageenhancement = 45;
    public short IcarResultDocuvimagepath = 46;
    public short IcarResultDocirimagepath = 47;
    public short IcarCfgOutputdigitalcopyuvimageres = 48;
    public short IcarCfgOutputdigitalcopyirimageres = 49;
    public short IcarCfgColormodedigitalcopyuvimage = 50;
    public short IcarCfgColormodedigitalcopyirimage = 51;
    public short IcarCfgDigitalcopyuvimageenhancement = 52;
    public short IcarCfgDigitalcopyirimageenhancement = 53;
    public short IcarResultDigitalcopyuvimagepath = 54;
    public short IcarResultDigitalcopyirimagepath = 55;
    public short IcarAcqEnableVisibleLighting = 56;
    public short IcarAcqEnableUvLighting = 57;
    public short IcarAcqEnableIrLighting = 58;
    public short IcarCfgInputdocimagexres = 65;
    public short IcarCfgInputdocimageyres = 66;
    public short IcarRfidDevice = 71;
    public short IcarResultRfidPhotoimagepath = 73;
    public short IcarScDevice = 86;
    public short IcarAcqAllImages = 129;
    public short IcarAcqSaveImages = 130;

    #endregion

    #region Private Properties
    private ICARCOMLib.ICAR m_icar;

		private string m_photo_image_path;
		private string m_digicopy_image_path;
	  private string m_doc_image_path;
	  private string m_signature_image_path;
	  private string m_fingerprint_image_path;

		private string m_cphoto_image_path;
		private string m_cdigicopy_image_path;
		private string m_cdoc_image_path;
		private string m_csignature_image_path;
		private string m_cfingerprint_image_path;


	  #endregion

    public override string Name
    {
      get { return "ICarBoxBasic"; }
    }

	  private Bitmap LoadImageFromStream(string _filename)
	  {
			using (FileStream stream = new FileStream(_filename, FileMode.Open, FileAccess.Read))
			{
				return (Image.FromStream(stream).Clone() as Bitmap).ToByteArray().ToImage() as Bitmap;
			}
		}

    //private Bitmap GetPhoto()
    //{
    //  if (!File.Exists(m_cphoto_image_path))
    //  {
    //    return null;
    //  }
    //  try
    //  {
    //    return LoadImageFromStream(m_cphoto_image_path);
    //  }
    //  catch (Exception)
    //  {
    //    return null;
    //  }
    //}
    //private Bitmap GetDigiCopy()
    //{
    //  if (!File.Exists(m_cdigicopy_image_path))
    //  {
    //    return null;
    //  }
    //  try
    //  {
    //    return LoadImageFromStream(m_cdigicopy_image_path);
    //  }
    //  catch (Exception)
    //  {
    //    return null;
    //  }
    //}
    //private Bitmap GetDoc()
    //{
    //  if (!File.Exists(m_cdoc_image_path))
    //  {
    //    return null;
    //  }
    //  try
    //  {
    //    return LoadImageFromStream(m_cdoc_image_path);
    //  }
    //  catch (Exception)
    //  {
    //    return null;
    //  }
    //}
    //private Bitmap GetSignature()
    //{
    //  if (!File.Exists(m_csignature_image_path))
    //  {
    //    return null;
    //  }
    //  try
    //  {
    //    return LoadImageFromStream(m_csignature_image_path);
    //  }
    //  catch (Exception)
    //  {
    //    return null;
    //  }
    //}

    //private Bitmap GetFingerPrint()
    //{
    //  if (!File.Exists(m_cfingerprint_image_path))
    //  {
    //    return null;
    //  }
    //  try
    //  {
    //    return LoadImageFromStream(m_cfingerprint_image_path);
    //  }
    //  catch (Exception)
    //  {
    //    return null;
    //  }
    //}


    /// <summary>
    /// if exists directory then delete it with all files
    /// </summary>
    public void DeletePhotoTemp(Guid? sessionGuid)
    {
      if (!Directory.Exists(AppData))
      {
        return;
      }
      try
      {
        if (File.Exists(m_photo_image_path))
          File.Delete(m_photo_image_path);
        if (File.Exists(m_digicopy_image_path))
          File.Delete(m_digicopy_image_path);
        if (File.Exists(m_doc_image_path))
          File.Delete(m_doc_image_path);
        if (File.Exists(m_signature_image_path))
          File.Delete(m_signature_image_path);
        if (File.Exists(m_fingerprint_image_path))
          File.Delete(m_fingerprint_image_path);

        if (!sessionGuid.HasValue) return;

        m_cphoto_image_path = Path.Combine(AppData, "icar-" + sessionGuid + "-photo.bmp");
        m_cdigicopy_image_path = Path.Combine(AppData, "icar-" + sessionGuid + "-digicopy.bmp");
        m_cdoc_image_path = Path.Combine(AppData, "icar-" + sessionGuid + "-doc.bmp");
        m_csignature_image_path = Path.Combine(AppData, "icar-" + sessionGuid + "-signature.bmp");
        m_cfingerprint_image_path = Path.Combine(AppData, "icar-" + sessionGuid + "-fingerprint.bmp");

        if (File.Exists(m_cphoto_image_path))
          File.Delete(m_cphoto_image_path);
        if (File.Exists(m_cdigicopy_image_path))
          File.Delete(m_cdigicopy_image_path);
        if (File.Exists(m_cdoc_image_path))
          File.Delete(m_cdoc_image_path);
        if (File.Exists(m_csignature_image_path))
          File.Delete(m_csignature_image_path);
        if (File.Exists(m_cfingerprint_image_path))
          File.Delete(m_cfingerprint_image_path);
      }
      catch (Exception)
      {
        // did not delete
      }
    }


    public override ENUM_IDScannerStatus Init()
    {
      try
      {
        if (!Directory.Exists(AppData)) Directory.CreateDirectory(AppData);

         m_photo_image_path = Path.Combine(AppData, "photo.bmp");
	       m_digicopy_image_path = Path.Combine(AppData, "digicopy.bmp");
				 m_doc_image_path= Path.Combine(AppData, "doc.bmp");
				 m_signature_image_path= Path.Combine(AppData, "signature.bmp");
				 m_fingerprint_image_path= Path.Combine(AppData, "fingerprint.bmp");






				m_icar = new ICARCOMLib.ICAR();
        m_icar.initialize();
        if (m_icar.checkError() != 0) throw new Exception();
        m_icar.setPropertyNumber(ConnectionProperty, ConnectionValue);
        if (m_icar.checkError() != 0) throw new Exception();
        m_icar.setPropertyString(IcarResultPhotoimagepath, m_photo_image_path);
        if (m_icar.checkError() != 0) throw new Exception();
				m_icar.setPropertyString(IcarResultDigitalcopyimagepath, m_digicopy_image_path);
				if (m_icar.checkError() != 0) throw new Exception();
	      m_icar.setPropertyString(IcarResultDocimagepath, m_doc_image_path);
				if (m_icar.checkError() != 0) throw new Exception();
	      m_icar.setPropertyString(IcarResultSignatureimagepath, m_signature_image_path);
				if (m_icar.checkError() != 0) throw new Exception();
	      m_icar.setPropertyString(IcarResultFingerprintimagepath, m_fingerprint_image_path);
				if (m_icar.checkError() != 0) throw new Exception();
				m_icar.DocumentRead += Icar_DocumentRead;
        if (m_icar.checkError() != 0) throw new Exception();
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
      }
      catch (AccessViolationException)
      {
        //Log(_ex);
        m_icar.cleanup();
        m_icar = null;
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.ERROR;
      }
      catch (Exception)
      {
        //Log(_ex);
        m_icar.cleanup();
        Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.ERROR;
        m_icar = null;
      }
      return Status;
    }

    private Guid? m_last_scan_session = null;

	  /// <summary>
	  /// Event gets results
	  /// </summary>
	  /// <param name="BstrDocRead"></param>
	  private void Icar_DocumentRead(ref string BstrDocRead)
	  {
	    Guid _scan_session = Guid.NewGuid();

		   m_cphoto_image_path = Path.Combine(AppData, "icar-"+ _scan_session + "-photo.bmp");
			 m_cdigicopy_image_path = Path.Combine(AppData, "icar-" + _scan_session + "-digicopy.bmp");
			 m_cdoc_image_path = Path.Combine(AppData, "icar-" + _scan_session + "-doc.bmp");
			 m_csignature_image_path = Path.Combine(AppData, "icar-" + _scan_session + "-signature.bmp");
			 m_cfingerprint_image_path = Path.Combine(AppData, "icar-" + _scan_session + "-fingerprint.bmp");


		  if (File.Exists(m_photo_image_path))
			  File.Copy(m_photo_image_path, m_cphoto_image_path);

		  if (File.Exists(m_digicopy_image_path))
				File.Copy(m_digicopy_image_path, m_cdigicopy_image_path);

		  if (File.Exists(m_doc_image_path))
				File.Copy(m_doc_image_path, m_cdoc_image_path);

		  if (File.Exists(m_signature_image_path))
				File.Copy(m_signature_image_path, m_csignature_image_path);

		  if (File.Exists(m_fingerprint_image_path))
				File.Copy(m_fingerprint_image_path, m_cfingerprint_image_path);








		  Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE;
		  Dictionary<string, object> _params_scan;
		  _params_scan = new Dictionary<string, object>();

		  DateTime _date;

		  var _list_names = DivideString(m_icar.getFieldContent("NAME") ?? "");
		  var _list_surnames = DivideString(m_icar.getFieldContent("SURNAME") ?? "");
		  if (_list_names.Length > 0)
			  _params_scan.Add("NAME", _list_names[0]);
		  else
			  m_icar.getFieldContent("NAME");

		  if (_list_surnames.Length == 0)
		  {
			  switch (_list_names.Length)
			  {
				  case 2:
					  _params_scan.Add("SURNAME", _list_names[1]);
					  break;
				  case 3:
					  _params_scan.Add("NAME2", _list_names[1]);
					  _params_scan.Add("SURNAME", _list_names[2]);
					  break;
				  case 4:
					  _params_scan.Add("NAME2", _list_names[1]);
					  _params_scan.Add("SURNAME", _list_names[2]);
					  _params_scan.Add("SURNAME2", _list_names[3]);
					  break;
			  }
		  }
		  else
		  {
			  if (_list_names.Length == 2)
			  {
				  _params_scan.Add("NAME2", _list_names[1]);
			  }
			  _params_scan.Add("SURNAME", _list_surnames[0]);
			  _params_scan.Add("SURNAME2", _list_surnames.Length > 1 ? _list_surnames[1] : "");

		  }

		  _params_scan.Add("EXPEDITOR", m_icar.getFieldContent("EXPEDITOR"));
		  _params_scan.Add("TYPE", IdentificationTypes.ScannedTypeToID(m_icar.getFieldContent("TYPE")));

		  _params_scan.Add("DOC_NUMBER", m_icar.getFieldContent("DOC_NUMBER"));
		  _params_scan.Add("ID_NUMBER", m_icar.getFieldContent("ID_NUMBER"));
		  _params_scan.Add("STREET_ADDRESS", m_icar.getFieldContent("STREET_ADDRESS"));
		  _params_scan.Add("CITY_ADDRESS", m_icar.getFieldContent("CITY_ADDRESS"));
		  int _country_id;
		  Countries.ISO2ToCountryId(Countries.ISO3toISO2(m_icar.getFieldContent("NATIONALITY")), out _country_id);
		  _params_scan.Add("NATIONALITY", _country_id);

		  if (!string.IsNullOrEmpty(m_icar.getFieldContent("SEX")))
		  {
			  GENDER _gender;
			  switch (m_icar.getFieldContent("SEX"))
			  {
				  case "M":
					  _gender = GENDER.MALE;
					  break;
				  case "F":
					  _gender = GENDER.FEMALE;
					  break;
				  default:
					  _gender = GENDER.UNKNOWN;
					  break;
			  }
			  if(_gender!=GENDER.UNKNOWN)
					_params_scan.Add("SEX", (int)_gender);
		  }

		  if (DateTime.TryParse(m_icar.getFieldContent("BIRTHDATE"),
			  new DateTimeFormatInfo() {DateSeparator = "-", ShortDatePattern = @"MM-dd-yyyy"}, DateTimeStyles.AssumeLocal,
			  out _date))
		  {
			  _params_scan.Add("BIRTHDATE", _date.Date);
		  }
		  if (DateTime.TryParse(m_icar.getFieldContent("EXPEDITION_DATE"),
			  new DateTimeFormatInfo() {DateSeparator = "-", ShortDatePattern = @"MM-dd-yyyy"}, DateTimeStyles.AssumeLocal,
			  out _date))
		  {
			  _params_scan.Add("EXPEDITION_DATE", _date.Date);
		  }

		  if (DateTime.TryParse(m_icar.getFieldContent("EXPIRY"),
			  new DateTimeFormatInfo() {DateSeparator = "-", ShortDatePattern = @"MM-dd-yyyy"}, DateTimeStyles.AssumeLocal,
			  out _date))
		  {
			  _params_scan.Add("EXPIRY", _date.Date);
		  }
		  _params_scan.Add("ADDRESS", m_icar.getFieldContent("ADDRESS"));
		  _params_scan.Add("ZIP", m_icar.getFieldContent("ZIP"));
		  _params_scan.Add("BIRTHPLACE", m_icar.getFieldContent("BIRTHPLACE"));
			_params_scan.Add("DIGICOPY", m_cdigicopy_image_path);
			_params_scan.Add("DOC", m_cdoc_image_path);
			_params_scan.Add("SIGNATURE", m_csignature_image_path);
			_params_scan.Add("FINGERPRINT", m_cfingerprint_image_path);
			_params_scan.Add("PHOTO", m_cphoto_image_path);
		  _params_scan.Add("STATE_ADDRESS", m_icar.getFieldContent("STATE_ADDRESS"));
		  //_params_scan.Add("PARENTS", m_icar.getFieldContent("PARENTS"));
		  //_params_scan.Add("BARCODE", m_icar.getFieldContent("BARCODE"));
		  //_params_scan.Add("OTHER", m_icar.getFieldContent("OTHER"));

		  ENUM_DetectableIDClass _found_type;
		  switch (m_icar.getFieldContent("TYPE"))
		  {
			  case "IDENTITY":
				  _found_type = ENUM_DetectableIDClass.ID_CARD;
				  break;
			  case "PASSPORT":
				  _found_type = ENUM_DetectableIDClass.PASSPORT;
				  break;
			  case "DRIVER_LICENSE":
				  _found_type = ENUM_DetectableIDClass.DRIVING_LICENCE;
				  break;
			  case "VISA":
				  _found_type = ENUM_DetectableIDClass.VISA;
				  break;
			  case "TRIPULATION":
				  _found_type = ENUM_DetectableIDClass.CREW_CARD;
				  break;
			  case "RESIDENT_CARD":
				  _found_type = ENUM_DetectableIDClass.RESIDENCE_CARD;
				  break;
			  case "PERSONAL_CARD":
				  _found_type = ENUM_DetectableIDClass.PERSONAL_CARD;
				  break;
			  default:
				  _found_type = ENUM_DetectableIDClass.UNKNOWN;
				  break;
		  }
			if (_found_type != ENUM_DetectableIDClass.UNKNOWN)
				_params_scan.Add("ID_TYPE", (int)_found_type);
		  _params_scan.Add("SIDE", m_icar.getFieldContent("SIDE"));
			_params_scan.Add("ISFRONT", IsFront(m_icar.getFieldContent("SIDE")));
			if (ScannerEvent != null)

			  ThreadPool.QueueUserWorkItem(delegate
			  {
					ScannerEvent(new IDScannerInfoEvent()
					{
						info = new IDScannerInfo() {IDScannerFixedFieldMapping = FixedFieldMapping, Fields = _params_scan}
					});
			  });

		    DeletePhotoTemp(m_last_scan_session);

	    m_last_scan_session = _scan_session;
	  }

		private bool IsFront(string p)
		{
			return p != null && p.Equals("1");
		}

	  /// <summary>
    /// Divides string and returns all items
    /// </summary>
    /// <param name="CompleteString"></param>
    /// <returns></returns>
    private string[] DivideString(String CompleteString)
    {
      if (string.IsNullOrEmpty(CompleteString))
        return new string[] { };
      var _list_names = new List<String>();

      char[] _dividers = { ' ', '\n' };
      try
      {
        if (CompleteString.IndexOf(' ') == 0 || CompleteString.IndexOf('\n') == 0)
        {
          _list_names.Add(CompleteString);
          return _list_names.ToArray();
        }
        string[] _string_isolate = CompleteString.Split(_dividers);

        foreach (String _str in _string_isolate)
        {
          _list_names.Add(_str);
        }
      }
      catch (Exception _ex)
      {
        Log(_ex);
      }
      return _list_names.ToArray();
    }

    private void Log(Exception Ex)
    {
      throw new Exception(Ex.Message);
    }


    protected override void Dispose (bool Disposing)
    {
      base.Dispose(Disposing);
      if (!Disposing)
        return;
      if (m_icar != null)
        m_icar.cleanup();
      m_icar = null;
      Status = ENUM_IDScannerStatus.DISCONNECTED;
    }

    public override void Scan()
    {
      Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.INUSE;
      m_icar.process();
    }

  
    public void ScanAdvanced(Dictionary<String, object> AdvancedParams)
    {

      AdvancedScan.ICAR_PROPERTY _pr;
      Status = ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.INUSE;


      //Minimum memory
      _pr = (AdvancedScan.ICAR_PROPERTY)AdvancedParams["ICAR_CFG_USE_MINIMUM_MEMORY"];
      m_icar.setPropertyNumber(_pr.Property, _pr.ValueToSet);

      //Photo colour
      _pr = (AdvancedScan.ICAR_PROPERTY)AdvancedParams["ICAR_CFG_COLORMODEPHOTOIMAGE"];
      m_icar.setPropertyNumber(_pr.Property, _pr.ValueToSet);

      //Remove accents
      _pr = (AdvancedScan.ICAR_PROPERTY)AdvancedParams["ICAR_CFG_REMOVEACCENTS"];
      m_icar.setPropertyNumber(_pr.Property, _pr.ValueToSet);

      //Quality mode
      _pr = (AdvancedScan.ICAR_PROPERTY)AdvancedParams["ICAR_CFG_QUALITY_MODE"];
      m_icar.setPropertyNumber(_pr.Property, _pr.ValueToSet);

    
      //Resolution images
      _pr = (AdvancedScan.ICAR_PROPERTY)AdvancedParams["ICAR_CFG_INPUTDOCIMAGEXRES"];
      m_icar.setPropertyNumber(_pr.Property, _pr.ValueToSet); //Resolution x
      m_icar.setPropertyNumber(_pr.Property + 1, _pr.ValueToSet);//Resolution y

      m_icar.process();
    }

    public override bool InstallDriver()
    {
      if (IsDriverInstalled())
      {
        return true;
      }
      Stream _stream =
        Assembly.GetExecutingAssembly()
          .GetManifestResourceStream("IDScannerConnectorICarIDBoxBasic.icar_driver_package.zip");

      ZipFile _zf = null;
      string _extractionroot = Path.Combine(AppData, "icar_driver_package");
      try
      {
        _zf = new ZipFile(_stream);
        foreach (ZipEntry _zip_entry in _zf)
        {
          if (!_zip_entry.IsFile)
          {
            continue; // Ignore directories
          }
          String _entry_file_name = _zip_entry.Name;

          byte[] _buffer = new byte[4096]; // 4K is optimum
          Stream _zip_stream = _zf.GetInputStream(_zip_entry);

          String _full_zip_to_path = Path.Combine(_extractionroot, _entry_file_name);
          string _directory_name = Path.GetDirectoryName(_full_zip_to_path);
          if (!string.IsNullOrEmpty(_directory_name))
            Directory.CreateDirectory(_directory_name);

          using (FileStream _stream_writer = File.Create(_full_zip_to_path))
          {
            StreamUtils.Copy(_zip_stream, _stream_writer, _buffer);
          }
        }
      }
      finally
      {
        if (_zf != null)
        {
          _zf.IsStreamOwner = true;
          _zf.Close();

        }
      }
      ProcessStartInfo _start_info = new ProcessStartInfo
        {
          WorkingDirectory = _extractionroot,
          FileName = _extractionroot + "\\install.bat"
        };

      try
      {
        new FileIOPermission(FileIOPermissionAccess.Read, _start_info.FileName).Demand();

        Process _proc = Process.Start(_start_info);
        if (_proc == null)
        {
          return false;
        }
        _proc.WaitForExit((60 * 1000) * 7);
        //Directory.Delete(_extractionroot, true);
      }
      catch (SecurityException)
      {
        //
      }
      return IsDriverInstalled();

    }
		const string SOFTWARE_NAME_KEY = "DisplayName";
		const string SOFTWARE_NAME_VALUE = "ICAR";
		const string SOFTWARE_NAME_VALUE2 = "IDBOX";

		const string DRIVER_NAME_KEY="FriendlyName";
	  const string DRIVER_NAME_VALUE = "IDBox USB Driver";

    public override bool IsDriverInstalled()
    {
      bool _found_driver = false;
	    RegistryKey _usb_devices = null;
	    try
	    {
		    _usb_devices =
			    Registry.LocalMachine.OpenSubKey(@"SYSTEM\ControlSet001\Control\Class\{36FC9E60-C465-11CF-8056-444553540000}");
	    }
	    catch
	    {
				//swallow
	    }
	    if (_usb_devices == null)
      {
        return false;
      }
      foreach (string _reg_key_names in _usb_devices.GetSubKeyNames())
      {
				RegistryKey _sub_key;
				try
				{
					_sub_key = _usb_devices.OpenSubKey(_reg_key_names);
				}
				catch
				{
					_sub_key = null;
				}
				if (_sub_key == null)
				{
					continue;
				}
				string _device_name = _sub_key.GetValue(DRIVER_NAME_KEY) as string;
				if (string.IsNullOrEmpty(_device_name) || !_device_name.Contains(DRIVER_NAME_VALUE))
        {
          continue;
        }
        _found_driver = true;
        break;
      }

      if (!_found_driver) return false;

      //64bit
	    RegistryKey _reg_key64 = null;
	    try
	    {
				_reg_key64= Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" + @"\");
	    }
	    catch
	    {
				//swallow
	    }
	    if (_reg_key64 == null)
      {
        //32bit
        RegistryKey _reg_key32 =
          Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" + @"\");
        if (_reg_key32 == null)
        {
          return false;
        }
        string[] _sub_key_names32 = _reg_key32.GetSubKeyNames();
        foreach (string _key_name in _sub_key_names32)
        {
          RegistryKey _regkey =
            Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" + @"\" + _key_name);


          if (_regkey == null)
          {
            continue;
          }
					string _software_name = _regkey.GetValue(SOFTWARE_NAME_KEY) as string;

          if (!string.IsNullOrEmpty(_software_name) &&
							(_software_name.ToUpperInvariant().Contains(SOFTWARE_NAME_VALUE) || _software_name.ToUpperInvariant().Contains(SOFTWARE_NAME_VALUE2)))
          {
            return true;
          }
        }
        return false;
      }
      string[] _sub_key_names64 = _reg_key64.GetSubKeyNames();
      foreach (string _key_name in _sub_key_names64)
      {
        RegistryKey _regkey =
          Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" + @"\" +
                                           _key_name);


        if (_regkey == null)
        {
          continue;
        }
        string _software_name = _regkey.GetValue("DisplayName") as string;

        if (!string.IsNullOrEmpty(_software_name) &&
						(_software_name.ToUpperInvariant().Contains(SOFTWARE_NAME_VALUE) || _software_name.ToUpperInvariant().Contains(SOFTWARE_NAME_VALUE2)))
        {
          return true;
        }
      }
      return false;
    }

    public override void ShowAdvancedScan()
    {
      new AdvancedScan(this).ShowDialog();
    }
    
    protected override Dictionary<ENUM_IDScannerFixedField, string> FixedFieldMapping
    {
      get
      {
        return new Dictionary<ENUM_IDScannerFixedField, string>()
          {
            {ENUM_IDScannerFixedField.DATE_OF_BIRTH, "BIRTHDATE"},
            {ENUM_IDScannerFixedField.ID_NUMBER, "ID_NUMBER"},
            {ENUM_IDScannerFixedField.ID_TYPE, "ID_TYPE"},
            {ENUM_IDScannerFixedField.NAME, "NAME"},
            {ENUM_IDScannerFixedField.NAME2, "NAME2"},
            {ENUM_IDScannerFixedField.SURNAME, "SURNAME"},
            {ENUM_IDScannerFixedField.SURNAME2, "SURNAME2"},
            {ENUM_IDScannerFixedField.PHOTO, "PHOTO"},
            {ENUM_IDScannerFixedField.SEX, "SEX"},
            {ENUM_IDScannerFixedField.BIRTHPLACE, "BIRTHPLACE"},
            {ENUM_IDScannerFixedField.ADDRESS, "STREET_ADDRESS"},
            {ENUM_IDScannerFixedField.CITY, "CITY_ADDRESS"},
            {ENUM_IDScannerFixedField.NATIONALITY, "NATIONALITY"},
            {ENUM_IDScannerFixedField.ZIP, "ZIP"},
						{ENUM_IDScannerFixedField.DOCUMENT_EXPIRY,"EXPIRY"},
            {ENUM_IDScannerFixedField.FRONT, "DOC"},
						{ENUM_IDScannerFixedField.IS_FRONT,"ISFRONT"}
          };
      }
    }
  }
}

