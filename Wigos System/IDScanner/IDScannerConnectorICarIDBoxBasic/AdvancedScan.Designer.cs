﻿namespace IDScannerConnectorICarIDBoxBasic
{
  partial class AdvancedScan
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlContainer = new System.Windows.Forms.Panel();
      this.pnlLeft = new System.Windows.Forms.Panel();
      this.grpResolution = new System.Windows.Forms.GroupBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.trkResolution = new System.Windows.Forms.TrackBar();
      this.grpDefaultParameters = new System.Windows.Forms.GroupBox();
      this.chkRemoveAccents = new System.Windows.Forms.CheckBox();
      this.chkImageColor = new System.Windows.Forms.CheckBox();
      this.chkMinimalMemory = new System.Windows.Forms.CheckBox();
      this.grpQualityMode = new System.Windows.Forms.GroupBox();
      this.chkTexture = new System.Windows.Forms.CheckBox();
      this.chkBrightness = new System.Windows.Forms.CheckBox();
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.btnScanDocument = new System.Windows.Forms.Button();
      this.pnlContainer.SuspendLayout();
      this.pnlLeft.SuspendLayout();
      this.grpResolution.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.trkResolution)).BeginInit();
      this.grpDefaultParameters.SuspendLayout();
      this.grpQualityMode.SuspendLayout();
      this.pnlBottom.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlContainer
      // 
      this.pnlContainer.Controls.Add(this.pnlLeft);
      this.pnlContainer.Controls.Add(this.pnlBottom);
      this.pnlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlContainer.Location = new System.Drawing.Point(0, 0);
      this.pnlContainer.Name = "pnlContainer";
      this.pnlContainer.Size = new System.Drawing.Size(308, 446);
      this.pnlContainer.TabIndex = 0;
      // 
      // pnlLeft
      // 
      this.pnlLeft.Controls.Add(this.grpResolution);
      this.pnlLeft.Controls.Add(this.grpDefaultParameters);
      this.pnlLeft.Controls.Add(this.grpQualityMode);
      this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLeft.Location = new System.Drawing.Point(0, 0);
      this.pnlLeft.MaximumSize = new System.Drawing.Size(306, 0);
      this.pnlLeft.MinimumSize = new System.Drawing.Size(306, 0);
      this.pnlLeft.Name = "pnlLeft";
      this.pnlLeft.Size = new System.Drawing.Size(306, 382);
      this.pnlLeft.TabIndex = 1;
      // 
      // grpResolution
      // 
      this.grpResolution.Controls.Add(this.label5);
      this.grpResolution.Controls.Add(this.label1);
      this.grpResolution.Controls.Add(this.label4);
      this.grpResolution.Controls.Add(this.label3);
      this.grpResolution.Controls.Add(this.label2);
      this.grpResolution.Controls.Add(this.trkResolution);
      this.grpResolution.Location = new System.Drawing.Point(8, 269);
      this.grpResolution.Name = "grpResolution";
      this.grpResolution.Size = new System.Drawing.Size(288, 104);
      this.grpResolution.TabIndex = 3;
      this.grpResolution.TabStop = false;
      this.grpResolution.Text = "xResolution";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(189, 52);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(25, 13);
      this.label5.TabIndex = 9;
      this.label5.Text = "650";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(74, 52);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(25, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "350";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(243, 61);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(33, 19);
      this.label4.TabIndex = 7;
      this.label4.Text = "800";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(129, 61);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(33, 19);
      this.label3.TabIndex = 6;
      this.label3.Text = "500";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(11, 61);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(33, 19);
      this.label2.TabIndex = 4;
      this.label2.Text = "200";
      // 
      // trkResolution
      // 
      this.trkResolution.LargeChange = 150;
      this.trkResolution.Location = new System.Drawing.Point(15, 26);
      this.trkResolution.Maximum = 800;
      this.trkResolution.Minimum = 200;
      this.trkResolution.Name = "trkResolution";
      this.trkResolution.Size = new System.Drawing.Size(258, 45);
      this.trkResolution.SmallChange = 150;
      this.trkResolution.TabIndex = 5;
      this.trkResolution.TickFrequency = 150;
      this.trkResolution.Value = 200;
      // 
      // grpDefaultParameters
      // 
      this.grpDefaultParameters.Controls.Add(this.chkRemoveAccents);
      this.grpDefaultParameters.Controls.Add(this.chkImageColor);
      this.grpDefaultParameters.Controls.Add(this.chkMinimalMemory);
      this.grpDefaultParameters.Location = new System.Drawing.Point(10, 12);
      this.grpDefaultParameters.Name = "grpDefaultParameters";
      this.grpDefaultParameters.Size = new System.Drawing.Size(288, 132);
      this.grpDefaultParameters.TabIndex = 1;
      this.grpDefaultParameters.TabStop = false;
      this.grpDefaultParameters.Text = "xDefault parameters";
      // 
      // chkRemoveAccents
      // 
      this.chkRemoveAccents.AutoSize = true;
      this.chkRemoveAccents.Location = new System.Drawing.Point(6, 89);
      this.chkRemoveAccents.Name = "chkRemoveAccents";
      this.chkRemoveAccents.Size = new System.Drawing.Size(141, 23);
      this.chkRemoveAccents.TabIndex = 2;
      this.chkRemoveAccents.Text = "xRemove accents";
      this.chkRemoveAccents.UseVisualStyleBackColor = true;
      // 
      // chkImageColor
      // 
      this.chkImageColor.AutoSize = true;
      this.chkImageColor.Location = new System.Drawing.Point(6, 58);
      this.chkImageColor.Name = "chkImageColor";
      this.chkImageColor.Size = new System.Drawing.Size(117, 23);
      this.chkImageColor.TabIndex = 1;
      this.chkImageColor.Text = "xImageColour";
      this.chkImageColor.UseVisualStyleBackColor = true;
      // 
      // chkMinimalMemory
      // 
      this.chkMinimalMemory.AutoSize = true;
      this.chkMinimalMemory.Location = new System.Drawing.Point(6, 29);
      this.chkMinimalMemory.Name = "chkMinimalMemory";
      this.chkMinimalMemory.Size = new System.Drawing.Size(141, 23);
      this.chkMinimalMemory.TabIndex = 0;
      this.chkMinimalMemory.Text = "xMinimalMemory";
      this.chkMinimalMemory.UseVisualStyleBackColor = true;
      // 
      // grpQualityMode
      // 
      this.grpQualityMode.Controls.Add(this.chkTexture);
      this.grpQualityMode.Controls.Add(this.chkBrightness);
      this.grpQualityMode.Location = new System.Drawing.Point(8, 160);
      this.grpQualityMode.Name = "grpQualityMode";
      this.grpQualityMode.Size = new System.Drawing.Size(288, 94);
      this.grpQualityMode.TabIndex = 2;
      this.grpQualityMode.TabStop = false;
      this.grpQualityMode.Text = "xQuality Mode";
      // 
      // chkTexture
      // 
      this.chkTexture.AutoSize = true;
      this.chkTexture.Location = new System.Drawing.Point(6, 55);
      this.chkTexture.Name = "chkTexture";
      this.chkTexture.Size = new System.Drawing.Size(98, 23);
      this.chkTexture.TabIndex = 5;
      this.chkTexture.Text = "chkTexture";
      this.chkTexture.UseVisualStyleBackColor = true;
      // 
      // chkBrightness
      // 
      this.chkBrightness.AutoSize = true;
      this.chkBrightness.Location = new System.Drawing.Point(6, 26);
      this.chkBrightness.Name = "chkBrightness";
      this.chkBrightness.Size = new System.Drawing.Size(104, 23);
      this.chkBrightness.TabIndex = 4;
      this.chkBrightness.Text = "xBrightness";
      this.chkBrightness.UseVisualStyleBackColor = true;
      // 
      // pnlBottom
      // 
      this.pnlBottom.Controls.Add(this.btnScanDocument);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 382);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(308, 64);
      this.pnlBottom.TabIndex = 0;
      // 
      // btnScanDocument
      // 
      this.btnScanDocument.FlatAppearance.BorderSize = 0;
      this.btnScanDocument.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
      this.btnScanDocument.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btnScanDocument.Location = new System.Drawing.Point(78, 16);
      this.btnScanDocument.Name = "btnScanDocument";
      this.btnScanDocument.Size = new System.Drawing.Size(133, 36);
      this.btnScanDocument.TabIndex = 2;
      this.btnScanDocument.Text = "xScan document";
      this.btnScanDocument.UseVisualStyleBackColor = true;
      this.btnScanDocument.Click += new System.EventHandler(this.btnScanDocument_Click);
      // 
      // AdvancedScan
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(308, 446);
      this.Controls.Add(this.pnlContainer);
      this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "AdvancedScan";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Opciones de escaneado avanzadas";
      this.pnlContainer.ResumeLayout(false);
      this.pnlLeft.ResumeLayout(false);
      this.grpResolution.ResumeLayout(false);
      this.grpResolution.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.trkResolution)).EndInit();
      this.grpDefaultParameters.ResumeLayout(false);
      this.grpDefaultParameters.PerformLayout();
      this.grpQualityMode.ResumeLayout(false);
      this.grpQualityMode.PerformLayout();
      this.pnlBottom.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlContainer;
    private System.Windows.Forms.Panel pnlLeft;
    private System.Windows.Forms.GroupBox grpDefaultParameters;
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.GroupBox grpResolution;
    private System.Windows.Forms.GroupBox grpQualityMode;
    private System.Windows.Forms.TrackBar trkResolution;
    private System.Windows.Forms.CheckBox chkRemoveAccents;
    private System.Windows.Forms.CheckBox chkImageColor;
    private System.Windows.Forms.CheckBox chkMinimalMemory;
    private System.Windows.Forms.Button btnScanDocument;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox chkTexture;
    private System.Windows.Forms.CheckBox chkBrightness;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label1;

  }
}