﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.IDScanner;
namespace IDScannerConnectorICarIDBoxBasic
{
  public partial class AdvancedScan : Form
  {

    public struct ICAR_PROPERTY
    {
      public byte Property;
      public int ValueToSet;
    }

    #region Consts

    const byte ICAR_ACQ_IDBOX_BUTTON_ENABLED = 98;
    const byte ICAR_ACQ_DEVICE_RESET = 138;
    const byte ICAR_CFG_USE_MINIMUM_MEMORY = 81;
    const byte ICAR_CFG_COLORMODEPHOTOIMAGE = 11;
    const byte ICAR_CFG_QUALITY_MODE = 88;
    const byte ICAR_CFG_REMOVEACCENTS = 3;
    const byte ICAR_CFG_MULTIPLE_RESOLUTION = 143;
    const byte ICAR_CFG_MULTIPLE_RESOLUTION_MIN = 144;
    const byte ICAR_CFG_MULTIPLE_RESOLUTION_MAX = 145;
    const byte ICAR_CFG_MULTIPLE_RESOLUTION_STEP = 146;
    const byte ICAR_CFG_INPUTDOCIMAGEXRES = 65;
    #endregion

    #region Privates Properties

    private ICarIDBoxBasicConnector m_carIDBoxBasicConnector;
    private Dictionary<String, object> m_advanced_params;
    private Boolean m_reset_scan;

    #endregion

    #region Public Methods
    /// <summary>
    /// Constructor
    /// </summary>
    public AdvancedScan()
    {
      InitializeComponent();
    }
    /// <summary>
    /// Constructor with driver ICARIDbox
    /// </summary>
    /// <param name="carIDBoxBasicConnector"></param>
    public AdvancedScan(ICarIDBoxBasicConnector carIDBoxBasicConnector)
      : this()
    {
      
      this.m_carIDBoxBasicConnector = carIDBoxBasicConnector;
      InitValues();
    }
    #endregion

    #region Events
    private void btnScanDocument_Click(object sender, EventArgs e)
    {
      this.SetParametersToScan();
      this.m_carIDBoxBasicConnector.ScanAdvanced(m_advanced_params);
      this.Dispose();
    }
#endregion

    #region Private Methods
    /// <summary>
    /// Initialize form  params 
    /// </summary>
    private void InitValues()
    {
      this.Text = Resource.String("STR_ADVANCED_SCAN");
      this.chkMinimalMemory.Text = Resource.String("STR_ADVANCED_SCAN_MINIMAL_MEMORY");
      this.chkMinimalMemory.Checked = false;
      this.chkImageColor.Text = Resource.String("STR_ADVANCED_SCAN_PHOTO_COLOURED");
      this.chkImageColor.Checked = true;
      this.chkRemoveAccents.Text = Resource.String("STR_ADVANCED_SCAN_REMOVE_ACCENTS");
      this.chkRemoveAccents.Checked = false;
      this.grpQualityMode.Text = Resource.String("STR_ADVANCED_SCAN_QUALITY_MODE");
      this.chkBrightness.Text = Resource.String("STR_ADVANCED_SCAN_QUALITY_MODE_BRIGHTNESS");
      this.chkBrightness.Checked = true;
      this.chkTexture.Text = Resource.String("STR_ADVANCED_SCAN_QUALITY_MODE_TEXTURE");
      this.grpResolution.Text = Resource.String("STR_ADVANCED_SCAN_RESOLUTION");
      this.grpDefaultParameters.Text = Resource.String("STR_ADVANCED_SCAN_DEFAULT_PARAMETERS");
      this.btnScanDocument.Text = Resource.String("STR_ADVANCED_SCAN_SCAN_BUTTON");
      this.trkResolution.Value = 200;
      m_reset_scan = false;
      m_advanced_params = new Dictionary<string, object>();
    }

    /// <summary>
    /// Sets 
    /// </summary>
    private void SetParametersToScan()
    {
      byte _value;

      if (this.chkMinimalMemory.Checked) _value = 1; else _value = 0;

      m_advanced_params.Add("ICAR_CFG_USE_MINIMUM_MEMORY", new ICAR_PROPERTY { Property = ICAR_CFG_USE_MINIMUM_MEMORY, ValueToSet = _value });

      if (this.chkImageColor.Checked) _value = 0; else _value = 1;
      m_advanced_params.Add("ICAR_CFG_COLORMODEPHOTOIMAGE", new ICAR_PROPERTY { Property = ICAR_CFG_COLORMODEPHOTOIMAGE, ValueToSet = _value });

      if (this.chkRemoveAccents.Checked) _value = 1; else _value = 0;
      m_advanced_params.Add("ICAR_CFG_REMOVEACCENTS", new ICAR_PROPERTY { Property = ICAR_CFG_REMOVEACCENTS, ValueToSet = _value });

      m_advanced_params.Add("ICAR_CFG_QUALITY_MODE", new ICAR_PROPERTY { Property = ICAR_CFG_QUALITY_MODE, ValueToSet = GetValueQualityMode(this.chkBrightness.Checked, this.chkTexture.Checked) });
      m_advanced_params.Add("ICAR_CFG_INPUTDOCIMAGEXRES", new ICAR_PROPERTY { Property = ICAR_CFG_INPUTDOCIMAGEXRES, ValueToSet = GetValueResolution() });

    }


    /// <summary>
    /// Sets parameters in Qualitiy Mode
    /// </summary>
    /// <param name="Brightness"></param>
    /// <param name="Texture"></param>
    /// <returns></returns>
    private byte GetValueQualityMode(Boolean Brightness, Boolean Texture)
    {

      byte _value;

      _value = 0;

      if (this.chkBrightness.Checked && this.chkTexture.Checked) _value = 3;
      if (!this.chkBrightness.Checked && this.chkTexture.Checked) _value = 2;
      if (this.chkBrightness.Checked && !this.chkTexture.Checked) _value = 1;
      if (!this.chkBrightness.Checked && !this.chkTexture.Checked) _value = 0;

      return _value;
    }

    /// <summary>
    /// Get Value to set new resolution to scan
    /// </summary>
    /// <returns></returns>
    private int GetValueResolution()
    {
      return this.trkResolution.Value;
    }

    #endregion

  }
}
