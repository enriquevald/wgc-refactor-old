﻿using System;
using System.Collections.Generic;
using System.IO;
using WSI.IDScanner.Model.Enums;

namespace WSI.IDScanner.Model
{
  public abstract class IDScannerDeviceBase: IIDScannerDevice
  {
    protected IDScannerInfoHandler ScannerEvent;
    protected ENUM_IDScannerStatus Status = ENUM_IDScannerStatus.UNKNOWN;
    protected readonly string AppData =
      Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WigosTemp");

    protected abstract Dictionary<ENUM_IDScannerFixedField, string> FixedFieldMapping { get; }

    public abstract string Name
    {
      get;
    }

    public abstract ENUM_IDScannerStatus Init();

    public bool IsAvailable()
    {
      return Status == (ENUM_IDScannerStatus.CONNECTED | ENUM_IDScannerStatus.IDLE);
    }
    public void AddListener(IDScannerInfoHandler EventHandler)
    {
      if (EventHandler == null)
        return;
      ScannerEvent += EventHandler;
    }

    public void RemoveListener(IDScannerInfoHandler EventHandler)
    {
      if (EventHandler == null)
      {
        ScannerEvent = null;
        return;
      }
      ScannerEvent -= EventHandler;
    }

    public ENUM_IDScannerStatus GetStatus()
    {
      return Status;
    }
    public abstract void Scan();

    public abstract bool InstallDriver();

    public abstract bool IsDriverInstalled();

    public abstract void ShowAdvancedScan();

    public  void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this); 
    }

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing)
        return;
      RemoveListener(null);
    }
  }
}
