using System;
using WigosModel.Interfaces;
using WSI.IDScanner.Model.Enums;

namespace WSI.IDScanner.Model
{
  public interface IIDScannerDevice :IWigosPlugin,IDisposable
  {
    string Name { get; }
    ENUM_IDScannerStatus Init();
    bool IsAvailable(); 
    void AddListener(IDScannerInfoHandler EventHandler);
    void RemoveListener(IDScannerInfoHandler EventHandler);
    ENUM_IDScannerStatus GetStatus();
    void Scan();
    bool InstallDriver();
    bool IsDriverInstalled();
    void ShowAdvancedScan();
  }
}