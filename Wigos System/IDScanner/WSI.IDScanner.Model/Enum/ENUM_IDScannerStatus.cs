using System;

namespace WSI.IDScanner.Model.Enums
{
  [Flags]
  public enum ENUM_IDScannerStatus
  {
    UNKNOWN = 0,
    DISCONNECTED = 1,
    CONNECTED = 2,
    IDLE = 4,
    INUSE = 8,
    ERROR = 16
  }
}