using System.Collections.Generic;

namespace WSI.IDScanner.Model
{
  public enum ENUM_IDScannerFixedField
  {
    NAME,
    NAME2,
    SURNAME,
    SURNAME2,
    ID_NUMBER,
    ID_TYPE,
    SEX,
    DATE_OF_BIRTH,
    PHOTO,
    BIRTHPLACE,
    NATIONALITY,
    ADDRESS,
    STATE,
    COUNTY,
    CITY,
    ZIP,
    DOCUMENT_EXPIRY,
    FRONT,
    IS_FRONT,
    COUNTRY
  }

  public enum ENUM_DetectableIDClass
  {
    UNKNOWN=0,
    OTHER=1,
    PASSPORT=2,
    ID_CARD=3,
    DRIVING_LICENCE=4,
    RESIDENCE_CARD=5,
    VISA=6,
    CREW_CARD=7,
    PERSONAL_CARD=8
  }


  public class IDScannerInfo
  {
    public Dictionary<string, object> Fields { get; set; }
    public Dictionary<ENUM_IDScannerFixedField, string> IDScannerFixedFieldMapping { get; set; }
  }
}