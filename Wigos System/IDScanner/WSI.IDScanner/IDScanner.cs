﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using WigosCommon;
using WSI.Common;
using WSI.IDScanner.Model;

namespace WSI.IDScanner
{
  public class IDScanner : IDisposable
  {
    private const string REGISTRY_KEY = "Software\\WigosCashier";
    private const string REGISTRY_VALUE = "ScannerEnabled";


	  public IDScanner()
	  {
		  try
		  {
			  Microsoft.Win32.RegistryKey _key;
			  _key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(REGISTRY_KEY) ??
							Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REGISTRY_KEY);
			  bool _enabled = false;
			  if (_key != null)
			  {
				  _enabled = Boolean.TryParse(_key.GetValue(REGISTRY_VALUE) as string, out _enabled);
				  _key.Close();
			  }
			  m_enabled = _enabled;
		  }
		  catch (Exception _ex)
		  {
			  Log.Exception(_ex);
		  }

      try
      {
        if (!Directory.Exists("DisabledDrivers"))
          Directory.CreateDirectory("DisabledDrivers");
        string _s = GeneralParam.GetString(String.Format("IDCardScanner.{0:00}", 01), "License", "");
        if (string.IsNullOrEmpty(_s))
        {

          if (File.Exists(@"IDScannerConnectorSnapShell.dll"))
          {
            if (File.Exists(@"DisabledDrivers\IDScannerConnectorSnapShell.dll"))
            {
              File.Delete(@"DisabledDrivers\IDScannerConnectorSnapShell.dll");
            }
            File.Move(@"IDScannerConnectorSnapShell.dll", @"DisabledDrivers\IDScannerConnectorSnapShell.dll");
          }
          if (File.Exists(@"DisabledDrivers\IDScannerConnectorICarIDBoxBasic.dll") && !File.Exists(@"IDScannerConnectorICarIDBoxBasic.dll"))
            File.Move(@"DisabledDrivers\IDScannerConnectorICarIDBoxBasic.dll", @"IDScannerConnectorICarIDBoxBasic.dll");
        }
        else
        {
          if (File.Exists(@"IDScannerConnectorICarIDBoxBasic.dll"))
          {

            if (File.Exists(@"DisabledDrivers\IDScannerConnectorICarIDBoxBasic.dll"))
            {
              File.Delete(@"DisabledDrivers\IDScannerConnectorICarIDBoxBasic.dll");
            }
            File.Move(@"IDScannerConnectorICarIDBoxBasic.dll", @"DisabledDrivers\IDScannerConnectorICarIDBoxBasic.dll");
          }

          if (File.Exists(@"DisabledDrivers\IDScannerConnectorSnapShell.dll") && !File.Exists(@"IDScannerConnectorSnapShell.dll"))
            File.Move(@"DisabledDrivers\IDScannerConnectorSnapShell.dll", @"IDScannerConnectorSnapShell.dll");
        }
      }
      catch
      {
        //swallow
      }


	  }

	  private static readonly Dictionary<string, IIDScannerDevice> m_scanner_devices_installed =
      new Dictionary<string, IIDScannerDevice>();

    private static readonly Dictionary<string, IIDScannerDevice> m_scanner_devices_no_driver =
      new Dictionary<string, IIDScannerDevice>();

    private static readonly object m_locker = new object();

    public int RefreshAllConnectors()
    {
      if (!m_enabled)
        return 0;
      lock (m_locker)
      {
        ICollection<IIDScannerDevice> _found_types;
        DynamicLibraryManager<IIDScannerDevice> _lib_manager;

        _lib_manager = new DynamicLibraryManager<IIDScannerDevice>();

        _found_types =
          _lib_manager.LoadAllCompatibleTypes(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            "IDScannerConnector").Values;
        if (_found_types.Count <= 0)
        {
          return m_scanner_devices_installed.Count;
        }
        foreach (var _found_type in _found_types)
        {
          if (m_scanner_devices_installed.ContainsKey(_found_type.Name) ||
              m_scanner_devices_no_driver.ContainsKey(_found_type.Name))
            continue;
          if (_found_type.IsDriverInstalled())
            m_scanner_devices_installed.Add(_found_type.Name, _found_type);
          else
            m_scanner_devices_no_driver.Add(_found_type.Name, _found_type);
        }

        return m_scanner_devices_installed.Count;
      }
    } //GetAllAttatchedDevices

    public int AttatchToAllAvailable(IDScannerInfoHandler EventHandler)
    {
      if (!m_enabled)
        return 0;
      int _connected_to=0;
      RefreshAllConnectors();
      lock (m_locker)
      {
        foreach (var _id_scanner_device in m_scanner_devices_installed)
        {
          if (_id_scanner_device.Value.IsAvailable())
          {
            _id_scanner_device.Value.AddListener(EventHandler);
            _connected_to++;
          }
        }
      }
      return _connected_to;
    }

    public void DetatchFromAllAvailable(IDScannerInfoHandler EventHandler)
    {
      lock (m_locker)
      {
        foreach (var _id_scanner_device in m_scanner_devices_installed)
        {
          if (_id_scanner_device.Value.IsAvailable())
          {
            _id_scanner_device.Value.RemoveListener(EventHandler);
          }
        }
      }
    }

    public void ForceScan()
    {
	    if (!m_enabled)
        return;
	    lock (m_locker)
	    {
		    foreach (var _id_scanner_device in m_scanner_devices_installed)
		    {
			    if(_id_scanner_device.Value.IsAvailable())
				    _id_scanner_device.Value.Scan();
		    }
	    }
    }

	  public void InitializeAll()
	  {
		  if (!m_enabled)
        return ;
		  lock (m_locker)
		  {
			  foreach (var _id_scanner_device in m_scanner_devices_installed)
			  {
				  _id_scanner_device.Value.Init();
			  }
		  }
	  }

	  public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this); 

    }

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing)
      {
        return;
      }
      lock (m_locker)
      {
        foreach (var _id_scanner_device in m_scanner_devices_installed)
        {
          _id_scanner_device.Value.Dispose();
        }
        m_scanner_devices_installed.Clear();
      }
    }

    public void InstallAllMissing()
    {
      if (!m_enabled)
        return;
      lock (m_locker)
      {
        foreach (string _connector_name in MissingDrivers())
        {
          InstallDriver(_connector_name);
        }
      }
    }

    private void InstallDriver(string ConnectorName)
    {
      if (!m_enabled)
        return;
      IIDScannerDevice _connector;
      if (m_scanner_devices_no_driver.TryGetValue(ConnectorName, out _connector))
      {
        InstallDriver(_connector);
      }
    }

    public void Install(string ConnectorName)
    {
      if (!m_enabled)
        return;
      lock (m_locker)
      {

        foreach (var _device in m_scanner_devices_no_driver)
        {
          if (_device.Key != ConnectorName)
          {
            continue;
          }
          InstallDriver(_device.Value);
          break;
        }
      }
    }

    public void AdvancedScan(string ConnectorName)
    {
      if (!m_enabled)
        return;

      lock (m_locker)
      {
        IIDScannerDevice _scanner_device = null;
        foreach (var _device in m_scanner_devices_installed)
        {
          if (_device.Key == ConnectorName)
            _scanner_device = _device.Value;

        }
        if (_scanner_device == null)
          foreach (var _device in m_scanner_devices_no_driver)
          {
            if (_device.Key == ConnectorName)
              _scanner_device = _device.Value;
          }
        if (_scanner_device != null)
          _scanner_device.ShowAdvancedScan();
      }
    }

    private void InstallDriver(IIDScannerDevice Conenctor)
    {
      if (!m_enabled)
        return;

      if (!Conenctor.InstallDriver())
      {
        return;
      }
      if (m_scanner_devices_no_driver.ContainsKey(Conenctor.Name))
        m_scanner_devices_no_driver.Remove(Conenctor.Name);
      if (!m_scanner_devices_installed.ContainsKey(Conenctor.Name))
      {
        m_scanner_devices_installed.Add(Conenctor.Name, Conenctor);
      }
    }


    public string[] MissingDrivers()
    {
      if (!m_enabled)
        return new string[]{};
      List<string> _retval = new List<string>();
      foreach (var _id_scanner_device in m_scanner_devices_no_driver)
      {
        _retval.Add(_id_scanner_device.Key);
      }
      return _retval.ToArray();
    }

    public string[] AllAvailable()
    {
      if (!m_enabled)
        return new string[] { };
      List<string> _retval = new List<string>();
      lock (m_locker)
      {
        foreach (var _id_scanner_device in m_scanner_devices_installed)
        {
          if (_id_scanner_device.Value.IsAvailable())
          {
            _retval.Add(_id_scanner_device.Key);
          }
        }
      }
      return _retval.ToArray();
    }

    public void Enable()
    {
      m_enabled = true;
      UpdateRegistryKey();
      RefreshAllConnectors();
      InstallAllMissing();
      InitializeAll();
      InitializeAll();
    }

    private bool m_enabled;

    public void Dissable()
    {
      DetatchFromAllAvailable(null);
      m_enabled = false;
      UpdateRegistryKey();
    }

    private void UpdateRegistryKey()
    {
      Microsoft.Win32.RegistryKey _key;
      _key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(REGISTRY_KEY,true) ??
            Microsoft.Win32.Registry.CurrentUser.CreateSubKey(REGISTRY_KEY);
	    if (_key != null)
	    {
		    _key.SetValue(REGISTRY_VALUE, m_enabled, Microsoft.Win32.RegistryValueKind.String);
		    _key.Close();
	    }
    }


    public bool Enabled()
    {
      return m_enabled;
    }
  }
}
