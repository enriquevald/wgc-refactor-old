//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_generate.cs
// 
//   DESCRIPTION : Generate excel ReportsTool
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 AMF    First release
// 16-MAY-2016 LTC    Product Backlog Item 12781:Puerto Rico: Stored retrieve meters collection to collection
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

namespace WSI.ReportsTool
{

  public partial class frm_generate : Form
  {
    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";
    protected String _file_path = "";

    //------------------------------------------------------------------------------
    // PURPOSE : Main
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public frm_generate()
    {
      frm_login _frm;

      _frm = new frm_login();
      _frm.ShowDialog();

      InitializeComponent();
      this.Text = "Exportación datos - Generador - @" + WGDB.DataSource.ToString();
      VisibleResult(false);
      this.dt_date.Value = WGDB.Now.AddMonths(-1);

      // LTC 16-MAY-2016
      DateTime _today;
      _today = Misc.TodayOpening();

      this.dt_date_ini.Value = _today;
      this.dt_date_fin.Value = _today.AddDays(1);

      this.dt_date_ini.MaxDate = _today; 

    } // frm_generate

    //------------------------------------------------------------------------------
    // PURPOSE : Reset labels result
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void VisibleResult(Boolean VisibleValue)
    {
      lbl_result.Visible = VisibleValue;
      lbl_file_name.Visible = VisibleValue;
      lnk_file_name.Visible = VisibleValue;
      lbl_folder_name.Visible = VisibleValue;
      lnk_folder_name.Visible = VisibleValue;
    } // ResetResult

    //------------------------------------------------------------------------------
    // PURPOSE : Create conecction
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateConnection()
    {
      StringBuilder _xml;

      _xml = new StringBuilder();
      _xml.AppendLine("<SiteConfig>");
      _xml.AppendLine("    <DBPrincipal>WIN-SERVER-4</DBPrincipal>");
      _xml.AppendLine("    <DBMirror>WIN-SERVER-4</DBMirror>");
      _xml.AppendLine("    <DBId>0</DBId>");
      _xml.AppendLine("</SiteConfig>");

      if (!ConfigurationFile.Init("WSI.ReportsTool.cfg", _xml.ToString()))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");

        Thread.Sleep(5000);
      }
    } // CreateConnection

    //------------------------------------------------------------------------------
    // PURPOSE : execute global procedure
    //
    //  PARAMS :
    //      - INPUT :
    //                - String Sql
    //                - DataSet Ds
    //
    //      - OUTPUT :
    //
    // RETURNS : True or False
    //
    //   NOTES :
    //
    private Boolean ExecuteProcedure(String Sql, out DataSet Ds)
    {
      Ds = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(Sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, Ds);

              return true;
            }
          }
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
        lbl_result.Text = "ERROR: Obteniendo datos";
      }

      return false;
    } // ExecuteProcedure

    //------------------------------------------------------------------------------
    // PURPOSE : Generate Excel with the data extract
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void GenerateExcel()
    {
      DataSet _ds;
      DataSet _ds_global;
      DataTable _dt;
      Dictionary<String, String> _params;
      String _sql_str;
      Int16 _i;
      DirectoryInfo _info;
      // LTC 12-MAY-2016
      String _date_ini = dt_date_ini.Value.Year.ToString("0000") + dt_date_ini.Value.Month.ToString("00") + dt_date_ini.Value.Day.ToString("00") + " " + dt_date_ini.Value.Hour.ToString("00") + ":" + dt_date_ini.Value.Minute.ToString("00");
      String _date_fin = dt_date_fin.Value.Year.ToString("0000") + dt_date_fin.Value.Month.ToString("00") + dt_date_fin.Value.Day.ToString("00") + " " + dt_date_fin.Value.Hour.ToString("00") + ":" + dt_date_fin.Value.Minute.ToString("00");

      try
      {
        CreateConnection();

        _sql_str = "EXECUTE SP_ReportsTool_Principal '" + dt_date_ini.Value.Year.ToString("0000") + dt_date_ini.Value.Month.ToString("00") + dt_date_ini.Value.Day.ToString("00") + "', '" + dt_date_fin.Value.Year.ToString("0000") + dt_date_fin.Value.Month.ToString("00") + dt_date_fin.Value.Day.ToString("00") + "'";

        if (!ExecuteProcedure(_sql_str, out _ds_global))
        {
          return;
        }

        _ds = new DataSet("NombreDataSet");
        _i = 1;

        foreach (DataRow _table_name in _ds_global.Tables[0].Rows)
        {
          _ds_global.Tables[_i].TableName = _table_name[0].ToString();
          _dt = new DataTable(_table_name[0].ToString());
          _ds.Tables.Add(_dt);
          _i++;
        }
        _ds_global.Tables.Remove(_ds_global.Tables[0].TableName);

        _params = new Dictionary<String, String>();

        if (ExcelConversion.DataSetToExcel(_ds_global, _file_path, _params, false))
        {
          VisibleResult(true);
          lbl_result.Text = String.Empty;
          _info = new DirectoryInfo(Path.GetDirectoryName(_file_path));
          lnk_folder_name.Text = _info.Name.ToString();
          lnk_folder_name.Links[0].LinkData = Path.GetDirectoryName(_file_path);
          lnk_file_name.Text = Path.GetFileName(_file_path);
          lnk_file_name.Links[0].LinkData = _file_path;
        }
        else
        {
          lbl_result.Text = "ERROR: Creando Excel";
          lbl_result.Visible = true;
        }
      }
      catch
      {
        lbl_result.Text = "ERROR: Obteniendo datos";
        lbl_result.Visible = true;
      }
    } // GenerateExcel

    //------------------------------------------------------------------------------
    // PURPOSE : Generate Click
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_generate_Click(object sender, EventArgs e)
    {
      Stream myStream;
      SaveFileDialog sfd_generate = new SaveFileDialog();

      try
      {

        if (dt_date_ini.Value > dt_date_fin.Value)
        {
          lbl_result.Text = "ERROR: La fecha inicial debe ser menor a la fecha final";
          lbl_result.Visible = true;
          return;
        }

        sfd_generate.Filter = "Excel Files (*.xlsx)|*.xlsx";
        sfd_generate.FilterIndex = 2;
        sfd_generate.RestoreDirectory = true;

        // LTC 12-MAY-2016
        sfd_generate.FileName = String.Format("Datos_{0}{1}.xlsx", dt_date_ini.Value.Year.ToString("0000") + dt_date_ini.Value.Month.ToString("00") + dt_date_ini.Value.Day.ToString("00") + "_", dt_date_fin.Value.Year.ToString("0000") + dt_date_fin.Value.Month.ToString("00") + dt_date_fin.Value.Day.ToString("00"));

        if (sfd_generate.ShowDialog() == DialogResult.OK)
        {
          if ((myStream = sfd_generate.OpenFile()) != null)
          {
            VisibleResult(false);
            _file_path = sfd_generate.FileName;
            myStream.Close();
            lbl_result.Text = "Generando...";
            lbl_result.Visible = true;
            this.Cursor = Cursors.WaitCursor;
            GenerateExcel();
            this.Cursor = Cursors.Arrow;
          }
        }
      }
      catch
      {
        lbl_result.Text = "ERROR: Archivo en uso";
        lbl_result.Visible = true;
      }
    } // btn_generate_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Exit
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_exit_Click(object sender, EventArgs e)
    {
      Environment.Exit(0);
    } // btn_exit_Click

    //------------------------------------------------------------------------------
    // PURPOSE : date value changed event
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dt_date_ValueChanged(object sender, EventArgs e)
    {
      VisibleResult(false);
    } // dt_date_ValueChanged

    //------------------------------------------------------------------------------
    // PURPOSE : On form closed
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - FormClosedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void frm_generate_FormClosed(object sender, FormClosedEventArgs e)
    {
      Environment.Exit(0);
    } // frm_generate_FormClosed

    //------------------------------------------------------------------------------
    // PURPOSE : File name link
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - LinkLabelLinkClickedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void lnk_file_name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      String _target;

      try
      {
        _target = e.Link.LinkData.ToString();

        System.Diagnostics.Process.Start(_target);
      }
      catch
      {
      }
    } // lnk_file_name_LinkClicked

    //------------------------------------------------------------------------------
    // PURPOSE : Folder name link
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - LinkLabelLinkClickedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void lnk_folder_name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      String _target;

      try
      {
        _target = e.Link.LinkData.ToString();

        System.Diagnostics.Process.Start(_target);
      }
      catch
      {
      }
    }

    // LTC 13-MAY-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Restore controls to initial state 
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - LinkLabelLinkClickedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_reset_Click(object sender, EventArgs e)
    {
      lnk_folder_name.Text = "";
      lnk_file_name.Text = "";
      VisibleResult(false);
      this.dt_date_ini.Value = Misc.TodayOpening();
      this.dt_date_fin.Value = Misc.TodayOpening().AddDays(1);
    }

  }
}