//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_login.cs
// 
//   DESCRIPTION : Login ReportsTool
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-MAR-2016 AMF    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.ReportsTool
{

  public partial class frm_login : Form
  {
    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";

    private SqlConnection sql_conn;
    private SqlTransaction sql_trx;
    private SqlCommand sql_command;
    private string sql_str;

    private Boolean _logged = false;

    //------------------------------------------------------------------------------
    // PURPOSE : Main
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public frm_login()
    {
      InitializeComponent();
      CreateConnection();
      this.Text = "Exportaci�n datos - Login de usuario - @" + WGDB.DataSource.ToString();

#if DEBUG
      String _aux_value1;
      String _aux_value2;

      if (Environment.GetEnvironmentVariable("LKS_VC_DEV") != null)
      {
        _aux_value1 = Environment.GetEnvironmentVariable("LKS_VC_CASHIER_USER_NAME");
        _aux_value2 = Environment.GetEnvironmentVariable("LKS_VC_CASHIER_USER_PASSWORD");

        if (_aux_value1 != null && _aux_value2 != null)
        {
          txt_username_name.Text = _aux_value1;
          txt_username_password.Text = _aux_value2;
        }
      }
#endif
    } // frm_login

    //------------------------------------------------------------------------------
    // PURPOSE : Ok Click
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_ok_Click(object sender, EventArgs e)
    {
      String _str_error;

      if (txt_username_name.Text == "")
      {
        return;
      }

      if (!LoginUser(out _str_error))
      {
        MessageBox.Show(_str_error);
      }
      else
      {
        _logged = true;

        Misc.WriteLog("[FORM CLOSE] wsi.reportstool\frm_login (ok)", Log.Type.Message);
        this.Close();
      }
    } // btn_ok_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Create connection
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateConnection()
    {
      StringBuilder _xml;

      _xml = new StringBuilder();
      _xml.AppendLine("<SiteConfig>");
      _xml.AppendLine("    <DBPrincipal>WIN-SERVER-4</DBPrincipal>");
      _xml.AppendLine("    <DBMirror>WIN-SERVER-4</DBMirror>");
      _xml.AppendLine("    <DBId>0</DBId>");
      _xml.AppendLine("</SiteConfig>");

      if (!ConfigurationFile.Init("WSI.ReportsTool.cfg", _xml.ToString()))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");

        Thread.Sleep(5000);
      }
    } // CreateConnection


    //------------------------------------------------------------------------------
    // PURPOSE : Login user
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //                - String ErrorStr
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean LoginUser(out String ErrorStr)
    {
      SqlDataReader _reader;
      Boolean _enabled;
      Object _obj;
      Object _obj_user;
      PasswordPolicy _pwd_policy;
      String AuditStr;

      sql_conn = null;
      sql_trx = null;

      _obj = null;
      _obj_user = null;
      _enabled = false;

      ErrorStr = "Usuario y/o contrase�a incorrectos.";
      AuditStr = "Contrase�a incorrecta.";

      try
      {
        // Checking that the username is not empty
        if (txt_username_name.Text == "")
        {

          return false;
        }

        // Checking that the password is not empty
        if (txt_username_password.Text == "")
        {
          //empty field not allowed
          WSI.Common.Auditor.Audit(ENUM_GUI.REPORTS_TOOL, // GuiId
                          0,                              // UserId
                          txt_username_name.Text,         // UserName
                          Environment.MachineName,        // ComputerName
                          4,                              // AuditCode
                          5000 + 107,                     // NlsId (GUI)
                          AuditStr, "", "", "", "");

          return false;
        }

        //CreateConnection();

        // Get Sql Connection
        sql_conn = WSI.Common.WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();

        _pwd_policy = new PasswordPolicy();

        // XCD 21-AUG-2012 Change GU_ENABLED to GU_BLOCK_REASON
        sql_str = "  SELECT   GU_BLOCK_REASON " +
                  "    FROM   GUI_USERS       " +
                  "   WHERE   GU_USERNAME = @p1 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;

        _obj = sql_command.ExecuteScalar();

        if (_obj == null)
        {
          //user not exist

          WSI.Common.Auditor.Audit(ENUM_GUI.REPORTS_TOOL, // GuiId
                         0,                               // UserId
                         "SU",                            // UserName
                         Environment.MachineName,         // ComputerName
                         4,                               // AuditCode
                         5000 + 457,                      // NlsId (GUI)
                         txt_username_name.Text, "", "", "", "");
          return false;
        }

        //check if password is correct and enabled state
        _enabled = ((Int32)_obj).Equals((Int32)WSI.Common.GUI_USER_BLOCK_REASON.NONE) ? true : false;

        sql_str = "";
        sql_str += "SELECT   GU_USER_ID    ";
        sql_str += "  FROM   GUI_USERS     ";
        sql_str += " WHERE   GU_USERNAME = @p1 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;
        _obj_user = sql_command.ExecuteScalar();
        if (_obj_user != null)
        {
          if (!_pwd_policy.VerifyCurrentPassword((int)_obj_user, txt_username_password.Text, sql_trx))
          {
            _obj_user = null;
          }
        }

        if (!_enabled || _obj_user == null)
        {//user locked or wrong password
          Int32 login_failures;
          Int32 _user_id;
          Boolean _is_corporate_user;

          login_failures = 0;
          _user_id = 0;
          _is_corporate_user = false;

          sql_str = " SELECT   GU_USER_ID " +
               "   FROM   GUI_USERS     " +
               "  WHERE   GU_USERNAME = @p1 ";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = sql_trx.Connection;
          sql_command.Transaction = sql_trx;

          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;

          _obj = sql_command.ExecuteScalar();

          if (_obj != null)
          {
            _user_id = (Int32)_obj;
          }

          if (_enabled)
          {
            // Increase Password failures
            sql_str = "UPDATE   GUI_USERS " +
                  " SET   GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1 " +
                " WHERE   GU_USERNAME = @p1 " +
                "   AND   GU_USER_ID <> 0 "; // only disable when is not a superuser

            sql_command = new SqlCommand(sql_str);
            sql_command.Connection = sql_trx.Connection;
            sql_command.Transaction = sql_trx;

            sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;
            sql_command.ExecuteNonQuery();
            sql_trx.Commit();
            sql_trx = sql_conn.BeginTransaction();

            sql_str = "  SELECT   ISNULL(GU_LOGIN_FAILURES, 0)    " +
                      "         , CAST (CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS BIT) AS GU_MASTER " +
                      "    FROM   GUI_USERS     " +
                      "   WHERE   GU_USERNAME = @p1 ";

            sql_command = new SqlCommand(sql_str);
            sql_command.Connection = sql_trx.Connection;
            sql_command.Transaction = sql_trx;

            sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;

            _reader = sql_command.ExecuteReader();
            if (_reader.Read())
            {
              login_failures = (Int32)_reader[0];
              _is_corporate_user = (Boolean)_reader[1];
            }
            _reader.Close();
            // Check MaxLoginAttempts parameter
            if (login_failures == _pwd_policy.MaxLoginAttempts)
            {
              AuditStr = "El usuario ha sido bloqueado.";

              sql_str = "UPDATE   GUI_USERS " +
                    "   SET   GU_BLOCK_REASON = GU_BLOCK_REASON | " + (Int32)GUI_USER_BLOCK_REASON.WRONG_PASSWORD +
                    "     ,   GU_LOGIN_FAILURES = 0 " +
                    " WHERE   GU_USERNAME = @p1 " +
                    "   AND   GU_USER_ID <> 0 "; // only block user when is not a superuser

              if (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
              {
                // Not block if is a corporate user!!
                sql_str += " AND GU_MASTER_ID IS NULL";
              }

              sql_command = new SqlCommand(sql_str);
              sql_command.Connection = sql_trx.Connection;
              sql_command.Transaction = sql_trx;

              sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GU_USERNAME").Value = txt_username_name.Text;
              sql_command.ExecuteNonQuery();
              sql_trx.Commit();
              sql_trx = sql_conn.BeginTransaction();

              if (!_is_corporate_user)
              {
                WSI.Common.Auditor.Audit(ENUM_GUI.REPORTS_TOOL, // GuiId
                               _user_id,                        // UserId
                               txt_username_name.Text,          // UserName
                               Environment.MachineName,         // ComputerName
                               4,                               // AuditCode        
                               5000 + 107,                      // NlsId (GUI)
                               AuditStr, "", "", "", "");
              }
              // Insert alarm MAX failed attemps of login
              WSI.Common.Alarm.Register(WSI.Common.AlarmSourceCode.User,
                            0, //Cashier.TerminalId, TODO AMF
                            txt_username_name.Text + "@" + Environment.MachineName,
                            WSI.Common.AlarmCode.User_MaxLoginAttemps);

              return false;
            }
          }
          else
          {
            //user locked
            if (_obj_user != null)
            {
              //password is correct
              AuditStr = "El usuario est� bloqueado.";
              ErrorStr = "El usuario est� bloqueado.";
            }
          }

          WSI.Common.Auditor.Audit(ENUM_GUI.REPORTS_TOOL, // GuiId
                         _user_id,                        // UserId
                         txt_username_name.Text,          // UserName
                         Environment.MachineName,         // ComputerName
                         4,                               // AuditCode
                         5000 + 107,                      // NlsId (GUI)
                         AuditStr, "", "", "", "");

          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        // throw new Exception(ex.Message);
        Log.Exception(ex);
        return false;
      }
    } // LoginUser

    //------------------------------------------------------------------------------
    // PURPOSE : On form closed
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - FormClosedEventArgs e
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void frm_login_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (!_logged)
      {
        Environment.Exit(0);
      }
    } // frm_login_FormClosed
  }
}