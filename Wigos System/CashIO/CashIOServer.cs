﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CashIOServer.cs
// 
//   DESCRIPTION : CashIOServer for CountR
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-MAY-2016 AMF    First release
// 23-AUG-2017 DPC    WIGOS-4620: WRKP - Service implementation - Create project self host
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.CountR;

namespace CashIO
{
  public class CashIOServer
  {
    #region " Members "

    static Thread m_thread;
    static Thread m_thread_offline;
    static Thread m_thread_timeout;

    List<CashIOClient> m_clients = new List<CashIOClient>();
    TcpListener m_listener;

    internal Int32 Version = 1;
    internal Int32 ProtocolVersion = 1;
    internal Int32 CasinoID = 0;
    internal String EMPTY_VALIDATION_NUMBER = String.Empty.PadLeft(18, '0');

    #endregion " Members "

    #region " Properties "

    public Int32 CountRCode { get; set; }
    private Boolean DoCommit { get; set; }
    private String RemoteIP { get; set; }

    #endregion " Properties "

    #region " Enums "

    public enum MessageType
    {
      Startup = 0x31,
      CheckAccount = 0x32,
      StartPayment = 0x33,
      EndPayment = 0x34,
      CancelTransaction = 0x35,
      DeleteMedia = 0x37,
      ChargeAccount = 0x38,
      LockAccount = 0x39,
      CreditsCleared = 0x41,
      PaymentReportMessage = 0x42,
      GetValidAccountNumber = 0x45,
      SetPINCodeForAccountNumber = 0x46,
      SendEvent = 0x47,
      SetUsedMediaType = 0x48,
      SendNumericEvent = 0x49,
      SendTextEvent = 0x4D
    }

    public enum Media
    {
      NoMedia = 0,
      MagneticCards = 1,
      Ticket = 2,
      SmartCards = 3,
      RFIDCards = 4
    }

    public enum MediaBit
    {
      NoMedia = 0,
      MagneticCards = 1,
      Ticket = 2,
      SmartCards = 4,
      RFIDCards = 8
    }

    private enum LOG_MODE
    {
      NONE = 0,
      WCP = 1,
      BD = 2,
      ALL = 3
    }

    #endregion " Enums "

    #region " Public Methods "

    /// <summary>
    ///  Init thread
    /// </summary>
    /// <param name="Port"></param>
    /// <returns></returns>
    public Boolean Init(Int32 Port)
    {

      // Main Thread
      m_thread = new Thread(ListenerThread);
      m_thread.Start(Port);

      // Offline Thread
      m_thread_offline = new Thread(ThreadOffline);
      m_thread_offline.Start();

      // Payment TimeOut Thread
      m_thread_timeout = new Thread(ThreadTimeOut);
      m_thread_timeout.Start();

      return true;
    } // Init

    /// <summary>
    /// Process request
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    public Boolean ProcessRequest(CashIOClient Client, String Request, out String Response)
    {
      String[] _field;
      Int32 _tick;
      Byte[] _message_bytes;
      MessageType _message;
      Int32 _countr;
      Boolean _continue;

      Response = null;
      _tick = Environment.TickCount;
      this.DoCommit = false;
      _continue = false;

      try
      {
        // Check Request
        _field = Request.Split(new String[1] { "," }, StringSplitOptions.None);
        if (_field.Length < 3)
        {
          return false;
        }
        if (_field[0].Length != 1)
        {
          return false;
        }

        // Check message type
        _message_bytes = Encoding.ASCII.GetBytes(_field[0]);
        if (_message_bytes.Length < 1)
        {
          return false;
        }
        _message = (MessageType)_message_bytes[0];

        // Open SQL Transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Check CashIO
          if (!Int32.TryParse(_field[1], out _countr))
          {
            Response = ResponseZ(_field[1], _field[2], CountRBusinessLogic.Ack.UnknownCashIO);

            return true;
          }
          // Startup/ CheckAccount/ GetValidAccountNumber, dont check if exists. Have specific ResponseZ
          if (_message != MessageType.Startup && _message != MessageType.CheckAccount && _message != MessageType.GetValidAccountNumber)
          {
            if (!CountR.DB_CheckCountR(_countr, Client.RemoteIP, _db_trx.SqlTransaction))
            {
              Response = ResponseZ(_field[1], _field[2], CountRBusinessLogic.Ack.UnknownCashIO);

              return true;
            }
          }
          CountRCode = _countr;

          // Process message
          switch (_message)
          {
            case MessageType.Startup:
              _continue = Message0x31_Startup(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.CheckAccount:
              _continue = Message0x32_CheckAccount(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.StartPayment:
              _continue = Message0x33_StartPayment(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.EndPayment:
              _continue = Message0x34_EndPayment(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.CancelTransaction:
              _continue = Message0x35_CancelTransaction(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.ChargeAccount:
              _continue = Message0x38_ChargeAccount(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.GetValidAccountNumber:
              _continue = Message0x45_GetValidAccountNumber(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.SendEvent:
              _continue = Message0x47_SendEvent(Client, _field, out Response, _db_trx.SqlTransaction);
              break;

            case MessageType.SetUsedMediaType:
              _continue = Message0x48_UsedMediaType(Client, _field, out Response);
              break;

            default:
              Response = ResponseZ(_field[1], _field[2], CountRBusinessLogic.Ack.ACK);
              _continue = true;
              break;
          }

          if (this.DoCommit)
          {
            _db_trx.Commit();
          }

          if (!_continue)
          {
            Response = ResponseZDefaultError(_field[1], _field[2], _message);
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        ManageLog(Request, Response, Environment.TickCount - _tick);
      }
    } // ProcessRequest

    /// <summary>
    /// Manage CashIO Log
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="Tick"></param>
    public void ManageLog(String Request, String Response, Int32 Tick)
    {
      try
      {
        if (CountR.CheckCountRShowLog(this.CountRCode))
        {
          switch ((LOG_MODE)GeneralParam.GetInt32("CountR", "Log.Mode", 0))
          {
            case LOG_MODE.WCP:
              PrintLogWCP(Request, Response, Tick);
              break;

            case LOG_MODE.BD:
              CountR.InsertCountRLog(this.CountRCode, Request, Response, Tick);
              break;

            case LOG_MODE.ALL:
              PrintLogWCP(Request, Response, Tick);
              CountR.InsertCountRLog(this.CountRCode, Request, Response, Tick);
              break;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ManageLog

    #endregion " Public Methods "

    #region " Private Methods "

    /// <summary>
    /// TCP listener service
    /// </summary>
    /// <param name="Port"></param>
    private void ListenerThread(Object Port)
    {
      List<CashIOClient> _to_delete;
      Int32 _port = (Int32)Port;

      m_listener = new TcpListener(IPAddress.Any, _port);
      m_listener.Start();
      m_listener.BeginAcceptTcpClient(AcceptClient, this);

      Log.Message("TcpListener CashIO Start. Port: " + _port.ToString());

      GetProtocolInfo();

      _to_delete = new List<CashIOClient>();

      while (true)
      {
        System.Threading.Thread.Sleep(10000);

        lock (m_clients)
        {
          _to_delete.AddRange(m_clients.FindAll(_cioc => _cioc.HasErrors || _cioc.HasTimeout || !_cioc.Connected));
        }

        if (_to_delete.Count > 0)
        {
          foreach (CashIOClient _cioc in _to_delete)
          {
           _cioc.Close();
          }

          lock (m_clients)
          {
            foreach (CashIOClient _cioc in _to_delete)
            {
              m_clients.Remove(_cioc);
            }
          }

          _to_delete.Clear();
        }
      }
    } // ListenerThread

    /// <summary>
    /// Thread to update Offline CountR
    /// </summary>
    private void ThreadOffline()
    {
      while (true)
      {
        try
        {
          Thread.Sleep(60 * 1000);

          if (Services.IsPrincipal("WCP"))
          {
            if (!CountR.DB_SetStatusOffLine())
            {
              Log.Warning("CashIOServer. Exception in function CountR.DB_SetStatusOffLine");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // ThreadOffline

    /// <summary>
    /// Thread to update payment TimeOut
    /// </summary>
    private void ThreadTimeOut()
    {
      while (true)
      {
        try
        {
          Thread.Sleep(60 * 1000);

          if (Services.IsPrincipal("WCP"))
          {
            if (!CountRTransaction.PaymentTimeOut())
            {
              Log.Warning("CashIOServer. Exception in function CountRTransaction.PaymentTimeOut");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // ThreadTimeOut

    /// <summary>
    /// Accept client
    /// </summary>
    /// <param name="AR"></param>
    private void AcceptClient(IAsyncResult AR)
    {
      TcpClient _tcp_client;
      CashIOClient _cioc;

      try
      {
        _tcp_client = this.m_listener.EndAcceptTcpClient(AR);

        if (TcpClientAccepted(_tcp_client))
        {
          _cioc = new CashIOClient(_tcp_client);
          lock (m_clients)
          {
            m_clients.Add(_cioc);
            _cioc.m_server = this;
          }
          _cioc.StartReading();
        }
        else
        {
          try
          {
            _tcp_client.Client.Close();
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          try
          {
            _tcp_client.Close();
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          try
          {
            _tcp_client = null;
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
      }
      catch (Exception _ex)
      {
        // Error accepting new client
        Log.Exception(_ex);
      }
      finally
      {
        try
        {
          this.m_listener.BeginAcceptTcpClient(AcceptClient, this);
        }
        catch (Exception _ex)
        {
          // Error preparing to accept new clients ...
          Log.Exception(_ex);
        }
      }
    } // AcceptClient

    /// <summary>
    /// TCO Client Accepted
    /// </summary>
    /// <param name="TcpClient"></param>
    /// <returns></returns>
    private Boolean TcpClientAccepted(TcpClient TcpClient)
    {
      String _remote_ip;

      try
      {
        // Get Remote IP
        _remote_ip = TcpClient.Client.RemoteEndPoint.ToString();
        _remote_ip = _remote_ip.Substring(0, _remote_ip.IndexOf(":"));

        // Disconnect previously connected clients ...
        lock (m_clients)
        {
          foreach (CashIOClient _client in m_clients)
          {
            if (_client.RemoteIP == _remote_ip)
            {
              // Disconnect previously connected clients ...
              _client.Close();
            }
          }
        }

        // Set RemoteIP
        this.RemoteIP = _remote_ip;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // TcpClientAccepted

    /// <summary>
    /// Get protocol info
    /// </summary>
    private void GetProtocolInfo()
    {
      this.Version = GeneralParam.GetInt32("CountR", "Server.Version", 1);
      this.ProtocolVersion = GeneralParam.GetInt32("CountR", "Server.ProtocolVersion", 1);
      this.CasinoID = GeneralParam.GetInt32("Site", "Identifier", 0);
    } // GetProtocolInfo

    /// <summary>
    /// Print WCP trace Log
    /// </summary>
    /// <param name="TraceIn"></param>
    /// <param name="TraceOut"></param>
    /// <param name="Tick"></param>
    private void PrintLogWCP(String TraceIn, String TraceOut, Int32 Tick)
    {
      Log.Message(String.Format("CountR {0} Trace Received: {1}", this.CountRCode, TraceIn));
      Log.Message(String.Format("CountR {0} Trace Transmit: {1}. Elapsed time: {2} ms", this.CountRCode, TraceOut, Tick));
    } // PrintLogWCP

    #region " Messages "

    /// <summary>
    /// Message 0x31 StartUp
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x31_Startup(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      DateTime _now;

      Response = String.Empty;

      try
      {
        // Set activity - Dont control if exists
        CountR.DB_CheckCountRAndSetActivity(CountRCode, this.RemoteIP, SqlTrx);

        _now = WGDB.Now;
        Response = String.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                                 "Z",
                                 Field[1],
                                 Field[2],
                                 _now.ToString("dd.MM.yyyy"),
                                 _now.ToString("HH:mm:ss"),
                                 this.Version,         // OS Version
                                 this.ProtocolVersion, // OS Protocol Version
                                 this.CasinoID);       // Casino ID

        this.DoCommit = true;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x31_Startup

    /// <summary>
    /// Message 0x32 CheckAccount
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x32_CheckAccount(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Media _media;
      ACKRedeemKiosk _redeem_kiosk;

      Response = String.Empty;

      try
      {
        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[5], out _media))
        {
          _redeem_kiosk = new ACKRedeemKiosk();
          _redeem_kiosk.Ack = CountRBusinessLogic.Ack.NAK;
          _redeem_kiosk.AckExtended = CountRBusinessLogic.AckExtended.UnableToValidate;
        }
        else if (_media != Media.Ticket)
        {
          _redeem_kiosk = new ACKRedeemKiosk();
          _redeem_kiosk.Ack = CountRBusinessLogic.Ack.NAK;
          _redeem_kiosk.AckExtended = CountRBusinessLogic.AckExtended.UnableToValidate;
        }
        else
        {
          // Check Account/Ticket
          CountRBusinessLogic.CheckAccount(this.CountRCode, Field[3], out _redeem_kiosk, SqlTrx);
        }

        Response = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                                 "Z",                                         // Message Type
                                 Field[1],                                    // CashIO
                                 Field[2],                                    // Packet
                                 (Int32)_redeem_kiosk.Ack,                    // Ack
                                 Field[3],                                    // Ticket ID
                                 _redeem_kiosk.AmountCents,                   // Value 1
                                 0,                                           // Deposit
                                 (Int32)_redeem_kiosk.AckExtended,            // Valid
                                 (Int32)_redeem_kiosk.PurseType,              // PurseType1
                                 0,                                           // Value 2
                                 (Int32)CountRBusinessLogic.PurseType.Unused, // PurseType2
                                 0,                                           // Value 3
                                 (Int32)CountRBusinessLogic.PurseType.Unused, // PurseType3
                                 0);                                          // Ratio

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x32_CheckAccount

    /// <summary>
    /// Message 0x33 StartPayment
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x33_StartPayment(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Int64 _amount;
      Media _media;
      //CountRBusinessLogic.PurseType _purse_type;
      ACKRedeemKiosk _redeem_kiosk;

      Response = String.Empty;

      try
      {
        // Get amount
        if (!Int64.TryParse(Field[4], out _amount))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[6], out _media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (_media != Media.Ticket)
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        //// Purse Type:  Commented because its optional and never send on tickets messages
        //if (!EnumEx.TryConvert<CountRBusinessLogic.PurseType>(Field[7], out _purse_type))
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}
        //if (_purse_type != CountRBusinessLogic.PurseType.Player)
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}

        // Start Payment transaction
        if (CountRBusinessLogic.StartPayment(this.CountRCode, Field[3], _amount, out _redeem_kiosk, SqlTrx))
        {
          this.DoCommit = true;
        }

        Response = ResponseZ(Field[1], Field[2], _redeem_kiosk.Ack);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x33_StartPayment

    /// <summary>
    /// Message 0x034 EndPayment
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x34_EndPayment(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Int64 _amount;
      Media _media;
      //CountRBusinessLogic.PurseType _purse_type;
      ACKRedeemKiosk _redeem_kiosk;

      Response = String.Empty;

      try
      {
        // Get amount
        if (!Int64.TryParse(Field[4], out _amount))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[6], out _media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (_media != Media.Ticket)
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        //// Purse Type Commented because its optional and never send on tickets messages
        //if (!EnumEx.TryConvert<CountRBusinessLogic.PurseType>(Field[7], out _purse_type))
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}
        //if (_purse_type != CountRBusinessLogic.PurseType.Player)
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}

        // End Payment transaction
        if (CountRBusinessLogic.EndPayment(this.CountRCode, Field[3], _amount, out _redeem_kiosk, SqlTrx))
        {
          this.DoCommit = true;
        }

        Response = ResponseZ(Field[1], Field[2], _redeem_kiosk.Ack);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x34_EndPayment

    /// <summary>
    /// Message 0x035 CancelTransaction
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x35_CancelTransaction(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Media _media;
      //CountRBusinessLogic.PurseType _purse_type;

      Response = String.Empty;

      try
      {
        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[4], out _media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (_media != Media.Ticket)
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        //// Purse Type Commented because its optional and never send on tickets messages
        //if (!EnumEx.TryConvert<CountRBusinessLogic.PurseType>(Field[5], out _purse_type))
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}
        //if (_purse_type != CountRBusinessLogic.PurseType.Player)
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}

        Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.ACK);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x35_CancelTransaction

    /// <summary>
    /// Message 0x038 ChargeAccount
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x38_ChargeAccount(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Int64 _amount;
      Int64 _account;
      Media _media;
      //CountRBusinessLogic.PurseType _purse_type;
      ACKRedeemKiosk _redeem_kiosk;

      Response = String.Empty;

      try
      {
        // Check account
        if (String.IsNullOrEmpty(Field[3]) || (Int64.TryParse(Field[3], out _account) && _account == 0))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Get amount
        if (!Int64.TryParse(Field[4], out _amount))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[6], out _media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (_media != Media.Ticket)
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        //// Purse Type Commented because its optional and never send on tickets messages
        //if (!EnumEx.TryConvert<CountRBusinessLogic.PurseType>(Field[8], out _purse_type))
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}
        //if (_purse_type != CountRBusinessLogic.PurseType.Player)
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}

        if (CountRBusinessLogic.ChargeAccount(this.CountRCode, Field[3], _amount, TITO_TICKET_TYPE.CASHABLE, 0, out _redeem_kiosk, SqlTrx))
        {
          this.DoCommit = true;
        }

        Response = ResponseZ(Field[1], Field[2], _redeem_kiosk.Ack);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x38_ChargeAccount

    /// <summary>
    /// Message 0x045 GetValidAccountNumber
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x45_GetValidAccountNumber(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      Media _media;
      //CountRBusinessLogic.PurseType _purse_type;
      ACKRedeemKiosk _redeem_kiosk;
      DateTime _now;
      Int64 _sequence_id;

      Response = String.Empty;

      try
      {
        // Check Media
        if (!EnumEx.TryConvert<Media>(Field[3], out _media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (_media != Media.Ticket)
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        //// Purse Type Commented because its optional and never send on tickets messages
        //if (!EnumEx.TryConvert<CountRBusinessLogic.PurseType>(Field[4], out _purse_type))
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}
        //if (_purse_type != CountRBusinessLogic.PurseType.Player)
        //{
        //  Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

        //  return true;
        //}

        if (CountRBusinessLogic.GetValidAccountNumber(this.CountRCode, out _redeem_kiosk, out _sequence_id, SqlTrx))
        {
          this.DoCommit = true;
        }

        _now = WGDB.Now;
        Response = String.Format("{0},{1},{2},{3},{4},{5},{6}",
                         "Z",                                                     // Message Type
                         Field[1],                                                // CashIO
                         Field[2],                                                // Packet
                         (Int32)_redeem_kiosk.Ack,                                // Ack
                         _redeem_kiosk.ValidationNumberSeq,                       // Ticket ID 
                         _now.ToString("dd.MM.yyyy"), // Date
                         _now.ToString("HH:mm:ss"));  // Time

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x45_GetValidAccountNumber

    /// <summary>
    /// Message 0x045 SendEvent
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean Message0x47_SendEvent(CashIOClient Client, String[] Field, out String Response, SqlTransaction SqlTrx)
    {
      ACKRedeemKiosk _redeem_kiosk;
      CountRBusinessLogic.Event _event;
      CountRBusinessLogic.SubEvent _sub_event;
      CountRBusinessLogic.Mode _mode;
      CountR _countr;

      Response = String.Empty;

      try
      {
        // Check Event
        if (!EnumEx.TryConvert<CountRBusinessLogic.Event>(Field[3], out _event))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Sub Event
        if (!EnumEx.TryConvert<CountRBusinessLogic.SubEvent>(Field[4], out _sub_event))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Mode
        if (!EnumEx.TryConvert<CountRBusinessLogic.Mode>(Field[5], out _mode))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Get CountR
        if (!CountR.DB_GetCountRByCode(this.CountRCode, out _countr, SqlTrx))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Send Event
        if (CountRBusinessLogic.SendEvent(this.CountRCode, _countr.Name, _event, _sub_event, _mode, out _redeem_kiosk, SqlTrx))
        {
          this.DoCommit = true;
        }

        Response = ResponseZ(Field[1], Field[2], _redeem_kiosk.Ack);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x47_SendEvent

    /// <summary>
    /// Message 0x48 UsedMediaType
    /// </summary>
    /// <param name="Client"></param>
    /// <param name="Field"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    private Boolean Message0x48_UsedMediaType(CashIOClient Client, String[] Field, out String Response)
    {
      Int32 _player_media_int;
      MediaBit _player_media;
      MediaBit _bonus_media;
      CountRBusinessLogic.Ack _ack;

      Response = String.Empty;

      try
      {
        _ack = CountRBusinessLogic.Ack.ACK;

        // Check Player Media
        if (!Int32.TryParse(Field[3], out _player_media_int))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }
        if (!EnumEx.TryConvert<MediaBit>(Field[3], out _player_media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check Bonus Media
        if (!EnumEx.TryConvert<MediaBit>(Field[4], out _bonus_media))
        {
          Response = ResponseZ(Field[1], Field[2], CountRBusinessLogic.Ack.NAK);

          return true;
        }

        // Check if only have player  media the bit 2 and bonus media no bit
        if (_player_media != MediaBit.Ticket || _bonus_media != MediaBit.NoMedia)
        {
          Log.Warning(String.Format("CountR - {0}: Connecting with incorrect player media and bonus media.", this.RemoteIP));
        }

        // Check Bit 2 = Ticket
        if ((_player_media_int & 0x02) != 0x02) _ack = CountRBusinessLogic.Ack.NAK;

        Response = ResponseZ(Field[1], Field[2], _ack);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Message0x48_UsedMediaType


    /// <summary>
    /// Response Z
    /// </summary>
    /// <param name="CashIO"></param>
    /// <param name="Packet"></param>
    /// <param name="Answer"></param>
    /// <returns></returns>
    private String ResponseZ(String CashIO, String Packet, CountRBusinessLogic.Ack Answer)
    {
      String _response;

      _response = String.Format("{0},{1},{2},{3}",
                                "Z",            // Message Type
                                CashIO,         // CashIO
                                Packet,         // Packet
                                (Int32)Answer); // Ack

      return _response;
    } // ResponseZ

    /// <summary>
    /// Response Z when any error
    /// </summary>
    /// <param name="CashIO"></param>
    /// <param name="Packet"></param>
    /// <param name="Type"></param>
    /// <returns></returns>
    private String ResponseZDefaultError(String CashIO, String Packet, MessageType Type)
    {
      String _response;

      switch (Type)
      {
        case MessageType.Startup:
          _response = String.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                                    "Z",
                                    CashIO,
                                    Packet,
                                    WGDB.Now.ToString("dd.MM.yyyy"),
                                    WGDB.Now.ToString("HH:mm:ss"),
                                    this.Version,         // OS Version
                                    this.ProtocolVersion, // OS Protocol Version
                                    this.CasinoID);       // Casino ID
          break;

        case MessageType.CheckAccount:
          _response = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                                    "Z",                                                     // Message Type
                                    CashIO,                                                  // CashIO
                                    Packet,                                                  // Packet
                                    (Int32)CountRBusinessLogic.Ack.NAK,                      // Ack
                                    EMPTY_VALIDATION_NUMBER,                                 // Ticket ID
                                    0,                                                       // Value 1
                                    0,                                                       // Deposit
                                    (Int32)CountRBusinessLogic.AckExtended.UnableToValidate, // Valid
                                    (Int32)CountRBusinessLogic.PurseType.Unused,             // PurseType1
                                    0,                                                       // Value 2
                                    (Int32)CountRBusinessLogic.PurseType.Unused,             // PurseType2
                                    0,                                                       // Value 3
                                    (Int32)CountRBusinessLogic.PurseType.Unused,             // PurseType3
                                    0);                                                      // Ratio
          break;

        case MessageType.GetValidAccountNumber:
          _response = String.Format("{0},{1},{2},{3},{4},{5},{6}",
                                    "Z",                                // Message Type
                                    CashIO,                             // CashIO
                                    Packet,                             // Packet
                                    (Int32)CountRBusinessLogic.Ack.NAK, // Ack
                                    EMPTY_VALIDATION_NUMBER,            // Ticket ID 
                                    WGDB.Now.ToString("dd.MM.yyyy"),    // Date
                                    WGDB.Now.ToString("HH:mm:ss"));     // Time
          break;

        default:
          _response = String.Format("{0},{1},{2},{3}",
                                    "Z",                                 // Message Type
                                    CashIO,                              // CashIO
                                    Packet,                              // Packet
                                    (Int32)CountRBusinessLogic.Ack.NAK); // Ack
          break;
      }

      return _response;
    } // ResponseZDefaultError

    #endregion " Messages "

    #endregion " Private Methods "
  }

  public static class EnumEx
  {
    /// <summary>
    /// Try Convert Enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Value"></param>
    /// <param name="Result"></param>
    /// <returns></returns>
    public static Boolean TryConvert<T>(String Value, out T Result)
    {
      Int32 _check;
      Boolean _success;

      Result = default(T);

      if (!Int32.TryParse(Value, out _check))
      {
        return false;
      }

      _success = Enum.IsDefined(typeof(T), _check);

      if (_success)
      {
        Result = (T)Enum.ToObject(typeof(T), _check);
      }

      return _success;
    } // TryConvert
  }
}
