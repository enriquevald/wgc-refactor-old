﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CashIOClient.cs
// 
//   DESCRIPTION : CashIOClient for CountR
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-MAY-2016 AMF    First release
// 12-JUL-2018 JGC    Bug 33596:[WIGOS-12521] : WSI.Protocols - log message when CountR kiosks connected - EXCEPTION: Object reference not set t an instance of an object
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Data.SqlClient;
using WSI.Common;

namespace CashIO
{
  public class CashIOClient
  {
    #region " Members "

    static Int64 m_sequence = 0;

    Int64 m_unique_id;
    String m_remote_ip;
    TcpClient m_tcp_client;
    NetworkStream m_net_stream;

    internal CashIOServer m_server;

    Int32 m_connected_tick;
    Int32 m_i_msg_tick;
    Int32 m_o_msg_tick;

    // Buffer
    Byte[] m_i_buffer;
    Int32 m_offset;

    // Error
    Boolean m_error;

    #endregion " Members "

    #region " Properties "

    public CashIOServer Server
    {
      get
      {
        return m_server;
      }
    }

    public String RemoteIP
    {
      get
      {
        return m_remote_ip;
      }
    }

    public Boolean HasErrors
    {
      get
      {
        return m_error;
      }
    }

    public Boolean HasTimeout
    {
      get
      {
        return ((Environment.TickCount - m_i_msg_tick) >= 1 * 60 * 1000);
      }
    }

    public Boolean Connected
    {
      get
      {
        try
        {
          return m_tcp_client.Connected;
        }
        catch { ;}

        return false;
      }
    }

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Contructor
    /// </summary>
    /// <param name="Client"></param>
    public CashIOClient(TcpClient Client)
    {
      try
      {
        m_unique_id = System.Threading.Interlocked.Increment(ref m_sequence);
        m_tcp_client = Client;
        m_net_stream = Client.GetStream();
        m_connected_tick = Environment.TickCount;
        m_i_msg_tick = m_connected_tick;
        m_o_msg_tick = m_connected_tick;

        m_tcp_client.ReceiveTimeout = 30000; // Receive timeout
        m_tcp_client.SendTimeout = 10000;    // Send timeout

        m_remote_ip = Client.Client.RemoteEndPoint.ToString();
        m_remote_ip = m_remote_ip.Substring(0, m_remote_ip.IndexOf(":"));

        m_i_buffer = new Byte[1000];
        m_offset = 0;
        m_error = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // CashIOClient

    /// <summary>
    /// Start buffer reading
    /// </summary>
    public void StartReading()
    {
      try
      {
        NetworkStream _net;

        if (m_error)
        {
          return;
        }

        _net = m_net_stream;
        if (_net == null)
        {
          m_error = true;

          return;
        }

        _net.BeginRead(m_i_buffer, m_offset, m_i_buffer.Length - m_offset, DataReceived, this);
      }
      catch
      {
        m_error = true;
      }
    } // StartReading

    #endregion " Public Methods "

    #region " Internal Methods "

    /// <summary>
    /// Close stream
    /// </summary>
    internal void Close()
    {
      try
      {
        NetworkStream _tmp;

        _tmp = m_net_stream;
        if (_tmp != null)
        {
          m_net_stream = null;
          _tmp.Close();
        }
      }
      catch
      { }

      try
      {
        TcpClient _tmp;

        _tmp = m_tcp_client;
        if (_tmp != null)
        {
          m_tcp_client = null;
          _tmp.Client.Disconnect(false);
        }
      }
      catch
      { }

      try
      {
        Byte[] _tmp;

        _tmp = m_i_buffer;
        if (_tmp != null)
        {
          m_i_buffer = null;
        }
      }
      catch
      { }
    } // Close

    #endregion " Internal Methods "

    #region " Private Methods "

    /// <summary>
    /// Process data received
    /// </summary>
    /// <param name="AR"></param>
    private void DataReceived(IAsyncResult AR)
    {
      String _request;
      Boolean _error;
      Int32 _num_read;
      Int32 _idx_end;
      String _response;
      Byte[] _reply;

      _error = true;

      try
      {
        // Added a null validation to avoid exception
        if (m_net_stream == null)
        {
          // Stream can be closed before so we have to check if m_net_stream is not null
          return;
        }

        _num_read = m_net_stream.EndRead(AR);
        if (_num_read == 0)
        {
          return;
        }

        m_offset += _num_read;
        _request = Encoding.ASCII.GetString(m_i_buffer, 0, m_offset);
        _idx_end = _request.IndexOf("\r\n");

        if (_idx_end < 0)
        {
          // Partial message
          if (m_offset < m_i_buffer.Length)
          {
            // Buffer not full: continue receiving ...
            _error = false;
          }
          return;
        }

        // Full msg received!
        _request = _request.Substring(0, _idx_end);
        m_offset = 0;
        m_i_msg_tick = Environment.TickCount;

        if (this.Server.ProcessRequest(this, _request, out _response))
        {
          if (!String.IsNullOrEmpty(_response))
          {
            NetworkStream _net;
            _net = m_net_stream;
            if (_net != null)
            {
              _reply = Encoding.ASCII.GetBytes(_response + "\r\n");
              _net.Write(_reply, 0, _reply.Length);
              m_o_msg_tick = Environment.TickCount;
              _error = false;
            }
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _error = true;

      }
      finally
      {
        if (!_error)
        {
          StartReading();
        }

        if (_error)
        {
          // Set error!
          m_error = true;
        }
      }
    } // DataReceived

    #endregion " Private Methods "
  }
}