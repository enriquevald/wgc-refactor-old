﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LCD_ProcessRequest.cs
//// 
////      DESCRIPTION: LCD process request
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 07-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-OCT-2015 AMF    First release.
//// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
//// 12-JAN-2016 AMF    PBI 8268: Debit Request only with play session active
//// 13-JAN-2016 AMF    PBI 8267: Rollback
//// 15-JAN-2016 AMF    PBI 8465: Globalization
//// 18-JAN-2016 AMF    PBI 8260: Traceability
//// 29-JAN-2016 AMF    PBI 8267: Check currency
//// 16-FEB-2016 AMF    PBI 9434: Award prizes
//// 02-MAR-2016 AMF    PBI 10193: WCPMessage and InSession
//// 09-MAR-2016 AMF    PBI 10452: New Rollback Request
//// 10-MAR-2016 AMF    PBI 10452: Remove old Rollback Request
//// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
//// 28-ABR-2017 DMT    PBI 26733:Wigos - Section List Template
//// 22-JUN-2017 FJC    WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  public class LCD_ProcessRequest
  {

    #region " Constants "

    private const String TYPE_TYPE = "type";
    private const String TYPE_LOGIN = "login";
    private const String TYPE_LOGOUT = "logout";
    private const String TYPE_SUCCESS = "success";
    private const String TYPE_DEBIT = "debit";
    private const String TYPE_CREDIT = "credit";
    private const String TYPE_BULKCREDIT = "bulkcredit";
    private const String TYPE_OFFLINECREDIT = "offlinecredit";
    private const String TYPE_OFFLINEDEBIT = "offlinedebit";
    private const String TYPE_ERROR = "error";
    private const String TYPE_GAME_CREATED = "gamecreated";
    private const String TYPE_GAME_STARTING = "gamestarting";
    private const String TYPE_BALANCE = "getbalance";
    private const String TYPE_ROLLBACK = "rollback";

    private const String BALANCE_RESERVED_DISABLED = "0";
    private const String BALANCE_RESERVED_ZERO = "0.00";
    private const String BALANCE_RESERVED_FORMATTED = "##.00";

    #endregion " Constants "

    #region " Members "

    private HttpListenerContext m_context;

    #endregion " Members "

    #region " Public methods "

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_ProcessRequest
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_ProcessRequest()
    {

    } // LCD_ProcessRequest

    //------------------------------------------------------------------------------
    // PURPOSE: New LCD_ProcessRequest
    // 
    //  PARAMS:
    //      - INPUT:
    //          - HttpListenerContext
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public LCD_ProcessRequest(HttpListenerContext Context)
    {
      m_context = Context;
    } // LCD_ProcessRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Request
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public void ProcessRequest()
    {
      try
      {
        if (!Misc.IsGameGatewayEnabled())
        {
          ProcessDisabled();

          return;
        }

        if (m_context.Request.HttpMethod == "GET")
        {
          ProcessGET();

          return;
        }

        if (m_context.Request.HttpMethod == "POST")
        {
          ProcessPOST(GameGateway.PROVIDER_TYPE.PimPamGO);

          return;
        }

        if (m_context.Request.HttpMethod == "HEAD")
        {
          m_context.Response.StatusCode = (int)HttpStatusCode.OK;
          m_context.Response.StatusDescription = HttpStatusCode.OK.ToString();
          m_context.Response.Close();

          return;
        }

        // default
        m_context.Response.StatusCode = (int)HttpStatusCode.NotImplemented;
        m_context.Response.StatusDescription = HttpStatusCode.NotImplemented.ToString();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        m_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        m_context.Response.StatusDescription = _ex.Message;
        m_context.Response.Close();
      }
      finally
      {
        try
        {
          m_context.Response.Close();
        }
        catch { }
      }
    } //  ProcessRequest

    #endregion " Public methods "

    #region " Request methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Login Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String LoginRequest(String Xml)
    {
      DefaultRequest _request;
      LoginResponse _response;
      LCD_Session _session;

      _response = new LoginResponse();

      try
      {
        _request = new DefaultRequest();
        _request = (DefaultRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.SessionId, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        _response = LoginResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error LoginRequest");
    } // LoginRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Logout Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String LogoutRequest(String Xml)
    {
      DefaultRequest _request;
      LogoutResponse _response;
      LCD_Session _session;

      _response = new LogoutResponse();

      try
      {
        _request = new DefaultRequest();
        _request = (DefaultRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.SessionId, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        _response = LogoutResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error LogoutRequest");
    } // LogoutRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod GetBalance Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String BalanceRequest(String Xml)
    {
      BalanceRequest _request;
      BalanceResponse _response;
      LCD_Session _session;

      _response = new BalanceResponse();

      try
      {
        _request = new BalanceRequest();
        _request = (BalanceRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.SessionId, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        _response = BalanceResponse(_request, ref _session);


        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error BalanceRequest");
    } // GetBalanceRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String DebitRequest(String Xml)
    {
      DebitRequest _request;
      CreditDebitResponse _response;
      LCD_Session _session;
      XmlDocument _doc;
      XmlNode _node;

      _response = new CreditDebitResponse();

      try
      {
        _request = new DebitRequest();
        _request = (DebitRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.SessionId, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        if (!_session.IsSessionWinUp(_session.SessionId, _session.TerminalId))
        {
        if (GeneralParam.GetBoolean("GameGateway", "DebitRequest.CheckSession", true) && !GameGateway.IsInPlaySession(ref _session))
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID, "Session Closed");
        }

        _session.TransactionId = _request.Transaction;
        _session.TransactionType = GameGateway.TRANSACTION_TYPE.BET;

        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/bets");
        _session.Bets = _node.OuterXml;

        _response = DebitResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error DebitRequest");
    } // DebitRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String CreditRequest(String Xml)
    {
      CreditRequest _request;
      CreditDebitResponse _response;
      LCD_Session _session;
      Decimal _jackpot_amount;
      XmlDocument _doc;
      XmlNode _node;

      _response = new CreditDebitResponse();

      try
      {
        _request = new CreditRequest();
        _request = (CreditRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.SessionId, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        foreach (CreditBet _bet in _request.Bets)
        {
          if (!AmountStringToDecimal((String.IsNullOrEmpty(_bet.Jackpot) ? BALANCE_RESERVED_ZERO : _bet.Jackpot), out _jackpot_amount))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect Jackpot amount");
          }

          _session.TotalJackpot += _jackpot_amount;
        }

        _session.TransactionId = _request.Transaction;
        _session.TransactionType = GameGateway.TRANSACTION_TYPE.PRIZE;

        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/bets");
        _session.Bets = _node.OuterXml;

        _response = CreditResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error CreditRequest");
    } // CreditRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Bulk Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String BulkCreditRequest(String Xml)
    {
      BulkCreditRequest _request;
      BulkCreditResponse _response;
      LCD_Session _session;
      XmlDocument _doc;
      XmlNode _node;

      try
      {
        _request = new BulkCreditRequest();
        _request = (BulkCreditRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.Authentication.PartnerId, _request.Authentication.Password);

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        _session.TransactionId = _request.Transaction;
        _session.TransactionType = GameGateway.TRANSACTION_TYPE.PRIZE_PENDING;

        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/credits");
        _session.Bets = _node.OuterXml;

        _response = BulkCreditResponse(_request, ref _session, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error BulkCreditRequest");
    } // BulkCreditRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Offline Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String OfflineCreditRequest(String Xml)
    {
      OfflineCreditRequest _request;
      OfflineResponse _response;
      LCD_Session _session;
      Decimal _jackpot_amount;
      XmlDocument _doc;
      XmlNode _node;

      try
      {
        _request = new OfflineCreditRequest();
        _request = (OfflineCreditRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(Int64.Parse(_request.UserId), _request.Authentication.PartnerId, _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        foreach (CreditBet _bet in _request.Bets)
        {
          if (!AmountStringToDecimal((String.IsNullOrEmpty(_bet.Jackpot) ? BALANCE_RESERVED_ZERO : _bet.Jackpot), out _jackpot_amount))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect Jackpot amount");
          }

          _session.TotalJackpot += _jackpot_amount;
        }

        _session.TransactionId = _request.Transaction;
        _session.TransactionType = GameGateway.TRANSACTION_TYPE.PRIZE;

        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/bets");
        _session.Bets = _node.OuterXml;

        _response = OfflineCreditResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error OfflineCreditRequest");
    } // OfflineCreditRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Offline Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String OfflineDebitRequest(String Xml)
    {
      OfflineDebitRequest _request;
      OfflineResponse _response;
      LCD_Session _session;
      XmlDocument _doc;
      XmlNode _node;

      try
      {
        _request = new OfflineDebitRequest();
        _request = (OfflineDebitRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(Int64.Parse(_request.UserId), _request.Authentication.PartnerId, _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        _session.TransactionId = _request.Transaction;
        _session.TransactionType = GameGateway.TRANSACTION_TYPE.BET;

        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/bets");
        _session.Bets = _node.OuterXml;

        _response = OfflineDebitResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error OfflineDebitRequest");
    } // OfflineDebitRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Game Create Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String GameCreatedRequest(String Xml)
    {
      GameCreatedRequest _request;
      GameCreatedStartingResponse _response;
      LCD_Session _session;

      _response = new GameCreatedStartingResponse();

      try
      {
        _request = new GameCreatedRequest();
        _request = (GameCreatedRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.Authentication.PartnerId, _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        _response = GameCreatedResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error GameCreatedRequest");
    } // GameCreatedRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Game Starting Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String GameStartingRequest(String Xml)
    {
      GameStartingRequest _request;
      GameCreatedStartingResponse _response;
      LCD_Session _session;

      _response = new GameCreatedStartingResponse();

      try
      {
        _request = new GameStartingRequest();
        _request = (GameStartingRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.Authentication.PartnerId, _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        _response = GameStartingResponse(_request, ref _session);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error GameStartingRequest");
    } // GameStartingRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod Rollback Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String RollbackRequest(String Xml)
    {
      RollbackRequest _request;
      BulkCreditResponse _response;
      LCD_Session _session;
      XmlDocument _doc;
      XmlNode _node;

      try
      {
        _request = new RollbackRequest();
        _request = (RollbackRequest)DeserializeRequest(Xml, _request);

        _session = new LCD_Session(_request.Authentication.PartnerId, _request.Authentication.Password);

        if (GeneralParam.GetBoolean("GameGateway", "CheckCurrency", true) && _request.Currency != _session.CurrencyIsoCode)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERROR, "Incorrect currency");
        }

        if (_request.GameInstanceId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameInstaceId missing");
        }
        else if (_request.GameId == String.Empty)
        {
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, "GameId missing");
        }
        else
        {
          _session.GameInstanceId = Int64.Parse(_request.GameInstanceId);
          _session.GameId = Int64.Parse(_request.GameId);

          if (!Gateway.ValidateGameGatewayGames(_session))
          {
            return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH, String.Format("GameInstaceId {0} - GameId {1} invalids.", _request.GameInstanceId, _request.GameId));
          }
        }

        _session.TransactionType = GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING;


        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _node = _doc.SelectSingleNode("/request/credits");
        _session.Bets = _node.OuterXml;

        _response = RollbackResponse(_request, ref _session, Int32.Parse(_request.Authentication.PartnerId), _request.Authentication.Password);

        if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return ErrorResponse(_session.ErrorCode, _session.ErrorMessage);
        }

        return SerializeResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL, "Error RollbackRequest");
    } // RollbackRequest

    #endregion " Request methods "

    #region " Private methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Process when funcionality is disabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ProcessDisabled()
    {
      ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_GENERAL, "Server disabled"));
    } // ProcessDisabled

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod GET
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ProcessGET()
    {
      ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_TRUSTEDIP, "GET non implemented"));
    } // ProcessGET

    //------------------------------------------------------------------------------
    // PURPOSE: Process HttpMethod POST
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private void ProcessPOST(GameGateway.PROVIDER_TYPE ProviderType)
    {
      String _xml_message = String.Empty;
      XmlDocument _xml_doc = new XmlDocument();
      String _xml_response = String.Empty;

      if (!ManageXmlMessage(ref _xml_message, ref _xml_doc))
      {
        return;
      }

      _xml_response = GetXmlResponse(_xml_message, _xml_doc.GetElementsByTagName(TYPE_TYPE)[0].InnerText);

      ManageContextResponse(_xml_response);

      GameGateway.ManageLog(_xml_message, _xml_response, ProviderType);

    } // ProcessPOST

    //------------------------------------------------------------------------------
    // PURPOSE: Get Xml response by Request Type
    // 
    //  PARAMS:
    //      - INPUT:
    //          - XmlMessage
    //          - Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String GetXmlResponse(String XmlMessage, String Type)
    {

      switch (Type)
      {
        case TYPE_LOGIN:
          return LoginRequest(XmlMessage);

        case TYPE_LOGOUT:
          return LogoutRequest(XmlMessage);

        case TYPE_DEBIT:
          return DebitRequest(XmlMessage);

        case TYPE_CREDIT:
          return CreditRequest(XmlMessage);

        case TYPE_BULKCREDIT:
          return BulkCreditRequest(XmlMessage);

        case TYPE_OFFLINECREDIT:
          return OfflineCreditRequest(XmlMessage);

        case TYPE_OFFLINEDEBIT:
          return OfflineDebitRequest(XmlMessage);

        case TYPE_GAME_CREATED:
          return GameCreatedRequest(XmlMessage);

        case TYPE_GAME_STARTING:
          return GameStartingRequest(XmlMessage);

        case TYPE_BALANCE:
          return BalanceRequest(XmlMessage);

        case TYPE_ROLLBACK:
          return RollbackRequest(XmlMessage);

        default:
          return ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_REQUESTTYPE, "Request Type : " + Type);
      }
    } // GetXmlResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Serialize Error Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ErrorCode
    //          - ErrorMessage
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String 
    //
    //   NOTES:
    //
    private String ErrorResponse(GameGateway.RESPONSE_ERROR_CODE ErrorCode, String ErrorMessage)
    {
      ErrorResponse _response;

      _response = new ErrorResponse();
      _response.Type = TYPE_ERROR;
      _response.Code = ((int)ErrorCode).ToString();
      _response.Message = ErrorMessage;

      return SerializeResponse(_response);
    } // ErrorResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Deserialize Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //          - ObjectRequest
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Object
    // 
    //   NOTES:
    //    
    private Object DeserializeRequest(String Xml, Object ObjectRequest)
    {
      MemoryStream _mem_stream;

      try
      {
        XmlSerializer _serializer = new XmlSerializer(ObjectRequest.GetType());
        _mem_stream = new MemoryStream(Encoding.UTF8.GetBytes(Xml));

        return _serializer.Deserialize(_mem_stream);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    } // DeserializeRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Serialize Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - ObjectResponse
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //   
    private String SerializeResponse(Object ObjectResponse)
    {
      XmlSerializer serializer = new XmlSerializer(ObjectResponse.GetType());
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      try
      {
        using (StringWriter writer = new Utf8StringWriter())
        {
          _name_spaces.Add("", "");
          serializer.Serialize(writer, ObjectResponse, _name_spaces);

          return writer.ToString();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // SerializeResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Xml message
    // 
    //  PARAMS:
    //      - INPUT:
    //          - XmlMessage
    //          - XmlDocument
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //   
    private Boolean ManageXmlMessage(ref String XmlMessage, ref XmlDocument XmlDoc)
    {
      StreamReader _data_text;

      try
      {
        _data_text = new StreamReader(m_context.Request.InputStream, m_context.Request.ContentEncoding);
        XmlMessage = _data_text.ReadToEnd();

        if (XmlMessage.Length > 0)
        {
          XmlDoc.LoadXml(XmlMessage);

          return true;
        }
        else
        {
          ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_BODY, "No body"));
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        ManageContextResponse(ErrorResponse(GameGateway.RESPONSE_ERROR_CODE.ERR_NO_BODY, "Error body: " + _ex.Message));
      }

      return false;
    } // ManageXmlMessage

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Context Response
    // 
    //  PARAMS:
    //      - INPUT:
    //          - XmlResponse
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //   
    private void ManageContextResponse(String XmlResponse)
    {
      Byte[] _bf;

      try
      {
        _bf = Encoding.UTF8.GetBytes(XmlResponse);

        m_context.Response.StatusCode = (int)HttpStatusCode.OK;
        m_context.Response.StatusDescription = "OK";
        m_context.Response.ContentLength64 = _bf.Length;
        m_context.Response.OutputStream.Write(_bf, 0, _bf.Length);
        m_context.Response.OutputStream.Close();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Message("ManageContextResponse: HttpListener not allowed");
      }
    } // ManageContextResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Convert String Amount to Decimal
    // 
    //  PARAMS:
    //      - INPUT:
    //          - StrAmount
    //
    //      - OUTPUT:
    //          - DecAmount
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean AmountStringToDecimal(String StrAmount, out Decimal DecAmount)
    {
      DecAmount = 0;

      if (String.IsNullOrEmpty(StrAmount.Trim()) || !Decimal.TryParse(StrAmount, System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat, out DecAmount))
      {
        return false;
      }

      return true;
    } // AmountStringToDecimal

    //------------------------------------------------------------------------------
    // PURPOSE: Check Balance Reserved in Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - StrAmount
    //          - LCD_Session
    //
    //      - OUTPUT:
    //          - DecAmount
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean CheckDebitBalanceReserved(String StrAmount, ref LCD_Session Session, out Decimal SubAmount)
    {
      SubAmount = 0;

      if (!AmountStringToDecimal(StrAmount, out SubAmount))
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
        Session.ErrorMessage = "Incorrect amount";

        return false;
      }

      if (Session.Balance.Reserved < SubAmount)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
        Session.ErrorMessage = "Not enough reserved";

        return false;
      }

      return true;
    } // CheckDebitBalanceReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Check Balance Non Reserved in Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - StrAmount
    //          - LCD_Session
    //
    //      - OUTPUT:
    //          - DecAmount
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean CheckDebitBalanceNonReserved(String StrAmount, ref LCD_Session Session, out Decimal SubAmount)
    {
      SubAmount = 0;

      if (!AmountStringToDecimal(StrAmount, out SubAmount))
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_GENERAL;
        Session.ErrorMessage = "Incorrect amount";

        return false;
      }

      if (Session.Balance.TotalRedeemable < SubAmount)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
        Session.ErrorMessage = "Not enough balance";

        return false;
      }

      return true;
    } // CheckDebitBalanceNonReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Return Balance tag
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    //   NOTES:
    //
    private String TagReservedBalance(LCD_Session Session)
    {
      if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS)
      {
        return BALANCE_RESERVED_DISABLED;
      }
      else
      {
        if (Session.Balance.Reserved == 0)
        {
          return BALANCE_RESERVED_ZERO;
        }
        else
        {
          return Session.Balance.Reserved.ToString(BALANCE_RESERVED_FORMATTED, System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat);
        }
      }
    } // TagReservedBalance

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Debit Response Non Reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DebitResponse
    // 
    //   NOTES:
    //
    private CreditDebitResponse DebitResponseNonReserved(DebitRequest Request, ref LCD_Session Session)
    {
      CreditDebitResponse _response;

      _response = new CreditDebitResponse();

      _response.Type = TYPE_DEBIT;
      _response.SessionId = Request.SessionId;

      try
      {
        // First, check if the account have enough balance
        if (!ProcessDebitResponseNonReserved(Request, ref _response, ref Session))
        {
          if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE)
          {
            // Second, if the account is in session, transfer credit from EGM
            if (GameGateway.IsInPlaySession(ref Session))
            {
              if (TransferCreditFromEGM(ref Session))
              {
                ProcessDebitResponseNonReserved(Request, ref _response, ref Session);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error DebitResponseNonReserved";
      }

      return _response;
    } // DebitResponseNonReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Debit Response Reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DebitResponse
    // 
    //   NOTES:
    //
    private CreditDebitResponse DebitResponseReserved(DebitRequest Request, ref LCD_Session Session)
    {
      Decimal _sub_amount;
      CreditDebitResponse _response;

      _response = new CreditDebitResponse();

      _response.Type = TYPE_DEBIT;
      _response.SessionId = Request.SessionId;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return _response;
          }

          if (!CheckDebitBalanceReserved(Request.Amount, ref Session, out _sub_amount))
          {
            return _response;
          }

          Session.TotalBets = Math.Max(Request.Bets.Length, 1);

          GameGateway.ProcessCreditDebit(0, _sub_amount, ref Session, _db_trx.SqlTransaction);

          _response.Balance = TagReservedBalance(Session);

          if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            _db_trx.Commit();
            _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error DebitResponseReserved";
      }

      return _response;
    } // DebitResponseReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Debit Response Mixed
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DebitResponse
    // 
    //   NOTES:
    //
    private CreditDebitResponse DebitResponseMixed(DebitRequest Request, Boolean FirsTime, ref LCD_Session Session)
    {
      Decimal _sub_amount;
      Decimal _sub_amount_partial;
      CreditDebitResponse _response;
      Boolean _need_amount_from_egm;

      _response = new CreditDebitResponse();

      _response.Type = TYPE_DEBIT;
      _response.SessionId = Request.SessionId;
      _need_amount_from_egm = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return _response;
          }

          if (!AmountStringToDecimal(Request.Amount, out _sub_amount))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
            Session.ErrorMessage = "DebitResponseMixed. Incorrect amount";

            return _response;
          }

          _sub_amount_partial = _sub_amount;

          // First: Reserved ==> Check if the account have enough reserved balance
          if (Session.Balance.Reserved >= _sub_amount_partial)
          {
            _sub_amount_partial = 0;
          }
          else
          {
            _sub_amount_partial = _sub_amount_partial - Session.Balance.Reserved;
          }

          // Second: Balance ==> Check if the account have enough balance
          if (_sub_amount_partial > 0)
          {
            if ((Session.Balance.TotalRedeemable - Session.Balance.Reserved) >= _sub_amount_partial)
            {
              _sub_amount_partial = 0;
            }
            else
            {
              _sub_amount_partial = _sub_amount_partial - (Session.Balance.TotalRedeemable - Session.Balance.Reserved);
            }
          }

          // Third: EGM ==> Check if the account is in play session
          if (_sub_amount_partial == 0)
          {
            _need_amount_from_egm = false;
          }
          else
          {
            if (!FirsTime)
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
              Session.ErrorMessage = "Not enough balance";

              return _response;
            }

            _need_amount_from_egm = true;
            if (!GameGateway.IsInPlaySession(ref Session))
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
              Session.ErrorMessage = "Not enough balance";

              return _response;
            }
          }

          if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK && !_need_amount_from_egm)
          {
            Session.TotalBets = Math.Max(Request.Bets.Length, 1);

            GameGateway.ProcessCreditDebit(0, _sub_amount, ref Session, _db_trx.SqlTransaction);

            if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
            {
              if (!FirsTime)
              {
                Gateway.UpdateGamegatewayCommand(Session, false, GetCreditStatus.FINISHED, _db_trx.SqlTransaction);
              }

              _db_trx.Commit();

              _response.Balance = TagReservedBalance(Session);
              _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

              return _response;
            }
          }
        }

        // Fourth: EGM ==> The account is in play session and get money from EGM
        if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK && _need_amount_from_egm)
        {
          if (TransferCreditFromEGM(ref Session))
          {
            return DebitResponseMixed(Request, false, ref Session);
          }
          else
          {
            //Error getting money from EGM
            return _response;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error DebitResponseMixed";
      }

      return _response;
    }//DebitResponseMixed

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Offline Debit Response Non Reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OfflineDebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - OfflineResponse
    // 
    //   NOTES:
    //
    private OfflineResponse OfflineDebitResponseNonReserved(OfflineDebitRequest Request, ref LCD_Session Session)
    {
      OfflineResponse _response;

      _response = new OfflineResponse();

      _response.Type = TYPE_OFFLINEDEBIT;
      _response.UserId = Request.UserId;

      try
      {
        // First, check if the account have enough balance
        if (!ProcessOfflineDebitResponseNonReserved(Request, ref _response, ref Session))
        {
          if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE)
          {
            // Second, if the account is in session, transfer credit from EGM
            if (GameGateway.IsInPlaySession(ref Session))
            {
              if (TransferCreditFromEGM(ref Session))
              {
                ProcessOfflineDebitResponseNonReserved(Request, ref _response, ref Session);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error OfflineDebitResponseNonReserved";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return _response;
    } // OfflineDebitResponseNonReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Manage Offline Debit Response Reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OfflineDebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - OfflineResponse
    // 
    //   NOTES:
    //
    private OfflineResponse OfflineDebitResponseReserved(OfflineDebitRequest Request, ref LCD_Session Session)
    {
      Decimal _sub_amount;
      OfflineResponse _response;

      _response = new OfflineResponse();

      _response.Type = TYPE_OFFLINEDEBIT;
      _response.UserId = Request.UserId;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return _response;
          }

          if (!CheckDebitBalanceReserved(Request.Amount, ref Session, out _sub_amount))
          {
            return _response;
          }

          Session.TotalBets = Math.Max(Request.Bets.Length, 1);

          GameGateway.ProcessCreditDebit(0, _sub_amount, ref Session, _db_trx.SqlTransaction);

          _response.Balance = TagReservedBalance(Session);

          if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            _db_trx.Commit();
            _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();
          }
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error OfflineDebitResponseReserved";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return _response;
    } // OfflineDebitResponseReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Transfer credit from EGM --> account --> GameGateWay
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean TransferCreditFromEGM(ref LCD_Session Session)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Gateway.InsertGamegatewayCommand(ref Session, _db_trx.SqlTransaction))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
            Session.ErrorMessage = "TransferCreditFromEGM.InsertGamegatewayCommand";

            Log.Message(Session.ErrorMessage);

            return false;
          }

          if (!GameGateway.GetCommandParameterGameGateway(ref Session, GameGateway.TYPE_WCP.DEBIT, _db_trx.SqlTransaction))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
            Session.ErrorMessage = "TransferCreditFromEGM.GetCommandParameterGameGateway";

            Log.Message(Session.ErrorMessage);

            return false;
          }

          if (!Gateway.UpdateGamegatewayCommand(Session, true, GetCreditStatus.PENDING, _db_trx.SqlTransaction))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
            Session.ErrorMessage = "TransferCreditFromEGM.UpdateGamegatewayCommand";

            Log.Message(Session.ErrorMessage);

            return false;
          }

          _db_trx.Commit();
        }

        if (!Gateway.GetGamegatewayCommandResponse(Session))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
          Session.ErrorMessage = "TransferCreditFromEGM. Not enough balance AccountId: " + Session.AccountId.ToString();

          Log.Message(Session.ErrorMessage);

          return false;
        }
        else
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.OK;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error TransferCreditFromEGM";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;
    } // TransferCreditFromEGM

    //------------------------------------------------------------------------------
    // PURPOSE: Process Debit response non reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DebitRequest
    //          - CreditDebitResponse
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean ProcessDebitResponseNonReserved(DebitRequest Request, ref CreditDebitResponse Response, ref LCD_Session Session)
    {
      Decimal _sub_amount;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return false;
          }

          if (CheckDebitBalanceNonReserved(Request.Amount, ref Session, out _sub_amount))
          {
            Session.TotalBets = Math.Max(Request.Bets.Length, 1);

            GameGateway.ProcessCreditDebit(0, _sub_amount, ref Session, _db_trx.SqlTransaction);

            if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
            {
              Gateway.UpdateGamegatewayCommand(Session, false, GetCreditStatus.FINISHED, _db_trx.SqlTransaction);

              _db_trx.Commit();

              Response.Balance = TagReservedBalance(Session);
              Response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

              return true;
            }
          }
          // Canceled by WCP, not necesary here
          //Gateway.UpdateWcpCommand(Session, true, GetCreditStatus.CANCELLED, _db_trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessDebitResponseNonReserved";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;
    } // ProcessDebitResponseNonReserved

    //------------------------------------------------------------------------------
    // PURPOSE: Process Offline Debit response non reserved
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OfflineDebitRequest
    //          - OfflineResponse
    //          - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    // 
    //   NOTES:
    //
    private Boolean ProcessOfflineDebitResponseNonReserved(OfflineDebitRequest Request, ref OfflineResponse Response, ref LCD_Session Session)
    {
      Decimal _sub_amount;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return false;
          }

          if (CheckDebitBalanceNonReserved(Request.Amount, ref Session, out _sub_amount))
          {
            Session.TotalBets = Math.Max(Request.Bets.Length, 1);

            GameGateway.ProcessCreditDebit(0, _sub_amount, ref Session, _db_trx.SqlTransaction);

            if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
            {
              Gateway.UpdateGamegatewayCommand(Session, false, GetCreditStatus.FINISHED, _db_trx.SqlTransaction);

              _db_trx.Commit();

              Response.Balance = TagReservedBalance(Session);
              Response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

              return true;
            }
          }
          // Canceled by WCP, not necesary here
          //Gateway.UpdateWcpCommand(Session, true, GetCreditStatus.CANCELLED, _db_trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessOfflineDebitResponseNonReserved";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;
    } // ProcessOfflineDebitResponseNonReserved

    #endregion " Private methods "

    #region " Response methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Login Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DefaultRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - LoginResponse
    // 
    //   NOTES:
    //
    private LoginResponse LoginResponse(DefaultRequest Request, ref LCD_Session Session)
    {
      LoginResponse _response;
      Int32 _mode_reserved;

      _response = new LoginResponse();
      _mode_reserved = (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return _response;
          }

          _response.Type = TYPE_LOGIN;
          _response.SessionId = Request.SessionId;
          _response.UserId = Session.AccountId.ToString();
          _response.AffiliateId = Session.AccountId.ToString();
          _response.NickName = Session.NickName;
          _response.Country = GeneralParam.GetString("RegionalOptions", "CountryISOCode2", "MX");
          _response.BalanceVisible = "0";

          _mode_reserved = GeneralParam.GetInt32("GameGateway", "Reserved.Enabled", (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS);

          if ((_mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS && GameGateway.AccountModeReserved(Session.AccountId))
              || _mode_reserved == (Int32)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
          {
            _response.BalanceVisible = "1";
          }
          _response.Balance = TagReservedBalance(Session);
        }
      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error LoginResponse";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return _response;
    } // LoginResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Logout Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DefaultRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - LogoutResponse
    // 
    //   NOTES:
    //
    private LogoutResponse LogoutResponse(DefaultRequest Request, ref LCD_Session Session)
    {
      LogoutResponse _response;

      _response = new LogoutResponse();

      try
      {
        _response.Type = TYPE_SUCCESS;
        _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _response;
    } // LogoutResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to GetBalance Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - BalanceRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - BalanceResponse
    // 
    //   NOTES:
    //
    private BalanceResponse BalanceResponse(BalanceRequest Request, ref LCD_Session Session)
    {
      BalanceResponse _response;
      _response = new BalanceResponse();

      _response.SessionId = Request.SessionId;
      _response.Type = Request.Type;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

          if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
          {
            return _response;
          }

          _response.Type = TYPE_BALANCE;
          _response.SessionId = Request.SessionId;

          _response.Accounts = new Account[1];

          _response.Accounts[0] = new Account();
          _response.Accounts[0].RealBalance = TagReservedBalance(Session);
          _response.Accounts[0].Currency = Session.CurrencyIsoCode;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _response;
    } // DebitResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DebitResponse
    // 
    //   NOTES:
    //
    private CreditDebitResponse DebitResponse(DebitRequest Request, ref LCD_Session Session)
    {
      CreditDebitResponse _response;
      _response = new CreditDebitResponse();

      if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS)
      {
        _response = DebitResponseReserved(Request, ref Session);

        return _response;
      }

      if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS)
      {
        _response = DebitResponseNonReserved(Request, ref Session);

        return _response;
      }

      if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
      {
        _response = DebitResponseMixed(Request, true, ref Session);

        return _response;
      }

      return _response;
    } // DebitResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CreditRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - CreditResponse
    // 
    //   NOTES:
    //
    private CreditDebitResponse CreditResponse(CreditRequest Request, ref LCD_Session Session)
    {
      CreditDebitResponse _response;
      Decimal _add_amount;

      _response = new CreditDebitResponse();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        GameGateway.GetDataAccount(ref Session, _db_trx.SqlTransaction);

        if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return _response;
        }

        _response.Type = TYPE_CREDIT;
        _response.SessionId = Request.SessionId;
        _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

        Session.TotalBets = Math.Max(Request.Bets.Length, 1);

        if (!AmountStringToDecimal(Request.Amount, out _add_amount))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Incorrect amount";

          return _response;
        }

        GameGateway.ProcessCreditDebit(_add_amount, 0, ref Session, _db_trx.SqlTransaction);

        _response.Balance = TagReservedBalance(Session);

        if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          _db_trx.Commit();
        }
      }

      return _response;
    } // CreditResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Bulk Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - BulkCreditRequest
    //          - LCD_Session
    //          - PartnerId
    //          - Password
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - BulkCreditResponse
    // 
    //   NOTES:
    //
    private BulkCreditResponse BulkCreditResponse(BulkCreditRequest Request, ref LCD_Session Session, Int32 PartnerId, String Password)
    {
      BulkCreditResponse _response;
      Dictionary<Int64, AccountCredit> _credits;
      Int32 _index;
      DataTable _dt_credits;
      BulkCredit[] _bulk_credit;
      Decimal _amount;
      Decimal _jackpot;
      LCD_Session _session;
      DataRow[] _rows;

      _response = new BulkCreditResponse();
      _credits = new Dictionary<Int64, AccountCredit>();
      _index = 0;

      _response.Type = TYPE_BULKCREDIT;
      _bulk_credit = new BulkCredit[1];

      foreach (Credit _credit in Request.Credits)
      {
        _session = (LCD_Session)Session.Clone();
        _session.AccountId = Int64.Parse(_credit.UserId);

        if (!AmountStringToDecimal(_credit.Amount, out _amount))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Incorrect amount";

          return _response;
        }

        if (!AmountStringToDecimal((String.IsNullOrEmpty(_credit.Jackpot) ? BALANCE_RESERVED_ZERO : _credit.Jackpot), out _jackpot))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Incorrect Jackpot amount";

          return _response;
        }

        if (_amount > 0 || _jackpot > 0)
        {
          if (_credits.ContainsKey(_session.AccountId))
          {
            _credits[_session.AccountId].TotalPrizes += _amount + _jackpot;
            _credits[_session.AccountId].TotalJackpot += _jackpot;
            _credits[_session.AccountId].TotalNumPrizes += 1;
          }
          else
          {
            _credits.Add(_session.AccountId, new AccountCredit(_amount, _jackpot, _session));

            Array.Resize(ref _bulk_credit, _index + 1);

            _bulk_credit[_index] = new BulkCredit();
            _bulk_credit[_index].UserId = _credit.UserId;
            _bulk_credit[_index].Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();
            _bulk_credit[_index].Balance = BALANCE_RESERVED_ZERO;

            _index++;
          }
        }
      }

      _response.Credits = _bulk_credit;

      _dt_credits = Gateway.DictionaryBulkToDataTable(_credits);

      if (_dt_credits.Rows.Count > 0)
      {
        if (!Gateway.InsertGameGatewayBatch(_dt_credits, Session.TransactionType, out _rows))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Error InsertGameGatewayBulk";

          if (_rows != null && _rows.Length > 0)
          {
            Session.ErrorMessage += String.Format(". Duplicated registers in TransactionId: [{0}]", _rows[0][Gateway.BULK_TRANSACTION]);
          }
          else
          {
            Session.ErrorMessage += ". Incorrect info";
          }
        }
      }
      else
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
        Session.ErrorMessage = "Error BulkCredit: non found credit registers";
      }

      if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
      {
        LCD_WebServer.InitExecuteBulkThread(Session.TransactionId);
      }

      return _response;
    } // BulkCreditResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Offline Credit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OfflineCreditRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - OfflineResponse
    // 
    //   NOTES:
    //
    private OfflineResponse OfflineCreditResponse(OfflineCreditRequest Request, ref LCD_Session Session)
    {
      OfflineResponse _response;
      Decimal _add_amount;

      _response = new OfflineResponse();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        GameGateway.GetDataAccount(ref Session, false, _db_trx.SqlTransaction);

        if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return _response;
        }

        _response.Type = TYPE_OFFLINECREDIT;
        _response.UserId = Request.UserId;
        _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

        Session.TotalBets = Math.Max(Request.Bets.Length, 1);

        if (!AmountStringToDecimal(Request.Amount, out _add_amount))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
          Session.ErrorMessage = "Incorrect amount";

          return _response;
        }

        GameGateway.ProcessCreditDebit(_add_amount, 0, ref Session, _db_trx.SqlTransaction);

        _response.Balance = TagReservedBalance(Session);

        if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          _db_trx.Commit();
        }
      }

      return _response;
    } // OfflineCreditResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Offline Debit Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OfflineDebitRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - OfflineResponse
    // 
    //   NOTES:
    //
    private OfflineResponse OfflineDebitResponse(OfflineDebitRequest Request, ref LCD_Session Session)
    {
      OfflineResponse _response;

      if (Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS
         || Session.ReservedCreditEnabled == MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED)
      {
        _response = OfflineDebitResponseReserved(Request, ref Session);
      }
      else
      {
        _response = OfflineDebitResponseNonReserved(Request, ref Session);
      }

      return _response;
    } // OfflineDebitResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Game Created Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - GameCreatedRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - GameCreatedStartingResponse
    // 
    //   NOTES:
    //
    private GameCreatedStartingResponse GameCreatedResponse(GameCreatedRequest Request, ref LCD_Session Session)
    {
      GameCreatedStartingResponse _response;

      _response = new GameCreatedStartingResponse();

      _response.Type = TYPE_GAME_CREATED;
      _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

      GameGateway.InsertGameGatewayGames(Request, ref Session, true);

      return _response;
    } // GameCreatedStartingResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Game Starting Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - GameStartingRequest
    //          - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - GameCreatedStartingResponse
    // 
    //   NOTES:
    //
    private GameCreatedStartingResponse GameStartingResponse(GameStartingRequest Request, ref LCD_Session Session)
    {
      GameCreatedStartingResponse _response;
      GameCreatedRequest _uknown;
      Decimal _jackpot_amount;

      _response = new GameCreatedStartingResponse();

      _response.Type = TYPE_GAME_STARTING;
      _response.Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();

      _uknown = GameGateway.CreateGameCreatedRequestUknown(Request.GameId, Request.GameInstanceId);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!GameGateway.InsertGameGatewayGames(_uknown, ref Session, false, _db_trx.SqlTransaction))
          {
            return _response;
          }

          if (!AmountStringToDecimal(Request.Jackpot, out _jackpot_amount))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_ACCOUNTING;
            Session.ErrorMessage = "Incorrect amount";

            return _response;
          }

          if (!Gateway.UpdateGameGatewayGameInstancesJackpot(Session, _jackpot_amount, _db_trx.SqlTransaction))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
            Session.ErrorMessage = String.Format("Error UpdateGameGatewayGameInstancesJackpot. GameInstanceId: {0}", Session.GameInstanceId);

            return _response;
          }

          if (Session.ShowNewGameMessage && Session.ShowLCDMessage)
          {
            if (!GameGateway.GetCommandParameterGameGateway(ref Session, GameGateway.TYPE_WCP.DRAW, _db_trx.SqlTransaction))
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
              Session.ErrorMessage = String.Format("Error GetDrawParameterGameGatewayGames. GameInstanceId: {0}", Session.GameInstanceId);

              return _response;
            }

            Gateway.GetGameGatewayTerminals(ref Session, _db_trx.SqlTransaction);

            if (!GameGateway.InsertGamegatewayCommandBatch(Session, GameGateway.TYPE_WCP.DRAW, _db_trx.SqlTransaction))
            {
              Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
              Session.ErrorMessage = String.Format("Error InsertGamegatewayCommandBatch. GameInstanceId: {0}", Session.GameInstanceId);

              return _response;
            }
          }

          Gateway.UpdateNonClosedGames(Request.GameId, ref Session, _db_trx.SqlTransaction);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = String.Format("Error GameStartingResponse. GameInstanceId: {0}", Session.GameInstanceId);
      }

      return _response;
    } // GameCreatedStartingResponse

    //------------------------------------------------------------------------------
    // PURPOSE: Response to Rollback Request
    // 
    //  PARAMS:
    //      - INPUT:
    //          - RollbackRequest
    //          - LCD_Session
    //          - PartnerId
    //          - Password
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - BulkCreditResponse
    // 
    //   NOTES:
    //
    private BulkCreditResponse RollbackResponse(RollbackRequest Request, ref LCD_Session Session, Int32 PartnerId, String Password)
    {
      BulkCreditResponse _response;
      Dictionary<String, RollbackCredits> _credits;
      Int32 _index;
      DataTable _dt_credits;
      BulkCredit[] _bulk_credit;
      Decimal _amount;
      LCD_Session _session;
      DataRow[] _rows;

      _response = new BulkCreditResponse();
      _credits = new Dictionary<String, RollbackCredits>();
      _index = 0;

      _response.Type = TYPE_ROLLBACK;
      _bulk_credit = new BulkCredit[1];

      foreach (RollbackCredit _credit in Request.Credits)
      {
        _session = (LCD_Session)Session.Clone();
        _session.AccountId = Int64.Parse(_credit.UserId);
        _session.TransactionId = _credit.Transaction;

        if (!AmountStringToDecimal(_credit.Amount, out _amount))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Error Rollback: Incorrect amount";

          return _response;
        }

        if (_amount > 0)
        {
          if (_credits.ContainsKey(_session.TransactionId))
          {
            Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
            Session.ErrorMessage = "Error Rollback: Transaction repeat";

            return _response;
          }
          else
          {
            _credits.Add(_session.TransactionId, new RollbackCredits(_amount, Int32.Parse(_credit.NumberOfBets), _session));

            Array.Resize(ref _bulk_credit, _index + 1);

            _bulk_credit[_index] = new BulkCredit();
            _bulk_credit[_index].UserId = _credit.UserId;
            _bulk_credit[_index].Code = ((int)GameGateway.RESPONSE_ERROR_CODE.OK).ToString();
            _bulk_credit[_index].Balance = BALANCE_RESERVED_ZERO;

            _index++;
          }
        }
      }

      _response.Credits = _bulk_credit;

      _dt_credits = Gateway.DictionaryRollbackToDataTable(_credits);

      if (_dt_credits.Rows.Count > 0)
      {
        if (!Gateway.InsertGameGatewayBatch(_dt_credits, Session.TransactionType, out _rows))
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
          Session.ErrorMessage = "Error InsertRollback";

          if (_rows != null && _rows.Length > 0)
          {
            Session.ErrorMessage += String.Format(". Incorrect Rollback in TransactionId: [{0}]", _rows[0][Gateway.BULK_TRANSACTION]);
          }
          else
          {
            Session.ErrorMessage += ". Incorrect info";
          }
        }
      }
      else
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERROR;
        Session.ErrorMessage = "Error Rollback: non found credit registers";
      }

      if (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK)
      {
        // TransactionId is null. Process ALL Rollback pending
        LCD_WebServer.InitExecuteBulkThread(Session.TransactionId);
      }

      return _response;
    } // RollbackResponse

    #endregion " Response methods "
  }
}
