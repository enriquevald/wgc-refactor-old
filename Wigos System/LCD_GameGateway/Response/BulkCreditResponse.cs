﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BulkCreditResponse.cs
//// 
////      DESCRIPTION: Bulk credit response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 29-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  [Serializable, XmlInclude(typeof(BulkCredit)), XmlRoot(ElementName = "response")]
  public class BulkCreditResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlArray(Order = 1, ElementName = "credits"), XmlArrayItem(ElementName = "credit")]
    public BulkCredit[] Credits { get; set; }
  }

  [Serializable, XmlRoot(ElementName = "credit")]
  public class BulkCredit
  {
    [XmlElementAttribute(Order = 0, ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "balance")]
    public String Balance { get; set; }

    [XmlElementAttribute(Order = 2, ElementName = "code")]
    public String Code { get; set; }
  }
}
