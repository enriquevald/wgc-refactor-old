﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: OfflineResponse.cs
//// 
////      DESCRIPTION: Offline response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 29-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "response")]
  public class OfflineResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(Order = 2, ElementName = "balance")]
    public String Balance { get; set; }

    [XmlElementAttribute(Order = 3, ElementName = "code")]
    public String Code { get; set; }
  }
}
