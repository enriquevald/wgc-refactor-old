﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LoginResponse.cs
//// 
////      DESCRIPTION: Login response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 23-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 23-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "response")]
  public class LoginResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "sessionid")]
    public String SessionId { get; set; }

    [XmlElementAttribute(Order = 2, ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(Order = 3, ElementName = "affiliatedid")]
    public String AffiliateId { get; set; }

    [XmlElementAttribute(Order = 4, ElementName = "nickname")]
    public String NickName { get; set; }

    [XmlElementAttribute(Order = 5, ElementName = "country")]
    public String Country { get; set; }

    [XmlElementAttribute(Order = 6, ElementName = "balancevisible")]
    public String BalanceVisible { get; set; }

    [XmlElementAttribute(Order = 7, ElementName = "balance")]
    public String Balance { get; set; }
  }
}
