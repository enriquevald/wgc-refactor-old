﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LogoutResponse.cs
//// 
////      DESCRIPTION: Logout response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 23-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 23-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "response")]
  public class LogoutResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "code")]
    public String Code { get; set; }
  }
}
