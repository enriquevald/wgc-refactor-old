﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LogoutResponse.cs
//// 
////      DESCRIPTION: Error response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 07-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-OCT-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "response")]
  public class ErrorResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "code")]
    public String Code { get; set; }

    [XmlElementAttribute(Order = 2, ElementName = "message")]
    public String Message { get; set; }
  }
}
