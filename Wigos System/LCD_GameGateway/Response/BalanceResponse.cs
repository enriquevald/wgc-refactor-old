﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BulkCreditResponse.cs
//// 
////      DESCRIPTION: Bulk credit response for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 29-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace LCD_GameGateway
{
  //[Serializable, XmlInclude(typeof(Account)), XmlRoot(ElementName = "response")]
  [Serializable, XmlRoot(ElementName = "response")]
  public class BalanceResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "sessionid")]
    public String SessionId { get; set; }

    [XmlArray(Order = 2, ElementName = "accounts"), XmlArrayItem(ElementName = "account")]
    public Account[] Accounts { get; set; }
  }

  [Serializable, XmlRoot(ElementName = "account")]
  public class Account
  {
    [XmlElementAttribute(Order = 0, ElementName = "real_balance")]
    public String RealBalance { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "currency")]
    public String Currency { get; set; }

    public Account()
    {
      this.RealBalance = "";
      this.Currency = "";
    }
  }
}
