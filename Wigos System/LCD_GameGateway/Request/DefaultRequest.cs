﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DefaultRequest.cs
//// 
////      DESCRIPTION: Default request for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 23-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 23-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "request")]
  public class DefaultRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "sessionid")]
    public String SessionId { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }
  }
}
