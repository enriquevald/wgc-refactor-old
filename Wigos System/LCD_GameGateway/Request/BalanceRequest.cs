﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: GameStartingRequest.cs
//// 
////      DESCRIPTION: Game starting request for Web Service
//// 
////           AUTHOR: Samuel González
//// 
////    CREATION DATE: 15-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 15-OCT-2015 SGB    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlRoot(ElementName = "request")]
  public class BalanceRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "sessionid")]
    public String SessionId { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }
  }
}