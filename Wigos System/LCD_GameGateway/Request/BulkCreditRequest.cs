﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BulkCreditRequest.cs
//// 
////      DESCRIPTION: Bulk credit request for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 23-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 23-SEP-2015 AMF    First release.
//// 15-JAN-2016 AMF    PBI 8267: Rollback
//// 10-MAR-2016 AMF    PBI 10452: Remove old Rollback Request
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlInclude(typeof(Credit)), XmlRoot(ElementName = "request")]
  public class BulkCreditRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "gameid")]
    public String GameId { get; set; }

    [XmlElementAttribute(ElementName = "gameinstanceid")]
    public String GameInstanceId { get; set; }

    [XmlElementAttribute(ElementName = "transaction")]
    public String Transaction { get; set; }

    [XmlArray(ElementName = "credits"), XmlArrayItem(ElementName = "credit")]
    public Credit[] Credits { get; set; }

    [XmlElementAttribute(ElementName = "currency")]
    public String Currency { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }

  }

  [Serializable, XmlRoot(ElementName = "credit")]
  public class Credit
  {
    [XmlElementAttribute(ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(ElementName = "betid")]
    public String BetId { get; set; }

    [XmlElementAttribute(ElementName = "amount")]
    public String Amount { get; set; }

    [XmlElementAttribute(ElementName = "jackpot")]
    public String Jackpot { get; set; }
  }

  public class AccountCredit
  {
    public Decimal TotalPrizes { get; set; }
    public Decimal TotalBets { get; set; }
    public Decimal TotalJackpot { get; set; }
    public Int32 TotalNumPrizes { get; set; }
    public Int32 TotalNumBets { get; set; }
    public LCD_Session Session { get; set; }

    public AccountCredit(Decimal TotalAmount, Decimal TotalJackpot, LCD_Session Session)
    {
      if (Session.TransactionType == GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING)
      {
        this.TotalBets = TotalAmount * -1;
        this.TotalNumBets = -1;
      }
      else
      {
        this.TotalPrizes = TotalAmount + TotalJackpot;
        this.TotalJackpot = TotalJackpot;
        this.TotalNumPrizes = 1;
      }

      this.Session = Session;
    }
  }
}
