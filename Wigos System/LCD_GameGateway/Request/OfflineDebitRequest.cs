﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: OfflineDebitRequest.cs
//// 
////      DESCRIPTION: Offline Debit request for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 29-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlInclude(typeof(DebitBet)), XmlRoot(ElementName = "request")]
  public class OfflineDebitRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(ElementName = "gameid")]
    public String GameId { get; set; }

    [XmlElementAttribute(ElementName = "gameinstanceid")]
    public String GameInstanceId { get; set; }

    [XmlElementAttribute(ElementName = "amount")]
    public String Amount { get; set; }

    [XmlElementAttribute(ElementName = "currency")]
    public String Currency { get; set; }

    [XmlElementAttribute(ElementName = "transaction")]
    public String Transaction { get; set; }

    [XmlArray(ElementName = "bets"), XmlArrayItem(ElementName = "bet")]
    public DebitBet[] Bets { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }
  }
}

