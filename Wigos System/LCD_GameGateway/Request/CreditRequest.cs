﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CreditRequest.cs
//// 
////      DESCRIPTION: Credit request for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 23-SEP-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 23-SEP-2015 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlInclude(typeof(CreditBet)), XmlRoot(ElementName = "request")]
  public class CreditRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "sessionid")]
    public String SessionId { get; set; }

    [XmlElementAttribute(ElementName = "gameid")]
    public String GameId { get; set; }

    [XmlElementAttribute(ElementName = "gameinstanceid")]
    public String GameInstanceId { get; set; }

    [XmlElementAttribute(ElementName = "amount")]
    public String Amount { get; set; }

    [XmlElementAttribute(ElementName = "currency")]
    public String Currency { get; set; }

    [XmlElementAttribute(ElementName = "transaction")]
    public String Transaction { get; set; }

    [XmlElementAttribute(ElementName = "bonus")]
    public String Bonus { get; set; }

    [XmlArray(ElementName = "bets"), XmlArrayItem(ElementName = "bet")]
    public CreditBet[] Bets { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }

  }

  [Serializable, XmlRoot(ElementName = "bet")]
  public class CreditBet
  {
    [XmlElementAttribute(ElementName = "betid")]
    public String BetId { get; set; }

    [XmlElementAttribute(ElementName = "amount")]
    public String Amount { get; set; }

    [XmlElementAttribute(ElementName = "jackpot")]
    public String Jackpot { get; set; }
  }
}