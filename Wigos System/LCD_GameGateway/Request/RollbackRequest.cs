﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: RollbackRequest.cs
//// 
////      DESCRIPTION: Rollback request for Web Service
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 09-MAR-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 09-MAR-2016 AMF    First release.
////------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using WSI.Common;

namespace LCD_GameGateway
{
  [Serializable, XmlInclude(typeof(DebitBet)), XmlRoot(ElementName = "request")]
  public class RollbackRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "gameid")]
    public String GameId { get; set; }

    [XmlElementAttribute(ElementName = "gameinstanceid")]
    public String GameInstanceId { get; set; }

    [XmlElementAttribute(ElementName = "currency")]
    public String Currency { get; set; }

    [XmlArray(ElementName = "credits"), XmlArrayItem(ElementName = "credit")]
    public RollbackCredit[] Credits { get; set; }

    [XmlElementAttribute(ElementName = "authentication")]
    public Authentication Authentication { get; set; }

  }

  [Serializable, XmlRoot(ElementName = "credit")]
  public class RollbackCredit
  {
    [XmlElementAttribute(ElementName = "userid")]
    public String UserId { get; set; }

    [XmlElementAttribute(ElementName = "transaction")]
    public String Transaction { get; set; }

    [XmlElementAttribute(ElementName = "numberofbets")]
    public String NumberOfBets { get; set; }

    [XmlElementAttribute(ElementName = "amount")]
    public String Amount { get; set; }
  }

  public class RollbackCredits
  {
    public Decimal Amount { get; set; }
    public Int32 NumberOfBets { get; set; }
    public LCD_Session Session { get; set; }

    public RollbackCredits(Decimal TotalAmount, Int32 NumBets, LCD_Session Session)
    {
      this.Amount = TotalAmount * -1;
      this.NumberOfBets = NumBets * -1;
      this.Session = Session;
    }
  }
}
