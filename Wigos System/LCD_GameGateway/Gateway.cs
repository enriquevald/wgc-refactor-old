﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Gateway.cs
//// 
////      DESCRIPTION: Gateway
//// 
////           AUTHOR: Samuel González
//// 
////    CREATION DATE: 14-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 14-OCT-2015 SGB    First release.
//// 11-DIC-2015 JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
//// 12-JAN-2016 AMF    PBI 8268: Debit Request only with play session active
//// 13-JAN-2016 AMF    PBI 8267: Rollback
//// 18-JAN-2016 AMF    PBI 8260: Traceability
//// 16-FEB-2016 AMF    PBI 9434: Award prizes
//// 09-MAR-2016 AMF    PBI 10452: New Rollback Request
//// 17-MAR-2016 FJC    PBI: LCD: Cambios varios
////------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Xml;
using System.IO;

namespace LCD_GameGateway
{
  public class Gateway
  {

    #region " Constants "

    public const String PLAY_SESSIONS_TYPE = "GPS_TYPE";
    public const String PLAY_SESSIONS_STATUS = "GPS_STATUS";
    public const String PLAY_SESSIONS_STARTED = "GPS_STARTED";
    public const String PLAY_SESSIONS_CREDIT_AMOUNT = "GPS_CREDIT_AMOUNT";
    public const String PLAY_SESSIONS_DEBIT_AMOUNT = "GPS_DEBIT_AMOUNT";
    public const String PLAY_SESSIONS_DIFF_BALANCE = "GPS_DIFF_AMOUNT";
    public const String PLAY_SESSIONS_LAST_UPDATED = "GPS_LAST_UPDATED";

    private const String BULK_USER_ID = "USER_ID";
    private const String BULK_BETS = "BETS";
    private const String BULK_TOTAL_BETS = "TOTAL_BETS";
    private const String BULK_NUM_BETS = "NUM_BETS";
    private const String BULK_TOTAL_PRIZES = "TOTAL_PRIZES";
    private const String BULK_JACKPOT = "JACKPOT";
    private const String BULK_NUM_PRIZES = "NUM_PRIZES";
    private const String BULK_GAME = "GAME";
    private const String BULK_GAME_INSTANCE = "GAME_INSTANCE";
    private const String BULK_PARTNER = "PARTNER";
    public const String BULK_TRANSACTION = "TRANSACTION";
    private const String BULK_EXISTS = "EXISTS";

    #endregion " Constants "

    #region " Public Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Insert WCP commands GameGatewayGetCredit for one terminal
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean InsertGamegatewayCommand(ref LCD_Session Session, SqlTransaction Trx)
    {
      Int64 _wcp_id;

      if (!InsertGatewayCommand(Session.TerminalId, out _wcp_id, Trx))
      {
        return false;
      }

      Session.WCPId = _wcp_id;

      return true;
    } // InsertGamegatewayCommand
    public static Boolean InsertGamegatewayCommand(Int64 TerminalId, out Int64 WCPId, SqlTransaction Trx)
    {
      if (!InsertGatewayCommand(TerminalId, out WCPId, Trx))
      {
        return false;
      }

      return true;
    } // InsertGamegatewayCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Update WCP commands
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - UpdateParameter
    //          - CreditStatus
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    public static Boolean UpdateGamegatewayCommand(LCD_Session Session, Boolean UpdateParameter, GetCreditStatus CreditStatus, SqlTransaction Trx)
    {
      return UpdateGamegatewayCommandInternal(Session.WcpParameter, Session.WCPId, UpdateParameter, CreditStatus, Trx);
    } // UpdateGamegatewayCommand
    public static Boolean UpdateGamegatewayCommand(String WcpParameter, Int64 WCPId, Boolean UpdateParameter, GetCreditStatus CreditStatus, SqlTransaction Trx)
    {
      return UpdateGamegatewayCommandInternal(WcpParameter, WCPId, UpdateParameter, CreditStatus, Trx);
    } // UpdateGamegatewayCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Get response from GAMEGATEWAY_COMMAND_MESSAGES for DebitRequest or SetBalance (FB)
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    public static Boolean GetGamegatewayCommandResponse(LCD_Session Session)
    {
      return GetGamegatewayCommandResponseInternal(Session.WcpParameter, Session.WCPId, Session.CreditTimeOut);
    } // UpdateGamegatewayCommand
    public static Boolean GetGamegatewayCommandResponse(String WcpParameter, Int64 WCPId, Int64 CreditTimeOut)
    {
      return GetGamegatewayCommandResponseInternal(WcpParameter, WCPId, CreditTimeOut);
    } // UpdateGamegatewayCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Dictionary of bulk credits to datatable
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Dictionary Credits
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - DataTable 
    //
    //   NOTES:
    //
    public static DataTable DictionaryBulkToDataTable(Dictionary<Int64, AccountCredit> Credits)
    {
      DataTable _dt_credits;
      DataRow _dr_credit;

      _dt_credits = new DataTable();
      _dt_credits.Columns.Add(BULK_USER_ID, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_BETS, Type.GetType("System.String"));
      _dt_credits.Columns.Add(BULK_TOTAL_BETS, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_NUM_BETS, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_TOTAL_PRIZES, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_JACKPOT, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_NUM_PRIZES, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_GAME, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_GAME_INSTANCE, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_PARTNER, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_TRANSACTION, Type.GetType("System.String"));
      _dt_credits.Columns.Add(BULK_EXISTS, Type.GetType("System.Boolean"));

      try
      {
        foreach (AccountCredit _credit in Credits.Values)
        {
          _dr_credit = _dt_credits.NewRow();
          _dr_credit[BULK_USER_ID] = _credit.Session.AccountId;
          _dr_credit[BULK_BETS] = GetBetsByAccountIdString(_credit.Session.Bets, _credit.Session.AccountId.ToString());
          _dr_credit[BULK_TOTAL_BETS] = _credit.TotalBets;
          _dr_credit[BULK_NUM_BETS] = _credit.TotalNumBets;
          _dr_credit[BULK_TOTAL_PRIZES] = _credit.TotalPrizes;
          _dr_credit[BULK_JACKPOT] = _credit.TotalJackpot;
          _dr_credit[BULK_NUM_PRIZES] = _credit.TotalNumPrizes;
          _dr_credit[BULK_GAME] = _credit.Session.GameId;
          _dr_credit[BULK_GAME_INSTANCE] = _credit.Session.GameInstanceId;
          _dr_credit[BULK_PARTNER] = _credit.Session.PartnerId;
          _dr_credit[BULK_TRANSACTION] = _credit.Session.TransactionId;

          _dt_credits.Rows.Add(_dr_credit);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_credits;
    } // DictionaryBulkToDataTable

    //------------------------------------------------------------------------------
    // PURPOSE: Dictionary of rollback credits to datatable
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Dictionary Credits
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - DataTable 
    //
    //   NOTES:
    //
    public static DataTable DictionaryRollbackToDataTable(Dictionary<String, RollbackCredits> Credits)
    {
      DataTable _dt_credits;
      DataRow _dr_credit;

      _dt_credits = new DataTable();
      _dt_credits.Columns.Add(BULK_USER_ID, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_BETS, Type.GetType("System.String"));
      _dt_credits.Columns.Add(BULK_TOTAL_BETS, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_NUM_BETS, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_TOTAL_PRIZES, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_JACKPOT, Type.GetType("System.Decimal"));
      _dt_credits.Columns.Add(BULK_NUM_PRIZES, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_GAME, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_GAME_INSTANCE, Type.GetType("System.Int64"));
      _dt_credits.Columns.Add(BULK_PARTNER, Type.GetType("System.Int32"));
      _dt_credits.Columns.Add(BULK_TRANSACTION, Type.GetType("System.String"));
      _dt_credits.Columns.Add(BULK_EXISTS, Type.GetType("System.Boolean"));

      try
      {
        foreach (RollbackCredits _credit in Credits.Values)
        {
          _dr_credit = _dt_credits.NewRow();
          _dr_credit[BULK_USER_ID] = _credit.Session.AccountId;
          _dr_credit[BULK_BETS] = GetBetsByTransactionString(_credit.Session.Bets, _credit.Session.TransactionId);
          _dr_credit[BULK_TOTAL_BETS] = _credit.Amount;
          _dr_credit[BULK_NUM_BETS] = _credit.NumberOfBets;
          _dr_credit[BULK_TOTAL_PRIZES] = 0;
          _dr_credit[BULK_JACKPOT] = 0;
          _dr_credit[BULK_NUM_PRIZES] = 0;
          _dr_credit[BULK_GAME] = _credit.Session.GameId;
          _dr_credit[BULK_GAME_INSTANCE] = _credit.Session.GameInstanceId;
          _dr_credit[BULK_PARTNER] = _credit.Session.PartnerId;
          _dr_credit[BULK_TRANSACTION] = _credit.Session.TransactionId;

          _dt_credits.Rows.Add(_dr_credit);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_credits;
    } // DictionaryRollbackToDataTable

    //------------------------------------------------------------------------------
    // PURPOSE: Get terminals with the GameGateway funcionality active
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Session
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    public static void GetGameGatewayTerminals(ref LCD_Session Session, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        Session.WcpTerminals = new DataTable();

        _sb.AppendLine("    SELECT   TG_TERMINAL_ID                                                                                   ");
        _sb.AppendLine("      FROM   TERMINAL_GROUPS                                                                                  ");
        _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TG_TERMINAL_ID        AND TE_CURRENT_PLAY_SESSION_ID IS NOT NULL   ");
        _sb.AppendLine("INNER JOIN   ACCOUNTS  ON AC_ACCOUNT_ID  = TE_CURRENT_ACCOUNT_ID AND (AC_TYPE <> @pVirC AND AC_TYPE <> @pVirT)");
        _sb.AppendLine("     WHERE   TG_ELEMENT_TYPE = @pElementType                                                                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.GAME_GATEWAY;
          _sql_cmd.Parameters.Add("@pVirC", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;
          _sql_cmd.Parameters.Add("@pVirT", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Session.WcpTerminals);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // GetGameGatewayTerminals

    #endregion " Public Methods "

    #region " Private Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Insert WCP commands GameGatewayGetCredit for one terminal
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    private static Boolean InsertGatewayCommand(Int64 TerminalId, out Int64 WCPId, SqlTransaction Trx)
    {
      StringBuilder _sb; ;
      SqlParameter _param;

      WCPId = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO GAMEGATEWAY_COMMAND_MESSAGES ");
        _sb.AppendLine("             (                            ");
        _sb.AppendLine("               GCM_TERMINAL_ID            ");
        _sb.AppendLine("             , GCM_CODE                   ");
        _sb.AppendLine("             , GCM_TYPE                   ");
        _sb.AppendLine("             , GCM_STATUS                 ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("     VALUES  (                            ");
        _sb.AppendLine("               @pTerminalId               ");
        _sb.AppendLine("             , @pCode                     ");
        _sb.AppendLine("             , @pType                     ");
        _sb.AppendLine("             , 0                          ");
        _sb.AppendLine("             )                            ");
        _sb.AppendLine("                                          ");
        _sb.AppendLine(" SET @pWCPId = SCOPE_IDENTITY()           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pCode", SqlDbType.Int).Value = WCP_CommandCode.GameGatewayGetCredit;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = GameGateway.TYPE_WCP.DEBIT;

          _param = _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            WCPId = (Int64)_param.Value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGatewayCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Insert WCP commands GameGatewayGetCredit for one terminal internal
    //
    //  PARAMS:
    //     - INPUT:
    //          - Session
    //          - SqlTransaction
    //
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - True or False
    //
    private static Boolean UpdateGamegatewayCommandInternal(String WcpParameter, Int64 WcpId, Boolean UpdateParameter, GetCreditStatus CreditStatus, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_COMMAND_MESSAGES  ");
        _sb.AppendLine("    SET   GCM_RESPONSE  = @pResponse    ");
        if (UpdateParameter)
        {
          _sb.AppendLine("        , GCM_PARAMETER = @pParameter ");
        }
        _sb.AppendLine("  WHERE   GCM_ID        = @pWCPId       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = CreditStatus;

          if (UpdateParameter)
          {
            _sql_cmd.Parameters.Add("@pParameter", SqlDbType.NVarChar).Value = WcpParameter;
          }

          _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt).Value = WcpId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGamegatewayCommandInternal

    //------------------------------------------------------------------------------
    // PURPOSE: Get response from GAMEGATEWAY_COMMAND_MESSAGES for DebitRequest internal
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LCD_Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True or False
    // 
    //   NOTES:
    //
    private static Boolean GetGamegatewayCommandResponseInternal(String WcpParameter, Int64 WcpId, Int64 CreditTimeOut)
    {
      StringBuilder _sb;
      Object _obj;
      DateTime _init_process = WGDB.Now;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   1                             ");
        _sb.AppendLine("   FROM   GAMEGATEWAY_COMMAND_MESSAGES  ");
        _sb.AppendLine("  WHERE   GCM_ID       = @pWCPId        ");
        _sb.AppendLine("    AND   GCM_RESPONSE = @pResponse     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pWCPId", SqlDbType.BigInt).Value = WcpId;
            _sql_cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = GetCreditStatus.UPDATED;

            while (_init_process.AddSeconds(CreditTimeOut) > WGDB.Now)
            {
              _obj = _sql_cmd.ExecuteScalar();

              if (_obj != null && _obj != DBNull.Value)
              {
                return true;
              }

              Thread.Sleep(500);
            }

            Gateway.UpdateGamegatewayCommandInternal(WcpParameter, WcpId, false, GetCreditStatus.CANCELLED, _db_trx.SqlTransaction);

            _db_trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetGamegatewayCommandResponse


    //------------------------------------------------------------------------------
    // PURPOSE: Get bets by accountid
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //          - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - String
    // 
    //   NOTES:
    //
    private static String GetBetsByAccountIdString(String Xml, String AccountId)
    {
      XmlDocument _doc;
      XmlNodeList _nodes;

      try
      {
        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _nodes = _doc.SelectNodes(String.Format("/credits/credit[userid != '{0}']", AccountId));

        foreach (XmlNode _node in _nodes)
        {
          _node.ParentNode.RemoveChild(_node);
        }

        return _doc.InnerXml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // GetBetsByAccountIdString

    //------------------------------------------------------------------------------
    // PURPOSE: Get bets by transactionid
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Xml
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - String
    // 
    //   NOTES:
    //
    private static String GetBetsByTransactionString(String Xml, String TransactionId)
    {
      XmlDocument _doc;
      XmlNodeList _nodes;

      try
      {
        _doc = new XmlDocument();
        _doc.Load(new StringReader(Xml));
        _nodes = _doc.SelectNodes(String.Format("/credits/credit[transaction != '{0}']", TransactionId));

        foreach (XmlNode _node in _nodes)
        {
          _node.ParentNode.RemoveChild(_node);
        }

        return _doc.InnerXml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // GetBetsByTransactionString

    #endregion " Private Methods "

    #region " Manage Gateway Bulk Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Batch insert in table gamegateway_bets
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Credits
    //        - TransactionType
    //
    //      - OUTPUT:
    //        - DataRow[]
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    public static Boolean InsertGameGatewayBatch(DataTable Credits, GameGateway.TRANSACTION_TYPE TransactionType, out DataRow[] Rows)
    {
      StringBuilder _sb;
      SqlParameter _param;

      Rows = null;

      try
      {
        _sb = GameGateway.GetInsertBetsStringBuilder(true, TransactionType == GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (TransactionType == GameGateway.TRANSACTION_TYPE.PRIZE || TransactionType == GameGateway.TRANSACTION_TYPE.PRIZE_PENDING)
            {
              _sql_cmd.Parameters.Add("@pTransactionTypeExists", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.PRIZE;
              _sql_cmd.Parameters.Add("@pTransactionTypePending", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.PRIZE_PENDING;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pTransactionTypeBet", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.BET;
              _sql_cmd.Parameters.Add("@pTransactionTypeExists", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.BET_ROLLBACK;
              _sql_cmd.Parameters.Add("@pTransactionTypePending", SqlDbType.Int).Value = GameGateway.TRANSACTION_TYPE.BET_ROLLBACK_PENDING;
            }
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).SourceColumn = BULK_GAME;
            _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).SourceColumn = BULK_GAME_INSTANCE;
            _sql_cmd.Parameters.Add("@pTransactionType", SqlDbType.Int).Value = TransactionType;
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar, 40).SourceColumn = BULK_TRANSACTION;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = BULK_USER_ID;
            _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).SourceColumn = BULK_PARTNER;
            _sql_cmd.Parameters.Add("@pEGMTerminalId", SqlDbType.Int).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = WGDB.Now;
            _sql_cmd.Parameters.Add("@pBets", SqlDbType.Xml).SourceColumn = BULK_BETS;
            _sql_cmd.Parameters.Add("@pNumBets", SqlDbType.Int).SourceColumn = BULK_NUM_BETS;
            _sql_cmd.Parameters.Add("@pTotalBet", SqlDbType.Money).SourceColumn = BULK_TOTAL_BETS;
            _sql_cmd.Parameters.Add("@pNumPrizes", SqlDbType.Int).SourceColumn = BULK_NUM_PRIZES;
            _sql_cmd.Parameters.Add("@pTotalPrize", SqlDbType.Money).SourceColumn = BULK_TOTAL_PRIZES;
            _sql_cmd.Parameters.Add("@pJackpotPrize", SqlDbType.Money).SourceColumn = BULK_JACKPOT;
            _sql_cmd.Parameters.Add("@pRelatedPsId", SqlDbType.BigInt).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pRelatedMvId", SqlDbType.BigInt).Value = DBNull.Value;

            _param = _sql_cmd.Parameters.Add("@ErrorRegister", SqlDbType.Bit);
            _param.Direction = ParameterDirection.Output;
            _param.SourceColumn = BULK_EXISTS;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

              if (_sql_da.Update(Credits) == Credits.Rows.Count)
              {
                Rows = Credits.Select(BULK_EXISTS + " = 1");

                if (Rows.Length == 0)
                {
                  if (!GeneralParam.GetBoolean("GameGateway", "AwardPrizes", true))
                  {
                    if (!UpdateGameGatewayGamesPending(Credits, _db_trx.SqlTransaction))
                    {
                      return false;
                    }
                  }

                  _db_trx.Commit();

                  return true;
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertGameGatewayBatch

    #endregion " Manage Gateway Bulk Methods "

    #region " Manage Gateway Games Methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Update table gamegateway_game_instances: Set ggi_finished
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Request
    //        - Session
    //        - DoUpdate
    //        - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //
    //   NOTES:
    //
    public static void UpdateNonClosedGames(String GameId, ref LCD_Session Session, SqlTransaction Trx)
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   GAMEGATEWAY_GAME_INSTANCES                       ");
        _sb.AppendLine("   SET   GGI_FINISHED   = DATEADD(MINUTE, 15, GGI_STARTS) ");
        _sb.AppendLine(" WHERE   GGI_PARTNER_ID = @pPartnerId                     ");
        _sb.AppendLine("   AND   GGI_GAME_ID    = @pGameId                        ");
        _sb.AppendLine("   AND   GGI_FINISHED IS NULL                             ");
        _sb.AppendLine("   AND   DATEADD(MINUTE, 15, GGI_STARTS) < GETDATE()      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = GameId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;
          _sql_cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    }//UpdateNonClosedGames

    //------------------------------------------------------------------------------
    // PURPOSE: Update jackpot in GameGateway_game_instances
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //          - Jackpot
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    public static Boolean UpdateGameGatewayGameInstancesJackpot(LCD_Session Session, Decimal Jackpot, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAME_INSTANCES              ");
        _sb.AppendLine("    SET   GGI_JACKPOT          = @pJackpot        ");
        _sb.AppendLine("  WHERE   GGI_GAME_INSTANCE_ID = @pGameInstanceId ");
        _sb.AppendLine("    AND   GGI_PARTNER_ID       = @pPartnerId      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pJackpot", SqlDbType.Money).Value = Jackpot;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGameGatewayGameInstancesJackpot

    //------------------------------------------------------------------------------
    // PURPOSE: Validate GameInstanceId
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Session
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static Boolean ValidateGameGatewayGames(LCD_Session Session)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   1                                        ");
        _sb.AppendLine("   FROM   GAMEGATEWAY_GAME_INSTANCES               ");
        _sb.AppendLine("  WHERE   GGI_GAME_INSTANCE_ID  = @pGameInstanceId ");
        _sb.AppendLine("    AND   GGI_GAME_ID          <> @pGameId         ");
        _sb.AppendLine("    AND   GGI_PARTNER_ID        = @pPartnerId      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).Value = Session.GameInstanceId;
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).Value = Session.GameId;
            _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).Value = Session.PartnerId;

            _obj = _sql_cmd.ExecuteScalar();

            if (_obj == null)
            {
              return true;
            }
            else if (_obj != DBNull.Value)
            {
              return (Int32)_obj == 1 ? false : true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ValidateGameGatewayGames

    //------------------------------------------------------------------------------
    // PURPOSE: Batch Update execution tables gameGateway_games and GameGateway_game_instances, columns pending
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Credits
    //          - SqlTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False
    //   NOTES:
    //
    private static Boolean UpdateGameGatewayGamesPending(DataTable Credits, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        // Change RowState
        foreach (DataRow _row in Credits.Rows)
        {
          _row.SetAdded();
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAME_INSTANCES                            ");
        _sb.AppendLine("    SET   GGI_PENDING          = GGI_PENDING     + @pPending    ");
        _sb.AppendLine("        , GGI_NUM_PENDING      = GGI_NUM_PENDING + @pNumPending ");
        _sb.AppendLine("  WHERE   GGI_GAME_INSTANCE_ID = @pGameInstanceId               ");
        _sb.AppendLine("    AND   GGI_PARTNER_ID       = @pPartnerId                    ");
        _sb.AppendLine("                                                                ");
        _sb.AppendLine(" UPDATE   GAMEGATEWAY_GAMES                                     ");
        _sb.AppendLine("    SET   GG_PENDING      = GG_PENDING     + @pPending          ");
        _sb.AppendLine("        , GG_NUM_PENDING  = GG_NUM_PENDING + @pNumPending       ");
        _sb.AppendLine("  WHERE   GG_GAME_ID      = @pGameId                            ");
        _sb.AppendLine("    AND   GG_PARTNER_ID   = @pPartnerId                         ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPending", SqlDbType.Money).SourceColumn = BULK_TOTAL_PRIZES;
          _sql_cmd.Parameters.Add("@pNumPending", SqlDbType.Int).SourceColumn = BULK_NUM_PRIZES;
          _sql_cmd.Parameters.Add("@pGameInstanceId", SqlDbType.BigInt).SourceColumn = BULK_GAME_INSTANCE;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.BigInt).SourceColumn = BULK_GAME;
          _sql_cmd.Parameters.Add("@pPartnerId", SqlDbType.Int).SourceColumn = BULK_PARTNER;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            if (_sql_da.Update(Credits) == Credits.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateGameGatewayGamesPending

    #endregion " Manage Gateway Games Methods "

  }
}
