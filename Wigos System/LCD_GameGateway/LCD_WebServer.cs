﻿////------------------------------------------------------------------------------
//// Copyright © 2015 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: LCD_WebServer.cs
//// 
////      DESCRIPTION: LCD web server
//// 
////           AUTHOR: Alberto Marcos
//// 
////    CREATION DATE: 06-OCT-2015
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-OCT-2015 AMF    First release.
//// 21-JAN-2016 AMF    PBI 8267: WorkerPool
//// 16-FEB-2016 AMF    PBI 9434: Award prizes
////------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using WSI.Common;

namespace LCD_GameGateway
{
  public class LCD_WebServer
  {

    #region " Members "

    static HttpListener m_listener;
    static Thread m_thread;
    static Thread m_thread_bulk;
    static String m_uriaddress = GeneralParam.GetString("GameGateway", "Uri", "http://+:7777/");
    static WorkerPool m_worker_pool;

    #endregion " Members "

    #region " Public methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Init webserver
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static Boolean Init()
    {
      if (!HttpListener.IsSupported)
      {
        Log.Error("HttpListener Not Supported");

        return false;
      }

      m_worker_pool = new WorkerPool("GameGateway_Listener", GeneralParam.GetInt32("GameGateway", "NumWorkers", 4, 4, 20));

      m_thread = new Thread(ListenerThread);
      m_thread.Start();

      m_thread_bulk = new Thread(BulkThread);
      m_thread_bulk.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Init execute bulk thread
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True or False 
    //
    //   NOTES:
    //
    public static void InitExecuteBulkThread(String TransactionId)
    {

      m_thread_bulk = new Thread(ExecuteBulkThread);
      m_thread_bulk.Start(TransactionId);

    } // InitExecuteBulkThread

    #endregion " Public methods "

    #region " Private methods "

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to process bulk request periodically
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void BulkThread()
    {
      while (true)
      {
        try
        {
          Thread.Sleep(60 * 1000);

          if (Services.IsPrincipal("WCP"))
          {
            if (!GameGateway.ManageGameGatewayBulk())
            {
              Log.Warning("LCD_GameGateway. Exception in function BulkThread->ManageGameGatewayBulk");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // BulkThread

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to process bulk request after insert bulk registers
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TransactionId
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void ExecuteBulkThread(Object TransactionId)
    {
      try
      {
        if (!GameGateway.ManageGameGatewayBulk((String)TransactionId))
        {
          Log.Warning("LCD_GameGateway. Exception in function ExecuteBulkThread->ManageGameGatewayBulk. TransactionId: [" + (String)TransactionId + "]");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // ExecuteBulkThread

    //------------------------------------------------------------------------------
    // PURPOSE: Thread to listener requests
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private static void ListenerThread()
    {
      try
      {
        if (!HttpListener.IsSupported)
        {
          Log.Error("HttpListener Not Supported");

          return;
        }

        if (m_listener == null)
        {
          m_listener = new HttpListener();
          m_listener.Prefixes.Add(m_uriaddress);
          m_listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
          m_listener.Start();

          Log.Message("HttpListener GameGateway Start. Uri: " + m_uriaddress);
        }

        while (true)
        {
          HttpListenerContext _ctx;

          _ctx = m_listener.GetContext();

          m_worker_pool.EnqueueTask(new RequestTask(_ctx));
        }
      }
      catch (HttpListenerException _ex)
      {
        Log.Error("Check that the current user has administrator permissions.");
        Log.Message("-------------------------------------");
        Log.Exception(_ex);
        Log.Message("-------------------------------------");
      }
    } // ListenerThread

    #endregion " Private methods "

  } // LCD_WebServer

  public class RequestTask : Task
  {
    private HttpListenerContext m_ctx;
    private LCD_ProcessRequest m_process_request;

    public RequestTask(HttpListenerContext Ctx)
    {
      m_ctx = Ctx;
    } // RequestTask

    public override void Execute()
    {
      try
      {
        m_process_request = new LCD_ProcessRequest(m_ctx);
        m_process_request.ProcessRequest();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Execute
  } // RequestTask

  public class Utf8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get { return Encoding.UTF8; }
    }
  } // Utf8StringWriter

} // LCD_GameGateway
