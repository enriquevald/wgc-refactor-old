﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WSI.WWP_Server")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WIN SYSTEMS INTERNATIONAL, LTD")]
[assembly: AssemblyProduct("WSI.WWP_Server")]
[assembly: AssemblyCopyright("Copyright © WIN SYSTEMS INTERNATIONAL, LTD. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("00.001")]
[assembly: AssemblyFileVersion("00.001")]
[assembly: GuidAttribute("cb7fb0b1-bf0a-41d4-9d3a-d00af7737806")]
