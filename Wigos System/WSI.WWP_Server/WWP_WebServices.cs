﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WWP_WebServices.cs
//
//   DESCRIPTION: WWP_WebServices class
//
//        AUTHOR: Joaquin Calero
// 
// CREATION DATE: 21-GEN-2016

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-GEN-2016 JCA    First version.
// 26-FEB-2016 JCA    PBI 8272: WebService CANJES SPACE:  Craer fichero configuracion local.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using WIN.WigosService;
using WSI.Common;
using WSI.WWP_CenterService.WigosService;

namespace WSI.WWP_CenterService
{
  class WWP_WebServices
  {
    /// <summary>
    /// Initialize WigosService
    /// </summary>
    public static void Init()
    {
      try
      {
        Uri _base_address;
        _base_address = GetBaseAdress();
        // Create the ServiceHost.
        WigosService.WigosService _ws = new WigosService.WigosService();

        ServiceHost host = new ServiceHost(_ws, _base_address);

        // Enable metadata publishing.
        ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
        smb.HttpGetEnabled = true;
        smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
        host.Description.Behaviors.Add(smb);

        // Open the ServiceHost to start listening for messages. Since
        // no endpoints are explicitly configured, the runtime will create
        // one endpoint per base address for each service contract implemented
        // by the service.
        host.Open();

        Log.Message("The service [WigosService] is ready at " + _base_address);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("The service [WigosService] cant run, check configuration!!!!");
      }
    }

    private static Uri GetBaseAdress()
    {
      Uri _base_address;
      _base_address = null;

      try
      {
        if (System.IO.File.Exists("WigosService.cfg"))
        {
          _base_address = new Uri(System.IO.File.ReadAllText("WigosService.cfg"));
      }
      }
      catch
      {
        Log.Error("Error in WigosService.cfg, take URL from GeneralParam");
      }

      try
      {
        if (_base_address == null)
        {
          _base_address = new Uri(GeneralParam.GetString("PlayerTracking.ExternalLoyaltyProgram", "WebService.Uri"));
        }
      }
      catch
      {
        Log.Error("Incorrect GP: PlayerTracking.ExternalLoyaltyProgram - WebService.Uri");
      }

      return _base_address;
    }
  }
}
