//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_ProcessSiteRequest.cs
// 
//   DESCRIPTION: Process for Table message.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 28-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-FEB-2013 ACC    First release.
// 28-MAR-2013 DDM    Fixed Bug #678.
// 05-DIC-2014 DCS    Added Task Download Lcd Messages
// 14-JAN-2015 DCS    Added Task Upload Handpays
// 17-MAR-2015 ANM    Added Task Upload Sells & buys
// 31-MAR-2017 MS     WIGOS-411 Creditlines
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;

namespace WSI.WWP_Server
{
  class ProcessSiteRequest
  {
    public static void Table(WWP_Message WwpRequest, WWP_Message WwpResponse, SqlTransaction SqlTrx)
    {
      WWP_MsgTable _request;
      WWP_MsgHeader _request_header;
      WWP_MsgTableReply _response;
      WWP_MsgHeader _response_header;
      TYPE_MULTISITE_SITE_TASKS _task;
      Int64 _site_sequence;

      _request = (WWP_MsgTable)WwpRequest.MsgContent;
      _request_header = (WWP_MsgHeader)WwpRequest.MsgHeader;
      _response = (WWP_MsgTableReply)WwpResponse.MsgContent;
      _response_header = (WWP_MsgHeader)WwpResponse.MsgHeader;

      _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;
      _task = TYPE_MULTISITE_SITE_TASKS.Unknown;
      _site_sequence = -1; // -1 is dummy value..

      try
      {
        switch (_request.Type)
        {
          case WWP_TableTypes.TABLE_TYPE_CONFIGURATION:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update / reply configuration
              //
              if (!MultiSite_CenterConfiguration.Update_Configuration(_request_header.SiteId, _request.DataSet, out _ds_reply))
              {
                Log.Error(" Calling Update_Configuration. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_POINTS_ACCOUNT_MOVEMENTS:
            {
              DataTable _dt_reply;
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update accounts use _dr_reply
              //              
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterPoints.Update_PointsAccountMovements(_request_header.SiteId, _dt_request, out _dt_reply))
              {
                Log.Error("Calling Update_PointsAccountMovements. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet();
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_POINTS:
            {
              DataTable _dt_reply;

              //
              // Switch to running the current task
              //
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if ((Int32)_request.DataSet.Tables[0].Rows[0][0] > 0)
              {
                _task = TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site;
              }
              else
              {
                _task = TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All;
              }
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Account Points 
              //
              if (!MultiSite_CenterPoints.Read_PendingAccountPoints(_request_header.SiteId,
                                                                    ((Int32)_request.DataSet.Tables[0].Rows[0][0]) == _request_header.SiteId,
                                                                    (Int64)_request.DataSet.Tables[0].Rows[0][1],         // SequenceId
                                                                    out _dt_reply))
              {
                Log.Error("Calling Read_PendingAccountPoints. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_ACCOUNT_PERSONAL_INFO:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update accounts use _dt_request
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterAccounts.Update_PersonalInfo(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_PersonalInfo. Site: " + _request_header.SiteId.ToString());

                return;
              }

              // REPLY OK
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PERSONAL_INFO:
            {
              DataTable _dt_reply;

              //
              // Switch to running the current task
              //
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if ((Int32)_request.DataSet.Tables[0].Rows[0][0] > 0)
              {
                _task = TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site;
              }
              else
              {
                _task = TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All;
              }
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Accounts 
              //
              if (!MultiSite_CenterAccounts.Read_Accounts(_request_header.SiteId,
                                                          ((Int32)_request.DataSet.Tables[0].Rows[0][0]) == _request_header.SiteId,
                                                          (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                          (Int64)_request.DataSet.Tables[0].Rows[0][2],  // AccountId --> 0: All
                                                          out _dt_reply))
              {
                Log.Error("Calling Read_Accounts. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_SYNC_PERSONAL_INFO:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Accounts 
              //
              if (!MultiSite_CenterAccounts.Read_Accounts((DataTable)_request.DataSet.Tables[0], out _ds_reply))
              {
                Log.Error("Calling Read_Accounts. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_GET_CENTER_SEQUENCES:
            {
              DataTable _dt_reply;

              //
              // Read Last Sequences
              //
              if (!MultiSite_CenterCommon.Read_LastCenterSequences(out _dt_reply))
              {
                Log.Error("Calling Read_LastCenterSequences. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_UPLOAD_SALES_PER_HOUR:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Sales Per Hour
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterSalesPerHour(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterSalesPerHour. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_PLAY_SESSIONS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update PlaySessions
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterPlaySessions(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterPlaySessions. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_AREAS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadAreas;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Areas
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterAreas(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterAreas. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_BANKS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadBanks;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Banks
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterBanks(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterBanks. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_PROVIDERS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadProviders;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Providers
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterProviders(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterProviders. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_TERMINALS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadTerminals;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Terminals
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterTerminals(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterTerminals. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_TERMINALS_CONNECTION:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Terminals Connected
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterTerminalsConnected(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterTerminalsConnected. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PROVIDERS_GAMES:
            {
              DataTable _dt_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Providers Games
              //
              if (!MultiSite_CenterGenericDownload.Read_ProvidersGames(_request_header.SiteId,
                                                                     (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                                      out _dt_reply))
              {
                Log.Error("Calling Read_ProvidersGames. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;
          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PROVIDERS:
            {
              DataTable _dt_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadProviders;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Providers
              //
              if (!MultiSite_CenterGenericDownload.Read_Providers(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _dt_reply))
              {
                Log.Error("Calling Read_Providers. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_GAME_PLAY_SESSIONS:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Providers Games
              //
              if (!MultiSite_CenterGenericUpload.Insert_GamePlaySessions(_request_header.SiteId, _request.DataSet, out _ds_reply))
              {
                Log.Error("Calling Insert_GamePlaySessions. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_MASTER_PROFILES:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Masters Profiles
              //
              if (!MultiSite_CenterGenericDownload.Read_MastersProfiles(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _ds_reply))
              {
                Log.Error("Calling Read_MastersProfiles. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;
          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_CORPORATE_USERS:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Masters Profiles
              //
              if (!MultiSite_CenterGenericDownload.Read_CorporateUsers(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _ds_reply))
              {
                Log.Error("Calling Read_CorporateUsers. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;
          case WWP_TableTypes.TABLE_TYPE_MULTISITE_REQUEST:
            {
              if (!MS_SiteRequest_Process.IncommingSiteRequestMessage(_request_header.SiteId, _request.DataSet.Tables[0]))
              {
                Log.Error("Calling IncommingSiteRequestMessage. Site: " + _request_header.SiteId.ToString());

                return;
              }

              // Nothing data to response
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;
             
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_ACCOUNTS_DOCUMENTS:
            {
              DataTable _dt_request;
              DataTable _dt_response;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments ;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Account Documents
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_AccountDocuments(_request_header.SiteId, _dt_request, out _dt_response))
              {
                Log.Error("Calling Update_AccountDocuments. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_response);
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;


          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_ACCOUNTS_DOCUMENTS:
            {
              DataTable _dt_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Account Documents
              //
              if (!MultiSite_CenterGenericDownload.Read_AccountDocuments(_request_header.SiteId,
                                                              (Int64)_request.DataSet.Tables[0].Rows[0][1],
                                                               out _dt_reply))
              {
                Log.Error("Calling Read_AccountDocuments. Site: " + _request_header.SiteId.ToString());
                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_reply);

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_UPLOAD_LAST_ACTIVITY:
            {
              DataTable _dt_request;
              DataTable _dt_response;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadLastActivity;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }
             
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_LastActivity(_request_header.SiteId, _dt_request, out _dt_response))
              {
                Log.Error("Calling Update_LastActivity. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response.DataSet.Tables.Add(_dt_response);
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_UPLOAD_CASHIER_SESIONS_GRUPED_BY_HOUR:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update CenterCasierSesionsGrupedByHour
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterCasierSesionsGrupedByHour(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterCasierSesionsGrupedByHour. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;


          case WWP_TableTypes.TABLE_TYPE_UPLOAD_ALARMS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadAlarms;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Update_CenterAlarms
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterAlarms(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CenterAlarms. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_ALARM_PATTERN:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Alarm Patterns
              //
              if (!MultiSite_CenterGenericDownload.Read_AlarmPatterns(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _ds_reply))
              {
                Log.Error("Calling Read_AlarmPatterns. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_LCD_MESSAGES:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages;
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Lcd Messages
              //
              if (!MultiSite_CenterGenericDownload.Read_LcdMessages(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _ds_reply))
              {
                Log.Error("Calling Read_LcdMessages. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_BUCKETS_CONFIG:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig; 
              _site_sequence = (Int64)_request.DataSet.Tables[0].Rows[0][1];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Buckets Config
              //
              if (!MultiSite_CenterGenericDownload.Read_BucketsConfig(_request_header.SiteId,
                                                           (Int64)_request.DataSet.Tables[0].Rows[0][1],  // SequenceId
                                                            out _ds_reply))
              {
                Log.Error("Calling Read_BucketsConfig. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;

          
          
          case WWP_TableTypes.TABLE_TYPE_UPLOAD_HANDPAYS:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadHandpays;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Update_Handpays
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterHandpays(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_Handpays. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_UPLOAD_SELLS_AND_BUYS:
            {
              DataSet _ds_reply;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys;
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Read Pending Providers Games
              //
              if (!MultiSite_CenterGenericUpload.Insert_SellsAndBuys(_request_header.SiteId, _request.DataSet, out _ds_reply))
              {
                Log.Error("Calling Sells And Buys. Site: " + _request_header.SiteId.ToString());

                return;
              }

              _response.Format = _request.Format;
              _response.DataSet = _ds_reply;

              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
            }
            break;


          case WWP_TableTypes.TABLE_TYPE_UPLOAD_CREDITLINES:
            {
              DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadCreditLines;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Update_Creditlines
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterCreditlines(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_Creditlines. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

          case WWP_TableTypes.TABLE_TYPE_UPLOAD_CREDITLINE_MOVEMENTS:
            {
               DataTable _dt_request;

              //
              // Switch to running the current task
              //
              _task = TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements;
              _site_sequence = (Int64)_request.DataSet.Tables[1].Rows[0][0];
              if (!UpdateToRunningTask(_request_header.SiteId, _task, _site_sequence))
              {
                return;
              }

              //
              // Update Update_Creditline Movements
              //
              _dt_request = _request.DataSet.Tables[0];
              if (!MultiSite_CenterGenericUpload.Update_CenterCreditlineMovements(_request_header.SiteId, _dt_request))
              {
                Log.Error("Calling Update_CreditlineMovements. Site: " + _request_header.SiteId.ToString());

                return;
              }
              _site_sequence = (Int64)_request.DataSet.Tables["SEQUENCE"].Rows[0][1];
              _response.Format = _request.Format;
              _response.DataSet = new DataSet("DataSet");
              _response_header.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

            }
            break;

        }
      }
      catch (WWP_Exception wwp_ex)
      {
        throw wwp_ex;
      }
      catch (Exception _ex)
      {
        WWP_MsgError _content;
        _content = new WWP_MsgError();
        _content.Description = _ex.Message;

        _response_header.MsgType = WWP_MsgTypes.WWP_MsgError;
        WwpResponse.MsgContent = _content;

        Log.Exception(_ex);
      }
      finally
      {
        if (_task != TYPE_MULTISITE_SITE_TASKS.Unknown)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (WWP_MultiSiteTask.Center_SetSiteReportingFlag(_request_header.SiteId, _task, _site_sequence, false, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
            else
            {
              Log.Error("Calling Set_ReportingFlag. Site: " + _request_header.SiteId.ToString() + ", Task: " + _task.ToString());
            }
          }
        }
      } // finally

    } // Table

    private static Boolean UpdateToRunningTask(Int32 SiteId, TYPE_MULTISITE_SITE_TASKS Task, Int64 SiteSequence)
    {

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (WWP_MultiSiteTask.Center_SetSiteReportingFlag(SiteId, Task, SiteSequence, true, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();
        }
        else
        {
          Log.Error(String.Format("Calling Set_ReportingFlag. Site: {0}", SiteId));

          return false;
        }
      }

      return true;
    }

  } // class ProcessSiteRequest

} // namespace WSI.WWP_Server

