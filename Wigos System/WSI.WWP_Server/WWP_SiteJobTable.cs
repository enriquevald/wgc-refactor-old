//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_SiteJobTable.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 22-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-MAR-2011 RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.WWP;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WWP_Server
{
  class WWP_SiteJobTable: WWP_SiteJob
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize data from table between Site and Center.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Processed ok. False: Otherwise.
    //
    protected override Boolean OnExecute()
    {
      String[] _parts;
      String _table_name;
      String _column_date;
      RemoteTable _remote;
      WWP_Message _wwp_request;
      WWP_MsgSqlCommand _request;
      WWP_Message _wwp_response;
      WWP_MsgSqlCommandReply _response;
      DataTable _site_changes;
      DataTable _center_changes;
      List<DateTime> _date_changes;
      Boolean _requested;
      IAsyncResult _iar;

      _parts = m_name.Split(',');

      _table_name = _parts[0];
      _column_date = "";
      if (_parts.Length > 1)
      {
        _column_date = _parts[1];
      }

      _remote = new RemoteTable(_table_name, m_site_id, _column_date);

      if (_remote.SiteChangesSqlCmd == null)
      {
        Log.Error(" Can't obtain SiteChanges SqlCommand. Can't sync table " + _table_name + ".");
        return false;
      }

      // Query Site Changes ...
      _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
      _request = (WWP_MsgSqlCommand)_wwp_request.MsgContent;

      // SiteChangesSqlCmd is SqlCommand to obtain the Site Changes.
      _request.SqlCommand = _remote.SiteChangesSqlCmd;

      _wwp_request.MsgHeader.SiteId = m_site_id;

      if (!WWP.WWP_Request(_wwp_request, 60000, out _wwp_response))
      {
        return false;
      }
      if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
      {
        return false;
      }
      //
      // On changes decide ...
      //
      _response = (WWP_MsgSqlCommandReply)_wwp_response.MsgContent;

      // Center Changes
      _center_changes = _remote.CenterChangesData.Copy();
      _center_changes.PrimaryKey = new DataColumn[] { _center_changes.Columns[0] };
      _center_changes.Columns[0].ColumnName = "DATE";
      _center_changes.Columns[1].ColumnName = "CENTER_ROW_COUNT";
      _center_changes.Columns[2].ColumnName = "CENTER_TIMESTAMP";

      // Site Changes just received
      _site_changes = _response.DataSet.Tables[0].Copy();
      _site_changes.PrimaryKey = new DataColumn[] { _site_changes.Columns[0] };
      _site_changes.Columns[0].ColumnName = "DATE";
      _site_changes.Columns[1].ColumnName = "SITE_ROW_COUNT";
      _site_changes.Columns[2].ColumnName = "SITE_TIMESTAMP";

      // Merge all data changes in one DataTable: _site_changes
      _site_changes.Columns.Add("CENTER_ROW_COUNT", _site_changes.Columns["SITE_ROW_COUNT"].DataType);
      _site_changes.Columns.Add("CENTER_TIMESTAMP", _site_changes.Columns["SITE_TIMESTAMP"].DataType);
      _site_changes.Merge(_center_changes);

      _date_changes = new List<DateTime>();
      foreach (DataRow _dr in _site_changes.Rows)
      {
        if (_dr["CENTER_TIMESTAMP"] == DBNull.Value)
        {
          _dr["CENTER_TIMESTAMP"] = new Byte[8];
        }
        _requested = false;
        _iar = null;

        while (RemoteTable.CompareTimestamp(_dr["SITE_TIMESTAMP"], _dr["CENTER_TIMESTAMP"]) > 0)
        {
          DateTime _from;
          Byte[] _timestamp;
          Byte[] _received;

          _from = (DateTime)_dr["DATE"];
          _timestamp = (Byte[])_dr["CENTER_TIMESTAMP"];

          if (!_requested)
          {
            // Query ...
            _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
            _request = (WWP_MsgSqlCommand)_wwp_request.MsgContent;
            _request.SqlCommand = _remote.GetSiteDataCommand(_timestamp, _from, _from.AddDays(1));

            _wwp_request.MsgHeader.SiteId = m_site_id;

            _iar = WWP_Server.Transaction.WWP_BeginRequest(_wwp_request, 60000, null);

            _requested = true;
          }

          if (!WWP_Server.Transaction.WWP_EndRequest(_iar, out _wwp_response))
          {
            // If error or timeout, exit while...
            break;
          }

          _response = (WWP_MsgSqlCommandReply)_wwp_response.MsgContent;

          // Save the DataTable received from the Site
          _remote.SiteTable = _response.DataSet.Tables[0];

          _received = _remote.GetLastTimestampReceived();

          // Update Center Timestamp as the last one received (memory)
          _dr["CENTER_TIMESTAMP"] = _received;
          
          if (RemoteTable.CompareTimestamp(_dr["SITE_TIMESTAMP"], _dr["CENTER_TIMESTAMP"]) > 0)
          {
            // Query ...
            _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
            _request = (WWP_MsgSqlCommand)_wwp_request.MsgContent;

            _from = (DateTime)_dr["DATE"];
            _request.SqlCommand = _remote.GetSiteDataCommand(_received, _from, _from.AddDays(1));

            _wwp_request.MsgHeader.SiteId = m_site_id;
            _iar = WWP_Server.Transaction.WWP_BeginRequest(_wwp_request, 60000, null);

            _requested = true;
          }

          // Save the DataTable received from the Site
          if (!_remote.Save())
          {
            // If error in Save, exit while...
            break; // while
          }

        } // while
      } // foreach

      return true;
    } // OnExecute

    #endregion // Public Methods

  } // WWP_SiteJobTable

} // WSI.WWP_Server
