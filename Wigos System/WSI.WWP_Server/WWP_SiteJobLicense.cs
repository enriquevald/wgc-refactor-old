//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_SiteJobLicense.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 23-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-FEB-2011 RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Reflection;
using WSI.WWP;
using System.Data;

namespace WSI.WWP_Server
{
  public class WWP_SiteJobLicense:WWP_SiteJob 
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Send a SqlCommand message to update the Site License.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Processed ok. False: Otherwise.
    //
    protected override Boolean OnExecute()
    {
      String _site_license;
      DateTime _expiration_date;
      DateTime _site_expiration_date;
      WWP_Message _wwp_request;
      WWP_MsgSqlCommand _request;
      WWP_Message _wwp_response;
      WWP_MsgSqlCommandReply _response;
      String _new_site_license;

      try
      {
        _site_license = GetSiteLicense();
        _expiration_date = GetExpirationDateFromSiteLicense(_site_license, SiteId);
        if (_expiration_date == DateTime.MinValue)
        {
          return false;
        }

        // Create a SqlCommand message
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
        _wwp_request.MsgHeader.SiteId = SiteId;

        // Set the SqlCommand to update the site license
        _request = (WWP_MsgSqlCommand)_wwp_request.MsgContent;
        _request.SqlCommand = GetSiteLicenseUpdateCommand(_site_license, _expiration_date);
        
        
        if ( !WWP.WWP_Request (_wwp_request, 30000, out _wwp_response) )
        {
          return false;
        }
        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          return false;
        }

        _response = (WWP_MsgSqlCommandReply)_wwp_response.MsgContent;

        _new_site_license = (String)_response.DataSet.Tables[0].Rows[0][0];
        if (String.IsNullOrEmpty(_new_site_license))
        {
          return false;
        }

        _site_expiration_date = GetExpirationDateFromSiteLicense(_new_site_license, SiteId);
        if (_expiration_date == DateTime.MinValue)
        {
          return false;
        }

        return (_site_expiration_date >= _expiration_date);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return true;
      }
    } // OnExecute

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Get the site license from DB.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetSiteLicense()
    {
      StringBuilder _sql_txt;
      String _db_license_str;

      _db_license_str = null;

      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("   SELECT   TOP 1 WL_LICENCE ");
      _sql_txt.AppendLine("     FROM   LICENCES ");
      _sql_txt.AppendLine(" ORDER BY   WL_EXPIRATION_DATE DESC, WL_INSERTION_DATE DESC ");

      // Don't catch possible exceptions, send them up.
      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
        {
          using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
          {
            if (_sql_reader.Read())
            {
              _db_license_str = _sql_reader.GetString(0);
            }
          }
        }
      }

      return _db_license_str;
    } // GetSiteLicense

    //------------------------------------------------------------------------------
    // PURPOSE : get the expiration date for a Site from a License.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String StrLicense
    //          - Int32 SiteId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DateTime
    //
    private static DateTime GetExpirationDateFromSiteLicense(String StrLicense,
                                                             Int32 SiteId)
    {
      XmlNodeList _sites;
      Int32 _site_id;
      DateTime _expiration_date;
      StringReader _str_reader;
      XmlReader _xml_reader;
      XmlDocument _xml_license;

      _expiration_date = DateTime.MinValue;

      if (String.IsNullOrEmpty(StrLicense))
      {
        return _expiration_date;
      }

      _str_reader = new StringReader(StrLicense);
      _xml_reader = XmlReader.Create(_str_reader);

      _xml_license = new XmlDocument();
      _xml_license.Load(_xml_reader);

      _sites = _xml_license.GetElementsByTagName("SiteLicense");

      foreach (XmlNode _site in _sites)
      {
        _site_id = Int32.Parse(_site.Attributes["SiteId"].Value);

        if (_site_id == SiteId)
        {
          _expiration_date = DateTime.Parse(_site.Attributes["LicenseNotValidAfter"].Value);

          break;
        }
      }

      return _expiration_date;
    } // GetExpirationDateFromSiteLicense

    //------------------------------------------------------------------------------
    // PURPOSE : Build the SQL Command to send to the client to update its license.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String StrLicense
    //          - DateTime ExpirationDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    private static SqlCommand GetSiteLicenseUpdateCommand(String StrLicense,
                                                          DateTime ExpirationDate)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_cmd;

      _sql_txt = new StringBuilder();

      // IF NOT EXISTS --> INSERT ELSE UPDATE AND DELETE(others)
      _sql_txt.AppendLine("IF NOT EXISTS ");
      _sql_txt.AppendLine("( ");
      _sql_txt.AppendLine("   SELECT   1 ");
      _sql_txt.AppendLine("     FROM   LICENCES ");
      _sql_txt.AppendLine(") ");
      _sql_txt.AppendLine("   INSERT INTO   LICENCES ");
      _sql_txt.AppendLine("               ( WL_LICENCE ");
      _sql_txt.AppendLine("               , WL_EXPIRATION_DATE ) ");
      _sql_txt.AppendLine("        VALUES ( @pLicense ");
      _sql_txt.AppendLine("               , @pExpirationDate ) ");
      _sql_txt.AppendLine("ELSE ");
      _sql_txt.AppendLine("BEGIN ");
      _sql_txt.AppendLine("   UPDATE   LICENCES ");
      _sql_txt.AppendLine("      SET   WL_LICENCE         = @pLicense ");
      _sql_txt.AppendLine("          , WL_EXPIRATION_DATE = @pExpirationDate ");
      _sql_txt.AppendLine("    WHERE   WL_ID = ( ");
      _sql_txt.AppendLine("                      SELECT   MIN (WL_ID) ");
      _sql_txt.AppendLine("                        FROM   LICENCES ");
      _sql_txt.AppendLine("                    ) ");
      _sql_txt.AppendLine("      AND   WL_EXPIRATION_DATE < @pExpirationDate ");
      _sql_txt.AppendLine("   DELETE   LICENCES ");
      _sql_txt.AppendLine("    WHERE   WL_ID > ( ");
      _sql_txt.AppendLine("                      SELECT   MIN (WL_ID) ");
      _sql_txt.AppendLine("                        FROM   LICENCES ");
      _sql_txt.AppendLine("                    ) ");
      _sql_txt.AppendLine("END ");
      _sql_txt.AppendLine("; ");

      // SELECT ACTUAL LICENSE
      _sql_txt.AppendLine("SELECT   WL_LICENCE ");
      _sql_txt.AppendLine("  FROM   LICENCES ");

      _sql_cmd = new SqlCommand(_sql_txt.ToString());
      _sql_cmd.Parameters.Add("@pLicense", SqlDbType.Xml).Value = StrLicense;
      _sql_cmd.Parameters.Add("@pExpirationDate", SqlDbType.DateTime).Value = ExpirationDate;

      return _sql_cmd;
    } // GetSiteLicenseUpdateCommand

    #endregion // Private Methods

  } // WWP_SiteJobLicense

} // WSI.WWP_Server
