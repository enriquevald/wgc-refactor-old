//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_SiteProcess.cs
// 
//   DESCRIPTION: Process pending Site Jobs.
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 14-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2011 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using System.Threading;
using System.Diagnostics;

namespace WSI.WWP_Server
{
  class WWP_SiteProcess
  {
    static List<WWP_SiteProcess> m_sites = new List<WWP_SiteProcess>();
    System.Threading.Thread m_thread = null;
    int     m_site_id;
    Int64   m_session_id;
    ManualResetEvent m_ev_connected = null;

    private WWP_SiteProcess (int SiteId, Int64 SessionId)
    {
      this.m_site_id = SiteId;
      this.m_session_id = SessionId;
      this.m_ev_connected = new ManualResetEvent(true);
      this.m_thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.SiteProcessThread));
      this.m_thread.Start();
    } // WWP_SiteProcess

    //------------------------------------------------------------------------------
    // PURPOSE : Wait for connected event until Timeout ms.
    //
    //  PARAMS :
    //      - INPUT :
    //          - int Timeout
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True: Connected event received. False: Otherwise.
    //
    private Boolean WaitConnected(int Timeout)
    {
      return m_ev_connected.WaitOne(Timeout);
    } // WaitConnected

    //------------------------------------------------------------------------------
    // PURPOSE : Mark a Site Disconnected.
    //
    //  PARAMS :
    //      - INPUT :
    //          - int SiteId
    //          - Int64 SessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void Stop(int SiteId, Int64 SessionId)
    {
      lock (m_sites)
      {
        foreach (WWP_SiteProcess _site in m_sites)
        {
          if (   _site.m_site_id    == SiteId
              && _site.m_session_id <= SessionId )
          {
            _site.m_ev_connected.Reset();
            
            return;            
          }
        }
      }
    } // Stop

    //------------------------------------------------------------------------------
    // PURPOSE : Mark a Site Connected.
    //
    //  PARAMS :
    //      - INPUT :
    //          - int SiteId
    //          - Int64 SessionId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void Start(int SiteId, Int64 SessionId)
    {
      lock (m_sites)
      {
        foreach (WWP_SiteProcess _site in m_sites)
        {
          if (   _site.m_site_id    == SiteId
              && _site.m_session_id <= SessionId )
          {
            _site.m_session_id = SessionId;
            _site.m_ev_connected.Set ();
            
            return;            
          }
        }
        
        // New site 
        m_sites.Add(new WWP_SiteProcess(SiteId, SessionId));
      }
    } // Start

    //------------------------------------------------------------------------------
    // PURPOSE : Site Thread to process Jobs.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void SiteProcessThread()
    {
      Boolean _prev_connected;
      Boolean _connected;
      Int32 _wait_hint;
      WWP_SiteJob _job;

      _wait_hint = 0; // No delay
      _prev_connected = false;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 1 * 60 * 1000; 
        
        _connected = WaitConnected(15 * 60 * 1000);
        if (_connected != _prev_connected)
        {
          // Save status
          _prev_connected = _connected;
        
          // Status changed
          if (!_connected)
          {
            // Disconnected
            _wait_hint = 0;
            
            continue;
          }
          // Connected again
        }

        _job = WWP_SiteJob.NextSiteJob(m_site_id);
        if ( _job == null )
        {
          // No jobs
          continue;
        }

        try
        {
          _job.PublicExecute();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
          
        // Success
        _wait_hint = 0;

      } // while
    } // SiteProcessThread

  } // WWP_SiteProcess

} // WSI.WWP_Server
