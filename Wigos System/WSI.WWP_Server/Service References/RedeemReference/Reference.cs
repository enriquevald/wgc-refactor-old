﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSI.WWP_CenterService.RedeemReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://siebel.com/CustomUI", ConfigurationName="RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow")]
    public interface CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow {
        
        // CODEGEN: Generating message contract since the operation CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="document/http://siebel.com/CustomUI:CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow" +
            "", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow(WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://siebel.com/CustomUI")]
    public partial class CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string numero_spcMiembroField;
        
        private string cDR_spcNumero_spcTarjetaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Numero_spcMiembro {
            get {
                return this.numero_spcMiembroField;
            }
            set {
                this.numero_spcMiembroField = value;
                this.RaisePropertyChanged("Numero_spcMiembro");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string CDR_spcNumero_spcTarjeta {
            get {
                return this.cDR_spcNumero_spcTarjetaField;
            }
            set {
                this.cDR_spcNumero_spcTarjetaField = value;
                this.RaisePropertyChanged("CDR_spcNumero_spcTarjeta");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.siebel.com/xml/LOY%20Transaction%20IO")]
    public partial class LoyTransaction : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string transacCanjeadaField;
        
        private string montoField;
        
        private string memberNumberField;
        
        private string pointsField;
        
        private string pointsAccumulatedField;
        
        private string productNameField;
        
        private string productTypeField;
        
        private string transactionDateField;
        
        private string transactionNumberField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string TransacCanjeada {
            get {
                return this.transacCanjeadaField;
            }
            set {
                this.transacCanjeadaField = value;
                this.RaisePropertyChanged("TransacCanjeada");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Monto {
            get {
                return this.montoField;
            }
            set {
                this.montoField = value;
                this.RaisePropertyChanged("Monto");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string MemberNumber {
            get {
                return this.memberNumberField;
            }
            set {
                this.memberNumberField = value;
                this.RaisePropertyChanged("MemberNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Points {
            get {
                return this.pointsField;
            }
            set {
                this.pointsField = value;
                this.RaisePropertyChanged("Points");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string PointsAccumulated {
            get {
                return this.pointsAccumulatedField;
            }
            set {
                this.pointsAccumulatedField = value;
                this.RaisePropertyChanged("PointsAccumulated");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string ProductName {
            get {
                return this.productNameField;
            }
            set {
                this.productNameField = value;
                this.RaisePropertyChanged("ProductName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string ProductType {
            get {
                return this.productTypeField;
            }
            set {
                this.productTypeField = value;
                this.RaisePropertyChanged("ProductType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string TransactionDate {
            get {
                return this.transactionDateField;
            }
            set {
                this.transactionDateField = value;
                this.RaisePropertyChanged("TransactionDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string TransactionNumber {
            get {
                return this.transactionNumberField;
            }
            set {
                this.transactionNumberField = value;
                this.RaisePropertyChanged("TransactionNumber");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://siebel.com/CustomUI")]
    public partial class CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string error_spcCodeField;
        
        private string error_spcMessageField;
        
        private LoyTransaction[] listOfLoyTransactionIoField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string Error_spcCode {
            get {
                return this.error_spcCodeField;
            }
            set {
                this.error_spcCodeField = value;
                this.RaisePropertyChanged("Error_spcCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string Error_spcMessage {
            get {
                return this.error_spcMessageField;
            }
            set {
                this.error_spcMessageField = value;
                this.RaisePropertyChanged("Error_spcMessage");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace="http://www.siebel.com/xml/LOY%20Transaction%20IO", Order=2)]
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public LoyTransaction[] ListOfLoyTransactionIo {
            get {
                return this.listOfLoyTransactionIoField;
            }
            set {
                this.listOfLoyTransactionIoField = value;
                this.RaisePropertyChanged("ListOfLoyTransactionIo");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://siebel.com/CustomUI", Order=0)]
        public WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input;
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest() {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest(WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input) {
            this.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input = CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://siebel.com/CustomUI", Order=0)]
        public WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output;
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse() {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse(WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output) {
            this.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output = CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowChannel : WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient : System.ServiceModel.ClientBase<WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow>, WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow {
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient() {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow(WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest request) {
            return base.Channel.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow(request);
        }
        
        public WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow(WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input) {
            WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest inValue = new WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowRequest();
            inValue.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input = CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input;
            WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflowResponse retVal = ((WSI.WWP_CenterService.RedeemReference.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow)(this)).CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow(inValue);
            return retVal.CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output;
        }
    }
}
