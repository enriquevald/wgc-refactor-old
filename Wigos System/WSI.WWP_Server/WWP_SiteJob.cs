//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_SiteJob.cs
// 
//   DESCRIPTION: Site Jobs Management: 
//                - Save and delete a SiteJob
//                - Retrieve all Site Jobs enabled
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 11-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAR-2011 RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.WWP_Server
{
  #region Enums
    
  public enum SiteJobType
  {
    TABLE = 0,
    LICENSE = 1,
    OTHER = 2,
    NOT_ASSIGNED = 999
  }

  #endregion // Enums

  public class WWP_SiteJobList : List<WWP_SiteJob>
  {
    public new void Add(WWP_SiteJob SiteJob)
    {
      base.Add(SiteJob);
    }
  } // WWP_SiteJobList

  public class WWP_SiteJob
  {
    #region Members

    protected Int32 m_site_id;
    protected SiteJobType m_type;
    protected String m_name;
    protected Boolean m_enabled;
    protected Int32 m_period;
    protected DateTime m_next_try;
    protected Int32 m_order;
    protected DateTime m_last_try;
    protected Int32 m_num_tries;
    protected DateTime m_last_try_ok;
    protected DateTime m_sync_date;
    protected Byte[] m_sync_timestamp;

    // private 
    TimeSpan m_try_started;

    #endregion // Members

    #region Properties

    public Int32 SiteId
    {
      get { return m_site_id; }
      set { m_site_id = value; }
    }
    public SiteJobType Type
    {
      get { return m_type; }
      set { m_type = value; }
    }
    public String Name
    {
      get { return m_name; }
      set { m_name = value; }
    }
    public Boolean Enabled
    {
      get { return m_enabled; }
      set { m_enabled = value; }
    }
    public Int32 Period
    {
      get { return m_period; }
      set { m_period = value; }
    }
    public DateTime NextTry
    {
      get { return m_next_try; }
      set { m_next_try = value; }
    }
    public Int32 Order
    {
      get { return m_order; }
      set { m_order = value; }
    }
    public DateTime LastTry
    {
      get { return m_last_try; }
      set { m_last_try = value; }
    }
    public Int32 NumTries
    {
      get { return m_num_tries; }
      set { m_num_tries = value; }
    }
    public DateTime LastTryOk
    {
      get { return m_last_try_ok; }
      set { m_last_try_ok = value; }
    }
    public DateTime SyncDate
    {
      get { return m_sync_date; }
      set { m_sync_date = value; }
    }
    public Byte[] SyncTimestamp
    {
      get { return m_sync_timestamp; }
      set { m_sync_timestamp = value; }
    }

    #endregion // Properties

    #region Constructor

    public WWP_SiteJob()
    {
      m_site_id = -1;
      m_type = SiteJobType.NOT_ASSIGNED;
      m_name = "";
      m_enabled = false;
      m_period = 0;
      m_next_try = DateTime.MinValue;
      m_order = 0;
      m_last_try = DateTime.MinValue;
      m_num_tries = 0;
      m_last_try_ok = DateTime.MinValue;
      m_sync_date = DateTime.MinValue;
      m_sync_timestamp = null;
    } // SiteJob

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Save the SiteJob to the DB. Can INSERT or UPDATE, depending of existence of the SiteJob.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Saved ok. False: Otherwise.
    //
    public Boolean Update()
    {
      StringBuilder _sql_str;
      SqlCommand _sql_cmd;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (m_site_id == -1
            || m_type == SiteJobType.NOT_ASSIGNED
            || m_name == ""
            || m_period == 0
            || m_next_try == DateTime.MinValue
            || m_order == 0)
          {
            Log.Error("SiteJob.DB_Save: Can't save SiteJob. Some properties have no value.");

            return false;
          }

          _sql_str = new StringBuilder();
          _sql_str.AppendLine("IF NOT EXISTS ");
          _sql_str.AppendLine("( ");
          _sql_str.AppendLine("   SELECT   1 ");
          _sql_str.AppendLine("     FROM   SITE_JOBS ");
          _sql_str.AppendLine("    WHERE   SJB_SITE_ID = @pSiteId ");
          _sql_str.AppendLine("      AND   SJB_TYPE    = @pType ");
          _sql_str.AppendLine("      AND   SJB_NAME    = @pName ");
          _sql_str.AppendLine(") ");
          _sql_str.AppendLine("   INSERT INTO   SITE_JOBS (SJB_SITE_ID ");
          _sql_str.AppendLine("                          , SJB_TYPE ");
          _sql_str.AppendLine("                          , SJB_NAME ");
          _sql_str.AppendLine("                          , SJB_ENABLED ");
          _sql_str.AppendLine("                          , SJB_PERIOD ");
          _sql_str.AppendLine("                          , SJB_NEXT_TRY ");
          _sql_str.AppendLine("                          , SJB_ORDER ");
          _sql_str.AppendLine("                          , SJB_LAST_TRY ");
          _sql_str.AppendLine("                          , SJB_LAST_TRY_OK ");
          _sql_str.AppendLine("                          , SJB_SYNC_DATE ");
          _sql_str.AppendLine("                          , SJB_SYNC_TIMESTAMP ");
          _sql_str.AppendLine("                           ) ");
          _sql_str.AppendLine("                    VALUES (@pSiteId ");
          _sql_str.AppendLine("                          , @pType ");
          _sql_str.AppendLine("                          , @pName ");
          _sql_str.AppendLine("                          , @pEnabled ");
          _sql_str.AppendLine("                          , @pPeriod ");
          _sql_str.AppendLine("                          , @pNextTry ");
          _sql_str.AppendLine("                          , @pOrder ");
          _sql_str.AppendLine("                          , @pLastTry ");
          _sql_str.AppendLine("                          , @pLastTryOk ");
          _sql_str.AppendLine("                          , @pSyncDate ");
          _sql_str.AppendLine("                          , @pSyncTimestamp ) ");
          _sql_str.AppendLine("ELSE ");
          _sql_str.AppendLine("   UPDATE   SITE_JOBS ");
          _sql_str.AppendLine("      SET   SJB_ENABLED        = @pEnabled ");
          _sql_str.AppendLine("          , SJB_PERIOD         = @pPeriod ");
          _sql_str.AppendLine("          , SJB_NEXT_TRY       = @pNextTry ");
          _sql_str.AppendLine("          , SJB_ORDER          = @pOrder ");
          _sql_str.AppendLine("          , SJB_LAST_TRY       = @pLastTry ");
          _sql_str.AppendLine("          , SJB_LAST_TRY_OK    = @pLastTryOk ");
          _sql_str.AppendLine("          , SJB_SYNC_DATE      = @pSyncDate ");
          _sql_str.AppendLine("          , SJB_SYNC_TIMESTAMP = @pSyncTimestamp ");
          _sql_str.AppendLine("    WHERE   SJB_SITE_ID        = @pSiteId ");
          _sql_str.AppendLine("      AND   SJB_TYPE           = @pType ");
          _sql_str.AppendLine("      AND   SJB_NAME           = @pName ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = m_site_id;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = m_type;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_name;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = m_enabled;
          _sql_cmd.Parameters.Add("@pPeriod", SqlDbType.Int).Value = m_period;
          _sql_cmd.Parameters.Add("@pNextTry", SqlDbType.DateTime).Value = m_next_try;
          _sql_cmd.Parameters.Add("@pOrder", SqlDbType.Int).Value = m_order;
          if (m_last_try != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pLastTry", SqlDbType.DateTime).Value = m_last_try;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pLastTry", SqlDbType.DateTime).Value = DBNull.Value;
          }
          if (m_last_try_ok != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pLastTryOk", SqlDbType.DateTime).Value = m_last_try_ok;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pLastTryOk", SqlDbType.DateTime).Value = DBNull.Value;
          }
          if (m_sync_date != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pSyncDate", SqlDbType.DateTime).Value = m_sync_date;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pSyncDate", SqlDbType.DateTime).Value = DBNull.Value;
          }

          if (m_sync_timestamp != null)
          {
            _sql_cmd.Parameters.Add("@pSyncTimestamp", SqlDbType.VarBinary).Value = m_sync_timestamp;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pSyncTimestamp", SqlDbType.VarBinary).Value = DBNull.Value;
          }

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Update

    //------------------------------------------------------------------------------
    // PURPOSE : Delete the SiteJob from the DB.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Deleted ok. False: Otherwise.
    //
    public Boolean Delete()
    {
      StringBuilder _sql_str;
      SqlCommand _sql_cmd;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (m_site_id == -1
            || m_type == SiteJobType.NOT_ASSIGNED
            || m_name == "")
          {
            Log.Error("SiteJob.DB_Delete: Can't delete. Some properties have no value.");

            return false;
          }

          _sql_str = new StringBuilder();
          _sql_str.AppendLine("DELETE   SITE_JOBS ");
          _sql_str.AppendLine(" WHERE   SJB_SITE_ID = @pSiteId ");
          _sql_str.AppendLine("   AND   SJB_TYPE    = @pType ");
          _sql_str.AppendLine("   AND   SJB_NAME    = @pName ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = m_site_id;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = m_type;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = m_name;

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Delete

    //------------------------------------------------------------------------------
    // PURPOSE : Get the next Site Job enabled.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - SiteJob
    //
    // RETURNS :
    //      - True if SiteJob is retrieved correctly. False, otherwise.
    //
    public static WWP_SiteJob NextSiteJob(Int32 SiteId)
    {
      StringBuilder _sql_str;
      SqlCommand _sql_cmd;
      WWP_SiteJob _job;
      Int32 _site_id;
      SiteJobType _type;
      String _name;
      Boolean _enabled;
      Int32 _period;
      DateTime _last_try;
      Int32 _num_tries;
      DateTime _last_try_ok;
      DateTime _sync_date;
      Byte[] _sync_timestamp;

      _job = null;
      _site_id = -1;
      _type = SiteJobType.NOT_ASSIGNED;
      _name = "";
      _enabled = false;
      _period = 0;
      _last_try = DateTime.MinValue;
      _num_tries = 0;
      _last_try_ok = DateTime.MinValue;
      _sync_date = DateTime.MinValue;
      _sync_timestamp = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str = new StringBuilder();
          _sql_str.AppendLine("SELECT   TOP 1 SJB_SITE_ID ");
          _sql_str.AppendLine("       , SJB_TYPE ");
          _sql_str.AppendLine("       , SJB_NAME ");
          _sql_str.AppendLine("       , SJB_ENABLED ");
          _sql_str.AppendLine("       , SJB_PERIOD ");
          _sql_str.AppendLine("       , SJB_LAST_TRY ");
          _sql_str.AppendLine("       , SJB_NUM_TRIES ");
          _sql_str.AppendLine("       , SJB_LAST_TRY_OK ");
          _sql_str.AppendLine("       , SJB_SYNC_DATE ");
          _sql_str.AppendLine("       , SJB_SYNC_TIMESTAMP ");
          _sql_str.AppendLine("  FROM   SITE_JOBS, SITES ");
          _sql_str.AppendLine(" WHERE   SJB_SITE_ID   = ST_SITE_ID ");
          _sql_str.AppendLine("   AND   SJB_SITE_ID   = @pSiteId ");
          _sql_str.AppendLine("   AND   SJB_ENABLED   = 1 ");
          _sql_str.AppendLine("   AND   SJB_NEXT_TRY <= GETDATE() ");
          _sql_str.AppendLine("   AND   ST_STATE      = @pSiteState ");
          _sql_str.AppendLine("ORDER BY SJB_NEXT_TRY ASC ");
          _sql_str.AppendLine("       , SJB_ORDER ASC ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
          _sql_cmd.Parameters.Add("@pSiteState", SqlDbType.Int).Value = (Int32)SITE_STATE.ENABLED;

          using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
          {
            if (_sql_reader.Read())
            {
              _site_id = _sql_reader.GetInt32(0);
              _type = (SiteJobType)_sql_reader.GetInt32(1);
              _name = _sql_reader.GetString(2);
              _enabled = _sql_reader.GetBoolean(3);
              _period = _sql_reader.GetInt32(4);
              _last_try = _sql_reader.IsDBNull(5) ? DateTime.MinValue : _sql_reader.GetDateTime(5);
              _num_tries = _sql_reader.GetInt32(6);
              _last_try_ok = _sql_reader.IsDBNull(7) ? DateTime.MinValue : _sql_reader.GetDateTime(7);
              _sync_date = _sql_reader.IsDBNull(8) ? DateTime.MinValue : _sql_reader.GetDateTime(8);
              _sync_timestamp = (Byte[])(_sql_reader.IsDBNull(9) ? null : _sql_reader.GetValue(9));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        _site_id = -1;
        Log.Exception(_ex);
      }

      if (_site_id == -1)
      {
        return null;
      }

      switch (_type)
      {
        case SiteJobType.TABLE:
          _job = new WWP_SiteJobTable();
          break;

        case SiteJobType.LICENSE:
          _job = new WWP_SiteJobLicense();
          break;

        case SiteJobType.OTHER:
          _job = new WWP_SiteJob();
          break;

        case SiteJobType.NOT_ASSIGNED:
        default:
          break;
      }

      if (_job != null)
      {
        _job.SiteId = _site_id;
        _job.Type = _type;
        _job.Name = _name;
        _job.Enabled = _enabled;
        _job.Period = _period;
        _job.LastTry = _last_try;
        _job.NumTries = _num_tries;
        _job.LastTryOk = _last_try_ok;
        _job.SyncDate = _sync_date;
        _job.SyncTimestamp = _sync_timestamp;
      }

      return _job;

    } // NextSiteJob

    //------------------------------------------------------------------------------
    // PURPOSE : Get all Site Jobs enabled.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - List of SiteJobs. If error occurs, an empty list is returned.
    //
    public static WWP_SiteJobList GetAllEnabled()
    {
      DataTable _dt_site_jobs;
      SqlDataAdapter _sql_da;
      StringBuilder _sql_str;
      SqlCommand _sql_cmd;
      WWP_SiteJob _site_job;
      WWP_SiteJobList _list_jobs;
      SiteJobType _type;

      _type = SiteJobType.NOT_ASSIGNED;

      _list_jobs = new WWP_SiteJobList();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str = new StringBuilder();
          _sql_str.AppendLine("SELECT   SJB_SITE_ID ");
          _sql_str.AppendLine("       , SJB_TYPE ");
          _sql_str.AppendLine("       , SJB_NAME ");
          _sql_str.AppendLine("       , SJB_ENABLED ");
          _sql_str.AppendLine("       , SJB_PERIOD ");
          _sql_str.AppendLine("       , SJB_LAST_TRY ");
          _sql_str.AppendLine("       , SJB_NUM_TRIES ");
          _sql_str.AppendLine("       , SJB_LAST_TRY_OK ");
          _sql_str.AppendLine("       , SJB_SYNC_DATE ");
          _sql_str.AppendLine("       , SJB_SYNC_TIMESTAMP ");
          _sql_str.AppendLine("  FROM   SITE_JOBS ");
          _sql_str.AppendLine(" WHERE   SJB_ENABLED = 1 ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());

          _dt_site_jobs = new DataTable("SITE_JOBS");
          _sql_da = new SqlDataAdapter();
          _sql_da.SelectCommand = _sql_cmd;

          _db_trx.Fill(_sql_da, _dt_site_jobs);

          _sql_cmd.Dispose();
          _sql_da.Dispose();

          foreach (DataRow _dr_site_job in _dt_site_jobs.Rows)
          {
            _site_job = null;

            _type = (SiteJobType)_dr_site_job[1];
            switch (_type)
            {
              case SiteJobType.TABLE:
                _site_job = new WWP_SiteJobTable();
                break;

              case SiteJobType.LICENSE:
                _site_job = new WWP_SiteJobLicense();
                break;

              case SiteJobType.OTHER:
                _site_job = new WWP_SiteJob();
                break;

              case SiteJobType.NOT_ASSIGNED:
              default:
                break;
            }

            if (_site_job != null)
            {
              _site_job.Type = _type;
              _site_job.SiteId = (Int32)_dr_site_job[0];
              _site_job.Name = (String)_dr_site_job[2];
              _site_job.Enabled = (Boolean)_dr_site_job[3];
              _site_job.Period = (Int32)_dr_site_job[4];
              _site_job.LastTry = (DateTime)(_dr_site_job[5] != DBNull.Value ? _dr_site_job[5] : DateTime.MinValue);
              _site_job.NumTries = (Int32)_dr_site_job[6];
              _site_job.LastTryOk = (DateTime)(_dr_site_job[7] != DBNull.Value ? _dr_site_job[7] : DateTime.MinValue);
              _site_job.SyncDate = (DateTime)(_dr_site_job[8] != DBNull.Value ? _dr_site_job[8] : DateTime.MinValue);
              _site_job.SyncTimestamp = (Byte[])(_dr_site_job[9] != DBNull.Value ? _dr_site_job[9] : null);

              _list_jobs.Add(_site_job);
            }
          } // foreach
        } // using DB_TRX
      }
      catch (Exception _ex)
      {
        _list_jobs = new WWP_SiteJobList();

        Log.Exception(_ex);
      }

      return _list_jobs;
    } // GetAllEnabled

    //------------------------------------------------------------------------------
    // PURPOSE : Set the properties of a SiteJob to indicate that a new try is starting.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteJob
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if SiteJob is updated correctly. False, otherwise.
    //
    public static Boolean SetTried(WWP_SiteJob Job)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      DateTime _now;

      Job.m_try_started = Performance.GetTickCount();
      _now = WGDB.Now;

      Job.NumTries++;
      Job.LastTry = _now;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str = new StringBuilder();
          _sql_str.AppendLine("UPDATE   SITE_JOBS ");
          _sql_str.AppendLine("   SET   SJB_NUM_TRIES = SJB_NUM_TRIES + 1 ");
          _sql_str.AppendLine("       , SJB_LAST_TRY  = @pLastTry ");
          _sql_str.AppendLine(" WHERE   SJB_SITE_ID   = @pSiteId ");
          _sql_str.AppendLine("   AND   SJB_TYPE      = @pType ");
          _sql_str.AppendLine("   AND   SJB_NAME      = @pName ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Job.m_site_id;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Job.m_type;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Job.m_name;
          _sql_cmd.Parameters.Add("@pLastTry", SqlDbType.DateTime).Value = _now;

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SetTried

    //------------------------------------------------------------------------------
    // PURPOSE : Update SiteJob statistics related to jobs, and indicate
    //            that a try has finished (action depends on Success parameter).
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteJob Job
    //          - Boolean Success
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void SetExecuted(WWP_SiteJob Job,
                                   Boolean Success)
    {
      TimeSpan _elapsed;
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      DateTime _min_next_try;

      _elapsed = Performance.GetElapsedTicks(Job.m_try_started);
      _min_next_try = WGDB.Now.AddMinutes(1);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (Success)
          {
            // Reset NumTries
            // Save ExcecutionTime
            Job.NumTries = 0;
            Job.LastTryOk = Job.LastTry;
            Job.NextTry = Job.LastTryOk.AddSeconds(Job.Period);
          }
          else
          {
            if (Job.NumTries == 0)
            {
              throw new Exception("Job has not been Tried.");
            }

            Job.NextTry = _min_next_try.AddMinutes(Job.NumTries % 3);
          }

          if (_min_next_try > Job.NextTry)
          {
            Job.NextTry = _min_next_try;
          }
          Job.NextTry = Job.NextTry.AddSeconds((60 - Job.NextTry.Second) % 60);

          _sql_str = new StringBuilder();
          _sql_str.AppendLine("UPDATE   SITE_JOBS ");
          _sql_str.AppendLine("   SET   SJB_NUM_TRIES            = CASE WHEN (@pSuccess = 1) THEN 0 ELSE SJB_NUM_TRIES END ");
          _sql_str.AppendLine("       , SJB_LAST_TRY_OK          = CASE WHEN (@pSuccess = 1) THEN SJB_LAST_TRY ELSE SJB_LAST_TRY_OK END ");
          _sql_str.AppendLine("       , SJB_NUM_EXECUTIONS       = CASE WHEN (@pSuccess = 1) THEN SJB_NUM_EXECUTIONS + 1 ELSE SJB_NUM_EXECUTIONS END ");
          _sql_str.AppendLine("       , SJB_TOTAL_EXECUTION_TIME = CASE WHEN (@pSuccess = 1) THEN SJB_TOTAL_EXECUTION_TIME + @pElapsed ELSE SJB_TOTAL_EXECUTION_TIME END ");
          _sql_str.AppendLine("       , SJB_NEXT_TRY             = @pNextTry ");
          _sql_str.AppendLine(" WHERE   SJB_SITE_ID              = @pSiteId ");
          _sql_str.AppendLine("   AND   SJB_TYPE                 = @pType ");
          _sql_str.AppendLine("   AND   SJB_NAME                 = @pName ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSuccess", SqlDbType.Bit).Value = Success;
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Job.m_site_id;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Job.m_type;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Job.m_name;
          _sql_cmd.Parameters.Add("@pNextTry", SqlDbType.DateTime).Value = Job.NextTry;
          _sql_cmd.Parameters.Add("@pElapsed", SqlDbType.BigInt).Value = (Int64)_elapsed.TotalMilliseconds;

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            _db_trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SetExecuted

    //------------------------------------------------------------------------------
    // PURPOSE : Update SiteJob statistics related to messages (commands).
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteJob Job
    //          - Int64 RemoteExecutionTime
    //          - Int64 RoundTripTime
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void SetMsgExecuted(WWP_SiteJob Job,
                                      Int64 RemoteExecutionTime,
                                      Int64 RoundTripTime)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // TODO: Set sjb_last_try_ok = sjb_last_try
          //       if Success 
          //          sjb_num_messages++
          //          sjb_xxx = sjb_xxx + RemoteExecutionTime
          //          sjb_xxx = sjb_xxx + RoundTripTime

          Job.LastTryOk = Job.LastTry;

          _sql_str = new StringBuilder();
          _sql_str.AppendLine("UPDATE   SITE_JOBS ");
          _sql_str.AppendLine("   SET   SJB_LAST_TRY_OK          = SJB_LAST_TRY ");
          _sql_str.AppendLine("       , SJB_NUM_MESSAGES         = SJB_NUM_MESSAGES + 1 ");
          _sql_str.AppendLine("       , SJB_TOTAL_MESSAGE_TIME   = SJB_TOTAL_MESSAGE_TIME + @pRemoteExecutionTime ");
          _sql_str.AppendLine("       , SJB_TOTAL_ROUNDTRIP_TIME = SJB_TOTAL_ROUNDTRIP_TIME + @pRoundTripTime ");
          _sql_str.AppendLine(" WHERE   SJB_SITE_ID              = @pSiteId ");
          _sql_str.AppendLine("   AND   SJB_TYPE                 = @pType ");
          _sql_str.AppendLine("   AND   SJB_NAME                 = @pName ");

          _sql_cmd = new SqlCommand(_sql_str.ToString());
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Job.m_site_id;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Job.m_type;
          _sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = Job.m_name;
          _sql_cmd.Parameters.Add("@pRemoteExecutionTime", SqlDbType.BigInt).Value = RemoteExecutionTime;
          _sql_cmd.Parameters.Add("@pRoundTripTime", SqlDbType.BigInt).Value = RoundTripTime;

          if (_db_trx.ExecuteNonQuery(_sql_cmd) == 1)
          {
            _db_trx.Commit();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SetMsgExecuted

    //------------------------------------------------------------------------------
    // PURPOSE : Execute a SiteJob.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteJob
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True if SiteJob is executed correctly. False, otherwise.
    //
    public void PublicExecute()
    {
      Boolean _success;
      
      _success = false;
      if (WWP_SiteJob.SetTried(this))
      {
        try
        {
          _success = this.OnExecute();
        }
        catch
        { }

        WWP_SiteJob.SetExecuted(this, _success);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Execute a SqlCommand (Virtual Method).
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Processed ok. False: Otherwise.
    //
    protected virtual Boolean OnExecute()
    {
      throw new Exception("Not implemented");
    } // OnExecute

    //------------------------------------------------------------------------------
    // PURPOSE : For test only.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Test()
    {
      WWP_SiteJob _site_job;
      List<WWP_SiteJob> _list_jobs;
      Int32 _count;

      _site_job = NextSiteJob(55);
      SetTried(_site_job);
      SetExecuted(_site_job, true);
      SetTried(_site_job);
      SetExecuted(_site_job, false);

      _site_job = new WWP_SiteJob();
      _site_job.SiteId = 55;
      _site_job.Type = SiteJobType.TABLE;
      _site_job.Name = "TEST 1";
      _site_job.Enabled = true;
      _site_job.Period = 3600;
      _site_job.NextTry = DateTime.Now.AddHours(1);
      _site_job.Order = 1;

      if (_site_job.Update())
      {
      }
      else
      {
      }

      _site_job = new WWP_SiteJob();
      _site_job.SiteId = 57;
      _site_job.Type = SiteJobType.LICENSE;
      _site_job.Name = "TEST 2";
      _site_job.Enabled = true;
      _site_job.Period = 7200;
      _site_job.NextTry = DateTime.Now.AddHours(2);
      _site_job.Order = 2;

      if (_site_job.Update())
      {
      }
      else
      {
      }

      _list_jobs = WWP_SiteJob.GetAllEnabled();
      _count = _list_jobs.Count;

      _site_job = new WWP_SiteJob();
      _site_job.SiteId = 57;
      _site_job.Type = SiteJobType.LICENSE;
      _site_job.Name = "TEST 2";
      _site_job.Enabled = false;
      _site_job.Period = 7200;
      _site_job.SyncDate = DateTime.MinValue;
      _site_job.NextTry = DateTime.Now.AddHours(3);
      _site_job.Order = 3;

      if (_site_job.Update())
      {
      }
      else
      {
      }

      _list_jobs = WWP_SiteJob.GetAllEnabled();
      _count = _list_jobs.Count;

      _site_job = new WWP_SiteJob();
      _site_job.SiteId = 57;
      _site_job.Type = SiteJobType.LICENSE;
      _site_job.Name = "TEST 2";

      if (_site_job.Delete())
      {
      }
      else
      {
      }

      _list_jobs = WWP_SiteJob.GetAllEnabled();
      _count = _list_jobs.Count;

      _site_job = NextSiteJob(57);

    } // Test

    #endregion // Public Methods

  } // WWP_SiteJob

} // WSI.Common
