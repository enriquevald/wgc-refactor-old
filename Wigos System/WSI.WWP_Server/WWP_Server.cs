//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_Server.cs
// 
//   DESCRIPTION: WWP server conections
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2011 ACC    First release.
// 08-MAR-2011 AJQ    Refurbished
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using System.Threading;


namespace WSI.WWP_Server
{
  public static class WWP
  {
    static int m_default_timeout = 60 * 1000;

    static WWP_Server m_wwp_server;

    public static WWP_Server Server
    {
      get { return WWP.m_wwp_server; }
      set { WWP.m_wwp_server = value; }
    }


    public static Boolean WWP_Request(WWP_Message Request, out WWP_Message Response)
    {
      return WWP_Request(Request, m_default_timeout, out Response);
    }

    public static Boolean WWP_Request(WWP_Message Request, int Timeout, out WWP_Message Response)
    {
      Response = null;

      try
      {
        IAsyncResult _iar;

        _iar = WWP_Server.Transaction.WWP_BeginRequest(Request, Timeout, null);

        if (WWP_Server.Transaction.WWP_EndRequest(_iar, out Response))
        {
          return true;
        }
      }
      catch { }

      return false;
    }

  }

  public class WWP_Server : SecureTcpServer
  {
    private const String WWP_STX = "<WWP_Message>";
    private const String WWP_ETX = "</WWP_Message>";
    private const Int32 WWP_MESSAGE_MAX_LENGTH = 32 * 1024 * 1024;

    /// <summary>
    /// Send an Xml message
    /// </summary>
    /// <param name="SiteId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Xml"></param>
    public bool SendTo(Int32 SiteId, Int64 SessionId, String Xml)
    {
      SecureTcpClient _client;
      
      _client = null;
      
      m_clients_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        foreach (SecureTcpClient client in Clients)
        {
          if (client.InternalId == SiteId)
          {
            if (SessionId == 0
                || SessionId == client.SessionId)
            {
              _client = client;
              
              break;
            }
          }
        }
      }
      finally
      {
        m_clients_rw_lock.ReleaseReaderLock();
      }

      if ( _client != null )
      {
        try 
        {
          return _client.Send(Xml);
        }
        catch 
        {}
      }

      return false;
    }

    public WWP_Server(IPEndPoint IPEndPoint)
      : base(IPEndPoint)
    {
      this.SetXmlServerParameters(WWP_STX, WWP_ETX, WWP_MESSAGE_MAX_LENGTH, Encoding.UTF8);
    }

    public override void ClientConnected(SecureTcpClient SecureClient)
    {
      base.ClientConnected(SecureClient);
    }

    public override void ClientDisconnected(SecureTcpClient SecureClient)
    {
      // Site disconnected:
      // - Running Transactions will be timeout near in the future ...
      // - Mark Site Session as disconnected

      // ACC 27-FEB-2013 Multisite mode: not execute WWP_SiteProcess (SuperCenter peocesses)
      //WWP_SiteProcess.Stop(SecureClient.InternalId, SecureClient.SessionId);
                                                       
      Common.BatchUpdate.TerminalSession.CloseSession(SecureClient.InternalId,
                                                       SecureClient.SessionId,
                                                       (int)WWP_SessionStatus.Disconnected);

      base.ClientDisconnected(SecureClient);
    }

    public override void XmlMesageReceived(SecureTcpClient SecureTcpClient, string XmlMessage)
    {
      try
      {
        WwpInputMsgProcessTask _wwp_task;
        WWP_Message _wwp_message;
        WWP_MsgTypes _msg_type;
        
        _wwp_message = WWP_Message.CreateMessage(XmlMessage);

        _msg_type = _wwp_message.MsgHeader.MsgType;
        if ( _msg_type.ToString().IndexOf("Reply") >= 0 
            || _wwp_message.MsgHeader.MsgType == WWP_MsgTypes.WWP_MsgError )
        {
          // This is a response message to some request ...
          Transaction.WWP_ResponseReceived(_wwp_message);

          return;
        }

        _wwp_task = new WwpInputMsgProcessTask(this, SecureTcpClient, _wwp_message);

        WWP_WorkerPool.Enqueue(_wwp_task);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Message("EXEP Msg: " + XmlMessage);
      }
    }


    internal class Transaction : IAsyncResult
    {
      private static List<Transaction> m_pending = new List<Transaction>();
      private static Timer m_timer = new Timer(new TimerCallback(Transaction.Timeout), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
      private static Int64 m_center_sequence_id = 1;
      TimeSpan m_tick = new TimeSpan();
      int m_timeout = 30000;
      Boolean m_completed = false;
      Object m_state = null;
      Object m_request = null;
      Object m_response = null;
      ManualResetEvent m_event = new ManualResetEvent(false);

      private static void Timeout(Object Nothing)
      {
        int _remaining;
        int _min_remaining;
        int _elapsed;

        _min_remaining = int.MaxValue;

        try
        {
          lock (m_pending)
          {
            foreach (Transaction _trx in m_pending)
            {
              if (_trx.m_completed)
              {
                continue;
              }

              _elapsed = (int)Performance.GetElapsedTicks(_trx.m_tick).TotalMilliseconds;

              if (_elapsed >= _trx.m_timeout)
              {
                _trx.m_completed = true;
                _trx.m_event.Set();

                continue;
              }

              _remaining = _trx.m_timeout - _elapsed;
              _min_remaining = Math.Min(_min_remaining, _remaining);
            }
          }
        }
        catch
        {
        }
        finally
        {
          if (_min_remaining == int.MaxValue)
          {
            _min_remaining = System.Threading.Timeout.Infinite;
          }
          try
          {
            m_timer.Change(_min_remaining, System.Threading.Timeout.Infinite);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
      }
      
      
      public static bool WWP_EndRequest(IAsyncResult IAResult, out WWP_Message WwpResponse)
      {
        Transaction _pending;

        WwpResponse = null;

        if (IAResult == null || IAResult.AsyncWaitHandle == null)
        {
          return false;
        }

        try
        {
          IAResult.AsyncWaitHandle.WaitOne();

          _pending = (Transaction)IAResult;

          lock (m_pending)
          {
            if (!m_pending.Contains(_pending))
            {
              return false;
            }
            m_pending.Remove(_pending);
          }

          if (_pending.m_response == null)
          {
            return false;
          }

          WwpResponse = (WWP_Message)_pending.m_response;

          return true;
        }
        catch
        {
          return false;
        }
      }


      public Transaction(WWP_Message Request, int Timeout, Object State)
      {
        // AJQ & ACC on 26-APR-2013, Assign a Msg Sequence Id
        Request.MsgHeader.SequenceId = Interlocked.Increment(ref m_center_sequence_id);

        m_request = Request;
        m_timeout = Timeout;
        m_state = State;

        lock (m_pending)
        {
          m_tick = Performance.GetTickCount();
          m_pending.Add(this);
          Transaction.Timeout(null);
        }
      }
      public static IAsyncResult WWP_BeginRequest(WWP_Message Request, int Timeout, Object State)
      {
        Transaction _pending;

        _pending = new Transaction(Request, Timeout, State);


        try
        {
          WWP.Server.SendTo(Request.MsgHeader.SiteId, 0, Request.ToXml());
        }
        catch 
        {
          Log.Error(" Can't send Request to SiteId: " + Request.MsgHeader.SiteId +
                    ", MsgType: " + Request.MsgHeader.MsgType + ".");
        }


        return _pending;
      }

      public static void WWP_ResponseReceived(WWP_Message Response)
      {
        if (Response == null)
        {
          return;
        }

        lock (m_pending)
        {
          foreach (Transaction _trx in m_pending)
          {
            if (_trx.m_request == null)
            {
              // No request ... �?�?
              continue;
            }
            if (_trx.m_response != null)
            {
              // Response already received
              continue;
            }

            WWP_Message _request;
            _request = (WWP_Message)_trx.m_request;

            if (Response.MsgHeader.SiteId == _request.MsgHeader.SiteId
                && Response.MsgHeader.SequenceId == _request.MsgHeader.SequenceId)
            {
              _trx.m_response = Response;
              _trx.m_completed = true;
              _trx.m_event.Set();

              return;
            }
          }
        }
      }

      public object AsyncState
      {
        get { return m_state; }
      }

      public System.Threading.WaitHandle AsyncWaitHandle
      {
        get { return m_event; }
      }

      public bool CompletedSynchronously
      {
        get { return false; }
      }

      public bool IsCompleted
      {
        get { return (m_response != null); }
      }
    }


  } // Class SecureTcpServer

}
