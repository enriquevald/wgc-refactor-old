//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_PendingnrollsQueue.cs
// 
//   DESCRIPTION: WWP_PendingEnrollsQueue class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-FEB-2011 ACC    First release.
// 07-MAY-2013 MPO    Solved defect #770
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WWP;
using System.Collections;

namespace WSI.WWP_Server
{
  /// <summary>
  /// Represents a enroll that is waiting to be processed 
  /// </summary>
  public class EnrollQueueElement
  {
    private WwpInputMsgProcessTask m_wwp_task;

    // Constructor
    public EnrollQueueElement(WwpInputMsgProcessTask Task)
    {
      m_wwp_task = Task;
    }

    //
    // Properties
    //

    public WwpInputMsgProcessTask WwpTask
    {
      get
      {
        return m_wwp_task;
      }
    }

    ////public Object WwpListener
    ////{
    ////  get
    ////  {
    ////    return m_wwp_task.WwpListener;
    ////  }
    ////}

    ////public Object WwpClient
    ////{
    ////  get
    ////  {
    ////    return m_wwp_task.WwpClient;
    ////  }
    ////}

    ////public WWP_Message XmlMessageEnroll
    ////{
    ////  get
    ////  {
    ////    return m_wwp_task.Request;
    ////  }
    ////}
  }

  public class EnrollProcessTask : Task
  {
    WwpInputMsgProcessTask m_wwp_task;

    public EnrollProcessTask(WwpInputMsgProcessTask WwpTask)
    {
      m_wwp_task = WwpTask;
    }

    public override void Execute()
    {
      WWP_PendingEnrollsQueue.EnrollWorker(m_wwp_task);
    }
  }

  /// <summary>
  /// Holds enroll data that awaits to be processed
  /// </summary>
  public class WWP_PendingEnrollsQueue
  {
    private Worker wwp_enroll_worker;
    private WaitableQueue wwp_enroll_waitable_queue;

    private static WWP_PendingEnrollsQueue wwp_pending_enrolls;

    private static List<int> m_unknown_sites = new List<int>();

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize an instance of the WWP_PendingEnrollsQueue class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      wwp_pending_enrolls = new WWP_PendingEnrollsQueue();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the Current instance of the WWP_PendingEnrollsQueue Class
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - instance of the WWP_PendingEnrollsQueue
    //
    //   NOTES :
    //
    public static WWP_PendingEnrollsQueue GetInstance()
    {
      return wwp_pending_enrolls;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Start workers for this tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void StartWorker()
    {
      // Create queue enrolls
      wwp_enroll_waitable_queue = new WaitableQueue();

      // - Message received processing worker
      // Start workers for WWP requests
      //
      // ONLY 1 WORKER for enroll trx !!!
      //
      wwp_enroll_worker = new Worker(wwp_enroll_waitable_queue);
      wwp_enroll_worker.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for start workers
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Start()
    {
      // Workers start
      wwp_pending_enrolls.StartWorker();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueues a enroll element
    //
    //  PARAMS :
    //      - INPUT :
    //         - EnrollElement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void EnqueueEnroll(EnrollQueueElement EnrollElement)
    {
      EnrollProcessTask enroll_process_task;

      // Enqueue DRAW
      enroll_process_task = new EnrollProcessTask(EnrollElement.WwpTask);
      wwp_enroll_waitable_queue.Enqueue(enroll_process_task);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll worker
    //
    //  PARAMS :
    //      - INPUT :
    //         - WwpTask
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void EnrollWorker(WwpInputMsgProcessTask WwpTask)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      SecureTcpClient _client;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      String _str_response;
      Boolean _error;
      Int64 _response_session_id;
      Int32 _response_site_internal_id;

      _wwp_request = WwpTask.Request;

      _client = (SecureTcpClient)WwpTask.WwpClient;

      _sql_conn = null;
      _sql_trx = null;
      _error = true;
      _wwp_response = null;

      try
      {
        _wwp_response = WwpTask.Response;

        _response_session_id = _wwp_request.MsgHeader.SessionId;
        _response_site_internal_id = _client.InternalId;

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();

        // Begin Transaction
        _sql_trx = _sql_conn.BeginTransaction();

        switch (_wwp_request.MsgHeader.MsgType)
        {
          case WWP_MsgTypes.WWP_MsgEnrollSite:
          {
            EnrollSite(_client, _wwp_request, _wwp_response, _sql_trx);
            _response_site_internal_id = _client.InternalId;
            _response_session_id = _client.SessionId;
          }
          break;

          case WWP_MsgTypes.WWP_MsgUnenrollSite:
          {
            UnenrollSite(_client, _wwp_request, _wwp_response, _sql_trx);
          }
          break;

          default:
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_ERROR);
          // Unreachable code detected
          // break;
        }

        _str_response = _wwp_response.ToXml();

        // End Transaction
        _sql_trx.Commit();

        // Send Enroll response
        _client.Send(_str_response);
        NetLog.Add(NetEvent.MessageSent, _client.Identity, _str_response);

        _error = false;
      }
      catch (WWP_Exception wwp_ex)
      {
        String wwp_error_response;
        wwp_error_response = null;
        try
        {
          if (_wwp_response == null)
          {
            _wwp_response = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgError);
          }
          _wwp_response.MsgHeader.ResponseCode = wwp_ex.ResponseCode;

          // Create error response
          wwp_error_response = _wwp_response.ToXml();

          _client.Send(wwp_error_response);
          NetLog.Add(NetEvent.MessageSent, _client.Identity, wwp_error_response);

        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, _client.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, _client.Identity + " b.1)", WwpTask.Request.ToXml());

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_error && _sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }

      } // try - finally

    } // EnrollWorker

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Site
    //
    //  PARAMS :
    //      - INPUT :
    //         - WwpClient
    //         - WwpRequestMessage
    //         - WwpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void EnrollSite(SecureTcpClient WwpClient, WWP_Message WwpRequestMessage, WWP_Message WwpResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 _site_id;
      Int64 _session_id;
      Boolean _enrolled_site;

      WWP_MsgEnrollSite _request_enroll;
      WWP_MsgEnrollSiteReply _response_enroll;

      _request_enroll = (WWP_MsgEnrollSite)WwpRequestMessage.MsgContent;
      _response_enroll = (WWP_MsgEnrollSiteReply)WwpResponseMessage.MsgContent;

      WwpClient.Identity = "SITE-" + WwpRequestMessage.MsgHeader.SiteId.ToString("0000");

      _site_id = WwpRequestMessage.MsgHeader.SiteId;

      // TODO ...
      //_request_enroll.ServerMacAddress
      //_request_enroll.SiteDatabaseVersion
      if ( ! DB_GetEnrollData (_site_id,
                               _request_enroll.SiteName,
                               WwpClient.RemoteEndPoint.Address.ToString(),
                               out _session_id,
                               out _response_enroll.LastSequenceId,
                               out _enrolled_site,
                               SqlTrx))
      {
        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_SITE_NOT_AUTHORIZED);
      }

      if ( _enrolled_site )
      {
        WwpClient.InternalId = _site_id;
        WwpClient.SessionId = _session_id;
        WwpResponseMessage.MsgHeader.SessionId = WwpClient.SessionId;

        // ACC 27-FEB-2013 Multisite mode: not execute WWP_SiteProcess (SuperCenter peocesses)
        //WWP_SiteProcess.Start (_site_id, _session_id);

        Common.BatchUpdate.TerminalSession.AddSession(_site_id,
                                                      _session_id,
                                                      0,
                                                      0,
                                                      _site_id.ToString(),          // WWP Protocol: Site Id
                                                      _request_enroll.SiteName,     // WWP Protocol: Site Name 
                                                      1);                           // WWP Protocol: TerminalType Not used

        WwpResponseMessage.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
      }
      else
      {
        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_SITE_NOT_AUTHORIZED);
      }
    } // EnrollSite



    //------------------------------------------------------------------------------
    // PURPOSE : Insert Site Tasks 
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteId
    //          - Trx
    //
    //      - OUTPUT :
    //          - None
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES : 
    //
    public static Boolean DB_InsertSiteTasks(Int32 SiteId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS (SELECT TOP 1 ST_SITE_ID FROM MS_SITE_TASKS WHERE ST_SITE_ID = @pSiteId AND ST_TASK_ID = @pTaskId)  ");
        _sb.AppendLine("  ");
        _sb.AppendLine(" INSERT MS_SITE_TASKS  ");
        _sb.AppendLine("     (  ST_SITE_ID ");
        _sb.AppendLine("      , ST_TASK_ID ");
        _sb.AppendLine("      , ST_ENABLED ");
        _sb.AppendLine("      , ST_INTERVAL_SECONDS  ) ");
        _sb.AppendLine(" SELECT ");
        _sb.AppendLine("        @pSiteId    ");
        _sb.AppendLine("      , @pTaskId    ");
        _sb.AppendLine("      , ST_ENABLED      ");
        _sb.AppendLine("      , ST_INTERVAL_SECONDS  ");
        _sb.AppendLine("  FROM MS_SITE_TASKS  ");
        _sb.AppendLine(" WHERE ST_SITE_ID = 0 ");
        _sb.AppendLine("   AND ST_TASK_ID = @pTaskId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

          SqlParameter _p = _cmd.Parameters.Add("@pTaskId", SqlDbType.Int);

          foreach (TYPE_MULTISITE_SITE_TASKS _task in Enum.GetValues(typeof(TYPE_MULTISITE_SITE_TASKS)))
          {
            if (_task == TYPE_MULTISITE_SITE_TASKS.Unknown)
            {
              continue;
            }
            _p.Value = (Int32)_task;
            _cmd.ExecuteNonQuery();             
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_InsertSiteTasks

    //------------------------------------------------------------------------------
    // PURPOSE : Get Enroll Data 
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteId
    //          - SiteName
    //          - Trx
    //
    //      - OUTPUT :
    //          - SessionId
    //          - SequenceId
    //          - Enrolled
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES : 
    //
    public static Boolean DB_GetEnrollData (int SiteId,
                                            String SiteName,
                                            String RemoteIP,
                                            out Int64 SessionId,
                                            out Int64 SequenceId,
                                            out Boolean Enrolled,
                                            SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _sql_reader;
      SqlParameter p;
      String _cmd_txt;
      Int64  _dummy_transaction_id;
      Int32  _site_state;
      Int32 _count;
      String _alarm_description;
      String _source_name;

      // Initialize output parameters
      SessionId = 0;
      SequenceId = 0;
      _dummy_transaction_id = 0;
      Enrolled = false;
      _site_state = 0; // Not Active

      _sql_reader = null;
      _sql_cmd = null;

      try
      {
        // 
        // Get Site Id
        //
        _cmd_txt = "";
        _cmd_txt += "SELECT   COUNT(*) ";
        _cmd_txt += "  FROM   SITES ";
        _cmd_txt += " WHERE   ST_SITE_ID =  @pSiteId ";
        _cmd_txt += "   AND   ST_STATE =  @pSiteStatus ";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
        _sql_cmd.Parameters.Add("@pSiteStatus", SqlDbType.Int).Value = SITE_STATE.ENABLED;        

        _count = -1;
        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          _count = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt32(0);
        }

        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
          _sql_reader = null;
        }

        // Site doesn't exist or is not enabled
        if (_count < 1)
        {
          if (!m_unknown_sites.Contains(SiteId))
          {
            //This is the first time this site attempts to connect.  Alarm will be generated
            _alarm_description = Resource.String(Alarm.ResourceId(AlarmCode.MultiSite_UnknownSiteTriedToConnect), SiteId);
            _source_name = Environment.MachineName + @"\\" + WSI.WWP_CenterService.MultiSite.MultiSiteCenterCommon.SERVICE_NAME;

            Alarm.Register(AlarmSourceCode.MultiSite, 0, _source_name, AlarmCode.MultiSite_UnknownSiteTriedToConnect, _alarm_description);

            m_unknown_sites.Add(SiteId);
          }

          return false;
        } // if (_count < 1)

        if (m_unknown_sites.Contains(SiteId))
        {
          //The site is properly inserted on the system.  Remove from unknown sites list.
          m_unknown_sites.Remove(SiteId);
        }

        if (!DB_InsertSiteTasks(SiteId, Trx))
        {
          Log.Error("Inserting into MS_SITE_TASKS table !!!");

          return false;
        }

        ////// Update Site Name
        ////_cmd_txt = "UPDATE SITES SET ST_NAME = @pSiteName WHERE ST_SITE_ID =  @pSiteId ";

        ////_sql_cmd = new SqlCommand(_cmd_txt);
        ////_sql_cmd.Connection = Trx.Connection;
        ////_sql_cmd.Transaction = Trx;

        ////_sql_cmd.Parameters.Add("@pSiteName", SqlDbType.NVarChar, 40, "ST_NAME").Value = SiteName;
        ////_sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int, 4, "ST_SITE_ID").Value = SiteId;

        ////if (_sql_cmd.ExecuteNonQuery() != 1)
        ////{
        ////  Log.Error("Updating SITES table !!!");

        ////  return false;
        ////}

        //
        // Get MAX Last Sequence Id, Transaction Id
        //
        _cmd_txt = "";
        _cmd_txt += "SELECT   MAX (WWS_LAST_SEQUENCE_ID)    ";
        _cmd_txt += "       , MAX (WWS_LAST_TRANSACTION_ID) ";
        _cmd_txt += "       , (SELECT ST_STATE FROM SITES WHERE ST_SITE_ID =  @pSiteId)";
        _cmd_txt += "  FROM   WWP_SESSIONS ";
        _cmd_txt += " WHERE   WWS_SITE_ID  = @pSiteId";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          SequenceId = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt64(0);
          _dummy_transaction_id = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt64(1);
          _site_state = _sql_reader.GetInt32(2);
        }

        _sql_reader.Close();
        _sql_reader.Dispose();
        _sql_reader = null;
        
        if ( _site_state != 1 ) // Active
        {
          return false;
        }

        //
        // Insert New Session
        //
        _cmd_txt = "";
        _cmd_txt += "INSERT INTO WWP_SESSIONS ( WWS_SITE_ID ";
        _cmd_txt += "                         , WWS_LAST_SEQUENCE_ID ";
        _cmd_txt += "                         , WWS_LAST_TRANSACTION_ID ";
        _cmd_txt += "                         , WWS_LAST_RCVD_MSG ";
        _cmd_txt += "                         , WWS_SERVER_NAME ";
        _cmd_txt += "                         )  ";
        _cmd_txt += "                  VALUES ( @pSiteId ";
        _cmd_txt += "                         , @pSequenceId ";
        _cmd_txt += "                         , @pTransactionId ";
        _cmd_txt += "                         , GETDATE() ";
        _cmd_txt += "                         , @pServerName ";
        _cmd_txt += "                         ) ";
        _cmd_txt += "                     SET   @pSessionId = SCOPE_IDENTITY() ";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
        _sql_cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).Value = SequenceId;
        _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = _dummy_transaction_id;
        _sql_cmd.Parameters.Add("@pServerName", SqlDbType.NVarChar).Value = Environment.MachineName;
        p = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
        p.Direction = ParameterDirection.Output;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("Inserting into WWP_SESSIONS table !!!");

          return false;
        }


        _cmd_txt = "";
        _cmd_txt = "UPDATE SITES SET ST_LAST_KNOWN_IP = @pIP WHERE ST_SITE_ID =  @pSiteId";
        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;
        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
        _sql_cmd.Parameters.Add("@pIP", SqlDbType.NVarChar).Value = RemoteIP;
        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("Inserting into WWP_SESSIONS table !!!");

          return false;
        }


        SessionId = (Int64)p.Value;
        
        Enrolled = true;

        //
        // Abandoned sessions: Moved to BatchUpdate
        //

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
        }
        if (_sql_cmd != null)
        {
          _sql_cmd.Dispose();
        }
      }

      return false;
    } // DB_GetEnrollData
    

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Site
    //
    //  PARAMS :
    //      - INPUT :
    //         - WwpClient
    //         - WwpRequestMessage
    //         - WwpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void UnenrollSite(SecureTcpClient WwpClient, WWP_Message WwpRequestMessage, WWP_Message WwpResponseMessage, SqlTransaction SqlTrx)
    {
      Common.BatchUpdate.TerminalSession.CloseSession(WwpClient.InternalId, WwpRequestMessage.MsgHeader.SessionId, (int)WWP_SessionStatus.Closed);
      WwpClient.InternalId = 0;
      WwpClient.SessionId = 0;

    } // UnenrollSite

    #endregion Private Functions

  } // classs WWP_PendingEnrollsQueue

} // namespace WSI.WWP_Server
