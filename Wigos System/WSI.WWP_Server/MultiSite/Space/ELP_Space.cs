﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ELP_Space.cs
// 
//   DESCRIPTION: Create and read headers from Space
// 
//        AUTHOR: Jesús García
// 
// CREATION DATE: -MAR-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-MAR-2018 JGC    First release.
// 26-MAR-2018 JGC    PBI 32313:[WIGOS-9404]: Codere_Mexico: Call to space to get token
// 26-MAR-2018 JGC    PBI 32312:[WIGOS-9407]: Codere_Mexico Space: Use token to call space servicesModification to add token security for calls to Space
//------------------------------------------------------------------------------

#region using

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using WSI.Common;

#endregion

namespace WSI.WWP_CenterService.MultiSite.Space
{
  public class ELP_Space
  {
    #region enums

    public enum HEADER_TYPE
    {
      WITHOUT_TOKEN = 0,
      WITH_TOKEN = 1
    }

    #endregion // enums

    #region members

    public String m_token;
    
    public static String m_namespace_america = string.Empty;
    public static String m_namespace_siebel = string.Empty;
    public static String m_tag_america = string.Empty;
    public static String m_tag_siebel = string.Empty;

    private String m_ws_user = string.Empty;
    private String m_ws_password = string.Empty;
    private String m_header_request = string.Empty;
    private Boolean m_extended_log_active = false;

    #endregion // members

    #region constants

    private const String SPACE_TOKEN_STRING = "SessionToken";
    private const String SPACE_NO_TOKEN_STRING = "UsernameToken";

    private const String SPACE__HEADER__SESSION_TYPE = "Stateless";
    private const String SPACE_HEADER_WITH_TOKEN = @"<soapenv:Header>
                                                      {0}
		                                                </soapenv:Header>";

    private const String SPACE_HEADER_WITHOUT_TOKEN = @"<soapenv:Header>
                                                          <UsernameToken xmlns=""http://siebel.com/webservices"">{0}</UsernameToken>
			                                                    <PasswordText xmlns=""http://siebel.com/webservices"">{1}</PasswordText>
			                                                    <SessionType xmlns=""http://siebel.com/webservices"">{2}</SessionType>
		                                                    </soapenv:Header>";

    private String SPACE_ACTION_POINTS = @"CDR_spcLOY_spcMember_spcPoint";
    private String SPACE_ACTION_REDEEM = @"CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow";
    private String SPACE_ACTION_CONFIRM = @"CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow";

    private const String DEFAULT_USER = @"APPUSERWIN";
    private const String DEFAULT_PASSWORD = @"APPUSERWIN";
    private const String DEFAULT_AMERICA_NAMESPACE = @"ns;http://america.crm.codere.com/xml";
    private const String DEFAULT_SIEBEL_NAMESPACE = @"ns;http://siebel.com/CustomUI";

    #endregion // constants

    #region constructor

    public ELP_Space()
    {
      this.m_token = string.Empty;
      this.m_extended_log_active = ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0);
      this.LoadParams();
    }

    #endregion // constructor

    #region public methods

    /// <summary>
    /// Do a request to SPACE service and get response
    /// </summary>
    /// <param name="HeaderType"></param>
    /// <param name="Input"></param>
    /// <param name="Action"></param>
    /// <param name="Url"></param>
    /// <param name="Timeout"></param>
    /// <param name="Output"></param>
    /// <param name="TokenError"></param>
    /// <returns></returns>
    public Boolean DoWebRequest(HEADER_TYPE HeaderType, String Input, ELP01_Requests Action, String Url, Int32 Timeout, out String Output, out Boolean TokenError)
    {
      string _do_action;
      string _request_namespace;

      _do_action = string.Empty;
      _request_namespace = string.Empty;
      Output = null;
      TokenError = false;

      try
      {
        // Set Header for soap request
        if (!this.GenerateHeaderRequest(HeaderType, out HeaderType))
        {
          Output = "Error generating header.";
          return false;
        }

        // Define the soap action to do
        if (!this.GetSoapAction(Action, out _do_action))
        {
          Output = "Error gettint soap action.";
          return false;
        }

        // Define namespace
        if (!this.GetRequestNamespace(Action, out _request_namespace))
        {
          Output = "Error getting namespace.";
          return false;
        }

        // Do the soap request and get the response from WS
        if (!this.ExecuteWebRequest(Input, _request_namespace,_do_action, Url, Timeout, out Output))
        {
          Output = "Error executing web request.";
          return false;
        }

        // Set token from response if it's necessary
        if (!this.SetTokenFromResponse(HeaderType, Output, out TokenError))
        {
          Output = "Error setting token from response.";
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > ExecuteWebRequest: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // ExecuteWebRequest

    #endregion // public methods

    #region private methods

    /// <summary>
    /// Generate the header for a request
    /// </summary>
    /// <param name="HeaderType"></param>
    /// <returns></returns>
    private Boolean GenerateHeaderRequest(HEADER_TYPE HeaderType, out HEADER_TYPE NewHeaderType)
    {
      Boolean _generate_header;

      NewHeaderType = HeaderType;
      _generate_header = false;

      try
      {

        if (string.IsNullOrEmpty(m_token))
        {
          HeaderType = ELP_Space.HEADER_TYPE.WITHOUT_TOKEN;
        }

        if (string.IsNullOrEmpty(m_header_request))
        {
          _generate_header = true;
        }
        else
        {
          switch (HeaderType)
          {
            case HEADER_TYPE.WITH_TOKEN:
              if (!m_header_request.Contains(SPACE_TOKEN_STRING))
              {
                _generate_header = true;
              }
              break;
            case HEADER_TYPE.WITHOUT_TOKEN:
              if (!m_header_request.Contains(SPACE_NO_TOKEN_STRING))
              {
                _generate_header = true;
              }
              break;
          }
        }

        if (_generate_header)
        {
          switch (HeaderType)
          {
            case HEADER_TYPE.WITH_TOKEN:
              m_header_request = string.Format("{0}", string.Format(SPACE_HEADER_WITH_TOKEN, m_token));
              break;
            case HEADER_TYPE.WITHOUT_TOKEN:
              m_header_request = string.Format("{0}", string.Format(SPACE_HEADER_WITHOUT_TOKEN, m_ws_user, m_ws_password, SPACE__HEADER__SESSION_TYPE));
              break;
          }
        }
        NewHeaderType = HeaderType;      

        if (m_extended_log_active) Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > GenerateHeaderRequest: {0}", NewHeaderType), Log.Type.Message);

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > GenerateHeaderRequest: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // GenerateHeaderRequest


    /// <summary>
    /// Set token from a Response of SPACE service
    /// </summary>
    /// <param name="HeaderType"></param>
    /// <param name="ResponseRequest"></param>
    /// <param name="TokenError"></param>
    /// <returns></returns>
    private Boolean SetTokenFromResponse(HEADER_TYPE HeaderType, String ResponseRequest, out Boolean TokenError)
    {
      XmlDocument _xml_doc;
      XmlNodeList _xml_node_list;

      _xml_doc = new XmlDocument();
      TokenError = false;

      try
      {
        _xml_doc.LoadXml(ResponseRequest);
        _xml_node_list = _xml_doc.SelectNodes("/*/*");

        foreach (XmlNode _node in _xml_node_list)
        {

          // TODO: IN WIGOS-9402 sais token error on header, but in the example is in the body tag
          if (_node.Name.ToLower().Contains("header"))
          {
            // TODO: Temporally literal, now we recive always 505: Internal Error.
            if (!string.IsNullOrEmpty(_node.InnerText))
            {
              if (_node.InnerText.ToLower().Contains("token is missing or invalid or has expired"))
              {
                TokenError = true;

                return false;
              }
              else
              {
                if (HeaderType.Equals(HEADER_TYPE.WITHOUT_TOKEN))
                {
                  m_token = _node.InnerXml;
                }
              }
            }
          }
        }

        if (m_extended_log_active) Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > SetTokenFromResponse: TokenError = {0}, Token = {1}", TokenError, m_token), Log.Type.Message);

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > SetTokenFromResponse: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // SetTokenFromResponse


    /// <summary>
    /// Get soap action for a request
    /// </summary>
    /// <param name="Action"></param>
    /// <param name="DoAction"></param>
    /// <returns></returns>
    private Boolean GetSoapAction(ELP01_Requests Action, out String DoAction)
    {
      DoAction = string.Empty;

      try
      {
        switch (Action)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            DoAction = SPACE_ACTION_POINTS;
            break;
          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            DoAction = SPACE_ACTION_REDEEM;
            break;
          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            DoAction = SPACE_ACTION_CONFIRM;
            break;
          default:
            break;
        }

        if (m_extended_log_active) Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > GetSoapAction: {0}", DoAction), Log.Type.Message);
        
        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > GetSoapAction: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // GetSoapAction


    /// <summary>
    /// Get namespace for a request
    /// </summary>
    /// <param name="Action"></param>
    /// <param name="Namespace"></param>
    /// <returns></returns>
    private Boolean GetRequestNamespace(ELP01_Requests Action, out String Namespace)
    {
      Namespace = string.Empty;

      try
      {
        switch (Action)
        {
          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            Namespace = string.IsNullOrEmpty(m_namespace_america) ? string.Empty : string.Format("{0}:", m_namespace_america);
            break;
          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            Namespace =  string.IsNullOrEmpty(m_namespace_siebel) ? string.Empty : string.Format("{0}:", m_namespace_siebel);
            break;
          default:
            break;
        }

        if (m_extended_log_active) Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > GetRequestNamespace: {0}", Namespace), Log.Type.Message);

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > GetRequestNamespace: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // GetHeaderNamespace


    /// <summary>
    /// Execute the request to SPACE service and get the response
    /// </summary>
    /// <param name="Input"></param>
    /// <param name="DoAction"></param>
    /// <param name="Namespace"></param>
    /// <param name="Url"></param>
    /// <param name="Timeout"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    private Boolean ExecuteWebRequest(String Input, String Namespace, String DoAction, String Url, Int32 Timeout, out String Response)
    {
      Response = string.Empty;

      try
      {
        if (m_extended_log_active) Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > ExecuteWebRequest: Url = {0}", Url), Log.Type.Message);

        // Creates a webRequest with the parameters
        WebRequest webRequest = WebRequest.Create(new Uri(Url));
        webRequest.Headers.Add("SOAPAction", string.Format("\"document/{0}{1}\"", Namespace, DoAction));
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Method = "POST";
        webRequest.Timeout = Timeout;

        // Set Soap Message with header and body
        XmlDocument soapEnvelopeDocument = new XmlDocument();
        soapEnvelopeDocument.LoadXml(string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">
																										{0}
																									<soapenv:Body>
																										{1}
																									</soapenv:Body>
																								</soapenv:Envelope>", m_header_request, Input));

        // Log Request for SPACE
        Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > Request: {0}", soapEnvelopeDocument.InnerXml), Log.Type.Message);

        // Get response from WS of Codere
        using (Stream stream = webRequest.GetRequestStream())
        {
          soapEnvelopeDocument.Save(stream);
        }

        using (WebResponse webResponse = webRequest.GetResponse())
        {
          using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
          {
            Response = rd.ReadToEnd();
            Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > Response: {0}", Response), Log.Type.Message);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        // Log Response for SPACE
        Misc.WriteLog(string.Format("ELP_SPACE > DoWebRequest > failed. Action: {0}, Error: {1}.", DoAction, _ex.Message), Log.Type.Message);
        ELPLog.Error(string.Format("ELP_SPACE > DoWebRequest: failed. Action: {0}, Error: {1}.", DoAction, _ex.Message));
        ELPLog.Exception(_ex);
      }

      return false;
    } // DowWebRequest


    /// <summary>
    /// Load general params and save in local variables
    /// </summary>
    private void LoadParams()
    {
      String _america;
      String _siebel;
      String[] _namespace_america;
      String[] _namespace_siebel;

      try
      {
        m_ws_user = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Username", DEFAULT_USER);
        m_ws_password = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "Password", DEFAULT_PASSWORD);

        _america = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "SpacenameAmerica", DEFAULT_AMERICA_NAMESPACE);
        _siebel = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "SpacenameSiebel", DEFAULT_SIEBEL_NAMESPACE);

        _namespace_america = _america.Split(';');
        _namespace_siebel = _siebel.Split(';');

        m_tag_america = _namespace_america.Count() > 0 ? _namespace_america[0] : string.Empty;
        m_tag_siebel = _namespace_siebel.Count() > 0 ? _namespace_siebel[0] : string.Empty;
        m_namespace_america = _namespace_america.Count() > 0 ? _namespace_america[1] : string.Empty;
        m_namespace_siebel = _namespace_siebel.Count() > 0 ? _namespace_siebel[1] : string.Empty;
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("ELP_SPACE > LoadParams: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }
      
    } // LoadParams

    #endregion // private methods

  } // ELP_Space
}
