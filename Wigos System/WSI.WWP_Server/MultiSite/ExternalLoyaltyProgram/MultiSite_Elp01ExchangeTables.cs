//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_Elp01ExchangeTables.cs
// 
//   DESCRIPTION: External Loyalty Program 01 exchange tables.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
// 03-MAY-2013 JAB    Main functionality implemented
// 13-MAY-2013 JAB    Fixed a bug when logging errors
// 13-JUN-2013 SMN    Fixed bug #846: The ac_last_activity field shouldn't be updated (method "InsertUpdateAccounts")
// 11-OCT-2013 JBP    Using connection pool in YAKs interface. 
// 07-MAR-2014 JBP    Added ThreadDelay for InputThread
// 12-MAR-2014 SMN    Fixed bug #WIG-720: The function Delete_ELP01_PlaySessions is deleting records that have not been transfered to Transaction Interface
// 10-APR-2014 SMN    Update function "ReadAnonymousAccounts" to read the anonymous accounts whose trackdata has been changed.
// 30-JUN-2014 SMN    Fixed bug #WIG-1057: Creation of accounts with Id = 0, partial commit, add new stat to general param
// 18-JUL-2014 SMN    If TitoMode is true, Anonymous thread will be disabled
// 23-JUL-2014 RRR    Changed Log to ELPLog
// 02-SET-2014 SMN    Fixed bug #WIG-1228: More than one center service ELP-Yaks processing  
// 28-OCT-2014 SMN    Added new functionality: BLOCK_REASON field can have more than one block reason.
// 17-MAR-2015 DHA    Added new functionality: Insert Cash Operations to Jaks
// 10-JUL-2015 MPO    WIG-2568: Play sessions duplicated in Space
// 01-SEP-2015 DHA    WIG-2631: insert chips_sales operation on TITO
// 28-SEP-2015 JML    Product Backlog Item 3828: Fiscal - 47
// 09-OCT-2015 FAV    Warnings for the Log file to find errors with data track equal to NULL
// 28-JAN-2016 AMF    PBI 8272: WebService SPACE. Check ELP01_TRACKDATA values
// 15-FEB-2016 AMF    PBI 9361: Dont recharge account blocked
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;
using System.Threading;

namespace WSI.WWP_CenterService.MultiSite
{
  public static class MS_ELP01_ExchangeTables
  {
    #region Enum
    private enum SPACE_REPORTED
    {
      //NULL = Uninformed
      INFORMED = 1, // Trackdata has a correct SpaceId
      INFORMED_AND_DUPLICATED = 2, // Trackdata has a correct SpaceId, but before it had another SpaceId
      UNINFORMED_TIMEOUT = 3 // WS doesn't respond for a 8 hours
    }
    #endregion // Enum

    #region Attributes

    private const Int32 YAKS_INTERFACE_MAX_ROWS = 1000;
    private const Int32 YAKS_INTERFACE_LOOP_COUNT = 20;
    private const Int32 YAKS_INTERFACE_LOOP_MIN_DELAY = 500;

    private static String m_alarm_source_name = "Elp01ExchangeTables";

    private static Boolean m_exists_venue_code_column = false;

    static Int32 m_count_selects = 0;
    static Int64 m_ticks_select = 0;
    static Int32 m_count_procesed = 0;
    static Int64 m_ticks_ws = 0;
    static Int64 m_ticks_save = 0;

    private static int m_stats_log_tick = Misc.GetTickCount();

    #endregion // Attributes

    #region Structs
    public struct ALARM_ERROR
    {
      public StringBuilder alarm_description;
      public Int32 tick_alarm;
    }

    public struct ELP01_PENDING_DOWNLOAD_ACCOUNT
    {
      public Int64 account_id;
      public String track_data;
      public Int32 site_id;
    }

    public struct ELP01_SPACE_REQUEST
    {
      public Int64 AccountId;
      public Int64 ExternalCustomerId;
      public String Trackdata;
      public Boolean Blocked;
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Start Input thread and Outpup thread.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _inputThread;
      Thread _outputThread;
      Thread _AnonymousAccountsThread;

      _inputThread = new Thread(InputThread);
      _inputThread.Name = "InputThread";
      _inputThread.Start();

      _outputThread = new Thread(OutputThread);
      _outputThread.Name = "OutputThread";
      _outputThread.Start();

      _AnonymousAccountsThread = new Thread(AnonymousAccountsThread);
      _AnonymousAccountsThread.Name = "AnonymousAccountsThread";
      _AnonymousAccountsThread.Start();

    }//Init

    // PURPOSE : Read IncCustomerNumber
    //
    //  PARAMS :
    //      - INPUT:
    //          - AccountId
    //          - Trx
    //
    //      - OUTPUT :
    //          - IncCustomerNumber
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean GetIncCustomerNumber(Int64 AccountId, out Int64 IncCustomerNumber)
    {
      Object _obj;
      String sql_str;

      IncCustomerNumber = 0;

      sql_str = " SELECT   ET_EXTERNAL_ACCOUNT_ID " +
                "   FROM   ELP01_TRACKDATA " +
                "  WHERE   ET_AC_ACCOUNT_ID = @pAccountId ";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(sql_str))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

            _obj = _db_trx.ExecuteScalar(_cmd);
          }

          if (_obj != null && _obj != DBNull.Value)
          {
            IncCustomerNumber = (Int64)(Decimal)_obj;

            return true;
          }
        }

        ELPLog.Warning("Wigos->Space, ExternalId not found!, AccountId: " + AccountId.ToString());

        return false;
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // GetIncCustomerNumber

    /// <summary>
    /// Check ELP01 Space Request
    /// </summary>
    /// <param name="SpaceRequest"></param>
    /// <returns></returns>
    public static Boolean CheckSpaceRequest(ref ELP01_SPACE_REQUEST SpaceRequest)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("     SELECT   ET_AC_ACCOUNT_ID                             ");
        _sb.AppendLine("            , ET_EXTERNAL_ACCOUNT_ID                       ");
        _sb.AppendLine("            , ISNULL(ET_EXTERNAL_TRACKDATA, '')            ");
        _sb.AppendLine("            , AC_BLOCKED                                   ");
        _sb.AppendLine("       FROM   ELP01_TRACKDATA                              ");
        _sb.AppendLine(" INNER JOIN   ACCOUNTS ON ET_AC_ACCOUNT_ID = AC_ACCOUNT_ID ");
        _sb.AppendLine("      WHERE   1 = 1                                        ");

        if (SpaceRequest.AccountId != 0)
        {
          _sb.AppendLine("  AND   ET_AC_ACCOUNT_ID       = @pAccountId         ");
        }
        if (SpaceRequest.ExternalCustomerId != 0)
        {
          _sb.AppendLine("  AND   ET_EXTERNAL_ACCOUNT_ID = @pExternalAccountId ");
        }
        if (!String.IsNullOrWhiteSpace(SpaceRequest.Trackdata))
        {
          _sb.AppendLine("  AND   ET_EXTERNAL_TRACKDATA  = @pTrackData         ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            if (SpaceRequest.AccountId != 0)
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = SpaceRequest.AccountId;
            }
            if (SpaceRequest.ExternalCustomerId != 0)
            {
              _sql_cmd.Parameters.Add("@pExternalAccountId", SqlDbType.BigInt).Value = SpaceRequest.ExternalCustomerId;
            }
            if (!String.IsNullOrWhiteSpace(SpaceRequest.Trackdata))
            {
              _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = SpaceRequest.Trackdata;
            }

            using (SqlDataReader _dr = _sql_cmd.ExecuteReader())
            {
              if (_dr.Read())
              {
                SpaceRequest.AccountId = _dr.GetInt64(0);
                SpaceRequest.ExternalCustomerId = (Int64)_dr.GetDecimal(1);
                SpaceRequest.Trackdata = _dr.GetString(2);
                SpaceRequest.Blocked = _dr.GetBoolean(3);

                if (!SpaceRequest.Blocked)
                {
                  return true;
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // CheckSpaceRequest

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Manage Play Sessions in External Loyalty Program
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void OutputThread()
    {
      SqlTransaction _sql_trans_sql_yaks;
      DataSet _ds_play_sessions;
      Int32 _wait_hint;

      _ds_play_sessions = null;
      _wait_hint = 0;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 30000;

        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 1)
        {
          continue;
        }

        if (!Services.IsPrincipal("WWP CENTER"))
        {
          continue;
        }

        using (SqlConnection _sql_conn_yaks = new SqlConnection(YaksConnectionString()))
        {
          try
          {
            _sql_conn_yaks.Open();

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!Read_ELP01_PlaySessions(out _ds_play_sessions, _db_trx.SqlTransaction))
              {
                ELPLog.Error(" *** Read_ELP01_PlaySessions failed. ROLLBACK.");

              } // if

              if (_ds_play_sessions != null
                  && _ds_play_sessions.Tables.Count == 0
                  && _ds_play_sessions.Tables[0].Rows.Count == 0)
              {
                _wait_hint = 2000;

              }

              _sql_trans_sql_yaks = _sql_conn_yaks.BeginTransaction();

              if (!InsertTransactionInterface(_ds_play_sessions, _sql_trans_sql_yaks))
              {
                ELPLog.Error(" *** InsertTransactionInterface failed. ROLLBACK.");

              } // if

              if (!Delete_ELP01_PlaySessions(_ds_play_sessions, _db_trx.SqlTransaction))
              {
                ELPLog.Error(" *** Delete_ELP01_PlaySessions failed. ROLLBACK.");

              } // if

              _sql_trans_sql_yaks.Commit();
              _db_trx.Commit();


            } // DB_TRX

          } // try
          catch (Exception _ex)
          {
            Log.Message("CONNECTION STRING: " + YaksConnectionString());

            ELPLog.Exception(_ex);
          }
        } // end using SQLConnection

        // DHA 17-MAR-2015: added cash interface
        if (!SendToYaks_CashInterface())
        {
          continue;
        }

        _wait_hint = 2000;

      } // while true

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Manage cash operations in External Loyalty Program
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean SendToYaks_CashInterface()
    {
      DataSet _dt_task68;
      String _filter_table;
      DataSet _filtered_data;

      _dt_task68 = null;
      _filter_table = String.Empty;
      _filtered_data = new DataSet();

      using (SqlConnection _sql_conn_yaks = new SqlConnection(YaksConnectionString()))
      {
        try
        {
          _sql_conn_yaks.Open();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!ReadTask68WorkData(out _dt_task68, _db_trx.SqlTransaction))
            {
              ELPLog.Error(" *** Read_Task68_work_data failed. ROLLBACK.");
              return false;

            } // if

            if (_dt_task68 != null
                && _dt_task68.Tables.Count == 0
                && _dt_task68.Tables[0].Rows.Count == 0)
            {
              return true;
            }

            using (SqlTransaction _sql_trans_sql_yaks = _sql_conn_yaks.BeginTransaction())
            {
              if (!InsertTransactionCashInterface(_dt_task68, _sql_trans_sql_yaks))
              {
                ELPLog.Error(" *** InsertTransactionInterfaceTask68 failed. ROLLBACK.");
                return false;

              } // if

              if (!DeleteELP01Task68(_dt_task68, _db_trx.SqlTransaction))
              {
                ELPLog.Error(" *** Delete_ELP01_task68 failed. ROLLBACK.");
                return false;
              } // if

              _sql_trans_sql_yaks.Commit();
            }

            _db_trx.Commit();

            return true;
          } // DB_TRX

        } // try
        catch (Exception _ex)
        {
          Log.Message("CONNECTION STRING: " + YaksConnectionString());

          ELPLog.Exception(_ex);
        }

        return false;
      } // end using SQLConnection

    } // SendToYaks_CashInterface

    private static bool DeleteELP01Task68(DataSet DSTask68, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      try
      {
        if (DSTask68 != null
          && DSTask68.Tables.Count > 0
          && DSTask68.Tables[0].Rows.Count > 0)
        {
          foreach (DataRow _dr_task68 in DSTask68.Tables[0].Rows)
          {
            _dr_task68.Delete();
          }

          _sb = new StringBuilder();
          _sb.Append("UPDATE  TASK68_WORK_DATA ");
          _sb.Append("   SET  TWD_SENT_TO_SPACE = 1 "); // Mark as sent
          _sb.Append("      WHERE  TWD_SITE_ID = @pTWD_SITE_ID ");
          _sb.Append("        AND  TWD_OPERATION_ID = @pTWD_ID ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Add("@pTWD_SITE_ID", SqlDbType.BigInt).SourceColumn = "TWD_SITE_ID";
            _sql_cmd.Parameters.Add("@pTWD_ID", SqlDbType.BigInt).SourceColumn = "TWD_OPERATION_ID";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.DeleteCommand = _sql_cmd;
              _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.ContinueUpdateOnError = false;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.Update(DSTask68.Tables[0]);

            } // SqlDataAdapter
          } // SqlCommand
        } // if

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }

    private static bool InsertTransactionCashInterface(DataSet DSTask68, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;
      Int32 _row_number;
      String _alarm_description;
      Int32 _count_rows_not_to_insert;

      _count_rows_not_to_insert = 0;

      try
      {
        _sb = new StringBuilder();
        if (DSTask68 != null
           && DSTask68.Tables.Count > 0
           && DSTask68.Tables[0].Rows.Count > 0)
        {
          DSTask68.Tables[0].AcceptChanges();
          // Mark rows as added.          
          foreach (DataRow _dr_task68 in DSTask68.Tables[0].Rows)
          {
            //Cashless --> Important Not insert Operations CHIPS_SALE and CHIPS_PURCHASE
            if (!WSI.Common.TITO.Utils.IsTitoMode() && (Int32)_dr_task68["TWD_OPERATION_TYPE"] != (Int32)OperationCode.CHIPS_SALE && (Int32)_dr_task68["TWD_OPERATION_TYPE"] != (Int32)OperationCode.CHIPS_PURCHASE)
            {
              _dr_task68.SetAdded();
            }
            //TITO --> Important Not insert Operations CHIPS_SALE
            else if (WSI.Common.TITO.Utils.IsTitoMode() && (Int32)_dr_task68["TWD_OPERATION_TYPE"] != (Int32)OperationCode.CHIPS_SALE)
            {
              _dr_task68.SetAdded();
            }
            else
            {
              _count_rows_not_to_insert++;
            }
          }

          _sb.Append(" IF EXISTS(SELECT TICKETNUMBER FROM CASHINTERFACE WHERE TICKETNUMBER = @pTicketNumber AND VENUECODE = @pVenueCode) ");
          _sb.Append("    UPDATE   CASHINTERFACE");
          _sb.Append("       SET   INCCUSTOMERNUMBER           = @pIncCustomerNumber ");
          _sb.Append("           , INCWINNUMBER                = @pAccountId ");
          _sb.Append("           , INCCARDNUMBER               = @pIncCardNumber ");
          _sb.Append("           , FECHAHORAEVENTO             = @pFechaHoraEvento ");
          _sb.Append("           , TIPOEVENTO                  = @pTipoEvento ");
          _sb.Append("           , RECARGATOTAL                = ISNULL(@pRecargaTotal, 0) ");
          _sb.Append("           , RECARGAEMPRESAA             = ISNULL(@pRecargaEmpresaA, 0) ");
          _sb.Append("           , RECARGAEMPRESAB             = ISNULL(@pRecargaEmpresaB, 0) ");
          _sb.Append("           , RECARGAIMPUESTO1            = ISNULL(@pRecargaImpuesto1, 0) ");
          _sb.Append("           , RECARGAIMPUESTO2            = ISNULL(@pRecargaImpuesto2, 0) ");
          _sb.Append("           , RECARGAMODODEPAGO           = ISNULL(@pRecargaModoDePago, 0) ");
          _sb.Append("           , RETIROTOTAL                 = ISNULL(@pRetiroTotal, 0) ");
          _sb.Append("           , RETIRODEVOLUCION            = ISNULL(@pRetiroDevolucion, 0) ");
          _sb.Append("           , RETIROPREMIO                = ISNULL(@pRetiroPremio, 0) ");
          _sb.Append("           , RETIROIMPUESTO1             = ISNULL(@pRetiroImpuesto1, 0) ");
          _sb.Append("           , RETIROIMPUESTO2             = ISNULL(@pRetiroImpuesto2, 0) ");
          _sb.Append("           , RETIROCARGOPORSERVICIO      = ISNULL(@pRetiroCargoPorServicio, 0) ");
          _sb.Append("           , RETIROREDONDEOPORDECIMALES  = ISNULL(@pRetiroRedondeoPorDecimales, 0) ");
          _sb.Append("           , RETIROREDONDEOPORRETENCION  = ISNULL(@pRetiroRedondeoPorRetencion, 0) ");
          _sb.Append("           , RETIROCONSTANCIA            = CASE WHEN @pRetiroConstancia = 1 THEN 'Y' ELSE 'N' END ");
          _sb.Append("           , RETIROTOTALPAGADOENEFECTIVO = ISNULL(@pRetiroTotalPagadoEnEfectivo, 0) ");
          _sb.Append("           , RETIROTOTALPAGADOENCHEQUE   = ISNULL(@pRetiroTotalPagadoEnCheque, 0) ");
          _sb.Append("           , DATEUPDATED                 = GETDATE() ");
          _sb.Append("     WHERE   TICKETNUMBER                = @pTicketNumber  ");
          _sb.Append("       AND   VENUECODE                   = @pVenueCode     ");
          _sb.Append(" ELSE");
          _sb.Append("    INSERT INTO   CASHINTERFACE ");
          _sb.Append("                ( TICKETNUMBER ");
          _sb.Append("                , INCCUSTOMERNUMBER ");
          _sb.Append("                , INCWINNUMBER");
          _sb.Append("                , INCCARDNUMBER ");
          _sb.Append("                , VENUECODE ");
          _sb.Append("                , FECHAHORAEVENTO ");
          _sb.Append("                , TIPOEVENTO ");
          _sb.Append("                , RECARGATOTAL ");
          _sb.Append("                , RECARGAEMPRESAA ");
          _sb.Append("                , RECARGAEMPRESAB ");
          _sb.Append("                , RECARGAIMPUESTO1 ");
          _sb.Append("                , RECARGAIMPUESTO2 ");
          _sb.Append("                , RECARGAMODODEPAGO ");
          _sb.Append("                , RETIROTOTAL ");
          _sb.Append("                , RETIRODEVOLUCION ");
          _sb.Append("                , RETIROPREMIO ");
          _sb.Append("                , RETIROIMPUESTO1 ");
          _sb.Append("                , RETIROIMPUESTO2 ");
          _sb.Append("                , RETIROCARGOPORSERVICIO ");
          _sb.Append("                , RETIROREDONDEOPORDECIMALES ");
          _sb.Append("                , RETIROREDONDEOPORRETENCION ");
          _sb.Append("                , RETIROCONSTANCIA ");
          _sb.Append("                , RETIROTOTALPAGADOENEFECTIVO ");
          _sb.Append("                , RETIROTOTALPAGADOENCHEQUE ");
          _sb.Append("                , DATEUPDATED) ");
          _sb.Append("         VALUES ");
          _sb.Append("                ( @pTicketNumber ");
          _sb.Append("                , @pIncCustomerNumber ");
          _sb.Append("                , @pAccountId ");
          _sb.Append("                , @pIncCardNumber ");
          _sb.Append("                , @pVenueCode ");
          _sb.Append("                , @pFechaHoraEvento ");
          _sb.Append("                , @pTipoEvento ");
          _sb.Append("                , ISNULL(@pRecargaTotal, 0) ");
          _sb.Append("                , ISNULL(@pRecargaEmpresaA, 0) ");
          _sb.Append("                , ISNULL(@pRecargaEmpresaB, 0) ");
          _sb.Append("                , ISNULL(@pRecargaImpuesto1, 0) ");
          _sb.Append("                , ISNULL(@pRecargaImpuesto2, 0) ");
          _sb.Append("                , ISNULL(@pRecargaModoDePago, 0) ");
          _sb.Append("                , ISNULL(@pRetiroTotal, 0) ");
          _sb.Append("                , ISNULL(@pRetiroDevolucion, 0) ");
          _sb.Append("                , ISNULL(@pRetiroPremio, 0) ");
          _sb.Append("                , ISNULL(@pRetiroImpuesto1, 0) ");
          _sb.Append("                , ISNULL(@pRetiroImpuesto2, 0) ");
          _sb.Append("                , ISNULL(@pRetiroCargoPorServicio, 0) ");
          _sb.Append("                , ISNULL(@pRetiroRedondeoPorDecimales, 0) ");
          _sb.Append("                , ISNULL(@pRetiroRedondeoPorRetencion, 0) ");
          _sb.Append("                , CASE WHEN @pRetiroConstancia = 1 THEN 'Y' ELSE 'N' END ");
          _sb.Append("                , ISNULL(@pRetiroTotalPagadoEnEfectivo, 0) ");
          _sb.Append("                , ISNULL(@pRetiroTotalPagadoEnCheque, 0) ");
          _sb.Append("                , GETDATE()) ");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.Decimal).SourceColumn = "TWD_OPERATION_ID";
            _sql_cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.Decimal).SourceColumn = "TWD_EXTERNAL_REFERENCE";
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "TWD_ACCOUNT_ID";
            _sql_cmd.Parameters.Add("@pIncCardNumber", SqlDbType.Decimal).SourceColumn = "TWD_EXTERNAL_CARD_NUMBER";
            _sql_cmd.Parameters.Add("@pVenueCode", SqlDbType.Decimal).SourceColumn = "TWD_SITE_ID";
            _sql_cmd.Parameters.Add("@pFechaHoraEvento", SqlDbType.DateTime).SourceColumn = "TWD_DATETIME";
            _sql_cmd.Parameters.Add("@pTipoEvento", SqlDbType.Decimal).SourceColumn = "TWD_OPERATION_TYPE";
            _sql_cmd.Parameters.Add("@pRecargaTotal", SqlDbType.Decimal).SourceColumn = "TWD_TOTAL_CASH_IN";
            _sql_cmd.Parameters.Add("@pRecargaEmpresaA", SqlDbType.Decimal).SourceColumn = "TWD_CASH_IN_SPLIT1";
            _sql_cmd.Parameters.Add("@pRecargaEmpresaB", SqlDbType.Decimal).SourceColumn = "TWD_CASH_IN_SPLIT2";
            _sql_cmd.Parameters.Add("@pRecargaImpuesto1", SqlDbType.Decimal).SourceColumn = "TWD_CASH_IN_TAX1";
            _sql_cmd.Parameters.Add("@pRecargaImpuesto2", SqlDbType.Decimal).SourceColumn = "TWD_CASH_IN_TAX2";
            _sql_cmd.Parameters.Add("@pRecargaModoDePago", SqlDbType.Decimal).SourceColumn = "TWD_PAYMENT_TYPE_CODE";
            _sql_cmd.Parameters.Add("@pRetiroTotal", SqlDbType.Decimal).SourceColumn = "TWD_TOTAL_CASH_OUT";
            _sql_cmd.Parameters.Add("@pRetiroDevolucion", SqlDbType.Decimal).SourceColumn = "TWD_DEVOLUTION";
            _sql_cmd.Parameters.Add("@pRetiroPremio", SqlDbType.Decimal).SourceColumn = "TWD_PRIZE";
            _sql_cmd.Parameters.Add("@pRetiroImpuesto1", SqlDbType.Decimal).SourceColumn = "TWD_ISR1";
            _sql_cmd.Parameters.Add("@pRetiroImpuesto2", SqlDbType.Decimal).SourceColumn = "TWD_ISR2";
            _sql_cmd.Parameters.Add("@pRetiroCargoPorServicio", SqlDbType.Decimal).SourceColumn = "TWD_SERVICE_CHARGE";
            _sql_cmd.Parameters.Add("@pRetiroRedondeoPorDecimales", SqlDbType.Decimal).SourceColumn = "TWD_DECIMAL_ROUNDING";
            _sql_cmd.Parameters.Add("@pRetiroRedondeoPorRetencion", SqlDbType.Decimal).SourceColumn = "TWD_RETENTION_ROUNDING";
            _sql_cmd.Parameters.Add("@pRetiroConstancia", SqlDbType.Bit).SourceColumn = "TWD_EVIDENCE";
            _sql_cmd.Parameters.Add("@pRetiroTotalPagadoEnEfectivo", SqlDbType.Decimal).SourceColumn = "TWD_CASH_OUT_MONEY";
            _sql_cmd.Parameters.Add("@pRetiroTotalPagadoEnCheque", SqlDbType.Decimal).SourceColumn = "TWD_CASH_OUT_CHECK";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.ContinueUpdateOnError = true;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

              _row_number = _sql_da.Update(DSTask68);

              if (_row_number != DSTask68.Tables[0].Rows.Count - _count_rows_not_to_insert)
              {
                ELPLog.Message("MultiSite_Elp01ExchangeTables - InsertTransactionCashInterface - " + _row_number.ToString() + " of " + (DSTask68.Tables[0].Rows.Count - _count_rows_not_to_insert).ToString() + " records inserted/updated.");

                foreach (DataRow _dr in DSTask68.Tables[0].Rows)
                {
                  //
                  // Alarm: Error Inserting into CashInterface the Task 68
                  //
                  if (_dr.HasErrors)
                  {
                    ELPLog.Message("     *** Error: " + _dr.RowError);

                    _alarm_description = Resource.String("STR_ELP_ALARM_INSERT_TRANSACION_INTERFACE_TASK_68"
                                                       , _dr["TWD_SITE_ID"]
                                                       , _dr["TWD_OPERATION_ID"]
                                                       , _dr.RowError);
                    Alarm.Register(AlarmSourceCode.ELP01
                                 , 0
                                 , m_alarm_source_name
                                 , AlarmCode.ELP_ErrorInsertCashInterface
                                 , _alarm_description);
                  }
                }

                if (DSTask68.Tables[0].HasErrors)
                {
                  RemoveErrorsDataset(DSTask68);
                }
              }
            }
          }
        }

        if (GeneralParam.GetBoolean("PlayerTracking.ExternalLoyaltyProgram", "ShowWarningChipsOperationsNotInserted", true) && _count_rows_not_to_insert > 0)
        {
          if (!WSI.Common.TITO.Utils.IsTitoMode())
          {
            Log.Error(String.Format("{0} operations of types CHIPS_SALE or CHIPS_PURCHASE have not been inserted.", _count_rows_not_to_insert));
          }
          else
          {
            Log.Error(String.Format("{0} operations of types CHIPS_SALE have not been inserted.", _count_rows_not_to_insert));
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }

    private static bool ReadTask68WorkData(out DataSet DSTask68, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      DSTask68 = new DataSet("TASK_68");
      try
      {

        // TODO: Se debe refactorizar ya que no es necesario recuperar todos los campos...
        _sb = new StringBuilder();
        _sb.Append("    SELECT   TOP (" + YAKS_INTERFACE_MAX_ROWS.ToString() + ") ");
        _sb.Append("             TWD_SITE_ID ");
        _sb.Append("           , TWD_OPERATION_ID ");
        _sb.Append("           , TWD_DATETIME ");
        _sb.Append("           , TWD_ACCOUNT_ID ");
        _sb.Append("           , TWD_EXTERNAL_REFERENCE ");
        _sb.Append("           , TWD_EXTERNAL_CARD_NUMBER ");
        _sb.Append("           , TWD_OPERATION_TYPE ");
        _sb.Append("           , TWD_TOTAL_CASH_IN ");
        _sb.Append("           , TWD_CASH_IN_SPLIT1 ");
        _sb.Append("           , TWD_CASH_IN_SPLIT2 ");
        _sb.Append("           , TWD_CASH_IN_TAX1 ");
        _sb.Append("           , TWD_CASH_IN_TAX2 ");
        _sb.Append("           , TWD_TOTAL_CASH_OUT ");
        _sb.Append("           , TWD_DEVOLUTION ");
        _sb.Append("           , TWD_PRIZE ");
        _sb.Append("           , TWD_ISR1 ");
        _sb.Append("           , TWD_ISR2 ");
        _sb.Append("           , TWD_CASH_OUT_MONEY ");
        _sb.Append("           , TWD_CASH_OUT_CHECK ");
        _sb.Append("           , TWD_RFC ");
        _sb.Append("           , TWD_CURP ");
        _sb.Append("           , TWD_NAME ");
        _sb.Append("           , TWD_ADDRESS ");
        _sb.Append("           , TWD_CITY ");
        _sb.Append("           , TWD_POSTAL_CODE ");
        _sb.Append("           , TWD_EVIDENCE_RFC ");
        _sb.Append("           , TWD_EVIDENCE_CURP ");
        _sb.Append("           , TWD_EVIDENCE_NAME ");
        _sb.Append("           , TWD_EVIDENCE_ADDRES ");
        _sb.Append("           , TWD_EVIDENCE_CITY ");
        _sb.Append("           , TWD_EVIDENCE_POSTAL_CODE ");
        _sb.Append("           , TWD_EVIDENCE ");
        _sb.Append("           , TWD_DOCUMENT_ID ");
        _sb.Append("           , TWD_RETENTION_ROUNDING ");
        _sb.Append("           , TWD_SERVICE_CHARGE ");
        _sb.Append("           , TWD_DECIMAL_ROUNDING ");
        _sb.Append("           , TWD_CASHIER_SESSION_ID ");
        _sb.Append("           , TWD_PRIZE_ABOVE ");
        _sb.Append("           , TWD_CASHIER_NAME ");
        _sb.Append("           , TWD_PRIZE_EXPIRED ");
        _sb.Append("           , TWD_VOUCHER_ID ");
        _sb.Append("           , TWD_FOLIO ");
        _sb.Append("           , TWD_PROMOTIONAL_AMOUNT ");
        _sb.Append("           , TWD_PAYMENT_TYPE_CODE ");
        _sb.Append("           , TWD_PAYMENT_TYPE ");
        _sb.Append("           , TWD_RECHARGE_POINTS_PRIZE ");
        _sb.Append("      FROM   TASK68_WORK_DATA ");
        _sb.Append("     WHERE   TWD_SENT_TO_SPACE = 0 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DSTask68);
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // OutputThread


    private static Boolean NonPrincipal_NotifyAccounts()
    {
      SecureTcpClient _client;
      DataTable _accounts;
      Int32 _prev_site_id;
      Int32 _site_id;
      WWP_Message _wwp_message;
      WWP_MsgCreateAccounts.AccountList _account_list;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!ReadAndDeleteAccountPendingDownload(out _accounts, _db_trx.SqlTransaction))
          {
            ELPLog.Error(" *** ReadAndDeleteAccountPendingDownload failed.");

            return false;
          }
          _db_trx.Commit();
        } // using

        if (_accounts.Rows.Count == 0)
        {
          return true;
        }

        _prev_site_id = -1;
        _client = null;
        _wwp_message = null;
        _account_list = null;

        foreach (DataRow _row in _accounts.Rows)
        {
          _site_id = (Int32)_row["EA_SITE_ID"];
          if (_site_id != _prev_site_id)
          {
            if (_client != null)
            {
              _client.Send(_wwp_message.ToXml());
            }

            _client = null;
            foreach (SecureTcpClient _aux in WWP_Server.WWP.Server.Clients)
            {
              if (_aux.InternalId == _site_id)
              {
                if (_aux.IsConnected)
                {
                  _prev_site_id = _site_id;
                  WWP_MsgCreateAccounts _wwp_accounts;

                  _client = _aux;
                  _wwp_message = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgCreateAccounts);
                  _wwp_accounts = (WWP_MsgCreateAccounts)_wwp_message.MsgContent;
                  _account_list = _wwp_accounts.List;
                }
                break;
              }
            }
          }

          if (_client == null)
          {
            continue;
          }

          // Add Account/Track to list
          _account_list.Add((Int64)_row["EA_ACCOUNT_ID"], (String)_row["EA_TRACKDATA"]);

        } // foreach

        if (_client != null)
        {
          _client.Send(_wwp_message.ToXml());
        }

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }

    private static Boolean ProcessLoyaltyCardInterface(out Boolean CardsReaded)
    {
      SqlTransaction _sql_trans_sql_conn_yaks;
      Boolean _exist_venue_code_checked;
      Int32 _tick_exists_venue_code;
      DataSet _ds_loyalty_card;
      DataTable _dt_loyalty_card_send_to_site;
      ALARM_ERROR _last_alarm;

      _last_alarm.alarm_description = new StringBuilder("");
      _last_alarm.tick_alarm = 0;
      _ds_loyalty_card = null;
      _dt_loyalty_card_send_to_site = null;
      _tick_exists_venue_code = Misc.GetTickCount();
      _exist_venue_code_checked = false;
      CardsReaded = false;
      _sql_trans_sql_conn_yaks = null;

      //
      // Loyalty Card Interface
      //
      using (SqlConnection _sql_conn_yaks = new SqlConnection(YaksConnectionString()))
      {
        try
        {
          _sql_conn_yaks.Open();
          _sql_trans_sql_conn_yaks = _sql_conn_yaks.BeginTransaction();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!m_exists_venue_code_column && (!_exist_venue_code_checked || Misc.GetElapsedTicks(_tick_exists_venue_code) > 5 * 60 * 1000))
            {
              _tick_exists_venue_code = Misc.GetTickCount();

              if (!CheckForVenueCode(_sql_trans_sql_conn_yaks))
              {
                ELPLog.Error(" *** CheckForVenueCode failed.");
              }
              else
              {
                _exist_venue_code_checked = true;
              }
            }

            if (!Read_LoyaltyCardInterface(out _ds_loyalty_card, _sql_trans_sql_conn_yaks))
            {
              ELPLog.Error(" *** Read_LoyaltyCardInterface failed. ROLLBACK.");

              return false;
            } // if

            if (!Delete_LoyaltyCardInterface(_ds_loyalty_card, _sql_trans_sql_conn_yaks))
            {
              return false;
            } // if


            if (_ds_loyalty_card.Tables[0].Rows.Count > 0)
            {
              CardsReaded = true;

              if (!ValidateTrackData(_ds_loyalty_card))
              {
                ELPLog.Error(" *** ValidateTrackData failed. ROLLBACK.");

                return false;
              } // if

              if (!UsedTrackData(_ds_loyalty_card, _db_trx.SqlTransaction))
              {
                ELPLog.Error(" *** UsedTrackData failed. ROLLBACK.");

                return false;
              } // if

              if (!UpdateAccountsTrackData(_ds_loyalty_card, _db_trx.SqlTransaction, _sql_trans_sql_conn_yaks, ref _last_alarm))
              {
                ELPLog.Error(" *** UpdateAccountsTrackData failed. ROLLBACK.");

                return false;
              } // if

              _dt_loyalty_card_send_to_site = _ds_loyalty_card.Tables[0].Copy();


            } // if _ds_loyalty_card.Tables[0].Rows.Count > 0

            _sql_trans_sql_conn_yaks.Commit();
            _db_trx.Commit();

          } // DB_TRX

          //Send: SiteID, Accountid, Tarckdata
          if (!NotifyToSite(_dt_loyalty_card_send_to_site))
          {
            ELPLog.Error(" *** NotifyToSite failed.");

          } // NotifyToSite


        } //try
        catch (Exception _ex)
        {
          Log.Message("CONNECTION STRING: " + YaksConnectionString());

          throw _ex;
        }

        return true;
      } // end connection using
    }

    private static Boolean ProcessMemberInterface()
    {
      SqlTransaction _sql_trans_sql_conn_yaks;
      DataSet _ds_member_interface;

      _sql_trans_sql_conn_yaks = null;
      _ds_member_interface = null;

      //
      // Member Interface
      //
      using (SqlConnection _sql_conn_yaks = new SqlConnection(YaksConnectionString()))
      {
        try
        {
          _sql_conn_yaks.Open();

          _sql_trans_sql_conn_yaks = _sql_conn_yaks.BeginTransaction();

          using (DB_TRX _db_trx = new DB_TRX())
          {

            if (!Read_MemberInterface(out _ds_member_interface, _sql_trans_sql_conn_yaks))
            {
              ELPLog.Error(" *** Read_MemberInterface failed. ROLLBACK.");

              return false;
            } // if


            // If not exists MemberInterface: continue
            if (_ds_member_interface.Tables[0].Rows.Count == 0)
            {
              return true;
            }

            // If exists LoyaltyCardInterface: continue
            if (ExistsLoyaltyCardInterface(_sql_trans_sql_conn_yaks))
            {

              return true;
            }

            if (!Delete_MemberInterface(_ds_member_interface, _sql_trans_sql_conn_yaks))
            {
              return false;
            }

            // Process accounts whose status in MemberInterface is 2.
            if (!CancelAccounts(_ds_member_interface, _db_trx.SqlTransaction))
            {
              return false;
            }

            if (_ds_member_interface.Tables[0].Rows.Count > 0)
            {
              if (!InsertUpdateAccounts(_ds_member_interface, _db_trx.SqlTransaction))
              {
                ELPLog.Error(" *** InsertUpdateAccounts failed. ROLLBACK.");

                return false;
              }
            }



            _sql_trans_sql_conn_yaks.Commit();
            _db_trx.Commit();

          } // DB_TRX

        } // try
        catch (Exception _ex)
        {
          Log.Message("CONNECTION STRING: " + YaksConnectionString());

          throw _ex;
        }

      } // end connection using

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Manage External Accounts and External Cards in External Loyalty Program
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void InputThread()
    {
      Int32 _wait_hint;
      long _elapsed;
      int _tick_loop;
      int _min_wait;
      Boolean _cards_readed;

      _wait_hint = 0;
      _tick_loop = Misc.GetTickCount();

      while (true)
      {
        _min_wait = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "SleepReadYaksInterface", YAKS_INTERFACE_LOOP_MIN_DELAY, 100, 2500);

        try
        {
          // ThreadDelay
          _elapsed = Misc.GetElapsedTicks(_tick_loop);
          if (_elapsed < _wait_hint)
          {
            _wait_hint = (int)((long)_wait_hint - _elapsed);
            Thread.Sleep(_wait_hint);

            _wait_hint = 5000; // Default wait
          }

          _tick_loop = Misc.GetTickCount();

          if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 1)
          {
            _wait_hint = 60000; // Wait 1 minute on non ELP system

            continue;
          }
          if (!Services.IsPrincipal("WWP CENTER"))
          {
            if (!NonPrincipal_NotifyAccounts())
            {
              ELPLog.Error("NonPrincipal_NotifyAccounts failed");
            }

            continue;
          }
          if (!ProcessLoyaltyCardInterface(out _cards_readed))
          {
            continue;
          }

          if (_cards_readed)
          {
            _wait_hint = _min_wait;

            continue;
          }

          if (!Services.IsPrincipal("WWP CENTER"))
          {
            continue;
          }

          if (!ProcessMemberInterface())
          {
            continue;
          }

          _wait_hint = _min_wait;

        }
        catch (Exception _ex)
        {
          ELPLog.Exception(_ex);

          _wait_hint = 10000; // Default wait on exception

          continue;
        }
      } // while True

    }// InputThread

    //------------------------------------------------------------------------------
    // PURPOSE : New accounts 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void AnonymousAccountsThread()
    {
      DataTable _dt_anonymous_accounts;
      Int32 _send_thread_sleep_time;

      Int64 _account_id;
      String _external_track_data;
      Int32 _site_id;
      Int64? _space_account_id;

      Int32 _tick_select = 0;
      Int32 _tick_ws = 0;
      Int32 _tick_save = 0;
      Boolean _trackdata_changed;
      DateTime _last_updated;

      Int32 _error_control_sleep_time;

      _dt_anonymous_accounts = null;
      _send_thread_sleep_time = 2000;
      _error_control_sleep_time = 2000;

      while (true)
      {
        Thread.Sleep(_send_thread_sleep_time);
        _send_thread_sleep_time = 30000;
        _error_control_sleep_time = 2000;

        // Only the service running as 'Principal' will excute the tasks.
        if (!Services.IsPrincipal("WWP CENTER"))
        {
          continue;
        }

        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 1)
        {
          continue;
        }

        // The thread is only enabled if IsTitoMode is false
        if (WSI.Common.TITO.Utils.IsTitoMode())
        {
          continue;
        }

        // If NotifySpaceChangesInAnonymousAccounts param has value = 0, then disable AnonymousAccountsThread
        // Initially this parameter was only used for indicates changes in anonymous accounts. Now it is using to be able to disable thread too.
        if (GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "NotifySpaceChangesInAnonymousAccounts", 0) == 0)
        {
          continue;
        }

        try
        {
          // WS time statistics 
          if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0)
          {
            WriteAnonymousAccountsStatistics();
          } // if

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _tick_select = Misc.GetTickCount();

            if (!Read_ELP01_AnonymousAccounts(out _dt_anonymous_accounts, _db_trx.SqlTransaction))
            {
              ELPLog.Error(" *** Read_ELP01_AnonymousAccounts failed.");

              continue;
            } // if
          } // using DB_TRX

          // If there are not exists AccountsPending: break
          if (_dt_anonymous_accounts.Rows.Count == 0)
          {
            _send_thread_sleep_time = 2000;

            continue;
          }

          m_count_selects += 1;
          m_ticks_select += Misc.GetElapsedTicks(_tick_select);

          foreach (DataRow _pending_account in _dt_anonymous_accounts.Rows)
          {

            m_count_procesed += 1;
            _tick_ws = Misc.GetTickCount();

            // Call to WS
            _account_id = Convert.ToInt64(_pending_account["AC_ACCOUNT_ID"]);
            if (String.IsNullOrEmpty(_pending_account["AC_MS_CREATED_ON_SITE_ID"].ToString()))
            {
              ELPLog.Warning("SiteId missing. Account: " + _account_id.ToString());
              _send_thread_sleep_time = 2000;
              _error_control_sleep_time = 2000;

              continue;
            }
            _external_track_data = _pending_account["EXTERNAL_TRACK_DATA"].ToString();
            _site_id = Convert.ToInt32(_pending_account["AC_MS_CREATED_ON_SITE_ID"]);
            _trackdata_changed = Convert.ToInt32(_pending_account["TRACKDATA_CHANGED"]) == 1 ? true : false;
            _last_updated = Convert.ToDateTime(_pending_account["ET_LAST_UPDATED"]);

            _space_account_id = null;
            if (!_trackdata_changed || (_trackdata_changed && GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "NotifySpaceChangesInAnonymousAccounts", 0) == 2))
            {
              if (!MS_ELP01_WS.SendAnonymousAccounts(_account_id, _external_track_data, _site_id, out _space_account_id, out _error_control_sleep_time))
              {
                //_send_thread_sleep_time = 2000;

                continue;
              }
              // End WS
            }

            m_ticks_ws += Misc.GetElapsedTicks(_tick_ws);

            // TODO --> to Batch???   More efficient, but delayed.
            _tick_save = Misc.GetTickCount();

            if (_space_account_id != null)
            {
              using (DB_TRX _db_trx = new DB_TRX())
              {
                if (UpdateElp01TrackData(_account_id, _external_track_data, _space_account_id, _trackdata_changed, _last_updated, _db_trx.SqlTransaction))
                {
                  _db_trx.Commit();
                }
                else
                {
                  ELPLog.Warning(" *** UpdateElp01TrackData failed. Account id:" + _account_id.ToString());

                  continue;
                }



              } // using DB_TRX
            }
            m_ticks_save += Misc.GetElapsedTicks(_tick_save);

          } // foreach 

          // WS time statistics reset when no active extended log.
          if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) == 0)
          {
            m_count_selects = 0;
            _tick_select = 0;
            m_count_procesed = 0;
            _tick_ws = 0;
            _tick_save = 0;
            m_ticks_select = 0;
            m_ticks_ws = 0;
            m_ticks_save = 0;
          } // if

          _send_thread_sleep_time = _error_control_sleep_time;

        } // try
        catch (Exception _ex)
        {
          ELPLog.Exception(_ex);
        }

      } // while true

    }  // AnonymousAccountsThread

    //------------------------------------------------------------------------------
    // PURPOSE : Check connection with YAKs BBDD
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static String YaksConnectionString()
    {
      try
      {
        return WGDB.CreateConnectionString(GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DbIpAdress1")
                                                                , GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DbIpAdress1")
                                                                , GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DbName")
                                                                , GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DbLogin")
                                                                , GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DbPassword")
                                                                , 3);
      }
      catch (Exception e)
      {
        ELPLog.Message("*** External Loyalty Program 01 enabled - Build Yaks Connection String failed. Exception: " + e.Message);
      }

      return String.Empty;

    } // CheckYaksConnection

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exists the new field VenueCode in Yaks Interface with Site Id
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : true if exists, false if not exists
    //
    private static Boolean CheckForVenueCode(SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   1 ");
        _sb.AppendLine("      FROM   SYSCOLUMNS AS SC ");
        _sb.AppendLine("INNER JOIN   SYSOBJECTS AS SO ");
        _sb.AppendLine("        ON   SC.ID=SO.ID ");
        _sb.AppendLine("       AND   SC.NAME = @pSysColumnName ");
        _sb.AppendLine("       AND   SO.NAME = @pSysObjectName ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          _cmd.Parameters.Add("@pSysColumnName", SqlDbType.NVarChar).Value = "VenueCode";
          _cmd.Parameters.Add("@pSysObjectName", SqlDbType.NVarChar).Value = "LoyaltyCardInterface";

          if (_cmd.ExecuteScalar() != null)
          {
            m_exists_venue_code_column = true;
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        ELPLog.Message("*** Check field VenueCode in table LoyaltyCardInterface on Yaks Interface failed. Exception: " + ex.Message);

        return false;
      }
    } // CheckForVenueCode

    //------------------------------------------------------------------------------
    // PURPOSE : Read Member interface table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //
    //      - OUTPUT :
    //          - DsMemberInterface
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Read_MemberInterface(out DataSet DsMemberInterface, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      DsMemberInterface = new DataSet("MEMBERINTERFACE");
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT  TOP " + YAKS_INTERFACE_MAX_ROWS.ToString());
        _sb.AppendLine("           RECORDNUMBER ");
        _sb.AppendLine("         , INCCUSTOMERNUMBER ");
        _sb.AppendLine("         , FIRSTNAME ");
        _sb.AppendLine("         , LASTNAME ");
        _sb.AppendLine("         , BIRTHDATE ");
        _sb.AppendLine("         , GENDER ");
        _sb.AppendLine("         , STATUS ");
        _sb.AppendLine("         , DATEADDED ");
        _sb.AppendLine("  FROM   MEMBERINTERFACE ");
        _sb.AppendLine("  ORDER BY RECORDNUMBER ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DsMemberInterface);
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // Read_MemberInterface

    //------------------------------------------------------------------------------
    // PURPOSE : Reads play sessions table
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //
    //      - OUTPUT :
    //          - DtPlaySessions
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Read_ELP01_PlaySessions(out DataSet DsPlaySessions, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      DsPlaySessions = new DataSet("ELP01_PLAY_SESSIONS");
      try
      {
        _sb = new StringBuilder();
        _sb.Append("    SELECT  TOP (" + YAKS_INTERFACE_MAX_ROWS.ToString() + ") ");
        _sb.Append("            EPS_ID                 ");
        _sb.Append("          , EPS_TICKET_NUMBER      ");
        _sb.Append("          , EPS_CUSTOMER_NUMBER    ");
        _sb.Append("          , EPS_SLOT_SERIAL_NUMBER ");
        _sb.Append("          , EPS_SLOT_HOUSE_NUMBER  ");
        _sb.Append("          , EPS_VENUE_CODE         ");
        _sb.Append("          , EPS_AREA_CODE          ");
        _sb.Append("          , EPS_BANK_CODE          ");
        _sb.Append("          , EPS_VENDOR_CODE        ");
        _sb.Append("          , EPS_GAME_CODE          ");
        _sb.Append("          , EPS_START_TIME         ");
        _sb.Append("          , EPS_END_TIME           ");
        _sb.Append("          , EPS_BET_AMOUNT         ");
        _sb.Append("          , EPS_PAID_AMOUNT        ");
        _sb.Append("          , EPS_GAMES_PLAYED       ");
        _sb.Append("          , EPS_INITIAL_AMOUNT     ");
        _sb.Append("          , EPS_ADITIONAL_AMOUNT   ");
        _sb.Append("          , EPS_FINAL_AMOUNT       ");
        _sb.Append("          , EPS_BET_COMB_CODE      ");
        _sb.Append("          , EPS_KINDOF_TICKET      ");
        _sb.Append("          , EPS_SEQUENCE_NUMBER    ");
        _sb.Append("          , EPS_CUPON_NUMBER       ");
        _sb.Append("          , EPS_DATE_UPDATED       ");
        _sb.Append("          , EPS_CARD_NUMBER        ");
        _sb.Append("          , EPS_DATE_INSERTED      ");
        _sb.Append("          , EPS_SITE_ID            ");
        _sb.Append("      FROM  ELP01_PLAY_SESSIONS             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DsPlaySessions);
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // Read_ELP01_PlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Reads loyalty card interface table
    //
    //  PARAMS :
    //      - INPUT : 
    //          - SqlTrans
    //
    //      - OUTPUT :
    //          - DtPlaySessions
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Read_LoyaltyCardInterface(out DataSet DsLoyaltyCardInterface, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;
      String _venue_code_select;

      if (m_exists_venue_code_column)
      {
        _venue_code_select = "          , ISNULL(VENUECODE, 0) AS VENUECODE";
      }
      else
      {
        _venue_code_select = "          , 0 AS VENUECODE       ";
      }

      DsLoyaltyCardInterface = new DataSet("LoyaltyCardInterface");
      try
      {
        _sb = new StringBuilder();

        // SELECT ---------------------------------------------------------
        _sb.AppendLine(" SELECT     RECORDNUMBER ");
        _sb.AppendLine("          , RIGHT('00000000000000000000' + CAST(CARDNUMBER as NVARCHAR), 20) AS CARDNUMBER ");
        _sb.AppendLine("          , INCCARDNUMBER ");
        _sb.AppendLine("          , INCCUSTOMERNUMBER ");
        _sb.AppendLine("          , STATUS ");
        _sb.AppendLine(_venue_code_select);
        _sb.AppendLine("          , DATEUPDATED ");
        //_sb.AppendLine("          , NULL AS ACCOUNTID "); // column is added later
        _sb.AppendLine("   FROM   LOYALTYCARDINTERFACE ");
        _sb.AppendLine("  WHERE   RECORDNUMBER IN ");
        _sb.AppendLine("          ( ");
        _sb.AppendLine("              SELECT  TOP " + YAKS_INTERFACE_MAX_ROWS.ToString());
        _sb.AppendLine("                      MAX(RECORDNUMBER) AS RECORDNUMBER");
        _sb.AppendLine("                FROM  LOYALTYCARDINTERFACE ");
        if (m_exists_venue_code_column)
        {
          _sb.AppendLine("            GROUP BY  INCCUSTOMERNUMBER, VENUECODE ");
          _sb.AppendLine("            ORDER BY  ISNULL(VENUECODE, 0) ");
        }
        else
        {
          _sb.AppendLine("            GROUP BY  INCCUSTOMERNUMBER ");
          _sb.AppendLine("            ORDER BY  RECORDNUMBER DESC ");
        }
        _sb.AppendLine("          ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DsLoyaltyCardInterface);
            DsLoyaltyCardInterface.Tables[0].Columns.Add("ACCOUNTID", Type.GetType("System.Int64")).AllowDBNull = true;
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // Read_LoyaltyCardInterface

    //------------------------------------------------------------------------------
    // PURPOSE : Delete old Play Sessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //          - DataSet DsLoyaltyCardInterface
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Delete_ELP01_PlaySessions(DataSet DsPlaySessions, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      try
      {
        if (DsPlaySessions != null
          && DsPlaySessions.Tables.Count > 0
          && DsPlaySessions.Tables[0].Rows.Count > 0)
        {
          foreach (DataRow _play_session in DsPlaySessions.Tables[0].Rows)
          {
            _play_session.Delete();
          }

          _sb = new StringBuilder();
          _sb.Append("DELETE FROM  ELP01_PLAY_SESSIONS ");
          _sb.Append("      WHERE  EPS_SITE_ID = @pEPS_SITE_ID ");
          _sb.Append("        AND  EPS_ID = @pEPS_ID ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Add("@pEPS_SITE_ID", SqlDbType.BigInt).SourceColumn = "EPS_SITE_ID";
            _sql_cmd.Parameters.Add("@pEPS_ID", SqlDbType.BigInt).SourceColumn = "EPS_ID";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.DeleteCommand = _sql_cmd;
              _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.ContinueUpdateOnError = false;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.Update(DsPlaySessions.Tables[0]);

            } // SqlDataAdapter
          } // SqlCommand
        } // if

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // Delete_ELP01_PlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Delete registers from Loyalty Card Interface table
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Delete_LoyaltyCardInterface(DataSet Ds, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;
      Int32 _row_number_deleted;
      Int32 _row_number_origin;
      DataSet _ds_copy;

      _row_number_deleted = 0;
      _row_number_origin = 0;
      _ds_copy = new DataSet();

      try
      {
        _row_number_origin = Ds.Tables[0].Rows.Count;
        if (_row_number_origin > 0)
        {
          _ds_copy = Ds.Copy();

          _sb = new StringBuilder();
          _sb.Append("DELETE FROM  LOYALTYCARDINTERFACE          ");
          _sb.Append("      WHERE  RECORDNUMBER <= @pRecordNumber ");
          _sb.Append("       AND   INCCUSTOMERNUMBER = @pIncCustomerNumber ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Clear();
            _sql_cmd.Parameters.Add("@pRecordNumber", SqlDbType.BigInt).SourceColumn = "RECORDNUMBER";
            _sql_cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.BigInt).SourceColumn = "INCCUSTOMERNUMBER";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              foreach (DataRow _dr in _ds_copy.Tables[0].Rows)
              {
                // Delete in DB
                _dr.AcceptChanges();
                _dr.Delete();
              } // foreach

              //Ds.Tables[1].AcceptChanges();

              //foreach (DataRow _dr_delete in Ds.Tables[1].Rows)
              //{
              //  _dr_delete.Delete();
              //}

              _sql_da.DeleteCommand = _sql_cmd;
              _sql_da.ContinueUpdateOnError = true;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
              _row_number_deleted = _sql_da.Update(_ds_copy.Tables[0]);

              if (_row_number_deleted != _row_number_origin)
              {

                return false;
              }

              if (_ds_copy.Tables[0].HasErrors)
              {
                ELPLog.Message("MultiSite_Elp01ExchangeTables - Delete_LoyaltyCardInterface - " + _row_number_deleted.ToString() + " of " + _row_number_origin.ToString() + " records");
              }

              //_row_number = _sql_da.Update(Ds.Tables[1]);

              //if (Ds.Tables[1].HasErrors)
              //{
              //  ELPLog.Message("MultiSite_Elp01ExchangeTables - Delete_LoyaltyCardInterface - " + _row_number.ToString() + " of " + Ds.Tables[1].Rows.Count.ToString() + " records");
              //}
            } // SqlDataAdapter
          } // SqlCommand
        } // if

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      ELPLog.Error(" *** Delete_LoyaltyCardInterface failed. ROLLBACK.");

      return false;
    } // Delete_LoyaltyCardInterface

    //------------------------------------------------------------------------------
    // PURPOSE : Delete a table
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Delete_MemberInterface(DataSet Ds, SqlTransaction SqlTrans)
    {
      StringBuilder _sql_sb;
      Int32 _row_number_origin;
      Int32 _row_number_deleted;
      DataSet _ds_copy;

      _row_number_origin = 0;
      _row_number_deleted = 0;
      _ds_copy = new DataSet();

      try
      {
        _row_number_origin = Ds.Tables[0].Rows.Count;
        if (_row_number_origin > 0)
        {
          _ds_copy = Ds.Copy();

          _sql_sb = new StringBuilder();
          _sql_sb.Append("DELETE FROM  MEMBERINTERFACE               ");
          _sql_sb.Append("      WHERE  RECORDNUMBER = @pRecordNumber ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Clear();
            _sql_cmd.Parameters.Add("@pRecordNumber", SqlDbType.BigInt).SourceColumn = "RECORDNUMBER";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              // Mark rows as added.
              _ds_copy.Tables[0].AcceptChanges();
              foreach (DataRow _dr in _ds_copy.Tables[0].Rows)
              {
                _dr.Delete();
              }
              _sql_da.DeleteCommand = _sql_cmd;
              _sql_da.ContinueUpdateOnError = true;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
              _row_number_deleted = _sql_da.Update(_ds_copy);

              //If not delete all records break and rollback
              if (_row_number_deleted != _row_number_origin)
              {
                return false;
              }

              if (_ds_copy.Tables[0].HasErrors)
              {
                ELPLog.Message("MultiSite_Elp01ExchangeTables - Delete_MemberInterface - " + _row_number_deleted.ToString() + " of " + _row_number_origin.ToString() + " records");
              }

            } // SqlDataAdapter
          } // SqlCommand
        } // if

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      ELPLog.Error(" *** Delete_MemberInterface failed. ROLLBACK.");

      return false;
    } // Delete_MemberInterface

    //------------------------------------------------------------------------------
    // PURPOSE : Cancel accounts whose status in MemberInterface is 2
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean CancelAccounts(DataSet Ds, SqlTransaction SqlTrx)
    {
      DataTable _dt_accounts_to_cancel;
      DataRow _dr;
      Int32 _idx_row;
      StringBuilder _sql_sb;

      try
      {
        _dt_accounts_to_cancel = Ds.Tables[0].Clone();

        // Delete and copy to another datatable the rows with status = 2
        for (_idx_row = Ds.Tables[0].Rows.Count - 1; _idx_row >= 0; _idx_row--)
        {
          _dr = Ds.Tables[0].Rows[_idx_row];
          if ((Decimal)_dr["STATUS"] == 2)
          {
            _dt_accounts_to_cancel.Rows.Add(_dr.ItemArray);
            Ds.Tables[0].Rows.Remove(_dr);
          }
        }

        if (_dt_accounts_to_cancel.Rows.Count > 0)
        {

          _sql_sb = new StringBuilder();

          // ACCOUNTS
          _sql_sb.AppendLine(" UPDATE   ACCOUNTS ");
          _sql_sb.AppendLine("    SET   AC_BLOCKED           = 1 ");
          _sql_sb.AppendLine("        , AC_BLOCK_REASON      = AC_BLOCK_REASON | @pExternalSystemCanceled ");
          _sql_sb.AppendLine("        , AC_BLOCK_DESCRIPTION = @pBlockDescription ");
          //_sql_sb.AppendLine("  WHERE   AC_BLOCK_REASON <> @pExternalSystemCanceled ");
          _sql_sb.AppendLine("    WHERE   AC_ACCOUNT_ID = (SELECT ET_AC_ACCOUNT_ID FROM ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                           WHERE  ET_EXTERNAL_ACCOUNT_ID = @pIncCustomerNumber)");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.Decimal).SourceColumn = "INCCUSTOMERNUMBER";
            _sql_cmd.Parameters.Add("@pExternalSystemCanceled", SqlDbType.Int).Value = AccountBlockReason.EXTERNAL_SYSTEM_CANCELED;
            _sql_cmd.Parameters.Add("@pBlockDescription", SqlDbType.NVarChar).Value = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "DescriptionCanceledAccount", "Cancelada por SPACE");

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.ContinueUpdateOnError = false;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.Update(_dt_accounts_to_cancel);

            } // SqlDataAdapter
          } // SqlCommand
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      ELPLog.Error(" *** CancelAccounts failed. ROLLBACK.");

      return false;
    } // CancelAccounts


    //------------------------------------------------------------------------------
    // PURPOSE : inserts data into transaction interface table
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean InsertTransactionInterface(DataSet Ds, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;
      Int32 _row_number;
      String _alarm_description;

      try
      {
        _sb = new StringBuilder();
        if (Ds != null
           && Ds.Tables.Count > 0
           && Ds.Tables[0].Rows.Count > 0)
        {
          Ds.Tables[0].AcceptChanges();
          // Mark rows as added.
          foreach (DataRow _dr_play_sessions in Ds.Tables[0].Rows)
          {
            _dr_play_sessions.SetAdded();
          }

          _sb.Append(" IF EXISTS(SELECT TICKETNUMBER FROM TRANSACTIONINTERFACE WHERE TICKETNUMBER = @pTicketNumber AND VENUECODE = @pVenueCode AND KINDOFTICKET = @pKindOfTicket) ");
          _sb.Append("    UPDATE   TRANSACTIONINTERFACE ");
          _sb.Append("       SET   INCCUSTOMERNUMBER = @pIncCustomerNumber ");
          _sb.Append("           , SLOTSERIALNUMBER  = @pSlotSerialNumber  ");
          _sb.Append("           , SLOTHOUSENUMBER   = @pSlotHouseNumber   ");
          _sb.Append("           , AREACODE          = @pAreaCode          ");
          _sb.Append("           , BANKCODE          = @pBankCode          ");
          _sb.Append("           , VENDORCODE        = @pVendorCode        ");
          _sb.Append("           , GAMECODE          = @pGameCode          ");
          _sb.Append("           , STARTTIME         = @pStartTime         ");
          _sb.Append("           , ENDTIME           = @pEndTime           ");
          _sb.Append("           , BETAMOUNT         = @pBetAmount         ");
          _sb.Append("           , PAIDAMOUNT        = @pPaidAmount        ");
          _sb.Append("           , GAMESPLAYED       = @pGamesPlayed       ");
          _sb.Append("           , INITIALAMOUNT     = @pInitialAmount     ");
          _sb.Append("           , ADITIONALAMOUNT   = @pAditionalAmount   ");
          _sb.Append("           , FINALAMOUNT       = @pFinalAmount       ");
          _sb.Append("           , BETCOMBCODE       = @pBetCombCode       ");
          _sb.Append("           , SEQUENCENUMBER    = @pSequenceNumber    ");
          _sb.Append("           , CUPONNUMBER       = @pCuponNumber       ");
          _sb.Append("           , DATEUPDATED       = GETDATE()           ");
          _sb.Append("           , INCCARDNUMBER     = @pIncCardNumber     ");
          _sb.Append("           , DATEINSERTED      = GETDATE()           ");
          _sb.Append("     WHERE   TICKETNUMBER      = @pTicketNumber      ");
          _sb.Append("       AND   VENUECODE         = @pVenueCode         ");
          _sb.Append("       AND   KINDOFTICKET      = @pKindOfTicket	   ");
          _sb.Append(" ELSE ");
          _sb.Append("    INSERT INTO   TRANSACTIONINTERFACE ");
          _sb.Append("                ( TICKETNUMBER         ");
          _sb.Append("                , INCCUSTOMERNUMBER    ");
          _sb.Append("                , SLOTSERIALNUMBER     ");
          _sb.Append("                , SLOTHOUSENUMBER      ");
          _sb.Append("                , VENUECODE            ");
          _sb.Append("                , AREACODE             ");
          _sb.Append("                , BANKCODE             ");
          _sb.Append("                , VENDORCODE           ");
          _sb.Append("                , GAMECODE             ");
          _sb.Append("                , STARTTIME            ");
          _sb.Append("                , ENDTIME              ");
          _sb.Append("                , BETAMOUNT            ");
          _sb.Append("                , PAIDAMOUNT           ");
          _sb.Append("                , GAMESPLAYED          ");
          _sb.Append("                , INITIALAMOUNT        ");
          _sb.Append("                , ADITIONALAMOUNT      ");
          _sb.Append("                , FINALAMOUNT          ");
          _sb.Append("                , BETCOMBCODE          ");
          _sb.Append("                , KINDOFTICKET         ");
          _sb.Append("                , SEQUENCENUMBER       ");
          _sb.Append("                , CUPONNUMBER          ");
          _sb.Append("                , DATEUPDATED          ");
          _sb.Append("                , INCCARDNUMBER        ");
          _sb.Append("                , DATEINSERTED)        ");
          _sb.Append("         VALUES                        ");
          _sb.Append("                (  @pTicketNumber      ");
          _sb.Append("                 , @pIncCustomerNumber ");
          _sb.Append("                 , @pSlotSerialNumber  ");
          _sb.Append("                 , @pSlotHouseNumber   ");
          _sb.Append("                 , @pVenueCode         ");
          _sb.Append("                 , @pAreaCode          ");
          _sb.Append("                 , @pBankCode          ");
          _sb.Append("                 , @pVendorCode        ");
          _sb.Append("                 , @pGameCode          ");
          _sb.Append("                 , @pStartTime         ");
          _sb.Append("                 , @pEndTime           ");
          _sb.Append("                 , @pBetAmount         ");
          _sb.Append("                 , @pPaidAmount        ");
          _sb.Append("                 , @pGamesPlayed       ");
          _sb.Append("                 , @pInitialAmount     ");
          _sb.Append("                 , @pAditionalAmount   ");
          _sb.Append("                 , @pFinalAmount       ");
          _sb.Append("                 , @pBetCombCode       ");
          _sb.Append("                 , @pKindOfTicket      ");
          _sb.Append("                 , @pSequenceNumber    ");
          _sb.Append("                 , @pCuponNumber       ");
          _sb.Append("                 , GETDATE()           ");
          _sb.Append("                 , @pIncCardNumber     ");
          _sb.Append("                 , GETDATE())          ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.Decimal).SourceColumn = "EPS_TICKET_NUMBER";
            _sql_cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.Decimal).SourceColumn = "EPS_CUSTOMER_NUMBER";
            _sql_cmd.Parameters.Add("@pSlotSerialNumber", SqlDbType.VarChar).SourceColumn = "EPS_SLOT_SERIAL_NUMBER";
            _sql_cmd.Parameters.Add("@pSlotHouseNumber", SqlDbType.VarChar).SourceColumn = "EPS_SLOT_HOUSE_NUMBER";
            _sql_cmd.Parameters.Add("@pVenueCode", SqlDbType.Decimal).SourceColumn = "EPS_VENUE_CODE";
            _sql_cmd.Parameters.Add("@pAreaCode", SqlDbType.Decimal).SourceColumn = "EPS_AREA_CODE";
            _sql_cmd.Parameters.Add("@pBankCode", SqlDbType.Decimal).SourceColumn = "EPS_BANK_CODE";
            _sql_cmd.Parameters.Add("@pVendorCode", SqlDbType.Decimal).SourceColumn = "EPS_VENDOR_CODE";
            _sql_cmd.Parameters.Add("@pGameCode", SqlDbType.Decimal).SourceColumn = "EPS_GAME_CODE";
            _sql_cmd.Parameters.Add("@pStartTime", SqlDbType.DateTime).SourceColumn = "EPS_START_TIME";
            _sql_cmd.Parameters.Add("@pEndTime", SqlDbType.DateTime).SourceColumn = "EPS_END_TIME";
            _sql_cmd.Parameters.Add("@pBetAmount", SqlDbType.Decimal).SourceColumn = "EPS_BET_AMOUNT";
            _sql_cmd.Parameters.Add("@pPaidAmount", SqlDbType.Decimal).SourceColumn = "EPS_PAID_AMOUNT";
            _sql_cmd.Parameters.Add("@pGamesPlayed", SqlDbType.Decimal).SourceColumn = "EPS_GAMES_PLAYED";
            _sql_cmd.Parameters.Add("@pInitialAmount", SqlDbType.Decimal).SourceColumn = "EPS_INITIAL_AMOUNT";
            _sql_cmd.Parameters.Add("@pAditionalAmount", SqlDbType.Decimal).SourceColumn = "EPS_ADITIONAL_AMOUNT";
            _sql_cmd.Parameters.Add("@pFinalAmount", SqlDbType.Decimal).SourceColumn = "EPS_FINAL_AMOUNT";
            _sql_cmd.Parameters.Add("@pBetCombCode", SqlDbType.Decimal).SourceColumn = "EPS_BET_COMB_CODE";
            _sql_cmd.Parameters.Add("@pKindOfTicket", SqlDbType.Decimal).SourceColumn = "EPS_KINDOF_TICKET";
            _sql_cmd.Parameters.Add("@pSequenceNumber", SqlDbType.Decimal).SourceColumn = "EPS_SEQUENCE_NUMBER";
            _sql_cmd.Parameters.Add("@pCuponNumber", SqlDbType.VarChar).SourceColumn = "EPS_CUPON_NUMBER";
            //_sql_cmd.Parameters.Add("@pDateUpdated", SqlDbType.DateTime).SourceColumn = "EPS_DATE_UPDATED";
            _sql_cmd.Parameters.Add("@pIncCardNumber", SqlDbType.Decimal).SourceColumn = "EPS_CARD_NUMBER";
            //_sql_cmd.Parameters.Add("@pDateInserted", SqlDbType.DateTime).SourceColumn = "EPS_DATE_INSERTED";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.ContinueUpdateOnError = true;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

              _row_number = _sql_da.Update(Ds);

              if (_row_number != Ds.Tables[0].Rows.Count)
              {
                ELPLog.Message("MultiSite_Elp01ExchangeTables - InsertTransactionInterface - " + _row_number.ToString() + " of " + Ds.Tables[0].Rows.Count.ToString() + " records inserted/updated.");

                foreach (DataRow _dr in Ds.Tables[0].Rows)
                {
                  //
                  // Alarm: Error Inserting into TransactionInterface the PlaySession
                  //
                  if (_dr.HasErrors)
                  {
                    ELPLog.Message("     *** Error: " + _dr.RowError);

                    _alarm_description = Resource.String("STR_ELP_ALARM_INSERT_TRANSACION_INTERFACE"
                                                       , _dr["EPS_VENUE_CODE"]
                                                       , _dr["EPS_TICKET_NUMBER"]
                                                       , _dr.RowError);
                    Alarm.Register(AlarmSourceCode.ELP01
                                 , 0
                                 , m_alarm_source_name
                                 , AlarmCode.ELP_ErrorInsertTransactionInterface
                                 , _alarm_description);
                  }
                }

                if (Ds.Tables[0].HasErrors)
                {
                  RemoveErrorsDataset(Ds);
                }
              }
            }
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // InsertTransactionInterface 

    //------------------------------------------------------------------------------
    // PURPOSE : Process SQL transactions.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean UpdateAccountsTrackData(DataSet Ds, SqlTransaction SqlTrans, SqlTransaction SqlTransYaks, ref ALARM_ERROR LastAlarm)
    {
      StringBuilder _sql_sb;
      StringBuilder _alarm_description;
      StringBuilder _log_description;
      String _xml_string;
      Int32 _row_number;
      Boolean _updated_uncompleted;
      //Int32 _count;
      Boolean _enable_extended_log;
      Boolean _show_error;

      _row_number = 0;
      try
      {
        if (Ds != null
          && Ds.Tables.Count > 0
          && Ds.Tables[0].Rows.Count > 0)
        {
          _enable_extended_log = false;
          if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x10) != 0)
          {
            _enable_extended_log = true;
          }

          // Mark rows as added.
          foreach (DataRow _dr in Ds.Tables[0].Rows)
          {
            if ((Decimal)_dr["STATUS"] != 0)
            {
              _dr.AcceptChanges();
              _dr.SetAdded();
            }

            if (_enable_extended_log)
            {
              Elp01Requests.ConvertRowToXml(_dr, false, out _xml_string);
              ELPLog.Message("SPACE - YAKS LoyaltyCardInterface: " + _xml_string);
            }

          }
          _sql_sb = new StringBuilder();
          _sql_sb.AppendLine(" DECLARE @pAccountId bigint ");
          _sql_sb.AppendLine(" DECLARE @TrackData NVARCHAR(50) ");
          _sql_sb.AppendLine(" DECLARE @NewAccount BIT ");

          _sql_sb.AppendLine(" SET NOCOUNT ON; ");

          _sql_sb.AppendLine(" SELECT @pAccountId = ET_AC_ACCOUNT_ID FROM ELP01_TRACKDATA");
          _sql_sb.AppendLine(" WHERE  ET_EXTERNAL_ACCOUNT_ID = @pIncCustomerNumber");

          _sql_sb.AppendLine(" SET @NewAccount = 0 ");
          _sql_sb.AppendLine(" IF @pAccountId IS NULL");
          _sql_sb.AppendLine(" BEGIN ");
          _sql_sb.AppendLine("      SET @NewAccount = 1 ");
          _sql_sb.AppendLine("      DECLARE   @SeqId     bigint ");
          _sql_sb.AppendLine("      UPDATE   SEQUENCES ");
          _sql_sb.AppendLine("         SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
          _sql_sb.AppendLine("       WHERE   SEQ_ID         = 4 ");
          _sql_sb.AppendLine("         SET   @SeqId      = (SELECT SequenceValue = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4) ");
          _sql_sb.AppendLine("         SET   @pAccountId  = 1000000 + @SeqId * 1000 ");
          _sql_sb.AppendLine("      INSERT   INTO ACCOUNTS(");
          _sql_sb.AppendLine("               AC_ACCOUNT_ID");
          _sql_sb.AppendLine("             , AC_HOLDER_NAME");
          _sql_sb.AppendLine("             , AC_HOLDER_NAME3");
          _sql_sb.AppendLine("             , AC_HOLDER_NAME1");
          _sql_sb.AppendLine("             , AC_HOLDER_GENDER");
          _sql_sb.AppendLine("             , AC_CREATED");
          _sql_sb.AppendLine("             , AC_HOLDER_BIRTH_DATE");
          _sql_sb.AppendLine("             , AC_HOLDER_LEVEL");
          _sql_sb.AppendLine("             , AC_BLOCKED");
          _sql_sb.AppendLine("             , AC_BLOCK_REASON");
          _sql_sb.AppendLine("             , AC_TRACK_DATA");
          _sql_sb.AppendLine("             , AC_TYPE");
          _sql_sb.AppendLine("             , AC_USER_TYPE");
          _sql_sb.AppendLine("             , AC_CARD_PAID");
          _sql_sb.AppendLine("             , AC_DEPOSIT");
          _sql_sb.AppendLine("             )VALUES(");
          _sql_sb.AppendLine("              @pAccountId");
          _sql_sb.AppendLine("            , @pName + ' ' + @pFirstName");
          _sql_sb.AppendLine("            , @pName");
          _sql_sb.AppendLine("            , @pFirstName");
          _sql_sb.AppendLine("            , CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'M' THEN 1 ELSE CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'F' THEN 2 ELSE 0 END END");
          //_sql_sb.AppendLine("            , @pAcCreated");
          _sql_sb.AppendLine("            , GETDATE()");
          _sql_sb.AppendLine("            , ISNULL(@pAcHolderBirthDate, '1900-01-01')");
          _sql_sb.AppendLine("            , 1 ");
          _sql_sb.AppendLine("            , 1 ");
          _sql_sb.AppendLine("            , @pBlockReason_ExternalSystem ");
          _sql_sb.AppendLine("            , '00000000000000000000-RECYCLED-' + CAST (@pAccountId AS NVARCHAR)");
          _sql_sb.AppendLine("            , 2 ");
          _sql_sb.AppendLine("            , @pAcUserType ");
          _sql_sb.AppendLine("            , 1 ");
          _sql_sb.AppendLine("            , 0 ");
          _sql_sb.AppendLine("            ) ");
          _sql_sb.AppendLine("      INSERT  INTO   ELP01_TRACKDATA( ");
          _sql_sb.AppendLine("              ET_AC_ACCOUNT_ID ");
          _sql_sb.AppendLine("            , ET_EXTERNAL_ACCOUNT_ID ");
          _sql_sb.AppendLine("            , ET_EXTERNAL_CARD_ID ");
          _sql_sb.AppendLine("            , ET_EXTERNAL_TRACKDATA ");
          _sql_sb.AppendLine("            , ET_EXTERNAL_CARD_ID_STATUS ");
          _sql_sb.AppendLine("            , ET_SPACE_REPORTED ");
          _sql_sb.AppendLine("            )VALUES( ");
          _sql_sb.AppendLine("              @pAccountId ");
          _sql_sb.AppendLine("            , @pIncCustomerNumber ");
          _sql_sb.AppendLine("          , NULL ");
          _sql_sb.AppendLine("          , 'RECYCLED(' + @pCardNumber + ')' ");
          _sql_sb.AppendLine("          , 0 ");
          _sql_sb.AppendLine("          , @SpaceReported) ");
          _sql_sb.AppendLine(" END ");

          // Recycled Other Accounts with same TrackData and it is not Anonymous
          _sql_sb.AppendLine(" DECLARE @OtherAccount BIGINT ");
          _sql_sb.AppendLine(" DECLARE @OtherAccount_UserType INT ");
          _sql_sb.AppendLine(" SET @TrackData = dbo.TrackDataToInternal(CAST(@pCardNumber AS NVARCHAR(255))) ");
          _sql_sb.AppendLine(" SELECT @OtherAccount = AC_ACCOUNT_ID, @OtherAccount_UserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_TRACK_DATA = @TrackData ");

          //-- _sql_sb.AppendLine(" IF @OtherAccount IS NOT NULL ");
          _sql_sb.AppendLine(" IF @OtherAccount IS NOT NULL AND ISNULL(@OtherAccount_UserType, 0) <> @pAcUserTypeAnonymous ");

          _sql_sb.AppendLine(" BEGIN ");
          _sql_sb.AppendLine("     UPDATE   ACCOUNTS ");
          _sql_sb.AppendLine("        SET   AC_TRACK_DATA = ('00000000000000000000-RECYCLED-' + CAST (AC_ACCOUNT_ID AS NVARCHAR)) ");
          _sql_sb.AppendLine("      WHERE   AC_ACCOUNT_ID = @OtherAccount ");
          _sql_sb.AppendLine(" END ");

          _sql_sb.AppendLine(" UPDATE   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("    SET   ET_EXTERNAL_TRACKDATA        = 'RECYCLED(' + ISNULL(ET_EXTERNAL_TRACKDATA,'') + ')' ");
          _sql_sb.AppendLine("      ,   ET_EXTERNAL_CARD_ID          = NULL ");
          _sql_sb.AppendLine("      ,   ET_EXTERNAL_CARD_ID_STATUS   = 0 ");
          _sql_sb.AppendLine("      ,   ET_SPACE_REPORTED            = CASE WHEN ET_EXTERNAL_ACCOUNT_ID = @pIncCustomerNumber THEN @SpaceReported_Duplicate ELSE @SpaceReported END ");
          _sql_sb.AppendLine("      ,   ET_LAST_UPDATED              = GETDATE() ");
          _sql_sb.AppendLine("  WHERE   ET_EXTERNAL_TRACKDATA        = @pCardNumber ");
          _sql_sb.AppendLine("    AND   (ET_AC_ACCOUNT_ID            <> ISNULL(@OtherAccount,0) ");
          _sql_sb.AppendLine("           OR  (ET_AC_ACCOUNT_ID = ISNULL(@OtherAccount,0) ");
          _sql_sb.AppendLine("                AND ISNULL(@OtherAccount_UserType, 0) <> @pAcUserTypeAnonymous)) ");

          // If Status = 1 And not exists other Anonymous Account Then Update Accounts / Elp01_TrackData
          _sql_sb.AppendLine(" IF @pStatus <> 0 AND (@OtherAccount IS NULL OR (@OtherAccount IS NOT NULL AND ISNULL(@OtherAccount_UserType, 0) <> @pAcUserTypeAnonymous)) ");
          _sql_sb.AppendLine(" BEGIN");
          _sql_sb.AppendLine("      UPDATE   ACCOUNTS      ");
          _sql_sb.AppendLine("         SET   AC_TRACK_DATA = @TrackData ");
          _sql_sb.AppendLine("             , AC_USER_TYPE = @pAcUserType ");
          _sql_sb.AppendLine("       WHERE   AC_ACCOUNT_ID = @pAccountId ");
          _sql_sb.AppendLine("      UPDATE   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("         SET   ET_EXTERNAL_CARD_ID        = @pIncCardNumber ");
          _sql_sb.AppendLine("           ,   ET_EXTERNAL_TRACKDATA      = @pCardNumber ");
          _sql_sb.AppendLine("           ,   ET_EXTERNAL_CARD_ID_STATUS = @pStatus ");
          _sql_sb.AppendLine("           ,   ET_SPACE_REPORTED          = CASE WHEN ET_EXTERNAL_TRACKDATA = @pCardNumber OR @NewAccount=1 OR ET_EXTERNAL_TRACKDATA like 'RECYCLED(%' THEN @SpaceReported ELSE @SpaceReported_Duplicate END ");
          _sql_sb.AppendLine("           ,   ET_LAST_UPDATED            = GETDATE() ");
          _sql_sb.AppendLine("       WHERE   ET_AC_ACCOUNT_ID           = @pAccountId  ");
          _sql_sb.AppendLine(" END ");

          _sql_sb.AppendLine(" SET @AccountId = @pAccountId ");

          _sql_sb.AppendLine(" SET NOCOUNT OFF; ");

          using (SqlCommand _cmd = new SqlCommand(_sql_sb.ToString(), SqlTrans.Connection, SqlTrans))
          {
            _cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.Decimal).SourceColumn = "INCCUSTOMERNUMBER";
            _cmd.Parameters.Add("@pCardNumber", SqlDbType.NVarChar).SourceColumn = "CARDNUMBER";
            _cmd.Parameters.Add("@pIncCardNumber", SqlDbType.Decimal).SourceColumn = "INCCARDNUMBER";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Decimal).SourceColumn = "STATUS"; // NUMERIC IS ALWAIS Decimal !!
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).Value = ELP01_DownloadSpaceAccount.HOLDER_NAME;
            _cmd.Parameters.Add("@pFirstName", SqlDbType.NVarChar).Value = String.Empty;
            _cmd.Parameters.Add("@pAcHolderGender", SqlDbType.NVarChar).Value = DBNull.Value;
            _cmd.Parameters.Add("@pAcHolderBirthDate", SqlDbType.DateTime).Value = DBNull.Value;
            //_cmd.Parameters.Add("@pAcCreated", SqlDbType.DateTime).SourceColumn = "DATEUPDATED";
            _cmd.Parameters.Add("@pAcUserType", SqlDbType.Int).Value = (Int32)ACCOUNT_USER_TYPE.PERSONAL;
            _cmd.Parameters.Add("@pAcUserTypeAnonymous", SqlDbType.Int).Value = (Int32)ACCOUNT_USER_TYPE.ANONYMOUS;
            _cmd.Parameters.Add("@pBlockReason_None", SqlDbType.Int).Value = (Int32)AccountBlockReason.NONE;
            _cmd.Parameters.Add("@pBlockReason_ExternalSystem", SqlDbType.Int).Value = (Int32)AccountBlockReason.EXTERNAL_SYSTEM;
            _cmd.Parameters.Add("@SpaceReported", SqlDbType.Int).Value = SPACE_REPORTED.INFORMED;
            _cmd.Parameters.Add("@SpaceReported_Duplicate", SqlDbType.Int).Value = SPACE_REPORTED.INFORMED_AND_DUPLICATED;

            SqlParameter _output_account_id = _cmd.Parameters.Add("@AccountId", SqlDbType.BigInt);
            _output_account_id.Direction = ParameterDirection.Output;
            _output_account_id.SourceColumn = "ACCOUNTID";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _cmd;
              _sql_da.ContinueUpdateOnError = true;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _row_number = _sql_da.Update(Ds.Tables[0]);

              _updated_uncompleted = false;
              _alarm_description = new StringBuilder();
              _log_description = new StringBuilder();

              if (Ds.Tables[0].HasErrors)
              {
                _alarm_description.AppendLine(Resource.String("STR_ELP_ALARM_UPDATE_ACCOUNTS_DB_ERROR"));
              }
              else
              {
                _alarm_description.AppendLine(Resource.String("STR_ELP_ALARM_UPDATE_ACCOUNTS_TRACKDATA"));
              }

              foreach (DataRow _dr in Ds.Tables[0].Rows)
              {
                if (_dr.HasErrors || _dr.RowState == DataRowState.Added)
                {
                  _updated_uncompleted = true;

                  if (_dr.HasErrors)
                  {
                    ELPLog.Message("     *** SPACE Error: LoyaltyCardInterface " + _dr.RowError);

                  }

                  _xml_string = "";
                  Elp01Requests.ConvertRowToXml(_dr, true, out _xml_string);
                  _alarm_description.AppendLine(_xml_string);
                  _log_description.AppendLine(" RECORDNUMBER=" + _dr["RECORDNUMBER"].ToString() + ", INCCUSTOMERNUMBER=" + _dr["INCCUSTOMERNUMBER"].ToString() + ";");
                }
              }

              //
              // Alarm: DataBase Error or Account not exist
              //
              if (_updated_uncompleted)
              {
                _show_error = false;
                if (LastAlarm.alarm_description.Equals(_alarm_description))
                {
                  if (Misc.GetElapsedTicks(LastAlarm.tick_alarm) > 300000) // 5 min
                  {
                    _show_error = true;
                  }
                }
                else
                {
                  _show_error = true;
                }

                if (_show_error)
                {
                  LastAlarm.alarm_description = _alarm_description;
                  LastAlarm.tick_alarm = Environment.TickCount;

                  ELPLog.Message("MultiSite_Elp01ExchangeTables - UpdateAccountsTrackData: Accounts from LoyaltyCardInterface --> DataBase Error. " + _log_description);

                  Alarm.Register(AlarmSourceCode.ELP01
                               , 0
                               , m_alarm_source_name
                               , AlarmCode.ELP_ErrorTrackdataRelation
                               , _alarm_description.ToString());
                }

                if (Ds.Tables[0].HasErrors)
                {
                  RemoveErrorsDataset(Ds);
                }

                ////if (Ds.Tables[0].HasErrors)
                ////{
                ////  // ACC ROLLBACK --> If Error not continue (EXECUTE in ORDER)
                ////  return false;
                ////}
              } // if
            } // SqlDataAdapter
          } // SqlCommand
        } // if

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // UpdateAccountsTrackData

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts or updates Accounts
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean InsertUpdateAccounts(DataSet Ds, SqlTransaction SqlTrans)
    {
      StringBuilder _sql_sb;
      StringBuilder _description;
      String _xml_string;
      Int32 _row_number;
      Boolean _enable_extended_log;

      _row_number = 0;
      try
      {
        _sql_sb = new StringBuilder();

        _sql_sb.AppendLine(" DECLARE   @AccountId bigint ");
        _sql_sb.AppendLine(" SELECT @AccountId = ET_AC_ACCOUNT_ID FROM ELP01_TRACKDATA");
        _sql_sb.AppendLine(" WHERE  ET_EXTERNAL_ACCOUNT_ID = @IncCustomerNumber");

        _sql_sb.AppendLine("  IF @AccountId IS NULL");
        _sql_sb.AppendLine("  BEGIN ");
        _sql_sb.AppendLine("    DECLARE   @SeqId     bigint ");
        _sql_sb.AppendLine("     UPDATE   SEQUENCES ");
        _sql_sb.AppendLine("        SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
        _sql_sb.AppendLine("      WHERE   SEQ_ID         = 4 ");
        _sql_sb.AppendLine("        SET   @SeqId      = (SELECT SequenceValue = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4) ");
        _sql_sb.AppendLine("        SET   @AccountId  = 1000000 + @SeqId * 1000 ");
        _sql_sb.AppendLine("     INSERT   INTO ACCOUNTS(");
        _sql_sb.AppendLine("              AC_ACCOUNT_ID");
        _sql_sb.AppendLine("            , AC_HOLDER_NAME");
        _sql_sb.AppendLine("            , AC_HOLDER_NAME3");
        _sql_sb.AppendLine("            , AC_HOLDER_NAME1");
        _sql_sb.AppendLine("            , AC_HOLDER_GENDER");
        _sql_sb.AppendLine("            , AC_CREATED");
        _sql_sb.AppendLine("            , AC_HOLDER_BIRTH_DATE");
        _sql_sb.AppendLine("            , AC_HOLDER_LEVEL");
        _sql_sb.AppendLine("            , AC_BLOCKED");
        _sql_sb.AppendLine("            , AC_BLOCK_REASON");
        _sql_sb.AppendLine("            , AC_BLOCK_DESCRIPTION ");
        _sql_sb.AppendLine("            , AC_TRACK_DATA");
        _sql_sb.AppendLine("            , AC_TYPE");
        _sql_sb.AppendLine("            , AC_USER_TYPE");
        _sql_sb.AppendLine("            , AC_CARD_PAID");
        _sql_sb.AppendLine("            , AC_DEPOSIT");
        _sql_sb.AppendLine("      )VALUES(");
        _sql_sb.AppendLine("              @AccountId");
        _sql_sb.AppendLine("            , @pName + ' ' + @pFirstName");
        _sql_sb.AppendLine("            , @pName");
        _sql_sb.AppendLine("            , @pFirstName");
        _sql_sb.AppendLine("            , CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'M' THEN 1 ELSE CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'F' THEN 2 ELSE 0 END END");
        //_sql_sb.AppendLine("            , @pAcCreated");
        _sql_sb.AppendLine("            , GETDATE()");
        _sql_sb.AppendLine("            , ISNULL(@pAcHolderBirthDate, '1900-01-01')");
        _sql_sb.AppendLine("            , 1");
        _sql_sb.AppendLine("            , CASE WHEN @pStatus = 0 THEN 1 ELSE 0 END ");
        _sql_sb.AppendLine("            , CASE WHEN @pStatus = 0 THEN @pBlockReason_ExternalSystem ELSE @pBlockReason_None END ");
        _sql_sb.AppendLine("            , NULL ");
        _sql_sb.AppendLine("            , ('00000000000000000000-RECYCLED-' + CAST (@AccountId AS NVARCHAR))");
        _sql_sb.AppendLine("            , 2 ");
        _sql_sb.AppendLine("            , @pAcUserType ");
        _sql_sb.AppendLine("            , 1 ");
        _sql_sb.AppendLine("            , 0 ");
        _sql_sb.AppendLine("  ) ");
        _sql_sb.AppendLine("    INSERT INTO   ELP01_TRACKDATA ");
        _sql_sb.AppendLine("                ( ET_AC_ACCOUNT_ID ");
        _sql_sb.AppendLine("                , ET_EXTERNAL_ACCOUNT_ID ");
        _sql_sb.AppendLine("                , ET_EXTERNAL_CARD_ID ");
        _sql_sb.AppendLine("                , ET_EXTERNAL_TRACKDATA ");
        _sql_sb.AppendLine("                , ET_EXTERNAL_CARD_ID_STATUS ");
        _sql_sb.AppendLine("      )VALUES( ");
        _sql_sb.AppendLine("                  @AccountId ");
        _sql_sb.AppendLine("                , @IncCustomerNumber ");
        _sql_sb.AppendLine("                , NULL ");
        _sql_sb.AppendLine("                , NULL ");
        _sql_sb.AppendLine("                , 0) ");
        _sql_sb.AppendLine("      END ");
        _sql_sb.AppendLine("    ELSE ");
        _sql_sb.AppendLine("      BEGIN ");
        _sql_sb.AppendLine("        UPDATE   ACCOUNTS ");
        _sql_sb.AppendLine("           SET   AC_HOLDER_NAME3      = @pName ");
        _sql_sb.AppendLine("               , AC_HOLDER_NAME1      = @pFirstName ");
        _sql_sb.AppendLine("               , AC_HOLDER_NAME       = @pName + ' ' + @pFirstName ");
        _sql_sb.AppendLine("               , AC_HOLDER_GENDER     = CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'M' THEN 1 ELSE CASE WHEN ISNULL(@pAcHolderGender, 'X') = 'F' THEN 2 ELSE 0 END END");
        _sql_sb.AppendLine("               , AC_HOLDER_BIRTH_DATE = ISNULL(@pAcHolderBirthDate, '1900-01-01') ");
        _sql_sb.AppendLine("               , AC_BLOCKED           = CASE WHEN @pStatus = 0 THEN 1 ELSE 0 END ");
        _sql_sb.AppendLine("               , AC_BLOCK_REASON      = CASE WHEN @pStatus = 0 THEN (AC_BLOCK_REASON | @pBlockReason_ExternalSystem) ELSE @pBlockReason_None END ");
        _sql_sb.AppendLine("               , AC_BLOCK_DESCRIPTION = NULL ");
        _sql_sb.AppendLine("         WHERE   AC_ACCOUNT_ID = @AccountId");
        _sql_sb.AppendLine("  END");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          _sql_cmd.Parameters.Clear();
          _sql_cmd.Parameters.Add("@pName", SqlDbType.VarChar).SourceColumn = "FIRSTNAME";
          _sql_cmd.Parameters.Add("@pFirstName", SqlDbType.VarChar).SourceColumn = "LASTNAME";
          _sql_cmd.Parameters.Add("@pAcHolderGender", SqlDbType.VarChar).SourceColumn = "GENDER";
          _sql_cmd.Parameters.Add("@pAcHolderBirthDate", SqlDbType.DateTime).SourceColumn = "BIRTHDATE";
          //_sql_cmd.Parameters.Add("@pAcCreated", SqlDbType.DateTime).SourceColumn = "DATEADDED";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "STATUS";
          _sql_cmd.Parameters.Add("@IncCustomerNumber", SqlDbType.Decimal).SourceColumn = "INCCUSTOMERNUMBER";
          _sql_cmd.Parameters.Add("@pAcUserType", SqlDbType.Int).Value = (Int32)ACCOUNT_USER_TYPE.PERSONAL;
          _sql_cmd.Parameters.Add("@pBlockReason_ExternalSystem", SqlDbType.Int).Value = (Int32)AccountBlockReason.EXTERNAL_SYSTEM;
          _sql_cmd.Parameters.Add("@pBlockReason_None", SqlDbType.Int).Value = (Int32)AccountBlockReason.NONE;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            // Mark rows as added.
            Ds.Tables[0].AcceptChanges();
            _enable_extended_log = false;
            if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x8) != 0)
            {
              _enable_extended_log = true;
            }
            foreach (DataRow _dr in Ds.Tables[0].Rows)
            {
              _dr.SetAdded();
              if (_enable_extended_log)
              {
                Elp01Requests.ConvertRowToXml(_dr, false, out _xml_string);
                ELPLog.Message("SPACE - YAKS MemberInterface: " + _xml_string);
              }
            }
            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.ContinueUpdateOnError = true;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _row_number = _sql_da.Update(Ds);

            if (_row_number != Ds.Tables[0].Rows.Count)
            {
              ELPLog.Message("MultiSite_Elp01ExchangeTables - InsertUpdateAccounts - " + _row_number.ToString() + " of " + Ds.Tables[0].Rows.Count.ToString() + " records inserted/updated.");

              //
              // Alarm: Update Account
              //
              _description = new StringBuilder();
              _description.AppendLine(Resource.String("STR_ELP_ALARM_INSERT_UPDATE_ACCOUNTS"));

              Alarm.Register(AlarmSourceCode.ELP01
                , 0
                , m_alarm_source_name
                , AlarmCode.ELP_ErrorUpdateAccount
                , _description.ToString());

              if (Ds.Tables[0].HasErrors)
              {
                foreach (DataRow _dr in Ds.Tables[0].Rows)
                {
                  if (_dr.HasErrors)
                  {
                    _xml_string = "";
                    Elp01Requests.ConvertRowToXml(_dr, true, out _xml_string);
                    _description.AppendLine(_xml_string);
                    ELPLog.Message("     *** SPACE Error: InsertUpdateAccounts " + _dr.RowError);
                  }
                }

                if (Ds.Tables[0].HasErrors)
                {
                  RemoveErrorsDataset(Ds);
                }

                // ACC ROLLBACK --> If Error not continue (EXECUTE in ORDER)
                return false;
              }
            } // if        
          } // SqlDataAdapter
        } // SqlCommand
        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }//InsertUpdateAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Delete rows with errors in a Dataset
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void RemoveErrorsDataset(DataSet Ds)
    {
      Int32 _idx_row;

      for (_idx_row = Ds.Tables[0].Rows.Count - 1; _idx_row >= 0; _idx_row--)
      {
        if (Ds.Tables[0].Rows[_idx_row].HasErrors)
        {
          Ds.Tables[0].Rows.RemoveAt(_idx_row);
        }
      }
    } // RemoveErrorsDataset

    //------------------------------------------------------------------------------
    // PURPOSE : Validate external trackdata
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //
    //      - OUTPUT :
    //
    // RETURNS : True if no exception is thrown
    //
    private static Boolean ValidateTrackData(DataSet Ds)
    {
      Int32 _idx_row;
      Int32 _cardType;
      String _external_trackdata;
      String _internal_trackdata;
      String _xml_row;
      StringBuilder _description;
      Boolean _exist_row_errors;

      try
      {
        _exist_row_errors = false;
        _description = new StringBuilder();
        _description.AppendLine(Resource.String("STR_ELP_ALARM_VALIDATE_TRACKDATA"));
        DataTable _table_delete = Ds.Tables[0].Clone();
        _table_delete.TableName = "Table2";
        Ds.Tables.Add(_table_delete);

        for (_idx_row = Ds.Tables[0].Rows.Count - 1; _idx_row >= 0; _idx_row--)
        {
          _cardType = 0;
          _internal_trackdata = "";
          _external_trackdata = "-1";
          // Get CardNumber 
          if (!Ds.Tables[0].Rows[_idx_row].IsNull("CARDNUMBER"))
          {
            _external_trackdata = Ds.Tables[0].Rows[_idx_row]["CARDNUMBER"].ToString();
          }
          // Test CardNumber
          if (!WSI.Common.CardNumber.TrackDataToInternal(_external_trackdata, out _internal_trackdata, out _cardType))
          {
            _exist_row_errors = true;
            _xml_row = "";
            Elp01Requests.ConvertRowToXml(Ds.Tables[0].Rows[_idx_row], true, out _xml_row);
            _description.AppendLine(_xml_row);
            //Add row to delete
            Ds.Tables[1].Rows.Add(new object[7]{
                                                Ds.Tables[0].Rows[_idx_row]["RECORDNUMBER"],
                                                Ds.Tables[0].Rows[_idx_row]["CardNumber"],                                  
                                                Ds.Tables[0].Rows[_idx_row]["IncCardNumber"],
                                                Ds.Tables[0].Rows[_idx_row]["IncCustomerNumber"],
                                                Ds.Tables[0].Rows[_idx_row]["Status"],
                                                Ds.Tables[0].Rows[_idx_row]["VenueCode"],
                                                Ds.Tables[0].Rows[_idx_row]["DateUpdated"]
                                                });
            // Delete Row with contains incorrect TrackData
            Ds.Tables[0].Rows[_idx_row].Delete();
          } // if
        } // for
        Ds.AcceptChanges();

        // Create Alarm
        if (_exist_row_errors)
        {
          //
          // Incorrects External TrackDatas
          //
          ELPLog.Message("MultiSite_Elp01ExchangeTables - ValidateTrackData. Error: " + _description.ToString());
          Alarm.Register(AlarmSourceCode.ELP01
                       , 0
                       , m_alarm_source_name
                       , AlarmCode.ELP_IncorrectExternalTrackdata
                       , _description.ToString());
        }

        return true;
      } //try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }//ValidateTrackData

    //------------------------------------------------------------------------------
    // PURPOSE : Validate that external trackdata is not repeated in any other account in our system
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS : True if no exception is thrown
    //
    private static Boolean UsedTrackData(DataSet Ds, SqlTransaction SqlTrans)
    {
      StringBuilder _sql_sb;
      StringBuilder _alarm_description;
      DataRow[] _dr_array;

      try
      {
        _sql_sb = new StringBuilder();
        _alarm_description = new StringBuilder();

        Ds.Tables[0].AcceptChanges();
        // Mark rows as added.
        foreach (DataRow _dr_play_sessions in Ds.Tables[0].Rows)
        {
          _dr_play_sessions.SetAdded();
        }

        _sql_sb.AppendLine("    DECLARE @pOtherAccountId BIGINT ");

        _sql_sb.AppendLine("    IF  @pStatus = 0  ");
        _sql_sb.AppendLine("    BEGIN");
        _sql_sb.AppendLine("      UPDATE   ACCOUNTS ");
        _sql_sb.AppendLine("         SET   AC_TRACK_DATA = ('00000000000000000000-RECYCLED-' + CAST (AC_ACCOUNT_ID AS NVARCHAR)) ");
        _sql_sb.AppendLine("       WHERE   AC_ACCOUNT_ID IN (SELECT ET_AC_ACCOUNT_ID FROM ELP01_TRACKDATA WHERE ET_EXTERNAL_ACCOUNT_ID = @pIncCustomerNumber) ");
        _sql_sb.AppendLine("         AND   AC_TRACK_DATA = dbo.TrackDataToInternal(@pCardNumber)");

        _sql_sb.AppendLine("      UPDATE   ELP01_TRACKDATA ");
        _sql_sb.AppendLine("         SET   ET_EXTERNAL_TRACKDATA        = 'RECYCLED(' + ISNULL(ET_EXTERNAL_TRACKDATA,'') + ')' ");
        _sql_sb.AppendLine("           ,   ET_EXTERNAL_CARD_ID          = NULL ");
        _sql_sb.AppendLine("           ,   ET_EXTERNAL_CARD_ID_STATUS   = @pStatus ");
        _sql_sb.AppendLine("           ,   ET_LAST_UPDATED              = GETDATE() ");
        _sql_sb.AppendLine("       WHERE   ET_EXTERNAL_ACCOUNT_ID       = @pIncCustomerNumber");
        _sql_sb.AppendLine("         AND   ET_EXTERNAL_TRACKDATA        = @pCardNumber");

        _sql_sb.AppendLine("    END ");
        _sql_sb.AppendLine("    ELSE ");
        _sql_sb.AppendLine("    BEGIN ");
        _sql_sb.AppendLine("      SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = dbo.TrackDataToInternal(@pCardNumber) AND AC_ACCOUNT_ID NOT IN (SELECT ET_AC_ACCOUNT_ID FROM ELP01_TRACKDATA WHERE ET_EXTERNAL_ACCOUNT_ID = @pIncCustomerNumber) AND AC_USER_TYPE <> @pAcUserTypeAnonymous), 0) ");
        _sql_sb.AppendLine("      IF @pOtherAccountId <> 0 ");
        _sql_sb.AppendLine("      BEGIN ");
        _sql_sb.AppendLine("        UPDATE   ACCOUNTS ");
        _sql_sb.AppendLine("           SET   AC_TRACK_DATA = ('00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR)) ");
        _sql_sb.AppendLine("         WHERE   AC_ACCOUNT_ID = @pOtherAccountId ");

        _sql_sb.AppendLine("        UPDATE   ELP01_TRACKDATA ");
        _sql_sb.AppendLine("           SET   ET_EXTERNAL_TRACKDATA        = 'RECYCLED(' + ISNULL(ET_EXTERNAL_TRACKDATA,'') + ')' ");
        _sql_sb.AppendLine("             ,   ET_EXTERNAL_CARD_ID          = NULL ");
        _sql_sb.AppendLine("             ,   ET_EXTERNAL_CARD_ID_STATUS   = 0 ");
        _sql_sb.AppendLine("             ,   ET_LAST_UPDATED              = GETDATE() ");
        _sql_sb.AppendLine("         WHERE   ET_AC_ACCOUNT_ID             = @pOtherAccountId");
        _sql_sb.AppendLine("           AND   ET_EXTERNAL_TRACKDATA        = @pCardNumber");

        _sql_sb.AppendLine("      END ");
        _sql_sb.AppendLine("    END ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_cmd.Parameters.Clear();
            _sql_cmd.Parameters.Add("@pIncCustomerNumber", SqlDbType.Decimal).SourceColumn = "INCCUSTOMERNUMBER";
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "STATUS";
            _sql_cmd.Parameters.Add("@pCardNumber", SqlDbType.VarChar).SourceColumn = "CARDNUMBER";
            _sql_cmd.Parameters.Add("@pAcUserTypeAnonymous", SqlDbType.Int).Value = (Int32)ACCOUNT_USER_TYPE.ANONYMOUS;

            _sql_da.InsertCommand = _sql_cmd;
            _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.ContinueUpdateOnError = false;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.Update(Ds.Tables[0]);
          }
        }

        //
        // Alarm: Try unasign card and not match "CustomerNumber-CardNumber"
        //
        _dr_array = Ds.Tables[0].Select("STATUS=0", "", DataViewRowState.Added);
        if (_dr_array.Length > 0)
        {
          _alarm_description.AppendLine(Resource.String("STR_ELP_ALARM_USED_TRACKDATA_0"));
          foreach (DataRow _dr in _dr_array)
          {
            _alarm_description.AppendLine(Resource.String("STR_ELP_ALARM_USED_TRACKDATA_1", _dr["CARDNUMBER"], _dr["INCCUSTOMERNUMBER"]));
          }

          ELPLog.Message("MultiSite_Elp01ExchangeTables - UsedTrackData. Error: " + _alarm_description.ToString());

          Alarm.Register(AlarmSourceCode.ELP01
           , 0
           , m_alarm_source_name
           , AlarmCode.ELP_ErrorTrackdataNotAssignedToAccount
           , _alarm_description.ToString());
        }
        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    }//UsedTrackData

    //------------------------------------------------------------------------------
    // PURPOSE : Read Pending Accounts table
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //
    //      - OUTPUT :
    //          - DtPlaySessions
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean Read_ELP01_AnonymousAccounts(out DataTable DtPlaySessions, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      DtPlaySessions = new DataTable("ELP01_ANONYMOUS_ACCOUNTS");
      try
      {
        _sb = new StringBuilder();
        _sb.Append(" SELECT  TOP (" + YAKS_INTERFACE_MAX_ROWS.ToString() + ") ");
        _sb.Append("               AC_ACCOUNT_ID ");
        _sb.Append("             , dbo.TrackDataToExternal(AC_TRACK_DATA) AS EXTERNAL_TRACK_DATA ");
        _sb.Append("             , ISNULL(AC_MS_CREATED_ON_SITE_ID, AC_MS_MODIFIED_ON_SITE_ID) AS AC_MS_CREATED_ON_SITE_ID ");
        _sb.Append("             , CASE WHEN ET_AC_ACCOUNT_ID IS NOT NULL AND dbo.TrackDataToExternal(AC_TRACK_DATA) <> ISNULL(ET_EXTERNAL_TRACKDATA,'') THEN 1 ELSE 0 END TRACKDATA_CHANGED ");
        _sb.Append("             , ISNULL(ET_LAST_UPDATED,GETDATE()) AS ET_LAST_UPDATED");
        _sb.Append("        FROM   ACCOUNTS WITH(INDEX(IX_ac_user_type)) ");
        _sb.Append("   LEFT JOIN   ELP01_TRACKDATA ON AC_ACCOUNT_ID = ET_AC_ACCOUNT_ID ");
        _sb.Append("       WHERE   AC_USER_TYPE = 0 ");
        _sb.Append("         AND   dbo.TrackDataToExternal(AC_TRACK_DATA) <> '' ");
        _sb.Append("         AND   ISNULL(AC_MS_CREATED_ON_SITE_ID, AC_MS_MODIFIED_ON_SITE_ID) > 0 ");
        _sb.Append("         AND   (ET_SPACE_REPORTED IS NULL ");
        _sb.Append("                OR dbo.TrackDataToExternal(AC_TRACK_DATA) <> ISNULL(ET_EXTERNAL_TRACKDATA,'')) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(DtPlaySessions);
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // Read_ELP01_PendingAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts or updates Elp01.TrackData
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ds
    //          - SqlTrans
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    private static Boolean UpdateElp01TrackData(Int64 AccountId, String ExternalTrackData, Int64? SpaceAccountId, Boolean TrackdataChanged, DateTime LastUpdated, SqlTransaction SqlTrans)
    {
      StringBuilder _sql_sb;

      try
      {
        _sql_sb = new StringBuilder();

        if (TrackdataChanged)
        {
          _sql_sb.AppendLine(" UPDATE   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("    SET   ET_EXTERNAL_TRACKDATA = @pExternalTrackData ");
          _sql_sb.AppendLine("        , ET_EXTERNAL_CARD_ID = @pExternalCardId ");
          _sql_sb.AppendLine("        , ET_LAST_UPDATED = GETDATE() ");
          _sql_sb.AppendLine("  WHERE   ET_AC_ACCOUNT_ID = @pAccountId ");
        }
        else
        {

          _sql_sb.AppendLine(" IF @pExternalAccountId IS NOT NULL AND EXISTS(SELECT 1 FROM ELP01_TRACKDATA WHERE ET_EXTERNAL_ACCOUNT_ID = @pExternalAccountId AND  ET_EXTERNAL_TRACKDATA NOT LIKE '%' + @pExternalTrackData + '%' )");
          _sql_sb.AppendLine(" BEGIN ");

          _sql_sb.AppendLine("           UPDATE  ELP01_TRACKDATA ");
          _sql_sb.AppendLine("              SET  ET_EXTERNAL_ACCOUNT_ID = NULL");
          _sql_sb.AppendLine("                ,  ET_LAST_UPDATED = GETDATE() ");
          _sql_sb.AppendLine("                ,  ET_SPACE_REPORTED = ISNULL(ET_SPACE_REPORTED, @pSpaceReported) ");
          _sql_sb.AppendLine("            WHERE  ET_EXTERNAL_ACCOUNT_ID = @pExternalAccountId ");
          _sql_sb.AppendLine("              AND  ET_EXTERNAL_TRACKDATA NOT LIKE '%' + @pExternalTrackData + '%' ");

          _sql_sb.AppendLine(" END ");

          _sql_sb.AppendLine(" DECLARE  @Personal_AccountId BIGINT ");
          _sql_sb.AppendLine(" DECLARE  @Personal_SpaceAccountId BIGINT ");

          _sql_sb.AppendLine("  SELECT  TOP 1 @Personal_AccountId = ET_AC_ACCOUNT_ID ");
          _sql_sb.AppendLine("    FROM  ELP01_TRACKDATA ");
          _sql_sb.AppendLine("   WHERE  ET_EXTERNAL_ACCOUNT_ID IS NOT NULL ");
          //_sql_sb.AppendLine("     AND  ET_EXTERNAL_ACCOUNT_ID = @pExternalAccountId ");
          _sql_sb.AppendLine("     AND  ET_EXTERNAL_TRACKDATA LIKE '%' + @pExternalTrackData + '%' ");
          _sql_sb.AppendLine("     AND  ET_AC_ACCOUNT_ID <> @pAccountId ");
          _sql_sb.AppendLine(" ORDER BY ET_EXTERNAL_ACCOUNT_ID DESC ");

          _sql_sb.AppendLine("  IF @Personal_AccountId IS NOT NULL ");
          _sql_sb.AppendLine("  BEGIN ");
          _sql_sb.AppendLine("           SELECT @Personal_SpaceAccountId = ET_EXTERNAL_ACCOUNT_ID FROM ELP01_TRACKDATA WHERE ET_AC_ACCOUNT_ID = @Personal_AccountId ");

          _sql_sb.AppendLine("           UPDATE  ELP01_TRACKDATA ");
          _sql_sb.AppendLine("              SET  ET_EXTERNAL_ACCOUNT_ID = NULL ");
          _sql_sb.AppendLine("                ,  ET_EXTERNAL_CARD_ID = NULL ");
          _sql_sb.AppendLine("                ,  ET_EXTERNAL_TRACKDATA = 'RECYCLED(' + ISNULL(ET_EXTERNAL_TRACKDATA,'') + ')' ");
          _sql_sb.AppendLine("                ,  ET_LAST_UPDATED = GETDATE() ");
          _sql_sb.AppendLine("            WHERE  ET_AC_ACCOUNT_ID = @Personal_AccountId ");

          _sql_sb.AppendLine("           IF NOT EXISTS ");
          _sql_sb.AppendLine("               (SELECT   1 ");
          _sql_sb.AppendLine("                  FROM   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                 WHERE   ET_AC_ACCOUNT_ID = @pAccountId ) ");
          _sql_sb.AppendLine("           BEGIN ");
          _sql_sb.AppendLine("                    INSERT   INTO ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                           ( ET_AC_ACCOUNT_ID ");
          _sql_sb.AppendLine("                           , ET_EXTERNAL_ACCOUNT_ID ");
          _sql_sb.AppendLine("                           , ET_EXTERNAL_CARD_ID ");
          _sql_sb.AppendLine("                           , ET_EXTERNAL_TRACKDATA ");
          _sql_sb.AppendLine("                           , ET_EXTERNAL_CARD_ID_STATUS ");
          _sql_sb.AppendLine("                           , ET_SPACE_REPORTED ");
          _sql_sb.AppendLine("                           , ET_REPLACED_ACCOUNT_ID ");
          _sql_sb.AppendLine("                           , ET_LAST_UPDATED) ");
          _sql_sb.AppendLine("                    VALUES ( @pAccountId ");
          _sql_sb.AppendLine("                           , @Personal_SpaceAccountId ");
          _sql_sb.AppendLine("                           , @pExternalCardId ");
          _sql_sb.AppendLine("                           , @pExternalTrackData ");
          _sql_sb.AppendLine("                           , 1 ");
          _sql_sb.AppendLine("                           , @pSpaceReported ");
          _sql_sb.AppendLine("                           , @Personal_AccountId ");
          _sql_sb.AppendLine("                           , GETDATE()) ");
          _sql_sb.AppendLine("           END ");
          _sql_sb.AppendLine("           ELSE ");
          _sql_sb.AppendLine("           BEGIN ");
          _sql_sb.AppendLine("                    UPDATE   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                       SET   ET_EXTERNAL_ACCOUNT_ID = @Personal_SpaceAccountId ");
          _sql_sb.AppendLine("                           , ET_SPACE_REPORTED = @pSpaceReported ");
          _sql_sb.AppendLine("                           , ET_REPLACED_ACCOUNT_ID = @Personal_AccountId ");
          _sql_sb.AppendLine("                           , ET_LAST_UPDATED = CASE WHEN ISNULL(@Personal_SpaceAccountId,0) <> ISNULL(ET_EXTERNAL_ACCOUNT_ID,0) OR ISNULL(ET_SPACE_REPORTED,0) <> ISNULL(@pSpaceReported,0) OR ISNULL(ET_REPLACED_ACCOUNT_ID,0) <> ISNULL(@Personal_AccountId,0) THEN GETDATE() ELSE ET_LAST_UPDATED END ");
          _sql_sb.AppendLine("                     WHERE   ET_AC_ACCOUNT_ID = @pAccountId ");
          _sql_sb.AppendLine("           END ");
          _sql_sb.AppendLine("  END");
          _sql_sb.AppendLine("  ELSE");
          _sql_sb.AppendLine("  BEGIN");
          _sql_sb.AppendLine("           IF NOT EXISTS ");
          _sql_sb.AppendLine("               (SELECT   1 ");
          _sql_sb.AppendLine("                  FROM   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                 WHERE   ET_AC_ACCOUNT_ID = @pAccountId ) ");
          _sql_sb.AppendLine("           BEGIN ");
          _sql_sb.AppendLine("                      INSERT   INTO ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                             ( ET_AC_ACCOUNT_ID ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_ACCOUNT_ID ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_CARD_ID ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_TRACKDATA ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_CARD_ID_STATUS ");
          _sql_sb.AppendLine("                             , ET_SPACE_REPORTED ");
          _sql_sb.AppendLine("                             , ET_LAST_UPDATED) ");
          _sql_sb.AppendLine("                      VALUES ( @pAccountId ");
          _sql_sb.AppendLine("                             , @pExternalAccountId ");
          _sql_sb.AppendLine("                             , @pExternalCardId ");
          _sql_sb.AppendLine("                             , @pExternalTrackData ");
          _sql_sb.AppendLine("                             , 1 ");
          _sql_sb.AppendLine("                             , CASE WHEN @pExternalAccountId IS NULL THEN NULL ELSE @pSpaceReported END ");
          _sql_sb.AppendLine("                             , GETDATE()) ");
          _sql_sb.AppendLine("           END ");
          _sql_sb.AppendLine("           ELSE ");
          _sql_sb.AppendLine("           BEGIN ");
          _sql_sb.AppendLine("                      UPDATE   ELP01_TRACKDATA ");
          _sql_sb.AppendLine("                         SET   ET_EXTERNAL_ACCOUNT_ID = @pExternalAccountId ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_TRACKDATA = @pExternalTrackData ");
          _sql_sb.AppendLine("                             , ET_EXTERNAL_CARD_ID = @pExternalCardId ");
          _sql_sb.AppendLine("                             , ET_SPACE_REPORTED = CASE WHEN @pExternalAccountId IS NULL THEN NULL ELSE @pSpaceReported END ");
          _sql_sb.AppendLine("                             , ET_LAST_UPDATED = CASE WHEN ISNULL(@pExternalAccountId,0) <> ISNULL(ET_EXTERNAL_ACCOUNT_ID,0) OR ISNULL(ET_EXTERNAL_TRACKDATA,'') <> ISNULL(@pExternalTrackData,'') OR ISNULL(ET_EXTERNAL_CARD_ID,0) <> ISNULL(@pExternalCardId,0) THEN GETDATE() ELSE ET_LAST_UPDATED END ");
          _sql_sb.AppendLine("                       WHERE   ET_AC_ACCOUNT_ID = @pAccountId ");
          _sql_sb.AppendLine("           END ");
          _sql_sb.AppendLine("  END");

        }

        _sql_sb.AppendLine("  IF DATEDIFF(MINUTE,@pLastUpdated,GETDATE()) > 60 -- 1h");
        _sql_sb.AppendLine("  BEGIN");
        _sql_sb.AppendLine("      UPDATE   ELP01_TRACKDATA ");
        _sql_sb.AppendLine("         SET   ET_SPACE_REPORTED = @pSpaceReported_Timeout ");
        _sql_sb.AppendLine("       WHERE   ET_AC_ACCOUNT_ID = @pAccountId ");
        _sql_sb.AppendLine("         AND   ET_SPACE_REPORTED IS NULL ");
        _sql_sb.AppendLine("  END");


        using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pSpaceReported", SqlDbType.Int).Value = SPACE_REPORTED.INFORMED;
          _sql_cmd.Parameters.Add("@pSpaceReported_Duplicate", SqlDbType.Int).Value = SPACE_REPORTED.INFORMED_AND_DUPLICATED;
          _sql_cmd.Parameters.Add("@pSpaceReported_Timeout", SqlDbType.Int).Value = SPACE_REPORTED.UNINFORMED_TIMEOUT;
          _sql_cmd.Parameters.Add("@pLastUpdated", SqlDbType.DateTime).Value = LastUpdated;

          if (SpaceAccountId <= 0)
          {
            _sql_cmd.Parameters.Add("@pExternalAccountId", SqlDbType.Decimal).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pExternalAccountId", SqlDbType.Decimal).Value = SpaceAccountId;
          }

          _sql_cmd.Parameters.Add("@pExternalCardId", SqlDbType.Decimal).Value = Convert.ToDecimal(ExternalTrackData);
          _sql_cmd.Parameters.Add("@pExternalTrackData", SqlDbType.NVarChar, 50).Value = ExternalTrackData;
          _sql_cmd.Parameters.Add("@pUserTypePersonal", SqlDbType.Int).Value = ACCOUNT_USER_TYPE.PERSONAL;

          if (_sql_cmd.ExecuteNonQuery() == 0)
          {
            ELPLog.Warning("UpdateElp01TrackData failed. AccountId: " + AccountId.ToString());

            return false;
          }
        } // SqlCommand

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Warning(_ex.ToString());
        ELPLog.Warning("UpdateElp01TrackData failed. AccountId: " + AccountId.ToString());
      }

      return false;
    } // UpdateElp01TrackData

    //------------------------------------------------------------------------------
    // PURPOSE : Show Anonymous accounts statistics
    //
    //  PARAMS :
    //      - INPUT :
    //         
    //      - OUTPUT : Write in log file
    //
    // RETURNS :
    //------------------------------------------------------------------------------
    private static void WriteAnonymousAccountsStatistics()
    {
      Int64 _tick_total;
      StringBuilder _msg;

      _tick_total = m_ticks_select + m_ticks_ws + m_ticks_save;

      if (_tick_total == 0 && m_count_selects == 0 && m_count_procesed == 0)
      {
        return;
      }

      if (Misc.GetElapsedTicks(m_stats_log_tick) < 5 * 60000)
      {
        return;
      }

      _msg = new StringBuilder();

      m_stats_log_tick = Misc.GetTickCount();

      if (m_count_selects > 0)
      {
        m_ticks_select /= m_count_selects;
      }

      if (m_count_procesed > 0)
      {
        m_ticks_ws /= m_count_procesed;
        m_ticks_save /= m_count_procesed;
      }

      _tick_total = m_ticks_select + m_ticks_ws + m_ticks_save;

      _msg.Append("SPACE: Anonymous Accounts Stats Avg times: [Accounts/Select (ms)/WS (ms)/Update (ms)/Total (ms)]: ");
      _msg.Append("[ " + m_count_procesed.ToString() +
                 " / " + m_ticks_select.ToString() +
                 " / " + m_ticks_ws.ToString() +
                 " / " + m_ticks_save.ToString() +
                 " / " + _tick_total.ToString() + " ]");

      ELPLog.Message(_msg.ToString());
      m_count_selects = 0;
      m_count_procesed = 0;
      m_ticks_select = 0;
      m_ticks_ws = 0;
      m_ticks_save = 0;

    } // WriteAnonymousAccountsStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Send To Site
    //
    //  PARAMS :
    //      - INPUT :
    //          - DsLoyaltyCard
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Error.
    private static Boolean NotifyToSite(DataTable DsLoyaltyCard)
    {
      SecureTcpClient _client;
      Int32 _prev_site_id;
      Int32 _site_id;
      WWP_Message _wwp_message;
      WWP_MsgCreateAccounts.AccountList _account_list;
      ELP01_PENDING_DOWNLOAD_ACCOUNT _pending_account;
      ////String _alarm_description;

      _prev_site_id = -1;
      _client = null;
      _wwp_message = null;
      _account_list = null;

      try
      {
        if (DsLoyaltyCard == null)
        {
          return true;
        }

        foreach (DataRow _row in DsLoyaltyCard.Select("STATUS = 1"))
        {
          _site_id = Convert.ToInt32(_row["VENUECODE"]);

          if (_row.IsNull("ACCOUNTID"))
          {
            ELPLog.Message(String.Format("MultiSite_Elp01ExchangeTables - NotifyToSite - Account incompleted. (Account:NULL | TrackData:{0} | Site:{1})", _row["CARDNUMBER"], _site_id));

            continue;
          }

          if (_site_id != _prev_site_id)
          {
            if (_client != null)
            {
              _client.Send(_wwp_message.ToXml());
            }

            _client = null;
            foreach (SecureTcpClient _aux in WWP_Server.WWP.Server.Clients)
            {
              if (_aux.InternalId == _site_id)
              {
                if (_aux.IsConnected)
                {
                  _prev_site_id = _site_id;
                  WWP_MsgCreateAccounts _wwp_accounts;

                  _client = _aux;
                  _wwp_message = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgCreateAccounts);
                  _wwp_accounts = (WWP_MsgCreateAccounts)_wwp_message.MsgContent;
                  _account_list = _wwp_accounts.List;
                }
                break;
              }
            }
          }

          if (_client == null)
          {
            _pending_account.site_id = Convert.ToInt32(_row["VENUECODE"]);
            _pending_account.account_id = Convert.ToInt64(_row["ACCOUNTID"]);
            _pending_account.track_data = _row["CARDNUMBER"].ToString();

            InsertAccountPendingDownload(_pending_account);

            continue;
          }

          // Add Account/Track to list
          _account_list.Add(Convert.ToInt64(_row["ACCOUNTID"]), _row["CARDNUMBER"].ToString());

        } // foreach

        if (_client != null)
        {
          _client.Send(_wwp_message.ToXml());
        }

        return true;
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // NotifyToSite

    //------------------------------------------------------------------------------
    // PURPOSE : inserts data into accounts pending to download table
    //
    //  PARAMS :
    //      - INPUT :
    //          - ELP01_PENDING_DOWNLOAD_ACCOUNT
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Error.
    private static Boolean InsertAccountPendingDownload(ELP01_PENDING_DOWNLOAD_ACCOUNT Account)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        if (Account.account_id > 0
          && Account.site_id > 0
          && !String.IsNullOrEmpty(Account.track_data))
        {
          _sb.Append(" IF NOT EXISTS(SELECT 1 FROM ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD WHERE EA_SITE_ID = @pSiteId AND EA_ACCOUNT_ID = @pAccountId) ");
          _sb.Append(" BEGIN ");
          _sb.Append("      INSERT INTO ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD ");
          _sb.Append("                 (EA_ACCOUNT_ID ");
          _sb.Append("                 ,EA_TRACKDATA ");
          _sb.Append("                 ,EA_SITE_ID ) ");
          _sb.Append("           VALUES ");
          _sb.Append("                 (@pAccountId ");
          _sb.Append("                 ,@pTrackData ");
          _sb.Append("                 ,@pSiteId) ");
          _sb.Append(" END ");
          _sb.Append(" ELSE ");
          _sb.Append(" BEGIN ");
          _sb.Append("      UPDATE ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD ");
          _sb.Append("         SET EA_TRACKDATA = @pTrackData ");
          _sb.Append("           , EA_SITE_ID = @pSiteId ");
          _sb.Append("       WHERE EA_SITE_ID = @pSiteId ");
          _sb.Append("         AND EA_ACCOUNT_ID = @pAccountId ");
          _sb.Append(" END ");

          if (Account.track_data == null)
          {
            Log.Warning(string.Format("MS_ELP01_ExchangeTables.InsertAccountPendingDownload: It has detected an Account with TrackData = null. AccountID:{0} SiteID:{1}", Account.account_id, Account.site_id));
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Account.account_id;
              _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.VarChar).Value = Account.track_data;
              _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Account.site_id;

              if (_sql_cmd.ExecuteNonQuery() != 1)
              {
                ELPLog.Message(String.Format("MultiSite_Elp01ExchangeTables - InsertAccountPendingDownload - Account not inserted. (Account: {0}, TrackData:{1} SiteId:{2})", Account.account_id, Account.track_data, Account.site_id));
              }
            } // _sql_cmd
            _db_trx.Commit();
          } // _db_trx
        }
        else
        {
          ELPLog.Message(String.Format("MultiSite_Elp01ExchangeTables - InsertAccountPendingDownload - Account incompleted. (Account:{0} | TrackData:{1} | Site:{2})", Account.account_id, Account.track_data, Account.site_id));
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // InsertAccountPendingDownload

    //------------------------------------------------------------------------------
    // PURPOSE : Read data from accounts pending to download table
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //
    //      - OUTPUT :
    //          - out DsAccountPendingDownload
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Error.
    private static Boolean ReadAndDeleteAccountPendingDownload(out DataTable DtAccountPendingDownload, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;
      DtAccountPendingDownload = new DataTable("ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD");

      try
      {
        _sb = new StringBuilder();

        _sb.Append(" DELETE FROM  ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD ");
        _sb.Append("      OUTPUT  DELETED.EA_ACCOUNT_ID ");
        _sb.Append("            , DELETED.EA_TRACKDATA ");
        _sb.Append("            , DELETED.EA_SITE_ID ");
        _sb.Append("       WHERE  EA_ACCOUNT_ID IN (SELECT   TOP " + YAKS_INTERFACE_MAX_ROWS.ToString() + " EA_ACCOUNT_ID FROM ELP01_ACCOUNTS_PENDING_TO_DOWNLOAD ORDER BY EA_SITE_ID) ");


        // TODO: Order by Site Id, AccountId


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(DtAccountPendingDownload);
          }
        }

        return true;
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // ReadAndDeleteAccountPendingDownload

    //------------------------------------------------------------------------------
    // PURPOSE : Check if Exists some record in LoyaltyCardInterface table
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrans
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True: Exist
    //      -  False: Not Exist
    private static Boolean ExistsLoyaltyCardInterface(SqlTransaction SqlTrans)
    {
      Boolean _exists;
      StringBuilder _sb;

      _exists = false;
      try
      {
        _sb = new StringBuilder();

        _sb.Append(" SELECT   TOP 1");
        _sb.Append("          RecordNumber ");
        _sb.Append("   FROM   LOYALTYCARDINTERFACE ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          if (_sql_cmd.ExecuteScalar() != null)
          {
            _exists = true;
          }
        }
      } // try
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return _exists;
    } // ExistsLoyaltyCardInterface

    #endregion // Private Methods

  } // static class 

} // namespace