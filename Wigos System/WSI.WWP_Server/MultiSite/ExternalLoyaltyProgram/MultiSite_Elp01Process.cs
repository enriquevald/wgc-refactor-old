//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_SiteRequestsProcess.cs
// 
//   DESCRIPTION: Site Requests process.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 24-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 24-APR-2013 ACC    First release.
// 23-JUL-2014 RRR    Changed Log to ELPLog
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;

namespace WSI.WWP_CenterService.MultiSite
{
  public static class MS_ELP01_Process
  {

    public enum WWP_Elp01RedeemAuditStatus
    {
      REDEEM_AUDIT_STATUS_REQUESTED = 0
    , REDEEM_AUDIT_STATUS_CONFIRM = 1
    }

    // PURPOSE : Insert a new Redeem Voucher Audit.
    //
    //  PARAMS :
    //      - INPUT:
    //            - SiteId
    //            - InputData
    //            - OutputData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    public static Boolean InsertElp01RedeemAudit(Int32 SiteId, Elp01InputData InputData, Elp01OutputData OutputData)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   ELP01_REDEEM_AUDIT   ");
        _sb.AppendLine("             ( ERA_VOUCHER_ID  ");
        _sb.AppendLine("             , ERA_SITE_ID  ");
        _sb.AppendLine("             , ERA_ACCOUNT_ID  ");
        _sb.AppendLine("             , ERA_STATUS  ");
        _sb.AppendLine("             , ERA_RESPONSE_AMOUNT_REDEEMABLE  ");
        _sb.AppendLine("             , ERA_RESPONSE_AMOUNT_NON_REDEEMABLE  ");
        _sb.AppendLine("             , ERA_RESPONSE_STATUS_CODE  ");
        _sb.AppendLine("             , ERA_CREATED ) ");
        _sb.AppendLine("      VALUES ( @pVoucherId  ");  
        _sb.AppendLine("             , @pSiteId  ");  
        _sb.AppendLine("             , @pAccountId  ");
        _sb.AppendLine("             , @pStatus  ");  
        _sb.AppendLine("             , @pResponseAmountRedeemable  ");
        _sb.AppendLine("             , @pResponseAmountNonRedeemable  ");
        _sb.AppendLine("             , @pResponseStatusCode  "); 
        _sb.AppendLine("             , GETDATE() ) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pVoucherId", SqlDbType.NVarChar).Value = InputData.VoucherId;
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InputData.AccountId;
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)WWP_Elp01RedeemAuditStatus.REDEEM_AUDIT_STATUS_REQUESTED;
            _sql_cmd.Parameters.Add("@pResponseAmountRedeemable", SqlDbType.Money).Value = ((Decimal)OutputData.AmountRedeemableCents / 100);
            _sql_cmd.Parameters.Add("@pResponseAmountNonRedeemable", SqlDbType.Money).Value = ((Decimal)OutputData.AmountNonRedeemableCents / 100);
            _sql_cmd.Parameters.Add("@pResponseStatusCode", SqlDbType.Int).Value = (Int32)OutputData.ResponseCodes;

            _db_trx.ExecuteNonQuery(_sql_cmd);
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // InsertElp01RedeemAudit

    // PURPOSE : Update Confirm Redeem Voucher Audit.
    //
    //  PARAMS :
    //      - INPUT:
    //            - SiteId
    //            - InputData
    //            - OutputData
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    public static Boolean UpdateElp01RedeemAudit(Int32 SiteId, Elp01InputData InputData, Elp01OutputData OutputData)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   ELP01_REDEEM_AUDIT  ");
        _sb.AppendLine("    SET   ERA_STATUS = @pStatus ");
        _sb.AppendLine(" WHERE   ERA_UNIQUE_ID = (      ");
        _sb.AppendLine("                            SELECT MAX(ERA_UNIQUE_ID) FROM ELP01_REDEEM_AUDIT ");
        _sb.AppendLine("                            WHERE   ERA_SITE_ID    = @pSiteId     ");
        _sb.AppendLine("                            AND   ERA_ACCOUNT_ID = @pAccountId    ");
        _sb.AppendLine("                            AND   ERA_VOUCHER_ID = @pVoucherId  ) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pVoucherId", SqlDbType.NVarChar).Value = InputData.VoucherId;
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InputData.AccountId;
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)WWP_Elp01RedeemAuditStatus.REDEEM_AUDIT_STATUS_CONFIRM;

            _db_trx.ExecuteNonQuery(_sql_cmd);
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return false;
    } // UpdateElp01RedeemAudit

  } // static class 
} // namespace

