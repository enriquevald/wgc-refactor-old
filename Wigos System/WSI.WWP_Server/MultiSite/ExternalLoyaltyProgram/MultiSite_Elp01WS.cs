//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_Elp01WS.cs
// 
//   DESCRIPTION: External Loyalty Program 01 WS calls.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
// 26-APR-2013 JVV    Create call WS methods
// 17-FBR-2014 JVV    Create call WS Anonymous Accounts
// 30-JUN-2014 SMN    Fixed bug #WIG-1057: Creation of accounts with Id = 0, partial commit, add new stat to general param
// 23-JUL-2014 RRR    Changed Log to ELPLog
// 26-MAR-2018 JGC    PBI 32313:[WIGOS-9404]: Codere_Mexico: Call to space to get token
// 26-MAR-2018 JGC    PBI 32312:[WIGOS-9407]: Codere_Mexico Space: Use token to call space servicesModification to add token security for calls to Space
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;
using WSI.WWP_CenterService.MultiSite.Space;
using WSI.WWP_CenterService.PointsReference;
using WSI.WWP_CenterService.RedeemReference;
using WSI.WWP_CenterService.ConfirmReference;
using WSI.WWP_CenterService.AnonymAccountsReference;
using System.Threading;
using System.Web.Services.Protocols;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.WWP_CenterService.MultiSite
{
  public static class MS_ELP01_WS
  {

    #region enums

    public enum Type_Serialize
    {
      input = 0,
      output = 1
    }

    public enum Type_Action
    {
      points = 0,
      redeem = 1,
      confirm = 2,
    }

    #endregion  // enums

    #region constants
    public const String GP_ELP01 = "ExternalLoyaltyProgram.Mode01";
    public const String GP_ELP01_URL = "http://csieapmex/eai_esn/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&WSSOAP=1";

    public const String SPACE_REDEEM_VOUCHER_REDEEMABLE = "Efectivo";
    public const String SPACE_REDEEM_VOUCHER_NON_REDEEMABLE = "Fondo Disponible";
    public const String SPACE_REDEEM_VOUCHER_RESTRICTED_TO_GROUP = "Grupo";

    #endregion // constants

    #region members
    private static ELP_Space m_elp_space = new ELP_Space();

    #endregion // members

    //------------------------------------------------------------------------------
    // PURPOSE: Call GetPointsSpace include login/logout
    //
    //  PARAMS:
    //      - INPUT:
    //            - IncCustomerNumber 
    //           
    //      - OUTPUT:
    //            - SpacePoints 
    //  
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void GetPoints(ELP_Space.HEADER_TYPE HeaderType, Int64 IncCustomerNumber, out Int64 SpacePoints, out ELP01_ResponseCodes ResponseCode)
    {
      Boolean _token_error;

      _token_error = false;
      SpacePoints = 0;
      ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;

      try
      {
        GetPointsSpace(HeaderType, IncCustomerNumber, out SpacePoints, out _token_error);
        ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK;
      }
      catch (System.Net.WebException _web_ex)
      {
        ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_WS_URL_FAILED;
        if (_web_ex.Status == System.Net.WebExceptionStatus.Timeout)
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TIMEOUT;
        }
      }
      catch (Exception _ex)
      {
        if (_token_error)
        {
          _ex = new Exception("Token is missing or invalid or has expired.");
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TOKEN_ERROR;
        }
        else
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;
        }

        ELPLog.Exception(_ex);
      }
    }// wsGetPoints

    //------------------------------------------------------------------------------
    // PURPOSE: Call RedeemSpaceVoucher include login/logout
    //
    //  PARAMS:
    //      - INPUT:
    //           - IncCustomerNumber 
    //           
    //      - OUTPUT:
    //            - RedeemableAmount 
    //            - NonRedeemableAmount 
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void RedeemVoucher(ELP_Space.HEADER_TYPE HeaderType,
                                     Int64 IncCustomerNumber,
                                     String VoucherId,
                                     out Int64 RedeemableAmountCents,
                                     out Int64 NonRedeemableAmountCents,
                                     out Int32 RestrictedToGroupId,
                                     out ELP01_ResponseCodes ResponseCode)
    {
      Boolean _token_error;

      _token_error = false;
      RedeemableAmountCents = 0;
      NonRedeemableAmountCents = 0;
      RestrictedToGroupId = 0;
      ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;

      try
      {
        if (RedeemSpaceVoucher(HeaderType,
                              IncCustomerNumber,
                              VoucherId,
                              out RedeemableAmountCents,
                              out NonRedeemableAmountCents,
                              out RestrictedToGroupId,
                              out _token_error))
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK;
        }
        else
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_INCORRECT_VOUCHER;
        }

      }
      catch (System.Net.WebException _web_ex)
      {
        ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_WS_URL_FAILED;
        if (_web_ex.Status == System.Net.WebExceptionStatus.Timeout)
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TIMEOUT;
        }
      }
      catch (Exception _ex)
      {
        if (_token_error)
        {
          _ex = new Exception("Token is missing or invalid or has expired.");
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TOKEN_ERROR;
        }
        else
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;
        }

        ELPLog.Exception(_ex);
      }

    }// wsRedeemVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Call ConfirmRedeemSpaceVoucher include login/logout
    //
    //  PARAMS:
    //      - INPUT:
    //           - IncCustomerNumber 
    //           - VoucherId
    //      - OUTPUT:
    //             
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void ConfirmRedeem(ELP_Space.HEADER_TYPE HeaderType,
                                     Int64 IncCustomerNumber,
                                     String VoucherId,
                                     out ELP01_ResponseCodes ResponseCode)
    {
      String _confirm_response_code;
      Boolean _token_error;

      ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;
      _token_error = false;

      try
      {
        ConfirmRedeemSpaceVoucher(HeaderType, VoucherId, out _confirm_response_code, out _token_error);
        ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK;
      }
      catch (System.Net.WebException _web_ex)
      {
        ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_WS_URL_FAILED;
        if (_web_ex.Status == System.Net.WebExceptionStatus.Timeout)
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TIMEOUT;
        }
      }
      catch (Exception _ex)
      {
        if (_token_error)
        {
          _ex = new Exception("Token is missing or invalid or has expired.");
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TOKEN_ERROR;
        }
        else
        {
          ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_ERROR;
        }

        ELPLog.Exception(_ex);
      }
    }// wsConfirmRedeem

    #region WS Calls
    //------------------------------------------------------------------------------
    // PURPOSE: Get points from SPACE

    //  PARAMS:
    //      - INPUT:
    //           - IncCustomerNumber 

    //      - OUTPUT:
    //           - SpacePoints    
    // RETURNS:
    //           - Boolean
    //   NOTES:

    private static void GetPointsSpace(ELP_Space.HEADER_TYPE HeaderType,
                                       Int64 IncCustomerNumber,
                                       out Int64 SpacePoints,
                                       out Boolean TokenError)
    {
      CDR_spcLOY_spcMember_spcPoint_Input _input;
      CDR_spcLOY_spcMember_spcPoint_Output _output;
      String _url;
      Int32 _time_out;
      String _xml_output;
      String _xml_input;
      ELP01_Requests _action;

      _input = new CDR_spcLOY_spcMember_spcPoint_Input();
      _output = new CDR_spcLOY_spcMember_spcPoint_Output();
      SpacePoints = 0;
      _url = string.Empty;
      _time_out = -1;
      _xml_output = string.Empty;
      _xml_input = string.Empty;
      _action = ELP01_Requests.ELP01_REQUEST_QUERY_POINTS;
      TokenError = false;

      try
      {
        _url = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "WsPoints.Url", GP_ELP01_URL);
        _time_out = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsPoints.Timeout", 30000);

        _input.Numero_spcMiembro = IncCustomerNumber.ToString();
        _xml_input = _input.Serialize(ELP_Space.m_tag_america, ELP_Space.m_namespace_america);

        if (m_elp_space.DoWebRequest(HeaderType, _xml_input, _action, _url, _time_out, out _xml_output, out TokenError))
        {
          _output = _output.Deserialize(_xml_output, ELP_Space.m_tag_america);
        }
        else
        {
          // In case of error, get the error to trace Exception
          _output.Error_spcCode = "KO";
          _output.Error_spcMessage = _xml_output;
        }

        if (_output.Error_spcCode.ToUpper() == "OK")
        {
          SpacePoints = Int64.Parse(_output.Puntos_spcMiembro);
        }
        else
        {
          throw new Exception(_output.Error_spcMessage);
        }
      }
      catch (Exception _ex)
      {
        throw _ex;
      }

    }// GetPointsSpace

    //------------------------------------------------------------------------------
    // PURPOSE: Confirm Redeem SPACE Voucher in to WIGOS
    //
    //  PARAMS:
    //      - INPUT:
    //           - IncCustomerNumber 
    //           - VoucherId
    //      - OUTPUT:
    //           - ResponseConfirm  
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    private static void ConfirmRedeemSpaceVoucher(ELP_Space.HEADER_TYPE HeaderType,
                                                  String VoucherId,
                                                  out String ResponseConfirm,
                                                  out Boolean TokenError)
    {
      CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow_Input _input;
      CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow_Output _output;
      String _url;
      Int32 _time_out;
      String _xml_output;
      String _xml_input;
      ELP01_Requests _action;

      ResponseConfirm = "";
      _input = new CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow_Input();
      _output = new CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow_Output();
      _url = string.Empty;
      _time_out = -1;
      _xml_output = string.Empty;
      _xml_input = string.Empty;
      _action = ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM;
      TokenError = false;

      try
      {
        if (!String.IsNullOrEmpty(VoucherId))
        {
          _url = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "WsConfirm.Url", GP_ELP01_URL);
          _time_out = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsConfirm.Timeout", 30000);

          _input.CDR_spcTransaccion_spcNumber = VoucherId;
          _xml_input = _input.Serialize(ELP_Space.m_tag_siebel, ELP_Space.m_namespace_siebel);

          if (m_elp_space.DoWebRequest(HeaderType, _xml_input, _action, _url, _time_out, out _xml_output, out TokenError))
          {
            _output = _output.Deserialize(_xml_output, ELP_Space.m_tag_siebel);
          }
          else
          {
            // In case of error, get the error to trace Exception
            _output.Error_spcCode = "KO";
            _output.Error_spcMessage = _xml_output;
          }

          if (_output.Error_spcCode.ToUpper() == "OK")
          {
            ResponseConfirm = _output.Error_spcMessage;
          }
          else
          {
            ResponseConfirm = _output.Error_spcMessage;
            throw new Exception(_output.Error_spcMessage);
          }
        } // if
        else
        {
          throw new ArgumentException("VoucherId is null or empty");
        }
      }
      catch (Exception _ex)
      {
        throw _ex;
      }

    }// ConfirmRedeemSpaceVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Redeem SPACE Voucher
    //
    //  PARAMS:
    //      - INPUT:
    //           - IncCustomerNumber 
    //
    //      - OUTPUT:
    //           - RedeemableAmount   
    //           - NonRedeemableAmount  
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    private static Boolean RedeemSpaceVoucher(ELP_Space.HEADER_TYPE HeaderType,
                                              Int64 IncCustomerNumber,
                                              String VoucherId,
                                              out Int64 RedeemableAmountCents,
                                              out Int64 NonRedeemableAmountCents,
                                              out Int32 RestrictedToGroupId,
                                              out Boolean TokenError)
    {
      //Random _rnd;
      CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input _input;
      CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output _output;
      String _restricted_to_group_key;
      Decimal _parsing_point;
      Decimal _parsing_comma;
      Int64 _amount_cents;
      String _url;
      Int32 _time_out;
      String _xml_output;
      String _xml_input;
      ELP01_Requests _action;

      RedeemableAmountCents = 0;
      NonRedeemableAmountCents = 0;
      RestrictedToGroupId = 0;
      _restricted_to_group_key = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "KeyGroupProductType", SPACE_REDEEM_VOUCHER_RESTRICTED_TO_GROUP);
      _input = new CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input();
      _output = new CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Output();
      _url = string.Empty;
      _time_out = -1;
      _xml_output = string.Empty;
      _xml_input = string.Empty;
      _action = ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM;
      TokenError = false;

      try
      {
        if (String.IsNullOrEmpty(VoucherId))
        {
          return false;
        }

        _url = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "WsRedeem.Url", GP_ELP01_URL);
        _time_out = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsRedeem.Timeout", 30000);

        _input.Numero_spcMiembro = IncCustomerNumber.ToString();
        _xml_input = _input.Serialize(ELP_Space.m_tag_siebel, ELP_Space.m_namespace_siebel);

        if (m_elp_space.DoWebRequest(HeaderType, _xml_input, _action, _url, _time_out, out _xml_output, out TokenError))
        {
          _output = _output.Deserialize(_xml_output, ELP_Space.m_tag_siebel);
        }
        else
        {
          // In case of error, get the error to trace Exception
          _output.Error_spcCode = "KO";
          _output.Error_spcMessage = _xml_output;
        }

        if (_output.Error_spcCode.ToUpper() != "OK")
        {
          throw new Exception(_output.Error_spcMessage);
        }

        // Search VoucherId 
        foreach (LoyTransaction _loy_transaction in _output.ListOfLoyTransactionIo)
        {
          if (_loy_transaction.TransactionNumber != VoucherId)
          {
            continue;
          }
          if (Int64.Parse(_loy_transaction.MemberNumber) != IncCustomerNumber)
          {
            continue;
          }

          _amount_cents = 0;
          if (Decimal.TryParse(_loy_transaction.Monto, out _parsing_point))
          {
            _amount_cents = (Int64)(_parsing_point * 100); // Amount To Cents
          }
          if (Decimal.TryParse(_loy_transaction.Monto.Replace(",", "."), out _parsing_comma))
          {
            _amount_cents = (Int64)(Math.Min(_parsing_comma, _parsing_point) * 100); // Amount To Cents
          }

          if (String.Compare(_loy_transaction.ProductType, SPACE_REDEEM_VOUCHER_REDEEMABLE, true) == 0)
          {
            RedeemableAmountCents = _amount_cents;

            return true;
          }

          if (String.Compare(_loy_transaction.ProductType, SPACE_REDEEM_VOUCHER_NON_REDEEMABLE, true) == 0)
          {
            NonRedeemableAmountCents = _amount_cents;
            return true;
          }

          if (_loy_transaction.ProductType.Contains(_restricted_to_group_key))
          {
            NonRedeemableAmountCents = _amount_cents;
            RestrictedToGroupId = -1;

            try
            {
              String _aux;
              _aux = _loy_transaction.ProductType;
              _aux = _aux.Substring(_aux.IndexOf(_restricted_to_group_key));
              _aux = _aux.Substring(_restricted_to_group_key.Length);

              if (!Int32.TryParse(_aux, out RestrictedToGroupId))
              {
                RestrictedToGroupId = -1;
              }
            }
            catch
            {

            }

            if (RestrictedToGroupId >= 1 && RestrictedToGroupId <= 9)
            {
              return true;
            }
          }
          throw new Exception("Unexpected ProductType: " + _loy_transaction.ProductType);
        } // foreach
      }
      catch (Exception _ex)
      {
        throw _ex;
      }

      return false;
    }// RedeemSpaceVoucher


    //------------------------------------------------------------------------------
    // PURPOSE: Send new WIGOS anonymous accounts to SPACE
    //
    //  PARAMS:
    //      - INPUT:
    //           - AccountId 
    //           - ExternalTrackdata 
    //           - SiteId
    //
    //      - OUTPUT:
    //           - SpaceAccountId   
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static Boolean SendAnonymousAccounts(Int64 AccountId,
                                                String ExternalTrackdata,
                                                Int32 SiteId,
                                                out Int64? SpaceAccountId,
                                                out Int32 AfterReturnSleepTime)
    {

      CDR_spcAnon_spcUser_1_Input _input;
      CDR_spcAnon_spcUser_1_Output _output;
      CDR_spcLOY_spcInsert_spcMember_spcAnon_spcWS _client;

      CDREntradaAnonimoIOData _inputData;
      EntradaData[] _request;
      Resultado[] _response;

      _request =  new EntradaData[1];
      _response = new Resultado[1];
      _inputData = new CDREntradaAnonimoIOData();

      SpaceAccountId = null;
      AfterReturnSleepTime = 2000;

      try
      {
        if (String.IsNullOrEmpty(ExternalTrackdata) || SiteId <= 0)
        {
          return false;
        }

        _input = new CDR_spcAnon_spcUser_1_Input();
        _output = new CDR_spcAnon_spcUser_1_Output();

        _client = new CDR_spcLOY_spcInsert_spcMember_spcAnon_spcWS();
        _client.Url = GeneralParam.GetString("ExternalLoyaltyProgram.Mode01", "WsAnonymAccounts.Url");
        _client.Timeout = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsAnonymAccounts.Timeout", 30000);

        _request[0] = new EntradaData();
        _request[0].CuentaWIN = AccountId.ToString();
        _request[0].NumeroTarjeta = ExternalTrackdata.ToString();
        _request[0].CodigoSala = SiteId.ToString();

        _inputData.recordcount = "1";
        _inputData.lastpage = true;
        _inputData.lastpageSpecified = true;
        _inputData.Entrada = _request;

        _input.CDREntradaAnonimoIO = _inputData;

        _output = _client.CDR_spcAnon_spcUser_1 (_input);
        _response = _output.CDRRespuestaInsertUpdateIO;

        if (_response[0].CodError != "0")
        {
          ELPLog.Error("Response error from WS-SPACE AnonymousAccounts: " + _response[0].MensajeError + " -- Accountid: " + _request[0].CuentaWIN + " Trackdata: " + _request[0].NumeroTarjeta + " SiteId: " + _request[0].CodigoSala);

          //return false;
        }
        Int64 _space_account_id;
        if (!Int64.TryParse(_response[0].Id, out _space_account_id))
        {
          SpaceAccountId = null;
        }
        else
        {
          SpaceAccountId = _space_account_id;
        }

        if (SpaceAccountId == null)
        {
          ELPLog.Error(String.Format("WS_SPACE return bad SpaceAccountId for AccountId: {0}. " + " Trackdata: {1}. SiteId: {2}. SpaceAccountId=[{3}]", AccountId, _request[0].NumeroTarjeta, _request[0].CodigoSala, _response[0].Id));

          return false;
        }

      }
      catch (Exception _ex)
      {
        AfterReturnSleepTime = 300000;
        ELPLog.Error("SendAnonymousAccounts (WS) failed.  Accountid: " + _request[0].CuentaWIN + " Trackdata: " + _request[0].NumeroTarjeta + " SiteId: " + _request[0].CodigoSala);
        ELPLog.Exception(_ex);

        return false;
      }

      return true;
    }// SendAnonymousAccounts

    /// <summary>
    /// Extensi�n method to Serialize input for each type of request for space
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Value"></param>
    /// <param name="Tag"></param>
    /// <param name="Namespace"></param>
    /// <returns></returns>
    private static string Serialize<T>(this T Value, String Tag, String Namespace)
    {
      if (Value == null)
      {
        return string.Empty;
      }

      XmlWriterSettings _xml_settings;
      XmlSerializerNamespaces _xml_namespace;
      XmlSerializer _xml_serializer;
      StringWriter _string_Writer;

      _xml_settings = new XmlWriterSettings();
      _string_Writer = new StringWriter();
      _xml_namespace = new XmlSerializerNamespaces();
      _xml_serializer = new XmlSerializer(typeof(T), new XmlRootAttribute { ElementName = typeof(T).Name, Namespace = Namespace });

      try
      {
        _xml_settings.Indent = true;
        _xml_settings.OmitXmlDeclaration = true;
        _xml_namespace.Add(Tag, Namespace);

        using (XmlWriter writer = XmlWriter.Create(_string_Writer, _xml_settings))
        {
          _xml_serializer.Serialize(writer, Value, _xml_namespace);
          return _string_Writer.ToString();
        }
      }
      catch (Exception ex)
      {
        throw new Exception("An error occurred", ex);
      }
    } // Serialize<T>

    /// <summary>
    /// Extensi�n method to Deserialize output for each type of request for space
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Value"></param>
    /// <param name="Response"></param>
    /// <param name="Tag"></param>
    /// <returns></returns>
    private static T Deserialize<T>(this T Value, String Response, String Tag)
    {
      if (Value == null || string.IsNullOrEmpty(Response))
      {
        return default(T);
      }

      XmlSerializer _xml_serializer;
      String _namespace_replace;
      String _type_name;

      _type_name = typeof(T).Name;
      _xml_serializer = new XmlSerializer(typeof(T), new XmlRootAttribute(_type_name));

      try
      {
        // We have to filter Response because namespace is making errors on deserialize object.
        _namespace_replace = string.Format("{0}:{1}", Tag, _type_name);
        Response = Response.Replace(_namespace_replace, _type_name);

        using (StringReader textReader = new StringReader(Response))
        {
          using (XmlReader reader = XmlReader.Create(textReader))
          {
            reader.ReadToDescendant(typeof(T).Name);
            return (T)_xml_serializer.Deserialize(reader.ReadSubtree());
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception("An error occurred", ex);
      }
    } // Deserialize<T>

     #endregion

  } // static class

} // namespace

