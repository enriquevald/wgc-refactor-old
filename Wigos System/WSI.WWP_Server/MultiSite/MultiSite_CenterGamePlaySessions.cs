//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_CenterGamePlaySessions.cs
// 
//   DESCRIPTION: Processes of game play sessions
// 
//        AUTHOR: Alberto Marcos Fernandez
// 
// CREATION DATE: 10-MAY-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-MAY-2013 AMF    First release.
// 04-JUN-2013 AMF    Passed to stored procedure
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;
using System.Data.Common;

namespace WSI.WWP_CenterService.MultiSite
{
  class MultiSite_CenterGamePlaySessions
  {
    private const Int16 REPLY_GPS_PLAY_SESSION_ID = 0;
    private const Int16 REPLY_GPS_GAME_ID = 1;

    private const Int16 REPLY_EPS_ID = 0;

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Center Game Play Sessions of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, GamesPlaySessions
    //
    //      - OUTPUT : _replyGamesPlaySessions
    //
    // RETURNS :
    //
    static public Boolean Insert_GamePlaySessions(Int32 SiteId, DataSet RequestPlaySessions, out DataSet ReplyPlaySessions)
    {
      StringBuilder _sb;
      Int32 _num_rows_insert;      
      DataRow _dr_reply;
      DataTable _replyGamesPlaySessions;
      DataTable _replyELP001PlaySessions;
      Int32 _count_game_play_sessions;
      Int32 _count_elp01_play_sessions;

      _replyGamesPlaySessions = new DataTable();
      _replyGamesPlaySessions.TableName = "GAME_PLAY_SESSIONS";
      _replyGamesPlaySessions.Columns.Add("GPS_PLAY_SESSION_ID", Type.GetType("System.Int64"));
      _replyGamesPlaySessions.Columns.Add("GPS_GAME_ID", Type.GetType("System.Int32"));

      _replyELP001PlaySessions = new DataTable();
      _replyELP001PlaySessions.TableName = "ELP01_PLAY_SESSIONS";
      _replyELP001PlaySessions.Columns.Add("EPS_ID", Type.GetType("System.Int64"));
      ReplyPlaySessions = new DataSet();

      try
      {
        _sb = new StringBuilder();
        _count_game_play_sessions = RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"].Rows.Count;
        _count_elp01_play_sessions = RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"].Rows.Count;

        if ((_count_game_play_sessions == 0) && (_count_elp01_play_sessions == 0))
        {
          ReplyPlaySessions.Tables.Add(_replyGamesPlaySessions);
          ReplyPlaySessions.Tables.Add(_replyELP001PlaySessions);

          return true;
        }

        _sb.AppendLine("EXECUTE Insert_GamePlaySessions ");
        _sb.AppendLine("  @pSiteId ");
        _sb.AppendLine(", @pPlaySessionId ");
        _sb.AppendLine(", @pGameId ");
        _sb.AppendLine(", @pAccountId ");
        _sb.AppendLine(", @pTerminalId ");
        _sb.AppendLine(", @pPlayedCount ");
        _sb.AppendLine(", @pPlayedAmount ");
        _sb.AppendLine(", @pWonCount ");
        _sb.AppendLine(", @pWonAmount ");
        _sb.AppendLine(", @pPayout ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            // GAME_PLAY_SESSIONS            
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "GPS_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GPS_GAME_ID";
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "GPS_ACCOUNT_ID";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "GPS_TERMINAL_ID";
            _cmd.Parameters.Add("@pPlayedCount", SqlDbType.Int).SourceColumn = "GPS_PLAYED_COUNT";
            _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "GPS_PLAYED_AMOUNT";
            _cmd.Parameters.Add("@pWonCount", SqlDbType.Int).SourceColumn = "GPS_WON_COUNT";
            _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "GPS_WON_AMOUNT";
            _cmd.Parameters.Add("@pPayout", SqlDbType.Money).SourceColumn = "GPS_PAYOUT";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              _sql_adapt.ContinueUpdateOnError = true;
              _num_rows_insert = _sql_adapt.Update(RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"]);

              if (_num_rows_insert != _count_game_play_sessions)
              {
                Log.Warning(String.Format("Not insert all game play sessions in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_game_play_sessions, _num_rows_insert));
              }
            }
          }

          _sb.Length = 0;
          _sb.AppendLine("EXECUTE Insert_Elp01PlaySessions "); 
          _sb.AppendLine("  @pSiteId ");
          _sb.AppendLine(", @pId ");
          _sb.AppendLine(", @pTicketNumber ");
          _sb.AppendLine(", @pSlotSerialNumber ");
          _sb.AppendLine(", @pSlotHouseNumber ");
          _sb.AppendLine(", @pVenueCode ");
          _sb.AppendLine(", @pAreaCode ");
          _sb.AppendLine(", @pBankCode ");
          _sb.AppendLine(", @pVendorCode ");
          _sb.AppendLine(", @pGameCode ");
          _sb.AppendLine(", @pStartTime ");
          _sb.AppendLine(", @pEndTime ");
          _sb.AppendLine(", @pBetAmount ");
          _sb.AppendLine(", @pPaidAmount ");
          _sb.AppendLine(", @pGamesPlayed ");
          _sb.AppendLine(", @pInitialAmount ");
          _sb.AppendLine(", @pAditionalAmount ");
          _sb.AppendLine(", @pFinalAmount ");
          _sb.AppendLine(", @pBetCombCode ");
          _sb.AppendLine(", @pKindofTicket ");
          _sb.AppendLine(", @pSequenceNumber ");
          _sb.AppendLine(", @pCuponNumber ");
          _sb.AppendLine(", @pDateUpdated ");
          _sb.AppendLine(", @pDateInserted ");
          _sb.AppendLine(", @pAccountId ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // ELP01          
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = "EPS_ID";
            _cmd.Parameters.Add("@pTicketNumber", SqlDbType.Decimal).SourceColumn = "EPS_TICKET_NUMBER";
            _cmd.Parameters.Add("@pSlotSerialNumber", SqlDbType.VarChar, 50).SourceColumn = "EPS_SLOT_SERIAL_NUMBER";
            _cmd.Parameters.Add("@pSlotHouseNumber", SqlDbType.VarChar, 40).SourceColumn = "EPS_SLOT_HOUSE_NUMBER";
            _cmd.Parameters.Add("@pVenueCode", SqlDbType.Int).SourceColumn = "eps_venue_code";
            _cmd.Parameters.Add("@pAreaCode", SqlDbType.Int).SourceColumn = "EPS_AREA_CODE";
            _cmd.Parameters.Add("@pBankCode", SqlDbType.Int).SourceColumn = "EPS_BANK_CODE";
            _cmd.Parameters.Add("@pVendorCode", SqlDbType.Int).SourceColumn = "EPS_VENDOR_CODE";
            _cmd.Parameters.Add("@pGameCode", SqlDbType.Int).SourceColumn = "EPS_GAME_CODE";
            _cmd.Parameters.Add("@pStartTime", SqlDbType.DateTime).SourceColumn = "EPS_START_TIME";
            _cmd.Parameters.Add("@pEndTime", SqlDbType.DateTime).SourceColumn = "EPS_END_TIME";
            _cmd.Parameters.Add("@pBetAmount", SqlDbType.Decimal).SourceColumn = "EPS_BET_AMOUNT";
            _cmd.Parameters.Add("@pPaidAmount", SqlDbType.Decimal).SourceColumn = "EPS_PAID_AMOUNT";
            _cmd.Parameters.Add("@pGamesPlayed", SqlDbType.Int).SourceColumn = "EPS_GAMES_PLAYED";
            _cmd.Parameters.Add("@pInitialAmount", SqlDbType.Decimal).SourceColumn = "EPS_INITIAL_AMOUNT";
            _cmd.Parameters.Add("@pAditionalAmount", SqlDbType.Decimal).SourceColumn = "EPS_ADITIONAL_AMOUNT";
            _cmd.Parameters.Add("@pFinalAmount", SqlDbType.Decimal).SourceColumn = "EPS_FINAL_AMOUNT";
            _cmd.Parameters.Add("@pBetCombCode", SqlDbType.Int).SourceColumn = "EPS_BET_COMB_CODE";
            _cmd.Parameters.Add("@pKindofTicket", SqlDbType.Int).SourceColumn = "EPS_KINDOF_TICKET";
            _cmd.Parameters.Add("@pSequenceNumber", SqlDbType.BigInt).SourceColumn = "EPS_SEQUENCE_NUMBER";
            _cmd.Parameters.Add("@pCuponNumber", SqlDbType.Decimal).SourceColumn = "EPS_CUPON_NUMBER";
            _cmd.Parameters.Add("@pDateUpdated", SqlDbType.DateTime).SourceColumn = "EPS_DATE_UPDATED";
            _cmd.Parameters.Add("@pDateInserted", SqlDbType.DateTime).SourceColumn = "EPS_DATE_INSERTED";
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "EPS_ACCOUNT_ID";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;              
              _num_rows_insert = _sql_adapt.Update(RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"]);

              if (_num_rows_insert != _count_elp01_play_sessions)
              {
                Log.Warning(String.Format("Not insert all ELP001_PLAY_SESSIONS in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_elp01_play_sessions, _num_rows_insert));
              }
            }
          }

          _db_trx.Commit();
        }

        foreach (DataRow _sessions_dr in RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"].Rows)
        {
          if (_sessions_dr.RowState == DataRowState.Unchanged)
          {
            _dr_reply = _replyGamesPlaySessions.NewRow();

            _dr_reply[REPLY_GPS_PLAY_SESSION_ID] = _sessions_dr["GPS_PLAY_SESSION_ID"];
            _dr_reply[REPLY_GPS_GAME_ID] = _sessions_dr["GPS_GAME_ID"];

            _replyGamesPlaySessions.Rows.Add(_dr_reply);
          }
        }


        foreach (DataRow _sessions_dr in RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"].Rows)
        {
          if (_sessions_dr.RowState == DataRowState.Unchanged)
          {
            _dr_reply = _replyELP001PlaySessions.NewRow();

            _dr_reply[REPLY_EPS_ID] = _sessions_dr["EPS_ID"];

            _replyELP001PlaySessions.Rows.Add(_dr_reply);
          }
        }

        ReplyPlaySessions.Tables.Add(_replyGamesPlaySessions);
        ReplyPlaySessions.Tables.Add(_replyELP001PlaySessions);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Insert_GamePlaySessions

  } // MultiSite_CenterGamePlaySessions 
}
