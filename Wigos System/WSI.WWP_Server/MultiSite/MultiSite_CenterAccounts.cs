//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_CenterAccounts.cs
//
//   DESCRIPTION : MultiSite_CenterAccounts class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAR-2013 AJQ    First release.
// 09-OCT-2015 FAV    Warnings for the Log file to find errors with data track equal to NULL
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using WSI.WWP;

namespace WSI.WWP_CenterService.MultiSite
{
  public class MultiSite_CenterAccounts
  {

    #region Enum
    private enum READ_ACCOUNT_STATUS
    {
      OK = 0,                       // OK
      DOCUMENT_DUPLICATE = 1,       // MultiSite: La cuenta (@p0) reporta un Documento '@p1' existente en otra cuenta
      CHANGE_TRACKDATA_ACCOUNT = 2, // MultiSite: La tarjeta '@p0' de la cuenta (@p1) se ha asignado a la cuenta (@p2)
      ACCOUNT_DATA_REPLACE = 4,     // MultiSite: La sala @p0 sobreescribe los datos de la cuenta (@p1) 
      WRONG_ACCOUNT_TYPE = 8        // MultiSite: La sala @p0 sube la cuenta anonima @p1 y en el centro es personal 
    }
    #endregion // Enum

    //------------------------------------------------------------------------------
    // PURPOSE: Select account data from multisite - center.
    //
    //  PARAMS:
    //      - INPUT: AccountId
    //               SiteId
    //               SeqId
    //          
    //      - OUTPUT: Accounts
    //
    // RETURNS:
    //      - True select.count > 0
    //      - False otherwise.
    //
    public static Boolean Read_Accounts(Int32 CallingSiteId, Boolean OnlyCallingSite, Int64 SequenceValue, Int64 AccountId, out DataTable Accounts)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      Accounts = new DataTable("ACCOUNTS");
      // DDM 14-08-2013 Fixed Bug WIG-117:  Set datetime to UTC. Sites with timezone differents can change the birth date
      Accounts.Columns.Add("AC_HOLDER_BIRTH_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;
      Accounts.Columns.Add("AC_BENEFICIARY_BIRTH_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;
      Accounts.Columns.Add("AC_HOLDER_WEDDING_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   TOP (@pMAXRows)");
          _sb.AppendLine("         AC_ACCOUNT_ID");
          _sb.AppendLine("       , AC_TRACK_DATA");
          _sb.AppendLine("       , AC_HOLDER_NAME");
          _sb.AppendLine("       , AC_HOLDER_ID");
          _sb.AppendLine("       , AC_HOLDER_ID_TYPE");
          _sb.AppendLine("       , AC_HOLDER_ADDRESS_01");
          _sb.AppendLine("       , AC_HOLDER_ADDRESS_02");
          _sb.AppendLine("       , AC_HOLDER_ADDRESS_03");
          _sb.AppendLine("       , AC_HOLDER_CITY");
          _sb.AppendLine("       , AC_HOLDER_ZIP");
          _sb.AppendLine("       , AC_HOLDER_EMAIL_01");
          _sb.AppendLine("       , AC_HOLDER_EMAIL_02");
          _sb.AppendLine("       , AC_HOLDER_TWITTER_ACCOUNT");
          _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_01");
          _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_02");
          _sb.AppendLine("       , AC_HOLDER_COMMENTS");
          _sb.AppendLine("       , AC_HOLDER_ID1");
          _sb.AppendLine("       , AC_HOLDER_ID2");
          _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID1");
          _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID2");
          _sb.AppendLine("       , AC_HOLDER_NAME1");
          _sb.AppendLine("       , AC_HOLDER_NAME2");
          _sb.AppendLine("       , AC_HOLDER_NAME3");
          _sb.AppendLine("       , AC_HOLDER_GENDER");
          _sb.AppendLine("       , AC_HOLDER_MARITAL_STATUS");
          _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE");
          _sb.AppendLine("       , AC_HOLDER_WEDDING_DATE");
          _sb.AppendLine("       , AC_HOLDER_LEVEL");
          _sb.AppendLine("       , AC_HOLDER_LEVEL_EXPIRATION");
          _sb.AppendLine("       , AC_HOLDER_LEVEL_ENTERED");
          _sb.AppendLine("       , 0 AS AC_HOLDER_LEVEL_NOTIFY"); // Never Notify!
          _sb.AppendLine("       , AC_PIN");
          _sb.AppendLine("       , AC_PIN_FAILURES");
          _sb.AppendLine("       , AC_PIN_LAST_MODIFIED");
          _sb.AppendLine("       , AC_BLOCKED");
          _sb.AppendLine("       , AC_ACTIVATED");
          _sb.AppendLine("       , AC_BLOCK_REASON");
          _sb.AppendLine("       , AC_HOLDER_IS_VIP");
          _sb.AppendLine("       , AC_HOLDER_TITLE");
          _sb.AppendLine("       , AC_HOLDER_NAME4");
          _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_01");
          _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_02");
          _sb.AppendLine("       , AC_HOLDER_STATE");
          _sb.AppendLine("       , AC_HOLDER_COUNTRY");
          _sb.AppendLine("       , AC_MS_HAS_LOCAL_CHANGES");
          _sb.AppendLine("       , AC_MS_CHANGE_GUID");
          _sb.AppendLine("       , AC_MS_CREATED_ON_SITE_ID");
          _sb.AppendLine("       , AC_MS_MODIFIED_ON_SITE_ID");
          _sb.AppendLine("       , AC_MS_LAST_SITE_ID");
          _sb.AppendLine("       , AC_MS_POINTS_SEQ_ID");
          _sb.AppendLine("       , AC_MS_POINTS_SYNCHRONIZED");
          _sb.AppendLine("       , AC_MS_PERSONAL_INFO_SEQ_ID");
          _sb.AppendLine("       , AC_POINTS_STATUS");
          _sb.AppendLine("       , AC_USER_TYPE");
          _sb.AppendLine("       , AC_DEPOSIT");
          _sb.AppendLine("       , AC_CARD_PAID");
          _sb.AppendLine("       , AC_BLOCK_DESCRIPTION");
          _sb.AppendLine("       , AC_MS_HASH");
          _sb.AppendLine("       , AC_CREATED");
          _sb.AppendLine("       , AC_EXTERNAL_REFERENCE");
          // new fields
          _sb.AppendLine("       , AC_HOLDER_OCCUPATION ");
          _sb.AppendLine("       , AC_HOLDER_EXT_NUM ");
          _sb.AppendLine("       , AC_HOLDER_NATIONALITY ");
          _sb.AppendLine("       , AC_HOLDER_BIRTH_COUNTRY ");
          _sb.AppendLine("       , AC_HOLDER_FED_ENTITY ");
          _sb.AppendLine("       , AC_HOLDER_ID1_TYPE ");
          _sb.AppendLine("       , AC_HOLDER_ID2_TYPE ");
          _sb.AppendLine("       , AC_HOLDER_ID3_TYPE ");
          _sb.AppendLine("       , AC_HOLDER_ID3 ");
          _sb.AppendLine("       , AC_HOLDER_HAS_BENEFICIARY ");
          _sb.AppendLine("       , AC_BENEFICIARY_NAME ");
          _sb.AppendLine("       , AC_BENEFICIARY_NAME1 ");
          _sb.AppendLine("       , AC_BENEFICIARY_NAME2 ");
          _sb.AppendLine("       , AC_BENEFICIARY_NAME3 ");
          _sb.AppendLine("       , AC_BENEFICIARY_BIRTH_DATE ");
          _sb.AppendLine("       , AC_BENEFICIARY_GENDER ");
          _sb.AppendLine("       , AC_BENEFICIARY_OCCUPATION ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID1_TYPE ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID1 ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID2_TYPE ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID2 ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID3_TYPE ");
          _sb.AppendLine("       , AC_BENEFICIARY_ID3");
          _sb.AppendLine("       , AC_HOLDER_OCCUPATION_ID");
          _sb.AppendLine("       , AC_BENEFICIARY_OCCUPATION_ID");
          _sb.AppendLine("       , AC_SHOW_COMMENTS_ON_CASHIER");
          _sb.AppendLine("       , AC_HOLDER_ADDRESS_COUNTRY");
          _sb.AppendLine("       , AC_HOLDER_ADDRESS_VALIDATION");

          if (AccountId > 0)
          {
            // An explicit Account has been requested
            _sb.AppendLine("  FROM   ACCOUNTS WITH (INDEX(PK_accounts))");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId");
          }
          else
          {
            if (SequenceValue > 0)
            {
              _sb.AppendLine("  FROM   ACCOUNTS WITH (INDEX(IX_ms_personal_info_seq_id)) ");
              _sb.AppendLine(" WHERE   AC_MS_PERSONAL_INFO_SEQ_ID > @pSeqId");
              if (OnlyCallingSite)
              {
                _sb.AppendLine(" AND   AC_MS_LAST_SITE_ID = @pSiteId");
              }
            }
            else
            {
              _sb.AppendLine("   FROM   ACCOUNTS ");
              if (OnlyCallingSite)
              {
                _sb.AppendLine("WHERE   AC_MS_LAST_SITE_ID = @pSiteId");
              }
            }
          }
          _sb.AppendLine(" ORDER   BY AC_MS_PERSONAL_INFO_SEQ_ID ASC, AC_ACCOUNT_ID");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;

            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = CallingSiteId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, Accounts);

            }

            if (!OnlyCallingSite)
            {
              WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All, SequenceValue, _db_trx.SqlTransaction);
            }
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Get_Accounts

    public static Boolean Update_PersonalInfo(Int32 SiteId, DataTable RequestPersonalInfo)
    {
      StringBuilder _sb;
      SqlParameter _p;
      Int32 _return_code;

      if (RequestPersonalInfo.Rows.Count == 0)
      {
        return true;
      }

      // Add output parameters
      RequestPersonalInfo.Columns.Add("R_AC_STATUS", Type.GetType("System.Int32"));
      RequestPersonalInfo.Columns.Add("R_AC_OTHER_ACCOUNT_ID", Type.GetType("System.Int64"));

      //_is_new_version = RequestPersonalInfo.Columns.Contains("AC_HOLDER_ID1_TYPE");
      _sb = new StringBuilder();

      if (RequestPersonalInfo.Columns.Count == 89)
      {
        _sb.AppendLine("EXECUTE Update_PersonalInfo ");
      }
      else
      {
        Log.Warning("It has detected a call to an old prodedure (Update_PersonalInfoForOldVersion)");
        _sb.AppendLine("EXECUTE Update_PersonalInfoForOldVersion ");
      }
      _sb.AppendLine("  @pCallingSiteId");	// int
      _sb.AppendLine(", @pAccountId");	//  bigint
      _sb.AppendLine(", @pTrackData "); //  nvarchar(50)
      _sb.AppendLine(", @pAccountCreated"); //  Datetime
      _sb.AppendLine(", @pHolderName "); //  nvarchar(200)
      _sb.AppendLine(", @pHolderId "); //  nvarchar(20)
      _sb.AppendLine(", @pHolderIdType "); //  int
      _sb.AppendLine(", @pHolderAddress01 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderAddress02 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderAddress03 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderCity "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderZip "); //   nvarchar(10) 
      _sb.AppendLine(", @pHolderEmail01 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderEmail02 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderTwitter "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderPhoneNumber01 "); //  nvarchar(20)
      _sb.AppendLine(", @pHolderPhoneNumber02 "); //  nvarchar(20)
      _sb.AppendLine(", @pHolderComments "); //  nvarchar(100)
      _sb.AppendLine(", @pHolderId1 "); //  nvarchar(20)
      _sb.AppendLine(", @pHolderId2 "); //  nvarchar(20)
      _sb.AppendLine(", @pHolderDocumentId1 "); //  bigint
      _sb.AppendLine(", @pHolderDocumentId2 "); //  bigint
      _sb.AppendLine(", @pHolderName1 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderName2 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderName3 "); //  nvarchar(50)
      _sb.AppendLine(", @pHolderGender "); //   int
      _sb.AppendLine(", @pHolderMaritalStatus "); //  int
      _sb.AppendLine(", @pHolderBirthDate "); //  datetime
      _sb.AppendLine(", @pHolderWeddingDate "); //  datetime
      _sb.AppendLine(", @pHolderLevel "); //  int
      _sb.AppendLine(", @pHolderLevelNotify "); //  int
      _sb.AppendLine(", @pHolderLevelEntered "); //  datetime
      _sb.AppendLine(", @pHolderLevelExpiration "); //  datetime
      _sb.AppendLine(", @pPin "); //  nvarchar(12)
      _sb.AppendLine(", @pPinFailures "); //  int
      _sb.AppendLine(", @pPinLastModified "); //  datetime
      _sb.AppendLine(", @pBlocked "); //  bit
      _sb.AppendLine(", @pActivated "); //  bit
      _sb.AppendLine(", @pBlockReason "); //  int
      _sb.AppendLine(", @pHolderIsVip "); //  int
      _sb.AppendLine(", @pHolderTitle "); //  nvarchar(15)                       
      _sb.AppendLine(", @pHolderName4 "); //  nvarchar(50)                       
      _sb.AppendLine(", @pHolderPhoneType01 "); //   int
      _sb.AppendLine(", @pHolderPhoneType02 "); //   int
      _sb.AppendLine(", @pHolderState "); //     nvarchar                    
      _sb.AppendLine(", @pHolderCountry "); //  nvarchar      
      _sb.AppendLine(", @pUserType "); //  nvarchar  
      _sb.AppendLine(", @pPersonalInfoSeqId "); //  bigint  
      _sb.AppendLine(", @pPointsStatus "); //  INT  
      _sb.AppendLine(", @pDeposit ");
      _sb.AppendLine(", @pCardPay ");
      _sb.AppendLine(", @pBlockDescription");
      _sb.AppendLine(", @pMSCreatedOnSiteId");
      _sb.AppendLine(", @pExternalReference");
      _sb.AppendLine(", @pHolderOccupation");
      _sb.AppendLine(", @pHolderExtNum");
      _sb.AppendLine(", @pHolderNationality ");
      _sb.AppendLine(", @pHolderBirthCountry ");
      _sb.AppendLine(", @pHolderFedEntity");
      _sb.AppendLine(", @pHolderId1Type ");
      _sb.AppendLine(", @pHolderId2Type ");
      _sb.AppendLine(", @pHolderId3Type ");
      _sb.AppendLine(", @pHolderId3");
      _sb.AppendLine(", @pHolderHasBeneficiary ");
      _sb.AppendLine(", @pBeneficiaryName ");
      _sb.AppendLine(", @pBeneficiaryName1");
      _sb.AppendLine(", @pBeneficiaryName2");
      _sb.AppendLine(", @pBeneficiaryName3");
      _sb.AppendLine(", @pBeneficiaryBirthDate ");
      _sb.AppendLine(", @pBeneficiaryGender ");
      _sb.AppendLine(", @pBeneficiaryOccupation ");
      _sb.AppendLine(", @pBeneficiaryId1Type ");
      _sb.AppendLine(", @pBeneficiaryId1 ");
      _sb.AppendLine(", @pBeneficiaryId2Type ");
      _sb.AppendLine(", @pBeneficiaryId2 ");
      _sb.AppendLine(", @pBeneficiaryId3Type ");
      _sb.AppendLine(", @pBeneficiaryId3 ");
      _sb.AppendLine(", @pHolderOccupationId ");
      _sb.AppendLine(", @pBeneficiaryOccupationId ");

      if (RequestPersonalInfo.Columns.Count >= 89)
      {
        _sb.AppendLine(", @pShowCommentsOnCashier ");
        _sb.AppendLine(", @pHolderAddressCountry ");
        _sb.AppendLine(", @pHolderAddressValidation ");
      }
      _sb.AppendLine(", @pOutputStatus          OUTPUT "); //  int
      _sb.AppendLine(", @pOutputAccountOtherId  OUTPUT "); //  bigint

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          SqlTransaction _trx = _db_trx.SqlTransaction;
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
          {
            _cmd.Parameters.Add("@pCallingSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AC_ACCOUNT_ID";
            _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50).SourceColumn = "AC_TRACK_DATA";
            _cmd.Parameters.Add("@pAccountCreated", SqlDbType.DateTime).SourceColumn = "AC_CREATED";
            _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 200).SourceColumn = "AC_HOLDER_NAME";
            _cmd.Parameters.Add("@pHolderId", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID";
            _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID_TYPE";
            _cmd.Parameters.Add("@pHolderAddress01", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_ADDRESS_01";
            _cmd.Parameters.Add("@pHolderAddress02", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_ADDRESS_02";
            _cmd.Parameters.Add("@pHolderAddress03", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_ADDRESS_03";
            _cmd.Parameters.Add("@pHolderCity", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_CITY";
            _cmd.Parameters.Add("@pHolderZip", SqlDbType.NVarChar, 10).SourceColumn = "AC_HOLDER_ZIP";
            _cmd.Parameters.Add("@pHolderEmail01", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_EMAIL_01";
            _cmd.Parameters.Add("@pHolderEmail02", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_EMAIL_02";
            _cmd.Parameters.Add("@pHolderTwitter", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_TWITTER_ACCOUNT";
            _cmd.Parameters.Add("@pHolderPhoneNumber01", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_PHONE_NUMBER_01";
            _cmd.Parameters.Add("@pHolderPhoneNumber02", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_PHONE_NUMBER_02";
            _cmd.Parameters.Add("@pHolderComments", SqlDbType.NVarChar, 100).SourceColumn = "AC_HOLDER_COMMENTS";
            _cmd.Parameters.Add("@pHolderId1", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID1";
            _cmd.Parameters.Add("@pHolderId2", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID2";
            _cmd.Parameters.Add("@pHolderDocumentId1", SqlDbType.BigInt).SourceColumn = "AC_HOLDER_DOCUMENT_ID1";
            _cmd.Parameters.Add("@pHolderDocumentId2", SqlDbType.BigInt).SourceColumn = "AC_HOLDER_DOCUMENT_ID2";
            _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_NAME1";
            _cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_NAME2";
            _cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_NAME3";
            _cmd.Parameters.Add("@pHolderGender", SqlDbType.Int).SourceColumn = "AC_HOLDER_GENDER";
            _cmd.Parameters.Add("@pHolderMaritalStatus", SqlDbType.Int).SourceColumn = "AC_HOLDER_MARITAL_STATUS";
            _cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_BIRTH_DATE";
            _cmd.Parameters.Add("@pHolderWeddingDate", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_WEDDING_DATE";
            _cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "AC_HOLDER_LEVEL";
            _cmd.Parameters.Add("@pHolderLevelNotify", SqlDbType.Int).SourceColumn = "AC_HOLDER_LEVEL_NOTIFY";
            _cmd.Parameters.Add("@pHolderLevelEntered", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_LEVEL_ENTERED";
            _cmd.Parameters.Add("@pHolderLevelExpiration", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_LEVEL_EXPIRATION";
            _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar, 12).SourceColumn = "AC_PIN";
            _cmd.Parameters.Add("@pPinFailures", SqlDbType.Int).SourceColumn = "AC_PIN_FAILURES";
            _cmd.Parameters.Add("@pPinLastModified", SqlDbType.DateTime).SourceColumn = "AC_PIN_LAST_MODIFIED";
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).SourceColumn = "AC_BLOCKED";
            _cmd.Parameters.Add("@pActivated", SqlDbType.Bit).SourceColumn = "AC_ACTIVATED";
            _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).SourceColumn = "AC_BLOCK_REASON";
            _cmd.Parameters.Add("@pHolderIsVip", SqlDbType.Int).SourceColumn = "AC_HOLDER_IS_VIP";
            _cmd.Parameters.Add("@pHolderTitle", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_TITLE";
            _cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_NAME4";
            _cmd.Parameters.Add("@pHolderPhoneType01", SqlDbType.Int).SourceColumn = "AC_HOLDER_PHONE_TYPE_01";
            _cmd.Parameters.Add("@pHolderPhoneType02", SqlDbType.Int).SourceColumn = "AC_HOLDER_PHONE_TYPE_02";
            _cmd.Parameters.Add("@pHolderState", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_STATE";
            _cmd.Parameters.Add("@pHolderCountry", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_COUNTRY";
            _cmd.Parameters.Add("@pUserType", SqlDbType.Int).SourceColumn = "AC_USER_TYPE";
            _cmd.Parameters.Add("@pPersonalInfoSeqId", SqlDbType.BigInt).SourceColumn = "AC_MS_PERSONAL_INFO_SEQ_ID";
            _cmd.Parameters.Add("@pPointsStatus", SqlDbType.Int).SourceColumn = "AC_POINTS_STATUS";
            _cmd.Parameters.Add("@pDeposit", SqlDbType.Money).SourceColumn = "AC_DEPOSIT";
            _cmd.Parameters.Add("@pCardPay", SqlDbType.Bit).SourceColumn = "AC_CARD_PAID";
            _cmd.Parameters.Add("@pBlockDescription", SqlDbType.NVarChar, 256).SourceColumn = "AC_BLOCK_DESCRIPTION";
            _cmd.Parameters.Add("@pMSCreatedOnSiteId", SqlDbType.Int).SourceColumn = "AC_MS_CREATED_ON_SITE_ID";
            _cmd.Parameters.Add("@pExternalReference", SqlDbType.NVarChar).SourceColumn = "AC_EXTERNAL_REFERENCE";
            _cmd.Parameters.Add("@pHolderOccupation", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_OCCUPATION";
            _cmd.Parameters.Add("@pHolderExtNum", SqlDbType.NVarChar, 10).SourceColumn = "AC_HOLDER_EXT_NUM";
            _cmd.Parameters.Add("@pHolderNationality", SqlDbType.Int).SourceColumn = "AC_HOLDER_NATIONALITY";
            _cmd.Parameters.Add("@pHolderBirthCountry", SqlDbType.Int).SourceColumn = "AC_HOLDER_BIRTH_COUNTRY";
            _cmd.Parameters.Add("@pHolderFedEntity", SqlDbType.Int).SourceColumn = "AC_HOLDER_FED_ENTITY";
            _cmd.Parameters.Add("@pHolderId1Type", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID1_TYPE";
            _cmd.Parameters.Add("@pHolderId2Type", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID2_TYPE";
            _cmd.Parameters.Add("@pHolderId3Type", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_ID3_TYPE";
            _cmd.Parameters.Add("@pHolderId3", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID3";
            _cmd.Parameters.Add("@pHolderHasBeneficiary", SqlDbType.Bit).SourceColumn = "AC_HOLDER_HAS_BENEFICIARY";
            _cmd.Parameters.Add("@pBeneficiaryName", SqlDbType.NVarChar, 200).SourceColumn = "AC_BENEFICIARY_NAME";
            _cmd.Parameters.Add("@pBeneficiaryName1", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME1";
            _cmd.Parameters.Add("@pBeneficiaryName2", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME2";
            _cmd.Parameters.Add("@pBeneficiaryName3", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME3";
            _cmd.Parameters.Add("@pBeneficiaryBirthDate", SqlDbType.DateTime).SourceColumn = "AC_BENEFICIARY_BIRTH_DATE";
            _cmd.Parameters.Add("@pBeneficiaryGender", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_GENDER";
            _cmd.Parameters.Add("@pBeneficiaryOccupation", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_OCCUPATION";
            _cmd.Parameters.Add("@pBeneficiaryId1Type", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_ID1_TYPE";
            _cmd.Parameters.Add("@pBeneficiaryId1", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID1";
            _cmd.Parameters.Add("@pBeneficiaryId2Type", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_ID2_TYPE";
            _cmd.Parameters.Add("@pBeneficiaryId2", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID2";
            _cmd.Parameters.Add("@pBeneficiaryId3Type", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_ID3_TYPE";
            _cmd.Parameters.Add("@pBeneficiaryId3", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID3";
            _cmd.Parameters.Add("@pHolderOccupationId", SqlDbType.Int).SourceColumn = "AC_HOLDER_OCCUPATION_ID";
            _cmd.Parameters.Add("@pBeneficiaryOccupationId", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_OCCUPATION_ID";

            if (RequestPersonalInfo.Columns.Count >= 89)
            {
              _cmd.Parameters.Add("@pShowCommentsOnCashier", SqlDbType.Bit).SourceColumn = "AC_SHOW_COMMENTS_ON_CASHIER";
              _cmd.Parameters.Add("@pHolderAddressCountry", SqlDbType.Int).SourceColumn = "AC_HOLDER_ADDRESS_COUNTRY";
              _cmd.Parameters.Add("@pHolderAddressValidation", SqlDbType.Int).SourceColumn = "AC_HOLDER_ADDRESS_VALIDATION";

            }

            _p = _cmd.Parameters.Add("@pOutputStatus", SqlDbType.Int, 4, "R_AC_STATUS");
            _p.Direction = ParameterDirection.Output;
            _p = _cmd.Parameters.Add("@pOutputAccountOtherId", SqlDbType.BigInt, 8, "R_AC_OTHER_ACCOUNT_ID");
            _p.Direction = ParameterDirection.Output;

            CheckTrackDataWithNullValue(RequestPersonalInfo);

            using (SqlDataAdapter _adapter = new SqlDataAdapter())
            {
              _adapter.InsertCommand = _cmd;
              _adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _adapter.UpdateBatchSize = 100;

              _adapter.Update(RequestPersonalInfo);
            }

            foreach (DataRow _request_dr in RequestPersonalInfo.Rows)
            {
              String _source_name;
              AlarmCode _alarm_code;
              String _alarm_description;

              _alarm_code = AlarmCode.Unknown;
              _alarm_description = "";

              if (_request_dr["R_AC_STATUS"] == DBNull.Value)
              {
                Log.Warning(" Update_PersonalInfo. Account not updated. AccountId:" + _request_dr["AC_ACCOUNT_ID"].ToString());

                continue;
              }

              _return_code = (Int32)_request_dr["R_AC_STATUS"];

              // OK
              if (_return_code == 0)
              {
                continue;
              }

              _source_name = Environment.MachineName + @"\\" + MultiSiteCenterCommon.SERVICE_NAME;

              // MultiSite: La cuenta (@p0) reporta un Documento '@p1' existente en otra cuenta
              if (((READ_ACCOUNT_STATUS)_return_code & READ_ACCOUNT_STATUS.DOCUMENT_DUPLICATE) > 0)
              {
                _alarm_code = AlarmCode.MultiSite_AccountWithNonUniqueHolderId;
                _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), ((Int64)_request_dr["AC_ACCOUNT_ID"]).ToString(), _request_dr["AC_HOLDER_ID"].ToString());
                Alarm.Register(AlarmSourceCode.MultiSite, 0, _source_name, (UInt32)_alarm_code, _alarm_description, AlarmSeverity.Warning, WGDB.Now, _trx);
              }

              // MultiSite: La tarjeta '@p0' de la cuenta (@p1) se ha asignado a la cuenta (@p2)
              if (((READ_ACCOUNT_STATUS)_return_code & READ_ACCOUNT_STATUS.CHANGE_TRACKDATA_ACCOUNT) > 0)
              {
                String _track_data;

                _alarm_code = AlarmCode.MultiSite_AccountWithExistsTrackData;
                _track_data = "";

                if (!WSI.Common.CardNumber.TrackDataToExternal(out _track_data, (String)_request_dr["AC_TRACK_DATA"], (int)CardTrackData.CardsType.PLAYER))
                {
                  _track_data = "* * * * * * * *";
                }
                _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), _track_data, ((Int64)_request_dr["R_AC_OTHER_ACCOUNT_ID"]).ToString(), ((Int64)_request_dr["AC_ACCOUNT_ID"]).ToString());
                Alarm.Register(AlarmSourceCode.MultiSite, 0, _source_name, (UInt32)_alarm_code, _alarm_description, AlarmSeverity.Warning, WGDB.Now, _trx);
              }

              // MultiSite: La sala @p0 sobreescribe los datos de la cuenta (@p1)
              if (((READ_ACCOUNT_STATUS)_return_code & READ_ACCOUNT_STATUS.ACCOUNT_DATA_REPLACE) > 0)
              {
                _alarm_code = AlarmCode.MultiSite_AccountWithChangesOfOtherSite;
                _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), SiteId.ToString("000"), ((Int64)_request_dr["AC_ACCOUNT_ID"]).ToString());
                Alarm.Register(AlarmSourceCode.MultiSite, 0, _source_name, (UInt32)_alarm_code, _alarm_description, AlarmSeverity.Warning, WGDB.Now, _trx);
              }

              // MultiSite: La sala @p0 sube la cuenta anonima @p1 y en el centro es personal
              if (((READ_ACCOUNT_STATUS)_return_code & READ_ACCOUNT_STATUS.WRONG_ACCOUNT_TYPE) > 0)
              {
                _alarm_code = AlarmCode.MultiSite_AccountWithWrongAccountType;
                _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), SiteId.ToString("000"), ((Int64)_request_dr["AC_ACCOUNT_ID"]).ToString());
                Alarm.Register(AlarmSourceCode.MultiSite, 0, _source_name, (UInt32)_alarm_code, _alarm_description, AlarmSeverity.Warning, WGDB.Now, _trx);
              }
            }
          }

          _trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    public static Boolean Read_Accounts(DataTable TracksData, out DataSet AccountInfoAndPoint)
    {

      StringBuilder _sb;
      List<String> _ls_track_data;
      String _tracks_data;

      _ls_track_data = new List<String>();
      AccountInfoAndPoint = new DataSet();

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   AC_ACCOUNT_ID");
      _sb.AppendLine("       , AC_TRACK_DATA");
      _sb.AppendLine("       , AC_HOLDER_NAME");
      _sb.AppendLine("       , AC_HOLDER_ID");
      _sb.AppendLine("       , AC_HOLDER_ID_TYPE");
      _sb.AppendLine("       , AC_HOLDER_ADDRESS_01");
      _sb.AppendLine("       , AC_HOLDER_ADDRESS_02");
      _sb.AppendLine("       , AC_HOLDER_ADDRESS_03");
      _sb.AppendLine("       , AC_HOLDER_CITY");
      _sb.AppendLine("       , AC_HOLDER_ZIP");
      _sb.AppendLine("       , AC_HOLDER_EMAIL_01");
      _sb.AppendLine("       , AC_HOLDER_EMAIL_02");
      _sb.AppendLine("       , AC_HOLDER_TWITTER_ACCOUNT");
      _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_01");
      _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_02");
      _sb.AppendLine("       , AC_HOLDER_COMMENTS");
      _sb.AppendLine("       , AC_HOLDER_ID1");
      _sb.AppendLine("       , AC_HOLDER_ID2");
      _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID1");
      _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID2");
      _sb.AppendLine("       , AC_HOLDER_NAME1");
      _sb.AppendLine("       , AC_HOLDER_NAME2");
      _sb.AppendLine("       , AC_HOLDER_NAME3");
      _sb.AppendLine("       , AC_HOLDER_GENDER");
      _sb.AppendLine("       , AC_HOLDER_MARITAL_STATUS");
      _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE");
      _sb.AppendLine("       , AC_HOLDER_WEDDING_DATE");
      _sb.AppendLine("       , AC_HOLDER_LEVEL");
      _sb.AppendLine("       , AC_HOLDER_LEVEL_EXPIRATION");
      _sb.AppendLine("       , AC_HOLDER_LEVEL_ENTERED");
      _sb.AppendLine("       , 0 AS AC_HOLDER_LEVEL_NOTIFY"); // never Notify!
      _sb.AppendLine("       , AC_PIN");
      _sb.AppendLine("       , AC_PIN_FAILURES");
      _sb.AppendLine("       , AC_PIN_LAST_MODIFIED");
      _sb.AppendLine("       , AC_BLOCKED");
      _sb.AppendLine("       , AC_ACTIVATED");
      _sb.AppendLine("       , AC_BLOCK_REASON");
      _sb.AppendLine("       , AC_HOLDER_IS_VIP");
      _sb.AppendLine("       , AC_HOLDER_TITLE");
      _sb.AppendLine("       , AC_HOLDER_NAME4");
      _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_01");
      _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_02");
      _sb.AppendLine("       , AC_HOLDER_STATE");
      _sb.AppendLine("       , AC_HOLDER_COUNTRY");

      _sb.AppendLine("       , AC_MS_HAS_LOCAL_CHANGES");
      _sb.AppendLine("       , AC_MS_CHANGE_GUID");
      _sb.AppendLine("       , AC_MS_CREATED_ON_SITE_ID");
      _sb.AppendLine("       , AC_MS_MODIFIED_ON_SITE_ID");
      _sb.AppendLine("       , AC_MS_PERSONAL_INFO_SEQ_ID");
      _sb.AppendLine("       , AC_MS_LAST_SITE_ID");
      _sb.AppendLine("       , AC_USER_TYPE");
      _sb.AppendLine("       , AC_POINTS_STATUS");
      _sb.AppendLine("       , AC_DEPOSIT");
      _sb.AppendLine("       , AC_CARD_PAID");
      _sb.AppendLine("       , AC_BLOCK_DESCRIPTION");
      _sb.AppendLine("       , AC_MS_HASH");
      _sb.AppendLine("       , AC_EXTERNAL_REFERENCE");

      _sb.AppendLine("  FROM   ACCOUNTS");
      _sb.AppendLine(" WHERE   AC_TRACK_DATA IN (#TracksData);");

      //
      _sb.AppendLine("SELECT   0 AS AM_MOVEMENT_ID ");
      _sb.AppendLine("       , 0 AS AM_STATUS ");
      _sb.AppendLine("       , AC_ACCOUNT_ID AS AM_ACCOUNT_ID ");
      _sb.AppendLine("       , AC_MS_POINTS_SEQ_ID ");
      _sb.AppendLine("       , ISNULL(dbo.GetBucketValue(" + (Int32)Buckets.BucketId.RedemptionPoints + ", AC_ACCOUNT_ID), 0) AS AC_POINTS");

      _sb.AppendLine("  FROM   ACCOUNTS");
      _sb.AppendLine(" WHERE   AC_TRACK_DATA IN (#TracksData);");

      try
      {
        foreach (DataRow _row in TracksData.Rows)
        {
          _ls_track_data.Add("'" + (String)_row[0] + "'");
        }

        if (_ls_track_data.Count == 0)
        {
          return true;
        }

        _tracks_data = String.Join(",", _ls_track_data.ToArray());

        _sb = _sb.Replace("#TracksData", _tracks_data);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTracksData", SqlDbType.NVarChar).Value = _tracks_data;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, AccountInfoAndPoint);

              return true;
            }

          }
        }
      }
      catch { }

      return false;

    }

    private static void CheckTrackDataWithNullValue(DataTable DownloadedAccounts)
    {
      try
      {
        foreach (DataRow row in DownloadedAccounts.Rows)
        {
          object value = row["AC_TRACK_DATA"];
          if (value == DBNull.Value)
          {
            Log.Warning(string.Format("MultiSite_CenterAccounts.Update_PersonalInfo: It has detected an Account with TrackData = null. AccountID:{0} Type:{1} UserType:{2} CreatedOnSite:{3}",
              row["AC_ACCOUNT_ID"].ToString(),
              row["AC_TYPE"].ToString(),
              row["AC_USER_TYPE"] == DBNull.Value ? "NULL" : row["AC_USER_TYPE"].ToString(),
              row["AC_MS_CREATED_ON_SITE_ID"] == DBNull.Value ? "NULL" : row["AC_MS_CREATED_ON_SITE_ID"].ToString()));
          }
        }
      }
      catch { }
    }

  }
}

