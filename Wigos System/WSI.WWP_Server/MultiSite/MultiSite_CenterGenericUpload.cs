//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   MultiSite_CenterGenericUpload.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 24-MAY-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 24-MAY-2013  MPO    Initial version
// 20-JUN-2013  JML    Update_CenterProviders move from delete file MultiSite_CenterProviders.cs
// 04-JUL-2013  JML    Insert_GamePlaySessions move from delete file Multisite_CenterGamePlaySession.cs
// 22-JUL-2014  AMF    Tito Columns.
// 29-JUL-2014  JBC    Fixed Bug WIG-1123: Modified Update_CenterPlaySessions BEFORE 44 Columns / After 43
// 14-JAN-2015  DCS    Added Task Upload Handpays
// 17-MAR-2015  ANM    Added Task Upload Sells & buys
// 27-MAR-2015  FOS    Change Task Upload Terminals and Terminals_connected
// 01-ABR-2015  FOS    DEFECT 918. Don't update files in YaksReplication
// 05-APR-2016  RAB    PBI 10787 (Phase 1): GameTable: Historification of the movements of box (MultiSite)
// 31-MAR-2017  MS     WIGOS-411 Creditlines
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.WWP_CenterService.MultiSite
{
  class MultiSite_CenterGenericUpload
  {
    private const Int16 REPLY_AD_ACCOUNT_ID = 0;
    private const Int16 REPLY_AD_MS_SEQUENCE_ID = 1;

    private const Int16 REPLY_GPS_PLAY_SESSION_ID = 0;
    private const Int16 REPLY_GPS_GAME_ID = 1;

    private const Int16 REPLY_EPS_ID = 0;

    private const Int16 REPLY_SELLS_BUYS_ID = 0;

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Areas of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterAreas(Int32 SiteId, DataTable CenterAreas)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterAreas.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_Areas ");
        _sb.AppendLine(" @pSiteId ");
        _sb.AppendLine(",@pAreaid ");
        _sb.AppendLine(",@pName");
        _sb.AppendLine(",@pSmoking ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pAreaid", SqlDbType.Int).SourceColumn = "AR_AREA_ID";
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "AR_NAME";
            _cmd.Parameters.Add("@pSmoking", SqlDbType.Bit).SourceColumn = "AR_SMOKING";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterAreas) == CenterAreas.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterAreas

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Sales Per Hour of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterSalesPerHour(Int32 SiteId, DataTable CenterSalesPerHour)
    {
      StringBuilder _sb;

      try
      {
        if (CenterSalesPerHour.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        if (CenterSalesPerHour.Columns.Count == 17)
        {
          _sb.AppendLine("EXECUTE Update_SalesPerHour ");
        }
        else
        {
          _sb.AppendLine("EXECUTE Update_SalesPerHourForOldVersion ");
        }

        _sb.AppendLine("  @pSiteId                      ");
        _sb.AppendLine(", @pBaseHour                    ");
        _sb.AppendLine(", @pTerminalId                  ");
        _sb.AppendLine(", @pGameId                      ");
        _sb.AppendLine(", @pPlayedCount                 ");
        _sb.AppendLine(", @pPlayedAmount                ");
        _sb.AppendLine(", @pWonCount                    ");
        _sb.AppendLine(", @pWonAmount                   ");
        _sb.AppendLine(", @pTerminalName                ");
        _sb.AppendLine(", @pGameName                    ");
        _sb.AppendLine(", @pTimeStamp                   ");
        _sb.AppendLine(", @pTheoreticalWonAmount        ");
        _sb.AppendLine(", @pPayout                      ");
        _sb.AppendLine(", @pUniqueId                    ");

        if (CenterSalesPerHour.Columns.Count >= 17)
        {
          _sb.AppendLine(", @pProgressiveProvisionAmount  ");
          _sb.AppendLine(", @pJackpotAmount               ");
          _sb.AppendLine(", @pProgressiveJackpotAmount    ");
          _sb.AppendLine(", @pProgressiveJackpotAmount0   ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "SPH_BASE_HOUR";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "SPH_TERMINAL_ID";
            _cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "SPH_GAME_ID";
            _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "SPH_PLAYED_COUNT";
            _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "SPH_PLAYED_AMOUNT";
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).SourceColumn = "SPH_TERMINAL_NAME";
            _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).SourceColumn = "SPH_GAME_NAME";
            _cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "SPH_WON_COUNT";
            _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "SPH_WON_AMOUNT";
            _cmd.Parameters.Add("@pTimeStamp", SqlDbType.BigInt).SourceColumn = "SPH_TIMESTAMP";
            _cmd.Parameters.Add("@pTheoreticalWonAmount", SqlDbType.Money).SourceColumn = "SPH_THEORETICAL_WON_AMOUNT";
            _cmd.Parameters.Add("@pPayout", SqlDbType.Money).SourceColumn = "SPH_PAYOUT";
            _cmd.Parameters.Add("@pUniqueId", SqlDbType.Money).SourceColumn = "SPH_UNIQUE_ID";

            if (CenterSalesPerHour.Columns.Count >= 17)
            {
              _cmd.Parameters.Add("@pProgressiveProvisionAmount", SqlDbType.Money).SourceColumn = "SPH_PROGRESSIVE_PROVISION_AMOUNT";
              _cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).SourceColumn = "SPH_JACKPOT_AMOUNT";
              _cmd.Parameters.Add("@pProgressiveJackpotAmount", SqlDbType.Money).SourceColumn = "SPH_PROGRESSIVE_JACKPOT_AMOUNT";
              _cmd.Parameters.Add("@pProgressiveJackpotAmount0", SqlDbType.Money).SourceColumn = "SPH_PROGRESSIVE_JACKPOT_AMOUNT_0";
            }

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterSalesPerHour) == CenterSalesPerHour.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterSalesPerHour

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Play Sessions of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, CenterPlaySessions
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterPlaySessions(Int32 SiteId, DataTable CenterPlaySessions)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterPlaySessions.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        if (CenterPlaySessions.Columns.Count == 57)
        {
          _sb.AppendLine(" EXECUTE Update_PlaySessions    ");
        }
        else
        {
          _sb.AppendLine(" EXECUTE Update_PlaySessionsForOldVersion    ");
        }

        _sb.AppendLine("  @pPlaySessionId              ");
        _sb.AppendLine(" ,@pAccountId                  ");
        _sb.AppendLine(" ,@pTerminalId                 ");
        _sb.AppendLine(" ,@pType                       ");
        _sb.AppendLine(" ,@pTypedata                   ");
        _sb.AppendLine(" ,@pStatus                     ");
        _sb.AppendLine(" ,@pStarted                    ");
        _sb.AppendLine(" ,@pInitialBalance             ");
        _sb.AppendLine(" ,@pPlayedCount                ");
        _sb.AppendLine(" ,@pPlayedAmount               ");
        _sb.AppendLine(" ,@pWonCount                   ");
        _sb.AppendLine(" ,@pWonAmount                  ");
        _sb.AppendLine(" ,@pCashin                     ");
        _sb.AppendLine(" ,@pCashout                    ");
        _sb.AppendLine(" ,@pFinished                   ");
        _sb.AppendLine(" ,@pFinalBalance               ");
        _sb.AppendLine(" ,@pLocked                     ");
        _sb.AppendLine(" ,@pStandalone                 ");
        _sb.AppendLine(" ,@pPromo                      ");
        _sb.AppendLine(" ,@pWcpTransactionId           ");
        _sb.AppendLine(" ,@pRedeemableCashin           ");
        _sb.AppendLine(" ,@pRedeemableCashout          ");
        _sb.AppendLine(" ,@pRedeemablePlayed           ");
        _sb.AppendLine(" ,@pRedeemableWon              ");
        _sb.AppendLine(" ,@pNonRedeemableCashin        ");
        _sb.AppendLine(" ,@pNonRedeemableCashout       ");
        _sb.AppendLine(" ,@pNonRedeemablePlayed        ");
        _sb.AppendLine(" ,@pNonRedeemableWon           ");
        _sb.AppendLine(" ,@pBalanceMismatch            ");
        _sb.AppendLine(" ,@pSpentUsed                  ");
        _sb.AppendLine(" ,@pCancellableAmount          ");
        _sb.AppendLine(" ,@pReportedBalanceMismatch    ");
        _sb.AppendLine(" ,@pReCashin                   ");
        _sb.AppendLine(" ,@pPromoReCashin              ");
        _sb.AppendLine(" ,@pReCashout                  ");
        _sb.AppendLine(" ,@pPromoReCashout             ");
        _sb.AppendLine(" ,@pSiteId                     ");
        _sb.AppendLine(" ,@pTimeStamp                  ");
        _sb.AppendLine(" ,@pReTicketIn                 ");
        _sb.AppendLine(" ,@pPromoReTicketIn            ");
        _sb.AppendLine(" ,@pBillsInAmount              ");
        _sb.AppendLine(" ,@pReTicketOut                ");
        _sb.AppendLine(" ,@pPromoNrTicketIn            ");
        _sb.AppendLine(" ,@pPromoNrTicketOut           ");

        if (CenterPlaySessions.Columns.Count == 57)
        {
          _sb.AppendLine(" ,@pRedeemablePlayedOriginal    ");
          _sb.AppendLine(" ,@pRedeemableWonOriginal       ");
          _sb.AppendLine(" ,@pNonRedeemablePlayedOriginal ");
          _sb.AppendLine(" ,@pNonRedeemableWonOriginal    ");
          _sb.AppendLine(" ,@pPlayedCountOriginal         ");
          _sb.AppendLine(" ,@pWonCountOriginal            ");
          _sb.AppendLine(" ,@pAuxFtReCashIn               ");
          _sb.AppendLine(" ,@pAuxFtNrCashIn               ");
          _sb.AppendLine(" ,@pReFoundInEgm                ");
          _sb.AppendLine(" ,@pNrFoundInEgm                ");
          _sb.AppendLine(" ,@pReRemainingInEgm            ");
          _sb.AppendLine(" ,@pNrRemainingInEgm            ");
          _sb.AppendLine(" ,@pHandpaysAmount              ");
          _sb.AppendLine(" ,@pHandpaysPaidAmount          ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters

            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "PS_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "PS_ACCOUNT_ID";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "PS_TERMINAL_ID";
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "PS_TYPE";
            _cmd.Parameters.Add("@pTypedata", SqlDbType.Xml).SourceColumn = "PS_TYPE_DATA";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "PS_STATUS";
            _cmd.Parameters.Add("@pStarted", SqlDbType.DateTime).SourceColumn = "PS_STARTED";
            _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "PS_INITIAL_BALANCE";
            _cmd.Parameters.Add("@pPlayedCount", SqlDbType.Int).SourceColumn = "PS_PLAYED_COUNT";
            _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "PS_PLAYED_AMOUNT";
            _cmd.Parameters.Add("@pWonCount", SqlDbType.Int).SourceColumn = "PS_WON_COUNT";
            _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "PS_WON_AMOUNT";
            _cmd.Parameters.Add("@pCashin", SqlDbType.Money).SourceColumn = "PS_CASH_IN";
            _cmd.Parameters.Add("@pCashout", SqlDbType.Money).SourceColumn = "PS_CASH_OUT";
            _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).SourceColumn = "PS_FINISHED";
            _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "PS_FINAL_BALANCE";
            _cmd.Parameters.Add("@pLocked", SqlDbType.DateTime).SourceColumn = "PS_LOCKED";
            _cmd.Parameters.Add("@pStandalone", SqlDbType.Int).SourceColumn = "PS_STAND_ALONE";
            _cmd.Parameters.Add("@pPromo", SqlDbType.Bit).SourceColumn = "PS_PROMO";
            _cmd.Parameters.Add("@pWcpTransactionId", SqlDbType.BigInt).SourceColumn = "PS_WCP_TRANSACTION_ID";
            _cmd.Parameters.Add("@pRedeemableCashin", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_CASH_IN";
            _cmd.Parameters.Add("@pRedeemableCashout", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_CASH_OUT";
            _cmd.Parameters.Add("@pRedeemablePlayed", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_PLAYED";
            _cmd.Parameters.Add("@pRedeemableWon", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_WON";
            _cmd.Parameters.Add("@pNonRedeemableCashin", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_CASH_IN";
            _cmd.Parameters.Add("@pNonRedeemableCashout", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_CASH_OUT";
            _cmd.Parameters.Add("@pNonRedeemablePlayed", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_PLAYED";
            _cmd.Parameters.Add("@pNonRedeemableWon", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_WON";
            _cmd.Parameters.Add("@pBalanceMismatch", SqlDbType.Bit).SourceColumn = "PS_BALANCE_MISMATCH";
            _cmd.Parameters.Add("@pSpentUsed", SqlDbType.Money).SourceColumn = "PS_SPENT_USED";
            _cmd.Parameters.Add("@pCancellableAmount", SqlDbType.Money).SourceColumn = "PS_CANCELLABLE_AMOUNT";
            _cmd.Parameters.Add("@pReportedBalanceMismatch", SqlDbType.Money).SourceColumn = "PS_REPORTED_BALANCE_MISMATCH";
            _cmd.Parameters.Add("@pReCashin", SqlDbType.Money).SourceColumn = "PS_RE_CASH_IN";
            _cmd.Parameters.Add("@pPromoReCashin", SqlDbType.Money).SourceColumn = "PS_PROMO_RE_CASH_IN";
            _cmd.Parameters.Add("@pReCashout", SqlDbType.Money).SourceColumn = "PS_RE_CASH_OUT";
            _cmd.Parameters.Add("@pPromoReCashout", SqlDbType.Money).SourceColumn = "PS_PROMO_RE_CASH_OUT";
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pTimeStamp", SqlDbType.BigInt).SourceColumn = "PS_TIMESTAMP";
            _cmd.Parameters.Add("@pReTicketIn", SqlDbType.Money).SourceColumn = "PS_RE_TICKET_IN";
            _cmd.Parameters.Add("@pPromoReTicketIn", SqlDbType.Money).SourceColumn = "PS_PROMO_RE_TICKET_IN";
            _cmd.Parameters.Add("@pBillsInAmount", SqlDbType.Money).SourceColumn = "PS_BILLS_IN_AMOUNT";
            _cmd.Parameters.Add("@pReTicketOut", SqlDbType.Money).SourceColumn = "PS_RE_TICKET_OUT";
            _cmd.Parameters.Add("@pPromoNrTicketIn", SqlDbType.Money).SourceColumn = "PS_PROMO_NR_TICKET_IN";
            _cmd.Parameters.Add("@pPromoNrTicketOut", SqlDbType.Money).SourceColumn = "PS_PROMO_NR_TICKET_OUT";

            if (CenterPlaySessions.Columns.Count == 57)
            {
              _cmd.Parameters.Add("@pRedeemablePlayedOriginal", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_PLAYED_ORIGINAL";
              _cmd.Parameters.Add("@pRedeemableWonOriginal", SqlDbType.Money).SourceColumn = "PS_REDEEMABLE_WON_ORIGINAL";
              _cmd.Parameters.Add("@pNonRedeemablePlayedOriginal", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_PLAYED_ORIGINAL";
              _cmd.Parameters.Add("@pNonRedeemableWonOriginal", SqlDbType.Money).SourceColumn = "PS_NON_REDEEMABLE_WON_ORIGINAL";
              _cmd.Parameters.Add("@pPlayedCountOriginal", SqlDbType.Money).SourceColumn = "PS_PLAYED_COUNT_ORIGINAL";
              _cmd.Parameters.Add("@pWonCountOriginal", SqlDbType.Money).SourceColumn = "PS_WON_COUNT_ORIGINAL";
              _cmd.Parameters.Add("@pAuxFtReCashIn", SqlDbType.Money).SourceColumn = "PS_AUX_FT_RE_CASH_IN";
              _cmd.Parameters.Add("@pAuxFtNrCashIn", SqlDbType.Money).SourceColumn = "PS_AUX_FT_NR_CASH_IN";
              _cmd.Parameters.Add("@pReFoundInEgm", SqlDbType.Money).SourceColumn = "PS_RE_FOUND_IN_EGM";
              _cmd.Parameters.Add("@pNrFoundInEgm", SqlDbType.Money).SourceColumn = "PS_NR_FOUND_IN_EGM";
              _cmd.Parameters.Add("@pReRemainingInEgm", SqlDbType.Money).SourceColumn = "PS_RE_REMAINING_IN_EGM";
              _cmd.Parameters.Add("@pNrRemainingInEgm", SqlDbType.Money).SourceColumn = "PS_NR_REMAINING_IN_EGM";
              _cmd.Parameters.Add("@pHandpaysAmount", SqlDbType.Money).SourceColumn = "PS_HANDPAYS_AMOUNT";
              _cmd.Parameters.Add("@pHandpaysPaidAmount", SqlDbType.Money).SourceColumn = "PS_HANDPAYS_PAID_AMOUNT";
            }

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterPlaySessions) == CenterPlaySessions.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } //Update_CenterPlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Banks of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterBanks(Int32 SiteId, DataTable CenterBanks)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterBanks.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_Banks ");
        _sb.AppendLine("  @pSiteId  ");
        _sb.AppendLine(", @pBankId  ");
        _sb.AppendLine(", @pAreaId  ");
        _sb.AppendLine(", @pName    ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pBankId", SqlDbType.Int).SourceColumn = "BK_BANK_ID";
            _cmd.Parameters.Add("@pAreaId", SqlDbType.Int).SourceColumn = "BK_AREA_ID";
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "BK_NAME";


            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterBanks) == CenterBanks.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterBanks

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Terminals of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterTerminals(Int32 SiteId, DataTable CenterTerminals)
    {
      StringBuilder _sb;

      try
      {
        if (CenterTerminals.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        if (CenterTerminals.Columns.Count >= 36)
        {
          _sb.AppendLine("EXECUTE Update_Terminals ");
        }
        else
        {
          _sb.AppendLine("EXECUTE Update_TerminalsForOldVersion ");
        }

        _sb.AppendLine("@pSiteId ");
        _sb.AppendLine(",@pTerminalId ");
        _sb.AppendLine(",@pType   ");
        _sb.AppendLine(",@pServerId   ");
        _sb.AppendLine(",@pBaseName  ");
        _sb.AppendLine(",@pExternalId   ");
        _sb.AppendLine(",@pBlocked   ");
        _sb.AppendLine(",@pActive   ");
        _sb.AppendLine(",@pProviderId  ");
        _sb.AppendLine(",@pClientId   ");
        _sb.AppendLine(",@pBuildId   ");
        _sb.AppendLine(",@pTerminalType   ");
        _sb.AppendLine(",@pVendorId   ");
        _sb.AppendLine(",@pStatus   ");
        _sb.AppendLine(",@pRetirementDate  ");
        _sb.AppendLine(",@pRetirementRequested  ");
        _sb.AppendLine(",@pDenomination  ");
        _sb.AppendLine(",@pMultiDenomination  ");
        _sb.AppendLine(",@pProgram  ");
        _sb.AppendLine(",@pTheoreticalPayout ");
        _sb.AppendLine(",@pProvId  ");
        _sb.AppendLine(",@pBankId  ");
        _sb.AppendLine(",@pFloorId ");
        _sb.AppendLine(",@pGameType  ");
        _sb.AppendLine(",@pActivationDate  ");
        _sb.AppendLine(",@pCurrentAccountId  ");
        _sb.AppendLine(",@pCurrentPlaySessionId  ");
        _sb.AppendLine(",@pRegistrationCode  ");
        _sb.AppendLine(",@pSasFlags  ");
        _sb.AppendLine(",@pSerialNumber ");
        _sb.AppendLine(",@pCabinetType");
        _sb.AppendLine(",@pJackpotContributionPct");
        _sb.AppendLine(",@pPosition  ");
        _sb.AppendLine(",@pMachineId  ");

        if (CenterTerminals.Columns.Count >= 36)
        {
          _sb.AppendLine(",@pMasterId  ");
          _sb.AppendLine(",@pChangeId  ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TE_TYPE";
            _cmd.Parameters.Add("@pServerId", SqlDbType.Int).SourceColumn = "TE_SERVER_ID";
            _cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).SourceColumn = "TE_EXTERNAL_ID";
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).SourceColumn = "TE_BLOCKED";
            _cmd.Parameters.Add("@pActive", SqlDbType.Bit).SourceColumn = "TE_ACTIVE";
            _cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).SourceColumn = "TE_PROVIDER_ID";
            _cmd.Parameters.Add("@pClientId", SqlDbType.SmallInt).SourceColumn = "TE_CLIENT_ID";
            _cmd.Parameters.Add("@pBuildId", SqlDbType.SmallInt).SourceColumn = "TE_BUILD_ID";
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).SourceColumn = "TE_TERMINAL_TYPE";
            _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).SourceColumn = "TE_VENDOR_ID";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "TE_STATUS";
            _cmd.Parameters.Add("@pRetirementDate", SqlDbType.DateTime).SourceColumn = "TE_RETIREMENT_DATE";
            _cmd.Parameters.Add("@pRetirementRequested", SqlDbType.DateTime).SourceColumn = "TE_RETIREMENT_REQUESTED";
            _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "TE_DENOMINATION";
            _cmd.Parameters.Add("@pMultiDenomination", SqlDbType.NVarChar).SourceColumn = "TE_MULTI_DENOMINATION";
            _cmd.Parameters.Add("@pProgram", SqlDbType.NVarChar).SourceColumn = "TE_PROGRAM";
            _cmd.Parameters.Add("@pTheoreticalPayout", SqlDbType.Money).SourceColumn = "TE_THEORETICAL_PAYOUT";
            _cmd.Parameters.Add("@pProvId", SqlDbType.Int).SourceColumn = "TE_PROV_ID";
            _cmd.Parameters.Add("@pBankId", SqlDbType.Int).SourceColumn = "TE_BANK_ID";
            _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).SourceColumn = "TE_FLOOR_ID";
            _cmd.Parameters.Add("@pGameType", SqlDbType.Int).SourceColumn = "TE_GAME_TYPE";
            _cmd.Parameters.Add("@pActivationDate", SqlDbType.DateTime).SourceColumn = "TE_ACTIVATION_DATE";
            _cmd.Parameters.Add("@pCurrentAccountId", SqlDbType.BigInt).SourceColumn = "TE_CURRENT_ACCOUNT_ID";
            _cmd.Parameters.Add("@pCurrentPlaySessionId", SqlDbType.BigInt).SourceColumn = "TE_CURRENT_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pRegistrationCode", SqlDbType.NVarChar).SourceColumn = "TE_REGISTRATION_CODE";
            _cmd.Parameters.Add("@pSasFlags", SqlDbType.Int).SourceColumn = "TE_SAS_FLAGS";
            _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).SourceColumn = "TE_SERIAL_NUMBER";
            _cmd.Parameters.Add("@pCabinetType", SqlDbType.NVarChar).SourceColumn = "TE_CABINET_TYPE";
            _cmd.Parameters.Add("@pJackpotContributionPct", SqlDbType.Decimal).SourceColumn = "TE_JACKPOT_CONTRIBUTION_PCT";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TE_TERMINAL_ID";
            _cmd.Parameters.Add("@pPosition", SqlDbType.Int).SourceColumn = "TE_POSITION";
            _cmd.Parameters.Add("@pMachineId", SqlDbType.NVarChar).SourceColumn = "TE_MACHINE_ID";

            if (CenterTerminals.Columns.Count >= 36)
            {
              _cmd.Parameters.Add("@pMasterId", SqlDbType.Int).SourceColumn = "TE_MASTER_ID";
              _cmd.Parameters.Add("@pChangeId", SqlDbType.Int).SourceColumn = "TE_CHANGE_ID";
              _cmd.Parameters.Add("@pBaseName", SqlDbType.NVarChar).SourceColumn = "TE_BASE_NAME";
            }
            else
            {
              _cmd.Parameters.Add("@pBaseName", SqlDbType.NVarChar).SourceColumn = "TE_NAME";
            }

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterTerminals) == CenterTerminals.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center TerminalGameTranslation of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterTerminalGameTranslation(Int32 SiteId, DataTable CenterTerminalGameTranslation)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterTerminalGameTranslation.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_TerminalGameTranslation ");
        _sb.AppendLine("          @pSiteId            ");
        _sb.AppendLine("        , @pTerminalId        ");
        _sb.AppendLine("        , @pSourceGameId      ");
        _sb.AppendLine("        , @pTargetGameId      ");
        _sb.AppendLine("        , @pPayoutIdx         ");
        _sb.AppendLine("        , @pTranslatedGameId  ");
        _sb.AppendLine("        , @pCreated           ");
        _sb.AppendLine("        , @pUpdated           ");



        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "tgt_terminal_id";
            _cmd.Parameters.Add("@pSourceGameId", SqlDbType.Int).SourceColumn = "tgt_source_game_id";
            _cmd.Parameters.Add("@pTargetGameId", SqlDbType.Int).SourceColumn = "tgt_target_game_id";
            _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).SourceColumn = "tgt_created";
            _cmd.Parameters.Add("@pUpdated", SqlDbType.DateTime).SourceColumn = "tgt_updated";
            _cmd.Parameters.Add("@pPayoutIdx", SqlDbType.Int).SourceColumn = "tgt_payout_idx";
            _cmd.Parameters.Add("@pTranslatedGameId", SqlDbType.Int).SourceColumn = "tgt_translated_game_id";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterTerminalGameTranslation) == CenterTerminalGameTranslation.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterTerminalGameTranslation

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center Games of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterGames(Int32 SiteId, DataTable CenterGames)
    {
      StringBuilder _sb;

      try
      {
        if (CenterGames.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE Update_Games ");
        _sb.AppendLine(" @pSiteId ");
        _sb.AppendLine(",@pGameId");
        _sb.AppendLine(",@pName");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "gm_game_id";
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "gm_name";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterGames) == CenterGames.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // Update_CenterGames

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update Center TerminalsConnected of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterTerminalsConnected(Int32 SiteId, DataTable TerminalsConnected)
    {
      StringBuilder _sb;

      try
      {
        if (TerminalsConnected.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE Update_TerminalsConnected");
        _sb.AppendLine("  @pSiteId");
        _sb.AppendLine(", @pMasterId");
        _sb.AppendLine(", @pDate");
        _sb.AppendLine(", @pTerminalId");
        _sb.AppendLine(", @pStatus");
        _sb.AppendLine(", @pConnected");
        _sb.AppendLine(", @pTimestamp");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            //FOS 27-03-2015
            //If site is not actualized, in column MasterId we put the value of terminalId
            if (TerminalsConnected.Columns.Count >= 6)
            {
              _cmd.Parameters.Add("@pMasterId", SqlDbType.Int).SourceColumn = "TC_MASTER_ID";
            }
            else
            {
              _cmd.Parameters.Add("@pMasterId", SqlDbType.Int).SourceColumn = "TC_TERMINAL_ID";
            }
            _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "TC_DATE";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TC_TERMINAL_ID";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "TC_STATUS";
            _cmd.Parameters.Add("@pConnected", SqlDbType.Bit).SourceColumn = "TC_CONNECTED";
            _cmd.Parameters.Add("@pTimestamp", SqlDbType.BigInt).SourceColumn = "TC_TIMESTAMP";


            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(TerminalsConnected) == TerminalsConnected.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterTerminalsConnected

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Center Providers of Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_CenterProviders(Int32 SiteId, DataTable CenterProviders)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterProviders.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_Providers ");
        _sb.AppendLine(" @pSiteId ");
        _sb.AppendLine(",@pProviderId");
        _sb.AppendLine(",@pName ");
        _sb.AppendLine(",@pHide");
        _sb.AppendLine(",@pPointsMultiplier ");
        _sb.AppendLine(",@p3gs ");
        _sb.AppendLine(",@p3gsVendorId ");
        _sb.AppendLine(",@p3gsVendorIp ");
        _sb.AppendLine(",@pSiteJackpot ");
        _sb.AppendLine(",@pOnlyRedeemable ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pProviderId", SqlDbType.Int).SourceColumn = "pv_id";
            _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "PV_NAME";
            _cmd.Parameters.Add("@pHide", SqlDbType.Bit).SourceColumn = "PV_HIDE";
            _cmd.Parameters.Add("@pPointsMultiplier", SqlDbType.Money).SourceColumn = "PV_POINTS_MULTIPLIER";
            _cmd.Parameters.Add("@p3gs", SqlDbType.Bit).SourceColumn = "PV_3GS";
            _cmd.Parameters.Add("@p3gsVendorId", SqlDbType.NVarChar).SourceColumn = "PV_3GS_VENDOR_ID";
            _cmd.Parameters.Add("@p3gsVendorIp", SqlDbType.NVarChar).SourceColumn = "PV_3GS_VENDOR_IP";
            _cmd.Parameters.Add("@pSiteJackpot", SqlDbType.Bit).SourceColumn = "PV_SITE_JACKPOT";
            _cmd.Parameters.Add("@pOnlyRedeemable", SqlDbType.Bit).SourceColumn = "PV_ONLY_REDEEMABLE";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterProviders) == CenterProviders.Rows.Count)
              {
                _db_trx.Commit();

                return true;

              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

      }
      return false;
    } // Update_CenterProviders

    //------------------------------------------------------------------------------
    // PURPOSE : Update account documents from the Site.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_AccountDocuments(Int32 SiteId, DataTable AccountDocuments, out DataTable ReplyTable)
    {
      StringBuilder _sb = new StringBuilder();
      Int32 _num_rows_affected;
      DataRow _dr_reply;
      Int32 _count_table;

      ReplyTable = new DataTable();
      ReplyTable.TableName = "ACCOUNT_DOCUMENTS";
      ReplyTable.Columns.Add("AD_ACCOUNT_ID", Type.GetType("System.Int64"));
      ReplyTable.Columns.Add("AD_MS_SEQUENCE_ID", Type.GetType("System.Int64"));

      try
      {
        if (AccountDocuments.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_AccountDocuments ");
        _sb.AppendLine(" @pAccountId ");
        _sb.AppendLine(",@pCreated");
        _sb.AppendLine(",@pModified ");
        _sb.AppendLine(",@pData");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AD_ACCOUNT_ID";
            _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).SourceColumn = "AD_CREATED";
            _cmd.Parameters.Add("@pModified", SqlDbType.DateTime).SourceColumn = "AD_MODIFIED";
            _cmd.Parameters.Add("@pData", SqlDbType.VarBinary).SourceColumn = "AD_DATA"; // -1


            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              _sql_adapt.ContinueUpdateOnError = true;
              _num_rows_affected = _sql_adapt.Update(AccountDocuments);
              _count_table = AccountDocuments.Rows.Count;

              if (_num_rows_affected != _count_table)
              {
                Log.Warning(String.Format("Not insert all account documents in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_table, _num_rows_affected));
              }
            }

          }
          _db_trx.Commit();
        }

        foreach (DataRow _ad_dr in AccountDocuments.Rows)
        {
          if (_ad_dr.RowState == DataRowState.Unchanged)
          {
            _dr_reply = ReplyTable.NewRow();

            _dr_reply[REPLY_AD_ACCOUNT_ID] = _ad_dr["AD_ACCOUNT_ID"];
            _dr_reply[REPLY_AD_MS_SEQUENCE_ID] = _ad_dr["AD_MS_SEQUENCE_ID"];

            ReplyTable.Rows.Add(_dr_reply);
          }
        }
        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }  // Update_AccountDocuments

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Center Game Play Sessions of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, GamesPlaySessions
    //
    //      - OUTPUT : _replyGamesPlaySessions
    //
    // RETURNS :
    //
    static public Boolean Insert_GamePlaySessions(Int32 SiteId, DataSet RequestPlaySessions, out DataSet ReplyPlaySessions)
    {
      StringBuilder _sb;
      Int32 _num_rows_insert;
      DataRow _dr_reply;
      DataTable _replyGamesPlaySessions;
      DataTable _replyELP001PlaySessions;
      Int32 _count_game_play_sessions;
      Int32 _count_elp01_play_sessions;

      _replyGamesPlaySessions = new DataTable();
      _replyGamesPlaySessions.TableName = "GAME_PLAY_SESSIONS";
      _replyGamesPlaySessions.Columns.Add("GPS_PLAY_SESSION_ID", Type.GetType("System.Int64"));
      _replyGamesPlaySessions.Columns.Add("GPS_GAME_ID", Type.GetType("System.Int32"));

      _replyELP001PlaySessions = new DataTable();
      _replyELP001PlaySessions.TableName = "ELP01_PLAY_SESSIONS";
      _replyELP001PlaySessions.Columns.Add("EPS_ID", Type.GetType("System.Int64"));
      ReplyPlaySessions = new DataSet();

      try
      {
        _sb = new StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // GAME_PLAY_SESSIONS
          _count_game_play_sessions = RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"].Rows.Count;

          if (_count_game_play_sessions > 0)
          {
            _sb.AppendLine("EXECUTE Insert_GamePlaySessions ");
            _sb.AppendLine("  @pSiteId ");
            _sb.AppendLine(", @pPlaySessionId ");
            _sb.AppendLine(", @pGameId ");
            _sb.AppendLine(", @pAccountId ");
            _sb.AppendLine(", @pTerminalId ");
            _sb.AppendLine(", @pPlayedCount ");
            _sb.AppendLine(", @pPlayedAmount ");
            _sb.AppendLine(", @pWonCount ");
            _sb.AppendLine(", @pWonAmount ");
            _sb.AppendLine(", @pPayout ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
              // GAME_PLAY_SESSIONS            
              _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "GPS_PLAY_SESSION_ID";
              _cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GPS_GAME_ID";
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "GPS_ACCOUNT_ID";
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "GPS_TERMINAL_ID";
              _cmd.Parameters.Add("@pPlayedCount", SqlDbType.Int).SourceColumn = "GPS_PLAYED_COUNT";
              _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "GPS_PLAYED_AMOUNT";
              _cmd.Parameters.Add("@pWonCount", SqlDbType.Int).SourceColumn = "GPS_WON_COUNT";
              _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "GPS_WON_AMOUNT";
              _cmd.Parameters.Add("@pPayout", SqlDbType.Money).SourceColumn = "GPS_PAYOUT";

              using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
              {
                _sql_adapt.InsertCommand = _cmd;
                _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _sql_adapt.UpdateBatchSize = 500;
                _sql_adapt.ContinueUpdateOnError = true;
                _num_rows_insert = _sql_adapt.Update(RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"]);

                if (_num_rows_insert != _count_game_play_sessions)
                {
                  Log.Warning(String.Format("Not insert all game play sessions in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_game_play_sessions, _num_rows_insert));
                }
              }// using SqlDataAdapter
            } // using SqlCommand

            foreach (DataRow _sessions_dr in RequestPlaySessions.Tables["GAME_PLAY_SESSIONS"].Rows)
            {
              if (_sessions_dr.RowState == DataRowState.Unchanged)
              {
                _dr_reply = _replyGamesPlaySessions.NewRow();

                _dr_reply[REPLY_GPS_PLAY_SESSION_ID] = _sessions_dr["GPS_PLAY_SESSION_ID"];
                _dr_reply[REPLY_GPS_GAME_ID] = _sessions_dr["GPS_GAME_ID"];

                _replyGamesPlaySessions.Rows.Add(_dr_reply);
              }
            }
            ReplyPlaySessions.Tables.Add(_replyGamesPlaySessions);

          } // if
          else
          {
            ReplyPlaySessions.Tables.Add(_replyGamesPlaySessions);
          }

          // ELP01_PLAY_SESSIONS
          _count_elp01_play_sessions = RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"].Rows.Count;

          if (_count_elp01_play_sessions > 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("EXECUTE Insert_Elp01PlaySessions ");
            _sb.AppendLine("  @pSiteId ");
            _sb.AppendLine(", @pId ");
            _sb.AppendLine(", @pTicketNumber ");
            _sb.AppendLine(", @pSlotSerialNumber ");
            _sb.AppendLine(", @pSlotHouseNumber ");
            _sb.AppendLine(", @pVenueCode ");
            _sb.AppendLine(", @pAreaCode ");
            _sb.AppendLine(", @pBankCode ");
            _sb.AppendLine(", @pVendorCode ");
            _sb.AppendLine(", @pGameCode ");
            _sb.AppendLine(", @pStartTime ");
            _sb.AppendLine(", @pEndTime ");
            _sb.AppendLine(", @pBetAmount ");
            _sb.AppendLine(", @pPaidAmount ");
            _sb.AppendLine(", @pGamesPlayed ");
            _sb.AppendLine(", @pInitialAmount ");
            _sb.AppendLine(", @pAditionalAmount ");
            _sb.AppendLine(", @pFinalAmount ");
            _sb.AppendLine(", @pBetCombCode ");
            _sb.AppendLine(", @pKindofTicket ");
            _sb.AppendLine(", @pSequenceNumber ");
            _sb.AppendLine(", @pCuponNumber ");
            _sb.AppendLine(", @pDateUpdated ");
            _sb.AppendLine(", @pDateInserted ");
            _sb.AppendLine(", @pAccountId ");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              // ELP01          
              _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
              _cmd.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = "EPS_ID";
              _cmd.Parameters.Add("@pTicketNumber", SqlDbType.Decimal).SourceColumn = "EPS_TICKET_NUMBER";
              _cmd.Parameters.Add("@pSlotSerialNumber", SqlDbType.VarChar, 50).SourceColumn = "EPS_SLOT_SERIAL_NUMBER";
              _cmd.Parameters.Add("@pSlotHouseNumber", SqlDbType.VarChar, 40).SourceColumn = "EPS_SLOT_HOUSE_NUMBER";
              _cmd.Parameters.Add("@pVenueCode", SqlDbType.Int).SourceColumn = "eps_venue_code";
              _cmd.Parameters.Add("@pAreaCode", SqlDbType.Int).SourceColumn = "EPS_AREA_CODE";
              _cmd.Parameters.Add("@pBankCode", SqlDbType.Int).SourceColumn = "EPS_BANK_CODE";
              _cmd.Parameters.Add("@pVendorCode", SqlDbType.Int).SourceColumn = "EPS_VENDOR_CODE";
              _cmd.Parameters.Add("@pGameCode", SqlDbType.Int).SourceColumn = "EPS_GAME_CODE";
              _cmd.Parameters.Add("@pStartTime", SqlDbType.DateTime).SourceColumn = "EPS_START_TIME";
              _cmd.Parameters.Add("@pEndTime", SqlDbType.DateTime).SourceColumn = "EPS_END_TIME";
              _cmd.Parameters.Add("@pBetAmount", SqlDbType.Decimal).SourceColumn = "EPS_BET_AMOUNT";
              _cmd.Parameters.Add("@pPaidAmount", SqlDbType.Decimal).SourceColumn = "EPS_PAID_AMOUNT";
              _cmd.Parameters.Add("@pGamesPlayed", SqlDbType.Int).SourceColumn = "EPS_GAMES_PLAYED";
              _cmd.Parameters.Add("@pInitialAmount", SqlDbType.Decimal).SourceColumn = "EPS_INITIAL_AMOUNT";
              _cmd.Parameters.Add("@pAditionalAmount", SqlDbType.Decimal).SourceColumn = "EPS_ADITIONAL_AMOUNT";
              _cmd.Parameters.Add("@pFinalAmount", SqlDbType.Decimal).SourceColumn = "EPS_FINAL_AMOUNT";
              _cmd.Parameters.Add("@pBetCombCode", SqlDbType.Int).SourceColumn = "EPS_BET_COMB_CODE";
              _cmd.Parameters.Add("@pKindofTicket", SqlDbType.Int).SourceColumn = "EPS_KINDOF_TICKET";
              _cmd.Parameters.Add("@pSequenceNumber", SqlDbType.BigInt).SourceColumn = "EPS_SEQUENCE_NUMBER";
              _cmd.Parameters.Add("@pCuponNumber", SqlDbType.Decimal).SourceColumn = "EPS_CUPON_NUMBER";
              _cmd.Parameters.Add("@pDateUpdated", SqlDbType.DateTime).SourceColumn = "EPS_DATE_UPDATED";
              _cmd.Parameters.Add("@pDateInserted", SqlDbType.DateTime).SourceColumn = "EPS_DATE_INSERTED";
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "EPS_ACCOUNT_ID";

              using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
              {
                _sql_adapt.InsertCommand = _cmd;
                _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _sql_adapt.UpdateBatchSize = 500;
                _num_rows_insert = _sql_adapt.Update(RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"]);

                if (_num_rows_insert != _count_elp01_play_sessions)
                {
                  Log.Warning(String.Format("Not insert all ELP001_PLAY_SESSIONS in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_elp01_play_sessions, _num_rows_insert));
                }
              } // using SqlDataAdapter
            } // using SqlCommand

            foreach (DataRow _sessions_dr in RequestPlaySessions.Tables["ELP01_PLAY_SESSIONS"].Rows)
            {
              if (_sessions_dr.RowState == DataRowState.Unchanged)
              {
                _dr_reply = _replyELP001PlaySessions.NewRow();

                _dr_reply[REPLY_EPS_ID] = _sessions_dr["EPS_ID"];

                _replyELP001PlaySessions.Rows.Add(_dr_reply);
              }
            }
            ReplyPlaySessions.Tables.Add(_replyELP001PlaySessions);

          } // if
          else
          {
            ReplyPlaySessions.Tables.Add(_replyELP001PlaySessions);
          }
          _db_trx.Commit();
        } // using DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Insert_GamePlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Request Sell sAnd Buys site's operations
    //
    //  PARAMS :
    //      - INPUT :  SiteId, RequestSellsAndBuys
    //
    //      - OUTPUT : _replySellsAndBuys
    //
    // RETURNS :
    //
    static public Boolean Insert_SellsAndBuys(Int32 SiteId, DataSet RequestSellsAndBuys, out DataSet ReplySellsAndBuys)
    {
      StringBuilder _sb;
      Int32 _num_rows_insert;
      DataRow _dr_reply;
      DataTable _replySellsAndBuys;
      Int32 _count_reply_sells_and_buys;

      ReplySellsAndBuys = new DataSet();

      _replySellsAndBuys = new DataTable();
      _replySellsAndBuys.TableName = "SELLS_AND_BUYS";
      _replySellsAndBuys.Columns.Add("SB_SELL_BUYS_ID", Type.GetType("System.Int64"));

      try
      {
        _sb = new StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // SELLS_AND_BUYS
          _count_reply_sells_and_buys = RequestSellsAndBuys.Tables[0].Rows.Count;

          if (_count_reply_sells_and_buys > 0)
          {
            _sb.AppendLine("EXECUTE Insert_Task68WorkData ");
            _sb.AppendLine("        @pSiteId              ");
            _sb.AppendLine("      , @pOperationId         ");
            _sb.AppendLine("      , @pDatetime            ");
            _sb.AppendLine("      , @pAccountId           ");
            _sb.AppendLine("      , @pOperType            ");
            _sb.AppendLine("      , @pTotalCashIn         ");
            _sb.AppendLine("      , @pCashInSplit1        ");
            _sb.AppendLine("      , @pCashInSplit2        ");
            _sb.AppendLine("      , @pCashInTax1          ");
            _sb.AppendLine("      , @pCashInTax2          ");
            _sb.AppendLine("      , @pTotalCashOut        ");
            _sb.AppendLine("      , @pDevolution          ");
            _sb.AppendLine("      , @pPrize               ");
            _sb.AppendLine("      , @pIsr1                ");
            _sb.AppendLine("      , @pIsr2                ");
            _sb.AppendLine("      , @pCashOutMoney        ");
            _sb.AppendLine("      , @pCashOutCheck        ");
            _sb.AppendLine("      , @pRfc                 ");
            _sb.AppendLine("      , @pCurp                ");
            _sb.AppendLine("      , @pName                ");
            _sb.AppendLine("      , @pAddress             ");
            _sb.AppendLine("      , @pCity                ");
            _sb.AppendLine("      , @pPostalCode          ");
            _sb.AppendLine("      , @pEvidenceRfc         ");
            _sb.AppendLine("      , @pEvidenceCurp        ");
            _sb.AppendLine("      , @pEvidenceName        ");
            _sb.AppendLine("      , @pEvidenceAddres      ");
            _sb.AppendLine("      , @pEvidenceCity        ");
            _sb.AppendLine("      , @pEvidencePostalCode  ");
            _sb.AppendLine("      , @pEvidence            ");
            _sb.AppendLine("      , @pDocumentId          ");
            _sb.AppendLine("      , @pRetentionRounding   ");
            _sb.AppendLine("      , @pServiceCharge       ");
            _sb.AppendLine("      , @pDecimalRounding     ");
            _sb.AppendLine("      , @pCashierSessionId    ");
            _sb.AppendLine("      , @pPrizeAbove          ");
            _sb.AppendLine("      , @pCashierName         ");
            _sb.AppendLine("      , @pPrizeExpired        ");
            _sb.AppendLine("      , @pVoucherId           ");
            _sb.AppendLine("      , @pFolio               ");
            _sb.AppendLine("      , @pPromotionalAmount   ");
            _sb.AppendLine("      , @pPaymentTypeCode     ");
            _sb.AppendLine("      , @pPaymentType         ");
            _sb.AppendLine("      , @pRechargePointsPrize");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              // Parameters
              _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
              _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "IdOperacion";
              _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).SourceColumn = "FechaHora";
              _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "NoCuenta";
              _cmd.Parameters.Add("@pOperType", SqlDbType.Int).SourceColumn = "TipoOperacion";
              _cmd.Parameters.Add("@pTotalCashIn", SqlDbType.Money).SourceColumn = "TotalRecarga";
              _cmd.Parameters.Add("@pCashInSplit1", SqlDbType.Money).SourceColumn = "EmpresaA";
              _cmd.Parameters.Add("@pCashInSplit2", SqlDbType.Money).SourceColumn = "EmpresaB";
              _cmd.Parameters.Add("@pCashInTax1", SqlDbType.Money).SourceColumn = "Impuesto1";
              _cmd.Parameters.Add("@pCashInTax2", SqlDbType.Money).SourceColumn = "Impuesto2";
              _cmd.Parameters.Add("@pTotalCashOut", SqlDbType.Money).SourceColumn = "TotalRetiro";
              _cmd.Parameters.Add("@pDevolution", SqlDbType.Money).SourceColumn = "Devolucion";
              _cmd.Parameters.Add("@pPrize", SqlDbType.Money).SourceColumn = "Premio";
              _cmd.Parameters.Add("@pIsr1", SqlDbType.Money).SourceColumn = "ISR1";
              _cmd.Parameters.Add("@pIsr2", SqlDbType.Money).SourceColumn = "ISR2";
              _cmd.Parameters.Add("@pCashOutMoney", SqlDbType.Money).SourceColumn = "RetiroEnEfectivo";
              _cmd.Parameters.Add("@pCashOutCheck", SqlDbType.Money).SourceColumn = "RetiroEnCheque";
              _cmd.Parameters.Add("@pRfc", SqlDbType.NVarChar).SourceColumn = "RFC";
              _cmd.Parameters.Add("@pCurp", SqlDbType.NVarChar).SourceColumn = "CURP";
              _cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "Nombre";
              _cmd.Parameters.Add("@pAddress", SqlDbType.NVarChar).SourceColumn = "Domicilio";
              _cmd.Parameters.Add("@pCity", SqlDbType.NVarChar).SourceColumn = "Localidad";
              _cmd.Parameters.Add("@pPostalCode", SqlDbType.NVarChar).SourceColumn = "CodigoPostal";
              _cmd.Parameters.Add("@pEvidenceRfc", SqlDbType.NVarChar).SourceColumn = "Constancia.RFC";
              _cmd.Parameters.Add("@pEvidenceCurp", SqlDbType.NVarChar).SourceColumn = "Constancia.CURP";
              _cmd.Parameters.Add("@pEvidenceName", SqlDbType.NVarChar).SourceColumn = "Constancia.Nombre";
              _cmd.Parameters.Add("@pEvidenceAddres", SqlDbType.NVarChar).SourceColumn = "Constancia.Domicilio";
              _cmd.Parameters.Add("@pEvidenceCity", SqlDbType.NVarChar).SourceColumn = "Constancia.Localidad";
              _cmd.Parameters.Add("@pEvidencePostalCode", SqlDbType.NVarChar).SourceColumn = "Constancia.CodigoPostal";
              _cmd.Parameters.Add("@pEvidence", SqlDbType.Bit).SourceColumn = "Constancia";
              _cmd.Parameters.Add("@pDocumentId", SqlDbType.Int).SourceColumn = "DocumentID";
              _cmd.Parameters.Add("@pRetentionRounding", SqlDbType.Money).SourceColumn = "RedondeoPorRetencion";
              _cmd.Parameters.Add("@pServiceCharge", SqlDbType.Money).SourceColumn = "CargoPorServicio";
              _cmd.Parameters.Add("@pDecimalRounding", SqlDbType.Money).SourceColumn = "RedondeoPorDecimales";
              _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).SourceColumn = "SesionCaja";
              _cmd.Parameters.Add("@pPrizeAbove", SqlDbType.Money).SourceColumn = "PremioEspecie";
              _cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar).SourceColumn = "CashierName";
              _cmd.Parameters.Add("@pPrizeExpired", SqlDbType.Money).SourceColumn = "PremioCaducado";
              _cmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).SourceColumn = "VoucherId";
              _cmd.Parameters.Add("@pFolio", SqlDbType.BigInt).SourceColumn = "Folio";
              _cmd.Parameters.Add("@pPromotionalAmount", SqlDbType.Money).SourceColumn = "SaldoPromocional";
              _cmd.Parameters.Add("@pPaymentTypeCode", SqlDbType.NVarChar).SourceColumn = "ClaveTipoPagoRecarga";
              _cmd.Parameters.Add("@pPaymentType", SqlDbType.NVarChar).SourceColumn = "FormaPago";
              _cmd.Parameters.Add("@pRechargePointsPrize", SqlDbType.Money).SourceColumn = "RecargaPorPuntos";

              using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
              {
                _sql_adapt.InsertCommand = _cmd;
                _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                _sql_adapt.UpdateBatchSize = 500;
                _sql_adapt.ContinueUpdateOnError = true;
                _num_rows_insert = _sql_adapt.Update(RequestSellsAndBuys.Tables[0]);

                if (_num_rows_insert != _count_reply_sells_and_buys)
                {
                  Log.Warning(String.Format("Not insert all sells and buys operations in Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_reply_sells_and_buys, _num_rows_insert));
                }
              }

              _db_trx.Commit();
            }

            foreach (DataRow _sessions_dr in RequestSellsAndBuys.Tables[0].Rows)
            {
              if (_sessions_dr.RowState == DataRowState.Unchanged)
              {
                _dr_reply = _replySellsAndBuys.NewRow();
                _dr_reply[REPLY_SELLS_BUYS_ID] = _sessions_dr["IdOperacion"];
                _replySellsAndBuys.Rows.Add(_dr_reply);
              }
            }
          }//if
          ReplySellsAndBuys.Tables.Add(_replySellsAndBuys);

        } // using DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Insert_SellsAndBuys


    //------------------------------------------------------------------------------
    // PURPOSE : Update last activity in the Site.
    //
    //  PARAMS :
    //      - INPUT : SiteId, LastActivity
    //
    //      - OUTPUT : _ReplyTable
    //
    // RETURNS :
    //
    static public Boolean Update_LastActivity(Int32 SiteId, DataTable LastActivity, out DataTable ReplyTable)
    {
      StringBuilder _sb = new StringBuilder();
      Int32 _num_rows_affected;
      DataRow _dr_reply;
      Int32 _count_table;

      ReplyTable = new DataTable();
      ReplyTable.TableName = "LAST_ACCOUNT_ACTIVITY";
      ReplyTable.Columns.Add("LAA_ACCOUNT_ID", Type.GetType("System.Int64"));
      ReplyTable.Columns.Add("LAA_TIMESTAMP", Type.GetType("System.Int64"));

      try
      {
        if (LastActivity.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_LastActivity ");
        _sb.AppendLine(" @pSite_Id");
        _sb.AppendLine(",@pAccount_Id");
        _sb.AppendLine(",@pLast_Activity");
        _sb.AppendLine(",@pRE_Balance");
        _sb.AppendLine(",@pPromo_RE_Balance");
        _sb.AppendLine(",@pPromo_NR_Balance");
        _sb.AppendLine(",@pIn_Session_RE_Balance");
        _sb.AppendLine(",@pIn_Session_Promo_RE_Balance");
        _sb.AppendLine(",@pIn_Session_Promo_NR_Balance");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSite_Id", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pAccount_Id", SqlDbType.BigInt).SourceColumn = "LAA_ACCOUNT_ID";
            _cmd.Parameters.Add("@pLast_Activity", SqlDbType.DateTime).SourceColumn = "LAA_LAST_ACTIVITY";
            _cmd.Parameters.Add("@pRE_Balance", SqlDbType.Money).SourceColumn = "LAA_RE_BALANCE";
            _cmd.Parameters.Add("@pPromo_RE_Balance", SqlDbType.Money).SourceColumn = "LAA_PROMO_RE_BALANCE";
            _cmd.Parameters.Add("@pPromo_NR_Balance", SqlDbType.Money).SourceColumn = "LAA_PROMO_NR_BALANCE";
            _cmd.Parameters.Add("@pIn_Session_RE_Balance", SqlDbType.Money).SourceColumn = "LAA_IN_SESSION_RE_BALANCE";
            _cmd.Parameters.Add("@pIn_Session_Promo_RE_Balance", SqlDbType.Money).SourceColumn = "LAA_IN_SESSION_PROMO_RE_BALANCE";
            _cmd.Parameters.Add("@pIn_Session_Promo_NR_Balance", SqlDbType.Money).SourceColumn = "LAA_IN_SESSION_PROMO_NR_BALANCE";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              //_sql_adapt.ContinueUpdateOnError = true;
              _num_rows_affected = _sql_adapt.Update(LastActivity);
              _count_table = LastActivity.Rows.Count;

              if (_num_rows_affected != _count_table)
              {
                Log.Warning(String.Format("Not insert all account activity in the Site {0}. Expected: {1}, Updated: {2}", SiteId, _count_table, _num_rows_affected));
              }
            }
          }
          _db_trx.Commit();

        }

        foreach (DataRow _ad_dr in LastActivity.Rows)
        {
          if (_ad_dr.RowState == DataRowState.Unchanged)
          {
            _dr_reply = ReplyTable.NewRow();

            _dr_reply[REPLY_AD_ACCOUNT_ID] = _ad_dr["LAA_ACCOUNT_ID"];
            _dr_reply[REPLY_AD_MS_SEQUENCE_ID] = _ad_dr["LAA_TIMESTAMP"];

            ReplyTable.Rows.Add(_dr_reply);
          }
        }
        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }  // Update_LastActivity

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Casier Sesions Gruped By Hour of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, CasierSesionsGrupedByHour
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    static public Boolean Update_CenterCasierSesionsGrupedByHour(Int32 SiteId, DataTable CenterCasierSesionsGrupedByHour)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CenterCasierSesionsGrupedByHour.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        // RAB 05-APR-2016: PBI 10787
        if (CenterCasierSesionsGrupedByHour.Columns.Count == 14)
        {
          _sb.AppendLine(" EXECUTE Update_CashierMovementsGrupedByHour ");
        }
        else
        {
          _sb.AppendLine("EXECUTE Update_CashierMovementsGrupedByHourForOldVersion ");
        }
        
        _sb.AppendLine("  @pSiteId                ");
        _sb.AppendLine(" ,@pDate                  ");
        _sb.AppendLine(" ,@pType                  ");
        _sb.AppendLine(" ,@pSubType               ");
        _sb.AppendLine(" ,@pCurrencyIsoCode       ");
        _sb.AppendLine(" ,@pCurrencyDenomination  ");
        _sb.AppendLine(" ,@pTypeCount             ");
        _sb.AppendLine(" ,@pSubAmount             ");
        _sb.AppendLine(" ,@pAddAmount             ");
        _sb.AppendLine(" ,@pAuxAmount             ");
        _sb.AppendLine(" ,@pInitialBalance        ");
        _sb.AppendLine(" ,@pFinalBalance          ");
        _sb.AppendLine(" ,@pTimeStamp             ");
        _sb.AppendLine(" ,@pUniqueId              ");

        // RAB 05-APR-2016: PBI 10787
        if(CenterCasierSesionsGrupedByHour.Columns.Count >= 14)
        {
          _sb.AppendLine(" ,@pCageCurrencyType     ");
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "CM_DATE";
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CM_TYPE";
            _cmd.Parameters.Add("@pSubType", SqlDbType.Int).SourceColumn = "CM_SUB_TYPE";
            _cmd.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar, 3).SourceColumn = "CM_CURRENCY_ISO_CODE";
            _cmd.Parameters.Add("@pCurrencyDenomination", SqlDbType.Money).SourceColumn = "CM_CURRENCY_DENOMINATION";
            _cmd.Parameters.Add("@pTypeCount", SqlDbType.Int).SourceColumn = "CM_TYPE_COUNT";
            _cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "CM_SUB_AMOUNT";
            _cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "CM_ADD_AMOUNT";
            _cmd.Parameters.Add("@pAuxAmount", SqlDbType.Money).SourceColumn = "CM_AUX_AMOUNT";
            _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "CM_INITIAL_BALANCE";
            _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "CM_FINAL_BALANCE";
            _cmd.Parameters.Add("@pTimeStamp", SqlDbType.BigInt).SourceColumn = "CM_TIMESTAMP";
            _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "CM_UNIQUE_ID";

            // RAB 05-APR-2016: PBI 10787
            if (CenterCasierSesionsGrupedByHour.Columns.Count >= 14)
            {
              _cmd.Parameters.Add("@pCageCurrencyType", SqlDbType.Int).SourceColumn = "CM_CAGE_CURRENCY_TYPE";
            }

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CenterCasierSesionsGrupedByHour) == CenterCasierSesionsGrupedByHour.Rows.Count)
              {
                _db_trx.Commit();

                return true;
              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // Update_CasierSesionsGrupedByHour

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Alarms of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, Alarms
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    static public Boolean Update_CenterAlarms(Int32 SiteId, DataTable Alarms)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (Alarms.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" EXECUTE Update_SiteAlarms ");
        _sb.AppendLine("  @pSiteid ");
        _sb.AppendLine(" ,@pAlarmId ");
        _sb.AppendLine(" ,@pSourceCode ");
        _sb.AppendLine(" ,@pSourceId ");
        _sb.AppendLine(" ,@pSourceName ");
        _sb.AppendLine(" ,@pAlarmCode ");
        _sb.AppendLine(" ,@pAlarmName ");
        _sb.AppendLine(" ,@pAlarmDescription ");
        _sb.AppendLine(" ,@pSeverity ");
        _sb.AppendLine(" ,@pReported ");
        _sb.AppendLine(" ,@pDatetime ");
        _sb.AppendLine(" ,@pAckDatetime ");
        _sb.AppendLine(" ,@pAckUserId ");
        _sb.AppendLine(" ,@pAckUserName ");
        _sb.AppendLine(" ,@pTimestamp");
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).SourceColumn = "AL_ALARM_ID";
            _cmd.Parameters.Add("@pSourceCode", SqlDbType.Int).SourceColumn = "AL_SOURCE_CODE";
            _cmd.Parameters.Add("@pSourceId", SqlDbType.BigInt).SourceColumn = "AL_SOURCE_ID";
            _cmd.Parameters.Add("@pSourceName", SqlDbType.NVarChar).SourceColumn = "AL_SOURCE_NAME";
            _cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).SourceColumn = "AL_ALARM_CODE";
            _cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).SourceColumn = "AL_ALARM_NAME";
            _cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).SourceColumn = "AL_ALARM_DESCRIPTION";
            _cmd.Parameters.Add("@pSeverity", SqlDbType.Int).SourceColumn = "AL_SEVERITY";
            _cmd.Parameters.Add("@pReported", SqlDbType.DateTime).SourceColumn = "AL_REPORTED";
            _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).SourceColumn = "AL_DATETIME";
            _cmd.Parameters.Add("@pAckDatetime", SqlDbType.DateTime).SourceColumn = "AL_ACK_DATETIME";
            _cmd.Parameters.Add("@pAckUserId", SqlDbType.Int).SourceColumn = "AL_ACK_USER_ID";
            _cmd.Parameters.Add("@pAckUserName", SqlDbType.NVarChar).SourceColumn = "AL_ACK_USER_NAME";
            _cmd.Parameters.Add("@pTimestamp", SqlDbType.BigInt).SourceColumn = "AL_TIMESTAMP";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(Alarms) == Alarms.Rows.Count)
              {
                _db_trx.Commit();

                return true;
              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // Update_CenterAlarms

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Handpays of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, Handpays
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    static public Boolean Update_CenterHandpays(Int32 SiteId, DataTable Handpays)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (Handpays.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" EXECUTE Update_SiteHandpays ");
        _sb.AppendLine("  @pSiteId ");
        _sb.AppendLine(" ,@pHandpayId ");
        _sb.AppendLine(" ,@pTerminalId ");
        _sb.AppendLine(" ,@pGameBaseName ");
        _sb.AppendLine(" ,@pDatetime ");
        _sb.AppendLine(" ,@pPreviousMeters ");
        _sb.AppendLine(" ,@pAmount ");
        _sb.AppendLine(" ,@pTeName ");
        _sb.AppendLine(" ,@pTeProviderId ");
        _sb.AppendLine(" ,@pMovementId ");
        _sb.AppendLine(" ,@pType ");
        _sb.AppendLine(" ,@pPlaySessionId ");
        _sb.AppendLine(" ,@pSiteJackpotIndex ");
        _sb.AppendLine(" ,@pSiteJackpotName ");
        _sb.AppendLine(" ,@pSiteJackpotAwardedOnTerminalId ");
        _sb.AppendLine(" ,@pSiteJackpotAwardedToAccountId ");
        _sb.AppendLine(" ,@pStatus ");
        _sb.AppendLine(" ,@pSiteJackpotNotified ");
        _sb.AppendLine(" ,@pTicketId ");
        _sb.AppendLine(" ,@pTransactionId ");
        _sb.AppendLine(" ,@pCandidatePlaySessionId ");
        _sb.AppendLine(" ,@pCandidatePrevPlaySessionId ");
        _sb.AppendLine(" ,@pLongPoll1bData ");
        _sb.AppendLine(" ,@pProgressiveId ");
        _sb.AppendLine(" ,@pLevel ");
        _sb.AppendLine(" ,@pStatusChanged ");
        _sb.AppendLine(" ,@pTaxBaseAmount ");
        _sb.AppendLine(" ,@pTaxAmount ");
        _sb.AppendLine(" ,@pTaxPct ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pHandpayId", SqlDbType.BigInt).SourceColumn = "HP_ID";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "HP_TERMINAL_ID";
            _cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar).SourceColumn = "HP_GAME_BASE_NAME";
            _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).SourceColumn = "HP_DATETIME";
            _cmd.Parameters.Add("@pPreviousMeters", SqlDbType.DateTime).SourceColumn = "HP_PREVIOUS_METERS";
            _cmd.Parameters.Add("@pAmount", SqlDbType.Money).SourceColumn = "HP_AMOUNT";
            _cmd.Parameters.Add("@pTeName", SqlDbType.NVarChar).SourceColumn = "HP_TE_NAME";
            _cmd.Parameters.Add("@pTeProviderId", SqlDbType.NVarChar).SourceColumn = "HP_TE_PROVIDER_ID";
            _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).SourceColumn = "HP_MOVEMENT_ID";
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "HP_TYPE";
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "HP_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pSiteJackpotIndex", SqlDbType.Int).SourceColumn = "HP_SITE_JACKPOT_INDEX";
            _cmd.Parameters.Add("@pSiteJackpotName", SqlDbType.NVarChar).SourceColumn = "HP_SITE_JACKPOT_NAME";
            _cmd.Parameters.Add("@pSiteJackpotAwardedOnTerminalId", SqlDbType.Int).SourceColumn = "HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID";
            _cmd.Parameters.Add("@pSiteJackpotAwardedToAccountId", SqlDbType.BigInt).SourceColumn = "HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "HP_STATUS";
            _cmd.Parameters.Add("@pSiteJackpotNotified", SqlDbType.Bit).SourceColumn = "HP_SITE_JACKPOT_NOTIFIED";
            _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).SourceColumn = "HP_TICKET_ID";
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "HP_TRANSACTION_ID";
            _cmd.Parameters.Add("@pCandidatePlaySessionId", SqlDbType.BigInt).SourceColumn = "HP_CANDIDATE_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pCandidatePrevPlaySessionId", SqlDbType.BigInt).SourceColumn = "HP_CANDIDATE_PREV_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pLongPoll1bData", SqlDbType.Xml).SourceColumn = "HP_LONG_POLL_1B_DATA";
            _cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).SourceColumn = "HP_PROGRESSIVE_ID";
            _cmd.Parameters.Add("@pLevel", SqlDbType.Int).SourceColumn = "HP_LEVEL";
            _cmd.Parameters.Add("@pStatusChanged", SqlDbType.DateTime).SourceColumn = "HP_STATUS_CHANGED";
            _cmd.Parameters.Add("@pTaxBaseAmount", SqlDbType.Money).SourceColumn = "HP_TAX_BASE_AMOUNT";
            _cmd.Parameters.Add("@pTaxAmount", SqlDbType.Money).SourceColumn = "HP_TAX_AMOUNT";
            _cmd.Parameters.Add("@pTaxPct", SqlDbType.Decimal).SourceColumn = "HP_TAX_PCT";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(Handpays) == Handpays.Rows.Count)
              {
                _db_trx.Commit();

                return true;
              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // Update_CenterHandpays

    //------------------------------------------------------------------------------
    // PURPOSE : Update Center Creditlines of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, Creditlines
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    static public Boolean Update_CenterCreditlines(Int32 SiteId, DataTable Creditlines)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (Creditlines.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" EXECUTE Update_Creditlines ");
        _sb.AppendLine("   @pSiteId ");
        _sb.AppendLine("  ,@pId ");
        _sb.AppendLine("  ,@pAccountId ");
        _sb.AppendLine("  ,@pLimitAmount ");
        _sb.AppendLine("  ,@pTTOAmount ");
        _sb.AppendLine("  ,@pSpentAmount ");
        _sb.AppendLine("  ,@pISOCode ");
        _sb.AppendLine("  ,@pStatus ");
        _sb.AppendLine("  ,@pIBAN ");
        _sb.AppendLine("  ,@pXMLSigners ");
        _sb.AppendLine("  ,@pCreation ");
        _sb.AppendLine("  ,@pUpdate ");
        _sb.AppendLine("  ,@pExpired ");
        _sb.AppendLine("  ,@pLastPayback ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = "CL_ID";
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "CL_ACCOUNT_ID";
            _cmd.Parameters.Add("@pLimitAmount", SqlDbType.Money).SourceColumn = "CL_LIMIT_AMOUNT";
            _cmd.Parameters.Add("@pTTOAmount", SqlDbType.Money).SourceColumn = "CL_TTO_AMOUNT";
            _cmd.Parameters.Add("@pSpentAmount", SqlDbType.Money).SourceColumn = "CL_SPENT_AMOUNT";
            _cmd.Parameters.Add("@pISOCode", SqlDbType.NVarChar).SourceColumn = "CL_ISO_CODE";
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "CL_STATUS";
            _cmd.Parameters.Add("@pIBAN", SqlDbType.NVarChar).SourceColumn = "CL_IBAN";
            _cmd.Parameters.Add("@pXMLSigners", SqlDbType.Xml).SourceColumn = "CL_XML_SIGNERS";
            _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).SourceColumn = "CL_CREATION";
            _cmd.Parameters.Add("@pUpdate", SqlDbType.DateTime).SourceColumn = "CL_UPDATE";
            _cmd.Parameters.Add("@pExpired", SqlDbType.DateTime).SourceColumn = "CL_EXPIRED";
            _cmd.Parameters.Add("@pLastPayback", SqlDbType.DateTime).SourceColumn = "CL_LAST_PAYBACK";


            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(Creditlines) == Creditlines.Rows.Count)
              {
                _db_trx.Commit();

                return true;
              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // Update_CreditLines
    //------------------------------------------------------------------------------
    // PURPOSE : Update Center Creditline Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :  SiteId, CreditlineMovements
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //
    static public Boolean Update_CenterCreditlineMovements(Int32 SiteId, DataTable CreditlineMovements)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        if (CreditlineMovements.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" EXECUTE Update_CreditlineMovements ");
        _sb.AppendLine("   @pSiteId ");
        _sb.AppendLine("  ,@pId ");
        _sb.AppendLine("  ,@pCreditlineId ");
        _sb.AppendLine("  ,@pOperationId ");
        _sb.AppendLine("  ,@pType ");
        _sb.AppendLine("  ,@pOldvalue ");
        _sb.AppendLine("  ,@pNewValue ");
        _sb.AppendLine("  ,@pCreation ");
        _sb.AppendLine("  ,@pCreationUser ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = "CLM_ID";
            _cmd.Parameters.Add("@pCreditlineId", SqlDbType.BigInt).SourceColumn = "CLM_CREDIT_LINE_ID";
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "CLM_OPERATION_ID";
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CLM_TYPE";
            _cmd.Parameters.Add("@pOldvalue", SqlDbType.Xml).SourceColumn = "CLM_OLD_VALUE";
            _cmd.Parameters.Add("@pNewValue", SqlDbType.Xml).SourceColumn = "CLM_NEW_VALUE";
            _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).SourceColumn = "CLM_CREATION";
            _cmd.Parameters.Add("@pCreationUser", SqlDbType.NVarChar).SourceColumn = "CLM_CREATION_USER";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_adapt.UpdateBatchSize = 500;
              if (_sql_adapt.Update(CreditlineMovements) == CreditlineMovements.Rows.Count)
              {
                _db_trx.Commit();

                return true;
              }
              else
              {
                _db_trx.Rollback();
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // Update_CreditLineMovements
  }
}
