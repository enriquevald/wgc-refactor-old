//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   MultiSite_CenterGenericDownload.cs
// DESCRIPTION:   
// AUTHOR:        Jos� Mart�nez L�pez 
// CREATION DATE: 20-JUN-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 20-JUN-2013  JML    Initial version
// 20-JUN-2013  JML    Read_Providers move from delete file MultiSite_CenterProviders.cs
// 20-JUN-2013  JML    Read_ProvidersGames move from delete file MultiSite_CenterProvidersGames.cs
// 20-JUN-2013  JML    Add Read_MastersProfiles
// 11-FEB-2014  RCI    iStats permission (104) include to download
// 05-DIC-2014  DCS    Added Task Download Lcd Messages
// 24-APR-2015  MPO    TFS-1258: Improve performance for timestamp tasks 
// 18-MAY-2015  JML    Multisite-Multicurrency permisions
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;
using WSI.WWP;

namespace WSI.WWP_CenterService.MultiSite
{
  class MultiSite_CenterGenericDownload
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Providers request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: Providers
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_Providers(Int32 CallingSiteId, Int64 SequenceValue, out DataTable Providers)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      Providers = new DataTable("PROVIDERS");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadProviders, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   TOP (@pMAXRows) ");
          _sb.AppendLine("            PV_ID ");
          _sb.AppendLine("          , PV_NAME ");
          _sb.AppendLine("          , PV_HIDE ");
          _sb.AppendLine("          , PV_POINTS_MULTIPLIER ");
          _sb.AppendLine("          , PV_3GS ");
          _sb.AppendLine("          , PV_3GS_VENDOR_ID ");
          _sb.AppendLine("          , PV_3GS_VENDOR_IP ");
          _sb.AppendLine("          , PV_SITE_JACKPOT ");
          _sb.AppendLine("          , PV_ONLY_REDEEMABLE ");
          _sb.AppendLine("          , PV_MS_SEQUENCE_ID ");
          _sb.AppendLine("     FROM   PROVIDERS ");
          _sb.AppendLine("    WHERE   PV_SITE_ID = @pSiteId");
          _sb.AppendLine("      AND   PV_MS_SEQUENCE_ID > @pSeqId");
          _sb.AppendLine(" ORDER BY   PV_MS_SEQUENCE_ID ASC, PV_ID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            // If mode 0 the value of site id will be 0
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = 0;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, Providers);
            }

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadProviders, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_Providers

    //------------------------------------------------------------------------------
    // PURPOSE: Select Providers Games data from multisite - center.
    //
    //  PARAMS:
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: Providers Games
    //
    // RETURNS:
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    public static Boolean Read_ProvidersGames(Int32 CallingSiteId, Int64 SequenceValue, out DataTable ProvidersGames)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      ProvidersGames = new DataTable("GAMES");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   TOP (@pMAXRows)  ");
          _sb.AppendLine("         GM_SITE_ID       ");
          _sb.AppendLine("       , GM_PV_ID         ");
          _sb.AppendLine("       , GM_GAME_ID       ");
          _sb.AppendLine("       , GM_GAME_NAME     ");
          _sb.AppendLine("       , GM_PAYOUT_1      ");
          _sb.AppendLine("       , GM_PAYOUT_2      ");
          _sb.AppendLine("       , GM_PAYOUT_3      ");
          _sb.AppendLine("       , GM_PAYOUT_4      ");
          _sb.AppendLine("       , GM_PAYOUT_5      ");
          _sb.AppendLine("       , GM_PAYOUT_6      ");
          _sb.AppendLine("       , GM_PAYOUT_7      ");
          _sb.AppendLine("       , GM_PAYOUT_8      ");
          _sb.AppendLine("       , GM_PAYOUT_9      ");
          _sb.AppendLine("       , GM_PAYOUT_10     ");
          _sb.AppendLine("       , GM_MS_SEQUENCE_ID");
          _sb.AppendLine("  FROM   GAMES  ");
          _sb.AppendLine(" WHERE   GM_SITE_ID = @pSiteId");
          _sb.AppendLine("   AND   GM_MS_SEQUENCE_ID > @pSeqId");
          _sb.AppendLine(" ORDER   BY GM_MS_SEQUENCE_ID ASC, GM_PV_ID, GM_GAME_ID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            // If mode 0 the value of site id will be 0
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = 0;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, ProvidersGames);
            }

            // TODO � Es necesario actualizar la secuencia en el centro ?
            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_ProvidersGames

    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Masters Profiles request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: MastersProfiles
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_MastersProfiles(Int32 CallingSiteId, Int64 SequenceValue, out DataSet MastersProfiles)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      MastersProfiles = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();


          _sb.AppendLine("DECLARE @GupId AS BIGINT ");

          _sb.AppendLine("   SELECT   TOP(1) @GupId = GUP_PROFILE_ID ");
          _sb.AppendLine("     FROM   GUI_USER_PROFILES ");
          _sb.AppendLine("     LEFT   JOIN GUI_PROFILE_FORMS ");
          _sb.AppendLine("       ON   GPF_PROFILE_ID = GUP_PROFILE_ID ");
          _sb.AppendLine("      AND   GPF_GUI_ID IN (14, 15, 104) ");
          _sb.AppendLine("      AND   ( GPF_READ_PERM IS NULL ");
          _sb.AppendLine("            OR (  GPF_READ_PERM = 1 ");
          _sb.AppendLine("               OR GPF_WRITE_PERM = 1 ");
          _sb.AppendLine("               OR GPF_DELETE_PERM = 1 ");
          _sb.AppendLine("               OR GPF_EXECUTE_PERM = 1 ) )  ");
          _sb.AppendLine("     LEFT   JOIN GUI_FORMS ");
          _sb.AppendLine("       ON   GF_GUI_ID = GPF_GUI_ID ");
          _sb.AppendLine("      AND   GF_FORM_ID = GPF_FORM_ID ");
          _sb.AppendLine("    WHERE   GUP_TIMESTAMP > CONVERT(BINARY(8),@pSeqId) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
          _sb.AppendLine("      AND   ISNULL(GUP_MASTER_ID, 0) > 0 ");
          _sb.AppendLine(" ORDER BY   GUP_TIMESTAMP ASC, GUP_PROFILE_ID ");

          _sb.AppendLine("   SELECT   GUP_NAME ");
          _sb.AppendLine("          , GUP_MAX_USERS ");
          _sb.AppendLine("          , cast(GUP_TIMESTAMP as BIGINT ) AS GUP_TIMESTAMP ");
          _sb.AppendLine("          , GUP_MASTER_ID ");
          _sb.AppendLine("          , GPF_GUI_ID ");
          _sb.AppendLine("          , GPF_FORM_ID ");
          _sb.AppendLine("          , GPF_READ_PERM ");
          _sb.AppendLine("          , GPF_WRITE_PERM ");
          _sb.AppendLine("          , GPF_DELETE_PERM ");
          _sb.AppendLine("          , GPF_EXECUTE_PERM ");
          _sb.AppendLine("          , GF_FORM_ORDER ");
          _sb.AppendLine("          , GF_NLS_ID ");
          _sb.AppendLine("     FROM   GUI_USER_PROFILES ");
          _sb.AppendLine("     LEFT   JOIN GUI_PROFILE_FORMS ");
          _sb.AppendLine("       ON   GPF_PROFILE_ID = GUP_PROFILE_ID ");
          _sb.AppendLine("      AND   GPF_GUI_ID IN (14, 15, 104) ");
          _sb.AppendLine("      AND   ( GPF_READ_PERM IS NULL ");
          _sb.AppendLine("            OR (  GPF_READ_PERM = 1 ");
          _sb.AppendLine("               OR GPF_WRITE_PERM = 1 ");
          _sb.AppendLine("               OR GPF_DELETE_PERM = 1 ");
          _sb.AppendLine("               OR GPF_EXECUTE_PERM = 1 ) )  ");
          _sb.AppendLine("     LEFT   JOIN GUI_FORMS ");
          _sb.AppendLine("       ON   GF_GUI_ID = GPF_GUI_ID ");
          _sb.AppendLine("      AND   GF_FORM_ID = GPF_FORM_ID ");
          _sb.AppendLine("    WHERE   GUP_PROFILE_ID = @GupId ");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // no pueden ir top(rows) porque podriamos dividir un perfil en dos grupos
            // _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(MastersProfiles);
            }

            MastersProfiles.Tables[0].TableName = "MASTERS_PROFILES";

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_MastersProfiles

    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Corporate Users request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: MastersUsers
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_CorporateUsers(Int32 CallingSiteId, Int64 SequenceValue, out DataSet MastersUsers)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      MastersUsers = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("   SELECT   TOP (@pMAXRows)  ");
          _sb.AppendLine("            GUP_MASTER_ID         ");
          _sb.AppendLine("          , GU_USERNAME           ");
          _sb.AppendLine("          , GU_ENABLED            ");
          _sb.AppendLine("          , GU_PASSWORD           ");
          _sb.AppendLine("          , GU_NOT_VALID_BEFORE   ");
          _sb.AppendLine("          , GU_NOT_VALID_AFTER    ");
          _sb.AppendLine("          , GU_LAST_CHANGED       ");
          _sb.AppendLine("          , GU_PASSWORD_EXP       ");
          _sb.AppendLine("          , GU_PWD_CHG_REQ        ");
          _sb.AppendLine("          , GU_LOGIN_FAILURES     ");
          _sb.AppendLine("          , GU_FULL_NAME          ");
          _sb.AppendLine("          , CAST(GU_TIMESTAMP AS BIGINT ) AS GU_TIMESTAMP ");
          _sb.AppendLine("          , GU_USER_TYPE          ");
          if (CurrencyMultisite.IsCenterAndMultiCurrency())
          {
            _sb.AppendLine("          , NULL AS GU_SALES_LIMIT        ");
            _sb.AppendLine("          , NULL AS GU_MB_SALES_LIMIT     ");
          }
          else
          {
            // is only center.. 
            _sb.AppendLine("          , GU_SALES_LIMIT        ");
            _sb.AppendLine("          , GU_MB_SALES_LIMIT     ");
          }
          _sb.AppendLine("          , GU_BLOCK_REASON       ");
          _sb.AppendLine("          , GU_MASTER_ID          ");
          _sb.AppendLine("          , GU_EMPLOYEE_CODE      ");
          _sb.AppendLine("     FROM   GUI_USERS             ");
          _sb.AppendLine("    INNER   JOIN GUI_USER_PROFILES ON GUP_PROFILE_ID = GU_PROFILE_ID ");
          _sb.AppendLine("    WHERE   GU_TIMESTAMP > CONVERT(BINARY(8),@pSeqId) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
          _sb.AppendLine("      AND   ISNULL(GU_MASTER_ID, 0) > 0 ");
          _sb.AppendLine(" ORDER BY   GU_TIMESTAMP ASC, GU_MASTER_ID ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(MastersUsers);
            }

            MastersUsers.Tables[0].TableName = "CORPORATE_USERS";

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_CorporateUsers


    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Account Documents request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: Account Documents
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_AccountDocuments(Int32 CallingSiteId, Int64 SequenceValue, out DataTable AccountDocuments)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      AccountDocuments = new DataTable("ACCOUNT_DOCUMENTS");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT   TOP (@pMAXRows) ");
          _sb.AppendLine("            AD_ACCOUNT_ID ");
          _sb.AppendLine("          , AD_CREATED ");
          _sb.AppendLine("          , AD_MODIFIED ");
          _sb.AppendLine("          , AD_DATA ");
          _sb.AppendLine("          , AD_MS_SEQUENCE_ID ");
          _sb.AppendLine("     FROM   ACCOUNT_DOCUMENTS ");
          _sb.AppendLine("    WHERE   AD_MS_SEQUENCE_ID > @pSeqId");
          _sb.AppendLine(" ORDER BY   AD_MS_SEQUENCE_ID ASC, AD_ACCOUNT_ID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, AccountDocuments);
            }

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_AccountDocuments

    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Alarm Patterns request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: Patterns
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_AlarmPatterns(Int32 CallingSiteId, Int64 SequenceValue, out DataSet Patterns)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      Patterns = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_AlarmPatterns: Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("  SELECT   TOP (@pMAXRows) ");
          _sb.AppendLine("           PT_ID ");
          _sb.AppendLine("         , PT_NAME ");
          _sb.AppendLine("         , PT_DESCRIPTION ");
          _sb.AppendLine("         , PT_ACTIVE ");
          _sb.AppendLine("         , PT_PATTERN ");
          _sb.AppendLine("         , PT_AL_CODE ");
          _sb.AppendLine("         , PT_AL_NAME ");
          _sb.AppendLine("         , PT_AL_DESCRIPTION ");
          _sb.AppendLine("         , PT_AL_SEVERITY ");
          _sb.AppendLine("         , PT_TYPE ");
          _sb.AppendLine("         , PT_SOURCE ");
          _sb.AppendLine("         , PT_LIFE_TIME ");
          _sb.AppendLine("         , PT_LAST_FIND ");
          _sb.AppendLine("         , PT_DETECTIONS ");
          _sb.AppendLine("         , PT_SCHEDULE_TIME_FROM ");
          _sb.AppendLine("         , PT_SCHEDULE_TIME_TO ");
          _sb.AppendLine("         , CAST(PT_TIMESTAMP AS BIGINT) AS PT_TIMESTAMP ");
          _sb.AppendLine("    FROM   PATTERNS ");
          _sb.AppendLine("   WHERE   PT_TIMESTAMP > CONVERT(BINARY(8),@pSeqId) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
          _sb.AppendLine("ORDER BY   PT_TIMESTAMP ASC, PT_ID ");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(Patterns);
            }

            Patterns.Tables[0].TableName = "PATTERNS";

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_AlarmPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Lcd Messages request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: LcdMessages
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_LcdMessages(Int32 CallingSiteId, Int64 SequenceValue, out DataSet LcdMessages)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      LcdMessages = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. ReadLcdMessages: Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("  SELECT   TOP (@pMAXRows)                                ");
          _sb.AppendLine("           MSG_UNIQUE_ID                                  ");
          _sb.AppendLine("         , MSG_TYPE                                       ");
          _sb.AppendLine("         , MSG_SITE_LIST                                  ");
          _sb.AppendLine("         , MSG_TERMINAL_LIST                              ");
          _sb.AppendLine("         , MSG_ACCOUNT_LIST                               ");
          _sb.AppendLine("         , MSG_ENABLED                                    ");
          _sb.AppendLine("         , MSG_ORDER                                      ");
          _sb.AppendLine("         , MSG_SCHEDULE_START                             ");
          _sb.AppendLine("         , MSG_SCHEDULE_END                               ");
          _sb.AppendLine("         , MSG_SCHEDULE_WEEKDAY                           ");
          _sb.AppendLine("         , MSG_SCHEDULE1_TIME_FROM                        ");
          _sb.AppendLine("         , MSG_SCHEDULE1_TIME_TO                          ");
          _sb.AppendLine("         , MSG_SCHEDULE2_ENABLED                          ");
          _sb.AppendLine("         , MSG_SCHEDULE2_TIME_FROM                        ");
          _sb.AppendLine("         , MSG_SCHEDULE2_TIME_TO                          ");
          _sb.AppendLine("         , MSG_MESSAGE                                    ");
          _sb.AppendLine("         , MSG_RESOURCE_ID                                ");
          _sb.AppendLine("         , CAST(MSG_TIMESTAMP AS BIGINT) AS MSG_TIMESTAMP ");
          _sb.AppendLine("    FROM   LCD_MESSAGES                                   ");
          _sb.AppendLine("   WHERE   MSG_TIMESTAMP > CONVERT(BINARY(8),@pSeqId) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
          _sb.AppendLine("ORDER BY   MSG_TIMESTAMP ASC, MSG_UNIQUE_ID               ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(LcdMessages);
            }

            LcdMessages.Tables[0].TableName = "LCD_MESSAGES";

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages, SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_LcdMessages


    //------------------------------------------------------------------------------
    // PURPOSE : Read Center Buckets Configuration request by Site.
    //
    //  PARAMS :
    //      - INPUT: SiteId
    //               SeqId
    //          
    //      - OUTPUT: Buckets Configuration
    //
    // RETURNS :
    //      - True select.count > 0
    //      - False otherwise.
    //------------------------------------------------------------------------------
    static public Boolean Read_BucketsConfig(Int32 CallingSiteId, Int64 SequenceValue, out DataSet _bucketsConfig)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      _bucketsConfig = new DataSet();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. ReadLcdMessages: Read_CenterMaxRowsOfTask.");

            return false;
          }

          if (CallingSiteId < 0)
          {
            CallingSiteId = 0;
          }

          _sb = new StringBuilder();

          _sb.AppendLine("     SELECT    TOP (@pMAXRows)                                 ");
          _sb.AppendLine("               BU_BUCKET_ID                                    ");
          _sb.AppendLine("       INTO                                                    ");
          _sb.AppendLine("               #TEMP_BUCKETS                                   ");
          _sb.AppendLine("       FROM                                                    ");
          _sb.AppendLine("               BUCKETS                                         ");
          _sb.AppendLine("      WHERE                                                    ");
          _sb.AppendLine("               BU_TIMESTAMP > CONVERT(BINARY(8),@pSeqId)       ");
          _sb.AppendLine("                                                               ");
          _sb.AppendLine("	--first table, modified records from buckets                 ");
          _sb.AppendLine("     SELECT                                                    ");
          _sb.AppendLine("               BUCKETS.BU_BUCKET_ID                            ");
          _sb.AppendLine("             , BU_NAME                                         ");
          _sb.AppendLine("             , BU_ENABLED                                      ");
          _sb.AppendLine("             , BU_SYSTEM_TYPE                                  ");
          _sb.AppendLine("             , BU_BUCKET_TYPE                                  ");
          _sb.AppendLine("             , BU_VISIBLE_FLAGS                                ");
          _sb.AppendLine("             , BU_ORDER_ON_REPORTS                             ");
          _sb.AppendLine("             , BU_EXPIRATION_DAYS                              ");
          _sb.AppendLine("             , BU_EXPIRATION_DATE                              ");
          _sb.AppendLine("             , BU_LEVEL_FLAGS                                  ");
          _sb.AppendLine("             , BU_K_FACTOR                                     ");
          _sb.AppendLine("             , CAST(BU_TIMESTAMP AS BIGINT) AS BU_TIMESTAMP    ");
          _sb.AppendLine("      FROM                                                     ");
          _sb.AppendLine("	          BUCKETS                                            ");
          _sb.AppendLine("INNER JOIN                                                     ");
          _sb.AppendLine("              #TEMP_BUCKETS TB                                 ");
          _sb.AppendLine("			  ON TB.BU_BUCKET_ID = BUCKETS.BU_BUCKET_ID              ");
          _sb.AppendLine("  ORDER BY    BUCKETS.BU_TIMESTAMP ASC, BUCKETS.BU_BUCKET_ID   ");
          _sb.AppendLine("                                                               ");
          _sb.AppendLine("                                                               ");
          _sb.AppendLine("	-- second table, all the levels from previous buckets        ");
          _sb.AppendLine("    SELECT    BUCKET_LEVELS.BUL_BUCKET_ID                      ");
          _sb.AppendLine("            , BUL_LEVEL_ID                                     ");
          _sb.AppendLine("            , BUL_A_FACTOR                                     ");
          _sb.AppendLine("            , BUL_B_FACTOR                                     ");
          _sb.AppendLine("      FROM                                                     ");
          _sb.AppendLine("              BUCKET_LEVELS                                    ");
          _sb.AppendLine("INNER JOIN                                                     ");
          _sb.AppendLine("              #TEMP_BUCKETS TB                                 ");
          _sb.AppendLine("              ON TB.BU_BUCKET_ID = BUCKET_LEVELS.BUL_BUCKET_ID ");
          _sb.AppendLine("  ORDER BY    BUCKET_LEVELS.BUL_BUCKET_ID                      ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceValue;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_bucketsConfig);
            }

            _bucketsConfig.Tables[0].TableName = "BUCKETS_CONFIG";
            _bucketsConfig.Tables[1].TableName = "BUCKETS_LEVELS_CONFIG";

            WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig , SequenceValue, _db_trx.SqlTransaction);

            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_BucketsConfig



  }
}
