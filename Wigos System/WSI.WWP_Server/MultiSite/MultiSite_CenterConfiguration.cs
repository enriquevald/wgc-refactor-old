//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_CenterTask.cs
// 
//   DESCRIPTION: Processes of the tasks
// 
//        AUTHOR: Dani Dom�nguez Mart�n
// 
// CREATION DATE: 27-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-MAR-2013 DDM    First release.
// 04-AUG-2014 JBC/MPO    WIG-1131: Download for enable/disable document types
// 05-DIC-2014 DCS    Added Task Download Lcd Messages
// 14-JAN-2015 DCS    Added Task Upload Handpays
// 28-APR-2015 DCS    Multisite Multicurrency -> Update currencies configuration
// 18-MAY-2015 JML    Multisite-Multicurrency permisions
// 14-DEC-2015 FOS    Backlog Item 1858: MultiSite MultiCurrency:Accounts Information
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
// 03-FEB-2016 JML    Bug 8943*: MultiSite MultiCurrency:Accounts Information: Identification types
// 27-JUL-2016 FAV    Fixed Bug 16029: IdentificationTypes in MultiSite doesn't insert new records
// 31-MAR-2017 MS     WIGOS-411 Creditlines
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.WWP_CenterService.MultiSite
{
  class MultiSite_CenterConfiguration
  {
    public static Boolean Update_Configuration(Int32 SiteId, DataSet DSConfiration, out DataSet Reply)
    {
      StringBuilder _sb;
      DataRow[] _tasks;
      String _sql;
      Int32 _num_rows_updated;

      

      // The task DownloadConfiguration don't update pending & sequence
      _tasks = DSConfiration.Tables["TASKS"].Select("ST_TASK_ID <> " + TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration.GetHashCode(), "ST_TASK_ID");
      Reply = new DataSet();

      try
      {
        _sb = new StringBuilder();

        //
        // SEQUENCE IN SITE
        //
        _sb.AppendLine("IF @pTaskId IN (@pUploadPointsMovements,");
        _sb.AppendLine("                @pUploadPersonalInfo, ");
        _sb.AppendLine("                @pDownloadPersonalInfo_Card,");
        _sb.AppendLine("                @pUploadProvidersGames,");
        _sb.AppendLine("                @pUploadGamePlaySessions, ");
        _sb.AppendLine("                @pUploadAccountDocuments, ");
        _sb.AppendLine("                @pUploadLastActivity, ");
        _sb.AppendLine("                @pUploadSellsAndBuys)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("     SET   ST_NUM_PENDING = @pNumPending ");
        _sb.AppendLine("   WHERE   ST_SITE_ID     = @pSiteId     ");
        _sb.AppendLine("     AND   ST_TASK_ID     = @pTaskId     ");
        _sb.AppendLine("END;");

        //
        // SEQUENCE IN MULTISITE
        //
        _sb.AppendLine("ELSE IF @pTaskId IN (@pDownloadPersonalInfo_All,");
        _sb.AppendLine("                     @pDownloadPoints_All,");
        _sb.AppendLine("                     @pDownloadProvidersGames,");
        _sb.AppendLine("                     @pDownloadProviders,");
        _sb.AppendLine("                     @pDownloadAccountDocuments)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  DECLARE @pCenterSequenceId as BIGINT ");
        _sb.AppendLine("");
        _sb.AppendLine("  SET @pCenterSequenceId = ( SELECT   SEQ_NEXT_VALUE - 1 FROM SEQUENCES ");
        _sb.AppendLine("                              WHERE   SEQ_ID = CASE WHEN (@pTaskId = @pDownloadPersonalInfo_All) THEN @pSeqTrackAccountChanges");
        _sb.AppendLine("                                                    WHEN (@pTaskId = @pDownloadPoints_All)       THEN @pSeqTrackPointChanges  ");
        _sb.AppendLine("                                                    WHEN (@pTaskId = @pDownloadProviders)        THEN @pSeqProviders  ");
        _sb.AppendLine("                                                    WHEN (@pTaskId = @pDownloadProvidersGames)   THEN @pSeqProvidersGames  ");
        _sb.AppendLine("                                                    WHEN (@pTaskId = @pDownloadAccountDocuments) THEN @pSeqAccountDocuments  ");
        _sb.AppendLine("                                                    ELSE  NULL ");
        _sb.AppendLine("                                                    END )  ");
        _sb.AppendLine("  IF @pCenterSequenceId IS NOT NULL ");
        _sb.AppendLine("  BEGIN ");
        _sb.AppendLine("    UPDATE   MS_SITE_TASKS  ");
        _sb.AppendLine("       SET   ST_REMOTE_SEQUENCE_ID  = @pLocalSiteSequenceIdFromSite  ");
        _sb.AppendLine("           , ST_NUM_PENDING         = CASE WHEN (@pCenterSequenceId - @pLocalSiteSequenceIdFromSite) < 0 THEN NULL");
        _sb.AppendLine("                                           ELSE  @pCenterSequenceId - @pLocalSiteSequenceIdFromSite   ");
        _sb.AppendLine("                                           END  ");
        _sb.AppendLine("           , ST_FORCE_RESYNCH       = CASE WHEN (@pLocalSiteSequenceIdFromSite = 0)                      THEN 0   ");
        _sb.AppendLine("                                           WHEN (@pCenterSequenceId - @pLocalSiteSequenceIdFromSite) < 0 THEN 1   ");
        _sb.AppendLine("                                           ELSE ST_FORCE_RESYNCH ");
        _sb.AppendLine("                                           END  ");
        _sb.AppendLine("     WHERE   ST_SITE_ID = @pSiteId  ");
        _sb.AppendLine("       AND   ST_TASK_ID = @pTaskId  ");
        _sb.AppendLine("  END  ");
        _sb.AppendLine("END;");

        //
        // TIMESTAMP IN SITE
        //
        _sb.AppendLine("ELSE IF @pTaskId IN (@pUploadSalesPerHour,");
        _sb.AppendLine("                     @pUploadPlaySessions,");
        _sb.AppendLine("                     @pUploadProviders, ");
        _sb.AppendLine("                     @pUploadBanks, ");
        _sb.AppendLine("                     @pUploadAreas, ");
        _sb.AppendLine("                     @pUploadTerminals, ");
        _sb.AppendLine("                     @pUploadTerminalsConnected, ");
        _sb.AppendLine("                     @pUploadCashierMovementsGrupedByHour, ");
        _sb.AppendLine("                     @pUploadAlarms, ");
        _sb.AppendLine("                     @pUploadHandpays, ");
        _sb.AppendLine("                     @pUploadCreditLines, ");
        _sb.AppendLine("                     @pUploadCreditLineMovements) ");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("     SET   ST_NUM_PENDING        = @pNumPending ");
        //if pRemoteSequenceIdFromSite = 0 is reset from site
        _sb.AppendLine("         , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pRemoteSequenceIdFromSite = 0) THEN 0 ELSE ST_LOCAL_SEQUENCE_ID END");
        _sb.AppendLine("         , ST_REMOTE_SEQUENCE_ID = CASE WHEN (@pRemoteSequenceIdFromSite = 0) THEN 0 ELSE @pLocalSiteSequenceIdFromSite END");
        _sb.AppendLine("   WHERE   ST_SITE_ID     = @pSiteId     ");
        _sb.AppendLine("     AND   ST_TASK_ID     = @pTaskId     ");
        _sb.AppendLine("END;");

        //
        // TIMESTAMP IN MULTISITE
        // 

        //// TODO: Hacerlo generico para m�s de una tabla ( usuarios corporativos)
        _sb.AppendLine("ELSE IF @pTaskId IN (@pDownloadMasterProfiles,@pDownloadCorporateUsers,@pDownloadAlarmPatterns,@pDownloadLcdMessages,@pDownloadBucketsConfig)");
        _sb.AppendLine("  BEGIN");
        _sb.AppendLine("   DECLARE @MaxSeq AS BIGINT ");
        _sb.AppendLine("   DECLARE @Count AS INT");

        _sb.AppendLine("   IF @pTaskid = @pDownloadMasterProfiles  ");
        _sb.AppendLine("    BEGIN                                  ");
        _sb.AppendLine("      SELECT   @MaxSeq = MAX(ISNULL(GUP_TIMESTAMP,0))  ");
        _sb.AppendLine("             , @Count  = SUM(CASE WHEN GUP_TIMESTAMP > ISNULL(@pLocalSiteSequenceIdFromSite , 0)  THEN 1  ");
        _sb.AppendLine("                                   WHEN (ST_FORCE_RESYNCH = 1)  THEN 1  ");
        _sb.AppendLine("                                   ELSE 0 END) ");
        _sb.AppendLine("        FROM   GUI_USER_PROFILES  ");
        _sb.AppendLine("             , MS_SITE_TASKS ");
        _sb.AppendLine("       WHERE   GUP_TIMESTAMP >= CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE ISNULL(@pLocalSiteSequenceIdFromSite, 0) END ");
        _sb.AppendLine("         AND   ST_TASK_ID = @pDownloadMasterProfiles ");
        _sb.AppendLine("         AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("         AND   GUP_MASTER_ID IS NOT NULL ");
        _sb.AppendLine("    END");
        _sb.AppendLine("   ELSE IF @pTaskId = @pDownloadCorporateUsers");
        _sb.AppendLine("     BEGIN");
        _sb.AppendLine("      SELECT   @MaxSeq = MAX(ISNULL(GU_TIMESTAMP,0))  ");
        _sb.AppendLine("             , @Count  = SUM(CASE WHEN GU_TIMESTAMP > ISNULL(@pLocalSiteSequenceIdFromSite , 0)  THEN 1  ");
        _sb.AppendLine("                               WHEN (ST_FORCE_RESYNCH = 1)  THEN 1  ");
        _sb.AppendLine("                               ELSE 0 END) ");
        _sb.AppendLine("        FROM   GUI_USERS  ");
        _sb.AppendLine("             , MS_SITE_TASKS ");
        _sb.AppendLine("       WHERE   GU_TIMESTAMP >= CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE ISNULL(@pLocalSiteSequenceIdFromSite, 0) END ");
        _sb.AppendLine("         AND   ST_TASK_ID = @pDownloadCorporateUsers ");
        _sb.AppendLine("         AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("         AND   GU_MASTER_ID IS NOT NULL ");
        _sb.AppendLine("     END");
        _sb.AppendLine("   ELSE IF @pTaskId = @pDownloadAlarmPatterns");
        _sb.AppendLine("     BEGIN");
        _sb.AppendLine("      SELECT   @MaxSeq = MAX(ISNULL(PT_TIMESTAMP,0))  ");
        _sb.AppendLine("             , @Count  = SUM(CASE WHEN PT_TIMESTAMP > ISNULL(@pLocalSiteSequenceIdFromSite , 0)  THEN 1  ");
        _sb.AppendLine("                               WHEN (ST_FORCE_RESYNCH = 1)  THEN 1  ");
        _sb.AppendLine("                               ELSE 0 END) ");
        _sb.AppendLine("        FROM   PATTERNS  ");
        _sb.AppendLine("             , MS_SITE_TASKS ");
        _sb.AppendLine("       WHERE   PT_TIMESTAMP >= CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE ISNULL(@pLocalSiteSequenceIdFromSite, 0) END ");
        _sb.AppendLine("         AND   ST_TASK_ID = @pDownloadAlarmPatterns ");
        _sb.AppendLine("         AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("         AND   PT_ID IS NOT NULL ");
        _sb.AppendLine("     END");
        _sb.AppendLine("   ELSE IF @pTaskId = @pDownloadLcdMessages");
        _sb.AppendLine("     BEGIN");
        _sb.AppendLine("      SELECT   @MaxSeq = MAX(ISNULL(MSG_TIMESTAMP,0))  ");
        _sb.AppendLine("             , @Count  = SUM(CASE WHEN MSG_TIMESTAMP > ISNULL(@pLocalSiteSequenceIdFromSite , 0)  THEN 1  ");
        _sb.AppendLine("                               WHEN (ST_FORCE_RESYNCH = 1)  THEN 1  ");
        _sb.AppendLine("                               ELSE 0 END) ");
        _sb.AppendLine("        FROM   LCD_MESSAGES  ");
        _sb.AppendLine("             , MS_SITE_TASKS ");
        _sb.AppendLine("       WHERE   MSG_TIMESTAMP >= CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE ISNULL(@pLocalSiteSequenceIdFromSite, 0) END ");
        _sb.AppendLine("         AND   ST_TASK_ID = @pDownloadLcdMessages ");
        _sb.AppendLine("         AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("         AND   MSG_UNIQUE_ID IS NOT NULL ");
        _sb.AppendLine("     END");
        _sb.AppendLine("   ELSE IF @pTaskId = @pDownloadBucketsConfig");
        _sb.AppendLine("     BEGIN");
        _sb.AppendLine("      SELECT   @MaxSeq = MAX(ISNULL(BU_TIMESTAMP,0))  ");
        _sb.AppendLine("             , @Count  = SUM(CASE WHEN BU_TIMESTAMP > ISNULL(@pLocalSiteSequenceIdFromSite , 0)  THEN 1  ");
        _sb.AppendLine("                               WHEN (ST_FORCE_RESYNCH = 1)  THEN 1  ");
        _sb.AppendLine("                               ELSE 0 END) ");
        _sb.AppendLine("        FROM   BUCKETS  ");
        _sb.AppendLine("             , MS_SITE_TASKS ");
        _sb.AppendLine("       WHERE   BU_TIMESTAMP >= CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE ISNULL(@pLocalSiteSequenceIdFromSite, 0) END ");
        _sb.AppendLine("         AND   ST_TASK_ID = @pDownloadBucketsConfig ");
        _sb.AppendLine("         AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("         AND   BU_MASTER_SEQUENCE_ID IS NOT NULL ");
        _sb.AppendLine("     END");
        _sb.AppendLine("                                          ");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS                  ");
        _sb.AppendLine("     SET   ST_NUM_PENDING        = @Count ");
        _sb.AppendLine("         , ST_LOCAL_SEQUENCE_ID  = @MaxSeq ");
        _sb.AppendLine("         , ST_REMOTE_SEQUENCE_ID = CASE WHEN (ST_FORCE_RESYNCH = 1) THEN 0 ELSE @pLocalSiteSequenceIdFromSite END");
        _sb.AppendLine("   WHERE   ST_TASK_ID = @pTaskid ");
        _sb.AppendLine("     AND   ST_SITE_ID = @pSiteId ");
        _sb.AppendLine("END;");

        // Replace one or more space with one space 
        _sql = (new System.Text.RegularExpressions.Regex(@"(\s+|\n)")).Replace(_sb.ToString(), " ");

        // Get Sql Connection 
        using (DB_TRX _db_trx = new DB_TRX())
        {          

          using (SqlCommand _cmd = new SqlCommand(_sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            foreach (Int32 _task_id in Enum.GetValues(typeof(TYPE_MULTISITE_SITE_TASKS)))
            {
              _cmd.Parameters.Add("@p" + Enum.GetName(typeof(TYPE_MULTISITE_SITE_TASKS), _task_id), SqlDbType.Int).Value = _task_id;
            }

            _cmd.Parameters.Add("@pSeqTrackAccountChanges", SqlDbType.Int).Value = WSI.Common.SequenceId.TrackAccountChanges;
            _cmd.Parameters.Add("@pSeqTrackPointChanges", SqlDbType.Int).Value = WSI.Common.SequenceId.TrackPointChanges;
            _cmd.Parameters.Add("@pSeqProvidersGames", SqlDbType.Int).Value = WSI.Common.SequenceId.ProvidersGames;
            _cmd.Parameters.Add("@pSeqProviders", SqlDbType.Int).Value = WSI.Common.SequenceId.Providers;
            _cmd.Parameters.Add("@pSeqAccountDocuments", SqlDbType.Int).Value = WSI.Common.SequenceId.AccountDocuments;
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

            _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).SourceColumn = "ST_TASK_ID";
            _cmd.Parameters.Add("@pNumPending", SqlDbType.Int).SourceColumn = "ST_NUM_PENDING";
            _cmd.Parameters.Add("@pLocalSiteSequenceIdFromSite", SqlDbType.BigInt).SourceColumn = "ST_LOCAL_SEQUENCE_ID";
            // This sequence is remote sequence from site. If value is 0 will do force!
            _cmd.Parameters.Add("@pRemoteSequenceIdFromSite", SqlDbType.BigInt).SourceColumn = "ST_REMOTE_SEQUENCE_ID";

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              _num_rows_updated = _sql_adapt.Update(_tasks);
            }

            if (_num_rows_updated != _tasks.Length)
            {
              Log.Warning(String.Format("Not updated all tasks in Site {0}. Expected: {1}, Updated: {2}", SiteId, _tasks.Length, _num_rows_updated));
            }
          } // Using Command


          //
          // Update / Insert Multisite - Currencies Only if MultiSite Multicurrency is enabled
          //
          if (WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency())
          {
            if (DSConfiration.Tables.Contains("CURRENCIES"))
            {
              if (!MultiSite_CenterConfiguration.Update_MultiCurrencies(SiteId, DSConfiration.Tables["CURRENCIES"], _db_trx.SqlTransaction))
              {
                Log.Error(" Calling Update_MultiCurrencies. Site: " + SiteId.ToString());
                _db_trx.Rollback();

                return false;
              }
            }
          }

          // Everything went ok!
          _db_trx.Commit();
        }

        //
        //Msg to site
        // 
        _sb.Length = 0;


        // 0 - General params 
        _sb.AppendLine("DECLARE   @pElpEnabled as BIT");
        _sb.AppendLine(" SELECT   @pElpEnabled = ISNULL(ST_ELP,0) ");
        _sb.AppendLine("   FROM   SITES ");
        _sb.AppendLine("  WHERE   ST_SITE_ID = @pSiteId");

        _sb.AppendLine("SELECT   GP_GROUP_KEY   ");
        _sb.AppendLine("       , GP_SUBJECT_KEY ");
        _sb.AppendLine("       , ISNULL(GP_KEY_VALUE, '') AS GP_KEY_VALUE ");
        _sb.AppendLine("       , GP_MS_DOWNLOAD_TYPE ");
        _sb.AppendLine("  FROM   GENERAL_PARAMS      ");
        _sb.AppendLine(" WHERE   GP_MS_DOWNLOAD_TYPE > 0 ");
        _sb.AppendLine("   AND   GP_GROUP_KEY+'.'+GP_SUBJECT_KEY <> 'PlayerTracking.ExternalLoyaltyProgram.Mode'");

        // If 'Elp' is disabled will discard ExternalLoyaltyProgram GP
        _sb.AppendLine("  AND   GP_GROUP_KEY <> CASE WHEN @pElpEnabled = 0 THEN 'ExternalLoyaltyProgram.Mode01' ELSE '' END");

        if (CurrencyMultisite.IsEnabledMultiCurrency())
        {
          //If 'Multicurrencies' is active then don't download 'Antilavado de dinero' configuration to the sities
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'AntiMoneyLaundering'     ");
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'Account.DefaultValues'   ");
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'Account.VisibleField'    ");
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'Account.RequestedField'  ");

        }

        if (CommonMultiSite.GetPlayerTrackingMode() == PlayerTracking_Mode.Site)
        {
          //If 'Is Configured Player Tracking In Site' then don't download 'Player tracking' configuration to the sities
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'PlayerTracking' ");
          _sb.AppendLine("  AND   GP_GROUP_KEY <> 'PlayerTracking.DoNotAwardPoints' ");
        }

        // Set Site GP PlayerTracking.ExternalLoyaltyProgram.Mode value to @pElpEnabled
        _sb.AppendLine("UNION ALL ");

        _sb.AppendLine("SELECT   GP_GROUP_KEY   ");
        _sb.AppendLine("       , GP_SUBJECT_KEY ");
        _sb.AppendLine("       , CAST(@pElpEnabled as NVARCHAR) AS GP_KEY_VALUE ");
        _sb.AppendLine("       , GP_MS_DOWNLOAD_TYPE ");
        _sb.AppendLine("  FROM   GENERAL_PARAMS      ");
        _sb.AppendLine(" WHERE   GP_GROUP_KEY+'.'+GP_SUBJECT_KEY = 'PlayerTracking.ExternalLoyaltyProgram.Mode'");


        // 1 - Tasks
        _sb.AppendLine("SELECT   ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_LOCAL_SEQUENCE_ID, ST_REMOTE_SEQUENCE_ID, ST_NUM_PENDING, ST_FORCE_RESYNCH, ST_MAX_ROWS_TO_UPLOAD, ST_START_TIME ");
        _sb.AppendLine("  FROM   MS_SITE_TASKS ");
        _sb.AppendLine(" WHERE   ST_SITE_ID = @pSiteId ");
        // 2 - Sequence
        _sb.AppendLine("SELECT   SEQ_ID, SEQ_NEXT_VALUE - 1 SEQ_LAST_VALUE FROM SEQUENCES");
        _sb.AppendLine(" ");

        if (!CurrencyMultisite.IsEnabledMultiCurrency())
        {
          // 3 - Identification Types
          _sb.AppendLine("SELECT IDT_ID, IDT_ENABLED, IDT_ORDER, IDT_NAME, IDT_COUNTRY_ISO_CODE2 FROM IDENTIFICATION_TYPES");
        }
        _sb.AppendLine(" ");

        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(Reply);
              Reply.Tables[0].TableName = "GENERAL_PARAMS";
              Reply.Tables[1].TableName = "TASKS";
              Reply.Tables[2].TableName = "SEQUENCES";
              if (!CurrencyMultisite.IsEnabledMultiCurrency())
              {
                Reply.Tables[3].TableName = "IDENTIFICATION_TYPES";
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }// Update_Tasks

    //------------------------------------------------------------------------------
    // PURPOSE : Update currencies in Multisite from sites. Only works when GP Multisite.Multicurrency is enabled.
    //
    //  PARAMS :
    //      - INPUT :
    //                - Site ID
    //                - Dataset from site (Include config and currencies)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: All the currencies are inserted or updated. False: Otherwise.
    //
    public static Boolean Update_MultiCurrencies(Int32 SiteId, DataTable DtCurrencies, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_da_currencies = null;
      SqlParameter _output_param;
      Int32 _response_code;
  
    _sb = new StringBuilder();

      try
      {
        DtCurrencies.Columns.Add("CE_UPDATED_STATUS", Type.GetType("System.Int32"));
        DtCurrencies.Columns.Add("CE_BEFORE_ISO_CODE", Type.GetType("System.String"));

        _sb.AppendLine(" EXECUTE MultiSite_UpdateCurrencies       ");
        _sb.AppendLine("                  @pIsoCode						    ");
        _sb.AppendLine("                , @pSiteId						    ");
        _sb.AppendLine("                , @pType							    ");
        _sb.AppendLine("                , @pDescription				    ");
        _sb.AppendLine("                , @pForeignCurrencyType	  ");
        _sb.AppendLine("                , @pNationalCurrencyType  ");
        _sb.AppendLine("                , @pChange						    ");
        _sb.AppendLine("                , @pResponseCode OUTPUT   ");
        _sb.AppendLine("                , @pBeforeCurrencyIsoCode OUTPUT   ");

        _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx);
        _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
        _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).SourceColumn = "CE_CURRENCY_ISO_CODE";
        _sql_cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar).SourceColumn = "CE_DESCRIPTION";
        _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CE_NATIONAL_CURRENCY";
        _sql_cmd.Parameters.Add("@pForeignCurrencyType", SqlDbType.Int).Value = WSI.Common.CurrencyType.Foreign;
        _sql_cmd.Parameters.Add("@pNationalCurrencyType", SqlDbType.Int).Value = WSI.Common.CurrencyType.National;
        _sql_cmd.Parameters.Add("@pChange", SqlDbType.Decimal).SourceColumn = "CE_CHANGE";

        _output_param = new SqlParameter("@pResponseCode", SqlDbType.Int, 4, "CE_UPDATED_STATUS");
        _output_param.Direction = ParameterDirection.Output;
        _sql_cmd.Parameters.Add(_output_param);

        _output_param = new SqlParameter("@pBeforeCurrencyIsoCode", SqlDbType.NVarChar, 3, "CE_BEFORE_ISO_CODE");
        _output_param.Direction = ParameterDirection.Output;
        _sql_cmd.Parameters.Add(_output_param);

        _sql_da_currencies = new SqlDataAdapter();
        _sql_da_currencies.InsertCommand = _sql_cmd;
        _sql_da_currencies.ContinueUpdateOnError = false;
        _sql_da_currencies.UpdateBatchSize = 500;
        _sql_da_currencies.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

        _sql_da_currencies.Update(DtCurrencies);

        foreach (DataRow _dr in DtCurrencies.Rows)
        {
          if (_dr["CE_UPDATED_STATUS"] == DBNull.Value)
          {
            continue;
          }

          _response_code = (Int32)_dr["CE_UPDATED_STATUS"];

          if (_dr["CE_BEFORE_ISO_CODE"] == DBNull.Value)
          {
            _dr["CE_BEFORE_ISO_CODE"] = string.Empty;
          }

          if ((_response_code & (Int32)StatusUpdateCurrencyType.NewCurrency) == (Int32)StatusUpdateCurrencyType.NewCurrency)
          {
            // Insert in CURRENCIES
            Alarm.Register(AlarmSourceCode.MultiSite,
                           SiteId,
                           "Site: " + SiteId,
                           (uint)AlarmCode.MultiSite_MultiCurrencyMultiSiteNewCurrency,
                           Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_CURRENCY", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"] }),
                           AlarmSeverity.Warning);
            Log.Message(Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_CURRENCY", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"] }));
          }

          if (((Int32)_dr["CE_UPDATED_STATUS"] & (Int32)StatusUpdateCurrencyType.NewSiteNationalCurrency) == (Int32)StatusUpdateCurrencyType.NewSiteNationalCurrency)
          {
            // Insert National Currency in SITE_CURRENCIES 
            Alarm.Register(AlarmSourceCode.MultiSite,
                           SiteId,
                           "Site: " + SiteId,
                           (uint)AlarmCode.MultiSite_MultiCurrencySiteChangeNationalCurrency,
                           Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_SITE_CURRENCY", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"], (String)_dr["CE_BEFORE_ISO_CODE"] }),
                           AlarmSeverity.Warning);
            Log.Message(Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_SITE_CURRENCY", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"], (String)_dr["CE_BEFORE_ISO_CODE"] }));
          }

          if (((Int32)_dr["CE_UPDATED_STATUS"] & (Int32)StatusUpdateCurrencyType.NewSiteForeignCurrency) == (Int32)StatusUpdateCurrencyType.NewSiteForeignCurrency)
          {
            // Insert Foreign Currency in SITE_CURRENCIES
            Alarm.Register(AlarmSourceCode.MultiSite,
                           SiteId,
                           "Site: " + SiteId,
                           (uint)AlarmCode.MultiSite_MultiCurrencySiteChangeForeignCurrency,
                           Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_SITE_CURRENCY_FOREIGN", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"], (String)_dr["CE_BEFORE_ISO_CODE"] }),
                           AlarmSeverity.Info);
            Log.Message(Resource.String("STR_MULTISITE_MULTICURRENCY_ALARM_NEW_SITE_CURRENCY_FOREIGN", new string[] { SiteId.ToString(), (String)_dr["CE_CURRENCY_ISO_CODE"], (String)_dr["CE_BEFORE_ISO_CODE"] }));
          }

        } // foreach

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Update_MultiCurrencies
  }
}