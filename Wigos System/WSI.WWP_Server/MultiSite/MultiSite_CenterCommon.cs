//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_CenterCommon.cs
//
//   DESCRIPTION : MultiSite_CenterCommon class
//
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 11-MAR-2013
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;

namespace WSI.WWP_CenterService.MultiSite
{
  class MultiSite_CenterCommon
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Get last sequences.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteId
    //
    //      - OUTPUT :
    //          - ReplySequences
    //
    // RETURNS :
    //
    static public Boolean Read_LastCenterSequences(out DataTable ReplySequences)
    {
      StringBuilder _sb = new StringBuilder();

      ReplySequences = new DataTable("REPLY_SEQUENCES");
      ReplySequences.Columns.Add("SEQ_ID", Type.GetType("System.Int32"));
      ReplySequences.Columns.Add("SEQ_VALUE", Type.GetType("System.Int64"));
      ReplySequences.PrimaryKey = new DataColumn[] { ReplySequences.Columns["MS_SEQ_ID"] };

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT  SEQ_ID ");
        _sb.AppendLine("      , SEQ_NEXT_VALUE - 1 AS SEQ_VALUE  ");
        _sb.AppendLine("  FROM  SEQUENCES ");
        _sb.AppendLine(" WHERE  SEQ_ID NOT IN (@pSeqAccountMovementsId) ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pSeqAccountMovementsId", SqlDbType.Int).Value = (Int32)WSI.Common.SequenceId.AccountMovements;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, ReplySequences);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Read_LastCenterSequences

  }
}
