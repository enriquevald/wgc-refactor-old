//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_SiteRequestsProcess.cs
// 
//   DESCRIPTION: Site Requests process.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
// 03-MAY-2013 XIT    Thread's logic added
// 23-JUL-2014 RRR    Changed Log to ELPLog
// 26-MAR-2018 JGC    PBI 32313:[WIGOS-9404]: Codere_Mexico: Call to space to get token
// 26-MAR-2018 JGC    PBI 32312:[WIGOS-9407]: Codere_Mexico Space: Use token to call space servicesModification to add token security for calls to Space
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;
using WSI.WWP_CenterService.MultiSite.Space;
using WSI.WWP_Server;
using System.Threading;
using System.Collections;
using System.Net;

namespace WSI.WWP_CenterService.MultiSite
{

  #region WorkerDefinitions

  public class SiteRequestTask : Task
  {
    private DataRow m_dr_site_request;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    private static int m_stats_log_tick = Misc.GetTickCount();

    private static int m_confirm_total_count = 0;
    private static int m_redeem_total_count = 0;
    private static int m_points_total_count = 0;

    private static long m_confirm_total_elapased = 0;
    private static long m_redeem_total_elapased = 0;
    private static long m_points_total_elapased = 0;

    private static long m_process_redeem_ellapsed = 0;
    private static long m_process_points_ellapsed = 0;
    private static long m_confirm_process_ellapsed = 0;

    private static long m_confirm_queued_elapased = 0;
    private static long m_redeem_queued_elapased = 0;
    private static long m_points_queued_elapased = 0;

    private static long m_confirm_ws_elapased = 0;
    private static long m_redeem_ws_elapased = 0;
    private static long m_points_ws_elapased = 0;

    //smnsmn

    public SiteRequestTask(DataRow DrSiteRequestTask)
    {
      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;

      m_dr_site_request = DrSiteRequestTask.Table.NewRow();
      for (Int32 _idx_item = 0; _idx_item < DrSiteRequestTask.ItemArray.Length; _idx_item++)
      {
        m_dr_site_request[_idx_item] = DrSiteRequestTask[_idx_item];
      }
    }

    public override void Execute()
    {
      long _tq;
      long _ws;
      ELP01_Requests _type_request;
      StringBuilder _msg;

      _type_request = ELP01_Requests.ELP01_REQUEST_UNKNOWN;
      m_tick_dequeued = Environment.TickCount;
      _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
      _ws = 0;

      try
      {
        if (_tq < GeneralParam.GetInt64("ExternalLoyaltyProgram.Mode01", "MaxQueuedTime", 20000))
        {
          MS_SiteRequest_Process.SiteRequestProcess(m_dr_site_request, out _ws, out _type_request);
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }
      finally
      {

        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _ws > 10000
            || (_tx - _ws) > 10000
            || _tt > 20000)
        {
          Log.Warning("MultiSite Request Process times: Queued/WS/Process/Total = " + _tq.ToString() + "/" + _ws.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString());
        }


        if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0)
        {
          AddEllapsedTime(_type_request, _tq, _ws, _tx, _tt);

          if (Misc.GetElapsedTicks(m_stats_log_tick) > 5 * 60000)
          {
            _msg = new StringBuilder();

            m_stats_log_tick = Misc.GetTickCount();

            _msg.Append("SPACE Stats Avg times (ms): [Count/Queued/WS/Process/Total]: ");
            _msg.Append(GetStatistics(ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM));
            _msg.Append(", " + GetStatistics(ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM));
            _msg.Append(", " + GetStatistics(ELP01_Requests.ELP01_REQUEST_QUERY_POINTS));

            ELPLog.Message(_msg.ToString());
          }
        } // if
      } // finally
    } // Execute

    private void AddEllapsedTime(ELP01_Requests TypeRequest, Int64 Tq, Int64 Ws, Int64 Tx, Int64 Tt)
    {
      switch (TypeRequest)
      {
        case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
          {
            Interlocked.Add(ref m_confirm_queued_elapased, Tq);
            Interlocked.Add(ref m_confirm_process_ellapsed, Tx);
            Interlocked.Add(ref m_confirm_ws_elapased, Ws);
            Interlocked.Add(ref m_confirm_total_elapased, Tt);
            Interlocked.Increment(ref m_confirm_total_count);

            break;
          }
        case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
          {
            Interlocked.Add(ref m_points_queued_elapased, Tq);
            Interlocked.Add(ref m_process_points_ellapsed, Tx);
            Interlocked.Add(ref m_points_ws_elapased, Ws);
            Interlocked.Add(ref m_points_total_elapased, Tt);
            Interlocked.Increment(ref m_points_total_count);

            break;
          }
        case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
          {
            Interlocked.Add(ref m_redeem_queued_elapased, Tq);
            Interlocked.Add(ref m_process_redeem_ellapsed, Tx);
            Interlocked.Add(ref m_redeem_ws_elapased, Ws);
            Interlocked.Add(ref m_redeem_total_elapased, Tt);
            Interlocked.Increment(ref m_redeem_total_count);

            break;
          }
      }
    } // AddEllapsedTime

    private String GetStatistics(ELP01_Requests TypeRequest)
    {
      Int32 _count;
      Int64 _elapsed_queued;
      Int64 _elapsed_process;
      Int64 _elapsed_total;
      Int64 _elapsed_ws;
      StringBuilder _msg;

      _msg = new StringBuilder();

      _count = 0;
      _elapsed_queued = 0;
      _elapsed_process = 0;
      _elapsed_total = 0;
      _elapsed_ws = 0;

      switch (TypeRequest)
      {
        case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
          {
            _count = Interlocked.Exchange(ref m_confirm_total_count, 0);
            _elapsed_queued = Interlocked.Exchange(ref m_confirm_queued_elapased, 0);
            _elapsed_process = Interlocked.Exchange(ref m_confirm_process_ellapsed, 0);
            _elapsed_ws = Interlocked.Exchange(ref m_confirm_ws_elapased, 0);
            _elapsed_total = Interlocked.Exchange(ref m_confirm_total_elapased, 0);

            _msg.Append("Confirm: ");

            break;
          }
        case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
          {
            _count = Interlocked.Exchange(ref m_points_total_count, 0);
            _elapsed_queued = Interlocked.Exchange(ref m_points_queued_elapased, 0);
            _elapsed_process = Interlocked.Exchange(ref m_process_points_ellapsed, 0);
            _elapsed_ws = Interlocked.Exchange(ref m_points_ws_elapased, 0);
            _elapsed_total = Interlocked.Exchange(ref m_points_total_elapased, 0);

            _msg.Append("Points: ");

            break;
          }
        case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
          {
            _count = Interlocked.Exchange(ref m_redeem_total_count, 0);
            _elapsed_queued = Interlocked.Exchange(ref m_redeem_queued_elapased, 0);
            _elapsed_process = Interlocked.Exchange(ref m_process_redeem_ellapsed, 0);
            _elapsed_ws = Interlocked.Exchange(ref m_redeem_ws_elapased, 0);
            _elapsed_total = Interlocked.Exchange(ref m_redeem_total_elapased, 0);

            _msg.Append("Redeem: ");

            break;
          }
      }

      if (_count > 0)
      {
        _elapsed_queued /= _count;
        _elapsed_process /= _count;
        _elapsed_total /= _count;
        _elapsed_ws /= _count;
      }

      _msg.Append("[" + _count.ToString() + "/" + _elapsed_queued + "/" + _elapsed_ws.ToString() + "/" + _elapsed_process.ToString() + "/" + _elapsed_total + "]");

      return _msg.ToString();
    } // GetStatistics

  } // class SiteRequestTask

  #endregion // WorkerDefinitions

  public static class MS_SiteRequest_Process
  {
    public static WorkerPool m_worker_pool_redeem;
    public static WorkerPool m_worker_pool_points;
    public static WorkerPool m_worker_pool_confirm;

    static Int32 _delete_old_request_tick = Environment.TickCount - 300000;

    //------------------------------------------------------------------------------
    // PURPOSE : Init Main thread & WorkerPool for site requests.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    public static void Init()
    {
      Int32 _num_workers;
      Thread _thread;

      _num_workers = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsRedeem.NumWorkers", 120);
      if (_num_workers == 0)
      {
        _num_workers = 120;
      }
      m_worker_pool_redeem = new WorkerPool("ELP01_RedeemRequests", _num_workers);


      _num_workers = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsPoints.NumWorkers", 240);
      if (_num_workers == 0)
      {
        _num_workers = 240;
      }
      m_worker_pool_points = new WorkerPool("ELP01_PointsRequests", _num_workers);


      _num_workers = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "WsConfirm.NumWorkers", 40);
      if (_num_workers == 0)
      {
        _num_workers = 40;
      }
      m_worker_pool_confirm = new WorkerPool("ELP01_ConfirmRequests", _num_workers);


      // Start Main Thread
      _thread = new Thread(new ThreadStart(SitesRequestsThread));
      _thread.Name = "Thread_Requests";
      _thread.Start();

      // Start Resend Thread
      _thread = new Thread(new ThreadStart(SitesRequestsResendThread));
      _thread.Name = "Thread_Resend";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Main Thread que procesa peticiones. Coge de BD las peticiones existentes y las encola
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    private static void SitesRequestsThread()
    {
      DataSet _requests;
      Int32 _wait_hint;
      int _tick_principal;
      Boolean _is_principal;


      _wait_hint = 0;
      _tick_principal = Misc.GetTickCount();
      _is_principal = false;


      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          if (!_is_principal)
          {
            _is_principal = Services.IsPrincipal("WWP CENTER");
            _tick_principal = Misc.GetTickCount();

            if (!_is_principal)
            {
              _wait_hint = 2000;

              continue;
            }
          }

          // Is Principal
          if (Misc.GetElapsedTicks(_tick_principal) >= 10000)
          {
            _is_principal = Services.IsPrincipal("WWP CENTER");
            _tick_principal = Misc.GetTickCount();

            if (!_is_principal)
            {
              continue;
            }
          }

          if (Misc.GetElapsedTicks(_delete_old_request_tick) > 10000)
          {
            if (!MS_SiteRequest_Database.SiteRequest_DeleteOldRequests())
            {
              Log.Error("SiteRequest_DeleteOldRequests failed.");
            }
            _delete_old_request_tick = Environment.TickCount;
          }

          if (!MS_SiteRequest_Database.SiteRequest_PendingToInProgress(out _requests))
          {
            _wait_hint = 5000;

            Log.Error("SiteRequest_PendingToInProgress Failed.");

            continue;
          }

          if (_requests.Tables[0].Rows.Count == 0)
          {
            _wait_hint = 500;

            continue;
          }


          foreach (DataRow _row in _requests.Tables[0].Rows)
          {
            Boolean _queued;

            _queued = false;


            switch ((int)_row["SR_MR_PRIORITY"])
            {
              case 1:
                m_worker_pool_redeem.EnqueueTask(new SiteRequestTask(_row));
                _queued = true;
                break;

              case 0:
              default:
                break;
            }

            if (_queued)
            {
              continue;
            }

            try
            {
              String _sr_input;

              _sr_input = (String)_row["SR_INPUT_DATA"];
              if (_sr_input.Contains("ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM"))
              {
                m_worker_pool_confirm.EnqueueTask(new SiteRequestTask(_row));
                _queued = true;
              }
            }
            catch
            { ; }

            if (_queued)
            {
              continue;
            }
            m_worker_pool_points.EnqueueTask(new SiteRequestTask(_row));
          }

          _wait_hint = 250;

          //if (Misc.GetElapsedTicks(_delete_old_request_tick) > 10000)
          //{
          //  if (!MS_SiteRequest_Database.SiteRequest_DeleteOldRequests())
          //  {
          //    Log.Error("SiteRequest_DeleteOldRequests failed.");
          //  }
          //  _delete_old_request_tick = Environment.TickCount;
          //}
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    }// SitesRequestsThread



    //------------------------------------------------------------------------------
    // PURPOSE : Funci�n que procesa una petici�n. Desencola una petici�n y la gestiona(envia por WS, recibe respuesta,actualiza BD con resultado
    //           ,devuelve a cliente, actualiza estado en BD.
    //
    //  PARAMS :
    //      - INPUT : 
    //          - InputData: ID de petici�n.   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    public static void SiteRequestProcess(DataRow InputSiteRequest, out long WsElapsedTime, out ELP01_Requests TypeRequest)
    {
      WsElapsedTime = 0;
      TypeRequest = ELP01_Requests.ELP01_REQUEST_UNKNOWN;

      switch ((WWP_MultiSiteRequestType)InputSiteRequest["SR_REQUEST_TYPE"])
      {
        case WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01:
          {
            ExecuteELP01Request(InputSiteRequest, out WsElapsedTime, out TypeRequest);
            break;
          }
        default:
          {
            break;
          }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Funci�n que procesa una petici�n. Desencola una petici�n y la gestiona(envia por WS, recibe respuesta,actualiza BD con resultado
    //           ,devuelve a cliente, actualiza estado en BD.
    //
    //  PARAMS :
    //      - INPUT : 
    //          - InputData: ID de petici�n.   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //

    private static int m_error_log_tick = Misc.GetTickCount();
    private static int m_error_points = 0;
    private static int m_error_redeem = 0;
    private static int m_error_confirm = 0;

    private static long m_confirm_ws_total_elapased = 0;
    private static long m_redeem_ws_total_elapased = 0;
    private static long m_points_ws_total_elapased = 0;

    private static void ExecuteELP01Request(DataRow InputSiteRequest, out long WsElapsedTime, out ELP01_Requests TypeRequest)
    {

      Int64 _points_response;
      Int64 _redeem_response;
      Int64 _not_redeem_response;
      Int32 _restricted_to_group_id;
      ELP01_ResponseCodes _response_code;
      Elp01InputData _input_data;
      Elp01OutputData _output_data;
      WWP_ResponseCodes _wwp_rc_response;
      Int64 _inc_customer_number;
      String _request_info;
      String _response_info;
      int _site_id;
      String _input;
      Boolean _ok;
      Boolean _log;
      Int64 _unique_id;
      Boolean _audit_insert;
      Boolean _audit_update;
      int _ws_request_tick;

      WsElapsedTime = 0;
      _audit_insert = false;
      _audit_update = false;
      _log = false;
      _request_info = String.Empty;
      _response_info = String.Empty;
      _input = String.Empty;
      TypeRequest = ELP01_Requests.ELP01_REQUEST_UNKNOWN;

      try
      {
        _input = "";
        _unique_id = (Int64)InputSiteRequest["SR_UNIQUE_ID"];
        _input_data = new Elp01InputData();
        _input = (String)InputSiteRequest["SR_INPUT_DATA"];
        _input_data.LoadXml(_input);
        _site_id = (int)InputSiteRequest["SR_SITE_ID"];
      }
      catch
      {
        ELPLog.Error("Elp01InputData.LoadXml() failed. Input: " + _input);

        return;
      }

      TypeRequest = _input_data.RequestType;

      switch (_input_data.RequestType)
      {
        case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
          _request_info = "CONFIRM";
          _log = ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x4) != 0);
          break;

        case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
          _request_info = "POINTS ";
          _log = ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x1) != 0);
          break;

        case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
          _request_info = "REDEEM ";
          _log = ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x2) != 0);
          break; ;

        default:
          Log.Error("Unknown RequestType: " + _input_data.RequestType.ToString() + ", Input: " + _input);

          return;
      }


      _ok = MS_ELP01_ExchangeTables.GetIncCustomerNumber(_input_data.AccountId, out _inc_customer_number);
      _request_info = "SPACE " + _request_info
                    + "  Site: " + _site_id.ToString("000")
                    + ", AccountId: " + _input_data.AccountId.ToString()
                    + ", SpaceId: " + _inc_customer_number.ToString()
                    + ", VoucherId: " + _input_data.VoucherId;
      if (!_ok)
      {
        _request_info += ", Input: " + _input;

        Log.Error("GetIncCustomerNumber Failed. " + _request_info);

        return;
      }

      _ws_request_tick = Misc.GetTickCount();

      Boolean _alarm = true;

      //Call to Space to do an action, if we get Token error, try again one time
     ExecuteELP01ActionRequest(ELP_Space.HEADER_TYPE.WITH_TOKEN, _input_data,_inc_customer_number, WsElapsedTime, _ws_request_tick, out _response_code, out _output_data, out _audit_insert, out _audit_update, out _alarm, 
                                out _points_response, out _redeem_response, out _not_redeem_response, out _restricted_to_group_id);

      // By now, if we not get ResponseCode = OK, retry one time always, because now we don't if error is because token or another reason.
      // if (_response_code.Equals(ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TOKEN_ERROR))
      if(!_response_code.Equals(ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK))
      {
        ExecuteELP01ActionRequest(ELP_Space.HEADER_TYPE.WITHOUT_TOKEN ,_input_data, _inc_customer_number, WsElapsedTime, _ws_request_tick, out _response_code, out _output_data, out _audit_insert, out _audit_update, out _alarm, 
                                  out _points_response, out _redeem_response, out _not_redeem_response, out _restricted_to_group_id);
      }

      if (_log && _alarm)
      {
        ELPLog.Message(_request_info + _response_info);
      }

      Boolean _connected_to_me;
      Array _clients;

      _connected_to_me = false;
      _clients = WWP_Server.WWP.Server.Clients.ToArray();
      foreach (SecureTcpClient _client in _clients)
      {
        if (_client.InternalId == _site_id)
        {
          _connected_to_me = true;
          break;
        }
      }

      if (_connected_to_me)
      {
        Trx_ResponseSiteRequestMessage(_site_id, (Int32)InputSiteRequest["SR_REQUEST_TYPE"], (Int64)InputSiteRequest["SR_MR_UNIQUE_ID"], (String)InputSiteRequest["SR_INPUT_DATA"], _output_data.ToXml(), 0, out _wwp_rc_response);
        if (!MS_SiteRequest_Database.SiteRequest_DeleteRequest(_unique_id))
        {
          ELPLog.Error("SiteRequest_DeleteRequest failed. " + _request_info + _response_info);
        }
      }
      else
      {
        if (!MS_SiteRequest_Database.SiteRequest_ResponseReceived(_unique_id, _output_data.ToXml()))
        {
          ELPLog.Error("SiteRequest_ResponseReceived failed. " + _request_info + _response_info);
        }
      }

      if (_audit_update)
      {
        if (!MS_ELP01_Process.UpdateElp01RedeemAudit((Int32)InputSiteRequest["SR_SITE_ID"], _input_data, _output_data))
        {
          ELPLog.Error("MS_ELP01_Process.InsertElp01RedeemAudit Failed. " + _request_info + _response_info);
        }
      }
      if (_audit_insert)
      {
        if (!MS_ELP01_Process.InsertElp01RedeemAudit((Int32)InputSiteRequest["SR_SITE_ID"], _input_data, _output_data))
        {
          ELPLog.Error("MS_ELP01_Process.InsertElp01RedeemAudit Failed. " + _request_info + _response_info);
        }
      }

      if (_alarm)
      {
        if (Misc.GetElapsedTicks(m_error_log_tick) > 60000)
        {
          String _msg;
          int _num_errors;

          m_error_log_tick = Misc.GetTickCount();
          _msg = "SPACE Errors";

          _num_errors = Interlocked.Exchange(ref m_error_redeem, 0);
          _msg += ", Redeem: " + _num_errors.ToString();
          _num_errors = Interlocked.Exchange(ref m_error_confirm, 0);
          _msg += ", Confirm: " + _num_errors.ToString();
          _num_errors = Interlocked.Exchange(ref m_error_points, 0);
          _msg += ", Points: " + _num_errors.ToString();

          Alarm.Register(AlarmSourceCode.ELP01, 0, Environment.MachineName + @"\\WSI.WWP_CenterService", AlarmCode.ELP_ErrorStatus, _msg);
          ELPLog.Error(_msg);
        }
      }
    } // ExecuteELP01Request

    /// <summary>
    /// Executes ELP01 action request to Space
    /// </summary>
    /// <param name="HeaderType"></param>
    /// <param name="InputData"></param>
    /// <param name="CustomerNumber"></param>
    /// <param name="WsElapsedTime"></param>
    /// <param name="RequestTick"></param>
    /// <param name="ResponseCode"></param>
    /// <param name="OutputData"></param>
    /// <param name="AuditInsert"></param>
    /// <param name="AuditUpdate"></param>
    /// <param name="Alarm"></param>
    /// <param name="PointsResponse"></param>
    /// <param name="RedeemResponse"></param>
    /// <param name="NotRedeemResponse"></param>
    /// <param name="RestrictedToGroupId"></param>
    private static void ExecuteELP01ActionRequest(ELP_Space.HEADER_TYPE HeaderType, Elp01InputData InputData, Int64 CustomerNumber, long WsElapsedTime, int RequestTick, out ELP01_ResponseCodes ResponseCode,
                                                  out Elp01OutputData OutputData, out Boolean AuditInsert, out Boolean AuditUpdate, out Boolean Alarm, out Int64 PointsResponse,
                                                  out Int64 RedeemResponse, out Int64 NotRedeemResponse, out Int32 RestrictedToGroupId)
    {
      ResponseCode = ELP01_ResponseCodes.ELP01_RESPONSE_CODE_UNKNOWN;
      OutputData = new Elp01OutputData();
      AuditInsert = false;
      AuditUpdate = false;
      Alarm = true;
      PointsResponse = -1;
      RedeemResponse = -1;
      NotRedeemResponse = -1;
      RestrictedToGroupId = -1;

      try
      {
        switch (InputData.RequestType)
        {
          case ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM:
            {
              MS_ELP01_WS.ConfirmRedeem(HeaderType, CustomerNumber, InputData.VoucherId, out ResponseCode);
              WsElapsedTime = Misc.GetElapsedTicks(RequestTick);

              if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0)
              {
                Interlocked.Add(ref m_confirm_ws_total_elapased, WsElapsedTime);
              }

              OutputData = new Elp01OutputData();
              OutputData.RequestType = ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM;
              OutputData.ResponseCodes = ResponseCode;

              if (ResponseCode == ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK)
              {
                AuditUpdate = true;
                Alarm = false;
              }
              else
              {
                Interlocked.Increment(ref m_error_confirm);
              }

              break;
            }

          case ELP01_Requests.ELP01_REQUEST_QUERY_POINTS:
            {
              MS_ELP01_WS.GetPoints(HeaderType, CustomerNumber, out  PointsResponse, out ResponseCode);
              WsElapsedTime = Misc.GetElapsedTicks(RequestTick);

              if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0)
              {
                Interlocked.Add(ref m_points_ws_total_elapased, WsElapsedTime);
              }
              OutputData = new Elp01OutputData();
              OutputData.RequestType = ELP01_Requests.ELP01_REQUEST_QUERY_POINTS;
              OutputData.Points = PointsResponse;
              OutputData.ResponseCodes = ResponseCode;


              if (ResponseCode == ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK)
              {
                Alarm = false;
              }
              else
              {
                Interlocked.Increment(ref m_error_points);
              }
              break;
            }
          case ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM:
            {
              MS_ELP01_WS.RedeemVoucher(HeaderType, CustomerNumber, InputData.VoucherId, out RedeemResponse, out NotRedeemResponse, out RestrictedToGroupId, out ResponseCode);
              WsElapsedTime = Misc.GetElapsedTicks(RequestTick);

              if ((GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "ExtendedLog") & 0x20) != 0)
              {
                Interlocked.Add(ref m_redeem_ws_total_elapased, WsElapsedTime);
              }
              OutputData = new Elp01OutputData();
              OutputData.RequestType = ELP01_Requests.ELP01_REQUEST_VOUCHER_REDEEM;
              OutputData.AmountRedeemableCents = RedeemResponse;
              OutputData.AmountNonRedeemableCents = NotRedeemResponse;
              OutputData.RestrictedToGroupId = RestrictedToGroupId;
              OutputData.ResponseCodes = ResponseCode;

              if (ResponseCode == ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK)
              {
                AuditInsert = true;
                Alarm = false;
              }
              else
              {
                if (ResponseCode == ELP01_ResponseCodes.ELP01_RESPONSE_CODE_INCORRECT_VOUCHER)
                {
                  Alarm = false;
                }
                else
                {
                  Interlocked.Increment(ref m_error_redeem);
                }
              }
              break;
            }
          default:
            {
              break;
            }
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Error(string.Format("MultiSite_SiteRequestsProcess > ExecuteELP01ActionRequest: failed. Error: {0}.", _ex.Message));
        ELPLog.Exception(_ex);
      }

    } // ExecuteAction

    //------------------------------------------------------------------------------
    // PURPOSE : Thread que procesa reenvios de informaci�n que fall� anteriormente
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    private static void SitesRequestsResendThread()
    {
      DataTable _responses;
      WWP_ResponseCodes _wwp_rc_response;
      Boolean _is_principal;

      int _wait_hint;

      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          _is_principal = Services.IsPrincipal("WWP CENTER");
          if (_is_principal)
          {
            _wait_hint = 10000;

            continue;
          }

          if (!MS_SiteRequest_Database.SiteRequest_GetRequestsWithResponsePendingToSend(out _responses))
          {
            _wait_hint = 5000;
            Log.Error("SiteRequest_GetRequestsWithResponsePendingToSend failed!");

            continue;
          }

          if (_responses.Rows.Count == 0)
          {
            _wait_hint = 500;
          }
          _wait_hint = 250;

          foreach (DataRow _response in _responses.Rows)
          {
            Trx_ResponseSiteRequestMessage((Int32)_response["SR_SITE_ID"], (Int32)_response["SR_REQUEST_TYPE"], (Int64)_response["SR_MR_UNIQUE_ID"],
                                           (String)_response["SR_INPUT_DATA"], (String)_response["SR_OUTPUT_DATA"], 0, out _wwp_rc_response);
          }
          foreach (DataRow _response in _responses.Rows)
          {
            if (!MS_SiteRequest_Database.SiteRequest_DeleteRequest((Int64)_response["SR_UNIQUE_ID"]))
            {
              Log.Error("MS_SiteRequest_Database.SiteRequest_DeleteRequest Failed.");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    } // SitesRequestsThread


    #region Transactions

    //------------------------------------------------------------------------------
    // PURPOSE : Incomming message from site
    //
    //  PARAMS :
    //      - INPUT : 
    //          - RequestDataTable   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              failed 
    //
    public static Boolean IncommingSiteRequestMessage(Int32 SiteId, DataTable RequestDataTable)
    {
      if (GeneralParam.GetBoolean("ExternalLoyaltyProgram.Mode01", "IgnoreRequestPoints", true))
      {
        Int32 _idx_row;
        DataRow _request;
        String _input;

        _idx_row = RequestDataTable.Rows.Count - 1;
        while (_idx_row >= 0)
        {
          _request = RequestDataTable.Rows[_idx_row];
          _input = (String)_request["MR_INPUT_DATA"];
          if (_input.Contains("ELP01_REQUEST_QUERY_POINTS"))
          {
            RequestDataTable.Rows.Remove(_request);
          }
          _idx_row--;
        }

        if (RequestDataTable.Rows.Count == 0)
        {
          return true;
        }
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!MS_SiteRequest_Database.SiteRequest_InsertRequests(SiteId, RequestDataTable, _db_trx.SqlTransaction))
          {
            Log.Error("MS_SiteRequest_Database.SiteRequest_InsertRequests Failed.");
          }
          else
          {
            _db_trx.Commit();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // IncommingSiteRequestMessage

    //------------------------------------------------------------------------------
    // PURPOSE : MultiSite request Trx
    //
    //  PARAMS :
    //      - INPUT : 
    //          - SiteUniqueId   
    //          - InputData   
    //          - OutputData
    //
    //      - OUTPUT :  
    //          - ResponseCode 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Timeout or failed trx (disconnect?)
    //
    public static Boolean Trx_ResponseSiteRequestMessage(Int32 SiteId, Int32 RequestType, Int64 SiteUniqueId, String InputData, String OutputData, Int32 Timeout, out WWP_ResponseCodes ResponseCode)
    {
      DataTable _request_datatable;
      DataRow _dr;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;

      ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;

      try
      {
        _request_datatable = new DataTable();
        _request_datatable.Columns.Add("SR_MR_UNIQUE_ID", System.Type.GetType("System.Int64"));
        _request_datatable.Columns.Add("SR_REQUEST_TYPE", System.Type.GetType("System.Int32"));
        _request_datatable.Columns.Add("SR_INPUT_DATA", System.Type.GetType("System.String"));
        _request_datatable.Columns.Add("SR_OUTPUT_DATA", System.Type.GetType("System.String"));

        _dr = _request_datatable.NewRow();
        _dr[0] = SiteUniqueId;
        _dr[1] = RequestType;
        _dr[2] = InputData;
        _dr[3] = OutputData;
        _request_datatable.Rows.Add(_dr);

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_request.MsgHeader.SiteId = SiteId;
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_MULTISITE_RESPONSE;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_datatable);

        //
        // Send to Site and wait response
        //
        if (!WSI.WWP_Server.WWP.WWP_Request(_wwp_request, Timeout, out _wwp_response))
        {
          if (Timeout > 0)
          {
            Log.Message("Trx_ResponseSiteRequestMessage: Timeout or Failed! TableType: TABLE_TYPE_MULTISITE_REQUEST");
          }

          return false;
        }

        ResponseCode = _wwp_response.MsgHeader.ResponseCode;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_ResponseSiteRequestMessage

    #endregion // Transactions

  } // static class 
} // namespace

