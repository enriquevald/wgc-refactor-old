//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_SiteRequestsDefinitions.cs
// 
//   DESCRIPTION: Site Requests definitions.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;

namespace WSI.WWP_CenterService.MultiSite
{

  // TODO ... defs, enums, constants, ...
  public enum WWP_SiteRequestStatus
  {
    SITE_REQUEST_STATUS_RECEIVED = 1,
    SITE_REQUEST_STATUS_INPROGRESS = 2,
    SITE_REQUEST_STATUS_OK = 3,
    SITE_REQUEST_STATUS_ERROR = 4,
    SITE_REQUEST_STATUS_TIMEOUT = 5,
    SITE_REQUEST_STATUS_PENDING_RESPONSE = 6 //Retries sending 
  }

  public static class MS_SiteRequest_Definitions
  {

  } // static class 
} // namespace

