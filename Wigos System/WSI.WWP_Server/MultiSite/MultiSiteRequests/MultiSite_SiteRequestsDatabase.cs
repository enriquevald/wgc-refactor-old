//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_SiteRequestsDatabase.cs
// 
//   DESCRIPTION: Site Requests database.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;

namespace WSI.WWP_CenterService.MultiSite
{
  public static class MS_SiteRequest_Database
  {

    #region Public Methods

    // PURPOSE : Delete a Site Requests that has been in process since at least 1 hour and status is not resend
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean SiteRequest_DeleteOldRequests()
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM    SITES_REQUESTS                                      ");
        _sb.AppendLine("       WHERE    SR_STATUS_CHANGED < DATEADD(SECOND, -60, GETDATE()) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);
          }
          _db_trx.Commit();

          return true;
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // MsRequest_DeleteResponsesTimeout

    //------------------------------------------------------------------------------
    // PURPOSE : Get firts 500 Sites Requests whose status is Sending_Failed
    //
    //  PARAMS :
    //      - INPUT:
    //            - None
    //
    //      - OUTPUT :
    //            - RequestDataTable : Data Table with results
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean SiteRequest_GetRequestsWithResponsePendingToSend(out DataTable RequestDataTable)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;
      RequestDataTable = new DataTable();
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   SR_UNIQUE_ID           ");
        _sb.AppendLine("       , SR_SITE_ID             ");
        _sb.AppendLine("       , SR_MR_UNIQUE_ID        ");
        _sb.AppendLine("       , SR_MR_PRIORITY         ");
        _sb.AppendLine("       , SR_REQUEST_TYPE        ");
        _sb.AppendLine("       , SR_INPUT_DATA          ");
        _sb.AppendLine("       , SR_OUTPUT_DATA         ");
        _sb.AppendLine("       , SR_STATUS              ");
        _sb.AppendLine("       , SR_STATUS_CHANGED      ");
        _sb.AppendLine("  FROM   SITES_REQUESTS         ");
        _sb.AppendLine(" WHERE   SR_STATUS = @pSrStatus ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pSrStatus", SqlDbType.Int).Value = WWP_SiteRequestStatus.SITE_REQUEST_STATUS_PENDING_RESPONSE;
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _nr = _sql_da.Fill(RequestDataTable);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_GetRequestsWithResponsePendingToSend

    //------------------------------------------------------------------------------
    // PURPOSE : Update a Site Requests from a datarow. It only updates the non null columns
    //
    //  PARAMS :
    //      - INPUT:
    //            - RequestDataRow: DataRow for update
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.  
    public static Boolean SiteRequest_ResponseReceived(Int64 UniqueId, String XmlOutputData)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        // Update Query
        _sb.AppendLine(" UPDATE   SITES_REQUESTS ");
        _sb.AppendLine("    SET   SR_STATUS         = @pResponseReceived ");
        _sb.AppendLine("        , SR_STATUS_CHANGED = GETDATE () ");
        _sb.AppendLine("        , SR_OUTPUT_DATA    = @pResponse ");
        _sb.AppendLine("  WHERE   SR_UNIQUE_ID      = @pUniqueId");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = UniqueId;
            _sql_cmd.Parameters.Add("@pResponseReceived", SqlDbType.Int).Value = (int)WWP_SiteRequestStatus.SITE_REQUEST_STATUS_PENDING_RESPONSE;
            if (String.IsNullOrEmpty(XmlOutputData))
            {
              _sql_cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pResponse", SqlDbType.NVarChar).Value = XmlOutputData;
            }
            _nr = _sql_cmd.ExecuteNonQuery();
            if (_nr == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_UpdateRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Delete a Site Requests
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId: Request Id for delete
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean SiteRequest_DeleteRequest(Int64 UniqueId)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM    SITES_REQUESTS            ");
        _sb.AppendLine("       WHERE    SR_UNIQUE_ID = @pSrUniqueId    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pSrUniqueId", SqlDbType.BigInt).Value = UniqueId;

            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);

          } //SqlCommand _sql_cmd

          _db_trx.Commit();

          return true;
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_DeleteRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Update Site Requests Status from a datarow.
    //
    //  PARAMS :
    //      - INPUT:
    //            - RequestDataRow : DataRow for update
    //            - NewStatus : New Status to update
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean SiteRequest_UpdateRequestStatus(DataRow RequestDataRow, WWP_SiteRequestStatus NewStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        // Update Query
        _sb.AppendLine(" UPDATE   SITES_REQUESTS                              ");
        _sb.AppendLine("    SET   SR_STATUS           = @pSrStatus            ");
        _sb.AppendLine("        , SR_STATUS_CHANGED   = CASE WHEN SR_STATUS = @pSrStatus               ");
        _sb.AppendLine("                                     THEN SR_STATUS_CHANGED ELSE GETDATE() END ");
        _sb.AppendLine("  WHERE   SR_UNIQUE_ID     = @pSrUniqueId        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // Add parameters
          _sql_cmd.Parameters.Add("@pSrUniqueId", SqlDbType.BigInt).Value = RequestDataRow["SR_UNIQUE_ID"];
          _sql_cmd.Parameters.Add("@pSrStatus", SqlDbType.Int).Value = (Int32)NewStatus;

          _nr = _sql_cmd.ExecuteNonQuery();

          if (_nr == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_UpdateRequestStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Sites Requests from DataTable
    //
    //  PARAMS :
    //      - INPUT:
    //            - RequestDataTable: DataTable for insert
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean SiteRequest_InsertRequests(Int32 SiteId, DataTable RequestDataTable, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _nr;
      Int32 _tick0;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF NOT EXISTS (SELECT 1 FROM SITES_REQUESTS WHERE SR_SITE_ID = @pSiteId AND SR_MR_UNIQUE_ID = @pSrMrUniqueId) ");
        _sb.AppendLine("BEGIN                             ");
        _sb.AppendLine("   INSERT INTO    SITES_REQUESTS  ");
        _sb.AppendLine("                ( SR_SITE_ID      ");
        _sb.AppendLine("                , SR_MR_UNIQUE_ID ");
        _sb.AppendLine("                , SR_MR_PRIORITY  ");
        _sb.AppendLine("                , SR_REQUEST_TYPE ");
        _sb.AppendLine("                , SR_INPUT_DATA   ");
        _sb.AppendLine("                , SR_STATUS)      ");
        _sb.AppendLine("        VALUES  ( @pSiteId        ");
        _sb.AppendLine("                , @pSrMrUniqueId  ");
        _sb.AppendLine("                , @pSrMrPriority  ");
        _sb.AppendLine("                , @pSrRequestType ");
        _sb.AppendLine("                , @pSrInputData   ");
        _sb.AppendLine("                , @pStatus       )");
        _sb.AppendLine("END                               ");
        _sb.AppendLine("ELSE                              ");
        _sb.AppendLine("BEGIN                             ");
        _sb.AppendLine("  UPDATE SITES_REQUESTS SET SR_STATUS = @pStatus, SR_STATUS_CHANGED = GETDATE() WHERE SR_SITE_ID = @pSiteId AND SR_MR_UNIQUE_ID = @pSrMrUniqueId");
        _sb.AppendLine("END                               ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)WWP_SiteRequestStatus.SITE_REQUEST_STATUS_RECEIVED;

            _sql_cmd.Parameters.Add("@pSrMrUniqueId", SqlDbType.BigInt).SourceColumn = "MR_UNIQUE_ID";
            _sql_cmd.Parameters.Add("@pSrMrPriority", SqlDbType.Int).SourceColumn = "MR_PRIORITY";
            _sql_cmd.Parameters.Add("@pSrRequestType", SqlDbType.Int).SourceColumn = "MR_REQUEST_TYPE";
            _sql_cmd.Parameters.Add("@pSrInputData", SqlDbType.NVarChar).SourceColumn = "MR_INPUT_DATA";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.InsertCommand = _sql_cmd;
              _nr = _sql_da.Update(RequestDataTable);
              // Don't check the number of inserted rows
              _db_trx.Commit();

              return true;
            }
          } // SqlCommand
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_InsertRequests

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Update and Get two tables with next information:
    //              - Table 0: first 500 Requests whose Priority is higth
    //              - Table 1: first 500 Requests whose Priority is normal
    //
    //  PARAMS :
    //      - INPUT:
    //            - SiteId : Site filer
    //            - NewStatus : New Status to aply in update
    //            - OriginalStatus : Status filter
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - RequestsDataSet : DataSet with results
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    public static Boolean SiteRequest_PendingToInProgress(out DataSet RequestsDataSet)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;
      RequestsDataSet = new DataSet();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE SITES_REQUESTS                        ");
        _sb.AppendLine("      SET    SR_STATUS          = @pStatusInProgress ");
        _sb.AppendLine("          ,  SR_STATUS_CHANGED  = GETDATE()   ");
        _sb.AppendLine("     OUTPUT  INSERTED.SR_UNIQUE_ID            ");
        _sb.AppendLine("          ,  INSERTED.SR_SITE_ID              ");
        _sb.AppendLine("          ,  INSERTED.SR_MR_UNIQUE_ID         ");
        _sb.AppendLine("          ,  INSERTED.SR_MR_PRIORITY          ");
        _sb.AppendLine("          ,  INSERTED.SR_REQUEST_TYPE         ");
        _sb.AppendLine("          ,  INSERTED.SR_INPUT_DATA           ");
        _sb.AppendLine("          ,  INSERTED.SR_OUTPUT_DATA          ");
        _sb.AppendLine("          ,  INSERTED.SR_STATUS               ");
        _sb.AppendLine("          ,  INSERTED.SR_STATUS_CHANGED       ");
        _sb.AppendLine("     WHERE   SR_STATUS = @pStatusReceived     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pStatusInProgress", SqlDbType.Int).Value = (Int32)WWP_SiteRequestStatus.SITE_REQUEST_STATUS_INPROGRESS;
            _sql_cmd.Parameters.Add("@pStatusReceived", SqlDbType.Int).Value = (Int32)WWP_SiteRequestStatus.SITE_REQUEST_STATUS_RECEIVED;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _nr = _sql_da.Fill(RequestsDataSet);

              _db_trx.Commit();

              return true;
            }
          } // SqlCommand
        }
      } // Try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // SiteRequest_GetUpdatingRequestsByStatus

    #endregion // Private Methods

  } // static class  
} // namespace
