//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_CenterPoints.cs
//
//   DESCRIPTION : MultiSite_CenterPoints class
//
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 05-MAY-2013
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-MAY-2013 JCM    First release.
// 09-MAY-2013 DDM    Fixed bug #779.
// 03-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronización MultiSite: Tablas Customer Buckets
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;
using WSI.WWP;

namespace WSI.WWP_CenterService.MultiSite
{
  static class MultiSite_CenterPoints
  {
    private const Int16 REPLY_AM_MOVEMENT_ID = 0;
    private const Int16 REPLY_AM_STATUS = 1;
    private const Int16 REPLY_AM_ACCOUNT_ID = 2;
    private const Int16 REPLY_AC_MS_POINTS_SEQ_ID = 3;
    private const Int16 REPLY_AC_POINTS = 4;
    private const Int16 REPLY_CBU_BUCKET_ID = 5;

    //------------------------------------------------------------------------------
    // PURPOSE : Update Accounts Points Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public Boolean Update_PointsAccountMovements(Int32 SiteId, DataTable RequestAccountMovements, out DataTable ReplyAccountMovements)
    {
      StringBuilder _sb = new StringBuilder();
      SqlParameter _p;
      DataRow _dr_reply;
      Boolean _ok;
      Int32 _updated;

      _ok = false;

      // Add output parameters
      RequestAccountMovements.Columns.Add("R_AM_MOVEMENT_ID", Type.GetType("System.Int64"));
      RequestAccountMovements.Columns.Add("R_AM_STATUS", Type.GetType("System.Int32"));
      RequestAccountMovements.Columns.Add("R_AM_ACCOUNT_ID", Type.GetType("System.Int64"));
      RequestAccountMovements.Columns.Add("R_AC_MS_POINTS_SEQ_ID", Type.GetType("System.Int64"));
      RequestAccountMovements.Columns.Add("R_AC_POINTS", Type.GetType("System.Decimal"));
      RequestAccountMovements.Columns.Add("R_CBU_BUCKET_ID", Type.GetType("System.Int64"));
      RequestAccountMovements.Columns.Add("R_CENTER_INITIAL_AC_POINTS", Type.GetType("System.Decimal"));

      ReplyAccountMovements = new DataTable("REPLY_ACCOUNT_MOVEMENTS");
      ReplyAccountMovements.Columns.Add("AM_MOVEMENT_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AM_STATUS", Type.GetType("System.Int32"));
      ReplyAccountMovements.Columns.Add("AM_ACCOUNT_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AC_MS_POINTS_SEQ_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AC_POINTS", Type.GetType("System.Decimal"));
      ReplyAccountMovements.Columns.Add("CBU_BUCKET_ID", Type.GetType("System.Int64"));

      ReplyAccountMovements.PrimaryKey = new DataColumn[] { ReplyAccountMovements.Columns["AM_MOVEMENT_ID"]
                                                          , ReplyAccountMovements.Columns["AM_ACCOUNT_ID"]
                                                          , ReplyAccountMovements.Columns["CBU_BUCKET_ID"] };

      if (RequestAccountMovements.Rows.Count == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_PointsAccountMovement ");
        _sb.AppendLine("  @pSiteId ");
        _sb.AppendLine(", @pMovementId ");
        _sb.AppendLine(", @pPlaySessionId ");
        _sb.AppendLine(", @pAccountId ");
        _sb.AppendLine(", @pTerminalId ");
        _sb.AppendLine(", @pWcpSequenceId ");
        _sb.AppendLine(", @pWcpTransactionId ");
        _sb.AppendLine(", @pDatetime ");
        _sb.AppendLine(", @pType ");
        _sb.AppendLine(", @pInitialBalance ");
        _sb.AppendLine(", @pSubAmount ");
        _sb.AppendLine(", @pAddAmount  ");
        _sb.AppendLine(", @pFinalBalance ");
        _sb.AppendLine(", @pCashierId ");
        _sb.AppendLine(", @pCashierName ");
        _sb.AppendLine(", @pTerminalName ");
        _sb.AppendLine(", @pOperationId ");
        _sb.AppendLine(", @pDetails ");
        _sb.AppendLine(", @pReasons ");
        _sb.AppendLine(", @pPlayerTrackingMode ");
        //Output parameters
        _sb.AppendLine(", @pOutputStatus         		    OUTPUT ");
        _sb.AppendLine(", @pOutputAcMsSeqId      		    OUTPUT ");
        _sb.AppendLine(", @pOutputPoints         		    OUTPUT ");
        _sb.AppendLine(", @pOutputCenterInitialBalance  OUTPUT ");
        _sb.AppendLine(", @pOutputBucketID       		    OUTPUT ");

        // TODO: Check if these movements must update ac_holder_level
        //  43 'HolderLevelChanged'           
        //  62 'ManualHolderLevelChanged'
        //

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Parameters
            _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
            _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).SourceColumn = "AM_MOVEMENT_ID";
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "AM_PLAY_SESSION_ID";
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AM_ACCOUNT_ID";
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "AM_TERMINAL_ID";
            _cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "AM_WCP_SEQUENCE_ID";
            _cmd.Parameters.Add("@pWcpTransactionId", SqlDbType.BigInt).SourceColumn = "AM_WCP_TRANSACTION_ID";
            _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).SourceColumn = "AM_DATETIME";
            _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "AM_TYPE";
            _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "AM_INITIAL_BALANCE";
            _cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "AM_SUB_AMOUNT";
            _cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "AM_ADD_AMOUNT";
            _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "AM_FINAL_BALANCE";
            _cmd.Parameters.Add("@pCashierId", SqlDbType.Int).SourceColumn = "AM_CASHIER_ID";
            _cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).SourceColumn = "AM_CASHIER_NAME";
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).SourceColumn = "AM_TERMINAL_NAME";
            _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "AM_OPERATION_ID";
            _cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).SourceColumn = "AM_DETAILS";
            _cmd.Parameters.Add("@pReasons", SqlDbType.NVarChar, 64).SourceColumn = "AM_REASONS";
            _cmd.Parameters.Add("@pPlayerTrackingMode", SqlDbType.Int).Value = CommonMultiSite.GetPlayerTrackingMode();

            _p = _cmd.Parameters.Add("@pOutputStatus", SqlDbType.Int, 4, "R_AM_STATUS");
            _p.Direction = ParameterDirection.Output;
            _p = _cmd.Parameters.Add("@pOutputAcMsSeqId", SqlDbType.BigInt, 8, "R_AC_MS_POINTS_SEQ_ID");
            _p.Direction = ParameterDirection.Output;
            _p = _cmd.Parameters.Add("@pOutputPoints", SqlDbType.Money, 16, "R_AC_POINTS");
            _p.Direction = ParameterDirection.Output;
            _p = _cmd.Parameters.Add("@pOutputCenterInitialBalance", SqlDbType.Money, 16, "R_CENTER_INITIAL_AC_POINTS");
            _p.Direction = ParameterDirection.Output;
            _p = _cmd.Parameters.Add("@pOutputBucketID", SqlDbType.BigInt, 8, "R_CBU_BUCKET_ID");
            _p.Direction = ParameterDirection.Output;

            using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
            {
              _sql_adapt.InsertCommand = _cmd;
              _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
              _sql_adapt.UpdateBatchSize = 500;
              _updated = _sql_adapt.Update(RequestAccountMovements);
              if (_updated == RequestAccountMovements.Rows.Count)
              {
                _db_trx.Commit();

                _ok = true;
              }
              else
              {
                Log.Warning(String.Format("Not updated account movements on SiteId {0}. Expected: {1}, Updated: {2}", SiteId.ToString(), RequestAccountMovements.Rows.Count, _updated));

                _db_trx.Rollback();
              }
            }
          }
        }

        return _ok;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        _ok = false;
      }
      finally
      {
        try
        {
          foreach (DataRow _request_dr in RequestAccountMovements.Rows)
          {
            if (_request_dr.HasErrors)
            {
              Log.Warning("Update_PointsAccountMovements not updated, errors have occurred on SiteId:" + SiteId.ToString() + ". RowError: " + _request_dr.RowError + "\r\n"
                        + " Movement_id:" + _request_dr[0].ToString() + ", AccountId:" + _request_dr[2].ToString() + ", Status:" + _request_dr[1].ToString());
            }

            _dr_reply = ReplyAccountMovements.NewRow();
            _dr_reply[REPLY_AM_MOVEMENT_ID] = _request_dr["AM_MOVEMENT_ID"];
            _dr_reply[REPLY_AM_ACCOUNT_ID] = _request_dr["AM_ACCOUNT_ID"];
            _dr_reply[REPLY_CBU_BUCKET_ID] = _request_dr.IsNull("R_CBU_BUCKET_ID") ? 0 : _request_dr["R_CBU_BUCKET_ID"];

            if (_request_dr.IsNull("R_AM_STATUS"))
            {
              _dr_reply[REPLY_AM_STATUS] = 1; // Error Retry
              _dr_reply[REPLY_AC_MS_POINTS_SEQ_ID] = DBNull.Value;
              _dr_reply[REPLY_AC_POINTS] = DBNull.Value;
            }
            else
            {
              if (_request_dr.IsNull("R_AC_POINTS") || _request_dr.IsNull("R_AC_MS_POINTS_SEQ_ID"))
              {
                _dr_reply[REPLY_AM_STATUS] = 1; // Error Retry
                _dr_reply[REPLY_AC_MS_POINTS_SEQ_ID] = DBNull.Value;
                _dr_reply[REPLY_AC_POINTS] = DBNull.Value;

                Log.Warning(" Update_PointsAccountMovements AC_POINTS is NULL. " + " MovementId:" + _request_dr[0].ToString() + ", AccountId:" + _request_dr[2].ToString());
              }
              else
              {
                _dr_reply[REPLY_AM_STATUS] = _request_dr["R_AM_STATUS"];
                _dr_reply[REPLY_AC_MS_POINTS_SEQ_ID] = _request_dr["R_AC_MS_POINTS_SEQ_ID"];
                _dr_reply[REPLY_AC_POINTS] = _request_dr["R_AC_POINTS"];
                _dr_reply[REPLY_CBU_BUCKET_ID] = _request_dr["R_CBU_BUCKET_ID"];
              }
            }

            if ((int)_dr_reply[REPLY_AM_STATUS] == 0 && (Decimal) _dr_reply[REPLY_AC_POINTS] < 0)
            {
              if (((Decimal)_request_dr["R_CENTER_INITIAL_AC_POINTS"]) >= 0 && ((Decimal)_request_dr["AM_ADD_AMOUNT"] - (Decimal)_request_dr["AM_SUB_AMOUNT"]) < 0)
              {
                Alarm.Register(AlarmSourceCode.MultiSite, 0, Environment.MachineName + @"\\" + MultiSiteCenterCommon.SERVICE_NAME
                              , AlarmCode.MultiSite_AccountWithNegativePoints
                              , Resource.String(Alarm.ResourceId(AlarmCode.MultiSite_AccountWithNegativePoints)
                                               , ((Int64)_request_dr["AM_ACCOUNT_ID"]).ToString()
                                               , ((Decimal)_request_dr["R_AC_POINTS"]).ToString(Common.Gift.FORMAT_POINTS)));
              }
            }

            ReplyAccountMovements.Rows.Add(_dr_reply);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
      return _ok;
    } // Update_PointsAccountMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Update Accounts Points Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteId
    //          - SequenceId
    //
    //      - OUTPUT :
    //          - ReplyAccountMovements
    //
    // RETURNS :
    //
    static public Boolean Read_PendingAccountPoints(Int32 CallingSiteId, Boolean OnlyCallingSite, Int64 SequenceId, out DataTable ReplyAccountMovements)
    {
      StringBuilder _sb;
      Int32 _max_rows;

      ReplyAccountMovements = new DataTable("REPLY_ACCOUNT_MOVEMENTS");
      ReplyAccountMovements.Columns.Add("AM_MOVEMENT_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AM_STATUS", Type.GetType("System.Int32"));
      ReplyAccountMovements.Columns.Add("AM_ACCOUNT_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AC_MS_POINTS_SEQ_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.Columns.Add("AC_POINTS", Type.GetType("System.Decimal"));
      ReplyAccountMovements.Columns.Add("CBU_BUCKET_ID", Type.GetType("System.Int64"));
      ReplyAccountMovements.PrimaryKey = new DataColumn[] {   ReplyAccountMovements.Columns["AM_MOVEMENT_ID"]
                                                            , ReplyAccountMovements.Columns["AM_ACCOUNT_ID"] 
                                                            , ReplyAccountMovements.Columns["CBU_BUCKET_ID"] };
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_CenterMaxRowsOfTask(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading. Read_CenterMaxRowsOfTask.");

            return false;
          }
          _sb = new StringBuilder();

          _sb.AppendLine("SELECT   TOP (@pMAXRows) ");
          _sb.AppendLine("         0 AS AM_MOVEMENT_ID ");            // REPLY_AM_MOVEMENT_ID --> 0
          _sb.AppendLine("       , 0 AS AM_STATUS ");                 // REPLY_AM_STATUS --> 0: OK
          _sb.AppendLine("       , AC_ACCOUNT_ID AS AM_ACCOUNT_ID "); // REPLY_AM_ACCOUNT_ID 
          _sb.AppendLine("       , AC_MS_POINTS_SEQ_ID ");            // REPLY_AC_MS_POINTS_SEQ_ID 
          _sb.AppendLine("       , CBU_VALUE AS AC_POINTS ");         // REPLY_AC_POINTS = 4;
          _sb.AppendLine("       , CBU_BUCKET_ID");                   // REPLY_CBU_BUCKET_ID = 5;
          _sb.AppendLine("  FROM   ACCOUNTS WITH(INDEX(IX_ms_points_seq_id))");
          _sb.AppendLine("  JOIN   CUSTOMER_BUCKET on CBU_CUSTOMER_ID = AC_ACCOUNT_ID");
		  //TODO: JOIN BUCKETS and left outer CUSTOMER
          _sb.AppendLine(" WHERE   AC_MS_POINTS_SEQ_ID > @pSeqId ");

          if (OnlyCallingSite)
          {
            _sb.AppendLine(" AND   AC_MS_LAST_SITE_ID = @pSiteId ");
          }

          _sb.AppendLine(" ORDER   BY AC_MS_POINTS_SEQ_ID ASC, AC_ACCOUNT_ID ");
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = _max_rows;
            _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceId;
            _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = CallingSiteId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, ReplyAccountMovements);
            }

            if (!OnlyCallingSite)
            {
              WWP_MultiSiteTask.Center_SetSiteSequence(CallingSiteId, TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All, SequenceId, _db_trx.SqlTransaction);
            }

            _db_trx.Commit();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Read_PendingAccountPoints

  }
}
