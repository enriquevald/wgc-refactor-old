//------------------------------------------------------------------------------
// Copyright © 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WWP Center to run as a service
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2011 ACC    First release.
// 29-JUL-2014 RRR    Fixed Bug WIG-1128: Added ELP enabled control to add new ELPLog
// 01-FEB-2016 AMF    PBI 8272: Space Requests
// 10-MAR-2016 FGB    PBI 10130: Multiple Buckets: Sincronización MultiSite: Kernel - Validar Expiración y Cambio Nivel
// 02-FEB-2018 OMC    Added new thread to toggle triggers related to services based on GeneralParam enabled.
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Net;
using WSI.Common;
using WSI.WWP_Server;
using WSI.WWP_CenterService;
using System.Threading;
using WSI.WWP_CenterService.MultiSite;

public partial class WWP_CenterService : CommonService
{
  private const string VERSION = "18.160";
  private const string SVC_NAME = "WWP_CenterService";
  private const string PROTOCOL_NAME = "WWP CENTER";
  private const int SERVICE_PORT = 13003;
  private const Boolean STAND_BY = false;
  private const string LOG_PREFIX = "WWP_CENTER";
  private const string CONFIG_FILE = "WSI.WWP_Server_Config";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE;

  private static WWP_Server m_server;

  public static void Main(String[] Args)
  {
    WWP_CenterService _this_service;

    _this_service = new WWP_CenterService();
    _this_service.Run(Args);
  } // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
  } // SetParameters



  //------------------------------------------------------------------------------
  // PURPOSE : Starts the service.
  //
  //  PARAMS :
  //      - INPUT :
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  protected override void OnServiceRunning(IPEndPoint ListeningOn)
  {
    // Start WWP Client
    WWP_Client.Init();
    WWP_Client.Start();

    // Start WWP Pending Enrolls Queue
    WWP_PendingEnrollsQueue.Init();
    WWP_PendingEnrollsQueue.Start();

    // Init BatchUpdate    
    WSI.Common.BatchUpdate.TerminalSession.Init("WWP",
                                            (Int32)WWP_SessionStatus.Opened,
                                            (Int32)WWP_SessionStatus.Abandoned,
                                            (Int32)WWP_SessionStatus.Disconnected,
                                            (Int32)WWP_SessionStatus.Timeout);

    m_server = new WWP_Server(ListeningOn);
    WSI.WWP_Server.WWP.Server = m_server;
    m_server.Start();

    // Start MultiSite Requests
    MS_SiteRequest_Process.Init();

    // Start Exchange tables process for Elp01 mode
    WSI.WWP_CenterService.MultiSite.MS_ELP01_ExchangeTables.Init();

    Log.Message("WWP_Server listening on: " + ListeningOn.ToString());
    Log.Message("");
    Log.Message("");

    // OMC 09-FEB-2018
    WSI.Common.TriggersValidation.TriggersValidation.Init();

    // RRR 29-JUL-2014: Start a Simple ELP Logger
    if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 1)
    {
      ELPLog.AddListener(new SimpleLogger("ELP_LOG"));
    }

    // DDM & AJQ, Start MultiSite Jobs 
    WWP_MultiSiteJobs.Start();

    // Start: Expire points Day Month
    // AMF 19-SEP-2015: BackLog 3685
    // FGB 10-MAR-2016: PBI 10130: Multiple Buckets: Sincronización MultiSite: Kernel - Validar Expiración y Cambio Nivel
    //WWP_AccountsPointsExpirationDayMonth.Init();
    WWP_BucketsExpirationDayMonth.Init();
    WWP_BucketsExpirationDayMonth.Start();

    // JCA 21-GEN-2016
    //Start ELP WebService
    WWP_WebServices.Init();

  } // OnServiceRunning

  //------------------------------------------------------------------------------
  // PURPOSE : Stops the service.
  //
  //  PARAMS :
  //      - INPUT :
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  protected override void OnServiceStopped()
  {
    // Disconnect all clients
    foreach (SecureTcpClient _client in m_server.Clients)
    {
      WSI.Common.BatchUpdate.TerminalSession.CloseSession(_client.InternalId,
                                                          _client.SessionId,
                                                          (int)WWP_SessionStatus.Disconnected);
    }

    // Wait for update wwp sessions in database...
    Thread.Sleep(1100);
  }

} // WWP_CenterService


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under system account
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //Assembly asembl;
    //asembl = Assembly.GetExecutingAssembly();
    //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

    //The service has a fixed name.
    serviceInstaller.ServiceName = "WWP_CenterService";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //Assembly asembl;
    //asembl = Assembly.GetExecutingAssembly();
    //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

    //The service has a fixed name.
    serviceInstaller.ServiceName = "WWP_CenterService";
  }
}
