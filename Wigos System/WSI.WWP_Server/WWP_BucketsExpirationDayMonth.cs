﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WWP_BucketsExpirationDayMonth.cs
//
//   DESCRIPTION: WWP_BucketsExpirationDayMonth class
//
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 10-MAR-2016

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-MAR-2016 FGB    First release. PBI 10130: Multiple Buckets: Sincronización MultiSite: Kernel - Validar Expiración y Cambio Nivel
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Messaging;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using WSI.Common;
using WSI.Common.TITO;

namespace WSI.WWP_CenterService
{
  public static class WWP_BucketsExpirationDayMonth
  {
    #region Attributes
    private static Thread m_thread;
    #endregion Attributes

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WWP_BucketsExpirationDayMonth class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      m_thread = new Thread(BucketsExpirationDayMonthThread);
      m_thread.Name = "BucketsExpirationDayMonthThread";
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }
    #endregion Public Methods

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void BucketsExpirationDayMonthThread()
    {
      DateTime _now;
      int _wait_hint;
      Boolean _is_multisite_center;

      while (true)
      {
        _now = WGDB.Now;
        _wait_hint = 60000 - (_now.Second * 1000) - _now.Millisecond;

        Thread.Sleep(_wait_hint);

        _is_multisite_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);
        if (!_is_multisite_center)
        {
          continue;
        }

        // Only the service running as 'Principal' will execute the tasks.
        if (!Services.IsPrincipal("WWP CENTER"))
        {
          continue;
        }

        if (CommonMultiSite.GetPlayerTrackingMode() == PlayerTracking_Mode.Site)
        {
          continue;
        }

        try
        {
          if (!WSI.Common.BucketsExpirationDayMonth.ProcessBucketsExpirationDayMonth())
          {
            Log.Warning("WSI.Common.BucketsExpirationDayMonth.ProcessBucketsExpirationDayMonth");
          }
        }
        catch (Exception _ex)
        {
          Log.Warning("WWP_BucketsExpirationDayMonth.BucketsExpirationDayMonthThread. Exception in function: ProcessBucketsExpirationDayMonth");
          Log.Exception(_ex);
        }
      }
    } // BucketsExpirationDayMonthThread
    #endregion Private Methods
  } // WWP_BucketsExpirationDayMonth
} // WSI.WWP_CenterService
