﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSI.WWP_CenterService.WigosService
{
  public class SharedDefinitions
  {
    public enum CreditType // Indica el tipo de crédito "Cashable", "NonCashable", "Tables"
    {
      Cashable = 0,
      NonCashable = 1,
      Tables = 2
    }

    //NOTA: Si envian un transactionID existente, se debe verificar que los datos son los mismos: SI: Retorno OK, NO (Datos diferentes): ERROR transaction EXISTS

    public enum Status
    {
      OK_TRANSFERRED = 1,
      OK_ALREADY_TRANSFERRED = 2,

      ERROR_CREDENTIALS = 10,

      ERROR_ACCOUNT_INFO = 20,
      ERROR_AMOUNT = 21,
      ERROR_NON_CASHABLE_GROUP = 22,
      ERROR_ACCOUNT_BLOCKED = 23,

      ERROR_TRANSACTIONID_ALREADY_EXISTS = 30, // Same TransactionID different Amount|Account|CreditType

      ERROR_SITE_NOT_CONNECTED = 100,


      ERROR_UNKNOWN_ERROR = 0,
      ERROR_INTERNAL_SERVER_ERROR = 200,
      ERROR_INTERNAL_SERVER_NOT_ALLOWED = 201
    }
  }
}
