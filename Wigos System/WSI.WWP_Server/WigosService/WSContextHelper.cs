﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using WSI.Common;

namespace WSI.WWP_CenterService.WigosService
{

  public class Login
  {
    public string User { get; set; }
    public string Password { get; set; }
  }

  public class WSContextHelper
  {
    public static Login GetLoginFromHeader(OperationContext Context, String UserIdIdentifier, String PasswordIdentifier)
    {
      RequestContext _requestcontext;
      MessageHeaders _headers;
      int _headerIndexUid;
      int _headerIndexPass;
      String _user;
      String _pass;

      try
      {
        _requestcontext = Context.RequestContext;
        _headers = _requestcontext.RequestMessage.Headers;

        _headerIndexUid = _headers.FindHeader(UserIdIdentifier, string.Empty);
        _headerIndexPass = _headers.FindHeader(PasswordIdentifier, string.Empty);
        if (_headerIndexPass == -1 || _headerIndexUid == -1)
        {
          return new Login();
        }

        _user = _headers.GetHeader<string>(_headerIndexUid);
        _pass = _headers.GetHeader<string>(_headerIndexPass);

        return new Login() { User = _user, Password = _pass };
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
        return new Login();
      }
    }

  }
}
