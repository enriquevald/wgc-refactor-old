﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WSI.WWP_CenterService.WigosService.Request;
using WSI.WWP_CenterService.WigosService.Response;

namespace WIN.WigosService
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWigosService" in both code and config file together.
  [ServiceContract]
  public interface IWigosService
  {

    [OperationContract]
    CreditResponse Credit(CreditRequest value);

  }
}
