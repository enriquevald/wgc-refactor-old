﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WIN.WigosService;
using WSI.Common;
using WSI.WWP_CenterService.WigosService.Request;
using WSI.WWP_CenterService.WigosService.Response;

namespace WSI.WWP_CenterService.WigosService
{
   //public delegate void MessageEventHandler(object sender, EventArgs e);
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WigosService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select WigosService.svc or WigosService.svc.cs at the Solution Explorer and start debugging.
  [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
  public class WigosService : IWigosService
  {
    //public event MessageEventHandler MessageReceived;
    public WigosService()
    {
      
    }
    public CreditResponse Credit(CreditRequest Value)
    {
      if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 1)
      {
        return new CreditResponse() { Status = SharedDefinitions.Status.ERROR_INTERNAL_SERVER_NOT_ALLOWED, TransactionId = Value.TransactionId };
      }
      
      //if (this.MessageReceived != null)
      //{
      //  this.MessageReceived(this, EventArgs.Empty);
      //}

      return RequestProcess.Canje(Value);
    }


  }
}

