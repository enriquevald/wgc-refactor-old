﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;

namespace WSI.WWP_CenterService.WigosService.Response
{
  [MessageContract]
  [DataContract]
  [Serializable]
  public class CreditResponse
  {
    [MessageBodyMember(Order = 0)]
    [DataMember]
    [XmlElementAttribute(IsNullable = false)]
    public String TransactionId { get; set; }               // Identificador único de la transacción (podría ser el voucher-id de Space)

    [MessageBodyMember(Order = 1)]
    [DataMember]
    [XmlElementAttribute(IsNullable = false)]
    public SharedDefinitions.Status Status { get; set; }    // 0:Normal, 1: Rollback

    [MessageBodyMember(Order = 2)]
    [DataMember]
    [XmlElementAttribute(IsNullable = false)]
    public String Description { get; set; }                 // Descripción

  }
}
