﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ProcessMessage.cs
// 
//   DESCRIPTION: Process Request from SPACE
// 
//        AUTHOR: Joaquin Calero / Alberto Marcos
// 
// CREATION DATE: 25-JAN-2016
// 
// REVISION HISTORY:
// 
// Date        Author    Description
// ----------- --------- -------------------------------------------------------
// 25-JAN-2016 JCA & AMF First release.
// 15-FEB-2016 AMF       PBI 9361: Dont recharge account blocked
// 29-FEB-2016 AMF       Bug 10118:Canjes SPACE: Envía AccountId a 0
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using WSI.Common;
using WSI.Common.Utilities;
using WSI.WWP;
using WSI.WWP_CenterService.MultiSite;
using WSI.WWP_CenterService.WigosService.Request;
using WSI.WWP_CenterService.WigosService.Response;

namespace WSI.WWP_CenterService.WigosService
{
  public static class RequestProcess
  {

    #region " Internal Methods "

    /// <summary>
    /// Canje SPACE
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    internal static CreditResponse Canje(CreditRequest Value)
    {
      CreditResponse _response;
      Login _login;
      Boolean _connected_to_me;
      Array _clients;
      MS_ELP01_ExchangeTables.ELP01_SPACE_REQUEST _space_request;
      DataTable _request_datatable;
      DataRow _dr;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      Int32 _aux;

      if (GeneralParam.GetBoolean("PlayerTracking.ExternalLoyaltyProgram", "WebService.LogEnabled", false))
      {
        ELPLog.Message(SerializeObject(Value));
      }

      _response = new CreditResponse() { TransactionId = Value.TransactionId, Status = SharedDefinitions.Status.ERROR_UNKNOWN_ERROR };
      _login = new Login() { User = "", Password = "" };

      try
      {
        _response.TransactionId = Value.TransactionId;

        _login = WSContextHelper.GetLoginFromHeader(OperationContext.Current, "UserId", "Password");

        if (!ValidateUser(_response, _login))
        {
          _response.Status = SharedDefinitions.Status.ERROR_CREDENTIALS;

          return _response;
        }

        _connected_to_me = false;
        _clients = WWP_Server.WWP.Server.Clients.ToArray();

        foreach (SecureTcpClient _client in _clients)
        {
          if (_client.InternalId == Value.SiteId)
          {
            _connected_to_me = true;
            break;
          }
        }

        if (!_connected_to_me)
        {
          _response.Status = SharedDefinitions.Status.ERROR_SITE_NOT_CONNECTED;

          return _response;
        }

        if (Value.AccountId == 0 && Value.ExternalCustomerId == 0 && String.IsNullOrWhiteSpace(Value.Trackdata))
        {
          ELPLog.Error("RequestProcess.Canje: Account data missing");

          _response.Status = SharedDefinitions.Status.ERROR_ACCOUNT_INFO;
          _response.Description = "Account data missing";

          return _response;
        }

        if (Value.AmountCents <= 0)
        {
          ELPLog.Error("RequestProcess.Canje: Amount incorrect");

          _response.Status = SharedDefinitions.Status.ERROR_AMOUNT;
          _response.Description = " Amount incorrect";

          return _response;
        }

        _space_request.AccountId = Value.AccountId;
        _space_request.ExternalCustomerId = Value.ExternalCustomerId;
        _space_request.Trackdata = Value.Trackdata;
        _space_request.Blocked = false;

        //Check correct account values and fill struct
        if (!MS_ELP01_ExchangeTables.CheckSpaceRequest(ref _space_request))
        {
          if (_space_request.Blocked)
          {
            ELPLog.Error("RequestProcess.Canje: Account blocked");

            _response.Status = SharedDefinitions.Status.ERROR_ACCOUNT_BLOCKED;
            _response.Description = "Account blocked";
          }
          else
          {
            ELPLog.Error("RequestProcess.Canje: Account data incorrect");

            _response.Status = SharedDefinitions.Status.ERROR_ACCOUNT_INFO;
            _response.Description = "Account data incorrect";
          }

          return _response;
        }

        _request_datatable = new DataTable();
        _request_datatable.Columns.Add("SITE_ID", System.Type.GetType("System.Int64"));
        _request_datatable.Columns.Add("ACCOUNT_ID", System.Type.GetType("System.Int64"));
        _request_datatable.Columns.Add("CREDIT_TYPE", System.Type.GetType("System.Int16"));
        _request_datatable.Columns.Add("AMOUNT", System.Type.GetType("System.Int64"));
        _request_datatable.Columns.Add("GROUP_ID", System.Type.GetType("System.Int32"));
        _request_datatable.Columns.Add("TRANSACTION_ID", System.Type.GetType("System.String"));

        _dr = _request_datatable.NewRow();
        _dr[0] = Value.SiteId;
        _dr[1] = _space_request.AccountId;
        _dr[2] = Value.CreditType;
        _dr[3] = Value.AmountCents;
        _dr[4] = Value.NonCashableGroupId;
        _dr[5] = Value.TransactionId;

        _request_datatable.Rows.Add(_dr);

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_request.MsgHeader.SiteId = Value.SiteId;
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_SPACE;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_datatable);

        if (!WSI.WWP_Server.WWP.WWP_Request(_wwp_request, out _wwp_response))
        {
          ELPLog.Error("Error WWP_Request");
        }

        // SPACE: WWP_ResponseCodes - 300
        _aux = (Int32)_wwp_response.MsgHeader.ResponseCode;
        _aux -= 300;
        _response.Status = (SharedDefinitions.Status)_aux;

        return _response;
      }
      catch (Exception _ex)
      {
        //GestorLogs.LogException(Ex.Message, Ex);
        ELPLog.Exception(_ex);

        _response.Status = SharedDefinitions.Status.ERROR_INTERNAL_SERVER_ERROR;

        return _response;
      }
      finally
      {
        if (GeneralParam.GetBoolean("PlayerTracking.ExternalLoyaltyProgram", "WebService.LogEnabled", false))
        {
          ELPLog.Message(SerializeObject(_response));
        }
      }
    } // Canje

    #endregion " Internal Methods "

    #region " Private Methods "

    /// <summary>
    /// Validate user login
    /// </summary>
    /// <param name="_response"></param>
    /// <param name="_login"></param>
    /// <returns></returns>
    private static Boolean ValidateUser(CreditResponse _response, Login _login)
    {
      if (String.IsNullOrWhiteSpace(_login.User) || String.IsNullOrWhiteSpace(_login.Password))
      {
        return false;
      }

      if (_login.User != GeneralParam.GetString("PlayerTracking.ExternalLoyaltyProgram", "WebService.UserId", "resu@user") || _login.Password != GeneralParam.GetString("PlayerTracking.ExternalLoyaltyProgram", "WebService.Password", "2016@reSu"))
      {
        return false;
      }

      return true;
    } // ValidateUser

    /// <summary>
    /// Deserialize objet
    /// </summary>
    /// <param name="Xml"></param>
    /// <param name="ObjectToDeserialize"></param>
    /// <returns></returns>
    private static Object DeserializeObject(String Xml, Object ObjectToDeserialize)
    {
      MemoryStream _mem_stream;

      try
      {
        XmlSerializer _serializer = new XmlSerializer(ObjectToDeserialize.GetType());
        _mem_stream = new MemoryStream(Encoding.UTF8.GetBytes(Xml));

        return _serializer.Deserialize(_mem_stream);
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return null;
    } // DeserializeObject

    /// <summary>
    /// Serialize Object
    /// </summary>
    /// <param name="ObjectToSerialize"></param>
    /// <returns></returns>
    private static String SerializeObject(Object ObjectToSerialize)
    {
      XmlSerializer serializer = new XmlSerializer(ObjectToSerialize.GetType());
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      try
      {
        using (StringWriter writer = new Utf8StringWriter())
        {
          _name_spaces.Add("", "");
          serializer.Serialize(writer, ObjectToSerialize, _name_spaces);

          return writer.ToString();
        }
      }
      catch (Exception _ex)
      {
        ELPLog.Exception(_ex);
      }

      return String.Empty;
    } // SerializeObject

    #endregion " Private Methods "
  }

  public class Utf8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get { return Encoding.UTF8; }
    }
  } // Utf8StringWriter
}
