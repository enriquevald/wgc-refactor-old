﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;
using System.Xml.Serialization;

namespace WSI.WWP_CenterService.WigosService.Request
{
  [MessageContract]
  [DataContract]
  [Serializable]
  public class CreditRequest : IEquatable<CreditRequest>
  {
    #region Fields & Properties
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    [XmlElementAttribute(IsNullable = false)]
    public String TransactionId { get; set; }                     // Identificador único de la transacción (podría ser el voucher-id de Space)

    [MessageBodyMember(Order = 1)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int64 ExternalCustomerId { get; set; }                 // Número de cliente en Space (0: si no se conoce)

    [MessageBodyMember(Order = 2)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public String Trackdata { get; set; }                         // Número de tarjeta 01234567890123456789 ("": si no se conoce)

    [MessageBodyMember(Order = 3)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int64 AccountId { get; set; }                          // Número de cuenta (0: si no se conoce)  

    [MessageBodyMember(Order = 4)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int32 SiteId { get; set; }                             // Identificador de la sala en la que se va a llevar a cabo la transacción

    [MessageBodyMember(Order = 5)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public SharedDefinitions.CreditType CreditType { get; set; }  // Indica el tipo de crédito "Cashable", "NonCashable", "Tables"

    [MessageBodyMember(Order = 6)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Decimal AmountCents { get; set; }                      // Indica la cantidad a transferir

    [MessageBodyMember(Order = 7)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int32 NonCashableGroupId { get; set; }                // Opcional, si se informa, en el caso de NonCashable indica el tipo de promoción (restricción por máquina)

    #endregion

    #region Public Methods
    #endregion

    public bool Equals(CreditRequest CreditRequestOther)
    {

      if (CreditRequestOther == null)
      {
        return false;
      }

      if (TransactionId != CreditRequestOther.TransactionId)
      {
        return false;
      }

      if (ExternalCustomerId != CreditRequestOther.ExternalCustomerId)
      {
        return false;
      }

      if (Trackdata != CreditRequestOther.Trackdata)
      {
        return false;
      }

      if (AccountId != CreditRequestOther.AccountId)
      {
        return false;
      }

      if (SiteId != CreditRequestOther.SiteId)
      {
        return false;
      }

      if (CreditType != CreditRequestOther.CreditType)
      {
        return false;
      }

      if (AmountCents != CreditRequestOther.AmountCents)
      {
        return false;
      }

      if (NonCashableGroupId != CreditRequestOther.NonCashableGroupId)
      {
        return false;
      }

      return true;
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals(obj as CreditRequest);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        return base.GetHashCode();
        //var hashCode = 13;
        //hashCode = (hashCode * 397) ^ MyNum;

        //var myStrHashCode =
        //    !string.IsNullOrEmpty(MyStr) ?
        //        MyStr.GetHashCode() : 0;

        //hashCode = (hashCode * 397) ^ MyStr;
        //hashCode = (hashCode * 397) ^ Time.GetHashCode();

        //return hashCode;
      }
    }
  }
}