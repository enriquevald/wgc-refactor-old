//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_Processor.cs
// 
//   DESCRIPTION: WWP Processor: process all request.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2011 ACC    First release.
// 04-MAR-2014 SMN    Create a new pool to process download accounts task
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WWP;
using System.Collections;

namespace WSI.WWP_Server
{
  #region WwpInputMsgProcessTask CLASS

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WwpInputMsgProcessTask
  // 
  //   DESCRIPTION : Class for encapsulate messages in a one task with listener, WWP_client and request message.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public class WwpInputMsgProcessTask : Task
  {
    Object m_wwp_listener;
    Object m_wwp_client;
    String m_xml_message;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    protected WWP_Message m_wwp_request = null;
    protected WWP_Message m_wwp_response = null;

    #region Properties

    protected String XmlResponse
    {
      get
      {
        return m_wwp_response.ToXml();
      }
    }
    protected internal String XmlRequest
    {
      get
      {
        return m_xml_message;
      }
    }
    public Object WwpListener
    {
      get
      {
        return m_wwp_listener;
      }
    }
    public Object WwpClient
    {
      get
      {
        return m_wwp_client;
      }
    }
    protected internal WWP_Message Request
    {
      get
      {
        return m_wwp_request;
      }
    }
    protected internal WWP_Message Response
    {
      get
      {
        return m_wwp_response;
      }
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Create a task
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpListener
    //          - WwpClient
    //          - XmlMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public WwpInputMsgProcessTask(Object WwpListener, Object WwpClient, WWP_Message Request)
    {
      m_wwp_listener = WwpListener;
      m_wwp_client = WwpClient;
      m_xml_message = "";
      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;

      m_wwp_request = Request;
      m_wwp_response = WWP_Message.CreateMessage(m_wwp_request.MsgHeader.MsgType + 1);
      m_wwp_response.MsgHeader.PrepareReply(m_wwp_request.MsgHeader);
    }
    
    public WwpInputMsgProcessTask(Object WwpListener, Object WwpClient, String XmlMessage)
    {
      throw new Exception ("Not implemented.");
    }
    

    //------------------------------------------------------------------------------
    // PURPOSE : Execute task. (Pool with BatchUpdate = False)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public override void Execute()
    {
      if (!AddToPending())
      {
        return;
      }

      try
      {
        WWP_Client.ProcessWwpInputMsgTask(this);
      }
      catch
      {
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _tx > 10000
            || _tt > 20000)
        {
          Log.Warning("WWP Message times: Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString() + " ----> " + m_xml_message);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Execute tasks grouping by message type and call ExecuteGroup. 
    //           (Pool with BatchUpdate = True)
    //
    //  PARAMS :
    //      - INPUT :
    //         - Tasks
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public override void Execute(Task[] Tasks)
    {
      ArrayList _tasks0;
      ArrayList _tasks1;
      WwpInputMsgProcessTask _wwp_task;
      WWP_MsgTypes _wwp_msg_type;
      SqlConnection _sql_conn;

      if (Tasks == null)
      {
        return;
      }
      if (Tasks.Length <= 0)
      {
        return;
      }

      _tasks0 = new ArrayList(Tasks);
      _tasks1 = new ArrayList(Tasks.Length);

      while (_tasks0.Count > 0)
      {
        // Get MsgType of the 1st task
        _wwp_task = (WwpInputMsgProcessTask)_tasks0[0];
        _wwp_msg_type = _wwp_task.Request.MsgHeader.MsgType;

        // Group all tasks of the same MsgType
        foreach (WwpInputMsgProcessTask _task in _tasks0)
        {
          if (_task.Request.MsgHeader.MsgType == _wwp_msg_type)
          {
            _tasks1.Add(_task);
          }
        } // foreach

        // Remove all the tasks from _task0
        if (_tasks1.Count == _tasks0.Count)
        {
          // _tasks1 contains all the tasks from _task0
          _tasks0.Clear();
        }
        else
        {
          foreach (WwpInputMsgProcessTask _task in _tasks1)
          {
            _tasks0.Remove(_task);
          } // foreach        
        }

        try
        {
          // Execute
          _sql_conn = WGDB.Connection();
          try
          {
            WwpInputMsgProcessTask.ExecuteGroup(_wwp_msg_type, _tasks1, _sql_conn);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
          if (_sql_conn != null)
          {
            try { _sql_conn.Close(); }
            catch { ; }
            _sql_conn.Dispose();
            _sql_conn = null;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while (_tasks0.Count > 0)
    } // Execute 

    //------------------------------------------------------------------------------
    // PURPOSE : Execute task (same type) in group. (Pool with BatchUpdate = True)
    //
    //  PARAMS :
    //      - INPUT :
    //         - MsgType
    //         - Tasks
    //         - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ExecuteGroup(WWP_MsgTypes MsgType, System.Collections.ArrayList Tasks, SqlConnection SqlConn)
    {
      ArrayList _pending;
      int _tick_arrived;
      int _tick_dequeued;
      int _tick_processed;

      _tick_dequeued = Environment.TickCount;
      _tick_processed = _tick_dequeued;
      _tick_arrived = _tick_dequeued;

      _pending = new ArrayList(Tasks.Count);

      try
      {
        // Check Timeout and/or Disconnected tasks
        foreach (WwpInputMsgProcessTask _task in Tasks)
        {
          _tick_arrived = Math.Min(_task.m_tick_arrived, _tick_arrived);

          if (_task.AddToPending())
          {
            _pending.Add(_task);
          }
          else
          {
            // Timeout/Disconnected: do nothing
          }
        }

        switch (MsgType)
        {

          default:
            {
              foreach (WwpInputMsgProcessTask _wwp_task in _pending)
              {
                _wwp_task.Execute();
              }
            }
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Message("EXCEPTION processiong: " + MsgType.ToString() + ", Details: " + _ex.Message);
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        _tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(_tick_arrived, _tick_dequeued);
        _tx = Misc.GetElapsedTicks(_tick_dequeued, _tick_processed);
        _tt = Misc.GetElapsedTicks(_tick_arrived, _tick_processed);

        if (_tx >= 10000
            || _tt >= 20000)
        {
          Log.Warning("MessageType " + MsgType.ToString() + " Count:" + _pending.Count + " Time Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString());
        }
      }
    } // ExecuteGroup

    //------------------------------------------------------------------------------
    // PURPOSE : Check queued time and connect status.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: queued time and connect status OK
    //      - False: otherwise
    //
    public Boolean AddToPending()
    {
      String _msg_type;
      long _interval;
      SecureTcpClient _tcp_client;
      SecureTcpServer _tcp_server;


      _msg_type = m_wwp_request.MsgHeader.MsgType.ToString();
      if (_msg_type.IndexOf("Reply") > 0)
      {
        // It is a reply  (Command, Logger, ...)
        // Never timeout
        return true;
      }

      m_tick_dequeued = Environment.TickCount;
      m_tick_processed = m_tick_dequeued;
      _interval = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
      if (_interval >= 10000)
      {
        // Ignore the task     
        Log.Warning("Message Ignored: timeout. Queued: " + _interval.ToString() + " ms. " + m_xml_message);

        return false;
      }

      _tcp_client = (SecureTcpClient)m_wwp_client;
      _tcp_server = (SecureTcpServer)m_wwp_listener;

      if (!_tcp_server.IsConnected(_tcp_client))
      {
        // Ignore the task     
        Log.Warning("Message Ignored: disconnected. Queued: " + _interval.ToString() + " ms. " + m_xml_message);

        return false;
      }

      return true;
    } // AddToPending

  }

  #endregion // WwpInputMsgProcessTask CLASS

  #region WWP_WorkerPool CLASS

  
  public enum WWP_Pool
  {
    WWP_POOL_DEFAULT,
    WWP_POOL_ENROLL,
    WWP_POOL_ANY_UPLOAD,
    WWP_POOL_ANY_DOWNLOAD,
    WWP_POOL_DOWNLOAD_ACCOUNTS,
    WWP_POOL_UPLOAD_PLAYSESSIONS,
    WWP_POOL_CONFIG_SEQ,
    WWP_POOL_MULTISITE_REQUEST,
  }
  
  public class WWP_PoolItem
  {
    #region Member
    
    private WorkerPool m_worker_pool;
    private Int32 m_num_workers;
    
    #endregion // Member

    #region Properties
    public WorkerPool WorkerPool
    {
      get { return m_worker_pool; }
      set { m_worker_pool = value; }
    }

    public Int32 NumWorkers
    {
      get { return m_num_workers; }
    }

    #endregion // Properties


    public WWP_PoolItem(Int32 NumWorkers)
    {
      m_num_workers = NumWorkers;
      m_worker_pool = null;
    } // WWP_PoolItem

  } // WWP_PoolItem
  
  
  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_WorkerPool
  // 
  //   DESCRIPTION : Class for create workers (in Pools) for process messages.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public static class WWP_WorkerPool
  {
    private static Dictionary<WWP_Pool, WWP_PoolItem> m_pools;
    private static Boolean m_use_default_pool_for_all_tasks;


    //------------------------------------------------------------------------------
    // PURPOSE : Return the Pool for this task.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Enum WWP_Pool
    //
    private static WWP_Pool GetPool(WwpInputMsgProcessTask Task)
    {
      WWP_TableTypes _type_task;

      switch (Task.Request.MsgHeader.MsgType)
      {
        case WWP_MsgTypes.WWP_MsgTable:
          if (m_use_default_pool_for_all_tasks)
          {
            return WWP_Pool.WWP_POOL_DEFAULT; 
          }

          _type_task = ((WWP_MsgTable)Task.Request.MsgContent).Type;
          switch (_type_task)
          {
            case WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PERSONAL_INFO:
              return WWP_Pool.WWP_POOL_DOWNLOAD_ACCOUNTS;

            case WWP_TableTypes.TABLE_TYPE_UPLOAD_GAME_PLAY_SESSIONS:
              return WWP_Pool.WWP_POOL_UPLOAD_PLAYSESSIONS;

            case WWP_TableTypes.TABLE_TYPE_CONFIGURATION:
            case WWP_TableTypes.TABLE_TYPE_GET_CENTER_SEQUENCES:
              return WWP_Pool.WWP_POOL_CONFIG_SEQ;

            case WWP_TableTypes.TABLE_TYPE_MULTISITE_REQUEST:
            case WWP_TableTypes.TABLE_TYPE_MULTISITE_RESPONSE:
              return WWP_Pool.WWP_POOL_MULTISITE_REQUEST;

            default:
              if (_type_task.ToString().Contains("UPLOAD"))
              {
                return WWP_Pool.WWP_POOL_ANY_UPLOAD;
              }
              else if (_type_task.ToString().Contains("DOWNLOAD"))
              {
                return WWP_Pool.WWP_POOL_ANY_DOWNLOAD; ;
              }
              else
              {
                return WWP_Pool.WWP_POOL_DEFAULT;
              }

          } // switch

        case WWP_MsgTypes.WWP_MsgEnrollSite:
        case WWP_MsgTypes.WWP_MsgUnenrollSite:
          return WWP_Pool.WWP_POOL_ENROLL;

        default:
          return WWP_Pool.WWP_POOL_DEFAULT;
      } // switch
    } // GetPool

    //------------------------------------------------------------------------------
    // PURPOSE : Init all Worker Pools.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Int32 _num_workers;

      m_use_default_pool_for_all_tasks = GeneralParam.GetInt32("WWP", "UseDefaultPoolForAllTasks", 0) == 1;

      m_pools = new Dictionary<WWP_Pool, WWP_PoolItem>();
      
      // Add Pools in dictionary: m_pools
      // m_pools.Add(WWP_Pool.<VALUE>.ToString(), new WWP_PoolItem(<NUM_WORKERS>));
      m_pools.Add(WWP_Pool.WWP_POOL_DEFAULT, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.Default", 10)));
      m_pools.Add(WWP_Pool.WWP_POOL_ENROLL, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.Enroll", 10)));
      m_pools.Add(WWP_Pool.WWP_POOL_ANY_UPLOAD, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.AnyUpload", 20)));
      m_pools.Add(WWP_Pool.WWP_POOL_ANY_DOWNLOAD, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.AnyDownload", 20)));
      m_pools.Add(WWP_Pool.WWP_POOL_DOWNLOAD_ACCOUNTS, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.DownloadAccounts", 50)));
      m_pools.Add(WWP_Pool.WWP_POOL_UPLOAD_PLAYSESSIONS, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.UploadPlaysession", 50)));
      m_pools.Add(WWP_Pool.WWP_POOL_CONFIG_SEQ, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.ConfigSeq", 50)));
      m_pools.Add(WWP_Pool.WWP_POOL_MULTISITE_REQUEST, new WWP_PoolItem(GeneralParam.GetInt32("WWP", "Pool.MultisiteRequest", 50)));

      // Initialize each WorkerPool of m_pools
      foreach (KeyValuePair<WWP_Pool, WWP_PoolItem> _pool in m_pools)
      {
        _num_workers = _pool.Value.NumWorkers;
        if (Environment.GetEnvironmentVariable(_pool.Key.ToString()) != null)
        {
          _num_workers = int.Parse(Environment.GetEnvironmentVariable(_pool.Key.ToString()));
        }
        _pool.Value.WorkerPool = new WorkerPool(_pool.Key.ToString(), _num_workers);
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue task into corresponding Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Enqueue(WwpInputMsgProcessTask Task)
    {
      m_pools[GetPool(Task)].WorkerPool.EnqueueTask(Task);
    } // Enqueue
  }

  #endregion // WWP_WorkerPool CLASS

  #region WWP_Client CLASS

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_Client
  // 
  //   DESCRIPTION : Class for process received messages.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public class WWP_Client : IXmlSink 
  {
    private static WWP_Client wwp_client;

    //------------------------------------------------------------------------------
    // PURPOSE : Init.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      wwp_client = new WWP_Client();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get instance for WWP_Client.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - WWP_Client object
    //
    public static WWP_Client GetInstance()
    {
      return wwp_client;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Starts the worker Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void StartWorkerPool ()
    {
      WWP_WorkerPool.Init();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Starts the WWP_Client.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Start()
    {
      // Workers start
      wwp_client.StartWorkerPool();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process WWP Message received:
    //              - Create Task.
    //              - Enqueue task into the worker Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpListener
    //          - WwpClient
    //          - XmlMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public virtual void Process(Object WwpListener, Object WwpClient, String XmlMessage)
    {
      WwpInputMsgProcessTask msg_input_task;

      msg_input_task = new WwpInputMsgProcessTask(WwpListener, WwpClient, XmlMessage);

      WWP_WorkerPool.Enqueue(msg_input_task);

    } // Process

    //------------------------------------------------------------------------------
    // PURPOSE : Process specific actions for WWP Message.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void ProcessWwpInputMsgTask(WwpInputMsgProcessTask  WwpTask)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      SecureTcpClient _client;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _error;

      _client = (SecureTcpClient)WwpTask.WwpClient;

      _sql_conn      = null;
      _sql_trx       = null;
      _error         = true;
      _wwp_response  = null;
      _wwp_request   = null;

      try
      {
        _wwp_request = WwpTask.Request;

        if (_wwp_request.MsgHeader.MsgType.ToString().IndexOf("Reply") > 0)
        {
          Log.Warning("Unexpected Message Type: " + _wwp_request.MsgHeader.MsgType.ToString());

          return;
        }

        _wwp_response = WwpTask.Response;

        switch (_wwp_request.MsgHeader.MsgType)
        {
          case WWP_MsgTypes.WWP_MsgKeepAlive:
            {
              String _msg_response_str;

              if (!Common.BatchUpdate.TerminalSession.SessionMessageReceived(_wwp_request.MsgHeader.SiteId,
                                                                             _wwp_request.MsgHeader.SessionId,
                                                                             _wwp_request.MsgHeader.SequenceId, 0))
              {
                throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID);
              }

              _msg_response_str = _wwp_response.ToXml();
              _client.Send(_msg_response_str);

              _error = false;

              return;
            }
          // Unreached code. 
          // break;

          default:
            break;
        }

        NetLog.Add(NetEvent.MessageReceived, _client.Identity, WwpTask.XmlRequest);

        // Get Sql Connection 
        _sql_conn = WGDB.Connection();

        //
        // Request from sites processing:
        //

        // Begin Transaction
        _sql_trx = _sql_conn.BeginTransaction();

        if (_wwp_request.MsgHeader.MsgType != WWP_MsgTypes.WWP_MsgEnrollSite)
        {
          if (_client.InternalId == 0)
          {
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_SITE_NOT_AUTHORIZED);
          }

          if (!Common.BatchUpdate.TerminalSession.SessionMessageReceived(_wwp_request.MsgHeader.SiteId,
                                                                         _wwp_request.MsgHeader.SessionId,
                                                                         _wwp_request.MsgHeader.SequenceId, 0))
          {
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID);
          }
        }

        if (_wwp_request.MsgHeader.MsgType == WWP_MsgTypes.WWP_MsgEnrollSite)
        {
          if (_wwp_request.MsgHeader.SiteId == 0)
          {
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_INVALID_SITE_ID);
          }
          if (_wwp_request.MsgHeader.SessionId != 0)
          {
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID);
          }
        }
        else
        {
          if (_wwp_request.MsgHeader.SiteId > 0)
          {
            if (_wwp_request.MsgHeader.SessionId == 0)
            {
              throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID);
            }

            if (!Common.BatchUpdate.TerminalSession.IsActive(_wwp_request.MsgHeader.SessionId))
            {
              throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_SITE_SESSION_NOT_VALID);
            }
          }
        }

        switch (_wwp_request.MsgHeader.MsgType)
        {
          case WWP_MsgTypes.WWP_MsgEnrollSite:
          {
            // Enqueue enroll in pending enrolls queue.
            WWP_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(WwpTask));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            _sql_trx.Commit();

            _error = false;

            return;
          }
          // Unreachable code detected
          // break;

          case WWP_MsgTypes.WWP_MsgUnenrollSite:
          {
            // Enqueue enroll in pending enrolls queue.
            WWP_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(WwpTask));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            _sql_trx.Commit();

            _error = false;

            return;
          }
          // Unreachable code detected
          // break;

          case WWP_MsgTypes.WWP_MsgSqlCommand:
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              WWP_ProcessRequest.SqlCommand(_wwp_request, _wwp_response, _db_trx.SqlTransaction);
              _db_trx.Rollback();
            }
            _error = false;
          }
          break;

          case WWP_MsgTypes.WWP_MsgTable:
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              ProcessSiteRequest.Table(_wwp_request, _wwp_response, _db_trx.SqlTransaction);
              _db_trx.Rollback();
            }
            _error = false;
          }
          break;

          default:
          break;
        }

        String str_response;

        str_response = _wwp_response.ToXml();

        // End Transaction
        _sql_trx.Commit();

        _client.Send(str_response);
        NetLog.Add(NetEvent.MessageSent, _client.Identity, str_response);

        _error = false;
      }
      catch (WWP_Exception wwp_ex)
      {
        String wwp_error_response;

        wwp_error_response = null;
        try
        {
          if (_wwp_response == null)
          {
            _wwp_response = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgError);
          }
          _wwp_response.MsgHeader.ResponseCode = wwp_ex.ResponseCode;
          if (_wwp_request != null)
          {
            _wwp_response.MsgHeader.SequenceId = _wwp_request.MsgHeader.SequenceId;
          }

          // Create error response
          wwp_error_response = _wwp_response.ToXml();

          _client.Send(wwp_error_response);
          NetLog.Add(NetEvent.MessageSent, _client.Identity, wwp_error_response);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch ( Exception ex )
      {
        NetLog.Add (NetEvent.MessageFormatError, _client.Identity + " a.1)", ex.Message);
        NetLog.Add (NetEvent.MessageFormatError, _client.Identity + " b.1)", WwpTask.XmlRequest);

        Log.Exception (ex);

        return;
      }
      finally
      {
        if ( _sql_trx != null )
        {
          if ( _error && _sql_trx.Connection != null )
          {
            _sql_trx.Rollback();
          }

          _sql_trx.Dispose ();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }
    }
  }

  #endregion // WWP_Client CLASS

}
