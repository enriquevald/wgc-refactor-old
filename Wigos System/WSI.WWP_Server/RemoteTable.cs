//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RemoteTable.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2011 AJQ    First release.
// 23-MAR-2011 RCI    Reorganize code.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WWP_Server
{
  public class RemoteTable
  {
    #region Members

    private Int32 m_site_id;

    // DataTable with the Site data received
    private DataTable m_site_table;

    private SqlCommand m_site_changes_cmd;

    private DataTable m_center_changes;

    private SqlCommand m_site_data_cmd;

    //private Boolean   m_error;
    //private Exception m_exception;

    // Center Schemas
    private DataTable m_center_table_schema;
    private static DataSet m_center_schemas = new DataSet("CenterSchemas");

    #endregion // Members

    #region Properties

    public DataTable SiteTable
    {
      get { return m_site_table; }
      set { m_site_table = value; }
    }

    public SqlCommand SiteChangesSqlCmd
    {
      get { return m_site_changes_cmd; }
    }

    public DataTable CenterChangesData
    {
      get { return m_center_changes; }
    }

    #endregion // Properties

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE : Init the following:
    //             - DataTable with the schema of the Center Table
    //             - DataTable with the Center Changes data
    //             - SqlCommand for Site Changes
    //             - SqlCommand for Site Data
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TableName
    //          - Int32 SiteId
    //          - String ColumnDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public RemoteTable(String TableName, Int32 SiteId, String ColumnDate)
    {
      m_site_id = SiteId;

      m_site_table = null;

      //
      // Get Center Table Schema
      //
      m_center_table_schema = RemoteTable.CenterSchema(TableName);

      //
      // Get Center Changes in a DataTable
      //
      m_center_changes = RemoteTable.CenterChanges(m_center_table_schema, ColumnDate, SiteId);
      if (m_center_changes == null)
      {
        return;
      }

      //
      // SqlCommand for Site Changes
      //
      m_site_changes_cmd = RemoteTable.SiteChangesCommand(m_center_table_schema, ColumnDate);
      if (m_site_changes_cmd == null)
      {
        return;
      }

      //
      // SqlCommand for Site Data
      //
      m_site_data_cmd = RemoteTable.SiteDataCommand(m_center_table_schema, ColumnDate, m_center_changes);

    } // RemoteTable

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Get the SqlCommand to retrieve data from Site.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Byte[] Timestamp
    //          - DateTime From
    //          - DateTime To
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    public SqlCommand GetSiteDataCommand(Byte[] Timestamp, DateTime From, DateTime To)
    {
      String _col_ts;

      if (m_site_data_cmd == null)
      {
        return null;
      }

      _col_ts = "@" + RemoteTable.TimestampColumnName(m_center_table_schema);
      if (m_site_data_cmd.Parameters.Contains(_col_ts))
      {
        m_site_data_cmd.Parameters[_col_ts].Value = Timestamp;
      }

      if (m_site_data_cmd.Parameters.Contains("@pFromDate"))
      {
        m_site_data_cmd.Parameters["@pFromDate"].Value = From;
      }
      if (m_site_data_cmd.Parameters.Contains("@pToDate"))
      {
        m_site_data_cmd.Parameters["@pToDate"].Value = To;
      }

      return m_site_data_cmd;
    } // GetSiteDataCommand

    //------------------------------------------------------------------------------
    // PURPOSE : Return the last timestamp received from the Site.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Byte[]
    //
    public Byte[] GetLastTimestampReceived()
    {
      if (m_site_table == null)
      {
        return new Byte[8];
      }

      if (m_site_table.Rows.Count > 0)
      {
        String _col_timestamp;

        _col_timestamp = RemoteTable.TimestampColumnName(m_site_table);
        if (!String.IsNullOrEmpty(_col_timestamp))
        {
          return (Byte[])m_site_table.Rows[m_site_table.Rows.Count - 1][_col_timestamp];
        }
      }

      return new Byte[8];
    } // GetLastTimestampReceived

    //------------------------------------------------------------------------------
    // PURPOSE : Compare two timestamps.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Object Ts1
    //          - Object Ts2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32
    //
    public static Int32 CompareTimestamp(Object Ts1, Object Ts2)
    {
      return (Int32)(TimestampToUInt64(Ts1) - TimestampToUInt64(Ts2));
    } // CompareTimestamp

    //------------------------------------------------------------------------------
    // PURPOSE : Save the SiteTable to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: SiteTable saved ok. False: Otherwise.
    //
    public Boolean Save()
    {
      SqlCommand _sql_cmd;
      Int32 _num_rows_updated;

      if (m_site_table == null ||
          m_site_table.Rows.Count == 0)
      {
        return true;
      }

      try
      {
        _sql_cmd = BuildFullUpdateCmd();
        if (_sql_cmd == null)
        {
          return true;
        }

        m_site_table.AcceptChanges();
        foreach (DataRow _dr in m_site_table.Rows)
        {
          _dr.SetModified();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.DeleteCommand = null;
            _sql_da.InsertCommand = null;
            _sql_da.UpdateCommand = _sql_cmd;
            _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.ContinueUpdateOnError = true;
            _sql_da.UpdateBatchSize = 500;

            _num_rows_updated = _db_trx.Update(_sql_da, m_site_table);

            if (_num_rows_updated == m_site_table.Rows.Count)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Save

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain the schema of TableName.
    //           It is saved in a static DataSet for future retrievings.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TableName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable CenterSchema(String TableName)
    {
      DataTable _table;

      lock (m_center_schemas)
      {
        if (m_center_schemas.Tables.Contains(TableName))
        {
          // Return a copy of the Schema
          return m_center_schemas.Tables[TableName].Clone();
        }
      }

      try
      {
        _table = new DataTable(TableName);

        using (SqlConnection _sql_conn = WGDB.Connection())
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter("SELECT * FROM " + TableName, _sql_conn))
          {
            // Get Center Table Schema
            _sql_da.FillSchema(_table, SchemaType.Source);
          }
        }

        lock (m_center_schemas)
        {
          if (!m_center_schemas.Tables.Contains(TableName))
          {
            m_center_schemas.Tables.Add(_table);
          }
          // Return a copy of the Schema
          return m_center_schemas.Tables[TableName].Clone();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    } // CenterSchema

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Center changes (timestamp).
    //              - If ColumnDate is not defined: A DataTable with a single row with timestamp and count is returned.
    //              - If ColumnDate is defined: DataTable has the changes grouped by day.
    //                For each day, timestamp and count is returned.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String ColumnDate
    //          - Int32 SiteId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    private static DataTable CenterChanges(DataTable Table, String ColumnDate, Int32 SiteId)
    {
      DataTable _table;
      String _changes_sql_txt;

      try
      {
        _table = new DataTable("CHANGES-[" + Table.TableName + "]");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _changes_sql_txt = GetChangesCommandText(Table, ColumnDate, SiteId);
          if (String.IsNullOrEmpty(_changes_sql_txt))
          {
            return null;
          }
          using (SqlCommand _sql_cmd = new SqlCommand(_changes_sql_txt))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, _table);
            }
          }
        }

        return _table;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // CenterChanges

    //------------------------------------------------------------------------------
    // PURPOSE : Return the SqlCommand for retrieving the Site Changes.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String ColumnDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    private static SqlCommand SiteChangesCommand(DataTable Table, String ColumnDate)
    {
      String _changes_sql_txt;

      _changes_sql_txt = GetChangesCommandText(Table, ColumnDate, -1);
      if (String.IsNullOrEmpty(_changes_sql_txt))
      {
        return null;
      }

      return new SqlCommand(_changes_sql_txt);
    } // SiteChangesCommand

    //------------------------------------------------------------------------------
    // PURPOSE : Return the CommandText for retrieving Changes from Center (if SiteId > -1)
    //           or Site (if SiteId == -1).
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String ColumnDate
    //          - Int32 SiteId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetChangesCommandText(DataTable Table, String ColumnDate, Int32 SiteId)
    {
      StringBuilder _select;
      String _group;
      String _group_by;
      String _count;
      String _timestamp;
      String _col_ts;
      String _col_site;

      if (String.IsNullOrEmpty(ColumnDate))
      {
        _group = "";
      }
      else
      {
        _group = "DATEADD (DAY, DATEDIFF (DAY, '01/01/2007', " + ColumnDate + "), '01/01/2007')";
      }
      _group_by = _group;

      _col_ts = RemoteTable.TimestampColumnName(Table);
      _col_site = "";

      if (SiteId > 0)
      {
        _col_site = RemoteTable.SiteIdColumnName(Table);
      }

      if (String.IsNullOrEmpty(_col_ts))
      {
        Log.Error("Table " + Table.TableName + " has NO timestamp column.");
        return "";
      }

      if (String.IsNullOrEmpty(_col_site))
      {
        _col_site = SiteId.ToString();
      }

      if (String.IsNullOrEmpty(_group))
      {
        _group = "CAST ('01/01/2007' AS DATETIME) AS DATE";
      }

      _timestamp = "ISNULL( MAX(" + _col_ts + "), 0) AS ROW_VERSION";
      _count = "COUNT (*) AS ROW_COUNT";

      _select = new StringBuilder();
      _select.AppendLine("SELECT   " + _group);
      _select.AppendLine("       , " + _count);
      _select.AppendLine("       , " + _timestamp);
      _select.AppendLine("  FROM   " + Table.TableName);
      if (SiteId > -1)
      {
        _select.AppendLine(" WHERE   " + _col_site + " = " + SiteId.ToString());
      }
      if (!String.IsNullOrEmpty(_group_by))
      {
        _select.AppendLine(" GROUP BY " + _group);
      }
      _select.AppendLine(" ORDER BY 1 ASC");

      return _select.ToString();
    } // GetChangesCommandText

    //------------------------------------------------------------------------------
    // PURPOSE : Return the SqlCommand for retrieving the Site Data for the Table.
    //              - Use the Timestamp Column to retrieve only the data not already retrieved.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String ColumnDate
    //          - DataTable CenterChangesTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    private static SqlCommand SiteDataCommand(DataTable Table, String ColumnDate, DataTable CenterChangesTable)
    {
      StringBuilder _select;
      SqlCommand _sql_cmd;
      String _col_ts;

      _col_ts = RemoteTable.TimestampColumnName(Table);

      if (String.IsNullOrEmpty(_col_ts))
      {
        Log.Error("Table " + Table.TableName + " has NO timestamp column.");
        return null;
      }

      _select = new StringBuilder();
      _select.AppendLine("SELECT   TOP 100 *");
      ////_select.AppendLine("SELECT   *");
      _select.AppendLine("  FROM   " + Table.TableName);
      _select.AppendLine(" WHERE   " + _col_ts + " > @" + _col_ts);
      if (!String.IsNullOrEmpty(ColumnDate))
      {
        _select.AppendLine("   AND   " + ColumnDate + " >= @pFromDate");
        _select.AppendLine("   AND   " + ColumnDate + " <  @pToDate");
      }
      _select.AppendLine(" ORDER BY " + _col_ts + " ASC");

      //////_select.AppendLine("SELECT   COUNT(*)");
      //////_select.AppendLine("  FROM   " + Table.TableName);
      //////_select.AppendLine(" WHERE   " + _col_ts + " > @" + _col_ts);
      //////if (!String.IsNullOrEmpty(ColumnDate))
      //////{
      //////  _select.AppendLine("   AND   " + ColumnDate + " >= @pFromDate");
      //////  _select.AppendLine("   AND   " + ColumnDate + " <  @pToDate");
      //////}

      _sql_cmd = new SqlCommand(_select.ToString());
      if (CenterChangesTable.Rows.Count > 0)
      {
        _sql_cmd.Parameters.Add("@" + _col_ts, SqlDbType.Timestamp).Value = CenterChangesTable.Rows[0][2];
      }
      else
      {
        _sql_cmd.Parameters.Add("@" + _col_ts, SqlDbType.Timestamp).Value = new Byte[8];
      }

      if (!String.IsNullOrEmpty(ColumnDate))
      {
        _sql_cmd.Parameters.Add("@pFromDate", SqlDbType.DateTime);
        _sql_cmd.Parameters.Add("@pToDate", SqlDbType.DateTime);
      }

      return _sql_cmd;
    } // SiteDataCommand

    //------------------------------------------------------------------------------
    // PURPOSE : Build the SqlCommand to full update the SuperCenter version of the SiteTable.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlCommand
    //
    private SqlCommand BuildFullUpdateCmd()
    {
      SqlCommand _full_update_cmd;
      StringBuilder _select;
      StringBuilder _update;
      StringBuilder _insert;
      List<String> _update_cols;
      List<String> _insert_cols;
      List<String> _pk_cols;
      DataColumn _col_site_id;
      String _col_site;
      StringBuilder _total_update;
      SqlParameter _sql_param;

      // Add SiteId column
      _col_site = RemoteTable.SiteIdColumnName(m_center_table_schema);
      _col_site_id = new DataColumn(_col_site, Type.GetType("System.Int32"));
      _col_site_id.AllowDBNull = false;
      _col_site_id.DefaultValue = m_site_id;
      m_site_table.Columns.Add(_col_site_id);
      _col_site_id.SetOrdinal(0);

      _update_cols = RemoteTable.UpdatableColumns(m_site_table, m_center_table_schema);
      _insert_cols = RemoteTable.UpdatableColumns(m_site_table, m_center_table_schema);
      _pk_cols = new List<String>();
      for (int _idx = 0; _idx < m_center_table_schema.PrimaryKey.Length; _idx++)
      {
        _pk_cols.Add(m_center_table_schema.PrimaryKey[_idx].ColumnName);
        _insert_cols.Add(m_center_table_schema.PrimaryKey[_idx].ColumnName);
      }

      _update = new StringBuilder();
      _update.AppendLine("UPDATE   " + m_center_table_schema.TableName);
      BuildWhereSetPart(_update, "   SET   ", "       , ", _update_cols);
      BuildWhereSetPart(_update, " WHERE   ", "     AND ", _pk_cols);

      _insert = new StringBuilder();
      _insert.AppendLine("INSERT INTO   " + m_center_table_schema.TableName);
      BuildInsertPart(_insert, _insert_cols);

      _select = new StringBuilder();
      _select.AppendLine("SELECT   1");
      _select.AppendLine("  FROM   " + m_center_table_schema.TableName);
      BuildWhereSetPart(_select, " WHERE   ", "   AND   ", _pk_cols);

      _total_update = new StringBuilder();
      _total_update.AppendLine("IF NOT EXISTS");
      _total_update.AppendLine("(");
      _total_update.AppendLine(_select.ToString());
      _total_update.AppendLine(")");
      _total_update.AppendLine(_insert.ToString());
      _total_update.AppendLine("ELSE");
      _total_update.AppendLine(_update.ToString());

      _full_update_cmd = new SqlCommand(_total_update.ToString());

      foreach (String _col_name in _insert_cols)
      {
        _sql_param = new SqlParameter();
        _sql_param.ParameterName = "@" + _col_name;
        _sql_param.SourceColumn = _col_name;
        _full_update_cmd.Parameters.Add(_sql_param);
      }

      return _full_update_cmd;
    } // BuildFullUpdateCmd

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Column Name of the Timestamp column of Table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String TimestampColumnName(DataTable Table)
    {
      return ColumnName(Table, "_timestamp");
    } // TimestampColumnName

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Column Name of the SiteId column of Table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String SiteIdColumnName(DataTable Table)
    {
      return ColumnName(Table, "_site_id");
    } // SiteIdColumnName

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Column Name of Table that ends with the parameter EndsWith.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable Table
    //          - String EndsWith
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String ColumnName(DataTable Table, String EndsWith)
    {
      foreach (DataColumn _dc in Table.Columns)
      {
        if (_dc.ColumnName.EndsWith(EndsWith))
        {
          return _dc.ColumnName;
        }
      }
      return "";
    } // ColumnName

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Names of the Updatable Columns that are common in
    //           the Site and Center Version of a Table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable SiteTable
    //          - DataTable CenterTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - List<String>
    //
    private static List<String> UpdatableColumns(DataTable SiteTable, DataTable CenterTable)
    {
      List<String> _updatable;
      List<DataColumn> _pk;

      _pk = new List<DataColumn>(CenterTable.PrimaryKey);
      _updatable = new List<String>();

      foreach (DataColumn _col in CenterTable.Columns)
      {
        if (_col.ReadOnly)
        {
          // Exclude ReadOnly columns
          continue;
        }
        if (_pk.Contains(_col))
        {
          // Exclude the PrimayKey
          continue;
        }
        if (SiteTable.Columns.Contains(_col.ColumnName))
        {
          _updatable.Add(_col.ColumnName);
        }
      }
      return _updatable;
    } // UpdatableColumns

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Names of the Common Columns of the SiteTable and CenterTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable SiteTable
    //          - DataTable CenterTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - List<String>
    //
    private static List<String> CommonColumns(DataTable SiteTable, DataTable CenterTable)
    {
      List<String> _intersect;

      _intersect = new List<String>();

      foreach (DataColumn _col in CenterTable.Columns)
      {
        if (SiteTable.Columns.Contains(_col.ColumnName))
        {
          _intersect.Add(_col.ColumnName);
        }
      }

      return _intersect;
    } // CommonColumns

    //------------------------------------------------------------------------------
    // PURPOSE : Check for the CenterTable.PrimaryKey == SiteTable.PrimaryKey.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable CenterTable
    //          - DataTable SiteTable
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean CheckPrimaryKey(DataTable CenterTable, DataTable SiteTable)
    {
      foreach (DataColumn _col in CenterTable.PrimaryKey)
      {
        if (!SiteTable.Columns.Contains(_col.ColumnName))
        {
          if (_col.ColumnName.Contains("site_id"))
          {
            // Don't care when the column is the SiteId
            continue;
          }
          return false;
        }
      }
      return true;
    } // CheckPrimaryKey

    //------------------------------------------------------------------------------
    // PURPOSE : Dynamically build the UPDATE or SELECT part of a CommandText in function of the
    //           parameter Columns, StrWhere and StrAnd.
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringBuilder SqlTxt
    //          - String StrWhere
    //          - String StrAnd
    //          - List<String> Columns
    //
    //      - OUTPUT :
    //          - StringBuilder SqlTxt
    //
    // RETURNS :
    //
    private static void BuildWhereSetPart(StringBuilder SqlTxt, String StrWhere, String StrAnd, List<String> Columns)
    {
      int _padding;
      String _column;

      _padding = 0;
      for (int _idx = 0; _idx < Columns.Count; _idx++)
      {
        _padding = Math.Max(_padding, Columns[_idx].Length);
      }

      for (int _idx = 0; _idx < Columns.Count; _idx++)
      {
        _column = Columns[_idx].PadRight(_padding);
        if (_idx == 0)
        {
          SqlTxt.Append(StrWhere);
        }
        else
        {
          SqlTxt.Append(StrAnd);
        }
        SqlTxt.Append(_column + " = @" + _column);
        SqlTxt.AppendLine();
      }
    } // BuildWhereSetPart

    //------------------------------------------------------------------------------
    // PURPOSE : Dynamically build the INSERT part of a CommandText in function of the parameter Columns.
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringBuilder SqlTxt
    //          - List<String> Columns
    //
    //      - OUTPUT :
    //          - StringBuilder SqlTxt
    //
    // RETURNS :
    //
    private static void BuildInsertPart(StringBuilder SqlTxt, List<String> Columns)
    {
      int _padding;

      _padding = 0;
      for (int _idx = 0; _idx < Columns.Count; _idx++)
      {
        _padding = Math.Max(_padding, Columns[_idx].Length);
      }

      for (int _idx = 0; _idx < Columns.Count; _idx++)
      {
        String _col_name;

        _col_name = (String)Columns[_idx];
        _col_name = _col_name.PadRight(_padding);

        if (_idx == 0)
        {
          SqlTxt.AppendLine("            ( " + _col_name);
        }
        else
        {
          if (_idx < Columns.Count - 1)
          {
            SqlTxt.AppendLine("            , " + _col_name);
          }
          else
          {
            SqlTxt.AppendLine("            , " + _col_name + ") ");
          }
        }
      }

      for (int _idx = 0; _idx < Columns.Count; _idx++)
      {
        String _col_name;

        _col_name = (String)Columns[_idx];
        _col_name = _col_name.PadRight(_padding);

        if (_idx == 0)
        {
          SqlTxt.AppendLine("     VALUES ( @" + _col_name);
        }
        else
        {
          if (_idx < Columns.Count - 1)
          {
            SqlTxt.AppendLine("            , @" + _col_name);
          }
          else
          {
            SqlTxt.AppendLine("            , @" + _col_name + ") ");
          }
        }
      }
    } // BuildInsertPart

    //------------------------------------------------------------------------------
    // PURPOSE : Convert a Timestamp object to its UInt64 representation.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Object SqlTimestamp
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - UInt64
    //
    private static UInt64 TimestampToUInt64(Object SqlTimestamp)
    {
      UInt64 _value;
      Type _type;
      Byte[] _arr;

      if (SqlTimestamp == null)
      {
        return 0;
      }
      _type = SqlTimestamp.GetType();
      if (_type != Type.GetType("System.Byte[]"))
      {
        return 0;
      }

      _arr = (Byte[])SqlTimestamp;

      Array.Reverse(_arr);

      if (_arr.Length == 4)
      {
        _value = (UInt64)BitConverter.ToUInt32(_arr, 0);
      }
      else
      {
        _value = BitConverter.ToUInt64(_arr, 0);
      }
      Array.Reverse(_arr);

      return _value;
    } // TimestampToUInt64

    #endregion // Private Methods

  } // RemoteTable

} // WSI.WWP_Server
