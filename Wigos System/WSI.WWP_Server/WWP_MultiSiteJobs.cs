//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WWP_MultiSiteJobs.cs
//
//   DESCRIPTION : WWP_MultiSiteJobs class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAR-2013 AMF    First release.
// 28-APR-2015 DCS    Multisite Multicurrency -> Thread manteinance exchange_rates
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
// 10-MAR-2016 FGB    PBI 10130: Multiple Buckets: Sincronización MultiSite: Kernel - Validar Expiración y Cambio Nivel
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.WWP_CenterService
{
  class WWP_MultiSiteJobs
  {
    #region Constants
    private const Int32 WAIT_FOR_NEXT_TRY = (5 * 60 * 1000);  // 5 minutes
    private const Int32 NUM_JOBS = 20;                        // Number of Jobs to perform on thread
    #endregion Constants

    #region Attributes
    private static Thread m_thread;
    #endregion Attributes

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WWP_MultiSiteJobs class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      m_thread = new Thread(Thread);
      m_thread.Name = "WWP_MultiSiteJobs.Thread";
      m_thread.Start();
    }

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    static private void Thread()
    {
      SqlConnection _sql_conn;
      Int32 _step;
      int _tick0;
      Boolean _is_multisite_center;
      Boolean _is_elp;
      long[] _ellapsed;

      _ellapsed = new long[NUM_JOBS];

      CommonStepJobs.Init();

      _is_multisite_center = false;

      while (true)
      {
        System.Threading.Thread.Sleep(WAIT_FOR_NEXT_TRY);

        _sql_conn = null;
        _step = 0;

        _is_multisite_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);
        if (!_is_multisite_center)
        {
          continue;
        }

        if (!Services.IsPrincipal("WWP CENTER"))
        {
          continue;
        }

        // Initializate the _ellapsed array
        for (int _idx_step = 0; _idx_step < NUM_JOBS; _idx_step++)
        {
          _ellapsed[_idx_step] = 0;
        }

        //ACC 10-MAY-2013 Extern Loyalty Program
        _is_elp = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 1;

        try
        {
          //
          // Only for Multisite Multicurrency
          //
          if (WSI.Common.CurrencyMultisite.IsEnabledMultiCurrency())
          {
            //
            // Manteinance Multiste Exchange Rates
            //
            _step++;
            _tick0 = Environment.TickCount;
            CommonStepJobs.ManteinanceExchangeRates();
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }
          
          //
          // If ExternalLoyaltyProgram is enabled i'm done
          //
          if (_is_elp)
          {
            //SPACE ACTIVATED
            continue;
          }         
          
          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          //
          // Buckets Expiration 
          // POINTS Expiration (Deprecated)
          //
          _step++;
          if (CommonMultiSite.GetPlayerTrackingMode() != PlayerTracking_Mode.Site)
          {
            _tick0 = Environment.TickCount;
            //FGB 09-MAR-2016 We change the Points Expiration by the Buckets Expiration
            //CommonStepJobs.AccountsPointsExpiration();
            Buckets.BucketsExpirationByInactivityDays();
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          //
          // Accounts Levels (Upgrade & Downgrade)
          //
          _step++;
          if (CommonMultiSite.GetPlayerTrackingMode() != PlayerTracking_Mode.Site)
          {
            _sql_conn = WGDB.Reconnect(_sql_conn);
            _tick0 = Environment.TickCount;
            CommonStepJobs.AccountsLevels(_sql_conn);
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          //
          // Delete empty Buckets
          //
          _step++;
          _tick0 = Environment.TickCount;
          //FGB 10-MAR-2016 Delete the empty buckets
          Buckets.BucketsEmpty();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          _step++;

          //
          // Delete empty Buckets when accountid 0
          //
          _step++;
          _tick0 = Environment.TickCount;
          //RLO 07-MAR-2017 Delete the empty buckets account id 0
          Buckets.BucketsCustomerIdEmpty();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          _step++;
          _ellapsed[_step] = 0;
          for (int _idx_step = 0; _idx_step < _step; _idx_step++)
          {
            if (_ellapsed[_idx_step] > 10000)
            {
              Log.Warning("PeriodicJob.Step[" + _idx_step.ToString() + "], Duration: " + _ellapsed[_idx_step].ToString() + " ms.");
            }
          }

        }
        catch (Exception _ex)
        {
          String _funtion = "NONE";

          switch (_step)
          {
            case 0:
              _funtion = "ManteinanceExchangeRates";
              break;
            case 1:
              _funtion = "BucketsExpirationByInactivityDays";
              break;
            case 2:
              _funtion = "AccountsLevels";
              break;
            case 3:
              _funtion = "BucketsEmpty";
              break;
            default:
              _funtion = "NONE";
              break;
          }

          Log.Warning("WWP_AccountCreditsExpiration.PeriodicJobThread. Exception in function: " + _funtion);
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // PeriodicJobThread

    #endregion Private Functions
  }
}
