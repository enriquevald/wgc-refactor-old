//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WWP_AccountsCreditsExpiration.cs
//
//   DESCRIPTION : WWP_AccountsCreditsExpiration class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAR-2013 AMF    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.WWP_CenterService
{
  class WWP_AccountsCreditsExpiration
  {
    private const Int32 WAIT_FOR_NEXT_TRY = (24 * 60 * 60 * 1000);  // 24 hours

    static Thread m_thread;

    static DateTime m_last_running_accounts_levels;

    //Indicates the random minutes to add to the running datetime of the update accounts levels.
    static Int32 m_rand_minutes_shift;


    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WWP_AccountsCreditsExpiration class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(PeriodicJobThread);
      m_thread.Name = "PeriodicJobThread";
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    static private void PeriodicJobThread()
    {
      SqlConnection _sql_conn;
      Int32 _step;
      Random _rnd;
      int _tick0;
      Boolean _is_multisite;
      long[] _ellapsed;
      DateTime _today;
      Int32 _wait_hint;

      _ellapsed = new long[20];

      // Initialize last running Date
      m_last_running_accounts_levels = WGDB.Now.Date.AddDays(-1);

      // Initialize the random minutes to shift
      _rnd = new Random(Environment.TickCount);
      m_rand_minutes_shift = _rnd.Next(0, 11);
      _is_multisite = false;
      _wait_hint = 0;

      while (true)
      {
        
        System.Threading.Thread.Sleep(_wait_hint);

        _is_multisite = GeneralParam.GetBoolean("Site", "IsCenter", false);

        _wait_hint = WAIT_FOR_NEXT_TRY;

        if (!_is_multisite)
        {
          _wait_hint = 3600000;
          continue;
        }

        _sql_conn = null;
        _step = 0;

        try
        {

          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          //
          // POINTS Expiration
          //
          if (!_is_multisite)
          {
            _tick0 = Environment.TickCount;
            CommonStepJobs.AccountsPointsExpiration();
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          }
          else
          {
            _ellapsed[_step] = 0; // DISABLED...
          }

          //
          // Accounts Levels (Upgrade & Downgrade)
          //
          _step++;
          if (!_is_multisite)
          {
            _sql_conn = WGDB.Reconnect(_sql_conn);
            _tick0 = Environment.TickCount;
            CommonStepJobs.AccountsLevels(m_rand_minutes_shift, m_last_running_accounts_levels, _sql_conn, out _today);
            m_last_running_accounts_levels = _today;
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }
          else
          {
            _ellapsed[_step] = 0; // DISABLED...
          }

          _step++;

          _ellapsed[_step] = 0;
          for (int _idx_step = 0; _idx_step < _step; _idx_step++)
          {
            if (_ellapsed[_idx_step] > 10000)
            {
              Log.Warning("PeriodicJob.Step[" + _idx_step.ToString() + "], Duration: " + _ellapsed[_idx_step].ToString() + " ms.");
            }
          }

        }
        catch (Exception _ex)
        {
          String _funtion = "NONE";

          switch (_step)
          {
            case 0:
              _funtion = "AccountsPointsExpiration";
              break;
            case 1:
              _funtion = "AccountsLevels";
              break;
            default:
              _funtion = "NONE";
              break;
          }

          Log.Warning("WWP_AccountCreditsExpiration.PeriodicJobThread. Exception in function: " + _funtion);
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // PeriodicJobThread

    #endregion Private Functions
  }
}
