//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerPromos_Class.cs
// 
//   DESCRIPTION: Manage LCD Params information
// 
//        AUTHOR: Joaquim Calero & Xavi Cots
// 
// CREATION DATE: 25-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 25-NOV-2014 XCD        First version.
// 22-FEB-2016 FJC        Product Backlog Item 9434:PimPamGo: Otorgar Premios.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPromoBalance(out WCP_IMessage Request, long SequenceId, long AccountId, long GameGatewayPromoBalanceAction)
    {
      WCP_Message _wcp_request;
      WCP_LCD_MsgGetPromoBalance _wcp_content;
      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_LCD_MsgGetPromoBalance);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_LCD_MsgGetPromoBalance)_wcp_request.MsgContent;
      _wcp_content.AccountId = (Int64)AccountId;
      _wcp_content.GameGatewayPromoBalanceAction= (Int16)GameGatewayPromoBalanceAction;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

    // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
    void WCP_IProtocol.GetReply_GetPromoBalance(WCP_IMessage ReceivedMessage, 
                                                out long CardPoints, 
                                                out long Redeemable, 
                                                out long PromoRedeemable, 
                                                out long PromoNotRedeemable, 
                                                out long Reserved,
                                                out long GameGatewayAwardedPrize)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetPromoBalanceReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetPromoBalanceReply)_msg.Message.MsgContent;

      CardPoints = (Int64) (Math.Floor(_message_content.PromoBalance.Points));
      Redeemable = (Int64)(_message_content.PromoBalance.Balance.Redeemable * 100m);
      PromoRedeemable = (Int64)(_message_content.PromoBalance.Balance.PromoRedeemable * 100m);
      PromoNotRedeemable = (Int64)(_message_content.PromoBalance.Balance.PromoNotRedeemable * 100m);
      Reserved = (Int64)(_message_content.PromoBalance.Balance.Reserved * 100m); // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
      
      // FJC Granted
      GameGatewayAwardedPrize = (Int64)(_message_content.GameGateWayAwardedPrizeAmount * 100m);
    }

  } // WCP_Protocol_Class
}