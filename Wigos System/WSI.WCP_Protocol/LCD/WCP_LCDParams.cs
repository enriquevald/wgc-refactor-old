//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerPromos_Class.cs
// 
//   DESCRIPTION: Manage LCD Params information
// 
//        AUTHOR: Joaquim Calero & Xavi Cots
// 
// CREATION DATE: 25-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ----------------------------------------------------------
// 25-JUL-2014 JCA & XCD        First version.
// 19-JAN-2016 FJC & ACC & JCA  Task 8544:Error Params BonoPLAY: SAS-HOST nuevo --> WCP antiguo 
// 22-FEB-2016 FJC              Product Backlog Item 9434:PimPamGo: Otorgar Premios.
// 04-APR-2016 FJC              PBI 9105:BonoPlay: LCD: Cambios varios
// 21-SEP-2017 FJC              WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 29-NOV-2017 FJC              WIGOS-3592 MobiBank: InTouch - Recharge request
//----------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetLCDParams(out WCP_IMessage Request, long SequenceId, Int32 TerminalId)
    {
      WCP_Message _wcp_request;
      WCP_LCD_MsgGetParameters _wcp_content;
      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_LCD_MsgGetParameters);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_LCD_MsgGetParameters)_wcp_request.MsgContent;

      _wcp_content.Terminal_Id = TerminalId;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

  } // WCP_Protocol_Class

  public partial class WCP_LCDParams_Class : WCP_ILCDParams
  {
    void WCP_ILCDParams.GetNumFunctionalities(WCP_IMessage ReceivedMessage, out Int32 NumFunc)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      NumFunc = _message_content.Functions.Count;
    }

    void WCP_ILCDParams.GetInTouchWebUrl(WCP_IMessage ReceivedMessage, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 120 + 1)] out String Url)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Url = _message_content.m_intouch_web_url;
    }

    void WCP_ILCDParams.GetFunctionality(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 Func)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Func = _message_content.Functions[Idx];
    }

    void WCP_ILCDParams.GetNumImages(WCP_IMessage ReceivedMessage, out Int32 NumPromoImages)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      NumPromoImages = _message_content.Images.Count;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Promo Image Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetImage(WCP_IMessage ReceivedMessage, int IndexImage, out WCP_WKT_Image Image)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Image = new WCP_WKT_Image();

      if (IndexImage < 0 || IndexImage >= _message_content.Images.Count)
      {
        return;
      }

      Image.ImageId = _message_content.Images[IndexImage].ImageId;
      Image.ResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _message_content.Images[IndexImage].ResourceId));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get GameGateway Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetGameGatewayParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsGamesGateWayParams Params)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Params = new WCP_LCD_ParamsGamesGateWayParams();

      Params.PartnerId = String.Empty;
      Params.ProviderName = String.Empty;
      Params.Url = String.Empty;
      Params.UrlTest = String.Empty;

      Params.GameGateway = _message_content.m_game_gateway_params.GameGateway;
      Params.ReservedCredit = _message_content.m_game_gateway_params.ReservedCredit;
      Params.AwardPrizes = (UInt16)_message_content.m_game_gateway_params.AwardPrizes;

      if (!String.IsNullOrEmpty(_message_content.m_game_gateway_params.PartnerId))
      {
        Params.PartnerId = _message_content.m_game_gateway_params.PartnerId;
      }
      if (!String.IsNullOrEmpty(_message_content.m_game_gateway_params.ProviderName))
      {
        Params.ProviderName = _message_content.m_game_gateway_params.ProviderName;
      }
      if (!String.IsNullOrEmpty(_message_content.m_game_gateway_params.Url))
      {
        Params.Url = _message_content.m_game_gateway_params.Url;
      }
      if (!String.IsNullOrEmpty(_message_content.m_game_gateway_params.UrlTest))
      {
        Params.UrlTest = _message_content.m_game_gateway_params.UrlTest;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get FB Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetFBParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsFBParams Params)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Params = new WCP_LCD_ParamsFBParams();

      Params.Enabled = _message_content.m_fb_params.Enabled;
      Params.PinRequest = _message_content.m_fb_params.PinRequest;
      Params.Url = String.Empty;
      Params.HostAddress = String.Empty;
      Params.Login = String.Empty;
      Params.Password = String.Empty;


      if (!String.IsNullOrEmpty(_message_content.m_fb_params.Url))
      {
        Params.Url = _message_content.m_fb_params.Url;
      }
      if (!String.IsNullOrEmpty(_message_content.m_fb_params.HostAddress))
      {
        Params.HostAddress = _message_content.m_fb_params.HostAddress;
      }
      if (!String.IsNullOrEmpty(_message_content.m_fb_params.Login))
      {
        Params.Login = _message_content.m_fb_params.Login;
      }
      if (!String.IsNullOrEmpty(_message_content.m_fb_params.Password))
      {
        Params.Password = _message_content.m_fb_params.Password;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get TerminalDraw Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetTerminalDrawParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsTerminalDrawParams Params)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Params = new WCP_LCD_ParamsTerminalDrawParams();

      Params.Enabled = _message_content.m_terminal_draw_params.Enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get TerminalReserve Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetTerminalReserveParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsTerminalReserveParams Params)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Params = new WCP_LCD_ParamsTerminalReserveParams();

      Params.Enabled = _message_content.m_terminal_reserve_params.Enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get MobiBank Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ILCDParams.GetMobiBankParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsMobiBankParams Params)
    {
      WCP_MessageEnvelope _msg;
      WCP_LCD_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_LCD_MsgGetParametersReply)_msg.Message.MsgContent;

      Params = new WCP_LCD_ParamsMobiBankParams();

      Params.Enabled = _message_content.m_mobibank_params.Enabled;
    }


    #region WCP_ILCDParams Members

    #endregion
  } // WCP_LCDParams_Class
}