//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProtocolGetReply.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 18-NOV-2011 ACC    Added Machine meters
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 21-JUN-2012 XIT    Modified GeParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 08-AUG-2012 ACC    Split WCP_Protocol.cs.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 06-NOV-2013 NMR    Messages reorganization
// 07-NOV-2013 NMR    Fixed error: missing parameter to send to Terminal
// 07-NOV-2013 NMR    Fixed error: missing parameter to machin_id Terminal
// 04-AUG-2014 ACC    Added TitoCloseSessionWhenRemoveCard parameter.
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 21-JAN-2015 JMV    JMV - Added number decimal parameters
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 06-OCT-2015 FJC    Product Backlog 4704
// 17-NOV-2015 ETP    Product Backlog Item 5835: Added transfer amounts.
// 19-JAN-2016 FJC    Product Backlog Item 7732:Salones(2): Introducci�n contadores PCD desde el Cajero: Parte WCP/SAS-Host
// 30-JAN-2017 FJC    PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
// 31-MAR-2017 FJC    PBI 26015:ZitroTrack
// 02-AUG-2017 JMM    PBI 28983: EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 19-MAR-2018 LRS    WIGOS-6150  Codere_Argentina_1428 - Apply tax on Bill in at EGM.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.GetReply_ReportPlay(WCP_IMessage ReceivedMessage, out long CardBalanceAfterPlay)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgReportPlayReply reply;

      CardBalanceAfterPlay = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgReportPlayReply)msg.Message.MsgContent;

      CardBalanceAfterPlay = reply.SessionBalanceAfterPlay;
    } // Get_ReportPlayReply

    void WCP_IProtocol.GetReply_PlayConditions(WCP_IMessage ReceivedMessage, out long CardBalanceBeforePlay)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgPlayConditionsReply reply;

      CardBalanceBeforePlay = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgPlayConditionsReply)msg.Message.MsgContent;

      CardBalanceBeforePlay = reply.SessionBalanceBeforePlay;
    } // Get_ReportPlayReply

    void WCP_IProtocol.GetReply_StartCardSession(WCP_IMessage ReceivedMessage, 
                                                 out long CardSessionId, 
                                                 out String HolderName, 
                                                 out long CardPoints, 
                                                 out long AllowCashIn, 
                                                 out long Redeemable, 
                                                 out long PromoRedeemable, 
                                                 out long PromoNotRedeemable,
                                                 out long ReservedMode)//, out String NRTransferAmounts)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgStartCardSessionReply reply;

      CardSessionId = 0;
      AllowCashIn = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgStartCardSessionReply)msg.Message.MsgContent;

      CardSessionId = reply.CardSessionId;
      HolderName = reply.HolderName;
      CardPoints = reply.Points;
      if (reply.AllowCashIn)
      {
        AllowCashIn = 1;
      }

      // ACC 03-AUG-2012 LKT New with Old WCP Service
      if (reply.Balance.TotalBalance == 0 && reply.CardBalance > 0)
      {
        reply.Balance.Redeemable = ((Decimal)reply.CardBalance) / 100m;
      }

      Redeemable = (Int64)(reply.Balance.Redeemable * 100m);
      PromoRedeemable = (Int64)(reply.Balance.PromoRedeemable * 100m);
      PromoNotRedeemable = (Int64)(reply.Balance.PromoNotRedeemable * 100m);
      ReservedMode = reply.Balance.ModeReserved ? 1 : 0;
     // NRTransferAmounts = (String)(reply.NR_TransferAmounts);

    } // Get_StartCardSessionReply

    void WCP_IProtocol.GetReply_CardInformation(WCP_IMessage ReceivedMessage, out long CardBalance, out String HolderName, out long CardPoints, out DateTime BirthDate, out String Level, out String LastTerminalName, out String NRTransferAmounts)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgCardInformationReply reply;

      CardBalance = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgCardInformationReply)msg.Message.MsgContent;

      CardBalance = reply.CardBalance;
      HolderName = reply.HolderName;
      CardPoints = reply.Points;
      BirthDate = reply.Birthday;
      Level = reply.Level;
      LastTerminalName = reply.LastTerminal;
      //ETP PBI 5835 Added optional transfer amounts.

      if (reply.NRTransferAmount == null)
      {
        NRTransferAmounts = "";
      }
      else
      {
        NRTransferAmounts = (reply.NRTransferAmount.Length > 40) ? reply.NRTransferAmount.Remove(40)
                                                                                    : reply.NRTransferAmount;
      }

    } // GetReply_CardInformation

    void WCP_IProtocol.GetReply_EndSession(WCP_IMessage ReceivedMessage, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName, out long CardPoints, out long Redeemable, out long PromoRedeemable, out long PromoNotRedeemable)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgEndSessionReply reply;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgEndSessionReply)msg.Message.MsgContent;

      HolderName = reply.HolderName;
      CardPoints = reply.Points;

      // ACC 03-AUG-2012 LKT New with Old WCP Service
      if (reply.Balance.TotalBalance == 0 && reply.CardBalance > 0)
      {
        reply.Balance.Redeemable = ((Decimal)reply.CardBalance) / 100m;
      }

      Redeemable = (Int64)(reply.Balance.Redeemable * 100m);
      PromoRedeemable = (Int64)(reply.Balance.PromoRedeemable * 100m);
      PromoNotRedeemable = (Int64)(reply.Balance.PromoNotRedeemable * 100m);

    } // GetReply_EndSession

    //------------------------------------------------------------------------------
    // PURPOSE: Get Parameters to return to C App
    // 
    //  PARAMS:
    //      - INPUT: WCP_IMessage ReceivedMessage
    //
    //      - OUTPUT:WCP_Parameters Parameters
    //
    // RETURNS: void
    // 
    //   NOTES:

    void WCP_IProtocol.GetReply_GetParameters(WCP_IMessage ReceivedMessage, out WCP_Parameters Parameters)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgGetParametersReply reply;
      WCP_Parameters parameters;
      DateTime utc_time;

      parameters = new WCP_Parameters();

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgGetParametersReply)msg.Message.MsgContent;

      parameters.connection_timeout = 1000 * reply.ConnectionTimeout;
      parameters.keep_alive_interval = 1000 * reply.KeepAliveInterval;

      // Get Local Time
      utc_time = msg.Message.MsgHeader.GetSystemTime;
      parameters.utc_datetime = utc_time.ToFileTime();
      parameters.time_zone = msg.Message.MsgHeader.GetTimeZone;

      parameters.machine_name = reply.MachineName;
      parameters.asset_number = reply.AssetNumber;

      //ADDED 21-JUN-2012 XIT
      if (reply.NoteAcceptorParameters == null)
      {
        parameters.read_barcode = false;
        parameters.num_currencies = 0;
        parameters.note_acceptor_enabled = false;
      }
      else
      {
        parameters.read_barcode = reply.NoteAcceptorParameters.ReadBarcode;
        parameters.num_currencies = reply.NoteAcceptorParameters.NumCurrencies;
        parameters.note_acceptor_enabled = reply.NoteAcceptorParameters.Enabled;
      }

      parameters.sas_flags = reply.SasFlags;

      if (reply.WelcomeMessageLine1 == null)
      {
        parameters.welcome_display_line_1 = "";
      }
      else
      {
        parameters.welcome_display_line_1 = (reply.WelcomeMessageLine1.Length > 40) ? reply.WelcomeMessageLine1.Remove(40)
                                                                                    : reply.WelcomeMessageLine1;
      }

      if (reply.WelcomeMessageLine1 == null)
      {
        parameters.welcome_display_line_2 = "";
      }
      else
      {
        parameters.welcome_display_line_2 = (reply.WelcomeMessageLine2.Length > 40) ? reply.WelcomeMessageLine2.Remove(40)
                                                                                    : reply.WelcomeMessageLine2;
      }

      if (reply.CurrencySymbol == null)
      {
        parameters.currency_symbol = "";
      }
      else
      {
        parameters.currency_symbol = (reply.CurrencySymbol.Length > 10) ? reply.CurrencySymbol.Remove(10)
                                                                      : reply.CurrencySymbol;
      }

      parameters.language_id = (uint)LanguageIdentifier.Spanish; //Spanish as default value
      if (reply.Language != null)
      {
        if (reply.Language == "en-US") //English
        {
          parameters.language_id = (uint)LanguageIdentifier.English;
        }
        else if (reply.Language == "es") //Spanish
        {
          parameters.language_id = (uint)LanguageIdentifier.Spanish;
        }
        else if (reply.Language == "fr") //French
        {
            parameters.language_id = (uint)LanguageIdentifier.French;
        }
      }

      parameters.system_mode = (uint)reply.SystemMode;
      parameters.wass_machine_locked = (uint)reply.WassMachineLocked;

      parameters.tito_close_session_when_remove_card = (uint)reply.TitoCloseSessionWhenRemoveCard;

      // Compatibility with old Service
      if (reply.TitoMode)
      {
        parameters.system_mode = 1;
      }

      // TITO mode params
      parameters.tito_mode = reply.TitoMode;

      if (parameters.tito_mode == true)
      {
        parameters.system_id = reply.SystemId;
        parameters.sequence_id = reply.SequenceId;
        parameters.machine_id = (Int32)reply.MachineId;
        parameters.allow_cashable_ticket_out = reply.AllowCashableTicketOut;
        parameters.allow_promotional_ticket_out = reply.AllowPromotionalTicketOut;
        parameters.allow_ticket_in = reply.AllowTicketIn;
        parameters.expiration_tickets_cashable = reply.ExpirationTicketsCashable;
        parameters.expiration_tickets_promotional = reply.ExpirationTicketsPromotional;
        parameters.tickets_max_creation_amount_cents = reply.TicketsMaxCreationAmount;
        parameters.bills_meters_required = reply.BillsMetersRequired;

        parameters.tito_ticket_localization = reply.TitoTicketLocalization == null ?
                                                  "" :
                                                  reply.TitoTicketLocalization.Length > 40 ?
                                                  reply.TitoTicketLocalization.Remove(40) :
                                                  reply.TitoTicketLocalization;

        parameters.tito_ticket_address_1 = reply.TitoTicketAddress_1 == null ?
                                                  "" :
                                                  reply.TitoTicketAddress_1.Length > 40 ?
                                                  reply.TitoTicketAddress_1.Remove(40) :
                                                  reply.TitoTicketAddress_1;

        parameters.tito_ticket_address_2 = reply.TitoTicketAddress_2 == null ?
                                                  "" :
                                                  reply.TitoTicketAddress_2.Length > 40 ?
                                                  reply.TitoTicketAddress_2.Remove(40) :
                                                  reply.TitoTicketAddress_2;

        parameters.tito_ticket_title_restricted = reply.TitoTicketTitleRestricted == null ?
                                                  "" :
                                                  reply.TitoTicketTitleRestricted.Length > 16 ?
                                                  reply.TitoTicketTitleRestricted.Remove(16) :
                                                  reply.TitoTicketTitleRestricted;

        parameters.tito_ticket_title_debit = reply.TitoTicketTitleDebit == null ?
                                                  "" :
                                                  reply.TitoTicketTitleDebit.Length > 16 ?
                                                  reply.TitoTicketTitleDebit.Remove(16) :
                                                  reply.TitoTicketTitleDebit;

        parameters.host_id = reply.HostId;
      }

      // JMV
      parameters.format_decimals_flag = reply.FormatDecimalsFlags;
      parameters.num_decimals = reply.NumDecimals;

      parameters.change_stacker_mode = (uint)reply.ChangeStackerMode;
      parameters.current_stacker_id = reply.CurrentStackerId;

      parameters.smib_protocol = (int)reply.Protocol;
      parameters.smib_protocol_request_params = reply.RequestProtocolParameters;      

      // FJC 29-JUN-2015
      parameters.technician_activity_timeout = (Int32)reply.TechnicianActivityTimeout;

      // JMM 21-MAR-2016: Mico2 mode
      parameters.mico2 = reply.Mico2;
      parameters.enable_disable_note_acceptor = reply.EnableDisableNoteAcceptor;

      // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
      parameters.enable_disable_terminal_draw = reply.EnableDisableTerminalDraw;

      // FJC 24-MAR-2017 PBI 26015:ZitroTrack
      parameters.enable_disable_zitro_track = reply.EnableDisableZitroTrack;
      
      // JMM 13-SEP-2017: BillIn lock limit
      parameters.bill_in_limit_lock_threshold = reply.BillInLimitLockThreshold;
      parameters.bill_in_limit_lock_mode = reply.BillInLimitLockMode;

      // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
      parameters.enable_disable_mobibank = reply.EnableDisableMobiBank;

      Parameters = parameters;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_Msg_GetCardTypeReply to return to C App
    // 
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT:WCP_Parameters Parameters
    //
    // RETURNS :
    // 
    //   NOTES :

    void WCP_IProtocol.GetReply_GetCardType(WCP_IMessage ReceivedMessage,
                                            out WCP_ICardType CardType,
                                            out long AccountId,
                                            [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName,
                                            [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String MessageLine1,
                                            [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String MessageLine2)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgGetCardTypeReply reply;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgGetCardTypeReply)msg.Message.MsgContent;




      switch (reply.CardType)
      {
        case WCP_CardTypes.CARD_TYPE_UNKNOWN:
          CardType = WCP_ICardType.Unknown;
          break;

        case WCP_CardTypes.CARD_TYPE_PLAYER:
          CardType = WCP_ICardType.Player;
          break;

        case WCP_CardTypes.CARD_TYPE_MOBILE_BANK:
          CardType = WCP_ICardType.MobileBank;
          break;

        case WCP_CardTypes.CARD_TYPE_PLAYER_PIN:
          CardType = WCP_ICardType.PlayerPin;
          break;

        case WCP_CardTypes.CARD_TYPE_TECH:
          CardType = WCP_ICardType.Tech;
          break;

        case WCP_CardTypes.CARD_TYPE_TECH_PIN:
          CardType = WCP_ICardType.TechPin;
          break;

        case WCP_CardTypes.CARD_TYPE_CHANGE_STACKER:
          CardType = WCP_ICardType.ChangeStacker;
          break;

        case WCP_CardTypes.CARD_TYPE_CHANGE_STACKER_PIN:
          CardType = WCP_ICardType.ChangeStackerPin;
          break;

        case WCP_CardTypes.CARD_TYPE_EMPLOYEE:
          CardType = WCP_ICardType.CardTypeEmployee;
          break;

        default:
          CardType = WCP_ICardType.Unknown;
          break;
      }

      AccountId = reply.AccountId;
      HolderName = (reply.HolderName.Length > 80) ? reply.HolderName.Remove(80)
                                                    : reply.HolderName;
  
      MessageLine1 = (reply.MessageLine1.Length > 50) ? reply.MessageLine1.Remove(50)
                                                      : reply.MessageLine1;
      MessageLine2 = (reply.MessageLine1.Length > 50) ? reply.MessageLine2.Remove(50)
                                                      : reply.MessageLine2;      

    } // GetReply_GetCardType

    //
    // Mobile Bank
    //
    void WCP_IProtocol.GetReply_MobileBankCardCheck(WCP_IMessage ReceivedMessage, out long MobileBankAuthorizedAmountx100)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgMobileBankCardCheckReply reply;

      MobileBankAuthorizedAmountx100 = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgMobileBankCardCheckReply)msg.Message.MsgContent;

      MobileBankAuthorizedAmountx100 = reply.MobileBankAuthorizedAmountx100;
    } // GetReply_MobileBankCardCheck

    void WCP_IProtocol.GetReply_MobileBankCardTransfer(WCP_IMessage ReceivedMessage, out long PlayerPreviousCardBalancex100, out long PlayerCurrentCardBalancex100)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgMobileBankCardTransferReply reply;

      PlayerPreviousCardBalancex100 = 0;
      PlayerCurrentCardBalancex100 = 0;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgMobileBankCardTransferReply)msg.Message.MsgContent;

      PlayerPreviousCardBalancex100 = reply.PlayerPreviousCardBalancex100;
      PlayerCurrentCardBalancex100 = reply.PlayerCurrentCardBalancex100;
    } // GetReply_MobileBankCardTransfer

    void WCP_IProtocol.GetReply_ReportEventList(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_MsgReportPlayReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_MsgReportPlayReply)msg.Message.MsgContent;
    }

    void WCP_IProtocol.GetRequest_ExecuteCommand(WCP_IMessage ReceivedMessage, 
                                                 out long CommandType,
                                                 [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 500)] out String CommandParameters,
                                                 [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String Msg_Line1,
                                                 [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String Msg_Line2,
                                                 out long Msg_TimeShowMessage,
                                                 out long Msg_NumTimes,
                                                 out long Msg_TimeBetweenMessages)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgExecuteCommand request;
      ShowMessageParameters _show_message;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      request = (WCP_MsgExecuteCommand)msg.Message.MsgContent;

      Msg_Line1 = "";
      Msg_Line2 = "";
      Msg_TimeShowMessage = 0;
      Msg_NumTimes = 0;
      Msg_TimeBetweenMessages = 0;

      CommandType = (long)request.Command;
      CommandParameters = request.CommandParameters;

      if (request.Command == WCP_CommandTypes.WCP_CMD_SHOW_MESSAGE)
      {
        _show_message = new ShowMessageParameters();
        _show_message.LoadXml(request.CommandParameters);

        Msg_Line1 = (_show_message.Line1.Length > 100) ? _show_message.Line1.Remove(100)
                                                       : _show_message.Line1;
        Msg_Line2 = (_show_message.Line1.Length > 100) ? _show_message.Line2.Remove(100)
                                                       : _show_message.Line2;
        Msg_TimeShowMessage = _show_message.TimeShowMessage;
        Msg_NumTimes = _show_message.NumTimes;
        Msg_TimeBetweenMessages = _show_message.TimeBetweenMessages;
      }
    }
    
    //FJC 12-NOV-2014
    void WCP_IProtocol.GetRequest_ExCmdSiteJackpotAwarded(WCP_IMessage ReceivedMessage,
                                                          out WCP_SiteJackpotAwarded SiteJackpotAwarded)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgExecuteCommand request;
      WSI.Common.SiteJackpotAwarded _site_jackpot_awarded;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      request = (WCP_MsgExecuteCommand)msg.Message.MsgContent;

      SiteJackpotAwarded.jackpot_id = 0;
      SiteJackpotAwarded.jackpot_datetime = 0;
      SiteJackpotAwarded.jackpot_level_id = 0;
      SiteJackpotAwarded.jackpot_level_name = "";
      SiteJackpotAwarded.jackpot_amount_cents = 0;
      SiteJackpotAwarded.jackpot_machine_name = "";
      SiteJackpotAwarded.winner_track_data = "";
      SiteJackpotAwarded.winner_holder_name = "";
      SiteJackpotAwarded.parameters_show_only_to_winner = 0;
      SiteJackpotAwarded.parameters_winner_show_time_seconds = 0;
      SiteJackpotAwarded.parameters_no_winner_show_time_seconds = 0;

      if (request.Command == WCP_CommandTypes.WCP_CMD_SITE_JACKPOT_AWARDED)
      {
        _site_jackpot_awarded = new WSI.Common.SiteJackpotAwarded();
        _site_jackpot_awarded.LoadXml(request.CommandParameters);
        
        //JackPot Data
        SiteJackpotAwarded.jackpot_id = _site_jackpot_awarded.JackpotId;
        SiteJackpotAwarded.jackpot_datetime = _site_jackpot_awarded.JackpotDatetime.ToFileTime();
        SiteJackpotAwarded.jackpot_level_id = _site_jackpot_awarded.JackpotLevelId;
        
        SiteJackpotAwarded.jackpot_level_name = (_site_jackpot_awarded.JackpotLevelName.Length > 100) ?
                                                                                      _site_jackpot_awarded.JackpotLevelName.Remove(100)
                                                                                    : _site_jackpot_awarded.JackpotLevelName;

        SiteJackpotAwarded.jackpot_amount_cents = _site_jackpot_awarded.JackpotAmountCents;

        SiteJackpotAwarded.jackpot_machine_name = (_site_jackpot_awarded.JackpotMachineName.Length > 100) ?
                                                                                      _site_jackpot_awarded.JackpotMachineName.Remove(100)
                                                                                    : _site_jackpot_awarded.JackpotMachineName;

        //Winner JackPot Data
        SiteJackpotAwarded.winner_track_data = (_site_jackpot_awarded.WinnerTrackData.Length > 50) ?
                                                                                      _site_jackpot_awarded.WinnerTrackData.Remove(50)
                                                                                    : _site_jackpot_awarded.WinnerTrackData;
        
        SiteJackpotAwarded.winner_holder_name = (_site_jackpot_awarded.WinnerHolderName.Length > 81) ?
                                                                                              _site_jackpot_awarded.WinnerHolderName.Remove(81)
                                                                                            : _site_jackpot_awarded.WinnerHolderName;

        //Parameters JackPot Data
        SiteJackpotAwarded.parameters_show_only_to_winner = _site_jackpot_awarded.ParametersShowOnlyToWinner;
        SiteJackpotAwarded.parameters_winner_show_time_seconds = _site_jackpot_awarded.ParametersWinnerShowTimeSeconds;
        SiteJackpotAwarded.parameters_no_winner_show_time_seconds = _site_jackpot_awarded.ParametersNoWinnerShowTimeSeconds;
      }
    }

    void WCP_IProtocol.GetReply_AddCredit(WCP_IMessage ReceivedMessage, out long BalanceAfterAdd, out Boolean AccountAlreadyHasPromo, out Boolean BillInGivesPromo)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgAddCreditReply _reply;

      BalanceAfterAdd = 0;
      AccountAlreadyHasPromo = false;
      BillInGivesPromo = false;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgAddCreditReply)_msg.Message.MsgContent;

      BalanceAfterAdd = _reply.SessionBalanceAfterAdd;
      AccountAlreadyHasPromo = _reply.AccountAlreadyHasPromo;
      BillInGivesPromo = _reply.BillInGivesPromo;

    } // GetReply_AddCredit

    void WCP_IProtocol.GetReply_SubsMachinePartialCredit(WCP_IMessage ReceivedMessage, 
                                                         out long RequestAmount, 
                                                         out long ObtainedAmount) 
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgSubsMachinePartialCredit _subs_machine_partial_credit;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      RequestAmount = 0;
      ObtainedAmount = 0;
      
      if (_reply.Command == WCP_CommandTypes.WCP_CMD_SUBS_MACHINE_PARTIAL_CREDIT)
      {
        _subs_machine_partial_credit = new WCP_MsgSubsMachinePartialCredit();
        _subs_machine_partial_credit.LoadXml(_reply.CommandParameters);

        RequestAmount = _subs_machine_partial_credit.RequestAmount;
        ObtainedAmount= _subs_machine_partial_credit.ObtainedAmount;
      }
    } // GetReply_SubsMachinePartialCredit

    void WCP_IProtocol.GetReply_ExcmdGameGateway(WCP_IMessage ReceivedMessage,
                                                 out WCP_GameGateWaySession GameGateWaySession)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgGameGateway _game_gateway_msg;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      GameGateWaySession = new WCP_GameGateWaySession();
      GameGateWaySession.MessageText = String.Empty;

      if (_reply.Command == WCP_CommandTypes.WCP_CMD_GAMEGATEWAY)
      {
        _game_gateway_msg = new WCP_MsgGameGateway();
        _game_gateway_msg.LoadXml(_reply.CommandParameters);

        GameGateWaySession.MessageType = _game_gateway_msg.MsgType;
        GameGateWaySession.MessageText = _game_gateway_msg.MsgText;
      }
    } // GetReply_ExcmdGameGateway

    void WCP_IProtocol.GetReply_ExcmdGameGatewayGetCredit(WCP_IMessage ReceivedMessage,
                                                          out WCP_GameGateWayGetCredit GameGateWayGetCredit)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgGameGatewayGetCredit _game_gateway_get_credit;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      GameGateWayGetCredit = new WCP_GameGateWayGetCredit();
      GameGateWayGetCredit.MessageText = String.Empty;

      if (_reply.Command == WCP_CommandTypes.WCP_CMD_GAMEGATEWAY_GET_CREDIT)
      {
        _game_gateway_get_credit = new WCP_MsgGameGatewayGetCredit();
        _game_gateway_get_credit.LoadXml(_reply.CommandParameters);

        GameGateWayGetCredit.AccountId = _game_gateway_get_credit.AccountId;
        GameGateWayGetCredit.RequestAmount= _game_gateway_get_credit.RequestAmount;
        GameGateWayGetCredit.Status = _game_gateway_get_credit.Status;
        GameGateWayGetCredit.MessageType = _game_gateway_get_credit.MsgType;
        GameGateWayGetCredit.MessageText = _game_gateway_get_credit.MsgText;
      }
    } // GetReply_ExcmdGameGatewayGetCredit

    void WCP_IProtocol.GetReply_GameGatewayGetCredit(WCP_IMessage ReceivedMessage,
                                                      out WCP_GameGateWayGetCredit GameGateWayGetCredit)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgGameGatewayGetCreditReply _reply;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgGameGatewayGetCreditReply)_msg.Message.MsgContent;

      GameGateWayGetCredit = new WCP_GameGateWayGetCredit();
      GameGateWayGetCredit.MessageText = String.Empty;
      GameGateWayGetCredit.Status = _reply.Status;

    } // GetReply_GameGatewayGetCredit

    void WCP_IProtocol.GetNumPCDMeters(WCP_IMessage ReceivedMessage, out long NumPCDMeters)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgExecuteCommand _reply;
      WSI.Common.PCD_UpdatePCDMeters _update_pcd_meters;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_request.Message.MsgContent;

      _update_pcd_meters = new WSI.Common.PCD_UpdatePCDMeters();
      _update_pcd_meters.LoadXml(_reply.CommandParameters);

      NumPCDMeters = _update_pcd_meters.UpdatablePCDMeterList.Count;
    } // GetNumPCDMeters

    void WCP_IProtocol.GetPCDMetersItem(WCP_IMessage ReceivedMessage, 
                                        int IndexPCDItem,
                                        out WCP_UpdatablePCDMeter UpdatePCDMeters)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgExecuteCommand _reply;
      WSI.Common.PCD_UpdatePCDMeters _update_pcd_meters;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_request.Message.MsgContent;

      _update_pcd_meters = new WSI.Common.PCD_UpdatePCDMeters();
      _update_pcd_meters.LoadXml(_reply.CommandParameters);

      UpdatePCDMeters = new WCP_UpdatablePCDMeter();

      if (IndexPCDItem < 0 || IndexPCDItem >= _update_pcd_meters.UpdatablePCDMeterList.Count)
      {
        return;
      }

      UpdatePCDMeters.Code   = _update_pcd_meters.UpdatablePCDMeterList[IndexPCDItem].Code;
      UpdatePCDMeters.Action = _update_pcd_meters.UpdatablePCDMeterList[IndexPCDItem].Action;
      UpdatePCDMeters.Value  = _update_pcd_meters.UpdatablePCDMeterList[IndexPCDItem].Value;
    } // GetPCDMetersItem

    void WCP_IProtocol.GetReply_ReserveTerminal(WCP_IMessage ReceivedMessage, out Int32 ReserveError, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 200)] out String ReserveErrorMsg, out Int32 ExpirationMinutes)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgReserveTerminalReply _reply;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgReserveTerminalReply)_request.Message.MsgContent;

      ReserveError = _reply.ReserveError;
      ReserveErrorMsg = _reply.ReserveErrorMsg;
      ExpirationMinutes = _reply.ExpirationMinutes;
    } // GetReply_ReserveTerminal

    void WCP_IProtocol.GetReply_MobiBankRecharge(WCP_IMessage ReceivedMessage)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgMobiBankRechargeReply _reply;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgMobiBankRechargeReply)_request.Message.MsgContent;
    } // GetReply_MobiBankRecharge    

    void WCP_IProtocol.GetReply_MobiBankCancelRequest(WCP_IMessage ReceivedMessage)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgMobiBankCancelRequestReply _reply;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgMobiBankCancelRequestReply)_request.Message.MsgContent;
    } // GetReply_MobiBankCancelRequest    


    void WCP_IProtocol.GetReply_MobiBankRechargeNotification(WCP_IMessage ReceivedMessage, out WCP_MobiBankRechargeNotification MobiBankRechargeNotification)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgMobiBankRechargeNotification _mobibank_recharge_notification;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_request.Message.MsgContent;
      MobiBankRechargeNotification = new WCP_MobiBankRechargeNotification();

      if (_reply.Command == WCP_CommandTypes.WCP_CMD_MOBIBANK_RECHARGE_NOTIFICATION)
      {
        _mobibank_recharge_notification = new WCP_MsgMobiBankRechargeNotification();
        _mobibank_recharge_notification.LoadXml(_reply.CommandParameters);
        MobiBankRechargeNotification.AccountId = _mobibank_recharge_notification.AccountId;
        MobiBankRechargeNotification.TransFerType = (Int32)_mobibank_recharge_notification.TransFerType;
        MobiBankRechargeNotification.TransFerAmount = _mobibank_recharge_notification.TransFerAmount;
      }
    } // GetReply_MobiBankRechargeNotification    

    void WCP_IProtocol.GetReply_MobiBankRequestTransFer(WCP_IMessage ReceivedMessage, out WCP_MobiBankRequestTransFer MobiBankRequestTransfer)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgMobiBankRequestTransfer _mobibank_request_transfer;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_request.Message.MsgContent;
      MobiBankRequestTransfer = new WCP_MobiBankRequestTransFer();

      if (_reply.Command == WCP_CommandTypes.WCP_CMD_MOBIBANK_REQUEST_TRANSFER)
      {
        _mobibank_request_transfer = new WCP_MsgMobiBankRequestTransfer();
        _mobibank_request_transfer.LoadXml(_reply.CommandParameters);
        MobiBankRequestTransfer.CmdId = _mobibank_request_transfer.CmdId;
        MobiBankRequestTransfer.RequestId = _mobibank_request_transfer.RequestId;
      }
    } // GetReply_MobiBankRequestTransFer    

    void WCP_IProtocol.GetReply_MobiBankCancelRequestFromApp(WCP_IMessage ReceivedMessage, out WCP_MobiBankCancelRequest MobiBankCancelRequestRechargeFromApp)
    {
      WCP_MessageEnvelope _request;
      WCP_MsgExecuteCommand _reply;
      WCP_MsgMobiBankCancelRequest _mobibank_cancel_request_recharge_from_app;

      _request = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgExecuteCommand)_request.Message.MsgContent;
      MobiBankCancelRequestRechargeFromApp = new WCP_MobiBankCancelRequest();

      if (_reply.Command == WCP_CommandTypes.WCP_CMD_MOBIBANK_CANCEL_REQUEST)
      {
        _mobibank_cancel_request_recharge_from_app = new WCP_MsgMobiBankCancelRequest();
        _mobibank_cancel_request_recharge_from_app.LoadXml(_reply.CommandParameters);
        MobiBankCancelRequestRechargeFromApp.AccountId = _mobibank_cancel_request_recharge_from_app.AccountId;
        MobiBankCancelRequestRechargeFromApp.CancelReason = (Int32)_mobibank_cancel_request_recharge_from_app.CancelReason;
      }
    } // GetReply_MobiBankCancelRequestRechargeFromApp    

  } // WCP_Protocol_Class 
}
