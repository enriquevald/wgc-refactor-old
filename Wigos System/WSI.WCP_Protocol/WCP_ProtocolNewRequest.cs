//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProtocolNewRequest.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 18-NOV-2011 ACC    Added Machine meters
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 21-JUN-2012 XIT    Modified GeParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 08-AUG-2012 ACC    Split WCP_Protocol.cs.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 24-OCT-2013 NMR    SystemId removed from message; renamed MachineValidation
// 06-NOV-2013 NMR    Messages reorganization
// 06-OCT-2015 FJC    Product Backlog 4704
// 19-FEB-2016 ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 22-NOV-2016 JMM    PBI 19744:Pago automático de handpays
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 19-MAR-2018 LRS    WIGOS-6150  Codere_Argentina_1428 - Apply tax on Bill in at EGM.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_Play(out WCP_IMessage Request,
                                       long SequenceId,
                                       long TransactionId,
                                       long CardSessionId,
                                       long CardBalanceBeforePlay,
                                       long CardBalanceAfterPlay,
                                       ushort GameId,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                       long PlayedAmount,
                                       long WonAmount,
                                       long Denomination,
                                       double PayoutPercentage)
    {
      WCP_Message WCP_request;
      WCP_MsgReportPlay WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportPlay);
      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content = (WCP_MsgReportPlay)WCP_request.MsgContent;

      WCP_content.TransactionId = TransactionId;
      WCP_content.CardSessionId = CardSessionId;
      WCP_content.SessionBalanceBeforePlay = CardBalanceBeforePlay;
      WCP_content.SessionBalanceAfterPlay = CardBalanceAfterPlay;
      WCP_content.GameId = GameName;
      WCP_content.PlayedAmount = PlayedAmount;
      WCP_content.Denomination = (int)Denomination;
      WCP_content.WonAmount = WonAmount;
      WCP_content.PayoutPercentage = PayoutPercentage;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    }

    void WCP_IProtocol.NewRequest_PlayConditions(out WCP_IMessage Request,
                                                 long SequenceId,
                                                 long CardSessionId,
                                                 long CardBalanceBeforePlay,
                                                 ushort GameId,
                                                 [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                                 long PlayedAmount)
    {
      WCP_Message WCP_request;
      WCP_MsgPlayConditions WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgPlayConditions);
      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content = (WCP_MsgPlayConditions)WCP_request.MsgContent;

      WCP_content.CardSessionId = CardSessionId;
      WCP_content.SessionBalanceBeforePlay = CardBalanceBeforePlay;
      WCP_content.GameId = GameName;
      WCP_content.BetAmount = PlayedAmount;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    }

    
    void WCP_IProtocol.NewRequest_StartCardSession(out WCP_IMessage Request,
                                                   long SequenceId,
                                                   long TransactionId,
                                                   [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData,
                                                   [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin,
                                                   ushort TransferMode,
                                                   long TransferRedeemableCents,
                                                   long TransferPromoRedeemableCents,
                                                   long TransferPromoNotRedeemableCents,
                                                   Boolean TransferWithBuckets)
    {

      WCP_Message WCP_request;
      WCP_MsgStartCardSession WCP_content;
      MultiPromos.AccountBalance _account_balance;

      _account_balance = MultiPromos.AccountBalance.Zero;

      _account_balance.Redeemable = TransferRedeemableCents * 0.01m;
      _account_balance.PromoRedeemable = TransferPromoRedeemableCents * 0.01m;
      _account_balance.PromoNotRedeemable = TransferPromoNotRedeemableCents * 0.01m;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgStartCardSession);

      WCP_request.MsgHeader.SequenceId = SequenceId;
      //WCP_request.MsgHeader.TransactionId = TransactionId ;

      WCP_content = (WCP_MsgStartCardSession)WCP_request.MsgContent;

      WCP_content.SetTransactionId(TransactionId);
      WCP_content.SetTrackData(0, TrackData);
      WCP_content.SetPin(Pin);

      WCP_content.SetTranferMode(TransferMode);
      WCP_content.SetBalanceToTransfer(_account_balance);

      WCP_content.SetTransferWithBuckets(TransferWithBuckets);

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    }

    //
    // Cancel Start Session
    //
    void WCP_IProtocol.NewRequest_CancelStartSession(out WCP_IMessage Request,
                                                     long SequenceId,
                                                     long TransactionId,                                                     
                                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData,
                                                     long CancelledRedeemableCents,
                                                     long CancelledPromoRedeemableCents,
                                                     long CancelledPromoNotRedeemableCents)
    {

      WCP_Message WCP_request;
      WCP_MsgCancelStartSession WCP_content;
      MultiPromos.AccountBalance _cancelled_balance;

      _cancelled_balance = MultiPromos.AccountBalance.Zero;

      _cancelled_balance.Redeemable = CancelledRedeemableCents * 0.01m;
      _cancelled_balance.PromoRedeemable = CancelledPromoRedeemableCents * 0.01m;
      _cancelled_balance.PromoNotRedeemable = CancelledPromoNotRedeemableCents * 0.01m;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgCancelStartSession);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgCancelStartSession)WCP_request.MsgContent;

      WCP_content.TransactionId = TransactionId;
      WCP_content.SetTrackData(0, TrackData);

      WCP_content.SetCancelledBalance(_cancelled_balance);      

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    }

    //
    // Report Event List
    //
    void WCP_IProtocol.NewRequest_ReportEventList(out WCP_IMessage Request,
                                                  out WCP_IReportEventList ReportEventList,
                                                  long SequenceId)
    {
      WCP_Message WCP_request;
      //WCP_MsgReportEventList WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportEventList);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      //WCP_content = (WCP_MsgReportEventList)WCP_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

      ReportEventList = WCP_ReportEventList.CreateEventList();
    }

    void WCP_IProtocol.NewRequest_EndSession(out WCP_IMessage Request,
                                             long SequenceId,
                                             long TransactionId,
                                             long CardSessionId,
                                             long Redeemable,
                                             long PromoRedeemable,
                                             long PromoNotRedeemable,
                                             bool HasCounters,
                                             long NumPlayed,
                                             long PlayedCents,
                                             long NumWon,
                                             long WonCents,
                                             long JackpotCents,
                                             long HandpaysAmountCents,
                                             bool OnlyAddBalances)
    {
      WCP_Message WCP_request;
      WCP_MsgEndSession WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgEndSession);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgEndSession)WCP_request.MsgContent;

      WCP_content.TransactionId = TransactionId;
      WCP_content.CardSessionId = CardSessionId;
      WCP_content.CardBalance = Redeemable + PromoRedeemable + PromoNotRedeemable;
      WCP_content.HasCounters = HasCounters;

      WCP_content.Balance.Redeemable = ((Decimal)Redeemable) / 100m;
      WCP_content.Balance.PromoRedeemable = ((Decimal)PromoRedeemable) / 100m;
      WCP_content.Balance.PromoNotRedeemable = ((Decimal)PromoNotRedeemable) / 100m;

      if (HasCounters)
      {
        WCP_content.PlayedCount = NumPlayed;
        WCP_content.PlayedCents = PlayedCents;
        WCP_content.WonCount = NumWon;
        WCP_content.WonCents = WonCents;
        WCP_content.JackpotCents = JackpotCents;
        WCP_content.HandpaysAmountCents = HandpaysAmountCents;
      }

      WCP_content.OnlyAddBalances = OnlyAddBalances;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    }

    void WCP_IProtocol.NewRequest_GetParameters(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message wcp_request;
      //WCP_MsgGetParameters wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetParameters);
      wcp_request.MsgHeader.SequenceId = SequenceId;
      //wcp_content = (WCP_MsgGetParameters)wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    }

    void WCP_IProtocol.NewRequest_GetCardType(out WCP_IMessage Request,
                                              long SequenceId,
                                              [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {

      WCP_Message WCP_request;
      WCP_MsgGetCardType WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetCardType);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgGetCardType)WCP_request.MsgContent;

      WCP_content.SetTrackData(0, TrackData);

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    }

    void WCP_IProtocol.NewRequest_MobileBankCardCheck(out WCP_IMessage Request,
                                                      long SequenceId,
                                                      [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String MobileBankCardTrackData,
                                                      [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin)
    {
      WCP_Message wcp_request;
      WCP_MsgMobileBankCardCheck wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMobileBankCardCheck);

      wcp_request.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgMobileBankCardCheck)wcp_request.MsgContent;

      wcp_content.MobileBankSetTrackData(0, MobileBankCardTrackData);
      wcp_content.Pin = Pin;

      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    }

    void WCP_IProtocol.NewRequest_MobileBankCardTransfer(out WCP_IMessage Request,
                                                         long SequenceId,
                                                         long TransactionId,
                                                         [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String MobileBankCardTrackData,
                                                         [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin,
                                                         [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String PlayerCardTrackData,
                                                         long AmountToTransferx100)
    {
      WCP_Message wcp_request;
      WCP_MsgMobileBankCardTransfer wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMobileBankCardTransfer);

      wcp_request.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgMobileBankCardTransfer)wcp_request.MsgContent;

      wcp_content.TransactionId = TransactionId;
      wcp_content.MobileBankSetTrackData(0, MobileBankCardTrackData);
      wcp_content.Pin = Pin;
      wcp_content.PlayerSetTrackData(0, PlayerCardTrackData);
      wcp_content.AmountToTransferx100 = AmountToTransferx100;

      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    }

    void WCP_IProtocol.NewReply_GetLoggerReply(out WCP_IMessage Reply,
                                               long SequenceId,
                                               [In, MarshalAs(UnmanagedType.BStr, SizeConst = (64 * 1024 - 1))] String LoggerMessages)
    {
      WCP_Message wcp_reply;
      WCP_MsgGetLoggerReply wcp_content;

      wcp_reply = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetLoggerReply);

      wcp_reply.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgGetLoggerReply)wcp_reply.MsgContent;

      wcp_content.LoggerMessages = LoggerMessages;

      Reply = WCP_MessageEnvelope.CreateEnvelope(wcp_reply);
    }

    void WCP_IProtocol.NewReply_ExecuteCommandReply(out WCP_IMessage Reply,
                                                    long SequenceId)
    {
      WCP_Message wcp_reply;

      wcp_reply = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommandReply);
      wcp_reply.MsgHeader.SequenceId = SequenceId;

      Reply = WCP_MessageEnvelope.CreateEnvelope(wcp_reply);
    }

    void WCP_IProtocol.NewRequest_AddCredit(out WCP_IMessage Request,
                                           long SequenceId,
                                           long TransactionId,
                                           long CardSessionId,
                                           long AmountToAdd,
                                           long AcceptedTime,
                                           long StackerId,
                                           [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String Currency,
                                           [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {

      WCP_Message WCP_request;
      WCP_MsgAddCredit WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgAddCredit);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgAddCredit)WCP_request.MsgContent;

      WCP_content.TransactionId = TransactionId;
      WCP_content.CardSessionId = CardSessionId;
      WCP_content.AmountToAdd = AmountToAdd;
      WCP_content.SetAcceptedTime(AcceptedTime);
      WCP_content.StackerId = StackerId;

      WCP_content.Currency = Currency;

      WCP_content.SetTrackData(0, TrackData);

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_AddCredit


    void WCP_IProtocol.NewRequest_MoneyEvent(out WCP_IMessage Request,
                               long SequenceId,
                               int LktMoneyEventId,
                               long LocalDatetime,
                               int Source,
                               int Target,
                               long Amount,
                               int Status,
                               long TransactionId,
                               long CardSessionId,
                               long StackerId,
                               [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {
      WCP_Message WCP_request;
      WCP_MsgMoneyEvent WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMoneyEvent);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgMoneyEvent)WCP_request.MsgContent;

      WCP_content.LktMoneyEventId = LktMoneyEventId;
      WCP_content.SetLocalDatetime(LocalDatetime);
      WCP_content.Source = (ENUM_MONEY_SOURCE)Source;
      WCP_content.Target = (ENUM_MONEY_TARGET)Target;
      WCP_content.Amount = Amount;
      WCP_content.Status = (ENUM_ACCEPTED_MONEY_STATUS)Status;
      WCP_content.TransactionId = TransactionId;
      WCP_content.CardSessionId = CardSessionId;
      WCP_content.StackerId = StackerId;

      WCP_content.SetTrackData(0, TrackData);

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_MoneyEvent



    void WCP_IProtocol.NewRequest_SubsMachinePartialCredit(out WCP_IMessage Request,
                                                           long SequenceId, 
                                                           long RequestAmount, 
                                                           long ObtainedAmount)
    {
      WCP_Message wcp_request;
      WCP_MsgSubsMachinePartialCredit wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgSubsMachinePartialCredit);
      wcp_request.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgSubsMachinePartialCredit)wcp_request.MsgContent;
      wcp_content.TransactionId = SequenceId;
      wcp_content.RequestAmount = RequestAmount;
      wcp_content.ObtainedAmount = ObtainedAmount;

      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    } // NewRequest_SubsMachinePartialCredit

    void WCP_IProtocol.NewRequest_GamegatewayProcesssMsg(out WCP_IMessage Request,
                                                         long SequenceId,
                                                         long CmdId)
    {
      WCP_Message wcp_request;
      WCP_MsgGameGatewayProcessMsg wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGameGatewayProcessMsg);
      wcp_request.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgGameGatewayProcessMsg)wcp_request.MsgContent;
      wcp_content.CmdId = CmdId;
      
      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    } // NewRequest_GamegatewayProcesssMsg

    void WCP_IProtocol.NewRequest_ExcmdGamegatewayGetCredit(out WCP_IMessage Request,
                                                            long SequenceId,
                                                            WCP_GameGateWayGetCredit GameGateWayGetCredit)
    {
      WCP_Message wcp_request;
      WCP_MsgGameGatewayGetCredit wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGameGatewayGetCredit);
      wcp_request.MsgHeader.SequenceId = SequenceId;

      wcp_content = (WCP_MsgGameGatewayGetCredit)wcp_request.MsgContent;

      wcp_content.AccountId = GameGateWayGetCredit.AccountId;
      wcp_content.RequestAmount = GameGateWayGetCredit.RequestAmount;
      wcp_content.Status = GameGateWayGetCredit.Status;
      wcp_content.MsgType = GameGateWayGetCredit.MessageType;
      wcp_content.TypeRequest = GameGateWayGetCredit.TypeRequest;
      wcp_content.CmdId = GameGateWayGetCredit.CmdId;
      wcp_content.MsgText = GameGateWayGetCredit.MessageText;

      Request = WCP_MessageEnvelope.CreateEnvelope(wcp_request);
    } // NewRequest_ExcmdGamegatewayGetCredit

    void WCP_IProtocol.NewRequest_ReserveTerminal (out WCP_IMessage Request,
                                                   long SequenceId,
                                                   [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {
      WCP_Message _wcp_request;
      WCP_MsgReserveTerminal _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReserveTerminal);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgReserveTerminal)_wcp_request.MsgContent;

      _wcp_content.TrackData = TrackData;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    } // NewRequest_ReserveTerminal

    void WCP_IProtocol.NewRequest_MobiBankRecharge (out WCP_IMessage Request,
                                                    long SequenceId, 
                                                    long AccountId)
    {
      WCP_Message _wcp_request;
      WCP_MsgMobiBankRecharge _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMobiBankRecharge);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgMobiBankRecharge)_wcp_request.MsgContent;

      _wcp_content.AccountId = AccountId;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    } // NewRequest_MobiBankRecharge 

    void WCP_IProtocol.NewRequest_MobiBankCancelRequest( out WCP_IMessage Request,
                                                         long SequenceId, 
                                                         long AccountId,
                                                         int CancelReason)
    {
      WCP_Message _wcp_request;
      WCP_MsgMobiBankCancelRequest _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMobiBankCancelRequest);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgMobiBankCancelRequest)_wcp_request.MsgContent;

      _wcp_content.AccountId = AccountId;
      _wcp_content.CancelReason = (MobileBankStatusCancelReason)CancelReason;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    } // NewRequest_MobiBankCancelRequest


    void WCP_IProtocol.NewRequest_MobiBankNotifyRechargeEGM(out WCP_IMessage Request,
                                                            long SequenceId,
                                                            long CmdId,
                                                            int Status,
                                                            long RequestId)
    {
      WCP_Message _wcp_request;
      WCP_MsgMobiBankNotifyRechargeEGM _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMobiBankNotifyRechargeEGM);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgMobiBankNotifyRechargeEGM)_wcp_request.MsgContent;

      _wcp_content.CmdId = CmdId;
      _wcp_content.Status =(MobileBankRechargeintoEGMStatus)Status;
      _wcp_content.RequestId = RequestId;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    } // NewRequest_MobiBankNotifyRechargeEGM 

  } // WCP_Protocol_Class
}
