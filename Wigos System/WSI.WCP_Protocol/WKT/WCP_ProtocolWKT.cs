//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProtocolWKT.cs
// 
//   DESCRIPTION: 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-AUG-2012 TJGXIT First release.
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;
using System.IO;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    #region Constants
    #endregion

    #region Enums
    #endregion

    #region Members
    private WaitableQueue m_download_request = new WaitableQueue();
    private Hashtable m_download_in_progress = new Hashtable();
    private ManualResetEvent m_ev_download_allowed = new ManualResetEvent(true);


    #endregion // Members

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Inits PromoBox specific threads and resources
    //              - Voucher printing
    //              - Resource dowloader
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    // 
    // RETURNS :
    // 
    //   NOTES :

    private void InitPromoBox()
    {
      Thread _thread;

      // Voucher Printing
      HtmlPrinter.Init();

      try
      {
        _thread = new Thread(new ThreadStart(ResourceDownloadManagerThread));
        _thread.Name = "WKT Resource Download Manager Thread";
        _thread.Start();
      }
      catch
      {
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Releases PromoBox specific threads and resources
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    // 
    // RETURNS :
    // 
    //   NOTES :

    private void StopPromoBox()
    {
      HtmlPrinter.Stop();
    }


    private Boolean IsBeingDownloaded(String ResourcePath)
    {
      lock (m_download_in_progress)
      {
        // Timeout previous downloads ...
        TimeoutDownloads();

        // Being downloaded ...
        return m_download_in_progress.ContainsKey(ResourcePath);
      }

    } // IsBeingDownloaded




    public void ClearDownloadsInProgress()
    {
      if (m_download_in_progress == null)
      {
        return;
      }

      lock (m_download_in_progress)
      {
        m_download_in_progress.Clear();
        UpdateDownloadAvailable();
      }

    } // ClearDownloadsInProgress


    private void DownloadCompleted(Int64 SequenceId)
    {
      lock (m_download_in_progress)
      {
        foreach (String _resource_path in m_download_in_progress.Keys)
        {
          DownloadRequest _item;

          _item = (DownloadRequest)m_download_in_progress[_resource_path];
          if (_item.SequenceId == SequenceId)
          {
            m_download_in_progress.Remove(_resource_path);

            UpdateDownloadAvailable();

            break;
          }
        }
      }
    } // DownloadCompleted

    internal class DownloadRequest
    {
      internal String ResourcePath = null;
      internal WCP_WKT.WKT_ResourceInfo ResourceInfo = null;

      internal int RequestCreated;
      internal int NumTries = 0;
      internal Int64 SequenceId = -1;
      internal int DownloadStarted = -1;

      internal DownloadRequest(WCP_WKT.WKT_ResourceInfo ResourceInfo)
      {
        this.RequestCreated = Environment.TickCount;
        this.ResourcePath = WCP_Resources.ResourcePath(ResourceInfo);
        this.ResourceInfo = ResourceInfo;
        this.NumTries = 0;
        this.SequenceId = 0;
        this.DownloadStarted = 0;
      }

    }

    private void SetBeingDownloaded(DownloadRequest DownloadItem, Int64 SequenceId)
    {
      if (SequenceId == 0 || DownloadItem == null)
      {
        return;
      }

      lock (m_download_in_progress)
      {
        DownloadItem.SequenceId = SequenceId;
        DownloadItem.DownloadStarted = Environment.TickCount;

        if (m_download_in_progress.ContainsKey(DownloadItem.ResourcePath))
        {
          m_download_in_progress[DownloadItem.ResourcePath] = DownloadItem;
        }
        else
        {
          m_download_in_progress.Add(DownloadItem.ResourcePath, DownloadItem);
        }
      }
    } // SetBeingDownloaded

    private void UpdateDownloadAvailable()
    {
      lock (m_download_in_progress)
      {
        if (m_download_in_progress.Count >= 5)
        {
          m_ev_download_allowed.Reset();
        }
        else
        {
          m_ev_download_allowed.Set();
        }
      }
    }

    private void WaitTillFewDownloadsInProgress()
    {
      UpdateDownloadAvailable();
      m_ev_download_allowed.WaitOne();

    } // WaitTillFewDownloadsInProgress

    private void TimeoutDownloads()
    {
      lock (m_download_in_progress)
      {
        foreach (String _resource_path in m_download_in_progress.Keys)
        {
          DownloadRequest _item;

          _item = (DownloadRequest)m_download_in_progress[_resource_path];

          if (Misc.GetElapsedTicks(_item.DownloadStarted) >= 60000)
          {
            // Timeout
            m_download_in_progress.Remove(_resource_path);
          }
        }

        UpdateDownloadAvailable();
      }

    } // TimeoutDownloads


    private void DeleteUnusedResources(Int64 ResourceId)
    {
      DirectoryInfo _folder;
      FileInfo[] _files;
      DateTime _now;
      TimeSpan _not_accessed;
      String _search_pattern;

      try
      {
        _now = DateTime.Now;
        _folder = new DirectoryInfo(WCP_Resources.ResourcesFolder);
        _search_pattern = WCP_Resources.ResourceName(ResourceId, null, null) + "*";
        _files = _folder.GetFiles(_search_pattern);

        foreach (FileInfo _file in _files)
        {
          _not_accessed = _now.Subtract(_file.LastAccessTime);

          if (_not_accessed.TotalDays >= 1 || ResourceId > 0)
          {
            try
            {
              File.Delete(_file.FullName);
            }
            catch
            { }
          }
        }
      }
      catch
      { }
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Resource download REQUEST thread
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    // 
    // RETURNS :
    // 
    //   NOTES :

    private void ResourceDownloadManagerThread()
    {
      WCP_IMessage _i_msg;
      WCP_IProtocol _i_protocol;
      Int64 _sequence_id;
      String _resource_path;
      int _tick_delete;
      DownloadRequest _request;
      WCP_WKT.WKT_ResourceInfo _resource;

      _i_protocol = new WCP_Protocol_Class();


      //
      // Delete resources not being used for "long time"
      //
      DeleteUnusedResources(0);
      _tick_delete = Environment.TickCount;

      while (true)
      {
        try
        {
          if (Misc.GetElapsedTicks(_tick_delete) > 60 * 60000) // 60 minutes
          {
            DeleteUnusedResources(0);
            _tick_delete = Environment.TickCount;
          }

          if (!WaitUntilConnected(60000))
          {
            continue;
          }

          _request = (DownloadRequest)m_download_request.Dequeue();
          if (_request.NumTries > 3 || Misc.GetElapsedTicks(_request.RequestCreated) >= 2 * 60000)
          {
            continue;
          }

          _request.NumTries++;
          _resource = _request.ResourceInfo;
          _resource_path = _request.ResourcePath;

          // Resource exists?
          if (File.Exists(_resource_path))
          {
            continue;
          }

          // Is It Being Downloaded?
          if (IsBeingDownloaded(_resource_path))
          {
            continue;
          }

          // Delete previous files: same resource_id but different hash
          DeleteUnusedResources(_resource.Resource_id);

          // Wait Till Few Downloads In Progress
          WaitTillFewDownloadsInProgress();

          GetSequenceId(out _sequence_id);
          _i_protocol.NewRequest_GetResources(out _i_msg, _sequence_id);
          WCP_Resources.SetResource(_i_msg, _resource);

          if (!WaitUntilConnected(0))
          {
            m_download_request.Enqueue(_request);

            continue;
          }

          SetBeingDownloaded(_request, _sequence_id);

          Send(_i_msg);
        }
        catch
        {
          Thread.Sleep(1000);
        }
      } // while (true)
    }

    //------------------------------------------------------------------------------
    // PURPOSE : WCP is in charge of requesting all resources related to those 
    //           PromoBox messages (replies) that include references to resources.
    //           In those cases WCP_WKT_MsgGetResource and WCP_WKT_MsgGetResourceReply 
    //           are completely managed in the WCP (sending and receiving) and never 
    //           reach the Terminal (Kiosk).
    //
    //           WCP will also manage the voucher printing for those PromoBox messages (replies) 
    //           that generate a print voucher.
    // 
    //  PARAMS :
    //      - INPUT :
    //          - WCP_Response : Message to process
    //
    //      - OUTPUT :
    // 
    // RETURNS : TRUE, queue the response to Terminal (Kiosk)
    //           FALSE, don't queue the response to Terminal (Kiosk)
    // 
    //   NOTES :

    private Boolean ProcessPromoBoxMessageReply(WCP_Message WCP_Response)
    {
      if (WCP_Response.MsgHeader.MsgType == WCP_MsgTypes.WCP_WKT_MsgGetResourceReply)
      {
        ProcessGetResourceReply(WCP_Response);

        return false;
      }

      if (WCP_Response.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_OK
        && WCP_Response.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        return true;
      }

      switch (WCP_Response.MsgHeader.MsgType)
      {
        // Print Voucher
        //case WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfoReply :       //TODO: UnComment when Msg implements IVouchers
        //case WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromosReply:      //TODO: UnComment when Msg implements IVouchers
        //case WCP_MsgTypes.WCP_WKT_MsgGetPlayerActivityReply:    //TODO: UnComment when Msg implements IVouchers
        case WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGiftReply:
        case WCP_MsgTypes.WCP_MsgAddCreditReply:
        case WCP_MsgTypes.WCP_WKT_MsgPrintRechargesListReply:
        case WCP_MsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbersReply:
        case WCP_MsgTypes.WCP_WKT_MsgPlayerRequestPromoReply:
        {
          WCP_WKT.IVouchers   _reply;
          Int32               _idx;

          _reply = (WCP_WKT.IVouchers)WCP_Response.MsgContent;
          for (_idx = 0; _idx < _reply.VoucherList.Count; _idx++)
          {
            HtmlPrinter.AddHtml ((String)_reply.VoucherList[_idx]);
          }
        }
        break;

        // Messages that generate a resource download request
        case WCP_MsgTypes.WCP_WKT_MsgGetPlayerGiftsReply:
        case WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromosReply:
        case WCP_MsgTypes.WCP_WKT_MsgGetNextAdvStepReply:
        case WCP_MsgTypes.WCP_WKT_MsgGetParametersReply:
          RequestDownloadResources ((WCP_WKT.IResources) WCP_Response.MsgContent);
        break;

        case WCP_MsgTypes.WCP_LCD_MsgGetParametersReply:
          RequestDownloadResources((WCP_WKT.IResources)WCP_Response.MsgContent);
        break;


        default :
        break;
      }

      return true;
    }

    private void ProcessGetResourceReply(WCP_Message WCP_Response)
    {
      WCP_WKT_MsgGetResourceReply _resource;
      String _resource_path;

      if (WCP_Response == null)
      {
        return;
      }

      try
      {
        if (WCP_Response.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
        {
          return;
        }

        _resource = (WCP_WKT_MsgGetResourceReply)WCP_Response.MsgContent;
        if (_resource.ChunkDataLength != _resource.ChunkData.Length)
        {
          return;
        }

        _resource_path = WCP_Resources.ResourcePath(_resource.WktResourceId, _resource.Hash, _resource.Extension);

        // AJQ 05-SEP-2012, Save the resource data to a file
        using (FileStream _fs = new FileStream(_resource_path, FileMode.Create))
        {
          using (BinaryWriter _writer = new BinaryWriter(_fs))
          {
            _writer.Write(_resource.ChunkData, 0, _resource.ChunkDataLength);
            _writer.Close();
          }
        }

      }
      catch
      {
      }
      finally
      {
        DownloadCompleted(WCP_Response.MsgHeader.SequenceId);
      }
    }

    private void RequestDownloadResources(WCP_WKT.IResources MsgContent)
    {
      foreach (WCP_WKT.WKT_ResourceInfo _resource in MsgContent.Resources)
      {
        m_download_request.Enqueue(new DownloadRequest(_resource));
      }
    }

    #endregion

    #region Public Methods
    #endregion
  }
}
