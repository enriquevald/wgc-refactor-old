//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PrintRechargesList.cs
// 
//   DESCRIPTION: Manage Player Recharges List printing information
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-OCT-2012 JMM     First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;

namespace WSI.WCP.WCP_Protocol
{

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_PrintRechargesList(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgPrintRechargesList _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgPrintRechargesList);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgPrintRechargesList)_wcp_request.MsgContent;

      _wcp_content.NotesList = new List<double>();
      _wcp_content.NotesList.Clear();

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

  } // WCP_Protocol_Class


  public partial class WCP_PrintRechargesList_Class : WCP_IPrintRechargesList
  {

    #region WCP_IPrintRechargesList Members
    //------------------------------------------------------------------------------
    // PURPOSE: Set the fields into the xml msg to send.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Message           : WCP_IMessage Request
    //          - Note Face Value   : double NoteValue
    //
    //      - OUTPUT:
    //          
    // RETURNS:
    //     
    //
    public void PrintRechargesListAddNote(WCP_IMessage Request, double NoteValue)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgPrintRechargesList _wcp_content;
      WCP_MessageEnvelope _wcp_envelope;

      _wcp_envelope = (WCP_MessageEnvelope)Request;
      _wcp_request = _wcp_envelope.Message;
      _wcp_content = (WCP_WKT_MsgPrintRechargesList)_wcp_request.MsgContent;

      _wcp_content.NotesList.Add(NoteValue);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set the fields into the xml msg to send.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Message           : WCP_IMessage Request
    //          - Note Face Value   : double NoteValue
    //
    //      - OUTPUT:
    //          
    // RETURNS:
    //     
    //
    public void PrintRechargesListSetTotal(WCP_IMessage Request, double TotalRechargesValue)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgPrintRechargesList _wcp_content;
      WCP_MessageEnvelope _wcp_envelope;

      _wcp_envelope = (WCP_MessageEnvelope)Request;
      _wcp_request = _wcp_envelope.Message;
      _wcp_content = (WCP_WKT_MsgPrintRechargesList)_wcp_request.MsgContent;

      _wcp_content.TotalRecharges = TotalRechargesValue;
    }

    #endregion
  }// WCP_PrintRechargesList class

}

