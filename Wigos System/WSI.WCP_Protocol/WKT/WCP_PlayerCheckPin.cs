//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerCheckPin.cs
// 
//   DESCRIPTION: Manage Player Information to send with other messages
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 04-JUL-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-JUL-2014 JMM    First version.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_PlayerCheckPin(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_MsgPlayerCheckPin _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgPlayerCheckPin);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_MsgPlayerCheckPin)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

  } // WCP_Protocol_Class
}
