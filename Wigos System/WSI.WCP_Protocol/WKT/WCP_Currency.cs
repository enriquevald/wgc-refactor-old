//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Currency_Class.cs
// 
//   DESCRIPTION: Manage GetParameters Message
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 29-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUN-2012 XIT    First version.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Currency_Class : WCP_ICurrency
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Obtain the names of the currency specified by the index
    // 
    //  PARAMS:
    //      - INPUT:
    //        - NumCurrency
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - IsoCode
    //        - Alias1
    //        - Alias2
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ICurrency.GetCurrencyName(WCP_IMessage ReceivedMessage, Int32 NumCurrency
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String IsoCode
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String Alias1
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String Alias2)
    {

      WCP_MsgGetParametersReply _reply;
      _reply = ProvideParametersReply(ReceivedMessage);

      IsoCode = _reply.NoteAcceptorParameters.AcceptedCurrencies[NumCurrency].IsoCode;
      Alias1 =  _reply.NoteAcceptorParameters.AcceptedCurrencies[NumCurrency].Alias_1;
      Alias2 =  _reply.NoteAcceptorParameters.AcceptedCurrencies[NumCurrency].Alias_2;
    }//GetCurrencyName


    //------------------------------------------------------------------------------
    // PURPOSE: Get the number of rejected values for the currency specified as NumCurrency
    // 
    //  PARAMS:
    //      - INPUT:
    //        - NumCurrency
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumRejected
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ICurrency.GetNumberRejected( WCP_IMessage ReceivedMessage, Int32 NumCurrency, out Int32 NumRejected)
    {
      WCP_MsgGetParametersReply _reply;
      _reply = ProvideParametersReply(ReceivedMessage);
      
      NumRejected = _reply.NoteAcceptorParameters.AcceptedCurrencies[NumCurrency].RejectedValues.Count;
    
    } //GetNumberRejected

    //------------------------------------------------------------------------------
    // PURPOSE: Get the value rejected specified by the currency index and rejected index
    // 
    //  PARAMS:
    //      - INPUT:
    //        - NumCurrency
    //        - NumRejectedValue
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - Value
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ICurrency.GetCurrencyValue( WCP_IMessage ReceivedMessage, Int32 IdxCurrency, Int32 NumRejectedValue, out Int32 Value)
    {
      WCP_MsgGetParametersReply _reply;
      _reply = ProvideParametersReply(ReceivedMessage);

      Value = _reply.NoteAcceptorParameters.AcceptedCurrencies[IdxCurrency].RejectedValues[NumRejectedValue];
    } //GetCurrencyValue


    //------------------------------------------------------------------------------
    // PURPOSE: Parses WCP_IMessage & returns WCP_MsgGetParametersReply
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //
    //      - OUTPUT:
    //        - WCP_MsgGetParametersReply
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    private WCP_MsgGetParametersReply ProvideParametersReply(WCP_IMessage ReceivedMessage)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgGetParametersReply _reply;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _reply = (WCP_MsgGetParametersReply)_msg.Message.MsgContent;

      return _reply;
    }//ProvideParametersReply


  }//WCP_Currency_Class

}
