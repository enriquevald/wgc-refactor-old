//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerPromos_Class.cs
// 
//   DESCRIPTION: Manage Promo Params information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 04-SEP-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-SEP-2012 XIT     First version.
// 17-DEC-2012 JMM     Gifts split info added.
// 27-NOV-2015 SGB    Backlog Item 6050: Insert general params struct & functions for promotions ticket TITO
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_WKT_Image
  {
    public Int64 ImageId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String ResourcePath;
  }

  public struct WCP_WKT_Message
  {
    public Int64 MsgId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String MsgText;
  }

  public struct WCP_WKT_General_Params
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 1024)]
    public String Key;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 1024)]
    public String Value;
  }

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPromoParams(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetParameters _wcp_content;
      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetParameters);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetParameters)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

  } // WCP_Protocol_Class

  public partial class WCP_PromoParams_Class : WCP_IPromoParams
  {
    void WCP_IPromoParams.GetNumFunctionalities(WCP_IMessage ReceivedMessage, out Int32 NumFunc)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      NumFunc = _message_content.Functionalities.Count;
    }

    void WCP_IPromoParams.GetFunctionality(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 Func)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      Func = _message_content.Functionalities[Idx];
    }

    void WCP_IPromoParams.GetNumImages(WCP_IMessage ReceivedMessage, out Int32 NumPromoImages)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      NumPromoImages = _message_content.Images.Count;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Promo Image Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 Idx
    //
    //      - OUTPUT:
    //        - WCP_PromoParamImage PromoImage
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPromoParams.GetImage(WCP_IMessage ReceivedMessage, int IndexImage, out WCP_WKT_Image Image)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      Image = new WCP_WKT_Image();

      if (IndexImage < 0 || IndexImage >= _message_content.Images.Count)
      {
        return;
      }

      Image.ImageId = _message_content.Images[IndexImage].ImageId;
      Image.ResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _message_content.Images[IndexImage].ResourceId));
    }

    public void GetNumCustomMessages(WCP_IMessage ReceivedMessage, out int NumMessages)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      NumMessages = _message_content.Messages.Count;
    }

    public void GetCustomMessage(WCP_IMessage ReceivedMessage, int IndexMessage, out WCP_WKT_Message Message)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      Message = new WCP_WKT_Message();

      if (IndexMessage < 0 || IndexMessage >= _message_content.Messages.Count)
      {
        return;
      }      

      Message.MsgId = _message_content.Messages[IndexMessage].Msg_id;
      Message.MsgText = _message_content.Messages[IndexMessage].Msg_Text;
    }

    public void GetScheduleTime(WCP_IMessage ReceivedMessage, out Int32 FromTime, out Int32 ToTime)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      FromTime = _message_content.ScheduleTime.From_Time;
      ToTime = _message_content.ScheduleTime.To_Time;
    }

    public void GetGiftsSplit(WCP_IMessage ReceivedMessage, 
                              out Int32 Enabled, 
                              out String CategoryName01, 
                              out String CategoryName02, 
                              out String CategoryName03,
                              out String CategoryShort01,
                              out String CategoryShort02,
                              out String CategoryShort03)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetParametersReply)_msg.Message.MsgContent;

      Enabled = _message_content.GiftsSplit.Enabled ? 1 : 0;
      CategoryName01  = _message_content.GiftsSplit.CategoryName01;
      CategoryName02  = _message_content.GiftsSplit.CategoryName02;
      CategoryName03  = _message_content.GiftsSplit.CategoryName03;
      CategoryShort01 = _message_content.GiftsSplit.CategoryShort01;
      CategoryShort02 = _message_content.GiftsSplit.CategoryShort02;
      CategoryShort03 = _message_content.GiftsSplit.CategoryShort03;
    }
    
    #region WCP_IPromoParams Members

    #endregion
  } // WCP_PromoParams_Class
}