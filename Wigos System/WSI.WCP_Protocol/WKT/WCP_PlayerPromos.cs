//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerPromos_Class.cs
// 
//   DESCRIPTION: Manage Player Promos information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 01-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2012 XIT     First version.
// 09-DEC-2014 JMM     Added by recharges promotions to the promotion list sent to the WKT
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_PlayerPromo
  {
    public Int64 Id;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String Name;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String Description;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String LargeResourcePath;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String SmallResourcePath;
    public Int64 PromoCreditType;
    public Int64 MinSpent;
    public Int64 MinSpentReward;
    public Int64 Spent;
    public Int64 SpentReward;
    public Int64 CurrentReward;
    public Int64 RechargesReward;
    public Int64 AvailableLimit;
  }

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPlayerPromos(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetPlayerPromos _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromos);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetPlayerPromos)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }

  } // WCP_Protocol_Class

  public partial class WCP_PlayerPromos_Class : WCP_IPlayerPromos
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player Promo Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 PromoIdx
    //        - Int32 ListId
    //
    //      - OUTPUT:
    //        - WCP_PlayerPromo PromoValue
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerPromos.GetPlayerPromo(WCP_IMessage ReceivedMessage
                                        , Int32 PromoIdx, Int32 ListId
                                        , out WCP_PlayerPromo PromoValue)
    {
      WCP_WKT_MsgGetPlayerPromosReply _message_content;

      _message_content = ProvidePlayerPromos(ReceivedMessage);
      WCP_WKT.WKT_Promo _promo = _message_content.Promos[ListId][PromoIdx];

      PromoValue = new WCP_PlayerPromo();
      PromoValue.Id = _promo.Id;
      PromoValue.Name = _promo.Name;
      PromoValue.LargeResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _promo.LargeResourceId));
      PromoValue.SmallResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _promo.SmallResourceId));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Number promotions of the specified list
    //
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage, ( really WCP_Message_Envelope )
    //        - Idx
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           NumPromos
    //                            
    // 
    void WCP_IPlayerPromos.GetNumberPromos(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 NumPromos)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerPromosReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerPromosReply)_msg.Message.MsgContent;

      NumPromos = _message_content.Promos[Idx].Count;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player Applicable Promo Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 PromoIdx    
    //
    //      - OUTPUT:
    //        - WCP_PlayerPromo PromoValue
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerPromos.GetPlayerApplicablePromo(WCP_IMessage ReceivedMessage
                                                  , Int32 PromoIdx
                                                  , out WCP_PlayerPromo PromoValue)
    {
      WCP_WKT_MsgGetPlayerPromosReply _message_content;

      _message_content = ProvidePlayerPromos(ReceivedMessage);
      WCP_WKT.WKT_ApplicablePromo _promo = _message_content.ApplicablePromos[PromoIdx];

      PromoValue = new WCP_PlayerPromo();
      PromoValue.Id = _promo.Id;
      PromoValue.Name = _promo.Name;
      PromoValue.Description = _promo.Description;
      PromoValue.LargeResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _promo.LargeResourceId));
      PromoValue.SmallResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, _promo.SmallResourceId));
      PromoValue.PromoCreditType = (Int64)_promo.PromoCreditType;
      PromoValue.MinSpent = (Int64)((Decimal)_promo.MinSpent);
      PromoValue.MinSpentReward = (Int64)((Decimal)_promo.MinSpentReward);
      PromoValue.Spent = (Int64)((Decimal)_promo.Spent);
      PromoValue.SpentReward = (Int64)((Decimal)_promo.SpentReward);
      PromoValue.CurrentReward = (Int64)((Decimal)_promo.CurrentReward);
      PromoValue.RechargesReward = (Int64)((Decimal)_promo.RechargesReward);
      PromoValue.AvailableLimit = (Int64)((Decimal)_promo.AvailableLimit);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Number applicable promotions
    //
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage, ( really WCP_Message_Envelope )    
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           NumPromos
    //                            
    // 
    void WCP_IPlayerPromos.GetNumberApplicablePromos(WCP_IMessage ReceivedMessage, out Int32 NumPromos)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerPromosReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerPromosReply)_msg.Message.MsgContent;

      NumPromos = _message_content.ApplicablePromos.Count;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Provide IPlayerPromos from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           IPlayerPromos
    //                            
    // 
    public static WCP_WKT_MsgGetPlayerPromosReply ProvidePlayerPromos(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerPromosReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerPromosReply)_msg.Message.MsgContent;

      return _message_content;
    }

  } // WCP_PlayerPromos_Class
}