//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerInfo_Class.cs
// 
//   DESCRIPTION: Manage Player Information to send with other messages
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 29-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUN-2012 XIT    First version.
// 23-NOV-2012 JMM    Level id & next level info added.
// 14-AUG-2013 JMM    Fixed Bug WIG-113: PromoBOX fails on note recharges when anti-moneylaundry activated
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPlayerInfo(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetPlayerInfo _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfo);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetPlayerInfo)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }



  } // WCP_Protocol_Class

  public partial class WCP_PlayerInfo_Class : WCP_IPlayerInfo
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Gets the number of promotions of a player
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumPromotions
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerInfo.GetNumberPromotions(WCP_IMessage ReceivedMessage, out Int32 NumPromotions)
    {
      WCP_WKT.WKT_PlayerInfo _player_info;
      _player_info = ProvidePlayerInfo(ReceivedMessage);

      NumPromotions = _player_info.BalanceParts.ActivePromotions.Count;
    }//GetNumberPromotions


    //------------------------------------------------------------------------------
    // PURPOSE: Gets the specific information of a Promotion associated with a Player
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumPromotions
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerInfo.GetPromotionInfo(WCP_IMessage ReceivedMessage, Int32 IdxPromotion, out WCP_PlayerPromotion Promotion)
    {
      WCP_WKT.WKT_PlayerInfo _player_info;
      _player_info = ProvidePlayerInfo(ReceivedMessage);

      Promotion.AwardedBalance =  (ulong)_player_info.BalanceParts.ActivePromotions[IdxPromotion].AwardedBalance;
      Promotion.CurrentBalance =  (ulong)_player_info.BalanceParts.ActivePromotions[IdxPromotion].CurrentBalance;
      Promotion.PromoDate =       _player_info.BalanceParts.ActivePromotions[IdxPromotion].PromoDate.ToFileTime();
      Promotion.PromoName =       _player_info.BalanceParts.ActivePromotions[IdxPromotion].PromoName;

    } //GetPromotionInfo



    //------------------------------------------------------------------------------
    // PURPOSE: Gets the basic information of a player
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - BasicPlayerInfo
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerInfo.GetBasicPlayerInfo(WCP_IMessage ReceivedMessage, out WCP_BasicPlayerInfoParameters BasicPlayerInfo)
    {
      WCP_WKT.WKT_PlayerInfo  _player_info;

      _player_info = ProvidePlayerInfo(ReceivedMessage);      

      BasicPlayerInfo = new WCP_BasicPlayerInfoParameters();

      BasicPlayerInfo.Birthday =            _player_info.Birthday.ToFileTime();
      BasicPlayerInfo.EnterDate =           _player_info.Level.EnterDate.ToFileTime();
      if (_player_info.Level.Expiration != null)
      {
        BasicPlayerInfo.Expiration = ((DateTime)_player_info.Level.Expiration).ToFileTime();
      }
      else
      {
        BasicPlayerInfo.Expiration = DateTime.MaxValue.ToFileTime();
      }

      BasicPlayerInfo.FullName                = _player_info.FullName;
      BasicPlayerInfo.Gender                  = (DrawGender)_player_info.Gender;
      BasicPlayerInfo.InSession               = _player_info.InSession;
      BasicPlayerInfo.LevelId                 = _player_info.Level.LevelId;
      BasicPlayerInfo.LevelName               = _player_info.Level.LevelName;
      BasicPlayerInfo.NextLevelId             = _player_info.NextLevel.LevelId;
      BasicPlayerInfo.NextLevelName           = _player_info.NextLevel.LevelName;     
      BasicPlayerInfo.PointsToNextLevel       = (ulong)_player_info.NextLevel.PointsToReach;
      BasicPlayerInfo.Money2Balance           = (ulong)_player_info.Money2Balance;
      BasicPlayerInfo.MoneyBalance            = (ulong)_player_info.MoneyBalance;
      BasicPlayerInfo.NonRedeemableBalance    = (ulong)_player_info.BalanceParts.NonRedeemableBalance;
      BasicPlayerInfo.Points2Balance          = (ulong)_player_info.Points2Balance;
      BasicPlayerInfo.PointsBalance           = (ulong)_player_info.PointsBalance;
      BasicPlayerInfo.RedeemableBalance       = (ulong)_player_info.BalanceParts.RedeemableBalance;
      BasicPlayerInfo.TerminalName            = _player_info.InSessionTerminalName;
      BasicPlayerInfo.NoteAcceptorEnabled     = _player_info.NoteAcceptorEnabled;
      BasicPlayerInfo.CurrentSpent            = (ulong)_player_info.CurrentSpent;
      BasicPlayerInfo.NextLevelPointsGenerated = (ulong)_player_info.NextLevel.PointsGenerated;
      BasicPlayerInfo.NextLevelPointsDisc     = (ulong)_player_info.NextLevel.PointsDiscretionaries;

    } //GetBasicPlayerInfo


    //------------------------------------------------------------------------------
    // PURPOSE: Parses WCP_IMessage and returns IExtendedPlayerInfo
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:      
    //         
    // RETURNS: BasicPlayerInfo
    // 
    //   NOTES:
    //
    private WCP_WKT.WKT_PlayerInfo ProvidePlayerInfo(WCP_IMessage ReceivedMessage)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT.WKT_PlayerInfo _player_info;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _player_info = ((WCP_WKT.IExtendedPlayerInfo)_msg.Message.MsgContent).PlayerInfo;
      return _player_info;
    }

  }//WCP_PlayerInfo_Class

}
