//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Resources.cs
// 
//   DESCRIPTION: Manage Resources 
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 31-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JUL-2012 XIT    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace WSI.WCP.WCP_Protocol
{

  ////////[StructLayout(LayoutKind.Sequential)]
  ////////public struct WCP_ResourceInfo
  ////////{
  ////////  public Int64 Id;
  ////////  [MarshalAs(UnmanagedType.BStr, SizeConst = 5)]
  ////////  public String Extension;
  ////////  [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
  ////////  public String Hash;
  ////////  public Int64 Length;
  ////////}


  public partial class WCP_Resources
  {
    public static String ResourcesFolder
    {
      get
      {
        String _path;

        _path = Path.Combine(Directory.GetCurrentDirectory(), "Temp");
        _path = Path.Combine(_path, "PromoBOX");
        _path = Path.Combine(_path, "Resources");

        if (!Directory.Exists(_path))
        {
          try
          {
            Directory.CreateDirectory(_path);
          }
          catch { }
        }

        return _path;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Gets the Path of the specified resource
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Info
    //
    //      - OUTPUT:
    //        - Path
    //         
    // RETURNS:
    // 
    //   NOTES:

    ////////public static void GetResourcePath(WCP_ResourceInfo Info, out String ResourcePath)
    ////////{
    ////////  if (Info.Id == 0)
    ////////  {
    ////////    ResourcePath = String.Empty;
    ////////  }
    ////////  else
    ////////  {
    ////////    ResourcePath = ResourcePath (Info.Id, Info.Hash, Info.Extension);
    ////////  }
    ////////}

    ////////public static void GetResourcePath(WCP_WKT.WKT_ResourceInfo Info, out String Path)
    ////////{
    ////////  WCP_ResourceInfo _wcp_resource;
    ////////  _wcp_resource = WCP_Resources.Get_WCP_ResourceInfo(Info);

    ////////  GetResourcePath(_wcp_resource, out Path);
    ////////}


    public static String ResourcePath(WCP_WKT.WKT_ResourceInfo Resource)
    {
      return ResourcePath(Resource.Resource_id, Resource.Hash, Resource.Extension);
    }

    public static String ResourcePath(Int64 ResourceId, Byte[] Hash, String Extension)
    {
      if (ResourceId > 0)
      {
        return Path.Combine(ResourcesFolder, ResourceName(ResourceId, Hash, Extension));
      }
      else
      {
        return string.Empty;
      }
    }

    public static String ResourceName(Int64 ResourceId, Byte[] Hash, String Extension)
    {
      Byte[] _byte;
      String _name;
      String _hexa;

      _name = "WKT_RESOURCE_";

      // Concat the ID
      if (ResourceId != 0)
      {
        _byte = BitConverter.GetBytes(ResourceId);
        _hexa = BitConverter.ToString(_byte);
        _hexa = _hexa.Replace("-", "");
        _name = _name + _hexa + "_";
      }

      if (Hash != null)
      {
        // Concat the Hash
        _hexa = BitConverter.ToString(Hash);
        _hexa = _hexa.Replace("-", "");
        _name = _name + _hexa;

        if (!String.IsNullOrEmpty(Extension))
        {
          if (!Extension.StartsWith("."))
          {
            Extension = "." + Extension;
          }

          _name = _name + Extension;
        }
      }

      return _name;
    }




    //------------------------------------------------------------------------------
    // PURPOSE: Gets the list of Resources in C# object
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_WKT.IResources
    //
    //      - OUTPUT:
    //        - WCP_WKT.WKT_ResourceInfoList
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    public static WCP_WKT.WKT_ResourceInfoList GetResources(WCP_WKT.IResources ReplyMessage)
    {
      return ReplyMessage.Resources;
    }


    //////////////------------------------------------------------------------------------------
    ////////////// PURPOSE: Returns the equivalent resourceinfo object in C# version
    ////////////// 
    //////////////  PARAMS:
    //////////////      - INPUT:
    //////////////        - WCP_ResourceInfo
    //////////////
    //////////////      - OUTPUT:
    //////////////        - 
    //////////////         
    ////////////// RETURNS: WCP_WKT.WKT_ResourceInfo
    ////////////// 
    //////////////   NOTES:
    //////////////
    ////////////public static WCP_WKT.WKT_ResourceInfo Get_WKT_ResourceInfo(WCP_ResourceInfo WcpResource)
    ////////////{
    ////////////  WCP_WKT.WKT_ResourceInfo _wkt_resource;
    ////////////  _wkt_resource = new WCP_WKT.WKT_ResourceInfo();

    ////////////  _wkt_resource.Resource_id = WcpResource.Id;
    ////////////  _wkt_resource.Extension = WcpResource.Extension;
    ////////////  _wkt_resource.Length = (Int32)WcpResource.Length;
    ////////////  UTF8Encoding _encoding = new System.Text.UTF8Encoding();
    ////////////  _wkt_resource.Hash = _encoding.GetBytes(WcpResource.Hash);

    ////////////  return _wkt_resource;
    ////////////}

    ////////////------------------------------------------------------------------------------
    //////////// PURPOSE: Returns the equivalent resourceinfo object in C version
    //////////// 
    ////////////  PARAMS:
    ////////////      - INPUT:
    ////////////        - WCP_WKT.WKT_ResourceInfo
    ////////////
    ////////////      - OUTPUT:
    ////////////        - 
    ////////////         
    //////////// RETURNS: WCP_ResourceInfo
    //////////// 
    ////////////   NOTES:
    ////////////
    //////////public static WCP_ResourceInfo Get_WCP_ResourceInfo(WCP_WKT.WKT_ResourceInfo WktResource)
    //////////{
    //////////  WCP_ResourceInfo _wcp_resource;
    //////////  _wcp_resource = new WCP_ResourceInfo();

    //////////  _wcp_resource.Id = WktResource.Resource_id;
    //////////  _wcp_resource.Length = WktResource.Length;
    //////////  _wcp_resource.Extension = WktResource.Extension;
    //////////  System.Text.UTF8Encoding _enc = new System.Text.UTF8Encoding();
    //////////  _wcp_resource.Hash = Convert.ToBase64String(WktResource.Hash);

    //////////  return _wcp_resource;
    //////////}


    //------------------------------------------------------------------------------
    // PURPOSE: Gets the Info of a Resource
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Info
    //
    //      - OUTPUT:
    //        - Path
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    public static WCP_WKT.WKT_ResourceInfo GetResource(WCP_WKT.IResources ReplyMessage, Int64 ResourceId)
    {
      if (ResourceId > 0 && ReplyMessage.Resources.Count > 0)
      {
        return ReplyMessage.Resources.Find(new WCP_WKT.FindResourceInfo(ResourceId).FindById);
      }

      return new WCP_WKT.WKT_ResourceInfo();
    }

    ////////////------------------------------------------------------------------------------
    //////////// PURPOSE: Converts a string to hexa
    //////////// 
    ////////////  PARAMS:
    ////////////      - INPUT:
    ////////////        - String AsciiString
    ////////////
    ////////////      - OUTPUT:
    ////////////        - 
    ////////////         
    //////////// RETURNS: String Hexa values 
    //////////// 
    ////////////   NOTES:

    //////////public static String ConvertStringToHex(String AsciiString)
    //////////{
    //////////  String _hex;

    //////////  _hex = String.Empty;
    //////////  foreach (char c in AsciiString)
    //////////  {
    //////////    int _tmp = c;
    //////////    _hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(_tmp.ToString()));
    //////////  }
    //////////  return _hex;
    //////////}

    //------------------------------------------------------------------------------
    // PURPOSE: Converts a hexa value to string
    // 
    //  PARAMS:
    //      - INPUT:
    //        - String HexValue
    //
    //      - OUTPUT:
    //         
    // RETURNS: String values
    //   NOTES:

    public static String ConvertHexToString(String HexValue)
    {
      String _str_value;

      _str_value = "";
      while (HexValue.Length > 0)
      {
        _str_value += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
        HexValue = HexValue.Substring(2, HexValue.Length - 2);
      }
      return _str_value;
    }

    public static void SetResource(WCP_IMessage Request,
                                    WCP_WKT.WKT_ResourceInfo Data)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetResource _wcp_content;
      WCP_MessageEnvelope _wcp_envelope;

      _wcp_envelope = (WCP_MessageEnvelope)Request;
      _wcp_request = _wcp_envelope.Message;
      _wcp_content = (WCP_WKT_MsgGetResource)_wcp_request.MsgContent;

      _wcp_content.WktResourceId = Data.Resource_id;
      _wcp_content.ChunkDataLength = Data.Length;
      _wcp_content.Offset = 0;

    } // SetResource


    public static WCP_WKT_MsgGetResourceReply ProvideResource(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetResourceReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetResourceReply)_msg.Message.MsgContent;

      return _message_content;
    }

  } // WCP_Resources


  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetResources(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetResource _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetResource);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_WKT_MsgGetResource)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }


  }
}
