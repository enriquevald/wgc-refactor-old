//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerActivity_Class.cs
// 
//   DESCRIPTION: Retrieve Player Activity information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 14-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2012 XIT     First version.
// 11-JUL-2014 AMF     Regionalization
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPlayerActivity(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetPlayerActivity _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetPlayerActivity);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetPlayerActivity)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }

  } // WCP_Protocol_Class

  public partial class WCP_PlayerActivity_Class : WCP_IPlayerActivity
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player Activity Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 ActivityIdx
    //
    //      - OUTPUT:
    //        - String ActDescription
    //        - DateTime ActDate
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerActivity.GetPlayerActivity(WCP_IMessage ReceivedMessage
                                        , Int32 ActivityIdx
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String ActDescription
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 25)]  out String ActDate
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 25)]  out String ActQuantity)
    {
      WCP_WKT_MsgGetPlayerActivityReply _message_content;
      _message_content = ProvidePlayerActivity(ReceivedMessage);

      ActDescription = _message_content.Activities[ActivityIdx].Description;
      ActDate = WSI.Common.Format.CustomFormatDateTime(DateTime.Parse(_message_content.Activities[ActivityIdx].Date), true);
      ActQuantity = _message_content.Activities[ActivityIdx].Quantity;
    }



    //------------------------------------------------------------------------------
    // PURPOSE: Get Number Activites
    //
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //        - Int32 NumActivities
    //         
    // RETURNS:
    //                            
    // 
    void WCP_IPlayerActivity.GetNumberActivities(WCP_IMessage ReceivedMessage, out Int32 NumActivities)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerActivityReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerActivityReply)_msg.Message.MsgContent;

      NumActivities = _message_content.Activities.Count;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Provide IPlayerActivity from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           IPlayerActivity
    //                            
    // 
    public static WCP_WKT_MsgGetPlayerActivityReply ProvidePlayerActivity(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerActivityReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerActivityReply)_msg.Message.MsgContent;

      return _message_content;
    }

  } // WCP_PlayerActivity_Class
}