//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerGift_Class.cs
// 
//   DESCRIPTION: Manage Player Gifts information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 30-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUL-2012 XIT    First version.
// 30-AUG-2013 JMM    Fixed Bug WIG-167: PromoBOX error on credit gifts with decimals amount.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;

namespace WSI.WCP.WCP_Protocol
{

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_PlayerGift
  {
    public Int64 GiftId;
    public GiftType Type;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String Name;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String Description;
    public Int64 Points;
    public Int64 NumCredits;
    public Int32 Stock;
    public Int32 MaximumUnits;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String SmallResourcePath;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String LargeResourcePath;

  }

  public enum GiftType
  {
    OBJECT = 0
    ,
    NOT_REDEEMABLE_CREDIT = 1
    ,
    DRAW_NUMBERS = 2
    ,
    SERVICES = 3
    ,
    REDEEMABLE_CREDIT = 4
    ,
    UNKNOWN = 999
  }

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_GetPlayerGifts(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetPlayerGifts _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetPlayerGifts);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetPlayerGifts)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }

  } // WCP_Protocol_Class

  public partial class WCP_PlayerGifts_Class : WCP_IPlayerGifts
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player Gift Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 GiftIdx
    //        - Int32 TableId
    //
    //      - OUTPUT:
    //        - WCP_PlayerGift GiftValue
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerGifts.GetPlayerGift(WCP_IMessage ReceivedMessage
                                        , Int32 GiftIdx, Int32 TableId
                                        , out WCP_PlayerGift GiftValue)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerGiftsReply _message_content;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerGiftsReply)_msg.Message.MsgContent;

      GiftValue = new WCP_PlayerGift();

      DataRow _row_gift = _message_content.GetPlayerGift(GiftIdx, TableId);
      GiftValue.GiftId = Int64.Parse(_row_gift["GI_GIFT_ID"].ToString());
      GiftValue.Name = _row_gift["GI_NAME"].ToString();
      GiftValue.Description = _row_gift["GI_DESCRIPTION"].ToString();
      GiftValue.Points = Int64.Parse(_row_gift["GI_POINTS"].ToString())*100;
      GiftValue.Type = (GiftType) Int32.Parse(_row_gift["GI_TYPE"].ToString());
      GiftValue.Stock = Int32.Parse(_row_gift["GI_CURRENT_STOCK"].ToString());
      GiftValue.MaximumUnits = Int32.Parse(_row_gift["MAX_ALLOWED_UNITS"].ToString());
      GiftValue.NumCredits = (Int64)((Decimal)_row_gift["GI_NON_REDEEMABLE"] * 100);

      GiftValue.SmallResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, (Int64)_row_gift["GI_SMALL_RESOURCE_ID"]));
      GiftValue.LargeResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content, (Int64)_row_gift["GI_LARGE_RESOURCE_ID"]));
    }

    void WCP_IPlayerGifts.GetNumberGifts(WCP_IMessage ReceivedMessage, Int32 TableId, out Int32 NumGifts)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerGiftsReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope) ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerGiftsReply) _msg.Message.MsgContent;

      NumGifts = _message_content.Gifts.Tables[TableId].Rows.Count;
    }

  } // WCP_PlayerGifts class
  
}