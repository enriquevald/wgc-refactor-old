//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerDraws.cs
// 
//   DESCRIPTION: Retrieve Player Available Draws information
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-APR-2013 JMM     First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;
using WSI.WCP.WCP_Protocol;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetPlayerDraws(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetPlayerDraws _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetPlayerDraws);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_WKT_MsgGetPlayerDraws)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }

  } // WCP_Protocol_Class

  public partial class WCP_PlayerDraws_Class : WCP_IPlayerDraws
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player Available Draws Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage
    //        - Int32 ActivityIdx
    //
    //      - OUTPUT:
    //        - String DrawName
    //        - DateTime DrawAvailableQuantity
    //         
    // RETURNS:
    // 
    //   NOTES:

    void WCP_IPlayerDraws.GetPlayerDraws(WCP_IMessage ReceivedMessage, 
                                         Int32 DrawIdx, 
                                         out Int64 DrawID, 
                                         [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String DrawName, 
                                         out Int64 NumPendingNumbers,
                                         out Int64 NumRealPendingNumbers)
    {
      WCP_WKT_MsgGetPlayerDrawsReply _message_content;
      _message_content = ProvidePlayerDraws(ReceivedMessage);

      DrawID                = _message_content.Draws[DrawIdx].DrawID;
      DrawName              = _message_content.Draws[DrawIdx].Name;
      NumPendingNumbers     = _message_content.Draws[DrawIdx].PendingNumbers;
      NumRealPendingNumbers = _message_content.Draws[DrawIdx].RealPendingNumbers;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Number Draws
    //
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //        - Int32 NumDraws
    //         
    // RETURNS:
    //                            
    // 
    void WCP_IPlayerDraws.GetNumberDraws(WCP_IMessage ReceivedMessage, out Int32 NumDraws)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerDrawsReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerDrawsReply)_msg.Message.MsgContent;

      NumDraws = _message_content.Draws.Count;
    } // GetNumberDraws

    //------------------------------------------------------------------------------
    // PURPOSE: Provide IPlayerDraws from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           IPlayerDraws
    //                            
    // 
    public static WCP_WKT_MsgGetPlayerDrawsReply ProvidePlayerDraws(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetPlayerDrawsReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetPlayerDrawsReply)_msg.Message.MsgContent;

      return _message_content;
    } // ProvidePlayerDraws

  } // WCP_PlayerDraws_Class
}