//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerRequestGift_Class.cs
// 
//   DESCRIPTION: Manage Player Request Gift information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 30-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUL-2012 XIT     First version.
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_PlayerRequestGift(out WCP_IMessage Request, long SequenceId, Int32 CreditType)
    {
      try
      {
        WCP_Message _wcp_request;
        WCP_WKT_MsgPlayerRequestGift _wcp_content;

        PrinterStatus _printer_status;
        String _printer_status_text;

        TitoTicketDefault _ticket_template;
        Boolean _gp_print_redeemable;
        Boolean _gp_print_no_redeemable;


        _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGift);

        _wcp_request.MsgHeader.SequenceId = SequenceId;
        _wcp_content = (WCP_WKT_MsgPlayerRequestGift)_wcp_request.MsgContent;

        if (m_terminal_type == TerminalTypes.PROMOBOX && WSI.Common.TITO.Utils.IsTitoMode())
        {
          _wcp_content.TitoPrinterIsReady = true;
          _gp_print_redeemable = (GIFT_TYPE)CreditType == GIFT_TYPE.REDEEMABLE_CREDIT
                         && GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false);

          _gp_print_no_redeemable = (GIFT_TYPE)CreditType == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
                                   && GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false);

          if (_gp_print_redeemable || _gp_print_no_redeemable)
          {
            _ticket_template = new TitoTicketDefault(Computer.Alias(Environment.MachineName));

            if (!TitoPrinter.Init(_ticket_template))
            {
              // Printer not initialized correctly
              Log.Message("Tito Printer not initialized correctly!");
            }

            if (!TitoPrinter.IsReady())
            {
              TitoPrinter.GetStatus(out _printer_status, out _printer_status_text);
              Log.Error("Ticket printer error. Printer Status: " + _printer_status_text);
              _wcp_content.TitoPrinterIsReady = false;
            }
          }
        }


        Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Request = null;

        return;
      }
    }

    void WCP_IProtocol.GetReply_PlayerRequestGift(WCP_IMessage ReceivedMessage, out long CardPoints, out long Redeemable, out long PromoRedeemable, out long PromoNotRedeemable, out long ErrorPrinter)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgPlayerRequestGiftReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgPlayerRequestGiftReply)_msg.Message.MsgContent;

      CardPoints = (Int64)(Math.Floor(_message_content.PromoBalance.Points));
      Redeemable = (Int64)(_message_content.PromoBalance.Balance.Redeemable * 100m);
      PromoRedeemable = (Int64)(_message_content.PromoBalance.Balance.PromoRedeemable * 100m);
      PromoNotRedeemable = (Int64)(_message_content.PromoBalance.Balance.PromoNotRedeemable * 100m);
      ErrorPrinter = (long)ERROR_TYPE.None;

      if (m_terminal_type == TerminalTypes.PROMOBOX && WSI.Common.TITO.Utils.IsTitoMode())
      {

        if ((GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket") && _message_content.Ticket.TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
          || (GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket") && _message_content.Ticket.TicketType == TITO_TICKET_TYPE.PROMO_REDEEM))
        {
          PrintTicketTITO((Int64)_message_content.Ticket.ValidationNumber,
                        (Int32)_message_content.Ticket.MachineTicketNumber,
                        (Int32)_message_content.Ticket.TicketType,
                        _message_content.Ticket.Amount,
                        ToWCPSystemTime(_message_content.Ticket.CreatedDateTime),
                        ToWCPSystemTime(_message_content.Ticket.ExpirationDateTime),
                        _message_content.Ticket.Address1,
                        _message_content.Ticket.Address2, 
                        out ErrorPrinter);
        }
      }

    }
  } // WCP_Protocol_Class


  public partial class WCP_PlayerRequestGift_Class : WCP_IPlayerRequestGift
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Set the fields into the xml msg to send.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Message           : WCP_IMessage Request
    //          - Gift Id           : Int64 Id
    //          - Points to spend   : Int64 Points
    //          - Quantity of gifts : Int32 Quantity
    //
    //      - OUTPUT:
    //          
    // RETURNS:
    //     
    //
    void WCP_IPlayerRequestGift.SetPlayerRequestGift(WCP_IMessage Request, Int64 Id, Int64 Points, Int32 Quantity, Int64 DrawId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgPlayerRequestGift _wcp_content;
      WCP_MessageEnvelope _wcp_envelope;

      _wcp_envelope = (WCP_MessageEnvelope)Request;
      _wcp_request = _wcp_envelope.Message;
      _wcp_content = (WCP_WKT_MsgPlayerRequestGift)_wcp_request.MsgContent;

      _wcp_content.GiftId = Id;
      _wcp_content.GiftNumUnits = Quantity;
      _wcp_content.GiftPrice = Points;
      _wcp_content.DrawId = DrawId;
    }

  }// WCP_PlayerGifts class



}

