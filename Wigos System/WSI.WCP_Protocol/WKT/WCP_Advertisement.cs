//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Advertisement.cs
// 
//   DESCRIPTION: Manage Advertisements.
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 30-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-JUL-2012 ANG   Initial Version
// 23-AUG-2012 XIT   Modified Structures
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WSI.WCP.WCP_Protocol
{

  #region "Structures Definition"

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_AdvStep
  {
    public WCP_AdvStaticImage[] Images;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_AdvStaticImage
  {
    public Int64 Duration;
    public WKT_Rectangle Area;
    public WKT_Rectangle Image;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 256)]
    public String ResourcePath;
  }

  public struct WKT_Rectangle
  {
    public float X;
    public float Y;
    public float Width;
    public float Height;
  }

  #endregion

  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Process Get Steps Request
    //
    //  PARAMS:
    //      - INPUT: Message Sequence Id
    //
    //      - OUTPUT: WCP_IMessage interface with request message data
    //
    // RETURNS:
    // TO DO : Change name To NewRequesst_GetAdsSteps() or similar
    void WCP_IProtocol.NewRequest_GetAdvSteps(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgGetNextAdvStep _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgGetNextAdvStep);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_WKT_MsgGetNextAdvStep)_wcp_request.MsgContent;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    }

  }

  public class WCP_Advertisement : WCP_IAdvertisement
  {

    #region WCP_IAdvertisement Members


    public void GetStaticImage(WCP_IMessage ReceivedMessage, int IdxStep, out WCP_AdvStaticImage StaticImage)
    {
      WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetNextAdvStepReply _message_content;

      try
      {
        StaticImage = new WCP_AdvStaticImage();

        _msg = (WCP_MessageEnvelope)ReceivedMessage;
        _message_content = (WCP_WKT_MsgGetNextAdvStepReply)_msg.Message.MsgContent;
        if (_message_content.m_steps.Count >= IdxStep)
        {
          StaticImage.Duration = _message_content.m_steps[IdxStep].StaticImages[0].Duration;
          StaticImage.Area.Width = _message_content.m_steps[IdxStep].StaticImages[0].AreaWidth;
          StaticImage.Area.Height = _message_content.m_steps[IdxStep].StaticImages[0].AreaHeight;
          StaticImage.Area.X = _message_content.m_steps[IdxStep].StaticImages[0].AreaPosition.X;
          StaticImage.Area.Y = _message_content.m_steps[IdxStep].StaticImages[0].AreaPosition.Y;

          StaticImage.ResourcePath = WCP_Resources.ResourcePath(WCP_Resources.GetResource(_message_content,_message_content.m_steps[IdxStep].StaticImages[0].Image.ResourceId));
          
          StaticImage.Image.Width = _message_content.m_steps[IdxStep].StaticImages[0].Image.ImageWidth;
          StaticImage.Image.Height = _message_content.m_steps[IdxStep].StaticImages[0].Image.ImageHeight;
          StaticImage.Image.X = _message_content.m_steps[IdxStep].StaticImages[0].Image.Position.X;
          StaticImage.Image.Y = _message_content.m_steps[IdxStep].StaticImages[0].Image.Position.Y;
        }

      }
      catch (Exception ex)
      {
        StaticImage = new WCP_AdvStaticImage();
        WSI.Common.Log.Error("Error en GetStaticImage() " + ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Number Ads
    //
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //        - Int32 NumImages
    //         
    // RETURNS:
    //                            
    // 
    public void GetNumberStaticImage(WCP_IMessage ReceivedMessage, out Int32 NumImages)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT_MsgGetNextAdvStepReply _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT_MsgGetNextAdvStepReply)_msg.Message.MsgContent;

      NumImages = _message_content.m_steps.Count;
    }


    #endregion
  }
}
