//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerFields.cs
// 
//   DESCRIPTION: Manage Player Fields to send messages.
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 20-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2012 ANG   Initial Version
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_PlayerFieldsValues
  {
    public Int32 FieldId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String Value;
    public Int32 Updateable;
    public Int32 Type;
    public Int32 MinLength;
    public Int32 MaxLength;
    public Int32 ErrorCode;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String ErrorDescription;
  }

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Allow to create Request message 
    // 
    //  PARAMS:
    //      - INPUT: SequenceId 
    //
    //      - OUTPUT: WCP_IMessage object with request message.
    //         
    // RETURNS:
    // 
    //   NOTES:
    // 
    void WCP_IProtocol.NewRequest_UpdatePlayerFields(out WCP_IMessage Request,
                                               long SequenceId)
    {

      WCP_Message _wcp_request;
      WCP_WKT_MsgSetPlayerFields _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgSetPlayerFields);
      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_WKT_MsgSetPlayerFields)_wcp_request.MsgContent;

      _wcp_content.m_player_fields_table = new WCP_WKT.WKT_PlayerFieldsTable();
      _wcp_content.m_player = new WCP_WKT.WKT_Player();

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    } // NewRequest_UpdatePlayerFields
  } // WCP_Protocol_Class

  public class WCP_PlayerFields_class : WCP_IPlayerFields
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Provide IGetPlayerfield from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //           IGetPlayerField , interface that allow show GetNumFields and GetField(Idx)
    //                             from received message
    // 
    public static WCP_WKT.WKT_IGetPlayerField ProvidePlayerField(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_WKT.WKT_IGetPlayerField _message_content;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_WKT.WKT_IGetPlayerField)_msg.Message.MsgContent;

      return _message_content;
    } // ProvidePlayerField

    //------------------------------------------------------------------------------
    // PURPOSE: Fill NumFields with number of fields included in ReceivedMessage
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumFields
    //         
    // RETURNS:
    // 
    //   NOTES:
    void WCP_IPlayerFields.GetNumFields(WCP_IMessage ReceivedMessage, out Int64 NumFields)
    {
      WCP_WKT.WKT_IGetPlayerField _message_content;
      _message_content = ProvidePlayerField(ReceivedMessage);

      NumFields = _message_content.FieldsNumber;

    } // GetNumFields

    //------------------------------------------------------------------------------
    // PURPOSE: Fill field structure from FieldId and Message data
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage 
    //        - FieldIndex
    //
    //      - OUTPUT:
    //        - WCP_PlayerFieldValues with field value and other info ( FieldName, MaxLength, MinLength..)
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerFields.GetPlayerFieldValues(WCP_IMessage ReceivedMessage, Int32 FieldIdx, out WCP_PlayerFieldsValues Fields)
    {
      WCP_WKT.WKT_IGetPlayerField _message_content;
      WCP_WKT.WKT_IPlayerField _field;

      _message_content = ProvidePlayerField(ReceivedMessage);

      Fields = new WCP_PlayerFieldsValues();

      _field = _message_content.GetField(FieldIdx);

      Fields.FieldId = (Int32)_field.FieldId;
      Fields.MaxLength = _field.MaxLength;
      Fields.MinLength = _field.MinLength;
      Fields.Type = (Int32)_field.Type;
      Fields.Updateable = _field.Updateable;
      Fields.Value = _field.Value;
      Fields.ErrorCode = (Int32)_field.ErrorCode;
      Fields.ErrorDescription = _field.ErrorDescription;

    } // GetPlayerFieldValues

    //------------------------------------------------------------------------------
    // PURPOSE: Add field to current WCP Message 
    // 
    //  PARAMS:
    //      - INPUT:
    //         Request WCP_Message
    //         FieldValue WCP_PlayerFieldsValues structure with Field value
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerFields.AddField(WCP_IMessage Request, WCP_PlayerFieldsValues FieldValue)
    {
      WCP_MessageEnvelope _envelope;
      WCP_WKT_MsgSetPlayerFields _message;

      _envelope = (WCP_MessageEnvelope)Request;
      _message = (WCP_WKT_MsgSetPlayerFields)_envelope.Message.MsgContent;

      WCP_WKT.WKT_PlayerFieldsRow _row = _message.m_player_fields_table.NewRow();

      _row.FieldId = (PLAYER_INFO_FIELD_TYPE)FieldValue.FieldId;
      _row.Value = FieldValue.Value;
      _row.Type = (WKT_FIELD_TYPE)FieldValue.Type;
      _row.Updateable = FieldValue.Updateable;
      _row.MinLength = FieldValue.MinLength;
      _row.MaxLength = FieldValue.MaxLength;
      _row.ErrorDescription = FieldValue.ErrorDescription;
      _row.ErrorCode = (WKT_UPDATE_FIELD_STATUS)FieldValue.ErrorCode;

      _message.m_player_fields_table.Rows.Add(_row);

    } // AddField
    


  }
}
