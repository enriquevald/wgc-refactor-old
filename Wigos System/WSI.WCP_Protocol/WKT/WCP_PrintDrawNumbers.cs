//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PrintDrawNumbers.cs
// 
//   DESCRIPTION: Manage Player Print Draw Numbers information
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 06-MAY-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAY-2013 JMM     First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_RequestPlayerDrawNumbers(out WCP_IMessage Request, long SequenceId)
    {
      try
      {
        WCP_Message _wcp_request;
        WCP_WKT_MsgRequestPlayerDrawNumbers _wcp_content;

        _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbers);

        _wcp_request.MsgHeader.SequenceId = SequenceId;
        _wcp_content = (WCP_WKT_MsgRequestPlayerDrawNumbers)_wcp_request.MsgContent;

        _wcp_content.m_draws_list = new List<Int64>();
        _wcp_content.m_draws_list.Clear();

        Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Request = null;

        return;
      }
    }

  } // WCP_Protocol_Class


  public partial class WCP_PrintDrawNumbers_Class : WCP_IPrintDrawNumbers
  {

    #region WCP_IPrintDrawNumbers Members
    //------------------------------------------------------------------------------
    // PURPOSE: Set the fields into the xml msg to send.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Message : WCP_IMessage Request
    //          - DrawID : Int64 DrawID
    //
    //      - OUTPUT:
    //          
    // RETURNS:

    public void PrintDrawNumbersAddDraw(WCP_IMessage Request, Int64 DrawID)
    {
      WCP_Message _wcp_request;
      WCP_WKT_MsgRequestPlayerDrawNumbers _wcp_content;
      WCP_MessageEnvelope _wcp_envelope;

      _wcp_envelope = (WCP_MessageEnvelope) Request;
      _wcp_request = _wcp_envelope.Message;
      _wcp_content = (WCP_WKT_MsgRequestPlayerDrawNumbers) _wcp_request.MsgContent;

      _wcp_content.m_draws_list.Add (DrawID);
    }

    #endregion
  }// WCP_PrintDrawNumbers_Class class

}

