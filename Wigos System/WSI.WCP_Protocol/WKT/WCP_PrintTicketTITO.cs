﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PrintTicketTITO.cs
// 
//   DESCRIPTION:  WCP_PrintTicketTITO class
// 
//        AUTHOR: Samuel González
// 
// CREATION DATE: 19-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-NOV-2015 SGB    First version.
// 18-OCT-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class
  {
    private enum ERROR_TYPE
    {
      None = 1,
      InitPrinter = 2,
      IsReady = 3,
      PrintTicket = 4,
      Other = 5
    }

    static private Boolean m_init = false;

    private void PrintTicketTITO(long Ticket_ValidationNumber, long Ticket_MachineTicketNumber,
                                                  long Ticket_TicketType, Decimal Ticket_Amount,
                                                  WCP_SystemTime Ticket_Created, WCP_SystemTime Ticket_Expiration,
                                                  String Addres1, String Address2, out long Error)
    {

      TitoTicketDefault _ticket_template;
      PrinterStatus _printer_status;
      String _printer_status_text;

      if (!m_init)
      {
        _ticket_template = new TitoTicketDefault(Computer.Alias(Environment.MachineName));
        if (!TitoPrinter.Init(_ticket_template))
        {
          // Printer not initialized correctly
          Log.Message("Tito Printer not initialized correctly!");
          Error = (long)ERROR_TYPE.InitPrinter;
          return;
        }

        m_init = true;
      }

      if (!TitoPrinter.IsReady())
      {
        TitoPrinter.GetStatus(out _printer_status, out _printer_status_text);
        Log.Error("Ticket printer error. Printer Status: " + _printer_status_text);
        Error = (long)ERROR_TYPE.IsReady;
        return;
      }

      if (!TitoPrinter.PrintTicket(Ticket_ValidationNumber, (Int32)Ticket_MachineTicketNumber, (TITO_TICKET_TYPE)Ticket_TicketType, Ticket_Amount, ToDateTime(Ticket_Created), ToDateTime(Ticket_Expiration), Addres1, Address2))
      {
        Log.Message("Error TitoPrinter.PrintTicket");
        Error = (long)ERROR_TYPE.PrintTicket;
        return;
      }
      Error = (long)ERROR_TYPE.None;
    }
  }

}
