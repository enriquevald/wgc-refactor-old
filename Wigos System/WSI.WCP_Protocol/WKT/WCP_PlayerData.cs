//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PlayerData_Class.cs
// 
//   DESCRIPTION: Manage Player Data information
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 29-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUN-2012 XIT    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{

  public class WCP_PlayerData_Class : WCP_IPlayerData
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Does a new request to specify Pin and TrackData info
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Pin
    //        - Request
    //        - TrackData
    //
    //      - OUTPUT:
    //        - Request
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IPlayerData.SetPlayerData(WCP_IMessage Request
                                               , [In, MarshalAs(UnmanagedType.BStr, SizeConst = 13)] String Pin                                  
                                               , [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {
        WCP_Message _wcp_request;
        WCP_WKT.IExtendedPlayer _wcp_content;
        WCP_MessageEnvelope _wcp_envelope;

        _wcp_envelope = (WCP_MessageEnvelope)Request;
        _wcp_request = _wcp_envelope.Message;
        _wcp_content = (WCP_WKT.IExtendedPlayer)_wcp_request.MsgContent;

        _wcp_content.Player.Pin = Pin;
        _wcp_content.Player.Trackdata = TrackData;
   
    } // NewRequest_PlayerData

  } // WCP_PlayerData_Class
}