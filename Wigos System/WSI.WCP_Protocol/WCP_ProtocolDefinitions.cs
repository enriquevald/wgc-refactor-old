//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProtocolDefinitions.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 18-NOV-2011 ACC    Added Machine meters
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 21-JUN-2012 XIT    Modified GeParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 08-AUG-2012 ACC    Split WCP_Protocol.cs.
// 23-NOV-2012 JMM    Level id & next level info added to WCP_BasicPlayerInfoParameters.
// 14-AUG-2013 JMM    Fixed Bug WIG-113: PromoBOX fails on note recharges when anti-moneylaundry activated
// 14-OCT-2013 JRM    Added TITO mode params to GetParameters message
// 07-NOV-2013 NMR    Fixed error: missing parameter to send to Terminal
// 14-NOV-2013 NMR    Fixed error: missing items in enum WCP_RC
// 02-APR-2014 JCOR   Added VersionSignature,VersionSignatureReply.
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 18-NOV-2014 FJC    Added limit messages from Mobile Bank & Cashier Recharges 
// 05-DIC-2014 FJC    Added more limit messages from Mobile Bank & Cashier Recharges 
// 21-JAN-2015 JMV    Added number decimal in WCP_Parameters
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 05-AUG-2015 YNM    TFS-3109: Annonymous account min allowed cash in amount recharge
// 19-JAN-2016 FJC    Product Backlog Item 7732:Salones(2): Introducci�n contadores PCD desde el Cajero: Parte WCP/SAS-Host
// 22-FEB-2016 FJC    Product Backlog Item 9434:PimPamGo: Otorgar Premios.
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 23-JAN-2017 FJC    PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
// 31-MAR-2017 FJC    PBI 26015:ZitroTrack
// 30-MAY-2017 FJC    PBI 27761:Sorteo de m�quina. Modificiones en la visualizaci�n de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session.
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 21-SEP-2017 FJC    WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 18-OCT-2017 FJC    WIGOS-5928 Terminal draw does not work neither in LCD nor 4 lines
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 13-FEB-2018 FJC    Fixed Bug WIGOS-8056 Application crashes when leaving empty a configuration field in Terminal Draw screen
// 16-FEB-2018 FJC    WIGOS-8082 [Ticket #12237] Sorteo en Terminal
// 19-MAR-2018 LRS    WIGOS-6150  Codere_Argentina_1428 - Apply tax on Bill in at EGM.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_SystemTime
  {
    public ushort wYear;
    public ushort wMonth;
    public ushort wDayOfWeek;
    public ushort wDay;
    public ushort wHour;
    public ushort wMinute;
    public ushort wSecond;
    public ushort wMilliseconds;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_Parameters
  {
    public int keep_alive_interval;
    public int connection_timeout;
    public long utc_datetime;
    public int time_zone;
    public uint asset_number;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String machine_name;
    public bool note_acceptor_enabled;
    public bool read_barcode;
    public int num_currencies;
    public uint sas_flags;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String welcome_display_line_1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String welcome_display_line_2;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 10)]
    public String currency_symbol;
    public uint language_id;
    public uint system_mode;
    public uint wass_machine_locked;
    
    // TITO mode params
    public bool tito_mode;
    public int system_id;
    public Int64 sequence_id;
    public int machine_id;
    public bool allow_cashable_ticket_out;
    public bool allow_promotional_ticket_out;
    public bool allow_ticket_in;
    public int expiration_tickets_cashable;
    public int expiration_tickets_promotional;
    public long tickets_max_creation_amount_cents;
    public Int32 bills_meters_required;
    public uint tito_close_session_when_remove_card;
    public Int32 host_id;

    //JMV - 21-01-15
    public byte format_decimals_flag;
    public byte num_decimals;

    public uint change_stacker_mode;
    public Int64 current_stacker_id;

    public int smib_protocol;
    public bool smib_protocol_request_params;

    // FJC 29-JUN-2005
    public Int32 technician_activity_timeout;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String	tito_ticket_localization;
   
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String	tito_ticket_address_1;
   
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String	tito_ticket_address_2;
    
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String	tito_ticket_title_restricted;
    
    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String	tito_ticket_title_debit;

    // JMM 21-MAR-2016: Mico2 mode
    public bool mico2;
    public bool enable_disable_note_acceptor;

    // FJC 30-JAN-2017 PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
    public bool enable_disable_terminal_draw;

    // FJC 24-MAR-2017 PBI 26015:ZitroTrack
    public bool enable_disable_zitro_track;

    // JMM 13-SEP-2017: BillIn lock limit
    public Int64 bill_in_limit_lock_threshold;
    public Int32 bill_in_limit_lock_mode;

    // FJC 30-NOV-2017 WIGOS-3998 MobiBank: InTouch - Card out
    public bool enable_disable_mobibank;

  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_BasicPlayerInfoParameters
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 80)]
    public String FullName;
    public DrawGender Gender;
    public long Birthday;
    public ulong MoneyBalance;
    public ulong PointsBalance;
    public ulong Money2Balance;
    public ulong Points2Balance;
    public bool InSession;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String TerminalName;
    public int LevelId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String LevelName;
    public long EnterDate;
    public long Expiration;
    public int NextLevelId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String NextLevelName;
    public ulong PointsToNextLevel;
    public ulong RedeemableBalance;
    public ulong NonRedeemableBalance;
    public bool NoteAcceptorEnabled;
    public ulong CurrentSpent;
    public ulong NextLevelPointsGenerated;
    public ulong NextLevelPointsDisc;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_PlayerPromotion
  {
    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String PromoName;
    public long PromoDate;
    public ulong AwardedBalance;
    public ulong CurrentBalance;
  }

  //FJC 12-NOV-2014
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_SiteJackpotAwarded
  {
    public long jackpot_id;
    public long jackpot_datetime;
    public long jackpot_level_id;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String jackpot_level_name;

    public long jackpot_amount_cents;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 100)]
    public String jackpot_machine_name;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 50)]
    public String winner_track_data;

    [MarshalAs(UnmanagedType.BStr, SizeConst = 81)]
    public String winner_holder_name;

    public long parameters_show_only_to_winner;
    public long parameters_winner_show_time_seconds;
    public long parameters_no_winner_show_time_seconds;
  }

  //GameGateway
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_LCD_ParamsGamesGateWayParams
  {
    [MarshalAs(UnmanagedType.U4)]
    public int GameGateway;
    [MarshalAs(UnmanagedType.U4)]
    public int TerminalId;
    [MarshalAs(UnmanagedType.U4)]
    public int ReservedCredit;
    [MarshalAs(UnmanagedType.U2)]
    public UInt16 AwardPrizes;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String Url;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String UrlTest;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String ProviderName;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 120)]
    public String PartnerId;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_GameGateWaySession
  {
    [MarshalAs(UnmanagedType.U8)]
    public long PlaySessionId;
    [MarshalAs(UnmanagedType.U8)]
    public long AccountId;
    [MarshalAs(UnmanagedType.U2)]
    public Int16 MessageType;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String MessageText;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_GameGateWayGetCredit
  {

    [MarshalAs(UnmanagedType.U8)]
    public long AccountId;
    [MarshalAs(UnmanagedType.U8)]
    public long RequestAmount;
    [MarshalAs(UnmanagedType.U2)]
    public Int16 Status;
    [MarshalAs(UnmanagedType.U2)]
    public Int16 MessageType;
    [MarshalAs(UnmanagedType.U2)]
    public Int16 TypeRequest;
    [MarshalAs(UnmanagedType.U8)]
    public Int64 CmdId;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String MessageText;
  } 

  // FB
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_LCD_ParamsFBParams
  {
    [MarshalAs(UnmanagedType.U4)]
    public int Enabled;
    [MarshalAs(UnmanagedType.U4)]
    public int PinRequest;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String HostAddress;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String Url;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 120)]
    public String Login;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 120)]
    public String Password;
  } // WCP_LCD_ParamsFBParams

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_UpdatablePCDMeter
  {
    [MarshalAs(UnmanagedType.U4)]
    public Int32 Code;
    [MarshalAs(UnmanagedType.U8)]
    public Int64 Value;
    [MarshalAs(UnmanagedType.U4)]
    public Int32 Action;
  }
  
  // 
  // Terminal Draw
  // 
  public class TERMINAL_DRAW_CONSTANTS
  {
    // IMPORTANT!!!! 
    // This constant(TERMINAL_DRAW_MAX_RECHARGES) must go in accordance with the following constants:
    // SAS-HOST (C):      TERMINAL_DRAW_MAX_RECHARGES 
    // LCD-InTouch (C#):  MAX_TERMINAL_DRAW_RECHARGES 
    public const int TERMINAL_DRAW_MAX_RECHARGES = 20;       // 13-02-2018 IMPORTANT!!!! with stable version the value was: (5)

    // This constant(TERMINAL_DRAW_MAX_MAX_GP_4_LINES) must go in accordance with the following constants:
    // SAS-HOST (C):      TERMINAL_DRAW_MAX_GP_4_LINES 
    public const int TERMINAL_DRAW_MAX_MAX_GP_4_LINES = 24;  // 13-02-2018 IMPORTANT!!!! with stable version the value was: (120)
    
  }
  // Terminal Draw (Recharge && Draw)
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_TerminalDrawsRecharges
  {
    public long operation_id;
    public long amount_total_cash_in;
    public long amount_re_bet;
    public long amount_nr_bet;
    public long amount_points_bet;
    public long game_id;
    public long game_type;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_TerminalDrawsGame
  {
    public long NumRecharges;
    
    [MarshalAs(UnmanagedType.BStr, SizeConst = 240)]
    public String url;

    // First Screen
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String FirstScreenMessageLine0;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String FirstScreenMessageLine1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String FirstScreenMessageLine2;
    public long FirstScreenTimeOut;
    public long TimeOutExpiresParticipateInDraw;
    public long ForceParticipateInDraw;

    // Second Screen
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String SecondScreenMessageLine0;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String SecondScreenMessageLine1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String SecondScreenMessageLine2;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String SecondScreenMessageLine3;
    public long SecondScreenTimeOut;
    public long ForceExecuteDraw;
    
    // Play Session Id
    public long PlaySessionId;

    // Total Bet Amount
    public long TotalBetAmount;

    // BalanceAmount
    public long TotalBalanceAmount;

    // Recharges
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_RECHARGES)]
    public WCP_TerminalDrawsRecharges[] Recharges;

    public WCP_TerminalDrawsGame(Int16 DummyValue)
    {
      NumRecharges = 0;
      PlaySessionId = 0;
      TotalBetAmount = 0;
      TotalBalanceAmount = 0;
      
      SecondScreenTimeOut = 0;
      TimeOutExpiresParticipateInDraw = 0;
      ForceParticipateInDraw = 0;
      ForceExecuteDraw = 0;
      
      url = string.Empty;
      
      //First Screen
      FirstScreenMessageLine0 = string.Empty;
      FirstScreenMessageLine1 = string.Empty;
      FirstScreenMessageLine2 = string.Empty;
      FirstScreenTimeOut = 0;

      //Second Screen
      SecondScreenMessageLine0 = string.Empty;
      SecondScreenMessageLine1 = string.Empty;
      SecondScreenMessageLine2 = string.Empty;
      SecondScreenMessageLine3 = string.Empty;
      
      Recharges = new WCP_TerminalDrawsRecharges[TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_RECHARGES];
    }
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_TerminalDrawsAccount 
  {
    public long AccountId;
    public long TerminalId;
    public long NumGames;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
    public WCP_TerminalDrawsGame[] Games;

    public WCP_TerminalDrawsAccount(Int16 DummyValue)
    {
      AccountId = 0;
      NumGames = 0;
      TerminalId = 0;
      Games = new WCP_TerminalDrawsGame[1];
    }
  }

  // 
  // Terminal Draw
  // 







  // Terminal Draw (Prize Plan)
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_TerminalDrawPrizePlanDetail
  {
    public long GameId;
    public long GameType;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 120)]
    public String TerminalGameUrl;
    [MarshalAs(UnmanagedType.BStr, SizeConst = 120)]
    public String TerminalGameName;
    
    //Winner
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultWinnerScreenMessageLine0;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultWinnerScreenMessageLine1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultWinnerScreenMessageLine2;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultWinnerScreenMessageLine3;

    //Looser
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultLooserScreenMessageLine0;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultLooserScreenMessageLine1;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultLooserScreenMessageLine2;
    [MarshalAs(UnmanagedType.BStr, SizeConst = TERMINAL_DRAW_CONSTANTS.TERMINAL_DRAW_MAX_MAX_GP_4_LINES)]
    public String ResultLooserScreenMessageLine3;

    public long IsWinner;
    public long AmountRePlayedDraw;
    public long AmountRePlayedWon;
    public long AmountNrPlayedDraw;
    public long AmountNrPlayedWon;
    public long AmountPointsPlayedDraw;
    public long AmountPointsPlayedWon;
    public long TerminalGameTimeOut;
    public long AmountTotalBalance;
    public long PlaySessionId;
    public long Forced;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_TerminalDrawPrizePlan
  {
    public long NumGames;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
    public WCP_TerminalDrawPrizePlanDetail[] Games;

    public WCP_TerminalDrawPrizePlan(Int16 Dummyvalue)
    {
      NumGames = 0;
      Games = new WCP_TerminalDrawPrizePlanDetail[1];
    }
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_LCD_ParamsTerminalDrawParams
  {
    [MarshalAs(UnmanagedType.U4)]
    public int Enabled;
  } // WCP_LCD_ParamsTerminalDrawParams

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_LCD_ParamsTerminalReserveParams
  {
    [MarshalAs(UnmanagedType.U4)]
    public int Enabled;
  } // WCP_LCD_ParamsTerminalReserveParams

  // MobiBank
  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_LCD_ParamsMobiBankParams
  {
    [MarshalAs(UnmanagedType.U4)]
    public int Enabled;
  } // WCP_LCD_ParamsMobiBankParams

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_MobiBankRechargeNotification
  {
    [MarshalAs(UnmanagedType.U8)]
    public long AccountId;

    [MarshalAs(UnmanagedType.U8)]
    public long TransFerType; // enum MobileBankRechargeNotificationType

    [MarshalAs(UnmanagedType.U8)]
    public long TransFerAmount;
  } // WCP_MobiBankRechargeNotification

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_MobiBankRequestTransFer
  {
    [MarshalAs(UnmanagedType.U8)]
    public long CmdId;

    [MarshalAs(UnmanagedType.U8)]
    public long RequestId; 
  } // WCP_MobiBankRechargeNotification

  [StructLayout(LayoutKind.Sequential)]
  public struct WCP_MobiBankCancelRequest
  {
    [MarshalAs(UnmanagedType.U8)]
    public long AccountId;

    [MarshalAs(UnmanagedType.U4)]
    public int CancelReason;
  } // WCP_MobiBankCancelRequest
  

  public enum DrawGender
  {
    ALL = 0,
    MEN_ONLY = 1,
    WOMEN_ONLY = 2
  }

  public enum WCP_Msg
  {
    // Request                            // Reply
      RequestService,                     RequestServiceReply
    , EnrollServer,                       EnrollServerReply
    , EnrollTerminal,                     EnrollTerminalReply
    , GetParameters,                      GetParametersReply
    , UnenrollTerminal,                   UnenrollTerminalReply
    , UnenrollServer,                     UnenrollServerReply
    , GetCardType,                        GetCardTypeReply
    , LockCard,                           LockCardReply
    , StartCardSession,                   StartCardSessionReply
    , PlayConditions,                     PlayConditionsReply
    , ReportPlay,                         ReportPlayReply
    , AddCredit,                          AddCreditReply
    , EndSession,                         EndSessionReply
    , ReportEvent,                        ReportEventReply
    , ExecuteCommand,                     ExecuteCommandReply
    , ReportMeters,                       ReportMetersReply
    , IntegrityCheck,                     IntegrityCheckReply
    , KeepAlive,                          KeepAliveReply
    , ReportEventList,                    ReportEventListReply
    , MobileBankCardCheck,                MobileBankCardCheckReply
    , MobileBankCardTransfer,             MobileBankCardTransferReply
    , Error,                              Unknown
    , ReportGameMeters,                   ReportGameMetersReply
    , GetLogger,                          GetLoggerReply
    , ReportHPCMeter,                     ReportHPCMeterReply
    , UpdateCardSession,                  UpdateCardSessionReply
    , CardInformation,                    CardInformationReply // Mobile bank: iPOD.
    , ReportMachineMeters,                ReportMachineMetersReply
    , MoneyEvent,                         MoneyEventReply
    , CancelStartSession,                 CancelStartSessionReply
    , CashSessionPendingClosing,          CashSessionPendingClosingReply
    , MBInformation,                      MBInformationReply
    , SasMeters,                          SasMetersReply
    , PlaySessionMeters,                  PlaySessionMetersReply
    , EGMHandpays,                        EGMHandpaysReply
    , BonusTransferStatus,                BonusTransferStatusReply
    , VersionSignature,                   VersionSignatureReply
    , CheckTicket,                        CheckTicketReply // Mobile bank: iPOD.
    , PayTicket,                          PayTicketReply // Mobile bank: iPOD.

    //JMM 03-JUL-2014: PIN validation message and its reply
    , PlayerCheckPin,                     PlayerCheckPinReply
    //XCD 30-APR-2015: Smib protocol parameters to send
    , GetProtocolParameters,              GetProtocolParametersReply
    //JMM 12-AUG-2015: Multi-denom info and its reply
    , ReportMultiDenomInfo,               ReportMultiDenomInfoReply
    
    //FJC 02-JAN-2016 (Feature 22239:Sorteo de m�quina)
    , TerminalDrawGetPendingDraw,         TerminalDrawGetPendingDrawReply
    , TerminalDrawProcessDraw,            TerminalDrawProcessDrawReply      
    
    //JMM 01-FEB-2017 Reserve Terminal
    , ReserveTerminal,                    ReserveTerminalReply
    
    //FJC 02-JAN-2016 (Feature 22239:Sorteo de m�quina)
    , TerminalDrawGameTimeElapsed,        TerminalDrawGameTimeElapsedReply

    //FJC 27-11-2017: WIGOS-3592 MobiBank: InTouch - Recharge request
    , MobiBankRecharge,                   MobiBankRechargeReply
    , MobiBankCancelRequest,              MobiBankCancelRequestReply
    , MobiBankNotifyRechargeEGM,          MobiBankNotifyRechargeEGMReply
                                          
    // PromoBOX
    , WKT_MsgGetParameters = 10001,       WKT_MsgGetParametersReply = 10002
    , WKT_MsgGetResource = 10003,         WKT_MsgGetResourceReply = 10004
    , WKT_MsgGetNextAdvStep = 10005,      WKT_MsgGetNextAdvStepReply = 10006
    , WKT_MsgGetPlayerGifts = 10007,      WKT_MsgGetPlayerGiftsReply = 10008
    , WKT_MsgPlayerRequestGift = 10009,   WKT_MsgPlayerRequestGiftReply = 10010
    , WKT_MsgSetPlayerFields = 10011,     WKT_MsgSetPlayerFieldsReply = 10012
    , WKT_MsgGetPlayerInfo = 10013,       WKT_MsgGetPlayerInfoReply = 10014
    , WKT_MsgGetPlayerPromos = 10015,     WKT_MsgGetPlayerPromosReply = 10016
    , WKT_MsgGetPlayerActivity = 10017,   WKT_MsgGetPlayerActivityReply = 10018
    , WKT_MsgPrintRechargesList = 10019,  WKT_MsgPrintRechargesListReply = 10020
    , WKT_MsgGetPlayerDraws = 10021,      WKT_MsgGetPlayerDrawsReply = 10022
    , WKT_MsgPrintDrawNumbers = 10023,    WKT_MsgPrintDrawNumbersReply = 10024
    , WKT_MsgPlayerRequestPromo = 10025,  WKT_MsgPlayerRequestPromoReply = 10026

    //TITO    
    , TITO_IsValidTicket = 20000,         TITO_IsValidTicketReply = 20001
    , TITO_CancelTicket = 20002,          TITO_CancelTicketReply = 20003
    , TITO_CreateTicket = 20004,          TITO_CreateTicketReply = 20005
    , TITO_MsgEgmParameters = 20006,      TITO_MsgEgmParametersReply = 20007
    , TITO_MsgValidationNumber = 20008,   TITO_MsgValidationNumberReply = 20009

    // LCD Display Messages 21000..22000
    , LCD_MsgGetParameters = 21001,       LCD_MsgGetParametersReply = 21002
    , LCD_MsgGetPromoBalance = 21003,     LCD_MsgGetPromoBalanceReply = 21004

    // GAMEGATEWAY Messages 22000..23000
      // FJC 06-OCT-2015 (Product Bakclog 4704)
    , SubsMachinePartialCredit = 22000,   SubsMachinePartialCreditReply = 22001
    , GameGatewayGetCredit = 22002,       GameGatewayGetCreditReply = 22003
      // FJC 10-MAR-2016 (PBI 9105:BonoPlay: LCD: Changes; Task 10362: BonoPlay LCD: Refactorizar mensajer�a.)
    , GameGatewayProcessMsg = 22004,      GameGatewayProcessMsgReply = 22005

  };

  public enum WCP_RC
  {
    OK
    ,
    ERROR
    ,
    DATABASE_OFFLINE
    ,
    SERVICE_NOT_AVAILABLE
    ,
    SERVICE_NOT_FOUND
    ,
    SERVER_NOT_AUTHORIZED
    ,
    SERVER_SESSION_NOT_VALID
    ,
    TERMINAL_NOT_AUTHORIZED
    ,
    TERMINAL_SESSION_NOT_VALID
    ,
    CARD_NOT_VALID
    ,
    CARD_BLOCKED
    ,
    CARD_EXPIRED
    ,
    CARD_ALREADY_IN_USE
    ,
    CARD_NOT_VALIDATED
    ,
    CARD_BALANCE_MISMATCH
    ,
    CARD_REMOTE_BALANCE_MISMATCH
    ,
    CARD_SESSION_NOT_CLOSED
    ,
    CARD_SESSION_NOT_VALID
    ,
    CARD_WRONG_PIN
    ,
    INVALID_SESSION_ID
    ,
    UNKNOWN_GAME_ID
    ,
    INVALID_AMOUNT
    ,
    EVENT_NOT_VALID
    ,
    CRC_MISMATCH
    ,
    NOT_ENOUGH_PLAYERS
    ,
    MSG_FORMAT_ERROR
    ,
    INVALID_MSG_TYPE
    ,
    INVALID_PROTOCOL_VERSION
    ,
    INVALID_SEQUENCE_ID
    ,
    INVALID_SERVER_ID
    ,
    INVALID_TERMINAL_ID
    ,
    INVALID_SERVER_SESSION_ID
    ,
    INVALID_TERMINAL_SESSION_ID
    ,
    INVALID_COMPRESSION_METHOD
    ,
    INVALID_RESPONSE_CODE
    ,
    UNKNOWN
    ,
    LAST
    ,
    SESSION_TIMEOUT
    ,
    RECHARGE_NOT_AUTHORIZED
    ,
    MB_EXCEEDED_TOTAL_RECHARGES_LIMIT 
    ,
    MB_EXCEEDED_RECHARGE_LIMIT
    ,
    MB_EXCEEDED_RECHARGES_NUMBER_LIMIT 
    ,
    EXCEEDED_MAX_ALLOWED_CASH_IN 
    ,
    EXCEEDED_MAX_ALLOWED_DAILY_CASH_IN
    ,
    ML_RECHARGE_NOT_AUTHORIZED
    ,
    ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED
    ,
    ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED
    ,
    ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED
    ,
    ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED
    ,
    MB_SESSION_CLOSED
    ,
    STACKER_ID_NOT_FOUND
    ,
    STACKER_ID_ALREADY_IN_USE
    ,
    GAMING_DAY_EXPIRED
    ,
    ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT
    ,
    GENERAL_PARAM_DUPLICATED
    ,
    TERMINAL_RESERVED
    ,
    ANOTHER_TERMINAL_RESERVED
    ,
    WKT_OK = 10000, // OK
    WKT_DISABLED = 10001, // Kiosk disabled         / Kiosco deshabilitado
    WKT_ERROR = 10002, // Error                  / Error
    WKT_MESSAGE_NOT_SUPPORTED = 10003, // Message Not Supported  / Mensaje no soportado
    WKT_RESOURCE_NOT_FOUND = 10004, // Resource Not found     / Recurso no encontrado

    // Login
    WKT_CARD_NOT_VALID = 10010, // Card Not Valid     / Tarjeta no v�lida
    WKT_ACCOUNT_BLOCKED = 10011, // Acccount Blocked   / Cuenta bloqueada
    WKT_ACCOUNT_NO_PIN = 10012, // PIN not configured / PIN sin configurar
    WKT_WRONG_PIN = 10013, // Wrong PIN          / PIN err�neo

    //GetRequest
    WKT_UNKNOWN = 10014,
    WKT_CARD_ERROR = 10015,
    WKT_NOT_ENOUGH_POINTS = 10016,
    WKT_NOT_AVAILABLE = 10017,
    WKT_NOT_ENOUGH_STOCK = 10018,
    WKT_CARD_HAS_NON_REDEEMABLE = 10019,
    WKT_FUNCTIONALITY_OFF_SCHEDULE = 10020,
    WKT_PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS = 10021,
    WKT_FUNCTIONALITY_NOT_ENABLED = 10022,
    WKT_PROMO_NOT_CURRENTLY_APPLICABLE = 10027,

    //TITO GetRequest
    TITO_NOT_ALLOWED_ISSUANCE = 10023,
    TITO_TICKET_ERROR = 10024,
    TITO_MESSAGE_NOT_SUPPORTED = 10025,
    TITO_COULD_NOT_CANCEL_TICKET = 10026,
    TITO_INVALID_VALIDATION_NUMBER = 10028,

    //WKT & TITO Errors
    WKT_TITO_PRINTER_ERROR = 10031,
  }

  public enum WCP_ICardType
  {
    Unknown           = 0
  , Player            = 1
  , MobileBank        = 2
  , PlayerPin         = 3
  , Tech              = 10
  , TechPin           = 11
  , ChangeStacker     = 12
  , ChangeStackerPin  = 13
  , CardTypeEmployee  = 14
}

  public enum WCP_IFundTransferType
  {
    UNKNOWN
  ,
    AFT
  , EFT
  }



}
