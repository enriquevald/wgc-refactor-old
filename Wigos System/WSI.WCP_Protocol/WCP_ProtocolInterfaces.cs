//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProtocolInterfices.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 18-NOV-2011 ACC    Added Machine meters
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 21-JUN-2012 XIT    Modified GeParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 08-AUG-2012 ACC    Split WCP_Protocol.cs.
// 29-OCT-2012 JMM    Added WCP_IPrintRechargesList.
// 17-DEC-2012 JMM    Gifts split info added.
// 29-APR-2013 JMM    PromoBOX print draw numbers added.
// 31-MAY-2013 JMM    Cashier Session Pending Closing added.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 24-OCT-2013 NMR    SystemId removed from message; renamed MachineValidation
// 06-NOV-2013 NMR    Removed Asset Number and terminalId
// 03-DIC-2013 NMR    Add property PlaySessionId to create tickets
// 02-APR-2014 JCOR   Added NewRequest_VersionSignature
// 11-JUL-2014 XCD    Calculate estimated points awarded
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 29-JUN-2015 FJC    Product Backlog Item 282.
// 14-SEP-2015 DHA    Product Backlog Item 3705: Added coins from terminals
// 17-NOV-2015 ETP    Product Backlog Item 5835: Added transfer amounts.
// 27-NOV-2015 SGB    Product Backlog Item 6050: Added & modify interfaces for general params.
// 09-NOV-2015 SGB    Product Backlog Item 6050: Added interface for read general params from PromoBox.
// 19-JAN-2016 FJC    Product Backlog Item 7732:Salones(2): Introducci�n contadores PCD desde el Cajero: Parte WCP/SAS-Host
// 19-FEB-2016 ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 22-FEB-2016 FJC    Product Backlog Item 9434:PimPamGo: Otorgar Premios.
// 16-MAR-2016 FJC    PBI 9105: BonoPlay: LCD: Cambios varios
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado
// 03-NOV-2016 FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
// 22-NOV-2016 JMM    PBI 19744:Pago autom�tico de handpays
// 23-JAN-2017 FJC    PBI 22245:Sorteo de m�quina - Ejecuci�n - US 2 - Ejecuci�n
// 02-AUG-2017 JMM    PBI 28983: EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
// 02-AUG-2017 JMM    PBI 28983:EGM Reserve - Phase 1 - Reserve on EGM by Carded Player
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 31-AUG-2017 JMM    WIGOS-4700-Promotional credit reported as cashable after card out/in: Modify LKT-SAS-HOST for send counters
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 12-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
// 21-SEP-2017 FJC    WIGOS-5280 Reserve EGM - Allow to reserve EGM in the LCD Intouch.
// 29-NOV-2017 FJC    WIGOS-3592 MobiBank: InTouch - Recharge request
// 19-MAR-2018 LRS    WIGOS-6150  Codere_Argentina_1428 - Apply tax on Bill in at EGM.
// 09-MAY-2018 FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public interface WCP_IPlayerData
  {
    void SetPlayerData(WCP_IMessage Request
                               , [In, MarshalAs(UnmanagedType.BStr, SizeConst = 13)] String Pin
                               , [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);
  }

  public interface WCP_IPlayerInfo
  {
    void GetBasicPlayerInfo(WCP_IMessage ReceivedMessage, out WCP_BasicPlayerInfoParameters BasicPlayerInfo);
    void GetNumberPromotions(WCP_IMessage ReceivedMessage, out Int32 NumPromotions);
    void GetPromotionInfo(WCP_IMessage ReceivedMessage, Int32 IdPromotion, out WCP_PlayerPromotion Promotion);
  }

  public interface WCP_IPlayerGifts
  {
    void GetPlayerGift(WCP_IMessage ReceivedMessage, Int32 GiftIdx, Int32 TableId, out WCP_PlayerGift GiftValue);
    void GetNumberGifts(WCP_IMessage ReceivedMessage, Int32 TableId, out Int32 NumGifts);
  }

  public interface WCP_IPlayerPromos
  {
    void GetNumberPromos(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 NumPromos);
    void GetPlayerPromo(WCP_IMessage ReceivedMessage
                                          , Int32 PromoIdx, Int32 ListId
                                          , out WCP_PlayerPromo PromoValue);

    void GetNumberApplicablePromos(WCP_IMessage ReceivedMessage, out Int32 NumPromos);
    void GetPlayerApplicablePromo(WCP_IMessage ReceivedMessage
                                , Int32 PromoIdx
                                , out WCP_PlayerPromo PromoValue);
  }

  public interface WCP_IPlayerRequestGift
  {
    void SetPlayerRequestGift(WCP_IMessage ReceivedMessage, Int64 Id, Int64 Points, Int32 Quantity, Int64 DrawId);
  }

  public interface WCP_IPlayerActivity
  {
    void GetNumberActivities(WCP_IMessage ReceivedMessage, out Int32 NumPromos);
    void GetPlayerActivity(WCP_IMessage ReceivedMessage, Int32 ActivityIdx
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 256)] out String ActDescription
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 25)]  out String ActDate
                                        , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 25)]  out String ActQuantity);
  }

  public interface WCP_ICurrency
  {
    void GetCurrencyValue(WCP_IMessage ReceivedMessage, Int32 NumCurrency, Int32 NumRejectedValue, out Int32 Value);
    void GetCurrencyName(WCP_IMessage ReceivedMessage, Int32 NumCurrency
                         , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String IsoCode
                         , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String Alias1
                         , [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 4)] out String Alias2);
    void GetNumberRejected(WCP_IMessage ReceivedMessage, Int32 NumCurrency, out Int32 NumRejected);
  }

  public interface WCP_IMessage
  {
    void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId);
    void GetMessageType(out WCP_Msg MessageType);
    void GetResponseCode(out WCP_RC ResponseCode);
    void SetResponseCode(WCP_RC ResponseCode);
  }

  public interface WCP_IReportEventList
  {
    void AddDeviceStatus(WCP_IMessage Request, long Code, long Status, long Priority, long Date);
    void AddOperation(WCP_IMessage Request, long Code, long Data, long Date, long TechMode, long TechnicalAccountId);
  }

  // Added ANG 29-JUN-2012
  public interface WCP_IPlayerFields
  {
    void GetNumFields(WCP_IMessage ReceivedMessage, out Int64 NumFields);
    void GetPlayerFieldValues(WCP_IMessage ReceivedMessage, Int32 FieldIdx, out WCP_PlayerFieldsValues Fields);
    void AddField(WCP_IMessage Request, WCP_PlayerFieldsValues FieldValue);
  }

  // Added ANG 30-JUL-2012
  public interface WCP_IAdvertisement
  {
    void GetStaticImage(WCP_IMessage ReceivedMessage, int IdxStep, out WCP_AdvStaticImage Step);
    void GetNumberStaticImage(WCP_IMessage ReceivedMessage, out Int32 NumImages);
  }


  public interface WCP_IPromoParams
  {
    void GetNumFunctionalities(WCP_IMessage ReceivedMessage, out Int32 NumFunc);
    void GetFunctionality(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 FunctionId);
    void GetNumImages(WCP_IMessage ReceivedMessage, out Int32 NumImages);
    void GetImage(WCP_IMessage ReceivedMessage, Int32 IndexImage, out WCP_WKT_Image Image);
    void GetNumCustomMessages(WCP_IMessage ReceivedMessage, out Int32 NumMessages);
    void GetCustomMessage(WCP_IMessage ReceivedMessage, Int32 IndexMessage, out WCP_WKT_Message Message);
    void GetScheduleTime(WCP_IMessage ReceivedMessage, out Int32 FromTime, out Int32 ToTime);
    void GetGiftsSplit(WCP_IMessage ReceivedMessage, out Int32 Enabled,
                       out String CategoryName01, out String CategoryName02, out String CategoryName03,
                       out String CategoryShort01, out String CategoryShort02, out String CategoryShort03);
  }

  public interface WCP_ILCDParams
  {
    void GetNumFunctionalities(WCP_IMessage ReceivedMessage, out Int32 NumFunc);
    void GetInTouchWebUrl(WCP_IMessage ReceivedMessage, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 120 + 1)] out String Url);
    void GetFunctionality(WCP_IMessage ReceivedMessage, Int32 Idx, out Int32 FunctionId);
    void GetNumImages(WCP_IMessage ReceivedMessage, out Int32 NumImages);
    void GetImage(WCP_IMessage ReceivedMessage, Int32 IndexImage, out WCP_WKT_Image Image);
    void GetGameGatewayParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsGamesGateWayParams Params);
    void GetFBParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsFBParams Params);
    void GetTerminalDrawParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsTerminalDrawParams Params);
    void GetTerminalReserveParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsTerminalReserveParams Params);
    void GetMobiBankParams(WCP_IMessage ReceivedMessage, out WCP_LCD_ParamsMobiBankParams Params);
  }

  // Added JMM 29-OCT-2012
  public interface WCP_IPrintRechargesList
  {
    void PrintRechargesListAddNote(WCP_IMessage Request, double NoteValue);
    void PrintRechargesListSetTotal(WCP_IMessage Request, double TotalRechargesValue);
  }

  // Added JMM 29-APR-2013
  public interface WCP_IPlayerDraws
  {
    void GetPlayerDraws(WCP_IMessage ReceivedMessage,
                         Int32 DrawIdx,
                         out Int64 DrawID,
                         [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String DrawName,
                         out Int64 NumPendingNumbers,
                         out Int64 NumRealPendingNumbers);
    void GetNumberDraws(WCP_IMessage ReceivedMessage, out Int32 NumDraws);
  }

  // Added JMM 06-MAY-2013
  public interface WCP_IPrintDrawNumbers
  {
    void PrintDrawNumbersAddDraw(WCP_IMessage Request, Int64 DrawID);
  }

  // Added JMM 09-OCT-2013
  public interface WCP_IPlayerRequestPromo
  {
    void SetPlayerRequestPromo(WCP_IMessage ReceivedMessage, Int64 PromoId, double PromoReward, Int64 PromoCreditType);
  }

  // Added JMM 28-JAN-2014
  public interface WCP_IBonusing
  {
    void GetBonusInfo(WCP_IMessage ReceivedMessage,
                      out Int64 TransactionID,
                      out Int64 CentsToTransferRE,
                      out Int64 CentsToTransferPromoRE,
                      out Int64 CentsToTransferPromoNR,
                      out Int32 Timeout,
                      out Int64 MinBetCents,
                      out Boolean NotRedimibleMustBeZero,
                      out int SasTaxStatus,
                      out Int32 TransferFlags);

    void GetNotificationBonusInfo(WCP_IMessage ReceivedMessage,
                                  out Int64 TransactionID);
  }

  public interface WCP_ISwValidation
  {
    void GetSwValidationInfo(WCP_IMessage ReceivedMessage,
                             out Int64 ValitadionID,
                             out Int32 Method,
                             out String Seed);
  }

  // Added JMM 13-AUG-2015: Multi-denomination
  public interface WCP_IMultiDenomInfo
  {
    void AddDenomination(out WCP_IMessage Request, WCP_IMessage ReceivedRequest, out int DenomIndex, int Id, long Value10000);
    void AddMeter(out WCP_IMessage Request, WCP_IMessage ReceivedRequest, int DenomIndex, int Id, long Value, long MaxValue);
    void AddGame(out WCP_IMessage Request,
                 WCP_IMessage ReceivedRequest,
                 out int GameIndex,
                 Int32 GameNumber,
                 Int64 MaxBet,
                 Int32 ProgressiveGroup,
                 Int32 ProgressiveLevels,
                 [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String GameName,
                 [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String PaytableName,
                 Int32 WagerCategories,
                 [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String GameId,
                 Int64 GameDenom,
                 Int64 GamePayOut);
    void AddGameDenom(out WCP_IMessage Request,
                      WCP_IMessage ReceivedRequest,
                      int GameIndex,
                      int Id,
                      long Value10000,
                      [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String PaytableName);
  }

  /// <summary>
  /// Interface for WCP Protocol. 
  /// </summary>
  public interface WCP_IProtocol
  {
    void IpConfig([Out, MarshalAs(UnmanagedType.BStr, SizeConst = 1000)] out String StrConfig);

    // Public Requests from Kiosk protocol

    // Basic operations
    void Start([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TerminalName, short TerminalType, IntPtr LoggerCallback);
    void Stop();

    void GetIsConnected([Out, MarshalAs(UnmanagedType.Bool)] out Boolean IsConnected);
    void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId);
    void GetTransactionId([Out, MarshalAs(UnmanagedType.I8)] out Int64 TransactionId);

    void WaitForMessage(out WCP_IMessage ReceivedMessage);
    void Send(WCP_IMessage MessageToSend);


    void NewRequest_StartCardSession(out WCP_IMessage Request,
                                     long SequenceId,
                                     long TransactionId,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin,
                                     ushort TransferMode,
                                     long TransferRedeemableCents,
                                     long TransferPromoRedeemableCents,
                                     long TransferPromoNotRedeemableCents,
                                     Boolean TransferWithBuckets);

    void NewRequest_CancelStartSession(out WCP_IMessage Request,
                                       long SequenceId,
                                       long TransactionId,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData,
                                       long CancelledRedeemableCents,
                                       long CancelledPromoRedeemableCents,
                                       long CancelledPromoNotRedeemableCents);

    void NewRequest_Play(out WCP_IMessage Request,
                         long SequenceId,
                         long TransactionId,
                         long CardSessionId,
                         long CardBalanceBeforePlay,
                         long CardBalanceAfterPlay,
                         ushort GameId,
                         [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                         long PlayedAmount,
                         long WonAmount,
                         long Denomination,
                         double PayoutPercentage);

    void NewRequest_EndSession(out WCP_IMessage Request,
                                   long SequenceId,
                                   long TransactionId,
                                   long CardSessionId,
                                   long Redeemable,
                                   long PromoRedeemable,
                                   long PromoNotRedeemable,
                                   bool HasCounters,
                                   long NumPlayed,
                                   long PlayedCents,
                                   long NumWon,
                                   long WonCents,
                                   long JackpotCents,
                                   long HandpaysAmountCents,
                                   bool OnlyAddBalances);


    void NewRequest_GetParameters(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetResources(out WCP_IMessage Request, long SequenceId);

    void NewRequest_PlayConditions(out WCP_IMessage Request,
                                   long SequenceId,
                                   long CardSessionId,
                                   long CardBalanceBeforePlay,
                                   ushort GameId,
                                   [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                   long PlayedAmount);

    void NewRequest_UpdatePlayerFields(out WCP_IMessage Request, long SequenceId);

    void GetReply_ReportPlay(WCP_IMessage ReceivedMessage, out long CardBalanceAfterPlay);
    void GetReply_StartCardSession(WCP_IMessage ReceivedMessage,
                                   out long CardSessionId,
                                   [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName,
                                   out long CardPoints,
                                   out long AllowCashIn,
                                   out long Redeemable,
                                   out long PromoRedeemable,
                                   out long PromoNotRedeemable,
                                   out long ReservedMode);

    void GetReply_EndSession(WCP_IMessage ReceivedMessage, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName, out long CardPoints, out long Redeemable, out long PromoRedeemable, out long PromoNotRedeemable);
    void GetReply_GetParameters(WCP_IMessage ReceivedMessage, out WCP_Parameters Parameters);
    void Init_WCP_PlayerFieldsValues(out WCP_PlayerFieldsValues PlayerData);
    void Init_Currencies(out WCP_ICurrency Currencies);
    void Init_PlayerInfo(out WCP_IPlayerInfo PlayerInfo);
    void Init_PlayerData(out WCP_IPlayerData PlayerData);
    void Init_PlayerFields(out WCP_IPlayerFields PlayerFields);
    void Init_PlayerGifts(out WCP_IPlayerGifts PlayerGifts);
    void Init_PlayerPromos(out WCP_IPlayerPromos PlayerPromos);
    void Init_PlayerRequestGift(out WCP_IPlayerRequestGift PlayerRequestGift);
    void Init_Advertisement(out WCP_IAdvertisement Advertisement);
    void Init_PlayerActivity(out WCP_IPlayerActivity Activity);
    void Init_PromoParams(out WCP_IPromoParams PromoParams);
    void Init_LCDParams(out WCP_ILCDParams LCDParams);
    void Init_PrintRechargesList(out WCP_IPrintRechargesList RechargesList);
    void Init_PlayerDraws(out WCP_IPlayerDraws DrawsList);
    void GetReply_PlayConditions(WCP_IMessage ReceivedMessage, out long CardBalanceBeforePlay);
    void GetReply_CardInformation(WCP_IMessage ReceivedMessage, out long CardBalance, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName, out long CardPoints, out DateTime BirthDate, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String Level, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String LastTerminalName, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 101)] out String NRTransferAmounts);
    void Init_PrintDrawNumbers(out WCP_IPrintDrawNumbers PrintDrawNumbersList);
    void Init_PlayerRequestPromo(out WCP_IPlayerRequestPromo PlayerRequestPromo);
    void Init_BonusInfo(out WCP_IBonusing BonusInfo);
    void Init_SwValidation(out WCP_ISwValidation SwValidation);
    void Init_MultiDenomInfo(out WCP_IMultiDenomInfo MultiDenomInfo);
    void Init_GameGateWayGetCredit(out WCP_GameGateWayGetCredit GameGateWayGetCredit);

    // Event List
    void NewRequest_ReportEventList(out WCP_IMessage Request, out WCP_IReportEventList ReportEventList, long SequenceId);
    void GetReply_ReportEventList(WCP_IMessage ReceivedMessage);

    // Get Card Type
    void NewRequest_GetCardType(out WCP_IMessage Request, long SequenceId,
                                [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);
    void GetReply_GetCardType(WCP_IMessage ReceivedMessage,
                              out WCP_ICardType CardType,
                              out long AccountId,
                              [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 81)] out String HolderName,
                              [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String MessageLine1,
                              [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String MessageLine2);

    // Get Card Information; response will obtained calling GetReply_CardInformation
    void NewRequest_GetCardInformation(out WCP_IMessage Request,
                                       long SequenceId,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);

    // Mobile Bank
    void NewRequest_MobileBankCardCheck(out WCP_IMessage Request,
                                        long SequenceId,
                                        [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String MobileBankCardTrackData,
                                        [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin);

    void NewRequest_MobileBankCardTransfer(out WCP_IMessage Request,
                                           long SequenceId,
                                           long TransactionId,
                                           [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String MobileBankCardTrackData,
                                           [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String Pin,
                                           [In, MarshalAs(UnmanagedType.BStr, SizeConst = 40)] String PlayerCardTrackData,
                                           long AmountToTransferx100);

    void GetReply_MobileBankCardCheck(WCP_IMessage ReceivedMessage,
                                      out long MobileBankAuthorizedAmountx100);

    void GetReply_MobileBankCardTransfer(WCP_IMessage ReceivedMessage,
                                         out long PlayerPreviousCardBalancex100,
                                         out long PlayerCurrentCardBalancex100);

    void GetReply_ReportGameMeters(WCP_IMessage ReceivedMessage);

    void NewRequest_ReportGameMeters(out WCP_IMessage Request,
                                     long SequenceId,
                                     ushort GameId,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameBaseName,
                                     long DenominationCents,
                                     long NumPlayed,
                                     long NumPlayedMaxValue,
                                     long PlayedCents,
                                     long PlayedMaxValueCents,
                                     long NumWon,
                                     long NumWonMaxValue,
                                     long WonCents,
                                     long WonMaxValueCents,
                                     long JackpotCents,
                                     long JackpotMaxValueCents,
                                     long ProgressiveJackpotCents,
                                     long ProgressiveJackpotMaxValueCents,
                                     bool IgnoreDeltas);

    void GetReply_ReportHPCMeter(WCP_IMessage ReceivedMessage);

    void NewRequest_ReportHPCMeter(out WCP_IMessage Request,
                                   long SequenceId,
                                   long HandPayCents,
                                   long HandPayMaxValueCents,
                                   bool IgnoreDeltas);

    void NewReply_GetLoggerReply(out WCP_IMessage Reply,
                                 long SequenceId,
                                 [In, MarshalAs(UnmanagedType.BStr, SizeConst = (64 * 1024))] String LoggerMessages);


    void GetRequest_ExecuteCommand(WCP_IMessage ReceivedMessage,
                                   out long CommandType,
                                   [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 500)] out String CommandParameters,
                                   [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String Msg_Line1,
                                   [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 100)] out String Msg_Line2,
                                   out long Msg_TimeShowMessage,
                                   out long Msg_NumTimes,
                                   out long Msg_TimeBetweenMessages);

    //FJC 12-NOV-2014
    void GetRequest_ExCmdSiteJackpotAwarded(WCP_IMessage ReceivedMessage,
                                            out WCP_SiteJackpotAwarded SiteJackpotAwarded);

    void NewReply_ExecuteCommandReply(out WCP_IMessage Reply,
                                      long SequenceId);

    void NewRequest_VersionSignature(out WCP_IMessage Resquest,
                                     long SequenceId,
                                     long ValidationID,
                                     long Method,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String Seed,
                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String Signature,
                                     long ReceivedStatus);

    void NewRequest_UpdateCardSession(out WCP_IMessage Request,
                                      long SequenceId,
                                      long CardSessionId,
                                      long NumPlayed,
                                      long PlayedCents,
                                      long NumWon,
                                      long WonCents,
                                      long JackpotCents);

    void GetReply_UpdateCardSession(WCP_IMessage ReceivedMessage, out long EstimatedPointsCents);

    //
    // play session
    void NewRequest_UpdatePlaySession(out WCP_IMessage Request,
                                      long SequenceId,
                                      long PlaySessionId,
                                      long Closed,
                                      long StartedDatetime,
                                      long FinishedDatetime,
                                      [In, MarshalAs(UnmanagedType.BStr, SizeConst = 32)] String TrackData1,
                                      [In, MarshalAs(UnmanagedType.BStr, SizeConst = 32)] String TrackData2);

    void UpdateRequest_CardSessionData(out WCP_IMessage ModifiedRequest,
                                       WCP_IMessage Request,
                                       long ToAccountOnCloseCashableCents,
                                       long ToAccountOnCloseRestrictedCents,
                                       long ToAccountOnCloseNonRestrictedCents,
                                       long OnStartSessionCashableCents,
                                       long OnStartSessionRestrictedCents,
                                       long OnStartSessionNonRestrictedCents,
                                       long OnEndSessionCashableCents,
                                       long OnEndSessionRestrictedCents,
                                       long OnEndSessionNonRestrictedCents);

    void UpdateRequest_PlaySessionMachineMeters(out WCP_IMessage ModifiedRequest,
                                                WCP_IMessage Request,
                                                long PlayedCents,
                                                long WonCents,
                                                long JackpotCents,
                                                long NumPlayed,
                                                long NumWon,
                                                long PlayedReedemable,
                                                long PlayedRestrictedCents,
                                                long PlayedNonRestrictedCents);

    void UpdateRequest_PlaySessionTITOMeters(out WCP_IMessage ModifiedRequest,
                                             WCP_IMessage Request,
                                             long TicketInCashableCents,
                                             long TicketInPromoNRCents,
                                             long TicketInPromoRECents,
                                             long TicketOutCashableCents,
                                             long TicketOutPromoNRCents,
                                             long BillCents,
                                             long CashInCoinsCents,
                                             long HandpayCents);

    void UpdateRequest_PlaySessionFTMeters(out WCP_IMessage ModifiedRequest,
                                           WCP_IMessage Request,
                                           long Type,
                                           long FTToGMCashableCents,
                                           long FTToGMRestrictedCents,
                                           long FTToGMNonRestrictedCents,
                                           long FTFromGMCashableCents,
                                           long FTFromGMRestrictedCents,
                                           long FTFromGMNonRestrictedCents);

    void GetReply_UpdatePlaySession(WCP_IMessage Message, out long EstimatedPointsCents);
    //

    void NewRequest_TITO_IsValidTicket(out WCP_IMessage Request,
                                       long SequenceId,
                                       long SystemId,
                                       long ValidationNumberSequence,
                                       long AmountCents);

    void GetReply_TITO_IsValidTicket(WCP_IMessage ReceivedMessage,
                                     out long AmountCents,
                                     out WCP_SystemTime ExpirationDate,
                                     out long PoolId,
                                     out long ValidateStatus,
                                     out long TicketId);

    void NewRequest_TITO_CancelTicket(out WCP_IMessage Request,
                                      long SequenceId,
                                      long SystemId,
                                      long ValidationNumberSequence,
                                      long StopCancellation,
                                      long TicketId,
                                      long MachineStatus,
                                      long ValidateStatus);

    void GetReply_TITO_CancelTicket(WCP_IMessage ReceivedMessage);

    void NewRequest_TITO_CreateTicket(out WCP_IMessage Request,
                                     long SequenceId,
                                     long TransactionId,
                                     long ValidationType,
                                     WCP_SystemTime Date,
                                     long ValidationNumberSequence,
                                     long MachineTicketNumber,
                                     long AmountCents,
                                     long SystemId,
                                     WCP_SystemTime ExpirationDate,
                                     long PoolId,
                                     long TicketType,
                                     long PlaySessionId);

    void GetReply_TITO_CreateTicket(WCP_IMessage ReceivedMessage);

    // EGM Parameters

    void NewRequest_TITO_EgmParameters(out WCP_IMessage Request,
                                       long SequenceId,
                                       long ValidationType,
                                       bool AllowCashableTicketOut,
                                       bool AllowPromotionalTicketOut,
                                       bool AllowTicketIn,
                                       long ValidationSequence,
                                       long MachineId,
                                       long Bonus,
                                       long Features,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String SASVersion,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String SASMachineName,
                                       [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String SASSerialNumber,
                                       long SasAssetNumber,
                                       long SasAcountDenomCents);

    void GetReply_TITO_EgmParameters(WCP_IMessage ReceivedMessage);

    //
    // Get Validation Number
    void NewRequest_TITO_GetValidationNumber(out WCP_IMessage Request,
                                             long SequenceId,
                                             long Type,
                                             Int64 Cents,
                                             long TransactionId);

    void GetReply_TITO_GetValidationNumber(WCP_IMessage ReceivedMessage,
                                           out long ValidationNumberSequence,
                                           out long SystemId,
                                           out long SequenceId,
                                           out long MachineId,
                                           out long TransactionId);
    //

    //
    // SAS meter process: one for message creation, second to add to list
    void NewRequest_SAS_Meters(out WCP_IMessage Request,
                               long SequenceId);

    void UpdateRequest_Add_SAS_Meter(out WCP_IMessage Request,
                                     WCP_IMessage ReceivedRequest,
                                     long Code,
                                     long GameId,
                                     long DenomCents,
                                     long Value,
                                     long MaxValue,
                                     bool IgnoreDeltas);

    void NewRequest_ReportMachineMeters(out WCP_IMessage Request,
                                        long SequenceId,
                                        bool PlayWonAvalaible,
                                        long PlayedCents,
                                        long PlayedMaxValueCents,
                                        long PlayedQuantity,
                                        long PlayedMaxValueQuantity,
                                        long WonCents,
                                        long WonMaxValueCents,
                                        long WonQuantity,
                                        long WonMaxValueQuantity,
                                        long JackpotCents,
                                        long JackpotMaxValueCents,
                                        long ProgressiveJackpotCents,
                                        long ProgressiveJackpotMaxValueCents,
                                        WCP_IFundTransferType FundTransferType,
                                        long ToGmCents,
                                        long ToGmMaxValueCents,
                                        long ToGmQuantity,
                                        long ToGmMaxValueQuantity,
                                        long FromGmCents,
                                        long FromGmMaxValueCents,
                                        long FromGmQuantity,
                                        long FromGmMaxValueQuantity,
                                        bool IgnoreDeltas,
                                        long AccountingDenomCents);

    void GetReply_ReportMachineMeters(WCP_IMessage ReceivedMessage);

    void NewRequest_AddCredit(out WCP_IMessage Request,
                             long SequenceId,
                             long TransactionId,
                             long CardSessionId,
                             long AmountToAdd,
                             long AcceptedTime,
                             long StackerId,
                             [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String Currency,
                             [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);

    void GetReply_AddCredit(WCP_IMessage ReceivedMessage, out long BalanceAfterAdd, out Boolean AccountAlreadyHasPromo, out Boolean BillInGivesPromo);

    void NewRequest_MoneyEvent(out WCP_IMessage Request,
                               long SequenceId,
                               int LktMoneyEventId,
                               long LocalDatetime,
                               int Source,
                               int Target,
                               long Amount,
                               int Status,
                               long TransactionId,
                               long CardSessionId,
                               long StackerId,
                               [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);

    void NewRequest_CashSessionPendingClosing(out WCP_IMessage Request,
                                              long SequenceId,
                                              long TransactionId,
                                              long StackerId,
                                              WCP_SystemTime ChangeDatetime,
                                              bool ReadMeters);
    void UpdateRequest_AddStackerMeter(out WCP_IMessage Request,
                                       WCP_IMessage ReceivedRequest,
                                       long Code,
                                       long GameId,
                                       long DenomCents,
                                       long Value,
                                       long MaxValue);
    void GetReply_SetCashSessionPendingClosing(WCP_IMessage ReceivedMessage, out long Result, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String AlreadyInsertedTerminalName);

    void NewRequest_EGM_Handpays(out WCP_IMessage Request,
                                 long SequenceId,
                                 Int64 TransactionID,
                                 long Datetime,
                                 Int64 HandpayType,
                                 Int64 HandpayCents,
                                 Int64 PlaySessionID,
                                 Int64 PreviousPlaySessionID,
                                 Byte ProgressiveGroup,
                                 Byte Level,
                                 Int64 PartialPayCents,
                                 Byte ResetID);

    void NewRequest_ReportBonus(out WCP_IMessage Request,
                                long SequenceId,
                                Int64 TransactionId,
                                Int32 Status,
                                Int64 CentsTransferredRe,
                                Int64 CentsTransferredPromoRe,
                                Int64 CentsTransferredPromoNr,
                                Int64 TransferredToPlaySessionId);

    void NewRequest_GetPlayerInfo(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetPlayerGifts(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetPlayerPromos(out WCP_IMessage Request, long SequenceId);
    void NewRequest_PlayerRequestGift(out WCP_IMessage Request, long SequenceId, Int32 CreditType);
    void GetReply_PlayerRequestGift(WCP_IMessage ReceivedMessage, out long CardPoints, out long Redeemable, out long PromoRedeemable, out long PromoNotRedeemable, out long ErrorPrinter);
    void NewRequest_GetAdvSteps(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetPlayerActivity(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetPromoParams(out WCP_IMessage Request, long SequenceId);
    void NewRequest_PrintRechargesList(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetPlayerDraws(out WCP_IMessage Request, long SequenceId);
    void NewRequest_RequestPlayerDrawNumbers(out WCP_IMessage Request, long SequenceId);
    void NewRequest_PlayerRequestPromo(out WCP_IMessage Request, long SequenceId, long CurrencyReward, Int32 CreditType);
    void GetReply_PlayerRequestPromo(WCP_IMessage ReceivedMessage, out long CardPoints, out long Redeemable, out long PromoRedeemable
                                    , out long PromoNotRedeemable, out long ErrorPrinter);
    void NewRequest_PlayerCheckPin(out WCP_IMessage Request, long SequenceId);
    void NewRequest_GetLCDParams(out WCP_IMessage Request, long SequenceId, Int32 TerminalId);
    void NewRequest_GetPromoBalance(out WCP_IMessage Request, long SequenceId, long AccountId, long GameGatewayPromoBalanceAction);
    // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
    void GetReply_GetPromoBalance(WCP_IMessage ReceivedMessage,
                                  out long CardPoints,
                                  out long Redeemable,
                                  out long PromoRedeemable,
                                  out long PromoNotRedeemable,
                                  out long Reserved,
                                  out long GameGatewayAwardedPrize);

    // TITO Get Validation Number
    void TITO_GetValidationNumber(long MachineId, long SequenceNumber, [Out, MarshalAs(UnmanagedType.I8)] out Int64 ValidationNumber, [Out, MarshalAs(UnmanagedType.Bool)] out Boolean IsValid);

    // LOCAL: Get Internal Track Data
    void Local_GetInternalCardType([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData, out WCP_ICardType CardType, out long InternalTrackData);

    // Get Smib protocol params
    void NewRequest_ProtocolParameters(out WCP_IMessage Request, long SequenceId, Int32 Protocol);
    void GetReply_SmibProtocol(WCP_IMessage Message, out Int32 Protocol);

    void GetReply_PcdPort(WCP_IMessage Message, Int32 PortType, Int32 PortNumber, out Int64 EgmNumber,
                                        out Double EgmNumberMultiplier, out Int32 PcdIoNumber, out Int32 PcdIoType,
                                        out UInt32 EdgeType, out UInt32 TimePulseDownMs, out UInt32 TimePulseUpMs);

    void GetReply_PcdPollingParams(WCP_IMessage Message, out UInt32 CashOutFinished,
                                                         out UInt32 MetersIdle,
                                                         out UInt32 MetersInSession,
                                                         out UInt32 MetersOnCashout);

    // MultiDenom Info
    void NewRequest_MachineMultiDenomInfo(out WCP_IMessage Request, long SequenceId);

    // Subs Machine Partial Credit (GameGateway)
    void NewRequest_SubsMachinePartialCredit(out WCP_IMessage Request,
                                             long SequenceId,
                                             long RequestAmount,
                                             long ObtainedAmount);

    void GetReply_SubsMachinePartialCredit(WCP_IMessage ReceivedMessage,
                                           out long RequestAmount,
                                           out long ObtainedAmount);


    void NewRequest_GamegatewayProcesssMsg(out WCP_IMessage Request,
                                           long SequenceId,
                                           long CmdId);

    void GetReply_ExcmdGameGateway(WCP_IMessage ReceivedMessage,
                                   out WCP_GameGateWaySession GameGateWaySession);

    // GameGateway (GetCredit (reserve, cancel reserve, etc.) - Execute Command)
    void NewRequest_ExcmdGamegatewayGetCredit(out WCP_IMessage Request,
                                              long SequenceId,
                                              WCP_GameGateWayGetCredit GameGateWayGetCredit);

    void GetReply_ExcmdGameGatewayGetCredit(WCP_IMessage ReceivedMessage,
                                            out WCP_GameGateWayGetCredit GameGateWayGetCredit);

    void GetReply_GameGatewayGetCredit(WCP_IMessage ReceivedMessage,
                                       out WCP_GameGateWayGetCredit GameGateWayGetCredit);

    // Update PCD Meters
    void GetNumPCDMeters(WCP_IMessage ReceivedMessage, out long NumPCDMeters);
    void GetPCDMetersItem(WCP_IMessage ReceivedMessage,
                          int IndexPCDItem,
                          out WCP_UpdatablePCDMeter UpdatePCDMeters);

    // TerminalDraw
    // TerminalDraw.GetPendingDraw Message
    void NewRequest_GetTerminalDraw(out WCP_IMessage Request,
                                    long SequenceId,
                                    Int64 AccountId,
                                    Int32 TerminalId);

    void GetPendingTerminalDrawRechargesByAccount(WCP_IMessage ReceivedMessage, out WCP_TerminalDrawsAccount RechargesGameAccount);

    // TerminalDraw.ProcessPendingDraw Message
    void Init_WCP_TerminalDrawsAccount(out WCP_TerminalDrawsAccount TerminalDrawsAccount);
    void NewRequest_ProcessTerminalDraw(out WCP_IMessage Request, long SequenceId, WCP_TerminalDrawsAccount ProcessDraw);
    void GetReply_ProcessTerminalDraw(WCP_IMessage ReceivedMessage, out WCP_TerminalDrawPrizePlan PrizePlan);
 
    void NewRequest_ReserveTerminal(out WCP_IMessage Request, long SequenceId, [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData);
    void GetReply_ReserveTerminal(WCP_IMessage ReceivedMessage, out Int32 ReserveError, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 200)] out String ReserveErrorMsg, out Int32 ExpirationMinutes);
    
    void NewRequest_TerminalDrawGameTimeElapsed(out WCP_IMessage Request, long SequenceId, Int64 PlaySessionId, Int64 GameTimeElapsed, Boolean ClosePlaySession);
    
    // FJC 27-11-2017: WIGOS-3592 MobiBank: InTouch - Recharge request 
    void NewRequest_MobiBankRecharge(out WCP_IMessage Request, long SequenceId, long AccountId);
    void GetReply_MobiBankRecharge(WCP_IMessage ReceivedMessage);
    void NewRequest_MobiBankCancelRequest(out WCP_IMessage Request, long SequenceId, long AccountId, int CancelReason);
    void GetReply_MobiBankCancelRequest(WCP_IMessage ReceivedMessage);
    void NewRequest_MobiBankNotifyRechargeEGM(out WCP_IMessage Request, long SequenceId, long CmdId, int Status, long RequestId);
    
    // Execute Commands MoviBank
    void GetReply_MobiBankRechargeNotification(WCP_IMessage ReceivedMessage, out WCP_MobiBankRechargeNotification MobiBankRechargeNotification);
    void GetReply_MobiBankRequestTransFer(WCP_IMessage ReceivedMessage, out WCP_MobiBankRequestTransFer MobiBankRequestTransFer);
    void GetReply_MobiBankCancelRequestFromApp(WCP_IMessage ReceivedMessage, out WCP_MobiBankCancelRequest MobiBankCancelRequestRechargeFromApp);

  } // WCP_IProtocol

  public class WCP_MessageEnvelope : WCP_IMessage
  {
    WCP_Message m_message;

    #region WCP_IMessage Members

    void WCP_IMessage.GetSequenceId(out long SequenceId)
    {
      SequenceId = Message.MsgHeader.SequenceId;
    }

    void WCP_IMessage.GetMessageType(out WCP_Msg MessageType)
    {
      MessageType = (WCP_Msg)((int)Message.MsgHeader.MsgType);
    }

    void WCP_IMessage.GetResponseCode(out WCP_RC ResponseCode)
    {
      ResponseCode = (WCP_RC)Message.MsgHeader.ResponseCode;
    }

    void WCP_IMessage.SetResponseCode(WCP_RC ResponseCode)
    {
      Message.MsgHeader.ResponseCode = (WSI.WCP.WCP_ResponseCodes)ResponseCode;
    }

    #endregion

    public static WCP_MessageEnvelope CreateEnvelope(WCP_Message Message)
    {
      WCP_MessageEnvelope msg;

      msg = new WCP_MessageEnvelope();

      msg.m_message = Message;

      return msg;
    }

    public WCP_Message Message
    {
      get { return m_message; }
    }
  }

  public class WCP_ReportEventList : WCP_IReportEventList
  {
    #region WCP_IReportEventList Members

    public void AddDeviceStatus(WCP_IMessage Request, long Code, long Status, long Priority, long Date)
    {
      WCP_Message wcp_request;
      WCP_MsgReportEventList wcp_content;
      WCP_DeviceStatus wcp_device_status;
      WCP_MessageEnvelope wcp_envelope;

      wcp_envelope = (WCP_MessageEnvelope)Request;
      wcp_request = wcp_envelope.Message;

      wcp_device_status = new WCP_DeviceStatus();

      wcp_device_status.Code = (DeviceCodes)Code;
      wcp_device_status.Status = (int)Status;
      wcp_device_status.Priority = (DevicePriority)Priority;
      wcp_device_status.LocalTime = DateTime.FromFileTime(Date);

      wcp_content = (WCP_MsgReportEventList)wcp_request.MsgContent;
      wcp_content.DeviceStatusList.Add(wcp_device_status);
      wcp_content.NumDeviceStatus++;
    }

    public void AddOperation(WCP_IMessage Request, long Code, long Data, long Date, long TechMode, long TechnicalAccountId)
    {
      WCP_Message wcp_request;
      WCP_MsgReportEventList wcp_content;
      WCP_Operation wcp_operation;
      WCP_MessageEnvelope wcp_envelope;

      wcp_envelope = (WCP_MessageEnvelope)Request;
      wcp_request = wcp_envelope.Message;

      wcp_operation = new WCP_Operation();

      wcp_operation.Code = (OperationCodes)Code;
      wcp_operation.Data = (UInt32)Data;
      wcp_operation.LocalTime = DateTime.FromFileTime(Date);
      wcp_operation.TechnicalAccountId = (Int64)TechnicalAccountId;
      wcp_operation.TechMode = (TechModeEnter)TechMode;

      wcp_content = (WCP_MsgReportEventList)wcp_request.MsgContent;
      wcp_content.OperationList.Add(wcp_operation);
      wcp_content.NumOperations++;
    }

    #endregion

    internal static WCP_IReportEventList CreateEventList()
    {
      WCP_ReportEventList ReportEventList = new WCP_ReportEventList();

      return ReportEventList;
    }
  }

  public interface WSI_IPrinter
  {
    void Printer_Init();
    void Printer_GetStatus(out Int32 Status);
    void Printer_Print([In, MarshalAs(UnmanagedType.BStr, SizeConst = 4096)] String Html);
  }

  public class WSI_Printer_Class : WSI_IPrinter
  {
    void WSI_IPrinter.Printer_Init()
    {
      // TEMP: Logger
      //Log.AddListener(new SimpleLogger("WSI_Printer_Logger"));

      HtmlPrinter.Init();
    }

    void WSI_IPrinter.Printer_GetStatus(out Int32 Status)
    {
      Int32 _dummy;

      Printer.GetStatus(out Status, out _dummy);
    }

    void WSI_IPrinter.Printer_Print([In, MarshalAs(UnmanagedType.BStr, SizeConst = 4096)] String Html)
    {
      HtmlPrinter.AddHtml(Html);
    }
  }

  public interface WSI_IGeneralParams
  {
    void GeneralParams_Init();
    void GetString(String Groupkey, String SubjectKey, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 256)] out String Value, String Default);
    void GetInt32(String Groupkey, String SubjectKey, out Int32 Value, Int32 Default);
    void GetInt64(String Groupkey, String SubjectKey, out Int64 Value, Int64 Default);
    void GetBoolean(String Groupkey, String SubjectKey, out Boolean Value, Boolean Default);
  }

  public class WSI_GeneralParams_Class : WSI_IGeneralParams
  {
    void WSI_IGeneralParams.GeneralParams_Init()
    {
    }

    void WSI_IGeneralParams.GetString(String Groupkey, String SubjectKey, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 256)] out String Value, String Default)
    {
      Value = GeneralParam.GetString(Groupkey, SubjectKey, Default);
    }

    void WSI_IGeneralParams.GetInt32(String Groupkey, String SubjectKey, out Int32 Value, Int32 Default)
    {
      Value = GeneralParam.GetInt32(Groupkey, SubjectKey, Default);
    }


    void WSI_IGeneralParams.GetInt64(String Groupkey, String SubjectKey, out Int64 Value, Int64 Default)
    {
      Value = GeneralParam.GetInt64(Groupkey, SubjectKey, Default);
    }


    void WSI_IGeneralParams.GetBoolean(String Groupkey, String SubjectKey, out Boolean Value, Boolean Default)
    {
      Value = GeneralParam.GetBoolean(Groupkey, SubjectKey, Default);
    }

  }

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.Init_WCP_PlayerFieldsValues(out WCP_PlayerFieldsValues PlayerFields)
    {
      PlayerFields = new WCP_PlayerFieldsValues();
    }

    void WCP_IProtocol.Init_Currencies(out WCP_ICurrency Currencies)
    {
      Currencies = new WCP_Currency_Class();
    }

    void WCP_IProtocol.Init_PlayerInfo(out WCP_IPlayerInfo PlayerInfo)
    {
      PlayerInfo = new WCP_PlayerInfo_Class();
    }

    void WCP_IProtocol.Init_PlayerData(out WCP_IPlayerData PlayerData)
    {
      PlayerData = new WCP_PlayerData_Class();
    }

    void WCP_IProtocol.Init_PlayerGifts(out WCP_IPlayerGifts PlayerGifts)
    {
      PlayerGifts = new WCP_PlayerGifts_Class();
    }

    void WCP_IProtocol.Init_PlayerRequestGift(out WCP_IPlayerRequestGift PlayerRequestGift)
    {
      PlayerRequestGift = new WCP_PlayerRequestGift_Class();
    }

    void WCP_IProtocol.Init_PlayerPromos(out WCP_IPlayerPromos PlayerPromos)
    {
      PlayerPromos = new WCP_PlayerPromos_Class();
    }

    void WCP_IProtocol.Init_PlayerFields(out WCP_IPlayerFields PlayerFields)
    {
      PlayerFields = new WCP_PlayerFields_class();
    }

    void WCP_IProtocol.Init_Advertisement(out WCP_IAdvertisement Advertisement)
    {
      Advertisement = new WCP_Advertisement();
    }

    void WCP_IProtocol.Init_PlayerActivity(out WCP_IPlayerActivity Activity)
    {
      Activity = new WCP_PlayerActivity_Class();
    }

    void WCP_IProtocol.Init_PromoParams(out WCP_IPromoParams PromoParams)
    {
      PromoParams = new WCP_PromoParams_Class();
    }

    void WCP_IProtocol.Init_LCDParams(out WCP_ILCDParams LCDParams)
    {
      LCDParams = new WCP_LCDParams_Class();
    }

    void WCP_IProtocol.Init_PrintRechargesList(out WCP_IPrintRechargesList RechargesList)
    {
      RechargesList = new WCP_PrintRechargesList_Class();
    }

    void WCP_IProtocol.Init_PlayerDraws(out WCP_IPlayerDraws DrawsList)
    {
      DrawsList = new WCP_PlayerDraws_Class();
    }

    void WCP_IProtocol.Init_PrintDrawNumbers(out WCP_IPrintDrawNumbers PrintDrawNumbersList)
    {
      PrintDrawNumbersList = new WCP_PrintDrawNumbers_Class();
    }

    void WCP_IProtocol.Init_PlayerRequestPromo(out WCP_IPlayerRequestPromo PlayerRequestPromo)
    {
      PlayerRequestPromo = new WCP_PlayerRequestPromo_Class();
    }

    void WCP_IProtocol.Init_BonusInfo(out WCP_IBonusing BonusInfo)
    {
      BonusInfo = new WCP_Bonusing_Class();
    }

    void WCP_IProtocol.Init_SwValidation(out WCP_ISwValidation SwValidation)
    {
      SwValidation = new WCP_SoftwareValidation_Class();
    }

    void WCP_IProtocol.Init_MultiDenomInfo(out WCP_IMultiDenomInfo MultiDenomInfo)
    {
      MultiDenomInfo = new WCP_MultiDenomInfo_Class();
    }

    void WCP_IProtocol.Init_GameGateWayGetCredit(out WCP_GameGateWayGetCredit GameGateWayGetCredit)
    {
      GameGateWayGetCredit = new WCP_GameGateWayGetCredit();
      GameGateWayGetCredit.MessageText = string.Empty;
    }

    void WCP_IProtocol.Init_WCP_TerminalDrawsAccount(out WCP_TerminalDrawsAccount TerminalDrawsAccount)
    {
      TerminalDrawsAccount = new WCP_TerminalDrawsAccount();
    }

  } // WCP_Protocol_Class
}

