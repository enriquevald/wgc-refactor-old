//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_SasMeters.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for meters process.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2013 NMR    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.WCP.WCP_Messages.Meters;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for SAS meters report message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgSasMeters
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_SAS_Meters(out WCP_IMessage Request,
                                             long SequenceId)
    {
      WCP_Message WCP_request;
      //WCP_MsgSasMeters WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgSasMeters);
      //WCP_content = (WCP_MsgSasMeters)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_SAS_Meters

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgSasMeters to add another meter in the list
    // 
    //  PARAMS :
    //      - INPUT :
    //        - WCP_MessageEnvelope: WCP_TITO_MsgSasMeters (initial request)
    //        - Code: meter code
    //        - GameId
    //        - DenomCents
    //        - Value: reported value for meter
    //        - MaxValue: max meter value for rollover
    //
    //      - OUTPUT:
    //        - WCP_MessageEnvelope: WCP_TITO_MsgSasMeters, request with new meter in the list
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_Add_SAS_Meter(out WCP_IMessage Request,
                                                   WCP_IMessage ReceivedRequest,
                                                   long Code,
                                                   long GameId,
                                                   long DenomCents,
                                                   long Value,
                                                   long MaxValue,
                                                   bool IgnoreDeltas)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgSasMeters WCP_content;
      SAS_Meter _meter;

      msg = (WCP_MessageEnvelope)ReceivedRequest;
      WCP_content = (WCP_MsgSasMeters)msg.Message.MsgContent;

      _meter = new SAS_Meter();
      _meter.Code = (Int32)Code;
      _meter.GameId = (Int32)GameId;
      _meter.DenomCents = (Decimal)DenomCents;
      _meter.Value = Value;
      _meter.MaxValue = MaxValue;
      _meter.IgnoreDeltas = IgnoreDeltas;

      WCP_content.Meters.Add(_meter);

      Request = ReceivedRequest;
    } // UpdateRequest_Add_SAS_Meter
  }
}
