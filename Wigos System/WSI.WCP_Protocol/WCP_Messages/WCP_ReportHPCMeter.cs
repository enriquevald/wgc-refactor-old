//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportHPCMeter.cs
// 
//   DESCRIPTION: WC2 Protocol Dll - Shared between Wigos Server and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Xavier Ib��ez
// 
// CREATION DATE: 20-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUL-2010 XID    First release.
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_ReportHPCMeter (out WCP_IMessage Request,
                                                  long SequenceId,
                                                  long HandPaysCents,
                                                  long HandPaysMaxValueCents,
                                                  bool IgnoreDeltas)
    {
      WCP_Message WCP_request;
      WCP_MsgReportHPCMeter WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportHPCMeter);
      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content = (WCP_MsgReportHPCMeter)WCP_request.MsgContent;

      WCP_content.HandPaysCents = HandPaysCents;
      WCP_content.HandPaysMaxValueCents = HandPaysMaxValueCents;
      WCP_content.IgnoreDeltas = IgnoreDeltas;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_ReportHPCMeter


    void WCP_IProtocol.GetReply_ReportHPCMeter(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_MsgReportHPCMeterReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_MsgReportHPCMeterReply)msg.Message.MsgContent;

    } // GetReply_ReportHPCMeter

  }
}
