//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Bonusing.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 22-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-JAN-2014 JMM    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;
using System.Xml;
using System.IO;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for EMG Handpays report message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgBonusTransferStatus
    //
    // RETURNS :
    // 
    //   NOTES :
    public void NewRequest_ReportBonus(out WCP_IMessage Request,
                                       long SequenceId,
                                       Int64 TransactionId,
                                       Int32 Status,
                                       Int64 CentsTransferredRe,
                                       Int64 CentsTransferredPromoRe,
                                       Int64 CentsTransferredPromoNr,
                                       Int64 TransferredToPlaySessionId)
    {
      WCP_Message WCP_request;
      WCP_MsgBonusTransferStatus WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgBonusTransferStatus);
      WCP_content = (WCP_MsgBonusTransferStatus)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content.TransactionId = TransactionId;
      WCP_content.Status = Status;
      WCP_content.CentsTransferredRe = CentsTransferredRe;
      WCP_content.CentsTransferredPromoRe = CentsTransferredPromoRe;
      WCP_content.CentsTransferredPromoNr = CentsTransferredPromoNr;
      WCP_content.TransferredToPlaySessionId = TransferredToPlaySessionId;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_ReportBonus
  }

  public partial class WCP_Bonusing_Class : WCP_IBonusing
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Get Bonus Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage    
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IBonusing.GetBonusInfo(WCP_IMessage ReceivedMessage,
                                    out Int64 TransactionID,
                                    out Int64 CentsToTransferRE,
                                    out Int64 CentsToTransferPromoRE,
                                    out Int64 CentsToTransferPromoNR,
                                    out Int32 Timeout,
                                    out Int64 MinBetCents,
                                    out Boolean NotRedimibleMustBeZero,
                                    out int SasTaxStatus,
                                    out Int32 TransferFlags)
    {
      WCP_CmdBonusAward _message_content;
      _message_content = ProvideBonusInfo(ReceivedMessage);

      TransactionID = _message_content.TransactionId;
      CentsToTransferRE = _message_content.CentsToTransferRe;
      CentsToTransferPromoRE = _message_content.CentsToTransferPromoRe;
      CentsToTransferPromoNR = _message_content.CentsToTransferPromoNr;
      Timeout = _message_content.Timeout;
      MinBetCents = _message_content.MinBetCents;
      NotRedimibleMustBeZero = _message_content.NotRedimibleMustBeZero;
      SasTaxStatus = _message_content.SasTaxStatus;
      TransferFlags = _message_content.TransferFlags;
    } //GetBonusInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Get Notification Bonus Information
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage    
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_IBonusing.GetNotificationBonusInfo(WCP_IMessage ReceivedMessage,
                                                out Int64 TransactionID)
    {
      WCP_CmdBonusStatus _message_content;
      _message_content = ProvideNotificationBonusInfo(ReceivedMessage);

      TransactionID = _message_content.TransactionId;
    } //GetNotificationBonusInfo

    //------------------------------------------------------------------------------
    // PURPOSE: Provide WCP_IBonusing from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //
    //
    //
    public static WCP_CmdBonusAward ProvideBonusInfo(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _message_content;
      WCP_CmdBonusAward _msg_bonus_award;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      _msg_bonus_award = new WCP_CmdBonusAward();
      _msg_bonus_award.LoadXml(XmlReader.Create(new StringReader(_message_content.CommandParameters)));      

      return _msg_bonus_award;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Provide WCP_IBonusing from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //
    //
    //
    public static WCP_CmdBonusStatus ProvideNotificationBonusInfo(WCP_IMessage ReceivedMessage)
    {
      WSI.WCP.WCP_Protocol.WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _message_content;
      WCP_CmdBonusStatus _msg_bonus_status;

      _msg = (WSI.WCP.WCP_Protocol.WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      _msg_bonus_status = new WCP_CmdBonusStatus();
      _msg_bonus_status.LoadXml(XmlReader.Create(new StringReader(_message_content.CommandParameters)));

      return _msg_bonus_status;
    }

  } // WCP_Bonusing_Class
}
