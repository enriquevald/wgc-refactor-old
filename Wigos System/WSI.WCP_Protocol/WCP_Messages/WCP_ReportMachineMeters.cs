//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportMachineMeters.cs
// 
//   DESCRIPTION: WC2 Protocol Dll - Shared between Wigos Bingo Server and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 16-NOV-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-NOV-2011 ACC    First release.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_ReportMachineMeters(out WCP_IMessage Request,
                                                      long SequenceId,
                                                      bool PlayWonAvalaible,
                                                      long PlayedCents,
                                                      long PlayedMaxValueCents,
                                                      long PlayedQuantity,
                                                      long PlayedMaxValueQuantity,
                                                      long WonCents,
                                                      long WonMaxValueCents,
                                                      long WonQuantity,
                                                      long WonMaxValueQuantity,
                                                      long JackpotCents,
                                                      long JackpotMaxValueCents,
                                                      long ProgressiveJackpotCents,
                                                      long ProgressiveJackpotMaxValueCents,
                                                      WCP_IFundTransferType FundTransferType,
                                                      long ToGmCents,
                                                      long ToGmMaxValueCents,
                                                      long ToGmQuantity,
                                                      long ToGmMaxValueQuantity,
                                                      long FromGmCents,
                                                      long FromGmMaxValueCents,
                                                      long FromGmQuantity,
                                                      long FromGmMaxValueQuantity,
                                                      bool IgnoreDeltas,
                                                      long AccountingDenomCents)
    {
      WCP_Message WCP_request;
      WCP_MsgReportMachineMeters WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportMachineMeters);
      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content = (WCP_MsgReportMachineMeters)WCP_request.MsgContent;

      WCP_content.PlayWonAvalaible = PlayWonAvalaible;

      WCP_content.PlayedCents = PlayedCents;
      WCP_content.PlayedMaxValueCents = PlayedMaxValueCents;
      WCP_content.PlayedQuantity = PlayedQuantity;
      WCP_content.PlayedMaxValueQuantity = PlayedMaxValueQuantity;

      WCP_content.WonCents = WonCents;
      WCP_content.WonMaxValueCents = WonMaxValueCents;
      WCP_content.WonQuantity = WonQuantity;
      WCP_content.WonMaxValueQuantity = WonMaxValueQuantity;

      // ACC 15-JUN-2012 Add Jackpot Cents into Machine Meters
      WCP_content.JackpotCents = JackpotCents;
      WCP_content.JackpotMaxValueCents = JackpotMaxValueCents;

      // ACC 07-AUG-2014 Add Progressive Jackpot Cents into Machine Meters
      WCP_content.ProgressiveJackpotCents = ProgressiveJackpotCents;
      WCP_content.ProgressiveJackpotMaxValueCents = ProgressiveJackpotMaxValueCents;

      switch (FundTransferType)
      {
        case WCP_IFundTransferType.AFT:
          WCP_content.FundTransferType = WCP_FundTransferType.FUND_TRANSFER_AFT;
        break;

        case WCP_IFundTransferType.EFT:
          WCP_content.FundTransferType = WCP_FundTransferType.FUND_TRANSFER_EFT;
        break;

        default:
          WCP_content.FundTransferType = WCP_FundTransferType.FUND_TRANSFER_UNKNOWN;
        break;
      }

      WCP_content.ToGmCents = ToGmCents;
      WCP_content.ToGmMaxValueCents = ToGmMaxValueCents;
      WCP_content.ToGmQuantity = ToGmQuantity;
      WCP_content.ToGmMaxValueQuantity = ToGmMaxValueQuantity;

      WCP_content.FromGmCents = FromGmCents;
      WCP_content.FromGmMaxValueCents = FromGmMaxValueCents;
      WCP_content.FromGmQuantity = FromGmQuantity;
      WCP_content.FromGmMaxValueQuantity = FromGmMaxValueQuantity;

      WCP_content.IgnoreDeltas = IgnoreDeltas;
      WCP_content.AccountingDenomCents = AccountingDenomCents; 

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_ReportMachineMeters


    void WCP_IProtocol.GetReply_ReportMachineMeters(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_MsgReportMachineMetersReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_MsgReportMachineMetersReply)msg.Message.MsgContent;

    } // GetReply_ReportMachineMeters

  }
}
