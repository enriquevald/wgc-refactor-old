//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportMachineMultiDenomInfo.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 18-JUN-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-JUN-2015 JMM    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;
using System.Xml;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for Multi-denomination info report message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgMachineMultiDenomInfo
    //
    // RETURNS :
    // 
    //   NOTES :
    public void NewRequest_MachineMultiDenomInfo(out WCP_IMessage Request, long SequenceId)
    {
      WCP_Message _wcp_request;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgMachineMultiDenomInfo);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);

    } // NewRequest_MachineMultiDenomInfo
  }

  public partial class WCP_MultiDenomInfo_Class : WCP_IMultiDenomInfo
  {
    public void AddDenomination(out WCP_IMessage Request, WCP_IMessage ReceivedRequest, out int DenomIndex, int Id, long Value10000)
    {
      WCP_MessageEnvelope _msg_envelope;
      WCP_MsgMachineMultiDenomInfo _wcp_content;
      WCP_MsgMachineMultiDenomInfo.Denomination _denom;

      _msg_envelope = (WCP_MessageEnvelope)ReceivedRequest;
      _wcp_content = (WCP_MsgMachineMultiDenomInfo)_msg_envelope.Message.MsgContent;

      _denom = new WCP_MsgMachineMultiDenomInfo.Denomination();

      _denom.Id = Id;
      _denom.Value10000 = Value10000;

      if (_wcp_content.DenominationList == null)
      {
        _wcp_content.DenominationList = new ArrayList();
      }

      DenomIndex = _wcp_content.DenominationList.Add(_denom);

      Request = ReceivedRequest;

    } // AddDenomination

    public void AddMeter(out WCP_IMessage Request,
                         WCP_IMessage ReceivedRequest,
                         int DenomIndex,
                         int Id,
                         long Value,
                         long MaxValue)
    {
      WCP_MessageEnvelope _msg_envelope;
      WCP_MsgMachineMultiDenomInfo _wcp_content;
      WCP_MsgMachineMultiDenomInfo.Denomination _denom;
      SAS_Meter _meter;

      _msg_envelope = (WCP_MessageEnvelope)ReceivedRequest;
      _wcp_content = (WCP_MsgMachineMultiDenomInfo)_msg_envelope.Message.MsgContent;

      if (_wcp_content.DenominationList != null)
      {
        _denom = (WCP_MsgMachineMultiDenomInfo.Denomination)_wcp_content.DenominationList[DenomIndex];

        if (_denom != null)
        {
          _meter = new SAS_Meter();
          _meter.Code = Id;
          _meter.Value = Value;
          _meter.MaxValue = MaxValue;

          if (_denom.MeterList == null)
          {
            _denom.MeterList = new List<SAS_Meter>();
          }

          _denom.MeterList.Add(_meter);
        }
      }
      else
      {
        // TODO JMM 17-AUG-2015: Error to the logger
      }

      Request = ReceivedRequest;
    } // AddMeter

    public void AddGame(out WCP_IMessage Request, 
                        WCP_IMessage ReceivedRequest, 
                        out int GameIndex, 
                        Int32 GameNumber, 
                        Int64 MaxBet,
                        Int32 ProgressiveGroup,
                        Int32 ProgressiveLevels,
                        [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String GameName,
                        [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String PaytableName,
                        Int32 WagerCategories,
                        [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String GameId,
                        Int64 GameDenom,
                        Int64 GamePayOut)
    {
      WCP_MessageEnvelope _msg_envelope;
      WCP_MsgMachineMultiDenomInfo _wcp_content;
      WCP_MsgMachineMultiDenomInfo.Game _game;

      _msg_envelope = (WCP_MessageEnvelope)ReceivedRequest;
      _wcp_content = (WCP_MsgMachineMultiDenomInfo)_msg_envelope.Message.MsgContent;

      if (_wcp_content.GameList == null)
      {
        _wcp_content.GameList = new ArrayList();
      }

      _game = new WCP_MsgMachineMultiDenomInfo.Game();

      _game.GameNumber = GameNumber;
      _game.MaxBet = MaxBet;
      _game.ProgressiveGroup = ProgressiveGroup;
      _game.ProgressiveLevels = ProgressiveLevels;
      _game.GameName = GameName;
      _game.PaytableName = PaytableName;
      _game.WagerCategories = WagerCategories;
      _game.GameId = GameId;
      _game.GameDenom = GameDenom;
      _game.GamePayOut = GamePayOut;

      GameIndex = _wcp_content.GameList.Add(_game);

      Request = ReceivedRequest;

    } // AddGame

    public void AddGameDenom(out WCP_IMessage Request, WCP_IMessage ReceivedRequest, int GameIndex, int Id, long Value10000, [In, MarshalAs(UnmanagedType.BStr, SizeConst = 20)] String PaytableName)
    {
      WCP_MessageEnvelope _msg_envelope;
      WCP_MsgMachineMultiDenomInfo _wcp_content;
      WCP_MsgMachineMultiDenomInfo.Game _game;
      WCP_MsgMachineMultiDenomInfo.Denomination _denom;      

      _msg_envelope = (WCP_MessageEnvelope)ReceivedRequest;
      _wcp_content = (WCP_MsgMachineMultiDenomInfo)_msg_envelope.Message.MsgContent;

      if (_wcp_content.GameList != null)
      {
        _game = (WCP_MsgMachineMultiDenomInfo.Game)_wcp_content.GameList[GameIndex];

        if (_game != null)
        {
          _denom = new WCP_MsgMachineMultiDenomInfo.Denomination();

          _denom.Id = Id;
          _denom.Value10000 = Value10000;
          _denom.PaytableName = PaytableName;

          if (_game.DenominationList == null)
          {
            _game.DenominationList = new ArrayList();
          }

          _game.DenominationList.Add(_denom);
        }
      }

      Request = ReceivedRequest;
    } // AddGameDenom
  }
}