﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Terminaldraw.cs
// 
//   DESCRIPTION: This Message, Gets Pending TerminalDraws for one Account
// 
//        AUTHOR: FJC
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First version.
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 16-FEB-2018 FJC    WIGOS-8082 [Ticket #12237] Sorteo en Terminal
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_GetTerminalDraw(out WCP_IMessage Request, long SequenceId, Int64 AccountId, Int32 TerminalId)
    {
      WCP_Message _wcp_request;
      WCP_MsgTerminalDrawGetPendingDraw _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgTerminalDrawGetPendingDraw);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgTerminalDrawGetPendingDraw)_wcp_request.MsgContent;
      _wcp_content.AccountId = AccountId;
      _wcp_content.TerminalId = TerminalId;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

    void WCP_IProtocol.GetPendingTerminalDrawRechargesByAccount(WCP_IMessage ReceivedMessage, out WCP_TerminalDrawsAccount RechargesGameAccount)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgTerminalDrawGetPendingDrawReply _message_content;
      Int32 _idx_game;
      Int32 _idx_recharge;

      _idx_game = 0;
      _idx_recharge = 0;
      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_MsgTerminalDrawGetPendingDrawReply)_msg.Message.MsgContent;

      RechargesGameAccount = new WCP_TerminalDrawsAccount(0);
      RechargesGameAccount.AccountId = _message_content.PendingDraw.AccountId;
      RechargesGameAccount.NumGames = _message_content.PendingDraw.Games.Count;

      if (RechargesGameAccount.NumGames > 0)
      {
        foreach (WSI.Common.TerminalDraw.Persistence.TerminalDrawGamesEntity _tdr in _message_content.PendingDraw.Games)
        {
          RechargesGameAccount.Games[_idx_game] = new WCP_TerminalDrawsGame(0);
          RechargesGameAccount.Games[_idx_game].url                             = _tdr.GameUrl;

          // First Screen
          RechargesGameAccount.Games[_idx_game].FirstScreenMessageLine0         = _tdr.FirstScreenMessageLine0;
          RechargesGameAccount.Games[_idx_game].FirstScreenMessageLine1         = _tdr.FirstScreenMessageLine1;
          RechargesGameAccount.Games[_idx_game].FirstScreenMessageLine2         = _tdr.FirstScreenMessageLine2;
          RechargesGameAccount.Games[_idx_game].FirstScreenTimeOut              = _tdr.FirstScreenTimeOut;
          RechargesGameAccount.Games[_idx_game].TimeOutExpiresParticipateInDraw = 0;
          if (_tdr.TimeoutExpiresParticipateInDraw)
          {
            RechargesGameAccount.Games[_idx_game].TimeOutExpiresParticipateInDraw = 1;
          }

          RechargesGameAccount.Games[_idx_game].ForceParticipateInDraw = 0;
          if (_tdr.ForceParticipateInDraw)
          {
            RechargesGameAccount.Games[_idx_game].ForceParticipateInDraw = 1;
          }

          // Second Screen
          RechargesGameAccount.Games[_idx_game].SecondScreenMessageLine0 = _tdr.SecondScreenMessageLine0;
          RechargesGameAccount.Games[_idx_game].SecondScreenMessageLine1 = _tdr.SecondScreenMessageLine1;
          RechargesGameAccount.Games[_idx_game].SecondScreenMessageLine2 = _tdr.SecondScreenMessageLine2;
          RechargesGameAccount.Games[_idx_game].SecondScreenMessageLine3 = _tdr.SecondScreenMessageLine3;
          RechargesGameAccount.Games[_idx_game].SecondScreenTimeOut = _tdr.SecondScreenTimeOut;
          RechargesGameAccount.Games[_idx_game].ForceExecuteDraw = (_tdr.ForceExecuteDraw ? 1: 0);
          
          // Play Session Id
          RechargesGameAccount.Games[_idx_game].PlaySessionId = _tdr.PlaySessionId;

          // Total Bet Amount
          RechargesGameAccount.Games[_idx_game].TotalBetAmount = (Int64)_tdr.TotalBetAmount;

          // Balance Amount
          RechargesGameAccount.Games[_idx_game].TotalBalanceAmount= (Int64)_tdr.BalanceAmount;

          // Num Recharges
          RechargesGameAccount.Games[_idx_game].NumRecharges = _message_content.PendingDraw.Games[_idx_game].Recharges.Count;

          // Recharges (detail)
          foreach (WSI.Common.TerminalDraw.Persistence.TerminalDrawRechargeEntity _tdrr in _message_content.PendingDraw.Games[_idx_game].Recharges)
          {
            RechargesGameAccount.Games[_idx_game].Recharges[_idx_recharge].operation_id = (Int64)_tdrr.OperationId;
            RechargesGameAccount.Games[_idx_game].Recharges[_idx_recharge].amount_total_cash_in = (Int64)_tdrr.AmountTotalCashIn;
            RechargesGameAccount.Games[_idx_game].Recharges[_idx_recharge].amount_re_bet = (Int64)_tdrr.AmountReBet;
            RechargesGameAccount.Games[_idx_game].Recharges[_idx_recharge].game_id = (Int64)_tdrr.GameId;
            RechargesGameAccount.Games[_idx_game].Recharges[_idx_recharge].game_type = (Int64)_tdrr.GameType;

            _idx_recharge++;
          }
          _idx_game++;
        }
      }
    }
  }
}