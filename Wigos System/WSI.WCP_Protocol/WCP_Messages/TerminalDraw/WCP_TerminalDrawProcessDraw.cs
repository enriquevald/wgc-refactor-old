﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Terminaldraw.cs
// 
//   DESCRIPTION: --
// 
//        AUTHOR: FJC
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First version.
// 30-MAY-2017 FJC    PBI 27761:Sorteo de máquina. Modificiones en la visualización de las pantallas (LCD y InTouch)). 
// 22-AUG-2017 FJC    Fixed Defect: WIGOS-4649 Terminal Draw: The Draw does not appear in the terminal and continue with start card session.
// 29-AUG-2017 FJC    WIGOS-4771 Terminal Draw. Include GameId for each recharge
// 04-SEP-2017 FJC    PBI 29532:Terminal Draw. Apply retrocompatibility (18.315 & 18.319)
// 18-OCT-2017 FJC    WIGOS-5928 Terminal draw does not work neither in LCD nor 4 lines
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_ProcessTerminalDraw(out WCP_IMessage Request, long SequenceId, WCP_TerminalDrawsAccount ProcessDraw)
    {
      WCP_Message _wcp_request;
      WCP_MsgTerminalDrawProcessDraw _wcp_content;
      Int32 _idx;
      Int32 _adx;
      WSI.Common.TerminalDraw.Persistence.RechargeData _recharge_data;

      _recharge_data = new WSI.Common.TerminalDraw.Persistence.RechargeData();

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgTerminalDrawProcessDraw);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgTerminalDrawProcessDraw)_wcp_request.MsgContent;

      for (_idx = 0; _idx < ProcessDraw.NumGames; _idx++)
      {
        for (_adx = 0; _adx < ProcessDraw.Games[_idx].NumRecharges; _adx++)
        {
          _recharge_data.AccountId = ProcessDraw.AccountId;
          _recharge_data.TerminalId = ProcessDraw.TerminalId;
          _recharge_data.OperationId = ProcessDraw.Games[_idx].Recharges[_adx].operation_id;
          _recharge_data.PlaySessionId = ProcessDraw.Games[_idx].PlaySessionId;
          _recharge_data.AmountTotalCashIn = ProcessDraw.Games[_idx].Recharges[_adx].amount_total_cash_in;
          _recharge_data.AmountReBet = ProcessDraw.Games[_idx].Recharges[_adx].amount_re_bet;
          _recharge_data.AmountNrBet = ProcessDraw.Games[_idx].Recharges[_adx].amount_nr_bet;
          _recharge_data.AmountPointsBet = ProcessDraw.Games[_idx].Recharges[_adx].amount_points_bet;
          _recharge_data.GameId = ProcessDraw.Games[_idx].Recharges[_adx].game_id;
          _recharge_data.GameType = ProcessDraw.Games[_idx].Recharges[_adx].game_type;

          _wcp_content.ProcessPendingDraw.AddRecharge(_recharge_data);
        }
      }

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }

    void WCP_IProtocol.GetReply_ProcessTerminalDraw(WCP_IMessage ReceivedMessage,
                                                    out WCP_TerminalDrawPrizePlan PrizePlan)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgTerminalDrawProcessDrawReply _message_content;
      Int64 _idx_game;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_MsgTerminalDrawProcessDrawReply)_msg.Message.MsgContent;
      _idx_game = 0;
      PrizePlan = new WCP_TerminalDrawPrizePlan(0);
      PrizePlan.NumGames = _message_content.PrizePlanGames.Games.Count;

      foreach (WSI.Common.TerminalDraw.Persistence.TerminalDrawPrizePlanDetail _terminal_draw_prize_plan in _message_content.PrizePlanGames.Games)
      {
        PrizePlan.Games[_idx_game] = new WCP_TerminalDrawPrizePlanDetail();

        PrizePlan.Games[_idx_game].GameId = _terminal_draw_prize_plan.GameId;
        PrizePlan.Games[_idx_game].GameType = _terminal_draw_prize_plan.GameType;
        PrizePlan.Games[_idx_game].TerminalGameUrl = _terminal_draw_prize_plan.TerminalGameUrl;
        PrizePlan.Games[_idx_game].TerminalGameName = _terminal_draw_prize_plan.TerminalGameName;

        // Result Winner
        PrizePlan.Games[_idx_game].ResultWinnerScreenMessageLine0 = _terminal_draw_prize_plan.ResultWinnerScreenMessageLine0;
        PrizePlan.Games[_idx_game].ResultWinnerScreenMessageLine1 = _terminal_draw_prize_plan.ResultWinnerScreenMessageLine1;
        PrizePlan.Games[_idx_game].ResultWinnerScreenMessageLine2 = _terminal_draw_prize_plan.ResultWinnerScreenMessageLine2;
        PrizePlan.Games[_idx_game].ResultWinnerScreenMessageLine3 = _terminal_draw_prize_plan.ResultWinnerScreenMessageLine3;

        // Result Looser
        PrizePlan.Games[_idx_game].ResultLooserScreenMessageLine0 = _terminal_draw_prize_plan.ResultLooserScreenMessageLine0;
        PrizePlan.Games[_idx_game].ResultLooserScreenMessageLine1 = _terminal_draw_prize_plan.ResultLooserScreenMessageLine1;
        PrizePlan.Games[_idx_game].ResultLooserScreenMessageLine2 = _terminal_draw_prize_plan.ResultLooserScreenMessageLine2;
        PrizePlan.Games[_idx_game].ResultLooserScreenMessageLine3 = _terminal_draw_prize_plan.ResultLooserScreenMessageLine3;

        PrizePlan.Games[_idx_game].IsWinner = 0;
        if (_terminal_draw_prize_plan.IsWinner)
        {
          PrizePlan.Games[_idx_game].IsWinner = 1;
        }

        PrizePlan.Games[_idx_game].AmountRePlayedDraw = (Int64)(_terminal_draw_prize_plan.AmountRePlayedDraw);
        PrizePlan.Games[_idx_game].AmountRePlayedWon = (Int64)(_terminal_draw_prize_plan.AmountRePlayedWon);
        PrizePlan.Games[_idx_game].AmountNrPlayedDraw = (Int64)(_terminal_draw_prize_plan.AmountNrPlayedDraw);
        PrizePlan.Games[_idx_game].AmountNrPlayedWon = (Int64)(_terminal_draw_prize_plan.AmountNrPlayedWon);
        PrizePlan.Games[_idx_game].AmountPointsPlayedDraw = (Int64)_terminal_draw_prize_plan.AmountPointsPlayedDraw;
        PrizePlan.Games[_idx_game].AmountPointsPlayedWon = (Int64)_terminal_draw_prize_plan.AmountPointsPlayedWon;
        PrizePlan.Games[_idx_game].TerminalGameTimeOut = (Int64)_terminal_draw_prize_plan.TerminalGameTimeOut;
        PrizePlan.Games[_idx_game].AmountTotalBalance = (Int64)_terminal_draw_prize_plan.AmountTotalBalance;
        PrizePlan.Games[_idx_game].PlaySessionId = (Int64)_terminal_draw_prize_plan.PlaySessionId;
        
        PrizePlan.Games[_idx_game].Forced = 0;
        if (_terminal_draw_prize_plan.Forced)
        {
          PrizePlan.Games[_idx_game].Forced = 1;
        }
        
        _idx_game++;
      }
    }
  }
}