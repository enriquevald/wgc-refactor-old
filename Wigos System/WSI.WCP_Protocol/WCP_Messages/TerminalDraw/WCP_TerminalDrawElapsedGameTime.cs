﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Terminaldraw.cs
// 
//   DESCRIPTION: --
// 
//        AUTHOR: FJC
// 
// CREATION DATE: 02-JAN-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JAN-2017 FJC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Runtime.Remoting.Messaging;
using WSI.WCP;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_TerminalDrawGameTimeElapsed(out WCP_IMessage Request, long SequenceId, Int64 PlaySessionId, Int64 GameTimeElapsed, Boolean ClosePlaySession)
    {
      WCP_Message _wcp_request;
      WCP_MsgTerminalDrawGameTimeElapsed _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgTerminalDrawGameTimeElapsed);

      _wcp_request.MsgHeader.SequenceId = SequenceId;

      _wcp_content = (WCP_MsgTerminalDrawGameTimeElapsed)_wcp_request.MsgContent;
      _wcp_content.PlaySessionId = PlaySessionId;
      _wcp_content.GameTimeElapsed = GameTimeElapsed;
      _wcp_content.ClosePlaySession = ClosePlaySession;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }
  }
}