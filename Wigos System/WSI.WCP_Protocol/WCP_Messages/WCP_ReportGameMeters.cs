//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportGameMeters.cs
// 
//   DESCRIPTION: WC2 Protocol Dll - Shared between Wigos Bingo Server and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 12-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_ReportGameMeters(out WCP_IMessage Request,
                                                    long SequenceId,
                                                    ushort GameId,
                                                    [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameName,
                                                    [In, MarshalAs(UnmanagedType.BStr, SizeConst = 260)] String GameBaseName,
                                                    long DenominationCents,
                                                    long NumPlayed,
                                                    long NumPlayedMaxValue,
                                                    long PlayedCents,
                                                    long PlayedMaxValueCents,
                                                    long NumWon,
                                                    long NumWonMaxValue,
                                                    long WonCents,
                                                    long WonMaxValueCents,
                                                    long JackpotCents,
                                                    long JackpotMaxValueCents,
                                                    long ProgressiveJackpotCents,
                                                    long ProgressiveJackpotMaxValueCents,
                                                    bool IgnoreDeltas)
    {
      WCP_Message WCP_request;
      WCP_MsgReportGameMeters WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportGameMeters);
      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content = (WCP_MsgReportGameMeters)WCP_request.MsgContent;

      WCP_content.GameId = GameName;
      WCP_content.GameBaseName = GameBaseName;

      WCP_content.DenominationCents = (int)DenominationCents;

      WCP_content.PlayedCount = NumPlayed;
      WCP_content.PlayedCountMaxValue = NumPlayedMaxValue;
      WCP_content.PlayedCents = PlayedCents;
      WCP_content.PlayedMaxValueCents = PlayedMaxValueCents;

      WCP_content.WonCount = NumWon;
      WCP_content.WonCountMaxValue = NumWonMaxValue;
      WCP_content.WonCents = WonCents;
      WCP_content.WonMaxValueCents = WonMaxValueCents;

      WCP_content.JackpotCents = JackpotCents;
      WCP_content.JackpotMaxValueCents = JackpotMaxValueCents;

      // ACC 07-AUG-2014 Add Progressive Jackpot Cents into Machine Meters
      WCP_content.ProgressiveJackpotCents = ProgressiveJackpotCents;
      WCP_content.ProgressiveJackpotMaxValueCents = ProgressiveJackpotMaxValueCents;

      // JMM 04-NOV-2015: PCD Update Meters -> On reset & fix, the center should ingore de deltas
      WCP_content.IgnoreDeltas = IgnoreDeltas;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_ReportGameMeters


    void WCP_IProtocol.GetReply_ReportGameMeters(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_MsgReportGameMetersReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_MsgReportGameMetersReply)msg.Message.MsgContent;

    } // GetReply_ReportGameMeters

  }
}
