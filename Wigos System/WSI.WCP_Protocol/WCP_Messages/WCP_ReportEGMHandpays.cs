//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportEGMHandpays.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for meters process.
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 02-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2013 JMM    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for EMG Handpays report message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgEGMHandpays
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_EGM_Handpays(out WCP_IMessage Request,
                                               long SequenceId,
                                               Int64 TransactionID,
                                               long Datetime,
                                               Int64 HandpayType,
                                               Int64 HandpayCents,
                                               Int64 PlaySessionID,
                                               Int64 PreviousPlaySessionID,
                                               Byte ProgressiveGroup,
                                               Byte Level,
                                               Int64 PartialPayCents,
                                               Byte ResetID)
    {
      WCP_Message WCP_request;
      WCP_MsgEGMHandpays WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgEGMHandpays);
      WCP_content = (WCP_MsgEGMHandpays)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content.TransactionID = TransactionID;
      WCP_content.Datetime = DateTime.FromFileTime(Datetime);
      WCP_content.HandpayType = (HANDPAY_TYPE)HandpayType;
      WCP_content.HandpayCents = HandpayCents;
      WCP_content.PlaySessionID = PlaySessionID;
      WCP_content.PreviousPlaySessionID = PreviousPlaySessionID;
      WCP_content.ProgressiveGroup = ProgressiveGroup;
      WCP_content.Level = Level;
      WCP_content.PartialPayCents = PartialPayCents;
      WCP_content.ResetID = ResetID;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_EGM_Handpays
  }
}
