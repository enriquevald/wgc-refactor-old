//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReportPlaySession.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for terminal machine meters + tito session meters.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 12-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-NOV-2013 NMR    First release.
// 11-JUL-2014 XCD    Calculate estimated points awarded
// 14-SEP-2015 DHA    Product Backlog Item 3705: Added coins from terminals
// 31-AUG-2017 JMM    WIGOS-4700-Promotional credit reported as cashable after card out/in: Modify LKT-SAS-HOST for send counters
// 12-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Runtime.InteropServices;
using WSI.WCP.WCP_Messages.Meters;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for Play Session message
    // 
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgPlaySession
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_UpdatePlaySession(out WCP_IMessage Request,
                                                    long SequenceId,
                                                    long PlaySessionId,
                                                    long Closed,
                                                    long StartedDatetime,
                                                    long FinishedDatetime,
                                                    [In, MarshalAs(UnmanagedType.BStr, SizeConst = 32)] String TrackData1,
                                                    [In, MarshalAs(UnmanagedType.BStr, SizeConst = 32)] String TrackData2)
    {
      WCP_Message WCP_request;
      WCP_MsgPlaySessionMeters WCP_content;
      
      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgPlaySessionMeters);
      WCP_content = (WCP_MsgPlaySessionMeters)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content.play_session_id = PlaySessionId;
      WCP_content.closed = Closed == 1;
      WCP_content.started_datetime = DateTime.FromFileTime(StartedDatetime);
      WCP_content.closed_datetime = DateTime.FromFileTime(FinishedDatetime);
      WCP_content.track_data1 = TrackData1;
      WCP_content.track_data2 = TrackData2;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_UpdatePlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Update request for Play Session message; set Card Session Data
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MsgPlaySession
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgPlaySession
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_CardSessionData (out WCP_IMessage ModifiedRequest,
                                                      WCP_IMessage Request,
                                                      long ToAccountOnCloseCashableCents,
                                                      long ToAccountOnCloseRestrictedCents,
                                                      long ToAccountOnCloseNonRestrictedCents,
                                                      long OnStartSessionCashableCents,
                                                      long OnStartSessionRestrictedCents,
                                                      long OnStartSessionNonRestrictedCents,
                                                      long OnEndSessionCashableCents,
                                                      long OnEndSessionRestrictedCents,
                                                      long OnEndSessionNonRestrictedCents)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgPlaySessionMeters _wcp_content;

      _msg = (WCP_MessageEnvelope)Request;
      _wcp_content = (WCP_MsgPlaySessionMeters)_msg.Message.MsgContent;

      _wcp_content.mico2_account_session = true;

      _wcp_content.to_account_on_close.Redeemable = ToAccountOnCloseCashableCents;
      _wcp_content.to_account_on_close.PromoNotRedeemable = ToAccountOnCloseRestrictedCents;
      _wcp_content.to_account_on_close.PromoRedeemable= ToAccountOnCloseNonRestrictedCents;

      _wcp_content.found_on_egm.Redeemable = OnStartSessionCashableCents;
      _wcp_content.found_on_egm.PromoNotRedeemable = OnStartSessionRestrictedCents;
      _wcp_content.found_on_egm.PromoRedeemable = OnStartSessionNonRestrictedCents;

      _wcp_content.remaining_on_egm.Redeemable = OnEndSessionCashableCents;
      _wcp_content.remaining_on_egm.PromoNotRedeemable = OnEndSessionRestrictedCents;
      _wcp_content.remaining_on_egm.PromoRedeemable = OnEndSessionNonRestrictedCents;      

      ModifiedRequest = Request;

    } // UpdateRequest_CardSessionData

    //------------------------------------------------------------------------------
    // PURPOSE : Update request for Play Session message; set machine meters
    // 
    //  PARAMS :
    //      - INPUT :
    //        - WCP_MsgPlaySession
    //        - Machine.PlayedCents
    //        - Machine.WonCents
    //        - Machine.JackpotCents
    //        - Machine.NumPlayed
    //        - Machine.NumWon
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgPlaySession
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_PlaySessionMachineMeters(out WCP_IMessage ModifiedRequest,
                                                              WCP_IMessage Request,
                                                              long PlayedCents,
                                                              long WonCents,
                                                              long JackpotCents,
                                                              long NumPlayed,
                                                              long NumWon,
                                                              long PlayedReedemable,
                                                              long PlayedRestrictedCents, 
                                                              long PlayedNonRestrictedCents )
    {
      WCP_MessageEnvelope msg;
      WCP_MsgPlaySessionMeters WCP_content;

      msg = (WCP_MessageEnvelope)Request;
      WCP_content = (WCP_MsgPlaySessionMeters)msg.Message.MsgContent;

      WCP_content.machine_meters.played_cents = PlayedCents;
      WCP_content.machine_meters.won_cents = WonCents;
      WCP_content.machine_meters.jackpot_cents = JackpotCents;
      WCP_content.machine_meters.num_played = NumPlayed;
      WCP_content.machine_meters.num_won = NumWon;
      WCP_content.machine_meters.played_reedemable_cents = PlayedReedemable;
      WCP_content.machine_meters.played_restricted_cents = PlayedRestrictedCents;
      WCP_content.machine_meters.played_non_restricted_cents = PlayedNonRestrictedCents;

      ModifiedRequest = Request;
    } // UpdateRequest_PlaySessionMachineMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update request for Play Session message; set Session meters (tickets, bills)
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MsgPlaySession
    //        - TitoMeters.TicketInCashableCents
    //        - TitoMeters.TicketInPromoNRCents
    //        - TitoMeters.TicketInPromoRECents
    //        - TitoMeters.TicketOutCashableCents
    //        - TitoMeters.TicketOutPromoNRCents
    //        - TitoMeters.BillCents
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgPlaySession
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_PlaySessionTITOMeters(out WCP_IMessage ModifiedRequest,
                                                           WCP_IMessage Request,
                                                           long TicketInCashableCents,
                                                           long TicketInPromoNRCents,
                                                           long TicketInPromoRECents,
                                                           long TicketOutCashableCents,
                                                           long TicketOutPromoNRCents,
                                                           long BillCents,
                                                           long CashInCoinsCents,
                                                           long HandpayCents)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgPlaySessionMeters WCP_content;

      msg = (WCP_MessageEnvelope)Request;
      WCP_content = (WCP_MsgPlaySessionMeters)msg.Message.MsgContent;

      WCP_content.tito_session_meters.ticket_in_cashable_cents = TicketInCashableCents;
      WCP_content.tito_session_meters.ticket_in_promo_nr_cents = TicketInPromoNRCents;
      WCP_content.tito_session_meters.ticket_in_promo_re_cents = TicketInPromoRECents;
      WCP_content.tito_session_meters.ticket_out_cashable_cents = TicketOutCashableCents;
      WCP_content.tito_session_meters.ticket_out_promo_nr_cents = TicketOutPromoNRCents;
      
      // Cash In Meters
      WCP_content.tito_session_meters.cash_in_coins_in_cents = CashInCoinsCents;
      WCP_content.tito_session_meters.cash_in_bills_in_cents = BillCents;
      WCP_content.tito_session_meters.cash_in_total_in_cents = CashInCoinsCents + BillCents;

      WCP_content.tito_session_meters.handpay_cents          = HandpayCents;

      ModifiedRequest = Request;
    } // UpdateRequest_PlaySessionTITOMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update request for Play Session message; set Session meters (tickets, bills)
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MsgPlaySession
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgPlaySession
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_PlaySessionFTMeters(out WCP_IMessage ModifiedRequest,
                                                         WCP_IMessage Request,
                                                         long Type,
                                                         long FTToGMCashableCents,
                                                         long FTToGMRestrictedCents,
                                                         long FTToGMNonRestrictedCents,
                                                         long FTFromGMCashableCents,
                                                         long FTFromGMRestrictedCents,
                                                         long FTFromGMNonRestrictedCents)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgPlaySessionMeters WCP_content;

      msg = (WCP_MessageEnvelope)Request;
      WCP_content = (WCP_MsgPlaySessionMeters)msg.Message.MsgContent;

      WCP_content.ft_meters.type                            = Type;
      WCP_content.ft_meters.to_gm_cashable_cents            = FTToGMCashableCents;
      WCP_content.ft_meters.to_gm_restricted_cents          = FTToGMRestrictedCents;
      WCP_content.ft_meters.to_gm_non_restricted_cents      = FTToGMNonRestrictedCents;
      WCP_content.ft_meters.from_gm_cashable_cents          = FTFromGMCashableCents;
      WCP_content.ft_meters.from_gm_restricted_cents        = FTFromGMRestrictedCents;
      WCP_content.ft_meters.from_gm_non_restricted_cents    = FTFromGMNonRestrictedCents;
      
      ModifiedRequest = Request;
    } // UpdateRequest_PlaySessionFTMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_MsgPlaySessionReply msg to return to C App
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MessageEnvelope: WCP_MsgPlaySessionReply
    //
    //      - OUTPUT:WCP_Parameters Parameters
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.GetReply_UpdatePlaySession(WCP_IMessage Message, out long EstimatedPointsCents)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgPlaySessionMetersReply reply;

      msg = (WCP_MessageEnvelope)Message;
      reply = (WCP_MsgPlaySessionMetersReply)msg.Message.MsgContent;

      EstimatedPointsCents = reply.EstimatedPointsCents;

    } // GetReply_UpdatePlaySession
  }
}
