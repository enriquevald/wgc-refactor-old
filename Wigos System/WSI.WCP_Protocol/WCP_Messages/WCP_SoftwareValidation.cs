//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_SoftwareValidation.cs
// 
//   DESCRIPTION: WCP_SoftwareValidation class
// 
//        AUTHOR: Jordi C�rdoba
// 
// CREATION DATE: 02-APR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-APR-2014 JCOR   First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;
using System.Xml;
using System.IO;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    
    public void NewRequest_VersionSignature(out WCP_IMessage Request,
                                             long SequenceId,
                                             long ValidationID,
                                             long Method,
                                             String Seed,
                                             String Signature,
                                             long ReceivedStatus)
    {
      WCP_Message WCP_request;
      WCP_MsgVersionSignature WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgVersionSignature);
      WCP_content = (WCP_MsgVersionSignature)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content.Method = (Int32) Method; 
      WCP_content.Seed = Seed;
      WCP_content.Signature = Signature;

      WCP_content.ReceivedStatus = (Int32)ReceivedStatus;
      WCP_content.ValitadionID = ValidationID;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_VersionSignature

  }

  public partial class WCP_SoftwareValidation_Class : WCP_ISwValidation
  {


    //------------------------------------------------------------------------------
    // PURPOSE: GetVersionSignatureInfo
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage ReceivedMessage    
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    void WCP_ISwValidation.GetSwValidationInfo(WCP_IMessage ReceivedMessage,
                                                       out Int64 ValitadionID,
                                                        out Int32 Method,
                                                        out String Seed)
    {


      WCP_CmdSwValidation _sw_validation;

      _sw_validation = ProvideSwValidation(ReceivedMessage);

      ValitadionID = _sw_validation.ValitadionID;
      Method = _sw_validation.Method;
      Seed = _sw_validation.Seed;

    } // GetVersionSignatureInfo


    //------------------------------------------------------------------------------
    // PURPOSE: Provide WCP_IBonusing from Received Message
    //
    //  PARAMS:
    //      - INPUT:
    //        - WCP_IMessage, ( really WCP_Message_Envelope )
    //
    //      - OUTPUT:
    //         
    // RETURNS:
    //
    //
    //
    public static WCP_CmdSwValidation ProvideSwValidation(WCP_IMessage ReceivedMessage)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgExecuteCommand _message_content;
      WCP_CmdSwValidation _msg_sw_signature;

      _msg = (WCP_MessageEnvelope)ReceivedMessage;
      _message_content = (WCP_MsgExecuteCommand)_msg.Message.MsgContent;

      _msg_sw_signature = new WCP_CmdSwValidation();
      _msg_sw_signature.LoadXml(XmlReader.Create(new StringReader(_message_content.CommandParameters)));

      return _msg_sw_signature;
    } // ProvideSwValidation
  }
}
