//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_UpdateCardSession.cs
// 
//   DESCRIPTION: WC2 Protocol Dll - Shared between Wigos Bingo Server and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 10-AUG-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-AUG-2010 RCI    First release.
// 11-JUL-2014 XCD    Calculate estimated points awarded
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {

    void WCP_IProtocol.NewRequest_UpdateCardSession(out WCP_IMessage Request,
                                                    long SequenceId,
                                                    long CardSessionId,
                                                    long NumPlayed,
                                                    long PlayedCents,
                                                    long NumWon,
                                                    long WonCents,
                                                    long JackpotCents)
    {
      WCP_Message WCP_request;
      WCP_MsgUpdateCardSession WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgUpdateCardSession);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgUpdateCardSession)WCP_request.MsgContent;

      WCP_content.CardSessionId = CardSessionId;
      WCP_content.PlayedCount = NumPlayed;
      WCP_content.PlayedCents = PlayedCents;
      WCP_content.WonCount = NumWon;
      WCP_content.WonCents = WonCents;
      WCP_content.JackpotCents = JackpotCents;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_UpdateCardSession


    void WCP_IProtocol.GetReply_UpdateCardSession(WCP_IMessage ReceivedMessage, out long EstimatedPointsCents)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgUpdateCardSessionReply reply;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgUpdateCardSessionReply)msg.Message.MsgContent;

      EstimatedPointsCents = reply.EstimatedPointsCents;
      
    } // GetReply_UpdateCardSession

  }
}
