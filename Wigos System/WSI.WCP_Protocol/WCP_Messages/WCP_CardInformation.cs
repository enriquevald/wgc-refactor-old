using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for Card Inserted message
    //           response will obtained calling GetReply_CardInformation
    // 
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_MsgCardInformation
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_GetCardInformation(out WCP_IMessage Request,
                                                     long SequenceId,
                                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData)
    {
      WCP_Message WCP_request;
      WCP_MsgCardInformation WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgCardInformation);
      WCP_content = (WCP_MsgCardInformation)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content.SetTransactionId(SequenceId);
      WCP_content.SetTrackData(0, TrackData);

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_GetCardInformation
  }
}
