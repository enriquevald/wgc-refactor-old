using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_ProtocolParameters(out WCP_IMessage Request, long SequenceId, Int32 Protocol)
    {
      WCP_Message _wcp_request;
      WCP_MsgGetProtocolParameters _wcp_content;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetProtocolParameters);

      _wcp_request.MsgHeader.SequenceId = SequenceId;
      _wcp_content = (WCP_MsgGetProtocolParameters)_wcp_request.MsgContent;

      _wcp_content.Protocol = Protocol;

      Request = WCP_MessageEnvelope.CreateEnvelope(_wcp_request);
    }


    void WCP_IProtocol.GetReply_SmibProtocol(WCP_IMessage Message, out Int32 Protocol)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgGetProtocolParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)Message;
      _message_content = (WCP_MsgGetProtocolParametersReply)_msg.Message.MsgContent;

      Protocol = (Int32)_message_content.SmibConfig.Protocol;
    }


    void WCP_IProtocol.GetReply_PcdPollingParams(WCP_IMessage Message, out UInt32 CashOutFinished,
                                                                       out UInt32 MetersIdle,
                                                                       out UInt32 MetersInSession,
                                                                       out UInt32 MetersOnCashout)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgGetProtocolParametersReply _message_content;

      _msg = (WCP_MessageEnvelope)Message;
      _message_content = (WCP_MsgGetProtocolParametersReply)_msg.Message.MsgContent;

      CashOutFinished = _message_content.SmibConfig.PcdParams.PollingParams.CashoutFinished;
      MetersIdle = _message_content.SmibConfig.PcdParams.PollingParams.MetersIdle;
      MetersInSession = _message_content.SmibConfig.PcdParams.PollingParams.MetersInSession;
      MetersOnCashout = _message_content.SmibConfig.PcdParams.PollingParams.MetersOnCashout;
    }

    void WCP_IProtocol.GetReply_PcdPort(WCP_IMessage Message, Int32 PortType, Int32 PortNumber, out Int64 EgmNumber,
                                        out Double EgmNumberMultiplier, out Int32 PcdIoNumber, out Int32 PcdIoType,
                                        out UInt32 EdgeType, out UInt32 TimePulseDownMs, out UInt32 TimePulseUpMs)
    {
      WCP_MessageEnvelope _msg;
      WCP_MsgGetProtocolParametersReply _message_content;
      SmibProtocols.PCDProtocolConfiguration.PCDPort _pcd_port;

      _msg = (WCP_MessageEnvelope)Message;
      _message_content = (WCP_MsgGetProtocolParametersReply)_msg.Message.MsgContent;

      if (PortType == (Int32)SmibProtocols.PCDProtocolConfiguration.IoType.INPUT)
      {
        _pcd_port = _message_content.SmibConfig.PcdParams.InputPort(PortNumber);
      }
      else if (PortType == (Int32)SmibProtocols.PCDProtocolConfiguration.IoType.OUTPUT)
      {
        _pcd_port = _message_content.SmibConfig.PcdParams.OutputPort(PortNumber);
      }
      else
      {
        _pcd_port = new SmibProtocols.PCDProtocolConfiguration.PCDPort();
      }      

      EgmNumber = _pcd_port.CodeNumber;
      EgmNumberMultiplier = Convert.ToDouble(_pcd_port.CodeNumberMultiplier);
      PcdIoNumber = _pcd_port.PcdIoNumber;
      PcdIoType = (Int32)_pcd_port.PcdIoType;
      EdgeType = (UInt32)_pcd_port.PulseEdgeType;
      TimePulseDownMs = _pcd_port.TimePulseDownMs;
      TimePulseUpMs = _pcd_port.TimePulseUpMs;
    }
  }
}
