//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_IsValidTicket.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for ticket checking.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2013 NMR    First release.
// 06-NOV-2013 NMR    Fixed: incorrect amount returns
//------------------------------------------------------------------------------

using System;
using System.Text;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for ticket testing message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //        - SystemId
    //        - ValidationNumber
    //        - Amount
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgIsValidTicket
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_TITO_IsValidTicket(out WCP_IMessage Request,
                                                     long SequenceId,
                                                     long SystemId,
                                                     long ValidationNumberSequence,
                                                     long AmountCents)
    {
      WCP_Message WCP_request;
      WCP_TITO_MsgIsValidTicket WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgIsValidTicket);

      WCP_content = (WCP_TITO_MsgIsValidTicket)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content.SystemId = (Int32)SystemId;
      WCP_content.ValidationNumberSequence = ValidationNumberSequence;
      WCP_content.AmountCents = AmountCents;      

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_TITO_IsValidTicket

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgIsValidTicketReply to return results to C App
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MessageEnvelope: WCP_TITO_MsgIsValidTicketReply
    //
    //      - OUTPUT:
    //        - Amount
    //        - ExpirationDate
    //        - PoolId
    //        - ValidateStatus
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.GetReply_TITO_IsValidTicket(WCP_IMessage ReceivedMessage,
                                                   out long AmountCents,
                                                   out WCP_SystemTime ExpirationDate,
                                                   out long PoolId,
                                                   out long ValidateStatus,
                                                   out long TicketId)
    {
      WCP_MessageEnvelope msg;
      WCP_TITO_MsgIsValidTicketReply reply;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_TITO_MsgIsValidTicketReply)msg.Message.MsgContent;

      AmountCents = reply.AmountCents;
      PoolId = reply.PoolId;
      ValidateStatus = (int)reply.ValidateStatus;
      ExpirationDate = ToWCPSystemTime (reply.ExpirationDate);
      TicketId = reply.TicketId;
    } // GetReply_TITO_IsValidTicket
  }
}
