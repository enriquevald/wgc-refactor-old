//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_ValidationNumber.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for ticket creation.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 20-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2013 NMR    First release.
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado
//------------------------------------------------------------------------------

using System;
using System.Text;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for ValidationNumberSequence creation
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgValidationNumber
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_TITO_GetValidationNumber(out WCP_IMessage             Request,
                                                           long                         SequenceId,
                                                           long                         Type,
                                                           Int64                        Cents,
                                                           long                         TransactionId)
    {
      WCP_Message WCP_request;           
      WCP_TITO_MsgValidationNumber WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgValidationNumber);

      WCP_content = (WCP_TITO_MsgValidationNumber)WCP_request.MsgContent;
      
      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content.AmountCents   = Cents;
      WCP_content.TicketType    = (Int32) Type;
      WCP_content.TransactionId = (Int64) TransactionId;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_TITO_GetValidationNumber

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgValidationNumberReply msg to return to C App
    // 
    //  PARAMS :
    //      - INPUT:
    //        - WCP_MessageEnvelope: WCP_TITO_MsgValidationNumberReply
    //
    //      - OUTPUT:
    //        - ValidationNumberSequence
    //        - SystemId
    //        - SequenceId
    //        - MachineId
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.GetReply_TITO_GetValidationNumber(WCP_IMessage ReceivedMessage,
                                                         out long ValidationNumberSequence,
                                                         out long SystemId,
                                                         out long SequenceId,
                                                         out long MachineId,
                                                         out long TransactionId)
    {
      WCP_MessageEnvelope msg;
      WCP_TITO_MsgValidationNumberReply reply;

      ValidationNumberSequence = 0;
      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_TITO_MsgValidationNumberReply)msg.Message.MsgContent;

      ValidationNumberSequence = reply.validation_number_sequence;
      SystemId = reply.system_id;
      SequenceId = reply.sequence_id;
      MachineId = reply.machine_id;
      TransactionId = reply.transaction_id;
    } // GetReply_TITO_GetValidationNumber
  }
}
