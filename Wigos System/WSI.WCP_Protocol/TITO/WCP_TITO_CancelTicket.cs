//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_CancelTicket.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for ticket cancel.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2013 NMR    First release.
// 03-NOV-2016 FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for ticket cancel message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //        - SystemId
    //        - ValidationNumber
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgCancelTicket
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_TITO_CancelTicket(out WCP_IMessage Request,
                                                    long SequenceId,
                                                    long SystemId,
                                                    long ValidationNumberSequence,
                                                    long StopCancellation,
                                                    long TicketId,
                                                    long MachineStatus,
                                                    long ValidateStatus)
    {
      WCP_Message WCP_request;
      WCP_TITO_MsgCancelTicket WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgCancelTicket);

      WCP_content = (WCP_TITO_MsgCancelTicket)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content.ValidationNumberSequence = ValidationNumberSequence;
      WCP_content.SystemId = (Int32)SystemId;
      WCP_content.StopCancellation = StopCancellation == 1;
      WCP_content.TicketId = TicketId;
      WCP_content.MachineStatus = (ENUM_TITO_REDEEM_TICKET_TRANS_STATUS)MachineStatus;
      WCP_content.ValidateStatus = (ENUM_TITO_IS_VALID_TICKET_MSG)ValidateStatus;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_TITO_CancelTicket

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgCancelTicketReply msg to return to C App
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - WCP_MessageEnvelope: WCP_TITO_MsgCancelTicketReply
    //
    //      - OUTPUT:WCP_Parameters Parameters
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.GetReply_TITO_CancelTicket(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_TITO_MsgCancelTicketReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_TITO_MsgCancelTicketReply)msg.Message.MsgContent;

    } // GetReply_TITO_CancelTicket
  }
}
