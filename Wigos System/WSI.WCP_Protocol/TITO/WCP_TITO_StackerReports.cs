//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_StackerReports.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for ticket creation.
//                To this method was copied procedures from WCP_ProtocolNewRequest
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 28-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-NOV-2013 NMR    First release.
//------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using System.Text;
using WSI.Common;

namespace WSI.WCP.WCP_Protocol
{
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    void WCP_IProtocol.NewRequest_CashSessionPendingClosing(out WCP_IMessage Request, long SequenceId, long TransactionId, long StackerId, WCP_SystemTime ChangeDatetime, bool ReadMeters)
    {
      WCP_Message WCP_request;
      WCP_MsgCashSessionPendingClosing WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgCashSessionPendingClosing);

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content = (WCP_MsgCashSessionPendingClosing)WCP_request.MsgContent;

      WCP_content.TransactionId = TransactionId;
      WCP_content.NewStackerId = StackerId;      
      WCP_content.ChangeDatetime = ToDateTime (ChangeDatetime);
      WCP_content.ReadMeters = ReadMeters;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);

    } // NewRequest_CashSessionPendingClosing

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_MsgCashSessionPendingClosing to add another meter in the list
    // 
    //  PARAMS :
    //      - INPUT :
    //        - WCP_MessageEnvelope: WCP_MsgCashSessionPendingClosing (initial request)
    //        - Code: meter code
    //        - GameId
    //        - DenomCents
    //        - Value: reported value for meter
    //        - MaxValue: max meter value for rollover
    //
    //      - OUTPUT:
    //        - WCP_MessageEnvelope: WCP_MsgCashSessionPendingClosing, request with new meter in the list
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.UpdateRequest_AddStackerMeter(out WCP_IMessage Request,
                                                     WCP_IMessage ReceivedRequest,
                                                     long Code,
                                                     long GameId,
                                                     long DenomCents,
                                                     long Value,
                                                     long MaxValue)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgCashSessionPendingClosing WCP_content;
      SAS_Meter _meter;

      msg = (WCP_MessageEnvelope)ReceivedRequest;
      WCP_content = (WCP_MsgCashSessionPendingClosing)msg.Message.MsgContent;

      _meter = new SAS_Meter();
      _meter.Code = (Int32)Code;
      _meter.GameId = (Int32)GameId;
      _meter.DenomCents = (Decimal)DenomCents;
      _meter.Value = Value;
      _meter.MaxValue = MaxValue;
      WCP_content.LastSasMeters.Add(_meter);

      Request = ReceivedRequest;
    } // UpdateRequest_AddStackerMeter

    void WCP_IProtocol.GetReply_SetCashSessionPendingClosing(WCP_IMessage ReceivedMessage, out long Result, [Out, MarshalAs(UnmanagedType.BStr, SizeConst = 51)] out String AlreadyInsertedTerminalName)
    {
      WCP_MessageEnvelope msg;
      WCP_MsgCashSessionPendingClosingReply reply;

      msg = (WCP_MessageEnvelope)ReceivedMessage;
      reply = (WCP_MsgCashSessionPendingClosingReply)msg.Message.MsgContent;

      Result = reply.Result;
      AlreadyInsertedTerminalName = (reply.AlreadyInsertedTerminalName.Length > 50) ? reply.AlreadyInsertedTerminalName.Remove(50)
                                                                                    : reply.AlreadyInsertedTerminalName;
    } // GetReply_SetCashSessionPendingClosing

  } // WCP_Protocol_Class
}
