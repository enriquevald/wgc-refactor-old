//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_CreateTicket.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for ticket creation.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2013 NMR    First release.
// 19-NOV-2013 NMR    Processed new ticket property
// 03-DIC-2013 NMR    Add property PlaySessionId to create tickets
// 27-NOV-2015 SGB    Backlog Item 4715: Change functions ToWCPSystemTime & ToDateTime to WCP_Protocol
//------------------------------------------------------------------------------

using System;
using System.Text;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for ticket creation message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //        - ValidationType
    //        - Date
    //        - ValidationNumber
    //        - Amount
    //        - SystemId
    //        - ExpirationDate
    //        - PoolId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgCreateTicket
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_TITO_CreateTicket(out WCP_IMessage Request,
                                                   long SequenceId,
                                                   long TransactionId,
                                                   long ValidationType,
                                                   WCP_SystemTime CreationDateTime,
                                                   long ValidationNumberSequence,
                                                   long MachineTicketNumber,
                                                   long AmountCents,
                                                   long SystemId,
                                                   WCP_SystemTime ExpirationDate,
                                                   long PoolId,
                                                   long TicketType,
                                                   long PlaySessionId)
    {
      WCP_Message WCP_request;
      WCP_TITO_MsgCreateTicket WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgCreateTicket);
      WCP_content = (WCP_TITO_MsgCreateTicket)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;

      WCP_content.TransactionId = TransactionId;
      WCP_content.ValidationType = (Int32)ValidationType;
      WCP_content.Date = ToDateTime(CreationDateTime);
      WCP_content.ValidationNumberSequence = ValidationNumberSequence;
      WCP_content.MachineTicketNumber = (Int32)MachineTicketNumber;
      WCP_content.AmountCents = AmountCents;
      WCP_content.SystemId = (Int32)SystemId;
      WCP_content.ExpirationDate = ToDateTime (ExpirationDate);
      WCP_content.PoolId = PoolId;
      WCP_content.TicketType = (Int32)TicketType;
      WCP_content.PlaySessionId = PlaySessionId;

      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_TITO_CreateTicket

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgCreateTicketReply msg to return to C App
    // 
    //  PARAMS :
    //      - INPUT:
    //        - WCP_MessageEnvelope: WCP_TITO_MsgCreateTicketReply
    //
    //      - OUTPUT:
    //
    // RETURNS :
    // 
    //   NOTES :

    void WCP_IProtocol.GetReply_TITO_CreateTicket(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_TITO_MsgCreateTicketReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_TITO_MsgCreateTicketReply)msg.Message.MsgContent;

    } // GetReply_TITO_CreateTicket
  }
}
