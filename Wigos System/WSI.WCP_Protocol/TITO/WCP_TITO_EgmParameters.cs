//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TITO_EgmParameters.cs: Split WCP_Protocol.cs
// 
//   DESCRIPTION: WCP_Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk for EGM parameters process.
// 
//        AUTHOR: Nelson Madrigal
// 
// CREATION DATE: 06-NOV-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-NOV-2013 NMR    First release.
// 06-NOV-2013 NMR    Removed Asset Number 
// 13-NOV-2013 NMR    Fixed error: missing message sequence 
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Runtime.InteropServices;

namespace WSI.WCP.WCP_Protocol
{
  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Create request for ticket creation message
    // 
    //  PARAMS :
    //      - INPUT : 
    //        - SequenceId
    //        - ValidationType
    //        - AllowCashableTicketOut
    //        - AllowPromotionalTicketOut
    //        - AllowTicketIn
    //        - SequenceId
    //        - MachineId
    //
    //      - OUTPUT:
    //        - WCP_Message: WCP_TITO_MsgEgmParameters
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.NewRequest_TITO_EgmParameters(out WCP_IMessage Request,
                                                     long SequenceId,
                                                     long ValidationType,
                                                     Boolean AllowCashableTicketOut,
                                                     Boolean AllowPromotionalTicketOut,
                                                     Boolean AllowTicketIn,
                                                     long ValidationSequence,
                                                     long MachineId,
                                                     long Bonus,
                                                     long Features,
                                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String SASVersion,
                                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 10)] String SASMachineName,
                                                     [In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String SASSerialNumber,
                                                     long SASAssetNumber,
                                                     long SASAccountDenomCents)
    {
      WCP_Message WCP_request;
      WCP_TITO_MsgEgmParameters WCP_content;

      WCP_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgEgmParameters);
      WCP_content = (WCP_TITO_MsgEgmParameters)WCP_request.MsgContent;

      WCP_request.MsgHeader.SequenceId = SequenceId;
      WCP_content.ValidationType = (Int32)ValidationType;
      WCP_content.AllowCashableTicketOut = AllowCashableTicketOut;
      WCP_content.AllowPromotionalTicketOut = AllowPromotionalTicketOut;
      WCP_content.AllowTicketIn = AllowTicketIn;
      WCP_content.ValidationSequence = ValidationSequence;
      WCP_content.MachineId = (Int32)MachineId;

      if (String.IsNullOrEmpty(SASVersion))
      {
        WCP_content.SASVersion = "";
      }
      else
      {
        WCP_content.SASVersion = (SASVersion.Length > 10) ? SASVersion.Remove(10)
                                                                                    : SASVersion;
      }

      if (String.IsNullOrEmpty(SASMachineName))
      {
        WCP_content.SASMachineName = "";
      }
      else
      {
        WCP_content.SASMachineName = (SASMachineName.Length > 10) ? SASMachineName.Remove(10)
                                                                                    : SASMachineName;
      }

      WCP_content.Bonus = (Int32)Bonus;
      WCP_content.Features = (Int32)Features;

      if (String.IsNullOrEmpty(SASSerialNumber))
      {
        WCP_content.SASSerialNumber = "";
      }
      else
      {
        WCP_content.SASSerialNumber = (SASSerialNumber.Length > 50) ? SASSerialNumber.Remove(50) : SASSerialNumber;
      }
      WCP_content.SASAssetNumber = (Int64)SASAssetNumber;
      WCP_content.SASAccountDenomCents = (Int64)SASAccountDenomCents;


      Request = WCP_MessageEnvelope.CreateEnvelope(WCP_request);
    } // NewRequest_TITO_EgmParameters

    //------------------------------------------------------------------------------
    // PURPOSE : Process WCP_TITO_MsgEgmParametersReply msg to return to C App
    // 
    //  PARAMS :
    //      - INPUT :
    //        - WCP_MessageEnvelope: WCP_TITO_MsgEgmParametersReply
    //
    //      - OUTPUT:
    //
    // RETURNS :
    // 
    //   NOTES :
    void WCP_IProtocol.GetReply_TITO_EgmParameters(WCP_IMessage ReceivedMessage)
    {
      //WCP_MessageEnvelope msg;
      //WCP_TITO_MsgEgmParametersReply reply;

      //msg = (WCP_MessageEnvelope)ReceivedMessage;
      //reply = (WCP_TITO_MsgEgmParametersReply)msg.Message.MsgContent;

    } // GetReply_TITO_EgmParameters
  }
}
