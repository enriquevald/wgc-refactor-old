//------------------------------------------------------------------------------
// Copyright © 2008-2009 Win Systems International, Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Protocol.cs
// 
//   DESCRIPTION: WCP Protocol Dll - Shared between Wigos and LKT.
//                Offers Methods used by a Kiosk to communicate directly with Wigos.
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 12-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-AUG-2008 RRT    First release.
// 18-NOV-2011 ACC    Added Machine meters
// 06-JUN-2012 ACC    Add Session Allow Cashin flag.
// 15-JUN-2012 ACC    Add Jackpot Cents into Machine Meters
// 21-JUN-2012 XIT    Modified GeParametersReply
// 19-JUL-2012 ACC    Added Sas Flags in get parameters trx.
// 05-AUG-2014 JMM    Enabled protocol logging on development or when LKT_PROTOCOL_LOGGER_ENABLED environment variable is enabled.
// 27-NOV-2015 SGB    Backlog Item 4715: Change functions ToWCPSystemTime & ToDateTime from WCP_TITO_CreateTicket
// 02-MAY-2016 ETP    Fixed bug 14082: Fixed conection errors with windows update.
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//------------------------------------------------------------------------------

using System;
using System.Timers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;
using WSI.WCP;
using WSI.Common;
using System.Net.NetworkInformation;
using WSI.Common.TITO;

namespace WSI.WCP.WCP_Protocol
{
  public delegate void WCP_LoggerWriteCallback([In, MarshalAs(UnmanagedType.LPStr, SizeConst = 255)] String Function, [In, MarshalAs(UnmanagedType.LPStr, SizeConst = 255)] String Value);

  /// <summary>
  /// Protocol Class
  /// </summary>
  public partial class WCP_Protocol_Class : WCP_IProtocol
  {
    #region Constants

    private const Int32 WCP_TIMEOUT = 30000;  // AJQ 19-ABR-2010, TCP is connected, so we wait a bit more ...

    #endregion

    #region Enums

    private enum WCP_PROTOCOL_STATUS
    {
      UNKNOWN,
      DISCONNECTED,
      ENROLLING,
      GETTING_PARAMETERS,
      CONNECTED,
    }
    #endregion

    #region Members

    private WaitableQueue m_queue_send;         // Queue to send messages.
    private WaitableQueue m_queue_receive;      // Queue for all responses.
    private WaitableQueue m_queue_responses;  // Queue for responses to Terminal. (Kiosk)

    private TCPClientSSL m_ssl_client;


    // Status
    private WCP_PROTOCOL_STATUS m_status = WCP_PROTOCOL_STATUS.UNKNOWN;
    private ManualResetEvent m_ev_connected = new ManualResetEvent(false);

    private long m_status_tick = 0;
    private AutoResetEvent m_status_changed = new AutoResetEvent(true);

    private String m_terminal_name = "";
    private TerminalTypes m_terminal_type = TerminalTypes.UNKNOWN;
    private Int64 m_terminal_session_id = 0;
    private Int64 m_sequence_id = 0;
    private Int64 m_transaction_id = 0;

    private int m_keep_alive_interval = 30000;
    private int m_connection_timeout = 60000;
    private int m_tick_last_msg_sent = Misc.GetTickCount();
    private int m_tick_last_msg_received = Misc.GetTickCount();

    private Thread m_wcp_main_thread;

    private StringBuilder m_sb = new StringBuilder(128 * 1024);

    private Boolean m_lcd_display_model = false;

    private WCP_PROTOCOL_STATUS Status
    {
      get
      {
        lock (this)
        {
          return m_status;
        }
      }
    }

    private WCP_LoggerWriteCallback m_logger_callback;

    #endregion // Members

    #region Private Methods

    private void LoggerWrite(String Function, String Value)
    {
      if (m_logger_callback != null)
      {
        m_logger_callback(Function.Substring(0, Math.Min(Function.Length, 240)), Value.Substring(0, Math.Min(Value.Length, 240)));
      }
    }

    private void KeepAliveWhileConnected()
    {
      long _elapsed_ticks;
      WCP_Message _wcp_request;
      String _xml_request;
      Byte[] _raw_request;

      while (Status == WCP_PROTOCOL_STATUS.CONNECTED)
      {
        try
        {
          if (WaitStatusChanged(1000) != WCP_PROTOCOL_STATUS.CONNECTED)
          {
            return;
          }
          if (!m_ssl_client.IsConnected)
          {
            SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }
          // Check Last Received Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_received);
          if (_elapsed_ticks > m_connection_timeout)
          {
            SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          // Check Last Sent Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_sent);
          if (_elapsed_ticks <= m_keep_alive_interval)
          {
            // A few time ago a message was sent: continue waiting
            continue;
          }

          _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgKeepAlive);
          _wcp_request.MsgHeader.TerminalId = m_terminal_name;
          _wcp_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
          this.GetSequenceId(out _wcp_request.MsgHeader.SequenceId);

          _xml_request = _wcp_request.ToXml();
          _raw_request = Encoding.UTF8.GetBytes(_xml_request);

          if (!m_ssl_client.Send(_raw_request))
          {
            SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          m_tick_last_msg_sent = Misc.GetTickCount();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          LoggerWrite("KeepAliveWhileConnected", "Exception: " + _ex.Message);

          SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);

          return;
        }
      }
    } // KeepAliveWhileConnected

    /// <summary>
    /// WCP_Protocol Constructor
    /// </summary>
    public WCP_Protocol_Class()
    {
      // Logger enabled on development or if LKT_PROTOCOL_LOGGER_ENABLED is set
      if (Environment.GetEnvironmentVariable("LKT_PROTOCOL_LOGGER_ENABLED") != null
       || Environment.GetEnvironmentVariable("LKS_VC_DEV") != null)
      {
        Log.AddListener(new SimpleLogger("LKT_PROTOCOL"));
      }

      // Queues
      m_queue_send = new WaitableQueue();         // Send to WC2 Server
      m_queue_receive = new WaitableQueue();      // Received from WC2 Server
      m_queue_responses = new WaitableQueue();  // Queue where kiosk remain blocked waiting for a response

      m_ssl_client = new TCPClientSSL(m_queue_receive);

      m_wcp_main_thread = new Thread(new ThreadStart(MainThread));
      m_wcp_main_thread.Name = "WCP Main Thread";
    } // WCP_Protocol

    /// <summary>
    /// Manage kiosk requests and periodically messages requests.
    /// </summary>
    private void MainThread()
    {
      WCP_PROTOCOL_STATUS _wcp_status;

      Log.Message("Main Thread Started");

      while (true)
      {
        try
        {
          _wcp_status = WaitStatusChanged(1000);

          switch (_wcp_status)
          {
            default:
            case WCP_PROTOCOL_STATUS.UNKNOWN:
              {
                SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);
              }
              break;

            case WCP_PROTOCOL_STATUS.CONNECTED:
              {
                KeepAliveWhileConnected();
              }
              break;

            case WCP_PROTOCOL_STATUS.DISCONNECTED:
              {
                WCP_QueryService _wcp_query_service;
                IPEndPoint _wcp_service_ipe;
                Boolean _disconnect;
                Boolean _enrolled;
                Boolean _msg_ok;
                int _wait_hint;


                _disconnect = true;
                _enrolled = false;
                _msg_ok = false;
                _wait_hint = 0;

                while (!_enrolled)
                {
                  Thread.Sleep(_wait_hint);
                  _wait_hint = 2000;

                  _wcp_service_ipe = null;
                  _wcp_query_service = null;

                  try
                  {
                    Log.Message("Query Service ...");

                    // Ask for WCP_Service's IP endpoint to the WCP_DirectoryService 
                    _wcp_query_service = new WCP_QueryService();
                    _wcp_service_ipe = _wcp_query_service.QueryService(5000);
                    if (_wcp_service_ipe == null)
                    {
                      continue;
                    }

                    Log.Message("WCP Service found at: " + _wcp_service_ipe.ToString());

                    // Connect to the WCP_Service
                    m_ssl_client = new TCPClientSSL(m_queue_receive);
                    m_ssl_client.Connect(_wcp_service_ipe);
                    if (!m_ssl_client.IsConnected)
                    {
                      m_ssl_client.Disconnect();
                      m_ssl_client = null;

                      continue;
                    }

                    Log.Message("Connected to:" + _wcp_service_ipe.ToString());
                    // AJQ 01-AUG-2014, Connected but on any error or not authorized response wait more time 
                    _wait_hint = 10000;

                    _msg_ok = false;


                    // Enroll
                    WCP_Message _wcp_request;
                    WCP_Message _wcp_response;
                    String _xml_request;
                    Byte[] _raw_request;
                    IAsyncResult _result;
                    WCP_MsgEnrollTerminal _wcp_msg_request;

                    _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgEnrollTerminal);

                    _wcp_request.MsgHeader.TerminalId = m_terminal_name;
                    _wcp_msg_request = (WCP_MsgEnrollTerminal)_wcp_request.MsgContent;
                    _wcp_msg_request.ProviderId = "WSI";
                    _wcp_msg_request.TerminalType = m_terminal_type;

                    _xml_request = _wcp_request.ToXml();
                    _raw_request = Encoding.UTF8.GetBytes(_xml_request);
                    _result = m_ssl_client.BeginSend(_raw_request, new AsyncCallback(SendCallback), m_ssl_client);

                    // Response received
                    string _xml_response = "";

                    // Enrolled
                    _xml_response = ReciveSyncMessage();
                    _wcp_response = WCP_Message.CreateMessage(_xml_response);
                    Log.Message("Enroll ResponseCode: " + _wcp_response.MsgHeader.ResponseCode.ToString());
                    if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_OK)
                    {
                      WCP_MsgEnrollTerminalReply _wcp_msg_reply;

                      _wcp_msg_reply = (WCP_MsgEnrollTerminalReply)_wcp_response.MsgContent;
                      _msg_ok = true;

                      lock (this)
                      {
                        m_terminal_session_id = _wcp_response.MsgHeader.TerminalSessionId;
                        m_sequence_id = Math.Max(m_sequence_id, 1 + _wcp_msg_reply.LastSequenceId);
                        m_transaction_id = Math.Max(m_transaction_id, 1 + _wcp_msg_reply.LastTransactionId);
                      }
                      Log.Message("ENROLL, SESSION ID: " + m_terminal_session_id);
                    }

                    if (!_msg_ok)
                    {
                      continue;
                    }

                    _msg_ok = false;

                    // Get Parameters
                    _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgGetParameters);
                    lock (this)
                    {
                      _wcp_request.MsgHeader.TerminalId = m_terminal_name;
                      _wcp_request.MsgHeader.TerminalSessionId = m_terminal_session_id;
                      this.GetSequenceId(out _wcp_request.MsgHeader.SequenceId);
                    }

                    _xml_request = _wcp_request.ToXml();
                    _raw_request = Encoding.UTF8.GetBytes(_xml_request);
                    _result = m_ssl_client.BeginSend(_raw_request, new AsyncCallback(SendCallback), m_ssl_client);

                    // Enrolled
                    String _xml_response2 = "";
                    _xml_response2 = ReciveSyncMessage();
                    _wcp_response = WCP_Message.CreateMessage(_xml_response2);
                    Log.Message("GetParameters ResponseCode: " + _wcp_response.MsgHeader.ResponseCode.ToString());
                    if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_OK)
                    {
                      WCP_MsgGetParametersReply _wcp_msg_reply;

                      _wcp_msg_reply = (WCP_MsgGetParametersReply)_wcp_response.MsgContent;
                      _msg_ok = true;

                      m_keep_alive_interval = 1000 * _wcp_msg_reply.KeepAliveInterval;
                      m_connection_timeout = 1000 * _wcp_msg_reply.ConnectionTimeout;
                    }
                    else
                    {
                      Log.Message("Session: " + m_terminal_session_id + " " + _wcp_request.MsgHeader.TerminalSessionId + " " + _wcp_response.MsgHeader.TerminalSessionId);
                    }


                    if (!_msg_ok)
                    {
                      continue;
                    }

                    // Start receiving data
                    m_ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), m_ssl_client);


                    _enrolled = true;
                    _disconnect = false;

                    SetStatus(WCP_PROTOCOL_STATUS.CONNECTED);

                    ClearDownloadsInProgress();

                  }
                  catch (Exception _ex)
                  {
                    Log.Exception(_ex);
                    LoggerWrite("MainThread", "Exception: " + _ex.Message);
                  }
                  finally
                  {
                    _wcp_query_service = null;

                    if (_disconnect)
                    {
                      if (m_ssl_client != null)
                      {
                        m_ssl_client.Disconnect();
                      }
                    }
                  }
                }
              }
              break;
          };
        }
        catch
        {
          try
          {
            m_ssl_client.Disconnect();
          }
          catch
          { }
          SetStatus(WCP_PROTOCOL_STATUS.UNKNOWN);
        }
      }
    } // MainThread

    private void SendCallback(IAsyncResult Result)
    {
      try
      {
        TCPClientSSL _ssl_client;

        m_tick_last_msg_sent = Misc.GetTickCount();

        _ssl_client = (TCPClientSSL)Result.AsyncState;
        _ssl_client.EndSend(Result);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        LoggerWrite("SendCallback", "Exception: " + _ex.Message);
      }
    } // SendCallback

    private void ReceiveCallback(IAsyncResult Result)
    {
      m_tick_last_msg_received = Misc.GetTickCount();
    } // ReceiveCallback

    private String ReciveSyncMessage()
    {
      StringBuilder _sb;
      String _message;
      Int32 _tick_start_receiving;
      IAsyncResult _result;
      Boolean _msg_ok = false;
      byte[] _raw_response;

      _sb = new StringBuilder();

      _tick_start_receiving = Misc.GetTickCount();

      try
      {


        while (Misc.GetElapsedTicks(_tick_start_receiving) < WCP_TIMEOUT)
        {
          _result = m_ssl_client.BeginReceive(new AsyncCallback(ReceiveCallback), m_ssl_client);
          if (_result.AsyncWaitHandle.WaitOne(WCP_TIMEOUT, false))
          {
            // Enrolled
            _raw_response = m_ssl_client.EndReceive(_result);
            _sb.Append(Encoding.UTF8.GetString(_raw_response));

            if (_sb.ToString().EndsWith("</WCP_Message>"))
            {
              //Message completed
              _msg_ok = true;
              break;
            }
          }
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_msg_ok)
      {
        _message = _sb.ToString();
      }
      else
      {
        _message = String.Empty;
        Log.Error("ReciveSyncMessage: Message not Correct");
      }

      return _message;
    }

    private void EnqueueReceivedMessage(IAsyncResult Result)
    {
      Boolean _error;
      Boolean _disconnect;
      TCPClientSSL _ssl_client;
      Byte[] _raw_response;
      String _xml_response;
      WCP_Message _wcp_response;
      Boolean _enqueue_message;
      String _str_error;

      // Default: Error occurred -> Disconnect
      _error = true;
      _disconnect = true;
      _wcp_response = null;
      _xml_response = null;
      _str_error = "Unknown";
      _ssl_client = null;

      try
      {
        m_tick_last_msg_received = Misc.GetTickCount();

        _ssl_client = (TCPClientSSL)Result.AsyncState;

        if (_ssl_client == null)
        {
          _str_error = "SSLClient is null";
          return;
        }

        _raw_response = _ssl_client.EndReceive(Result);
        if (_raw_response == null)
        {
          _str_error = "Raw is null";
          return;
        }

        if (_raw_response.Length == 0)
        {
          _str_error = "Raw is empty";
          return;
        }

        String _xml_partial;
        int _idx0;
        int _idx1;

        _disconnect = false;

        _str_error = "Parsing ...";
        _xml_partial = Encoding.UTF8.GetString(_raw_response);
        m_sb.Append(_xml_partial);


        while (true)
        {
          _xml_response = null;

          if (m_sb.Length < "<WCP_Message></WCP_Message>".Length)
          {
            break;
          }

          _xml_partial = m_sb.ToString();
          _idx0 = _xml_partial.IndexOf("<WCP_Message>");
          if (_idx0 >= 0)
          {
            _idx1 = _xml_partial.IndexOf("</WCP_Message>");

            if (_idx1 > 0)
            {
              _idx1 = _idx1 + "</WCP_Message>".Length;

              _xml_response = _xml_partial.Substring(_idx0, _idx1 - _idx0);
              _xml_partial = _xml_partial.Substring(_idx1);
              m_sb.Length = 0;
              m_sb.Append(_xml_partial);
            }
          }

          if (String.IsNullOrEmpty(_xml_response))
          {
            break;
          }

          _str_error = "CreateMessage(Xml)";
          _wcp_response = WCP_Message.CreateMessage(_xml_response);

          if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID)
          {
            _str_error = "SESSION NOT VALID";
            _disconnect = true;
            m_sb.Length = 0; // Delete any pending received data to be processed 

            return;
          }

          _str_error = "EnqueueMessage ...";
          _enqueue_message = true;

          if (m_terminal_type == TerminalTypes.PROMOBOX
            || m_lcd_display_model)
          {
            // WCP is in charge of requesting all resources related to those 
            // PromoBox messages (replies) that include references to resources.
            //
            // WCP will also manage the voucher printing for those PromoBox messages (replies) 
            // that generate a print voucher.
            _enqueue_message = ProcessPromoBoxMessageReply(_wcp_response);
          }

          if (_enqueue_message
           && _wcp_response.MsgHeader.MsgType != WCP_MsgTypes.WCP_MsgKeepAliveReply)
          {
            m_queue_responses.Enqueue(_wcp_response);
          }

        } // while

        _error = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        LoggerWrite("EnqueueReceivedMessage", "Exception: " + _ex.Message);

        if (_wcp_response != null)
        {
          LoggerWrite("EnqueueReceivedMessage", "Exception In " + _wcp_response.MsgHeader.MsgType.ToString());
        }

        _error = true;
      }
      finally
      {
        if (_error)
        {
          try
          {
            LoggerWrite("EnqueueReceivedMessage", "Error: " + _str_error);

            if (_wcp_response != null)
            {
              LoggerWrite("EnqueueReceivedMessage", " MessageType: " + _wcp_response.MsgHeader.MsgType.ToString());
            }
            else if (_xml_response != null)
            {
              int _beginning_tag;
              int _ending_tag;

              _beginning_tag = _xml_response.IndexOf("<MsgType>");
              _ending_tag = _xml_response.IndexOf("</MsgType>");

              if (_beginning_tag > 0 && _ending_tag > 0)
              {
                LoggerWrite("EnqueueReceivedMessage", "MessageType: " + _xml_response.Substring(_beginning_tag, _ending_tag - _beginning_tag));
              }
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
            LoggerWrite("EnqueueReceivedMessage", "Exception finally: " + _ex.Message);
          }
        }

        try
        {
          if (!_disconnect)
          {
            _str_error = "BeginReceive ...";
            _ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), _ssl_client);
          }
        }
        catch (Exception _ex)
        {
          LoggerWrite("EnqueueReceivedMessage", "BeginReceive Failed! Ex:" + _ex.Message);
          _disconnect = true;
        }

        if (_disconnect)
        {
          SetStatus(WCP_PROTOCOL_STATUS.DISCONNECTED);
        }
      }
    } // EnqueueReceivedMessage

    private WCP_PROTOCOL_STATUS WaitStatusChanged(int MillisecondsTimeout)
    {
      m_status_changed.WaitOne(MillisecondsTimeout, false);

      return m_status;
    }

    private bool WaitUntilConnected(int MillisecondsTimeout)
    {
      return m_ev_connected.WaitOne(MillisecondsTimeout, false);
    }


    private WCP_PROTOCOL_STATUS SetStatus(WCP_PROTOCOL_STATUS NewStatus)
    {
      WCP_PROTOCOL_STATUS old_status;

      lock (this)
      {
        old_status = m_status;

        if (old_status != NewStatus)
        {
          m_status = NewStatus;
          m_status_tick = Environment.TickCount;
          m_status_changed.Set();

          if (NewStatus == WCP_PROTOCOL_STATUS.CONNECTED)
          {
            m_ev_connected.Set();
          }
          else
          {
            m_ev_connected.Reset();
          }

          if (NewStatus == WCP_PROTOCOL_STATUS.DISCONNECTED)
          {
            m_ssl_client.Disconnect();
          }
          Log.Message("WCP_PROTOCOL_STATUS changed: " + old_status.ToString() + " --> " + m_status.ToString());
          LoggerWrite("SetStatus", "WCP_PROTOCOL_STATUS changed: " + old_status.ToString() + " --> " + m_status.ToString());
        }
      }

      return old_status;
    }

    private DateTime ToDateTime(WCP_SystemTime Value)
    {
      if (Value.wYear >= 9999) return DateTime.MaxValue;
      if (Value.wYear <= 1) return DateTime.MinValue;

      return new DateTime(Value.wYear, Value.wMonth, Value.wDay,
                           Value.wHour, Value.wMinute, Value.wSecond,
                           Value.wMilliseconds);
    }

    private WCP_SystemTime ToWCPSystemTime(DateTime Value)
    {
      WCP_SystemTime _st;
      DateTime _value;

      _value = Value;
      if (_value.Year >= 9999) _value = DateTime.MaxValue;
      if (_value.Year <= 1) _value = DateTime.MinValue;

      _st = new WCP_SystemTime();
      _st.wYear = (ushort)_value.Year;
      _st.wMonth = (ushort)_value.Month;
      _st.wDay = (ushort)_value.Day;
      _st.wHour = (ushort)_value.Hour;
      _st.wMinute = (ushort)_value.Minute;
      _st.wSecond = (ushort)_value.Second;
      _st.wMilliseconds = (ushort)_value.Millisecond;

      return _st;
    }

    #endregion

    #region Public Methods


    private String Split20(String X)
    {
      String[] _separator = new String[1];
      String[] _words;
      string _line;
      _separator[0] = " ";
      StringBuilder _sb;
      _sb = new StringBuilder();

      X = X.Replace("  ", " ");
      _words = X.Split(_separator, StringSplitOptions.RemoveEmptyEntries);

      _line = "";
      for (int i = 0; i < _words.Length; i++)
      {
        if (_line.Length == 0)
        {
          _line = _words[i];

          continue;
        }

        if (_line.Length + 1 + _words[i].Length <= 20)
        {
          _line = _line + " " + _words[i];

          continue;
        }

        _sb.AppendLine(_line);

        _line = _words[i];
      }
      if (_line.Length > 0)
      {
        _sb.AppendLine(_line);
      }

      return _sb.ToString();

    }

    void WCP_IProtocol.IpConfig([Out, MarshalAs(UnmanagedType.BStr, SizeConst = 1000)] out String StrConfig)
    {
      StrConfig = "[ IP CONFIG ]";

      try
      {
        IPInterfaceProperties _ip_properties;
        IPv4InterfaceProperties _ip4;
        StringBuilder _sb;
        String _mac;
        String _name;

        int _svr;

        int _num_ni;
        int _num_ip;
        int _index;

        StrConfig = String.Empty;
        _sb = new StringBuilder();

        _num_ip = 0;
        _num_ni = 0;
        foreach (NetworkInterface _net_interface in NetworkInterface.GetAllNetworkInterfaces())
        {
          if (_net_interface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
          {
            continue;
          }
          if (_net_interface.OperationalStatus != OperationalStatus.Up)
          {
            continue;
          }
          _num_ni++;
          _ip_properties = _net_interface.GetIPProperties();
          foreach (UnicastIPAddressInformation _ip in _ip_properties.UnicastAddresses)
          {
            if (_ip.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;
            _num_ip++;
          }
        }

        _num_ni = 0;
        foreach (NetworkInterface _net_interface in NetworkInterface.GetAllNetworkInterfaces())
        {
          if (_net_interface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
          {
            continue;
          }
          if (_net_interface.OperationalStatus != OperationalStatus.Up)
          {
            continue;
          }
          _num_ni++;

          _ip_properties = _net_interface.GetIPProperties();
          _ip4 = _ip_properties.GetIPv4Properties();

          if (_ip4.IsAutomaticPrivateAddressingEnabled && _ip4.IsAutomaticPrivateAddressingActive)
          {
            _sb.AppendLine("APIPA");
          }
          else
          {
            if (_ip4.IsDhcpEnabled)
            {
              _sb.AppendLine("DHCP");
            }
            else
            {
              _sb.AppendLine("STATIC");
            }
          }

          foreach (UnicastIPAddressInformation _ip in _ip_properties.UnicastAddresses)
          {
            if (_ip.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

            if (_num_ip == 1)
            {
              _sb.AppendLine(String.Format("IP:  {0}", _ip.Address.ToString().PadLeft(15)));
            }
            else
            {
              _sb.AppendLine(String.Format("IP1: {0}", _ip.Address.ToString().PadLeft(15)));
            }
            break;
          }

          _svr = 1;
          foreach (IPAddress _ip in _ip_properties.DnsAddresses)
          {
            if (_ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

            _sb.AppendLine(String.Format("DNS{0}:{1}", _svr, _ip.ToString().PadLeft(15)));
            if (_svr >= 2)
            {
              break;
            }
            _svr++;
          }
          break;
        }

        if (_num_ni == 0)
        {
          _sb.AppendLine("NETWORK INTERFACES:");
          _sb.AppendLine("DISABLED");
          StrConfig = _sb.ToString();

          return;
        }

        if (_num_ni > 1)
        {
          _sb.AppendLine("--------------------");
          _sb.AppendLine("NETWORK INTERFACES:");
          _sb.AppendLine("COUNT: " + _num_ni.ToString());
        }

        _index = 0;
        foreach (NetworkInterface _net_interface in NetworkInterface.GetAllNetworkInterfaces())
        {
          if (_net_interface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
          {
            continue;
          }
          if (_net_interface.OperationalStatus != OperationalStatus.Up)
          {
            continue;
          }

          _index++;
          _sb.AppendLine("--------------------");

          _mac = _net_interface.GetPhysicalAddress().ToString();
          if (_mac.Length == 12)
          {
            _mac = _mac.Substring(0, 4) + "-" + _mac.Substring(4, 4) + "-" + _mac.Substring(8, 4);
          }
          _sb.AppendLine(String.Format("MAC:{0}", _mac.PadLeft(16)));


          _ip_properties = _net_interface.GetIPProperties();
          _ip4 = _ip_properties.GetIPv4Properties();

          _sb.AppendLine("--IP--");
          _svr = 1;
          foreach (UnicastIPAddressInformation _ip in _ip_properties.UnicastAddresses)
          {
            if (_ip.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

            if (_num_ip == 1)
            {
              _sb.AppendLine(String.Format("IP:  {0}", _ip.Address.ToString().PadLeft(15)));
            }
            else
            {
              _sb.AppendLine(String.Format("IP{0}: {1}", _svr, _ip.Address.ToString().PadLeft(15)));
            }
            _sb.AppendLine(String.Format("MASK:{0}", _ip.IPv4Mask.ToString().PadLeft(15)));
            if (_svr >= 4)
            {
              break;
            }
            _svr++;
          }

          _svr = 1;
          foreach (GatewayIPAddressInformation _ip in _ip_properties.GatewayAddresses)
          {
            if (_ip.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

            _sb.AppendLine(String.Format("GATE:{1}", _svr, _ip.Address.ToString().PadLeft(15)));
            if (_svr >= 2)
            {
              break;
            }
            _svr++;
          }

          _sb.AppendLine("--DNS--");
          _svr = 1;
          foreach (IPAddress _ip in _ip_properties.DnsAddresses)
          {
            if (_ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

            _sb.AppendLine(String.Format("DNS{0}:{1}", _svr, _ip.ToString().PadLeft(15)));
            if (_svr >= 4)
            {
              break;
            }
            _svr++;
          }

          _sb.AppendLine("--DHCP--");
          if (_ip4.IsDhcpEnabled)
          {
            _sb.AppendLine("DHCP:ENABLED");

            _svr = 1;
            foreach (IPAddress _ip in _ip_properties.DhcpServerAddresses)
            {
              if (_ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

              _sb.AppendLine(String.Format("SRV{0}:{1}", _svr, _ip.ToString().PadLeft(15)));
              if (_svr >= 4)
              {
                break;
              }
              _svr++;
            }
          }
          else
          {
            _sb.AppendLine("DHCP:DISABLED");
          }

          if (_ip4.IsAutomaticPrivateAddressingEnabled && _ip4.IsAutomaticPrivateAddressingActive)
          {
            _sb.AppendLine("--APIPA--");
            _sb.AppendLine("APIPA:ACTIVE");
          }

          _sb.AppendLine("--NAME--");
          _name = Split20(_net_interface.Name.ToUpper());
          _sb.Append(_name);

          if (_index >= 2)
          {
            break; // Only the first TWO interfaces are listed.
          }
        }

        StrConfig = _sb.ToString();
      }
      catch
      { }
    }



    /// <summary>
    /// Enroll Terminal: LKT Request.
    /// </summary>
    void WCP_IProtocol.Start([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TerminalName, short TerminalType, IntPtr LoggerCallback)
    {
      String _lkt_virtual_upper_display;

      m_terminal_name = TerminalName;
      m_terminal_type = (TerminalTypes)TerminalType;
      _lkt_virtual_upper_display = System.Environment.GetEnvironmentVariable("LKT_VIRTUAL_UPPER_DISPLAY");

      m_lcd_display_model = _lkt_virtual_upper_display.Equals("5") || _lkt_virtual_upper_display.Equals("501");

      Log.Message(m_lcd_display_model.ToString());

      m_wcp_main_thread.Start();

      // PromoBox (Automated Kiosk) requires special resources 
      if (m_terminal_type == TerminalTypes.PROMOBOX
        || m_lcd_display_model)
      {
        InitPromoBox();
      }

      // Logger Callback
      if (!LoggerCallback.Equals(IntPtr.Zero))
      {
        try
        {
          m_logger_callback = (WCP_LoggerWriteCallback)Marshal.GetDelegateForFunctionPointer(LoggerCallback, Type.GetType("WSI.WCP.WCP_Protocol.WCP_LoggerWriteCallback"));
        }
        catch (Exception e)
        {
          Log.Message(e.Message);
        }
      }
    } // StartProtocol

    /// <summary>
    /// 
    /// </summary>
    void WCP_IProtocol.Stop()
    {
      if (m_terminal_type == TerminalTypes.PROMOBOX
        || m_lcd_display_model)
      {
        StopPromoBox();
      }

      // Do nothing ...
      // Do nothing ...
      Log.Message("WCP STOP!!!");
      Log.Message("");
    } // Stop

    public void GetIsConnected(out bool IsConnected)
    {
      IsConnected = (m_status == WCP_PROTOCOL_STATUS.CONNECTED);
    }

    public void GetSequenceId([Out, MarshalAs(UnmanagedType.I8)] out Int64 SequenceId)
    {
      lock (this)
      {
        SequenceId = m_sequence_id++;
      }

    } // GetSequenceId

    public void GetTransactionId([Out, MarshalAs(UnmanagedType.I8)] out Int64 TransactionId)
    {
      lock (this)
      {
        TransactionId = m_transaction_id++;
      }

    } // GetTransactionId

    public void Send(WCP_IMessage MessageToSend)
    {
      WCP_Message _request;
      WCP_MessageEnvelope _envelope;

      _envelope = (WCP_MessageEnvelope)MessageToSend;
      _request = _envelope.Message;

      Send(_request);
    }

    private void Send(WCP_Message MessageToSend)
    {
      String _xml;
      Byte[] _raw;

      lock (this)
      {
        MessageToSend.MsgHeader.TerminalId = m_terminal_name;
        MessageToSend.MsgHeader.TerminalSessionId = m_terminal_session_id;
      }

      _xml = MessageToSend.ToXml();

      _raw = Encoding.UTF8.GetBytes(_xml);

      Log.Message("" + MessageToSend.MsgHeader.SequenceId.ToString("000000") + " " + MessageToSend.MsgHeader.MsgType.ToString() + ": " + MessageToSend.MsgHeader.ResponseCode.ToString());

      //m_ssl_client.BeginSend(raw, new AsyncCallback(SendCallback), m_ssl_client);
      m_tick_last_msg_sent = Misc.GetTickCount();
      m_ssl_client.Send(_raw);
      m_tick_last_msg_sent = Misc.GetTickCount();
    }

    /// <summary>
    /// Function called by Kiosk to obtain a response to a pending request.
    /// </summary>
    public void WaitForMessage(out WCP_IMessage ReceivedMessage)
    {
      WCP_Message wcp_message;

      ReceivedMessage = null;
      wcp_message = null;

      try
      {
        wcp_message = (WCP_Message)m_queue_responses.Dequeue();

        Log.Message("    " + wcp_message.MsgHeader.SequenceId.ToString("000000") + " " + wcp_message.MsgHeader.MsgType.ToString() + ": " + wcp_message.MsgHeader.ResponseCode.ToString());

        ReceivedMessage = WCP_MessageEnvelope.CreateEnvelope(wcp_message);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        LoggerWrite("WaitForMessage", "Exception: " + _ex.Message);

        if (wcp_message != null)
        {
          LoggerWrite("WaitForMessage", "Exception In " + wcp_message.MsgHeader.MsgType.ToString());
        }
      }

    } // Response_GetMsgType

    //------------------------------------------------------------------------------
    // PURPOSE: TITO: Gets Validation number 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumPromotions
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    public void TITO_GetValidationNumber(long MachineId, long SequenceNumber, [Out, MarshalAs(UnmanagedType.I8)] out Int64 ValidationNumber, [Out, MarshalAs(UnmanagedType.Bool)] out Boolean IsValid)
    {

      SystemValidationNumber _tito = new SystemValidationNumber();
      ValidationNumber = _tito.ValidationNumber(true, (Int32)MachineId, (Int32)SequenceNumber);
      IsValid = ValidationNumber > 0;
      
    }

    //------------------------------------------------------------------------------
    // PURPOSE: TITO: Gets Internal Card Type 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ReceivedMessage
    //
    //      - OUTPUT:
    //        - NumPromotions
    //         
    // RETURNS:
    // 
    //   NOTES:
    //
    public void Local_GetInternalCardType([In, MarshalAs(UnmanagedType.BStr, SizeConst = 50)] String TrackData, out WCP_ICardType CardType, out long InternalTrackData)
    {
      String _internal_track_data;
      Int32 _internal_card_type;

      CardType = WCP_ICardType.Unknown;

      WSI.Common.CardNumber.TrackDataToInternal(TrackData, out _internal_track_data, out _internal_card_type);

      switch (_internal_card_type)
      {
        case 0:    // Player
          CardType = WCP_ICardType.Player;
          break;

        default:   // Tech
          CardType = WCP_ICardType.Tech;
          break;
      }

      if (!long.TryParse(_internal_track_data.Substring(_internal_track_data.Length - 9, 9), out InternalTrackData))
      {
        InternalTrackData = 0;
      }
    }

    #endregion
  }
}
