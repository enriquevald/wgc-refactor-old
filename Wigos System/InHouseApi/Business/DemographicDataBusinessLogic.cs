﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DemographicDataBusinessLogic.cs
// 
//   DESCRIPTION: DemographicData Business Logic class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using WSI.Common;
using InHouseApi.Classes.Common;
using InHouseApi.Shared;
using InHouseApi.Classes.Response;
using System.Security.Cryptography.X509Certificates;

namespace InHouseApi.Business
{
  internal class DemographicDataBusinessLogic : BaseBusinessLogic
  {
    #region     "Constructor"
    public DemographicDataBusinessLogic()
    {
    } // DemographicDataBusinessLogic
    #endregion  "Constructor"

    #region     "Properties"
    public string MethodVersion { get; set; } // MethodVersion
    public X509Certificate2 ClientCertificate { get; set; } // ClientCertificate
    public Boolean ClientAuthorization { get; set; } // ClientAuthorization

    #endregion  "Properties"

    #region "Public Methods"
    /// <summary>
    /// Get customer demographic data
    /// </summary>
    /// <param name="SiteId"></param>
    /// <param name="CustomerId"></param>
    /// <param name="Response"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    public Boolean GetDemographicData(Int16 SiteId, Int64 CustomerId, ref InHouseCustomerResponse Response, ref HttpStatusCode Status)
    {
      DataTable _datatable_sites;
      DataTable _datatable_site_services;
      Boolean _is_ok = false;
      Int16 _current_site_id = 0;
      Customer _customer;

      _customer = null;

      try
      {
        if ((ClientAuthorization && ClientCertificate != null) || !ClientAuthorization)
        {
          //**************************
          //IsCenter or requested site
          //**************************
          if (!SharedDefinitions.IsMultisite(out _datatable_site_services, out _datatable_sites))
          {
            if (!Int16.TryParse(GeneralParam.GetString("Site", "Identifier"), out _current_site_id))
              _current_site_id = 0;

            if (SiteId == 0 || SiteId == _current_site_id)
            {
              //Return data!
              _is_ok = GetDemographicDataFromMultisiteDB(_current_site_id, CustomerId, out Status, out _customer);
              Status = GetCustomerDemographicData_HttpStatusCode(_customer, CustomerId);
              Response.customer = _customer;

            }

            else
            {
              //A different site is call
              Status = HttpStatusCode.NotAcceptable;
            }
          }

          //*********
          //MULTISITE
          //*********
          else
          {
            if (SiteId == 0)
            {
              _is_ok = GetDemographicDataFromMultisiteDB(_current_site_id, CustomerId, out Status, out _customer);
              Status = GetCustomerDemographicData_HttpStatusCode(_customer, CustomerId);
              Response.customer = _customer;
            }

            else
            {
              //The site exist on multisite's site_list.
              Status = HttpStatusCode.NotFound;
              if (SharedDefinitions.IsValidMultisiteSite(_datatable_site_services, SiteId))
              {
                Response = DoCallChildSite_GetCustomerDemographicDataFromSite(SiteId, CustomerId);
                _customer = Response.customer;
                Status = GetCustomerDemographicData_HttpStatusCode(_customer, CustomerId);
              }

              else
              {
                //Site not exists on multisite's site_list.
                Status = HttpStatusCode.NotAcceptable;
              }
            }
          }

          if (_is_ok)
          {
            //---------------------------------------------------
            // FAKE DATA GENERATION
            //---------------------------------------------------
            if (_customer == null)
            {
              Status = HttpStatusCode.NotFound;
              _is_ok = false;
            }
            else
            {
              Response.customer = _customer;
              Status = HttpStatusCode.OK;
              _is_ok = true;
            }
          }
        }
        else
        {
          Status = HttpStatusCode.Unauthorized;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetDemographicData: SiteId: {0}, CustomerId: {1}", SiteId, CustomerId));
        _is_ok = false;
      }
      return _is_ok;
    }
    #endregion // Public Methods

    #region "Private Methods"
    /// <summary>
    /// Get data from identity card
    /// </summary>
    /// <param name="DocumentTypeId"></param>
    /// <param name="DocumentValue"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Document GetIdentityCardData(Int64 DocumentTypeId, String DocumentValue, SqlTransaction SqlTrx)
    {
      Document _doc;
      StringBuilder _sb;

      _doc = new Document();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   IDT_ID                         ");
        _sb.AppendLine("        , IDT_COUNTRY_ISO_CODE2          ");
        _sb.AppendLine("        , IDT_NAME                       ");
        _sb.AppendLine("   FROM   IDENTIFICATION_TYPES           ");
        _sb.AppendLine("  WHERE   IDT_ID  = @pIdentificationType ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pIdentificationType", SqlDbType.BigInt).Value = DocumentTypeId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              _doc.wigosCode = _sql_reader.GetInt32(_sql_reader.GetOrdinal("IDT_ID"));
              _doc.countryCode = _sql_reader.GetString(_sql_reader.GetOrdinal("IDT_COUNTRY_ISO_CODE2"));
              _doc.documentName = _sql_reader.GetString(_sql_reader.GetOrdinal("IDT_NAME"));
              _doc.value = DocumentValue;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetIdentityCardData-> DocumentTypeId: {0}, DocumentValue: {1}", DocumentTypeId, DocumentValue));
        Log.Exception(_ex);
      }

      return _doc;
    }

    /// <summary>
    /// Get LastUpdate in TimeZoneFormat
    /// </summary>
    /// <param name="LastUpdateInUTCTime"></param>
    /// <returns></returns>
    private static String GetDateLastUpdateInTimeZoneFormat(DateTime LastUpdateInUTCTime)
    {
      //String StrTimeZone;
      //StrTimeZone = GetTimeZoneBetweenDates(LastUpdateInLocalTime, LastUpdateInUTCTime);

      //return String.Format("{0} {1}", LastUpdateInLocalTime.ToString("yyyy-MM-dd'T'HH:mm"), StrTimeZone);
      DateTime _dt = new DateTime(LastUpdateInUTCTime.Year, LastUpdateInUTCTime.Month, LastUpdateInUTCTime.Day,
                                  LastUpdateInUTCTime.Hour, LastUpdateInUTCTime.Minute, LastUpdateInUTCTime.Second,
                                  DateTimeKind.Utc);

      return _dt.ToString("yyyy-MM-ddTHH:mm:ssK");
    }

    /// <summary>
    /// Get customer data
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="SiteId"></param>
    /// <param name="customer"></param>
    /// <returns></returns>
    private static Customer GetCustomerData(DataRow Row, Int64 SiteId, Customer customer, SqlTransaction _sql_trx)
    {
      Customer _customer_out;

      _customer_out = customer;

      try
      {
        _customer_out.siteId = SiteId;
        _customer_out.customerId = (Int64)Row["AC_ACCOUNT_ID"];

        _customer_out.person = new Classes.Common.Person();
        _customer_out.person.first_name = (Row["AC_HOLDER_NAME3"] != null ? Row["AC_HOLDER_NAME3"] : string.Empty).ToString();
        _customer_out.person.second_name = (Row["AC_HOLDER_NAME4"] != null ? Row["AC_HOLDER_NAME4"] : string.Empty).ToString();
        _customer_out.person.surnames = (Row["AC_HOLDER_NAME1"] != null ? Row["AC_HOLDER_NAME1"] : string.Empty).ToString() + SharedDefinitions.SPACE + (String)(Row["AC_HOLDER_NAME2"] != null ? Row["AC_HOLDER_NAME2"] : string.Empty).ToString();
        _customer_out.person.birthdate = (Row["AC_HOLDER_BIRTH_DATE"] != null ? Row["AC_HOLDER_BIRTH_DATE"] : DBNull.Value).ToString();  //birthdate: "1969-09-19"

        _customer_out.address = new Classes.Common.Address();
        _customer_out.address.adr1 = (Row["AC_HOLDER_ADDRESS_01"] != null ? Row["AC_HOLDER_ADDRESS_01"] : string.Empty).ToString();
        _customer_out.address.adr2 = (Row["AC_HOLDER_ADDRESS_02"] != null ? Row["AC_HOLDER_ADDRESS_02"] : string.Empty).ToString();
        _customer_out.address.adr3 = (Row["AC_HOLDER_EXT_NUM"] != null ? Row["AC_HOLDER_EXT_NUM"] : string.Empty).ToString();
        _customer_out.address.zip = (Row["AC_HOLDER_ZIP"] != null ? Row["AC_HOLDER_ZIP"] : string.Empty).ToString();
        _customer_out.address.city = (Row["AC_HOLDER_CITY"] != null ? Row["AC_HOLDER_CITY"] : string.Empty).ToString();
        _customer_out.address.country = (Row["CO_NAME"] != null ? Row["CO_NAME"] : string.Empty).ToString();

        customer.phones = GetCustomerPhonesFromRow(Row);

        customer.emails = GetCustomerEMailsFromRow(Row);

        customer.documents = GetCustomerDocumentsFromRow(Row, _sql_trx);

        _customer_out.lastUpdate = GetDateLastUpdateInTimeZoneFormat((DateTime)Row["AC_LAST_UPDATE_IN_UTC_TIME"]);  //lastUpdate in UTC time
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetCustomerData -> Site: {0}", SiteId));
        Log.Exception(_ex);
      }

      return _customer_out;
    }

    /// <summary>
    /// get customer phones
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static String[] GetCustomerPhonesFromRow(DataRow Row)
    {
      List<String> _phones_list;
      String _phone;

      _phones_list = new List<String>();

      try
      {
        if (!Row.IsNull("AC_HOLDER_PHONE_NUMBER_01"))
        {
          _phone = (String)Row["AC_HOLDER_PHONE_NUMBER_01"];

          if (!String.IsNullOrEmpty(_phone))
          {
            _phones_list.Add(_phone);
          }
        }

        if (!Row.IsNull("AC_HOLDER_PHONE_NUMBER_02"))
        {
          _phone = (String)Row["AC_HOLDER_PHONE_NUMBER_02"];

          if (!String.IsNullOrEmpty(_phone))
          {
            _phones_list.Add(_phone);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("InHouseApi.Business.DemographicDataBusinessLogic.GetCustomerPhonesFromRow");
        Log.Exception(_ex);
      }

      return _phones_list.ToArray();
    }

    /// <summary>
    /// Get customer emails
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static String[] GetCustomerEMailsFromRow(DataRow Row)
    {
      List<String> _emails_list;
      String _email;

      _emails_list = new List<String>();

      try
      {
        if (!Row.IsNull("AC_HOLDER_EMAIL_01"))
        {
          _email = (String)Row["AC_HOLDER_EMAIL_01"];

          if (!String.IsNullOrEmpty(_email))
          {
            _emails_list.Add(_email);
          }
        }

        if (!Row.IsNull("AC_HOLDER_EMAIL_02"))
        {
          _email = (String)Row["AC_HOLDER_EMAIL_02"];

          if (!String.IsNullOrEmpty(_email))
          {
            _emails_list.Add(_email);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("InHouseApi.Business.DemographicDataBusinessLogic.GetCustomerEMailsFromRow");
        Log.Exception(_ex);
      }

      return _emails_list.ToArray();
    }

    /// <summary>
    /// Get customer documents
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Classes.Common.Document[] GetCustomerDocumentsFromRow(DataRow Row, SqlTransaction SqlTrx)
    {
      List<Classes.Common.Document> _documents_list;

      _documents_list = new List<Classes.Common.Document>();

      try
      {
        AddDocumentToList(Row, _documents_list, "AC_HOLDER_ID_TYPE", "AC_HOLDER_ID", SqlTrx);

        AddDocumentToList(Row, _documents_list, "AC_HOLDER_ID1_TYPE", "AC_HOLDER_ID1", SqlTrx);

        AddDocumentToList(Row, _documents_list, "AC_HOLDER_ID2_TYPE", "AC_HOLDER_ID2", SqlTrx);

        AddDocumentToList(Row, _documents_list, "AC_HOLDER_ID3_TYPE", "AC_HOLDER_ID3", SqlTrx);
      }
      catch (Exception _ex)
      {
        Log.Error("InHouseApi.Business.DemographicDataBusinessLogic.GetCustomerDocumentsFromRow");
        Log.Exception(_ex);
      }

      return _documents_list.ToArray();
    }

    /// <summary>
    /// Add document to List
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="DocumentsList"></param>
    /// <param name="DocumentIdTypeFieldName"></param>
    /// <param name="DocumentValueFieldName"></param>
    /// <param name="SqlTrx"></param>
    private static void AddDocumentToList(DataRow Row, List<Classes.Common.Document> DocumentsList, String DocumentIdTypeFieldName, String DocumentValueFieldName, SqlTransaction SqlTrx)
    {
      Int32 _identity_type;
      String _doc_value;
      Document _document;

      try
      {
        if ((!Row.IsNull(DocumentIdTypeFieldName)) && (!Row.IsNull(DocumentValueFieldName)))
        {
          _identity_type = (Int32)Row[DocumentIdTypeFieldName];
          _doc_value = (String)Row[DocumentValueFieldName];

          _document = GetIdentityCardData(_identity_type, _doc_value, SqlTrx);

          if (_document != null)
          {
            DocumentsList.Add(_document);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.AddDocumentToList -> DocumentIdType: {0}, DocumentValue: {1}", DocumentIdTypeFieldName, DocumentValueFieldName));
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// Get customer data from DB
    /// </summary>
    /// <param name="SiteId"></param>
    /// <param name="CustomerId"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="CustomerTable"></param>
    /// <returns></returns>
    private static Boolean DB_GetCustomerData(Int64 SiteId, Int64 CustomerId, SqlTransaction SqlTrx, out DataTable CustomerTable)
    {
      StringBuilder _sb;

      CustomerTable = new DataTable("CUSTOMER_DATA");

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("          SELECT   AC_CREATION_SITE_ID                                                     ");
        _sb.AppendLine("                 , AC_ACCOUNT_ID                                                           ");
        //Person
        _sb.AppendLine("                 , AC_HOLDER_NAME3, AC_HOLDER_NAME4, AC_HOLDER_NAME2, AC_HOLDER_NAME1      ");
        _sb.AppendLine("                 , AC_HOLDER_BIRTH_DATE                                                    ");
        //Address
        _sb.AppendLine("                 , AC_HOLDER_ADDRESS_01                                                    ");
        _sb.AppendLine("                 , AC_HOLDER_ADDRESS_02                                                    ");
        _sb.AppendLine("                 , AC_HOLDER_ADDRESS_03                                                    ");
        _sb.AppendLine("                 , AC_HOLDER_EXT_NUM                                                       ");
        _sb.AppendLine("                 , AC_HOLDER_ZIP, AC_HOLDER_CITY, CO_NAME                                  ");
        //Phones
        _sb.AppendLine("                 , AC_HOLDER_PHONE_NUMBER_01, AC_HOLDER_PHONE_NUMBER_02                    ");
        //eMails
        _sb.AppendLine("                 , AC_HOLDER_EMAIL_01, AC_HOLDER_EMAIL_02                                  ");
        //Documents
        _sb.AppendLine("                 , AC_HOLDER_ID, AC_HOLDER_ID_TYPE                                         ");
        _sb.AppendLine("                 , AC_HOLDER_ID1, AC_HOLDER_ID1_TYPE                                       ");
        _sb.AppendLine("                 , AC_HOLDER_ID2, AC_HOLDER_ID2_TYPE                                       ");
        _sb.AppendLine("                 , AC_HOLDER_ID3, AC_HOLDER_ID3_TYPE                                       ");
        //LastUpdate
        _sb.AppendLine("                 , AC_LAST_UPDATE_IN_LOCAL_TIME, AC_LAST_UPDATE_IN_UTC_TIME                ");
        _sb.AppendLine("            FROM   ACCOUNTS                                                                ");
        _sb.AppendLine(" LEFT OUTER JOIN   COUNTRIES                                                               ");
        _sb.AppendLine("              ON   CO_COUNTRY_ID = AC_HOLDER_ADDRESS_COUNTRY                               ");
        _sb.AppendLine("             AND   CO_LANGUAGE_ID = @pLanguageId                                           ");
        _sb.AppendLine("           WHERE   AC_ACCOUNT_ID = @pCustomerId                                            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pCustomerId", SqlDbType.Int).Value = CustomerId;
          _sql_cmd.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = (Int32)LanguageIdentifier.English;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(CustomerTable);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.DB_GetCustomerData -> SiteId: {0}, CustomerId: {1}", SiteId, CustomerId));
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get data from DB
    /// </summary>
    /// <param name="SiteId"></param>
    /// <param name="CustomerId"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="Status"></param>
    /// <returns></returns>
    private static Boolean GetDemographicDataFromMultisiteDB(Int64 SiteId, Int64 CustomerId, out HttpStatusCode Status, out Customer customer)
    {
      DataTable _customer_data;
      DataRow _dr;
      SqlTransaction _sql_trx;

      Status = HttpStatusCode.InternalServerError;
      customer = new Classes.Common.Customer();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _sql_trx = _db_trx.SqlTransaction;

        if (!DB_GetCustomerData(SiteId, CustomerId, _sql_trx, out _customer_data))
        {
          Status = HttpStatusCode.NotFound;
          return false;
        }

        if ((_customer_data == null) || (_customer_data.Rows.Count == 0))
        {
          Status = HttpStatusCode.NotFound;
          return false;
        }

        _dr = _customer_data.Rows[0];

        try
        {
          customer = GetCustomerData(_dr, SiteId, customer, _db_trx.SqlTransaction);

          Status = HttpStatusCode.OK;

          return true;
        }
        catch (Exception _ex)
        {
          Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetDemographicDataFromMultisiteDB -> SiteId: {0}, CustomerId: {1}", SiteId, CustomerId));
          Log.Exception(_ex);
        }
      }

      return false;
    }

    /// <summary>
    /// Get customer demographic data from Site
    /// </summary>
    /// <param name="SiteId"></param>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    private InHouseCustomerResponse DoCallChildSite_GetCustomerDemographicDataFromSite(Int16 SiteId, Int64 CustomerId)
    {
      InHouseCustomerResponse _child_response = new InHouseCustomerResponse();
      DataTable _site_url_list = SharedDefinitions.GetSiteListIPPortPairs(SiteId);

      foreach (DataRow _dr in _site_url_list.Rows)
      {
        try
        {
          _child_response = SharedDefinitions.DoCallChildSite<InHouseCustomerResponse>(string.Format("https://{0}:{1}/InHouseApi/{2}/customer/0/{3}", _dr["ss_ip"], _dr["ss_port"], this.MethodVersion, CustomerId), SiteId.ToString(), _dr["SS_ID"].ToString());
          if (_child_response != null && _child_response.customer != null)
          {
            base.SetCurrentIPPortPairAsCurrent(_dr["SS_ID"].ToString(), _dr["SS_SITE_ID"].ToString());
            break;
          }
        }
        catch (Exception _ex)
        {
          System.Diagnostics.Debugger.Log(1, "", string.Format("DoCallChildSite_GetCustomerDemographicDataFromSite :: Error: {0}", _ex.Message));
        }

      }

      return (_child_response == null ? new InHouseCustomerResponse() : _child_response);
    }

    /// <summary>
    /// Get Http status code
    /// </summary>
    /// <param name="customer"></param>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    private static HttpStatusCode GetCustomerDemographicData_HttpStatusCode(Customer customer, Int64 CustomerId)
    {
      if ((customer != null) && (customer.customerId.Equals(CustomerId)))
      {
        return HttpStatusCode.OK;
      }

      return HttpStatusCode.NotFound;
    }
    #endregion "Private Methods"
  }
}
