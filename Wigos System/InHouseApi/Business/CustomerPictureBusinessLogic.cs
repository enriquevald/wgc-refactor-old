﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CustomerPictureBusinessLogic.cs
// 
//   DESCRIPTION: CustomerPicture Business Logic class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Linq;
using System.Collections;
using InHouseApi.Classes.Request;
using InHouseApi.Classes.Response;
using InHouseApi.Classes.Common;
using InHouseApi.Shared;
using WSI.Common;
using System.Security.Cryptography.X509Certificates;

namespace InHouseApi.Business
{
  internal class CustomerPictureBusinessLogic : BaseBusinessLogic
  {
    #region " Variables "

    private DataTable _datatable_sites;
    private DataTable _datatable_site_services;

    #endregion

    #region " Properties "

    public string MethodVersion { get; set; } // MethodVersion

    public X509Certificate2 ClientCertificate { get; set; } // ClientCertificate
    public Boolean ClientAuthorization { get; set; } // ClientAuthorization

    #endregion


    #region " Constructor "

    public CustomerPictureBusinessLogic()
    {
    } // CustomerPictureBusinessLogic

    #endregion // Constructor

    #region " Public Methods "

    public Boolean GetCustomerPicture(Int16 SiteId, Int64 CustomerId, ref InHouseCustomerPictureResponse Response, ref HttpStatusCode Status)
    {
      Boolean _is_ok = false;
      Int16 _current_site_id = 0;
      try
      {
        if ((ClientAuthorization && ClientCertificate != null) || !ClientAuthorization)
        {
          if (SharedDefinitions.IsMultisite(out _datatable_site_services, out _datatable_sites))
          {
            //MULTISITE
            if (SiteId.Equals(0))
            {
              //Currently MULTISITE is not storing customer_photo SQL Table.
              Status = HttpStatusCode.NoContent;
            }
            else
            {
              if (SharedDefinitions.IsValidMultisiteSite(_datatable_site_services, SiteId))
              {
                //Site exists on multisite's site_list, then make the Request.
                Response = DoCallChildSite_GetCustomerPicture(SiteId, CustomerId);
                if (Response != null && Response.CustomerPicture != null && Response.CustomerPicture.Picture != null)
                {
                Status = GetCustomerPicture_HttpStatusCode(Response.CustomerPicture, CustomerId);
                  if (Response.CustomerPicture.Picture.Equals(SharedDefinitions.NO_PHOTO))
                  {
                    Response.CustomerPicture.Picture = SharedDefinitions.SPACE;
                    Status = HttpStatusCode.NoContent;
              }

                }

              else
              {
                  Status = HttpStatusCode.NoContent;
                }
              }
              else
              {
                //Site not exists on multisite's site_list.
                Status = HttpStatusCode.NotAcceptable;
              }
            }
          }
          else
          {
            if (!Int16.TryParse(GeneralParam.GetString("Site", "Identifier"), out _current_site_id))             
              _current_site_id = 0; 
            if (SiteId.Equals(0) || SiteId.Equals(_current_site_id))
            {
              //Return data!
              using (DB_TRX _db_trx = new DB_TRX())
              {
                CustomerPicture _customer_picture;
                _is_ok = DB_GetCustomerPicture(CustomerId, _db_trx.SqlTransaction, out Status, out _customer_picture);
                Status = GetCustomerPicture_HttpStatusCode(_customer_picture, CustomerId);

                if (_customer_picture.Picture == SharedDefinitions.NO_PHOTO)
                {
                  _customer_picture.Picture = SharedDefinitions.SPACE;
                  _customer_picture.LastUpdate = SharedDefinitions.SPACE;
                }


                Response.CustomerPicture = _customer_picture;
              }
            }
            else
            {
              Status = HttpStatusCode.NotAcceptable;
            }
          }
        }
        else
        {
          Status = HttpStatusCode.Unauthorized;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Error(string.Format("InHouseApi.Business.CustomerPictureBusinessLogic.GetCustomerPicture: SiteId: {0}, CustomerId: {1}", SiteId, CustomerId));
        _is_ok = false;
      }
      return _is_ok;
    } // GetCustomerPicture

    #endregion // Public Methods

    #region " Private Methods "

    private static Boolean DB_GetCustomerPicture(Int64 CustomerId, SqlTransaction SqlTrx, out HttpStatusCode Status, out CustomerPicture _customer_picture)
    {
      StringBuilder _sb;
      string _query;
      Int16 _site_id = 0;

      _sb = new StringBuilder();
      _customer_picture = new CustomerPicture();
      Status = HttpStatusCode.InternalServerError;

      try
      {

        _sb.AppendLine("SELECT A.AC_ACCOUNT_ID, AP.APH_PHOTO, AP.APH_LAST_UPDATE ");
        _sb.AppendLine("FROM ACCOUNTS A");
        _sb.AppendLine("LEFT JOIN ACCOUNT_PHOTO AP");
        _sb.AppendLine("	ON A.AC_ACCOUNT_ID = AP.APH_ACCOUNT_ID");
        _sb.AppendLine("WHERE A.AC_ACCOUNT_ID = @pCustomerId");

        _query = _sb.ToString();
        using (SqlCommand _cmd = new SqlCommand(_query, SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.Int).Value = CustomerId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              _customer_picture.CustomerId = (Int64)_sql_reader["AC_ACCOUNT_ID"];
              _customer_picture.Picture = (_sql_reader["APH_PHOTO"].Equals(DBNull.Value) ? SharedDefinitions.NO_PHOTO : Convert.ToBase64String((byte[])_sql_reader["APH_PHOTO"]));
              _customer_picture.LastUpdate = (string)_sql_reader["APH_LAST_UPDATE"].ToString();
              if (!Int16.TryParse(GeneralParam.GetString("Site", "Identifier"), out _site_id))
                _site_id = 0;
              _customer_picture.SiteId = _site_id;

              if (_customer_picture.Picture == SharedDefinitions.NO_PHOTO)
                Status = HttpStatusCode.NoContent;
              else
                Status = HttpStatusCode.OK;
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.DB_GetCustomerPicture-> CustomerId: {0}", CustomerId));
        Log.Exception(_ex);
      }
      return false;
    } // DB_GetCustomerPicture

    private static HttpStatusCode GetCustomerPicture_HttpStatusCode(CustomerPicture CustPict, Int64 CustomerId)
    {
      HttpStatusCode _http_status_code = HttpStatusCode.InternalServerError;
      
      if (string.IsNullOrEmpty(CustPict.Picture) || CustPict.Picture == SharedDefinitions.NO_PHOTO)
      {
        _http_status_code = HttpStatusCode.NoContent;

        if ((!CustPict.CustomerId.Equals(CustomerId)))
        {
          _http_status_code = HttpStatusCode.NotFound;
        }
      }
      else
      {
        _http_status_code = HttpStatusCode.OK;
      }

      return _http_status_code;
    } // GetCustomerPicture_HttpStatusCode

    private InHouseCustomerPictureResponse DoCallChildSite_GetCustomerPicture(short SiteId, long CustomerId)
    {
      InHouseCustomerPictureResponse _child_response = new InHouseCustomerPictureResponse();
      DataTable _site_url_list = SharedDefinitions.GetSiteListIPPortPairs(SiteId);

      foreach (DataRow _dr in _site_url_list.Rows)
      {
        try
        {
          _child_response = SharedDefinitions.DoCallChildSite<InHouseCustomerPictureResponse>(string.Format("https://{0}:{1}/InHouseApi/{2}/customerPicture/0/{3}", _dr["ss_ip"], _dr["ss_port"], MethodVersion, CustomerId), SiteId.ToString(), _dr["SS_ID"].ToString());
          if (_child_response != null && _child_response.CustomerPicture != null)
          {
            base.SetCurrentIPPortPairAsCurrent(_dr["SS_ID"].ToString(), _dr["SS_SITE_ID"].ToString());
            break;
          }
        }
        catch (Exception _ex)
        {
          System.Diagnostics.Debugger.Log(1, "", string.Format("DoCallChildSite_GetCustomerPicture :: Error: {0}", _ex.Message));
        }
       
      }
      
      
      return _child_response;
    } // DoCallChildSite_GetCustomerPicture

    #endregion // Private Methods

  } // CustomerPictureBusinessLogic

} // InHouseApi.Business
