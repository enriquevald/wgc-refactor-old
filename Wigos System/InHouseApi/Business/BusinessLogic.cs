﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BusinessLogic.cs
// 
//   DESCRIPTION: BusinessLogic class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using InHouseApi.Classes.Common;
using InHouseApi.Classes.Response;
using InHouseApi.Shared;
using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using WSI.Common;

namespace InHouseApi.Business
{
    // TODO: Refactor. Must KILL static uses and review use of public properties and private data

    public class BusinessLogic
    {
        #region Properties

        public String IP { get; set; }
        public String MethodVersion { get; set; }

        public X509Certificate2 ClientCertificate { get; set; }
        public Boolean ClientAuthorization { get; set; }

        #endregion

        #region Public Methods

        public bool GetEvents(Int64 eventId, out InHouseEventResponse response, out HttpStatusCode status)
        {
            bool result = false;

            EventsBusinessLogic eventBusinessLogic = new EventsBusinessLogic
            {
                MethodVersion = this.MethodVersion,
                ClientCertificate = this.ClientCertificate,
                ClientAuthorization = this.ClientAuthorization
            };

            response = new InHouseEventResponse();
            status = HttpStatusCode.InternalServerError;

            try
            {
                result = eventBusinessLogic.GetEvents(eventId, ref response, ref status);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                if (response == null)
                {
                    response = new InHouseEventResponse();
                    response.Events.Add(
                        new Event
                        {
                            CustomerId = 0,
                            EventDateTime = SharedDefinitions.SPACE,
                            Id = 0,
                            Inserted = SharedDefinitions.SPACE,
                            SiteId = 0,
                            Type = 0
                        });
                }
            }

            return result;
        }

        public bool GetStatus(Int16 siteId, string host, Int64 port, out InHouseStatusResponse response, out HttpStatusCode status)
        {
            status = HttpStatusCode.InternalServerError;

            bool result = false;
            StatusBusinessLogic statusBusinessLogic = new StatusBusinessLogic
            {
                MethodVersion = this.MethodVersion,
                ClientCertificate = this.ClientCertificate,
                ClientAuthorization = this.ClientAuthorization
            };
            response = new InHouseStatusResponse();

            try
            {
                result = statusBusinessLogic.GetStatus(siteId, host, Convert.ToInt16(port), ref response, ref status);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                if (response == null)
                {
                    response = new InHouseStatusResponse
                    {
                        Status = new Status
                        {
                            SiteId = 0,
                            Ip = SharedDefinitions.SPACE,
                            Port = 0,
                            LastTimeUsed = DateTime.MinValue
                        }
                    };
                }
            }

            return result;
        }

        public bool GetDemographicData(Int16 siteId, Int64 customerId, out InHouseCustomerResponse response, out HttpStatusCode status)
        {
            bool result = false;
            DemographicDataBusinessLogic demographicDataBusinessLogic = new DemographicDataBusinessLogic
            {
                MethodVersion = this.MethodVersion,
                ClientCertificate = this.ClientCertificate,
                ClientAuthorization = this.ClientAuthorization
            };

            response = new InHouseCustomerResponse();
            response.customer = new Customer();
            status = HttpStatusCode.InternalServerError;

            try
            {
                result = demographicDataBusinessLogic.GetDemographicData(siteId, customerId, ref response, ref status);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                if (response == null)
                {
                    response = new InHouseCustomerResponse();
                    response.customer = new Customer
                    {
                        address = new Address()
                        {
                            adr1 = SharedDefinitions.SPACE,
                            adr2 = SharedDefinitions.SPACE,
                            adr3 = SharedDefinitions.SPACE,
                            city = SharedDefinitions.SPACE,
                            country = SharedDefinitions.SPACE,
                            zip = SharedDefinitions.SPACE
                        },
                        customerId = customerId,
                        lastUpdate = SharedDefinitions.SPACE,
                        person = new Person
                        {
                            birthdate = SharedDefinitions.SPACE,
                            first_name = SharedDefinitions.SPACE,
                            second_name = SharedDefinitions.SPACE,
                            surnames = SharedDefinitions.SPACE
                        }
                    };
                    response.customer.documents = new Document[]
                    {
                        new Document
                        {
                            countryCode = SharedDefinitions.SPACE, 
                            documentName = SharedDefinitions.SPACE, 
                            value = SharedDefinitions.SPACE, 
                            wigosCode = 0
                        }
                    };
                    response.customer.emails = new string[] { SharedDefinitions.SPACE };
                }
            }

            return result;
        }

        public bool GetCustomerPicture(Int16 siteId, Int64 customerId, out InHouseCustomerPictureResponse response, ref HttpStatusCode status)
        {
            // TODO: Refactor. Review response use and refactor it with class properties

            bool result = false;
            CustomerPictureBusinessLogic customerPictureBusinessLogic = new CustomerPictureBusinessLogic
            {
                MethodVersion = this.MethodVersion,
                ClientCertificate = this.ClientCertificate,
                ClientAuthorization = this.ClientAuthorization
            };

            response = new InHouseCustomerPictureResponse();
            response.CustomerPicture = new CustomerPicture();
            status = HttpStatusCode.NotFound;

            try
            {
                result = customerPictureBusinessLogic.GetCustomerPicture(siteId, customerId, ref response, ref status);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                if (response == null)
                {
                    response = new InHouseCustomerPictureResponse();
                    response.CustomerPicture = new CustomerPicture
                    {
                        CustomerId = customerId,
                        SiteId = siteId,
                        Picture = SharedDefinitions.SPACE,
                        LastUpdate = SharedDefinitions.SPACE
                    };
                }
            }

            return result;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Output specified 
        /// </summary>
        /// <param name="logMessage">Message to log</param>
        /// <remarks>TODO: Change this 'Temporary Method' to log through out the OutPut Console by File logging and make it non static.</remarks>
        public static void OutputLog(string logMessage)
        {
            Log.Message(logMessage);
        }

        #endregion
    }
}
