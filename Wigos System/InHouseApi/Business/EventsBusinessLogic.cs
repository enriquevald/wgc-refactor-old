﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: EventsBusinessLogic.cs
// 
//   DESCRIPTION: Events Business Logic class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
// 03-NOV-2017 JBM    WIGOS-5304, WIGOS-5302, WIGOS-5298
// 18-MAY-2018 XGJ    WIGOS-11987 - Error in InHouseApi --> EXCEPTION: Column 'SS_SS_SITE_ID' does not belong to table SITE_
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Linq;
using System.Collections;
using InHouseApi.Classes.Request;
using InHouseApi.Classes.Response;
using InHouseApi.Classes.Common;
using InHouseApi.Shared;
using WSI.Common;
using System.Security.Cryptography.X509Certificates;

namespace InHouseApi.Business
{
  internal class EventsBusinessLogic : BaseBusinessLogic
  {

    #region " Variables "

    private List<Event> _list_events = new List<Event>() { };
    private DataTable _datatable_sites;
    private DataTable _datatable_site_services;

    #endregion

    #region " Constructor "

    public EventsBusinessLogic()
    {
    } // EventsBusinessLogic

    #endregion // Constructor


    #region " Properties "

    public string MethodVersion { get; set; } // MethodVersion
    public X509Certificate2 ClientCertificate { get; set; } // ClientCertificate
    public Boolean ClientAuthorization { get; set; } // ClientAuthorization

    #endregion


    #region " Private Methods "

    private Boolean DB_GetEvents(Int64 EventId, SqlTransaction SqlTrx, out HttpStatusCode Status)
    {
      StringBuilder _sb;

      Status = HttpStatusCode.InternalServerError;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("     SELECT  TOP(100)  IHE_EVENT_ID ID                                                                                    ");
        _sb.AppendLine("             , CONVERT(VARCHAR(30), IHE_EVENT_INSERTED_IN_LOCAL_TIME, 126) + CONVERT(VARCHAR(3), DATEDIFF(HH,IHE_EVENT_INSERTED_IN_LOCAL_TIME,IHE_EVENT_INSERTED_IN_UTC_TIME)) INSERTED       ");
        _sb.AppendLine("             , IHE_SITE_ID SITEID                                                                                 ");
        _sb.AppendLine("             , CONVERT(VARCHAR(30), IHE_EVENT_DATETIME_IN_LOCAL_TIME, 126) + CONVERT(VARCHAR(3), DATEDIFF(HH,IHE_EVENT_DATETIME_IN_LOCAL_TIME,IHE_EVENT_DATETIME_IN_UTC_TIME)) EVENTDATETIME  ");
        _sb.AppendLine("             , IHE_EVENT_TYPE	TYPE                                                                                ");
        _sb.AppendLine("             , IHE_CUSTOMER_ID	CUSTOMERID                                                                        ");
        _sb.AppendLine("       FROM    IN_HOUSE_EVENTS                                                                                    ");

        if (EventId != 0)
        {
          _sb.AppendLine("    WHERE    IHE_EVENT_ID >= @paramId                                                                           ");
        }

        _sb.AppendLine("   ORDER BY    1 ASC	                                                                                            ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _cmd.Parameters.Add("@paramId", SqlDbType.Int).Value = EventId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              Event _event;

              _event = new Event();

              _event.Id = _sql_reader.GetInt64(0);
              _event.Inserted = _sql_reader.GetString(1);
              _event.SiteId = _sql_reader.GetInt32(2);
              _event.EventDateTime = _sql_reader.GetString(3);
              _event.Type = _sql_reader.GetInt32(4);
              _event.CustomerId = _sql_reader.GetInt64(5);

              _list_events.Add(_event);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("BusinessLogic.DB_GetEvents-> Id: {0}", EventId));
        Log.Exception(_ex);
      }
      return false;
    } // DB_GetEvents

    private HttpStatusCode GetEvents_HttpStatusCode(Int64 EventId)
    {
      HttpStatusCode _http_status_code = HttpStatusCode.InternalServerError;
      if (_list_events.Count <= 0 && EventId != 0)
      {
        _http_status_code = HttpStatusCode.NotFound;
      }
      else
      {
        if (_list_events.Count <= 0)
        {
          _http_status_code = HttpStatusCode.NoContent;
        }
        else
        {
          _http_status_code = HttpStatusCode.OK;
        }
      }
      return _http_status_code;
    } // GetCustomerPicture_HttpStatusCode

    private static Boolean DB_DelEvents(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  DELETE FROM    IN_HOUSE_EVENTS                                                                                     ");
        _sb.AppendLine("        WHERE    IHE_EVENT_ID <= @lastEventId                                                                        ");
        _sb.AppendLine("          AND    DATEDIFF(HH, CONVERT(DATETIME, IHE_EVENT_INSERTED_IN_LOCAL_TIME), GETDATE()) >= @deleteThreshold    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@lastEventId", SqlDbType.Int).Value = Shared.SharedDefinitions.LastEventId;
          _cmd.Parameters.Add("@deleteThreshold", SqlDbType.Int).Value = GeneralParam.GetInt32("InHouseAPI", "DeleteThreshold");

          _cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

    }
    private static Boolean DB_DelTodayVisits(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  DELETE FROM    TODAY_VISITS                                                                                         ");
        _sb.AppendLine("        WHERE    DATEDIFF(DD, CONVERT(DATETIME, CONVERT(VARCHAR,TV_GAMING_DAY)), GETDATE()) >= @deleteThreshold       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@deleteThreshold", SqlDbType.Int).Value = GeneralParam.GetInt32("InHouseAPI", "DeleteThreshold") / 24; // We divide by 24 to obtain the delete threshold in days

          _cmd.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

    }

    private Boolean GetChildEvents(SqlTransaction SqlTrx)
    {
      foreach (DataRow _site in _datatable_sites.Rows)
      {

        InHouseEventResponse _child_response = new InHouseEventResponse();
        Int16 _site_id;

        if (!Int16.TryParse(_site.ItemArray[0].ToString(), out _site_id))
          _site_id = 0;
        DataTable _site_url_list = SharedDefinitions.GetSiteListIPPortPairs(_site_id);

        foreach (DataRow _dr in _site_url_list.Rows)
        {
          try
          {
            _child_response = SharedDefinitions.DoCallChildSite<InHouseEventResponse>(string.Format("https://{0}:{1}/InHouseApi/{2}/event", _dr["ss_ip"], _dr["ss_port"], MethodVersion), _site_id.ToString(), _dr["SS_ID"].ToString());
            if (_child_response != null && _child_response.Events != null)
            {
              insertChildEventInDB(_child_response.Events, SqlTrx);

              base.SetCurrentIPPortPairAsCurrent(_dr["SS_ID"].ToString(), _dr["SS_SITE_ID"].ToString());
              break;
            }
          }
          catch (Exception _ex)
          {
            System.Diagnostics.Debugger.Log(1, "", string.Format("DoCallChildSite_GetCustomerPicture :: Error: {0}", _ex.Message));
            Log.Exception(_ex);
          }

        }

      }
      return true;
    }

    /**
     *  Method to insert in multite database the site events
     *  Input List of events
     **/
    private static Boolean insertChildEventInDB(List<Event> _list_child_events, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Boolean _return = false;

      foreach (Event _event in _list_child_events)
      {
        try
        {

          DateTime _inserted = DateTime.Parse(_event.Inserted.Substring(0, 19));
          DateTime _inserted_utc = DateTime.Parse(_event.Inserted.Substring(0, 19));
          DateTime _eventDatetime = DateTime.Parse(_event.EventDateTime.Substring(0, 19));
          DateTime _eventDatetime_utc = DateTime.Parse(_event.EventDateTime.Substring(0, 19));

          string[] _event_inserted_str_negative = _event.Inserted.Split('T')[1].Split('-');
          string[] _event_inserted_str_positive = _event.Inserted.Split('T')[1].Split('+');
          double _event_inserted_utc;

          string[] _event_datetime_str_negative = _event.EventDateTime.Split('T')[1].Split('-');
          string[] _event_datetime_str_positive = _event.EventDateTime.Split('T')[1].Split('+');
          double _event_datetime_utc;

          if (_event_inserted_str_negative.Length > 1)
          {
            if (!double.TryParse(_event_inserted_str_negative[1], out _event_inserted_utc))
            {
              _event_inserted_utc = 0.0;
            }
            _event_inserted_utc = _event_inserted_utc * -1.0;
          }
          else
          {
            if (_event_inserted_str_positive.Length > 1)
            {
              if (!double.TryParse(_event_inserted_str_positive[1], out _event_inserted_utc))
              {
                _event_inserted_utc = 0.0;
              }
            }
            else
            {
              _event_inserted_utc = 0.0;
            }
          }

          _inserted_utc = _inserted_utc.AddHours(_event_inserted_utc);


          if (_event_datetime_str_negative.Length > 1)
          {
            if (!double.TryParse(_event_datetime_str_negative[1], out _event_datetime_utc))
              _event_datetime_utc = 0.0;
            _event_datetime_utc = _event_datetime_utc * -1.0;
          }
          else
          {
            if (_event_datetime_str_positive.Length > 1)
            {
              if (!double.TryParse(_event_datetime_str_positive[1], out _event_datetime_utc))
                _event_datetime_utc = 0.0;
            }
            else
            {
              _event_datetime_utc = 0.0;
            }
          }

          _eventDatetime_utc = _eventDatetime_utc.AddHours(_event_datetime_utc);
          
          _sb = new StringBuilder();

          _sb.AppendLine("  IF NOT EXISTS  ( SELECT   1                                                                                                                         ");
          _sb.AppendLine("                     FROM   IN_HOUSE_EVENTS                                                                                                           ");
          _sb.AppendLine("                    WHERE	  IHE_EVENT_SITE_ID = @eventSiteID                                                                                          ");
          _sb.AppendLine("                      AND   IHE_SITE_ID = @siteID                                                                                                     ");
          _sb.AppendLine("                  )                                                                                                                                   ");
          _sb.AppendLine("          BEGIN                                                                                                                                       ");
          _sb.AppendLine("    INSERT INTO    IN_HOUSE_EVENTS(IHE_SITE_ID, IHE_EVENT_INSERTED_IN_LOCAL_TIME, IHE_EVENT_INSERTED_IN_UTC_TIME, " +
                                           "IHE_EVENT_DATETIME_IN_LOCAL_TIME, IHE_EVENT_DATETIME_IN_UTC_TIME, IHE_EVENT_TYPE, IHE_CUSTOMER_ID, IHE_EVENT_SITE_ID)               ");
          _sb.AppendLine("         VALUES    (@siteID, @eventInsertedDatetime, @eventInsertedDatetimeUTC, @eventDatetime, @eventDatetimeUTC, @type, @customerID, @eventSiteID)  ");
          _sb.AppendLine("            END                                                                                                                                       ");


          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@siteID", SqlDbType.Int).Value = _event.SiteId;
            _cmd.Parameters.Add("@eventInsertedDatetime", SqlDbType.DateTime).Value = _inserted;
            _cmd.Parameters.Add("@eventInsertedDatetimeUTC", SqlDbType.DateTime).Value = _inserted_utc;
            _cmd.Parameters.Add("@eventDatetime", SqlDbType.DateTime).Value = _eventDatetime;
            _cmd.Parameters.Add("@eventDatetimeUTC", SqlDbType.DateTime).Value = _eventDatetime_utc;
            _cmd.Parameters.Add("@type", SqlDbType.Int).Value = _event.Type;
            _cmd.Parameters.Add("@customerID", SqlDbType.BigInt).Value = _event.CustomerId;
            _cmd.Parameters.Add("@eventSiteID", SqlDbType.BigInt).Value = _event.Id;

            _cmd.ExecuteNonQuery();

          }
        }
        catch (Exception _ex)
        {
          _return = false;
          throw _ex;
        }

        _return = true;
      }
      return _return;      
    }

    #endregion // Private Methods

    #region " Public Methods "
    public Boolean GetEvents(Int64 EventId, ref InHouseEventResponse Response, ref HttpStatusCode Status)
    {
      Boolean _is_ok = false;
      Boolean _delete_ok = false;
      Boolean _delete_tv_ok = false;

      try
      {
        if ((ClientAuthorization && ClientCertificate != null) || !ClientAuthorization)
        { 
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (Response.Events == null)
              Response.Events = new List<Event>();
            // I'm multisite
            if (SharedDefinitions.IsMultisite(out _datatable_site_services, out _datatable_sites))
            {
              // Refill our events table with sites events
              GetChildEvents(_db_trx.SqlTransaction);

              // Delete events of in_house_events table
              _delete_ok = DB_DelEvents(_db_trx.SqlTransaction);

              // Get events of database
              _is_ok = DB_GetEvents(EventId, _db_trx.SqlTransaction, out Status);

              // Send response
              
              for (int i = 0; i < _list_events.Count && i < 100; i++)
              {
                Response.Events.Add(_list_events[i]);
                // We obtain the last event id
                Shared.SharedDefinitions.LastEventId = _list_events[i].Id;
              }

              if (_delete_ok && _is_ok)
              {
                _db_trx.Commit();
              }

              if (_delete_ok && _is_ok)
              {
                _db_trx.Commit();
              }
             }
            // I'm site
            else
            {
              // Delete events of today_visits table
              _delete_tv_ok = DB_DelTodayVisits(_db_trx.SqlTransaction);

              // Delete events of in_house_events table
              _delete_ok = DB_DelEvents(_db_trx.SqlTransaction);

              // Get events of database
              _is_ok = DB_GetEvents(EventId, _db_trx.SqlTransaction, out Status);

              // Send response
              for (int i = 0; i < _list_events.Count && i < 100; i++)
              {
                Response.Events.Add(_list_events[i]);
                // We obtain the last event id
                Shared.SharedDefinitions.LastEventId = _list_events[i].Id;
              }

              if (_delete_ok && _is_ok && _delete_tv_ok)
              {
                _db_trx.Commit();
              }
            }
            Status = GetEvents_HttpStatusCode(EventId);
          }
        }
        else
        {
          Status = HttpStatusCode.Unauthorized;
          _is_ok = false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Message(String.Format("InHouseApi.Business.EventsBusinessLogic.GetEvents: Error: {0} ", EventId));
        _is_ok = false;
      }
      return _is_ok;
    } // GetEvents

    #endregion // Public Methods

  } // EventsBusinessLogic
} // InHouseApi.Business
