﻿//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: BaseController.cs
// 
//   DESCRIPTION: Base Controller, common functionalities for all InHouseAPI controllers.
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 08-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-FEB-2018 OMC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;

namespace InHouseApi.Business
{
  internal class BaseBusinessLogic
  {

    #region     "Constructor"
    public BaseBusinessLogic()
    {

    } // BaseBusinessLogic
    #endregion  "Constructor"


    #region "Public Methods"
    public bool SetCurrentIPPortPairAsCurrent(string SiteIdPK, string SiteId)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  UPDATE SITE_SERVICES                            ");
        _sb.AppendLine("    SET SS_LAST_TIME_USED = @lastTimeUsed         ");
        _sb.AppendLine("    WHERE    SS_ID = @Id                          ");
        _sb.AppendLine("      AND    SS_SITE_ID = @SiteId                 ");

        using (DB_TRX _db_trx = new DB_TRX())
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _cmd.Parameters.Add("@lastTimeUsed", SqlDbType.DateTime).Value = DateTime.Now;
          _cmd.Parameters.Add("@Id", SqlDbType.Int).Value = SiteIdPK;
          _cmd.Parameters.Add("@SiteId", SqlDbType.Int).Value = SiteId;

          _cmd.ExecuteNonQuery();
          _db_trx.SqlTransaction.Commit();
          return true;
        }
        
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    } // SetCurrentIPPortPairAsCurrent
    #endregion // "Public Methods"
  } // BaseBusinessLogic
} // InHouseApi.Business