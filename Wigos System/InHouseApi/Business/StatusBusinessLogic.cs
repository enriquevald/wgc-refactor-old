﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StatusBusinessLogic.cs
// 
//   DESCRIPTION: StatusBusinessLogic class
// 
//        AUTHOR: Roberto Afonso
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-FEB-2018 RAR    First version.
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using InHouseApi.Classes.Response;
using InHouseApi.Classes.Common;
using WSI.Common;
using System.Security.Cryptography.X509Certificates;
using InHouseApi.Shared;

namespace InHouseApi.Business
{
    // TODO: Refactor. Must KILL static uses and review use of public properties and private data
    //       Other logics to refactor: certificate validation check

    internal class StatusBusinessLogic : BaseBusinessLogic
    {
      
        #region Data Members

        private Status statusData;

        #endregion

        #region Properties

        public string MethodVersion { get; set; }
        public X509Certificate2 ClientCertificate { get; set; }
        public Boolean ClientAuthorization { get; set; }

        #endregion

        #region Public Methods

        public bool GetStatus(Int16 SiteId, string Host, Int16 Port, ref InHouseStatusResponse Response, ref HttpStatusCode Status)
        {
          DataTable _datatable_sites;
          DataTable _datatable_site_services;
          Boolean _is_ok = false;
          Int16 _current_site_id = 0;
          Status _status;

          _status = null;

          try
          {
            if ((ClientAuthorization && ClientCertificate != null) || !ClientAuthorization)
            {
              //**************************
              //IsCenter or requested site
              //**************************
              if (!SharedDefinitions.IsMultisite(out _datatable_site_services, out _datatable_sites))
              {
                if (!Int16.TryParse(GeneralParam.GetString("Site", "Identifier"), out _current_site_id))
                  _current_site_id = 0;

                if (SiteId == 0 || SiteId == _current_site_id)
                {
                  //Return data!
                  _status = new Status()
                  {
                    Ip = Host
                    ,
                    Port = Port
                    ,
                    LastTimeUsed = DateTime.Now
                    ,
                    SiteId = SiteId
                  };
                  _is_ok = true;
                  Status = GetStatusData_HttpStatusCode(_status);
                  Response.Status = _status;
                }
                else
                {
                  //A different site is call
                  Status = HttpStatusCode.NotAcceptable;
                }
              }

              //*********
              //MULTISITE
              //*********
              else
              {
                if (SiteId == 0)
                {
                  Status = GetStatusData_HttpStatusCode(_status);
                  Response.Status = new Status() { 
                      Ip=Host
                    , Port=Port
                    , LastTimeUsed=DateTime.Now
                    , SiteId=SiteId 
                  };
                }else{
                  //The site exist on multisite's site_list.
                  Status = HttpStatusCode.NotFound;
                  if (SharedDefinitions.IsValidMultisiteSite(_datatable_site_services, SiteId))
                  {
                    Response = DoCallChildSite_GetStatusDataFromSite(SiteId);
                    _status = Response.Status;
                    Status = GetStatusData_HttpStatusCode(_status);
                  }else{
                    //Site not exists on multisite's site_list.
                    Status = HttpStatusCode.NotAcceptable;
                  }
                }
              }

              if (_is_ok)
              {
                //---------------------------------------------------
                // FAKE DATA GENERATION
                //---------------------------------------------------
                if (_status == null)
                {
                  Status = HttpStatusCode.NotFound;
                  _is_ok = false;
                }
                else
                {
                  Response.Status = _status;
                  Status = HttpStatusCode.OK;
                  _is_ok = true;
                }
              }
            }
            else
            {
              Status = HttpStatusCode.Unauthorized;
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);
            Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetDemographicData: SiteId: {0}", SiteId));
            _is_ok = false;
          }
          return _is_ok;
            //bool result = false;

            //try
            //{
            //    if ((this.ClientAuthorization && this.ClientCertificate != null) || !this.ClientAuthorization)
            //    {
            //        using (DB_TRX dbTrx = new DB_TRX())
            //        {
            //            this.statusData = null;
            //            result = this.DB_GetStatus(siteId, host, port, dbTrx.SqlTransaction, out status);
            //            response.Status = this.statusData;
            //        }
            //    }
            //    else
            //    {
            //        status = HttpStatusCode.Unauthorized;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Log.Exception(ex);
            //    Log.Message(string.Format("InHouseApi.Business.StatusBusinessLogic.GetStatus: Error: {0} ", siteId));
            //    result = false;
            //}

            //return result;
        } // GetStatus


        private bool GetStatusDataFromMultisiteDB(Int16 SiteId, string Host, Int16 Port, out HttpStatusCode Status, out Status _status)
        {
          DataTable _status_data;
          DataRow _dr;
          SqlTransaction _sql_trx;

          Status = HttpStatusCode.InternalServerError;
          _status = new Classes.Common.Status();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sql_trx = _db_trx.SqlTransaction;

            if (!DB_GetStatusData(SiteId, Host, Port, _sql_trx, out _status_data))
            {
              Status = HttpStatusCode.NotFound;
              return false;
            }

            if ((_status_data == null) || (_status_data.Rows.Count == 0))
            {
              Status = HttpStatusCode.NotFound;
              return false;
            }

            _dr = _status_data.Rows[0];

            try
            {
              _status = GetStatusData(_dr, SiteId, Host, Port, _db_trx.SqlTransaction);

              Status = HttpStatusCode.OK;

              return true;
            }
            catch (Exception _ex)
            {
              Log.Error(string.Format("InHouseApi.Business.DemographicDataBusinessLogic.GetDemographicDataFromMultisiteDB -> SiteId: {0}", SiteId));
              Log.Exception(_ex);
            }
          }

          return false;
        }

        private Status GetStatusData(DataRow _dr, Int16 SiteId, string Host, short Port, SqlTransaction sqlTransaction)
        {
          Status _status;
          _status = new Status(){
            SiteId = Convert.ToInt16(_dr["SS_SITE_ID"]),
            Ip = _dr["SS_IP"].ToString(),
            Port = Convert.ToInt16(_dr["SS_PORT"]),
            LastTimeUsed = Convert.ToDateTime(_dr["SS_LAST_TIME_USED"])
          };
          return _status;
        }

        #endregion

        #region Private Methods

        private bool DB_GetStatusData(Int16 SiteId, string Host, Int16 Port, SqlTransaction SqlTrx, out DataTable StatusTable)
        {
          bool result = false;

          StatusTable = new DataTable("STATUS_DATA");

            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("SELECT ss_id, ss_ip, ss_port, ss_last_time_used");
                stringBuilder.AppendLine("FROM site_services");
                stringBuilder.AppendLine("WHERE ss_site_id = @siteId AND ss_ip = @host AND ss_port = @port");

                using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), SqlTrx.Connection, SqlTrx))
                {
                  sqlCommand.Parameters.Add("@siteId", SqlDbType.BigInt).Value = SiteId;
                    sqlCommand.Parameters.Add("@host", SqlDbType.VarChar).Value = Host;
                    sqlCommand.Parameters.Add("@port", SqlDbType.VarChar).Value = Port.ToString();
                    using (SqlDataAdapter _da = new SqlDataAdapter(sqlCommand))
                    {
                      _da.Fill(StatusTable);

                      return true;
                    }
                }
            }
            catch (Exception ex)
            {
              Log.Error(string.Format("BusinessLogic.DB_GetEvents-> Id: {0}", SiteId));
              Log.Exception(ex);
            }

            return result;
        } // DB_GetStatus

        /// <summary>
        /// Get status data from Site
        /// </summary>
        /// <param name="SiteId"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        private InHouseStatusResponse DoCallChildSite_GetStatusDataFromSite(Int16 SiteId)
        {
          InHouseStatusResponse _child_response = new InHouseStatusResponse();
          DataTable _site_url_list = SharedDefinitions.GetSiteListIPPortPairs(SiteId);

          foreach (DataRow _dr in _site_url_list.Rows)
          {
            try
            {
              _child_response = SharedDefinitions.DoCallChildSite<InHouseStatusResponse>(string.Format("https://{0}:{1}/InHouseApi/{2}/status/0", _dr["ss_ip"], _dr["ss_port"], this.MethodVersion), SiteId.ToString(), _dr["SS_ID"].ToString());
              if (_child_response != null && _child_response.Status != null)
              {
                base.SetCurrentIPPortPairAsCurrent(_dr["SS_ID"].ToString(), _dr["SS_SITE_ID"].ToString());
                break;
              }
            }
            catch (Exception _ex)
            {
              System.Diagnostics.Debugger.Log(1, "", string.Format("DoCallChildSite_GetStatusDataFromSite :: Error: {0}", _ex.Message));
            }

          }

          return (_child_response == null ? new InHouseStatusResponse() { Status=new Status() } : _child_response);
        } // InHouseStatusResponse

        /// <summary>
        /// Get Http status code
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        private static HttpStatusCode GetStatusData_HttpStatusCode(Status status)
        {
            return HttpStatusCode.OK;
        }
        #endregion

    } // StatusBusinessLogic : BaseBusinessLogic
  } // InHouseApi.Business