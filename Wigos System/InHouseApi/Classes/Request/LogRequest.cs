﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LogRequest.cs
// 
//   DESCRIPTION: LogRequest class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;

namespace InHouseApi.Classes.Request
{
    public class LogRequest
    {

      private Dictionary<String, String> _m_headers;

      public String Url { get; set; } // Url
      public Dictionary<String, String> Headers { get { return _m_headers; } } // Headers
      public String Method { get; set; } // Method
      public Object Body { get; set; } // Body
      public X509Certificate2 ClientCertificate { get; set; } // ClientCertificate
      
      public void AddHeaders(string _key, string _value) 
      {
        if (_m_headers == null)
          _m_headers = new Dictionary<string, string>();
        _m_headers.Add(_key, _value);
      } // AddHeaders

      public void ModifyHeader(string _key, string _value)
      {
        if (_m_headers == null)
          _m_headers = new Dictionary<string, string>();
        _m_headers[_key.ToString().ToLower()] += string.Format("|{0}", _value);
      } // ModifyHeader
    } // LogRequest
  } // InHouseApi.Classes.Request
