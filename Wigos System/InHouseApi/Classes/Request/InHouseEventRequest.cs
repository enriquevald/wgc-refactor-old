﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InHouseEventRequest.cs
// 
//   DESCRIPTION: InHouseEventRequest class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InHouseApi.Classes.Common;

namespace InHouseApi.Classes.Request
{
  public class InHouseEventRequest
  {
    public Event Event { get; set; } // Event 
  } // InHouseEventRequest
} // InHouseApi.Classes.Request