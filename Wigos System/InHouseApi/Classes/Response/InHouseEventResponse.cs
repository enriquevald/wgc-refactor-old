﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InHouseEventResponse.cs
// 
//   DESCRIPTION: InHouseEventResponse class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 19-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-OCT-2017 OMC    First version.
// 30-OCT-2017 OMC    Modification to turn class serializable.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InHouseApi.Classes.Common;
using System.Runtime.Serialization;

namespace InHouseApi.Classes.Response
{
  [Serializable]
  //[KnownType(typeof(List<Event>))]
  public class InHouseEventResponse
  {
    public List<Event> Events { get; set; } // Events

  } // InHouseEventResponse

} // InHouseApi.Classes.Response
