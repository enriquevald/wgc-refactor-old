﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LogResponse.cs
// 
//   DESCRIPTION: LogResponse class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InHouseApi.Classes.Response
{
    public class LogResponse
    {
      private Dictionary<String, String> _m_headers;

      public Int32 HttpCode { get; set; } // HttpCode
      public Dictionary<String, String> Headers { get { return _m_headers; } } // Headers
      public Object Body { get; set; } // Body

      public void AddHeaders(string _key, string _value)
      {
        if (_m_headers == null)
          _m_headers = new Dictionary<string, string>();
        _m_headers.Add(_key, _value);
      } // AddHeaders
      public void ModifyHeader(string _key, string _value)
      {
        if (_m_headers == null)
          _m_headers = new Dictionary<string, string>();
        _m_headers[_key.ToString().ToLower()] += string.Format("|{0}", _value);
      } // ModifyHeader

    } // LogResponse
  } // InHouseApi.Classes.Response
