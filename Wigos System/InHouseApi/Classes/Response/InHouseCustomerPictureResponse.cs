﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InHouseCustomerPictureResponse.cs
// 
//   DESCRIPTION: InHouseCustomerPictureResponse class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InHouseApi.Classes.Common;
using System.Runtime.Serialization;

namespace InHouseApi.Classes.Response
{
  [Serializable]
  public class InHouseCustomerPictureResponse
  {
    public CustomerPicture CustomerPicture { get; set; } // CustomerPicture
  }
}
