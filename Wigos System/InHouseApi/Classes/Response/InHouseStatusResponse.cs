﻿using System;

namespace InHouseApi.Classes.Response
{
    using InHouseApi.Classes.Common;

    [Serializable]
    public class InHouseStatusResponse
    {
        public Status Status { get; set; }
    }
}