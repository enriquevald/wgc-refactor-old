﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Status.cs
// 
//   DESCRIPTION: Status class
// 
//        AUTHOR: Roberto Afonso
// 
// CREATION DATE: 07-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-FEB-2018 RAR    First version.
//------------------------------------------------------------------------------

using System;

namespace InHouseApi.Classes.Common
{
    public class Status
    {
        public Int16 SiteId { get; set; }

        public string Ip { get; set; }

        public Int16 Port { get; set; }

        public DateTime LastTimeUsed { get; set; }
    }
}