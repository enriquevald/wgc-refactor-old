﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CustomerPicture.cs
// 
//   DESCRIPTION: CustomerPicture class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InHouseApi.Classes.Common
{
  [Serializable]
  public class CustomerPicture
  {
    public Int16 SiteId { get; set; } // SiteId
    public Int64 CustomerId { get; set; } // CustomerId
    public String Picture { get; set; } // Picture
    public String LastUpdate { get; set; } // LastUpdate
  }
}
