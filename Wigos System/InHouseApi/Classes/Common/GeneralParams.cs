﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GeneralParams.cs
// 
//   DESCRIPTION: GeneralParams class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using WSI.Common;

namespace InHouseApi.Classes.Common
{
  public class GeneralParams
  {
    #region Public Functions
    /// <summary>
    /// Indicates if Feature is enabled or not
    /// </summary>
    /// <returns></returns>
    public static Boolean IsEnabled()
    {
      return GeneralParam.GetBoolean("InHouseAPI", "Enabled", false);
    }
    /// <summary>
    /// Indicates the list of printers that are setup in the system.
    /// List MUST BE pipe ('|') separated.
    /// </summary>
    /// <returns></returns>
    public static Boolean IsReceptionVisitEnabled()
    {
      return GeneralParam.GetBoolean("InHouseAPI", "ReceptionVisitEnabled", false);
    }
    /// <summary>
    /// Indicates lapse of time that thread awaits between each draw printing.
    /// </summary>
    /// <returns></returns>
    public static Boolean IsPlaySessionEnabled()
    {
      return GeneralParam.GetBoolean("InHouseAPI", "PlaySessionEnabled", false);
    }
    /// <summary>
    /// Interval in seconds to start a query to each child site. It can use SITES table to know the IP of each server.
    /// By default, this value is set to 30 seconds.
    /// </summary>
    /// <returns></returns>
    public static Int32 GetAskChildsInterval()
    {
      return GeneralParam.GetInt32("InHouseAPI", "AskChildsInterval", 30);
    }
    /// <summary>
    /// Threshold in hours to delete old data. Every call to visit will delete all data corresponding to a timestamp before send it, so all old and no useful data is never sent. 
    /// For example, if a DeleteThreshold of 3 hours is set, all previous data older than 3 hours from now will not be sent. 
    /// By default, this value is set to 48 hours.
    /// </summary>
    /// <returns></returns>
    public static Int32 GetDeleteThreshold()
    {
      return GeneralParam.GetInt32("InHouseAPI", "DeleteThreshold", 48);
    }
    #endregion
  } // GeneralParams
} // InHouseApi.Classes.Common
