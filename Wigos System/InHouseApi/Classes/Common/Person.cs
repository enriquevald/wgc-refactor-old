﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Person.cs
// 
//   DESCRIPTION: Person class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;

namespace InHouseApi.Classes.Common
{
  public class Person
  {
    public String first_name { get; set; }

    public String second_name { get; set; }

    public String surnames { get; set; }

    public String birthdate { get; set; }
  }
}