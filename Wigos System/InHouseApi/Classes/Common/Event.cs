﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Event.cs
// 
//   DESCRIPTION: Event class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InHouseApi.Classes.Common
{
  public class Event
  {
    public Int64 Id { get; set; } // Id
    public String Inserted { get; set; } // Inserted
    public Int64 SiteId { get; set; } // SiteId
    public String EventDateTime { get; set; } // EventDateTime
    public Int64 Type { get; set; } // Type
    public Int64 CustomerId { get; set; } // CustomerId
  } // Event
} // InHouseApi.Classes.Common