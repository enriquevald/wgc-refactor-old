﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Document.cs
// 
//   DESCRIPTION: Document class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;

namespace InHouseApi.Classes.Common
{
  public class Document
  {
    public Int64 wigosCode { get; set; }

    public String countryCode { get; set; }

    public String documentName { get; set; }

    public String value { get; set; }
  }
}