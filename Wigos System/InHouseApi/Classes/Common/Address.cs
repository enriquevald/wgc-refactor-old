﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Address.cs
// 
//   DESCRIPTION: Address class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------
using System;

namespace InHouseApi.Classes.Common
{
  public class Address
  {
    public String adr1 { get; set; }

    public String adr2 { get; set; }

    public String adr3 { get; set; }

    public String zip { get; set; }

    public String city { get; set; }

    public String country { get; set; }
  }
}