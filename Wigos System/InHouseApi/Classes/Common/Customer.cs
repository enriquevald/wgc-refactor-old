﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Customer.cs
// 
//   DESCRIPTION: Customer class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 20-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;

namespace InHouseApi.Classes.Common
{
  public class Customer
  {
    private ReadOnlyCollection<string> _m_phones;
    private ReadOnlyCollection<string> _m_emails;
    private ReadOnlyCollection<Document> _m_documents;

    public Int64 siteId { get; set; }

    public Int64 customerId { get; set; }

    public Person person { get; set; }

    public Address address { get; set; }

    public string[] phones { get; set; }

    public string[] emails { get; set; }

    public Document[] documents { get; set; }

    public String lastUpdate { get; set; }

  } // Customer
} // InHouseApi.Classes.Common