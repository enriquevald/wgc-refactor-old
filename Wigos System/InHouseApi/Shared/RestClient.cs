﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: RestClient.cs
// 
//   DESCRIPTION: RestClient class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 26-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

public enum HttpVerb
{
  GET,
  POST,
  PUT,
  DELETE
} // HttpVerb

namespace InHouseApi.Shared
{
  public class RestClient
  {
    
    #region " Constants "
    private const string ContentTypeApplicationJSON = "application/json";
    private const string PostDataEmpty = "";
    #endregion

    #region " Properties "
    
    public string EndPoint { get; set; } // EndPoint
    public HttpVerb Method { get; set; } // Method
    public string ContentType { get; set; } // ContentType
    public string PostData { get; set; } // PostData
    public X509Certificate2 ClientCertificate { get; set; } // ClientCertificate
    public string SiteId { get; set; } // SiteId
    public string SiteId_PK { get; set; } // SiteId

    #endregion

    #region " Constructors "
    public RestClient()
    {
      EndPoint = string.Empty;
      Method = HttpVerb.GET;
      ContentType = ContentTypeApplicationJSON;
      PostData = PostDataEmpty;
    } // RestClient 

    #endregion

    #region " Public Methods "

    public string MakeRequest()
    {
      return MakeRequest(string.Empty);
    } // MakeRequestHttpClient
    public string MakeRequest(string parameters)
    {
      string _responseValue = string.Empty;      
      string _thumbprint = string.Empty;
      HttpWebRequest _request;
      try
      {
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        _request = (HttpWebRequest)WebRequest.Create(new System.Uri(EndPoint + parameters));
        if (!string.IsNullOrEmpty(SiteId))
          _thumbprint = GetSiteCertificateThumbPrint(SiteId, SiteId_PK);
        _request.Headers.Clear();
        _request.Method = (string.IsNullOrEmpty(Method.ToString()) ? "GET" : Method.ToString());
        _request.Timeout = Convert.ToInt16(ConfigurationManager.AppSettings.Get("InHouseAPI_ChildRequest_TimeOut")) * 1000;
        _request.KeepAlive = false;
        if (ClientCertificate != null)
          _request.ClientCertificates.Add(ClientCertificate);
        if (!string.IsNullOrEmpty(_thumbprint))
          _request.Headers.Add(HttpRequestHeader.Authorization, string.Format("Bearer {0}", _thumbprint));
        using (WebResponse _response = _request.GetResponse())
        {
        using (Stream _stream = _response.GetResponseStream())         
          {
        using (StreamReader _reader = new StreamReader(_stream))
        {
          _responseValue = _reader.ReadLine();
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Business.BusinessLogic.OutputLog(string.Format("InHouseAPI :: Error Calling to ChildSite, Id: '{0}' with URI: {1}. Message: {2}", SiteId, EndPoint, _ex.Message));
      }
      return _responseValue;
    } // MakeRequestHttpClient

    public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    private string GetSiteCertificateThumbPrint(string SiteId, string IdPK)
          {
      X509Certificate2Collection _certificates = null;
      X509Certificate2 _cert;
      string _thumbprint = string.Empty;
      try
      {
        _certificates = new X509Certificate2Collection();
        if (File.Exists(string.Format(@"client_certificate\Site_{0}_Id_{1}.pfx", SiteId, IdPK)))
        {
          _certificates.Import(string.Format(@"client_certificate\Site_{0}_Id_{1}.pfx", SiteId, IdPK), string.Empty, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);
        if (_certificates.Count > 0)
            {
          _cert = new X509Certificate2(_certificates[0]);
          ClientCertificate = _cert;
          _thumbprint = _cert.Thumbprint;
          if (string.IsNullOrEmpty(_thumbprint))
            Business.BusinessLogic.OutputLog(string.Format(@"'client_certificate\Site_{0}_Id_{1}.pfx' thumbprint not found.", SiteId, IdPK));
            }
        else
        {
          throw new Exception(string.Format(@"Client certificate Not Found on path: 'client_certificate\Site_{0}_Id_{1}.pfx' it cannot connect with service in url: {2}.", SiteId, IdPK, EndPoint));
          }
        }
        else 
        {
          Business.BusinessLogic.OutputLog(string.Format(@"InHouseAPI :: 'client_certificate\Site_{0}_Id_{1}.pfx' file not found. Must be present to be able to make requests to the site '{0}'.", SiteId, IdPK));
        }
      }
      catch (Exception _ex)
      {
        throw _ex;
      }
      return _thumbprint.ToLower();
    } // GetSiteCertificateThumbPrint

    #endregion

  } // RestClient

} // InHouseApi.Shared