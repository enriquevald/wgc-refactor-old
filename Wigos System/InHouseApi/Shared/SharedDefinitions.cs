﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SharedDefinitions.cs
// 
//   DESCRIPTION: Shared Definitions class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 02-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;

namespace InHouseApi.Shared
{
  public class SharedDefinitions
  {

    #region " Constants "
    
    public const string NO_PHOTO = "NO_PHOTO";
    public const string SPACE = " ";
    #endregion
  
    #region " Private Variables "

    private static Int64 _m_LastEventId; // m_LastEventId

    #endregion // Private Variables

    #region " Internal Static Methods "

    internal static bool IsMultisite(out DataTable SiteServices, out DataTable Sites)
    {
      bool _is_multisite = false;
      string _lang = string.Empty;
      bool _site_selector = false;

      WSI.Common.WGDB.ReadSiteMultiSite(out _is_multisite, out Sites, out _lang, out _site_selector);
      WSI.Common.WGDB.ReadSiteMultiSite_SiteServices(out _is_multisite, out SiteServices);

      return _is_multisite;
    }  // IsMultisite

    internal static DataTable GetSiteListIPPortPairs(short SiteId)
    {
      DataTable _datatable_sites;
      DataTable _datatable_returned;
      DataTable _datatable_site_services;

      IsMultisite(out _datatable_site_services, out _datatable_sites);

      _datatable_returned = new DataTable();
      _datatable_returned = _datatable_site_services.Clone();

      if (_datatable_site_services != null && _datatable_site_services.Rows.Count > 0)
      {
        foreach (DataRow _dr in _datatable_site_services.Select(string.Format("SS_SITE_ID={0}", SiteId),"SS_LAST_TIME_USED DESC"))
      {
          if (FlagsHelper.IsSet<SiteServices>((SiteServices)_dr["SS_SITE_SERVICES_RELATED"], SiteServices.IN_HOUSE_API))
            _datatable_returned.ImportRow(_dr);
        }
      }
      _datatable_returned.AcceptChanges();
      return _datatable_returned;
    } // GetLastKnownIP

    internal static Boolean IsValidMultisiteSite(DataTable SitesDatatable, Int16 SiteId)
    {
      Boolean _is_valid = false;

      if ((SitesDatatable != null) && (SitesDatatable.Rows.Count > 0))
      {
        _is_valid = (SitesDatatable.Select(string.Format("SS_SITE_ID={0}", SiteId)).Length > 0);
      }

      return _is_valid;
    } // IsValidMultisiteSite

    internal static T DoCallChildSite<T>(string _uri, string _site_id = null, string _site_id_primary_key = null)
    {
      InHouseApi.Shared.RestClient rc;
      string _json = string.Empty;
      object _result;
      try
      {

        _result = typeof(T);
        //Setup the RestClient to make client call.
        rc = new Shared.RestClient() { EndPoint = _uri, ContentType = "application/json", Method = HttpVerb.GET, PostData = string.Empty, SiteId = _site_id, SiteId_PK = _site_id_primary_key };

        //Call WebAPI & set Casted Result!
        _json = rc.MakeRequest();
        _result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(_json);
      }
      catch (Exception ex)
      {
        throw ex;
      }

      return (T)_result;
    } // DoCallChildSite

    #endregion // Internal Static Methods

    #region " Public Static Methods "

    public static Int64 LastEventId
    {
      get 
      {
        return _m_LastEventId;
      }
      set
      {
        _m_LastEventId = value;
      }
    } // LastEventId

    #endregion // Public Static Methods

  } // SharedDefinitions

} // InHouseApi.Shared