﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Protocols_Service.cs
// 
//   DESCRIPTION: Procedures for Protocols to run as a service
// 
//        AUTHOR: Enric Tomas (Header only)
// 
// CREATION DATE: 17-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-OCT-2017 ETP    PBI 29415:WRKP TITO - WIGOS Redemption Kiosk Protocol
// ----------- ------ ----------------------------------------------------------

using KioskRedemptionWebApi;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WigosHubCommon;

using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Common.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Bootstrapper;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services;
using WinSys.Wigos.Protocols.S2S.Gsa.Integration.Services.V1_2_2;

using WSI.Common;
using ILogger = WigosHubCommon.ILogger;
using System.Configuration.Install;
using System.ServiceProcess;
using CashIO;

public partial class Protocols_Service : CommonService
{
  private const string VERSION = "18.039";
  private const string SVC_NAME = "Protocols_Service";
  private const string PROTOCOL_NAME = "WSI.Protocols";
  private const int SERVICE_PORT = 13004;
  private const Boolean STAND_BY = false;
  private const string LOG_PREFIX = "WSI.Protocols";
  private const string CONFIG_FILE = "WSI_Protocols.Configuration";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE;

	private IUnityContainer gsaS2sContainer;
	private IS2SGsaService gsaS2sService;

  public static void Main(String[] Args)
  {
    Protocols_Service _this_service;

    _this_service = new Protocols_Service();
    _this_service.Run(Args);
  }

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
    BigIncrementsData.LoadData = true;
    OperationVoucherParams.LoadData = true;

  } // SetParameters

  private static void RegisterDependenciesInIoC()
  {
    //IoC.Container.RegisterInstance<ILogger>(new WigosLogger(), new ContainerControlledLifetimeManager());
    //IoC.Container.RegisterType<IFileSystem, FileSystem>();
    //IoC.Container.RegisterType<IFileSystemDirectory, FileSystemDirectory>();
    //IoC.Container.RegisterType<IInterchangeSession, SessionInterchange.SessionInterchange>();
    //IoC.Container.RegisterType<IInterchangeEvent, InterchangeEvent>();
    //IoC.Container.RegisterType<IInterchangeMeter, InterchangeMeter>();
    ///IoC.Container.RegisterType<IInterchangeAccount, AccountInterchange.AccountInterchange>();
    //WigosEGMConsumer.WigosEGMConsumer.Init(IoC.Container);
  }

  /// <summary>
  /// Starts the service
  /// </summary>
  /// <param name="ipe"></param>
  protected override void OnServiceRunning(IPEndPoint Ipe)
  {
    try
    {
    if (Misc.IsCountREnabled())
    {
        // WRKP
        Listener _listener = new Listener();

        // CountR
      CashIOServer _cashio_server;
      _cashio_server = new CashIOServer();
      _cashio_server.Init(GeneralParam.GetInt32("CountR", "Protocols.Port", 2600));
    }
      else
      {
        Log.Message("The feature Countr/RedemptionKiosk is DISABLED");
    }

    // Only for testing purpouse we add this line of code & project reference! InHouseApiWebAPI.      
    if (InHouseApi.Classes.Common.GeneralParams.IsEnabled())
    {
      InHouseApiWebAPI.Listener _in_house_api = new InHouseApiWebAPI.Listener();
    }
    else
    {
      Log.Message("The feature InHouseApi is DISABLED");
    }

			if (Gsa.IsEnabled)
			{
				this.gsaS2sContainer = new UnityContainer();
				Unity.Configure(this.gsaS2sContainer);
				this.gsaS2sService = this.gsaS2sContainer.Resolve<IS2SGsaService>();
				//TODO: Refactor. Abstract correctly to make injection by Unity
				GsaS2SServiceHostInitializer gsaS2sServiceListener = new GsaS2SServiceHostInitializer(
					this.gsaS2sService, 
					new LogService(WinSys.Wigos.Architecture.Enums.LogLevel.Information));		// Initializer service has forced log to view basics & start information
			}
			else
			{
				Log.Message("The feature Wigos S2S GSA service is DISABLED");
			}
  }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }
  }
}

public class WigosLogger : WigosHubCommon.ILogger
{
  public void Log(LogLevel Level, string Message)
  {
    if (Level >= LogLevel)
    {
      WSI.Common.Log.Message(Message);
    }
  }

  public void Exception(Exception _ex)
  {
    WSI.Common.Log.Exception(_ex);
  }

  public void Chain(WigosHubCommon.ILogger SubLogger)
  {
    throw new NotImplementedException();
  }

  public LogLevel LogLevel { get; set; }

  public ILogger Parent { get; set; }
}

[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under network account and will be configured later with a correct username and password.
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WSI.Protocols";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WSI.Protocols";
  }
}
