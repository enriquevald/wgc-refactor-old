﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_TerminalMeterTest.cs
// 
//   DESCRIPTION: Unit test for AFIP_TerminalMeter
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 08-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2016 FGB    First release 
// ----------- ------ ----------------------------------------------------------

using Moq;
using NUnit.Framework;
using System;
using System.Data;
using WSI.AFIP_Client.AFIP_Classes;
using WSI.AFIP_Client.AFIP_WS_JAZALocal;

namespace WSI.AFIP_ClientTest
{
  [TestFixture]
  public class AFIP_TerminalMeterTest
  {
    WSI.AFIP_Client.AFIP_WS_JAZALocal.DETALLE_TERMINALES[] m_request_details_list;
    AFIP_TerminalMeter m_meter_details;
    AFIP_TerminalMeter_STUB m_meter_stub;
    Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES> m_pending_moq;
    DataTable m_datatable;
    Boolean m_bool;

    [SetUp]
    public void SetUpValues()
    {
      m_meter_stub = new AFIP_TerminalMeter_STUB();
      m_pending_moq = new Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES>();
      m_bool = false;
      m_datatable = null;

      //Set request values
      m_pending_moq.Object.fechaJornada = new DateTime(2016, 06, 01, 12, 38, 00);
      m_pending_moq.Object.version = 13;
      m_pending_moq.Object.Maquinas = new String[0]; //Empty array of strings (Requested terminals)
    }

    [TestCase]
    public void When_No_Meters_Data_Expect_Return_Error()
    {
      //Empty datatable
      m_meter_stub.GetTerminalsMetersDatatableEmpty((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);

      //It returns an error
      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_Request_Is_Correct_Expect_Return_Values()
    {
      //Get datatable with records
      m_meter_stub.GetTerminalsMetersDatatableWithRecords((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      
      Assert.IsTrue(m_request_details_list.Length > 0);
    }

    [TestCase]
    public void When_Request_Date_Is_Empty_Expect_No_Data()
    {
      //Get empty datatable
      m_meter_stub.GetTerminalsMetersDatatableEmpty((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      //Empty date
      m_pending_moq.Object.fechaJornada = null;

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);

      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_Request_Version_Is_Empty_Expect_No_Data()
    {
      //Get datatable with records
      m_meter_stub.GetTerminalsMetersDatatableWithRecords((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      //Empty version
      m_pending_moq.Object.version = null;

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      
      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_Request_Version_Is_Negative_Expect_No_Data()
    {
      //Get datatable with records
      m_meter_stub.GetTerminalsMetersDatatableWithRecords((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      //Negative version
      m_pending_moq.Object.version = -1;

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_DataTable_Created_Correctly_Expect_Return_Values()
    {
      //Get datatable with records
      m_meter_stub.GetTerminalsMetersDatatableWithRecords((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable);

      m_meter_details = new AFIP_TerminalMeter(m_datatable, false);
      m_bool = m_meter_details.GetTerminalMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      Assert.IsTrue(m_bool);
    }

    [TestCase]
    public void When_DataTable_Created_Incorrectly_Expect_Throws_NoNullAllowedException()
    {
      //We create the DataTable incorrectly, with a null value in a no nullable field
      Assert.Throws<System.Data.NoNullAllowedException>(() => m_meter_stub.GetTerminalsMetersDatatableThrowsException((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, (String)m_pending_moq.Object.Maquinas.ToString(), out m_datatable));
    }
  }
}
