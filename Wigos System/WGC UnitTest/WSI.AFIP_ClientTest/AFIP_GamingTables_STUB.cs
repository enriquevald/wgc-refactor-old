﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client_GamingTables_STUB.cs
// 
//   DESCRIPTION: STUB to use in Automatic Test
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 23-May-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-May-2016 FOS    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;

namespace WSI.AFIP_ClientTest
{
  public class AFIP_GamingTables_STUB
  {
    /// <summary>
    /// This Method matching the structure of dataTable with thew registers from stored 
    /// </summary>
    /// <returns></returns>
    public DataTable Fill_GamingTables_Stub()
    {
      DataRow _new_row;
      DataTable _dt_gaming_tables;

      _dt_gaming_tables = CreateGamingTableDatatable();
      _new_row = _dt_gaming_tables.NewRow();

      _new_row["ID_TABLE_TYPE"] = 1;
      _new_row["DESC_TABLE_TYPE"] = "Naipes";
      _new_row["TABLE_COUNT"] = 1;
      _new_row["OPENING_CASH"] = 1;
      _new_row["CLOSING_CASH"] = 1;
      _new_row["OPENING_CHIPS"] = 1;
      _new_row["CLOSING_CHIPS"] = 1;
      _new_row["CASH_OUT_TOTAL"] = 1;
      _new_row["CASH_IN_TOTAL"] = 1;
      _new_row["CHIPS_OUT_TOTAL"] = 1;
      _new_row["CHIPS_IN_TOTAL"] = 1;
      _new_row["CHIP_SALES_TOTAL"] = 1;
      _new_row["CHIPS_BUY_TOTAL"] = 1;

      _dt_gaming_tables.Rows.Add(_new_row);

      return _dt_gaming_tables;
    }

    /// <summary>
    /// This Method matching the structure of dataTable (that's incorrect) with thew registers from stored and throw and exception when it used
    /// </summary>
    /// <returns></returns>
    public DataTable Fill_Badly_GamingTables_Stub()
    {
      DataRow _new_row;
      DataTable _dt_gaming_tables;

      _dt_gaming_tables = CreateGamingTableDatatable();
      _new_row = _dt_gaming_tables.NewRow();

      _new_row["ID_TABLE_TYPE"] = 1;
      _new_row["DESC_TABLE_TYPE"] = "Naipes";
      _new_row["TABLE_COUNT"] = 1;
      _new_row["OPENING_CASH"] = 1;
      _new_row["CLOSING_CASH"] = 1;
      _new_row["OPENING_CHIPS"] = 1;
      _new_row["CLOSING_CHIPS"] = 1;
      _new_row["CASH_OUT_TOTAL"] = 1;
      _new_row["CASH_IN_TOTAL"] = 1;
      _new_row["CHIPS_OUT_TOTAL"] = 1;
      _new_row["CHIPS_IN_TOTAL"] = 1;
      _new_row["CHIPS_SALES_TOTAL"] = 1; //change the column name
      _new_row["CHIP_BUY_TOTAL"] = 1; //change the column name

      _dt_gaming_tables.Rows.Add(_new_row);

      return _dt_gaming_tables;
    }

    /// <summary>
    /// Create de Structure of AFIPClient_gamingTables dataTable
    /// </summary>
    /// <returns></returns>
    public DataTable CreateGamingTableDatatable()
    {
      DataTable _dt_gaming_table_details;

      _dt_gaming_table_details = new DataTable();
      _dt_gaming_table_details.Columns.Add("ID_TABLE_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("DESC_TABLE_TYPE", Type.GetType("System.String")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("TABLE_COUNT", Type.GetType("System.Int32")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("OPENING_CASH", Type.GetType("System.Decimal")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("CLOSING_CASH", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("OPENING_CHIPS", Type.GetType("System.Decimal")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("CLOSING_CHIPS", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("CASH_OUT_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("CASH_IN_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("CHIPS_OUT_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("CHIPS_IN_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = false;
      _dt_gaming_table_details.Columns.Add("CHIP_SALES_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_gaming_table_details.Columns.Add("CHIPS_BUY_TOTAL", Type.GetType("System.Decimal")).AllowDBNull = true;

      return _dt_gaming_table_details;
    }
  }
}
