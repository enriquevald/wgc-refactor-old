﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client_Request_STUB.cs
// 
//   DESCRIPTION: Provider that returns Requests's STUB data for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 17-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2016 FGB    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using WSI.AFIP_Client.AFIP_Classes;

namespace WSI.AFIP_ClientTest
{
  public class AFIP_Request_STUB
  {
    #region "Private Methods"
    /// <summary>
    /// Creates the DataTable to use
    /// </summary>
    /// <returns></returns>
    private DataTable CreateRequestDatatable()
    {
      return new AFIP_Client_Common().CreateResultDatatable(true);
    }

    #endregion    
    
    #region "Public Methods"
    /// <summary>
    /// Returns the terminal meters for a request
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="TerminalId"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetRequestMetersDatatableWithRecords(Int32 RequestId, DateTime RequestTime, String TerminalId, out DataTable DtTerminalDetail)
    {
      DataRow _new_row;

      DtTerminalDetail = CreateRequestDatatable();
      _new_row = DtTerminalDetail.NewRow();

      //Terminal 00001 - Sequence 1
      _new_row[AFIP_Client_Common.RESULT_REQUEST_ID] = RequestId;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = TerminalId;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "AFIP_XX_" + TerminalId.ToString();
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 1;
      _new_row[AFIP_Client_Common.RESULT_AFIP_STATE] = ENUM_AFIP_TERMINAL_STATE.ACTIVE;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1000;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2000;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3000;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 4000;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 11, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 3999;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 4999;

      _new_row[AFIP_Client_Common.RESULT_INTERVAL_FROM] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_INTERVAL_TO] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 23, 59, 59);

      DtTerminalDetail.Rows.Add(_new_row);

      //Terminal 00001 - Sequence 2
      _new_row = DtTerminalDetail.NewRow();

      _new_row[AFIP_Client_Common.RESULT_REQUEST_ID] = RequestId;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = TerminalId;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "AFIP_XX_" + TerminalId.ToString();
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 2;
      _new_row[AFIP_Client_Common.RESULT_AFIP_STATE] = ENUM_AFIP_TERMINAL_STATE.ACTIVE;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 0.5;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 12, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3999;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 4999;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 23, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 5999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 6999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 7999;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 8999;

      _new_row[AFIP_Client_Common.RESULT_INTERVAL_FROM] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_INTERVAL_TO] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 23, 59, 59);

      DtTerminalDetail.Rows.Add(_new_row);

      return true;
    }

    /// <summary>
    /// Throws an exception filling the request meters datatable with no records for a terminal and a time
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="TerminalId"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetRequestMetersDatatableThrowsException(Int32 RequestId, DateTime RequestTime, String TerminalId, out DataTable DtTerminalDetail)
    {
      DataRow _new_row;

      DtTerminalDetail = CreateRequestDatatable();
      _new_row = DtTerminalDetail.NewRow();

      //Terminal 00001 - Sequence 1

      //We don't fill the RESULT_REQUEST_ID so it returns an exception 
      //_new_row[AFIP_Client_Common.RESULT_REQUEST_ID] = _version;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = TerminalId;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "AFIP_XX_" + TerminalId.ToString();
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 1;
      _new_row[AFIP_Client_Common.RESULT_AFIP_STATE] = ENUM_AFIP_TERMINAL_STATE.ACTIVE;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1000;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2000;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3000;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 4000;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(RequestTime.Year, RequestTime.Month, RequestTime.Day, 11, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 3999;
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 4999;

      DtTerminalDetail.Rows.Add(_new_row);

      return true;
    }

    /// <summary>
    /// Returns the request meters datatable with no records for a terminal and a time
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="TerminalId"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetRequestMetersDatatableEmpty(Int32 RequestId, DateTime RequestTime, String TerminalId, out DataTable DtTerminalDetail)
    {
      DtTerminalDetail = CreateRequestDatatable();

      return true;
    }
    #endregion
  }
}