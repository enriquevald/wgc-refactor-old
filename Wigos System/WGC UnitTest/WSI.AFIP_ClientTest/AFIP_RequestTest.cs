﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_RequestTest.cs
// 
//   DESCRIPTION: Unit test for AFIP_Request
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 08-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2016 FGB    First release 
// ----------- ------ ----------------------------------------------------------

using Moq;
using NUnit.Framework;
using System;
using System.Data;
using WSI.AFIP_Client.AFIP_Classes;

namespace WSI.AFIP_ClientTest
{
  [TestFixture]
  public class AFIP_RequestTest
  {
    WSI.AFIP_Client.AFIP_WS_JAZALocal.DETALLE_SOLICITUDES[] m_request_details_list;
    AFIP_Request m_request_details;
    AFIP_Request_STUB m_request_stub;
    Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES> m_pending_moq;
    DataTable m_datatable;
    Boolean m_bool;

    [SetUp]
    public void SetUpValues()
    {
      m_request_stub = new AFIP_Request_STUB();
      m_pending_moq = new Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES>();
      m_bool = false;
      m_datatable = null;

      m_pending_moq.Object.idSolicitud = 13;
      m_pending_moq.Object.FechaHoraSolicitud = new DateTime(2016, 06, 01, 12, 38, 00);
      m_pending_moq.Object.idMaquina = "AFIP_XX_001";
    }

    [TestCase]
    public void When_No_Meters_Data_Expect_No_Error()
    {
      //Get empty datatable
      m_request_stub.GetRequestMetersDatatableEmpty((Int32)m_pending_moq.Object.idSolicitud, (DateTime)m_pending_moq.Object.FechaHoraSolicitud, (String)m_pending_moq.Object.idMaquina, out m_datatable);

      m_request_details = new AFIP_Request(m_datatable, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      
      Assert.IsTrue(m_bool);
    }

    [TestCase]
    public void When_No_Meters_Data_Expect_No_Values()
    {
      //Get empty datatable
      m_request_stub.GetRequestMetersDatatableEmpty((Int32)m_pending_moq.Object.idSolicitud, (DateTime)m_pending_moq.Object.FechaHoraSolicitud, (String)m_pending_moq.Object.idMaquina, out m_datatable);

      m_request_details = new AFIP_Request(m_datatable, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      
      Assert.IsTrue(m_request_details_list.Length <= 0);
    }

    [TestCase]
    public void When_Meters_Data_Expect_Return_Values()
    {
      //Get correctly datatable
      m_request_stub.GetRequestMetersDatatableWithRecords((Int32)m_pending_moq.Object.idSolicitud, (DateTime)m_pending_moq.Object.FechaHoraSolicitud, (String)m_pending_moq.Object.idMaquina, out m_datatable);

      m_request_details = new AFIP_Request(m_datatable, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);
      
      Assert.IsTrue(m_request_details_list.Length > 0);
    }

    [TestCase]
    public void When_Request_Time_Is_Empty_Expect_No_Data()
    {
      m_pending_moq.Object.FechaHoraSolicitud = null;
      m_request_details = new AFIP_Request(null, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);

      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_Request_Id_Is_Empty_Expect_No_Data()
    {
      m_pending_moq.Object.idSolicitud = null;
      m_request_details = new AFIP_Request(null, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);

      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_Request_Terminal_Is_Empty_Expect_No_Data()
    {
      m_pending_moq.Object.idMaquina = null;
      m_request_details = new AFIP_Request(null, false);
      m_bool = m_request_details.GetRequestMeter((AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES)m_pending_moq.Object, out m_request_details_list);

      Assert.IsFalse(m_bool);
    }

    [TestCase]
    public void When_DataTable_Created_Incorrectly_Expect_Throws_NoNullAllowedException()
    {
      //We create the DataTable incorrectly, with a null value in a no nullable field
      Assert.Throws<System.Data.NoNullAllowedException>(() => m_request_stub.GetRequestMetersDatatableThrowsException((Int32)m_pending_moq.Object.idSolicitud, (DateTime)m_pending_moq.Object.FechaHoraSolicitud, (String)m_pending_moq.Object.idMaquina, out m_datatable));
    }
  }
}
