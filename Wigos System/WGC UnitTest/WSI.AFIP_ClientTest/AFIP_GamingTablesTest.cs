﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client_GamingTablesTest.cs
// 
//   DESCRIPTION: Unit test for AFIP_Client_GamingTables
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 23-May-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-May-2016 FOS    First release 
// ----------- ------ ----------------------------------------------------------

using Moq;
using NUnit.Framework;
using System;
using System.Data;
using WSI.AFIP_Client.AFIP_Classes;

namespace WSI.AFIP_ClientTest
{
  [TestFixture]
  public class AFIP_GamingTablesTest
  {
    Boolean m_bool;
    WSI.AFIP_Client.AFIP_WS_JAZALocal.DETALLE_MESAS[] m_gaming_tables_details_list;
    AFIP_GamingTables m_gaming_tables_details;
    Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES> m_pending_moq;
    DataTable m_dt;
    AFIP_GamingTables_STUB m_gaming_tables_stub;

    [SetUp]
    public void SetUpValues()
    {
      m_gaming_tables_stub = new AFIP_GamingTables_STUB();
      m_pending_moq = new Mock<AFIP_Client.AFIP_WS_JAZALocal.PENDIENTES>();
      m_dt = m_gaming_tables_stub.CreateGamingTableDatatable();
      m_bool = false;

      m_pending_moq.Object.fechaJornada = new DateTime();
      m_pending_moq.Object.version = 1;
    }

    [TestCase]
    public void Check_Object_Gaming_Tables()
    {
      m_gaming_tables_details = new AFIP_GamingTables(m_dt, false); //Dependency injection.
      m_bool = m_gaming_tables_details.GetGamingTablesDetails((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, out m_gaming_tables_details_list);
      Assert.IsTrue(m_bool);
    }

    [TestCase]
    public void Check_If_Gaming_Tables_Return_Values_Without_Any_Request()
    {
      m_gaming_tables_details = new AFIP_GamingTables(m_dt, false); //Dependency injection.
      m_bool = m_gaming_tables_details.GetGamingTablesDetails((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, out m_gaming_tables_details_list);
      Assert.IsTrue(m_gaming_tables_details_list.Length <= 0);

    }
    [TestCase]
    public void Check_If_Gaming_Tables_Return_Values_With_Request()
    {
      m_dt = m_gaming_tables_stub.Fill_GamingTables_Stub();
      m_gaming_tables_details = new AFIP_GamingTables(m_dt, false); //Dependency injection.
      m_bool = m_gaming_tables_details.GetGamingTablesDetails((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, out m_gaming_tables_details_list);
      Assert.IsTrue(m_gaming_tables_details_list.Length > 0);
    }

    [TestCase]
    public void Check_If_StartDate_IsNull()
    {
      m_pending_moq.Object.fechaJornada = null;
      Assert.Throws<System.InvalidOperationException>(() => m_gaming_tables_details.GetGamingTablesDetails((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, out m_gaming_tables_details_list));
    }

    [TestCase]
    public void Check_If_Version_IsNull()
    {
      m_pending_moq.Object.version = null;
      Assert.Throws<System.InvalidOperationException>(() => m_gaming_tables_details.GetGamingTablesDetails((DateTime)m_pending_moq.Object.fechaJornada, (Int32)m_pending_moq.Object.version, out m_gaming_tables_details_list));
    }

    [TestCase]
    public void Check_DataTable_Structure()
    {
      Assert.Throws<System.ArgumentException>(() => m_gaming_tables_stub.Fill_Badly_GamingTables_Stub());
    }
  }
}
