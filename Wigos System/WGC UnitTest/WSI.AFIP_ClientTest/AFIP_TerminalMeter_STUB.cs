﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client_TerminalMeter_STUB.cs
// 
//   DESCRIPTION: STUB to use in Automatic Test
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 12-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAY-2016 FGB    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using WSI.AFIP_Client.AFIP_Classes;

namespace WSI.AFIP_ClientTest
{
  public class AFIP_TerminalMeter_STUB
  {
    #region "Private Methods"
    /// <summary>
    /// Creates the DataTable to use
    /// </summary>
    /// <returns></returns>
    private DataTable CreateTerminalsMetersDatatable()
    {
      return new AFIP_Client_Common().CreateResultDatatable(false);
    }
    #endregion    
    
    #region "Public Methods"
    /// <summary>
    /// Returns the terminal meters datatable for a date
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="Version"></param>
    /// <param name="RequestedTerminals"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetTerminalsMetersDatatableWithRecords(DateTime SessionDate, Int32 Version, String RequestedTerminals, out DataTable DtTerminalDetail)
    {
      DataRow _new_row;

      DtTerminalDetail = CreateTerminalsMetersDatatable();

      //Terminal 00001 - Secuencia 1
      _new_row = DtTerminalDetail.NewRow();
      _new_row[AFIP_Client_Common.RESULT_SESSION_DATE] = SessionDate;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 1;
      _new_row[AFIP_Client_Common.RESULT_VERSION] = Version;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 10;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1000;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2000;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3000;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 11, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 19;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 3999;

      _new_row[AFIP_Client_Common.RESULT_INTERVAL_FROM] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_INTERVAL_TO] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 23, 59, 59);

      DtTerminalDetail.Rows.Add(_new_row);

      //Terminal 00001 - Secuencia 2
      _new_row = DtTerminalDetail.NewRow();
      _new_row[AFIP_Client_Common.RESULT_SESSION_DATE] = SessionDate;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 2;
      _new_row[AFIP_Client_Common.RESULT_VERSION] = Version;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 12, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 19;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3999;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 23, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 39;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 3999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 4999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 5999;

      _new_row[AFIP_Client_Common.RESULT_INTERVAL_FROM] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_INTERVAL_TO] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 23, 59, 59);

      DtTerminalDetail.Rows.Add(_new_row);

      //Terminal 00002
      _new_row = DtTerminalDetail.NewRow();
      _new_row[AFIP_Client_Common.RESULT_SESSION_DATE] = SessionDate;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = "00002";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "00002";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 1;
      _new_row[AFIP_Client_Common.RESULT_VERSION] = Version;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 20;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 5000;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 6000;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 7000;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 23, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 29;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 5999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 6999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 7999;

      _new_row[AFIP_Client_Common.RESULT_INTERVAL_FROM] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_INTERVAL_TO] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 23, 59, 59);

      DtTerminalDetail.Rows.Add(_new_row);

      return true;
    }

    /// <summary>
    /// Throws an exception filling the terminal meters datatable for a date
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="Version"></param>
    /// <param name="RequestedTerminals"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetTerminalsMetersDatatableThrowsException(DateTime SessionDate, Int32 Version, String RequestedTerminals, out DataTable DtTerminalDetail)
    {
      DataRow _new_row;

      DtTerminalDetail = CreateTerminalsMetersDatatable();

      //Terminal 00001 - Secuencia 1
      _new_row = DtTerminalDetail.NewRow();
      
      //We don't fill the RESULT_SESSION_DATE so it returns an exception
      //_new_row[AFIP_Client_Common.RESULT_SESSION_DATE] = SessionDate;
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ID] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = "00001";
      _new_row[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 1;
      _new_row[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = 1;
      _new_row[AFIP_Client_Common.RESULT_VERSION] = Version;

      _new_row[AFIP_Client_Common.RESULT_DATE_START] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 00, 00, 00);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 10;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_START] = 1000;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_START] = 2000;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_START] = 3000;

      _new_row[AFIP_Client_Common.RESULT_DATE_END] = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day, 11, 59, 59);
      _new_row[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 19;
      _new_row[AFIP_Client_Common.RESULT_COIN_IN_END] = 1999;
      _new_row[AFIP_Client_Common.RESULT_COIN_OUT_END] = 2999;
      _new_row[AFIP_Client_Common.RESULT_JACKPOTS_END] = 3999;

      DtTerminalDetail.Rows.Add(_new_row);

      return true;
    }

    /// <summary>
    /// Returns the terminal meters datatable with no records for a date
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="Version"></param>
    /// <param name="RequestedTerminals"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetTerminalsMetersDatatableEmpty(DateTime SessionDate, Int32 Version, String RequestedTerminals, out DataTable DtTerminalDetail)
    {
      DtTerminalDetail = CreateTerminalsMetersDatatable();

      return true;
    }
    #endregion
  }
}