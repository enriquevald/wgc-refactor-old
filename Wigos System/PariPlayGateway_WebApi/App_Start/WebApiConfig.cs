﻿using PariPlay_GameGateway.Common;
using System;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services

      // Connect to DB
      WGDB.Init(Util.GetSetting<Int32>("DBId"), Util.GetSetting<String>("DBPrincipal"), Util.GetSetting<String>("DBMirror"));
      WGDB.SetApplication(Util.AppName, Util.CurrentVersion);
      WGDB.ConnectAs("WGROOT");

      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "{controller}"
      );
    }
  }
}
