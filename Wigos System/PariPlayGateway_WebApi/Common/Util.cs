﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Util.cs
//// 
////      DESCRIPTION: Utils
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 02-SEP-2016 JBP    Added ResponseMapping function.
////------------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Reflection;

namespace PariPlay_GameGateway.Common
{
  public static class Util
  {

    #region "Properties"

    /// <summary>
    /// Get the Current Version
    /// </summary>
    public static string CurrentVersion
    {
      get
      {
        Assembly _assembly;
        Version _version;

        _assembly = Assembly.GetExecutingAssembly();
        _version = _assembly.GetName().Version;

        return _version.Major.ToString("00") + "." + _version.Minor.ToString("000");
      }
    } // CurrentVersion

    /// <summary>
    /// Get the app name
    /// </summary>
    public static string AppName
    {
      get
      {
        Assembly _assembly;
        String _name;

        _assembly = Assembly.GetExecutingAssembly();
        _name = _assembly.GetName().Name;

        return _name;
      }
    } // CurrentVersion

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Get the ConfigurationFile Settings
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Key"></param>
    /// <returns></returns>
    public static T GetSetting<T>(String Key)
    {
      return GetSetting<T>(Key, default(T));
    } //GetSetting

    /// <summary>
    /// Get the ConfigurationFile Settings
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Key"></param>
    /// <param name="DefaultValue"></param>
    /// <returns></returns>
    public static T GetSetting<T>(String Key, T DefaultValue)
    {
      try
      {
        var value = ConfigurationManager.AppSettings[Key];

        return (T)Convert.ChangeType(value, typeof(T));
      }
      catch (Exception)
      {
        return DefaultValue;
      }
    } //GetSetting

    #endregion

  }
}