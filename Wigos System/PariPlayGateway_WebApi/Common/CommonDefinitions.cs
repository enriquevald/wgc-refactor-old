﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: ErrorCode.cs
//// 
////      DESCRIPTION: Enum for errors code
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 02-SEP-2016 JBP    Added GetBalanceMode enum.
////------------------------------------------------------------------------------


using Newtonsoft.Json;
using PariPlay_GameGateway.Models;
using System;
using System.Net.Http;
using System.Text;
using WSI.Common;
namespace PariPlay_GameGateway.Common
{
  #region " Enums "

  public enum ErrorCode
  {
    InsufficientFunds = 1,
    InvalidNegativeAmount = 3,
    AuthenticationFailed = 4,
    TokenExpired = 5,
    UnknownTransactionId = 6,
    InvalidGame = 7,
    InvalidToken = 8,
    InvalidRound = 9,
    InvalidUserId = 10,
    TransactionAlreadySettled = 11,
    AccountIsLocked = 12,
    PlayerLimitExceededTurnover = 13,
    PlayerLimitExceededSessionTime = 14,
    PlayerLimitExceededSessionStake = 15,
    PlayerLimitExceededSessionLoss = 16,
    RoundAlreadyEnded = 17,
    TransactionAlreadyCancelled = 18,
    PlayerHaveOpenedRounds = 19,
    NoRoundsLeft = 20,
    GeneralError = 900
  }

  #endregion

  #region " Logger "

  public class Logger
  {
    /// <summary>
    /// Save Log
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Result"></param>
    public void SaveLog(object Request, object Result)
    {
      String _json_result;
      String _json_request;

      _json_request = JsonConvert.SerializeObject(Request).ToString();
      _json_result = JsonConvert.SerializeObject(Result).ToString();

      GameGateway.ManageLog(_json_request, _json_result, GameGateway.PROVIDER_TYPE.PariPlay);

    } // SaveLog
  }

  #endregion

}