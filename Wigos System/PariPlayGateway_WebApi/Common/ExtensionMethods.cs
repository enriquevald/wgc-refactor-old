﻿using PariPlay_GameGateway.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PariPlay_GameGateway.Common
{
  public static class ExtensionMethods
  {

    #region " Credit/Debit/EndGame Request "

    /// <summary>
    /// Map DebitCreditRequest to end CreditRequest
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static CreditRequest ToCreditRequest(this DebitCreditRequest Request)
    {
      return new CreditRequest()
      {
        Account = Request.Account,
        Amount = Request.CreditAmount,
        CreditType = Request.CreditType,
        EndGame = false,
        Feature = Request.Feature,
        FeatureId = Request.FeatureId,
        GameCode = Request.GameCode,
        PlayerId = Request.PlayerId,
        RoundId = Request.RoundId,
        Token = Request.Token,
        TransactionId = Request.TransactionId
      };
    } // ToCreditRequest

    /// <summary>
    /// Map DebitRequest to end EndGameRequest
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static DebitRequest ToDebitRequest(this DebitCreditRequest Request)
    {
      return new DebitRequest()
      {
        Account = Request.Account,
        Amount = Request.DebitAmount,
        EndGame = false,
        Feature = Request.Feature,
        FeatureId = Request.FeatureId,
        GameCode = Request.GameCode,
        PlayerId = Request.PlayerId,
        RoundId = Request.RoundId,
        Token = Request.Token,
        TransactionId = Request.TransactionId
      };
    } // ToDebitRequest

    /// <summary>
    /// Map DebitRequest to end EndGameRequest
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static EndGameRequest ToEndGameRequest(this DebitRequest Request)
    {
      return new EndGameRequest()
      {
        Account = Request.Account,
        GameCode = Request.GameCode,
        PlayerId = Request.PlayerId,
        RoundId = Request.RoundId,
        Token = Request.Token,
        TransactionConfiguration = Request.TransactionConfiguration,
        TransactionId = Request.TransactionId
      };
    } // ToEndGameRequest

    /// <summary>
    /// Map CreditRequest to end EndGameRequest
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static EndGameRequest ToEndGameRequest(this CreditRequest Request)
    {
      return new EndGameRequest()
      {
        Account = Request.Account,
        GameCode = Request.GameCode,
        PlayerId = Request.PlayerId,
        RoundId = Request.RoundId,
        Token = Request.Token,
        TransactionConfiguration = Request.TransactionConfiguration,
        TransactionId = Request.TransactionId
      };
    } // ToEndGameRequest

    /// <summary>
    /// Map DebitCreditRequest to end EndGameRequest
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public static EndGameRequest ToEndGameRequest(this DebitCreditRequest Request)
    {
      return new EndGameRequest()
      {
        Account = Request.Account,
        GameCode = Request.GameCode,
        PlayerId = Request.PlayerId,
        RoundId = Request.RoundId,
        Token = Request.Token,
        TransactionId = Request.TransactionId
      };
    } // ToEndGameRequest

    #endregion

  }
}