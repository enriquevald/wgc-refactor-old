﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CreditController.cs
//// 
////      DESCRIPTION: Credit Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 06-SEP-2016 JBP    Implementation.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class CreditController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// Credit action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] CreditRequest Data)
    {
      return Execute(() => Credit(Data));
    } // Post

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Process credit request
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo Credit(CreditRequest Data)
    {
      return new RequestResponseCombo(new Credit(this).ProcessRequest(Data), Data);
    } // Credit

    #endregion
  }
}



