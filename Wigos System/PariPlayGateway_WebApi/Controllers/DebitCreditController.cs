﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitCreditController.cs
//// 
////      DESCRIPTION: DebitCredit Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 06-SEP-2016 JBP    Implementation
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class DebitCreditController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// Debit action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] DebitCreditRequest Data)
    {
      return Execute(() => DebitCredit(Data));
    } // Post

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Debit/Credit action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo DebitCredit(DebitCreditRequest Data)
    {
      return new RequestResponseCombo(new DebitCredit(this).ProcessRequest(Data), Data);
    } // DebitCredit

    #endregion

  }
}
