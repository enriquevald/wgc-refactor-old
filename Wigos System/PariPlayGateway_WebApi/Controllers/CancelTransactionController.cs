﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CancelTransactionController.cs
//// 
////      DESCRIPTION: CancelTransaction Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class CancelTransactionController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// CancelTransaction action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] CancelTransactionRequest Data)
    {
      return Execute(() => CancelTransaction(Data));
    } // Post

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Process cancel transaction request
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo CancelTransaction(CancelTransactionRequest Data)
    {
      return new RequestResponseCombo(new CancelTransaction(this).ProcessRequest(Data), Data);
    } // CancelTransaction

    #endregion

  }
}
