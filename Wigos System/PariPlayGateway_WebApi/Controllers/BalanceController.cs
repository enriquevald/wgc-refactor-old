﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BalanceController.cs
//// 
////      DESCRIPTION: Balance Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 02-SEP-2016 JBP    Adapted for Authenticate request.
//// 06-SEP-2016 JBP    Refactor.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class BalanceController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// Get Balance action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] GetBalanceRequest Data)
    {
      return Execute(() => GetBalance(Data));
    } // Post

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Process GetBalance request
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo GetBalance(GetBalanceRequest Data)
    {
      return new RequestResponseCombo(new Balance(this).ProcessRequest(Data), Data);
    } // GetBalance

    #endregion

  }
}
