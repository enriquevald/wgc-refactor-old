﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitController.cs
//// 
////      DESCRIPTION: Debit Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 06-SEP-2016 JBP    Implementation.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class DebitController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// Debit action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] DebitRequest Data)
    {
      return Execute(() => Debit(Data));
    } // Post

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Process debit request
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo Debit(DebitRequest Data)
    {
      return new RequestResponseCombo(new Debit(this).ProcessRequest(Data), Data);
    } // Debit

    #endregion

  }
}
