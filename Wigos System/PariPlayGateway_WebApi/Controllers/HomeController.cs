﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: HomeController.cs
//// 
////      DESCRIPTION: Default Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class HomeController : ApiController
  {

  }
}
