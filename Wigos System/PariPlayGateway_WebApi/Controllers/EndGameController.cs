﻿using PariPlay_GameGateway.Classes;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: EndGameController.cs
//// 
////      DESCRIPTION: EndGame Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class EndGameController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// EndGame action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody] EndGameRequest Data)
    {
      return Execute(() => EndGame(Data));
    } // Post

    #endregion

    #region "Private Methods"

    /// <summary>
    /// EndGame action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo EndGame(EndGameRequest Data)
    {
      return new RequestResponseCombo(new EndGame(this).ProcessRequest(Data), Data);
    } // EndGame

    #endregion
  }
}
