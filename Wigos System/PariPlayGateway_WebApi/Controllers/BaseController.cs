﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseController.cs
//// 
////      DESCRIPTION: Base Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 06-SEP-2016 JBP    Refactor.
////------------------------------------------------------------------------------
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;
using Newtonsoft.Json;
using PariPlay_GameGateway.Common;
using PariPlay_GameGateway.Models;

namespace PariPlay_GameGateway.Controllers
{
  public class BaseController : ApiController
  {

    #region "Protected Methods"

    /// <summary>
    /// Execute
    /// </summary>
    /// <param name="Func"></param>
    /// <returns></returns>
    protected HttpResponseMessage Execute(Func<RequestResponseCombo> Func)
    {
      try
      {
        // Execute request
        var _result = Func.Invoke();

        // Save log
        new Logger().SaveLog(_result.Request                                       // JSON Request
                            , _result.Response.Content.ReadAsStringAsync().Result); // JSON Response

        return _result.Response;
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
        return Request.CreateResponse(HttpStatusCode.InternalServerError, _ex.Message);
      }
    } // Execute

    #endregion

  }
}
