﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CancelBetController.cs
//// 
////      DESCRIPTION: CancelBet Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------

using PariPlay_GameGateway.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Controllers
{
    public class CancelBetController : BaseController
    {

      #region "HTTP Verbs"

      /// <summary>
      /// CancelTransaction action
      /// </summary>
      /// <param name="Data"></param>
      /// <returns></returns>
      [HttpPost]
      public HttpResponseMessage Post([FromBody] CancelTransactionRequest Data)
      {
        return Execute(() =>
          CancelBet(Data)
          , Data.Token);
      }//Post

      #endregion

      #region "Private Methods"

      /// <summary>
      /// Cancel bet action
      /// </summary>
      /// <param name="Data"></param>
      /// <returns></returns>
      private HttpResponseMessage CancelBet(CancelTransactionRequest Data)
      {
        LCD_Session _session;
        CancelTransactionResponse _response;

        // Internal Authentification
        _session = new LCD_Session();

        //if (_session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        //{
        //  return RequestErrorResponse(_session.ErrorCode);
        //}

        //// Set TransactionType
        //_session.TransactionType = GameGateway.TRANSACTION_TYPE.;

        // Get account balance
        _response = GetResponse(_session);

        // Return CancelTransactionResponse jSon
        return Request.CreateResponse(HttpStatusCode.OK, _response);

      } // CancelBet

      /// <summary>
      /// Get cancel transaction response
      /// </summary>
      /// <returns></returns>
      private CancelTransactionResponse GetResponse(LCD_Session Session)
      {
        CancelTransactionResponse _response;

        _response = new CancelTransactionResponse();

        try
        {
          _response.Balance = 200;
          _response.BonusBalance = 500;
          _response.Timestamp = "";
          _response.RoundsLeft = 5;
          _response.ExtraData = "xxxx";
          _response.TransactionId = "1234";
        }
        catch (Exception _ex)
        {
          Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
          Session.ErrorMessage = "Error CancelBetController.GetResponse";

          Log.Exception(_ex);
          Log.Message(Session.ErrorMessage);
      }

        return _response;

      } // GetResponse

      #endregion

    }
}
