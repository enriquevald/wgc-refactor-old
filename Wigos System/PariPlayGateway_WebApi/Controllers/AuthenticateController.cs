﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BalanceController.cs
//// 
////      DESCRIPTION: Balance Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
//// 02-SEP-2016 JBP    Moved to BalanceController and adapted for GetBalance.
////------------------------------------------------------------------------------

using PariPlay_GameGateway.Classes;
using PariPlay_GameGateway.Models;
using System.Net.Http;
using System.Web.Http;

namespace PariPlay_GameGateway.Controllers
{
  public class AuthenticateController : BaseController
  {

    #region "HTTP Verbs"

    /// <summary>
    /// Post action
    /// </summary>
    /// <param name="Data"></param>
    ///// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Post([FromBody]AuthenticateRequest Data)
    {
      return Execute(() => Authenticate(Data));
    } // Post

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Authenticate action
    /// </summary>
    /// <param name="Data"></param>
    /// <returns></returns>
    private RequestResponseCombo Authenticate(AuthenticateRequest Data)
    {
      return new RequestResponseCombo(new Balance(this).ProcessRequest(Data, true), Data);
    } // Authenticate

    #endregion

  }
}
