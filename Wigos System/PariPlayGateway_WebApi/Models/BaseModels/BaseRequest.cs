﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseRequest.cs
//// 
////      DESCRIPTION: Base Request for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 06-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using System;

namespace PariPlay_GameGateway.Models
{
  public abstract class BaseRequest : IBaseRequest
  {
    public String Token { get; set; }
    public AccountRequest Account { get; set; }
  }

  public interface IBaseRequest
  {
    String Token { get; set; }
    AccountRequest Account { get; set; }
  }
}
