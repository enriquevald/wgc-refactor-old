﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseResponse.cs
//// 
////      DESCRIPTION: Base Response for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 06-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class BaseResponse
  {
    public decimal Balance { get; set; }
    public decimal BonusBalance { get; set; }
    public string Timestamp { get; set; }
  }
}
