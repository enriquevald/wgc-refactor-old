﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseDebitCreditRequest.cs
//// 
////      DESCRIPTION: BaseDebitCredit Response for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 06-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class BaseDebitCreditRequest : BaseRequest
  {
    public string GameCode { get; set; }
    public string PlayerId { get; set; }
    public string RoundId { get; set; }
    public string TransactionId { get; set; }
    public string Feature { get; set; }
    public string FeatureId { get; set; }
  }

}
