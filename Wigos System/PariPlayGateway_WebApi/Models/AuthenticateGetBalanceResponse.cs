﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseBalanceResponse.cs
//// 
////      DESCRIPTION: BaseBalance Response for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 06-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 06-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class AuthenticateGetBalanceResponse : BaseResponse
  {
    public int RoundsLeft { get; set; }
  }
}
