﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: AwardFreeRoundsPointsResponse.cs
//// 
////      DESCRIPTION: AwardFreeRoundsPointsResponse Model
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 01-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 01-SEP-2016 PDM    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class AwardFreeRoundsPointsResponse : BaseResponse
  {
  }
}