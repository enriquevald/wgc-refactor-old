﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitResponse.cs
//// 
////      DESCRIPTION: Debit Response for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class DebitResponse : BaseResponse
  {
    public string TransactionId { get; set; }
  }
}
