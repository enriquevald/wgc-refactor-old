﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: AwardFreeRoundsPointsRequest.cs
//// 
////      DESCRIPTION: AwardFreeRoundsPointsRequest Model
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 01-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 01-SEP-2016 PDM    First release.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class AwardFreeRoundsPointsRequest : BaseRequest
  {
  }
}