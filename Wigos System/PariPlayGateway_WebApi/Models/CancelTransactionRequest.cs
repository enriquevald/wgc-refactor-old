﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CancelTransactionRequest.cs
//// 
////      DESCRIPTION: Cancel Transaction Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
////------------------------------------------------------------------------------
using System;

namespace PariPlay_GameGateway.Models
{
  public class CancelTransactionRequest : BaseRequest
  {
    public string RefTransactionId { get; set; }
    public string GameCode { get; set; }
    public string PlayerId { get; set; }
    public string RoundId { get; set; }
    public Boolean CancelEntireRound { get; set; }
    public string TransactionId { get; set; }
    public string Reason { get; set; }
    public decimal Amount { get; set; }
  }
}
