﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitCreditResponse.cs
//// 
////      DESCRIPTION: Debit Credit Response for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class DebitCreditResponse : BaseResponse
  {
    public string TransactionId { get; set; }
    public string ExtraData { get; set; }
    public int RoundsLeft { get; set; }
  }
}
