﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CreditResponse.cs
//// 
////      DESCRIPTION: Credit Response for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class CreditResponse : BaseResponse
  {
    public string TransactionId  { get; set;}
  }
}
