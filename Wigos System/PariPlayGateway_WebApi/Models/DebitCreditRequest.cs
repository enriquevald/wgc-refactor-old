﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitCreditRequest.cs
//// 
////      DESCRIPTION: Debit Credit Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class DebitCreditRequest : BaseDebitCreditRequest
  {
    public decimal DebitAmount { get; set; }
    public decimal CreditAmount { get; set; }
    public string CreditType { get; set; }
  }
}
