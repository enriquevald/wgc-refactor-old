﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: RequestResponseCombo.cs
//// 
////      DESCRIPTION: RequestResponseCombo class
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 28-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 28-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------

using System.Net.Http;
namespace PariPlay_GameGateway.Models
{
  public class RequestResponseCombo
  {
    public HttpResponseMessage Response { get; private set; }
    public object Request { get; private set; }

    public RequestResponseCombo(HttpResponseMessage HttpResponseMessage, object Data)
    {
      Response = HttpResponseMessage;
      Request = Data;
    } // RequestResponseCombo
  }
}
