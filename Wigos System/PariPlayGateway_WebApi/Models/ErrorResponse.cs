﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: ErrorResponse.cs
//// 
////      DESCRIPTION: Error Response
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------
using System;

namespace PariPlay_GameGateway.Models
{
  public class ErrorResponse
  {
    public Int32 ErrorCode { get; set; }
    public Decimal Balance { get; set; }
  }
}