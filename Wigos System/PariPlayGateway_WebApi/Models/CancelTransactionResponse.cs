﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CancelTransactionResponse.cs
//// 
////      DESCRIPTION: Cancel Transaction Response for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------

namespace PariPlay_GameGateway.Models
{
  public class CancelTransactionResponse : BaseResponse
  {
    public string TransactionId { get; set; }
    public string ExtraData { get; set; }
    public int RoundsLeft { get; set; }
  }
}
