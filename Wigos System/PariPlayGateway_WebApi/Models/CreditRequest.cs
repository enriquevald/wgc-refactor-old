﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: CreditRequest.cs
//// 
////      DESCRIPTION: Credit Request for Web Service
//// 
////           AUTHOR: Jorge Concheyro
//// 
////    CREATION DATE: 29-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-AGO-2016 JRC    First release.
//// 06-SEP-2016 JBP    Add inherits.
////------------------------------------------------------------------------------
using System;

namespace PariPlay_GameGateway.Models
{
  public class CreditRequest : BaseDebitCreditRequest
  {
    public decimal Amount { get; set;}
    public string CreditType { get; set;}
    public Boolean EndGame { get; set;}
    public int[] TransactionConfiguration { get; set;}    
   }
}
