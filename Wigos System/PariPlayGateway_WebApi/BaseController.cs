﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: BaseController.cs
//// 
////      DESCRIPTION: Base Controller
//// 
////           AUTHOR: Pablo Molina
//// 
////    CREATION DATE: 31-AGO-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 31-AGO-2016 PDM    First release.
////------------------------------------------------------------------------------

using PariPlay_GameGateway.Common;
using PariPlay_GameGateway.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Controllers
{
  public class BaseController : ApiController
  {

    #region "Protected Methods"

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Func"></param>
    /// <returns></returns>
    protected HttpResponseMessage Execute(Func<HttpResponseMessage> Func, String Token)
    {
      try
      {
        //if (!VerifyToken(Token))
        //{
        //  return Request.CreateResponse(HttpStatusCode.BadRequest, BuildErrorResponse(ErrorCode.InvalidToken, 0, 0, 0));
        //}

        return Func.Invoke();
      }
      catch (Exception _ex)
      {
        Log.Error(_ex.Message);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.InternalServerError, _ex.Message);
        return response;
      }
    }//Execute

    /// <summary>
    /// Verify the token
    /// </summary>
    /// <param name="Token"></param>
    /// <returns></returns>
    protected Boolean VerifyToken(String Token)
    {
      String _tokenpattern;

      _tokenpattern = Util.GetSetting<String>("TokenPattern");

      if (!String.IsNullOrEmpty(_tokenpattern))
      {
        return true;
      }

      return true;
    }//VerifyToken

    /// <summary>
    /// Build the error response
    /// </summary>
    /// <param name="ErrorCode"></param>
    /// <param name="Balance"></param>
    /// <param name="BonusBalance"></param>
    /// <param name="RoundsLeft"></param>
    /// <returns>ErrorResponse</returns>
    //protected ErrorResponse BuildErrorResponse(ErrorCode ErrorCode, Decimal Balance, Decimal BonusBalance, int RoundsLeft)
    //{
    //  ErrorResponse _response;

    //  _response = new ErrorResponse();

    //  _response.ErrorCode = (int)ErrorCode;
    //  _response.Balance = Balance;
    //  _response.BonusBalance = BonusBalance;
    //  _response.RoundsLeft = RoundsLeft;

    //  return _response;
    //}//BuildErrorResponse

    #endregion
  }
}
