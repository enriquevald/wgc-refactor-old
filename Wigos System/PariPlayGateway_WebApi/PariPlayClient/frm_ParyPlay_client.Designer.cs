﻿namespace PariPlayClient
{
  partial class frm_ParyPlay_client
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.label10 = new System.Windows.Forms.Label();
      this.edt_auth_rep_timestamp = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.edt_auth_rep_rounds_left = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.edt_auth_rep_financial_mode = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.edt_auth_rep_bonus_balance = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.edt_auth_rep_balance = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.edt_auth_Password_req = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.edt_auth_UserName_req = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.edt_auth_rep = new System.Windows.Forms.TextBox();
      this.btnAuthenticate = new System.Windows.Forms.Button();
      this.edt_auth_token_req = new System.Windows.Forms.TextBox();
      this.edt_auth_req = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.label27 = new System.Windows.Forms.Label();
      this.edt_debit_transaction_configuration_req = new System.Windows.Forms.TextBox();
      this.label26 = new System.Windows.Forms.Label();
      this.edt_debit_feature_id_req = new System.Windows.Forms.TextBox();
      this.label25 = new System.Windows.Forms.Label();
      this.edt_debit_feature_req = new System.Windows.Forms.TextBox();
      this.label24 = new System.Windows.Forms.Label();
      this.edt_debit_endgame_req = new System.Windows.Forms.TextBox();
      this.label23 = new System.Windows.Forms.Label();
      this.edt_debit_amount_req = new System.Windows.Forms.TextBox();
      this.label22 = new System.Windows.Forms.Label();
      this.edt_debit_transaction_id_req = new System.Windows.Forms.TextBox();
      this.label21 = new System.Windows.Forms.Label();
      this.edt_debit_round_id_req = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.edt_debit_timestamp_rep = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.edt_debit_transaction_id_rep = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.edt_debit_bonus_balance_rep = new System.Windows.Forms.TextBox();
      this.label15 = new System.Windows.Forms.Label();
      this.edt_debit_balance_rep = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.edt_debit_playerid_req = new System.Windows.Forms.TextBox();
      this.label17 = new System.Windows.Forms.Label();
      this.edt_debit_gamecode_req = new System.Windows.Forms.TextBox();
      this.label18 = new System.Windows.Forms.Label();
      this.edt_debit_rep = new System.Windows.Forms.TextBox();
      this.btnDebit = new System.Windows.Forms.Button();
      this.edt_debit_token_req = new System.Windows.Forms.TextBox();
      this.edt_debit_req = new System.Windows.Forms.TextBox();
      this.label19 = new System.Windows.Forms.Label();
      this.label20 = new System.Windows.Forms.Label();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.label43 = new System.Windows.Forms.Label();
      this.edt_credit_feature_req = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.edt_credit_transaction_configuration_req = new System.Windows.Forms.TextBox();
      this.label28 = new System.Windows.Forms.Label();
      this.edt_credit_featureid_req = new System.Windows.Forms.TextBox();
      this.label29 = new System.Windows.Forms.Label();
      this.edt_credit_endgame_req = new System.Windows.Forms.TextBox();
      this.label30 = new System.Windows.Forms.Label();
      this.edt_credit_credit_type_req = new System.Windows.Forms.TextBox();
      this.label31 = new System.Windows.Forms.Label();
      this.edt_credit_amount_req = new System.Windows.Forms.TextBox();
      this.label32 = new System.Windows.Forms.Label();
      this.edt_credit_transactionid_req = new System.Windows.Forms.TextBox();
      this.label33 = new System.Windows.Forms.Label();
      this.edt_credit_roundid_req = new System.Windows.Forms.TextBox();
      this.label34 = new System.Windows.Forms.Label();
      this.edt_credit_timestamp_rep = new System.Windows.Forms.TextBox();
      this.label35 = new System.Windows.Forms.Label();
      this.edt_credit_transaction_id_rep = new System.Windows.Forms.TextBox();
      this.label36 = new System.Windows.Forms.Label();
      this.edt_credit_bonus_balance_rep = new System.Windows.Forms.TextBox();
      this.label37 = new System.Windows.Forms.Label();
      this.edt_credit_balance_rep = new System.Windows.Forms.TextBox();
      this.label38 = new System.Windows.Forms.Label();
      this.edt_credit_playerid_req = new System.Windows.Forms.TextBox();
      this.label39 = new System.Windows.Forms.Label();
      this.edt_credit_gamecode_req = new System.Windows.Forms.TextBox();
      this.label40 = new System.Windows.Forms.Label();
      this.edt_credit_rep = new System.Windows.Forms.TextBox();
      this.btnCredit = new System.Windows.Forms.Button();
      this.edt_credit_token_req = new System.Windows.Forms.TextBox();
      this.edt_credit_req = new System.Windows.Forms.TextBox();
      this.label41 = new System.Windows.Forms.Label();
      this.label42 = new System.Windows.Forms.Label();
      this.tabPage4 = new System.Windows.Forms.TabPage();
      this.label44 = new System.Windows.Forms.Label();
      this.label45 = new System.Windows.Forms.Label();
      this.edt_balance_timestamp_rep = new System.Windows.Forms.TextBox();
      this.edt_balance_rounds_left_rep = new System.Windows.Forms.TextBox();
      this.label47 = new System.Windows.Forms.Label();
      this.edt_balance_bonus_balance_rep = new System.Windows.Forms.TextBox();
      this.label48 = new System.Windows.Forms.Label();
      this.edt_balance_balance_rep = new System.Windows.Forms.TextBox();
      this.label49 = new System.Windows.Forms.Label();
      this.edt_balance_password_req = new System.Windows.Forms.TextBox();
      this.label50 = new System.Windows.Forms.Label();
      this.edt_balance_username_req = new System.Windows.Forms.TextBox();
      this.label51 = new System.Windows.Forms.Label();
      this.edt_balance_rep = new System.Windows.Forms.TextBox();
      this.btnBalance = new System.Windows.Forms.Button();
      this.edt_balance_token_req = new System.Windows.Forms.TextBox();
      this.edt_balance_req = new System.Windows.Forms.TextBox();
      this.label52 = new System.Windows.Forms.Label();
      this.label53 = new System.Windows.Forms.Label();
      this.tabPage5 = new System.Windows.Forms.TabPage();
      this.label56 = new System.Windows.Forms.Label();
      this.label55 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_timestamp_rep = new System.Windows.Forms.TextBox();
      this.edt_cancel_bet_roundsleft_rep = new System.Windows.Forms.TextBox();
      this.label54 = new System.Windows.Forms.Label();
      this.label46 = new System.Windows.Forms.Label();
      this.label69 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_amount_req = new System.Windows.Forms.TextBox();
      this.edt_cancel_bet_reason_req = new System.Windows.Forms.TextBox();
      this.edt_cancel_bet_transactionid_req = new System.Windows.Forms.TextBox();
      this.label57 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_cancelentireround_req = new System.Windows.Forms.TextBox();
      this.label58 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_roundid_req = new System.Windows.Forms.TextBox();
      this.label59 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_playerid_req = new System.Windows.Forms.TextBox();
      this.label60 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_extradata_rep = new System.Windows.Forms.TextBox();
      this.label61 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_transactionid_rep = new System.Windows.Forms.TextBox();
      this.label62 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_bonusbalance_rep = new System.Windows.Forms.TextBox();
      this.label63 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_balance_rep = new System.Windows.Forms.TextBox();
      this.label64 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_gamecode_req = new System.Windows.Forms.TextBox();
      this.label65 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_reftransactionid_req = new System.Windows.Forms.TextBox();
      this.label66 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_rep = new System.Windows.Forms.TextBox();
      this.btnCancelBet = new System.Windows.Forms.Button();
      this.edt_cancel_bet_token_req = new System.Windows.Forms.TextBox();
      this.edt_cancel_bet_req = new System.Windows.Forms.TextBox();
      this.label67 = new System.Windows.Forms.Label();
      this.label68 = new System.Windows.Forms.Label();
      this.tabPage6 = new System.Windows.Forms.TabPage();
      this.label87 = new System.Windows.Forms.Label();
      this.label73 = new System.Windows.Forms.Label();
      this.edt_debitcredit_timestamp_rep = new System.Windows.Forms.TextBox();
      this.edt_debitcredit_roundsleft_rep = new System.Windows.Forms.TextBox();
      this.label71 = new System.Windows.Forms.Label();
      this.label70 = new System.Windows.Forms.Label();
      this.edt_debitcredit_feature_req = new System.Windows.Forms.TextBox();
      this.label72 = new System.Windows.Forms.Label();
      this.edt_debitcredit_featureid_req = new System.Windows.Forms.TextBox();
      this.edt_debitcredit_credit_type_req = new System.Windows.Forms.TextBox();
      this.label74 = new System.Windows.Forms.Label();
      this.edt_debitcredit_credit_amount_req = new System.Windows.Forms.TextBox();
      this.label75 = new System.Windows.Forms.Label();
      this.edt_debitcredit_debit_amount_req = new System.Windows.Forms.TextBox();
      this.label76 = new System.Windows.Forms.Label();
      this.edt_debitcredit_transactionid_req = new System.Windows.Forms.TextBox();
      this.label77 = new System.Windows.Forms.Label();
      this.edt_debitcredit_roundid_req = new System.Windows.Forms.TextBox();
      this.label78 = new System.Windows.Forms.Label();
      this.edt_debitcredit_extradata_rep = new System.Windows.Forms.TextBox();
      this.label79 = new System.Windows.Forms.Label();
      this.edt_debitcredit_transactionid_rep = new System.Windows.Forms.TextBox();
      this.label80 = new System.Windows.Forms.Label();
      this.edt_debitcredit_bonusbalance_rep = new System.Windows.Forms.TextBox();
      this.label81 = new System.Windows.Forms.Label();
      this.edt_debitcredit_balance_rep = new System.Windows.Forms.TextBox();
      this.label82 = new System.Windows.Forms.Label();
      this.edt_debitcredit_playerid_req = new System.Windows.Forms.TextBox();
      this.label83 = new System.Windows.Forms.Label();
      this.edt_debitcredit_gamecode_req = new System.Windows.Forms.TextBox();
      this.label84 = new System.Windows.Forms.Label();
      this.edt_debitcredit_rep = new System.Windows.Forms.TextBox();
      this.btnDebitCredit = new System.Windows.Forms.Button();
      this.edt_debitcredit_token_req = new System.Windows.Forms.TextBox();
      this.edt_debitcredit_req = new System.Windows.Forms.TextBox();
      this.label85 = new System.Windows.Forms.Label();
      this.label86 = new System.Windows.Forms.Label();
      this.tabPage7 = new System.Windows.Forms.TabPage();
      this.label88 = new System.Windows.Forms.Label();
      this.edt_endgame_transactionconfiguration_req = new System.Windows.Forms.TextBox();
      this.label93 = new System.Windows.Forms.Label();
      this.edt_endgame_transactionId_req = new System.Windows.Forms.TextBox();
      this.label94 = new System.Windows.Forms.Label();
      this.edt_endgame_roundid_req = new System.Windows.Forms.TextBox();
      this.label95 = new System.Windows.Forms.Label();
      this.edt_endgame_timestamp_rep = new System.Windows.Forms.TextBox();
      this.label96 = new System.Windows.Forms.Label();
      this.edt_endgame_transactionid_rep = new System.Windows.Forms.TextBox();
      this.label97 = new System.Windows.Forms.Label();
      this.edt_endgame_bonusbalance_rep = new System.Windows.Forms.TextBox();
      this.label98 = new System.Windows.Forms.Label();
      this.edt_endgame_balance_rep = new System.Windows.Forms.TextBox();
      this.label99 = new System.Windows.Forms.Label();
      this.edt_endgame_playerid_req = new System.Windows.Forms.TextBox();
      this.label100 = new System.Windows.Forms.Label();
      this.edt_endgame_gamecode_req = new System.Windows.Forms.TextBox();
      this.label101 = new System.Windows.Forms.Label();
      this.edt_endgame_rep = new System.Windows.Forms.TextBox();
      this.btnEndGame = new System.Windows.Forms.Button();
      this.edt_endgame_token_req = new System.Windows.Forms.TextBox();
      this.edt_endgame_req = new System.Windows.Forms.TextBox();
      this.label102 = new System.Windows.Forms.Label();
      this.label103 = new System.Windows.Forms.Label();
      this.label89 = new System.Windows.Forms.Label();
      this.edt_debit_password_req = new System.Windows.Forms.TextBox();
      this.label90 = new System.Windows.Forms.Label();
      this.edt_debit_username_req = new System.Windows.Forms.TextBox();
      this.label91 = new System.Windows.Forms.Label();
      this.edt_credit_password_req = new System.Windows.Forms.TextBox();
      this.label92 = new System.Windows.Forms.Label();
      this.edt_credit_username_req = new System.Windows.Forms.TextBox();
      this.label104 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_password_req = new System.Windows.Forms.TextBox();
      this.label105 = new System.Windows.Forms.Label();
      this.edt_cancel_bet_username_req = new System.Windows.Forms.TextBox();
      this.label106 = new System.Windows.Forms.Label();
      this.edt_debitcredit_password_req = new System.Windows.Forms.TextBox();
      this.label107 = new System.Windows.Forms.Label();
      this.edt_debitcredit_username_req = new System.Windows.Forms.TextBox();
      this.label108 = new System.Windows.Forms.Label();
      this.edt_endgame_password_req = new System.Windows.Forms.TextBox();
      this.label109 = new System.Windows.Forms.Label();
      this.edt_endgame_username_req = new System.Windows.Forms.TextBox();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.tabPage4.SuspendLayout();
      this.tabPage5.SuspendLayout();
      this.tabPage6.SuspendLayout();
      this.tabPage7.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage3);
      this.tabControl1.Controls.Add(this.tabPage4);
      this.tabControl1.Controls.Add(this.tabPage5);
      this.tabControl1.Controls.Add(this.tabPage6);
      this.tabControl1.Controls.Add(this.tabPage7);
      this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl1.Location = new System.Drawing.Point(0, 0);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(692, 612);
      this.tabControl1.TabIndex = 0;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.label10);
      this.tabPage1.Controls.Add(this.edt_auth_rep_timestamp);
      this.tabPage1.Controls.Add(this.label9);
      this.tabPage1.Controls.Add(this.edt_auth_rep_rounds_left);
      this.tabPage1.Controls.Add(this.label8);
      this.tabPage1.Controls.Add(this.edt_auth_rep_financial_mode);
      this.tabPage1.Controls.Add(this.label7);
      this.tabPage1.Controls.Add(this.edt_auth_rep_bonus_balance);
      this.tabPage1.Controls.Add(this.label6);
      this.tabPage1.Controls.Add(this.edt_auth_rep_balance);
      this.tabPage1.Controls.Add(this.label5);
      this.tabPage1.Controls.Add(this.edt_auth_Password_req);
      this.tabPage1.Controls.Add(this.label4);
      this.tabPage1.Controls.Add(this.edt_auth_UserName_req);
      this.tabPage1.Controls.Add(this.label3);
      this.tabPage1.Controls.Add(this.edt_auth_rep);
      this.tabPage1.Controls.Add(this.btnAuthenticate);
      this.tabPage1.Controls.Add(this.edt_auth_token_req);
      this.tabPage1.Controls.Add(this.edt_auth_req);
      this.tabPage1.Controls.Add(this.label2);
      this.tabPage1.Controls.Add(this.label1);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(684, 586);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Authenticate";
      this.tabPage1.UseVisualStyleBackColor = true;
      this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(334, 156);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(58, 13);
      this.label10.TabIndex = 20;
      this.label10.Text = "Timestamp";
      // 
      // edt_auth_rep_timestamp
      // 
      this.edt_auth_rep_timestamp.Location = new System.Drawing.Point(480, 153);
      this.edt_auth_rep_timestamp.Name = "edt_auth_rep_timestamp";
      this.edt_auth_rep_timestamp.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_rep_timestamp.TabIndex = 9;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(334, 130);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(65, 13);
      this.label9.TabIndex = 18;
      this.label9.Text = "Rounds Left";
      // 
      // edt_auth_rep_rounds_left
      // 
      this.edt_auth_rep_rounds_left.Location = new System.Drawing.Point(480, 127);
      this.edt_auth_rep_rounds_left.Name = "edt_auth_rep_rounds_left";
      this.edt_auth_rep_rounds_left.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_rep_rounds_left.TabIndex = 8;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(334, 104);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(79, 13);
      this.label8.TabIndex = 16;
      this.label8.Text = "Financial Mode";
      // 
      // edt_auth_rep_financial_mode
      // 
      this.edt_auth_rep_financial_mode.Location = new System.Drawing.Point(480, 101);
      this.edt_auth_rep_financial_mode.Name = "edt_auth_rep_financial_mode";
      this.edt_auth_rep_financial_mode.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_rep_financial_mode.TabIndex = 7;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(334, 78);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(79, 13);
      this.label7.TabIndex = 14;
      this.label7.Text = "Bonus Balance";
      // 
      // edt_auth_rep_bonus_balance
      // 
      this.edt_auth_rep_bonus_balance.Location = new System.Drawing.Point(480, 75);
      this.edt_auth_rep_bonus_balance.Name = "edt_auth_rep_bonus_balance";
      this.edt_auth_rep_bonus_balance.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_rep_bonus_balance.TabIndex = 6;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(334, 52);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(46, 13);
      this.label6.TabIndex = 12;
      this.label6.Text = "Balance";
      // 
      // edt_auth_rep_balance
      // 
      this.edt_auth_rep_balance.Location = new System.Drawing.Point(480, 49);
      this.edt_auth_rep_balance.Name = "edt_auth_rep_balance";
      this.edt_auth_rep_balance.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_rep_balance.TabIndex = 5;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 104);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(53, 13);
      this.label5.TabIndex = 10;
      this.label5.Text = "Password";
      // 
      // edt_auth_Password_req
      // 
      this.edt_auth_Password_req.Location = new System.Drawing.Point(131, 101);
      this.edt_auth_Password_req.Name = "edt_auth_Password_req";
      this.edt_auth_Password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_Password_req.TabIndex = 2;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(5, 78);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(57, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "UserName";
      // 
      // edt_auth_UserName_req
      // 
      this.edt_auth_UserName_req.Location = new System.Drawing.Point(131, 75);
      this.edt_auth_UserName_req.Name = "edt_auth_UserName_req";
      this.edt_auth_UserName_req.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_UserName_req.TabIndex = 1;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(5, 52);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(38, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Token";
      // 
      // edt_auth_rep
      // 
      this.edt_auth_rep.Location = new System.Drawing.Point(337, 231);
      this.edt_auth_rep.Multiline = true;
      this.edt_auth_rep.Name = "edt_auth_rep";
      this.edt_auth_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_auth_rep.TabIndex = 10;
      // 
      // btnAuthenticate
      // 
      this.btnAuthenticate.Location = new System.Drawing.Point(99, 401);
      this.btnAuthenticate.Name = "btnAuthenticate";
      this.btnAuthenticate.Size = new System.Drawing.Size(112, 23);
      this.btnAuthenticate.TabIndex = 4;
      this.btnAuthenticate.Text = "Execute";
      this.btnAuthenticate.UseVisualStyleBackColor = true;
      this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
      // 
      // edt_auth_token_req
      // 
      this.edt_auth_token_req.Location = new System.Drawing.Point(131, 49);
      this.edt_auth_token_req.Name = "edt_auth_token_req";
      this.edt_auth_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_auth_token_req.TabIndex = 0;
      // 
      // edt_auth_req
      // 
      this.edt_auth_req.Location = new System.Drawing.Point(8, 231);
      this.edt_auth_req.Multiline = true;
      this.edt_auth_req.Name = "edt_auth_req";
      this.edt_auth_req.Size = new System.Drawing.Size(307, 164);
      this.edt_auth_req.TabIndex = 3;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(493, 12);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(55, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Response";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(111, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(47, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Request";
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.label89);
      this.tabPage2.Controls.Add(this.edt_debit_password_req);
      this.tabPage2.Controls.Add(this.label90);
      this.tabPage2.Controls.Add(this.edt_debit_username_req);
      this.tabPage2.Controls.Add(this.label27);
      this.tabPage2.Controls.Add(this.edt_debit_transaction_configuration_req);
      this.tabPage2.Controls.Add(this.label26);
      this.tabPage2.Controls.Add(this.edt_debit_feature_id_req);
      this.tabPage2.Controls.Add(this.label25);
      this.tabPage2.Controls.Add(this.edt_debit_feature_req);
      this.tabPage2.Controls.Add(this.label24);
      this.tabPage2.Controls.Add(this.edt_debit_endgame_req);
      this.tabPage2.Controls.Add(this.label23);
      this.tabPage2.Controls.Add(this.edt_debit_amount_req);
      this.tabPage2.Controls.Add(this.label22);
      this.tabPage2.Controls.Add(this.edt_debit_transaction_id_req);
      this.tabPage2.Controls.Add(this.label21);
      this.tabPage2.Controls.Add(this.edt_debit_round_id_req);
      this.tabPage2.Controls.Add(this.label11);
      this.tabPage2.Controls.Add(this.edt_debit_timestamp_rep);
      this.tabPage2.Controls.Add(this.label13);
      this.tabPage2.Controls.Add(this.edt_debit_transaction_id_rep);
      this.tabPage2.Controls.Add(this.label14);
      this.tabPage2.Controls.Add(this.edt_debit_bonus_balance_rep);
      this.tabPage2.Controls.Add(this.label15);
      this.tabPage2.Controls.Add(this.edt_debit_balance_rep);
      this.tabPage2.Controls.Add(this.label16);
      this.tabPage2.Controls.Add(this.edt_debit_playerid_req);
      this.tabPage2.Controls.Add(this.label17);
      this.tabPage2.Controls.Add(this.edt_debit_gamecode_req);
      this.tabPage2.Controls.Add(this.label18);
      this.tabPage2.Controls.Add(this.edt_debit_rep);
      this.tabPage2.Controls.Add(this.btnDebit);
      this.tabPage2.Controls.Add(this.edt_debit_token_req);
      this.tabPage2.Controls.Add(this.edt_debit_req);
      this.tabPage2.Controls.Add(this.label19);
      this.tabPage2.Controls.Add(this.label20);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(684, 586);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Debit";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // label27
      // 
      this.label27.AutoSize = true;
      this.label27.Location = new System.Drawing.Point(7, 349);
      this.label27.Name = "label27";
      this.label27.Size = new System.Drawing.Size(117, 13);
      this.label27.TabIndex = 55;
      this.label27.Text = "Transaction Configurat.";
      // 
      // edt_debit_transaction_configuration_req
      // 
      this.edt_debit_transaction_configuration_req.Location = new System.Drawing.Point(133, 346);
      this.edt_debit_transaction_configuration_req.Name = "edt_debit_transaction_configuration_req";
      this.edt_debit_transaction_configuration_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_transaction_configuration_req.TabIndex = 9;
      // 
      // label26
      // 
      this.label26.AutoSize = true;
      this.label26.Location = new System.Drawing.Point(7, 323);
      this.label26.Name = "label26";
      this.label26.Size = new System.Drawing.Size(57, 13);
      this.label26.TabIndex = 53;
      this.label26.Text = "Feature ID";
      // 
      // edt_debit_feature_id_req
      // 
      this.edt_debit_feature_id_req.Location = new System.Drawing.Point(133, 320);
      this.edt_debit_feature_id_req.Name = "edt_debit_feature_id_req";
      this.edt_debit_feature_id_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_feature_id_req.TabIndex = 8;
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(7, 297);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(43, 13);
      this.label25.TabIndex = 51;
      this.label25.Text = "Feature";
      // 
      // edt_debit_feature_req
      // 
      this.edt_debit_feature_req.Location = new System.Drawing.Point(133, 294);
      this.edt_debit_feature_req.Name = "edt_debit_feature_req";
      this.edt_debit_feature_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_feature_req.TabIndex = 7;
      // 
      // label24
      // 
      this.label24.AutoSize = true;
      this.label24.Location = new System.Drawing.Point(7, 271);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(57, 13);
      this.label24.TabIndex = 49;
      this.label24.Text = "End Game";
      // 
      // edt_debit_endgame_req
      // 
      this.edt_debit_endgame_req.Location = new System.Drawing.Point(133, 268);
      this.edt_debit_endgame_req.Name = "edt_debit_endgame_req";
      this.edt_debit_endgame_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_endgame_req.TabIndex = 6;
      // 
      // label23
      // 
      this.label23.AutoSize = true;
      this.label23.Location = new System.Drawing.Point(7, 245);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(43, 13);
      this.label23.TabIndex = 47;
      this.label23.Text = "Amount";
      // 
      // edt_debit_amount_req
      // 
      this.edt_debit_amount_req.Location = new System.Drawing.Point(133, 242);
      this.edt_debit_amount_req.Name = "edt_debit_amount_req";
      this.edt_debit_amount_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_amount_req.TabIndex = 5;
      // 
      // label22
      // 
      this.label22.AutoSize = true;
      this.label22.Location = new System.Drawing.Point(7, 219);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(77, 13);
      this.label22.TabIndex = 45;
      this.label22.Text = "Transaction ID";
      // 
      // edt_debit_transaction_id_req
      // 
      this.edt_debit_transaction_id_req.Location = new System.Drawing.Point(133, 216);
      this.edt_debit_transaction_id_req.Name = "edt_debit_transaction_id_req";
      this.edt_debit_transaction_id_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_transaction_id_req.TabIndex = 4;
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(7, 193);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(53, 13);
      this.label21.TabIndex = 43;
      this.label21.Text = "Round ID";
      // 
      // edt_debit_round_id_req
      // 
      this.edt_debit_round_id_req.Location = new System.Drawing.Point(133, 190);
      this.edt_debit_round_id_req.Name = "edt_debit_round_id_req";
      this.edt_debit_round_id_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_round_id_req.TabIndex = 3;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(335, 140);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(58, 13);
      this.label11.TabIndex = 41;
      this.label11.Text = "Timestamp";
      // 
      // edt_debit_timestamp_rep
      // 
      this.edt_debit_timestamp_rep.Location = new System.Drawing.Point(481, 137);
      this.edt_debit_timestamp_rep.Name = "edt_debit_timestamp_rep";
      this.edt_debit_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_timestamp_rep.TabIndex = 15;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(335, 114);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(77, 13);
      this.label13.TabIndex = 37;
      this.label13.Text = "Transaction ID";
      // 
      // edt_debit_transaction_id_rep
      // 
      this.edt_debit_transaction_id_rep.Location = new System.Drawing.Point(481, 111);
      this.edt_debit_transaction_id_rep.Name = "edt_debit_transaction_id_rep";
      this.edt_debit_transaction_id_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_transaction_id_rep.TabIndex = 14;
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(335, 88);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(79, 13);
      this.label14.TabIndex = 35;
      this.label14.Text = "Bonus Balance";
      // 
      // edt_debit_bonus_balance_rep
      // 
      this.edt_debit_bonus_balance_rep.Location = new System.Drawing.Point(481, 85);
      this.edt_debit_bonus_balance_rep.Name = "edt_debit_bonus_balance_rep";
      this.edt_debit_bonus_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_bonus_balance_rep.TabIndex = 13;
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(335, 62);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(46, 13);
      this.label15.TabIndex = 33;
      this.label15.Text = "Balance";
      // 
      // edt_debit_balance_rep
      // 
      this.edt_debit_balance_rep.Location = new System.Drawing.Point(481, 59);
      this.edt_debit_balance_rep.Name = "edt_debit_balance_rep";
      this.edt_debit_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_balance_rep.TabIndex = 12;
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(7, 167);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(50, 13);
      this.label16.TabIndex = 31;
      this.label16.Text = "Player ID";
      // 
      // edt_debit_playerid_req
      // 
      this.edt_debit_playerid_req.Location = new System.Drawing.Point(133, 164);
      this.edt_debit_playerid_req.Name = "edt_debit_playerid_req";
      this.edt_debit_playerid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_playerid_req.TabIndex = 2;
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(7, 141);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(60, 13);
      this.label17.TabIndex = 29;
      this.label17.Text = "GameCode";
      // 
      // edt_debit_gamecode_req
      // 
      this.edt_debit_gamecode_req.Location = new System.Drawing.Point(133, 138);
      this.edt_debit_gamecode_req.Name = "edt_debit_gamecode_req";
      this.edt_debit_gamecode_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_gamecode_req.TabIndex = 1;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(7, 62);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(38, 13);
      this.label18.TabIndex = 27;
      this.label18.Text = "Token";
      // 
      // edt_debit_rep
      // 
      this.edt_debit_rep.Location = new System.Drawing.Point(338, 380);
      this.edt_debit_rep.Multiline = true;
      this.edt_debit_rep.Name = "edt_debit_rep";
      this.edt_debit_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_debit_rep.TabIndex = 16;
      // 
      // btnDebit
      // 
      this.btnDebit.Location = new System.Drawing.Point(101, 550);
      this.btnDebit.Name = "btnDebit";
      this.btnDebit.Size = new System.Drawing.Size(112, 23);
      this.btnDebit.TabIndex = 11;
      this.btnDebit.Text = "Execute";
      this.btnDebit.UseVisualStyleBackColor = true;
      this.btnDebit.Click += new System.EventHandler(this.btnDebit_Click);
      // 
      // edt_debit_token_req
      // 
      this.edt_debit_token_req.Location = new System.Drawing.Point(133, 59);
      this.edt_debit_token_req.Name = "edt_debit_token_req";
      this.edt_debit_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_token_req.TabIndex = 0;
      // 
      // edt_debit_req
      // 
      this.edt_debit_req.Location = new System.Drawing.Point(10, 380);
      this.edt_debit_req.Multiline = true;
      this.edt_debit_req.Name = "edt_debit_req";
      this.edt_debit_req.Size = new System.Drawing.Size(307, 164);
      this.edt_debit_req.TabIndex = 10;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(495, 22);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(55, 13);
      this.label19.TabIndex = 22;
      this.label19.Text = "Response";
      // 
      // label20
      // 
      this.label20.AutoSize = true;
      this.label20.Location = new System.Drawing.Point(113, 22);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(47, 13);
      this.label20.TabIndex = 21;
      this.label20.Text = "Request";
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.label91);
      this.tabPage3.Controls.Add(this.edt_credit_password_req);
      this.tabPage3.Controls.Add(this.label92);
      this.tabPage3.Controls.Add(this.edt_credit_username_req);
      this.tabPage3.Controls.Add(this.label43);
      this.tabPage3.Controls.Add(this.edt_credit_feature_req);
      this.tabPage3.Controls.Add(this.label12);
      this.tabPage3.Controls.Add(this.edt_credit_transaction_configuration_req);
      this.tabPage3.Controls.Add(this.label28);
      this.tabPage3.Controls.Add(this.edt_credit_featureid_req);
      this.tabPage3.Controls.Add(this.label29);
      this.tabPage3.Controls.Add(this.edt_credit_endgame_req);
      this.tabPage3.Controls.Add(this.label30);
      this.tabPage3.Controls.Add(this.edt_credit_credit_type_req);
      this.tabPage3.Controls.Add(this.label31);
      this.tabPage3.Controls.Add(this.edt_credit_amount_req);
      this.tabPage3.Controls.Add(this.label32);
      this.tabPage3.Controls.Add(this.edt_credit_transactionid_req);
      this.tabPage3.Controls.Add(this.label33);
      this.tabPage3.Controls.Add(this.edt_credit_roundid_req);
      this.tabPage3.Controls.Add(this.label34);
      this.tabPage3.Controls.Add(this.edt_credit_timestamp_rep);
      this.tabPage3.Controls.Add(this.label35);
      this.tabPage3.Controls.Add(this.edt_credit_transaction_id_rep);
      this.tabPage3.Controls.Add(this.label36);
      this.tabPage3.Controls.Add(this.edt_credit_bonus_balance_rep);
      this.tabPage3.Controls.Add(this.label37);
      this.tabPage3.Controls.Add(this.edt_credit_balance_rep);
      this.tabPage3.Controls.Add(this.label38);
      this.tabPage3.Controls.Add(this.edt_credit_playerid_req);
      this.tabPage3.Controls.Add(this.label39);
      this.tabPage3.Controls.Add(this.edt_credit_gamecode_req);
      this.tabPage3.Controls.Add(this.label40);
      this.tabPage3.Controls.Add(this.edt_credit_rep);
      this.tabPage3.Controls.Add(this.btnCredit);
      this.tabPage3.Controls.Add(this.edt_credit_token_req);
      this.tabPage3.Controls.Add(this.edt_credit_req);
      this.tabPage3.Controls.Add(this.label41);
      this.tabPage3.Controls.Add(this.label42);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage3.Size = new System.Drawing.Size(684, 586);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Credit";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // label43
      // 
      this.label43.AutoSize = true;
      this.label43.Location = new System.Drawing.Point(7, 323);
      this.label43.Name = "label43";
      this.label43.Size = new System.Drawing.Size(43, 13);
      this.label43.TabIndex = 90;
      this.label43.Text = "Feature";
      // 
      // edt_credit_feature_req
      // 
      this.edt_credit_feature_req.Location = new System.Drawing.Point(133, 320);
      this.edt_credit_feature_req.Name = "edt_credit_feature_req";
      this.edt_credit_feature_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_feature_req.TabIndex = 8;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(7, 375);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(117, 13);
      this.label12.TabIndex = 88;
      this.label12.Text = "Transaction Configurat.";
      // 
      // edt_credit_transaction_configuration_req
      // 
      this.edt_credit_transaction_configuration_req.Location = new System.Drawing.Point(133, 372);
      this.edt_credit_transaction_configuration_req.Name = "edt_credit_transaction_configuration_req";
      this.edt_credit_transaction_configuration_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_transaction_configuration_req.TabIndex = 10;
      // 
      // label28
      // 
      this.label28.AutoSize = true;
      this.label28.Location = new System.Drawing.Point(7, 349);
      this.label28.Name = "label28";
      this.label28.Size = new System.Drawing.Size(57, 13);
      this.label28.TabIndex = 87;
      this.label28.Text = "Feature ID";
      // 
      // edt_credit_featureid_req
      // 
      this.edt_credit_featureid_req.Location = new System.Drawing.Point(133, 346);
      this.edt_credit_featureid_req.Name = "edt_credit_featureid_req";
      this.edt_credit_featureid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_featureid_req.TabIndex = 9;
      // 
      // label29
      // 
      this.label29.AutoSize = true;
      this.label29.Location = new System.Drawing.Point(7, 295);
      this.label29.Name = "label29";
      this.label29.Size = new System.Drawing.Size(57, 13);
      this.label29.TabIndex = 86;
      this.label29.Text = "End Game";
      // 
      // edt_credit_endgame_req
      // 
      this.edt_credit_endgame_req.Location = new System.Drawing.Point(133, 292);
      this.edt_credit_endgame_req.Name = "edt_credit_endgame_req";
      this.edt_credit_endgame_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_endgame_req.TabIndex = 7;
      // 
      // label30
      // 
      this.label30.AutoSize = true;
      this.label30.Location = new System.Drawing.Point(7, 269);
      this.label30.Name = "label30";
      this.label30.Size = new System.Drawing.Size(61, 13);
      this.label30.TabIndex = 85;
      this.label30.Text = "Credit Type";
      // 
      // edt_credit_credit_type_req
      // 
      this.edt_credit_credit_type_req.Location = new System.Drawing.Point(133, 266);
      this.edt_credit_credit_type_req.Name = "edt_credit_credit_type_req";
      this.edt_credit_credit_type_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_credit_type_req.TabIndex = 6;
      // 
      // label31
      // 
      this.label31.AutoSize = true;
      this.label31.Location = new System.Drawing.Point(7, 243);
      this.label31.Name = "label31";
      this.label31.Size = new System.Drawing.Size(43, 13);
      this.label31.TabIndex = 84;
      this.label31.Text = "Amount";
      // 
      // edt_credit_amount_req
      // 
      this.edt_credit_amount_req.Location = new System.Drawing.Point(133, 240);
      this.edt_credit_amount_req.Name = "edt_credit_amount_req";
      this.edt_credit_amount_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_amount_req.TabIndex = 5;
      // 
      // label32
      // 
      this.label32.AutoSize = true;
      this.label32.Location = new System.Drawing.Point(7, 217);
      this.label32.Name = "label32";
      this.label32.Size = new System.Drawing.Size(77, 13);
      this.label32.TabIndex = 83;
      this.label32.Text = "Transaction ID";
      // 
      // edt_credit_transactionid_req
      // 
      this.edt_credit_transactionid_req.Location = new System.Drawing.Point(133, 214);
      this.edt_credit_transactionid_req.Name = "edt_credit_transactionid_req";
      this.edt_credit_transactionid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_transactionid_req.TabIndex = 4;
      // 
      // label33
      // 
      this.label33.AutoSize = true;
      this.label33.Location = new System.Drawing.Point(7, 191);
      this.label33.Name = "label33";
      this.label33.Size = new System.Drawing.Size(53, 13);
      this.label33.TabIndex = 82;
      this.label33.Text = "Round ID";
      // 
      // edt_credit_roundid_req
      // 
      this.edt_credit_roundid_req.Location = new System.Drawing.Point(133, 188);
      this.edt_credit_roundid_req.Name = "edt_credit_roundid_req";
      this.edt_credit_roundid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_roundid_req.TabIndex = 3;
      // 
      // label34
      // 
      this.label34.AutoSize = true;
      this.label34.Location = new System.Drawing.Point(336, 141);
      this.label34.Name = "label34";
      this.label34.Size = new System.Drawing.Size(58, 13);
      this.label34.TabIndex = 81;
      this.label34.Text = "Timestamp";
      // 
      // edt_credit_timestamp_rep
      // 
      this.edt_credit_timestamp_rep.Location = new System.Drawing.Point(482, 138);
      this.edt_credit_timestamp_rep.Name = "edt_credit_timestamp_rep";
      this.edt_credit_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_timestamp_rep.TabIndex = 16;
      // 
      // label35
      // 
      this.label35.AutoSize = true;
      this.label35.Location = new System.Drawing.Point(336, 115);
      this.label35.Name = "label35";
      this.label35.Size = new System.Drawing.Size(77, 13);
      this.label35.TabIndex = 80;
      this.label35.Text = "Transaction ID";
      // 
      // edt_credit_transaction_id_rep
      // 
      this.edt_credit_transaction_id_rep.Location = new System.Drawing.Point(482, 112);
      this.edt_credit_transaction_id_rep.Name = "edt_credit_transaction_id_rep";
      this.edt_credit_transaction_id_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_transaction_id_rep.TabIndex = 15;
      // 
      // label36
      // 
      this.label36.AutoSize = true;
      this.label36.Location = new System.Drawing.Point(336, 89);
      this.label36.Name = "label36";
      this.label36.Size = new System.Drawing.Size(79, 13);
      this.label36.TabIndex = 79;
      this.label36.Text = "Bonus Balance";
      // 
      // edt_credit_bonus_balance_rep
      // 
      this.edt_credit_bonus_balance_rep.Location = new System.Drawing.Point(482, 86);
      this.edt_credit_bonus_balance_rep.Name = "edt_credit_bonus_balance_rep";
      this.edt_credit_bonus_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_bonus_balance_rep.TabIndex = 14;
      // 
      // label37
      // 
      this.label37.AutoSize = true;
      this.label37.Location = new System.Drawing.Point(336, 63);
      this.label37.Name = "label37";
      this.label37.Size = new System.Drawing.Size(46, 13);
      this.label37.TabIndex = 78;
      this.label37.Text = "Balance";
      // 
      // edt_credit_balance_rep
      // 
      this.edt_credit_balance_rep.Location = new System.Drawing.Point(482, 60);
      this.edt_credit_balance_rep.Name = "edt_credit_balance_rep";
      this.edt_credit_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_balance_rep.TabIndex = 13;
      // 
      // label38
      // 
      this.label38.AutoSize = true;
      this.label38.Location = new System.Drawing.Point(7, 165);
      this.label38.Name = "label38";
      this.label38.Size = new System.Drawing.Size(50, 13);
      this.label38.TabIndex = 77;
      this.label38.Text = "Player ID";
      // 
      // edt_credit_playerid_req
      // 
      this.edt_credit_playerid_req.Location = new System.Drawing.Point(133, 162);
      this.edt_credit_playerid_req.Name = "edt_credit_playerid_req";
      this.edt_credit_playerid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_playerid_req.TabIndex = 2;
      // 
      // label39
      // 
      this.label39.AutoSize = true;
      this.label39.Location = new System.Drawing.Point(7, 139);
      this.label39.Name = "label39";
      this.label39.Size = new System.Drawing.Size(60, 13);
      this.label39.TabIndex = 76;
      this.label39.Text = "GameCode";
      // 
      // edt_credit_gamecode_req
      // 
      this.edt_credit_gamecode_req.Location = new System.Drawing.Point(133, 136);
      this.edt_credit_gamecode_req.Name = "edt_credit_gamecode_req";
      this.edt_credit_gamecode_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_gamecode_req.TabIndex = 1;
      // 
      // label40
      // 
      this.label40.AutoSize = true;
      this.label40.Location = new System.Drawing.Point(7, 63);
      this.label40.Name = "label40";
      this.label40.Size = new System.Drawing.Size(38, 13);
      this.label40.TabIndex = 75;
      this.label40.Text = "Token";
      // 
      // edt_credit_rep
      // 
      this.edt_credit_rep.Location = new System.Drawing.Point(339, 399);
      this.edt_credit_rep.Multiline = true;
      this.edt_credit_rep.Name = "edt_credit_rep";
      this.edt_credit_rep.Size = new System.Drawing.Size(327, 143);
      this.edt_credit_rep.TabIndex = 18;
      // 
      // btnCredit
      // 
      this.btnCredit.Location = new System.Drawing.Point(101, 546);
      this.btnCredit.Name = "btnCredit";
      this.btnCredit.Size = new System.Drawing.Size(112, 23);
      this.btnCredit.TabIndex = 12;
      this.btnCredit.Text = "Execute";
      this.btnCredit.UseVisualStyleBackColor = true;
      this.btnCredit.Click += new System.EventHandler(this.btnCredit_Click);
      // 
      // edt_credit_token_req
      // 
      this.edt_credit_token_req.Location = new System.Drawing.Point(133, 60);
      this.edt_credit_token_req.Name = "edt_credit_token_req";
      this.edt_credit_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_token_req.TabIndex = 0;
      // 
      // edt_credit_req
      // 
      this.edt_credit_req.Location = new System.Drawing.Point(10, 399);
      this.edt_credit_req.Multiline = true;
      this.edt_credit_req.Name = "edt_credit_req";
      this.edt_credit_req.Size = new System.Drawing.Size(307, 140);
      this.edt_credit_req.TabIndex = 11;
      // 
      // label41
      // 
      this.label41.AutoSize = true;
      this.label41.Location = new System.Drawing.Point(495, 23);
      this.label41.Name = "label41";
      this.label41.Size = new System.Drawing.Size(55, 13);
      this.label41.TabIndex = 74;
      this.label41.Text = "Response";
      // 
      // label42
      // 
      this.label42.AutoSize = true;
      this.label42.Location = new System.Drawing.Point(113, 23);
      this.label42.Name = "label42";
      this.label42.Size = new System.Drawing.Size(47, 13);
      this.label42.TabIndex = 73;
      this.label42.Text = "Request";
      // 
      // tabPage4
      // 
      this.tabPage4.Controls.Add(this.label44);
      this.tabPage4.Controls.Add(this.label45);
      this.tabPage4.Controls.Add(this.edt_balance_timestamp_rep);
      this.tabPage4.Controls.Add(this.edt_balance_rounds_left_rep);
      this.tabPage4.Controls.Add(this.label47);
      this.tabPage4.Controls.Add(this.edt_balance_bonus_balance_rep);
      this.tabPage4.Controls.Add(this.label48);
      this.tabPage4.Controls.Add(this.edt_balance_balance_rep);
      this.tabPage4.Controls.Add(this.label49);
      this.tabPage4.Controls.Add(this.edt_balance_password_req);
      this.tabPage4.Controls.Add(this.label50);
      this.tabPage4.Controls.Add(this.edt_balance_username_req);
      this.tabPage4.Controls.Add(this.label51);
      this.tabPage4.Controls.Add(this.edt_balance_rep);
      this.tabPage4.Controls.Add(this.btnBalance);
      this.tabPage4.Controls.Add(this.edt_balance_token_req);
      this.tabPage4.Controls.Add(this.edt_balance_req);
      this.tabPage4.Controls.Add(this.label52);
      this.tabPage4.Controls.Add(this.label53);
      this.tabPage4.Location = new System.Drawing.Point(4, 22);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage4.Size = new System.Drawing.Size(684, 586);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "Balance";
      this.tabPage4.UseVisualStyleBackColor = true;
      // 
      // label44
      // 
      this.label44.AutoSize = true;
      this.label44.Location = new System.Drawing.Point(336, 129);
      this.label44.Name = "label44";
      this.label44.Size = new System.Drawing.Size(58, 13);
      this.label44.TabIndex = 41;
      this.label44.Text = "Timestamp";
      // 
      // label45
      // 
      this.label45.AutoSize = true;
      this.label45.Location = new System.Drawing.Point(336, 103);
      this.label45.Name = "label45";
      this.label45.Size = new System.Drawing.Size(65, 13);
      this.label45.TabIndex = 40;
      this.label45.Text = "Rounds Left";
      // 
      // edt_balance_timestamp_rep
      // 
      this.edt_balance_timestamp_rep.Location = new System.Drawing.Point(482, 126);
      this.edt_balance_timestamp_rep.Name = "edt_balance_timestamp_rep";
      this.edt_balance_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_timestamp_rep.TabIndex = 32;
      // 
      // edt_balance_rounds_left_rep
      // 
      this.edt_balance_rounds_left_rep.Location = new System.Drawing.Point(482, 100);
      this.edt_balance_rounds_left_rep.Name = "edt_balance_rounds_left_rep";
      this.edt_balance_rounds_left_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_rounds_left_rep.TabIndex = 31;
      // 
      // label47
      // 
      this.label47.AutoSize = true;
      this.label47.Location = new System.Drawing.Point(336, 77);
      this.label47.Name = "label47";
      this.label47.Size = new System.Drawing.Size(79, 13);
      this.label47.TabIndex = 38;
      this.label47.Text = "Bonus Balance";
      // 
      // edt_balance_bonus_balance_rep
      // 
      this.edt_balance_bonus_balance_rep.Location = new System.Drawing.Point(482, 74);
      this.edt_balance_bonus_balance_rep.Name = "edt_balance_bonus_balance_rep";
      this.edt_balance_bonus_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_bonus_balance_rep.TabIndex = 29;
      // 
      // label48
      // 
      this.label48.AutoSize = true;
      this.label48.Location = new System.Drawing.Point(336, 51);
      this.label48.Name = "label48";
      this.label48.Size = new System.Drawing.Size(46, 13);
      this.label48.TabIndex = 37;
      this.label48.Text = "Balance";
      // 
      // edt_balance_balance_rep
      // 
      this.edt_balance_balance_rep.Location = new System.Drawing.Point(482, 48);
      this.edt_balance_balance_rep.Name = "edt_balance_balance_rep";
      this.edt_balance_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_balance_rep.TabIndex = 28;
      // 
      // label49
      // 
      this.label49.AutoSize = true;
      this.label49.Location = new System.Drawing.Point(7, 103);
      this.label49.Name = "label49";
      this.label49.Size = new System.Drawing.Size(53, 13);
      this.label49.TabIndex = 36;
      this.label49.Text = "Password";
      // 
      // edt_balance_password_req
      // 
      this.edt_balance_password_req.Location = new System.Drawing.Point(133, 100);
      this.edt_balance_password_req.Name = "edt_balance_password_req";
      this.edt_balance_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_password_req.TabIndex = 25;
      // 
      // label50
      // 
      this.label50.AutoSize = true;
      this.label50.Location = new System.Drawing.Point(7, 77);
      this.label50.Name = "label50";
      this.label50.Size = new System.Drawing.Size(57, 13);
      this.label50.TabIndex = 33;
      this.label50.Text = "UserName";
      // 
      // edt_balance_username_req
      // 
      this.edt_balance_username_req.Location = new System.Drawing.Point(133, 74);
      this.edt_balance_username_req.Name = "edt_balance_username_req";
      this.edt_balance_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_username_req.TabIndex = 24;
      // 
      // label51
      // 
      this.label51.AutoSize = true;
      this.label51.Location = new System.Drawing.Point(7, 51);
      this.label51.Name = "label51";
      this.label51.Size = new System.Drawing.Size(38, 13);
      this.label51.TabIndex = 30;
      this.label51.Text = "Token";
      // 
      // edt_balance_rep
      // 
      this.edt_balance_rep.Location = new System.Drawing.Point(339, 230);
      this.edt_balance_rep.Multiline = true;
      this.edt_balance_rep.Name = "edt_balance_rep";
      this.edt_balance_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_balance_rep.TabIndex = 35;
      // 
      // btnBalance
      // 
      this.btnBalance.Location = new System.Drawing.Point(101, 400);
      this.btnBalance.Name = "btnBalance";
      this.btnBalance.Size = new System.Drawing.Size(112, 23);
      this.btnBalance.TabIndex = 27;
      this.btnBalance.Text = "Execute";
      this.btnBalance.UseVisualStyleBackColor = true;
      this.btnBalance.Click += new System.EventHandler(this.btnBalance_Click);
      // 
      // edt_balance_token_req
      // 
      this.edt_balance_token_req.Location = new System.Drawing.Point(133, 48);
      this.edt_balance_token_req.Name = "edt_balance_token_req";
      this.edt_balance_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_balance_token_req.TabIndex = 22;
      // 
      // edt_balance_req
      // 
      this.edt_balance_req.Location = new System.Drawing.Point(10, 230);
      this.edt_balance_req.Multiline = true;
      this.edt_balance_req.Name = "edt_balance_req";
      this.edt_balance_req.Size = new System.Drawing.Size(307, 164);
      this.edt_balance_req.TabIndex = 26;
      // 
      // label52
      // 
      this.label52.AutoSize = true;
      this.label52.Location = new System.Drawing.Point(495, 11);
      this.label52.Name = "label52";
      this.label52.Size = new System.Drawing.Size(55, 13);
      this.label52.TabIndex = 23;
      this.label52.Text = "Response";
      // 
      // label53
      // 
      this.label53.AutoSize = true;
      this.label53.Location = new System.Drawing.Point(113, 11);
      this.label53.Name = "label53";
      this.label53.Size = new System.Drawing.Size(47, 13);
      this.label53.TabIndex = 21;
      this.label53.Text = "Request";
      // 
      // tabPage5
      // 
      this.tabPage5.Controls.Add(this.label104);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_password_req);
      this.tabPage5.Controls.Add(this.label105);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_username_req);
      this.tabPage5.Controls.Add(this.label56);
      this.tabPage5.Controls.Add(this.label55);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_timestamp_rep);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_roundsleft_rep);
      this.tabPage5.Controls.Add(this.label54);
      this.tabPage5.Controls.Add(this.label46);
      this.tabPage5.Controls.Add(this.label69);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_amount_req);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_reason_req);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_transactionid_req);
      this.tabPage5.Controls.Add(this.label57);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_cancelentireround_req);
      this.tabPage5.Controls.Add(this.label58);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_roundid_req);
      this.tabPage5.Controls.Add(this.label59);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_playerid_req);
      this.tabPage5.Controls.Add(this.label60);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_extradata_rep);
      this.tabPage5.Controls.Add(this.label61);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_transactionid_rep);
      this.tabPage5.Controls.Add(this.label62);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_bonusbalance_rep);
      this.tabPage5.Controls.Add(this.label63);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_balance_rep);
      this.tabPage5.Controls.Add(this.label64);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_gamecode_req);
      this.tabPage5.Controls.Add(this.label65);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_reftransactionid_req);
      this.tabPage5.Controls.Add(this.label66);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_rep);
      this.tabPage5.Controls.Add(this.btnCancelBet);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_token_req);
      this.tabPage5.Controls.Add(this.edt_cancel_bet_req);
      this.tabPage5.Controls.Add(this.label67);
      this.tabPage5.Controls.Add(this.label68);
      this.tabPage5.Location = new System.Drawing.Point(4, 22);
      this.tabPage5.Name = "tabPage5";
      this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage5.Size = new System.Drawing.Size(684, 586);
      this.tabPage5.TabIndex = 4;
      this.tabPage5.Text = "Cancel Bet";
      this.tabPage5.UseVisualStyleBackColor = true;
      // 
      // label56
      // 
      this.label56.AutoSize = true;
      this.label56.Location = new System.Drawing.Point(341, 136);
      this.label56.Name = "label56";
      this.label56.Size = new System.Drawing.Size(55, 13);
      this.label56.TabIndex = 95;
      this.label56.Text = "Extra data";
      // 
      // label55
      // 
      this.label55.AutoSize = true;
      this.label55.Location = new System.Drawing.Point(341, 162);
      this.label55.Name = "label55";
      this.label55.Size = new System.Drawing.Size(65, 13);
      this.label55.TabIndex = 94;
      this.label55.Text = "Rounds Left";
      // 
      // edt_cancel_bet_timestamp_rep
      // 
      this.edt_cancel_bet_timestamp_rep.Location = new System.Drawing.Point(487, 185);
      this.edt_cancel_bet_timestamp_rep.Name = "edt_cancel_bet_timestamp_rep";
      this.edt_cancel_bet_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_timestamp_rep.TabIndex = 93;
      // 
      // edt_cancel_bet_roundsleft_rep
      // 
      this.edt_cancel_bet_roundsleft_rep.Location = new System.Drawing.Point(487, 159);
      this.edt_cancel_bet_roundsleft_rep.Name = "edt_cancel_bet_roundsleft_rep";
      this.edt_cancel_bet_roundsleft_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_roundsleft_rep.TabIndex = 92;
      // 
      // label54
      // 
      this.label54.AutoSize = true;
      this.label54.Location = new System.Drawing.Point(13, 295);
      this.label54.Name = "label54";
      this.label54.Size = new System.Drawing.Size(44, 13);
      this.label54.TabIndex = 91;
      this.label54.Text = "Reason";
      // 
      // label46
      // 
      this.label46.AutoSize = true;
      this.label46.Location = new System.Drawing.Point(13, 243);
      this.label46.Name = "label46";
      this.label46.Size = new System.Drawing.Size(105, 13);
      this.label46.TabIndex = 90;
      this.label46.Text = "Cancel Entire Round";
      // 
      // label69
      // 
      this.label69.AutoSize = true;
      this.label69.Location = new System.Drawing.Point(13, 139);
      this.label69.Name = "label69";
      this.label69.Size = new System.Drawing.Size(97, 13);
      this.label69.TabIndex = 89;
      this.label69.Text = "Ref Transaction ID";
      // 
      // edt_cancel_bet_amount_req
      // 
      this.edt_cancel_bet_amount_req.Location = new System.Drawing.Point(139, 318);
      this.edt_cancel_bet_amount_req.Name = "edt_cancel_bet_amount_req";
      this.edt_cancel_bet_amount_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_amount_req.TabIndex = 64;
      // 
      // edt_cancel_bet_reason_req
      // 
      this.edt_cancel_bet_reason_req.Location = new System.Drawing.Point(139, 292);
      this.edt_cancel_bet_reason_req.Name = "edt_cancel_bet_reason_req";
      this.edt_cancel_bet_reason_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_reason_req.TabIndex = 63;
      // 
      // edt_cancel_bet_transactionid_req
      // 
      this.edt_cancel_bet_transactionid_req.Location = new System.Drawing.Point(139, 266);
      this.edt_cancel_bet_transactionid_req.Name = "edt_cancel_bet_transactionid_req";
      this.edt_cancel_bet_transactionid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_transactionid_req.TabIndex = 62;
      // 
      // label57
      // 
      this.label57.AutoSize = true;
      this.label57.Location = new System.Drawing.Point(13, 321);
      this.label57.Name = "label57";
      this.label57.Size = new System.Drawing.Size(43, 13);
      this.label57.TabIndex = 84;
      this.label57.Text = "Amount";
      // 
      // edt_cancel_bet_cancelentireround_req
      // 
      this.edt_cancel_bet_cancelentireround_req.Location = new System.Drawing.Point(139, 240);
      this.edt_cancel_bet_cancelentireround_req.Name = "edt_cancel_bet_cancelentireround_req";
      this.edt_cancel_bet_cancelentireround_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_cancelentireround_req.TabIndex = 61;
      // 
      // label58
      // 
      this.label58.AutoSize = true;
      this.label58.Location = new System.Drawing.Point(13, 269);
      this.label58.Name = "label58";
      this.label58.Size = new System.Drawing.Size(77, 13);
      this.label58.TabIndex = 83;
      this.label58.Text = "Transaction ID";
      // 
      // edt_cancel_bet_roundid_req
      // 
      this.edt_cancel_bet_roundid_req.Location = new System.Drawing.Point(139, 214);
      this.edt_cancel_bet_roundid_req.Name = "edt_cancel_bet_roundid_req";
      this.edt_cancel_bet_roundid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_roundid_req.TabIndex = 60;
      // 
      // label59
      // 
      this.label59.AutoSize = true;
      this.label59.Location = new System.Drawing.Point(13, 217);
      this.label59.Name = "label59";
      this.label59.Size = new System.Drawing.Size(53, 13);
      this.label59.TabIndex = 82;
      this.label59.Text = "Round ID";
      // 
      // edt_cancel_bet_playerid_req
      // 
      this.edt_cancel_bet_playerid_req.Location = new System.Drawing.Point(139, 188);
      this.edt_cancel_bet_playerid_req.Name = "edt_cancel_bet_playerid_req";
      this.edt_cancel_bet_playerid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_playerid_req.TabIndex = 59;
      // 
      // label60
      // 
      this.label60.AutoSize = true;
      this.label60.Location = new System.Drawing.Point(341, 188);
      this.label60.Name = "label60";
      this.label60.Size = new System.Drawing.Size(58, 13);
      this.label60.TabIndex = 81;
      this.label60.Text = "Timestamp";
      // 
      // edt_cancel_bet_extradata_rep
      // 
      this.edt_cancel_bet_extradata_rep.Location = new System.Drawing.Point(487, 133);
      this.edt_cancel_bet_extradata_rep.Name = "edt_cancel_bet_extradata_rep";
      this.edt_cancel_bet_extradata_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_extradata_rep.TabIndex = 71;
      // 
      // label61
      // 
      this.label61.AutoSize = true;
      this.label61.Location = new System.Drawing.Point(341, 110);
      this.label61.Name = "label61";
      this.label61.Size = new System.Drawing.Size(77, 13);
      this.label61.TabIndex = 80;
      this.label61.Text = "Transaction ID";
      // 
      // edt_cancel_bet_transactionid_rep
      // 
      this.edt_cancel_bet_transactionid_rep.Location = new System.Drawing.Point(487, 107);
      this.edt_cancel_bet_transactionid_rep.Name = "edt_cancel_bet_transactionid_rep";
      this.edt_cancel_bet_transactionid_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_transactionid_rep.TabIndex = 70;
      // 
      // label62
      // 
      this.label62.AutoSize = true;
      this.label62.Location = new System.Drawing.Point(341, 84);
      this.label62.Name = "label62";
      this.label62.Size = new System.Drawing.Size(79, 13);
      this.label62.TabIndex = 79;
      this.label62.Text = "Bonus Balance";
      // 
      // edt_cancel_bet_bonusbalance_rep
      // 
      this.edt_cancel_bet_bonusbalance_rep.Location = new System.Drawing.Point(487, 81);
      this.edt_cancel_bet_bonusbalance_rep.Name = "edt_cancel_bet_bonusbalance_rep";
      this.edt_cancel_bet_bonusbalance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_bonusbalance_rep.TabIndex = 69;
      // 
      // label63
      // 
      this.label63.AutoSize = true;
      this.label63.Location = new System.Drawing.Point(341, 58);
      this.label63.Name = "label63";
      this.label63.Size = new System.Drawing.Size(46, 13);
      this.label63.TabIndex = 78;
      this.label63.Text = "Balance";
      // 
      // edt_cancel_bet_balance_rep
      // 
      this.edt_cancel_bet_balance_rep.Location = new System.Drawing.Point(487, 55);
      this.edt_cancel_bet_balance_rep.Name = "edt_cancel_bet_balance_rep";
      this.edt_cancel_bet_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_balance_rep.TabIndex = 68;
      // 
      // label64
      // 
      this.label64.AutoSize = true;
      this.label64.Location = new System.Drawing.Point(13, 191);
      this.label64.Name = "label64";
      this.label64.Size = new System.Drawing.Size(50, 13);
      this.label64.TabIndex = 77;
      this.label64.Text = "Player ID";
      // 
      // edt_cancel_bet_gamecode_req
      // 
      this.edt_cancel_bet_gamecode_req.Location = new System.Drawing.Point(139, 162);
      this.edt_cancel_bet_gamecode_req.Name = "edt_cancel_bet_gamecode_req";
      this.edt_cancel_bet_gamecode_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_gamecode_req.TabIndex = 58;
      // 
      // label65
      // 
      this.label65.AutoSize = true;
      this.label65.Location = new System.Drawing.Point(13, 165);
      this.label65.Name = "label65";
      this.label65.Size = new System.Drawing.Size(60, 13);
      this.label65.TabIndex = 76;
      this.label65.Text = "GameCode";
      // 
      // edt_cancel_bet_reftransactionid_req
      // 
      this.edt_cancel_bet_reftransactionid_req.Location = new System.Drawing.Point(139, 136);
      this.edt_cancel_bet_reftransactionid_req.Name = "edt_cancel_bet_reftransactionid_req";
      this.edt_cancel_bet_reftransactionid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_reftransactionid_req.TabIndex = 57;
      // 
      // label66
      // 
      this.label66.AutoSize = true;
      this.label66.Location = new System.Drawing.Point(13, 58);
      this.label66.Name = "label66";
      this.label66.Size = new System.Drawing.Size(38, 13);
      this.label66.TabIndex = 75;
      this.label66.Text = "Token";
      // 
      // edt_cancel_bet_rep
      // 
      this.edt_cancel_bet_rep.Location = new System.Drawing.Point(344, 376);
      this.edt_cancel_bet_rep.Multiline = true;
      this.edt_cancel_bet_rep.Name = "edt_cancel_bet_rep";
      this.edt_cancel_bet_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_cancel_bet_rep.TabIndex = 72;
      // 
      // btnCancelBet
      // 
      this.btnCancelBet.Location = new System.Drawing.Point(107, 546);
      this.btnCancelBet.Name = "btnCancelBet";
      this.btnCancelBet.Size = new System.Drawing.Size(112, 23);
      this.btnCancelBet.TabIndex = 67;
      this.btnCancelBet.Text = "Execute";
      this.btnCancelBet.UseVisualStyleBackColor = true;
      this.btnCancelBet.Click += new System.EventHandler(this.btnCancelBet_Click);
      // 
      // edt_cancel_bet_token_req
      // 
      this.edt_cancel_bet_token_req.Location = new System.Drawing.Point(139, 58);
      this.edt_cancel_bet_token_req.Name = "edt_cancel_bet_token_req";
      this.edt_cancel_bet_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_token_req.TabIndex = 56;
      // 
      // edt_cancel_bet_req
      // 
      this.edt_cancel_bet_req.Location = new System.Drawing.Point(16, 376);
      this.edt_cancel_bet_req.Multiline = true;
      this.edt_cancel_bet_req.Name = "edt_cancel_bet_req";
      this.edt_cancel_bet_req.Size = new System.Drawing.Size(307, 164);
      this.edt_cancel_bet_req.TabIndex = 66;
      // 
      // label67
      // 
      this.label67.AutoSize = true;
      this.label67.Location = new System.Drawing.Point(501, 18);
      this.label67.Name = "label67";
      this.label67.Size = new System.Drawing.Size(55, 13);
      this.label67.TabIndex = 74;
      this.label67.Text = "Response";
      // 
      // label68
      // 
      this.label68.AutoSize = true;
      this.label68.Location = new System.Drawing.Point(119, 18);
      this.label68.Name = "label68";
      this.label68.Size = new System.Drawing.Size(47, 13);
      this.label68.TabIndex = 73;
      this.label68.Text = "Request";
      // 
      // tabPage6
      // 
      this.tabPage6.Controls.Add(this.label106);
      this.tabPage6.Controls.Add(this.edt_debitcredit_password_req);
      this.tabPage6.Controls.Add(this.label107);
      this.tabPage6.Controls.Add(this.edt_debitcredit_username_req);
      this.tabPage6.Controls.Add(this.label87);
      this.tabPage6.Controls.Add(this.label73);
      this.tabPage6.Controls.Add(this.edt_debitcredit_timestamp_rep);
      this.tabPage6.Controls.Add(this.edt_debitcredit_roundsleft_rep);
      this.tabPage6.Controls.Add(this.label71);
      this.tabPage6.Controls.Add(this.label70);
      this.tabPage6.Controls.Add(this.edt_debitcredit_feature_req);
      this.tabPage6.Controls.Add(this.label72);
      this.tabPage6.Controls.Add(this.edt_debitcredit_featureid_req);
      this.tabPage6.Controls.Add(this.edt_debitcredit_credit_type_req);
      this.tabPage6.Controls.Add(this.label74);
      this.tabPage6.Controls.Add(this.edt_debitcredit_credit_amount_req);
      this.tabPage6.Controls.Add(this.label75);
      this.tabPage6.Controls.Add(this.edt_debitcredit_debit_amount_req);
      this.tabPage6.Controls.Add(this.label76);
      this.tabPage6.Controls.Add(this.edt_debitcredit_transactionid_req);
      this.tabPage6.Controls.Add(this.label77);
      this.tabPage6.Controls.Add(this.edt_debitcredit_roundid_req);
      this.tabPage6.Controls.Add(this.label78);
      this.tabPage6.Controls.Add(this.edt_debitcredit_extradata_rep);
      this.tabPage6.Controls.Add(this.label79);
      this.tabPage6.Controls.Add(this.edt_debitcredit_transactionid_rep);
      this.tabPage6.Controls.Add(this.label80);
      this.tabPage6.Controls.Add(this.edt_debitcredit_bonusbalance_rep);
      this.tabPage6.Controls.Add(this.label81);
      this.tabPage6.Controls.Add(this.edt_debitcredit_balance_rep);
      this.tabPage6.Controls.Add(this.label82);
      this.tabPage6.Controls.Add(this.edt_debitcredit_playerid_req);
      this.tabPage6.Controls.Add(this.label83);
      this.tabPage6.Controls.Add(this.edt_debitcredit_gamecode_req);
      this.tabPage6.Controls.Add(this.label84);
      this.tabPage6.Controls.Add(this.edt_debitcredit_rep);
      this.tabPage6.Controls.Add(this.btnDebitCredit);
      this.tabPage6.Controls.Add(this.edt_debitcredit_token_req);
      this.tabPage6.Controls.Add(this.edt_debitcredit_req);
      this.tabPage6.Controls.Add(this.label85);
      this.tabPage6.Controls.Add(this.label86);
      this.tabPage6.Location = new System.Drawing.Point(4, 22);
      this.tabPage6.Name = "tabPage6";
      this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage6.Size = new System.Drawing.Size(684, 586);
      this.tabPage6.TabIndex = 5;
      this.tabPage6.Text = "DebitCredit";
      this.tabPage6.UseVisualStyleBackColor = true;
      // 
      // label87
      // 
      this.label87.AutoSize = true;
      this.label87.Location = new System.Drawing.Point(342, 138);
      this.label87.Name = "label87";
      this.label87.Size = new System.Drawing.Size(57, 13);
      this.label87.TabIndex = 130;
      this.label87.Text = "Extra Data";
      // 
      // label73
      // 
      this.label73.AutoSize = true;
      this.label73.Location = new System.Drawing.Point(342, 164);
      this.label73.Name = "label73";
      this.label73.Size = new System.Drawing.Size(65, 13);
      this.label73.TabIndex = 129;
      this.label73.Text = "Rounds Left";
      // 
      // edt_debitcredit_timestamp_rep
      // 
      this.edt_debitcredit_timestamp_rep.Location = new System.Drawing.Point(488, 187);
      this.edt_debitcredit_timestamp_rep.Name = "edt_debitcredit_timestamp_rep";
      this.edt_debitcredit_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_timestamp_rep.TabIndex = 128;
      // 
      // edt_debitcredit_roundsleft_rep
      // 
      this.edt_debitcredit_roundsleft_rep.Location = new System.Drawing.Point(488, 161);
      this.edt_debitcredit_roundsleft_rep.Name = "edt_debitcredit_roundsleft_rep";
      this.edt_debitcredit_roundsleft_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_roundsleft_rep.TabIndex = 127;
      // 
      // label71
      // 
      this.label71.AutoSize = true;
      this.label71.Location = new System.Drawing.Point(13, 268);
      this.label71.Name = "label71";
      this.label71.Size = new System.Drawing.Size(73, 13);
      this.label71.TabIndex = 126;
      this.label71.Text = "Credit Amount";
      // 
      // label70
      // 
      this.label70.AutoSize = true;
      this.label70.Location = new System.Drawing.Point(13, 322);
      this.label70.Name = "label70";
      this.label70.Size = new System.Drawing.Size(43, 13);
      this.label70.TabIndex = 125;
      this.label70.Text = "Feature";
      // 
      // edt_debitcredit_feature_req
      // 
      this.edt_debitcredit_feature_req.Location = new System.Drawing.Point(139, 319);
      this.edt_debitcredit_feature_req.Name = "edt_debitcredit_feature_req";
      this.edt_debitcredit_feature_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_feature_req.TabIndex = 99;
      // 
      // label72
      // 
      this.label72.AutoSize = true;
      this.label72.Location = new System.Drawing.Point(13, 348);
      this.label72.Name = "label72";
      this.label72.Size = new System.Drawing.Size(57, 13);
      this.label72.TabIndex = 123;
      this.label72.Text = "Feature ID";
      // 
      // edt_debitcredit_featureid_req
      // 
      this.edt_debitcredit_featureid_req.Location = new System.Drawing.Point(139, 345);
      this.edt_debitcredit_featureid_req.Name = "edt_debitcredit_featureid_req";
      this.edt_debitcredit_featureid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_featureid_req.TabIndex = 100;
      // 
      // edt_debitcredit_credit_type_req
      // 
      this.edt_debitcredit_credit_type_req.Location = new System.Drawing.Point(139, 291);
      this.edt_debitcredit_credit_type_req.Name = "edt_debitcredit_credit_type_req";
      this.edt_debitcredit_credit_type_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_credit_type_req.TabIndex = 98;
      // 
      // label74
      // 
      this.label74.AutoSize = true;
      this.label74.Location = new System.Drawing.Point(13, 294);
      this.label74.Name = "label74";
      this.label74.Size = new System.Drawing.Size(61, 13);
      this.label74.TabIndex = 121;
      this.label74.Text = "Credit Type";
      // 
      // edt_debitcredit_credit_amount_req
      // 
      this.edt_debitcredit_credit_amount_req.Location = new System.Drawing.Point(139, 265);
      this.edt_debitcredit_credit_amount_req.Name = "edt_debitcredit_credit_amount_req";
      this.edt_debitcredit_credit_amount_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_credit_amount_req.TabIndex = 97;
      // 
      // label75
      // 
      this.label75.AutoSize = true;
      this.label75.Location = new System.Drawing.Point(13, 242);
      this.label75.Name = "label75";
      this.label75.Size = new System.Drawing.Size(71, 13);
      this.label75.TabIndex = 120;
      this.label75.Text = "Debit Amount";
      // 
      // edt_debitcredit_debit_amount_req
      // 
      this.edt_debitcredit_debit_amount_req.Location = new System.Drawing.Point(139, 239);
      this.edt_debitcredit_debit_amount_req.Name = "edt_debitcredit_debit_amount_req";
      this.edt_debitcredit_debit_amount_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_debit_amount_req.TabIndex = 96;
      // 
      // label76
      // 
      this.label76.AutoSize = true;
      this.label76.Location = new System.Drawing.Point(13, 216);
      this.label76.Name = "label76";
      this.label76.Size = new System.Drawing.Size(77, 13);
      this.label76.TabIndex = 119;
      this.label76.Text = "Transaction ID";
      // 
      // edt_debitcredit_transactionid_req
      // 
      this.edt_debitcredit_transactionid_req.Location = new System.Drawing.Point(139, 213);
      this.edt_debitcredit_transactionid_req.Name = "edt_debitcredit_transactionid_req";
      this.edt_debitcredit_transactionid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_transactionid_req.TabIndex = 95;
      // 
      // label77
      // 
      this.label77.AutoSize = true;
      this.label77.Location = new System.Drawing.Point(13, 190);
      this.label77.Name = "label77";
      this.label77.Size = new System.Drawing.Size(53, 13);
      this.label77.TabIndex = 118;
      this.label77.Text = "Round ID";
      // 
      // edt_debitcredit_roundid_req
      // 
      this.edt_debitcredit_roundid_req.Location = new System.Drawing.Point(139, 187);
      this.edt_debitcredit_roundid_req.Name = "edt_debitcredit_roundid_req";
      this.edt_debitcredit_roundid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_roundid_req.TabIndex = 94;
      // 
      // label78
      // 
      this.label78.AutoSize = true;
      this.label78.Location = new System.Drawing.Point(342, 190);
      this.label78.Name = "label78";
      this.label78.Size = new System.Drawing.Size(58, 13);
      this.label78.TabIndex = 117;
      this.label78.Text = "Timestamp";
      // 
      // edt_debitcredit_extradata_rep
      // 
      this.edt_debitcredit_extradata_rep.Location = new System.Drawing.Point(488, 135);
      this.edt_debitcredit_extradata_rep.Name = "edt_debitcredit_extradata_rep";
      this.edt_debitcredit_extradata_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_extradata_rep.TabIndex = 107;
      // 
      // label79
      // 
      this.label79.AutoSize = true;
      this.label79.Location = new System.Drawing.Point(342, 112);
      this.label79.Name = "label79";
      this.label79.Size = new System.Drawing.Size(77, 13);
      this.label79.TabIndex = 116;
      this.label79.Text = "Transaction ID";
      // 
      // edt_debitcredit_transactionid_rep
      // 
      this.edt_debitcredit_transactionid_rep.Location = new System.Drawing.Point(488, 109);
      this.edt_debitcredit_transactionid_rep.Name = "edt_debitcredit_transactionid_rep";
      this.edt_debitcredit_transactionid_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_transactionid_rep.TabIndex = 106;
      // 
      // label80
      // 
      this.label80.AutoSize = true;
      this.label80.Location = new System.Drawing.Point(342, 86);
      this.label80.Name = "label80";
      this.label80.Size = new System.Drawing.Size(79, 13);
      this.label80.TabIndex = 115;
      this.label80.Text = "Bonus Balance";
      // 
      // edt_debitcredit_bonusbalance_rep
      // 
      this.edt_debitcredit_bonusbalance_rep.Location = new System.Drawing.Point(488, 83);
      this.edt_debitcredit_bonusbalance_rep.Name = "edt_debitcredit_bonusbalance_rep";
      this.edt_debitcredit_bonusbalance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_bonusbalance_rep.TabIndex = 105;
      // 
      // label81
      // 
      this.label81.AutoSize = true;
      this.label81.Location = new System.Drawing.Point(342, 60);
      this.label81.Name = "label81";
      this.label81.Size = new System.Drawing.Size(46, 13);
      this.label81.TabIndex = 114;
      this.label81.Text = "Balance";
      // 
      // edt_debitcredit_balance_rep
      // 
      this.edt_debitcredit_balance_rep.Location = new System.Drawing.Point(488, 57);
      this.edt_debitcredit_balance_rep.Name = "edt_debitcredit_balance_rep";
      this.edt_debitcredit_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_balance_rep.TabIndex = 104;
      // 
      // label82
      // 
      this.label82.AutoSize = true;
      this.label82.Location = new System.Drawing.Point(13, 164);
      this.label82.Name = "label82";
      this.label82.Size = new System.Drawing.Size(50, 13);
      this.label82.TabIndex = 113;
      this.label82.Text = "Player ID";
      // 
      // edt_debitcredit_playerid_req
      // 
      this.edt_debitcredit_playerid_req.Location = new System.Drawing.Point(139, 161);
      this.edt_debitcredit_playerid_req.Name = "edt_debitcredit_playerid_req";
      this.edt_debitcredit_playerid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_playerid_req.TabIndex = 93;
      // 
      // label83
      // 
      this.label83.AutoSize = true;
      this.label83.Location = new System.Drawing.Point(13, 138);
      this.label83.Name = "label83";
      this.label83.Size = new System.Drawing.Size(60, 13);
      this.label83.TabIndex = 112;
      this.label83.Text = "GameCode";
      // 
      // edt_debitcredit_gamecode_req
      // 
      this.edt_debitcredit_gamecode_req.Location = new System.Drawing.Point(139, 135);
      this.edt_debitcredit_gamecode_req.Name = "edt_debitcredit_gamecode_req";
      this.edt_debitcredit_gamecode_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_gamecode_req.TabIndex = 92;
      // 
      // label84
      // 
      this.label84.AutoSize = true;
      this.label84.Location = new System.Drawing.Point(13, 60);
      this.label84.Name = "label84";
      this.label84.Size = new System.Drawing.Size(38, 13);
      this.label84.TabIndex = 111;
      this.label84.Text = "Token";
      // 
      // edt_debitcredit_rep
      // 
      this.edt_debitcredit_rep.Location = new System.Drawing.Point(345, 373);
      this.edt_debitcredit_rep.Multiline = true;
      this.edt_debitcredit_rep.Name = "edt_debitcredit_rep";
      this.edt_debitcredit_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_debitcredit_rep.TabIndex = 108;
      // 
      // btnDebitCredit
      // 
      this.btnDebitCredit.Location = new System.Drawing.Point(107, 543);
      this.btnDebitCredit.Name = "btnDebitCredit";
      this.btnDebitCredit.Size = new System.Drawing.Size(112, 23);
      this.btnDebitCredit.TabIndex = 103;
      this.btnDebitCredit.Text = "Execute";
      this.btnDebitCredit.UseVisualStyleBackColor = true;
      this.btnDebitCredit.Click += new System.EventHandler(this.btnDebitCredit_Click);
      // 
      // edt_debitcredit_token_req
      // 
      this.edt_debitcredit_token_req.Location = new System.Drawing.Point(139, 57);
      this.edt_debitcredit_token_req.Name = "edt_debitcredit_token_req";
      this.edt_debitcredit_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_token_req.TabIndex = 91;
      // 
      // edt_debitcredit_req
      // 
      this.edt_debitcredit_req.Location = new System.Drawing.Point(16, 373);
      this.edt_debitcredit_req.Multiline = true;
      this.edt_debitcredit_req.Name = "edt_debitcredit_req";
      this.edt_debitcredit_req.Size = new System.Drawing.Size(307, 164);
      this.edt_debitcredit_req.TabIndex = 102;
      // 
      // label85
      // 
      this.label85.AutoSize = true;
      this.label85.Location = new System.Drawing.Point(501, 20);
      this.label85.Name = "label85";
      this.label85.Size = new System.Drawing.Size(55, 13);
      this.label85.TabIndex = 110;
      this.label85.Text = "Response";
      // 
      // label86
      // 
      this.label86.AutoSize = true;
      this.label86.Location = new System.Drawing.Point(119, 20);
      this.label86.Name = "label86";
      this.label86.Size = new System.Drawing.Size(47, 13);
      this.label86.TabIndex = 109;
      this.label86.Text = "Request";
      // 
      // tabPage7
      // 
      this.tabPage7.Controls.Add(this.label108);
      this.tabPage7.Controls.Add(this.edt_endgame_password_req);
      this.tabPage7.Controls.Add(this.label109);
      this.tabPage7.Controls.Add(this.edt_endgame_username_req);
      this.tabPage7.Controls.Add(this.label88);
      this.tabPage7.Controls.Add(this.edt_endgame_transactionconfiguration_req);
      this.tabPage7.Controls.Add(this.label93);
      this.tabPage7.Controls.Add(this.edt_endgame_transactionId_req);
      this.tabPage7.Controls.Add(this.label94);
      this.tabPage7.Controls.Add(this.edt_endgame_roundid_req);
      this.tabPage7.Controls.Add(this.label95);
      this.tabPage7.Controls.Add(this.edt_endgame_timestamp_rep);
      this.tabPage7.Controls.Add(this.label96);
      this.tabPage7.Controls.Add(this.edt_endgame_transactionid_rep);
      this.tabPage7.Controls.Add(this.label97);
      this.tabPage7.Controls.Add(this.edt_endgame_bonusbalance_rep);
      this.tabPage7.Controls.Add(this.label98);
      this.tabPage7.Controls.Add(this.edt_endgame_balance_rep);
      this.tabPage7.Controls.Add(this.label99);
      this.tabPage7.Controls.Add(this.edt_endgame_playerid_req);
      this.tabPage7.Controls.Add(this.label100);
      this.tabPage7.Controls.Add(this.edt_endgame_gamecode_req);
      this.tabPage7.Controls.Add(this.label101);
      this.tabPage7.Controls.Add(this.edt_endgame_rep);
      this.tabPage7.Controls.Add(this.btnEndGame);
      this.tabPage7.Controls.Add(this.edt_endgame_token_req);
      this.tabPage7.Controls.Add(this.edt_endgame_req);
      this.tabPage7.Controls.Add(this.label102);
      this.tabPage7.Controls.Add(this.label103);
      this.tabPage7.Location = new System.Drawing.Point(4, 22);
      this.tabPage7.Name = "tabPage7";
      this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage7.Size = new System.Drawing.Size(684, 586);
      this.tabPage7.TabIndex = 6;
      this.tabPage7.Text = "EndGame";
      this.tabPage7.UseVisualStyleBackColor = true;
      // 
      // label88
      // 
      this.label88.AutoSize = true;
      this.label88.Location = new System.Drawing.Point(13, 240);
      this.label88.Name = "label88";
      this.label88.Size = new System.Drawing.Size(117, 13);
      this.label88.TabIndex = 88;
      this.label88.Text = "Transaction Configurat.";
      // 
      // edt_endgame_transactionconfiguration_req
      // 
      this.edt_endgame_transactionconfiguration_req.Location = new System.Drawing.Point(139, 237);
      this.edt_endgame_transactionconfiguration_req.Name = "edt_endgame_transactionconfiguration_req";
      this.edt_endgame_transactionconfiguration_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_transactionconfiguration_req.TabIndex = 61;
      // 
      // label93
      // 
      this.label93.AutoSize = true;
      this.label93.Location = new System.Drawing.Point(13, 214);
      this.label93.Name = "label93";
      this.label93.Size = new System.Drawing.Size(77, 13);
      this.label93.TabIndex = 83;
      this.label93.Text = "Transaction ID";
      // 
      // edt_endgame_transactionId_req
      // 
      this.edt_endgame_transactionId_req.Location = new System.Drawing.Point(139, 211);
      this.edt_endgame_transactionId_req.Name = "edt_endgame_transactionId_req";
      this.edt_endgame_transactionId_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_transactionId_req.TabIndex = 60;
      // 
      // label94
      // 
      this.label94.AutoSize = true;
      this.label94.Location = new System.Drawing.Point(13, 188);
      this.label94.Name = "label94";
      this.label94.Size = new System.Drawing.Size(53, 13);
      this.label94.TabIndex = 82;
      this.label94.Text = "Round ID";
      // 
      // edt_endgame_roundid_req
      // 
      this.edt_endgame_roundid_req.Location = new System.Drawing.Point(139, 185);
      this.edt_endgame_roundid_req.Name = "edt_endgame_roundid_req";
      this.edt_endgame_roundid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_roundid_req.TabIndex = 59;
      // 
      // label95
      // 
      this.label95.AutoSize = true;
      this.label95.Location = new System.Drawing.Point(341, 136);
      this.label95.Name = "label95";
      this.label95.Size = new System.Drawing.Size(58, 13);
      this.label95.TabIndex = 81;
      this.label95.Text = "Timestamp";
      // 
      // edt_endgame_timestamp_rep
      // 
      this.edt_endgame_timestamp_rep.Location = new System.Drawing.Point(487, 133);
      this.edt_endgame_timestamp_rep.Name = "edt_endgame_timestamp_rep";
      this.edt_endgame_timestamp_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_timestamp_rep.TabIndex = 71;
      // 
      // label96
      // 
      this.label96.AutoSize = true;
      this.label96.Location = new System.Drawing.Point(341, 110);
      this.label96.Name = "label96";
      this.label96.Size = new System.Drawing.Size(77, 13);
      this.label96.TabIndex = 80;
      this.label96.Text = "Transaction ID";
      // 
      // edt_endgame_transactionid_rep
      // 
      this.edt_endgame_transactionid_rep.Location = new System.Drawing.Point(487, 107);
      this.edt_endgame_transactionid_rep.Name = "edt_endgame_transactionid_rep";
      this.edt_endgame_transactionid_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_transactionid_rep.TabIndex = 70;
      // 
      // label97
      // 
      this.label97.AutoSize = true;
      this.label97.Location = new System.Drawing.Point(341, 84);
      this.label97.Name = "label97";
      this.label97.Size = new System.Drawing.Size(79, 13);
      this.label97.TabIndex = 79;
      this.label97.Text = "Bonus Balance";
      // 
      // edt_endgame_bonusbalance_rep
      // 
      this.edt_endgame_bonusbalance_rep.Location = new System.Drawing.Point(487, 81);
      this.edt_endgame_bonusbalance_rep.Name = "edt_endgame_bonusbalance_rep";
      this.edt_endgame_bonusbalance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_bonusbalance_rep.TabIndex = 69;
      // 
      // label98
      // 
      this.label98.AutoSize = true;
      this.label98.Location = new System.Drawing.Point(341, 58);
      this.label98.Name = "label98";
      this.label98.Size = new System.Drawing.Size(46, 13);
      this.label98.TabIndex = 78;
      this.label98.Text = "Balance";
      // 
      // edt_endgame_balance_rep
      // 
      this.edt_endgame_balance_rep.Location = new System.Drawing.Point(487, 55);
      this.edt_endgame_balance_rep.Name = "edt_endgame_balance_rep";
      this.edt_endgame_balance_rep.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_balance_rep.TabIndex = 68;
      // 
      // label99
      // 
      this.label99.AutoSize = true;
      this.label99.Location = new System.Drawing.Point(13, 162);
      this.label99.Name = "label99";
      this.label99.Size = new System.Drawing.Size(50, 13);
      this.label99.TabIndex = 77;
      this.label99.Text = "Player ID";
      // 
      // edt_endgame_playerid_req
      // 
      this.edt_endgame_playerid_req.Location = new System.Drawing.Point(139, 159);
      this.edt_endgame_playerid_req.Name = "edt_endgame_playerid_req";
      this.edt_endgame_playerid_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_playerid_req.TabIndex = 58;
      // 
      // label100
      // 
      this.label100.AutoSize = true;
      this.label100.Location = new System.Drawing.Point(13, 136);
      this.label100.Name = "label100";
      this.label100.Size = new System.Drawing.Size(60, 13);
      this.label100.TabIndex = 76;
      this.label100.Text = "GameCode";
      // 
      // edt_endgame_gamecode_req
      // 
      this.edt_endgame_gamecode_req.Location = new System.Drawing.Point(139, 133);
      this.edt_endgame_gamecode_req.Name = "edt_endgame_gamecode_req";
      this.edt_endgame_gamecode_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_gamecode_req.TabIndex = 57;
      // 
      // label101
      // 
      this.label101.AutoSize = true;
      this.label101.Location = new System.Drawing.Point(13, 58);
      this.label101.Name = "label101";
      this.label101.Size = new System.Drawing.Size(38, 13);
      this.label101.TabIndex = 75;
      this.label101.Text = "Token";
      // 
      // edt_endgame_rep
      // 
      this.edt_endgame_rep.Location = new System.Drawing.Point(344, 376);
      this.edt_endgame_rep.Multiline = true;
      this.edt_endgame_rep.Name = "edt_endgame_rep";
      this.edt_endgame_rep.Size = new System.Drawing.Size(327, 164);
      this.edt_endgame_rep.TabIndex = 72;
      // 
      // btnEndGame
      // 
      this.btnEndGame.Location = new System.Drawing.Point(107, 546);
      this.btnEndGame.Name = "btnEndGame";
      this.btnEndGame.Size = new System.Drawing.Size(112, 23);
      this.btnEndGame.TabIndex = 67;
      this.btnEndGame.Text = "Execute";
      this.btnEndGame.UseVisualStyleBackColor = true;
      this.btnEndGame.Click += new System.EventHandler(this.btnEndGame_Click);
      // 
      // edt_endgame_token_req
      // 
      this.edt_endgame_token_req.Location = new System.Drawing.Point(139, 55);
      this.edt_endgame_token_req.Name = "edt_endgame_token_req";
      this.edt_endgame_token_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_token_req.TabIndex = 56;
      // 
      // edt_endgame_req
      // 
      this.edt_endgame_req.Location = new System.Drawing.Point(16, 376);
      this.edt_endgame_req.Multiline = true;
      this.edt_endgame_req.Name = "edt_endgame_req";
      this.edt_endgame_req.Size = new System.Drawing.Size(307, 164);
      this.edt_endgame_req.TabIndex = 66;
      // 
      // label102
      // 
      this.label102.AutoSize = true;
      this.label102.Location = new System.Drawing.Point(501, 18);
      this.label102.Name = "label102";
      this.label102.Size = new System.Drawing.Size(55, 13);
      this.label102.TabIndex = 74;
      this.label102.Text = "Response";
      // 
      // label103
      // 
      this.label103.AutoSize = true;
      this.label103.Location = new System.Drawing.Point(119, 18);
      this.label103.Name = "label103";
      this.label103.Size = new System.Drawing.Size(47, 13);
      this.label103.TabIndex = 73;
      this.label103.Text = "Request";
      // 
      // label89
      // 
      this.label89.AutoSize = true;
      this.label89.Location = new System.Drawing.Point(7, 114);
      this.label89.Name = "label89";
      this.label89.Size = new System.Drawing.Size(53, 13);
      this.label89.TabIndex = 59;
      this.label89.Text = "Password";
      // 
      // edt_debit_password_req
      // 
      this.edt_debit_password_req.Location = new System.Drawing.Point(133, 111);
      this.edt_debit_password_req.Name = "edt_debit_password_req";
      this.edt_debit_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_password_req.TabIndex = 57;
      // 
      // label90
      // 
      this.label90.AutoSize = true;
      this.label90.Location = new System.Drawing.Point(7, 88);
      this.label90.Name = "label90";
      this.label90.Size = new System.Drawing.Size(57, 13);
      this.label90.TabIndex = 58;
      this.label90.Text = "UserName";
      // 
      // edt_debit_username_req
      // 
      this.edt_debit_username_req.Location = new System.Drawing.Point(133, 85);
      this.edt_debit_username_req.Name = "edt_debit_username_req";
      this.edt_debit_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debit_username_req.TabIndex = 56;
      // 
      // label91
      // 
      this.label91.AutoSize = true;
      this.label91.Location = new System.Drawing.Point(7, 114);
      this.label91.Name = "label91";
      this.label91.Size = new System.Drawing.Size(53, 13);
      this.label91.TabIndex = 94;
      this.label91.Text = "Password";
      // 
      // edt_credit_password_req
      // 
      this.edt_credit_password_req.Location = new System.Drawing.Point(133, 111);
      this.edt_credit_password_req.Name = "edt_credit_password_req";
      this.edt_credit_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_password_req.TabIndex = 92;
      // 
      // label92
      // 
      this.label92.AutoSize = true;
      this.label92.Location = new System.Drawing.Point(7, 88);
      this.label92.Name = "label92";
      this.label92.Size = new System.Drawing.Size(57, 13);
      this.label92.TabIndex = 93;
      this.label92.Text = "UserName";
      // 
      // edt_credit_username_req
      // 
      this.edt_credit_username_req.Location = new System.Drawing.Point(133, 85);
      this.edt_credit_username_req.Name = "edt_credit_username_req";
      this.edt_credit_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_credit_username_req.TabIndex = 91;
      // 
      // label104
      // 
      this.label104.AutoSize = true;
      this.label104.Location = new System.Drawing.Point(13, 113);
      this.label104.Name = "label104";
      this.label104.Size = new System.Drawing.Size(53, 13);
      this.label104.TabIndex = 99;
      this.label104.Text = "Password";
      // 
      // edt_cancel_bet_password_req
      // 
      this.edt_cancel_bet_password_req.Location = new System.Drawing.Point(139, 110);
      this.edt_cancel_bet_password_req.Name = "edt_cancel_bet_password_req";
      this.edt_cancel_bet_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_password_req.TabIndex = 97;
      // 
      // label105
      // 
      this.label105.AutoSize = true;
      this.label105.Location = new System.Drawing.Point(13, 87);
      this.label105.Name = "label105";
      this.label105.Size = new System.Drawing.Size(57, 13);
      this.label105.TabIndex = 98;
      this.label105.Text = "UserName";
      // 
      // edt_cancel_bet_username_req
      // 
      this.edt_cancel_bet_username_req.Location = new System.Drawing.Point(139, 84);
      this.edt_cancel_bet_username_req.Name = "edt_cancel_bet_username_req";
      this.edt_cancel_bet_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_cancel_bet_username_req.TabIndex = 96;
      // 
      // label106
      // 
      this.label106.AutoSize = true;
      this.label106.Location = new System.Drawing.Point(13, 112);
      this.label106.Name = "label106";
      this.label106.Size = new System.Drawing.Size(53, 13);
      this.label106.TabIndex = 134;
      this.label106.Text = "Password";
      // 
      // edt_debitcredit_password_req
      // 
      this.edt_debitcredit_password_req.Location = new System.Drawing.Point(139, 109);
      this.edt_debitcredit_password_req.Name = "edt_debitcredit_password_req";
      this.edt_debitcredit_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_password_req.TabIndex = 132;
      // 
      // label107
      // 
      this.label107.AutoSize = true;
      this.label107.Location = new System.Drawing.Point(13, 86);
      this.label107.Name = "label107";
      this.label107.Size = new System.Drawing.Size(57, 13);
      this.label107.TabIndex = 133;
      this.label107.Text = "UserName";
      // 
      // edt_debitcredit_username_req
      // 
      this.edt_debitcredit_username_req.Location = new System.Drawing.Point(139, 83);
      this.edt_debitcredit_username_req.Name = "edt_debitcredit_username_req";
      this.edt_debitcredit_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_debitcredit_username_req.TabIndex = 131;
      // 
      // label108
      // 
      this.label108.AutoSize = true;
      this.label108.Location = new System.Drawing.Point(13, 110);
      this.label108.Name = "label108";
      this.label108.Size = new System.Drawing.Size(53, 13);
      this.label108.TabIndex = 98;
      this.label108.Text = "Password";
      // 
      // edt_endgame_password_req
      // 
      this.edt_endgame_password_req.Location = new System.Drawing.Point(139, 107);
      this.edt_endgame_password_req.Name = "edt_endgame_password_req";
      this.edt_endgame_password_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_password_req.TabIndex = 96;
      // 
      // label109
      // 
      this.label109.AutoSize = true;
      this.label109.Location = new System.Drawing.Point(13, 84);
      this.label109.Name = "label109";
      this.label109.Size = new System.Drawing.Size(57, 13);
      this.label109.TabIndex = 97;
      this.label109.Text = "UserName";
      // 
      // edt_endgame_username_req
      // 
      this.edt_endgame_username_req.Location = new System.Drawing.Point(139, 81);
      this.edt_endgame_username_req.Name = "edt_endgame_username_req";
      this.edt_endgame_username_req.Size = new System.Drawing.Size(184, 20);
      this.edt_endgame_username_req.TabIndex = 95;
      // 
      // frm_ParyPlay_client
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(692, 612);
      this.Controls.Add(this.tabControl1);
      this.Name = "frm_ParyPlay_client";
      this.Text = "Pari Play Client";
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tabPage1.PerformLayout();
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      this.tabPage3.ResumeLayout(false);
      this.tabPage3.PerformLayout();
      this.tabPage4.ResumeLayout(false);
      this.tabPage4.PerformLayout();
      this.tabPage5.ResumeLayout(false);
      this.tabPage5.PerformLayout();
      this.tabPage6.ResumeLayout(false);
      this.tabPage6.PerformLayout();
      this.tabPage7.ResumeLayout(false);
      this.tabPage7.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox edt_auth_rep_rounds_left;
    private System.Windows.Forms.TextBox edt_auth_rep_financial_mode;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox edt_auth_rep_bonus_balance;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox edt_auth_rep_balance;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox edt_auth_Password_req;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox edt_auth_UserName_req;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox edt_auth_rep;
    private System.Windows.Forms.Button btnAuthenticate;
    private System.Windows.Forms.TextBox edt_auth_token_req;
    private System.Windows.Forms.TextBox edt_auth_req;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.TabPage tabPage4;
    private System.Windows.Forms.TabPage tabPage5;
    private System.Windows.Forms.TabPage tabPage6;
    private System.Windows.Forms.TabPage tabPage7;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox edt_debit_timestamp_rep;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox edt_debit_transaction_id_rep;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox edt_debit_bonus_balance_rep;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox edt_debit_balance_rep;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox edt_debit_playerid_req;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.TextBox edt_debit_gamecode_req;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox edt_debit_rep;
    private System.Windows.Forms.Button btnDebit;
    private System.Windows.Forms.TextBox edt_debit_token_req;
    private System.Windows.Forms.TextBox edt_debit_req;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.Label label27;
    private System.Windows.Forms.TextBox edt_debit_transaction_configuration_req;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.TextBox edt_debit_feature_id_req;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.TextBox edt_debit_feature_req;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox edt_debit_endgame_req;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox edt_debit_amount_req;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox edt_debit_transaction_id_req;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.TextBox edt_debit_round_id_req;
    private System.Windows.Forms.Label label43;
    private System.Windows.Forms.TextBox edt_credit_feature_req;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox edt_credit_transaction_configuration_req;
    private System.Windows.Forms.Label label28;
    private System.Windows.Forms.TextBox edt_credit_featureid_req;
    private System.Windows.Forms.Label label29;
    private System.Windows.Forms.TextBox edt_credit_endgame_req;
    private System.Windows.Forms.Label label30;
    private System.Windows.Forms.TextBox edt_credit_credit_type_req;
    private System.Windows.Forms.Label label31;
    private System.Windows.Forms.TextBox edt_credit_amount_req;
    private System.Windows.Forms.Label label32;
    private System.Windows.Forms.TextBox edt_credit_transactionid_req;
    private System.Windows.Forms.Label label33;
    private System.Windows.Forms.TextBox edt_credit_roundid_req;
    private System.Windows.Forms.Label label34;
    private System.Windows.Forms.TextBox edt_credit_timestamp_rep;
    private System.Windows.Forms.Label label35;
    private System.Windows.Forms.TextBox edt_credit_transaction_id_rep;
    private System.Windows.Forms.Label label36;
    private System.Windows.Forms.TextBox edt_credit_bonus_balance_rep;
    private System.Windows.Forms.Label label37;
    private System.Windows.Forms.TextBox edt_credit_balance_rep;
    private System.Windows.Forms.Label label38;
    private System.Windows.Forms.TextBox edt_credit_playerid_req;
    private System.Windows.Forms.Label label39;
    private System.Windows.Forms.TextBox edt_credit_gamecode_req;
    private System.Windows.Forms.Label label40;
    private System.Windows.Forms.TextBox edt_credit_rep;
    private System.Windows.Forms.Button btnCredit;
    private System.Windows.Forms.TextBox edt_credit_token_req;
    private System.Windows.Forms.TextBox edt_credit_req;
    private System.Windows.Forms.Label label41;
    private System.Windows.Forms.Label label42;
    private System.Windows.Forms.TextBox edt_auth_rep_timestamp;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label44;
    private System.Windows.Forms.Label label45;
    private System.Windows.Forms.TextBox edt_balance_timestamp_rep;
    private System.Windows.Forms.TextBox edt_balance_rounds_left_rep;
    private System.Windows.Forms.Label label47;
    private System.Windows.Forms.TextBox edt_balance_bonus_balance_rep;
    private System.Windows.Forms.Label label48;
    private System.Windows.Forms.TextBox edt_balance_balance_rep;
    private System.Windows.Forms.Label label49;
    private System.Windows.Forms.TextBox edt_balance_password_req;
    private System.Windows.Forms.Label label50;
    private System.Windows.Forms.TextBox edt_balance_username_req;
    private System.Windows.Forms.Label label51;
    private System.Windows.Forms.TextBox edt_balance_rep;
    private System.Windows.Forms.Button btnBalance;
    private System.Windows.Forms.TextBox edt_balance_token_req;
    private System.Windows.Forms.TextBox edt_balance_req;
    private System.Windows.Forms.Label label52;
    private System.Windows.Forms.Label label53;
    private System.Windows.Forms.Label label56;
    private System.Windows.Forms.Label label55;
    private System.Windows.Forms.TextBox edt_cancel_bet_timestamp_rep;
    private System.Windows.Forms.TextBox edt_cancel_bet_roundsleft_rep;
    private System.Windows.Forms.Label label54;
    private System.Windows.Forms.Label label46;
    private System.Windows.Forms.Label label69;
    private System.Windows.Forms.TextBox edt_cancel_bet_amount_req;
    private System.Windows.Forms.TextBox edt_cancel_bet_reason_req;
    private System.Windows.Forms.TextBox edt_cancel_bet_transactionid_req;
    private System.Windows.Forms.Label label57;
    private System.Windows.Forms.TextBox edt_cancel_bet_cancelentireround_req;
    private System.Windows.Forms.Label label58;
    private System.Windows.Forms.TextBox edt_cancel_bet_roundid_req;
    private System.Windows.Forms.Label label59;
    private System.Windows.Forms.TextBox edt_cancel_bet_playerid_req;
    private System.Windows.Forms.Label label60;
    private System.Windows.Forms.TextBox edt_cancel_bet_extradata_rep;
    private System.Windows.Forms.Label label61;
    private System.Windows.Forms.TextBox edt_cancel_bet_transactionid_rep;
    private System.Windows.Forms.Label label62;
    private System.Windows.Forms.TextBox edt_cancel_bet_bonusbalance_rep;
    private System.Windows.Forms.Label label63;
    private System.Windows.Forms.TextBox edt_cancel_bet_balance_rep;
    private System.Windows.Forms.Label label64;
    private System.Windows.Forms.TextBox edt_cancel_bet_gamecode_req;
    private System.Windows.Forms.Label label65;
    private System.Windows.Forms.TextBox edt_cancel_bet_reftransactionid_req;
    private System.Windows.Forms.Label label66;
    private System.Windows.Forms.TextBox edt_cancel_bet_rep;
    private System.Windows.Forms.Button btnCancelBet;
    private System.Windows.Forms.TextBox edt_cancel_bet_token_req;
    private System.Windows.Forms.TextBox edt_cancel_bet_req;
    private System.Windows.Forms.Label label67;
    private System.Windows.Forms.Label label68;
    private System.Windows.Forms.Label label87;
    private System.Windows.Forms.Label label73;
    private System.Windows.Forms.TextBox edt_debitcredit_timestamp_rep;
    private System.Windows.Forms.TextBox edt_debitcredit_roundsleft_rep;
    private System.Windows.Forms.Label label71;
    private System.Windows.Forms.Label label70;
    private System.Windows.Forms.TextBox edt_debitcredit_feature_req;
    private System.Windows.Forms.Label label72;
    private System.Windows.Forms.TextBox edt_debitcredit_featureid_req;
    private System.Windows.Forms.TextBox edt_debitcredit_credit_type_req;
    private System.Windows.Forms.Label label74;
    private System.Windows.Forms.TextBox edt_debitcredit_credit_amount_req;
    private System.Windows.Forms.Label label75;
    private System.Windows.Forms.TextBox edt_debitcredit_debit_amount_req;
    private System.Windows.Forms.Label label76;
    private System.Windows.Forms.TextBox edt_debitcredit_transactionid_req;
    private System.Windows.Forms.Label label77;
    private System.Windows.Forms.TextBox edt_debitcredit_roundid_req;
    private System.Windows.Forms.Label label78;
    private System.Windows.Forms.TextBox edt_debitcredit_extradata_rep;
    private System.Windows.Forms.Label label79;
    private System.Windows.Forms.TextBox edt_debitcredit_transactionid_rep;
    private System.Windows.Forms.Label label80;
    private System.Windows.Forms.TextBox edt_debitcredit_bonusbalance_rep;
    private System.Windows.Forms.Label label81;
    private System.Windows.Forms.TextBox edt_debitcredit_balance_rep;
    private System.Windows.Forms.Label label82;
    private System.Windows.Forms.TextBox edt_debitcredit_playerid_req;
    private System.Windows.Forms.Label label83;
    private System.Windows.Forms.TextBox edt_debitcredit_gamecode_req;
    private System.Windows.Forms.Label label84;
    private System.Windows.Forms.TextBox edt_debitcredit_rep;
    private System.Windows.Forms.Button btnDebitCredit;
    private System.Windows.Forms.TextBox edt_debitcredit_token_req;
    private System.Windows.Forms.TextBox edt_debitcredit_req;
    private System.Windows.Forms.Label label85;
    private System.Windows.Forms.Label label86;
    private System.Windows.Forms.Label label88;
    private System.Windows.Forms.TextBox edt_endgame_transactionconfiguration_req;
    private System.Windows.Forms.Label label93;
    private System.Windows.Forms.TextBox edt_endgame_transactionId_req;
    private System.Windows.Forms.Label label94;
    private System.Windows.Forms.TextBox edt_endgame_roundid_req;
    private System.Windows.Forms.Label label95;
    private System.Windows.Forms.TextBox edt_endgame_timestamp_rep;
    private System.Windows.Forms.Label label96;
    private System.Windows.Forms.TextBox edt_endgame_transactionid_rep;
    private System.Windows.Forms.Label label97;
    private System.Windows.Forms.TextBox edt_endgame_bonusbalance_rep;
    private System.Windows.Forms.Label label98;
    private System.Windows.Forms.TextBox edt_endgame_balance_rep;
    private System.Windows.Forms.Label label99;
    private System.Windows.Forms.TextBox edt_endgame_playerid_req;
    private System.Windows.Forms.Label label100;
    private System.Windows.Forms.TextBox edt_endgame_gamecode_req;
    private System.Windows.Forms.Label label101;
    private System.Windows.Forms.TextBox edt_endgame_rep;
    private System.Windows.Forms.Button btnEndGame;
    private System.Windows.Forms.TextBox edt_endgame_token_req;
    private System.Windows.Forms.TextBox edt_endgame_req;
    private System.Windows.Forms.Label label102;
    private System.Windows.Forms.Label label103;
    private System.Windows.Forms.Label label89;
    private System.Windows.Forms.TextBox edt_debit_password_req;
    private System.Windows.Forms.Label label90;
    private System.Windows.Forms.TextBox edt_debit_username_req;
    private System.Windows.Forms.Label label91;
    private System.Windows.Forms.TextBox edt_credit_password_req;
    private System.Windows.Forms.Label label92;
    private System.Windows.Forms.TextBox edt_credit_username_req;
    private System.Windows.Forms.Label label104;
    private System.Windows.Forms.TextBox edt_cancel_bet_password_req;
    private System.Windows.Forms.Label label105;
    private System.Windows.Forms.TextBox edt_cancel_bet_username_req;
    private System.Windows.Forms.Label label106;
    private System.Windows.Forms.TextBox edt_debitcredit_password_req;
    private System.Windows.Forms.Label label107;
    private System.Windows.Forms.TextBox edt_debitcredit_username_req;
    private System.Windows.Forms.Label label108;
    private System.Windows.Forms.TextBox edt_endgame_password_req;
    private System.Windows.Forms.Label label109;
    private System.Windows.Forms.TextBox edt_endgame_username_req;
  }
}

