﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Balance.cs
//// 
////      DESCRIPTION: Balance Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Common;
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class Balance : Base
  {

    #region " Constructor "

    /// <summary>
    /// Balance constructor
    /// </summary>
    /// <param name="Controller"></param>
    public Balance(ApiController Controller) : base(Controller) { ; } // Balance

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Process Autenticate Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(BaseRequest Request)
    {
      return ProcessRequest(Request, false);
    } // ProcessRequest
    public HttpResponseMessage ProcessRequest(BaseRequest Request, Boolean IsAuthentication)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      AuthenticateGetBalanceResponse _response;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Request data validation and authentication (Base)
          if (!StartSession(Request, IsAuthentication, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = GetBalanceInternal(ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return AuthenticateGetBalanceResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // ProcessRequest

    #endregion

    #region "Protected Methods"

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    protected Boolean StartSession(BaseRequest Request, Boolean IsAuthentication, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      Boolean _is_expierd;

      // Validation and authentication request
      if (!WigosAuthentication(Request, out Session, out ErrorCode, out _is_expierd))
      {
        return false;
      }

      // Request data validation
      if (!RequestDataValidation(Request.Token, _is_expierd, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, IsAuthentication, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Token"></param>
    /// <param name="IsExpired"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(String Token, Boolean IsExpired, out ErrorCode ErrorCode)
    {
      ErrorCode = ErrorCode.GeneralError;

      // Expierd Token 
      if (IsExpired)
      {
        ErrorCode = ErrorCode.TokenExpired;
        Log.Message(String.Format("AuthenticateGetBalance.RequestDataValidation: Token '{0}' is expired.", Token));

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get account balance internal
    /// </summary>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private AuthenticateGetBalanceResponse GetBalanceInternal(ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      // Process GetBalance response
      if (!ProcessGetBalanceRequest(ref Session, SqlTrx))
      {
        return null;
      }

      // Get account balance
      return GetResponse(Session);

    } // GetBalanceInternal

    /// <summary>
    /// Process Balance request
    /// </summary>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ProcessGetBalanceRequest(ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      try
      {
        // Get account data
        GameGateway.GetDataAccount(ref Session, SqlTrx);

        return (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK);

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessGetBalanceResponse";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;

    } // ProcessGetBalanceRequest

    /// <summary>
    /// Prepare balance response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private AuthenticateGetBalanceResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null)
      {
        return null;
      }

      // return success response
      return new AuthenticateGetBalanceResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0,
        RoundsLeft = 1
      };

    } // GetResponse

    #endregion

  }
}