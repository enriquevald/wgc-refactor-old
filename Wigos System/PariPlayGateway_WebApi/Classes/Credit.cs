﻿using PariPlay_GameGateway.Common;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Credit.cs
//// 
////      DESCRIPTION: Credit Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class Credit : Base
  {

    #region " Constructor "

    /// <summary>
    /// Credit constructor
    /// </summary>
    /// <param name="Controller"></param>
    public Credit(ApiController Controller) : base(Controller) { ; } // Credit

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Process Credit Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(CreditRequest Request)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      CreditResponse _response;

      try
      {
        // Start Sql Transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Request data validation and authentication
          if (!StartSession(Request, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = CreditInternal(Request, ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return CreditResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // ProcessRequest

    /// <summary>
    /// Process Credit Request
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean ProcessRequest(CreditRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      return (CreditInternal(Request, ref Session, SqlTrx) != null);

    } // ProcessRequest

    #endregion

    #region "Protected Methods"

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    protected Boolean StartSession(CreditRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      Boolean _is_expierd;

      // Validation and authentication request
      if (!WigosAuthentication(Request, out Session, out ErrorCode, out _is_expierd))
      {
        return false;
      }

      // Set TransactionId for credit operation    
      Session.TransactionId = Request.TransactionId;

      // Request data validation
      if (!RequestDataValidation(Request, Session, _is_expierd, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="IsExpired"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(CreditRequest Request, LCD_Session Session, Boolean IsExpired, out ErrorCode ErrorCode)
    {
      Int64 _transaction_id;

      ErrorCode = ErrorCode.GeneralError;

      // Expierd Token 
      if (IsExpired)
      {
        ErrorCode = ErrorCode.TokenExpired;
        Log.Message(String.Format("Credit.RequestDataValidation: Token '{0}' is expired.", Request.Token));

        return false;
      }

      // PlayerId 
      if (Request.PlayerId != Session.AccountId.ToString())
      {
        ErrorCode = ErrorCode.InvalidUserId;
        Log.Message("Credit.RequestDataValidation: Invalid PlayerId.");

        return false;
      }

      // GameCode 
      if (Request.GameCode != Session.GameName)
      {
        ErrorCode = ErrorCode.InvalidGame;
        Log.Message("Credit.RequestDataValidation: Invalid GameCode.");

        return false;
      }

      // Amount 
      if (Request.Amount <= 0)
      {
        ErrorCode = ErrorCode.InvalidNegativeAmount;
        Log.Message("Credit.RequestDataValidation: Amount must be greater than 0.");

        return false;
      }

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId)
        || !Int64.TryParse(Request.TransactionId, out _transaction_id))
      {
        ErrorCode = ErrorCode.UnknownTransactionId;
        Log.Message("Credit.RequestDataValidation: TransactionId is not valid.");

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get Credit internal response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private CreditResponse CreditInternal(CreditRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      // Set Transaction type
      Session.TransactionType = GameGateway.TRANSACTION_TYPE.PRIZE;

      // Process credit response
      if (!ProcessCreditRequest(Request.Amount, ref Session, SqlTrx))
      {
        return null;
      }

      // End game action
      if (Request.EndGame)
      {
        if (!new EndGame(this.Controller).ProcessRequest(Request.ToEndGameRequest(), ref Session, SqlTrx))
        {
          return null;
        }
      }

      // Get credit response
      return GetResponse(Session);

    } // CreditInternal

    /// <summary>
    /// Process Credit request
    /// </summary>
    /// <param name="AddAmount"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ProcessCreditRequest(Decimal AddAmount, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      try
      {
        // Get account data
        GameGateway.GetDataAccount(ref Session, SqlTrx);

        if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return false;
        }

        // Call to process credit operation
        GameGateway.ProcessCreditDebit(AddAmount, 0, ref Session, SqlTrx);

        return (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK);

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessCreditRequest";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;

    } // ProcessCreditRequest

    /// <summary>
    /// Prepare credit response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private CreditResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null)
      {
        return null;
      }

      // return success response
      return new CreditResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        TransactionId = Session.TransactionId,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0
      };

    } // GetResponse

    #endregion

  }
}