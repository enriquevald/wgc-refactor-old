﻿using PariPlay_GameGateway.Common;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Balance.cs
//// 
////      DESCRIPTION: CancelTransaction Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class CancelTransaction : Base
  {

    #region " Constructor "

    /// <summary>
    /// CancelTransaction constructor
    /// </summary>
    /// <param name="Controller"></param>
    public CancelTransaction(ApiController Controller) : base(Controller) { ; } // CancelTransaction

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Process CancelTransaction Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(CancelTransactionRequest Request)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      CancelTransactionResponse _response;

      try
      {
        // Start Sql Transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Start session
          if (!StartSession(Request, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = CancelTransactionInternal(ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return DebitResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // ProcessRequest

    #endregion

    #region "Protected Methods"

    /// <summary>
    /// Validate and authentication request data
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    protected Boolean StartSession(CancelTransactionRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      // Wigos authentication
      if (!WigosAuthentication(Request, out Session, out ErrorCode))
      {
        return false;
      }

      // Set TransactionId for cancel transaction operation                
      Session.TransactionId = Request.TransactionId;

      // Request data validation
      if (!RequestDataValidation(Request, Session, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(CancelTransactionRequest Request, LCD_Session Session, out ErrorCode ErrorCode)
    {
      Int64 _transaction_id;

      ErrorCode = ErrorCode.GeneralError;

      // PlayerId 
      if (Request.PlayerId != Session.AccountId.ToString())
      {
        ErrorCode = ErrorCode.InvalidUserId;
        Log.Message("CancelTransaction.RequestDataValidation: Invalid PlayerId.");

        return false;
      }

      // GameCode 
      if (Request.GameCode != Session.GameName)
      {
        ErrorCode = ErrorCode.InvalidGame;
        Log.Message("CancelTransaction.RequestDataValidation: Invalid GameCode.");

        return false;
      }

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId)
        || !Int64.TryParse(Request.TransactionId, out _transaction_id))
      {
        ErrorCode = ErrorCode.UnknownTransactionId;
        Log.Message("CancelTransaction.RequestDataValidation: TransactionId is not valid.");

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Cancel transaction internal response
    /// </summary>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private CancelTransactionResponse CancelTransactionInternal(ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      // Process cancel transaction response
      if (!ProcessCancelTransactionRequest(ref Session, SqlTrx))
      {
        return null;
      }

      // Get cancel transaction response
      return GetResponse(Session);

    } // CancelTransactionInternal

    /// <summary>
    /// Process cancel transaction request
    /// </summary>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ProcessCancelTransactionRequest(ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      try
      {
        // TODO JBP: Cancel transaction
        //------------------------------

        return (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK);

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessCancelTransactionRequest";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;

    } // ProcessCancelTransactionRequest

    /// <summary>
    /// Prepare CancelTransaction response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private CancelTransactionResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null || Session.Balance == null)
      {
        return null;
      }

      // return success response
      return new CancelTransactionResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        TransactionId = Session.TransactionId,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0,
        ExtraData = "",
        RoundsLeft = 1
      };

    } // GetResponse

    #endregion

  }
}