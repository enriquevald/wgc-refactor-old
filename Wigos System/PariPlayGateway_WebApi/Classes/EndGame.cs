﻿using PariPlay_GameGateway.Common;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: EndGame.cs
//// 
////      DESCRIPTION: EndGame Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class EndGame : Base
  {

    #region " Constructor "

    /// <summary>
    /// EndGame constructor
    /// </summary>
    /// <param name="Controller"></param>
    public EndGame(ApiController Controller) : base(Controller) { ; }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Process EndGame Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(EndGameRequest Request)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      EndGameResponse _response;

      try
      {
        // Start Sql Transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Request data validation and authentication
          if (!StartSession(Request, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = EndGameInternal(Request, ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return EndGameResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // ProcessRequest

    /// <summary>
    /// Process EndGame Request
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean ProcessRequest(EndGameRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      return (EndGameInternal(Request, ref Session, SqlTrx) != null);

    } // ProcessRequest

    #endregion

    #region " Protected Methods "

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    protected Boolean StartSession(EndGameRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      Boolean _is_expierd;

      // Validation and authentication request
      if (!WigosAuthentication(Request, out Session, out ErrorCode, out _is_expierd))
      {
        return false;
      }

      // Set TransactionId data for debit/credit operation
      Session.TransactionId = Request.TransactionId;

      // Request data validation
      if (!RequestDataValidation(Request, Session, _is_expierd, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="IsExpired"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(EndGameRequest Request, LCD_Session Session, Boolean IsExpired, out ErrorCode ErrorCode)
    {
      Int64 _transaction_id;

      ErrorCode = ErrorCode.GeneralError;

      // Expierd Token 
      if (IsExpired)
      {
        ErrorCode = ErrorCode.RoundAlreadyEnded;
        Log.Message(String.Format("EndGame.RequestDataValidation: Token '{0}' is expired.", Request.Token));

        return false;
      }

      // PlayerId 
      if (Request.PlayerId != Session.AccountId.ToString())
      {
        ErrorCode = ErrorCode.InvalidUserId;
        Log.Message("EndGame.RequestDataValidation: Invalid PlayerId.");

        return false;
      }

      // GameCode 
      if (Request.GameCode != Session.GameName)
      {
        ErrorCode = ErrorCode.InvalidGame;
        Log.Message("EndGame.RequestDataValidation: Invalid GameCode.");

        return false;
      }

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId)
        || !Int64.TryParse(Request.TransactionId, out _transaction_id))
      {
        ErrorCode = ErrorCode.UnknownTransactionId;
        Log.Message("EndGame.RequestDataValidation: TransactionId is not valid.");

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get EndGame internal response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private EndGameResponse EndGameInternal(EndGameRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      // Process credit response
      if (!ProcessEndGameRequest(ref Session, SqlTrx))
      {
        return null;
      }

      // Get EndGame response
      return GetResponse(Session);

    } // EndGameInternal

    /// <summary>
    /// Process EndGame request
    /// </summary>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ProcessEndGameRequest(ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      try
      {
        // Get Balance
        GameGateway.GetDataAccount(ref Session, SqlTrx);

        if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return false;
        }

        // EndGame session
        return Session.PariPlay_EndGameInstance(SqlTrx);

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessEndGameRequest";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;

    } // ProcessEndGameRequest

    /// <summary>
    /// Prepare end game response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private EndGameResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null)
      {
        return null;
      }

      // return success response
      return new EndGameResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        TransactionId = Session.TransactionId,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0
      };

    } // GetResponse

    #endregion

  }
}