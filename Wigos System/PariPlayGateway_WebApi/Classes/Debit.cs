﻿using PariPlay_GameGateway.Common;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Debit.cs
//// 
////      DESCRIPTION: Debit Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class Debit : Base
  {

    #region " Constructor "

    /// <summary>
    /// Debit constructor
    /// </summary>
    /// <param name="Controller"></param>
    public Debit(ApiController Controller) : base(Controller) { ; } // Debit

    #endregion

    #region "Public Methods"

    /// <summary>
    /// Process Debit Request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(DebitRequest Request)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      DebitResponse _response;

      try
      {
        // Start Sql Transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Request data validation and authentication
          if (!StartSession(Request, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = DebitInternal(Request, ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return DebitResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // ProcessRequest

    /// <summary>
    /// Process Debit Request
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean ProcessRequest(DebitRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      return (DebitInternal(Request, ref Session, SqlTrx) != null);

    } // ProcessRequest

    #endregion

    #region "Proteceted Methods"

    /// <summary>
    /// Validate and authentication request data
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    protected Boolean StartSession(DebitRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      Boolean _is_expierd;

      // Validation and authentication request
      if (!WigosAuthentication(Request, out Session, out ErrorCode, out _is_expierd))
      {
        return false;
      }

      // Set TransactionId data for debit operation
      Session.TransactionId = Request.TransactionId;

      // Request data validation
      if (!RequestDataValidation(Request, Session, _is_expierd, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="IsExpired"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(DebitRequest Request, LCD_Session Session, Boolean IsExpired, out ErrorCode ErrorCode)
    {
      Int64 _transaction_id;

      ErrorCode = ErrorCode.GeneralError;

      // Expierd Token 
      if (IsExpired)
      {
        ErrorCode = ErrorCode.TokenExpired;
        Log.Message(String.Format("Debit.RequestDataValidation: Token '{0}' is expired.", Request.Token));

        return false;
      }

      // PlayerId 
      if (Request.PlayerId != Session.AccountId.ToString())
      {
        ErrorCode = ErrorCode.InvalidUserId;
        Log.Message("Debit.RequestDataValidation: Invalid PlayerId.");

        return false;
      }

      // GameCode 
      if (Request.GameCode != Session.GameName)
      {
        ErrorCode = ErrorCode.InvalidGame;
        Log.Message("Debit.RequestDataValidation: Invalid GameCode.");

        return false;
      }

      // Amount 
      if (Request.Amount <= 0)
      {
        ErrorCode = ErrorCode.InvalidNegativeAmount;
        Log.Message("Debit.RequestDataValidation: Amount must be greater than 0.");

        return false;
      }

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId)
        || !Int64.TryParse(Request.TransactionId, out _transaction_id))
      {
        ErrorCode = ErrorCode.UnknownTransactionId;
        Log.Message("Debit.RequestDataValidation: TransactionId is not valid.");

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get Debit internal response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private DebitResponse DebitInternal(DebitRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      Session.TransactionType = GameGateway.TRANSACTION_TYPE.BET;

      // Process Debit response
      if (!ProcessDebitRequest(Request.Amount, ref Session, SqlTrx))
      {
        return null;
      }

      // End game action
      if (Request.EndGame)
      {
        if (!new EndGame(this.Controller).ProcessRequest(Request.ToEndGameRequest(), ref Session, SqlTrx))
        {
          return null;
        }
      }

      // Get debit response
      return GetResponse(Session);

    } // DebitInternal

    /// <summary>
    /// Process Debit request
    /// </summary>
    /// <param name="SubAmount"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean ProcessDebitRequest(Decimal SubAmount, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      try
      {
        // Get account data
        GameGateway.GetDataAccount(ref Session, SqlTrx);

        if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
        {
          return false;
        }

        // Check balance
        if (!CheckDebitBalance(SubAmount, ref Session))
        {
          return false;
        }

        // Call to process debit operation
        GameGateway.ProcessCreditDebit(0, SubAmount, ref Session, SqlTrx);

        return (Session.ErrorCode == GameGateway.RESPONSE_ERROR_CODE.OK);

      }
      catch (Exception _ex)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL;
        Session.ErrorMessage = "Error ProcessDebitResponse";

        Log.Exception(_ex);
        Log.Message(Session.ErrorMessage);
      }

      return false;

    } // ProcessDebitRequest

    /// <summary>
    /// Prepare Debit response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private DebitResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null)
      {
        return null;
      }

      // return success response
      return new DebitResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        TransactionId = Session.TransactionId,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0
      };

    } // GetResponse

    /// <summary>
    /// Check Balance in Debit Request
    /// </summary>
    /// <param name="SubAmount"></param>
    /// <param name="Session"></param>
    /// <returns></returns>
    private Boolean CheckDebitBalance(Decimal SubAmount, ref LCD_Session Session)
    {
      // Check balance
      if (Session.Balance.TotalRedeemable < SubAmount)
      {
        Session.ErrorCode = GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE;
        Session.ErrorMessage = "Debit.CheckDebitBalance: Not enough balance.";

        return false;
      }

      return true;

    } // CheckDebitBalance

    #endregion

  }
}