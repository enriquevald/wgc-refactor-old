﻿////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: Base.cs
//// 
////      DESCRIPTION: Base Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using Newtonsoft.Json;
using PariPlay_GameGateway.Common;
using PariPlay_GameGateway.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class Base
  {
    #region " Members "

    private ApiController m_controller;

    #endregion

    #region " Properties "

    public ApiController Controller
    {
      get { return m_controller; }
      set { m_controller = value; }
    } // Controller

    #endregion

    #region " Constructor "

    /// <summary>
    /// Base constructor
    /// </summary>
    /// <param name="Controller"></param>
    public Base(ApiController Controller)
    {
      this.Controller = Controller;
    } // Base

    #endregion

    #region " Protected Methods "

    #region " Data Validation "

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="IsExpired"></param>
    /// <returns></returns>
    protected Boolean WigosAuthentication(BaseRequest Request, out LCD_Session Session, out ErrorCode ErrorCode)
    {
      Boolean _is_expired;
      return WigosAuthentication(Request, out Session, out ErrorCode, out _is_expired);
    } // WigosAuthentication
    protected Boolean WigosAuthentication(BaseRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, out Boolean IsExpired)
    {
      Session = null;
      ErrorCode = ErrorCode.GeneralError;
      IsExpired = true;

      // Data validation
      if (!AuthenticationDataValidation(Request, out ErrorCode))
      {
        return false;
      }

      // Internal Authentication
      Session = new LCD_Session(Request.Token, Request.Account.UserName, Request.Account.Password, out IsExpired);

      if (Session.ErrorCode != GameGateway.RESPONSE_ERROR_CODE.OK)
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // WigosAuthentication

    /// <summary>
    /// Authentication validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    protected Boolean AuthenticationDataValidation(BaseRequest Request, out ErrorCode ErrorCode)
    {
      ErrorCode = ErrorCode.GeneralError;

      // Request
      if (Request == null)
      {
        ErrorCode = ErrorCode.GeneralError;
        Log.Message("AuthenticationDataValidation: Invalid request.");

        return false;
      }

      // Token
      if (String.IsNullOrEmpty(Request.Token))
      {
        ErrorCode = ErrorCode.InvalidToken;
        Log.Message("AuthenticationDataValidation: Token is null or empty.");

        return false;
      }

      // Account
      if (Request.Account == null)
      {
        ErrorCode = ErrorCode.AuthenticationFailed;
        Log.Message("AuthenticationDataValidation: Account is null.");

        return false;
      }

      // Username
      if (String.IsNullOrEmpty(Request.Account.UserName))
      {
        ErrorCode = ErrorCode.AuthenticationFailed;
        Log.Message("AuthenticationDataValidation: UserName is null or empty.");

        return false;
      }

      // Password
      if (String.IsNullOrEmpty(Request.Account.Password))
      {
        ErrorCode = ErrorCode.AuthenticationFailed;
        Log.Message("AuthenticationDataValidation: Password is null or empty.");

        return false;
      }

      return true;

    } // AuthenticationDataValidation

    #endregion

    /// <summary>
    /// Build Error response
    /// </summary>
    /// <param name="ErrorCode"></param>
    /// <param name="Balance"></param>
    /// <returns></returns>
    protected ErrorResponse BuildErrorResponse(ErrorCode ErrorCode, Decimal Balance)
    {
      return new ErrorResponse()
      {
        ErrorCode = (int)ErrorCode,
        Balance = Balance
      };

    } // BuildErrorResponse

    #endregion

    #region " Responses "

    /// <summary>
    /// Request success response
    /// </summary>
    /// <param name="Response"></param>
    /// <returns></returns>
    public HttpResponseMessage RequestSuccessResponse(object Response)
    {
      return Controller.Request.CreateResponse(HttpStatusCode.OK, Response);
    } // RequestSuccessResponse

    /// <summary>
    /// Prepare request error response
    /// </summary>
    /// <param name="Error"></param>
    /// <param name="Balance"></param>
    /// <returns></returns>
    public HttpResponseMessage RequestErrorResponse(GameGateway.RESPONSE_ERROR_CODE Error)
    {
      return RequestErrorResponse(Error, 0);
    } // RequestErrorResponse
    public HttpResponseMessage RequestErrorResponse(GameGateway.RESPONSE_ERROR_CODE Error, Decimal Balance)
    {
      return RequestErrorResponse(ResponseMapping(Error), Balance);
    } // RequestErrorResponse

    /// <summary>
    /// Prepare request error response
    /// </summary>
    /// <param name="Error"></param>
    /// <param name="Balance"></param>
    /// <returns></returns>
    public HttpResponseMessage RequestErrorResponse()
    {
      return this.Controller.Request.CreateResponse(HttpStatusCode.BadRequest, BuildErrorResponse(ErrorCode.GeneralError, 0));
    } // RequestErrorResponse
    public HttpResponseMessage RequestErrorResponse(ErrorCode Error)
    {
      return this.Controller.Request.CreateResponse(HttpStatusCode.BadRequest, BuildErrorResponse(Error, 0));
    } // RequestErrorResponse
    public HttpResponseMessage RequestErrorResponse(ErrorCode Error, Decimal Balance)
    {
      return this.Controller.Request.CreateResponse(HttpStatusCode.BadRequest, BuildErrorResponse(Error, Balance));
    } // RequestErrorResponse

    /// <summary>
    /// Response error from GameGateWay to ParyPlay error code
    /// </summary>
    /// <param name="ResponseError"></param>
    public ErrorCode ResponseMapping(GameGateway.RESPONSE_ERROR_CODE ResponseError)
    {
      ErrorCode _error_code;

      _error_code = ErrorCode.GeneralError;

      switch (ResponseError)
      {
        case GameGateway.RESPONSE_ERROR_CODE.OK:

          break;
        case GameGateway.RESPONSE_ERROR_CODE.NOT_ENOUGH_BALANCE:
          _error_code = ErrorCode.InsufficientFunds;
          break;
        case GameGateway.RESPONSE_ERROR_CODE.ACCOUNT_BLOCKED:
          _error_code = ErrorCode.AccountIsLocked;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_INVALID_NEGATIVE_AMOUNT:
          _error_code = ErrorCode.InvalidNegativeAmount;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_UNKNOWN_TRANSACTIONID:
          _error_code = ErrorCode.UnknownTransactionId;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_INVALID_GAME:
          _error_code = ErrorCode.InvalidGame;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_NO_SESSIONID:
          _error_code = ErrorCode.PlayerLimitExceededSessionLoss;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_ROUND_ALREADY_ENDED:
          _error_code = ErrorCode.RoundAlreadyEnded;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_TRANSACTION_ALREADY_CANCELLED:
          _error_code = ErrorCode.TransactionAlreadyCancelled;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_HAVE_OPENED_ROUNDS:
          _error_code = ErrorCode.PlayerHaveOpenedRounds;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_NO_ROUNDS_LEFT:
          _error_code = ErrorCode.NoRoundsLeft;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_OP_AUTHENTICATION:
        case GameGateway.RESPONSE_ERROR_CODE.ERR_WRONG_AUTH:
          _error_code = ErrorCode.AuthenticationFailed;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_TOKEN_EXPIRED:
          _error_code = ErrorCode.TokenExpired;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_TOKEN_INVALID:
          _error_code = ErrorCode.InvalidToken;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_ROUND_INVALID:
          _error_code = ErrorCode.InvalidRound;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_LIMIT_EXCEEDED_TURNOVER:
          _error_code = ErrorCode.PlayerLimitExceededTurnover;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_LIMIT_EXCEEDED_SESSION_TIME:
          _error_code = ErrorCode.PlayerLimitExceededSessionTime;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_PLAYER_LIMIT_EXCEEDED_SESSION_STAKE:
          _error_code = ErrorCode.PlayerLimitExceededSessionStake;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_INVALID_USER_ID:
          _error_code = ErrorCode.InvalidUserId;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERR_TRANSACTION_ALREADY_SETTLED:
          _error_code = ErrorCode.TransactionAlreadySettled;
          break;

        case GameGateway.RESPONSE_ERROR_CODE.ERROR:
        case GameGateway.RESPONSE_ERROR_CODE.ERR_OP_GENERAL:
        case GameGateway.RESPONSE_ERROR_CODE.ERR_GENERAL:
        default:
          _error_code = ErrorCode.GeneralError;
          break;
      }

      return _error_code;

    } // ResponseMapping

    #endregion

  }
}