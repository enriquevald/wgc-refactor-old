﻿using PariPlay_GameGateway.Common;
////------------------------------------------------------------------------------
//// Copyright © 2016 Win Systems International.
////------------------------------------------------------------------------------
//// 
////      MODULE NAME: DebitCredit.cs
//// 
////      DESCRIPTION: DebitCredit Class for Web Service
//// 
////           AUTHOR: Javi Barea
//// 
////    CREATION DATE: 07-SEP-2016
//// 
//// REVISION HISTORY:
//// 
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 07-SEP-2016 JBP    First release.
////------------------------------------------------------------------------------
using PariPlay_GameGateway.Models;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSI.Common;

namespace PariPlay_GameGateway.Classes
{
  public class DebitCredit : Base
  {

    #region " Constructor "

    /// <summary>
    /// DebitCredit constructor
    /// </summary>
    /// <param name="Controller"></param>
    public DebitCredit(ApiController Controller) : base(Controller) { ; }

    #endregion

    #region " Public Methods "

    /// <summary>
    /// Process DebitCredit Request balance 
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public HttpResponseMessage ProcessRequest(DebitCreditRequest Request)
    {
      ErrorCode _error_code;
      LCD_Session _session;
      DebitCreditResponse _response;

      try
      {
        // Start Sql transaction
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Request data validation and authentication
          if (!StartSession(Request, out _session, out _error_code, _db_trx.SqlTransaction))
          {
            return RequestErrorResponse(_error_code);
          }

          _response = DebitCreditInternal(Request, ref _session, _db_trx.SqlTransaction);

          if (_response == null)
          {
            return RequestErrorResponse(_session.ErrorCode, _session.ResponseBalance);
          }

          _db_trx.Commit();
        }

        // Return DebitCreditResponse JSON
        return RequestSuccessResponse(_response);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestErrorResponse();

    } // DebitCredit

    #endregion

    #region " Protected Methods "

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    protected Boolean StartSession(DebitCreditRequest Request, out LCD_Session Session, out ErrorCode ErrorCode, SqlTransaction SqlTrx)
    {
      Boolean _is_expierd;

      // Validation and authentication request
      if (!WigosAuthentication(Request, out Session, out ErrorCode, out _is_expierd))
      {
        return false;
      }

      // Set TransactionId data for debit/credit operation
      Session.TransactionId = Request.TransactionId;

      // Request data validation
      if (!RequestDataValidation(Request, Session, _is_expierd, out ErrorCode))
      {
        return false;
      }

      // Manage token
      if (!Session.PariPlay_TokenManager(Request.Token, SqlTrx))
      {
        ErrorCode = ResponseMapping(Session.ErrorCode);
        return false;
      }

      return true;

    } // StartSession

    #endregion

    #region "Private Methods"

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="IsExpired"></param>
    /// <param name="ErrorCode"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(DebitCreditRequest Request, LCD_Session Session, Boolean IsExpired, out ErrorCode ErrorCode)
    {
      Int64 _transaction_id;

      ErrorCode = ErrorCode.GeneralError;

      // Expierd Token 
      if (IsExpired)
      {
        ErrorCode = ErrorCode.TokenExpired;
        Log.Message(String.Format("DebitCredit.RequestDataValidation: Token '{0}' is expired.", Request.Token));

        return false;
      }

      // PlayerId 
      if (Request.PlayerId != Session.AccountId.ToString())
      {
        ErrorCode = ErrorCode.InvalidUserId;
        Log.Message("DebitCredit.RequestDataValidation: Invalid PlayerId.");

        return false;
      }

      // GameCode 
      if (Request.GameCode != Session.GameName)
      {
        ErrorCode = ErrorCode.InvalidGame;
        Log.Message("DebitCredit.RequestDataValidation: Invalid GameCode.");

        return false;
      }

      // DebitAmount 
      if (Request.DebitAmount <= 0)
      {
        ErrorCode = ErrorCode.InvalidNegativeAmount;
        Log.Message("DebitCredit.RequestDataValidation: DebitAmount must be greater than 0.");

        return false;
      }

      // CreditAmount 
      if (Request.CreditAmount <= 0)
      {
        ErrorCode = ErrorCode.InvalidNegativeAmount;
        Log.Message("DebitCredit.RequestDataValidation: CreditAmount must be greater than 0.");

        return false;
      }

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId)
        || !Int64.TryParse(Request.TransactionId, out _transaction_id))
      {
        ErrorCode = ErrorCode.UnknownTransactionId;
        Log.Message("DebitCredit.RequestDataValidation: TransactionId is not valid.");

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get DebitCredit internal response
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private DebitCreditResponse DebitCreditInternal(DebitCreditRequest Request, ref LCD_Session Session, SqlTransaction SqlTrx)
    {
      // Debit action
      if (!new Debit(Controller).ProcessRequest(Request.ToDebitRequest(), ref Session, SqlTrx))
      {
        return null;
      }

      // Credit action
      if (!new Credit(Controller).ProcessRequest(Request.ToCreditRequest(), ref Session, SqlTrx))
      {
        return null;
      }

      // End game action
      if (!new EndGame(this.Controller).ProcessRequest(Request.ToEndGameRequest(), ref Session, SqlTrx))
      {
        return null;
      }

      // Get DebitCredit response
      return GetResponse(Session);

    } // DebitCreditInternal

    /// <summary>
    /// Prepare DebitCredit response
    /// </summary>
    /// <param name="Session"></param>
    /// <returns></returns>
    private DebitCreditResponse GetResponse(LCD_Session Session)
    {
      // Check Session
      if (Session == null)
      {
        return null;
      }

      return new DebitCreditResponse()
      {
        Balance = Session.Balance.TotalRedeemable,
        TransactionId = Session.TransactionId,
        Timestamp = WGDB.Now.ToString("yyyy-MM-ddThh:mm:ssZ"),

        // Default Values
        BonusBalance = 0,
        ExtraData = "",
        RoundsLeft = 0
      };

    } // GetResponse

    #endregion

  }
}