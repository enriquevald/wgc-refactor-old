﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameGatewaySimulator.Common.Extensions
{
  public static class XmlDocumentExtensions
  {
    public static byte[] ToByteArray(this XmlDocument xmlDocument)
    {
      Encoding encoding = Encoding.UTF8;
      byte[] docAsBytes = encoding.GetBytes(xmlDocument.OuterXml);
      return docAsBytes;
    }
  }
}
