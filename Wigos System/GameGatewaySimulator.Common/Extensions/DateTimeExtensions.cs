﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Common.Extensions
{
  public static class DateTimeExtensions
  {
    public static long ToEpoch(this DateTime date)
    {
      return (long)(date.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }
  }
}
