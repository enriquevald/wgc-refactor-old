﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Common.Logging
{
  public static class AssemblyLogging
  {
    public static void LogLoadedAssemblies(ILog logger, string filterByName = null)
    {
      AppDomain.CurrentDomain.GetAssemblies()
                             .Where(x => filterByName == null || x.FullName.Contains(filterByName))
                             .ToList()
                             .ForEach(x =>
                             {
                               var fileVersion = FileVersionInfo.GetVersionInfo(x.Location);

                               logger.InfoFormat(string.Format(" => {0} - V{1}", fileVersion.InternalName,
                                   fileVersion.FileVersion));
                             });
    }
  }
}
