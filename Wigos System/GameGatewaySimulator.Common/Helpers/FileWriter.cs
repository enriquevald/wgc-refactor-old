﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Common.Helpers
{
  public class FileWriter : IDisposable
  {
    private bool IsDisposed { get; set; }

    private static FileWriter instance;
    public static FileWriter Instance
    {
      get
      {
        if (instance == null || instance.IsDisposed)
        { 
          instance = new FileWriter();
          instance.IsDisposed = false;
        }

        return instance;
      }
    }

    private ReaderWriterLockSlim readerWriterLock;
    private ReaderWriterLockSlim ReaderWriterLock
    {
      get
      {
        if (readerWriterLock == null)
          readerWriterLock = new ReaderWriterLockSlim();

        return readerWriterLock;
      }
    }

    public void ThreadSafeWrite(string text, string fileName, bool addNewLine)
    {
      ReaderWriterLock.EnterWriteLock();
      try
      {
        if (addNewLine)
          text += Environment.NewLine;

        File.AppendAllText(fileName, text);
      }
      finally
      {
        ReaderWriterLock.ExitWriteLock();
      }
    }

    public void Dispose()
    {
      Dispose(true);
      // This object will be cleaned up by the Dispose method.
      // Therefore, you should call GC.SupressFinalize to
      // take this object off the finalization queue
      // and prevent finalization code for this object
      // from executing a second time.
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
      // Check to see if Dispose has already been called.
      if (!this.IsDisposed)
      {
        // If disposing equals true, dispose all managed
        // and unmanaged resources.
        if (disposing)
        {
          // Dispose managed resources.
          if (readerWriterLock != null)
            readerWriterLock.Dispose();
        }

        // Call the appropriate methods to clean up
        // unmanaged resources here.
        // If disposing is false,
        // only the following code is executed.

        // Note disposing has been done.
        IsDisposed = true;
      }
    }
  }
}
