﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGatewaySimulator.Common.Helpers
{
  public static class RandomGenerator
  {
    private static Random randomInstance;
    private static Random RandomInstance
    {
      get
      {
        if (randomInstance == null)
          randomInstance = new Random();

        return randomInstance;
      }
    }

    public static int GetRandom(int min, int max)
    {
      return RandomInstance.Next(min, max + 1);
    }
  }
}
