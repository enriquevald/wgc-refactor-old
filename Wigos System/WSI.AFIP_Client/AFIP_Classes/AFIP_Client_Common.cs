﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client_Common.cs
// 
//   DESCRIPTION: Common Class for afip project
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 06-Jun-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-Jun-2016 FOS    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.AFIP_Client.AFIP_Classes
{
  #region "Enums"
  public enum ENUM_METER_CODE
  {
    COIN_IN = 0,        //Coin In
    COIN_OUT = 1,       //Coin Out
    JACKPOTS = 2,       //Jackpots
    GAMES_PLAYED = 5    //Games Played
  }

  public enum ENUM_AFIP_TERMINAL_STATE
  {
    ACTIVE = 1,
    RETIRED = 2,
    WITHOUT_METERS = 3
  }
  #endregion

  public class AFIP_Client_Common
  {
    #region "Constants GamingTables"
    internal const string GAMING_TABLE_TYPE = "ID_TABLE_TYPE";
    internal const string OPENING_CASH = "OPENING_CASH";
    internal const string CLOSING_CASH = "CLOSING_CASH";
    internal const string OPENING_CHIPS = "OPENING_CHIPS";
    internal const string CLOSING_CHIPS = "CLOSING_CHIPS";
    internal const string CASH_IN_TOTAL = "CASH_IN_TOTAL";
    internal const string CASH_OUT_TOTAL = "CASH_OUT_TOTAL";
    internal const string CHIPS_IN_TOTAL = "CHIPS_IN_TOTAL";
    internal const string CHIPS_OUT_TOTAL = "CHIPS_OUT_TOTAL";
    internal const string CHIP_SALES_TOTAL = "CHIP_SALES_TOTAL";
    internal const string CHIPS_BUY_TOTAL = "CHIPS_BUY_TOTAL";
    internal const string GAMING_TABLE_QUANTITY = "TABLE_COUNT";
    #endregion

    #region "Constants TerminalMeter"
    public const Int32 FIRST_SEQUENCE_NUMBER = 1;

    //Field from DB
    public const String TERMINAL_ID = "TERMINAL_ID";
    public const String REGISTRATION_CODE = "REGISTRATION_CODE";
    public const String SAS_ACCOUNTING_DENOM = "SAS_ACCOUNTING_DENOM";
    public const String METER_DATE = "METER_DATE";
    public const String METER_CODE = "METER_CODE";
    public const String METER_TYPE = "METER_TYPE";
    public const String INI_VALUE = "INI_VALUE";
    public const String FIN_VALUE = "FIN_VALUE";
    public const String GROUP_ID = "GROUP_ID";
    public const String METER_ORIGIN = "METER_ORIGIN";
    public const String METER_MAX_VALUE = "METER_MAX_VALUE";
    public const String INTERVAL_FROM = "INTERVAL_FROM";
    public const String INTERVAL_TO = "INTERVAL_TO";

    //Fields to send results for the request
    public const String RESULT_REQUEST_ID = "REQUEST_ID";
    public const String RESULT_AFIP_STATE = "AFIP_STATE";

    //Fields to send results for the terminalmeter
    public const String RESULT_SESSION_DATE = "EVENT_DATETIME";
    public const String RESULT_VERSION = "VERSION";

    //Common fields to send results (for request & terminalmeter)
    public const String RESULT_TERMINAL_ID = "TERMINAL_ID";
    public const String RESULT_TERMINAL_REGISTRATION_CODE = "TERMINAL_REGISTRATION_CODE";
    public const String RESULT_TERMINAL_ACCOUNTING_DENOM = "TERMINAL_DENOMINATION";
    public const String RESULT_SEQUENCE_NUMBER = "SEQUENCE_NUMBER";

    public const String RESULT_DATE_START = "SEQUENCE_DATE_START";
    public const String RESULT_GAMES_PLAYED_START = "GAMES_PLAYED_START";
    public const String RESULT_COIN_IN_START = "COIN_IN_START";
    public const String RESULT_COIN_OUT_START = "COIN_OUT_START";
    public const String RESULT_JACKPOTS_START = "JACKPOTS_START";

    public const String RESULT_DATE_END = "SEQUENCE_DATE_END";
    public const String RESULT_GAMES_PLAYED_END = "GAMES_PLAYED_END";
    public const String RESULT_COIN_IN_END = "COIN_IN_END";
    public const String RESULT_COIN_OUT_END = "COIN_OUT_END";
    public const String RESULT_JACKPOTS_END = "JACKPOTS_END";

    public const String RESULT_INTERVAL_FROM = "INTERVAL_FROM";
    public const String RESULT_INTERVAL_TO = "INTERVAL_TO";
    #endregion

    #region "Public Methods"
    /// <summary>
    /// Create the datatables to return the results
    /// </summary>
    /// <param name="HasToCreateRequestTable">Indicates if it needs to create the request special fields</param>
    /// <returns></returns>
    public DataTable CreateResultDatatable(Boolean HasToCreateRequestTable)
    {
      DataTable _dt_request;

      _dt_request = new DataTable();

      if (HasToCreateRequestTable)
      {
        _dt_request.Columns.Add(AFIP_Client_Common.RESULT_REQUEST_ID, Type.GetType("System.Int32")).AllowDBNull = false;
        _dt_request.Columns.Add(AFIP_Client_Common.RESULT_AFIP_STATE, Type.GetType("System.Int32")).AllowDBNull = false;
      }
      else
      {
        _dt_request.Columns.Add(AFIP_Client_Common.RESULT_SESSION_DATE, Type.GetType("System.DateTime")).AllowDBNull = false;
        _dt_request.Columns.Add(AFIP_Client_Common.RESULT_VERSION, Type.GetType("System.Int32")).AllowDBNull = false;
      }

      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_TERMINAL_ID, Type.GetType("System.String")).AllowDBNull = false;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE, Type.GetType("System.String")).AllowDBNull = false;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM, Type.GetType("System.Decimal")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_SEQUENCE_NUMBER, Type.GetType("System.Int32")).AllowDBNull = false;

      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_DATE_START, Type.GetType("System.DateTime")).AllowDBNull = false;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_GAMES_PLAYED_START, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_COIN_IN_START, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_COIN_OUT_START, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_JACKPOTS_START, Type.GetType("System.Int64")).AllowDBNull = true;

      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_DATE_END, Type.GetType("System.DateTime")).AllowDBNull = false;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_GAMES_PLAYED_END, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_COIN_IN_END, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_COIN_OUT_END, Type.GetType("System.Int64")).AllowDBNull = true;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_JACKPOTS_END, Type.GetType("System.Int64")).AllowDBNull = true;

      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_INTERVAL_TO, Type.GetType("System.DateTime")).AllowDBNull = false;
      _dt_request.Columns.Add(AFIP_Client_Common.RESULT_INTERVAL_FROM, Type.GetType("System.DateTime")).AllowDBNull = false;

      return _dt_request;
    }
    #endregion
  }
}
