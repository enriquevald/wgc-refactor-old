﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Provider_TerminalMeter_DB.cs
// 
//   DESCRIPTION: Provider that returns Terminal Meter's DB data for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 12-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAY-2016 FGB    First release 
// 18-ABR-2018 JML    Bug 32378:WIGOS-10107 AFIP - It don't send the record of the reset event correctly.
// 18-ABR-2018 JML    Bug 32379:WIGOS-10108 AFIP - It don't send the record of the rollover event correctly.
// 18-ABR-2018 JML    Bug 32380:WIGOS-10109 AFIP - It don't send the record of the denom. change event correctly.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_TerminalMeter_DB
  {
    #region "Public Methods"
    /// <summary>
    /// Returns the terminal meters for a date
    /// </summary>
    /// <param name="StartDate">Start date to send to the SP</param>
    /// <param name="EndDate">End date to send to the SP</param>
    /// <param name="RequestTerminals">Filter for RequestTerminals</param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    public Boolean GetTerminalsMeters(DateTime StartDate, DateTime EndDate, String RequestTerminals, out DataTable DtTerminalDetail)
    {
      DtTerminalDetail = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SP_AFIP_GetTerminalMetersHistory", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;

            _sql_cmd.Parameters.Add("@pStartDateTime", SqlDbType.DateTime).Value = StartDate;
            _sql_cmd.Parameters.Add("@pEndDateTime", SqlDbType.DateTime).Value = EndDate;
            _sql_cmd.Parameters.Add("@pPendingType", SqlDbType.Int).Value = (Int32)AFIP_Common.PendingType.Terminals;
            _sql_cmd.Parameters.Add("@pTerminalsAFIP", SqlDbType.NVarChar).Value = RequestTerminals;
            _sql_cmd.Parameters.Add("@pTerminalStatusActive", SqlDbType.Int).Value = (Int32)TerminalStatus.ACTIVE;
            _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.NVarChar).Value = Misc.GamingTerminalTypeListToString();

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              DtTerminalDetail = new DataTable();
              _sql_da.Fill(DtTerminalDetail);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Terminals: Error in AFIP_Provider_TerminalMeter_DB.GetTerminalsMeters: " + _ex.Message);
      }

      return false;
    }
    #endregion
  }
}
