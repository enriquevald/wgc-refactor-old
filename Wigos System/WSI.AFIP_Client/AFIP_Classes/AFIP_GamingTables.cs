﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_GamingTables.cs
// 
//   DESCRIPTION: Class to process all from gamingtables
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 12-May-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAY-2016 FOS    First release 
// 23-JAN-2017 FGB    Bug 23207: AFIP_Client: Envio de jornadas desde Wigos no coincide con lo solicitado por Jaza Local y se muestra la jornada anterior.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_GamingTables
  {
    public DataTable m_dt_pending_gaming_tables;
    private Boolean m_enable_message_trace;

    #region "Constructor"
    public AFIP_GamingTables()
      : this(null, AFIP_Common.AFIPEnableMessageTrace)
    {
    }

    public AFIP_GamingTables(DataTable DtPendingGamingTables, Boolean EnableMessageTrace)
    {
      m_dt_pending_gaming_tables = DtPendingGamingTables;
      m_enable_message_trace = EnableMessageTrace;
    }
    #endregion

    /// <summary>
    /// Function to return gamingtablesDetail list
    /// </summary>
    /// <param name="StartDate">Date to search the data</param>
    /// <param name="Version">Value from JazaLocal</param>
    /// <param name="GamingTablesDetailList">List of gamingtabledetails</param>
    /// <returns></returns>
    public Boolean GetGamingTablesDetails(DateTime StartDate, Int32 Version, out AFIP_WS_JAZALocal.DETALLE_MESAS[] GamingTablesDetailList)
    {
      AFIP_WS_JAZALocal.DETALLE_MESAS _gaming_tables_detail;
      List<AFIP_WS_JAZALocal.DETALLE_MESAS> _gaming_tables_detail_list;
      Decimal _calculated_benefit;
      Decimal _real_benefit;
      AFIP_GamingTables_DB AFIP_gamingTables_db;

      GamingTablesDetailList = null;

      if (StartDate == null)
      {
        Log.Error("Requested date is empty");

        return false;
      }

      if (Version <= 0)
      {
        Log.Error("Requested version is empty");

        return false;
      }

      _gaming_tables_detail_list = new List<AFIP_WS_JAZALocal.DETALLE_MESAS>();

      try
      {
        if (m_dt_pending_gaming_tables == null)
        {
          m_dt_pending_gaming_tables = new DataTable();
          AFIP_gamingTables_db = new AFIP_GamingTables_DB();
          m_dt_pending_gaming_tables = AFIP_gamingTables_db.GamingTables_CurrentDate(StartDate);
        }

        foreach (DataRow _row in m_dt_pending_gaming_tables.Rows)
        {
          _gaming_tables_detail = new AFIP_WS_JAZALocal.DETALLE_MESAS();

          _gaming_tables_detail.fechaJornada = StartDate;
          _gaming_tables_detail.Version = Version;
          _gaming_tables_detail.TipoMesa = _row[AFIP_Client_Common.GAMING_TABLE_TYPE].ToString();
          _gaming_tables_detail.CantidadMesas = (Int32)_row[AFIP_Client_Common.GAMING_TABLE_QUANTITY];
          _gaming_tables_detail.EfectivoApertura = (Decimal)_row[AFIP_Client_Common.OPENING_CASH];
          _gaming_tables_detail.EfectivoCierre = (Decimal)_row[AFIP_Client_Common.CLOSING_CASH];
          _gaming_tables_detail.ImporteEqFichasApertura = (Decimal)_row[AFIP_Client_Common.OPENING_CHIPS];
          _gaming_tables_detail.ImporteEqFichasCierre = (Decimal)_row[AFIP_Client_Common.CLOSING_CHIPS];
          _gaming_tables_detail.TotalRetiros = (Decimal)_row[AFIP_Client_Common.CASH_OUT_TOTAL];
          _gaming_tables_detail.TotalReposiciones = (Decimal)_row[AFIP_Client_Common.CASH_IN_TOTAL];
          _gaming_tables_detail.TotalReposicionesEqFichas = (Decimal)_row[AFIP_Client_Common.CHIPS_IN_TOTAL];
          _gaming_tables_detail.TotalVentas = (Decimal)_row[AFIP_Client_Common.CHIP_SALES_TOTAL];
          _gaming_tables_detail.TotalPagos = (Decimal)_row[AFIP_Client_Common.CHIPS_BUY_TOTAL];
          _gaming_tables_detail.TotalRetirosEqFichas = (Decimal)_row[AFIP_Client_Common.CHIPS_OUT_TOTAL];

          _calculated_benefit = (Decimal)_row[AFIP_Client_Common.OPENING_CASH] + (Decimal)_row[AFIP_Client_Common.CASH_IN_TOTAL] - (Decimal)_row[AFIP_Client_Common.CASH_OUT_TOTAL];
          _real_benefit = (Decimal)_row[AFIP_Client_Common.CLOSING_CASH];

          _gaming_tables_detail.DiferenciaCaja = (_calculated_benefit - _real_benefit);

          _gaming_tables_detail.TotalEqTicketsFondPromOtorg = 1;
          _gaming_tables_detail.TotalEqTicketsFondPromRecup = 1;
          _gaming_tables_detail_list.Add(_gaming_tables_detail);
        }

        //Log to XML
        if ((m_enable_message_trace) && (_gaming_tables_detail_list.Count > 0))
        {
          LogToXML(StartDate, Version, _gaming_tables_detail_list);
        }

        GamingTablesDetailList = _gaming_tables_detail_list.ToArray();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("GamingTables: Error at GetGamingTablesDetails: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Log to XML
    /// </summary>
    /// <param name="StartDate"></param>
    /// <param name="Version"></param>
    /// <param name="GamingTablesDetailList"></param>
    /// <returns></returns>
    private Boolean LogToXML(DateTime StartDate, Int32 Version, List<AFIP_WS_JAZALocal.DETALLE_MESAS> GamingTablesDetailList)
    {
      String _tables_XML;

      try
      {
        Log.Message(String.Format("GamingTables: START: elements to be sent - Date: {0} - Version: {1}", StartDate.ToString("yyyy-MM-dd"), Version));

        _tables_XML = XMLList.ListToXML(GamingTablesDetailList);
        Log.Message(_tables_XML);

        Log.Message(String.Format("GamingTables: END: elements to be sent - Date: {0} - Version: {1}", StartDate.ToString("yyyy-MM-dd"), Version));

        return false;
      }
      catch (Exception _ex)
      {
        Log.Message(String.Format("GamingTables: Error in AFIP_GamingTables.LogToXML - Date: {0} - Version: {1} - {2}", StartDate.ToString("yyyy-MM-dd"), Version, _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Returns list with dummy item
    /// </summary>
    /// <param name="StartDate"></param>
    /// <param name="Version"></param>
    /// <param name="GamingTablesDetailList"></param>
    /// <returns></returns>
    public Boolean GetDefaultValues(DateTime StartDate, Int32 Version, out AFIP_WS_JAZALocal.DETALLE_MESAS[] GamingTablesDetailList)
    {
      AFIP_WS_JAZALocal.DETALLE_MESAS _gaming_tables_detail;
      List<AFIP_WS_JAZALocal.DETALLE_MESAS> _gaming_tables_detail_list;
      DateTime _start_date;

      _gaming_tables_detail = new AFIP_WS_JAZALocal.DETALLE_MESAS();

      GamingTablesDetailList = null;

      if (!AFIP_Common.GetSessionStartTime(StartDate, out _start_date))
      {
        Log.Error("GamingTables: Error getting start session date in GetDefaultValues");
        return false;
      }

      _gaming_tables_detail_list = new List<AFIP_WS_JAZALocal.DETALLE_MESAS>();

      _gaming_tables_detail.fechaJornada = _start_date;
      _gaming_tables_detail.Version = Version;
      _gaming_tables_detail.TipoMesa = "99";
      _gaming_tables_detail.CantidadMesas = 0;
      _gaming_tables_detail.EfectivoApertura = 0;
      _gaming_tables_detail.EfectivoCierre = 0;
      _gaming_tables_detail.ImporteEqFichasApertura = 0;
      _gaming_tables_detail.ImporteEqFichasCierre = 0;
      _gaming_tables_detail.TotalRetiros = 0;
      _gaming_tables_detail.TotalReposiciones = 0;
      _gaming_tables_detail.TotalReposicionesEqFichas = 0;
      _gaming_tables_detail.TotalVentas = 0;
      _gaming_tables_detail.TotalPagos = 0;
      _gaming_tables_detail.DiferenciaCaja = 0;
      _gaming_tables_detail.TotalEqTicketsFondPromOtorg = 0;
      _gaming_tables_detail.TotalEqTicketsFondPromRecup = 0;
      _gaming_tables_detail.TotalRetirosEqFichas = 0;
      _gaming_tables_detail_list.Add(_gaming_tables_detail);

      //Log to XML
      if ((m_enable_message_trace) && (_gaming_tables_detail_list.Count > 0))
      {
        LogToXML(StartDate, Version, _gaming_tables_detail_list);
      }

      GamingTablesDetailList = _gaming_tables_detail_list.ToArray();

      return true;
    }
  }
}
