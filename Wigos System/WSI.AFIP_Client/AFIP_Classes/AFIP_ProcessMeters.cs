﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_ProcessMeters.cs
// 
//   DESCRIPTION: Provider that returns Requests's DB data for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 17-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2016 FGB    First release 
// 18-ABR-2018 JML    Bug 32378:WIGOS-10107 AFIP - It don't send the record of the reset event correctly.
// 18-ABR-2018 JML    Bug 32379:WIGOS-10108 AFIP - It don't send the record of the rollover event correctly.
// 18-ABR-2018 JML    Bug 32380:WIGOS-10109 AFIP - It don't send the record of the denom. change event correctly.
// 07-MAY-2018 JML    Fixed Bug 32575:WIGOS-10720 AFIPClient: The last known meters before connection & disconnection are not reported correctly to Jaza Local. 
// 06-JUN-2018 JML    Fixed Bug 32902:WIGOS-12599 AFIP Client: Error sending meters with rollover when the accounting denomination is different to 0,01.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using WSI.Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  //Object for processing settings
  public class AFIP_ProcessMeters_Settings
  {
    public readonly Boolean IsRequest;
    public readonly DateTime? RequestDate;
    public readonly Int32 RequestVersion;
    public readonly DateTime? RequestTime;
    public readonly Int32 RequestId;
    public readonly DateTime StartDate; //Start Date of the interval asked to the SP
    public readonly DateTime EndDate;   //End Date of the interval asked to the SP

    public AFIP_ProcessMeters_Settings(Boolean IsRequest, DateTime? RequestDate, Int32 RequestVersion, DateTime? RequestTime, Int32 RequestId, DateTime StartDate, DateTime EndDate)
    {
      this.IsRequest = IsRequest;
      this.RequestDate = RequestDate;
      this.RequestVersion = RequestVersion;
      this.RequestTime = RequestTime;
      this.RequestId = RequestId;
      this.StartDate = StartDate;
      this.EndDate = EndDate;
    }
  }

  //Internal object for transferring data for common fields
  internal class AFIP_CommonFields
  {
    public String TerminalId;
    public String RegistrationCode;
    public Decimal SASDenomination;
    public Int16 SequenceNumber;
    public DateTime MeterDate;
    public DateTime IntervalFrom;
    public DateTime IntervalTo;
  }

  public class AFIP_ProcessMeters
  {
    #region "Constants"
    private const Decimal DEFAULT_SAS_ACCOUNTING_DENOM = 0.01M;
    private const Decimal CENTS_CONVERSION = 100M;  //In TSMH the meters values are in cents
    #endregion

    #region "Members"
    private AFIP_ProcessMeters_Settings m_settings;
    #endregion

    #region "Constructor"
    public AFIP_ProcessMeters(AFIP_ProcessMeters_Settings Process_Settings)
    {
      m_settings = Process_Settings;
    }
    #endregion

    #region "Public Methods"

    /// <summary>
    /// Process the meters in DtMetersDate generating processed data (sequences) in DtResultMeters
    /// </summary>
    /// <param name="DtMetersDate"></param>
    /// <param name="DtResultMeters"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    public Boolean ProcessMeters(DataTable DtMetersDate, out DataTable DtResultMeters, out String ErrorMessage)
    {
      DataTable DtProcessedMeters;

      //Process meters sequences
      if (!ProcessMetersSequences(DtMetersDate, out DtProcessedMeters, out ErrorMessage))
      {
        DtResultMeters = null;

        return false;
      }

      DtResultMeters = DtProcessedMeters;

      return true;
    }

    #endregion

    #region "Private Methods"
    /// <summary>
    /// Process the meters in DtMetersDate generating processed data (sequences) in DtResultMeters
    /// </summary>
    /// <param name="DtMetersDate"></param>
    /// <param name="DtResultMeters"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean ProcessMetersSequences(DataTable DtMetersDate, out DataTable DtResultMeters, out String ErrorMessage)
    {
      DataRow[] _rows_group_filter;
      DataRow _row_result;
      Int16 _sequence_number;
      Decimal _sas_accounting_denom;
      AFIP_CommonFields _afip_commonfields;
      ENUM_SAS_METER_HISTORY _last_meter_type;
      ENUM_SAS_METER_HISTORY _actual_meter_type;
      Int64 _initial_value;
      Int64 _final_value;
      ENUM_METER_CODE _meter_code;
      Int32 _mark_for_discard_group;
      DataRow _last_hourly_row_result;

      ErrorMessage = String.Empty;

      DtResultMeters = new AFIP_Client_Common().CreateResultDatatable(m_settings.IsRequest);
      _last_meter_type = ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
      _actual_meter_type = ENUM_SAS_METER_HISTORY.TSMH_HOURLY;

      _mark_for_discard_group = 0;
      _last_hourly_row_result = DtResultMeters.NewRow();

      try
      {
        _sequence_number = AFIP_Client_Common.FIRST_SEQUENCE_NUMBER;
        _row_result = null;

        foreach (DataRow _row_meter in DtMetersDate.Rows)
        {
          _actual_meter_type = (ENUM_SAS_METER_HISTORY)_row_meter[AFIP_Client_Common.METER_TYPE];
          _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];

          // Discard 4 meters (one group) after TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION
          //       It's necessary because sometimes then next group is hourly instead of a TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION
          if (_actual_meter_type == ENUM_SAS_METER_HISTORY.TSMH_HOURLY && _mark_for_discard_group > 0
              && (_row_result != null)
              && (_row_meter[AFIP_Client_Common.TERMINAL_ID].ToString().Equals(_row_result[AFIP_Client_Common.RESULT_TERMINAL_ID].ToString())))  //We change the Terminal
          {
            if (_meter_code == ENUM_METER_CODE.COIN_IN)
            {
              _mark_for_discard_group -= 1;
            }

            if (_mark_for_discard_group > 0)
            {
              continue;
            }
          }
          _mark_for_discard_group = 0;
          if (_actual_meter_type == ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION)
          {
            _mark_for_discard_group = 2;
          }


          //Is there a Terminal change or is the first record
          if ((_row_result == null)
                || (!_row_meter[AFIP_Client_Common.TERMINAL_ID].ToString().Equals(_row_result[AFIP_Client_Common.RESULT_TERMINAL_ID].ToString()))  //We change the Terminal
                || (_actual_meter_type != ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS && _last_meter_type == ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS)
                || (_actual_meter_type != ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION && _last_meter_type == ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION))
          {
            if (_row_result != null) //It is not the first record
            {
              DtResultMeters.Rows.Add(_row_result); //Save previous record
            }

            //New record
            if (_row_result != null
               && ((_row_meter[AFIP_Client_Common.TERMINAL_ID].ToString().Equals(_row_result[AFIP_Client_Common.RESULT_TERMINAL_ID].ToString()))
                   && ((_actual_meter_type != ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS && _last_meter_type == ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS)
                      || (_actual_meter_type != ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION && _last_meter_type == ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION)
                      )
                  )
               )
            {
              _sequence_number++;
            }
            else
            {
              _sequence_number = AFIP_Client_Common.FIRST_SEQUENCE_NUMBER;
            }
            _row_result = DtResultMeters.NewRow();

            //Initialize common fields values
            _afip_commonfields = InitializeCommonFieldsValues(_row_meter, _sequence_number);

            SetMeterCommonFields(_row_result, _afip_commonfields);
          }

          _initial_value = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
          _final_value = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];

          switch ((ENUM_SAS_METER_HISTORY)_row_meter[AFIP_Client_Common.METER_TYPE])
          {
            // First Time
            case ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME:
              _sas_accounting_denom = GetMeterSASAccountingDenomination(_row_meter);
              SetMeterStartForFirstRegister(_row_result, _meter_code, _initial_value, _sas_accounting_denom);
              _row_result[AFIP_Client_Common.RESULT_DATE_START] = _row_meter[AFIP_Client_Common.METER_DATE];

              break;

            //Hourly
            case ENUM_SAS_METER_HISTORY.TSMH_HOURLY:
            case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION:
            case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION:

              //  METER_INI_VALUE       METER_FIN_VALUE
              //              758                   895
              _sas_accounting_denom = GetMeterSASAccountingDenomination(_row_meter);
              SetMeterHourly(_row_result, _meter_code, _initial_value, _final_value, _sas_accounting_denom);
              _row_result[AFIP_Client_Common.RESULT_DATE_END] = _row_meter[AFIP_Client_Common.METER_DATE];

              break;

            // Pre-event meters for reset or denomination change
            case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_LAST_KNOWN_METERS:
              _sas_accounting_denom = GetMeterSASAccountingDenomination(_row_meter);
              SetMeterHourly(_row_result, _meter_code, _initial_value, _final_value, _sas_accounting_denom);
              _row_result[AFIP_Client_Common.RESULT_DATE_END] = _row_meter[AFIP_Client_Common.METER_DATE];

              break;

            //Service RAM Clear
            case ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR:
              //  METER_INI_VALUE       METER_FIN_VALUE
              //              758                     0
              //Steps:
              //1. Get the group records of the Service RAM Clear
              //2. Set previous record meter final values to the values before the Service RAM Clear
              //3. Set next record meter initial values to the values after the Service RAM Clear

              //1. Get the group records of the Service RAM Clear
              _rows_group_filter = GetGroupedByRowsFromMetersDatatable(DtMetersDate, _row_meter);

              //2. Set at previous record the meters final values to value before the Service RAM Clear
              SetMeterFinalsWithSequenceBeforeValuesForServiceRAMClear(_row_result, _rows_group_filter);
              _row_result[AFIP_Client_Common.RESULT_DATE_END] = _row_meter[AFIP_Client_Common.METER_DATE];

              //3. Set next record meter initial values to value after the Service RAM Clear
              SetMeterInitialsWithSequenceAfterValuesForServiceRAMClear(_row_result, _rows_group_filter);

              break;

            //Rollover
            case ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER:
              //  METER_INI_VALUE       METER_FIN_VALUE
              //              758                   621
              //Steps:
              //1. Get the group records of the Rollover
              //2. Set previous record's meters final values to after value (or max_value if rollover)
              //3. Save previous record
              //4. Add Rollover
              //5. Set next record meter initial values to the values after the Rollover

              //1. Get the group records of the Rollover
              _last_hourly_row_result.ItemArray = _row_result.ItemArray.Clone() as object[];

              _rows_group_filter = GetGroupedByRowsFromMetersDatatable(DtMetersDate, _row_meter);

              //2. Set previous record's meters final values to after value (or max_value if rollover)
              SetMeterFinalsWithSequenceAfterValuesForRollover(_row_result, _rows_group_filter);
              _row_result[AFIP_Client_Common.RESULT_DATE_END] = _row_meter[AFIP_Client_Common.METER_DATE];

              //3. Save previous record
              DtResultMeters.Rows.Add(_row_result);

              //4. Add Rollover
              _sequence_number++;
              _row_result = DtResultMeters.NewRow();

              //Initialize common fields values
              _afip_commonfields = InitializeCommonFieldsValues(_row_meter, _sequence_number);

              //Set common fields
              SetMeterCommonFields(_row_result, _afip_commonfields);

              //5. Set next record meter initial values to value after the Rollover
              SetMeterInitialsWithSequenceAfterValuesForRollover(_row_result, _last_hourly_row_result, _rows_group_filter);

              _last_hourly_row_result = DtResultMeters.NewRow();

              break;

            //SAS Account Denom Change
            case ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE:
              //  METER_INI_VALUE       METER_FIN_VALUE
              //              758                   895
              //Steps:
              //1. Get the group records of the SAS Account Denom Change
              //2. Set previous record meter final values to the values before the SAS Account Denom Change
              //3. Set next record meter initial values to the values after the SAS Account Denom Change

              //1. Get the group records of the Denom Change
              _rows_group_filter = GetGroupedByRowsFromMetersDatatable(DtMetersDate, _row_meter);

              //2. Set at previous record the meters final values to value before the Denom Change
              SetMeterFinalsWithSequenceAfterValuesForSASAccountDenomChange(_row_result, _rows_group_filter);
              _row_result[AFIP_Client_Common.RESULT_DATE_END] = _row_meter[AFIP_Client_Common.METER_DATE];

              //5. Set next record meter initial values to value after the SAS Account Denom Change
              SetMeterInitialsWithSequenceAfterValuesForSASAccountDenomChange(_row_result, _rows_group_filter);

              break;

            default:
              break;
          }
          _last_meter_type = _actual_meter_type;
        }

        //Add the last record
        if (DtMetersDate.Rows.Count > 0)
        {
          if (!_row_result.IsNull(AFIP_Client_Common.RESULT_TERMINAL_ID))
          {
            DtResultMeters.Rows.Add(_row_result);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMessage = "Error in ProcessMetersSequences: " + _ex.Message;
      }

      return false;
    }

    /// <summary>
    /// Creates and initializes the common fields from RowResult
    /// </summary>
    /// <param name="RowMeter"></param>
    /// <param name="SequenceNumber"></param>
    /// <returns></returns>
    private AFIP_CommonFields InitializeCommonFieldsValues(DataRow RowMeter, Int16 SequenceNumber)
    {
      AFIP_CommonFields _afip_commonfields;
      Decimal _sas_accounting_denom;

      _sas_accounting_denom = GetMeterSASAccountingDenomination(RowMeter);

      _afip_commonfields = new AFIP_CommonFields();

      _afip_commonfields.TerminalId = RowMeter[AFIP_Client_Common.TERMINAL_ID].ToString();
      _afip_commonfields.RegistrationCode = RowMeter[AFIP_Client_Common.REGISTRATION_CODE].ToString();
      _afip_commonfields.SASDenomination = _sas_accounting_denom;
      _afip_commonfields.SequenceNumber = SequenceNumber;
      _afip_commonfields.MeterDate = (DateTime)RowMeter[AFIP_Client_Common.METER_DATE];
      _afip_commonfields.IntervalFrom = (DateTime)RowMeter[AFIP_Client_Common.INTERVAL_FROM];
      _afip_commonfields.IntervalTo = (DateTime)RowMeter[AFIP_Client_Common.INTERVAL_TO];

      return _afip_commonfields;
    }

    /// <summary>
    /// Returns the rows that pertain to the group
    /// </summary>
    /// <param name="DtMetersDate"></param>
    /// <param name="RowMeter"></param>
    /// <returns></returns>
    private DataRow[] GetGroupedByRowsFromMetersDatatable(DataTable DtMetersDate, DataRow RowMeter)
    {
      ENUM_SAS_METER_HISTORY _meter_type_group;
      ENUM_SAS_METER_HISTORY _meter_type;
      Int64 _group_id;

      _meter_type = (ENUM_SAS_METER_HISTORY)(Int32)RowMeter[AFIP_Client_Common.METER_TYPE];

      //Get the Group Id
      _group_id = GetGroupId(RowMeter);

      //Get the MeterType of the group records by the MeterType
      _meter_type_group = GetGroupTypeByMeterType(_meter_type);

      //Filter by GROUP_ID and METER_TYPE
      return DtMetersDate.Select(AFIP_Client_Common.GROUP_ID + " = " + _group_id.ToString()
                                  + " AND " + AFIP_Client_Common.METER_TYPE + " = " + ((Int32)_meter_type_group).ToString());
    }

    /// <summary>
    /// Get the Group Id
    /// </summary>
    /// <param name="RowMeter"></param>
    /// <returns></returns>
    private Int64 GetGroupId(DataRow RowMeter)
    {
      Int64 _group_id;

      _group_id = -1;
      if (!RowMeter.IsNull(AFIP_Client_Common.GROUP_ID))
      {
        _group_id = (Int64)RowMeter[AFIP_Client_Common.GROUP_ID];
      }

      return _group_id;
    }

    /// <summary>
    /// Returns the MeterType of the group records by the MeterType
    /// </summary>
    /// <param name="MeterType"></param>
    /// <returns></returns>
    private ENUM_SAS_METER_HISTORY GetGroupTypeByMeterType(ENUM_SAS_METER_HISTORY MeterType)
    {
      ENUM_SAS_METER_HISTORY _meter_type_group;
      switch (MeterType)
      {
        //Rollover
        case ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER:
          _meter_type_group = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_ROLLOVER;
          break;
        //Service RAM Clear
        case ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR:
          _meter_type_group = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR;
          break;
        //Denom Change
        case ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE:
          _meter_type_group = ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE;
          break;
        default:
          _meter_type_group = ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
          break;
      }

      return _meter_type_group;
    }

    /// <summary>
    /// Returns the SAS meter denomination
    /// </summary>
    /// <param name="RowMeter"></param>
    /// <returns></returns>
    private decimal GetMeterSASAccountingDenomination(DataRow RowMeter)
    {
      Decimal _sas_denom;
      _sas_denom = DEFAULT_SAS_ACCOUNTING_DENOM;
      if (!RowMeter.IsNull(AFIP_Client_Common.SAS_ACCOUNTING_DENOM) && (Decimal)RowMeter[AFIP_Client_Common.SAS_ACCOUNTING_DENOM] != 0)
      {
        _sas_denom = (Decimal)RowMeter[AFIP_Client_Common.SAS_ACCOUNTING_DENOM];
      }

      return _sas_denom;
    }

    /// <summary>
    /// Set the values of meter common fields
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="CommonFields"></param>
    private void SetMeterCommonFields(DataRow RowResult, AFIP_CommonFields CommonFields)
    {
      if (m_settings.IsRequest)
      {
        RowResult[AFIP_Client_Common.RESULT_REQUEST_ID] = m_settings.RequestId;
        RowResult[AFIP_Client_Common.RESULT_AFIP_STATE] = ENUM_AFIP_TERMINAL_STATE.ACTIVE; //TODO: FGB: Check if correct
      }
      else
      {
        RowResult[AFIP_Client_Common.RESULT_SESSION_DATE] = m_settings.RequestDate;
        RowResult[AFIP_Client_Common.RESULT_VERSION] = m_settings.RequestVersion;
      }

      RowResult[AFIP_Client_Common.RESULT_TERMINAL_ID] = CommonFields.TerminalId;
      RowResult[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = CommonFields.RegistrationCode;
      RowResult[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = CommonFields.SASDenomination;
      RowResult[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = CommonFields.SequenceNumber;

      RowResult[AFIP_Client_Common.RESULT_DATE_START] = CommonFields.MeterDate;
      RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_COIN_IN_START] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_COIN_OUT_START] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_JACKPOTS_START] = DBNull.Value;

      RowResult[AFIP_Client_Common.RESULT_DATE_END] = CommonFields.MeterDate;
      RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_COIN_IN_END] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_COIN_OUT_END] = DBNull.Value;
      RowResult[AFIP_Client_Common.RESULT_JACKPOTS_END] = DBNull.Value;

      RowResult[AFIP_Client_Common.RESULT_INTERVAL_FROM] = CommonFields.IntervalFrom;
      RowResult[AFIP_Client_Common.RESULT_INTERVAL_TO] = CommonFields.IntervalTo;
    }

    /// <summary>
    /// Set the meter Initial Values to 0 and the Final Values with the sequence after values if rollover in the meter
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array of records with the sequence values of the Service Rollover
    private void SetMeterInitialsWithSequenceAfterValuesForRollover(DataRow RowResult, DataRow LastHourlyRowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
        _value_end = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];
        _sas_denom = GetMeterSASAccountingDenomination(_row_meter);

        //Is a RollOver
        if (_value_end < _value_start)
        {
          _value_start = 0; //Set the start to 0
        }
        else
        {
          _value_start = _value_end; //The start value after a RollOver has to be the last known value
        }

        SetMeterStartAndEnd(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }

      RowResult[AFIP_Client_Common.RESULT_COIN_IN_END] = LastHourlyRowResult[AFIP_Client_Common.RESULT_COIN_IN_END];
      RowResult[AFIP_Client_Common.RESULT_COIN_OUT_END] = LastHourlyRowResult[AFIP_Client_Common.RESULT_COIN_OUT_END];
      RowResult[AFIP_Client_Common.RESULT_JACKPOTS_END] = LastHourlyRowResult[AFIP_Client_Common.RESULT_JACKPOTS_END];
      RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = LastHourlyRowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_END];

    }

    /// <summary>
    /// Set the meter Initial Values before and the Final Values with the sequence after values of SAS Account Denom Change
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array od records with the sequence values of the SAS Account Denom Change
    private void SetMeterInitialsWithSequenceAfterValuesForSASAccountDenomChange(DataRow RowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
        _value_end = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];
        _sas_denom = GetMeterSASAccountingDenomination(_row_meter);

        SetMeterStartAndEnd(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }
    }

    /// <summary>
    /// Set the meter Initial Values to 0 and the Final Values with the sequence after values for Service RAM Clear
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array od records with the sequence values of the Service RAM Clear
    private void SetMeterInitialsWithSequenceAfterValuesForServiceRAMClear(DataRow RowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = 0;
        _value_end = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];
        _sas_denom = GetMeterSASAccountingDenomination(_row_meter);

        SetMeterStartAndEnd(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }
    }

    /// <summary>
    /// Set the meter Final Values with the sequence after values for Rollover
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array od records with the sequence values</param>
    private void SetMeterFinalsWithSequenceAfterValuesForRollover(DataRow RowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Int64? _max_value_from_tsmh;
      Int64? _max_value; // credits or units
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
        _value_end = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];
        _sas_denom = (Decimal)RowResult[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM];

        //Is a RollOver
        if (_value_end < _value_start)
        {
          //Meter has max value
          if (!_row_meter.IsNull(AFIP_Client_Common.METER_MAX_VALUE))
          {
            _max_value_from_tsmh = (Int64)_row_meter[AFIP_Client_Common.METER_MAX_VALUE];

            switch (_meter_code)
            {
              case ENUM_METER_CODE.COIN_IN:
              case ENUM_METER_CODE.COIN_OUT:
              case ENUM_METER_CODE.JACKPOTS:

                // Pass to credits necessary to obtain the maximum value
                _max_value = (Int64)(_max_value_from_tsmh / (CENTS_CONVERSION * _sas_denom));
                _max_value = (_max_value - 1 ?? 0);

                // It is necessary to pass it to centimos because SetMeterEndAndStartIfIsNull works in Centimos and does the conversion later.
                _value_end = (Int64)(_max_value * CENTS_CONVERSION * (Decimal)RowResult[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM]);

                break;
              default:
                //Meter in units
                _max_value = _max_value_from_tsmh;
                _value_end = (_max_value - 1 ?? 0);

                break;
            }
          }
        }

        SetMeterEndAndStartIfIsNull(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }
    }

    /// <summary>
    /// Set the meter Final Values with the sequence after values for SAS Account Denom Change
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array od records with the sequence values</param>
    private void SetMeterFinalsWithSequenceAfterValuesForSASAccountDenomChange(DataRow RowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
        _value_end = (Int64)_row_meter[AFIP_Client_Common.FIN_VALUE];
        _sas_denom = GetMeterSASAccountingDenomination(_row_meter);

        SetMeterEndAndStartIfIsNull(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }
    }

    /// <summary>
    /// Set the meter Final Values with the sequence after values for Reset
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="RowsSequence">Array od records with the sequence values</param>
    private void SetMeterFinalsWithSequenceBeforeValuesForServiceRAMClear(DataRow RowResult, DataRow[] RowsSequence)
    {
      ENUM_METER_CODE _meter_code;
      Int64 _value_start;
      Int64 _value_end;
      Decimal _sas_denom;

      foreach (DataRow _row_meter in RowsSequence)
      {
        _meter_code = (ENUM_METER_CODE)(Int32)_row_meter[AFIP_Client_Common.METER_CODE];
        _value_start = (Int64)_row_meter[AFIP_Client_Common.INI_VALUE];
        //Set end of Service RAM Clear with start value
        _value_end = _value_start;
        _sas_denom = GetMeterSASAccountingDenomination(_row_meter);

        SetMeterEndAndStartIfIsNull(RowResult, _meter_code, _value_start, _value_end, _sas_denom);
      }
    }

    /// <summary>
    /// Set the Meter with the hourly values
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="MeterCode"></param>
    /// <param name="ValueStart"></param>
    /// <param name="ValueEnd"></param>
    /// <param name="SASDenom"></param>
    private void SetMeterHourly(DataRow RowResult, ENUM_METER_CODE MeterCode, Int64 ValueStart, Int64 ValueEnd, Decimal SASDenom)
    {
      SetMeterEndAndStartIfIsNull(RowResult, MeterCode, ValueStart, ValueEnd, SASDenom);
    }

    /// <summary>
    /// Converts values in cents to meter value (integer)
    /// </summary>
    /// <param name="ValueInCents"></param>
    /// <param name="SASDenom"></param>
    /// <returns></returns>
    private Int64 ConvertCentsToMeterValue(Int64 ValueInCents, Decimal SASDenom)
    {
      Decimal _decimal_value;
      Int64 _meter_value;

      // ValueInCents = 33625    ==>    ((33625 / 100) / 0.05)    ==>    (336.25 / 0.05)    ==>    METER_VALUE = 6725 

      //This line is to avoid lossing decimals as the division
      _decimal_value = (((Decimal)ValueInCents / CENTS_CONVERSION) / SASDenom);
      _meter_value = (Int64)_decimal_value;

      return _meter_value;
    }

    /// <summary>
    /// Set the Meter end value and start if it is null
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="MeterCode"></param>
    /// <param name="ValueStart"></param>
    /// <param name="ValueEnd"></param>
    /// <param name="SASDenom"></param>
    private void SetMeterEndAndStartIfIsNull(DataRow RowResult, ENUM_METER_CODE MeterCode, Int64 ValueStart, Int64 ValueEnd, Decimal SASDenom)
    {
      switch (MeterCode)
      {
        case ENUM_METER_CODE.COIN_IN:
          //Meter arrives in cents -> translate to meter value
          if (RowResult.IsNull(AFIP_Client_Common.RESULT_COIN_IN_START))
          {
            RowResult[AFIP_Client_Common.RESULT_COIN_IN_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);
          }

          RowResult[AFIP_Client_Common.RESULT_COIN_IN_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.COIN_OUT:
          //Meter arrives in cents -> translate to meter value
          if (RowResult.IsNull(AFIP_Client_Common.RESULT_COIN_OUT_START))
          {
            RowResult[AFIP_Client_Common.RESULT_COIN_OUT_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);
          }

          RowResult[AFIP_Client_Common.RESULT_COIN_OUT_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.JACKPOTS:
          //Meter arrives in cents -> translate to meter value
          if (RowResult.IsNull(AFIP_Client_Common.RESULT_JACKPOTS_START))
          {
            RowResult[AFIP_Client_Common.RESULT_JACKPOTS_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);
          }

          RowResult[AFIP_Client_Common.RESULT_JACKPOTS_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.GAMES_PLAYED:
          //Meter arrives in units          
          if (RowResult.IsNull(AFIP_Client_Common.RESULT_GAMES_PLAYED_START))
          {
            RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = ValueStart;
          }

          RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = ValueEnd;

          break;
        default:
          break;
      }
    }

    /// <summary>
    /// Set the Meter start and end value
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="MeterCode"></param>
    /// <param name="ValueStart"></param>
    /// <param name="ValueEnd"></param>
    /// <param name="SASDenom"></param>
    private void SetMeterStartAndEnd(DataRow RowResult, ENUM_METER_CODE MeterCode, Int64 ValueStart, Int64 ValueEnd, Decimal SASDenom)
    {
      switch (MeterCode)
      {
        case ENUM_METER_CODE.COIN_IN:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_COIN_IN_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          RowResult[AFIP_Client_Common.RESULT_COIN_IN_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.COIN_OUT:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_COIN_OUT_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          RowResult[AFIP_Client_Common.RESULT_COIN_OUT_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.JACKPOTS:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_JACKPOTS_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          RowResult[AFIP_Client_Common.RESULT_JACKPOTS_END] = ConvertCentsToMeterValue(ValueEnd, SASDenom);

          break;
        case ENUM_METER_CODE.GAMES_PLAYED:
          //Meter arrives in units          
          RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = ValueStart;

          RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = ValueEnd;

          break;
        default:
          break;
      }
    }

    /// <summary>
    /// Set the Meter start value
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="MeterCode"></param>
    /// <param name="ValueStart"></param>
    /// <param name="ValueEnd"></param>
    /// <param name="SASDenom"></param>
    private void SetMeterStartForFirstRegister(DataRow RowResult, ENUM_METER_CODE MeterCode, Int64 ValueStart, Decimal SASDenom)
    {
      switch (MeterCode)
      {
        case ENUM_METER_CODE.COIN_IN:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_COIN_IN_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          break;
        case ENUM_METER_CODE.COIN_OUT:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_COIN_OUT_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          break;
        case ENUM_METER_CODE.JACKPOTS:
          //Meter arrives in cents -> translate to meter value
          RowResult[AFIP_Client_Common.RESULT_JACKPOTS_START] = ConvertCentsToMeterValue(ValueStart, SASDenom);

          break;
        case ENUM_METER_CODE.GAMES_PLAYED:
          //Meter arrives in units          
          RowResult[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = ValueStart;

          break;
        default:
          break;
      }
    }
    #endregion
  }
}
