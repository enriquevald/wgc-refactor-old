﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Provider_Request_DB.cs
// 
//   DESCRIPTION: Provider that returns Requests's DB data for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 17-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2016 FGB    First release 
// 15-MAR-2018 JML    Bug 31907:WIGOS-8806 [Ticket #13051] [Ticket#2018030710003365] AFIP - Trámites con Mesa de Ayuda - Sistema Jaza - Apoyo ate
// 18-ABR-2018 JML    Bug 32378:WIGOS-10107 AFIP - It don't send the record of the reset event correctly.
// 18-ABR-2018 JML    Bug 32379:WIGOS-10108 AFIP - It don't send the record of the rollover event correctly.
// 18-ABR-2018 JML    Bug 32380:WIGOS-10109 AFIP - It don't send the record of the denom. change event correctly.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_Request_DB
  {
    #region "Public Methods"
    /// <summary>
    /// Returns the terminal meters for a request
    /// </summary>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="RequestTerminals"></param>
    /// <param name="DtRequestDetail"></param>
    /// <returns></returns>
    public Boolean GetRequestMeters(DateTime StartDate, DateTime EndDate, String RequestTerminals, out DataTable DtRequestDetail)
    {
      DtRequestDetail = null;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SP_AFIP_GetTerminalMetersHistory", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.Parameters.Add("@pStartDateTime", SqlDbType.DateTime).Value = StartDate;
            _sql_cmd.Parameters.Add("@pEndDateTime", SqlDbType.DateTime).Value = EndDate;
            _sql_cmd.Parameters.Add("@pPendingType", SqlDbType.Int).Value = (Int32)AFIP_Common.PendingType.Request;
            _sql_cmd.Parameters.Add("@pTerminalsAFIP", SqlDbType.NVarChar).Value = RequestTerminals;
            _sql_cmd.Parameters.Add("@pTerminalStatusActive", SqlDbType.Int).Value = (Int32)TerminalStatus.ACTIVE;
            _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.NVarChar).Value = Misc.GamingTerminalTypeListToString();

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              DtRequestDetail = new DataTable();
              _sql_da.Fill(DtRequestDetail);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Request: Error in AFIP_Request_DB.GetRequestMeters: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Get terminal status
    /// </summary>
    /// <param name="RequestTime"></param>
    /// <param name="RequestTerminal"></param>
    /// <param name="AfipTerminalStatus"></param>
    /// <returns></returns>
    public Boolean GetRequestTerminalStatus(DateTime RequestTime, String RequestTerminal, out ENUM_AFIP_TERMINAL_STATE AfipTerminalStatus)
    {
      AfipTerminalStatus = ENUM_AFIP_TERMINAL_STATE.RETIRED;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SP_AFIP_GetRequestedTerminalStatus", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.Parameters.Add("@pRequestDateTime", SqlDbType.DateTime).Value = RequestTime;
            _sql_cmd.Parameters.Add("@pTerminalStatusRetired", SqlDbType.Int).Value = (Int32)WSI.Common.TerminalStatus.RETIRED;
            _sql_cmd.Parameters.Add("@pTerminalAFIP", SqlDbType.NVarChar).Value = RequestTerminal;
            _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.NVarChar).Value = Misc.GamingTerminalTypeListToString();

            SqlParameter _param;
            _param = _sql_cmd.Parameters.Add("@pStatusRequestedTerminal", SqlDbType.Int);
            _param.Direction = ParameterDirection.Output;

            _sql_cmd.ExecuteNonQuery();

            //Get status
            AfipTerminalStatus = (ENUM_AFIP_TERMINAL_STATE)(Int32)_param.Value;

            _db_trx.Commit();
          }
        }

        //It has retrieved the terminal status
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Request: Error in AFIP_Request_DB.GetRequestTerminalStatus: " + _ex.Message);
      }

      return false;
    }

    #endregion
  }
}
