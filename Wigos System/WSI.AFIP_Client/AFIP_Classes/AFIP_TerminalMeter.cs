﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_TerminalMeter.cs
// 
//   DESCRIPTION: Common classes for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 12-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAY-2016 FGB    First release 
// 23-JAN-2017 FGB    Bug 23207: AFIP_Client: Envio de jornadas desde Wigos no coincide con lo solicitado por Jaza Local y se muestra la jornada anterior.
// 21-NOV-2017 FGB    WIGOS-6745: AFIP: Pendiente de respuesta
// 30-NOV-2017 FGB    WIGOS-6745: AFIP: Pendiente de respuesta - Undone previous change because it is not a BUG.
//                      When there is no data then we should notify AFIP the casino that date did not work.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_TerminalMeter
  {
    #region "Members"
    private DataTable m_dt_terminalmeter;
    private Boolean m_enable_message_trace;
    private const String RESULT_METERS_TABLE_NAME = "RESULTMETERS";
    #endregion

    #region "Constructor"
    public AFIP_TerminalMeter(DataTable DtTerminalMeter, Boolean EnableMessageTrace)
    {
      m_dt_terminalmeter = DtTerminalMeter;
      m_enable_message_trace = EnableMessageTrace;
    }

    public AFIP_TerminalMeter()
      : this(null, AFIP_Common.AFIPEnableMessageTrace)
    {
    }
    #endregion

    #region "Public Functions"
    /// <summary>
    /// Get meters for Pending data
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="TerminalDetailList">Array of DETALLE_TERMINALES</param>
    /// <returns></returns>
    public Boolean GetTerminalMeter(AFIP_WS_JAZALocal.PENDIENTES Pending,
                                    out AFIP_WS_JAZALocal.DETALLE_TERMINALES[] TerminalDetailList)
    {
      DataTable _dt_meters_date;
      DataTable _dt_result_meters;
      String _error_msg;
      DateTime _request_date;
      Int32 _request_version;
      String _filter_for_requested_terminals;
      DateTime _start_date;
      DateTime _end_date;

      TerminalDetailList = null;

      try
      {
        //Is a valid request
        if (!IsValidTerminalMeterRequest(Pending, out _error_msg))
        {
          Log.Error(_error_msg);

          return false;
        }

        //Get values of the request 
        if (!GetRequestValues(Pending, out _request_date, out _request_version))
        {
          Log.Error("Terminals: Not valid values in request");

          return false;
        }

        //If there is a default Datatable then use it
        if (m_dt_terminalmeter != null)
        {
          _dt_result_meters = m_dt_terminalmeter;
        }
        else
        {
          //Get filter for requested terminals
          if (!GetFilterForRequestedTerminals(Pending, out _filter_for_requested_terminals))
          {
            Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.GetFilterForRequestedTerminals - PendingId: {0} - RequestDate: {1} - RequestVersion: {2}", Pending.idPendiente, _request_date.ToString("yyyy-MM-dd"), _request_version.ToString()));

            return false;
          }

          if (!AFIP_Common.GetSessionStartAndClosingTime(_request_date, out _start_date, out _end_date, false))
          {
            Log.Error(String.Format("Terminals: Error in AFIP_Common.GetSessionStartAndClosingTime - PendingId: {0} - RequestDate: {1} - RequestVersion: {2}", Pending.idPendiente, _request_date.ToString("yyyy-MM-dd"), _request_version.ToString()));
            return false;
          }

          //Get the requested date meters
          if (!GetTerminalsMeters(_start_date, _end_date, _filter_for_requested_terminals, out _dt_meters_date))
          {
            Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.GetTerminalsMeters - PendingId: {0} - RequestDate: {1} - RequestVersion: {2}", Pending.idPendiente, _request_date.ToString("yyyy-MM-dd"), _request_version.ToString()));

            return false;
          }

          //Process the terminal meters to obtain the data to send to AFIP
          if (!ProcessMeters(_dt_meters_date, _request_date, _request_version, _start_date, _end_date, out _dt_result_meters, out _error_msg))
          {
            Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.ProcessMetersSequences - PendingId: {0} - RequestDate: {1} - RequestVersion: {2} - {3}", Pending.idPendiente, _request_date.ToString("yyyy-MM-dd"), _request_version.ToString(), _error_msg));

            return false;
          }
        }

        //Generate the array to send
        if (!AddRecordsToArrayToSend(_dt_result_meters, _request_date, _request_version, out TerminalDetailList))
        {
          Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.AddRecordsToArrayToSend - PendingId: {0} - RequestDate: {1} - RequestVersion: {2}", Pending.idPendiente, _request_date.ToString("yyyy-MM-dd"), _request_version.ToString()));

          return false;
        }

        //Enable message trace
        if (m_enable_message_trace)
        {
          LogToXML(Pending, _dt_result_meters, _request_date, _request_version);
        }

        //Everything OK
        Log.Message(String.Format("Terminals: Data obtained OK for PendingId: {0}", Pending.idPendiente));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Terminals: Error at GetTerminalMeter: {0}", _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Log to XML
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="DtResultMeters"></param>
    /// <param name="RequestDate"></param>
    /// <param name="RequestVersion"></param>
    /// <returns></returns>
    private Boolean LogToXML(AFIP_WS_JAZALocal.PENDIENTES Pending, DataTable DtResultMeters, DateTime RequestDate, Int32 RequestVersion)
    {
      String _meters_XML;
      String _error_message;

      try
      {
        Log.Message(String.Format("Terminals START: elements to be sent - PendingId: {0} - RequestDate: {1}", Pending.idPendiente, RequestDate.ToString("yyyy-MM-dd")));

        if (!AFIP_Common.GenerateXMLFromDatatable(DtResultMeters, RESULT_METERS_TABLE_NAME, out _meters_XML, out _error_message))
        {
          Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.GenerateXMLFromDatatable - PendingId: {0} - RequestDate: {1} - {2}", Pending.idPendiente, RequestDate.ToString("yyyy-MM-dd"), _error_message));
          return false;
        }

        if (!String.IsNullOrEmpty(_meters_XML))
        {
          Log.Message(_meters_XML);
        }

        Log.Message(String.Format("Terminals END: elements to be sent - PendingId: {0} - RequestDate: {1}", Pending.idPendiente, RequestDate.ToString("yyyy-MM-dd")));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.LogToXML - PendingId: {0} - RequestDate: {1} - {2}", Pending.idPendiente, RequestDate.ToString("yyyy-MM-dd"), _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Process the meters in DtMetersDate generating processed data in DtResultMeters
    /// </summary>
    /// <param name="DtMetersDate"></param>
    /// <param name="RequestDate"></param>
    /// <param name="RequestVersion"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="DtResultMeters"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean ProcessMeters(DataTable DtMetersDate, DateTime RequestDate, Int32 RequestVersion, DateTime StartDate, DateTime EndDate, out DataTable DtResultMeters, out String ErrorMessage)
    {
      AFIP_ProcessMeters_Settings _process_settings;
      AFIP_ProcessMeters _afip_process_meters;

      _process_settings = new AFIP_ProcessMeters_Settings(false, RequestDate, RequestVersion, null, -1, StartDate, EndDate);
      _afip_process_meters = new AFIP_ProcessMeters(_process_settings);

      return _afip_process_meters.ProcessMeters(DtMetersDate, out DtResultMeters, out ErrorMessage);
    }

    /// <summary>
    /// Moves the DataTable to an Array to send
    /// </summary>
    /// <param name="DtTerminalsMeters"></param>
    /// <param name="RequestDate"></param>
    /// <param name="RequestVersion"></param>
    /// <param name="ArrayTerminalDetail"></param>
    /// <returns></returns>
    private Boolean AddRecordsToArrayToSend(DataTable DtTerminalsMeters, DateTime RequestDate, Int32 RequestVersion, out AFIP_WS_JAZALocal.DETALLE_TERMINALES[] ArrayTerminalDetail)
    {
      List<AFIP_WS_JAZALocal.DETALLE_TERMINALES> _terminal_to_send_list;
      AFIP_WS_JAZALocal.DETALLE_TERMINALES _detail;

      ArrayTerminalDetail = null;

      try
      {
        _terminal_to_send_list = new List<AFIP_WS_JAZALocal.DETALLE_TERMINALES>();

        //Add records to list to send
        foreach (DataRow _row in DtTerminalsMeters.Rows)
        {
          MapRowMeterToTerminalDetail(_row, out _detail);

          _terminal_to_send_list.Add(_detail);
        }

        if (_terminal_to_send_list.Count == 0)
        {
          //If there are not records, log message and return false
          Log.Error(String.Format("Terminals: Error in AddRecordsToArrayToSend: There are no records for RequestDate: {0}", RequestDate.ToString("yyyy-MM-dd")));

          return false;
        }

        //Convert List to Array
        ArrayTerminalDetail = _terminal_to_send_list.ToArray();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Terminals: Error at AddRecordsToArrayToSend: {0}", _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Creates Dummy record
    /// </summary>
    /// <param name="RequestDate"></param>
    /// <param name="RequestVersion"></param>
    /// <param name="DummyTerminalDetail"></param>
    /// <returns></returns>
    public Boolean CreateDummyTerminalDetail(DateTime RequestDate, Int32 RequestVersion, out AFIP_WS_JAZALocal.DETALLE_TERMINALES DummyTerminalDetail)
    {
      AFIP_WS_JAZALocal.DETALLE_TERMINALES _detail;

      DummyTerminalDetail = null;

      try
      {
        //Generate an empty record
        _detail = new AFIP_WS_JAZALocal.DETALLE_TERMINALES();
        _detail.fechaJornada = RequestDate;
        _detail.Version = RequestVersion;

        //_detail.secuencia = 1; //TODO: FGB: qUITAR????

        DummyTerminalDetail = _detail;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Terminals: Error at CreateDummyTerminalDetail: {0}", _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Gets values from request    
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="RequestDate"></param>
    /// <param name="RequestVersion"></param>
    /// <returns></returns>
    private Boolean GetRequestValues(AFIP_WS_JAZALocal.PENDIENTES Pending, out DateTime RequestDate, out Int32 RequestVersion)
    {
      RequestDate = Pending.fechaJornada ?? WGDB.Now;
      RequestVersion = Pending.version ?? 0;

      return true;
    }

    /// <summary>
    /// Verifies that the pending object has values in mandatory fields
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean IsValidTerminalMeterRequest(AFIP_WS_JAZALocal.PENDIENTES Pending, out String ErrorMessage)
    {
      ErrorMessage = String.Empty;

      try
      {
        if (Pending == null)
        {
          ErrorMessage = "Terminals: Requested object is empty";

          return false;
        }

        if (Pending.fechaJornada == null)
        {
          ErrorMessage = "Terminals: Requested date is empty";

          return false;
        }

        if (Pending.version == null)
        {
          ErrorMessage = "Terminals: Requested version is empty";

          return false;
        }

        if (Pending.version <= 0)
        {
          ErrorMessage = "Terminals: Requested version is not valid";

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMessage = String.Format("Terminals: IsValidTerminalMeterRequest error: {0}", _ex.Message);
      }

      return false;
    }
    #endregion

    /// <summary>
    /// Get the terminals meters for a Session date
    /// </summary>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="RequestTerminals"></param>
    /// <param name="DtTerminalDetail"></param>
    /// <returns></returns>
    #region "Private Functions"
    private Boolean GetTerminalsMeters(DateTime StartDate, DateTime EndDate, String RequestTerminals, out DataTable DtTerminalDetail)
    {
      DtTerminalDetail = new DataTable();

      try
      {
        AFIP_TerminalMeter_DB _afip_terminalmeter_db;
        _afip_terminalmeter_db = new AFIP_TerminalMeter_DB();

        return _afip_terminalmeter_db.GetTerminalsMeters(StartDate, EndDate, RequestTerminals, out DtTerminalDetail);
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Terminals: Error in AFIP_TerminalMeter.GetTerminalsMeters - StartDate: {0} - EndDate: {1} - RequestTerminals: {2} - {3} "
                                , StartDate.ToString("yyyy-MM-dd HH:mm:ss"), EndDate.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminals, _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Returns the filter for the requested terminals in the Pending data
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="TerminalsFilter"></param>
    /// <returns></returns>
    private Boolean GetFilterForRequestedTerminals(AFIP_WS_JAZALocal.PENDIENTES Pending, out String TerminalsFilter)
    {
      TerminalsFilter = String.Empty;

      try
      {
        //Are there any requested terminals
        if ((Pending.Maquinas != null) && (Pending.Maquinas.Length > 0))
        {
          //Get the filter string for the requested terminals
          TerminalsFilter = String.Join(", ", Pending.Maquinas);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Terminals: Error in AFIP_TerminalMeter.GetFilterForRequestedTerminals: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Maps from a RowResult to DETALLE_TERMINALES
    /// </summary>
    /// <param name="RowResult"></param>
    /// <param name="TerminalDetail"></param>
    /// <returns></returns>
    private Boolean MapRowMeterToTerminalDetail(DataRow RowMeter, out AFIP_WS_JAZALocal.DETALLE_TERMINALES TerminalDetail)
    {
      TerminalDetail = null;

      try
      {
        TerminalDetail = new AFIP_WS_JAZALocal.DETALLE_TERMINALES();

        TerminalDetail.fechaJornada = Convert.ToDateTime(RowMeter[AFIP_Client_Common.RESULT_SESSION_DATE]);
        TerminalDetail.IdMaquina = RowMeter[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE].ToString();
        TerminalDetail.Denominacion = Convert.ToDecimal(RowMeter[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM]);

        TerminalDetail.secuencia = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER]);
        TerminalDetail.Version = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_VERSION]);

        TerminalDetail.fechaHoraSecuenciaInicio = Convert.ToDateTime(RowMeter[AFIP_Client_Common.RESULT_DATE_START]);
        TerminalDetail.ContJuegosIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_GAMES_PLAYED_START]);
        TerminalDetail.ContCoinInIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_IN_START]);
        TerminalDetail.ContCoinOutIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_OUT_START]);
        TerminalDetail.ContJackpotIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_JACKPOTS_START]);

        TerminalDetail.fechaHoraSecuenciaFin = Convert.ToDateTime(RowMeter[AFIP_Client_Common.RESULT_DATE_END]);
        TerminalDetail.ContJuegosFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_GAMES_PLAYED_END]);
        TerminalDetail.ContCoinInFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_IN_END]);
        TerminalDetail.ContCoinOutFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_OUT_END]);
        TerminalDetail.ContJackpotFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_JACKPOTS_END]);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Terminals: Error in AFIP_TerminalMeter.MapRowMeterToTerminalDetail: " + _ex.Message);
      }

      return false;
    }

    #endregion
  }
}