﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Request.cs
// 
//   DESCRIPTION: Common classes for AFIP Service
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 17-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2016 FGB    First release 
// 15-MAR-2018 JML    Bug 31907:WIGOS-8806 [Ticket #13051] [Ticket#2018030710003365] AFIP - Trámites con Mesa de Ayuda - Sistema Jaza - Apoyo ate
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_Request
  {
    #region "Members"
    private DataTable m_dt_request;
    private Boolean m_enable_message_trace;
    private const String RESULT_METERS_TABLE_NAME = "RESULTREQUEST";
    private const Int32 SEQUENCE_NUMBER_FOR_TERMINAL_NO_ACTIVE = 1;
    #endregion

    #region "Constructor"
    public AFIP_Request(DataTable DtRequest, Boolean EnableMessageTrace)
    {
      m_dt_request = DtRequest;
      m_enable_message_trace = EnableMessageTrace;
    }

    public AFIP_Request()
      : this(null, AFIP_Common.AFIPEnableMessageTrace)
    {
    }
    #endregion

    #region "Public Functions"
    /// <summary>
    /// Get meters for Pending data
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="TerminalDetailList">Array of DETALLE_SOLICITUDES</param>
    /// <returns></returns>
    public Boolean GetRequestMeter(AFIP_WS_JAZALocal.PENDIENTES Pending,
                                   out AFIP_WS_JAZALocal.DETALLE_SOLICITUDES[] TerminalDetailList)
    {
      DataTable _dt_request_meters;
      DataTable _dt_result_meters;
      Int32 _request_id;
      DateTime _request_time;
      String _request_terminal_code;
      String _error_msg;
      DateTime _start_date;
      DateTime _end_date;
      ENUM_AFIP_TERMINAL_STATE _afip_terminal_status;

      TerminalDetailList = null;

      try
      {
        //Is a valid request
        if (!IsValidRequest(Pending, out _error_msg))
        {
          Log.Error(String.Format("Request: Error in AFIP_Request.IsValidRequest: {0}", _error_msg));
          return false;
        }

        //Get values of the request
        if (!GetRequestValues(Pending, out _request_id, out _request_time, out _request_terminal_code, out _error_msg))
        {
          Log.Error(String.Format("Request: Error in AFIP_Request.GetRequestValues - Not valid values in request: {0}", _error_msg));
          return false;
        }

        //If there is a default Datatable then use it
        if (m_dt_request != null)
        {
          _dt_result_meters = m_dt_request;
          _afip_terminal_status = ENUM_AFIP_TERMINAL_STATE.ACTIVE;
        }
        else
        {
          //Get terminal status
          if (!GetRequestTerminalStatus(_request_time, _request_terminal_code, out _afip_terminal_status))
          {
            Log.Error(String.Format("Request: Error in AFIP_Common.GetRequestTerminalStatus - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", _request_id.ToString(), _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString()));
            return false;
          }

          //Is RequestTerminalCode active?
          if (IsAfipTerminalStatusActive(_afip_terminal_status))
          {
            if (!AFIP_Common.GetSessionStartAndClosingTime(_request_time, out _start_date, out _end_date, true))
            {
              Log.Error(String.Format("Request: Error in AFIP_Common.GetSessionStartAndClosingTime - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", _request_id.ToString(), _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString()));
              return false;
            }

            //Get the current date meters
            if (!GetRequestMeters(_start_date, _end_date, _request_terminal_code, out _dt_request_meters))
            {
              Log.Error(String.Format("Request: Error in AFIP_Request.GetRequestMeters - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", _request_id.ToString(), _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString()));
              return false;
            }

            //Process the request meters to obtain the data to send to AFIP
            if (!ProcessMeters(_dt_request_meters, _request_id, _request_time, _start_date, _end_date, out _dt_result_meters, out _error_msg))
            {
              Log.Error(String.Format("Request: Error in AFIP_Request.ProcessMetersSequences - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2} - {3}", _request_id.ToString(), _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString(), _error_msg));
              return false;
            }
          }
          else
          {
            //Terminal is RETIRED or WITHOUT_METERS
            //Get meters for terminal status
            if (!GetRequestMetersForTerminalNotActive(_request_id, _request_terminal_code, _afip_terminal_status, out _dt_result_meters))
            {
              Log.Error(String.Format("Request: Error in AFIP_Common.GetRequestMetersForTerminalNotActive - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", _request_id, _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString()));
              return false;
            }
          }
        }

        //Generate the array to send.
        if (!AddRecordsToArrayToSend(_dt_result_meters, out TerminalDetailList))
        {
          Log.Error(String.Format("Request: Error in AFIP_Request.AddRecordsToArrayToSend - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", _request_id.ToString(), _request_time.ToString("yyyy-MM-dd HH:mm:ss"), _request_terminal_code.ToString()));
          return false;
        }

        //Enable message trace
        if (m_enable_message_trace)
        {
          LogToXML(_dt_result_meters, _request_id, _request_time, _request_terminal_code);
        }

        //Everything OK
        Log.Message(String.Format("Request: Data obtained OK for PendienteId: {0} - RequestId: {1}", Pending.idPendiente, _request_id.ToString()));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error in AFIP_Request.GetRequestMeter: {0}", _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Get meters for a terminal NOT active
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="RequestTerminalCode"></param>
    /// <param name="AfipTerminalStatus"></param>
    /// <param name="DtRequestDetail"></param>
    /// <returns></returns>
    private Boolean GetRequestMetersForTerminalNotActive(Int32 RequestId, String RequestTerminalCode, ENUM_AFIP_TERMINAL_STATE AfipTerminalStatus, out DataTable DtRequestDetail)
    {
      DataTable _dt;
      DataRow _dr;
      DateTime _current_date;

      _dt = new AFIP_Client_Common().CreateResultDatatable(true);

      _dr = _dt.NewRow();

      //We set DateTime.Now because DateTime.MinValue returns an error when treated by Jaza Local
      _current_date = DateTime.Now;

      //Even that only ther first 3 fields are not NULLABLE,
      //  all fields must have a value to overcome a JAZA Central error.
      _dr[AFIP_Client_Common.RESULT_REQUEST_ID] = RequestId;
      _dr[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE] = RequestTerminalCode;
      _dr[AFIP_Client_Common.RESULT_AFIP_STATE] = (Int32)AfipTerminalStatus;

      _dr[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER] = SEQUENCE_NUMBER_FOR_TERMINAL_NO_ACTIVE;
      _dr[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM] = 0D;

      _dr[AFIP_Client_Common.RESULT_DATE_START] = _current_date;
      _dr[AFIP_Client_Common.RESULT_GAMES_PLAYED_START] = 0;
      _dr[AFIP_Client_Common.RESULT_COIN_IN_START] = 0;
      _dr[AFIP_Client_Common.RESULT_COIN_OUT_START] = 0;
      _dr[AFIP_Client_Common.RESULT_JACKPOTS_START] = 0;

      _dr[AFIP_Client_Common.RESULT_DATE_END] = _current_date;
      _dr[AFIP_Client_Common.RESULT_GAMES_PLAYED_END] = 0;
      _dr[AFIP_Client_Common.RESULT_COIN_IN_END] = 0;
      _dr[AFIP_Client_Common.RESULT_COIN_OUT_END] = 0;
      _dr[AFIP_Client_Common.RESULT_JACKPOTS_END] = 0;

      //Fiels that are not nullable, but there are internal and not mapped
      _dr[AFIP_Client_Common.RESULT_TERMINAL_ID] = "666666";
      _dr[AFIP_Client_Common.RESULT_INTERVAL_TO] = _current_date;
      _dr[AFIP_Client_Common.RESULT_INTERVAL_FROM] = _current_date;

      _dt.Rows.Add(_dr);

      DtRequestDetail = _dt;

      return true;
    }

    #endregion

    #region "Private Functions"
    /// <summary>
    /// Log datatable to XML
    /// </summary>
    /// <param name="DtResultMeters"></param>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="RequestTerminalCode"></param>
    /// <returns></returns>
    private Boolean LogToXML(DataTable DtResultMeters, Int32 RequestId, DateTime RequestTime, String RequestTerminalCode)
    {
      String _meters_XML;
      String _error_message;

      try
      {
        Log.Message(String.Format("Request START: elements to be sent - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", RequestId.ToString(), RequestTime.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode.ToString()));

        if (!AFIP_Common.GenerateXMLFromDatatable(DtResultMeters, RESULT_METERS_TABLE_NAME, out _meters_XML, out _error_message))
        {
          Log.Error(String.Format("Request: Error in AFIP_Common.GenerateXMLFromDatatable - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2} - {3}", RequestId.ToString(), RequestTime.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode.ToString(), _error_message));
          return false;
        }

        if (!String.IsNullOrEmpty(_meters_XML))
        {
          Log.Message(_meters_XML);
        }

        Log.Message(String.Format("Request END: elements to be sent - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2}", RequestId.ToString(), RequestTime.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode.ToString()));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error in AFIP_Request.LogToXML - RequestId: {0} - RequestTime: {1} - RequestTerminalCode: {2} - {3}", RequestId.ToString(), RequestTime.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode.ToString(), _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Process the meters in DtRequestMeters generating processed data in DtResultRequestMeters
    /// </summary>
    /// <param name="DtRequestMeters"></param>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="DtResultRequestMeters"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean ProcessMeters(DataTable DtRequestMeters, Int32 RequestId, DateTime RequestTime, DateTime StartDate, DateTime EndDate, out DataTable DtResultRequestMeters, out String ErrorMessage)
    {
      AFIP_ProcessMeters_Settings _process_settings;
      AFIP_ProcessMeters _afip_process_meters;

      //Process meters
      _process_settings = new AFIP_ProcessMeters_Settings(true, null, -1, RequestTime, RequestId, StartDate, EndDate);
      _afip_process_meters = new AFIP_ProcessMeters(_process_settings);

      //Process meters sequences
      return _afip_process_meters.ProcessMeters(DtRequestMeters, out DtResultRequestMeters, out ErrorMessage);
    }

    /// <summary>
    /// Return the list of records to send
    /// </summary>
    /// <param name="DtRequestMeters"></param>
    /// <param name="ArrayTerminalDetail"></param>
    /// <returns></returns>
    private Boolean AddRecordsToArrayToSend(DataTable DtRequestMeters, out AFIP_WS_JAZALocal.DETALLE_SOLICITUDES[] ArrayTerminalDetail)
    {
      List<AFIP_WS_JAZALocal.DETALLE_SOLICITUDES> _terminal_to_send_list;
      AFIP_WS_JAZALocal.DETALLE_SOLICITUDES _detail;

      ArrayTerminalDetail = null;

      try
      {
        _terminal_to_send_list = new List<AFIP_WS_JAZALocal.DETALLE_SOLICITUDES>();

        //If there are rows.
        if (DtRequestMeters.Rows.Count > 0)
        {
          foreach (DataRow _row in DtRequestMeters.Rows)
          {
            if (!MapRowMeterToRequestDetail(_row, out _detail))
            {
              Log.Error("Request: Error in AFIP_Request.MapRowMeterToRequestDetail");
              ArrayTerminalDetail = null;
              return false;
            }

            _terminal_to_send_list.Add(_detail);
          }
        }

        //Convert List to Array
        ArrayTerminalDetail = _terminal_to_send_list.ToArray();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error at AFIP_Request.AddRecordsToArrayToSend: {0}", _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Verifies that the pendiing object has values in mandatory fields
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean IsValidRequest(AFIP_WS_JAZALocal.PENDIENTES Pending, out String ErrorMessage)
    {
      ErrorMessage = String.Empty;

      if (Pending == null)
      {
        ErrorMessage = "Request: Requested object is empty";
        return false;
      }

      if (Pending.FechaHoraSolicitud == null)
      {
        ErrorMessage = "Request: Requested FechaHoraSolicitud is empty";
        return false;
      }

      if (Pending.idSolicitud == null)
      {
        ErrorMessage = "Request: Requested RequestId is empty";
        return false;
      }

      if (Pending.idMaquina == null)
      {
        ErrorMessage = "Request: Requested IdMaquina is empty";
        return false;
      }

      return true;
    }

    /// <summary>
    /// Is terminal status active
    /// </summary>
    /// <param name="AfipTerminalStatus"></param>
    /// <returns></returns>
    private Boolean IsAfipTerminalStatusActive(ENUM_AFIP_TERMINAL_STATE AfipTerminalStatus)
    {
      return (AfipTerminalStatus == ENUM_AFIP_TERMINAL_STATE.ACTIVE);
    }

    /// <summary>
    /// Gets values from request
    /// </summary>
    /// <param name="Pending"></param>
    /// <param name="RequestId"></param>
    /// <param name="RequestTime"></param>
    /// <param name="RequestTerminalCode"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns>
    private Boolean GetRequestValues(AFIP_WS_JAZALocal.PENDIENTES Pending, out Int32 RequestId, out DateTime RequestTime, out String RequestTerminalCode, out String ErrorMessage)
    {
      ErrorMessage = String.Empty;

      RequestId = 0;
      RequestTime = DateTime.Now;
      RequestTerminalCode = String.Empty;

      try
      {
        RequestId = Pending.idSolicitud ?? 0;
        RequestTime = Pending.FechaHoraSolicitud ?? DateTime.Now;
        RequestTerminalCode = Pending.idMaquina ?? String.Empty;

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMessage = _ex.Message;
      }

      return false;
    }

    /// <summary>
    /// Returns a DataTable with the meters for the request
    /// </summary>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="RequestTerminalCode"></param>
    /// <param name="DtRequestDetail"></param>
    /// <returns></returns>
    private Boolean GetRequestMeters(DateTime StartDate, DateTime EndDate, String RequestTerminalCode, out DataTable DtRequestDetail)
    {
      DtRequestDetail = new DataTable();

      try
      {
        AFIP_Request_DB _afip_request_db;
        _afip_request_db = new AFIP_Request_DB();

        return (_afip_request_db.GetRequestMeters(StartDate, EndDate, RequestTerminalCode, out DtRequestDetail));
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error in AFIP_Request.GetRequestMeters - StartDate: {0} - EndDate: {1} - RequestTerminalCode: {2} - {3} ",
                                StartDate.ToString("yyyy-MM-dd HH:mm:ss"), EndDate.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode, _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Get terminal status at request time
    /// </summary>
    /// <param name="RequestTime"></param>
    /// <param name="RequestTerminalCode"></param>
    /// <param name="TerminalStatus"></param>
    /// <returns></returns>
    private Boolean GetRequestTerminalStatus(DateTime RequestTime, String RequestTerminalCode, out ENUM_AFIP_TERMINAL_STATE TerminalStatus)
    {
      TerminalStatus = ENUM_AFIP_TERMINAL_STATE.RETIRED;

      try
      {
        AFIP_Request_DB _afip_request_db;
        _afip_request_db = new AFIP_Request_DB();

        return (_afip_request_db.GetRequestTerminalStatus(RequestTime, RequestTerminalCode, out TerminalStatus));
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error in AFIP_Request.GetRequestTerminalStatus - RequestTime: {0} - RequestTerminalCode: {1} - {2} ",
                                RequestTime.ToString("yyyy-MM-dd HH:mm:ss"), RequestTerminalCode, _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Maps a DataRow to DETALLE_SOLICITUDES
    /// </summary>
    /// <param name="RowMeter"></param>
    /// <param name="RequestDetail"></param>
    /// <returns></returns>
    private Boolean MapRowMeterToRequestDetail(DataRow RowMeter, out AFIP_WS_JAZALocal.DETALLE_SOLICITUDES RequestDetail)
    {
      RequestDetail = null;

      try
      {
        RequestDetail = new AFIP_WS_JAZALocal.DETALLE_SOLICITUDES();

        RequestDetail.IdSolicitud = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_REQUEST_ID]);
        RequestDetail.IdMaquina = RowMeter[AFIP_Client_Common.RESULT_TERMINAL_REGISTRATION_CODE].ToString();
        RequestDetail.secuencia = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_SEQUENCE_NUMBER]);
        RequestDetail.EstadoAfip = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_AFIP_STATE]);

        RequestDetail.Denominacion = Convert.ToDecimal(RowMeter[AFIP_Client_Common.RESULT_TERMINAL_ACCOUNTING_DENOM]);

        RequestDetail.fechaHoraSecuenciaInicio = Convert.ToDateTime(RowMeter[AFIP_Client_Common.RESULT_DATE_START]);
        RequestDetail.ContJuegosIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_GAMES_PLAYED_START]);
        RequestDetail.ContCoinInIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_IN_START]);
        RequestDetail.ContCoinOutIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_OUT_START]);
        RequestDetail.ContJackpotIni = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_JACKPOTS_START]);

        RequestDetail.fechaHoraSecuenciaFin = Convert.ToDateTime(RowMeter[AFIP_Client_Common.RESULT_DATE_END]);
        RequestDetail.ContJuegosFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_GAMES_PLAYED_END]);
        RequestDetail.ContCoinInFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_IN_END]);
        RequestDetail.ContCoinOutFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_COIN_OUT_END]);
        RequestDetail.ContJackpotFin = Convert.ToInt32(RowMeter[AFIP_Client_Common.RESULT_JACKPOTS_END]);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Request: Error at AFIP_Request.MapRowMeterToRequestDetail: {0}", _ex.Message));
      }

      return false;
    }

    #endregion
  }
}