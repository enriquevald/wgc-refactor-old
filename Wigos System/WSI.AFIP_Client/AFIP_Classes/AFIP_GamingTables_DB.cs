﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_GamingTables_DB.cs
// 
//   DESCRIPTION: Class for access to DB
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 06-Jun-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-Jun-2016 FOS    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP_Classes
{
  public class AFIP_GamingTables_DB
  {

    /// <summary>
    /// Function that getData from Database
    /// </summary>
    /// <param name="RequestDate">Datestart to find data values</param>
    /// <returns></returns>
    public DataTable GamingTables_CurrentDate(DateTime SessionDate)
    {
      DateTime _start_date;
      DataTable _dt_meter;

      try
      {
        if (!AFIP_Common.GetSessionStartTime(SessionDate, out _start_date))
        {
          Log.Error("Error getting start session date in GamingTables_CurrentDate");

          return null;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand("SP_AFIP_GAMING_TABLES_TYPE", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandType = CommandType.StoredProcedure;
            _sql_cmd.Parameters.Add("@pStarDateTime", SqlDbType.DateTime).Value = _start_date;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _dt_meter = new DataTable();
              _sql_da.Fill(_dt_meter);

              return _dt_meter;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error in GamingTables_CurrentDate: " + _ex.Message);

        return null;
      }
    }
  }
}
