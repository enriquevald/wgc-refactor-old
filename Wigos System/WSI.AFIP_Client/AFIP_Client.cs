﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Client.cs
// 
//   DESCRIPTION: Common Procedures to the AFIP Service
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 06-May-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-May-2016 FOS    First release 
// 18-ABR-2018 JML    Bug 32378:WIGOS-10107 AFIP - It don't send the record of the reset event correctly.
// 18-ABR-2018 JML    Bug 32379:WIGOS-10108 AFIP - It don't send the record of the rollover event correctly.
// 18-ABR-2018 JML    Bug 32380:WIGOS-10109 AFIP - It don't send the record of the denom. change event correctly.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading;
using WSI.Common;
using WSI.Common.AFIP_Common;

namespace WSI.AFIP_Client.AFIP
{
  class AFIP_Client
  {
    #region "Constants"
    private const Int32 WAIT_SECONDS = 20;  // 20 seg.
    private const Int32 WAIT_DISABLED_SECONDS = 300; // 5 Min 
    private const Int32 WAIT_NOT_ALIVE_SECONDS = 30;  // 30 seg.
    #endregion

    #region "Members"
    private static Thread m_thread;
    private static AFIP_WS_JAZALocal.Service m_ws;
    #endregion

    #region "Public Methods"
    public static void Init()
    {
      //Initialize thread
      m_thread = new Thread(PendingReportsThread);
      m_thread.Name = "PendingReportsThread";
      m_thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      m_thread.Start();
    }
    #endregion

    #region "Private Methods"

    /// <summary>
    /// Thread that check if exists pending reports.
    /// </summary>
    private static void PendingReportsThread()
    {
      Int32 _ellapsed_time;
      Int32 _wait_seconds;

      _wait_seconds = WAIT_SECONDS;

      while (true)
      {
        _ellapsed_time = (1000 * _wait_seconds);
        Thread.Sleep(_ellapsed_time);

        //Is Enabled
        if (!AFIP_Common.IsEnabled)
        {
          _wait_seconds = WAIT_DISABLED_SECONDS;

          continue;
        }

        // Try to connect
        if (!ServiceConnect())
        {
          _wait_seconds = WAIT_NOT_ALIVE_SECONDS;

          continue;
        }

        //Get elapsed time
        _wait_seconds = AFIP_Common.FrequencyToGetPendingTasks;

        try
        {
          ProcessData();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        } // try
      } // while

    } //PendingReportsThread

    /// <summary>
    /// Tries to connect to the primary and secondary Jaza Local service
    /// </summary>
    /// <returns></returns>
    private static Boolean ServiceConnect()
    {
      Boolean _service_connected;

      _service_connected = false;

      _service_connected = ConnectToServerAdress(AFIP_Common.AFIPServerAddress);

      if (!_service_connected) // Principal is not connected we try to connect to the second URL
      {
        _service_connected = ConnectToServerAdress(AFIP_Common.AFIPServerAddress2);
      }

      return _service_connected;
    }

    /// <summary>
    /// Tries to connect to a Jaza Local server address
    /// </summary>
    /// <param name="ServerAddress"></param>
    /// <returns></returns>
    private static Boolean ConnectToServerAdress(String ServerAddress)
    {
      Boolean _service_connected;
      _service_connected = false;

      try
      {
        m_ws = new AFIP_WS_JAZALocal.Service();
        m_ws.Url = ServerAddress;

        //Is JAZALocal alive?
        m_ws.InformarEstado();

        _service_connected = true;
      }
      catch (Exception _ex)
      {
        Log.Error(string.Format("JAZALocal service at {0} is not alive: {1}", ServerAddress, _ex.Message));
      }

      return _service_connected;
    }

    /// <summary>
    /// Get mock pending list
    /// </summary>
    /// <returns></returns>
    private static AFIP_WS_JAZALocal.PENDIENTES[] GetMockPendingList()
    {
      List<AFIP_WS_JAZALocal.PENDIENTES> _mock_list;
      _mock_list = new List<AFIP_WS_JAZALocal.PENDIENTES>();

      AFIP_WS_JAZALocal.PENDIENTES _mock;
      //_mock = new AFIP_WS_JAZALocal.PENDIENTES();
      //_mock.idTipo = (int)AFIP_Common.PendingType.Request;
      //_mock.idMaquina = "956325";
      //_mock.FechaHoraSolicitud = DateTime.Now;
      //_mock.idSolicitud = 111;
      //_mock.idPendiente = 777;

      //_mock_list.Add(_mock);

      _mock = new AFIP_WS_JAZALocal.PENDIENTES();
      _mock.idTipo = (int)AFIP_Common.PendingType.Terminals;
      _mock.fechaJornada = new DateTime(2018, 07, 21, 07, 00, 00);
      _mock.fechaJornada = new DateTime(2018, 07, 22, 07, 00, 00);
      _mock.fechaJornada = new DateTime(2018, 07, 23, 07, 00, 00);
      _mock.fechaJornada = new DateTime(2018, 07, 24, 07, 00, 00);
      _mock.fechaJornada = new DateTime(2018, 07, 25, 07, 00, 00);
      _mock.fechaJornada = new DateTime(2018, 07, 26, 07, 00, 00);
      _mock.FechaHoraSolicitud = DateTime.Now;
      _mock.idSolicitud = 111;
      _mock.idPendiente = 777;
      _mock.version = 5;

      _mock_list.Add(_mock);


      //_mock = new AFIP_WS_JAZALocal.PENDIENTES();
      //_mock.idTipo = (int)AFIP_Common.PendingType.Request;
      //_mock.idMaquina = "10002100";
      //_mock.FechaHoraSolicitud = DateTime.Now;
      //_mock.idSolicitud = 222;
      //_mock.idPendiente = 888;

      //_mock_list.Add(_mock);

      return _mock_list.ToArray();
    }

    /// <summary>
    /// Get pending list
    /// </summary>
    /// <param name="IsRealData"></param>
    /// <returns></returns>
    private static AFIP_WS_JAZALocal.PENDIENTES[] GetPendingList(Boolean IsRealData)
    {
      if (IsRealData)
      {
        return m_ws.ObtenerPendientes();
      }

      //Mocking data
      return GetMockPendingList();
    }

    /// <summary>
    /// Method that processes request and sends Data to webservice (JAZALocal)
    /// </summary>
    private static void ProcessData()
    {
      AFIP_WS_JAZALocal.PENDIENTES[] _pending_list;
      _pending_list = GetPendingList(true);

      foreach (AFIP_WS_JAZALocal.PENDIENTES _pending in _pending_list)
      {
        switch ((AFIP_Common.PendingType)_pending.idTipo)
        {
          case AFIP_Common.PendingType.Terminals:
            ProcessAndSendTerminals(_pending);
            break;

          case AFIP_Common.PendingType.GamingTables:
            ProcessAndSendGamingTables(_pending);
            break;

          case AFIP_Common.PendingType.Request:
            ProcessAndSendRequest(_pending);
            break;

          case AFIP_Common.PendingType.Bingo:
            ProcessAndSendBingo(_pending);
            break;

          default:
            Log.Error(String.Format("ProcessData: Pending type {0} invalid", _pending.idTipo.ToString()));
            break;
        }
      }
    }

    /// <summary>
    /// Function that processes and sends pending Terminals
    /// </summary>
    /// <param name="Pending">list of pending terminals</param>
    private static Boolean ProcessAndSendTerminals(AFIP_WS_JAZALocal.PENDIENTES Pending)
    {
      AFIP_WS_JAZALocal.DETALLE_TERMINALES[] _terminal_detail_list;
      AFIP_Classes.AFIP_TerminalMeter _terminal_meter;

      try
      {
        _terminal_meter = new AFIP_Classes.AFIP_TerminalMeter();

        if (!_terminal_meter.GetTerminalMeter(Pending, out _terminal_detail_list))
        {
          Log.Error("Terminals: Error at GetTerminalMeter, report not sent");
          return false;
        }

        Log.Message(String.Format("Terminals: Sending terminals data for PendingId: {0}", Pending.idPendiente));

        m_ws.InsertarTerminales(Pending.idPendiente, _terminal_detail_list);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error sending Terminals to JazaLocal: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Function that process pending gamingTables
    /// </summary>
    /// <param name="Pending">List of pending gamingTables</param>
    private static Boolean ProcessAndSendGamingTables(AFIP_WS_JAZALocal.PENDIENTES Pending)
    {
      AFIP_WS_JAZALocal.DETALLE_MESAS[] _gaming_tables_details_list;
      AFIP_Classes.AFIP_GamingTables _gaming_tables;

      try
      {
        _gaming_tables = new AFIP_Classes.AFIP_GamingTables();

        if (!_gaming_tables.GetGamingTablesDetails((DateTime)Pending.fechaJornada, (Int32)Pending.version, out _gaming_tables_details_list))
        {
          Log.Error("Error at GetGamingTablesDetails, report not sent");
          return false;
        }

        if (_gaming_tables_details_list.Length == 0)
        {
          _gaming_tables.GetDefaultValues((DateTime)Pending.fechaJornada, (Int32)Pending.version, out _gaming_tables_details_list);
        }

        Log.Message(String.Format("\t Sending gaming tables data for PendingId: {0}", Pending.idPendiente));

        m_ws.InsertarMesas(Pending.idPendiente, _gaming_tables_details_list);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error sending Gaming Tables to JazaLocal: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Function that processes and sends pending Requests
    /// </summary>
    /// <param name="Pending">List of pending request</param>
    private static Boolean ProcessAndSendRequest(AFIP_WS_JAZALocal.PENDIENTES Pending)
    {
      AFIP_WS_JAZALocal.DETALLE_SOLICITUDES[] _request_detail_list;
      AFIP_Classes.AFIP_Request _request;

      try
      {
        _request = new AFIP_Classes.AFIP_Request();
        if (!_request.GetRequestMeter(Pending, out _request_detail_list))
        {
          Log.Error("Request: Error at GetRequestMeter, report not sent");
          return false;
        }

        Log.Message(String.Format("Request: Sending Request data for PendienteId: {0} - RequestId: {1}", Pending.idPendiente, (Pending.idSolicitud ?? 0)));

        m_ws.InsertarSolicitudes(Pending.idPendiente, _request_detail_list);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error sending Request to JazaLocal: " + _ex.Message);
      }

      return false;
    }

    /// <summary>
    /// Process and send Bingo 
    /// </summary>
    /// <param name="Pending"></param>
    private static void ProcessAndSendBingo(AFIP_WS_JAZALocal.PENDIENTES Pending)
    {
      //Testing Bingo
      //AFIP_WS_JAZALocal.DETALLE_BINGO[] _bingo_details_list;
      //AFIP_WS_JAZALocal.DETALLE_BINGO  _bingo_detail;
      //List<AFIP_WS_JAZALocal.DETALLE_BINGO> list;
      //AFIP_WS_JAZALocal.CABECERA_BINGO _cabecera;
      //
      //
      //list = new List<AFIP_WS_JAZALocal.DETALLE_BINGO>();
      //_cabecera = new AFIP_WS_JAZALocal.CABECERA_BINGO();
      //_bingo_detail = new AFIP_WS_JAZALocal.DETALLE_BINGO();
      //
      //
      //  _bingo_detail.fechaJornada = (DateTime) Pending.fechaJornada;
      //  _bingo_detail.Version =(Int32)Pending.version;
      //  _bingo_detail.nroPartida = 1;
      //  _bingo_detail.NroSerie =1;
      //  _bingo_detail.FechaHoraInicio  = Pending.FechaHoraSolicitud;
      //  _bingo_detail.ValorCarton =1;
      //  _bingo_detail.CantidadCartonesSerie =1;
      //  _bingo_detail.NroPrimerCartonVendido =1;
      //  _bingo_detail.TotalVendido =1;
      //  _bingo_detail.TotalPremiosPagados= 1;
      //  _bingo_detail.CantidadCartonesVendidos =1;
      //  _bingo_detail.NroUltimoCartonVendido =1;
      //
      //  list.Add(_bingo_detail);
      //  _bingo_details_list = list.ToArray();
      //
      //  m_ws.InsertarBingo(Pending.idPendiente,_cabecera,_bingo_details_list);
    }

    #endregion
  }
}
