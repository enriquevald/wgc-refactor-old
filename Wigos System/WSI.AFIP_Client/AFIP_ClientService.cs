//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_ClientService.cs
// 
//   DESCRIPTION: AFIP reporting service
// 
//        AUTHOR: Dani Dominguez (Header only)
// 
// CREATION DATE: 03-MAY-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-MAY-2016 DDM    First release (Header).
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Net;
using System.ServiceProcess;
using WSI.AFIP_Client.AFIP;
using WSI.Common;

public partial class AFIP_ClientService : CommonService
{
  private const string VERSION = "18.031";
  private const string SVC_NAME = "AFIP_Client";
  private const string PROTOCOL_NAME = "AFIP_Client";
  private const int SERVICE_PORT = 13080; // _pending definir puerto
  private const Boolean STAND_BY = true;
  private const string LOG_PREFIX = "AFIP_Client";
  private const string CONFIG_FILE = "WSI.AFIP_Client_Config";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT;

  public static void Main(String[] Args)
  {
    AFIP_ClientService _this_service;

    _this_service = new AFIP_ClientService();
    _this_service.Run(Args);
  } // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
    OperationVoucherParams.LoadData = true;
  } // SetParameters

  protected override void OnServiceBeforeInit()
  {
    // RCI 10-JAN-2011: Start Mailing processing
  } // OnServiceBeforeInit

  /// <summary>
  /// Starts the service
  /// </summary>
  /// <param name="ipe"></param>
  protected override void OnServiceRunning(IPEndPoint Ipe)
  {
    AFIP_Client.Init();
  } // OnServiceRunning
} // AFIP_ClientService


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under system account
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "AFIP_Client";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "AFIP_Client";
  }
}
