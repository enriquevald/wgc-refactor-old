﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Lite.Repositories
{
	// TODO: Refactor. Basic replica from WinSys.Wigos.Gsa.S2S.Persistence but in .NET 2.0 to integrate with Wigos

	/// <summary>
	/// Logic for SequenceRepository
	/// </summary>
	/// <remarks>
	/// Multithreading safe
	/// </remarks>
	public class SequenceRepository 
	{
		private const string SequenceIdParameter = @"@sequenceId";
		private const string InitialValueParameter = "@initialValue";
		private const string GetNextValueSql = @"SELECT seq_next_value FROM sequences WHERE seq_id = @sequenceId";
		private const string SetInitialValueSql = @"INSERT INTO sequences (seq_id, seq_next_value) VALUES (@sequenceId, @initialValue)";
		private const string IncrementNextValueSql = @"UPDATE sequences SET seq_next_value = (SELECT seq_next_value + 1 FROM sequences WHERE seq_id = @sequenceId) WHERE seq_id = @sequenceId";

		private readonly Dictionary<SequenceId, object> sequencesDictionary;

		public SequenceRepository()
		{
			this.sequencesDictionary = new Dictionary<SequenceId, object>();
		}

		#region ISequenceRepository

		public long GetCurrentValue(SequenceId sequenceId)
		{
			return this.GetSequenceValue(
				sequenceId,
				isNextValue: false);
		}

		public long GetNextValue(SequenceId sequenceId)
		{
			return this.GetSequenceValue(
				sequenceId,
				isNextValue: true);
		}

		#endregion

		private object GetSequenceLock(SequenceId sequenceId)
		{
			object result;

			lock (this.sequencesDictionary)
			{
				if (this.sequencesDictionary.ContainsKey(sequenceId))
				{
					result = this.sequencesDictionary[sequenceId];
				}
				else
				{
					result = new object();
					this.sequencesDictionary.Add(
						sequenceId, 
						result);
				}
			}

			return result;
		}

		private void InitializeSequenceValueInDataBase(
			SequenceId sequenceId,
			long initialValue,
			SqlTransaction sqlTransaction)
		{
			using (SqlCommand sqlCommand = new SqlCommand(
				SequenceRepository.SetInitialValueSql,
				sqlTransaction.Connection,
				sqlTransaction))
			{
				sqlCommand.Parameters.Add(
					SequenceRepository.SequenceIdParameter,
					SqlDbType.Int).Value = sequenceId;
				sqlCommand.Parameters.Add(
					SequenceRepository.InitialValueParameter,
					SqlDbType.BigInt).Value = initialValue;

				sqlCommand.ExecuteNonQuery();
			}
		}

		private long GetSequenceValue(
			SequenceId sequenceId, 
			bool isNextValue)
		{
			long result;

			lock (this.GetSequenceLock(sequenceId))
			{
				using (DB_TRX dataBaseTransaction = new DB_TRX())
				{
					try
					{
						long? nextValueSequence = this.GetSequenceValueFromDataBase(
							sequenceId,
							dataBaseTransaction.SqlTransaction,
							isNextValue);
						if (nextValueSequence.HasValue)
						{
							result = nextValueSequence.Value;
						}
						else
						{
							result = 0;
							this.InitializeSequenceValueInDataBase(
								sequenceId, 
								1, 
								dataBaseTransaction.SqlTransaction);
						}

						dataBaseTransaction.Commit();
					}
					catch
					{
						dataBaseTransaction.Rollback();
						throw;
					}
				}
			}

			return result;
		}

		private long? GetSequenceValueFromDataBase(
			SequenceId sequenceId,
			SqlTransaction sqlTransaction,
			bool isNextValue)
		{
			long? result = null;

			StringBuilder sqlCommandBuilder = new StringBuilder(SequenceRepository.GetNextValueSql);
			if (isNextValue)
			{
				sqlCommandBuilder.Append(";" + SequenceRepository.IncrementNextValueSql);
			}

			using (SqlCommand sqlCommand = new SqlCommand(
				sqlCommandBuilder.ToString(),
				sqlTransaction.Connection,
				sqlTransaction))
			{
				sqlCommand.Parameters.Add(
					SequenceRepository.SequenceIdParameter,
					SqlDbType.Int).Value = (int)sequenceId;
				using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
				{
					if (sqlDataReader.Read())
					{
						result = isNextValue ? sqlDataReader.GetInt64(0) : sqlDataReader.GetInt64(0) - 1;
					}
				}
			}

			return result;
		}
	}
}