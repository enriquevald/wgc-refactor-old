//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PopulateDemoData.cs
// 
//   DESCRIPTION: Populate the necessary data for the iPad Demo.
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 12-APR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-APR-2011 RCI    First release.
// 31-JAN-2013 QMP    Adapted for ICE 2013.
// 29-OCT-2013 LEM    TITO movements don't modify total amount cashier session data
// 08-MAR-2017 FGB    PBI 25268: Third TAX - Payment Voucher
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Threading;
using System.Globalization;

namespace WSI.PopulateDemoData
{
  class PopulateDemoData
  {
    struct DateRange
    {
      internal DateTime from;
      internal DateTime to;
    }

    private static Boolean IsTITOMode
    {
      get { return (TITO_MODE > 0); }
    }

    #region Configuration

    //
    // XML Config file supports this parameters.
    //
    private static String m_config_file = "WSI.PopulateDemoData.cfg";

    // Exclusive finish date.
    private static DateTime FINISH_DATE;
    // Number of months to populate data: from now to @period_in_months months ago.
    private static Int32 PERIOD_IN_MONTHS = 18;

    private static Int32 TITO_MODE = 0;

    // Prefix for account holder name.
    private static String PREFIX_ACCOUNT_HOLDER_NAME = "PD_ACCOUNT_";
    // Number of accounts to use to play.
    private static Int32 NUM_ACCOUNTS = 1000;

    // SiteId: Format to '0000'.
    private static String SITE_ID = "1234";
    private static String BASE_TRACK_DATA = "878700000";

    // The demo only have one game. This is its name.
    private static String GAME_NAME = "PD_GAME";

    // Range for random the netwin percentage.
    private static Double LOWER_NETWIN_PCT = -0.10;
    private static Double UPPER_NETWIN_PCT = 0.20;

    // Prefix for terminal name.
    private static String PREFIX_TERMINAL_NAME = "PD_T";
    // Range for random the number of terminals per provider.
    private static Int32 LOWER_NUM_TERMINALS = 10;
    private static Int32 UPPER_NUM_TERMINALS = 15;

    // Range for random the number of sessions per terminal and day.
    private static Int32 LOWER_NUM_SESSIONS = 1;
    private static Int32 UPPER_NUM_SESSIONS = 10;


    // Cashier parameters.
    private static String CASHIER_USER_PROFILE_NAME = "CAJERO";
    private static String CASHIER_USER_NAME = "PD_CASHIER_USER";
    private static String CASHIER_TERMINAL_NAME = "PD_CASHIER_TERM";

    // Range for random the cashier result.
    private static Double LOWER_CASHIER_RESULT = 50000.0;
    private static Double UPPER_CASHIER_RESULT = 100000.0;

    // Range for random the cashier result.
    private static Int32 LOWER_SESSION_MOVEMENTS = 1;
    private static Int32 UPPER_SESSION_MOVEMENTS = 10;
    private static Int32 NUM_SESSIONS_PER_DAY    = 1;
    
    #endregion // Configuration

    #region Constants

    // Index Column for DataTable m_dt_accounts.
    private const Int16 AC_ACCOUNT_ID = 0;
    private const Int16 AC_HOLDER_NAME = 1;
    private const Int16 AC_TRACK_DATA = 2;
    private const Int16 AC_USER_TYPE = 3;
    private const Int16 AC_HOLDER_LEVEL = 4;
    private const Int16 AC_HOLDER_BIRTH_DATE = 5;

    // Index Column for DataTable m_dt_terminals.
    private const Int16 TE_TERMINAL_ID = 0;
    private const Int16 TE_TERMINAL_NAME = 1;
    private const Int16 TE_PROVIDER_ID = 2;

    // Index Column for DataTable m_dt_terminals_connected.
    private const Int16 TC_TERMINAL_ID = 0;
    private const Int16 TC_DATE = 1;

    // Index Column for DataTable m_dt_play_sessions.
    private const Int16 PS_ACCOUNT_ID = 0;
    private const Int16 PS_TERMINAL_ID = 1;
    private const Int16 PS_TRACK_DATA = 2;
    private const Int16 PS_STARTED = 3;
    private const Int16 PS_INITIAL_BALANCE = 4;
    private const Int16 PS_PLAYED_COUNT = 5;
    private const Int16 PS_PLAYED_AMOUNT = 6;
    private const Int16 PS_WON_COUNT = 7;
    private const Int16 PS_WON_AMOUNT = 8;
    private const Int16 PS_FINISHED = 9;
    private const Int16 PS_FINAL_BALANCE = 10;
    private const Int16 PS_REDEEMABLE_PLAYED = 11;
    private const Int16 PS_REDEEMABLE_WON = 12;
    private const Int16 PS_REDEEMABLE_TICKET_IN = 13;
    private const Int16 PS_REDEEMABLE_TICKET_OUT = 14;
    private const Int16 PS_CASH_IN = 15;

    // Index Column for DataTable m_dt_sales.
    private const Int16 SPH_BASE_HOUR = 0;
    private const Int16 SPH_TERMINAL_ID = 1;
    private const Int16 SPH_TERMINAL_NAME = 2;
    private const Int16 SPH_GAME_ID = 3;
    private const Int16 SPH_GAME_NAME = 4;
    private const Int16 SPH_PLAYED_COUNT = 5;
    private const Int16 SPH_PLAYED_AMOUNT = 6;
    private const Int16 SPH_WON_COUNT = 7;
    private const Int16 SPH_WON_AMOUNT = 8;
    private const Int16 SPH_NUM_ACTIVE_TERMINALS = 9;

    // Index Column for DataTable m_dt_cashier_sessions.
    private const Int16 CS_SESSION_ID = 0;
    private const Int16 CS_NAME = 1;
    private const Int16 CS_OPENING_DATE = 2;
    private const Int16 CS_CLOSING_DATE = 3;
    private const Int16 CS_BALANCE = 4;

    // Index Column for DataTable m_dt_cashier_movements.
    private const Int16 CM_SESSION_ID = 0;
    private const Int16 CM_DATE = 1;
    private const Int16 CM_TYPE = 2;
    private const Int16 CM_INITIAL_BALANCE = 3;
    private const Int16 CM_ADD_AMOUNT = 4;
    private const Int16 CM_SUB_AMOUNT = 5;
    private const Int16 CM_FINAL_BALANCE = 6;
    private const Int16 CM_AUX_AMOUNT = 7;

    #endregion // Constants

    private static TYPE_SPLITS m_splits;

    private static Int32 m_opening_hour;
    private static Int32 m_opening_minutes;

    private static Random m_rnd;
    private static DateTime m_rnd_base_datetime;

    private static DataTable m_dt_accounts = null;
    private static SqlDataAdapter m_sql_da_accounts = null;

    private static Int32 m_game_id = 0;

    private static Int32 m_cashier_terminal_id = 0;

    private static Int32 m_cashier_user_id = 0;

    private static Int32 m_lower_delay_per_session;
    private static Int32 m_upper_delay_per_session;
    private static Int32 m_lower_playtime_per_session;
    private static Int32 m_upper_playtime_per_session;

    private static DataTable m_dt_terminals = null;
    private static SqlDataAdapter m_sql_da_terminals = null;
    private static SqlDataAdapter m_sql_da_term_game_relation = null;

    private static DataTable m_dt_terminals_connected = null;
    private static SqlDataAdapter m_sql_da_terms_connected = null;

    private static DataTable m_dt_play_sessions = null;
    private static SqlDataAdapter m_sql_da_play_sessions = null;

    private static DataTable m_dt_sales = null;
    private static SqlDataAdapter m_sql_da_sales = null;

    private static DataTable m_dt_cashier_sessions = null;
    private static SqlDataAdapter m_sql_da_cashier_sessions = null;

    private static DataTable m_dt_cashier_movements = null;
    private static SqlDataAdapter m_sql_da_cashier_movements = null;

    private static Int32 m_num_active_terminals = 0;

    private static String m_game_name             = String.Empty;
    private static String m_cashier_terminal_name = String.Empty;
    private static String m_cashier_user_name     = String.Empty;


    internal class ProviderData
    {
      private String m_provider;
      private Int32 m_num_terminals;

      internal String Provider
      {
        get { return m_provider; }
      }
      internal Int32 NumTerminals
      {
        set { m_num_terminals = value; }
        get { return m_num_terminals; }
      }

      internal ProviderData(String Provider, Int32 NumTerminals)
      {
        m_provider = Provider;
        m_num_terminals = NumTerminals;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Main routine.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String[] Args
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static void Main(String[] Args)
    {
      DateTime _from_date;
      DateTime _to_date;
      List<ProviderData> _list_providers;
      Int32 _idx_provider;
      Int32 _aux_int;
      Double _aux_double;
      String _aux_string;
      Int32 _minutes_per_session;
      Int32 _playtime_per_session;
      Int32 _delay_per_session;
      List<Int64>[] _month_players_id;
      List<String>[] _month_players_track;
      Int64 _selected_player_id;
      String _selected_player_track;

      // Start a Console Logger
      Log.AddListener(new ConsoleLogger());

      // Configuration default values 
      StringBuilder _xml;
      _xml = new StringBuilder();
      _xml.AppendLine("<SiteConfig>");
      _xml.AppendLine("    <DBPrincipal>192.168.200.1</DBPrincipal>");
      _xml.AppendLine("    <DBMirror>192.168.200.2</DBMirror>");
      _xml.AppendLine("    <DBId>0</DBId>");
      _xml.AppendLine("    <PopulateDemo>");
      _xml.AppendLine("        <SiteId>" + SITE_ID + "</SiteId>");
      _xml.AppendLine("        <ExclusiveFinishDate>" + DateTime.Now + "</ExclusiveFinishDate>");
      _xml.AppendLine("        <PeriodInMonths>" + PERIOD_IN_MONTHS + "</PeriodInMonths>");
      _xml.AppendLine("        <PrefixAccountHolderName>" + PREFIX_ACCOUNT_HOLDER_NAME + "</PrefixAccountHolderName>");
      _xml.AppendLine("        <NumAccounts>" + NUM_ACCOUNTS + "</NumAccounts>");
      _xml.AppendLine("        <BaseTrackData>" + BASE_TRACK_DATA + "</BaseTrackData>");
      _xml.AppendLine("        <GameName>" + GAME_NAME + "</GameName>");
      _xml.AppendLine("        <LowerNetwinPct>" + LOWER_NETWIN_PCT + "</LowerNetwinPct>");
      _xml.AppendLine("        <UpperNetwinPct>" + UPPER_NETWIN_PCT + "</UpperNetwinPct>");
      _xml.AppendLine("        <PrefixTerminalName>" + PREFIX_TERMINAL_NAME + "</PrefixTerminalName>");
      _xml.AppendLine("        <LowerNumTerminals>" + LOWER_NUM_TERMINALS + "</LowerNumTerminals>");
      _xml.AppendLine("        <UpperNumTerminals>" + UPPER_NUM_TERMINALS + "</UpperNumTerminals>");
      _xml.AppendLine("        <LowerNumSessions>" + LOWER_NUM_SESSIONS + "</LowerNumSessions>");
      _xml.AppendLine("        <UpperNumSessions>" + UPPER_NUM_SESSIONS + "</UpperNumSessions>");
      _xml.AppendLine("        <CashierUserProfileName>" + CASHIER_USER_PROFILE_NAME + "</CashierUserProfileName>");
      _xml.AppendLine("        <CashierUserName>" + CASHIER_USER_NAME + "</CashierUserName>");
      _xml.AppendLine("        <CashierTerminalName>" + CASHIER_TERMINAL_NAME + "</CashierTerminalName>");
      _xml.AppendLine("        <LowerCashierResult>" + LOWER_CASHIER_RESULT + "</LowerCashierResult>");
      _xml.AppendLine("        <UpperCashierResult>" + UPPER_CASHIER_RESULT + "</UpperCashierResult>");
      _xml.AppendLine("        <NumSessionsPerDay>" + NUM_SESSIONS_PER_DAY + "</NumSessionsPerDay>");
      _xml.AppendLine("        <LowerMovementsBySessions>" + LOWER_SESSION_MOVEMENTS + "</LowerMovementsBySessions>");
      _xml.AppendLine("        <UpperMovementsBySessions>" + UPPER_SESSION_MOVEMENTS + "</UpperMovementsBySessions>");
      _xml.AppendLine("        <TITOMode>" + TITO_MODE + "</TITOMode>");
      _xml.AppendLine("    </PopulateDemo>");
      _xml.AppendLine("</SiteConfig>");

      if (!ConfigurationFile.Init(m_config_file, _xml.ToString()))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        Environment.Exit(1);
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication("PopulateDemoData", "18.001");
      WGDB.ConnectAs("WGROOT");

      // Wait for DB Connection.
      Thread.Sleep(1500);

      // Read ClosingTime: Hour and Minutes.
      if (!Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out m_opening_hour))
      {
        Log.Error(" Can't read General Parameter: WigosGUI.ClosingTime. This parameter is needed to continue.");

        Environment.Exit(1);
      }
      Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTimeMinutes"), out m_opening_minutes);

      // Read Split Parameters.
      if (!Split.ReadSplitParameters(out m_splits))
      {
        Log.Error(" Can't read Split Parameters.");

        Environment.Exit(1);
      }

      // Read optional config parameters
      _aux_string = ConfigurationFile.GetSetting("SiteId");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        SITE_ID = _aux_string;
      }
      DateTime.TryParse(ConfigurationFile.GetSetting("ExclusiveFinishDate"), out FINISH_DATE);
      if (FINISH_DATE == DateTime.MinValue)
      {
        FINISH_DATE = WGDB.Now;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("PeriodInMonths"), out _aux_int))
      {
        PERIOD_IN_MONTHS = _aux_int;
      }
      _aux_string = ConfigurationFile.GetSetting("PrefixAccountHolderName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        PREFIX_ACCOUNT_HOLDER_NAME = _aux_string;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("NumAccounts"), out _aux_int))
      {
        NUM_ACCOUNTS = _aux_int;
      }
      _aux_string = ConfigurationFile.GetSetting("BaseTrackData");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        BASE_TRACK_DATA = _aux_string;
      }
      _aux_string = ConfigurationFile.GetSetting("GameName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        GAME_NAME = _aux_string;
        m_game_name = _aux_string;
      }
      if (Double.TryParse(ConfigurationFile.GetSetting("LowerNetwinPct"), out _aux_double))
      {
        LOWER_NETWIN_PCT = _aux_double;
      }
      if (Double.TryParse(ConfigurationFile.GetSetting("UpperNetwinPct"), out _aux_double))
      {
        UPPER_NETWIN_PCT = _aux_double;
      }
      _aux_string = ConfigurationFile.GetSetting("PrefixTerminalName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        PREFIX_TERMINAL_NAME = _aux_string;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("LowerNumTerminals"), out _aux_int))
      {
        LOWER_NUM_TERMINALS = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("UpperNumTerminals"), out _aux_int))
      {
        UPPER_NUM_TERMINALS = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("LowerNumSessions"), out _aux_int))
      {
        LOWER_NUM_SESSIONS = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("UpperNumSessions"), out _aux_int))
      {
        UPPER_NUM_SESSIONS = _aux_int;
      }
      _aux_string = ConfigurationFile.GetSetting("CashierUserProfileName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        CASHIER_USER_PROFILE_NAME = _aux_string;
      }
      _aux_string = ConfigurationFile.GetSetting("CashierUserName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        CASHIER_USER_NAME = _aux_string;
        m_cashier_user_name = _aux_string;
      }
      _aux_string = ConfigurationFile.GetSetting("CashierTerminalName");
      if (!String.IsNullOrEmpty(_aux_string))
      {
        CASHIER_TERMINAL_NAME = _aux_string;
        m_cashier_terminal_name = _aux_string;
      }
      if (Double.TryParse(ConfigurationFile.GetSetting("LowerCashierResult"), out _aux_double))
      {
        LOWER_CASHIER_RESULT = _aux_double;
      }
      if (Double.TryParse(ConfigurationFile.GetSetting("UpperCashierResult"), out _aux_double))
      {
        UPPER_CASHIER_RESULT = _aux_double;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("NumSessionsPerDay"), out _aux_int))
      {
        NUM_SESSIONS_PER_DAY = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("LowerMovementsBySessions"), out _aux_int))
      {
        LOWER_SESSION_MOVEMENTS = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("UpperMovementsBySessions"), out _aux_int))
      {
        UPPER_SESSION_MOVEMENTS = _aux_int;
      }
      if (Int32.TryParse(ConfigurationFile.GetSetting("TITOMode"), out _aux_int))
      {
        TITO_MODE = _aux_int;
      }

      Log.Message("Configuration Parameters:");
      Log.Message("  DbId:                      " + ConfigurationFile.GetSetting("DBId") + ".");
      Log.Message("  SiteId:                    " + SITE_ID + ".");
      Log.Message("  ExclusiveFinishDate:       " + FINISH_DATE.ToShortDateString() + ".");
      Log.Message("  PeriodInMonths:            " + PERIOD_IN_MONTHS + ".");
      Log.Message("  NumAccounts:               " + NUM_ACCOUNTS + ".");
      Log.Message("  BaseTrackData:             " + BASE_TRACK_DATA + ".");
      Log.Message("  GameName:                  " + GAME_NAME + ".");
      Log.Message("  LowerNetwinPct:            " + LOWER_NETWIN_PCT + ".");
      Log.Message("  UpperNetwinPct:            " + UPPER_NETWIN_PCT + ".");
      Log.Message("  LowerNumTerminals:         " + LOWER_NUM_TERMINALS + ".");
      Log.Message("  UpperNumTerminals:         " + UPPER_NUM_TERMINALS + ".");
      Log.Message("  LowerNumSessions:          " + LOWER_NUM_SESSIONS + ".");
      Log.Message("  UpperNumSessions:          " + UPPER_NUM_SESSIONS + ".");
      Log.Message("  CashierUserProfileName:    " + CASHIER_USER_PROFILE_NAME + ".");
      Log.Message("  CashierUserName:           " + CASHIER_USER_NAME + ".");
      Log.Message("  CashierTerminalName:       " + CASHIER_TERMINAL_NAME + ".");
      Log.Message("  LowerCashierResult:        " + LOWER_CASHIER_RESULT + ".");
      Log.Message("  UpperCashierResult:        " + UPPER_CASHIER_RESULT + ".");
      Log.Message("  NumSessionsPerDay:         " + NUM_SESSIONS_PER_DAY + ".");
      Log.Message("  LowerMovementsBySessions:  " + LOWER_SESSION_MOVEMENTS + ".");
      Log.Message("  UpperMovementsBySessions:  " + UPPER_SESSION_MOVEMENTS + ".");
      Log.Message("  TITOMode:                  " + TITO_MODE + ".");

      _list_providers = Config_ReadProviders(m_config_file);

      // If there is no providers in the config file...
      if (_list_providers == null)
      {
        // The providers to insert.
        _list_providers = new List<ProviderData>();
        _list_providers.Add(new ProviderData("Atronic", 0));
        _list_providers.Add(new ProviderData("BCM", 0));
        _list_providers.Add(new ProviderData("Cadillac Jack", 0));
        _list_providers.Add(new ProviderData("DigiDeal", 0));
        _list_providers.Add(new ProviderData("EIBE", 0));
        _list_providers.Add(new ProviderData("Metronia", 0));
        _list_providers.Add(new ProviderData("Novomatic", 0));
        _list_providers.Add(new ProviderData("Williams", 0));
        _list_providers.Add(new ProviderData("WSI", 0));
        _list_providers.Add(new ProviderData("ZITRO", 0));
      }
      Log.Message("  Num. Providers: " + _list_providers.Count);

      // Always use the same seed to generate the same random numbers.
      m_rnd = new Random(0);
      m_rnd_base_datetime = new DateTime(2007, 01, 01);

      // Get the number of active terminals, needed to insert SPH.
      m_num_active_terminals = 0;

      foreach (ProviderData _provider_data in _list_providers)
      {
        _provider_data.NumTerminals = m_rnd.Next(LOWER_NUM_TERMINALS, UPPER_NUM_TERMINALS + 1);
        m_num_active_terminals += _provider_data.NumTerminals;

        Log.Message("    Provider: " + _provider_data.Provider + ", NumTerminals: " + _provider_data.NumTerminals + ".");
      }
      Log.Message("");

      // Get Populate Game Id.
      if (!GetPdGameId())
      {
        Log.Error(" Can't obtain the GameId for the 'Population Data' Game.");

        Environment.Exit(1);
      }

      // Get Cashier TerminalId.
      if (!GetPdCashierTerminalId())
      {
        Log.Error(" Can't obtain the Cashier TerminalId for the demo.");

        Environment.Exit(1);
      }

      // Get Cashier UserId.
      if (!GetPdCashierUserId())
      {
        Log.Error(" Can't obtain the Cashier UserId for the demo.");

        Environment.Exit(1);
      }

      // Define the delay and duration of the sessions to distribute the play_sessions during the day.

      // 1200 minutes = 20 hours: Plays during 20 hours of the day.
      _minutes_per_session = 1200 / UPPER_NUM_SESSIONS;
      // 70% of time is playing. 30% is delay between sessions.
      _playtime_per_session = (Int32)(0.7 * _minutes_per_session);
      _delay_per_session = _minutes_per_session - _playtime_per_session;

      m_lower_playtime_per_session = (Int32)(0.75 * _playtime_per_session);
      m_upper_playtime_per_session = (Int32)(1.25 * _playtime_per_session);
      m_lower_delay_per_session = (Int32)(0.75 * _delay_per_session);
      m_upper_delay_per_session = (Int32)(1.25 * _delay_per_session);

      // The GetPd*Id() routines from above need to be called before InitSqlAdapters().
      InitSqlAdapters();

      // Create accounts to use to play.
      if (!CreateAccounts())
      {
        Log.Error(" Can't create all needed accounts.");

        Environment.Exit(1);
      }

      // Trunc date to start of the day (00:00h).
      _to_date = new DateTime(FINISH_DATE.Year, FINISH_DATE.Month, FINISH_DATE.Day);
      // Number of months to populate data: from now to PERIOD_IN_MONTHS months ago.
      _from_date = _to_date.AddMonths(-PERIOD_IN_MONTHS);
      _from_date = new DateTime(_from_date.Year, _from_date.Month, 1);

      _idx_provider = 1;

      // Select which customers will play each month, randomly
      _month_players_id = new List<Int64>[PERIOD_IN_MONTHS + 1];
      _month_players_track = new List<String>[PERIOD_IN_MONTHS + 1];

      for (Int32 _idx_month = 0; _idx_month < PERIOD_IN_MONTHS + 1; _idx_month++)
      {
        _month_players_id[_idx_month] = new List<Int64>();
        _month_players_track[_idx_month] = new List<String>();

        // For a given month, only 20% of accounts will play
        for (Int32 _idx_account = 0; _idx_account < (NUM_ACCOUNTS / 5); _idx_account++)
        {
          GetRandomAccountId(out _selected_player_id, out _selected_player_track);
          _month_players_id[_idx_month].Add(_selected_player_id);
          _month_players_track[_idx_month].Add(_selected_player_track);
        }
      }

      if (NUM_ACCOUNTS > 0)
      {
        foreach (ProviderData _provider_data in _list_providers)
        {
          if (!CreateDataForProvider(_provider_data, _idx_provider, _from_date, _to_date, _month_players_id, _month_players_track))
          {
            Log.Error(" Can't create data for provider " + _provider_data.Provider +
                      ". NumTerminals: " + _provider_data.NumTerminals + ".");

            Environment.Exit(1);
          }

          _idx_provider++;
        }
      }

      for (int i = 1; i <= NUM_SESSIONS_PER_DAY; i++)
      {
        m_game_name             = GAME_NAME + i.ToString().PadLeft(3, '0');
        m_cashier_terminal_name = CASHIER_TERMINAL_NAME + i.ToString().PadLeft(3, '0');
        m_cashier_user_name     = CASHIER_USER_NAME + i.ToString().PadLeft(3, '0');
        // Insert Cashier Sessions.
        if (!CreateDataForCashierSessions(_from_date, _to_date))
        {
          Log.Error(" Can't create data for Cashier Sessions.");

          Environment.Exit(1);
        }

        m_cashier_user_id = 0;

        // Get new Cashier UserId.
        if (!GetPdCashierUserId())
        {
          Log.Error(" Can't obtain the Cashier UserId for the demo.");

          Environment.Exit(1);
        }

        Log.Message(i.ToString() + "/" + NUM_SESSIONS_PER_DAY.ToString() + " completado.");
      }

      Log.Message("Done.");
      Log.Message("");

      Log.Message("Finished. All OK!");
      System.Threading.Thread.Sleep(2500);

      Environment.Exit(0);
    } // Main

    //------------------------------------------------------------------------------
    // PURPOSE : Init the Sql Adapters needed to insert all terminals' data.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void InitSqlAdapters()
    {
      StringBuilder _sql_txt;
      SqlCommand _select_sql_cmd;
      SqlCommand _insert_sql_cmd;
      SqlCommand _update_sql_cmd;
      SqlParameter _parameter;

      //
      // ACCOUNTS
      //

      // Create the DataTable for Accounts.
      m_dt_accounts = new DataTable("ACCOUNTS");
      m_dt_accounts.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64"));
      m_dt_accounts.Columns.Add("HOLDER_NAME", Type.GetType("System.String"));
      m_dt_accounts.Columns.Add("TRACK_DATA", Type.GetType("System.String"));
      m_dt_accounts.Columns.Add("USER_TYPE", Type.GetType("System.Int32"));
      m_dt_accounts.Columns.Add("HOLDER_LEVEL", Type.GetType("System.Int32"));
      m_dt_accounts.Columns.Add("HOLDER_BIRTH_DATE", Type.GetType("System.DateTime"));

      // Create the SqlCommand to select existing Accounts.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("SELECT   AC_ACCOUNT_ID  AS ACCOUNT_ID ");
      _sql_txt.AppendLine("       , AC_HOLDER_NAME AS HOLDER_NAME ");
      _sql_txt.AppendLine("       , AC_TRACK_DATA  AS TRACK_DATA ");
      _sql_txt.AppendLine("  FROM   ACCOUNTS ");
      _sql_txt.AppendLine(" WHERE   AC_HOLDER_NAME LIKE @pPrefixAccountHolderName ");

      _select_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _select_sql_cmd.Parameters.Add("@pPrefixAccountHolderName", SqlDbType.NVarChar).Value = PREFIX_ACCOUNT_HOLDER_NAME + "%";

      // Create the SqlCommand to insert Accounts.
      _sql_txt = new StringBuilder();

      _sql_txt.AppendLine("      DECLARE   @SeqId     bigint ");
      _sql_txt.AppendLine("      UPDATE   SEQUENCES ");
      _sql_txt.AppendLine("         SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
      _sql_txt.AppendLine("       WHERE   SEQ_ID         = 4 ");
      _sql_txt.AppendLine("         SET   @SeqId      = (SELECT SequenceValue = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4) ");
      _sql_txt.AppendLine("         SET   @pAccountId  = 2000000 + @SeqId * 1000 ");

      _sql_txt.AppendLine("INSERT INTO ACCOUNTS ( AC_ACCOUNT_ID ");
      _sql_txt.AppendLine("                     , AC_TYPE ");
      _sql_txt.AppendLine("                     , AC_HOLDER_NAME ");
      _sql_txt.AppendLine("                     , AC_BLOCKED ");
      _sql_txt.AppendLine("                     , AC_TRACK_DATA ");
      _sql_txt.AppendLine("                     , AC_BALANCE ");
      _sql_txt.AppendLine("                     , AC_INITIAL_CASH_IN ");
      _sql_txt.AppendLine("                     , AC_USER_TYPE ");
      _sql_txt.AppendLine("                     , AC_HOLDER_LEVEL ");
      _sql_txt.AppendLine("                     , AC_HOLDER_BIRTH_DATE");
      _sql_txt.AppendLine("                     ) ");
      _sql_txt.AppendLine("              VALUES ( @pAccountId ");
      _sql_txt.AppendLine("                     , 2 ");
      _sql_txt.AppendLine("                     , @pHolderName ");
      _sql_txt.AppendLine("                     , 0 ");
      _sql_txt.AppendLine("                     , @pTrackData ");
      _sql_txt.AppendLine("                     , 1000000 ");
      _sql_txt.AppendLine("                     , 1000000 ");
      _sql_txt.AppendLine("                     , @pUserType ");
      _sql_txt.AppendLine("                     , @pHolderLevel ");
      _sql_txt.AppendLine("                     , @pHolderBirthDate ");
      _sql_txt.AppendLine("                     ) ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).SourceColumn = "HOLDER_NAME";
      _insert_sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).SourceColumn = "TRACK_DATA";
      _insert_sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).SourceColumn = "USER_TYPE";
      _insert_sql_cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "HOLDER_LEVEL";
      _insert_sql_cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).SourceColumn = "HOLDER_BIRTH_DATE";

      _parameter = _insert_sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt);
      _parameter.SourceColumn = "ACCOUNT_ID";
      _parameter.Direction = ParameterDirection.Output;

      // Create the SqlCommand to update Accounts.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE   ACCOUNTS ");
      _sql_txt.AppendLine("   SET   AC_TYPE            = 2 ");
      _sql_txt.AppendLine("       , AC_HOLDER_NAME     = @pHolderName ");
      _sql_txt.AppendLine("       , AC_BLOCKED         = 0 ");
      _sql_txt.AppendLine("       , AC_TRACK_DATA      = @pTrackData ");
      _sql_txt.AppendLine("       , AC_BALANCE         = 1000000 ");
      _sql_txt.AppendLine("       , AC_INITIAL_CASH_IN = 1000000 ");
      _sql_txt.AppendLine("       , AC_USER_TYPE         = @pUserType ");
      _sql_txt.AppendLine("       , AC_HOLDER_LEVEL      = @pHolderLevel ");
      _sql_txt.AppendLine("       , AC_HOLDER_BIRTH_DATE = @pHolderBirthDate ");
      _sql_txt.AppendLine(" WHERE   AC_ACCOUNT_ID      = @pAccountId ");

      _update_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _update_sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "ACCOUNT_ID";
      _update_sql_cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).SourceColumn = "HOLDER_NAME";
      _update_sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).SourceColumn = "TRACK_DATA";
      _update_sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).SourceColumn = "USER_TYPE";
      _update_sql_cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "HOLDER_LEVEL";
      _update_sql_cmd.Parameters.Add("@pHolderBirthDate", SqlDbType.DateTime).SourceColumn = "HOLDER_BIRTH_DATE";

      // Create the SqlDataAdapter to insert Accounts.
      m_sql_da_accounts = new SqlDataAdapter();
      m_sql_da_accounts.SelectCommand = _select_sql_cmd;
      m_sql_da_accounts.InsertCommand = _insert_sql_cmd;
      m_sql_da_accounts.UpdateCommand = _update_sql_cmd;
      m_sql_da_accounts.DeleteCommand = null;

      m_sql_da_accounts.ContinueUpdateOnError = false;
      m_sql_da_accounts.UpdateBatchSize = 500;
      m_sql_da_accounts.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
      m_sql_da_accounts.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // TERMINALS AND TERMINAL-GAME RELATION
      //

      // Create the DataTable for the Terminals.
      m_dt_terminals = new DataTable("TERMINALS");
      m_dt_terminals.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_terminals.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));
      m_dt_terminals.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));

      // Create the SqlCommand to select existing Terminals.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("SELECT   TE_TERMINAL_ID AS TERMINAL_ID ");
      _sql_txt.AppendLine("       , TE_NAME        AS TERMINAL_NAME ");
      _sql_txt.AppendLine("       , TE_PROVIDER_ID AS PROVIDER_ID ");
      _sql_txt.AppendLine("  FROM   TERMINALS ");
      _sql_txt.AppendLine(" WHERE   TE_NAME     LIKE @pPrefixTerminalName ");

      _select_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _select_sql_cmd.Parameters.Add("@pPrefixTerminalName", SqlDbType.NVarChar).Value = PREFIX_TERMINAL_NAME + "%";

      // Create the SqlCommand to insert Terminals.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("INSERT INTO TERMINALS ( TE_TYPE ");
      _sql_txt.AppendLine("                      , TE_BASE_NAME ");
      _sql_txt.AppendLine("                      , TE_EXTERNAL_ID ");
      _sql_txt.AppendLine("                      , TE_BLOCKED ");
      _sql_txt.AppendLine("                      , TE_ACTIVE ");
      _sql_txt.AppendLine("                      , TE_PROVIDER_ID ");
      _sql_txt.AppendLine("                      , TE_TERMINAL_TYPE ");
      _sql_txt.AppendLine("                      , TE_VENDOR_ID ");
      _sql_txt.AppendLine("                      , TE_STATUS ");
      _sql_txt.AppendLine("                      , TE_MASTER_ID ");
      _sql_txt.AppendLine("                      , TE_CHANGE_ID ");
      _sql_txt.AppendLine("                      , TE_ISO_CODE ");
      _sql_txt.AppendLine("                      ) ");
      _sql_txt.AppendLine("               VALUES ( 1 ");
      _sql_txt.AppendLine("                      , @pTerminalName ");
      _sql_txt.AppendLine("                      , @pTerminalName ");
      _sql_txt.AppendLine("                      , 0 ");
      _sql_txt.AppendLine("                      , 1 ");
      _sql_txt.AppendLine("                      , @pProviderId ");
      _sql_txt.AppendLine("                      , 5 ");             // SAS-HOST
      _sql_txt.AppendLine("                      , @pProviderId ");
      _sql_txt.AppendLine("                      , 2 ");             // RETIRED
      _sql_txt.AppendLine("                      , 0 ");             // MASTER ID
      _sql_txt.AppendLine("                      , 0 ");             // VERSION
      _sql_txt.AppendLine("                      , @pIsoCode ");      // Iso Code
      _sql_txt.AppendLine("                      ) ");
      _sql_txt.AppendLine(" SET     @pTerminalId = SCOPE_IDENTITY()");

      _sql_txt.AppendLine("UPDATE   TERMINALS ");
      _sql_txt.AppendLine("   SET   TE_MASTER_ID = TE_TERMINAL_ID ");
      _sql_txt.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _insert_sql_cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).SourceColumn = "PROVIDER_ID";
      _insert_sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();

      _parameter = _insert_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int);
      _parameter.SourceColumn = "TERMINAL_ID";
      _parameter.Direction = ParameterDirection.Output;

      // Create the SqlCommand to update Terminals.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE   TERMINALS ");
      _sql_txt.AppendLine("   SET   TE_TYPE          = 1 ");
      _sql_txt.AppendLine("       , TE_EXTERNAL_ID   = @pTerminalName ");
      _sql_txt.AppendLine("       , TE_BLOCKED       = 0 ");
      _sql_txt.AppendLine("       , TE_ACTIVE        = 1 ");
      _sql_txt.AppendLine("       , TE_PROVIDER_ID   = @pProviderId ");
      _sql_txt.AppendLine("       , TE_TERMINAL_TYPE = 5 ");
      _sql_txt.AppendLine("       , TE_VENDOR_ID     = @pProviderId ");
      _sql_txt.AppendLine("       , TE_STATUS        = 2 ");
      _sql_txt.AppendLine(" WHERE   TE_TERMINAL_ID   = @pTerminalId ");

      _update_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _update_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _update_sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _update_sql_cmd.Parameters.Add("@pProviderId", SqlDbType.NVarChar).SourceColumn = "PROVIDER_ID";

      // Create the SqlDataAdapter to insert Terminals.
      m_sql_da_terminals = new SqlDataAdapter();
      m_sql_da_terminals.SelectCommand = _select_sql_cmd;
      m_sql_da_terminals.InsertCommand = _insert_sql_cmd;
      m_sql_da_terminals.UpdateCommand = _update_sql_cmd;
      m_sql_da_terminals.DeleteCommand = null;

      m_sql_da_terminals.ContinueUpdateOnError = false;
      m_sql_da_terminals.UpdateBatchSize = 500;
      m_sql_da_terminals.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
      m_sql_da_terminals.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      // Create the SqlCommand to insert Terminal-Game Relation.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("IF NOT EXISTS ( ");
      _sql_txt.AppendLine("  SELECT   1 ");
      _sql_txt.AppendLine("    FROM   TERMINAL_GAME_TRANSLATION ");
      _sql_txt.AppendLine("   WHERE   TGT_TERMINAL_ID    = @pTerminalId ");
      _sql_txt.AppendLine("     AND   TGT_SOURCE_GAME_ID = @pGameId ");
      _sql_txt.AppendLine("  ) ");
      _sql_txt.AppendLine("BEGIN ");
      _sql_txt.AppendLine("  INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID ");
      _sql_txt.AppendLine("                                        , TGT_SOURCE_GAME_ID ");
      _sql_txt.AppendLine("                                        , TGT_TARGET_GAME_ID ");
      _sql_txt.AppendLine("                                        ) ");
      _sql_txt.AppendLine("                                 VALUES ( @pTerminalId ");
      _sql_txt.AppendLine("                                        , @pGameId ");
      _sql_txt.AppendLine("                                        , @pGameId ");
      _sql_txt.AppendLine("                                        ) ");
      _sql_txt.AppendLine("END ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _insert_sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = m_game_id;

      // Create the SqlDataAdapter to insert Terminal-Game Relation.
      m_sql_da_term_game_relation = new SqlDataAdapter();
      m_sql_da_term_game_relation.SelectCommand = null;
      m_sql_da_term_game_relation.InsertCommand = _insert_sql_cmd;
      m_sql_da_term_game_relation.UpdateCommand = null;
      m_sql_da_term_game_relation.DeleteCommand = null;

      m_sql_da_term_game_relation.ContinueUpdateOnError = false;
      m_sql_da_term_game_relation.UpdateBatchSize = 500;
      m_sql_da_term_game_relation.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // TERMINALS CONNECTED
      //

      // Create the DataTable for the Terminals Connected.
      m_dt_terminals_connected = new DataTable("TERMINALS_CONNECTED");
      m_dt_terminals_connected.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_terminals_connected.Columns.Add("DATE", Type.GetType("System.DateTime"));

      // Create the SqlCommand to insert Terminals Connected.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("IF NOT EXISTS ( ");
      _sql_txt.AppendLine("  SELECT   1 ");
      _sql_txt.AppendLine("    FROM   TERMINALS_CONNECTED ");
      _sql_txt.AppendLine("   WHERE   TC_DATE        = @pDate ");
      _sql_txt.AppendLine("     AND   TC_TERMINAL_ID = @pTerminalId ");
      _sql_txt.AppendLine("  ) ");
      _sql_txt.AppendLine("BEGIN ");
      _sql_txt.AppendLine("  INSERT INTO TERMINALS_CONNECTED ( TC_DATE ");
      _sql_txt.AppendLine("                                  , TC_TERMINAL_ID ");
      _sql_txt.AppendLine("                                  , TC_STATUS ");
      _sql_txt.AppendLine("                                  , TC_CONNECTED ");
      _sql_txt.AppendLine("                                  , TC_MASTER_ID ");
      _sql_txt.AppendLine("                                  ) ");
      _sql_txt.AppendLine("                           VALUES ( @pDate ");
      _sql_txt.AppendLine("                                  , @pTerminalId ");
      _sql_txt.AppendLine("                                  , 0 ");
      _sql_txt.AppendLine("                                  , 1 ");
      _sql_txt.AppendLine("                                  , @pTerminalId ");
      _sql_txt.AppendLine("                                  ) ");
      _sql_txt.AppendLine("END ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _insert_sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "DATE";

      // Create the SqlDataAdapter to insert Terminals Connected.
      m_sql_da_terms_connected = new SqlDataAdapter();
      m_sql_da_terms_connected.SelectCommand = null;
      m_sql_da_terms_connected.InsertCommand = _insert_sql_cmd;
      m_sql_da_terms_connected.UpdateCommand = null;
      m_sql_da_terms_connected.DeleteCommand = null;

      m_sql_da_terms_connected.ContinueUpdateOnError = false;
      m_sql_da_terms_connected.UpdateBatchSize = 500;
      m_sql_da_terms_connected.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // PLAY SESSIONS
      //

      // Create the DataTable for the Play Sessions.
      m_dt_play_sessions = new DataTable("PLAY_SESSIONS");
      m_dt_play_sessions.Columns.Add("ACCOUNT_ID", Type.GetType("System.Int64"));
      m_dt_play_sessions.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_play_sessions.Columns.Add("TRACK_DATA", Type.GetType("System.String"));
      m_dt_play_sessions.Columns.Add("STARTED", Type.GetType("System.DateTime"));
      m_dt_play_sessions.Columns.Add("INITIAL_BALANCE", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_play_sessions.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_play_sessions.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("FINISHED", Type.GetType("System.DateTime"));
      m_dt_play_sessions.Columns.Add("FINAL_BALANCE", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("REDEEMABLE_PLAYED", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("REDEEMABLE_WON", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("REDEEMABLE_TICKET_IN", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("REDEEMABLE_TICKET_OUT", Type.GetType("System.Decimal"));
      m_dt_play_sessions.Columns.Add("CASH_IN", Type.GetType("System.Decimal"));
      
      // Create the SqlCommand to insert play sessions.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID ");
      _sql_txt.AppendLine("                          , PS_TERMINAL_ID ");
      _sql_txt.AppendLine("                          , PS_TYPE ");
      _sql_txt.AppendLine("                          , PS_TYPE_DATA ");
      _sql_txt.AppendLine("                          , PS_STATUS ");
      _sql_txt.AppendLine("                          , PS_STARTED ");
      _sql_txt.AppendLine("                          , PS_INITIAL_BALANCE ");
      _sql_txt.AppendLine("                          , PS_PLAYED_COUNT ");
      _sql_txt.AppendLine("                          , PS_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("                          , PS_WON_COUNT ");
      _sql_txt.AppendLine("                          , PS_WON_AMOUNT ");
      _sql_txt.AppendLine("                          , PS_FINISHED ");
      _sql_txt.AppendLine("                          , PS_FINAL_BALANCE ");
      _sql_txt.AppendLine("                          , PS_LOCKED ");
      _sql_txt.AppendLine("                          , PS_STAND_ALONE ");
      _sql_txt.AppendLine("                          , PS_PROMO ");
      _sql_txt.AppendLine("                          , PS_REDEEMABLE_PLAYED ");
      _sql_txt.AppendLine("                          , PS_REDEEMABLE_WON ");
      _sql_txt.AppendLine("                          , PS_REDEEMABLE_CASH_IN ");
      _sql_txt.AppendLine("                          , PS_REDEEMABLE_CASH_OUT ");
      _sql_txt.AppendLine("                          , PS_RE_TICKET_IN ");
      _sql_txt.AppendLine("                          , PS_RE_TICKET_OUT ");
      _sql_txt.AppendLine("                          , PS_CASH_IN ");
      _sql_txt.AppendLine("                          ) ");
      _sql_txt.AppendLine("                   VALUES ( @pAccountId ");
      _sql_txt.AppendLine("                          , @pTerminalId ");
      _sql_txt.AppendLine("                          , 2 ");             // 2 = TYPE_WIN
      _sql_txt.AppendLine("                          , @pTrackData ");
      _sql_txt.AppendLine("                          , 1 ");             // 1 = Closed
      _sql_txt.AppendLine("                          , @pStartDate ");
      _sql_txt.AppendLine("                          , @pInitialBalance ");
      _sql_txt.AppendLine("                          , @pPlayedCount ");
      _sql_txt.AppendLine("                          , @pPlayedAmount ");
      _sql_txt.AppendLine("                          , @pWonCount ");
      _sql_txt.AppendLine("                          , @pWonAmount ");
      _sql_txt.AppendLine("                          , @pFinishDate ");
      _sql_txt.AppendLine("                          , @pFinalBalance ");
      _sql_txt.AppendLine("                          , NULL ");
      _sql_txt.AppendLine("                          , 1 ");
      _sql_txt.AppendLine("                          , 0 ");
      _sql_txt.AppendLine("                          , @pRedeemablePlayed ");
      _sql_txt.AppendLine("                          , @pRedeemableWon ");
      _sql_txt.AppendLine("                          , @pRedeemablePlayed ");
      _sql_txt.AppendLine("                          , @pRedeemableWon ");
      _sql_txt.AppendLine("                          , @pRedeemableTicketIn ");
      _sql_txt.AppendLine("                          , @pRedeemableTicketOut ");
      _sql_txt.AppendLine("                          , @pCashIn ");
      _sql_txt.AppendLine("                          ) ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "ACCOUNT_ID";
      _insert_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _insert_sql_cmd.Parameters.Add("@pTrackData", SqlDbType.Xml).SourceColumn = "TRACK_DATA";
      _insert_sql_cmd.Parameters.Add("@pStartDate", SqlDbType.DateTime).SourceColumn = "STARTED";
      _insert_sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "INITIAL_BALANCE";
      _insert_sql_cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _insert_sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _insert_sql_cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _insert_sql_cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _insert_sql_cmd.Parameters.Add("@pFinishDate", SqlDbType.DateTime).SourceColumn = "FINISHED";
      _insert_sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "FINAL_BALANCE";
      _insert_sql_cmd.Parameters.Add("@pRedeemablePlayed", SqlDbType.Money).SourceColumn = "REDEEMABLE_PLAYED";
      _insert_sql_cmd.Parameters.Add("@pRedeemableWon", SqlDbType.Money).SourceColumn = "REDEEMABLE_WON";
      _insert_sql_cmd.Parameters.Add("@pRedeemableTicketIn", SqlDbType.Money).SourceColumn = "REDEEMABLE_TICKET_IN";
      _insert_sql_cmd.Parameters.Add("@pRedeemableTicketOut", SqlDbType.Money).SourceColumn = "REDEEMABLE_TICKET_OUT";
      _insert_sql_cmd.Parameters.Add("@pCashIn", SqlDbType.Money).SourceColumn = "CASH_IN";

      // Create the SqlDataAdapter to insert play sessions.
      m_sql_da_play_sessions = new SqlDataAdapter();
      m_sql_da_play_sessions.SelectCommand = null;
      m_sql_da_play_sessions.InsertCommand = _insert_sql_cmd;
      m_sql_da_play_sessions.UpdateCommand = null;
      m_sql_da_play_sessions.DeleteCommand = null;

      m_sql_da_play_sessions.ContinueUpdateOnError = false;
      m_sql_da_play_sessions.UpdateBatchSize = 1000;
      m_sql_da_play_sessions.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // SALES PER HOUR
      //

      // Create the DataTable for the Sales Per Hour.
      m_dt_sales = new DataTable("SALES_PER_HOUR");
      m_dt_sales.Columns.Add("BASE_HOUR", Type.GetType("System.DateTime"));
      m_dt_sales.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_sales.Columns.Add("TERMINAL_NAME", Type.GetType("System.String"));
      m_dt_sales.Columns.Add("GAME_ID", Type.GetType("System.Int32"));
      m_dt_sales.Columns.Add("GAME_NAME", Type.GetType("System.String"));
      m_dt_sales.Columns.Add("PLAYED_COUNT", Type.GetType("System.Int64"));
      m_dt_sales.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_sales.Columns.Add("WON_COUNT", Type.GetType("System.Int64"));
      m_dt_sales.Columns.Add("WON_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_sales.Columns.Add("NUM_ACTIVE_TERMINALS", Type.GetType("System.Int32"));

      m_dt_sales.PrimaryKey = new DataColumn[] { m_dt_sales.Columns["BASE_HOUR"],
                                                 m_dt_sales.Columns["TERMINAL_ID"],
                                                 m_dt_sales.Columns["GAME_ID"] };

      // Create the SqlCommand to insert sales per hour.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("INSERT INTO SALES_PER_HOUR ( SPH_BASE_HOUR ");
      _sql_txt.AppendLine("                           , SPH_TERMINAL_ID ");
      _sql_txt.AppendLine("                           , SPH_TERMINAL_NAME ");
      _sql_txt.AppendLine("                           , SPH_GAME_ID ");
      _sql_txt.AppendLine("                           , SPH_GAME_NAME ");
      _sql_txt.AppendLine("                           , SPH_PLAYED_COUNT ");
      _sql_txt.AppendLine("                           , SPH_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("                           , SPH_WON_COUNT ");
      _sql_txt.AppendLine("                           , SPH_WON_AMOUNT ");
      _sql_txt.AppendLine("                           , SPH_NUM_ACTIVE_TERMINALS ");
      _sql_txt.AppendLine("                           , SPH_LAST_PLAY_ID ");
      _sql_txt.AppendLine("                           ) ");
      _sql_txt.AppendLine("                    VALUES ( @pBaseHour ");
      _sql_txt.AppendLine("                           , @pTerminalId ");
      _sql_txt.AppendLine("                           , @pTerminalName ");
      _sql_txt.AppendLine("                           , @pGameId ");
      _sql_txt.AppendLine("                           , @pGameName ");
      _sql_txt.AppendLine("                           , @pPlayedCount ");
      _sql_txt.AppendLine("                           , @pPlayedAmount ");
      _sql_txt.AppendLine("                           , @pWonCount ");
      _sql_txt.AppendLine("                           , @pWonAmount ");
      _sql_txt.AppendLine("                           , @pNumActiveTerminals ");
      _sql_txt.AppendLine("                           , 0 ");
      _sql_txt.AppendLine("                           ) ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
      _insert_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      _insert_sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).SourceColumn = "TERMINAL_NAME";
      _insert_sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";
      _insert_sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).SourceColumn = "GAME_NAME";
      _insert_sql_cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "PLAYED_COUNT";
      _insert_sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "PLAYED_AMOUNT";
      _insert_sql_cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "WON_COUNT";
      _insert_sql_cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "WON_AMOUNT";
      _insert_sql_cmd.Parameters.Add("@pNumActiveTerminals", SqlDbType.Int).SourceColumn = "NUM_ACTIVE_TERMINALS";

      // Create the SqlDataAdapter to insert sales per hour.
      m_sql_da_sales = new SqlDataAdapter();
      m_sql_da_sales.SelectCommand = null;
      m_sql_da_sales.InsertCommand = _insert_sql_cmd;
      m_sql_da_sales.UpdateCommand = null;
      m_sql_da_sales.DeleteCommand = null;

      m_sql_da_sales.ContinueUpdateOnError = false;
      m_sql_da_sales.UpdateBatchSize = 1000;
      m_sql_da_sales.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // CASHIER_SESSIONS
      //

      // Create the DataTable for the Cashier Sessions.
      m_dt_cashier_sessions = new DataTable("CASHIER_SESSIONS");
      m_dt_cashier_sessions.Columns.Add("SESSION_ID", Type.GetType("System.Int64"));
      m_dt_cashier_sessions.Columns.Add("NAME", Type.GetType("System.String"));
      m_dt_cashier_sessions.Columns.Add("OPENING_DATE", Type.GetType("System.DateTime"));
      m_dt_cashier_sessions.Columns.Add("CLOSING_DATE", Type.GetType("System.DateTime"));
      m_dt_cashier_sessions.Columns.Add("BALANCE", Type.GetType("System.Decimal"));

      // Create the SqlCommand to insert cashier sessions.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine(" INSERT INTO CASHIER_SESSIONS ( CS_NAME          ");
      _sql_txt.AppendLine("                              , CS_CASHIER_ID    ");
      _sql_txt.AppendLine("                              , CS_USER_ID       ");
      _sql_txt.AppendLine("                              , CS_OPENING_DATE  ");
      _sql_txt.AppendLine("                              , CS_CLOSING_DATE  ");
      _sql_txt.AppendLine("                              , CS_STATUS        ");
      _sql_txt.AppendLine("                              , CS_BALANCE       ");
      _sql_txt.AppendLine("                              , CS_TAX_A_PCT     ");
      _sql_txt.AppendLine("                              , CS_TAX_B_PCT     ");
      _sql_txt.AppendLine("                              )                  ");
      _sql_txt.AppendLine("                       VALUES ( @pName           ");
      _sql_txt.AppendLine("                              , @pCashierId      ");
      _sql_txt.AppendLine("                              , @pUserId         ");
      _sql_txt.AppendLine("                              , @pOpeningDate    ");
      _sql_txt.AppendLine("                              , @pClosingDate    ");
      _sql_txt.AppendLine("                              , @pStatusClosed   ");
      _sql_txt.AppendLine("                              , 0                ");
      _sql_txt.AppendLine("                              , @pTaxAPct        ");
      _sql_txt.AppendLine("                              , @pTaxBPct        ");
      _sql_txt.AppendLine("                              )                  ");
      _sql_txt.AppendLine("  SET             @pSessionId = SCOPE_IDENTITY() ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pName", SqlDbType.NVarChar).SourceColumn = "NAME";
      _insert_sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = m_cashier_terminal_id;
      _insert_sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_cashier_user_id;
      _insert_sql_cmd.Parameters.Add("@pOpeningDate", SqlDbType.DateTime).SourceColumn = "OPENING_DATE";
      _insert_sql_cmd.Parameters.Add("@pClosingDate", SqlDbType.DateTime).SourceColumn = "CLOSING_DATE";
      _insert_sql_cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.CLOSED;
      _insert_sql_cmd.Parameters.Add("@pTaxAPct", SqlDbType.Decimal).Value = m_splits.company_a.tax_pct;
      _insert_sql_cmd.Parameters.Add("@pTaxBPct", SqlDbType.Decimal).Value = m_splits.enabled_b ? m_splits.company_b.tax_pct : 0;

      _parameter = _insert_sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
      _parameter.SourceColumn = "SESSION_ID";
      _parameter.Direction = ParameterDirection.Output;

      // Create the SqlCommand to update cashier sessions.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE   CASHIER_SESSIONS ");
      _sql_txt.AppendLine("   SET   CS_BALANCE    = @pBalance ");
      _sql_txt.AppendLine(" WHERE   CS_SESSION_ID = @pSessionId ");

      _update_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _update_sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
      _update_sql_cmd.Parameters.Add("@pBalance", SqlDbType.Money).SourceColumn = "BALANCE";

      // Create the SqlDataAdapter to insert/update cashier sessions.
      m_sql_da_cashier_sessions = new SqlDataAdapter();
      m_sql_da_cashier_sessions.SelectCommand = null;
      m_sql_da_cashier_sessions.InsertCommand = _insert_sql_cmd;
      m_sql_da_cashier_sessions.UpdateCommand = _update_sql_cmd;
      m_sql_da_cashier_sessions.DeleteCommand = null;

      m_sql_da_cashier_sessions.ContinueUpdateOnError = false;
      m_sql_da_cashier_sessions.UpdateBatchSize = 1000;
      m_sql_da_cashier_sessions.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
      m_sql_da_cashier_sessions.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      //
      // CASHIER MOVEMENTS
      //

      // Create the DataTable for the Cashier Movements.
      m_dt_cashier_movements = new DataTable("CASHIER_MOVEMENTS");
      m_dt_cashier_movements.Columns.Add("SESSION_ID", Type.GetType("System.Int64"));
      m_dt_cashier_movements.Columns.Add("DATE", Type.GetType("System.DateTime"));
      m_dt_cashier_movements.Columns.Add("TYPE", Type.GetType("System.Int32"));
      m_dt_cashier_movements.Columns.Add("INITIAL_BALANCE", Type.GetType("System.Decimal"));
      m_dt_cashier_movements.Columns.Add("ADD_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_cashier_movements.Columns.Add("SUB_AMOUNT", Type.GetType("System.Decimal"));
      m_dt_cashier_movements.Columns.Add("FINAL_BALANCE", Type.GetType("System.Decimal"));

      // Create the SqlCommand to insert cashier movements.
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("INSERT INTO CASHIER_MOVEMENTS ( CM_SESSION_ID ");
      _sql_txt.AppendLine("                              , CM_CASHIER_ID ");
      _sql_txt.AppendLine("                              , CM_USER_ID ");
      _sql_txt.AppendLine("                              , CM_DATE ");
      _sql_txt.AppendLine("                              , CM_TYPE ");
      _sql_txt.AppendLine("                              , CM_INITIAL_BALANCE ");
      _sql_txt.AppendLine("                              , CM_ADD_AMOUNT ");
      _sql_txt.AppendLine("                              , CM_SUB_AMOUNT ");
      _sql_txt.AppendLine("                              , CM_FINAL_BALANCE ");
      _sql_txt.AppendLine("                              , CM_USER_NAME ");
      _sql_txt.AppendLine("                              , CM_CASHIER_NAME ");
      _sql_txt.AppendLine("                              , CM_CARD_TRACK_DATA ");
      _sql_txt.AppendLine("                              , CM_ACCOUNT_ID ");
      _sql_txt.AppendLine("                              , CM_ACCOUNT_MOVEMENT_ID ");
      _sql_txt.AppendLine("                              , CM_OPERATION_ID ");
      _sql_txt.AppendLine("                              ) ");
      _sql_txt.AppendLine("                       VALUES ( @pSessionId ");
      _sql_txt.AppendLine("                              , @pCashierId ");
      _sql_txt.AppendLine("                              , @pUserId ");
      _sql_txt.AppendLine("                              , @pDate ");
      _sql_txt.AppendLine("                              , @pType ");
      _sql_txt.AppendLine("                              , @pInitialBalance ");
      _sql_txt.AppendLine("                              , @pAddAmount ");
      _sql_txt.AppendLine("                              , @pSubAmount ");
      _sql_txt.AppendLine("                              , @pFinalBalance ");
      _sql_txt.AppendLine("                              , @pUserName ");
      _sql_txt.AppendLine("                              , @pCashierName ");
      _sql_txt.AppendLine("                              , NULL ");
      _sql_txt.AppendLine("                              , NULL ");
      _sql_txt.AppendLine("                              , NULL ");
      _sql_txt.AppendLine("                              , NULL ");
      _sql_txt.AppendLine("                              ) ");

      _insert_sql_cmd = new SqlCommand(_sql_txt.ToString());
      _insert_sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
      _insert_sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = m_cashier_terminal_id;
      _insert_sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_cashier_user_id;
      _insert_sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "DATE";
      _insert_sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE";

      //_insert_sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "INITIAL_BALANCE";

      //_insert_sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "ADD_AMOUNT";

      //_insert_sql_cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "SUB_AMOUNT";

      //_insert_sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "FINAL_BALANCE";
      
      SqlParameter parameter_initial_balance = new SqlParameter("@pInitialBalance", SqlDbType.Decimal);
      parameter_initial_balance.SourceColumn= "INITIAL_BALANCE";
      parameter_initial_balance.Precision = 12;
      parameter_initial_balance.Scale = 2;

      SqlParameter parameter_add_amount = new SqlParameter("@pAddAmount", SqlDbType.Decimal);
      parameter_add_amount.SourceColumn = "ADD_AMOUNT";
      parameter_add_amount.Precision = 12;
      parameter_add_amount.Scale = 2;

      SqlParameter parameter_sub_amount = new SqlParameter("@pSubAmount", SqlDbType.Decimal);
      parameter_sub_amount.SourceColumn = "SUB_AMOUNT";
      parameter_sub_amount.Precision = 12;
      parameter_sub_amount.Scale = 2;

      SqlParameter parameter_final_balance= new SqlParameter("@pFinalBalance", SqlDbType.Decimal);
      parameter_final_balance.SourceColumn= "FINAL_BALANCE";
      parameter_final_balance.Precision = 12;
      parameter_final_balance.Scale = 2;

      _insert_sql_cmd.Parameters.Add(parameter_initial_balance);
      _insert_sql_cmd.Parameters.Add(parameter_add_amount);
      _insert_sql_cmd.Parameters.Add(parameter_sub_amount);
      _insert_sql_cmd.Parameters.Add(parameter_final_balance);


      _insert_sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar).Value = m_cashier_user_name;
      _insert_sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar).Value = m_cashier_terminal_name;

      
      // Create the SqlDataAdapter to insert cashier movements.
      m_sql_da_cashier_movements = new SqlDataAdapter();
      m_sql_da_cashier_movements.SelectCommand = null;
      m_sql_da_cashier_movements.InsertCommand = _insert_sql_cmd;
      m_sql_da_cashier_movements.UpdateCommand = null;
      m_sql_da_cashier_movements.DeleteCommand = null;

      m_sql_da_cashier_movements.ContinueUpdateOnError = false;
      m_sql_da_cashier_movements.UpdateBatchSize = 1000;
      m_sql_da_cashier_movements.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

    } // InitSqlAdapters

    //------------------------------------------------------------------------------
    // PURPOSE : Create the accounts to use to play.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: All the accounts are created. False: Otherwise.
    //
    private static Boolean CreateAccounts()
    {
      String _base_track_data;
      String _track_data;
      UInt32 _idx_account;
      String _holder_name;
      UInt64 _base_card;
      UInt64 _track_number;
      DataRow _data_row;
      DataRow[] _rows;
      Int32 _num_rows_updated;
      Int32 _random_holder_level;
      Int32 _random_holder_age;

      // Base track data
      _base_track_data = SITE_ID + BASE_TRACK_DATA;

      Log.Message("Creating " + NUM_ACCOUNTS + " Accounts to use to play. BaseTrackData: " + _base_track_data + ".");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _db_trx.Fill(m_sql_da_accounts, m_dt_accounts);
      }

      try
      {
        if (!UInt64.TryParse(_base_track_data, out _base_card))
        {
          Log.Message("BaseTrackData (" + _base_track_data + ") must be an integer greater than 0.");

          return false;
        }

        // Insert values to the DataTable.
        _idx_account = 0;
        while (_idx_account < NUM_ACCOUNTS)
        {
          _holder_name = PREFIX_ACCOUNT_HOLDER_NAME + _idx_account.ToString("0000");
          _track_number = _base_card + _idx_account;
          _track_data = _track_number.ToString("0000000000000");

          _rows = m_dt_accounts.Select("HOLDER_NAME = '" + _holder_name + "'");
          if (_rows.Length == 0)
          {
            _data_row = m_dt_accounts.NewRow();
            _data_row[AC_HOLDER_NAME] = _holder_name;
            _data_row[AC_TRACK_DATA] = _track_data;

            // Make accounts personalized and assign them to different levels
            _random_holder_level = m_rnd.Next(100);

            if (_random_holder_level < 15 && !IsTITOMode)
            {
              _data_row[AC_USER_TYPE] = 0;
              _data_row[AC_HOLDER_LEVEL] = 0; // Approx. 15% of accounts are Anonymous
            }
            else if (_random_holder_level < 50)
            {
              _data_row[AC_USER_TYPE] = 1;
              _data_row[AC_HOLDER_LEVEL] = 1; // Approx. 35% of accounts are Level 1
            }
            else if (_random_holder_level < 75)
            {
              _data_row[AC_USER_TYPE] = 1;
              _data_row[AC_HOLDER_LEVEL] = 2; // Approx. 25% of accounts are Level 2
            }
            else if (_random_holder_level < 90)
            {
              _data_row[AC_USER_TYPE] = 1;
              _data_row[AC_HOLDER_LEVEL] = 3; // Approx. 15% of accounts are Level 3
            }
            else
            {
              _data_row[AC_USER_TYPE] = 1;
              _data_row[AC_HOLDER_LEVEL] = 4; // Approx. 10% of accounts are Level 4
            }

            // Assign different ages to personalized accounts
            _random_holder_age = m_rnd.Next(100);

            if (_random_holder_age < 11)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-20); // Approx. 11% of accounts are 18-24
            }
            else if (_random_holder_age < 25)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-30); // Approx. 14% of accounts are 25-34
            }
            else if (_random_holder_age < 42)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-40); // Approx. 17% of accounts are 35-44
            }
            else if (_random_holder_age < 64)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-50); // Approx. 22% of accounts are 45-54
            }
            else if (_random_holder_age < 83)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-60); // Approx. 19% of accounts are 55-64
            }
            else if (_random_holder_age < 92)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-70); // Approx. 9% of accounts are 65-74
            }
            else if (_random_holder_age < 98)
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-80); // Approx. 6% of accounts are 75-84
            }
            else
            {
              _data_row[AC_HOLDER_BIRTH_DATE] = FINISH_DATE.AddYears(-90); // Approx. 2% of accounts are 85+
            }

            m_dt_accounts.Rows.Add(_data_row);
          }
          else
          {
            _data_row = _rows[0];
            if (!_track_data.Equals(_data_row[AC_TRACK_DATA]))
            {
              _data_row[AC_TRACK_DATA] = _track_data;
            }
          }

          _idx_account++;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Update accounts.
          _num_rows_updated = _db_trx.Update(m_sql_da_accounts, m_dt_accounts);

          _db_trx.Commit();

          Log.Message("Done.");
          Log.Message("");

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // CreateAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Insert all necessary terminal data for a provider.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ProviderData ProviderData
    //          - Int32 IdxProvider
    //          - DateTime FromDate
    //          - DateTime ToDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: All necessary data for a terminal provider is inserted. False: Otherwise.
    //
    private static Boolean CreateDataForProvider(ProviderData ProviderData,
                                                 Int32 IdxProvider,
                                                 DateTime FromDate,
                                                 DateTime ToDate,
                                                 List<long>[] MonthPlayersId,
                                                 List<String>[] MonthPlayersTrack)
    {
      DataRow _data_row;
      Int32 _idx_term;
      String _terminal_name;
      Int32 _terminal_id;
      Int32 _num_rows_updated;
      DateTime _previous_from_date;
      DateTime _previous_to_date;
      DateRange _date_range;
      List<DateRange> _list_date_range;
      DateTime _current_date;
      DateTime _opening_date;
      DateTime _started_date;
      DateTime _finished_date;
      Double _minutes;
      Int32 _idx_session;
      Int32 _num_sessions;
      Int64 _account_id;
      String _track_data;
      Currency _initial_balance;
      Currency _played_amount;
      Currency _won_amount;
      Int64 _played_count;
      Int64 _won_count;
      Decimal _netwin_pct;
      DateTime _base_hour;
      String _filter;
      DataRow[] _rows;
      TimeSpan _time_minmax_date;
      TimeSpan _time_mem;
      TimeSpan _time_db;
      TimeSpan _rnd_timespan;
      Double _terminal_scale_amount;
      Int32 _random_duration;
      List<DateTime> _processed_months;
      DateTime _current_month;
      Int32 _index_month;
      Currency _nr_initial;
      Currency _nr_final;
      Currency _final_balance;
      Currency _nr_played;
      Currency _nr_won;
      Currency _re_ticket_in_amount;
      Currency _re_ticket_out_amount;
      Currency _cash_in;
      List<Int64> _month_players_id;
      List<String> _month_players_track;
      Int32 _random_account;

      Log.Message("---> Start Provider: " + ProviderData.Provider + ".");

      Log.Message("Creating " + ProviderData.NumTerminals + " Terminals. Also creating the Terminal-Game relation.");

      m_dt_terminals.Rows.Clear();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // This Fill loads all demo terminals for all providers.
        _db_trx.Fill(m_sql_da_terminals, m_dt_terminals);
      }

      // Add rows with new terminals for the current Provider.
      _idx_term = 1;
      while (_idx_term <= ProviderData.NumTerminals)
      {
        _terminal_name = PREFIX_TERMINAL_NAME + IdxProvider.ToString("00") + "-" + _idx_term.ToString("000");
        _rows = m_dt_terminals.Select("TERMINAL_NAME = '" + _terminal_name + "'");
        if (_rows.Length == 0)
        {
          _data_row = m_dt_terminals.NewRow();
          _data_row[TE_TERMINAL_NAME] = _terminal_name;
          _data_row[TE_PROVIDER_ID] = ProviderData.Provider;
          m_dt_terminals.Rows.Add(_data_row);
        }
        else
        {
          _data_row = _rows[0];
          // Touch DataRow to mark this row must stay in the DataTable.
          _data_row[TE_PROVIDER_ID] = ProviderData.Provider;
        }

        _idx_term++;
      } // while

      // Delete from memory unchanged rows. They are not valid anymore.
      // NO delete from DB, because they can have foreign keys and can belong to another provider.
      _rows = m_dt_terminals.Select("", "", DataViewRowState.Unchanged);
      foreach (DataRow _row in _rows)
      {
        m_dt_terminals.Rows.Remove(_row);
      }

      // Here, we have all the terminals from the current Provider in the DataTable.

      // Insert new terminals and terminal-game relation.
      // Need to insert now to obtain the TerminalId.
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Update terminals.
          _num_rows_updated = _db_trx.Update(m_sql_da_terminals, m_dt_terminals);

          m_dt_terminals.AcceptChanges();
          foreach (DataRow _row in m_dt_terminals.Rows)
          {
            _row.SetAdded();
          }

          // Insert NEW terminal-game relation.
          _num_rows_updated = _db_trx.Update(m_sql_da_term_game_relation, m_dt_terminals);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Inserting terminals and terminal-game relation.");
        Log.Exception(_ex);

        return false;
      }

      Log.Message("Done.");

      Log.Message("Generating Terminals Connected.");

      m_dt_terminals_connected.Rows.Clear();

      // For each terminal and date (between FromDate - ToDate), add a row to terminals connected DataTable.
      foreach (DataRow _row in m_dt_terminals.Rows)
      {
        _current_date = FromDate;
        while (_current_date < ToDate)
        {
          _data_row = m_dt_terminals_connected.NewRow();
          _data_row[TC_TERMINAL_ID] = _row[TE_TERMINAL_ID];
          _data_row[TC_DATE] = _current_date;
          m_dt_terminals_connected.Rows.Add(_data_row);

          _current_date = _current_date.AddDays(1);
        }
      }

      // Insert terminals connected.
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _num_rows_updated = _db_trx.Update(m_sql_da_terms_connected, m_dt_terminals_connected);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Inserting terminals connected.");
        Log.Exception(_ex);

        return false;
      }

      // Free memory as this DataTable is not needed anymore.
      m_dt_terminals_connected.Rows.Clear();

      Log.Message("Done.");


      // Finally, insert play sessions and sales for each terminal.

      Log.Message("Creating Play Sessions and Sales Per Hour.");

      // For each terminal and date, obtain a random number of sessions and,
      // insert these sessions and related sales per hour.
      foreach (DataRow _row in m_dt_terminals.Rows)
      {
        m_dt_play_sessions.Rows.Clear();
        m_dt_sales.Rows.Clear();

        _terminal_id = (Int32)_row[TE_TERMINAL_ID];
        _terminal_name = (String)_row[TE_TERMINAL_NAME];

        _time_minmax_date = Performance.GetTickCount();

        _list_date_range = new List<DateRange>();

        GetMinMaxDateSalesForTerminalId(_terminal_id, out _previous_from_date, out _previous_to_date);

        if (_previous_from_date == DateTime.MinValue && _previous_to_date == DateTime.MaxValue)
        {
          _date_range = new DateRange();
          _date_range.from = FromDate;
          _date_range.to = ToDate;
          _list_date_range.Add(_date_range);
        }
        else
        {
          if (_previous_from_date > DateTime.MinValue)
          {
            _previous_from_date = Misc.Opening(_previous_from_date, m_opening_hour, m_opening_minutes);
            _previous_from_date = _previous_from_date.Date;
          }
          if (_previous_to_date < DateTime.MaxValue)
          {
            _previous_to_date = Misc.Opening(_previous_to_date, m_opening_hour, m_opening_minutes);
            _previous_to_date = _previous_to_date.AddDays(1).Date;
          }
          // If New FromDate < Previous FromDate, make a new range: [New FromDate - Previous FromDate)
          if (FromDate < _previous_from_date)
          {
            _date_range = new DateRange();
            _date_range.from = FromDate;
            _date_range.to = _previous_from_date;
            _list_date_range.Add(_date_range);
          }
          // If New ToDate > Previous ToDate, make a new range: [Previous ToDate - New ToDate)
          if (ToDate > _previous_to_date)
          {
            _date_range = new DateRange();
            _date_range.from = _previous_to_date;
            _date_range.to = ToDate;
            _list_date_range.Add(_date_range);
          }
        }

        _time_minmax_date = Performance.GetElapsedTicks(_time_minmax_date);

        _time_mem = Performance.GetTickCount();

        _terminal_scale_amount = m_rnd.Next(1000, 2000) / 1000.0;

        _processed_months = new List<DateTime>();

        foreach (DateRange _range in _list_date_range)
        {
          _current_date = _range.from;
          _index_month = -1;

          while (_current_date < _range.to)
          {
            _rnd_timespan = _current_date.Subtract(m_rnd_base_datetime);
            m_rnd = new Random((Int32)(_rnd_timespan.TotalDays * _terminal_id));

            _opening_date = _current_date.AddHours(m_opening_hour).AddMinutes(m_opening_minutes);
            _started_date = _opening_date;
            _finished_date = _opening_date;

            _num_sessions = m_rnd.Next(LOWER_NUM_SESSIONS, UPPER_NUM_SESSIONS + 1);

            _idx_session = 0;


            while (true) // (_idx_session < _num_sessions)
            {
              // Get the month we are currently processing
              _current_month = new DateTime(_started_date.Year, _started_date.Month, 1);

              // If it's a new month, increment the index and add it to processed list
              if (!_processed_months.Contains(_current_month))
              {
                _index_month += 1;
                _processed_months.Add(_current_month);
              }

              _started_date = _started_date.AddMinutes(60);

              // Add some variability
              _random_duration = m_rnd.Next(100);

              if (_random_duration % 10 == 0)
              {
                break;
              }

              if (_random_duration % 3 == 0)
              {
                continue;
              }

              // Duration of sessions is a RANDOM(duration_session).
              _minutes = 30 * (0.05 + (1 + Math.Cos(2 * Math.PI * (_started_date.TimeOfDay.TotalMinutes - 20 * 60) / 1440)) * 0.5);
              _minutes = _minutes + (m_rnd.Next(1 + (Int32)(60 * _minutes * 10 / 100)) / 15.0);

              // Increase session duration from 17:00h to 23:00h
              if (_started_date.Hour < 15 || _started_date.Hour > 24)
              {
                _minutes = _minutes * 0.70;
              }
              if (_started_date.Hour < 15)
              {
                _minutes = _minutes * 0.70;
              }

              _minutes = _minutes * 3;

              // Manual adjustment of some providers
              if ((String)_row[TE_PROVIDER_ID] == "Atronicus")
              {
                _minutes = _minutes * 1.7;
              }
              else if ((String)_row[TE_PROVIDER_ID] == "Cadillac Joe")
              {
                _minutes = _minutes * 0.5;
              }
              else if ((String)_row[TE_PROVIDER_ID] == "Metrunia")
              {
                _minutes = _minutes * 0.75;
              }
              else if ((String)_row[TE_PROVIDER_ID] == "WSI")
              {
                _minutes = _minutes * 2;
              }

              // Add variability depending of the day of the week
              if (_opening_date.DayOfWeek == DayOfWeek.Monday ||
                  _opening_date.DayOfWeek == DayOfWeek.Wednesday ||
                  _opening_date.DayOfWeek == DayOfWeek.Thursday)
              {
                _minutes = _minutes * 0.50;
              }
              else if (_opening_date.DayOfWeek == DayOfWeek.Tuesday)
              {
                _minutes = _minutes * 0.60;
              }
              else if (_opening_date.DayOfWeek == DayOfWeek.Friday)
              {
                _minutes = _minutes * 0.80;
              }
              else if (_opening_date.DayOfWeek == DayOfWeek.Saturday ||
                       _opening_date.DayOfWeek == DayOfWeek.Sunday)
              {
                _minutes = _minutes * 1.25;
              }

              _finished_date = _started_date.AddMinutes(_minutes);

              // If day is over, finish this day. Start new day!
              if (_finished_date >= _opening_date.AddDays(1))
              {
                break;
              }


              // Calculate values for the fake play session:
              //  * PLAYED_AMOUNT = RAND(100, 4,000). For precission: RAND(1000, 40000)/10
              //  * NETWIN_PCT    = RAND(LOWER_NETWIN_PCT, UPPER_NETWIN_PCT).
              //                    For precission: RAND(1000*LOWER_NETWIN_PCT, 1000*UPPER_NETWIN_PCT)/1000
              //  * WON_AMOUNT    = PLAYED_AMOUNT * (1 - NETWIN_PCT)
              //  * PLAYED_COUNT  = RAND(25, 50)
              //  * WON_COUNT     = RAND(5, 15)

              _played_amount = (Decimal)(_terminal_scale_amount * _minutes * m_rnd.Next(10, 150)); // m_rnd.Next(10, 100)
              _netwin_pct = m_rnd.Next((Int32)(1000 * LOWER_NETWIN_PCT), (Int32)(1000 * UPPER_NETWIN_PCT) + 1) / 1000m;
              _won_amount = _played_amount * (1 - _netwin_pct);

              _played_count = (Int32)(_minutes * 20);
              _won_count = m_rnd.Next(0, (Int32)_played_count);

              if (!IsTITOMode)
              {
                _initial_balance = 100 * m_rnd.Next(1, 6);         // Redimible
                _nr_initial = Math.Round(_initial_balance / 2, 2); // No Redimible

                _initial_balance += _nr_initial;

                _final_balance = _initial_balance - _played_amount + _won_amount;
                if (_final_balance < 0)
                {
                  _initial_balance -= _final_balance;
                  _final_balance = _initial_balance - _played_amount + _won_amount;
                }
                
                _nr_final = Math.Min(_nr_initial, _final_balance);

                _nr_played = 0;
                if (_nr_final < _nr_initial)
                {
                  _nr_played = _nr_initial - _nr_final;
                }
                _nr_won = _nr_final - _nr_initial + _nr_played;

                _re_ticket_in_amount = 0;
                _re_ticket_out_amount = 0;
                _cash_in = 0;
              }
              else
              {
                _initial_balance = 0;
                _final_balance = 0;
                _nr_initial = 0;
                _nr_final = 0;
                _nr_played = 0;
                _nr_won = 0;

                _cash_in = m_rnd.Next(0, 100);
                _re_ticket_in_amount = m_rnd.Next(1, 50);

                _re_ticket_out_amount = (_cash_in + _re_ticket_in_amount + _won_amount) - _played_amount;

                // Chapuza para evitar negativos
                if (_re_ticket_out_amount < 0)
                {
                  _re_ticket_in_amount += (_re_ticket_out_amount * -1) + _re_ticket_in_amount;
                  _re_ticket_out_amount = (_cash_in + _re_ticket_in_amount + _won_amount) - _played_amount;

                  if (_re_ticket_out_amount < 0)
                  {
                    return false;
                  }
                }
              }              
                            
              // Select account randomly from pre-selected month players
              _month_players_id = MonthPlayersId[_index_month];
              _month_players_track = MonthPlayersTrack[_index_month];

              _random_account = m_rnd.Next(_month_players_id.Count - 1);

              _account_id = _month_players_id[_random_account];
              _track_data = _month_players_track[_random_account];
              
              _data_row = m_dt_play_sessions.NewRow();
              _data_row[PS_ACCOUNT_ID] = _account_id;
              _data_row[PS_TERMINAL_ID] = _terminal_id;
              _data_row[PS_TRACK_DATA] = _track_data;
              _data_row[PS_STARTED] = _started_date;
              _data_row[PS_INITIAL_BALANCE] = (Decimal)_initial_balance;
              _data_row[PS_PLAYED_COUNT] = _played_count;
              _data_row[PS_PLAYED_AMOUNT] = (Decimal)_played_amount;
              _data_row[PS_WON_COUNT] = _won_count;
              _data_row[PS_WON_AMOUNT] = (Decimal)_won_amount;
              _data_row[PS_FINISHED] = _finished_date;
              _data_row[PS_FINAL_BALANCE] = (Decimal)_final_balance;
              _data_row[PS_REDEEMABLE_PLAYED] = _played_amount - _nr_played;
              _data_row[PS_REDEEMABLE_WON] = _won_amount - _nr_won;
              _data_row[PS_REDEEMABLE_TICKET_IN] = (Decimal)_re_ticket_in_amount;
              _data_row[PS_REDEEMABLE_TICKET_OUT] = (Decimal)_re_ticket_out_amount;
              _data_row[PS_CASH_IN] = (Decimal)_cash_in;

              //JBP

              m_dt_play_sessions.Rows.Add(_data_row);

              _base_hour = new DateTime(_finished_date.Year, _finished_date.Month, _finished_date.Day, _finished_date.Hour, 0, 0);

              _filter = "     BASE_HOUR   = '" + _base_hour.ToString("yyyy-MM-ddTHH:mm:ss", DateTimeFormatInfo.InvariantInfo) + "'" +
                        " AND TERMINAL_ID = " + _terminal_id +
                        " AND GAME_ID     = " + m_game_id;

              _rows = m_dt_sales.Select(_filter);

              if (_rows.Length == 0)
              {
                _data_row = m_dt_sales.NewRow();
                _data_row[SPH_BASE_HOUR] = _base_hour;
                _data_row[SPH_TERMINAL_ID] = _terminal_id;
                _data_row[SPH_TERMINAL_NAME] = _terminal_name;
                _data_row[SPH_GAME_ID] = m_game_id;
                _data_row[SPH_GAME_NAME] = m_game_name;
                _data_row[SPH_PLAYED_COUNT] = _played_count;
                _data_row[SPH_PLAYED_AMOUNT] = (Decimal)_played_amount;
                _data_row[SPH_WON_COUNT] = _won_count;
                _data_row[SPH_WON_AMOUNT] = (Decimal)_won_amount;
                _data_row[SPH_NUM_ACTIVE_TERMINALS] = m_num_active_terminals;
                m_dt_sales.Rows.Add(_data_row);
              }
              else
              {
                _data_row = _rows[0];

                _data_row.BeginEdit();
                _data_row[SPH_PLAYED_COUNT] = (Int64)_data_row[SPH_PLAYED_COUNT] + _played_count;
                _data_row[SPH_PLAYED_AMOUNT] = (Decimal)_data_row[SPH_PLAYED_AMOUNT] + _played_amount;
                _data_row[SPH_WON_COUNT] = (Int64)_data_row[SPH_WON_COUNT] + _won_count;
                _data_row[SPH_WON_AMOUNT] = (Decimal)_data_row[SPH_WON_AMOUNT] + _won_amount;
                _data_row.EndEdit();
              }

              _idx_session++;
            } // while _idx_session

            _current_date = _current_date.AddDays(1);
          } // while _current_date
        } // foreach _range

        _time_mem = Performance.GetElapsedTicks(_time_mem);

        _time_db = Performance.GetTickCount();

        // Insert play sessions and sales_per_hour.
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _num_rows_updated = _db_trx.Update(m_sql_da_play_sessions, m_dt_play_sessions);

            _num_rows_updated = _db_trx.Update(m_sql_da_sales, m_dt_sales);

            _db_trx.Commit();
          }
        }
        catch (Exception _ex)
        {
          Log.Error(" Inserting play sessions and sales per hour.");
          Log.Exception(_ex);

          return false;
        }

        _time_db = Performance.GetElapsedTicks(_time_db);

        Log.Message("Terminal - Time in ms (Id/Name/TimeMinMaxDate/TimeMem/TimeDB) = (" +
                    _terminal_id + "/" + _terminal_name + "/" +
                    _time_minmax_date.TotalMilliseconds + "/" + _time_mem.TotalMilliseconds + "/" +
                    _time_db.TotalMilliseconds + ")");
      } // foreach _row

      Log.Message("Done.");

      Log.Message("---> End   Provider: " + ProviderData.Provider + ".");
      Log.Message("");

      return true;
    } // CreateDataForProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Insert data for Cashier Sessions.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime FromDate
    //          - DateTime ToDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Data for Cashier Sessions is inserted. False: Otherwise.
    //
    private static Boolean CreateDataForCashierSessions(DateTime FromDate, DateTime ToDate)
    {
      Int32 _num_rows_inserted;
      DateTime _previous_from_date;
      DateTime _previous_to_date;
      DateRange _date_range;
      List<DateRange> _list_date_range;
      DateTime _current_date;
      DataRow _data_row;
      DateTime _opening_date;
      DateTime _mov_date;
      String _name;
      Int64 _session_id;
      Currency _result;
      Currency _total_inputs;
      Currency _total_outputs;

      Currency _tax1;
      Currency _tax2;
      Currency _tax3;
      Currency _total_taxes;

      Percentage _tax1_on_prize_pct;
      Percentage _tax2_on_prize_pct;
      Percentage _tax3_on_prize_pct;
      Percentage _total_tax_pct;

      Currency _amount;
      Currency _final_balance;
      Currency _input_split_a;
      Currency _input_split_b;
      Currency _prize_coupon;
      Currency _prize;
      Decimal _tax;
      String _str_tax;

      Currency _cancellation;
      Currency _cancel_split_a;
      Currency _cancel_split_b;
      TimeSpan _rnd_timespan;
      Double _factor;
      TimeSpan _num_days;
      Int32 _accumulated_days;
      Int32 _movements_by_session;

      _num_days = ToDate - FromDate;

      Log.Message("Creating Cashier Sessions.");

      // Tax 1 Pct
      _tax = 0;
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Pct");
      if (_str_tax != null)
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _tax);
      }
      _tax1_on_prize_pct = _tax / 100;

      // Tax 2 Pct
      _tax = 0;
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Pct");
      if (_str_tax != null)
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _tax);
      }
      _tax2_on_prize_pct = _tax / 100;

      // Tax 3 Pct
      _tax = 0;
      _str_tax = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Pct");
      if (_str_tax != null)
      {
        Decimal.TryParse(Format.DBNumberToLocalNumber(_str_tax), out _tax);
      }
      _tax3_on_prize_pct = _tax / 100;

      _total_tax_pct = (_tax1_on_prize_pct + _tax2_on_prize_pct + _tax3_on_prize_pct);

      _list_date_range = new List<DateRange>();

      GetMinMaxDateCashierSessions(out _previous_from_date, out _previous_to_date);

      if (_previous_from_date == DateTime.MinValue && _previous_to_date == DateTime.MaxValue)
      {
        _date_range = new DateRange();
        _date_range.from = FromDate;
        _date_range.to = ToDate;
        _list_date_range.Add(_date_range);
      }
      else
      {
        if (_previous_from_date > DateTime.MinValue)
        {
          _previous_from_date = Misc.Opening(_previous_from_date, m_opening_hour, m_opening_minutes);
          _previous_from_date = _previous_from_date.Date;
        }
        
        if (_previous_to_date < DateTime.MaxValue)
        {
          _previous_to_date = Misc.Opening(_previous_to_date, m_opening_hour, m_opening_minutes);
          _previous_to_date = _previous_to_date.AddDays(1).Date;
        }
        
        // If New FromDate < Previous FromDate, make a new range: [New FromDate - Previous FromDate)
        if (FromDate < _previous_from_date)
        {
          _date_range = new DateRange();
          _date_range.from = FromDate;
          _date_range.to = _previous_from_date;
          _list_date_range.Add(_date_range);
        }
        
        // If New ToDate > Previous ToDate, make a new range: [Previous ToDate - New ToDate)
        if (ToDate > _previous_to_date)
        {
          _date_range = new DateRange();
          _date_range.from = _previous_to_date;
          _date_range.to = ToDate;
          _list_date_range.Add(_date_range);
        }
      }

      m_dt_cashier_sessions.Rows.Clear();

      foreach (DateRange _range in _list_date_range)
      {
        _current_date = _range.from;

        while (_current_date < _range.to)
        {
          _rnd_timespan = _current_date.Subtract(m_rnd_base_datetime);
          m_rnd = new Random((Int32)_rnd_timespan.TotalDays);

          _data_row = m_dt_cashier_sessions.NewRow();

          _opening_date = _current_date.AddHours(m_opening_hour).AddMinutes(m_opening_minutes);
          // Random minutes to _opening_date: 45 minutes.
          _opening_date = _opening_date.AddMinutes(m_rnd.Next(4500 + 1) / 100.0);

          _name = m_cashier_user_name + " " + _opening_date.ToString("dd/MMM/yyyy HH:mm");
          _data_row[CS_NAME] = _name.ToUpper();
          _data_row[CS_OPENING_DATE] = _opening_date;
          // Closing Date minimum is 15 hours later + RANDOM(0 hours, 5 hours).
          _data_row[CS_CLOSING_DATE] = _opening_date.AddHours(15 + (m_rnd.Next(500 + 1) / 100.0));

          // Set this value after all movements are inserted.
          _data_row[CS_BALANCE] = 0;

          m_dt_cashier_sessions.Rows.Add(_data_row);

          _current_date = _current_date.AddDays(1);
        } // while _current_date
      } // foreach _range

      // Insert cashier sessions.
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _db_trx.Update(m_sql_da_cashier_sessions, m_dt_cashier_sessions);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Inserting cashier sessions.");
        Log.Exception(_ex);

        return false;
      }

      Log.Message("Done.");
      Log.Message("");

      Log.Message("Creating Cashier Movements.");

      _accumulated_days = 0;
      _num_rows_inserted = 0;

      foreach (DataRow _row in m_dt_cashier_sessions.Rows)
      {
        _movements_by_session = m_rnd.Next(LOWER_SESSION_MOVEMENTS, UPPER_SESSION_MOVEMENTS);

        _session_id = (Int64)_row[CS_SESSION_ID];
        _mov_date = (DateTime)_row[CS_OPENING_DATE];

        _final_balance = 0;

        // Movement: OPEN_SESSION: 25,000 To 50,000 (Step 5,000)
        _amount = 25000 + (m_rnd.Next(5 + 1) * 5000);
        AddCashierMovement(CASHIER_MOVEMENT.OPEN_SESSION, _amount, _session_id, _mov_date, ref _final_balance);

        for (int i = 0; i < _movements_by_session; i++)
        {
          _accumulated_days++;
          _factor = (1 + 0.50 * _accumulated_days / _num_days.TotalDays);

          // RESULT = TOTAL_INPUTS - (TOTAL_OUTPUTS + TOTAL_TAXES)
          _result = m_rnd.Next((Int32)(LOWER_CASHIER_RESULT * 100), (Int32)(UPPER_CASHIER_RESULT * 100) + 1) / 100m;
          _result = _result * (Decimal)_factor;

          // TOTAL_INPUTS = 10 * RANDOM(LOWER_RESULT, UPPER_RESULT)
          _total_inputs = m_rnd.Next((Int32)(LOWER_CASHIER_RESULT * 100), (Int32)(UPPER_CASHIER_RESULT * 100) + 1) / 10m;
          _total_inputs = _total_inputs * (Decimal)_factor;

          _total_outputs = _total_inputs - _result;
          // TOTAL_TAXES = RAND(_total_tax_pct) of TOTAL_OUTPUTS
          _total_taxes = (m_rnd.Next(((Int32)(_total_tax_pct * 100) * 100) + 1) / 10000m) * _total_outputs;
          // Recalculate TOTAL_OUTPUTS
          _total_outputs = _total_outputs - _total_taxes;

          // Movement: CASH_IN: total_inputs
          _mov_date = _mov_date.AddMinutes(m_rnd.Next(100, 400));

          // Calculate cash in splits
          _input_split_a = Math.Round(_total_inputs * m_splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
          _input_split_b = _total_inputs - _input_split_a;
          _prize_coupon = m_splits.prize_coupon ? _input_split_b : 0;

          if (_prize_coupon > 0)
          {
            AddCashierMovement(CASHIER_MOVEMENT.PRIZE_COUPON, _prize_coupon, _session_id, _mov_date, ref _final_balance);
          }

          if (_input_split_a > 0)
          {
            AddCashierMovement(CASHIER_MOVEMENT.CASH_IN_SPLIT1, _input_split_a, _session_id, _mov_date, ref _final_balance);
          }

          if (_input_split_b > 0)
          {
            AddCashierMovement(CASHIER_MOVEMENT.CASH_IN_SPLIT2, _input_split_b, _session_id, _mov_date, ref _final_balance);
          }

          AddCashierMovement(CASHIER_MOVEMENT.CASH_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);

          AddCashierMovement(CASHIER_MOVEMENT.ACCOUNT_BLOCKED, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.ACCOUNT_UNBLOCKED, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CAGE_FILLER_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CAGE_FILLER_OUT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CANCEL_SPLIT1, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CANCEL_SPLIT2, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_CREATION, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_DEPOSIT_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_DEPOSIT_OUT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_PERSONALIZATION, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_RECYCLED, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CARD_REPLACEMENT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CASINO_CHIPS_EXCHANGE_SPLIT1, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CASINO_CHIPS_EXCHANGE_SPLIT2, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHECK_PAYMENT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_CHANGE_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_CHANGE_OUT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_LAST_MOVEMENT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_PURCHASE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_SALE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CHIPS_SALE_TOTAL, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.DEV_SPLIT1, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.DEV_SPLIT2, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.FILLER_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.FILLER_OUT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.FILLER_OUT_CHECK, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.HANDPAY, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.HANDPAY_CANCELLATION, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MANUAL_HANDPAY, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MB_CASH_IN, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MB_CASH_IN_COVER_COUPON, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MB_CASH_LOST, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.MB_PRIZE_COUPON, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1, _total_inputs, _session_id, _mov_date, ref _final_balance);
          AddCashierMovement(CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2, _total_inputs, _session_id, _mov_date, ref _final_balance);

          ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

          // Movement: CASH_OUT: total_outputs + total_taxes
          //           total_outputs = prizes + cancellation + devolution

          _mov_date = _mov_date.AddMinutes(m_rnd.Next(100, 400));

          _prize = Math.Round(_total_taxes / _total_tax_pct, 2, MidpointRounding.AwayFromZero);
          _prize = Math.Min(_prize, _total_outputs);
          _cancellation = (_total_outputs + _total_taxes) - _prize;

          _tax1 = Math.Round(_prize * _tax1_on_prize_pct, 2, MidpointRounding.AwayFromZero);
          //_tax2 = _total_taxes - _tax1;
          _tax2 = Math.Round(_prize * _tax2_on_prize_pct, 2, MidpointRounding.AwayFromZero);
          _tax3 = Math.Round(_prize * _tax3_on_prize_pct, 2, MidpointRounding.AwayFromZero);

          _cancel_split_a = 0;
          _cancel_split_b = 0;
          if (_cancellation > 0)
          {
            _cancel_split_a = Math.Round(_cancellation * m_splits.company_a.cancellation_pct / 100, 2, MidpointRounding.AwayFromZero);
            _cancel_split_b = _cancellation - _cancel_split_a;
          }

          if (_cancel_split_a > 0)
          {
            AddCashierMovement(CASHIER_MOVEMENT.CANCEL_SPLIT1, _cancel_split_a, _session_id, _mov_date, ref _final_balance);
            if (m_splits.company_b.cancellation_pct > 0)
            {
              AddCashierMovement(CASHIER_MOVEMENT.CANCEL_SPLIT2, _cancel_split_b, _session_id, _mov_date, ref _final_balance);
            }
          }
          
          if (_prize > 0)
          {
            // 2. Prize
            AddCashierMovement(CASHIER_MOVEMENT.PRIZES, _prize, _session_id, _mov_date, ref _final_balance);

            // 3. Taxes 
            if (_tax1 > 0)
            {
              // Tax 1
              AddCashierMovement(CASHIER_MOVEMENT.TAX_ON_PRIZE1, _tax1, _session_id, _mov_date, ref _final_balance);
            }

            if (_tax2 > 0)
            {
              // Tax 2
              AddCashierMovement(CASHIER_MOVEMENT.TAX_ON_PRIZE2, _tax2, _session_id, _mov_date, ref _final_balance);
            }

            if (_tax3 > 0)
            {
              // Tax 3
              AddCashierMovement(CASHIER_MOVEMENT.TAX_ON_PRIZE3, _tax3, _session_id, _mov_date, ref _final_balance);
            }
          }

          AddCashierMovement(CASHIER_MOVEMENT.CASH_OUT, _total_outputs, _session_id, _mov_date, ref _final_balance);
        }

        // Movement: CLOSE_SESSION
        _mov_date = (DateTime)_row[CS_CLOSING_DATE];
        AddCashierMovement(CASHIER_MOVEMENT.CLOSE_SESSION, 0, _session_id, _mov_date, ref _final_balance);

        // Finally, update Balance Cashier Session
        _row[CS_BALANCE] = (Decimal)_final_balance;

        // Insert cashier movements.
        try
        {
          //_drs_cm = _dt_cashier_movements.
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _db_trx.Update(m_sql_da_cashier_movements, m_dt_cashier_movements);

            // Update CS_BALANCE
            _db_trx.Update(m_sql_da_cashier_sessions, m_dt_cashier_sessions);

            _db_trx.Commit();
            _num_rows_inserted++;
            m_dt_cashier_movements.Rows.Clear();

            if ( _num_rows_inserted % 10 == 0)
            {
              Log.Message("Procesado : " + (((float)_num_rows_inserted / m_dt_cashier_sessions.Rows.Count) * 100).ToString("0.00") + "%. " + _num_rows_inserted + "/" + m_dt_cashier_sessions.Rows.Count + " sesiones procesadas"); 
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Error(" Inserting cashier movements and updating cashier sessions.");
          Log.Exception(_ex);

          return false;
        }
      }

      if (_num_rows_inserted != m_dt_cashier_sessions.Rows.Count)
      {
        Log.Warning("Not all cashier sessions have been updated! (Updated/Total) = (" + _num_rows_inserted + "/" + m_dt_cashier_sessions.Rows.Count + ")");
      }

      return true;
    } // CreateDataForCashierSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Add a Cashier Movement into the DataTable and keep FinalBalance updated.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CASHIER_MOVEMENT MovementType
    //          - Currency Amount
    //          - Int64 SessionId
    //          - DateTime MovementDate
    //          - Currency FinalBalance
    //
    //      - OUTPUT :
    //          - Currency FinalBalance
    //
    // RETURNS :
    //
    private static void AddCashierMovement(CASHIER_MOVEMENT MovementType,
                                           Currency Amount,
                                           Int64 SessionId,
                                           DateTime MovementDate,
                                           ref Currency FinalBalance)
    {
      Currency _initial_balance;
      Currency _balance_increment;
      Currency _add_amount;
      Currency _sub_amount;
      DataRow _data_row;

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.OPEN_SESSION:
        case CASHIER_MOVEMENT.FILLER_IN:
        case CASHIER_MOVEMENT.CASH_IN:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.PRIZE_COUPON:
          _add_amount = Amount;
          _sub_amount = 0;
          break;

        case CASHIER_MOVEMENT.FILLER_OUT:
        case CASHIER_MOVEMENT.CASH_OUT:
        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
        case CASHIER_MOVEMENT.DEV_SPLIT1:
        case CASHIER_MOVEMENT.DEV_SPLIT2:
        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
          _add_amount = 0;
          _sub_amount = Amount;
          break;

        case CASHIER_MOVEMENT.CLOSE_SESSION:
        default:
          _add_amount = 0;
          _sub_amount = 0;
          break;
      }

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.DEV_SPLIT1:
        case CASHIER_MOVEMENT.DEV_SPLIT2:
        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
        case CASHIER_MOVEMENT.PRIZE_COUPON:
          _balance_increment = 0;
          break;

        default:
          _balance_increment = _add_amount - _sub_amount;
          break;
      }

      _initial_balance = FinalBalance;
      FinalBalance = FinalBalance + _balance_increment;

      _data_row = m_dt_cashier_movements.NewRow();
      _data_row[CM_SESSION_ID] = SessionId;
      _data_row[CM_DATE] = MovementDate;
      _data_row[CM_TYPE] = MovementType;
      _data_row[CM_INITIAL_BALANCE] = (Decimal)_initial_balance;
      _data_row[CM_ADD_AMOUNT] = (Decimal)_add_amount;
      _data_row[CM_SUB_AMOUNT] = (Decimal)_sub_amount;
      _data_row[CM_FINAL_BALANCE] = (Decimal)FinalBalance;
      
      m_dt_cashier_movements.Rows.Add(_data_row);
    } // AddCashierMovement

    //------------------------------------------------------------------------------
    // PURPOSE : Get the minimum and maximum SALES_PER_HOUR.SPH_BASE_HOUR date for the specified TerminalId.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 TerminalId
    //
    //      - OUTPUT :
    //          - DateTime MinDate
    //          - DateTime MaxDate
    //
    // RETURNS :
    //
    private static void GetMinMaxDateSalesForTerminalId(Int32 TerminalId, out DateTime MinDate, out DateTime MaxDate)
    {
      StringBuilder _sql_txt;

      MinDate = DateTime.MinValue;
      MaxDate = DateTime.MaxValue;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   MIN(SPH_BASE_HOUR) ");
        _sql_txt.AppendLine("       , MAX(SPH_BASE_HOUR) ");
        _sql_txt.AppendLine("  FROM   SALES_PER_HOUR ");
        _sql_txt.AppendLine(" WHERE   SPH_TERMINAL_ID = @pTerminalId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              if (_sql_reader.Read())
              {
                MinDate = _sql_reader.IsDBNull(0) ? DateTime.MinValue : _sql_reader.GetDateTime(0);
                MaxDate = _sql_reader.IsDBNull(1) ? DateTime.MaxValue : _sql_reader.GetDateTime(1);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // GetMinMaxDateSalesForTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE : Get the minimum and maximum CASHIER_SESSIONS.CS_OPENING_DATE date for the demo.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - DateTime MinDate
    //          - DateTime MaxDate
    //
    // RETURNS :
    //      - DateTime
    //
    private static void GetMinMaxDateCashierSessions(out DateTime MinDate, out DateTime MaxDate)
    {
      StringBuilder _sql_txt;

      MinDate = DateTime.MinValue;
      MaxDate = DateTime.MaxValue;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT   MIN(CS_OPENING_DATE) ");
        _sql_txt.AppendLine("       , MAX(CS_OPENING_DATE) ");
        _sql_txt.AppendLine("  FROM   CASHIER_SESSIONS ");
        _sql_txt.AppendLine(" WHERE   CS_USER_ID = @pUserId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = m_cashier_user_id;
            using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              if (_sql_reader.Read())
              {
                MinDate = _sql_reader.IsDBNull(0) ? DateTime.MinValue : _sql_reader.GetDateTime(0);
                MaxDate = _sql_reader.IsDBNull(1) ? DateTime.MaxValue : _sql_reader.GetDateTime(1);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // GetMinMaxDateCashierSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Read providers from configuration file.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String ConfigFile
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - List<ProviderData>
    //
    private static List<ProviderData> Config_ReadProviders(String ConfigFile)
    {
      FileStream _stream;
      XmlDocument _xml_config_file;
      XmlNodeList _xml_node_list;
      String _value;
      List<ProviderData> _list_providers;

      _list_providers = null;

      try
      {
        _stream = new FileStream(ConfigFile, FileMode.Open, FileAccess.Read);

        // Load configuration file
        _xml_config_file = new XmlDocument();
        _xml_config_file.Load(_stream);

        _xml_node_list = _xml_config_file.GetElementsByTagName("Provider");
        if (_xml_node_list != null
            && _xml_node_list.Count > 0)
        {
          _list_providers = new List<ProviderData>();

          foreach (XmlNode _xml_node in _xml_node_list)
          {
            _value = _xml_node.FirstChild.InnerText;
            _list_providers.Add(new ProviderData(_value, 0));
          }
        }
      }
      catch
      {
      }

      return _list_providers;
    } // Config_ReadProviders

    //------------------------------------------------------------------------------
    // PURPOSE : Get a random PD AccountId.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - Int64 AccountId
    //          - String TrackData
    //
    // RETURNS :
    //
    private static void GetRandomAccountId(out Int64 AccountId, out String TrackData)
    {
      Int32 _idx_account;
      DataRow _row;

      _idx_account = m_rnd.Next(m_dt_accounts.Rows.Count);
      _row = m_dt_accounts.Rows[_idx_account];

      AccountId = (Int64)_row[AC_ACCOUNT_ID];
      TrackData = (String)_row[AC_TRACK_DATA];
    } // GetRandomAccountId

    //------------------------------------------------------------------------------
    // PURPOSE : Insert the game for the Population Data and store its Id.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Game Id retrieved successfuly. False: Otherwise.
    //
    private static Boolean GetPdGameId()
    {
      StringBuilder _sql_txt;
      Object _obj;
      SqlParameter _parameter;

      if (m_game_id > 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Find the Game, if exists...
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   GM_GAME_ID ");
          _sql_txt.AppendLine("  FROM   GAMES ");
          _sql_txt.AppendLine(" WHERE   GM_NAME = @pGameName ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = m_game_name;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);
          }

          if (_obj != null)
          {
            // The Game is already inserted. Return its value.
            m_game_id = (Int32)_obj;

            return true;
          }

          // The Game doesn't exist. Must insert it.
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("INSERT INTO GAMES ( GM_NAME ");
          _sql_txt.AppendLine("                  ) ");
          _sql_txt.AppendLine("           VALUES ( @pGameName ");
          _sql_txt.AppendLine("                  ) ");
          _sql_txt.AppendLine(" SET     @pGameId = SCOPE_IDENTITY() ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = m_game_name;
            _parameter = _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int);
            _parameter.Direction = ParameterDirection.Output;

            _db_trx.ExecuteNonQuery(_sql_cmd);

            if (_parameter.Value != null)
            {
              _db_trx.Commit();

              // The Game has been inserted correctly. Return its value.
              m_game_id = (Int32)_parameter.Value;

              return true;
            }
          }
        }

        Log.Error(" You shouldn't arrive here. Can't obtain 'Population Data' Game.");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetPdGameId

    //------------------------------------------------------------------------------
    // PURPOSE : Insert the Cashier Terminal for the demo and store its Id.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: CashierTerminalId retrieved successfuly. False: Otherwise.
    //
    private static Boolean GetPdCashierTerminalId()
    {
      StringBuilder _sql_txt;
      Object _obj;
      SqlParameter _parameter;

      if (m_cashier_terminal_id > 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Find the Cashier Terminal, if exists...
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   CT_CASHIER_ID ");
          _sql_txt.AppendLine("  FROM   CASHIER_TERMINALS ");
          _sql_txt.AppendLine(" WHERE   CT_NAME = @pCashierTerminalName ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pCashierTerminalName", SqlDbType.NVarChar).Value = m_cashier_terminal_name;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);
          }

          if (_obj != null)
          {
            // The Cashier Terminal is already inserted. Return its value.
            m_cashier_terminal_id = (Int32)_obj;

            return true;
          }

          // The Cashier Terminal doesn't exist. Must insert it.
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("INSERT INTO CASHIER_TERMINALS ( CT_NAME ");
          _sql_txt.AppendLine("                              ) ");
          _sql_txt.AppendLine("                       VALUES ( @pCashierTerminalName ");
          _sql_txt.AppendLine("                              ) ");
          _sql_txt.AppendLine(" SET      @pCashierTerminalId = SCOPE_IDENTITY() ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pCashierTerminalName", SqlDbType.NVarChar).Value = m_cashier_terminal_name;
            _parameter = _sql_cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.Int);
            _parameter.Direction = ParameterDirection.Output;

            _db_trx.ExecuteNonQuery(_sql_cmd);

            if (_parameter.Value != null)
            {
              _db_trx.Commit();

              // The Cashier Terminal has been inserted correctly. Return its value.
              m_cashier_terminal_id = (Int32)_parameter.Value;

              return true;
            }
          }
        }

        Log.Error(" You shouldn't arrive here. Can't obtain Cashier Terminal for the demo.");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetPdCashierTerminalId

    //------------------------------------------------------------------------------
    // PURPOSE : Insert the Cashier User for the demo and store its Id.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: CashierUserId retrieved successfuly. False: Otherwise.
    //
    private static Boolean GetPdCashierUserId()
    {
      StringBuilder _sql_txt;
      Object _obj;
      Int32 _profile_id;

      if (m_cashier_user_id > 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Find the Cashier User, if exists...
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   GU_USER_ID ");
          _sql_txt.AppendLine("  FROM   GUI_USERS ");
          _sql_txt.AppendLine(" WHERE   GU_USERNAME = @pCashierUserName ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pCashierUserName", SqlDbType.NVarChar).Value = m_cashier_user_name;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);
          }

          if (_obj != null)
          {
            // The Cashier User is already inserted. Return its value.
            m_cashier_user_id = (Int32)_obj;

            return true;
          }

          // The Cashier User doesn't exist. Must insert it.

          // Obtain max UserId + 1, for the next id.
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   MAX(ISNULL(GU_USER_ID, 0)) + 1 ");
          _sql_txt.AppendLine("  FROM   GUI_USERS ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            m_cashier_user_id = (Int32)_db_trx.ExecuteScalar(_sql_cmd);
          }

          // Obtain the ProfileId for the CASHIER_USER_PROFILE_NAME.
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT   GUP_PROFILE_ID ");
          _sql_txt.AppendLine("  FROM   GUI_USER_PROFILES ");
          _sql_txt.AppendLine(" WHERE   GUP_NAME = @pProfileName ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pProfileName", SqlDbType.NVarChar).Value = CASHIER_USER_PROFILE_NAME;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);
          }

          if (_obj == null)
          {
            Log.Error(" Can't obtain ProfileId for ProfileName: " + CASHIER_USER_PROFILE_NAME + ".");

            return false;
          }

          _profile_id = (Int32)_obj;

          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("INSERT INTO GUI_USERS ( GU_USER_ID ");
          _sql_txt.AppendLine("                      , GU_PROFILE_ID ");
          _sql_txt.AppendLine("                      , GU_USERNAME ");
          _sql_txt.AppendLine("                      , GU_ENABLED ");
          _sql_txt.AppendLine("                      , GU_PASSWORD ");
          _sql_txt.AppendLine("                      , GU_NOT_VALID_BEFORE ");
          _sql_txt.AppendLine("                      , GU_NOT_VALID_AFTER ");
          _sql_txt.AppendLine("                      , GU_LAST_CHANGED ");
          _sql_txt.AppendLine("                      , GU_PASSWORD_EXP ");
          _sql_txt.AppendLine("                      , GU_PWD_CHG_REQ ");
          _sql_txt.AppendLine("                      , GU_FULL_NAME ");
          _sql_txt.AppendLine("                      , GU_LOGIN_FAILURES ");
          _sql_txt.AppendLine("                      ) ");
          _sql_txt.AppendLine("               VALUES ( @pCashierUserId ");
          _sql_txt.AppendLine("                      , @pProfileId ");
          _sql_txt.AppendLine("                      , @pCashierUserName ");
          _sql_txt.AppendLine("                      , 1 ");
          _sql_txt.AppendLine("                      , CAST('00' AS binary(40)) ");
          _sql_txt.AppendLine("                      , GETDATE() - 3 ");
          _sql_txt.AppendLine("                      , NULL ");
          _sql_txt.AppendLine("                      , GETDATE() ");
          _sql_txt.AppendLine("                      , NULL ");
          _sql_txt.AppendLine("                      , 0 ");
          _sql_txt.AppendLine("                      , @pCashierUserName ");
          _sql_txt.AppendLine("                      , 0 ");
          _sql_txt.AppendLine("                      ) ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString()))
          {
            _sql_cmd.Parameters.Add("@pCashierUserId", SqlDbType.Int).Value = m_cashier_user_id;
            _sql_cmd.Parameters.Add("@pProfileId", SqlDbType.Int).Value = _profile_id;
            _sql_cmd.Parameters.Add("@pCashierUserName", SqlDbType.NVarChar).Value = m_cashier_user_name;

            _db_trx.ExecuteNonQuery(_sql_cmd);

            _db_trx.Commit();

            return true;
          }
        }

        //Log.Error(" You shouldn't arrive here. Can't obtain Cashier User for the demo.");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetPdCashierUserId

  } // PopulateDemoData

} // WSI.PopulateDemoData
