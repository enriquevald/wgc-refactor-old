using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Security.Cryptography;
using System.Text;
using System.IO;


  public partial class TrackData
  {

    //------------------------------------------------------------------------------
    // PURPOSE: SQL Function to return Encrypted/External Track Data from Database. 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData: Internal track data.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - card_track_data: Encrypted track data.
    // 
    //   NOTES:
    //          - SQLFunction.
    //
    [SqlFunction()]
    public static String ToExternal(String CardTrackData)
    {
      String card_track_data;
      Boolean rc;

      if (CardTrackData == null || CardTrackData.Length <= 0) return "";

      rc = CardNumber.TrackDataToExternal(out card_track_data, CardTrackData, 0);

      return card_track_data;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: SQL Function to return Decrypted/Internal Track Data from Database.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - CardTrackData: Encrypted track data.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - card_track_data: Internal track data.
    // 
    //   NOTES:
    //          - SQLFunction.
    //
    [SqlFunction()]
    public static String ToInternal(String CardTrackData)
    {
      String card_track_data;
      int card_type;

      if (CardTrackData == null || CardTrackData.Length <= 0) return "";

      if (!CardNumber.TrackDataToInternal(CardTrackData, out card_track_data, out card_type))
        return "";

      return card_track_data;
    }
   
}

public class CardNumber
{
  #region Constants

  private const Int32 CHECKSUM_MODULE = 9973;

  public enum CriptMode
  {
    NONE = 0,
    WCP = 1,
  }

  #endregion

  #region Attributes

  #endregion

  #region Methods


  private static readonly byte[] TRACKDATA_CRYPTO_KEY = new byte[] { 35, 33, 94, 72, 71, 94, 36, 35, 70, 68, 83, 72, 74, 118, 98, 45 };
  private static readonly byte[] TRACKDATA_CRYPTO_IV = new byte[] { 1 };


  //------------------------------------------------------------------------------
  // PURPOSE: Receive the track data and split it into the card data components.
  // 
  //  PARAMS:
  //      - INPUT:
  //          - ExternalTrackData: Encrypted track data.
  //
  //      - OUTPUT:
  //          - CardId: internal card identifier. (the one stored in the database)
  //          - CardType: card type.
  //
  // RETURNS:
  //      - True: Incoming track data has been decrypted and the checksum has been 
  //              correctly verified. Output parameters contain the valid results
  //      - False: Incoming track data has some obvious format errors (length) or 
  //               it fails the checksum test.
  // 
  //   NOTES:
  //    Card pattern: (20 digits)
  //      LCCCCSSSSNNNNNNNNNTT
  //    Where:
  //    - L: must be 0 (1 digit)
  //    - CCCC: Checksum. (4 digits)
  //    - SSSSNNNNNNNNN: Internal TrackData. (13 digits)
  //      - SSSS:      Site ID.         (4 digits)
  //      - NNNNNNNNN: Sequence Number. (9 digits)
  //    - TT is the card type (2 digits)
  //
  public static bool TrackDataToInternal(String ExternalTrackData, out String InternalTrackData, out int CardType)
  {
    RC4CryptoServiceProvider _rc4_csp;
    String leading_digit_str;
    String checksum_str;
    String card_id_str;
    String card_type_str;
    UInt32 calc_checksum;
    String calc_checksum_str;
    UInt64 enc_input_data;
    UInt64 output_data;
    UInt64 card_id;
    byte[] output_dec = new byte[8];
    byte[] checksum_data;
    String max_value_str;

    // Initialize
    InternalTrackData = "";
    CardType = 0;

    try
    {
      // Check for old format compatibility
      if (ExternalTrackData.Length < 13)
      {
        // Invalid card number
        return false;
      }

      _rc4_csp = new RC4CryptoServiceProvider();
      _rc4_csp.KeySize = 64;

      // Decrypt card data
      //Get a decryptor that uses the same key and IV as the encryptor.
      ICryptoTransform _decryptor = _rc4_csp.CreateDecryptor(TRACKDATA_CRYPTO_KEY, TRACKDATA_CRYPTO_IV);

      // Check that incoming parameter is not too big for an UInt64
      max_value_str = UInt64.MaxValue.ToString();

      if (String.Compare(ExternalTrackData, max_value_str) > 0)
      {
        return false;
      } // if

      // Obtain encrypted data
      enc_input_data = UInt64.Parse(ExternalTrackData);

      // Convert the int 64 into a byte array
      byte[] _bytes_to_decrypt;
      _bytes_to_decrypt = BitConverter.GetBytes(enc_input_data);

      // Now decrypt the previously encrypted message using the decryptor
      // obtained in the above step.
      MemoryStream _ms = new MemoryStream(_bytes_to_decrypt);
      CryptoStream _cs = new CryptoStream(_ms, _decryptor, CryptoStreamMode.Read);

      _cs.Read(output_dec, 0, 8);
      output_data = BitConverter.ToUInt64(output_dec, 0);

      // Obtain the 20 digit-long string
      ExternalTrackData = output_data.ToString();
      ExternalTrackData = ExternalTrackData.PadLeft(20, '0');

      // Split Card number into components:
      // It should follow the pattern (20 digits)
      //    LCCCCNNNNNNNNNNNNNTT
      // Where:
      //  - L: must be 0 (1 digit)
      //  - CCCC: Checksum. (4 digits)
      //  - NNNNNNNNNNNNN: Card identifier. (13 digits)
      //    - Site id. (4 digits)
      //    - sequence. (9 digits)
      //  - TT is the card type (2 digits)
      //
      leading_digit_str = ExternalTrackData.Substring(0, 1);
      checksum_str = ExternalTrackData.Substring(1, 4);
      card_id_str = ExternalTrackData.Substring(5, 13);
      card_type_str = ExternalTrackData.Substring(18, 2);

      // Check parts
      if (leading_digit_str != "0")
      {
        // Leading digit must be zero 
        return false;
      }

      calc_checksum = 0;
      card_id = UInt64.Parse(String.Concat(card_id_str, card_type_str));
      checksum_data = BitConverter.GetBytes(card_id);

      for (int i = 0; i < checksum_data.Length; i++)
      {
        calc_checksum += (Byte)(checksum_data[i]);
      }

      calc_checksum = calc_checksum % CHECKSUM_MODULE;
      calc_checksum_str = string.Format("{0:D4}", calc_checksum);

      if (checksum_str != calc_checksum_str)
      {
        // Incorrect checksum
        return false;
      }

      // Prepare results
      InternalTrackData = card_id_str;
      CardType = int.Parse(card_type_str);

      return true;
    }
    catch
    {
      return false;
    }

  } // TrackDataToCardNumber

  //------------------------------------------------------------------------------
  // PURPOSE: Convert the card number into a encrypted track data to be recorded.
  // 
  //  PARAMS:
  //      - INPUT:
  //          - CardId: internal card identifier (the one stored in the database)
  //          - CardType: card type.
  //
  //      - OUTPUT:
  //          - TrackData: Encrypted card data.
  //
  // RETURNS:
  //      - True: Track data has been built using the incoming parameters
  //      - False: CardId format error or CardType not supported
  // 
  //   NOTES:
  //
  //    Card pattern (20 digits)
  //      LCCCCNNNNNNNNNNNNNTT
  //    Where:
  //    - L: must be 0 (1 digit)
  //    - CCCC: Checksum. (4 digits)
  //    - NNNNNNNNNNNNN: Card identifier. (13 digits)
  //      - Site id. (4 digits)
  //      - sequence. (9 digits)
  //    - TT is the card type (2 digits)
  //
  public static bool TrackDataToExternal(out String ExternalTrackData, String InternalTrackData, int CardType)
  {
    RC4CryptoServiceProvider _rc4_csp;
    String _str_aux;
    String _aux_checksum;
    UInt32 _checksum;
    UInt64 _i64_aux;
    byte[] _checksum_bytes;

    // Initialize
    ExternalTrackData = "";

    try
    {
      // Check incoming parameters
      if (InternalTrackData.Length != 13)
      {
        return false;
      }

      if (CardType != 0)
      {
        // Non-supported card type
        return false;
      }

      _rc4_csp = new RC4CryptoServiceProvider();
      _rc4_csp.KeySize = 64;

      // Get an encryptor.
      ICryptoTransform _rc4 = _rc4_csp.CreateEncryptor(TRACKDATA_CRYPTO_KEY, TRACKDATA_CRYPTO_IV);

      // Encrypt the data as an array of encrypted bytes in memory.
      MemoryStream _ms = new MemoryStream();
      CryptoStream _cs = new CryptoStream(_ms, _rc4, CryptoStreamMode.Write);

      // Calculate checksum
      _i64_aux = UInt64.Parse(String.Concat(InternalTrackData, String.Format("{0:D2}", CardType)));
      _checksum_bytes = BitConverter.GetBytes(_i64_aux);

      _checksum = 0;
      for (int i = 0; i < _checksum_bytes.Length; i++)
      {
        _checksum += (Byte)(_checksum_bytes[i]);
      }

      _checksum = _checksum % CHECKSUM_MODULE;
      _aux_checksum = string.Format("{0:D4}", _checksum);

      // Build complete string
      _str_aux = String.Concat(_aux_checksum, InternalTrackData, String.Format("{0:D2}", CardType));

      // Encrypt the formatted buffer
      _i64_aux = UInt64.Parse(_str_aux);

      // Convert the int 64 into a byte arrary
      byte[] _to_encrypt;
      _to_encrypt = BitConverter.GetBytes(_i64_aux);

      // Write all data to the crypto stream and flush it.
      _cs.Write(_to_encrypt, 0, _to_encrypt.Length);
      _cs.FlushFinalBlock();

      // Get the encrypted array of bytes.
      byte[] _ext_trackdata = _ms.ToArray();

      ExternalTrackData = BitConverter.ToUInt64(_ext_trackdata, 0).ToString();
      // Format number using 20 digits (add zero if necessary)
      ExternalTrackData = ExternalTrackData.PadLeft(20, '0');

      return true;
    }
    catch
    {
      return false;
    }

  } // TrackDataToExternal


  //------------------------------------------------------------------------------
  // PURPOSE: Compose the card internal id. 
  // 
  // PARAMS:
  //      - INPUT:
  //          - SiteId: Site id.
  //          - Sequence: Sequence number.
  //
  //      - OUTPUT:
  //
  // RETURNS:
  //      - Card number.
  // 
  // NOTES:
  //
  public static String MakeInternalTrackData(int SiteId, Int64 Sequence)
  {
    return String.Concat(String.Format("{0:D4}", SiteId), String.Format("{0:D9}", Sequence));

  } // MakeInternalTrackData

  //------------------------------------------------------------------------------
  // PURPOSE: Split card number into its components:
  //          - Site Id.
  //          - Sequence number.
  // 
  // PARAMS:
  //      - INPUT:
  //          - CardNumber: Composed card internal id.
  //
  //      - OUTPUT:
  //          - SiteId: Site id.
  //          - Sequence: Sequence number.
  //
  // RETURNS:
  //
  // NOTES:
  //
  public static void SplitInternalTrackData(String InternalTrackData, out Int32 SiteId, out Int64 Sequence)
  {
    SiteId = Convert.ToInt32(InternalTrackData.Substring(0, 4));
    Sequence = Convert.ToInt64(InternalTrackData.Substring(4));

  } // SplitInternalTrackData


  public static Boolean ExternalTrackDataBelongToSite(int SiteId, String ExternalTrackData)
  {
    int _site_id;
    int _card_type;
    String _internal_trackdata;
    Int64 _sequence;


    if (!TrackDataToInternal(ExternalTrackData, out _internal_trackdata, out _card_type))
    {
      // Failed!
      return false;
    }

    if (_card_type != 0)
    {
      // Only 0 is supported
      return false;
    }

    SplitInternalTrackData(_internal_trackdata, out _site_id, out _sequence);

    return (SiteId == _site_id);
  } // ExternalTrackDataBelongToSite

  #endregion

  //------------------------------------------------------------------------------
  // PURPOSE: Split card number into its components:
  //          - Site Id.
  //          - Sequence number.
  // 
  // PARAMS:
  //      - INPUT:
  //          - Mode: Mode used to encrypt the track data.
  //          - ReceivedTrackData: Received Track data.
  //
  //      - OUTPUT:
  //          - DBTrackData: Track data stored on db.
  //
  // RETURNS:
  //
  // NOTES:
  //
  public static Boolean ConvertTrackData(CriptMode Mode, String ReceivedTrackData, out String DBTrackData)
  {
    DBTrackData = ReceivedTrackData;

    if (Mode == CriptMode.WCP)
    {
      int card_type;

      return TrackDataToInternal(ReceivedTrackData, out DBTrackData, out card_type);
    }

    return true;
  }

} // class CardNumber


internal sealed class ARCFourManagedTransform : ICryptoTransform
{

  #region Attributes

  private byte[] transform_key;
  private int key_length;
  private byte[] data_permutation;
  private byte index1;
  private byte index2;
  private bool dispose;

  #endregion

  #region Methods

  /// <summary>
  /// Initializes a new instance of the ARCFourManagedTransform class.
  /// </summary>
  /// <param name="key">The key used to initialize the ARCFour state.</param>
  public ARCFourManagedTransform(byte[] Key)
  {
    transform_key = (byte[])Key.Clone();
    key_length = Key.Length;
    data_permutation = new byte[256];

    Init();

  } // ARCFourManagedTransform

  /// <summary>
  /// Finalizes the object.
  /// </summary>
  //~ARCFourManagedTransform()
  //{
  //  Dispose();
  //} // ~ARCFourManagedTransform

  /// <summary>
  /// Gets a value indicating whether the current transform can be reused.
  /// </summary>
  /// <value>This property returns <b>true</b>.</value>
  public bool CanReuseTransform
  {
    get
    {
      return true;
    }
  }

  /// <summary>
  /// Gets a value indicating whether multiple blocks can be transformed.
  /// </summary>
  /// <value>This property returns <b>true</b>.</value>
  public bool CanTransformMultipleBlocks
  {
    get
    {
      return true;
    }
  }

  /// <summary>
  /// Gets the input block size.
  /// </summary>
  /// <value>The size of the input data blocks in bytes.</value>
  public int InputBlockSize
  {
    get
    {
      return 1;
    }
  }

  /// <summary>
  /// Gets the output block size.
  /// </summary>
  /// <value>The size of the input data blocks in bytes.</value>
  public int OutputBlockSize
  {
    get
    {
      return 1;
    }
  }

  /// <summary>
  /// Transforms the specified region of the input byte array and copies the resulting transform to the
  /// specified region of the output byte array.
  /// </summary>
  /// <param name="inputBuffer">The input for which to compute the transform.</param>
  /// <param name="inputOffset">The offset into the input byte array from which to begin using data.</param>
  /// <param name="inputCount">The number of bytes in the input byte array to use as data.</param>
  /// <param name="outputBuffer">The output to which to write the transform.</param>
  /// <param name="outputOffset">The offset into the output byte array from which to begin writing data.</param>
  /// <returns>The number of bytes written.</returns>
  /// <exception cref="ObjectDisposedException">The object has been disposed.</exception>
  /// <exception cref="ArgumentNullException"><paramref name="inputBuffer"/> or <paramref name="outputBuffer"/> is a null reference.</exception>
  /// <exception cref="ArgumentOutOfRangeException"><paramref name="inputOffset"/>, <paramref name="inputCount"/> or <paramref name="outputOffset"/> is invalid.</exception>
  public int TransformBlock(byte[] InputBuffer, int InputOffset, int InputCount, byte[] OutputBuffer, int OutputOffset)
  {
    if (dispose)
    {
      throw new ObjectDisposedException(this.GetType().FullName);
    }

    if (InputBuffer == null)
    {
      throw new ArgumentNullException("inputBuffer", "TransformBlock: Input buffer is null");
    }

    if (OutputBuffer == null)
    {
      throw new ArgumentNullException("outputBuffer", "TransformBlock: Output buffer is null");
    }

    if (InputOffset < 0 || OutputOffset < 0 || InputOffset + InputCount > InputBuffer.Length || OutputOffset + InputCount > OutputBuffer.Length)
    {
      throw new ArgumentOutOfRangeException("TransformBlock: Parameters out of range");
    }

    byte j, temp;
    int length = InputOffset + InputCount;

    //fixed (byte* permutation = data_permutation, output = OutputBuffer, input = InputBuffer)
    //{
    //  for (; InputOffset < length; InputOffset++, OutputOffset++)
    //  {
    //    // Update indexes
    //    index1 = (byte)((index1 + 1) % 256);
    //    index2 = (byte)((index2 + permutation[index1]) % 256);

    //    // Swap m_State.permutation[m_State.index1] and m_State.permutation[m_State.index2]
    //    temp = permutation[index1];
    //    permutation[index1] = permutation[index2];
    //    permutation[index2] = temp;

    //    // Transform byte
    //    j = (byte)((permutation[index1] + permutation[index2]) % 256);
    //    output[OutputOffset] = (byte)(input[InputOffset] ^ permutation[j]);
    //  }
    //}

    //fixed (byte* permutation = data_permutation, output = OutputBuffer, input = InputBuffer)
    //{
      for (; InputOffset < length; InputOffset++, OutputOffset++)
      {
        // Update indexes
        index1 = (byte)((index1 + 1) % 256);
        index2 = (byte)((index2 + data_permutation[index1]) % 256);

        // Swap m_State.permutation[m_State.index1] and m_State.permutation[m_State.index2]
        temp = data_permutation[index1];
        data_permutation[index1] = data_permutation[index2];
        data_permutation[index2] = temp;

        // Transform byte
        j = (byte)((data_permutation[index1] + data_permutation[index2]) % 256);
        OutputBuffer[OutputOffset] = (byte)(InputBuffer[InputOffset] ^ data_permutation[j]);
      }
    //}

    return InputCount;
  } // TransformBlock

  /// <summary>
  /// Transforms the specified region of the specified byte array.
  /// </summary>
  /// <param name="inputBuffer">The input for which to compute the transform.</param>
  /// <param name="inputOffset">The offset into the byte array from which to begin using data.</param>
  /// <param name="inputCount">The number of bytes in the byte array to use as data.</param>
  /// <returns>The computed transform.</returns>
  /// <exception cref="ObjectDisposedException">The object has been disposed.</exception>
  /// <exception cref="ArgumentNullException"><paramref name="inputBuffer"/> is a null reference.</exception>
  /// <exception cref="ArgumentOutOfRangeException"><paramref name="inputOffset"/> or <paramref name="inputCount"/> is invalid.</exception>
  public byte[] TransformFinalBlock(byte[] InputBuffer, int InputOffset, int InputCount)
  {
    if (dispose)
    {
      throw new ObjectDisposedException(GetType().FullName);
    }

    byte[] ret = new byte[InputCount];
    TransformBlock(InputBuffer, InputOffset, InputCount, ret, 0);
    Init();

    return ret;
  } // TransformFinalBlock

  /// <summary>
  /// This method (re)initializes the cipher.
  /// </summary>
  private void Init()
  {
    byte temp;

    // Init state variable
    for (int i = 0; i < data_permutation.Length; i++)
    {
      data_permutation[i] = (byte)i;
    }

    index1 = 0;
    index2 = 0;
    // Randomize, using key
    for (int j = 0, i = 0; i < data_permutation.Length; i++)
    {
      j = (j + data_permutation[i] + transform_key[i % key_length]) % 256;

      // Swap m_State.permutation[i] and m_State.permutation[j]
      temp = data_permutation[i];
      data_permutation[i] = data_permutation[j];
      data_permutation[j] = temp;
    }
  } // Init

  /// <summary>
  /// Disposes of the cryptographic parameters.
  /// </summary>
  public void Dispose()
  {
    if (!dispose)
    {
      Array.Clear(transform_key, 0, transform_key.Length);
      Array.Clear(data_permutation, 0, data_permutation.Length);
      index1 = 0;
      index2 = 0;
      dispose = true;
      GC.SuppressFinalize(this);
    }
  } // Dispose

  #endregion

} // ARCFourManagedTransform

/// <summary>
/// Accesses the managed version of the ARCFour algorithm. This class cannot be inherited.
/// ARCFour is fully compatible with the RC4<sup>TM</sup> algorithm.
/// </summary>
/// <remarks>
/// RC4 is a trademark of RSA Data Security Inc.
/// </remarks>
public sealed class ARCFourManaged : RC4
{
  #region Attributes

  private bool is_disposed;

  #endregion

  #region Methods

  /// <summary>
  /// Constructor method: Initializes a new instance of the ARCFourManaged class.
  /// </summary>
  /// <remarks>
  /// The default keysize is 128 bits.
  /// </remarks>
  public ARCFourManaged()
  {
  } // ARCFourManaged

  /// <summary>
  /// Creates a symmetric <see cref="RC4"/> decryptor object with the specified Key.
  /// </summary>
  /// <param name="rgbKey">The secret key to be used for the symmetric algorithm.</param>
  /// <param name="rgbIV">This parameter is not used an should be set to a null reference, 
  /// or to an array with zero or one bytes.</param>
  /// <returns>A symmetric ARCFour decryptor object.</returns>
  /// <remarks>This method decrypts an encrypted message created using the <see cref="CreateEncryptor"/> 
  /// overload with the same signature.</remarks>
  public override ICryptoTransform CreateDecryptor(byte[] RgbKey, byte[] RgbIV)
  {
    if (is_disposed)
    {
      throw new ObjectDisposedException(GetType().FullName);
    }

    if (RgbKey == null)
    {
      throw new ArgumentNullException("RgbKey", "CreateDecryptor: Rgbkey is null");
    }

    if (RgbKey.Length == 0 || RgbKey.Length > 256)
    {
      throw new CryptographicException("CreateDecryptor: Invalid key size");
    }

    if (RgbIV != null && RgbIV.Length > 1)
    {
      throw new CryptographicException("CreateDecryptor: Invalid IV size");
    }

    return new ARCFourManagedTransform(RgbKey);
  } // CreateDecryptor

  /// <summary>
  /// Creates a symmetric <see cref="RC4"/> encryptor object with the specified Key.
  /// </summary>
  /// <param name="rgbKey">The secret key to be used for the symmetric algorithm.</param>
  /// <param name="rgbIV">This parameter is not used an should be set to a null reference, or to an array with zero or one bytes.</param>
  /// <returns>A symmetric ARCFour encryptor object.</returns>
  /// <remarks>Use the <see cref="CreateDecryptor"/> overload with the same signature to decrypt the result of this method.</remarks>
  public override ICryptoTransform CreateEncryptor(byte[] RgbKey, byte[] RgbIV)
  {
    return CreateDecryptor(RgbKey, RgbIV);
  } // CreateEncryptor

  /// <summary>
  /// Releases the unmanaged resources used by the <see cref="ARCFourManaged"/> and optionally
  /// releases the managed resources.
  /// </summary>
  /// <param name="disposing"><b>true</b> to release both managed and unmanaged resources; <b>false</b>
  /// to release only unmanaged resources.</param>
  protected override void Dispose(bool Disposing)
  {
    base.Dispose(Disposing);
    is_disposed = true;
  } // Dispose

  #endregion

} //  ARCFourManaged

/// <summary>
/// Defines a wrapper object to access the cryptographic service provider (CSP)
/// version of the RC4 algorithm. This class cannot be inherited.
/// </summary>
public sealed class RC4CryptoServiceProvider : RC4
{
  #region Attributes

  private ARCFourManaged arcfourmanaged;
  private bool dispose;

  #endregion

  #region Methods

  /// <summary>
  /// Initializes a new instance of the <see cref="RC4CryptoServiceProvider"/> class.
  /// </summary>
  /// <exception cref="CryptographicException">An error occurs while acquiring the CSP.</exception>
  public RC4CryptoServiceProvider()
  {
    arcfourmanaged = new ARCFourManaged();
  }

  /// <summary>
  /// Finalizes the RC4CryptoServiceProvider.
  /// </summary>
  //~RC4CryptoServiceProvider()
  //{
  //  Dispose();
  //}

  /// <summary>
  /// Gets or sets the block size of the cryptographic operation in bits.
  /// </summary>
  /// <value>The block size of RC4 is always 8 bits.</value>
  /// <exception cref="CryptographicException">The block size is invalid.</exception>
  public override int BlockSize
  {
    get
    {
      return arcfourmanaged.BlockSize;
    }
    set
    {
      if (value != 8)
      {
        throw new CryptographicException("BlockSize: Invalid block size");
      }

      arcfourmanaged.BlockSize = value;
    }
  }

  /// <summary>
  /// Gets or sets the feedback size of the cryptographic operation in bits.
  /// </summary>
  /// <value>This property always throws a <see cref="CryptographicException"/>.</value>
  /// <exception cref="CryptographicException">This exception is always thrown.</exception>
  /// <remarks>RC4 doesn't use the FeedbackSize property.</remarks>
  public override int FeedbackSize
  {
    get
    {
      return arcfourmanaged.FeedbackSize;
    }
    set
    {
      arcfourmanaged.FeedbackSize = value;
    }
  }

  /// <summary>
  /// Gets or sets the initialization vector (IV) for the symmetric algorithm.
  /// </summary>
  /// <value>This property always returns a byte array of length one. 
  /// The value of the byte in the array is always set to zero.</value>
  /// <exception cref="CryptographicException">An attempt is made to set the IV to an invalid instance.</exception>
  /// <remarks>RC4 doesn't use the IV property, however the property accepts IV's of up to one byte
  /// (RC4's <see cref="BlockSize"/>) in order to interoperate with software that has been written with
  /// the use of block ciphers in mind.</remarks>
  public override byte[] IV
  {
    get
    {
      return arcfourmanaged.IV;
    }
    set
    {
      arcfourmanaged.IV = value;
    }
  }

  /// <summary>
  /// Gets or sets the secret key for the symmetric algorithm.
  /// </summary>
  /// <value>The secret key to be used for the symmetric algorithm.</value>
  public override byte[] Key
  {
    get
    {
      return arcfourmanaged.Key;
    }
    set
    {
      arcfourmanaged.Key = value;
    }
  } // Key

  /// <summary>
  /// Gets or sets the size of the secret key used by the symmetric algorithm in bits.
  /// </summary>
  /// <value>The size of the secret key used by the symmetric algorithm.</value>
  /// <exception cref="CryptographicException">The key size is not valid.</exception>
  public override int KeySize
  {
    get
    {
      return arcfourmanaged.KeySize;
    }
    set
    {
      arcfourmanaged.KeySize = value;
    }
  } // KeySize

  /// <summary>
  /// Gets the block sizes that are supported by the symmetric algorithm.
  /// </summary>
  /// <value>An array containing the block sizes supported by the algorithm.</value>
  /// <remarks>Only a block size of one byte is supported by the RC4 algorithm.</remarks>
  public override KeySizes[] LegalBlockSizes
  {
    get
    {
      return arcfourmanaged.LegalBlockSizes;
    }
  } // LegalBlockSizes

  /// <summary>
  /// Gets the key sizes that are supported by the symmetric algorithm.
  /// </summary>
  /// <value>An array containing the key sizes supported by the algorithm.</value>
  /// <remarks>Only key sizes that match an entry in this array are supported by
  /// the symmetric algorithm.</remarks>
  public override KeySizes[] LegalKeySizes
  {
    get
    {
      return arcfourmanaged.LegalKeySizes;
    }
  } // LegalKeySizes

  /// <summary>
  /// Gets or sets the mode for operation of the symmetric algorithm.
  /// </summary>
  /// <value>The mode for operation of the symmetric algorithm.</value>
  /// <remarks>RC4 only supports the OFB cipher mode. See <see cref="CipherMode"/> for a
  /// description of this mode.</remarks>
  /// <exception cref="CryptographicException">The cipher mode is not OFB.</exception>
  public override CipherMode Mode
  {
    get
    {
      return arcfourmanaged.Mode;
    }
    set
    {
      arcfourmanaged.Mode = value;
    }
  } // Mode

  /// <summary>
  /// Gets or sets the padding mode used in the symmetric algorithm.
  /// </summary>
  /// <value>The padding mode used in the symmetric algorithm. This property always returns
  /// PaddingMode: None.</value>
  /// <exception cref="CryptographicException">The padding mode is set to a padding mode other
  /// than PaddingMode: None.</exception>
  public override PaddingMode Padding
  {
    get
    {
      return arcfourmanaged.Padding;
    }
    set
    {
      arcfourmanaged.Padding = value;
    }
  } // Padding

  /// <summary>
  /// This is a stub method.
  /// </summary>
  /// <remarks>Since the RC4 cipher doesn't use an Initialization Vector, this method 
  /// will not do anything. Created to be a crypto service provider methods compatible.</remarks>
  public override void GenerateIV()
  {
    arcfourmanaged.GenerateIV();
  } // GenerateIV

  /// <summary>
  /// Generates a random Key to be used for the algorithm.
  /// </summary>
  /// <remarks>Use this method to generate a random key when none is specified.</remarks>
  public override void GenerateKey()
  {
    arcfourmanaged.GenerateKey();
  } // GenerateKey

  /// <summary>
  /// Creates a symmetric decryptor object with the specified Key.
  /// </summary>
  /// <param name="rgbKey">The secret key to be used for the symmetric algorithm. </param>
  /// <param name="rgbIV">Not used in RC4. It can be a null reference or a byte array with a length less than 2.</param>
  /// <returns>A symmetric decryptor object.</returns>
  /// <remarks>This method decrypts an encrypted message created using the <see cref="CreateEncryptor"/> overload with the same parameters.</remarks>
  /// <exception cref="ObjectDisposedException">The object is disposed.</exception>
  /// <exception cref="ArgumentNullException"><paramref name="rgbKey"/> is a null reference (<b>Nothing</b> in Visual Basic).</exception>
  /// <exception cref="CryptographicException"></exception>
  public override ICryptoTransform CreateDecryptor(byte[] RgbKey, byte[] RgbIV)
  {
    if (dispose)
    {
      throw new ObjectDisposedException(GetType().FullName, "CreateDecryptor: Dispose");
    }

    if (RgbKey == null)
    {
      throw new ArgumentNullException("rgbKey", "CreateDecryptor: rbgkey is null");
    }

    if (RgbKey.Length == 0 || RgbKey.Length > 256)
    {
      throw new CryptographicException("CreateDecryptor: Invalid Key Size");
    }

    if (RgbIV != null && RgbIV.Length > 1)
    {
      throw new CryptographicException("CreateDecryptor: Invalid IV Size");
    }

    return arcfourmanaged.CreateDecryptor(RgbKey, RgbIV);

  } // CreateDecryptor

  /// <summary>
  /// Creates a symmetric encryptor object with the specified Key.
  /// </summary>
  /// <param name="rgbKey">The secret key to be used for the symmetric algorithm. </param>
  /// <param name="rgbIV">Not used in RC4. It can be a null reference or a byte array with a length less than 2.</param>
  /// <returns>A symmetric encryptor object.</returns>
  /// <remarks>Use the <see cref="CreateDecryptor"/> overload with the same parameters to decrypt the result of this method.</remarks>
  /// <exception cref="ObjectDisposedException">The object is disposed.</exception>
  /// <exception cref="ArgumentNullException"><paramref name="rgbKey"/> is a null reference (<b>Nothing</b> in Visual Basic).</exception>
  /// <exception cref="CryptographicException"></exception>
  public override ICryptoTransform CreateEncryptor(byte[] RgbKey, byte[] RgbIV)
  {
    return this.CreateDecryptor(RgbKey, RgbIV);
  } // CreateEncryptor

  /// <summary>
  /// Dispose method.
  /// </summary>
  private void Dispose()
  {
    if (!dispose)
    {
      dispose = true;

      if (arcfourmanaged != null)
      {
        arcfourmanaged.Clear();
        arcfourmanaged = null;
      }

      GC.SuppressFinalize(this);
    }
  } // Dispose

  #endregion

} // RC4CryptoServiceProvider

public abstract class RC4 : SymmetricAlgorithm
{
  #region Attributes

  #endregion

  #region Properties

  /// <summary>
  /// Constructor method.
  /// Class RC4 is defined as any C# framework cryptographic algorithm
  /// having the same class attributes.
  /// </summary>
  /// <remarks>
  /// The default keysize is 64 bits.
  /// </remarks>
  protected RC4()
  {
    KeySizeValue = 64;
  }

  /// <summary>
  /// Gets or sets the block size of the cryptographic operation in bits.
  /// </summary>
  /// <value>The block size of RC4 is always 8 bits.</value>
  /// <exception cref="CryptographicException">The block size is invalid.</exception>
  public override int BlockSize
  {
    get
    {
      return 8;
    }
    set
    {
      if (value != 8)
      {
        throw new CryptographicException("BlockSize: Error Invalid Block Size");
      }
    }
  } // BlockSize

  /// <summary>
  /// Gets or sets the feedback size of the cryptographic operation in bits.
  /// </summary>
  /// <value>This property always throws a <see cref="CryptographicException"/>.</value>
  /// <exception cref="NotSupportedException">This exception is always thrown.</exception>
  /// <remarks>RC4 doesn't use the FeedbackSize property.</remarks>
  public override int FeedbackSize
  {
    get
    {
      throw new NotSupportedException();
    }
    set
    {
      throw new NotSupportedException();
    }
  } // FeedbackSize

  /// <summary>
  /// Gets or sets the initialization vector (IV) for the symmetric algorithm.
  /// </summary>
  /// <value>This property always returns a byte array of length one.
  /// The value of the byte in the array is always set to zero.</value>
  /// <exception cref="CryptographicException">An attempt is made to set the IV to an invalid instance.</exception>
  /// <remarks>RC4 doesn't use the IV property, however the property accepts IV's of up to one byte (RC4's <see cref="BlockSize"/>) in
  /// order to interoperate with software that has been written with the use of block ciphers in mind.</remarks>
  public override byte[] IV
  {
    get
    {
      return new byte[1];
    }
    set
    {
      if (value != null && value.Length > 1)
      {
        throw new CryptographicException("IV: Error Invalid IV Size");
      }
    }
  } // IV

  /// <summary>
  /// Gets the block sizes that are supported by the symmetric algorithm.
  /// </summary>
  /// <value>An array containing the block sizes supported by the algorithm.</value>
  /// <remarks>Only a block size of one byte is supported by the RC4 algorithm.</remarks>
  public override KeySizes[] LegalBlockSizes
  {
    get
    {
      return new KeySizes[] { new KeySizes(8, 8, 0) };
    }
  } // LegalBlockSizes

  /// <summary>
  /// Gets the key sizes that are supported by the symmetric algorithm.
  /// </summary>
  /// <value>An array containing the key sizes supported by the algorithm.</value>
  /// <remarks>Only key sizes that match an entry in this array are supported by the symmetric algorithm.</remarks>
  public override KeySizes[] LegalKeySizes
  {
    get
    {
      return new KeySizes[] { new KeySizes(8, 2048, 8) };
    }
  } // LegalKeySizes

  /// <summary>
  /// Gets or sets the mode for operation of the symmetric algorithm.
  /// </summary>
  /// <value>The mode for operation of the symmetric algorithm.</value>
  /// <remarks>RC4 only supports the OFB cipher mode. See <see cref="CipherMode"/> for a description of this mode.</remarks>
  /// <exception cref="CryptographicException">The cipher mode is not OFB.</exception>
  public override CipherMode Mode
  {
    get
    {
      return CipherMode.OFB;
    }
    set
    {
      if (value != CipherMode.OFB)
      {
        throw new CryptographicException("Mode: Error Invalid Cipher Mode");
      }
    }
  } // Mode

  /// <summary>
  /// Gets or sets the padding mode used in the symmetric algorithm.
  /// </summary>
  /// <value>The padding mode used in the symmetric algorithm. This property always returns PaddingMode:None.</value>
  /// <exception cref="CryptographicException">The padding mode is set to a padding mode other than PaddingMode.None.</exception>
  public override PaddingMode Padding
  {
    get
    {
      return PaddingMode.None;
    }
    set
    {
      if (value != PaddingMode.None)
      {
        throw new CryptographicException("Padding: Error Invalid Padding Mode");
      }
    }
  } // Padding

  #endregion

  #region Methods

  /// <summary>
  /// This is a stub method.
  /// </summary>
  /// <remarks>Since the RC4 cipher doesn't use an Initialization Vector, this method will not do anything.</remarks>
  public override void GenerateIV()
  {
  } // GenerateIV

  /// <summary>
  /// Generates a random Key to be used for the algorithm.
  /// </summary>
  /// <remarks>Use this method to generate a random key when none is specified.</remarks>
  public override void GenerateKey()
  {
    RNGCryptoServiceProvider rng;

    rng = new RNGCryptoServiceProvider();

    byte[] key = new byte[KeySize / 8];

    rng.GetBytes(key);

    Key = key;
  } // GenerateKey

  /// <summary>
  /// Creates an instance of the default cryptographic object used to perform the RC4 transformation.
  /// </summary>
  /// <returns>The instance of a cryptographic object used to perform the RC4 transformation.</returns>
  /// <exception cref="CryptographicException">An error occurs while acquiring the CSP.</exception>
  /// <exception cref="InvalidOperationException">FIPS compliance is required, but the current implementation 
  /// isn't FIPS verified.</exception>
  public static new RC4 Create()
  {
    return Create("ARCFOUR");
  }

  /// <summary>
  /// Creates an instance of the specified cryptographic object used to perform the RC4 transformation.
  /// </summary>
  /// <param name="algName">The name of the specific implementation of <see cref="RC4"/> to create.</param>
  /// <returns>A cryptographic object.</returns>
  /// <exception cref="CryptographicException">An error occurs while acquiring the CSP.</exception>
  /// <exception cref="InvalidOperationException">FIPS compliance is required, but the current implementation isn't FIPS verified.</exception>
  public static new RC4 Create(string AlgName)
  {
    if (AlgName == null)
    {
      throw new ArgumentNullException("algName", "Create: Error_ParamNull");
    }

    if (string.Equals(AlgName, "RC4", StringComparison.InvariantCultureIgnoreCase))
    {
      return new RC4CryptoServiceProvider();
    }
    else if (string.Equals(AlgName, "ARCFOUR", StringComparison.InvariantCultureIgnoreCase))
    {
      return new ARCFourManaged();
    }
    return null;
  } // Create

  #endregion
}
