﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common;

namespace WSI.SQLBusinessLogic.Test
{
  [TestClass]
  public class FBMBillInTests
  {
    [TestMethod]
    public void TestBillInNoMismatch()
    {

      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_307;" + "user=sa;password=Manager00;"))
      {
        _con.Open();
        IDbCommand _dbc = _con.CreateCommand();
        _dbc.CommandText =
          "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
        decimal _initial_balance = (decimal)_dbc.ExecuteScalar();

        using (IDbTransaction _trx = _con.BeginTransaction())
        {
          var _startstate = T3GS.Start("FBM", "DPC001", 100, "09739858831662244640", _trx as SqlTransaction);

          //play 5 games at 10 each play
          //win 200
          //insert bill of 1000
          
          MultiPromos.PlayedWonMeters _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)1000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 50,
            PlayedCount = 5,
            WonAmount = (decimal)200.00,
            WonCount = 1
          };
          decimal _newtotal = _startstate.IniBalance.TotalBalance + (decimal)1150.00;

          var v = T3GS.Update("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);


           _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)2000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 100,
            PlayedCount = 10,
            WonAmount = (decimal)400.00,
            WonCount = 2
          };
           _newtotal += (decimal)1150.00;

          var z =T3GS.Update("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);

          Console.Write(v);
          Console.Write(z);

          _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)3000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 150,
            PlayedCount = 15,
            WonAmount = (decimal)600.00,
            WonCount = 3
          };
          _newtotal += (decimal)1150.00;
          var _endstate = T3GS.End("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);
          _dbc = _trx.Connection.CreateCommand();
          _dbc.Transaction = _trx;
          _dbc.CommandText =
            "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
          decimal _final_balance = (decimal)_dbc.ExecuteScalar();

          Assert.AreEqual(MultiPromos.EndSessionStatus.Ok, _endstate.StatusCode);
          Assert.AreEqual(_newtotal, _endstate.PlaySessionFinalBalance.TotalBalance);
          Assert.AreNotEqual(_final_balance, _initial_balance);
          _trx.Commit();
        }
      }
    }

    [TestMethod]
    public void TestBillInBigMismatch()
    {

      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_307;" + "user=sa;password=Manager00;"))
      {
        _con.Open();
        IDbCommand _dbc = _con.CreateCommand();
        _dbc.CommandText =
          "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
        decimal _initial_balance = (decimal)_dbc.ExecuteScalar();
        using (IDbTransaction _trx = _con.BeginTransaction())
        {
          var _startstate = T3GS.Start("FBM", "DPC001", 100, "09739858831662244640", _trx as SqlTransaction);

          //play 5 games at 10 each play
          //win 200
          //insert bill of 1000

          MultiPromos.PlayedWonMeters _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)1000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 50,
            PlayedCount = 5,
            WonAmount = (decimal)200.00,
            WonCount = 1
          };
          decimal _newtotal = _startstate.IniBalance.TotalBalance + (decimal)1150.00;

          T3GS.Update("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);

          _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)1500.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 100,
            PlayedCount = 10,
            WonAmount = (decimal)400.00,
            WonCount = 2
          };
          _newtotal = _newtotal + 1150;
          var _endstate = T3GS.End("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);
          _dbc = _trx.Connection.CreateCommand();
          _dbc.Transaction = _trx;
          _dbc.CommandText =
            "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
          decimal _final_balance = (decimal)_dbc.ExecuteScalar();

          Assert.AreEqual(MultiPromos.EndSessionStatus.BalanceMismatch, _endstate.StatusCode);
          Assert.AreNotEqual(_newtotal, _endstate.PlaySessionFinalBalance.TotalBalance);
          Assert.AreNotEqual(_final_balance, _initial_balance);
          _trx.Rollback();
        }
      }
    }
    
    [TestMethod]
    public void TestBillInSmalMismatchl()
    {

      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_307;" + "user=sa;password=Manager00;"))
      {
        _con.Open();
        IDbCommand _dbc = _con.CreateCommand();
        _dbc.CommandText =
          "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
        decimal _initial_balance= (decimal)_dbc.ExecuteScalar();
        using (IDbTransaction _trx = _con.BeginTransaction())
        {
          var _startstate = T3GS.Start("FBM", "DPC001", 100, "09739858831662244640", _trx as SqlTransaction);

          //play 5 games at 10 each play
          //win 200
          //insert bill of 1000
          
          MultiPromos.PlayedWonMeters _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)1000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 50,
            PlayedCount = 5,
            WonAmount = (decimal)200.00,
            WonCount = 1
          };
          decimal _newtotal = _startstate.IniBalance.TotalBalance + (decimal)1150.00;

          T3GS.Update("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);

          _playmeters = new MultiPromos.PlayedWonMeters
          {
            BillInAmount = (decimal)2000.00,
            HandpaysAmount = 0,
            JackpotAmount = 0,
            PlayedAmount = 100,
            PlayedCount = 10,
            WonAmount = (decimal)400.00,
            WonCount = 2
          };
          _newtotal = _newtotal + 1100;
          var _endstate = T3GS.End("FBM", "DPC001", 100, "09739858831662244640", _startstate.PlaySessionId,
            _playmeters,
            _newtotal, _trx as SqlTransaction);

          _dbc = _trx.Connection.CreateCommand();
          _dbc.Transaction = _trx;
          _dbc.CommandText =
            "select ac_balance from accounts where ac_track_data= dbo.TrackDataToInternal('09739858831662244640')";
          decimal _final_balance = (decimal)_dbc.ExecuteScalar();
          Assert.AreEqual(MultiPromos.EndSessionStatus.Ok, _endstate.StatusCode);
          Assert.AreEqual(_newtotal, _endstate.PlaySessionFinalBalance.TotalBalance);
          Assert.AreNotEqual(_final_balance,_initial_balance);
          _trx.Rollback();
        }
      }
    }

  }
}
