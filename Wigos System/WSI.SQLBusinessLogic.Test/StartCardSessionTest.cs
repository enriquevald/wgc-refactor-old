﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common;

namespace WSI.SQLBusinessLogic.Test
{
  [TestClass]
  public class StartCardSessionTest
  {
    /// <summary>
    /// Test for testing StartCardSession and see obtained Playable Balance
    /// </summary>
    [TestMethod]
    public void StartCardSession()
    {
      MultiPromos.StartSessionOutput _output;
      Decimal _playable_balance;

      using (IDbConnection _con = new SqlConnection("Data" + " Source=VSCAGE;" + "Initial Catalog=wgdb_307;" + "user=sa;password=Manager00;"))
      {
        _con.Open();
        IDbCommand _dbc = _con.CreateCommand();
        _dbc.CommandText = "SELECT AC_BALANCE FROM ACCOUNTS WHERE AC_TRACK_DATA= DBO.TRACKDATATOINTERNAL('09804450850226071080')";
        decimal _initial_balance = (decimal)_dbc.ExecuteScalar();

        using (IDbTransaction _trx = _con.BeginTransaction())
        {
          _output = T3GS.Start("FBM", "DPC001", 100, "09804450850226071080", _trx as SqlTransaction);
          if (_output.PlayableBalance.GetOnlyReserved)
          {
            _playable_balance =  _output.PlayableBalance.Reserved;
          }
          else
          {
            _playable_balance =  _output.PlayableBalance.TotalBalance;
          }

          _trx.Commit();
        }
        
      }
    }
  }
}
