﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SqlProcedures = WSI.Common.SqlProcedures;
using WSI.Common;

namespace WSI.SQLBusinessLogic.Test
{
  [TestClass]
  class EndCardSessionTests
  {
    [TestMethod]
    public void TestEndCardSessionLosing100Credits()
    {
      string _query_play_session_base;
      string _holder_name_like = "CAGE A";
      string _vendor_id = "FBM";
      string _dummy_serial_number = "DPC001";
      int _dummy_machine_number = 100;
      string _dummy_external_trackdata;
      long _play_session_id;
      decimal _played_amount;
      decimal _won_amount;
      decimal _played_reedemable;
      decimal _played_non_restricted_cents;
      decimal _played_restricted_cents;

      int _played_count;
      int _won_count;
      decimal _calculated_balance;
      decimal _bill_in;
      int _status_code;
      string _status_text;
      string _status_error;
      decimal _initial_balance;

      MultiPromos.EndSessionOutput _output;
      MultiPromos.PlayedWonMeters _meters;

      _status_code = 4;
      _status_text = "Access Denied";
      _status_error = "";


      try
      {
        using (SqlConnection _conn = new SqlConnection("Data" +
                           " Source=127.0.0.1;" + "Initial Catalog=wgdb_307;" + "user=sa;password=Manager00;"))
        {
          _conn.Open();

          _query_play_session_base = string.Format("select TOP(1) # from play_sessions where ps_account_id = (SELECT ac_account_id as AccountId FROM ACCOUNTS WHERE ac_holder_name like '%{0}%') order by ps_play_session_id desc", _holder_name_like);

          IDbCommand dummyInfoCMD = _conn.CreateCommand();
          dummyInfoCMD.CommandText = string.Format("SELECT  TOP(1) DBO.TRACKDATATOEXTERNAL(AC_TRACK_DATA) as TrackData FROM ACCOUNTS WHERE ac_holder_name like '%{0}%'", _holder_name_like);
          _dummy_external_trackdata = (string)dummyInfoCMD.ExecuteScalar();
          dummyInfoCMD.CommandText = _query_play_session_base.Replace("#", "ps_play_session_id");
          _play_session_id = (long)dummyInfoCMD.ExecuteScalar();
          dummyInfoCMD.CommandText = _query_play_session_base.Replace("#", "ps_initial_balance");
          _initial_balance = (decimal)dummyInfoCMD.ExecuteScalar();
          dummyInfoCMD.CommandText = _query_play_session_base.Replace("#", "ps_final_balance");
         
          _bill_in = 0;
          _won_count = 1;
          _won_amount = 1;
          _played_count = 1;
          _played_amount = 101;
          _played_reedemable = 101;
          _played_non_restricted_cents = 0;
          _played_restricted_cents = 0;

          if (_initial_balance>0)
          {
            _calculated_balance = _initial_balance + _won_amount - _played_amount + _bill_in;
          }
          else
          {
            _bill_in = 0;
            _won_count = 0;
            _won_amount = 0;
            _played_count = 0;
            _played_amount = 0;
            _calculated_balance = 0;
            _played_reedemable = 0;
            _played_non_restricted_cents = 0;
            _played_restricted_cents = 0;

          }
          
          
          using (SqlTransaction _trx = _conn.BeginTransaction())
          {
            _trx.Save("Trx_3GS_EndCardSession");

            try
            {
              _meters = new MultiPromos.PlayedWonMeters();
              _meters.PlayedCount = _played_count;
              _meters.PlayedAmount = _played_amount;
              _meters.WonCount = _won_count;
              _meters.WonAmount = _won_amount;
              _meters.JackpotAmount = 0;
              _meters.BillInAmount = _bill_in;

              _meters.PlayedReedemable = _played_reedemable;
              _meters.PlayedNonRestrictedAmount = _played_non_restricted_cents;
              _meters.PlayedRestrictedAmount = _played_restricted_cents;


              _output = T3GS.End(_vendor_id, _dummy_serial_number, _dummy_machine_number, _dummy_external_trackdata, _play_session_id, _meters, _calculated_balance, _trx);

              T3GS.Translate(_output, out _status_code, out _status_text, out _status_error);
            }
            catch (Exception _ex)
            {
              try
              {
                _trx.Rollback("Trx_3GS_EndCardSession");
              }
              catch { }

              throw _ex;
            }
            finally
            {
              _trx.Commit();
            }
            Assert.AreEqual(_output.PlaySessionFinalBalance.BalanceEGM.TotalBalance, _calculated_balance);
          }
        }
      }
      catch (Exception _ex)
      {
        _status_code = 4;
        _status_text = "Access Denied";
        _status_error = "Trx_3GS_EndCardSession failed!, Exception: " + _ex.Message;
      }
    } //TestEndCardSession
  }
}
