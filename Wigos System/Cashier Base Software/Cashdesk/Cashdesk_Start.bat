rem software update
:SOFT_UPDATE
if not exist "c:\Wigos\Cashdesk\Release_Candidate" goto START_APP
call "c:\Wigos\Cashdesk\ChangeWallpaper.bat" "c:\Wigos\Cashdesk\UM_UpdatingApplication.bmp"
xcopy "c:\Wigos\Cashdesk\Release_Candidate\*.*" "c:\Wigos\Cashdesk\*.*" /R /K /Y
del "c:\Wigos\Cashdesk\Release_Candidate\*.*" /Q /F
rd "c:\Wigos\Cashdesk\Release_Candidate"

rem software start
:START_APP
call "c:\Wigos\Cashdesk\ChangeWallpaper.bat" "c:\Wigos\Cashdesk\UM_Logo.bmp"
start /WAIT /D"c:\Wigos\Cashdesk" c:\Wigos\Cashdesk\WSI.Cashier.exe
