This file contains supplementary information about this MAGICARD CD-ROM.
The information contained in this document is subject to change without notice.
Ultra Electronics shall not be liable for errors contained herein or
for incidental consequential damages in connection with the furnishing,
performance or use of this material.

Applications
============
The 32-bit software on this disc is suitable for the following PC operating
systems:
    Windows 2000, Windows 2003 Server, WINDOWS XP and Windows Vista.
    It it not intended for Windows 95/98, Windows ME or Windows NT or Windows Vista.
    Please note that the Magicard Printer Drivers support Windows Vista through
    Microsofts Basic Signature.


Virus Protection
================
Ultra Electronics Card Systems has checked this CD-ROM for known viruses at
all stages of production, but cannot accept liability for damage to your data
or computer system which may occur while using either the disc or any
software contained on it.
By running the disk, you agree with these conditions. It is good practise to
run a virus checker on any new software before running it on your computer
and to make regular backup copies of your data.


Installation
============
This disc should autoplay on insertion into your CD-ROM drive. If it fails to
start automatically:

Run D:\AUTORUN.EXE from your WINDOWS Start button
(if D: is your CD-ROM drive letter).

Have you rebooted your system with the CD-ROM in the drive?
If so, reboot without the CD-ROM in the drive and only insert it once the
system is running and you have logged in.


Contents
========

The User Guides are in Adobe Acrobat format. 

To view the Acrobat documents, you need to have the free Acrobat Reader installed on your computer.

Adobe Acrobat Reader (Ver 9)
Run: D:\software\READER9\Setup.exe (or one of the alternative
languages if included). This software allows you to view and print the User Guides in *.PDF
format, provided on this disc. This is free software, subject to terms and
conditions.

Copyright
=========
This CD-ROM contains proprietary information belonging to Ultra Electronics Limited
and may not wholly or partially be copied, stored  in a data retrieval
system, disclosed to third parties or used for any purpose other than that for
which it was supplied, without the express written authority of
Ultra Electronics Limited. All rights are reserved.

Other than the third party software provided, all software provided on this
disc is copyright Ultra Electronics Limited.

HoloKote is a trademark of Ultra Electronics Limited. MAGICARD� is registered as a Trademark with
the United States Patent and Trademark Office, Reg. No. 2,197,276

Microsoft and Windows are trademarks of Microsoft Corporation.

=============================================================
Copyright � 2009 Ultra Electronics Limited, Weymouth, UK



*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************

                    AMENDMENT RECORD for UltraDriver CD

UltraDriver CD Changes

*************************************************************************
    DATE     DCR      VERSION
   10-09-09  30814    V1.4.1.0  Issue 1

1. Upgraded Rio/Tango 2e + L, Avalon, X-Series, Enduro And Pronto driver to V1.4.1.0
2. Magicard Updater Added
3. Dot Net 2.0 Framework Check and update software Added
4. New Installation - Step 3 page added to support the Magicard Updater
5. Added Romanian Language Support

*************************************************************************
    DATE     DCR      VERSION
   10-6-09  30760-2  V1.3.4&5  Issue 2

1. Corrected the Pronto Web link to download the latest Pronto firmware. 
   It previously incorrectly connected the user to the Enduro 
   firmware download page instead.

*************************************************************************
    DATE     DCR      VERSION
   26-05-09  30753     V1.3.4&5  Issue 1

1. Added Magicard Pronto driver V1.3.5.0 and support to the CD.
2. Upgraded Enduro driver to V1.3.5.0.
3. Deleted Alto, Opera & Tempo drivers from CD.
4. Upgraded the Magicard Support Utility to V3.19.
5. Upgraded to the latest User Manuals for the RioTango2e+L, Enduro and Pronto.

*************************************************************************
    DATE     DCR      VERSION
   07-04-09  30695     V1.3.4  Issue 1

1. Added Chinese translations for Autorun.
2. Added Turkish Driver and autorun translations
3. Improved the UltraDriver installation Autoplay guidance.
4. Added Web Links to product registration.
5. Added Web Links to Magicard HoloKote Tool.
6. Drivers Upgraded to V1.3.4.0.
7  Magicard Support Utility Upgraded to V3.18.
8  Upgraded Enduro Quickstart Guide to Issue 5.
9. Modified contact details.
 

*************************************************************************
    DATE     DCR     VERSION
   06-10-08 30464  V1.3.2 Issue 1

1. Updated all drivers to V1.3.2.0.
2. Added colour sharpening to all drivers
3. Fixed Issue with the mis-aligned radio button and text in the security
   dialog box on the card back on double sided printers.
4. Fixed issue with Overcoat holes not working correctly.
5. Fixed issue with batch printing clearing print jobs when an error is 
   introduced over the ethernet on RioTango2e+l printers.
6. Updated Magicard Support Utility to V3.14.


*************************************************************************
    DATE     DCR     VERSION
   17-07-08 30334  V1.3.0 Issue 1

1. Upgraded all drivers to V1.3.0.0.
2. Added Russian and Polish translations

NOTE: If the Printer Driver Language is selected to be other than English
      the version will be displayed as V1.3.0.0 Beta3. Please ignore the 
      Beta3 text as this is a fully Signed Driver release

*************************************************************************
    DATE     DCR     VERSION
   18-04-08 30214  V1.2.0 Issue 2

1. Added Enduro Model.
2. Modified various graphics especially opening language page.
3. Udgraded Magicard Support Utility to enable Enduro compatibility.


                    AMENDMENT RECORD for UltraDriver CD
*************************************************************************
    DATE     DCR     VERSION
   27-03-08  30164  V1.2.0 Issue 1

1. All Printer Drivers are upgraded to V1.2.0.0 which now resolves driver 
   installation issues on Windows Vista. Please refer to individual 
   amendment records below on details of changes for each driver.  
2. Link to website added on completion of driver installation to encourage
   the customer to ensure printer is installed with the latest firmware 
   version for optimum performance.
3. Prevented need to re-enter language selection during Driver installation.
4. Added a 'Support' link onto the front page of each model.
5. Included latest version of the Magicard Support Utility V3.12.
6. Included latest version of the Magicard ID V3.0.4.

*************************************************************************
    DATE     DCR     VERSION
   4-09-07  29822    V1.1.0 Issue 2
   
  1. Corrected the problem whereby the MagApi.DLL file was not being copied
     into the C:/Windows/System32 directory.
  2. Corrected spelling on German Sample cards.
  3. Upgraded Magicard ID to V3.0.2.
  4. Upgraded Magicard Support Utility to V3.11.
  5. Modified English front page to provide a link to an added Support page.
  6. Corrected error when front page title disappeared when cursor moves over it.

  NOTE: The driver remains unchanged at V1.1.0.0

*************************************************************************
    DATE     DCR     VERSION
   31-05-07 29708    V1.1.0 Issue 1
   
  1. Added language selections for the driver. These include French, Spanish,
     German, Portuguese, Italian and Simplified Chinese.
  2. Provided translations for navigating through pages. Note however that 
     Chinese translations have not been made for these pages.
  3. Magicard Contacts have been modified to reflect local contacts.
  4. Changes to the Sample Cards have been made.
  5. Most of the QuickStart Guides have been translated into the relevant
     language.


*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************

                    AMENDMENT RECORD Pronto Driver
				
*************************************************************************
    DATE     DCR     VERSION
   17/07/09 30814   V1.4.1.0 Issue 1	

1. Modification for Splitting Multiple Page files into individual print jobs in Vista.
2. Correction to make bPolling and bPausingDisable registry flags match checkbox settings on the Advanced tab.
3. Use of separate debug flags for Driver LM and SM.
4. Romanian language support added.

*************************************************************************
    DATE     DCR     VERSION
   23-04-09 30695  V1.3.5.0 Issue 1

1. Firmware Version now included in header.
2. Warning to user added if sending a job with 'magnetic encoding only'
   selected and no magnetic data included in the job.
3. Fixed bug which caused '$$33' corrupt error message.
4. Added 'Disable Status Polling' in the 'Advanced' tab to resolve issues
   when the user is implementing the Windows Terminal Services facility.

*************************************************************************
    DATE     DCR     VERSION
   18-03-09 30661  V1.3.4.0 Issue 1

1. Use of environment variable to access 'Program Files' for help file access.
2. Use of separate debug flags for Driver LM and SM.
3. Printer name now available throughout the driver.
4  Slider bugs corrected on 2003 server whereby they were operating back to front.
5. Driver name available throughout driver - required by MagAPI.DLL.
6. MagApi V2.0.2.0 - Buffer sizes changed to 750 for mag encoding.
7. MagAPi V2.0.2.0 - Alpha characters allowed on tracks 2 & 3.
8. Various Pronto Driver improvements
9. Pronto - display of Firmware version is unicode.
10 Pronto - mag encoding - couldn't get COEL in header other than in mag 
   string itself is now resolved.


*************************************************************************
    DATE     DCR     VERSION
   05-12-08  30551  V1.3.3.0 Issue 1
                   

  1. Initial release
*************************************************************************


*************************************************************************
*************************************************************************
*************************************************************************


                    AMENDMENT RECORD Enduro Driver

*************************************************************************
    DATE     DCR     VERSION
   17/07/09 30814   V1.4.1.0 Issue 1	

1. Modification for Splitting Multiple Page files into individual print jobs in Vista.
2. Correction to make bPolling and bPausingDisable registry flags match checkbox settings on the Advanced tab.
3. Use of separate debug flags for Driver LM and SM.
4. Romanian language support added.

*************************************************************************
    DATE     DCR     VERSION
   23-04-09 30695  V1.3.5.0 Issue 1

1. Firmware Version now included in header.
2. Warning to user added if sending a job with 'magnetic encoding only'
   selected and no magnetic data included in the job.
3. Fixed bug which caused '$$33' corrupt error message.
4. Added 'Disable Status Polling' in the 'Advanced' tab to resolve issues
   when the user is implementing the Windows Terminal Services facility.
*************************************************************************

   DATE       DCR         VERSION
   09-03-09  30661        V1.3.4.0 

1. Use of environment variable to access 'Program Files' for help file access.
2. Use of separate debug flags for Driver LM and SM.
3. Printer name now available throughout the driver.
4  Slider bugs corrected on 2003 server whereby they were operating back to front.
5. Driver name available throughout driver - required by MagAPI.DLL.
6. MagApi V2.0.2.0 - Buffer sizes changed to 750 for mag encoding.
7. MagAPi V2.0.2.0 - Alpha characters allowed on tracks 2 & 3.
8. Various Pronto Driver improvements
9. Enduro - display of Firmware version is unicode.
10 Enduro - mag encoding - couldn't get COEL in header other than in mag 
   string itself is now resolved.

*************************************************************************

   DATE     DCR     VERSION
   05-12-08  30551  V1.3.3.0


1.  Sharpening setting now included in Save & Restore
2.  Switchable Debug output messages for all printers.
3.  Firmware version now stored in the registry in Unicode format.
4.  Pausing of the print spooler can now be disabled via a checkbox on the
    Advanced tab
5.  CR79 cards now supported.
6.  Fixed issue with sharpening function causing ghosting on all values
    other than -2.
7.  Fixed problem with routing of jobs to the incorrect printer queue when
    multiple printers are used on one PC on split compound jobs.



*************************************************************************

   DATE      DCR      VERSION
   23-09-08 30443   V1.3.2.0                      

  1. Fixed Issue with the mis-aligned radio button and text in the security
     dialog box on the card back. 
  2. Fixed issue with Overcoat holes not working correctly.
  3. Added Image Sharpening to the driver in advanced tab.

*************************************************************************


    DATE      DCR      VERSION
   17-7-08   30334     V1.3.0.0

  1. Added language selections for the driver. These include Polish and
     Russian.
  2. Added LAN parameter that always reads from the resource file.
  3. Added Vista SP1 fix.
  4. Added more time when spooling a print job to prevent "print job failed"
     timeout error message 
  5. Added MCP200 Printer to Enduro Installation
  6. Added Batch control for Enduro
  7. Serial printer status refresh now working from within application � Printer Status Dialogue. 

NOTE: If the Printer Driver Language is selected to be other than English
      the version will be displayed as V1.3.0.0 Beta3. Please ignore the 
      Beta3 text as this is a fully Signed Driver release

*************************************************************************

     DATE      DCR      VERSION
   18-04-08   30214    V1.2.0.0       

1. Initial introduction into main UltraDriver CD 


*************************************************************************


                    AMENDMENT RECORD Rio2e/Tango2e/Tango+L Driver
*************************************************************************
     DATE     DCR     VERSION
   17/07/09 30814   V1.4.1.0 Issue 1	

1. Modification for Splitting Multiple Page files into individual print jobs in Vista.
2. Correction to make bPolling and bPausingDisable registry flags match checkbox settings on the Advanced tab.
3. Use of separate debug flags for Driver LM and SM.
4. Romanian language support added.
    
*************************************************************************

   DATE     DCR     VERSION
   18-03-09 30661  V1.3.4.0 

1. Use of environment variable to access 'Program Files' for help file access.
2. Use of separate debug flags for Driver LM and SM.
3. Printer name now available throughout the driver.
4  Slider bugs corrected on 2003 server whereby they were operating back to front.
5. Driver name available throughout driver - required by MagAPI.DLL.
6. MagApi V2.0.2.0 - Buffer sizes changed to 750 for mag encoding.
7. MagAPi V2.0.2.0 - Alpha characters allowed on tracks 2 & 3.

*************************************************************************
    DATE     DCR     VERSION
   27-11-08 30537-2  V1.3.3.0

1.  Fixed problem with routing of jobs to the incorrect printer queue when
    multiple printers are used on one PC on split compound jobs.
2.  Sharpening setting now included in Save & Restore
3.  Switchable debug output messages made available.
4.  HoloKote Flex selection provided.
6.  Dye Film Selection button on Advanced tab, and dialog box.
7.  Pausing of the print spooler can now be disabled via a checkbox on the
    Advanced tab
10. Fixed issue with sharpening function causing ghosting on all values
    other than -2.

*************************************************************************

   DATE      DCR      VERSION
   30-09-08 30448   V1.3.2.0                      

  1. Fixed Issue with the mis-aligned radio button and text in the security
     dialog box on the card back. 
  2. Fixed issue with Overcoat holes not working correctly.
  3. Added Image Sharpening to the driver in advanced tab.
  4. Fixed issue with batch printing clearing print jobs when an error is 
     introduced over the ethernet.

*************************************************************************

  DATE     DCR      VERSION
 17-7-08  30334     V1.3.0.0                     

  1. Added language selections for the driver. These include Polish and
     Russian.
  2. Added LAN parameter that always reads from the resource file.
  3. Added Vista SP1 fix.
  4. Added more time when spooling a print job to prevent "print job failed"
     timeout error message 

NOTE: If the Printer Driver Language is selected to be other than English
      the version will be displayed as V1.3.0.0 Beta3. Please ignore the 
      Beta3 text as this is a fully Signed Driver release

*************************************************************************
   
     DATE      DCR      VERSION
   12-2-08   30114     V1.2.0.0                        

  1.  Vista support
  2.  Numbers included in Arabic text are printed the correct way, not
      reversed (i.e. right to left)
  3.  Able to create PRNs in Vista, and in XP without printer connected
  4.  Fix to ensure temporary buffers are cleared between tracks when handling
      journalled mag encoding data.
      (introduced in beta V1.1.0.8).
  5.  Correction to laminator - patch and overlay switched around. 
      (introduced in beta V1.1.0.6).
  6.  Introduced context sensitive help.
  7.  Addition of new laminator film type: Registered Overlay.
      (introduced in beta V1.1.0.4).
  8.  Addition of Holokote Map function. 
      (introduced in beta V1.1.0.2).
  9.  Fix to laminator to make LAM9 same as LAM5.
      (introduced in beta V1.1.0.2).
  10. Modified to deal with problem with errors while batch printing causing
      the spooler to empty the queue.
     
      (introduced in beta V1.1.0.1).


*************************************************************************
    DATE     DCR     VERSION
   31-05-07 29708    V1.1.0.0
   
  1. Added language selections for the driver. These include French, Spanish,
     German, Portuguese, Italian and Simplified Chinese.
     NOTE1: The Rio and Tango 2e firmware V3.10 or later includes translations
            for the Status Monitor error messages and the LCD messages.
     NOTE:2 The Tango +L firmware V2.10 or later includes translations
            for the Status Monitor error messages. The LCD messages are not 
            translated.

*************************************************************************
    DATE     DCR      VERSION
   27-4-07  29650    V1.0.7.0
   
  1. Modified RioTango Driver due to changes in the MAGAPI.DLL including 
     additional commands, MSV and MSR, which provide the 'Magnetic Read Data 
     Only' functionality. 
  2. MAGAPI.DLL updated to v2.0.1.0

*************************************************************************
   DATE     DCR      VERSION
   1-2-07   29419    V1.0.6.0
   
   
  1. Modification to prevent lockup on ethernet batch printing
  2. Preserve mag encoding data received in the front header for writing
     when printing dual-sided card.
  
*************************************************************************
    DATE     DCR      VERSION
   05-10-06  29303    V1.0.3.0

  1. Modified to support Tango+L

*************************************************************************
    DATE     DCR      VERSION
   15-8-06  29198    V1.0.2.0

  1. Modification to mag encoding problem - if glyphs are presented, they
     will be converted - the font used for the text MUST be Arial.  This
     is a rough fix to overcome problems with .Net, pending a full
     implementation of code to handle glyphs in the proper manner.
  2. Correction to .Net printing problem where it wouldn't print image.
  3. Minor correction to text string in the setup files
  4. Now reports landscape/portrait orientation correctly
  5. Modification so that mag encoding strings can be accepted even if 
     they do not have a comma after the track number.
  6. Drivers can now be updated when an installation takes place of a
     signed driver over an earlier signed driver.
  7. Modified to correct reported problem where a bordered filled square
     was not printed correctly - printed without border, or boder only
     printed.
  8. Modified to correct duplex problems and extra blank cards being
     printed when switching between Rio and Tango, particularly when used
     with CorelDraw.

*************************************************************************
    DATE     DCR      VERSION
   6-4-06   29055     V1.0.1 

  1. Changed Setup installation wording for plug and play installation.
  2. Modified Image Position adjustments page.

*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************


           AMENDMENT RECORD Avalon Driver 

*************************************************************************
    DATE     DCR     VERSION
   17/07/09 30814   V1.4.1.0 Issue 1	

1. Modification for Splitting Multiple Page files into individual print jobs in Vista.
2. Correction to make bPolling and bPausingDisable registry flags match checkbox settings on the Advanced tab.
3. Use of separate debug flags for Driver LM and SM.
4. Romanian language support added.

*************************************************************************
    DATE     DCR     VERSION
   18-03-09 30661  V1.3.4.0 Issue 1

1. Use of environment variable to access 'Program Files' for help file access.
2. Use of separate debug flags for Driver LM and SM.
3. Printer name now available throughout the driver.
4  Slider bugs corrected on 2003 server whereby they were operating back to front.
5. Driver name available throughout driver - required by MagAPI.DLL.
6. MagApi V2.0.2.0 - Buffer sizes changed to 750 for mag encoding.
7. MagAPi V2.0.2.0 - Alpha characters allowed on tracks 2 & 3.


*************************************************************************

     DATE     DCR     VERSION
   05-12-08  30551  V1.3.3.0

1.  Fixed problem with routing of jobs to the incorrect printer queue when
    multiple printers are used on one PC on split compound jobs.
2.  Sharpening setting now included in Save & Restore
3.  Switchable debug output messages made available.
4.  Dye Film Selection button on Advanced tab, and dialog box.
5.  Pausing of the print spooler can now be disabled via a checkbox on the
    Advanced tab
6. Fixed issue with sharpening function causing ghosting on all values
    other than -2.
7. Modified file access routines to use Session Variables.   


*************************************************************************

   DATE      DCR      VERSION
   30-09-08 30448   V1.3.2.0                      

  1. Fixed Issue with the mis-aligned radio button and text in the security
     dialog box on the card back. 
  2. Fixed issue with Overcoat holes not working correctly.
  3. Added Image Sharpening to the driver in advanced tab.

*************************************************************************

  DATE     DCR      VERSION
 17-7-08  30334     V1.3.0.0                     

  1. Added language selections for the driver. These include Polish and
     Russian.
  2. Added LAN parameter that always reads from the resource file.
  3. Added Vista SP1 fix.
  4. Added more time when spooling a print job to prevent "print job failed"
     timeout error message 

NOTE: If the Printer Driver Language is selected to be other than English
      the version will be displayed as V1.3.0.0 Beta3. Please ignore the 
      Beta3 text as this is a fully Signed Driver release

*************************************************************************
    DATE      DCR      VERSION
   12-2-08   30114     V1.2.0.0                        

  1.  Vista support
  2.  Numbers included in Arabic text are printed the correct way, not
      reversed (i.e. right to left)
  3.  Able to create PRNs in Vista, and in XP without printer connected
  4.  Fix to ensure temporary buffers are cleared between tracks when handling
      journalled mag encoding data.
      (introduced in beta V1.1.0.8).
  5.  Introduced context sensitive help.
  6.  Modified to deal with problem with errors while batch printing causing
      the spooler to empty the queue.
     
      (introduced in beta V1.1.0.1).

*************************************************************************
    DATE     DCR     VERSION
   31-05-07 29708    V1.1.0.0
   
  1. Added language selections for the driver. These include French, Spanish,
     German, Portuguese, Italian and Simplified Chinese.
     
     NOTE1: The Alto Opera Tempo firmware V4.10 or later includes translations
            for the Status Monitor error messages. 

*************************************************************************
    DATE     DCR      VERSION
   27-4-07   29650    V1.0.7.0
  
  1. Added 'Overcoat Options' to back side of Avalon Duo Driver.
 
*************************************************************************
   DATE     DCR      VERSION
   1-2-07   29419    V1.0.6.0
   
   1. Change to setup.exe on model selection.

 *************************************************************************
    DATE     DCR      VERSION
   05-10-06  29303    V1.0.3.0

  1. Modified to support Avalon Duo


*************************************************************************
    DATE     DCR      VERSION
   15-8-06  29198    V1.0.2.0

  1. Modification to mag encoding problem - if glyphs are presented, they
     will be converted - the font used for the text MUST be Arial.  This
     is a rough fix to overcome problems with .Net, pending a full
     implementation of code to handle glyphs in the proper manner.
  2. Correction to .Net printing problem where it wouldn't print image.
  3. Minor correction to text string in the setup files
  4. Now reports landscape/portrait orientation correctly
  5. Modification so that mag encoding strings can be accepted even if 
     they do not have a comma after the track number.
  6. Drivers can now be updated when an installation takes place of a
     signed driver over an earlier signed driver.
  7. Modified to correct reported problem where a bordered filled square
     was not printed correctly - printed without border, or boder only
     printed.
  8. Modified to correct duplex problems and extra blank cards being
     printed when switching between Rio and Tango, particularly when used
     with CorelDraw.

*************************************************************************
    DATE     DCR      VERSION
   6-4-06   29055     V1.0.1 

  1. Changed Setup installation wording for plug and play installation.
  2. Modified Image Position adjustments page.

*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************


           AMENDMENT RECORD X-Series Driver 

*************************************************************************
   DATE     DCR      VERSION
   10-09-09  30814    V1.4.1.0  Issue 1

1. Inital Release

*************************************************************************



*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************

                    AMENDMENT RECORD Magicard Support Utility

*****************************************************************************************************************	
     DATE         DCR        VERSION
    04-06-09     30814        V3.20

1. Enable USB enumeration to recognise IDMAKER
2. Enable all Enduro Family Printer models to be recognised individually


*****************************************************************************************************************	
     DATE         DCR        VERSION
    21-05-09     30752        V3.19

1. Fix a bug with the display of model type
2. Included compatability with Enduro Special.

*****************************************************************************************************************	

     DATE         DCR        VERSION
    03-04-09     30691        V3.18

1. Belt and Braces for the msv data buffers to ensure that there are no buffer overflows.

*****************************************************************************************************************	
 
     DATE         DCR        VERSION
    27-01-09     30602        V3.17

1.	New and Enhanced USB Printer Selection Interface

*****************************************************************************************************************	
     DATE         DCR        VERSION
    10-01-09     30602        V3.16

1. The Enduro/Pronto firmware requires a different protocol string for magnetic encoding.

*****************************************************************************************************************	
     DATE         DCR        VERSION
    04-12-08     30537        V3.15

1. Include PRONTO Compatability.
2. Fix the Enduro/Pronto VSR and MSV interface.

*************************************************************************
     DATE         DCR        VERSION
    27-08-08     30374        V3.14

1. Use MIP command when SAVE button is pressed on Printer Identity tab
   when connected to an Enduro printer
2. Send a RST after the DEF command when default button used, change focus
   back to Comms tab, disconnecting printer.

*************************************************************************

     DATE         DCR        VERSION
    17-03-08     30179        V3.13

1. Include Enduro Compatability

***********************************************************************	


     DATE       DCR       VERSION
    05-12-07   30060       V3.12

1. Identify connection type within Query reports	
2. Implement new Sensor Query feature

*************************************************************************

     DATE        DCR        VERSION
    30-8-07     29822        V3.11

1. Mag Read function included for all models.	
2. Added ability to upgrade Tango+L laminator firmware

*************************************************************************

     DATE        DCR        VERSION
    24-11-06    29382        V3.07

1. Addition of "Mag Read" page.
2. General GUI tidy-up to accomodate 2 rows of tabs (was previously 1).

*************************************************************************

     DATE        DCR        VERSION
    29-8-06     29261        V3.05

1. Added display if PID in Printer ID page.

*************************************************************************

     DATE        DCR        VERSION
    15-8-06     29198        V3.04

1. Major redesign of layout, with addition of "Statistics and Commands"
   and "Magnetic Encoding" pages.

*************************************************************************

     DATE        DCR        VERSION
    28-6-06     29144        V3.03

1. Adding adjustment of the printhead position

*************************************************************************

     DATE        DCR        VERSION
    31-3-06     29055        V3.02

1. Correct inclusion of the INPOUT32.DLL in the Installshield build

*************************************************************************

     DATE        DCR        VERSION
    28-3-06     29021        V3.01

1. Correction of bug whereby some PCs crashed when PRN file is downloaded.
   Packet sized reduced to 10000 byte chunks, not default 64k bytes.

*************************************************************************

     DATE        DCR        VERSION
    24-3-06      Not         V3.00
               Released

1. Addition of Ethernet Tools.
2. Correction to the installation build to include INPOUT32.DLL

*************************************************************************

     DATE        DCR        VERSION
   10-01-06     28957        V2.01

1. The presence of other devices on the USB bus is handled on XP but not
   on Windows 98.  Here the printer must still be the only item connected
   to the USB bus

*************************************************************************

     DATE        DCR        VERSION
   12-09-05     28806        V2.00

1. Added Ethernet comms
2. Corrected situation where Printer must be the ONLY thing on the bus
   when using USB.  Now, there must be only one Magicard printer on the
   bus, but the presence of other devices is handled.

*************************************************************************

     DATE        DCR        VERSION
   14-2-05      28497        V1.03

1. Modification of the QPR facility to operate with Rio 2 & Tango 2
2. Connection now possible using USB or LPT.

*************************************************************************

     DATE        DCR        VERSION
   12-10-04     28281        V1.02

1. Provision of the Query Printer (QPR) facility.

*************************************************************************

    DATE       DCR        VERSION
   1-6-04     28134        V1.01

1. Renamed from 'Alto Opera Support Tool' to 'Magicard Support Utility'
2. Amendment to Installshield GUID to prevent conflict with other tools
3. Change Installshield scripts to alter mode of uninstallation i.e. by
   selection from the Add/Remove Programs applet of Control Panel, rather
   than by a second running of the installation .exe file.
3. After performing a firmware upgrade, Utility closes down.
4. Provision for displaying 20 'special' models, and Avalon.

*************************************************************************

    DATE       DCR        VERSION
   3-2-04                  V1.00

1. Initial Release



*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************
*************************************************************************

            MAGICARD ID BADGING SOFTWARE
           =============================

                  HISTORY OF CHANGES
                  ==================


_______________________________________________________________


Version 3.0.5 - 9 February 2009 - DCR 30586
---------------------------------------------
1. Correction to allocation of the text strings for German for the Database Import Wizard.
2. Addition of function to allow a user to specify a range of cards to print ('From xxx to yyy') rather than having to 
   select each one individually.
  
____________________________________________________

Version 3.0.4 - 22 January 2008 - DCR 30060
---------------------------------------------
1. The Magnetic Encoding was modified so that it is to always be proceeeded by a change to Black Trie Type Ariel.
   This was needed to cater for changes in the Magicard Printer Driver.

_____________________________________________________

Version 3.0.3 - 13 November 2007 - DCR 29967
---------------------------------------------
1. Problem resolved whereby when a custom layout is created, static text fields are not saved when the project is saved or backed up.
2. Resolved issue where the software doesn't record the last directory used for importing the backgrounds. 
_____________________________________________________

Version 3.0.2 - 29 August 2007 - DCR 29822
---------------------------------------------
1. Added German Language Support
2. Improved Vista Compatibility
3. New Help File and manual.
4. Minor Interface Bug Fixes

_______________________________________________________________

Version 3.0.1 - May 2007
---------------------------------------------
1. Correction to duplex operation

_______________________________________________________________

Version 3.0.0 - February 2006
---------------------------------------------
1. Duplex operation
_______________________________________________________________

Version 2.3.0 - December 2005
---------------------------------------------
major functional improvements in response to customer demand, including the following:- 
1. Added the ability to import, crop and resize images.
2. Added support for mulitple images on a single card.
3. Logos and static images can now be placed on custom designed cards.
_______________________________________________________________

Version 2.1.0 - July 2004
---------------------------------------------
major functional improvements in response to customer demand, as follows:- 
1. Added the ability to Batch print cards. The use can now capture photographs and ID card data,
    and then select a batch of cards from the database to be printed, as one job.
2. Added the ability to Import and Export the data, card design and images as a Project.
   This enables users to work with several card designs and databases within one installation of
   Magicard ID.
3. Added the ability to provide multiple language versions of Magicard ID.
    This is achieved by providing 'Language Packs' which can be downloaded from the website.
    NOTE: If your required language is not yet available, we can supply an Excel file to enable 
               you to provide us with your own translations. 
_______________________________________________________________

Version 2.0.2 - October 2003 - initial public release 
--------------------------------------------------------------------------------