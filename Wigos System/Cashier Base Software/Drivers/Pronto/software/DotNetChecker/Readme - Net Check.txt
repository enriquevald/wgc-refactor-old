.NET version checker

Description:
a small program that checks for the most recent installed version of .NET on a computer.
the program uses the registry to find the .NET install path, the program uses this directory
to iterate through the installation folders to determine what the most recent installed version is
the program exports the result to a text file (c:\MS.NET VERSION.txt as default)

Args:
[Quiet mode]

-Q or -q to turn on
NULL or 0 to turn off

quiet mode, when enabled, will quit the program the instant its finished
quiet mode, when disabled, will leave the console open until the user manually quits the app

[Output path]

example: c:\documents\version\

Output path specifies the folder where the output file will be saved.
Note: folder paths with spaces will need to be abbreviated using shorthand file names
../program files/.. would be ../progra~1/.. in shorthand

[Output file]

example: myVersion.txt

Output file specifies the fine name and extension of the file that is produced.
Note: file names with spaces should be encapsulated in quotes such as "My Output.txt"
