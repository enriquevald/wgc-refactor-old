KUBE200 driver package for Custom Engineering Spa Kube 200 dpi printer:

Windows 2000.
Windows XP.
Windows 2K3.

Installation procedure:

Serial, parallel and network interface
For (W2K, XP, W2K3) you must execute the standard printer installation procedure.

USB interface
Follow the automatic wizard procedure.

This driver support printer:
"Custom KUBE 80mm (200dpi)"

The "Custom KUBE 80mm (200dpi)" prints on a paper width size of 80mm but the printable
region width is 76mm.

This printer supports some page format:

"KUBE Roll 80mm"
"KUBE Roll Short 80mm"
"KUBE 80x80mm"
"KUBE 80x120mm"
"KUBE 80x160mm"
"KUBE 80x200mm"
"KUBE 80x240mm"
"KUBE 80x280mm"
"KUBE 80x400mm"

If the system driver properties section shows these format pages and other not here
specified, you must select only the above mentioned pages format.

Note about BlackMark Feature:
 we suggest to use "KUBE Roll" page format when You are using Notch Alignmemnt.

