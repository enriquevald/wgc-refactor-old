KUBE200 driver package for Custom Engineering Spa Kube 200 dpi printer:

Windows 2000.
Windows XP.
Windows 2K3.

Installation procedure:

Serial, parallel and network interface
For (W2K, XP, W2K3) you must execute the standard printer installation procedure.

USB interface
Follow the automatic wizard procedure.

This driver support printer:
"Custom KUBE 82.5mm (200dpi)"

The "Custom KUBE 82.5mm (200dpi)" prints on a paper width size of 82.5mm but the printable
region width is 80mm.

This printer supports some page format:

"KUBE Roll 82.5mm"
"KUBE Roll Short 82.5mm"
"KUBE 82.5x82.5mm"
"KUBE 82.5x125mm"
"KUBE 82.5x165mm"
"KUBE 82.5x205mm"
"KUBE 82.5x245mm"
"KUBE 82.5x285mm"
"KUBE 82.5x405mm"

If the system driver properties section shows these format pages and other not here
specified, you must select only the above mentioned pages format.

Note about BlackMark Feature:
 we suggest to use "KUBE Roll" page format when You are using Notch Alignmemnt.

