Leggete con attenzione i seguenti termini e le seguenti condizioni. Caricando ed utilizzando dei files Custom 
Engineering accettate di essere vincolati a tali termini e condizioni. Qualora non accettiate tali termini e
condizioni, non caricate nessuno dei suddetti files. Questa Licenza vi concede il diritto non esclusivo di 
utilizzare il Software Custom Engineering e la relativa documentazione ("Software") ai seguenti termini e condizioni. 

1. Licenza. Potete : (a) utilizzare e (b) copiare il Software in forma leggibile da una macchina solo a scopo di 
back-up o uso in una singola postazione di lavoro, a patto che venga riprodotto l'avviso di copyright e le diciture
relative al marchio Custom Engineering. 

2. Restrizioni. E' vietato distribuire copie di questo Software. Il Software contiene dei segreti industriali e 
allo scopo di proteggerli � fatto divieto di decompilare, disfare, smontare o ridurre in altro modo il Software
a una forma percepibile dall'uomo. E' fatto divieto di modificare, adattare, tradurre, prestare, noleggiare, 
affittare, rivendere a scopo di lucro, distribuire o creare dei derivati basati sul software o su parte di esso. 

3. Propriet�. Il Software � un segreto industriale di propriet� di Custom Engineering. Custom Engineering conserva
il titolo, la propriet� e i diritti di propriet� intellettuale nei confronti del Software e di tutte le copie 
successive indipendentemente dalla forma o dal mezzo. Il Software � protetto dalle leggi di copyright dei trattati
internazionali sul copyright. Questa Licenza non costituisce una vendita del Software. 

4. Rescissione. Questa Licenza rimane in vigore fino alla sua rescissione. Questa Licenza cesser� automaticamente 
senza preavviso qualora non vengano rispettate queste disposizioni. Alla cessazione della Licenza tutte le copie
del Software, comprese quelle parziali, dovranno venire distrutte. 

5. Rinuncia della Garanzia. Accettate tutti i rischi che possano sorgere dal caricamento del software, compresi, 
ma non limitati a, eventuali errori di trasmissione o corruzione dei dati o di software esistenti. Custom 
Engineering non concede alcuna garanzia, esplicita o implicita, e in particolare respinge qualsiasi garanzia 
di non infrazione di diritti di terzi, garanzie di commerciabilit� o idoneit� a particolari scopi. 

6. Limitazione della Responsabilit�. In nessuna circostanza e per nessun contratto, illecito, teoria legale o
altro, la Custom Engineering o i suoi fornitori o rivenditori saranno responsabili nei vostri confronti o nei 
confronti di altri per danni indiretti, speciali, incidentali o consequenziali di qualsiasi natura, compresi,
ma senza limitarsi ad essi, danni per perdite di profitti, dati, clintela, interruzione del lavoro, guasti o
cattivo funzionamento dei computer o qualsiasi altra perdita o danno commerciale neppure nel caso in cui la 
Custom Engineering sia stata avvisata della possiblit� di tali danni, inoltre la Custom Engineering ed i suoi 
collaboratori non possono essere ritenuti responsabili per eventuali carenze, errori od omissioni nel materiale 
software impiegato e distribuito. 

1. Esportazione. Caricando il Software, riconoscete che questo limita l'esportazione e la riesportazione del 
Software stesso. Inoltre, vi impegnate a non esportare o riesportare il Software o i mezzi in qualsiasi forma 
senza l'opportuna autorizzazione della Custom Engineering. 

2. Generalit�. Accettate che questo costituisca l'intero accordo in merito a questa Licenza. Per apportare 
modifiche a questa Licenza, � richiesto un documento redatto per iscritto da entrambe le parti. Vi assumerete 
la completa responsabilit� di un uso legale e responsabile di questo Software. Qualora alcuni provvedimenti 
di questa Licenza siano dichiarati inapplicabili in qualsiasi giurisdizione, tali provvedimenti verranno
considerati inscindibili da questa Licenza e non influiranno sulle parti restanti della stessa. Tutti i 
diritti riguardanti il Software non specificatamente accordati in questa Licenza sono riservati alla Custom 
Engineering. 

3. Si suggerisce di eseguire una copia di sicurezza dei dati importanti nel proprio sistema e della sua 
configurazione prima di usare questi programmi ed installarne i contenuti, � comunque sconsigliabile eseguire 
i programmi presentati in un computer connesso ad una rete. 

FORO COMPETENTE 
La competenza giudiziale a decidere ogni eventuale controversia spetter� esclusivamente all'Autorit� 
Giudiziaria di Parma.
