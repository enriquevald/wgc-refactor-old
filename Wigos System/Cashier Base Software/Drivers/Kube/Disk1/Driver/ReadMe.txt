============================================================================
Driver Installation Wizard

Copyright(C) 2007 Custom Engineering
All Rights Reserved
============================================================================

Please see the license agreement included in the license dialog for limitations on the duplication and distribution of this software and other important information.

============================================================================
System Requirements
============================================================================
 Operating System:    Windows� 2000 Professional
                      Windows� 2000 Server
                      Windows� XP Home Edition
                      Windows� XP Professional Edition

                      TCP/IP protocol must be installed on the machine (for the Ethernet Port)

 Minimum Hardware:    
    CPU:              Pentium III PC or higher
    RAM:              128MB

 Disk Space:          12MB of space for the installed software

============================================================================
POS/Retail & Gaming Printers
============================================================================

============================================================================
Drivers Package Feature 1.01
============================================================================

	Drivers Package:
	|
	|
	|
	+--- "Custom KUBE 80mm (200dpi)" Printer
	|	|
	|	|
	|	+--- STANDARD driver (Package Rel. 3.04)
	|		|
	|		+--- GPD    	Rel. 1.28
	|	
	|
	+--- "Custom KUBE 82.5mm (200dpi)" Printer
		|
		|
		+--- STANDARD driver (Package Rel. 3.04)
			|
			+--- GPD    	Rel. 1.28
				

============================================================================
Technical Support
============================================================================

More information and updates can be found on the following websites:

http://www.custom.it
http://www.custom.biz
