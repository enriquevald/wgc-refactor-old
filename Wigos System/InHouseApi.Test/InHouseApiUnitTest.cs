﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InHouseApiUnitTest.cs
// 
//   DESCRIPTION: InHouseApi Unit Test class
// 
//        AUTHOR: Oscar Mas
// 
// CREATION DATE: 18-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-OCT-2017 OMC    First version.
//------------------------------------------------------------------------------

using System;
using InHouseApi.Business;
//using Newtonsoft.Json;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography.X509Certificates;

namespace InHouseApi.Test
{
  [TestClass]
  public class InHouseApiUnitTest
  {

    #region " Variables "
      
      private BusinessLogic m_business_logic = new BusinessLogic() { IP = "127.0.0.1", MethodVersion = "v1" };

    #endregion // Variables

    #region " Test Methods "

      [TestMethod]
    public void GetEventsTest()
    {
      InHouseApi.Classes.Response.InHouseEventResponse _response = new InHouseApi.Classes.Response.InHouseEventResponse();
      System.Net.HttpStatusCode _status = System.Net.HttpStatusCode.BadRequest;
      Boolean _method_result;
      try
      {
        _method_result = m_business_logic.GetEvents(1, out _response, out _status);
      }
      catch (Exception)
      {
        _method_result = false;
      }
      Assert.AreEqual(true, _method_result, string.Format("Method '{0}()' has been succesfully executed!", "GetEvents"));
    } // GetEventsTest

    [TestMethod]
    public void GetDemographicDataTest()
    {
      InHouseApi.Classes.Response.InHouseCustomerResponse _response = new InHouseApi.Classes.Response.InHouseCustomerResponse();
      System.Net.HttpStatusCode _status = System.Net.HttpStatusCode.BadRequest;
      Boolean _method_result;
      try
      {
        _method_result = m_business_logic.GetDemographicData(306, 1, out _response, out _status);
      }
      catch (Exception)
      {
        _method_result = false;
      }      
      Assert.AreEqual(true, _method_result, string.Format("Method '{0}()' has been succesfully executed!", "GetDemographicData"));
    } // GetDemographicDataTest

    [TestMethod]
    public void GetCustomerPictureTest()
    {
      InHouseApi.Classes.Response.InHouseCustomerPictureResponse _response = new InHouseApi.Classes.Response.InHouseCustomerPictureResponse();
      System.Net.HttpStatusCode _status = System.Net.HttpStatusCode.BadRequest;
      Boolean _method_result;
      try
      {
        _method_result = m_business_logic.GetCustomerPicture(707, 1, out _response, ref _status);
      }
      catch (Exception)
      {
        _method_result = false;
      }
      Assert.AreEqual(true, _method_result, string.Format("Method '{0}()' has been succesfully executed!", "GetCustomerPicture"));
    } // GetCustomerPictureTest
    
    #endregion // Test Methods

  } // InHouseApiUnitTest
} // InHouseApi.Test
