﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle ("WSI.GDS_Service")]
[assembly: AssemblyDescription ("")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("WIN SYSTEMS INTERNATIONAL, LTD.")]
[assembly: AssemblyProduct ("WSI.GDS_Service")]
[assembly: AssemblyCopyright("Copyright © WIN SYSTEMS INTERNATIONAL, LTD. 2007")]
[assembly: AssemblyTrademark ("")]
[assembly: AssemblyCulture ("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible (false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid ("e4a3caea-ddca-4d75-95cc-0be4dabd8a53")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("100.0.0.0")]
[assembly: AssemblyFileVersion("100.0.0.0")]
