using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using WSI.Common;
using WSI.GDS;

using WSI.GDS_Service.Properties;
using System.Messaging;
using System.Security.Cryptography;
using System.Xml;
using System.IO;
using System.Data.SqlClient;

namespace WSI.GDS_Service
{

  class Program
  {
    public const string GDS_SERVICE_VERSION = "18.001";

    ///// <summary>
    ///// Tests
    ///// </summary>
    //static void XXXX ()
    //{
    //  MessageQueue rcv_queue;
    //  GDS_Message GDS_request;
    //  GDS_Message GDS_response;
    //  Message m;
    //  UInt32 sequence;

    //  //GetInfo info;

    //  //info = new GetInfo ();
    //  //String x;
    //  //x = info.GetCPUId ();

       
    //  //x = info.GetMACAddress ();
    //  //x = info.GetVolumeSerial ("C");
    //  //x = info.GetHardDriveSerial ();

    //  string queuepath = "";

    //  if ( !MessageQueue.Exists (@".\Private$\WIN50ReplyQ") )
    //  {
    //    rcv_queue = MessageQueue.Create (@".\Private$\WIN50ReplyQ");
    //  }
    //  else
    //  {
    //    rcv_queue = new MessageQueue (@".\Private$\WIN50ReplyQ");
    //  }
    //  rcv_queue.Formatter = new CJMessageFormatter ();
    //  rcv_queue.Purge ();

    //  queuepath = @"FormatName:DIRECT=HTTP://172.17.4.99/msmq/Private$/WIN50RecQ";

    //  //queuepath = @".\Private$\WIN12RecQ";
      
    //  MessageQueue tmpqueue = new MessageQueue (queuepath);
    //  tmpqueue.Formatter = new CJMessageFormatter ();

    //  //while ( true )
    //  //{
    //  //  String xx;
    //  //  String str_xml;

    //  //  m = rcv_queue.Receive (new TimeSpan (0, 0, 10));

    //  //  str_xml = (String) m.Body;

    //  //  cj_response = CJ_Message.CreateMessage (str_xml);

    //  //  switch ( cj_response.MsgHeader.MsgType )
    //  //  {
    //  //    case CJ_MsgTypes.ErrorMessage:
    //  //    break;
    //  //    case CJ_MsgTypes.PlayerCardInReply:
    //  //    break;
    //  //    case CJ_MsgTypes.PlayerCardOutReply:
    //  //    break;
    //  //    case CJ_MsgTypes.PlayACKMsg:
    //  //    break;
    //  //  }
    //  //}

    //  sequence = 1;
    //  while ( true )
    //  {
    //    String xx;
    //    String str_xml;
    //    Decimal balance;

    //    while ( true )
    //    {
    //      // Card In
    //      cj_request = CJ_Message.CreateMessage (CJ_MsgTypes.PlayerCardIn);
    //      cj_request.MsgHeader.EPSName = "12345";
    //      cj_request.MsgHeader.SequenceNumber = sequence++;
    //      cj_request.MsgHeader.TrackingNumber1 = "156429151156318135";
    //      //cj_request.MsgHeader.TrackingNumber1 = "100000000000000000";
    //      cj_request.MsgHeader.TrackingNumber2 = "";
    //      cj_request.MsgHeader.TrackingNumber3 = "";
    //      xx = cj_request.ToXml ();
    //      tmpqueue.Send (xx);


    //      try
    //      {
    //        m = rcv_queue.Receive (new TimeSpan (0, 0, 10));

    //        str_xml = (String) m.Body;

    //        //////m = new Message ();
    //        ////while ( true )
    //        ////{
    //        ////  tmpqueue.Send (xx);
    //        ////  //m = rcv_queue.Peek ();
    //        ////  m = rcv_queue.Receive (new TimeSpan (0, 0, 10));

    //        ////  str_xml = (String) m.Body;

    //        ////  if ( xx == str_xml )
    //        ////  {
    //        ////    ;
    //        ////  }
    //        ////  xx = str_xml;
    //        ////}

    //        ////////{
    //        ////////  Byte[] buffer;

    //        ////////  String aux1;
    //        ////////  String aux2;
    //        ////////  int idx;

    //        ////////  //str_xml = xx;

    //        ////////  idx = str_xml.IndexOf ("<Checksum>", 0);
    //        ////////  aux1 = str_xml.Substring (0, idx + 10);

    //        ////////  idx = str_xml.IndexOf ("</Checksum>", 0);
    //        ////////  aux2 = str_xml.Substring (idx);

    //        ////////  aux1 = aux1 + aux2;

    //        ////////  buffer = Encoding.Unicode.GetBytes (aux1);

    //        ////////  idx = (int) Misc.Checksum (buffer);

    //        ////////}

    //        cj_response = CJ_Message.CreateMessage (str_xml);
    //        String aux = cj_response.ToXml ();

    //        if ( cj_response.MsgHeader.SequenceNumber == cj_request.MsgHeader.SequenceNumber )
    //        {
    //          if (   cj_response.MsgHeader.MsgType == CJ_MsgTypes.PlayerCardInReply )
    //          {
    //            CJ_MsgPlayerCardInReply reply;

    //            reply = (CJ_MsgPlayerCardInReply) cj_response.MsgContent;

    //            if ( reply.ErrorCode == CJ_ErrCode.NoError )
    //            {
    //              balance = ( (CJ_MsgPlayerCardInReply) cj_response.MsgContent ).CashBalance;

    //              break;
    //            }
    //          }
    //        }
    //      }
    //      catch
    //      { 
    //      }
    //    }

    //    System.Threading.Thread.Sleep (5000);

    //    DateTime t0;
    //    DateTime t1;

    //    t0 = DateTime.Now;

    //    // Play 
    //    for (int i=0; i < 100; i++)
    //    {
    //      CJ_MsgPlayMsg request;

    //      cj_request = CJ_Message.CreateMessage (CJ_MsgTypes.PlayMsg);
    //      request = (CJ_MsgPlayMsg) cj_request.MsgContent;

    //      cj_request.MsgHeader.EPSName = "12345";
    //      cj_request.MsgHeader.SequenceNumber = sequence++; ;
    //      cj_request.MsgHeader.TrackingNumber1 = "156429151156318135";
    //      cj_request.MsgHeader.TrackingNumber2 = "";
    //      cj_request.MsgHeader.TrackingNumber3 = "";

    //      request.Denomination    = 25;
    //      request.TotalBetCents = 100;
    //      request.TotalWinCents = 0;
    //      request.StartingBalance = (uint) balance;
    //      request.EndingBalance = (uint) (balance - request.TotalBetCents + request.TotalWinCents);
    //      request.GameDescription = "GAME0";

    //      balance = request.EndingBalance;

    //      String prev_play;
          
    //      prev_play = xx;
    //      xx = prev_play;


    //      xx = cj_request.ToXml ();
    //      tmpqueue.Send (xx);
    //      try
    //      {
    //        m = rcv_queue.Receive (new TimeSpan (0, 0, 10));

    //        str_xml = (String) m.Body;

    //        cj_response = CJ_Message.CreateMessage (str_xml);


    //      }
    //      catch
    //      {
    //        ;
    //      }

    //      // System.Threading.Thread.Sleep (1000);
    //    }

    //    t1 = DateTime.Now;

    //    TimeSpan ts;
    //    ts = t1.Subtract (t0);


    //    System.Threading.Thread.Sleep (5000);

    //    while ( true )
    //    {
    //      CJ_MsgPlayerCardOut request;
    //      // Card In
    //      cj_request = CJ_Message.CreateMessage (CJ_MsgTypes.PlayerCardOut);
    //      cj_request.MsgHeader.EPSName = "12345";
    //      cj_request.MsgHeader.SequenceNumber = sequence++;
    //      cj_request.MsgHeader.TrackingNumber1 = "156429151156318135";
    //      cj_request.MsgHeader.TrackingNumber2 = "";
    //      cj_request.MsgHeader.TrackingNumber3 = "";

    //      request = (CJ_MsgPlayerCardOut) cj_request.MsgContent;
    //      request.CashBalance = (uint) balance;

    //      xx = cj_request.ToXml ();
    //      tmpqueue.Send (xx);

    //      try
    //      {
    //        m = rcv_queue.Receive (new TimeSpan (0, 0, 30));

    //        str_xml = (String) m.Body;

    //        cj_response = CJ_Message.CreateMessage (str_xml);

    //        if ( cj_response.MsgHeader.MsgType == CJ_MsgTypes.PlayerCardOutReply )
    //        {
    //          break;
    //        }
    //      }
    //      catch
    //      {
    //        ;
    //      }
    //    }

    //    System.Threading.Thread.Sleep (10000);
    //  }
    //}


    /// <summary>
    /// Starts the service
    /// </summary>
    /// <param name="ipe"></param>
    static public void StartService(IPEndPoint Ipe)
    {
      GDS_DirectoryService ds;
      IXmlSink executor;
      GDS_Server server;
      String service_ip_address;

      // Get Local IpAddress
      service_ip_address = ConfigurationFile.GetSetting("LocalIP");
      if (service_ip_address == "")
      {
        Log.Error(" No Discovery Service IP address is available. Application stopped");

        return;
      }        

      // Start Play Statistics
      Statistics.Init();
      Statistics.Start();

      // Start CJ messages processing
      GDS_Client.Init();
//        ConfigurationFile.GetSetting("VendorID"), ConfigurationFile.GetSetting("VendorIP"), ConfigurationFile.GetSetting("RemoteIP"));
      GDS_Client.Start();

      // Start GDS Pending Enrolls Queue
      GDS_PendingEnrollsQueue.Init();
      GDS_PendingEnrollsQueue.Start();

      executor = GDS_Client.GetInstance();

      server = new GDS_Server(Ipe);

      server.Sink = executor;

      WSI.GDS.GDS.Server = server;

      server.Start();
      
      GDS_Scheduler gds;

      gds = new GDS_Scheduler();
      gds.Init();
      gds.Start();


      // Start directory Service
      ds = new GDS_DirectoryService(Ipe);

      Log.Message("GDS_Service listening on: " + service_ip_address);
      Log.Message("Directory Service says:   " + service_ip_address);
      Log.Message("");
      Log.Message("");
    }


    /// <summary>
    /// Main Algorithm
    /// </summary>
    /// <param name="args"></param>
    //The Main function is now public and renamed to Main_Old
    static public void Main_Old(Object argums) 
    {
      //GDS_DirectoryService ds;
      //GDS_Server server;
      IPEndPoint ipe;
      //IXmlSink  executor;
      NetLogger netlog;
      Settings settings;
      String default_xml;
      String service_ip_address;
      // The new Object param should be converted to the type of the old
      String[] args;
      String client_id_str;
      int client_id;

      args = (String[])argums;
      // Configuration default values 
      default_xml = "<SiteConfig>" +
                      "<DBPrincipal>192.168.200.1</DBPrincipal>" +
                      "<DBMirror>192.168.200.2</DBMirror>" +
                      "<DBId>0</DBId>" + 
                      "<Server>" +
                        "<LocalIP>192.168.1.13</LocalIP>" +
                        "<CJ>" +
                           "<VendorID>WIN12</VendorID>" +
                           "<VendorIP>192.168.200.1</VendorIP>" +
                           "<RemoteIP>10.1.1.112</RemoteIP>" +
                        "</CJ>" +
                      "</Server>" +
                    "</SiteConfig>";

      // Start application logger
      netlog = new NetLogger ();

      // 12/JUL/2007 AJQ The Per Terminal Netlog has been commented out
      // NetLog.AddListener(new NetLoggerPerTerminal());
      NetLog.AddListener (netlog);
      Log.AddListener (new SimpleLogger ("GDS"));

      Log.Message("============ Starting WSI Service ============");  

      if (!ConfigurationFile.Init("WSI.GDS_Configuration", default_xml))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      //GetInfo x = new GetInfo();

      //string mac = x.GetMACAddress();
      ////MultipleDequeue ();
      //XXXX ();

      DateTime license_not_valid_after;

      if (!Misc.CheckLicense(out license_not_valid_after))
      {
        System.Threading.Thread.Sleep(5000);
        Environment.Exit(0);

        return;
      }
      Misc.StartLicenseThread();

      // Get Local IpAddress
      //service_ip_address = DNSUtility.GetPrivateIPAddress();
      service_ip_address = ConfigurationFile.GetSetting("LocalIP");
      if (service_ip_address == "")
      {
        Log.Error(" No Discovery Service IP address is available. Application stopped");

        return;
      }        

      // Service IP End Point
      ipe = new IPEndPoint(IPAddress.Parse(service_ip_address), 13000);


      settings = new Settings();
      // Connect to database
      // APB 12/11/2008: 'Client Identifier' is now taken from configuration file instead of from a hard-coded constant
      client_id_str = ConfigurationFile.GetSetting("DBId");
      client_id = Convert.ToInt32(client_id_str);
      WGDB.Init(client_id, ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication  ("GDS_Service", GDS_SERVICE_VERSION);
      WGDB.ConnectAs("WGROOT");

      // Start Cache
      DbCache.Init ();
      DbCache.Start ();

      ServiceStatus.InitService("GDS", service_ip_address, false, GDS_SERVICE_VERSION);

      StartService(ipe);
      //Running Bucle
      ServiceStatus.RunningService("GDS", service_ip_address, false, GDS_SERVICE_VERSION);
    }
  }
}



