using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;

public class StandardService : ServiceBase
{


  public static void Main()
  {
    // 2008-08-18 - DRG
    // Use this lines for debug pruposes
    //
    StandardService s = new StandardService();
    s.OnStart(null);

    //ServiceBase.Run(new StandardService());
  }

  public StandardService()
  {
    this.CanStop = true;
    this.AutoLog = true;
  }

  protected override void OnStart(string[] args)
  {
    Assembly asembl;
    asembl = Assembly.GetExecutingAssembly();
    int init_str;
    //The Service will be executed on the Assembly (.exe) folder.
    init_str = asembl.Location.IndexOf(asembl.ManifestModule.Name);
    Directory.SetCurrentDirectory(asembl.Location.Substring(0, init_str));
    System.Threading.Thread t;
    t = new System.Threading.Thread(WSI.GDS_Service.Program.Main_Old); //Use the Current main renamed to Main_Old
    t.Start(args);
    //This is where your service would do something useful when it starts
  }

  protected override void OnStop()
  {
    //This is where your service would do something useful when it stops
  }

  private void InitializeComponent()
  {
    this.CanHandlePowerEvent = true;
    this.CanHandleSessionChangeEvent = true;
    this.CanPauseAndContinue = true;
    this.CanShutdown = true;
  }

}


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under network account and will be configured later with a correct username and password.
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }


  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "GDS_Service";
  }


  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "GDS_Service";
  }
}
