//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Terminal.cs
// 
//   DESCRIPTION: Implements the calls to the stored procedures.
//                The stored procedures implement Start, Update and End Session.
//
//        AUTHOR: 
// 
// CREATION DATE: 30-APR-2010 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-APR-2010 MBF   First version
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common;

namespace WSI.Alesis_ClientSimulator
{
  public enum StatusCode
  {
    Success,
    AccountNumberNotValid,
    AccountAlreadyInUse,
    MachineNumberNotValid,
    SessionNumberNotValid,
    AccessDenied,
    NotConnectedToDatabase,
    Error,
    AccountInSession,
  }

  public enum Procedure
  {
    SessionStart,
    SessionUpdate,
    SessionEnd,
    SendEvent,
    AccountStatus
  }

  public enum SendEventCode
  {
    JackpotWon    = 1,
    KioskBlocked  = 2,
    CallAttendant = 3,
  }

  static public class Procedures
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Start Alesis card session
    //
    //  PARAMS:
    //      - INPUT:
    //          - Terminal
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode StartCardSession(Terminal Terminal)
    {
      StatusCode    _status_code;
      String        _status_text;

      _status_code = ExecuteProcedure(Terminal, Terminal.sql_connection,
                                       Procedure.SessionStart, 
                                       Terminal.m_track_data,
                                       Terminal.m_vendor_id,
                                       Terminal.m_serial_number,
                                       Terminal.m_machine_id,
                                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                                       out Terminal.current_balance,
                                       out Terminal.play_session_id,
                                       out _status_text);

      return _status_code;
    } // StartCardSession

    //------------------------------------------------------------------------------
    // PURPOSE: End Alesis card session
    //
    //  PARAMS:
    //      - INPUT:
    //          - Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode EndCardSession(Terminal Terminal)
    {
      StatusCode _status_code;
      String     _status_text;
      Decimal    _aux_terminal_balance;
      long       _aux_current_session_id;

      _status_code = ExecuteProcedure(Terminal, Terminal.sql_connection,
                                      Procedure.SessionEnd,
                                      Terminal.m_track_data,
                                      Terminal.m_vendor_id,
                                      Terminal.m_serial_number,
                                      Terminal.m_machine_id,
                                      Terminal.play_session_id,
                                      Terminal.total_played_count,
                                      Terminal.total_played_amount,
                                      Terminal.total_won_count,
                                      Terminal.total_won_amount,
                                      Terminal.current_balance,
                                      0, 0, 0,
                                      out _aux_terminal_balance,
                                      out _aux_current_session_id, 
                                      out _status_text);

      return _status_code;
    } // EndCardSession


    //------------------------------------------------------------------------------
    // PURPOSE: Report play to Alesis
    //
    //  PARAMS:
    //      - INPUT:
    //          - Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode ReportPlay(Terminal Terminal)
    {
      StatusCode _status_code;
      String     _status_text;
      Decimal    _aux_terminal_balance;
      long       _aux_current_session_id;

      _status_code = StatusCode.Error;


      _status_code = ExecuteProcedure(Terminal, Terminal.sql_connection,
                                      Procedure.SessionUpdate,
                                      Terminal.m_track_data,
                                      Terminal.m_vendor_id,
                                      Terminal.m_serial_number,
                                      Terminal.m_machine_id,
                                      Terminal.play_session_id,
                                      Terminal.total_played_count,
                                      Terminal.total_played_amount,
                                      Terminal.total_won_count,
                                      Terminal.total_won_amount,
                                      Terminal.current_balance,
                                      0, 0, 0,
                                      out _aux_terminal_balance,
                                      out _aux_current_session_id,
                                      out _status_text);

      return _status_code;
    } // ReportPlay

    //------------------------------------------------------------------------------
    // PURPOSE: Report event
    //
    //  PARAMS:
    //      - INPUT:
    //          - AleTerminal
    //          - EventId
    //          - PrizeAmount
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode ReportEvent(Terminal Terminal, SendEventCode EventId, Decimal PrizeAmount)
    {
      StatusCode _status_code;
      String _status_text;
      Decimal _aux_terminal_balance;
      long _aux_current_session_id;

      _status_code = StatusCode.Error;

      _status_code = ExecuteProcedure(Terminal, Terminal.sql_connection,
                                      Procedure.SendEvent,
                                      Terminal.m_track_data,
                                      Terminal.m_vendor_id,
                                      Terminal.m_serial_number,
                                      Terminal.m_machine_id,
                                      Terminal.play_session_id,
                                      Terminal.total_played_count,
                                      Terminal.total_played_amount,
                                      Terminal.total_won_count,
                                      Terminal.total_won_amount,
                                      Terminal.current_balance,
                                      0,
                                      (Int32) EventId,
                                      PrizeAmount,
                                      out _aux_terminal_balance,
                                      out _aux_current_session_id,
                                      out _status_text);

      return _status_code;
    } // ReportEvent

    //------------------------------------------------------------------------------
    // PURPOSE: Account Status
    //
    //  PARAMS:
    //      - INPUT:
    //          - Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode AccountStatus(Terminal Terminal)
    {
      StatusCode _status_code;
      String _status_text;
      Decimal _aux_terminal_balance;
      long _aux_current_session_id;

      _status_code = StatusCode.Error;

      _status_code = ExecuteProcedure(Terminal, Terminal.sql_connection,
                                      Procedure.AccountStatus,
                                      Terminal.m_track_data,
                                      Terminal.m_vendor_id,
                                      Terminal.m_serial_number,
                                      Terminal.m_machine_id,
                                      Terminal.play_session_id,
                                      Terminal.total_played_count,
                                      Terminal.total_played_amount,
                                      Terminal.total_won_count,
                                      Terminal.total_won_amount,
                                      Terminal.current_balance,
                                      0,
                                      0,
                                      0,
                                      out _aux_terminal_balance,
                                      out _aux_current_session_id,
                                      out _status_text);

      return _status_code;
    } // AccountStatus

    private static StatusCode ExecuteProcedure (Terminal      Terminal,
                                                SqlConnection Connection,
                                                Procedure     ProcedureID,
                                                String        TrackData,
                                                String        VendorID,
                                                String        SerialNumber,
                                                int           MachineID,
                                                Int64         PlaySessionID,
                                                int           TotalGamesPlayed,
                                                Decimal       TotalAmountPlayed,
                                                int           TotalGamesWon,
                                                Decimal       TotalAmountWon,
                                                Decimal       FinalBalance,
                                                Decimal       CurrJackpotAmount,
                                                Int32         EventId,
                                                Decimal       PrizeAmount,
                                                out Decimal   CurrentBalance,
                                                out long      CurrentSessionID,
                                                out String    StatusText)
    {
      SqlConnection _sql_conn;
      SqlCommand    _sql_cmd;
      SqlDataReader _sql_reader;
      StatusCode    _status_code;
      String        _status_text;
      String        _sql_str;
      Int32         _status_num;

      _status_code = StatusCode.Error;
      _status_text = "ERROR";
      _sql_conn    = null;
      _sql_cmd     = null;
      _sql_reader  = null;
      _sql_str     = "";
      _status_num  = 0;

      CurrentBalance   = 0;
      CurrentSessionID = 0;
      StatusText       = _status_text;

      switch (ProcedureID)
      {
        case Procedure.SessionStart:
          _sql_str = "EXECUTE zsp_SessionStart  @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                           @pCurrJackpotAmt, "
                  + "                           @pEmgSessionID";
          break;

        case Procedure.SessionUpdate:
          _sql_str = "EXECUTE zsp_SessionUpdate @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                           @pSessionID, "
                  + "                           @pCreditsPlayed, @pCreditsWon, @pGamesPlayed, @pGamesWon,  "
                  + "                           @pAcctBalance, "
                  + "                           @pCurrJackpotAmt";
          break;

        case Procedure.SessionEnd:
          _sql_str = "EXECUTE zsp_SessionEnd    @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                           @pSessionID, "
                  + "                           @pCreditsPlayed, @pCreditsWon, @pGamesPlayed, @pGamesWon,  "
                  + "                           @pAcctBalance, "
                  + "                           @pCurrJackpotAmt";
          break;

        case Procedure.SendEvent:
          _sql_str = "EXECUTE zsp_SendEvent    @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                         @pEventID, "
                  + "                         @pPauoutAmt";
          break;

        case Procedure.AccountStatus:
          _sql_str = "EXECUTE zsp_Accountstatus @pAccountID, @pVendorID, @pMachineID";
          break;

        default:
          return StatusCode.Error;
      } // switch (ProcedureID)

      try
      {
        _sql_conn = Terminal.LockConnection ();
        if (_sql_conn == null)
        {
          return StatusCode.NotConnectedToDatabase;
        }
        if (_sql_conn.State != ConnectionState.Open)
        {
          return StatusCode.NotConnectedToDatabase;
        }

        _sql_cmd            = new SqlCommand(_sql_str);
        _sql_cmd.Connection = _sql_conn;

        _sql_cmd.Parameters.Add("@pAccountID",      SqlDbType.NVarChar, 24).Value = TrackData;
        _sql_cmd.Parameters.Add("@pVendorID",       SqlDbType.NVarChar, 16).Value = VendorID;
        _sql_cmd.Parameters.Add("@pSerialNumber",   SqlDbType.NVarChar, 30).Value = SerialNumber;
        _sql_cmd.Parameters.Add("@pMachineID",      SqlDbType.Int).Value          = MachineID;
        _sql_cmd.Parameters.Add("@pCurrJackpotAmt", SqlDbType.Int).Value          = 0;

        if (ProcedureID == Procedure.SessionUpdate
         || ProcedureID == Procedure.SessionEnd)
        {
          _sql_cmd.Parameters.Add("@pSessionID",     SqlDbType.BigInt).Value = PlaySessionID;
          _sql_cmd.Parameters.Add("@pCreditsPlayed", SqlDbType.Money).Value  = TotalAmountPlayed;
          _sql_cmd.Parameters.Add("@pCreditsWon",    SqlDbType.Money).Value  = TotalAmountWon;
          _sql_cmd.Parameters.Add("@pGamesPlayed",   SqlDbType.Int).Value    = TotalGamesPlayed;
          _sql_cmd.Parameters.Add("@pGamesWon",      SqlDbType.Int).Value    = TotalGamesWon;
          _sql_cmd.Parameters.Add("@pAcctBalance",  SqlDbType.Money).Value   = FinalBalance;
        }

        if (ProcedureID == Procedure.SessionStart)
        {
          _sql_cmd.Parameters.Add("@pEmgSessionID", SqlDbType.BigInt).Value = 0;
        }

        if (ProcedureID == Procedure.SendEvent)
        {
          _sql_cmd.Parameters.Add("@pEventID", SqlDbType.Int).Value = EventId;
          _sql_cmd.Parameters.Add("@pPauoutAmt", SqlDbType.Money).Value = PrizeAmount;
        }

        _sql_conn.ResetStatistics();


        _sql_reader = _sql_cmd.ExecuteReader();

        if (_sql_reader.Read())
        {
          _status_num  = _sql_reader.GetInt32(0);
          _status_code = GetStatusCode(ProcedureID, EventId, _status_num);

          if (_status_num == 0)
          {
            if (ProcedureID == Procedure.SessionStart)
            {
              CurrentSessionID = _sql_reader.GetInt64(2);
              CurrentBalance   = _sql_reader.GetDecimal(3);
            }
          }
        }

        System.Collections.Hashtable _stats = (System.Collections.Hashtable)_sql_conn.RetrieveStatistics();
        Terminal.m_sql_net_time += (UInt64)((long)_stats["NetworkServerTime"]);
        Terminal.m_sql_cpu_time += (UInt64)((long)_stats["ExecutionTime"]);

        return _status_code;
      }
      catch (Exception _ex)
      {
        StatusText = "!!! " + _ex.Message;
        return _status_code;
      }
      finally
      {
        Terminal.UnlockConnection(null);

        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader = null;
        }
        if (_sql_cmd != null)
        {
          _sql_cmd = null;
        }
       }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Status Code from alesis procedure return code
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: alesis procedure return code
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    private static StatusCode GetStatusCode(Procedure ProcedureID, int EventId, int Value)
    {
      switch (ProcedureID)
      {
        case Procedure.SessionStart:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.AccountAlreadyInUse;
            case 3:
              return StatusCode.MachineNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SessionUpdate:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.MachineNumberNotValid;
            case 3:
              return StatusCode.SessionNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SessionEnd:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.MachineNumberNotValid;
            case 3:
              return StatusCode.SessionNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SendEvent:
          switch (Value)
          {
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 3:
              return StatusCode.MachineNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              if (Value == 100 + EventId)
              {
                return StatusCode.Success;
              }
              return StatusCode.Error;
          }
        // break
        
        case Procedure.AccountStatus:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.AccountInSession;
            case 3:
              return StatusCode.MachineNumberNotValid;
            default:
              return StatusCode.Error;
          }

        default:
          return StatusCode.Error;
      }
    }
  }

}
