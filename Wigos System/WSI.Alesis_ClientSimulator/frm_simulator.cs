//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_simulator.cs
// 
//   DESCRIPTION: The Alesis Client Simulator Form.
// 
//        AUTHOR: 
// 
// CREATION DATE: 30-APR-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-APR-2010 MBF    First release.
// 19-DEC-2016 JBP    Task 21699:FBM: Mejorar LOG
// 15-JUN-2017 XGJ    Bug 28079:S2S - Accountinfo is not displaying the correct information
// 28-SEP-2017 DPC    WIGOS-2816: S2S- Wrong strings when a user introduce an incorrect "SessionID" in the procedure "SessionEnd"
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using WSI.Common;
using System.ServiceModel;
using System.Xml;
using CommonListenerServiceDefinition;
using Meter = CommonListenerServiceDefinition.Meter;

//using System.Xml.Linq;
//using CommonListenerServiceDefinition;


namespace WSI.Alesis_ClientSimulator
{

  public partial class frm_simulator : Form
  {

    private ICommonListenerServiceDefinition _service;

    int m_tick0;
    int m_tmr_tick;
    int m_sp_selected;
    Simulator.Statistics m_stats0;
    Simulator.Statistics m_stats1;
    Simulator.Statistics m_stats5;
    Simulator.Statistics m_diff0;
    Simulator.Statistics m_diff1;
    Simulator.Statistics m_diff5;

    String[] _array_machine_num;
    String[] _array_trackdata;

    DataTable m_dt_terminals = null;
    SqlDataAdapter m_adap_terminals = null;
    SqlDataAdapter m_adap_terminals_3gs = null;
    SqlDataAdapter m_adap_terminals_pending = null;
    SqlDataAdapter m_adap_term_game = null;
    SqlCommand m_cmd_insert_game = null;
    SqlCommand m_cmd_select_game = null;
    SqlCommand m_cmd_select_terminal = null;
    SqlCommand m_cmd_select_terminal_3gs = null;

    DataTable m_dt_accounts = null;
    SqlDataAdapter m_adap_accounts = null;
    SqlCommand m_cmd_reset_accounts = null;
    SqlCommand m_cmd_set_balance_accounts = null;

    enum STATUS_SESSION
    {
      SESSION_OK,
      SESSION_NOK
    }

    string m_status_session;

    internal enum SP_INDEX
    {
      SessionStart = 0,
      SessionUpdate = 1,
      SessionEnd = 2,
      AccountStatus = 3,
      ReportEvent = 4,
      ReportMeters = 5,
      AccountInfo = 6,
      CardInfo = 7
    }

    public frm_simulator()
    {
      String[] _array_serial_num;

      _array_serial_num = new String[] {
            "3GS",
            "ALBERTO",
            "MIQUEL",
            "SANTI",
            "RAUL"
      };
      _array_machine_num = new String[] {
            "10000",
            "1012700",
            "1012900",
            "1013300",
            "1013100"
      };
      _array_trackdata = new String[] {
            "9950000000001",
            "1234000000097",
            "1234000000099",
            "1234000000097",
            "1234000000097"
      };

      InitializeComponent();

      txt_min_session_plays.Text = "10";
      txt_max_session_plays.Text = "100";
      txt_min_delay_between_plays.Text = "1000";
      txt_max_delay_between_plays.Text = "2000";
      txt_min_delay_between_sessions.Text = "100";
      txt_max_delay_between_sessions.Text = "200";

      //txt_ip_address_1.Text = "93.90.30.44";
      //txt_ip_address_2.Text = "93.90.30.44";
      txt_ip_address_1.Text = "VSCAGE";
      txt_ip_address_2.Text = "";
      txt_db_name.Text = "WGDB_000";
      txt_db_user.Text = "WGROOT_000";
      txt_db_pass.Text = "Wigos_ro_000";

      txt_num_terminals.Text = "1";
      txt_vendor_id.Text = "3GS";
      cmb_serial_num.Text = _array_serial_num[0];
      txt_machine_num.Text = _array_machine_num[0];

      txt_card_track_data.Text = "9950000000001";

      cmb_serial_num.Items.AddRange(_array_serial_num);

      lbl_head.Text = FormatLabelString("#Terms", "#Conn", "#Started", "#Plays",
                                        "Starts/s", "Updates/s", "Ends/s", "ms/Start", "ms/Update", "ms/End", "Sql-CPU", "Sql-Net");
      lbl_head_totals.Text = FormatLabelString("#Terms", "#Conn", "#Started", "#Plays", "Starts", "Updates", "Ends", "Sql-CPU", "Sql-Net");

      // WS Tab
      this.InitializeWSTab();
      this.ShowWSControls(false);
      this.txt_IPadress.Text = "http://127.0.0.1:7776";
    }

    private void InitializeWSTab()
    {
      this.pnl_session_update.Dock = DockStyle.Fill;
      this.pnl_session_end.Dock = DockStyle.Fill;
      this.pnl_account_status.Dock = DockStyle.Fill;
      this.pnl_report_event.Dock = DockStyle.Fill;
      this.pnl_Report_Meters.Dock = DockStyle.Fill;

      this.pnl_session_update.Visible = false;
      this.pnl_session_end.Visible = false;
      this.pnl_account_status.Visible = false;
      this.pnl_report_event.Visible = false;
      this.pnl_Report_Meters.Visible = false;
      this.pnl_account_info.Visible = false;
      this.pnl_Card_Info.Visible = false;

      this.pnl_start_session.Visible = true;
      this.cmb_sp.SelectedIndex = (int)SP_INDEX.SessionStart;

    }

    private String GetConnectString()
    {
      SqlConnectionStringBuilder _csb;

      if (txt_ip_address_1.Text.Equals(""))
      {
        return "";
      }
      if (txt_db_name.Text.Equals(""))
      {
        return "";
      }
      if (txt_db_user.Text.Equals(""))
      {
        return "";
      }
      if (txt_db_pass.Text.Equals(""))
      {
        return "";
      }
      if (txt_ip_address_2.Text.Equals(""))
      {
        txt_ip_address_2.Text = txt_ip_address_1.Text;
      }

      _csb = new SqlConnectionStringBuilder();

      // Prepare connection parameters
      _csb.ConnectTimeout = 30;
      _csb.DataSource = txt_ip_address_1.Text;
      _csb.FailoverPartner = txt_ip_address_2.Text;
      _csb.InitialCatalog = txt_db_name.Text;
      _csb.IntegratedSecurity = false;
      _csb.Pooling = true;
      _csb.AsynchronousProcessing = true;
      _csb.MaxPoolSize = 1000;
      _csb.MinPoolSize = 10;
      _csb.UserID = txt_db_user.Text;
      _csb.Password = txt_db_pass.Text;

      return _csb.ConnectionString;
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      RefreshCurrentTerminal();
      //RefreshTotals();
    }

    private void RefreshCurrentTerminal()
    {
      Simulator.Statistics m_diff_totals;

      lbl_statistics_1.Text = BuildStatisticsString(m_diff1, false);
      lbl_statistics_5.Text = BuildStatisticsString(m_diff5, false);
      lbl_statistics_avg.Text = BuildStatisticsString(m_diff0, false);

      m_diff_totals = m_diff0;
      m_diff_totals.interval = 1000;
      lbl_totals.Text = BuildStatisticsString(m_diff_totals, true);
    }

    private void StartSim()
    {
      String _trackdata;
      String _connect_str;
      int _num_terminals;

      if (txt_num_terminals.Text == null
       || txt_num_terminals.Text.Equals(""))
      {
        _num_terminals = 1;
      }
      else
      {
        _num_terminals = int.Parse(txt_num_terminals.Text);
      }

      if (txt_card_track_data.Text == null
       || txt_card_track_data.Text.Equals(""))
      {
        if (txt_encrypted_card_track_data.Text == null
         || txt_encrypted_card_track_data.Text.Equals(""))
        {
          return;
        }

        _trackdata = Terminal.DecryptTrackData(txt_encrypted_card_track_data.Text);
      }
      else
      {
        _trackdata = txt_card_track_data.Text;
      }

      _connect_str = GetConnectString();
      if (_connect_str == null || _connect_str == "")
      {
        return;
      }

      try
      {
        Simulator.Start(_num_terminals,
                         _connect_str,
                         txt_vendor_id.Text,
                         int.Parse(txt_machine_num.Text),
                         cmb_serial_num.Text.Trim().ToUpper() + "_TERMINAL",
                         _trackdata,
                         int.Parse(txt_min_session_plays.Text),
                         int.Parse(txt_max_session_plays.Text),
                         int.Parse(txt_min_delay_between_plays.Text),
                         int.Parse(txt_max_delay_between_plays.Text),
                         int.Parse(txt_min_delay_between_sessions.Text),
                         int.Parse(txt_max_delay_between_sessions.Text),
                         int.Parse(txt_max_plays_x_update.Text),
                         int.Parse(txt_min_update_x_sec.Text),
                         chk_report_jackpot.Checked,
                         chk_account_status.Checked);
      }
      catch
      {
        return;
      }

      m_tick0 = Environment.TickCount;
      m_stats0 = Simulator.QueryCounters();
      m_stats1 = m_stats0;
      m_stats5 = m_stats0;
      m_tmr_tick = 0;

      tmr_stats.Start();

      m_btn_start.Text = "Stop";
      lbl_sim_status.Text = "Running";
      lbl_sim_status.ForeColor = Color.FromArgb(0, 192, 0);
      if (tab_simulator.SelectedIndex == 0) tab_simulator.SelectedIndex = 1;
    }

    private void StopSim()
    {
      Simulator.Stop();
      tmr_stats.Stop();

      Simulator.WaitForTerminalFinish();
      tmr_stats_Tick(null, null);

      m_btn_start.Text = "Start";
      lbl_sim_status.Text = "Stopped";
      lbl_sim_status.ForeColor = Color.FromArgb(255, 128, 128);
    }

    private void btn_start_Click(object sender, EventArgs e)
    {
      Int32 _num_terms;
      Cursor _prev_cursor;

      if (!Int32.TryParse(txt_num_terminals.Text, out _num_terms))
      {
        MessageBox.Show("Num. Terminals must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (_num_terms > 1000)
      {
        MessageBox.Show("Num. Terminals must be less or equal than 1000.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      _prev_cursor = this.Cursor;
      this.Cursor = Cursors.WaitCursor;

      if (m_btn_start.Text == "Start")
      {
        StartSim();
      }
      else
      {
        StopSim();
      }
      this.Cursor = _prev_cursor;
    }

    private String FormatLabelString(String NumTerminals, String NumConnected, String NumStarted,
                                     String Plays, String Starts, String Updates, String Ends,
                                     String ElapsedStart, String ElapsedPlay, String ElapsedEnd, String SqlCpu, String SqlNet)
    {
      return String.Format("{0,10}", NumTerminals)
           + String.Format("{0,10}", NumConnected)
           + String.Format("{0,10}", NumStarted)
           + String.Format("{0,10}", Plays)
           + String.Format("{0,10}", Starts)
           + String.Format("{0,10}", Updates)
           + String.Format("{0,10}", Ends)
           + String.Format("{0,10}", ElapsedStart)
           + String.Format("{0,10}", ElapsedPlay)
           + String.Format("{0,10}", ElapsedEnd)
           + String.Format("{0,10}", SqlCpu)
           + String.Format("{0,10}", SqlNet);
    }

    private String FormatLabelString(String NumTerminals, String NumConnected, String NumStarted,
                                     String Plays, String Starts, String Updates, String Ends, String SqlCpu, String SqlNet)
    {
      return FormatLabelString(NumTerminals, NumConnected, NumStarted, Plays, Starts, Updates, Ends, "", "", "", SqlCpu, SqlNet);
    }

    private String BuildStatisticsString(Simulator.Statistics Stats, Boolean IsTotals)
    {
      Decimal _started = 0;
      Decimal _plays = 0;
      Decimal _updates = 0;
      Decimal _finished = 0;
      Decimal _ms_elapsed_start = 0;
      Decimal _ms_elapsed_update = 0;
      Decimal _ms_elapsed_end = 0;
      String _str_elapsed_start;
      String _str_elapsed_update;
      String _str_elapsed_end;
      String _str;

      if (Stats.interval > 0)
      {
        _started = (((Decimal)Stats.sum_started) / Stats.interval) * 1000;
        _plays = (((Decimal)Stats.sum_plays) / Stats.interval) * 1000;
        _updates = (((Decimal)Stats.sum_updates) / Stats.interval) * 1000;
        _finished = (((Decimal)Stats.sum_finished) / Stats.interval) * 1000;
      }

      if (!IsTotals)
      {
        if (Stats.sum_started > 0)
        {
          _ms_elapsed_start = (Stats.sum_interval_start / Stats.sum_started);
          _str_elapsed_start = _ms_elapsed_start.ToString("0.0");
        }
        else
        {
          _str_elapsed_start = "---";
        }
        if (Stats.sum_updates > 0)
        {
          _ms_elapsed_update = (Stats.sum_interval_update / Stats.sum_updates);
          _str_elapsed_update = _ms_elapsed_update.ToString("0.0");
        }
        else
        {
          _str_elapsed_update = "---";
        }
        if (Stats.sum_finished > 0)
        {
          _ms_elapsed_end = (Stats.sum_interval_end / Stats.sum_finished);
          _str_elapsed_end = _ms_elapsed_end.ToString("0.0");
        }
        else
        {
          _str_elapsed_end = "---";
        }

        _str = FormatLabelString(Stats.num_terminals.ToString(),
                                 Stats.num_connected.ToString(),
                                 Stats.num_started.ToString(),
                                 _plays.ToString("0"),
                                 _started.ToString("0.0"),
                                 _updates.ToString("0.0"),
                                 _finished.ToString("0.0"),
                                 _str_elapsed_start,
                                 _str_elapsed_update,
                                 _str_elapsed_end,
                                 Stats.sum_sql_cpu.ToString(),
                                 Stats.sum_sql_net.ToString());
      }
      else
      {
        _str = FormatLabelString(Stats.num_terminals.ToString(),
                                 Stats.num_connected.ToString(),
                                 Stats.num_started.ToString(),
                                 _plays.ToString("0"),
                                 _started.ToString("0"),
                                 _updates.ToString("0"),
                                 _finished.ToString("0"),
                                 Stats.sum_sql_cpu.ToString(),
                                 Stats.sum_sql_net.ToString());
      }

      return _str;
    }

    private void tmr_stats_Tick(object sender, EventArgs e)
    {
      Simulator.Statistics _current;
      int _elapsed;

      m_tmr_tick++;
      _current = Simulator.QueryCounters();
      _elapsed = Environment.TickCount;
      if (_elapsed >= m_tick0)
      {
        _elapsed -= m_tick0;
      }
      else
      {
        _elapsed += (int.MaxValue - m_tick0) + 1;
      }

      m_diff0 = Simulator.DiffCounters(_current, m_stats0, _elapsed);

      m_diff1 = Simulator.DiffCounters(_current, m_stats1, 1000);
      m_stats1 = _current;

      if (m_tmr_tick % 5 == 0)
      {
        m_diff5 = Simulator.DiffCounters(_current, m_stats5, 5000);
        m_stats5 = _current;
      }
    }

    private void txt_encrypted_card_track_data_Validated(object sender, EventArgs e)
    {
      if (txt_encrypted_card_track_data.Text == null
       || txt_encrypted_card_track_data.Text.Equals(""))
      {
        return;
      }
      txt_encrypted_card_track_data.Text = txt_encrypted_card_track_data.Text.PadLeft(20, '0');
      txt_card_track_data.Text = "";
    }

    private void txt_card_track_data_Validated(object sender, EventArgs e)
    {
      if (txt_card_track_data.Text == null
       || txt_card_track_data.Text.Equals(""))
      {
        return;
      }
      txt_card_track_data.Text = txt_card_track_data.Text.PadLeft(13, '0');
      txt_encrypted_card_track_data.Text = "";
    }

    private void cmb_serial_num_SelectedIndexChanged(object sender, EventArgs e)
    {
      txt_machine_num.Text = _array_machine_num[cmb_serial_num.SelectedIndex];
      if (txt_encrypted_card_track_data.Text == null
       || txt_encrypted_card_track_data.Text.Equals(""))
      {
        txt_card_track_data.Text = _array_trackdata[cmb_serial_num.SelectedIndex];
      }
    }

    private void btn_set_terminals_Click(object sender, EventArgs e)
    {
      Int32 _num_terms;
      Int32 _machine_id;
      Int32 _game_id;
      DataRow _row;
      String _serial_number;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Int32 _nr_updated;

      // Create terminals if not exist: need to create in table terminals, terminals_3gs and terminal_game_translation.

      if (!Int32.TryParse(txt_num_terminals.Text, out _num_terms))
      {
        MessageBox.Show("Num. Terminals must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (_num_terms > 1000)
      {
        MessageBox.Show("Num. Terminals must be less or equal than 1000.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (!Int32.TryParse(txt_machine_num.Text, out _machine_id))
      {
        MessageBox.Show("Machine ID must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (txt_vendor_id.Text == "")
      {
        MessageBox.Show("Vendor ID is empty.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (cmb_serial_num.Text == "")
      {
        MessageBox.Show("Serial Number is empty.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      _sql_conn = ConnectDB();
      if (_sql_conn == null)
      {
        MessageBox.Show("Can't connect to DB.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      if (!CheckSqlConnection(_sql_conn))
      {
        return;
      }

      InitSQLTerminals();

      _serial_number = cmb_serial_num.Text.Trim().ToUpper() + "_TERMINAL";

      if (TerminalIsNot3GS(txt_vendor_id.Text, _serial_number, _sql_conn))
      {
        MessageBox.Show("Terminal " + cmb_serial_num.Text.Trim().ToUpper() + " exists and is not an Alesis terminal.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      // Insert Default Game3GS. Retrieve game_id.
      _game_id = InsertAndSelectGame3GS(_sql_conn);

      if (_game_id == 0)
      {
        MessageBox.Show("Unable to retrieve the GameID for game: GAME_3GS.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      _sql_trx = null;

      try
      {
        for (UInt32 _idx = 0; _idx < _num_terms; _idx++)
        {
          _row = m_dt_terminals.NewRow();

          _row[0] = txt_vendor_id.Text;
          _row[1] = _serial_number + "_" + _idx.ToString("000");
          _row[2] = _row[0] + "-" + _row[1];
          _row[3] = _machine_id + _idx;
          _row[5] = _game_id;

          m_dt_terminals.Rows.Add(_row);
        }

        // BEGIN TRX
        _sql_trx = _sql_conn.BeginTransaction();

        // Insert TERMINALS
        m_adap_terminals.InsertCommand.Connection = _sql_conn;
        m_adap_terminals.InsertCommand.Transaction = _sql_trx;
        _nr_updated = m_adap_terminals.Update(m_dt_terminals);

        // Insert TERMINALS 3GS
        m_dt_terminals.AcceptChanges();
        foreach (DataRow _data_row in m_dt_terminals.Rows)
        {
          _data_row.SetAdded();
        }
        m_adap_terminals_3gs.InsertCommand.Connection = _sql_conn;
        m_adap_terminals_3gs.InsertCommand.Transaction = _sql_trx;
        _nr_updated = m_adap_terminals_3gs.Update(m_dt_terminals);

        // Insert TERMINAL_GAME_TRANSLATION
        m_dt_terminals.AcceptChanges();
        foreach (DataRow _data_row in m_dt_terminals.Rows)
        {
          _data_row.SetAdded();
        }
        m_adap_term_game.InsertCommand.Connection = _sql_conn;
        m_adap_term_game.InsertCommand.Transaction = _sql_trx;
        _nr_updated = m_adap_term_game.Update(m_dt_terminals);

        // Delete TERMINALS_PENDING
        m_dt_terminals.AcceptChanges();
        foreach (DataRow _data_row in m_dt_terminals.Rows)
        {
          _data_row.Delete();
        }
        m_adap_terminals_pending.DeleteCommand.Connection = _sql_conn;
        m_adap_terminals_pending.DeleteCommand.Transaction = _sql_trx;
        _nr_updated = m_adap_terminals_pending.Update(m_dt_terminals);

        // COMMIT
        _sql_trx.Commit();

        MessageBox.Show("Terminals set.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_dt_terminals.Clear();

        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { ; }
          }
        }
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { ; }
          _sql_conn.Dispose();
          _sql_conn = null;
        }
      }
    } // btn_set_terminals_Click

    private void btn_set_accounts_Click(object sender, EventArgs e)
    {
      DataRow _row;
      Int32 _num_terms;
      UInt64 _base_card;
      UInt64 _track;
      String _trackdata;
      String _trackdata_min;
      String _trackdata_max;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;

      if (!Int32.TryParse(txt_num_terminals.Text, out _num_terms))
      {
        MessageBox.Show("Num. Terminals must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (!UInt64.TryParse(txt_card_track_data.Text, out _base_card))
      {
        MessageBox.Show("Internal Trackdata must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      _sql_conn = ConnectDB();
      if (_sql_conn == null)
      {
        MessageBox.Show("Can't connect to DB.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      if (!CheckSqlConnection(_sql_conn))
      {
        return;
      }

      InitSQLAccounts();

      for (UInt32 _idx = 0; _idx < _num_terms; _idx++)
      {
        _row = m_dt_accounts.NewRow();

        _track = _base_card + _idx;
        _trackdata = _track.ToString("0000000000000");
        _row[0] = _trackdata;

        m_dt_accounts.Rows.Add(_row);
      }

      _track = _base_card;
      _trackdata_min = _track.ToString("0000000000000");
      _track = _base_card + (UInt32)_num_terms - 1;
      _trackdata_max = _track.ToString("0000000000000");

      _sql_trx = null;

      try
      {
        _sql_trx = _sql_conn.BeginTransaction();

        m_adap_accounts.InsertCommand.Connection = _sql_conn;
        m_adap_accounts.InsertCommand.Transaction = _sql_trx;

        Int32 _n = m_adap_accounts.Update(m_dt_accounts);

        m_cmd_reset_accounts.Connection = _sql_conn;
        m_cmd_reset_accounts.Transaction = _sql_trx;
        m_cmd_set_balance_accounts.Connection = _sql_conn;
        m_cmd_set_balance_accounts.Transaction = _sql_trx;

        m_cmd_reset_accounts.Parameters[0].Value = _trackdata_min;
        m_cmd_reset_accounts.Parameters[1].Value = _trackdata_max;
        m_cmd_set_balance_accounts.Parameters[0].Value = _trackdata_min;
        m_cmd_set_balance_accounts.Parameters[1].Value = _trackdata_max;

        m_cmd_reset_accounts.ExecuteNonQuery();
        m_cmd_set_balance_accounts.ExecuteNonQuery();

        _sql_trx.Commit();

        MessageBox.Show("Accounts set.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_dt_accounts.Clear();

        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { ; }
          }
        }
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { ; }
          _sql_conn.Dispose();
          _sql_conn = null;
        }
      }
    } // btn_set_accounts_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Connect to DB
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SqlConnection
    //   NOTES :
    //
    private SqlConnection ConnectDB()
    {
      SqlConnection _sql_conn;
      String _connect_str;

      try
      {
        _connect_str = GetConnectString();

        if (_connect_str == null)
        {
          return null;
        }
        if (_connect_str.Equals(""))
        {
          return null;
        }

        _sql_conn = new SqlConnection(_connect_str);
        if (_sql_conn == null)
        {
          return null;
        }

        try
        {
          _sql_conn.Open();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          return null;
        }

        return _sql_conn;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    } // ConnectDB

    //------------------------------------------------------------------------------
    // PURPOSE : Determine if can access to DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConnection SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    //
    private bool CheckSqlConnection(SqlConnection SqlConn)
    {
      SqlCommand _sql_query;

      _sql_query = new SqlCommand("SELECT COUNT(*) FROM TERMINALS WHERE TE_TERMINAL_ID = 0");
      _sql_query.Connection = SqlConn;

      try
      {
        _sql_query.ExecuteScalar();
        return true;
      }
      catch (SqlException _sql_ex)
      {
        switch (_sql_ex.Number)
        {
          case 229:
            MessageBox.Show("DB User " + txt_db_user.Text + " has no permissions to access tables in DB " + txt_db_name.Text + ".", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            break;
          default:
            MessageBox.Show(_sql_ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            break;
        }
        return false;
      }
    } // CheckSqlConnection

    //------------------------------------------------------------------------------
    // PURPOSE : Determine if a terminal is type win but not 3gs.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String VendorId
    //          - String SerialNumber
    //          - SqlConnection SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    //
    private Boolean TerminalIsNot3GS(String VendorId, String SerialNumber, SqlConnection SqlConn)
    {
      Boolean is_win_terminal;
      Boolean is_3gs_terminal;

      try
      {
        m_cmd_select_terminal.Connection = SqlConn;
        m_cmd_select_terminal.Parameters[0].Value = SerialNumber + "_000";
        m_cmd_select_terminal.Parameters[1].Value = SerialNumber + "_999";

        is_win_terminal = ((Int32)m_cmd_select_terminal.ExecuteScalar() > 0);

        m_cmd_select_terminal_3gs.Connection = SqlConn;
        m_cmd_select_terminal_3gs.Parameters[0].Value = VendorId;
        m_cmd_select_terminal_3gs.Parameters[1].Value = SerialNumber + "_000";
        m_cmd_select_terminal_3gs.Parameters[2].Value = SerialNumber + "_999";

        is_3gs_terminal = ((Int32)m_cmd_select_terminal_3gs.ExecuteScalar() > 0);

        return is_win_terminal && !is_3gs_terminal;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return true;
      }
    } // TerminalIsNot3GS

    //------------------------------------------------------------------------------
    // PURPOSE : Insert and retrieve GAME_ID for generic game GAME_3GS.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConnection SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32 GameId
    //
    //   NOTES :
    //
    private Int32 InsertAndSelectGame3GS(SqlConnection SqlConn)
    {
      Object _obj;

      try
      {
        m_cmd_insert_game.Connection = SqlConn;

        if (m_cmd_insert_game.ExecuteNonQuery() == 1)
        {
          return (Int32)m_cmd_insert_game.Parameters[0].Value;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == 2601)
        {
          m_cmd_select_game.Connection = SqlConn;

          try
          {
            _obj = m_cmd_select_game.ExecuteScalar();
            if (_obj != null)
            {
              return (Int32)_obj;
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
        else
        {
          Log.Exception(_sql_ex);
        }
      }

      return 0;
    } // InsertGame3GS

    //------------------------------------------------------------------------------
    // PURPOSE : Init the data needed for set terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitSQLTerminals()
    {
      String _sql_query;
      SqlParameter _parameter;



      // If already init... return.
      if (m_dt_terminals != null)
      {
        return;
      }

      // Init the DataTable for insert accounts.
      m_dt_terminals = new DataTable("TERMINALS");
      m_dt_terminals.Columns.Add("VENDOR_ID", Type.GetType("System.String"));
      m_dt_terminals.Columns.Add("SERIAL_NUMBER", Type.GetType("System.String"));
      // row.Item("EXTERNAL_ID") = row.Item("VENDOR_ID") + "-" + row.Item("SERIAL_NUMBER")
      m_dt_terminals.Columns.Add("EXTERNAL_ID", Type.GetType("System.String"));
      m_dt_terminals.Columns.Add("MACHINE_NUMBER", Type.GetType("System.Int32"));
      m_dt_terminals.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
      m_dt_terminals.Columns.Add("GAME_ID", Type.GetType("System.Int32"));


      // Init Terminals SQLAdapter

      m_adap_terminals = new SqlDataAdapter();
      m_adap_terminals.SelectCommand = null;
      m_adap_terminals.UpdateCommand = null;
      m_adap_terminals.DeleteCommand = null;
      m_adap_terminals.UpdateBatchSize = 500;
      m_adap_terminals.ContinueUpdateOnError = true;

      _sql_query = "INSERT INTO TERMINALS " +
                   " ( TE_TYPE " +
                   " , TE_NAME " +
                   " , TE_EXTERNAL_ID " +
                   " , TE_BLOCKED " +
                   " , TE_ACTIVE " +
                   " , TE_PROVIDER_ID " +
                   " , TE_TERMINAL_TYPE " +
                   " , TE_VENDOR_ID " +
                   " ) " +
                   "VALUES " +
                   " ( 1 " +
                   " , @pSerialNumber " +
                   " , @pVendorSerialNumber " +
                   " , 0 " +
                   " , 1 " +
                   " , @pVendorId " +
                   " , @pTerminalType3GS " +
                   " , @pVendorId " +
                   " ) SET @pTerminalId = SCOPE_IDENTITY() ";

      m_adap_terminals.InsertCommand = new SqlCommand(_sql_query);
      m_adap_terminals.InsertCommand.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).SourceColumn = "SERIAL_NUMBER";
      m_adap_terminals.InsertCommand.Parameters.Add("@pVendorSerialNumber", SqlDbType.NVarChar).SourceColumn = "EXTERNAL_ID";
      m_adap_terminals.InsertCommand.Parameters.Add("@pVendorId", SqlDbType.NVarChar).SourceColumn = "VENDOR_ID";
      m_adap_terminals.InsertCommand.Parameters.Add("@pTerminalType3GS", SqlDbType.SmallInt).Value = (Int16)TerminalTypes.T3GS;
      m_adap_terminals.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

      _parameter = m_adap_terminals.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int);
      _parameter.SourceColumn = "TERMINAL_ID";
      _parameter.Direction = ParameterDirection.Output;


      // Init Terminal Game Translation SQLAdapter

      m_adap_term_game = new SqlDataAdapter();
      m_adap_term_game.SelectCommand = null;
      m_adap_term_game.UpdateCommand = null;
      m_adap_term_game.DeleteCommand = null;
      m_adap_term_game.UpdateBatchSize = 500;
      m_adap_term_game.ContinueUpdateOnError = true;

      _sql_query = "INSERT INTO TERMINAL_GAME_TRANSLATION " +
                   " ( TGT_TERMINAL_ID " +
                   " , TGT_SOURCE_GAME_ID " +
                   " , TGT_TARGET_GAME_ID " +
                   " ) " +
                   "VALUES " +
                   " ( @pTerminalId " +
                   " , @pSourceGameId " +
                   " , @pSourceGameId " +
                   " ) ";

      m_adap_term_game.InsertCommand = new SqlCommand(_sql_query);
      m_adap_term_game.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 0, "TERMINAL_ID");
      m_adap_term_game.InsertCommand.Parameters.Add("@pSourceGameId", SqlDbType.Int, 0, "GAME_ID");
      m_adap_term_game.InsertCommand.UpdatedRowSource = UpdateRowSource.None;


      // Init Terminals 3GS SQLAdapter

      m_adap_terminals_3gs = new SqlDataAdapter();
      m_adap_terminals_3gs.SelectCommand = null;
      m_adap_terminals_3gs.UpdateCommand = null;
      m_adap_terminals_3gs.DeleteCommand = null;
      m_adap_terminals_3gs.UpdateBatchSize = 500;
      m_adap_terminals_3gs.ContinueUpdateOnError = true;

      _sql_query = "INSERT INTO TERMINALS_3GS " +
                   " ( T3GS_VENDOR_ID " +
                   " , T3GS_SERIAL_NUMBER " +
                   " , T3GS_MACHINE_NUMBER " +
                   " , T3GS_TERMINAL_ID " +
                   " ) " +
                   "VALUES " +
                   " ( @pVendorId " +
                   " , @pSerialNumber " +
                   " , @pMachineNumber " +
                   " , @pTerminalId " +
                   " ) ";

      m_adap_terminals_3gs.InsertCommand = new SqlCommand(_sql_query);
      m_adap_terminals_3gs.InsertCommand.Parameters.Add("@pVendorId", SqlDbType.NVarChar).SourceColumn = "VENDOR_ID";
      m_adap_terminals_3gs.InsertCommand.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).SourceColumn = "SERIAL_NUMBER";
      m_adap_terminals_3gs.InsertCommand.Parameters.Add("@pMachineNumber", SqlDbType.Int).SourceColumn = "MACHINE_NUMBER";
      m_adap_terminals_3gs.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
      m_adap_terminals_3gs.InsertCommand.UpdatedRowSource = UpdateRowSource.None;


      // Init Terminals Pending SQLAdapter

      m_adap_terminals_pending = new SqlDataAdapter();
      m_adap_terminals_pending.SelectCommand = null;
      m_adap_terminals_pending.UpdateCommand = null;
      m_adap_terminals_pending.InsertCommand = null;
      m_adap_terminals_pending.UpdateBatchSize = 500;
      m_adap_terminals_pending.ContinueUpdateOnError = true;

      _sql_query = " DELETE FROM TERMINALS_PENDING " +
                   " WHERE TP_VENDOR_ID     = @pVendorId " +
                   "  AND TP_SERIAL_NUMBER = @pSerialNumber ";

      m_adap_terminals_pending.DeleteCommand = new SqlCommand(_sql_query);
      m_adap_terminals_pending.DeleteCommand.Parameters.Add("@pVendorId", SqlDbType.NVarChar).SourceColumn = "VENDOR_ID";
      m_adap_terminals_pending.DeleteCommand.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).SourceColumn = "SERIAL_NUMBER";
      m_adap_terminals_pending.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;


      // Init SQL Command for INSERT Game GAME_3GS
      _sql_query = "INSERT INTO GAMES " +
                   " ( GM_NAME " +
                   " ) " +
                   "VALUES " +
                   " ( 'GAME_3GS' " +
                   " ) SET @pGameId = SCOPE_IDENTITY() ";

      m_cmd_insert_game = new SqlCommand(_sql_query);
      m_cmd_insert_game.Parameters.Add("@pGameId", SqlDbType.Int).Direction = ParameterDirection.Output;


      // Init SQL Command for SELECT Game GAME_3GS
      _sql_query = "SELECT GM_GAME_ID " +
                   "  FROM GAMES " +
                   " WHERE GM_NAME = 'GAME_3GS'";

      m_cmd_select_game = new SqlCommand(_sql_query);


      // Init SQL Command for determine if a SERIAL_NUMBER is a 3GS Terminal
      _sql_query = "SELECT COUNT(*) " +
                   "  FROM TERMINALS " +
                   "     , TERMINALS_3GS " +
                   " WHERE TE_TERMINAL_ID     = T3GS_TERMINAL_ID " +
                   "   AND T3GS_VENDOR_ID     = @pVendorId " +
                   "   AND T3GS_SERIAL_NUMBER >= @pSerialNumberMin " +
                   "   AND T3GS_SERIAL_NUMBER <= @pSerialNumberMax ";

      m_cmd_select_terminal_3gs = new SqlCommand(_sql_query);
      m_cmd_select_terminal_3gs.Parameters.Add("@pVendorId", SqlDbType.NVarChar);
      m_cmd_select_terminal_3gs.Parameters.Add("@pSerialNumberMin", SqlDbType.NVarChar);
      m_cmd_select_terminal_3gs.Parameters.Add("@pSerialNumberMax", SqlDbType.NVarChar);


      // Init SQL Command for determine if a SERIAL_NUMBER is a Win Terminal
      _sql_query = "SELECT COUNT(*) " +
                   "  FROM TERMINALS " +
                   " WHERE TE_NAME >= @pSerialNumberMin " +
                   "   AND TE_NAME <= @pSerialNumberMax ";

      m_cmd_select_terminal = new SqlCommand(_sql_query);
      m_cmd_select_terminal.Parameters.Add("@pSerialNumberMin", SqlDbType.NVarChar);
      m_cmd_select_terminal.Parameters.Add("@pSerialNumberMax", SqlDbType.NVarChar);

    }  // InitSQLTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Init the data needed for set accounts:
    //           - The DataTable for insert accounts.
    //           - The SQL Adapter to insert accounts.
    //           - The UPDATE commands to reset accounts.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitSQLAccounts()
    {
      SqlCommand _sql_cmd_insert;
      StringBuilder _sql_query;

      // If already init... return.
      if (m_dt_accounts != null)
      {
        return;
      }

      // Init the DataTable for insert accounts.
      m_dt_accounts = new DataTable("ACCOUNTS");
      m_dt_accounts.Columns.Add("TRACKDATA", Type.GetType("System.String"));


      // Init the SQL Adapter to insert accounts.
      _sql_query = new StringBuilder();
      _sql_query.AppendLine("INSERT INTO ACCOUNTS ");
      _sql_query.AppendLine("           ( AC_TYPE ");
      _sql_query.AppendLine("           , AC_BLOCKED ");
      _sql_query.AppendLine("           , AC_TRACK_DATA) ");
      _sql_query.AppendLine("VALUES     ( 2, 0, @pTrackData )");

      _sql_cmd_insert = new SqlCommand(_sql_query.ToString());
      _sql_cmd_insert.Parameters.Add("@pTrackData", SqlDbType.NVarChar).SourceColumn = "TRACKDATA";

      m_adap_accounts = new SqlDataAdapter();
      m_adap_accounts.SelectCommand = null;
      m_adap_accounts.InsertCommand = _sql_cmd_insert;
      m_adap_accounts.UpdateCommand = null;
      m_adap_accounts.DeleteCommand = null;
      m_adap_accounts.ContinueUpdateOnError = false;

      m_adap_accounts.UpdateBatchSize = 500;
      m_adap_accounts.InsertCommand.UpdatedRowSource = UpdateRowSource.None;


      // Init the UPDATE commands to reset accounts.
      _sql_query = new StringBuilder();
      _sql_query.AppendLine("UPDATE ACCOUNTS ");
      _sql_query.AppendLine("   SET AC_LAST_TERMINAL_ID         = AC_CURRENT_TERMINAL_ID ");
      _sql_query.AppendLine("     , AC_LAST_TERMINAL_NAME       = AC_CURRENT_TERMINAL_NAME ");
      _sql_query.AppendLine("     , AC_LAST_PLAY_SESSION_ID     = AC_CURRENT_PLAY_SESSION_ID ");
      _sql_query.AppendLine("     , AC_CURRENT_TERMINAL_ID      = NULL ");
      _sql_query.AppendLine("     , AC_CURRENT_TERMINAL_NAME    = NULL ");
      _sql_query.AppendLine("     , AC_CURRENT_PLAY_SESSION_ID  = NULL ");
      _sql_query.AppendLine("     , AC_TYPE                     = 2 ");
      _sql_query.AppendLine(" WHERE AC_TRACK_DATA              >= @pTrackDataMin ");
      _sql_query.AppendLine("   AND AC_TRACK_DATA              <= @pTrackDataMax ");
      _sql_query.AppendLine("   AND AC_CURRENT_TERMINAL_ID IS NOT NULL ");

      m_cmd_reset_accounts = new SqlCommand(_sql_query.ToString());
      m_cmd_reset_accounts.Parameters.Add("@pTrackDataMin", SqlDbType.NVarChar);
      m_cmd_reset_accounts.Parameters.Add("@pTrackDataMax", SqlDbType.NVarChar);


      _sql_query = new StringBuilder();
      _sql_query.AppendLine("UPDATE ACCOUNTS ");
      _sql_query.AppendLine("   SET AC_BALANCE         = 1000 ");
      _sql_query.AppendLine("     , AC_RE_BALANCE      = 1000 ");
      _sql_query.AppendLine("     , AC_INITIAL_CASH_IN = 1000 ");
      _sql_query.AppendLine("     , AC_LAST_ACTIVITY   = GETDATE() ");
      _sql_query.AppendLine("     , AC_CREATED         = GETDATE() ");
      _sql_query.AppendLine(" WHERE AC_TRACK_DATA     >= @pTrackDataMin ");
      _sql_query.AppendLine("   AND AC_TRACK_DATA     <= @pTrackDataMax ");

      m_cmd_set_balance_accounts = new SqlCommand(_sql_query.ToString());
      m_cmd_set_balance_accounts.Parameters.Add("@pTrackDataMin", SqlDbType.NVarChar);
      m_cmd_set_balance_accounts.Parameters.Add("@pTrackDataMax", SqlDbType.NVarChar);

    } // InitSQLAccounts

    #region WEB Service

    private void ShowWSControls(Boolean ShowWSButtons)
    {
      // Controls Visibility
      //this.m_btn_start.Visible    = !ShowWSButtons;
      //this.lbl_sim_status.Visible = !ShowWSButtons;

      // WS Buttons
      this.btn_send.Visible = false;
      this.btn_clear_log.Visible = ShowWSButtons;
      this.btn_random.Visible = ShowWSButtons;
    } // ShowWSControls

    private void ShowWSPanel()
    {

      this.pnl_start_session.Visible = false;
      this.pnl_session_update.Visible = false;
      this.pnl_session_end.Visible = false;
      this.pnl_account_status.Visible = false;
      this.pnl_report_event.Visible = false;
      this.pnl_Report_Meters.Visible = false;
      this.pnl_account_info.Visible = false;
      this.pnl_Card_Info.Visible = false;

      switch ((SP_INDEX)this.SP_Selected)
      {
        case SP_INDEX.SessionStart:
          this.pnl_start_session.Visible = true;
          //this.pnl_session_start.Visible = true;
          break;
        case SP_INDEX.SessionUpdate:
          this.pnl_session_update.Visible = true;
          break;
        case SP_INDEX.SessionEnd:
          this.pnl_session_end.Visible = true;
          break;
        case SP_INDEX.AccountStatus:
          this.pnl_account_status.Visible = true;
          break;
        case SP_INDEX.ReportEvent:
          this.pnl_report_event.Visible = true;
          break;
        case SP_INDEX.ReportMeters:
          this.pnl_Report_Meters.Visible = true;
          break;
        case SP_INDEX.AccountInfo:
          this.pnl_account_info.Visible = true;
          break;
        case SP_INDEX.CardInfo:
          this.pnl_Card_Info.Visible = true;
          break;
      }
  
    } // ShowWSPanel

    private void tab_simulator_SelectedIndexChanged(object sender, EventArgs e)
    {
      TabPage _page = (sender as TabControl).SelectedTab;
      this.ShowWSControls((_page.Name == "tab_ws"));
      if (tab_simulator.SelectedIndex == 2)
        btn_send.Visible = true;
      else
        this.btn_send.Visible = false;
    } // tab_simulator_SelectedIndexChanged

    private void cmb_sp_SelectedIndexChanged(object sender, EventArgs e)
    {
      Int32 _selected = (sender as ComboBox).SelectedIndex;

      if (_selected != this.SP_Selected)
      {
        this.SP_Selected = _selected;
        this.ShowWSPanel();
      }
    }

    private void btn_send_Click(object sender, EventArgs e)
    {
      //System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

      //config.AppSettings.Settings["UserId"].Value = "myUserId";
      //config.Save(ConfigurationSaveMode.Modified);
      //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
      //configuration.AppSettings.Settings["endpoint address"].Value = "xx";
      //configuration.Save();

      //ConfigurationManager.RefreshSection("Client");

      this.ExecuteStored(this.cmb_sp.SelectedIndex);
    }

    private void ExecuteStored(int Selected)
    {
      BasicHttpBinding _binding = new BasicHttpBinding();
      EndpointAddress _endpoint = new EndpointAddress(txt_IPadress.Text);
      _service = rd_ws.Checked ? new ChannelFactory<ICommonListenerServiceDefinition>(_binding, _endpoint).CreateChannel() : null;
      switch ((SP_INDEX)Selected)
      {
        case SP_INDEX.SessionStart:
          this.ExecuteSessionStart();
          break;
        case SP_INDEX.SessionUpdate:
          this.ExecuteSessionUpdate();
          break;
        case SP_INDEX.SessionEnd:
          this.ExecuteSessionEnd();
          break;
        case SP_INDEX.AccountStatus:
          this.ExecuteAccountStatus();
          break;
        case SP_INDEX.ReportEvent:
          this.ExecuteReportEvent();
          break;
        case SP_INDEX.ReportMeters:
          this.ExecuteReportMeters();
          break;
        case SP_INDEX.AccountInfo:
          this.ExecuteAccountInfo();
          break;
        case SP_INDEX.CardInfo:
          this.ExecuteCardInfo();
          break;
      }
      _service = null;
    }


    private static String ReadGeneralParams(String Group, String Subject, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      String _value;

      _value = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY    = @pGroup ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY  = @pSubject ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar, 50).Value = Group;
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar, 50).Value = Subject;

          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _value = (String)_obj;
          }
        }
      }
      catch
      { }

      return _value;
    } // ReadGeneralParams


    private void ExecuteSessionStart()
    {
      string _vendor_id;
      string _serial_number;
      int _machine_number;
      long _vendor_session_id;
      string _track_data;

      _track_data = this.txt_session_start_account_id.Text;
      _vendor_id = this.txt_session_start_vendor_id.Text;
      _serial_number = this.txt_session_start_serial_number.Text;

      // TrackData
      if (String.IsNullOrEmpty(_track_data))
      {
        MessageBox.Show("El valor de 'AccountID' no tiene un formato correcto");
        return;
      }

      // VendorId
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'VendorID' no tiene un formato correcto");
        return;
      }

      // SerialNumber
      if (String.IsNullOrEmpty(_serial_number))
      {
        MessageBox.Show("El valor de 'Serial Number' no tiene un formato correcto");
        return;
      }

      // MachineNumber
      if (!Int32.TryParse(this.txt_session_start_machine_number.Text, out _machine_number))
      {
        MessageBox.Show("El valor de 'Machine Number' no tiene un formato correcto");
        return;
      }

      // VendorSessionId
      if (!Int64.TryParse(this.txt_session_start_vendor_session_id.Text, out _vendor_session_id))
      {
        _vendor_session_id = 0;
      }

      if (this.rd_ws.Checked)
      {
        try
        {
          CommonSessionStartResult _results;
          
          _results = _service.WS2S_StartCardSession(_vendor_id, _serial_number, _machine_number, _vendor_session_id, _track_data);

          if (_results == null) txt_ws_log.Text += "Session Start: Unabled to read status result\n";
          else
          {
            txt_ws_log.Text += "Session Start: " + _results.StatusText + "\n";
            txt_ws_log.Text += "Session Id: " + _results.SessionId.ToString() + "\n";
          }
        }
        catch (Exception ex)
        {
          txt_ws_log.Text += ex.Message + "\n";
        }
      }
      else
      {
        SessionStartResult _results = null;
        int _bd_selector;
        _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;

        _results = this.SP_StartSession(_vendor_id, _serial_number, _machine_number, _vendor_session_id, _track_data, _bd_selector);

        if (_results == null) txt_ws_log.Text += "Session Start: Unabled to read status result\n";
        else
        {
          txt_ws_log.Text += "Session Start: " + _results.StatusText + "\n";
          if (_results.SessionId != null) txt_ws_log.Text += "Session Id: " + _results.SessionId + "\n";
        }
      }
    }

    private void ExecuteSessionUpdate()
    {

      if (rd_ws.Checked)
      {
        try
        {
          if (_service != null)
          {
            CommonResult _results;


            _results = _service.WS2S_UpdateCardSession(txt_session_update_vendor_id.Text, Convert.ToInt64(Convert.ToDouble(txt_session_update_session_id.Text)), Convert.ToDecimal(txt_session_update_credits_played.Text),
            Convert.ToDecimal(this.txt_session_update_credits_won.Text), Convert.ToInt32(this.txt_session_update_games_played.Text), Convert.ToInt32(this.txt_session_update_games_won.Text),
            Convert.ToDecimal(this.txt_session_update_bill_in.Text), Convert.ToDecimal(this.txt_session_update_credits_balance.Text), 0, 0, 0);

            if (_results == null) txt_ws_log.Text += "Update Session: Unabled to read status result\n";
            else txt_ws_log.Text += "Update Session: " + _results.StatusText + "\n";
          }
          else
          {
            txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
          }

        }
        catch (Exception ex)
        {
          txt_ws_log.Text = txt_ws_log.Text + "\n" + ex.Message;
        }
      }
      else
      {
        // Set data and execute 'SessionUpdate' SP

        int _bd_selector;
        _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;

        this.ExecuteSessionSP(Procedure.SessionUpdate
                             , this.txt_session_update_vendor_id.Text
                             , this.txt_session_update_session_id.Text
                             , this.txt_session_update_credits_played.Text
                             , this.txt_session_update_credits_won.Text
                             , this.txt_session_update_games_played.Text
                             , this.txt_session_update_games_won.Text
                             , this.txt_session_update_bill_in.Text
                             , this.txt_session_update_credits_balance.Text
                             , _bd_selector
                             , this.txt_session_start_account_id.Text
                             , this.txt_session_start_machine_number.Text
                             , this.txt_session_start_serial_number.Text);
      }

    }

    private void ExecuteSessionEnd()
    {

      if (this.rd_ws.Checked)
      {
        try
        {
          if (_service != null)
          {
            CommonResult _results;


            _results = _service.WS2S_EndCardSession(this.txt_session_end_vendor_id.Text, Convert.ToInt64(this.txt_session_end_session_id.Text), Convert.ToDecimal(this.txt_session_end_credits_played.Text), Convert.ToDecimal(this.txt_session_end_credits_won.Text),
               Convert.ToInt32(this.txt_session_end_games_played.Text), Convert.ToInt32(this.txt_session_end_games_won.Text), Convert.ToDecimal(this.txt_session_end_bill_in.Text), Convert.ToDecimal(this.txt_session_end_credits_balance.Text), 0, 0, 0);

            if (_results == null) txt_ws_log.Text += "End Session: Unabled to read status result\n";
            else
            {
              txt_ws_log.Text += "End Session: " + _results.StatusText + "\n";
              //if (_results.StatusCode == 0 || _results.StatusCode == 5)
              //{
              //  _service.Close();
              //  _service = null;
              //  txt_ws_log.Text += "Session closed.\n";
              //}
            }
          }
          else
          {
            txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
          }
        }
        catch (Exception ex)
        {
          txt_ws_log.Text += ex.Message + "\n";
        }
      }
      else
      {
        // Set data and execute 'SessionEnd' SP
        int _bd_selector;
        _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;

        this.ExecuteSessionSP(Procedure.SessionEnd
                             , this.txt_session_end_vendor_id.Text
                             , this.txt_session_end_session_id.Text
                             , this.txt_session_end_credits_played.Text
                             , this.txt_session_end_credits_won.Text
                             , this.txt_session_end_games_played.Text
                             , this.txt_session_end_games_won.Text
                             , this.txt_session_end_bill_in.Text
                             , this.txt_session_end_credits_balance.Text
                             , _bd_selector
                             , this.txt_session_start_account_id.Text
                             , this.txt_session_start_machine_number.Text
                             , this.txt_session_start_serial_number.Text);
      }
    }

    private void ExecuteAccountStatus()
    {
      string _vendor_id;
      string _track_data;

      _vendor_id = this.txt_account_status_vendor_id.Text;
      _track_data = this.txt_account_status_account_id.Text;

      // VendorId
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'VendorID' no tiene un formato correcto");
        return;
      }

      // TrackData
      if (String.IsNullOrEmpty(_track_data))
      {
        MessageBox.Show("El valor de 'TrackData' no tiene un formato correcto");
        return;
      }

      if (rd_ws.Checked)
      {
        try
        {
          if (_service != null)
          {
            CommonAccountResult _results;

            _results = _service.WS2S_AccountStatus(_vendor_id, _track_data);

            if (_results == null) txt_ws_log.Text += "Account Status: Unabled to read status result\n";
            else txt_ws_log.Text += "Account Status: " + _results.StatusText + "\n"
                                 + " Balance: " + _results.Balance + "\n"
                                 + " VID: " + _results.VID;
          }
          else
          {
            txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
          }
        }
        catch (Exception ex)
        {
          txt_ws_log.Text += ex.Message + "\n";
        }
      }
      else
      {
        AccountStatusResult _results = null;
        int _bd_selector;
        _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;

        _results = this.SP_AccountGetInfo(_vendor_id, _track_data, _bd_selector);

        if (_results == null) txt_ws_log.Text += "Account Status: Unabled to read status result\n";
        else txt_ws_log.Text += "Account Status: " + _results.StatusText + "\n";
      }

    }

    private void ExecuteReportEvent()
    {
      string _vendor_id;
      string _serial_number;
      int _machine_number;
      int _event_id;
      decimal _amount;
      long _session_id;

      _vendor_id = this.txt_report_event_vendor_id.Text;
      _serial_number = this.txt_report_event_serial_number.Text;

      // VendorID
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'VendorID' no tiene un formato correcto");
        return;
      }

      // SerialNumber
      if (String.IsNullOrEmpty(_serial_number))
      {
        MessageBox.Show("El valor de 'Serial Number' no tiene un formato correcto");
        return;
      }

      // MachineNumber
      if (!Int32.TryParse(this.txt_report_event_machine_number.Text, out _machine_number))
      {
        MessageBox.Show("El valor de 'Machine Number' no tiene un formato correcto");
        return;
      }

      // EventId
      if (!Int32.TryParse(this.txt_report_event_event_id.Text, out _event_id))
      {
        MessageBox.Show("El valor de 'EventId' no tiene un formato correcto");
        return;
      }

      // PayoutAmount
      if (!Decimal.TryParse(this.txt_report_event_payout_amount.Text, out _amount))
      {
        MessageBox.Show("El valor de 'Payout Amount' no tiene un formato correcto");
        return;
      }

      // SessionId
      if (!Int64.TryParse(this.txt_report_event_session_id.Text, out _session_id))
      {
        MessageBox.Show("El valor de 'SessionID' no tiene un formato correcto");
        return;
      }

      if (rd_ws.Checked)
      {
        try
        {
          if (_service != null)
          {
            CommonResult _results;

            //CheckWSStatus();

            _results = _service.WS2S_ReportEvent(_vendor_id, _serial_number, _machine_number, _event_id, _amount, _session_id);
            if (_results == null) txt_ws_log.Text += "Report Event: Unabled to read status result\n";
            else txt_ws_log.Text += "Report Event: " + _results.StatusText + "\n";
          }
          else
          {
            txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
          }
        }
        catch (Exception ex)
        {
          txt_ws_log.Text += ex.Message + "\n";
        }
      }
      else
      {
        GenericResult _result = null;
        int _bd_selector;
        _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;

        _result = this.SP_ReportEvent(_vendor_id, _serial_number, _machine_number, _event_id, _amount, _session_id, _bd_selector);

        if (_result == null) txt_ws_log.Text += "Report Event: Unabled to read status result\n";
        else txt_ws_log.Text += "Report Event: " + _result.StatusText + "\n";
      }

    }

    private void ExecuteAccountInfo()
    {
      string _track_data, _vendor_id;

      _track_data = this.pnl_info_txt_track_data.Text;

      _vendor_id = this.pnl_info_txt_vendor_id.Text;

      // VendorID
      if (String.IsNullOrEmpty(_track_data))
      {
        MessageBox.Show("El valor de 'trackdata' no tiene un formato correcto");
        return;
      }
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'vendor' no tiene un formato correcto");
        return;
      }
      if (rd_ws.Checked)
      {
        if (_service != null)
        {
          CommonAccountDetailsResult _results;
          try
          {
            _results = _service.WS2S_AccountInfo(_vendor_id, _track_data);
            if (_results == null) txt_ws_log.Text += "Account Info: Unabled to read status result\n";
            else
            {
              if (_results.StatusCode == 0 && _results.StatusText.ToUpper() != "OK")
              {
                txt_ws_log.Text = _results.StatusText;
              }
              else
              {
              txt_ws_log.Text += "AccountInfo: " + _results.StatusText + "\n\n";
              txt_ws_log.Text += "AccountNumber: " + _results.AccountNumber.ToString() + "\n";
              txt_ws_log.Text += "Name: " + _results.Name.ToString() + "\n";
              txt_ws_log.Text += "FirstSurname: " + _results.FirstSurname.ToString() + "\n";
              txt_ws_log.Text += "SecondSurname: " + _results.SecondSurname.ToString() + "\n";
              txt_ws_log.Text += "Gender: " + _results.Gender.ToString() + "\n";
              txt_ws_log.Text += "BirthDate: " + _results.BirthDate.ToString() + "\n";
              txt_ws_log.Text += "Email: " + _results.Email.ToString() + "\n";
              txt_ws_log.Text += "AuxEmail " + _results.AuxEmail.ToString() + "\n";
              txt_ws_log.Text += "MobileNumber: " + _results.MobileNumber.ToString() + "\n";
              txt_ws_log.Text += "FixedPhone: " + _results.FixedPhone.ToString() + "\n";
              txt_ws_log.Text += "ClientTypeCode: " + _results.ClientTypeCode.ToString() + "\n";
              txt_ws_log.Text += "ClientType: " + _results.ClientType.ToString() + "\n";
              txt_ws_log.Text += "FiscalCode: " + _results.FiscalCode.ToString() + "\n";
              txt_ws_log.Text += "Nationality: " + _results.Nationality.ToString() + "\n";
              txt_ws_log.Text += "Street: " + _results.Street.ToString() + "\n";
              txt_ws_log.Text += "HouseNumber: " + _results.HouseNumber.ToString() + "\n";
              txt_ws_log.Text += "Colony: " + _results.Colony.ToString() + "\n";
              txt_ws_log.Text += "State: " + _results.State.ToString() + "\n";
              txt_ws_log.Text += "CityCode: " + _results.CityCode.ToString() + "\n";
              txt_ws_log.Text += "City: " + _results.City.ToString() + "\n";
              txt_ws_log.Text += "DocumentTypeCode: " + _results.DocumentTypeCode.ToString() + "\n";
              txt_ws_log.Text += "DocumentType: " + _results.DocumentType.ToString() + "\n";
              txt_ws_log.Text += "Document2Type: " + _results.Document2Type.ToString() + "\n";
              txt_ws_log.Text += "Document3Type: " + _results.Document3Type.ToString() + "\n";
              txt_ws_log.Text += "DocumentNumber: " + _results.DocumentNumber.ToString() + "\n";
              txt_ws_log.Text += "Document2Number: " + _results.Document2Number.ToString() + "\n";
              txt_ws_log.Text += "Document3Number: " + _results.Document3Number.ToString() + "\n";
              txt_ws_log.Text += "LevelCode: " + _results.LevelCode.ToString() + "\n";
              txt_ws_log.Text += "Level: " + _results.Level.ToString() + "\n";
              txt_ws_log.Text += "Points: " + _results.Points.ToString() + "\n";
            }
          }
          }
          catch
          {
            txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
          }

        }
        else
        {
          txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
        }
      }
      else
      {
        GenericResult _result = null;
        XmlDocument _xdoc, _meters;
        string _listofmeters;
        _xdoc = new XmlDocument();
        _meters = new XmlDocument();

        try
        {
          int _bd_selector;
          _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;
          _result = this.SP_AccountInfo(_vendor_id, _track_data, _bd_selector);

          if (_result == null) txt_ws_log.Text += "Account Info: Unabled to read status result\n";
          else txt_ws_log.Text += "Account Info: " + _result.StatusText + "\n";
        }

        catch
        {
          txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
        }

      }
    }

    private void ExecuteCardInfo()
    {
      string _track_data;

      _track_data = this.pnl_card_info_track_data.Text;

      // TrackData
      if (String.IsNullOrEmpty(_track_data))
      {
        MessageBox.Show("El valor de 'trackdata' no tiene un formato correcto");
        return;
      }

      if (rd_ws.Checked)
      {
        if (_service != null)
        {
          //CommonCardResult _results;
          CommonResult _results;

          try
          {
            _results = null;
            _results = _service.WS2S_CardInfo(_track_data);
            if (_results == null) txt_ws_log.Text += "Card Info: Unabled to read status result\n";
            else txt_ws_log.Text += "CustomerId: " + ((CommonCardResult)_results).CustomerId + "\n"
              + "AccountType: " + ((CommonCardResult)_results).AccountType + "\n"
              + "CardIndex: " + ((CommonCardResult)_results).CardIndex + "\n"
              + "CardStatus: " + ((CommonCardResult)_results).CardStatus + "\n";
          }
          catch
          {
            txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
          }

        }
        else
        {
          txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
        }
      }
      else
      {
        GenericResult _result = null;
        XmlDocument _xdoc, _meters;
        string _listofmeters;
        _xdoc = new XmlDocument();
        _meters = new XmlDocument();

        try
        {
          int _bd_selector;
          _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;
          _result = this.SP_CardInfo(_track_data, _bd_selector);

          if (_result == null) txt_ws_log.Text += "Account Info: Unabled to read status result\n";
          else txt_ws_log.Text += "Account Info: " + _result.StatusText + "\n";
        }

        catch
        {
          txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
        }

      }
    }

    private void ExecuteReportMeters()
    {
      string _vendor_id;
      string _serial_number;
      int _machine_number;

      _vendor_id = this.txt_meters_vendorID.Text;
      _serial_number = this.txt_meters_serialnumber.Text;

      // VendorID
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'VendorID' no tiene un formato correcto");
        return;
      }

      // SerialNumber
      if (String.IsNullOrEmpty(_serial_number))
      {
        MessageBox.Show("El valor de 'Serial Number' no tiene un formato correcto");
        return;
      }

      // MachineNumber
      if (!Int32.TryParse(this.txt_meters_machine_number.Text, out _machine_number))
      {
        MessageBox.Show("El valor de 'Machine Number' no tiene un formato correcto");
        return;
      }

      if (rd_ws.Checked)
      {
        if (_service != null)
        {
          Meters _meters;
          _meters = new Meters();
          CommonResult _results;

          XmlDocument _xdoc = new XmlDocument();
          try
          {
            _xdoc.LoadXml(this.txt_meters_xml.Text);
            foreach (XmlNode item in _xdoc.DocumentElement.ChildNodes)
            {
              Meter _meter;
              _meter = new Meter();
              _meter.Code = Convert.ToInt16(item.Attributes.GetNamedItem("Code").Value);
              _meter.Value = Convert.ToInt32(item.Attributes.GetNamedItem("Value").Value);
              _meters.Add(_meter);
            }

            //CheckWSStatus();

            _results = _service.WS2S_ReportMeters(_vendor_id, _serial_number, _machine_number, _meters);
            if (_results == null) txt_ws_log.Text += "Report Event: Unabled to read status result\n";
            else txt_ws_log.Text += "Report Meters: " + _results.StatusText + "\n";
          }
          catch
          {
            txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
          }

        }
        else
        {
          txt_ws_log.Text = txt_ws_log.Text + "Sesi�n cerrada. Primero hay que hacer un Start Session\n";
        }
      }
      else
      {
        GenericResult _result = null;
        XmlDocument _xdoc, _meters;
        string _listofmeters;
        _xdoc = new XmlDocument();
        _meters = new XmlDocument();

        try
        {
          _xdoc.LoadXml(this.txt_meters_xml.Text);
          _listofmeters = null;

          foreach (XmlNode item in _xdoc.DocumentElement.ChildNodes)
          {
            _listofmeters += item.OuterXml;
          }

          int _bd_selector;
          _bd_selector = (this.rd_zsp.Checked == true) ? 0 : 2;
          _meters.LoadXml(string.Format("<root>{0}</root>", _listofmeters));
          _result = this.SP_ReportMeters(_vendor_id, _serial_number, _machine_number, _meters, _bd_selector);

          if (_result == null) txt_ws_log.Text += "Report Meters: Unabled to read status result\n";
          else txt_ws_log.Text += "Meters Meters: " + _result.StatusText + "\n";
        }

        catch
        {
          txt_ws_log.Text = txt_ws_log.Text + "El XML no es correcto. Por favor revisadlo y volver a probar.\n";
        }

      }
    }

    private void ExecuteSessionSP(Procedure ProcedureId
                                  , string VendorID
                                  , string SessionID
                                  , string AmountPlayed
                                  , string AmountWon
                                  , string GamesPlayed
                                  , string GamesWon
                                  , string BillIn
                                  , string CurrentBalance
                                  , int Zsp_S2S
                                  , string TrackData
                                  , string MachineNumber
                                  , string SerialNumber)
    {
      string _vendor_id;
      long _session_id;
      decimal _amount_played;
      decimal _amount_won;
      int _games_played;
      int _games_won;
      decimal _bill_in;
      decimal _current_balance;
      GenericResult _result;
      string _TrackData;
      int _machineNumber;
      string _serialnumber;

      // VendorID
      _vendor_id = VendorID;
      if (String.IsNullOrEmpty(_vendor_id))
      {
        MessageBox.Show("El valor de 'VendorID' no tiene un formato correcto");
        return;
      }

      // SessionID
      if (!Int64.TryParse(SessionID, out _session_id))
      {
        MessageBox.Show("El valor de 'SessionID' no tiene un formato correcto");
        return;
      }

      // AmountPlayed
      if (!Decimal.TryParse(AmountPlayed, out _amount_played))
      {
        MessageBox.Show("El valor de 'Amount Played' no tiene un formato correcto");
        return;
      }

      // AmountWon
      if (!Decimal.TryParse(AmountWon, out _amount_won))
      {
        MessageBox.Show("El valor de 'Amount Won' no tiene un formato correcto");
        return;
      }

      // GamesPlayed
      if (!Int32.TryParse(GamesPlayed, out _games_played))
      {
        MessageBox.Show("El valor de 'Games Played' no tiene un formato correcto");
        return;
      }

      // GamesWon
      if (!Int32.TryParse(GamesWon, out _games_won))
      {
        MessageBox.Show("El valor de 'Games Won' no tiene un formato correcto");
        return;
      }

      // BillIn
      if (!Decimal.TryParse(BillIn, out _bill_in))
      {
        MessageBox.Show("El valor de 'Bill In' no tiene un formato correcto");
        return;
      }

      // CurrentBalance
      if (!Decimal.TryParse(CurrentBalance, out _current_balance))
      {
        MessageBox.Show("El valor de 'Credits Balance' no tiene un formato correcto");
        return;
      }
      _TrackData = TrackData;
      if (MachineNumber == "" || MachineNumber == null) MachineNumber = "0";
      _machineNumber = Convert.ToInt16(MachineNumber);
      _serialnumber = SerialNumber;

      // Call SP
      _result = this.SP_Session(ProcedureId, _vendor_id, _session_id, _amount_played, _amount_won, _games_played, _games_won, _bill_in, _current_balance, Zsp_S2S, _TrackData, _machineNumber, _serialnumber);

      string _operation = null;
      _operation = (ProcedureId == Procedure.SessionEnd) ? "End " : "Update";
      if (_result == null) txt_ws_log.Text += "Session " + _operation + ": Unabled to read status result\n";
      else txt_ws_log.Text += _operation + "Session: " + _result.StatusText + "\n";
    }

    #region " TODO DELETE: SIMULAMOS LO QUE HARA EL WS Y DEBEMOS LLAMAR AL WS. "

    #region " Constants "

    private const int SP_RESPONSE_STATUS = 0;
    private const int SP_RESPONSE_TEXT = 1;
    private const int SP_RESPONSE_PARAM3 = 2;
    private const int SP_RESPONSE_PARAM4 = 3;

    #endregion

    #region " Stored Procedure Results Types "

    public class GenericResult
    {
      public int StatusCode { get; set; }
      public string StatusText { get; set; }
      public bool IsError { get; set; }
      public string LogMessage { get; set; }
    }

    public class SessionStartResult : GenericResult
    {
      public long SessionId { get; set; }
      public decimal Balance { get; set; }
    }

    public class AccountStatusResult : GenericResult
    {
      public decimal Balance { get; set; }
      public string VID { get; set; }
    }

    #endregion


    #region " Stored Procedure Calls "

    /// <summary>
    /// Stored procedure 'StartSession'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="VendorSessionId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SessionStartResult</returns>
    private SessionStartResult SP_StartSession(string VendorId, string SerialNumber, int MachineNumber, long VendorSessionId, string TrackData, int _zsp_s2s)
    {
      SqlConnection _sql_conn = new SqlConnection();
      SqlCommand _cmd;

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPStartSessionCommand(VendorId, SerialNumber, MachineNumber, VendorSessionId, TrackData, _zsp_s2s);
      _cmd.Connection = _sql_conn;

      // Execute command
      return this.ExecuteSessionStartSP(Procedure.SessionStart, _cmd);
    }

    /// <summary>
    /// Stored procedure 'UpdateSession' or 'EndSession'
    /// </summary>
    /// <param name="CmdType"></param>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="BillIn"></param>
    /// <param name="CurrentBalance"></param>
    /// <returns>GenericResult</returns>
    private GenericResult SP_Session(Procedure ProcedureID
                                    , string VendorId
                                    , long SessionId
                                    , decimal AmountPlayed
                                    , decimal AmountWon
                                    , int GamesPlayed
                                    , int GamesWon
                                    , decimal BillIn
                                    , decimal CurrentBalance
                                    , int Zsp_S2S
                                    , string TrackData
                                    , int MachineNumber
                                    , string SerialNumber)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn = new SqlConnection();

      if (!this.CheckBillIn(AmountPlayed, AmountWon, BillIn))
      {
        return this.IncorrectBillInGenericResult();
      }

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPSessionCommand(ProcedureID, VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon, CurrentBalance, BillIn, Zsp_S2S, TrackData, MachineNumber, SerialNumber);
      _cmd.Connection = _sql_conn;

      // Execute command
      return this.ExecuteSP(ProcedureID, _cmd);
    }

    /// <summary>
    /// Stored procedure 'UpdateSession' or 'EndSession'
    /// </summary>
    /// <param name="CmdType"></param>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="BillIn"></param>
    /// <param name="CurrentBalance"></param>
    /// <returns>GenericResult</returns>
    private GenericResult SP_Session(Procedure ProcedureID
                                    , string VendorId
                                    , long SessionId
                                    , decimal AmountPlayed
                                    , decimal AmountWon
                                    , int GamesPlayed
                                    , int GamesWon
                                    , decimal BillIn
                                    , decimal CurrentBalance
                                    , int Zsp_S2S
                                    , string TrackData
                                    , string SerialNumber
                                    , int MachineNumber)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn = new SqlConnection();

      if (!this.CheckBillIn(AmountPlayed, AmountWon, BillIn))
      {
        return this.IncorrectBillInGenericResult();
      }

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPSessionCommand(ProcedureID, VendorId, SessionId, AmountPlayed, AmountWon, GamesPlayed, GamesWon, CurrentBalance, BillIn, Zsp_S2S, TrackData, MachineNumber, SerialNumber);
      _cmd.Connection = _sql_conn;

      // Execute command
      return this.ExecuteSP(ProcedureID, _cmd);
    }

    /// <summary>
    /// Stored procedure 'AccountGetInfo'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="TrackData"></param>
    /// <returns>AccountStatusResult</returns>
    private AccountStatusResult SP_AccountGetInfo(string VendorId, string TrackData, int Zsp_S2S)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn;

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPAccountGetInfo(VendorId, TrackData, Zsp_S2S);
      _cmd.Connection = _sql_conn;

      return this.ExecuteAccountStatusSP(Procedure.AccountStatus, _cmd);
    }

    /// <summary>
    /// Stored procedure 'AccountGetInfo'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="TrackData"></param>
    /// <returns>AccountStatusResult</returns>
    private AccountStatusResult SP_AccountInfo(string VendorId, string TrackData, int Zsp_S2S)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn;

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPAccountInfo(VendorId, TrackData, Zsp_S2S);
      _cmd.Connection = _sql_conn;

      return this.ExecuteAccountStatusSP(Procedure.AccountStatus, _cmd);
    }
    private AccountStatusResult SP_CardInfo(string TrackData, int Zsp_S2S)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn;

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPCardGetInfo(TrackData, Zsp_S2S);
      _cmd.Connection = _sql_conn;

      return this.ExecuteCardIntoSP(Procedure.AccountStatus, _cmd);
    }

    /// <summary>
    /// Stored procedure 'ReportEvent'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="EventId"></param>
    /// <param name="Amount"></param>
    /// <param name="SessionId"></param>
    /// <returns>GenericResult</returns>
    private GenericResult SP_ReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount, long SessionId, int Zsp_S2S)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn = new SqlConnection();

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPReportEvent(VendorId, SerialNumber, MachineNumber, EventId, Amount, SessionId, Zsp_S2S);
      _cmd.Connection = _sql_conn;

      // Execute command
      return this.ExecuteSP(Procedure.SendEvent, EventId, _cmd);
    }

    private GenericResult SP_ReportMeters(string VendorId, string SerialNumber, int MachineNumber, XmlDocument Meters, int Zsp_S2S)
    {
      SqlCommand _cmd;
      SqlConnection _sql_conn = new SqlConnection();

      // Get sql connection
      _sql_conn = ConnectDB();

      // Get sql command
      _cmd = this.GetSPReportMeters(VendorId, SerialNumber, MachineNumber, Meters, Zsp_S2S);
      _cmd.Connection = _sql_conn;

      // Execute command
      return this.ExecuteSP(Procedure.SendEvent, _cmd);
    }

    #endregion

    #region " Stored Procedure Commands "

    /// <summary>
    /// Stored procedure command of 'StartSession'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="VendorSessionId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SqlCommand</returns>
    private SqlCommand GetSPStartSessionCommand(string VendorId, string SerialNumber, int MachineNumber, long VendorSessionId, string TrackData, int _zsp_s2s)
    {
      SqlCommand _sql_command;

      if (_zsp_s2s == 0)
      {
        // Set Sql command
        _sql_command = new SqlCommand();
        _sql_command.CommandText = "zsp_SessionStart";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@AccountID", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@VendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = SerialNumber;
        _sql_command.Parameters.Add("@MachineNumber", SqlDbType.Int).Value = MachineNumber;
        _sql_command.Parameters.Add("@VendorSessionId", SqlDbType.BigInt).Value = VendorSessionId;

        // Unused required params
        _sql_command.Parameters.Add("@CurrentJackpot", SqlDbType.Money).Value = DBNull.Value;
        //_sql_command.Parameters.Add("@pGameTitle", SqlDbType.VarChar).Value = DBNull.Value;
        //_sql_command.Parameters.Add("@pDummy", SqlDbType.Int).Value = DBNull.Value;
      }
      else
      {
        // Set Sql command
        _sql_command = new SqlCommand();
        _sql_command.CommandText = "S2S_SessionStart";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@pTrackData", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@pVendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@pSerialNumber", SqlDbType.VarChar).Value = SerialNumber;
        _sql_command.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;
        _sql_command.Parameters.Add("@pVendorSessionId", SqlDbType.BigInt).Value = VendorSessionId;
      }



      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'UpdateSession' or 'EndSession'
    /// </summary>
    /// <param name="ProcedureID"></param>
    /// <param name="VendorId"></param>
    /// <param name="SessionId"></param>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="GamesPlayed"></param>
    /// <param name="GamesWon"></param>
    /// <param name="CreditBalance"></param>
    /// <returns>SqlCommand</returns>
    private SqlCommand GetSPSessionCommand(Procedure ProcedureID
                                          , string VendorId
                                          , long SessionId
                                          , decimal AmountPlayed
                                          , decimal AmountWon
                                          , int GamesPlayed
                                          , int GamesWon
                                          , decimal CreditBalance
                                          , decimal Billin
                                          , int Zsp_S2S
                                          , string TrackData
                                          , int MachineNumber
                                          , string SerialNumber)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      if (Zsp_S2S == 0)
      {
        // Set Sql command
        _sql_command.CommandText = (ProcedureID == Procedure.SessionEnd) ? "zsp_SessionEnd" : "zsp_SessionUpdate";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@VendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@SessionId", SqlDbType.BigInt).Value = SessionId;
        _sql_command.Parameters.Add("@AmountPlayed", SqlDbType.Money).Value = AmountPlayed;
        _sql_command.Parameters.Add("@AmountWon", SqlDbType.Money).Value = AmountWon;
        _sql_command.Parameters.Add("@GamesPlayed", SqlDbType.Int).Value = GamesPlayed;
        _sql_command.Parameters.Add("@GamesWon", SqlDbType.Int).Value = GamesWon;
        _sql_command.Parameters.Add("@CreditBalance", SqlDbType.Money).Value = CreditBalance;
        _sql_command.Parameters.Add("@BillIn", SqlDbType.Money).Value = Billin;

        // Unused required params
        _sql_command.Parameters.Add("@AccountID", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = SerialNumber;
        _sql_command.Parameters.Add("@MachineNumber", SqlDbType.Int).Value = MachineNumber;
        _sql_command.Parameters.Add("@CurrentJackpot", SqlDbType.Money).Value = DBNull.Value;
      }
      else
      {
        // Set Sql command
        _sql_command.CommandText = (ProcedureID == Procedure.SessionEnd) ? "S2S_EndCardSession" : "S2S_UpdateCardSession";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@pVendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
        _sql_command.Parameters.Add("@pAmountPlayed", SqlDbType.Money).Value = AmountPlayed;
        _sql_command.Parameters.Add("@pAmountWon", SqlDbType.Money).Value = AmountWon;
        _sql_command.Parameters.Add("@pGamesPlayed", SqlDbType.Int).Value = GamesPlayed;
        _sql_command.Parameters.Add("@pGamesWon", SqlDbType.Int).Value = GamesWon;
        _sql_command.Parameters.Add("@pCurrentBalance", SqlDbType.Money).Value = CreditBalance;
        _sql_command.Parameters.Add("@pBillIn", SqlDbType.Money).Value = Billin;
      }


      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'AccountGetInfo'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="TrackData"></param>
    /// <returns>SqlCommand</returns>
    private SqlCommand GetSPAccountGetInfo(string VendorId, string TrackData, int Zsp_S2S)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      if (Zsp_S2S == 0)
      {
        // Set Sql command
        _sql_command.CommandText = "zsp_AccountStatus";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@AccountID", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@VendorId", SqlDbType.VarChar).Value = VendorId;

        // Unused required params
        _sql_command.Parameters.Add("@MachineNumber", SqlDbType.Int).Value = DBNull.Value;
      }
      else
      {
        // Set Sql command
        _sql_command.CommandText = "S2S_AccountStatus";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@pTrackData", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@pVendorId", SqlDbType.VarChar).Value = VendorId;
      }

      return _sql_command;
    }

    private SqlCommand GetSPCardGetInfo(string TrackData, int Zsp_S2S)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      if (Zsp_S2S == 0)
      {
        return null;
      }
      
        // Set Sql command
        _sql_command.CommandText = "S2S_cardinfo";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@TrackData", SqlDbType.VarChar).Value = TrackData;
      

      return _sql_command;
    }

    private SqlCommand GetSPAccountInfo(string VendorId, string TrackData, int Zsp_S2S)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      if (Zsp_S2S == 0)
      {
        // Set Sql command
        _sql_command.CommandText = "zsp_AccountInfo";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@TrackData", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@VendorID", SqlDbType.VarChar).Value = VendorId;


      }
      else
      {
        // Set Sql command
        _sql_command.CommandText = "S2S_AccountInfo";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@TrackData", SqlDbType.VarChar).Value = TrackData;
        _sql_command.Parameters.Add("@VendorID", SqlDbType.VarChar).Value = VendorId;
      }

      return _sql_command;
    }

    /// <summary>
    /// Stored procedure command of 'SendEvent'
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="SerialNumber"></param>
    /// <param name="MachineNumber"></param>
    /// <param name="EventId"></param>
    /// <param name="Amount"></param>
    /// <param name="SessionId"></param>
    /// <returns>SqlCommand</returns>
    private SqlCommand GetSPReportEvent(string VendorId, string SerialNumber, int MachineNumber, int EventId, decimal Amount, long SessionId, int Zsp_S2S)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      if (Zsp_S2S == 0)
      {
        _sql_command.CommandText = "zsp_SendEvent";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@VendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = SerialNumber;
        _sql_command.Parameters.Add("@MachineNumber", SqlDbType.Int).Value = MachineNumber;
        _sql_command.Parameters.Add("@EventID", SqlDbType.BigInt).Value = EventId;
        _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = Amount;

        // Unused required params
        _sql_command.Parameters.Add("@AccountID", SqlDbType.VarChar).Value = DBNull.Value;

      }
      else
      {
        _sql_command.CommandText = "S2S_ReportEvent";
        _sql_command.CommandType = CommandType.StoredProcedure;

        // Add params
        _sql_command.Parameters.Add("@pVendorId", SqlDbType.VarChar).Value = VendorId;
        _sql_command.Parameters.Add("@pSerialNumber", SqlDbType.VarChar).Value = SerialNumber;
        _sql_command.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;
        _sql_command.Parameters.Add("@pEventID", SqlDbType.BigInt).Value = EventId;
        _sql_command.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
        _sql_command.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
      }


      return _sql_command;
    }

    private SqlCommand GetSPReportMeters(string VendorId, string SerialNumber, int MachineNumber, XmlDocument Meters, int Zsp_S2S)
    {
      SqlCommand _sql_command;
      _sql_command = new SqlCommand();

      _sql_command.CommandText = (Zsp_S2S == 0) ? "zsp_ReportMeters" : "S2S_ReportMeters";
      _sql_command.CommandType = CommandType.StoredProcedure;

      // Add params
      _sql_command.Parameters.Add("@pVendorId", SqlDbType.VarChar).Value = VendorId;
      _sql_command.Parameters.Add("@pSerialNumber", SqlDbType.VarChar).Value = SerialNumber;
      _sql_command.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;
      _sql_command.Parameters.Add("@pXmlMeters", SqlDbType.Xml).Value = Meters.InnerXml;
      return _sql_command;

    }

    #endregion

    #region " Stored Porcedure Executions "

    /// <summary>
    /// Execute procedure with generic result
    /// </summary>
    /// <param name="ProcedureID"></param>
    /// <param name="SqlCmd"></param>
    /// <returns>GenericResult</returns>
    private GenericResult ExecuteSP(Procedure ProcedureID, SqlCommand SqlCmd)
    {
      return this.ExecuteSP(ProcedureID, 0, SqlCmd);
    }

    /// <summary>
    /// Execute procedure with generic result
    /// </summary>
    /// <param name="ProcedureID"></param>
    /// <param name="EventId"></param>
    /// <param name="SqlCmd"></param>
    /// <returns>GenericResult</returns>
    private GenericResult ExecuteSP(Procedure ProcedureID, int EventId, SqlCommand SqlCmd)
    {
      SqlDataReader _dr;
      GenericResult _result = new GenericResult();

      try
      {
        _result.IsError = true;
        _result.StatusCode = (Int32)MultiPromos.EndSessionStatus.FatalError;
        _result.StatusText = "Access Denied";

        _dr = SqlCmd.ExecuteReader();

        if (_dr.Read())
        {
          //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
          _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
          _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);
          _result.IsError = (_result.StatusCode != 0);
        }
      }
      catch (Exception _ex)
      {
        _result.StatusText = "!!! " + _ex.Message;
        _result.IsError = true;
      }

      return _result;
    } // ExecuteSP

    /// <summary>
    /// Execute procedure
    /// </summary>
    /// <param name="ProcedureID"></param>
    /// <param name="SqlCmd"></param>
    /// <returns>SessionStartResult</returns>
    private SessionStartResult ExecuteSessionStartSP(Procedure ProcedureID, SqlCommand SqlCmd)
    {
      SqlDataReader _dr;
      SessionStartResult _result = new SessionStartResult();

      _result = new SessionStartResult();
      _result.StatusCode = -1; // StatusCode.Error;
      _result.StatusText = "ERROR";

      try
      {
        _dr = SqlCmd.ExecuteReader();
        if (_dr.Read())
        {
          //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
          _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
          _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

          //if (_result.StatusCode == StatusCode.Success)
          if (_result.StatusCode == 0 || _result.StatusCode == 2)
          {
            _result.SessionId = _dr.GetInt64(SP_RESPONSE_PARAM3);
            _result.Balance = _dr.GetDecimal(SP_RESPONSE_PARAM4);
          }
        }
      }
      catch (Exception _ex)
      {
        _result.StatusText = "!!! " + _ex.Message;
      }

      return _result;

      #region Codi anterior per eliminar
      //}


      ////using (DB_TRX _db_trx = new DB_TRX())
      ////{
      ////  _result = new SessionStartResult();
      ////  _result.StatusCode = -1; // StatusCode.Error;
      ////  _result.StatusText = "ERROR";

      ////  try
      ////  {
      ////    _dr = SqlCmd.ExecuteReader();
      ////    if (_dr.Read())
      ////    {
      ////      //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
      ////      _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
      ////      _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

      ////      //if (_result.StatusCode == StatusCode.Success)
      ////      if (_result.StatusCode == 0)
      ////      {
      ////        _result.SessionId = _dr.GetInt64  (SP_RESPONSE_PARAM3);
      ////        _result.Balance   = _dr.GetDecimal(SP_RESPONSE_PARAM4);
      ////      }
      ////    }
      ////  }
      ////  catch (Exception _ex)
      ////  {
      ////    _result.StatusText = "!!! " + _ex.Message;            
      ////  }

      ////  return _result;
      ////}
      #endregion

    }

    /// <summary>
    /// Execute procedure
    /// </summary>
    /// <param name="ProcedureID"></param>
    /// <param name="SqlCmd"></param>
    /// <returns>AccountStatusResult</returns>
    private AccountStatusResult ExecuteAccountStatusSP(Procedure ProcedureID, SqlCommand SqlCmd)
    {
      SqlDataReader _dr;
      AccountStatusResult _result = new AccountStatusResult();

      _result.StatusCode = -1; // StatusCode.Error;
      _result.StatusText = "ERROR";

      try
      {
        _dr = SqlCmd.ExecuteReader();
        if (_dr.Read())
        {
          //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
          _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
          _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

          //if (_result.StatusCode == StatusCode.Success)
          if (_result.StatusCode == 0)
          {
            _result.Balance = _dr.GetDecimal(SP_RESPONSE_PARAM3);
            _result.VID = _dr.GetString(SP_RESPONSE_PARAM4);
          }
        }
      }
      catch (Exception _ex)
      {
        _result.StatusText = "!!! " + _ex.Message;
      }

      return _result;

      #region codi anterior per eliminar
      //using (DB_TRX _db_trx = new DB_TRX())
      //{
      //  _result = new AccountStatusResult();
      //  _result.StatusCode = -1; // StatusCode.Error;
      //  _result.StatusText = "ERROR";

      //  try
      //  {
      //    _dr = SqlCmd.ExecuteReader();
      //    if (_dr.Read())
      //    {
      //      //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
      //      _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
      //      _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

      //      //if (_result.StatusCode == StatusCode.Success)
      //      if (_result.StatusCode == 0)
      //      {
      //        _result.Balance = _dr.GetDecimal(SP_RESPONSE_PARAM3);
      //        _result.VID     = _dr.GetString (SP_RESPONSE_PARAM4);
      //      }
      //    }
      //  }
      //  catch (Exception _ex)
      //  {
      //    _result.StatusText = "!!! " + _ex.Message;
      //  }

      //  return _result;
      //}
      #endregion
    }

    private AccountStatusResult ExecuteCardIntoSP(Procedure ProcedureID, SqlCommand SqlCmd)
    {
      SqlDataReader _dr;
      AccountStatusResult _result = new AccountStatusResult();

      _result.StatusCode = -1; // StatusCode.Error;
      _result.StatusText = "ERROR";

      try
      {
        _dr = SqlCmd.ExecuteReader();
        if (_dr.Read())
        {
          //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
          _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
          _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

          //if (_result.StatusCode == StatusCode.Success)
          if (_result.StatusCode == 0)
          {
            _result.Balance = _dr.GetDecimal(SP_RESPONSE_PARAM3);
            _result.VID = _dr.GetString(SP_RESPONSE_PARAM4);
          }
        }
      }
      catch (Exception _ex)
      {
        _result.StatusText = "!!! " + _ex.Message;
      }

      return _result;

      #region codi anterior per eliminar
      //using (DB_TRX _db_trx = new DB_TRX())
      //{
      //  _result = new AccountStatusResult();
      //  _result.StatusCode = -1; // StatusCode.Error;
      //  _result.StatusText = "ERROR";

      //  try
      //  {
      //    _dr = SqlCmd.ExecuteReader();
      //    if (_dr.Read())
      //    {
      //      //_result.StatusCode = this.GetStatusCode(ProcedureID, _dr.GetInt32(SP_RESPONSE_STATUS));
      //      _result.StatusCode = _dr.GetInt32(SP_RESPONSE_STATUS);
      //      _result.StatusText = _dr.GetString(SP_RESPONSE_TEXT);

      //      //if (_result.StatusCode == StatusCode.Success)
      //      if (_result.StatusCode == 0)
      //      {
      //        _result.Balance = _dr.GetDecimal(SP_RESPONSE_PARAM3);
      //        _result.VID     = _dr.GetString (SP_RESPONSE_PARAM4);
      //      }
      //    }
      //  }
      //  catch (Exception _ex)
      //  {
      //    _result.StatusText = "!!! " + _ex.Message;
      //  }

      //  return _result;
      //}
      #endregion
    }

    #endregion

    /// <summary>
    /// Check if BillIn is correct
    /// </summary>
    /// <param name="AmountPlayed"></param>
    /// <param name="AmountWon"></param>
    /// <param name="BillIn"></param>
    /// <returns>bool</returns>
    private bool CheckBillIn(decimal AmountPlayed, decimal AmountWon, decimal BillIn)
    {
      return true;
    }

    /// <summary>
    /// Result indicates incorrect result on check BillIn
    /// </summary>
    /// <returns>GenericResult</returns>
    private GenericResult IncorrectBillInGenericResult()
    {
      return new GenericResult() { StatusCode = -1, StatusText = "!!! Incorrect BillIn check" };
    }

    #endregion

    #endregion

    internal int SP_Selected
    {
      get { return this.m_sp_selected; }
      set { this.m_sp_selected = value; }
    }

    private void btn_clear_log_Click(object sender, EventArgs e)
    {
      txt_ws_log.Text = "";
    }

    private void btn_random_Click(object sender, EventArgs e)
    {

    }

    private void gbox_ReportMeters_Enter(object sender, EventArgs e)
    {

    }

    private void frm_simulator_Load(object sender, EventArgs e)
    {

    }
  }
}
