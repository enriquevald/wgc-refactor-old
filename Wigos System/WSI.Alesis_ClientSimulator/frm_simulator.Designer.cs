namespace WSI.Alesis_ClientSimulator
{
  partial class frm_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.txt_ip_address_2 = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.txt_db_pass = new System.Windows.Forms.TextBox();
      this.label15 = new System.Windows.Forms.Label();
      this.txt_db_user = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.txt_db_name = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.txt_vendor_id = new System.Windows.Forms.TextBox();
      this.txt_ip_address_1 = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.txt_encrypted_card_track_data = new System.Windows.Forms.TextBox();
      this.label17 = new System.Windows.Forms.Label();
      this.txt_card_track_data = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.cmb_serial_num = new System.Windows.Forms.ComboBox();
      this.txt_num_terminals = new System.Windows.Forms.TextBox();
      this.label18 = new System.Windows.Forms.Label();
      this.txt_machine_num = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.tmr_refresh = new System.Windows.Forms.Timer(this.components);
      this.m_btn_start = new System.Windows.Forms.Button();
      this.tmr_stats = new System.Windows.Forms.Timer(this.components);
      this.lbl_statistics_1 = new System.Windows.Forms.Label();
      this.lbl_statistics_5 = new System.Windows.Forms.Label();
      this.lbl_statistics_avg = new System.Windows.Forms.Label();
      this.lbl_head = new System.Windows.Forms.Label();
      this.lbl_sim_status = new System.Windows.Forms.Label();
      this.lbl_totals = new System.Windows.Forms.Label();
      this.lbl_head_totals = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.tab_simulator = new System.Windows.Forms.TabControl();
      this.tab_parameters = new System.Windows.Forms.TabPage();
      this.lbl_ws_url = new System.Windows.Forms.Label();
      this.txt_IPadress = new System.Windows.Forms.TextBox();
      this.label22 = new System.Windows.Forms.Label();
      this.txt_min_update_x_sec = new System.Windows.Forms.TextBox();
      this.label21 = new System.Windows.Forms.Label();
      this.txt_max_plays_x_update = new System.Windows.Forms.TextBox();
      this.btn_set_accounts = new System.Windows.Forms.Button();
      this.btn_set_terminals = new System.Windows.Forms.Button();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.chk_account_status = new System.Windows.Forms.CheckBox();
      this.chk_report_jackpot = new System.Windows.Forms.CheckBox();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.txt_max_delay_between_sessions = new System.Windows.Forms.TextBox();
      this.txt_min_delay_between_sessions = new System.Windows.Forms.TextBox();
      this.label20 = new System.Windows.Forms.Label();
      this.txt_max_delay_between_plays = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.txt_min_session_plays = new System.Windows.Forms.TextBox();
      this.txt_min_delay_between_plays = new System.Windows.Forms.TextBox();
      this.txt_max_session_plays = new System.Windows.Forms.TextBox();
      this.label19 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.tab_statistics = new System.Windows.Forms.TabPage();
      this.tab_ws = new System.Windows.Forms.TabPage();
      this.gbox_log = new System.Windows.Forms.GroupBox();
      this.txt_ws_log = new System.Windows.Forms.RichTextBox();
      this.gbox_ws = new System.Windows.Forms.GroupBox();
      this.pnl_start_session = new System.Windows.Forms.Panel();
      this.pnl_account_info = new System.Windows.Forms.Panel();
      this.groupBox7 = new System.Windows.Forms.GroupBox();
      this.label23 = new System.Windows.Forms.Label();
      this.pnl_info_txt_vendor_id = new System.Windows.Forms.TextBox();
      this.label30 = new System.Windows.Forms.Label();
      this.pnl_info_txt_track_data = new System.Windows.Forms.TextBox();
      this.gb_connection = new System.Windows.Forms.GroupBox();
      this.rd_ws = new System.Windows.Forms.RadioButton();
      this.rd_s2s = new System.Windows.Forms.RadioButton();
      this.rd_zsp = new System.Windows.Forms.RadioButton();
      this.btn_random = new System.Windows.Forms.Button();
      this.pnl_container = new System.Windows.Forms.Panel();
      this.pnl_Card_Info = new System.Windows.Forms.Panel();
      this.groupBox8 = new System.Windows.Forms.GroupBox();
      this.label25 = new System.Windows.Forms.Label();
      this.pnl_card_info_track_data = new System.Windows.Forms.TextBox();
      this.pnl_session_update = new System.Windows.Forms.Panel();
      this.gbox_session_update = new System.Windows.Forms.GroupBox();
      this.lbl_session_update_credits_balance = new System.Windows.Forms.Label();
      this.txt_session_update_credits_balance = new System.Windows.Forms.TextBox();
      this.lbl_session_update_bill_in = new System.Windows.Forms.Label();
      this.txt_session_update_bill_in = new System.Windows.Forms.TextBox();
      this.lbl_session_update_games_won = new System.Windows.Forms.Label();
      this.txt_session_update_games_won = new System.Windows.Forms.TextBox();
      this.lbl_session_update_games_played = new System.Windows.Forms.Label();
      this.txt_session_update_games_played = new System.Windows.Forms.TextBox();
      this.lbl_session_update_credits_won = new System.Windows.Forms.Label();
      this.txt_session_update_credits_won = new System.Windows.Forms.TextBox();
      this.lbl_session_update_credits_played = new System.Windows.Forms.Label();
      this.txt_session_update_credits_played = new System.Windows.Forms.TextBox();
      this.lbl_session_update_session_id = new System.Windows.Forms.Label();
      this.txt_session_update_session_id = new System.Windows.Forms.TextBox();
      this.lbl_session_update_vendor_id = new System.Windows.Forms.Label();
      this.txt_session_update_vendor_id = new System.Windows.Forms.TextBox();
      this.pnl_report_event = new System.Windows.Forms.Panel();
      this.gbox_report_event = new System.Windows.Forms.GroupBox();
      this.lbl_report_event_session_id = new System.Windows.Forms.Label();
      this.txt_report_event_session_id = new System.Windows.Forms.TextBox();
      this.lbl_report_event_payout_amount = new System.Windows.Forms.Label();
      this.txt_report_event_payout_amount = new System.Windows.Forms.TextBox();
      this.lbl_report_event_machine_number = new System.Windows.Forms.Label();
      this.txt_report_event_machine_number = new System.Windows.Forms.TextBox();
      this.lbl_report_event_serial_number = new System.Windows.Forms.Label();
      this.txt_report_event_serial_number = new System.Windows.Forms.TextBox();
      this.lbl_report_event_vendor_id = new System.Windows.Forms.Label();
      this.txt_report_event_vendor_id = new System.Windows.Forms.TextBox();
      this.lbl_report_event_event_id = new System.Windows.Forms.Label();
      this.txt_report_event_event_id = new System.Windows.Forms.TextBox();
      this.pnl_session_end = new System.Windows.Forms.Panel();
      this.gbox_session_end = new System.Windows.Forms.GroupBox();
      this.lbl_session_end_credits_balance = new System.Windows.Forms.Label();
      this.txt_session_end_credits_balance = new System.Windows.Forms.TextBox();
      this.lbl_session_end_bill_in = new System.Windows.Forms.Label();
      this.txt_session_end_bill_in = new System.Windows.Forms.TextBox();
      this.lbl_session_end_games_won = new System.Windows.Forms.Label();
      this.txt_session_end_games_won = new System.Windows.Forms.TextBox();
      this.lbl_session_end_games_played = new System.Windows.Forms.Label();
      this.txt_session_end_games_played = new System.Windows.Forms.TextBox();
      this.lbl_session_end_credits_won = new System.Windows.Forms.Label();
      this.txt_session_end_credits_won = new System.Windows.Forms.TextBox();
      this.lbl_session_end_credits_played = new System.Windows.Forms.Label();
      this.txt_session_end_credits_played = new System.Windows.Forms.TextBox();
      this.lbl_session_end_session_id = new System.Windows.Forms.Label();
      this.txt_session_end_session_id = new System.Windows.Forms.TextBox();
      this.lbl_session_end_vendor_id = new System.Windows.Forms.Label();
      this.txt_session_end_vendor_id = new System.Windows.Forms.TextBox();
      this.pnl_account_status = new System.Windows.Forms.Panel();
      this.gbox_account_status = new System.Windows.Forms.GroupBox();
      this.lbl_account_status_vendor_id = new System.Windows.Forms.Label();
      this.txt_account_status_vendor_id = new System.Windows.Forms.TextBox();
      this.lbl_account_status_account_id = new System.Windows.Forms.Label();
      this.txt_account_status_account_id = new System.Windows.Forms.TextBox();
      this.pnl_Report_Meters = new System.Windows.Forms.Panel();
      this.gbox_Meters = new System.Windows.Forms.GroupBox();
      this.lbl_meters_xml = new System.Windows.Forms.Label();
      this.txt_meters_xml = new System.Windows.Forms.TextBox();
      this.lbl_meters_machine_number = new System.Windows.Forms.Label();
      this.txt_meters_machine_number = new System.Windows.Forms.TextBox();
      this.lbl_meters_serialnumber = new System.Windows.Forms.Label();
      this.txt_meters_serialnumber = new System.Windows.Forms.TextBox();
      this.lbl_meters_vendorid = new System.Windows.Forms.Label();
      this.txt_meters_vendorID = new System.Windows.Forms.TextBox();
      this.cmb_sp = new System.Windows.Forms.ComboBox();
      this.lbl_sp = new System.Windows.Forms.Label();
      this.btn_send = new System.Windows.Forms.Button();
      this.btn_clear_log = new System.Windows.Forms.Button();
      this.gbox_session_start = new System.Windows.Forms.GroupBox();
      this.lbl_session_start_vendor_session_id = new System.Windows.Forms.Label();
      this.txt_session_start_vendor_session_id = new System.Windows.Forms.TextBox();
      this.lbl_session_start_machine_number = new System.Windows.Forms.Label();
      this.txt_session_start_machine_number = new System.Windows.Forms.TextBox();
      this.lbl_session_start_serial_number = new System.Windows.Forms.Label();
      this.txt_session_start_serial_number = new System.Windows.Forms.TextBox();
      this.lbl_session_start_vendor_id = new System.Windows.Forms.Label();
      this.txt_session_start_vendor_id = new System.Windows.Forms.TextBox();
      this.lbl_session_start_account_id = new System.Windows.Forms.Label();
      this.txt_session_start_account_id = new System.Windows.Forms.TextBox();
      this.groupBox5.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.tab_simulator.SuspendLayout();
      this.tab_parameters.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.tab_statistics.SuspendLayout();
      this.tab_ws.SuspendLayout();
      this.gbox_log.SuspendLayout();
      this.gbox_ws.SuspendLayout();
      this.pnl_start_session.SuspendLayout();
      this.pnl_account_info.SuspendLayout();
      this.groupBox7.SuspendLayout();
      this.gb_connection.SuspendLayout();
      this.pnl_container.SuspendLayout();
      this.pnl_Card_Info.SuspendLayout();
      this.groupBox8.SuspendLayout();
      this.pnl_session_update.SuspendLayout();
      this.gbox_session_update.SuspendLayout();
      this.pnl_report_event.SuspendLayout();
      this.gbox_report_event.SuspendLayout();
      this.pnl_session_end.SuspendLayout();
      this.gbox_session_end.SuspendLayout();
      this.pnl_account_status.SuspendLayout();
      this.gbox_account_status.SuspendLayout();
      this.pnl_Report_Meters.SuspendLayout();
      this.gbox_Meters.SuspendLayout();
      this.gbox_session_start.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.txt_ip_address_2);
      this.groupBox5.Controls.Add(this.label16);
      this.groupBox5.Controls.Add(this.txt_db_pass);
      this.groupBox5.Controls.Add(this.label15);
      this.groupBox5.Controls.Add(this.txt_db_user);
      this.groupBox5.Controls.Add(this.label2);
      this.groupBox5.Controls.Add(this.txt_db_name);
      this.groupBox5.Controls.Add(this.label13);
      this.groupBox5.Controls.Add(this.txt_vendor_id);
      this.groupBox5.Controls.Add(this.txt_ip_address_1);
      this.groupBox5.Controls.Add(this.label12);
      this.groupBox5.Controls.Add(this.label14);
      this.groupBox5.Location = new System.Drawing.Point(6, 6);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(248, 196);
      this.groupBox5.TabIndex = 15;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Conection";
      // 
      // txt_ip_address_2
      // 
      this.txt_ip_address_2.Location = new System.Drawing.Point(89, 76);
      this.txt_ip_address_2.Name = "txt_ip_address_2";
      this.txt_ip_address_2.Size = new System.Drawing.Size(153, 20);
      this.txt_ip_address_2.TabIndex = 102;
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(10, 79);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(67, 13);
      this.label16.TabIndex = 22;
      this.label16.Text = "IP Address 2";
      // 
      // txt_db_pass
      // 
      this.txt_db_pass.Location = new System.Drawing.Point(89, 162);
      this.txt_db_pass.Name = "txt_db_pass";
      this.txt_db_pass.Size = new System.Drawing.Size(153, 20);
      this.txt_db_pass.TabIndex = 105;
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(10, 165);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(53, 13);
      this.label15.TabIndex = 20;
      this.label15.Text = "Password";
      // 
      // txt_db_user
      // 
      this.txt_db_user.Location = new System.Drawing.Point(89, 133);
      this.txt_db_user.Name = "txt_db_user";
      this.txt_db_user.Size = new System.Drawing.Size(153, 20);
      this.txt_db_user.TabIndex = 104;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(10, 136);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(29, 13);
      this.label2.TabIndex = 18;
      this.label2.Text = "User";
      // 
      // txt_db_name
      // 
      this.txt_db_name.Location = new System.Drawing.Point(89, 104);
      this.txt_db_name.Name = "txt_db_name";
      this.txt_db_name.Size = new System.Drawing.Size(153, 20);
      this.txt_db_name.TabIndex = 103;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(10, 107);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(53, 13);
      this.label13.TabIndex = 16;
      this.label13.Text = "DB Name";
      // 
      // txt_vendor_id
      // 
      this.txt_vendor_id.Location = new System.Drawing.Point(89, 18);
      this.txt_vendor_id.Name = "txt_vendor_id";
      this.txt_vendor_id.Size = new System.Drawing.Size(153, 20);
      this.txt_vendor_id.TabIndex = 100;
      // 
      // txt_ip_address_1
      // 
      this.txt_ip_address_1.Location = new System.Drawing.Point(89, 46);
      this.txt_ip_address_1.Name = "txt_ip_address_1";
      this.txt_ip_address_1.Size = new System.Drawing.Size(153, 20);
      this.txt_ip_address_1.TabIndex = 101;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(10, 21);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(55, 13);
      this.label12.TabIndex = 9;
      this.label12.Text = "Vendor ID";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(10, 49);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(67, 13);
      this.label14.TabIndex = 13;
      this.label14.Text = "IP Address 1";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.txt_encrypted_card_track_data);
      this.groupBox2.Controls.Add(this.label17);
      this.groupBox2.Controls.Add(this.txt_card_track_data);
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Location = new System.Drawing.Point(260, 125);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(242, 74);
      this.groupBox2.TabIndex = 20;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Trackdata";
      // 
      // txt_encrypted_card_track_data
      // 
      this.txt_encrypted_card_track_data.Location = new System.Drawing.Point(92, 43);
      this.txt_encrypted_card_track_data.MaxLength = 20;
      this.txt_encrypted_card_track_data.Name = "txt_encrypted_card_track_data";
      this.txt_encrypted_card_track_data.Size = new System.Drawing.Size(144, 20);
      this.txt_encrypted_card_track_data.TabIndex = 110;
      this.txt_encrypted_card_track_data.Validated += new System.EventHandler(this.txt_encrypted_card_track_data_Validated);
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(13, 46);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(45, 13);
      this.label17.TabIndex = 3;
      this.label17.Text = "External";
      // 
      // txt_card_track_data
      // 
      this.txt_card_track_data.Location = new System.Drawing.Point(92, 16);
      this.txt_card_track_data.MaxLength = 13;
      this.txt_card_track_data.Name = "txt_card_track_data";
      this.txt_card_track_data.Size = new System.Drawing.Size(144, 20);
      this.txt_card_track_data.TabIndex = 109;
      this.txt_card_track_data.Validated += new System.EventHandler(this.txt_card_track_data_Validated);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(42, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Internal";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.cmb_serial_num);
      this.groupBox1.Controls.Add(this.txt_num_terminals);
      this.groupBox1.Controls.Add(this.label18);
      this.groupBox1.Controls.Add(this.txt_machine_num);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Location = new System.Drawing.Point(260, 6);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(242, 113);
      this.groupBox1.TabIndex = 19;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Terminal";
      // 
      // cmb_serial_num
      // 
      this.cmb_serial_num.FormattingEnabled = true;
      this.cmb_serial_num.Location = new System.Drawing.Point(91, 45);
      this.cmb_serial_num.Name = "cmb_serial_num";
      this.cmb_serial_num.Size = new System.Drawing.Size(144, 21);
      this.cmb_serial_num.TabIndex = 107;
      this.cmb_serial_num.SelectedIndexChanged += new System.EventHandler(this.cmb_serial_num_SelectedIndexChanged);
      // 
      // txt_num_terminals
      // 
      this.txt_num_terminals.Location = new System.Drawing.Point(92, 16);
      this.txt_num_terminals.MaxLength = 4;
      this.txt_num_terminals.Name = "txt_num_terminals";
      this.txt_num_terminals.Size = new System.Drawing.Size(82, 20);
      this.txt_num_terminals.TabIndex = 106;
      this.txt_num_terminals.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(13, 19);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(62, 13);
      this.label18.TabIndex = 15;
      this.label18.Text = "# Terminals";
      // 
      // txt_machine_num
      // 
      this.txt_machine_num.Location = new System.Drawing.Point(92, 73);
      this.txt_machine_num.Name = "txt_machine_num";
      this.txt_machine_num.Size = new System.Drawing.Size(144, 20);
      this.txt_machine_num.TabIndex = 108;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(13, 76);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(62, 13);
      this.label4.TabIndex = 11;
      this.label4.Text = "Machine ID";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(13, 48);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(73, 13);
      this.label3.TabIndex = 13;
      this.label3.Text = "Serial Number";
      // 
      // tmr_refresh
      // 
      this.tmr_refresh.Enabled = true;
      this.tmr_refresh.Interval = 1000;
      this.tmr_refresh.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // m_btn_start
      // 
      this.m_btn_start.Location = new System.Drawing.Point(9, 389);
      this.m_btn_start.Name = "m_btn_start";
      this.m_btn_start.Size = new System.Drawing.Size(119, 34);
      this.m_btn_start.TabIndex = 117;
      this.m_btn_start.Text = "Start";
      this.m_btn_start.UseVisualStyleBackColor = true;
      this.m_btn_start.Click += new System.EventHandler(this.btn_start_Click);
      // 
      // tmr_stats
      // 
      this.tmr_stats.Interval = 1000;
      this.tmr_stats.Tick += new System.EventHandler(this.tmr_stats_Tick);
      // 
      // lbl_statistics_1
      // 
      this.lbl_statistics_1.AutoSize = true;
      this.lbl_statistics_1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_statistics_1.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_statistics_1.Location = new System.Drawing.Point(116, 50);
      this.lbl_statistics_1.Name = "lbl_statistics_1";
      this.lbl_statistics_1.Size = new System.Drawing.Size(84, 14);
      this.lbl_statistics_1.TabIndex = 33;
      this.lbl_statistics_1.Text = "Stats 1 sec";
      // 
      // lbl_statistics_5
      // 
      this.lbl_statistics_5.AutoSize = true;
      this.lbl_statistics_5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_statistics_5.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_statistics_5.Location = new System.Drawing.Point(116, 72);
      this.lbl_statistics_5.Name = "lbl_statistics_5";
      this.lbl_statistics_5.Size = new System.Drawing.Size(91, 14);
      this.lbl_statistics_5.TabIndex = 34;
      this.lbl_statistics_5.Text = "Stats 5 secs";
      // 
      // lbl_statistics_avg
      // 
      this.lbl_statistics_avg.AutoSize = true;
      this.lbl_statistics_avg.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_statistics_avg.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_statistics_avg.Location = new System.Drawing.Point(116, 95);
      this.lbl_statistics_avg.Name = "lbl_statistics_avg";
      this.lbl_statistics_avg.Size = new System.Drawing.Size(70, 14);
      this.lbl_statistics_avg.TabIndex = 35;
      this.lbl_statistics_avg.Text = "Stats avg";
      // 
      // lbl_head
      // 
      this.lbl_head.AutoSize = true;
      this.lbl_head.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_head.Location = new System.Drawing.Point(116, 29);
      this.lbl_head.Name = "lbl_head";
      this.lbl_head.Size = new System.Drawing.Size(77, 14);
      this.lbl_head.TabIndex = 36;
      this.lbl_head.Text = "label_head";
      // 
      // lbl_sim_status
      // 
      this.lbl_sim_status.AutoSize = true;
      this.lbl_sim_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_sim_status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
      this.lbl_sim_status.Location = new System.Drawing.Point(133, 393);
      this.lbl_sim_status.Name = "lbl_sim_status";
      this.lbl_sim_status.Size = new System.Drawing.Size(99, 25);
      this.lbl_sim_status.TabIndex = 37;
      this.lbl_sim_status.Text = "Stopped";
      // 
      // lbl_totals
      // 
      this.lbl_totals.AutoSize = true;
      this.lbl_totals.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_totals.ForeColor = System.Drawing.SystemColors.HotTrack;
      this.lbl_totals.Location = new System.Drawing.Point(116, 148);
      this.lbl_totals.Name = "lbl_totals";
      this.lbl_totals.Size = new System.Drawing.Size(49, 14);
      this.lbl_totals.TabIndex = 38;
      this.lbl_totals.Text = "TOTALS";
      // 
      // lbl_head_totals
      // 
      this.lbl_head_totals.AutoSize = true;
      this.lbl_head_totals.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_head_totals.Location = new System.Drawing.Point(116, 126);
      this.lbl_head_totals.Name = "lbl_head_totals";
      this.lbl_head_totals.Size = new System.Drawing.Size(126, 14);
      this.lbl_head_totals.TabIndex = 39;
      this.lbl_head_totals.Text = "label_head_totals";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.label8);
      this.groupBox3.Controls.Add(this.label7);
      this.groupBox3.Controls.Add(this.label6);
      this.groupBox3.Controls.Add(this.label5);
      this.groupBox3.Controls.Add(this.lbl_head);
      this.groupBox3.Controls.Add(this.lbl_statistics_1);
      this.groupBox3.Controls.Add(this.lbl_head_totals);
      this.groupBox3.Controls.Add(this.lbl_statistics_5);
      this.groupBox3.Controls.Add(this.lbl_statistics_avg);
      this.groupBox3.Controls.Add(this.lbl_totals);
      this.groupBox3.Location = new System.Drawing.Point(6, 6);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(1135, 198);
      this.groupBox3.TabIndex = 40;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Statistics";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.Location = new System.Drawing.Point(6, 148);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(56, 14);
      this.label8.TabIndex = 43;
      this.label8.Text = "TOTALS:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label7.Location = new System.Drawing.Point(6, 95);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(63, 14);
      this.label7.TabIndex = 42;
      this.label7.Text = "Average:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.Location = new System.Drawing.Point(6, 72);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(112, 14);
      this.label6.TabIndex = 41;
      this.label6.Text = "Last 5 seconds:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(6, 50);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(91, 14);
      this.label5.TabIndex = 40;
      this.label5.Text = "Last second:";
      // 
      // tab_simulator
      // 
      this.tab_simulator.Controls.Add(this.tab_parameters);
      this.tab_simulator.Controls.Add(this.tab_statistics);
      this.tab_simulator.Controls.Add(this.tab_ws);
      this.tab_simulator.Location = new System.Drawing.Point(9, 12);
      this.tab_simulator.Name = "tab_simulator";
      this.tab_simulator.SelectedIndex = 0;
      this.tab_simulator.Size = new System.Drawing.Size(1155, 371);
      this.tab_simulator.TabIndex = 41;
      this.tab_simulator.SelectedIndexChanged += new System.EventHandler(this.tab_simulator_SelectedIndexChanged);
      // 
      // tab_parameters
      // 
      this.tab_parameters.Controls.Add(this.lbl_ws_url);
      this.tab_parameters.Controls.Add(this.txt_IPadress);
      this.tab_parameters.Controls.Add(this.label22);
      this.tab_parameters.Controls.Add(this.txt_min_update_x_sec);
      this.tab_parameters.Controls.Add(this.label21);
      this.tab_parameters.Controls.Add(this.txt_max_plays_x_update);
      this.tab_parameters.Controls.Add(this.btn_set_accounts);
      this.tab_parameters.Controls.Add(this.btn_set_terminals);
      this.tab_parameters.Controls.Add(this.groupBox6);
      this.tab_parameters.Controls.Add(this.groupBox4);
      this.tab_parameters.Controls.Add(this.groupBox5);
      this.tab_parameters.Controls.Add(this.groupBox1);
      this.tab_parameters.Controls.Add(this.groupBox2);
      this.tab_parameters.Location = new System.Drawing.Point(4, 22);
      this.tab_parameters.Name = "tab_parameters";
      this.tab_parameters.Padding = new System.Windows.Forms.Padding(3);
      this.tab_parameters.Size = new System.Drawing.Size(1147, 345);
      this.tab_parameters.TabIndex = 0;
      this.tab_parameters.Text = "Parameters";
      this.tab_parameters.UseVisualStyleBackColor = true;
      // 
      // lbl_ws_url
      // 
      this.lbl_ws_url.AutoSize = true;
      this.lbl_ws_url.Location = new System.Drawing.Point(509, 217);
      this.lbl_ws_url.Name = "lbl_ws_url";
      this.lbl_ws_url.Size = new System.Drawing.Size(99, 13);
      this.lbl_ws_url.TabIndex = 121;
      this.lbl_ws_url.Text = "Web Services URL";
      // 
      // txt_IPadress
      // 
      this.txt_IPadress.Location = new System.Drawing.Point(508, 235);
      this.txt_IPadress.Name = "txt_IPadress";
      this.txt_IPadress.Size = new System.Drawing.Size(309, 20);
      this.txt_IPadress.TabIndex = 120;
      // 
      // label22
      // 
      this.label22.AutoSize = true;
      this.label22.Location = new System.Drawing.Point(645, 168);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(117, 13);
      this.label22.TabIndex = 119;
      this.label22.Text = "MinUpdatesPerSecond";
      this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // txt_min_update_x_sec
      // 
      this.txt_min_update_x_sec.Location = new System.Drawing.Point(648, 184);
      this.txt_min_update_x_sec.MaxLength = 7;
      this.txt_min_update_x_sec.Name = "txt_min_update_x_sec";
      this.txt_min_update_x_sec.Size = new System.Drawing.Size(76, 20);
      this.txt_min_update_x_sec.TabIndex = 118;
      this.txt_min_update_x_sec.Text = "30";
      this.txt_min_update_x_sec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(505, 168);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(103, 13);
      this.label21.TabIndex = 117;
      this.label21.Text = "MaxPlaysPerUpdate";
      this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // txt_max_plays_x_update
      // 
      this.txt_max_plays_x_update.Location = new System.Drawing.Point(508, 184);
      this.txt_max_plays_x_update.MaxLength = 7;
      this.txt_max_plays_x_update.Name = "txt_max_plays_x_update";
      this.txt_max_plays_x_update.Size = new System.Drawing.Size(76, 20);
      this.txt_max_plays_x_update.TabIndex = 116;
      this.txt_max_plays_x_update.Text = "1";
      this.txt_max_plays_x_update.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // btn_set_accounts
      // 
      this.btn_set_accounts.Location = new System.Drawing.Point(508, 132);
      this.btn_set_accounts.Name = "btn_set_accounts";
      this.btn_set_accounts.Size = new System.Drawing.Size(89, 23);
      this.btn_set_accounts.TabIndex = 24;
      this.btn_set_accounts.Text = "Set Accounts";
      this.btn_set_accounts.UseVisualStyleBackColor = true;
      this.btn_set_accounts.Click += new System.EventHandler(this.btn_set_accounts_Click);
      // 
      // btn_set_terminals
      // 
      this.btn_set_terminals.Location = new System.Drawing.Point(508, 96);
      this.btn_set_terminals.Name = "btn_set_terminals";
      this.btn_set_terminals.Size = new System.Drawing.Size(89, 23);
      this.btn_set_terminals.TabIndex = 23;
      this.btn_set_terminals.Text = "Set Terminals";
      this.btn_set_terminals.UseVisualStyleBackColor = true;
      this.btn_set_terminals.Click += new System.EventHandler(this.btn_set_terminals_Click);
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.chk_account_status);
      this.groupBox6.Controls.Add(this.chk_report_jackpot);
      this.groupBox6.Location = new System.Drawing.Point(508, 6);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(125, 66);
      this.groupBox6.TabIndex = 22;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = "Options";
      // 
      // chk_account_status
      // 
      this.chk_account_status.AutoSize = true;
      this.chk_account_status.Location = new System.Drawing.Point(16, 41);
      this.chk_account_status.Name = "chk_account_status";
      this.chk_account_status.Size = new System.Drawing.Size(99, 17);
      this.chk_account_status.TabIndex = 1;
      this.chk_account_status.Text = "Account Status";
      this.chk_account_status.UseVisualStyleBackColor = true;
      // 
      // chk_report_jackpot
      // 
      this.chk_report_jackpot.AutoSize = true;
      this.chk_report_jackpot.Location = new System.Drawing.Point(16, 18);
      this.chk_report_jackpot.Name = "chk_report_jackpot";
      this.chk_report_jackpot.Size = new System.Drawing.Size(99, 17);
      this.chk_report_jackpot.TabIndex = 0;
      this.chk_report_jackpot.Text = "Report Jackpot";
      this.chk_report_jackpot.UseVisualStyleBackColor = true;
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.txt_max_delay_between_sessions);
      this.groupBox4.Controls.Add(this.txt_min_delay_between_sessions);
      this.groupBox4.Controls.Add(this.label20);
      this.groupBox4.Controls.Add(this.txt_max_delay_between_plays);
      this.groupBox4.Controls.Add(this.label9);
      this.groupBox4.Controls.Add(this.txt_min_session_plays);
      this.groupBox4.Controls.Add(this.txt_min_delay_between_plays);
      this.groupBox4.Controls.Add(this.txt_max_session_plays);
      this.groupBox4.Controls.Add(this.label19);
      this.groupBox4.Controls.Add(this.label11);
      this.groupBox4.Controls.Add(this.label10);
      this.groupBox4.Location = new System.Drawing.Point(97, 210);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(405, 83);
      this.groupBox4.TabIndex = 21;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Timing in ms";
      // 
      // txt_max_delay_between_sessions
      // 
      this.txt_max_delay_between_sessions.Location = new System.Drawing.Point(88, 54);
      this.txt_max_delay_between_sessions.MaxLength = 7;
      this.txt_max_delay_between_sessions.Name = "txt_max_delay_between_sessions";
      this.txt_max_delay_between_sessions.Size = new System.Drawing.Size(94, 20);
      this.txt_max_delay_between_sessions.TabIndex = 112;
      this.txt_max_delay_between_sessions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_min_delay_between_sessions
      // 
      this.txt_min_delay_between_sessions.Location = new System.Drawing.Point(88, 32);
      this.txt_min_delay_between_sessions.MaxLength = 7;
      this.txt_min_delay_between_sessions.Name = "txt_min_delay_between_sessions";
      this.txt_min_delay_between_sessions.Size = new System.Drawing.Size(94, 20);
      this.txt_min_delay_between_sessions.TabIndex = 111;
      this.txt_min_delay_between_sessions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label20
      // 
      this.label20.AutoSize = true;
      this.label20.Location = new System.Drawing.Point(78, 16);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(118, 13);
      this.label20.TabIndex = 10;
      this.label20.Text = "DelayBetweenSessions";
      // 
      // txt_max_delay_between_plays
      // 
      this.txt_max_delay_between_plays.Location = new System.Drawing.Point(205, 54);
      this.txt_max_delay_between_plays.MaxLength = 7;
      this.txt_max_delay_between_plays.Name = "txt_max_delay_between_plays";
      this.txt_max_delay_between_plays.Size = new System.Drawing.Size(94, 20);
      this.txt_max_delay_between_plays.TabIndex = 114;
      this.txt_max_delay_between_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(309, 16);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(85, 13);
      this.label9.TabIndex = 3;
      this.label9.Text = "PlaysPerSession";
      this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // txt_min_session_plays
      // 
      this.txt_min_session_plays.Location = new System.Drawing.Point(313, 32);
      this.txt_min_session_plays.MaxLength = 7;
      this.txt_min_session_plays.Name = "txt_min_session_plays";
      this.txt_min_session_plays.Size = new System.Drawing.Size(76, 20);
      this.txt_min_session_plays.TabIndex = 115;
      this.txt_min_session_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_min_delay_between_plays
      // 
      this.txt_min_delay_between_plays.Location = new System.Drawing.Point(205, 32);
      this.txt_min_delay_between_plays.MaxLength = 7;
      this.txt_min_delay_between_plays.Name = "txt_min_delay_between_plays";
      this.txt_min_delay_between_plays.Size = new System.Drawing.Size(94, 20);
      this.txt_min_delay_between_plays.TabIndex = 113;
      this.txt_min_delay_between_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_max_session_plays
      // 
      this.txt_max_session_plays.Location = new System.Drawing.Point(313, 54);
      this.txt_max_session_plays.MaxLength = 7;
      this.txt_max_session_plays.Name = "txt_max_session_plays";
      this.txt_max_session_plays.Size = new System.Drawing.Size(76, 20);
      this.txt_max_session_plays.TabIndex = 116;
      this.txt_max_session_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label19.Location = new System.Drawing.Point(44, 57);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(30, 13);
      this.label19.TabIndex = 7;
      this.label19.Text = "MAX";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(202, 16);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(101, 13);
      this.label11.TabIndex = 6;
      this.label11.Text = "DelayBetweenPlays";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label10.Location = new System.Drawing.Point(44, 35);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(27, 13);
      this.label10.TabIndex = 4;
      this.label10.Text = "MIN";
      // 
      // tab_statistics
      // 
      this.tab_statistics.Controls.Add(this.groupBox3);
      this.tab_statistics.Location = new System.Drawing.Point(4, 22);
      this.tab_statistics.Name = "tab_statistics";
      this.tab_statistics.Padding = new System.Windows.Forms.Padding(3);
      this.tab_statistics.Size = new System.Drawing.Size(1147, 345);
      this.tab_statistics.TabIndex = 1;
      this.tab_statistics.Text = "Statistics";
      this.tab_statistics.UseVisualStyleBackColor = true;
      // 
      // tab_ws
      // 
      this.tab_ws.Controls.Add(this.gbox_log);
      this.tab_ws.Controls.Add(this.gbox_ws);
      this.tab_ws.Location = new System.Drawing.Point(4, 22);
      this.tab_ws.Name = "tab_ws";
      this.tab_ws.Padding = new System.Windows.Forms.Padding(3);
      this.tab_ws.Size = new System.Drawing.Size(1147, 345);
      this.tab_ws.TabIndex = 2;
      this.tab_ws.Text = "S2S";
      this.tab_ws.UseVisualStyleBackColor = true;
      // 
      // gbox_log
      // 
      this.gbox_log.Controls.Add(this.txt_ws_log);
      this.gbox_log.Location = new System.Drawing.Point(480, 6);
      this.gbox_log.Name = "gbox_log";
      this.gbox_log.Size = new System.Drawing.Size(661, 326);
      this.gbox_log.TabIndex = 3;
      this.gbox_log.TabStop = false;
      this.gbox_log.Text = "Log";
      // 
      // txt_ws_log
      // 
      this.txt_ws_log.Location = new System.Drawing.Point(6, 23);
      this.txt_ws_log.Name = "txt_ws_log";
      this.txt_ws_log.Size = new System.Drawing.Size(649, 297);
      this.txt_ws_log.TabIndex = 4;
      this.txt_ws_log.Text = "";
      // 
      // gbox_ws
      // 
      this.gbox_ws.Controls.Add(this.pnl_start_session);
      this.gbox_ws.Controls.Add(this.pnl_account_info);
      this.gbox_ws.Controls.Add(this.gb_connection);
      this.gbox_ws.Controls.Add(this.btn_random);
      this.gbox_ws.Controls.Add(this.pnl_container);
      this.gbox_ws.Controls.Add(this.cmb_sp);
      this.gbox_ws.Controls.Add(this.lbl_sp);
      this.gbox_ws.Location = new System.Drawing.Point(6, 6);
      this.gbox_ws.Name = "gbox_ws";
      this.gbox_ws.Size = new System.Drawing.Size(468, 326);
      this.gbox_ws.TabIndex = 2;
      this.gbox_ws.TabStop = false;
      this.gbox_ws.Text = "WS";
      // 
      // pnl_start_session
      // 
      this.pnl_start_session.Controls.Add(this.gbox_session_start);
      this.pnl_start_session.Location = new System.Drawing.Point(0, 104);
      this.pnl_start_session.Name = "pnl_start_session";
      this.pnl_start_session.Size = new System.Drawing.Size(471, 241);
      this.pnl_start_session.TabIndex = 122;
      // 
      // pnl_account_info
      // 
      this.pnl_account_info.Controls.Add(this.groupBox7);
      this.pnl_account_info.Location = new System.Drawing.Point(3, 107);
      this.pnl_account_info.Name = "pnl_account_info";
      this.pnl_account_info.Size = new System.Drawing.Size(465, 219);
      this.pnl_account_info.TabIndex = 4;
      // 
      // groupBox7
      // 
      this.groupBox7.Controls.Add(this.label23);
      this.groupBox7.Controls.Add(this.pnl_info_txt_vendor_id);
      this.groupBox7.Controls.Add(this.label30);
      this.groupBox7.Controls.Add(this.pnl_info_txt_track_data);
      this.groupBox7.Location = new System.Drawing.Point(13, 5);
      this.groupBox7.Name = "groupBox7";
      this.groupBox7.Size = new System.Drawing.Size(438, 202);
      this.groupBox7.TabIndex = 2;
      this.groupBox7.TabStop = false;
      this.groupBox7.Text = "AccountInfo";
      // 
      // label23
      // 
      this.label23.AutoSize = true;
      this.label23.Location = new System.Drawing.Point(14, 29);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(53, 13);
      this.label23.TabIndex = 13;
      this.label23.Text = "Vendor Id";
      // 
      // pnl_info_txt_vendor_id
      // 
      this.pnl_info_txt_vendor_id.Location = new System.Drawing.Point(14, 43);
      this.pnl_info_txt_vendor_id.Name = "pnl_info_txt_vendor_id";
      this.pnl_info_txt_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.pnl_info_txt_vendor_id.TabIndex = 12;
      // 
      // label30
      // 
      this.label30.AutoSize = true;
      this.label30.Location = new System.Drawing.Point(14, 85);
      this.label30.Name = "label30";
      this.label30.Size = new System.Drawing.Size(58, 13);
      this.label30.TabIndex = 11;
      this.label30.Text = "TrackData";
      // 
      // pnl_info_txt_track_data
      // 
      this.pnl_info_txt_track_data.Location = new System.Drawing.Point(14, 99);
      this.pnl_info_txt_track_data.Name = "pnl_info_txt_track_data";
      this.pnl_info_txt_track_data.Size = new System.Drawing.Size(199, 20);
      this.pnl_info_txt_track_data.TabIndex = 10;
      // 
      // gb_connection
      // 
      this.gb_connection.Controls.Add(this.rd_ws);
      this.gb_connection.Controls.Add(this.rd_s2s);
      this.gb_connection.Controls.Add(this.rd_zsp);
      this.gb_connection.Location = new System.Drawing.Point(6, 19);
      this.gb_connection.Name = "gb_connection";
      this.gb_connection.Size = new System.Drawing.Size(253, 42);
      this.gb_connection.TabIndex = 121;
      this.gb_connection.TabStop = false;
      this.gb_connection.Text = "Connection";
      // 
      // rd_ws
      // 
      this.rd_ws.AutoSize = true;
      this.rd_ws.Checked = true;
      this.rd_ws.Location = new System.Drawing.Point(154, 18);
      this.rd_ws.Name = "rd_ws";
      this.rd_ws.Size = new System.Drawing.Size(87, 17);
      this.rd_ws.TabIndex = 4;
      this.rd_ws.TabStop = true;
      this.rd_ws.Text = "Web Service";
      this.rd_ws.UseVisualStyleBackColor = true;
      // 
      // rd_s2s
      // 
      this.rd_s2s.AutoSize = true;
      this.rd_s2s.Location = new System.Drawing.Point(89, 18);
      this.rd_s2s.Name = "rd_s2s";
      this.rd_s2s.Size = new System.Drawing.Size(45, 17);
      this.rd_s2s.TabIndex = 3;
      this.rd_s2s.TabStop = true;
      this.rd_s2s.Text = "S2S";
      this.rd_s2s.UseVisualStyleBackColor = true;
      // 
      // rd_zsp
      // 
      this.rd_zsp.AutoSize = true;
      this.rd_zsp.Location = new System.Drawing.Point(22, 18);
      this.rd_zsp.Name = "rd_zsp";
      this.rd_zsp.Size = new System.Drawing.Size(46, 17);
      this.rd_zsp.TabIndex = 2;
      this.rd_zsp.TabStop = true;
      this.rd_zsp.Text = "ZSP";
      this.rd_zsp.UseVisualStyleBackColor = true;
      // 
      // btn_random
      // 
      this.btn_random.Location = new System.Drawing.Point(332, 81);
      this.btn_random.Name = "btn_random";
      this.btn_random.Size = new System.Drawing.Size(119, 23);
      this.btn_random.TabIndex = 120;
      this.btn_random.Text = "Random";
      this.btn_random.UseVisualStyleBackColor = true;
      this.btn_random.Click += new System.EventHandler(this.btn_random_Click);
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_Card_Info);
      this.pnl_container.Controls.Add(this.pnl_session_update);
      this.pnl_container.Controls.Add(this.pnl_report_event);
      this.pnl_container.Controls.Add(this.pnl_session_end);
      this.pnl_container.Controls.Add(this.pnl_account_status);
      this.pnl_container.Controls.Add(this.pnl_Report_Meters);
      this.pnl_container.Location = new System.Drawing.Point(0, 110);
      this.pnl_container.Name = "pnl_container";
      this.pnl_container.Size = new System.Drawing.Size(467, 216);
      this.pnl_container.TabIndex = 2;
      // 
      // pnl_Card_Info
      // 
      this.pnl_Card_Info.Controls.Add(this.groupBox8);
      this.pnl_Card_Info.Location = new System.Drawing.Point(0, 3);
      this.pnl_Card_Info.Name = "pnl_Card_Info";
      this.pnl_Card_Info.Size = new System.Drawing.Size(465, 219);
      this.pnl_Card_Info.TabIndex = 5;
      // 
      // groupBox8
      // 
      this.groupBox8.Controls.Add(this.label25);
      this.groupBox8.Controls.Add(this.pnl_card_info_track_data);
      this.groupBox8.Location = new System.Drawing.Point(13, 5);
      this.groupBox8.Name = "groupBox8";
      this.groupBox8.Size = new System.Drawing.Size(438, 202);
      this.groupBox8.TabIndex = 2;
      this.groupBox8.TabStop = false;
      this.groupBox8.Text = "CardInfo";
      // 
      // label25
      // 
      this.label25.AutoSize = true;
      this.label25.Location = new System.Drawing.Point(14, 23);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(58, 13);
      this.label25.TabIndex = 11;
      this.label25.Text = "TrackData";
      // 
      // pnl_card_info_track_data
      // 
      this.pnl_card_info_track_data.Location = new System.Drawing.Point(14, 37);
      this.pnl_card_info_track_data.Name = "pnl_card_info_track_data";
      this.pnl_card_info_track_data.Size = new System.Drawing.Size(199, 20);
      this.pnl_card_info_track_data.TabIndex = 10;
      // 
      // pnl_session_update
      // 
      this.pnl_session_update.Controls.Add(this.gbox_session_update);
      this.pnl_session_update.Location = new System.Drawing.Point(0, 3);
      this.pnl_session_update.Name = "pnl_session_update";
      this.pnl_session_update.Size = new System.Drawing.Size(465, 219);
      this.pnl_session_update.TabIndex = 1;
      // 
      // gbox_session_update
      // 
      this.gbox_session_update.Controls.Add(this.lbl_session_update_credits_balance);
      this.gbox_session_update.Controls.Add(this.txt_session_update_credits_balance);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_bill_in);
      this.gbox_session_update.Controls.Add(this.txt_session_update_bill_in);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_games_won);
      this.gbox_session_update.Controls.Add(this.txt_session_update_games_won);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_games_played);
      this.gbox_session_update.Controls.Add(this.txt_session_update_games_played);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_credits_won);
      this.gbox_session_update.Controls.Add(this.txt_session_update_credits_won);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_credits_played);
      this.gbox_session_update.Controls.Add(this.txt_session_update_credits_played);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_session_id);
      this.gbox_session_update.Controls.Add(this.txt_session_update_session_id);
      this.gbox_session_update.Controls.Add(this.lbl_session_update_vendor_id);
      this.gbox_session_update.Controls.Add(this.txt_session_update_vendor_id);
      this.gbox_session_update.Location = new System.Drawing.Point(13, 5);
      this.gbox_session_update.Name = "gbox_session_update";
      this.gbox_session_update.Size = new System.Drawing.Size(438, 202);
      this.gbox_session_update.TabIndex = 2;
      this.gbox_session_update.TabStop = false;
      this.gbox_session_update.Text = "Session Update";
      // 
      // lbl_session_update_credits_balance
      // 
      this.lbl_session_update_credits_balance.AutoSize = true;
      this.lbl_session_update_credits_balance.Location = new System.Drawing.Point(228, 135);
      this.lbl_session_update_credits_balance.Name = "lbl_session_update_credits_balance";
      this.lbl_session_update_credits_balance.Size = new System.Drawing.Size(76, 13);
      this.lbl_session_update_credits_balance.TabIndex = 25;
      this.lbl_session_update_credits_balance.Text = "Credit Balance";
      // 
      // txt_session_update_credits_balance
      // 
      this.txt_session_update_credits_balance.Location = new System.Drawing.Point(228, 149);
      this.txt_session_update_credits_balance.Name = "txt_session_update_credits_balance";
      this.txt_session_update_credits_balance.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_credits_balance.TabIndex = 24;
      // 
      // lbl_session_update_bill_in
      // 
      this.lbl_session_update_bill_in.AutoSize = true;
      this.lbl_session_update_bill_in.Location = new System.Drawing.Point(11, 135);
      this.lbl_session_update_bill_in.Name = "lbl_session_update_bill_in";
      this.lbl_session_update_bill_in.Size = new System.Drawing.Size(32, 13);
      this.lbl_session_update_bill_in.TabIndex = 23;
      this.lbl_session_update_bill_in.Text = "Bill In";
      // 
      // txt_session_update_bill_in
      // 
      this.txt_session_update_bill_in.Location = new System.Drawing.Point(11, 149);
      this.txt_session_update_bill_in.Name = "txt_session_update_bill_in";
      this.txt_session_update_bill_in.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_bill_in.TabIndex = 22;
      // 
      // lbl_session_update_games_won
      // 
      this.lbl_session_update_games_won.AutoSize = true;
      this.lbl_session_update_games_won.Location = new System.Drawing.Point(228, 95);
      this.lbl_session_update_games_won.Name = "lbl_session_update_games_won";
      this.lbl_session_update_games_won.Size = new System.Drawing.Size(66, 13);
      this.lbl_session_update_games_won.TabIndex = 21;
      this.lbl_session_update_games_won.Text = "Games Won";
      // 
      // txt_session_update_games_won
      // 
      this.txt_session_update_games_won.Location = new System.Drawing.Point(228, 109);
      this.txt_session_update_games_won.Name = "txt_session_update_games_won";
      this.txt_session_update_games_won.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_games_won.TabIndex = 20;
      // 
      // lbl_session_update_games_played
      // 
      this.lbl_session_update_games_played.AutoSize = true;
      this.lbl_session_update_games_played.Location = new System.Drawing.Point(11, 95);
      this.lbl_session_update_games_played.Name = "lbl_session_update_games_played";
      this.lbl_session_update_games_played.Size = new System.Drawing.Size(75, 13);
      this.lbl_session_update_games_played.TabIndex = 19;
      this.lbl_session_update_games_played.Text = "Games Played";
      // 
      // txt_session_update_games_played
      // 
      this.txt_session_update_games_played.Location = new System.Drawing.Point(11, 109);
      this.txt_session_update_games_played.Name = "txt_session_update_games_played";
      this.txt_session_update_games_played.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_games_played.TabIndex = 18;
      // 
      // lbl_session_update_credits_won
      // 
      this.lbl_session_update_credits_won.AutoSize = true;
      this.lbl_session_update_credits_won.Location = new System.Drawing.Point(228, 55);
      this.lbl_session_update_credits_won.Name = "lbl_session_update_credits_won";
      this.lbl_session_update_credits_won.Size = new System.Drawing.Size(65, 13);
      this.lbl_session_update_credits_won.TabIndex = 17;
      this.lbl_session_update_credits_won.Text = "Credits Won";
      // 
      // txt_session_update_credits_won
      // 
      this.txt_session_update_credits_won.Location = new System.Drawing.Point(228, 69);
      this.txt_session_update_credits_won.Name = "txt_session_update_credits_won";
      this.txt_session_update_credits_won.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_credits_won.TabIndex = 16;
      // 
      // lbl_session_update_credits_played
      // 
      this.lbl_session_update_credits_played.AutoSize = true;
      this.lbl_session_update_credits_played.Location = new System.Drawing.Point(11, 55);
      this.lbl_session_update_credits_played.Name = "lbl_session_update_credits_played";
      this.lbl_session_update_credits_played.Size = new System.Drawing.Size(74, 13);
      this.lbl_session_update_credits_played.TabIndex = 15;
      this.lbl_session_update_credits_played.Text = "Credits Played";
      // 
      // txt_session_update_credits_played
      // 
      this.txt_session_update_credits_played.Location = new System.Drawing.Point(11, 69);
      this.txt_session_update_credits_played.Name = "txt_session_update_credits_played";
      this.txt_session_update_credits_played.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_credits_played.TabIndex = 14;
      // 
      // lbl_session_update_session_id
      // 
      this.lbl_session_update_session_id.AutoSize = true;
      this.lbl_session_update_session_id.Location = new System.Drawing.Point(228, 17);
      this.lbl_session_update_session_id.Name = "lbl_session_update_session_id";
      this.lbl_session_update_session_id.Size = new System.Drawing.Size(55, 13);
      this.lbl_session_update_session_id.TabIndex = 13;
      this.lbl_session_update_session_id.Text = "SessionID";
      // 
      // txt_session_update_session_id
      // 
      this.txt_session_update_session_id.Location = new System.Drawing.Point(228, 31);
      this.txt_session_update_session_id.Name = "txt_session_update_session_id";
      this.txt_session_update_session_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_session_id.TabIndex = 12;
      // 
      // lbl_session_update_vendor_id
      // 
      this.lbl_session_update_vendor_id.AutoSize = true;
      this.lbl_session_update_vendor_id.Location = new System.Drawing.Point(11, 17);
      this.lbl_session_update_vendor_id.Name = "lbl_session_update_vendor_id";
      this.lbl_session_update_vendor_id.Size = new System.Drawing.Size(52, 13);
      this.lbl_session_update_vendor_id.TabIndex = 11;
      this.lbl_session_update_vendor_id.Text = "VendorID";
      // 
      // txt_session_update_vendor_id
      // 
      this.txt_session_update_vendor_id.Location = new System.Drawing.Point(11, 31);
      this.txt_session_update_vendor_id.Name = "txt_session_update_vendor_id";
      this.txt_session_update_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_update_vendor_id.TabIndex = 10;
      // 
      // pnl_report_event
      // 
      this.pnl_report_event.Controls.Add(this.gbox_report_event);
      this.pnl_report_event.Location = new System.Drawing.Point(0, 3);
      this.pnl_report_event.Name = "pnl_report_event";
      this.pnl_report_event.Size = new System.Drawing.Size(465, 219);
      this.pnl_report_event.TabIndex = 3;
      // 
      // gbox_report_event
      // 
      this.gbox_report_event.Controls.Add(this.lbl_report_event_session_id);
      this.gbox_report_event.Controls.Add(this.txt_report_event_session_id);
      this.gbox_report_event.Controls.Add(this.lbl_report_event_payout_amount);
      this.gbox_report_event.Controls.Add(this.txt_report_event_payout_amount);
      this.gbox_report_event.Controls.Add(this.lbl_report_event_machine_number);
      this.gbox_report_event.Controls.Add(this.txt_report_event_machine_number);
      this.gbox_report_event.Controls.Add(this.lbl_report_event_serial_number);
      this.gbox_report_event.Controls.Add(this.txt_report_event_serial_number);
      this.gbox_report_event.Controls.Add(this.lbl_report_event_vendor_id);
      this.gbox_report_event.Controls.Add(this.txt_report_event_vendor_id);
      this.gbox_report_event.Controls.Add(this.lbl_report_event_event_id);
      this.gbox_report_event.Controls.Add(this.txt_report_event_event_id);
      this.gbox_report_event.Location = new System.Drawing.Point(13, 5);
      this.gbox_report_event.Name = "gbox_report_event";
      this.gbox_report_event.Size = new System.Drawing.Size(438, 202);
      this.gbox_report_event.TabIndex = 1;
      this.gbox_report_event.TabStop = false;
      this.gbox_report_event.Text = "Report Event";
      // 
      // lbl_report_event_session_id
      // 
      this.lbl_report_event_session_id.AutoSize = true;
      this.lbl_report_event_session_id.Location = new System.Drawing.Point(228, 95);
      this.lbl_report_event_session_id.Name = "lbl_report_event_session_id";
      this.lbl_report_event_session_id.Size = new System.Drawing.Size(55, 13);
      this.lbl_report_event_session_id.TabIndex = 21;
      this.lbl_report_event_session_id.Text = "SessionID";
      // 
      // txt_report_event_session_id
      // 
      this.txt_report_event_session_id.Location = new System.Drawing.Point(228, 109);
      this.txt_report_event_session_id.Name = "txt_report_event_session_id";
      this.txt_report_event_session_id.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_session_id.TabIndex = 20;
      // 
      // lbl_report_event_payout_amount
      // 
      this.lbl_report_event_payout_amount.AutoSize = true;
      this.lbl_report_event_payout_amount.Location = new System.Drawing.Point(11, 95);
      this.lbl_report_event_payout_amount.Name = "lbl_report_event_payout_amount";
      this.lbl_report_event_payout_amount.Size = new System.Drawing.Size(79, 13);
      this.lbl_report_event_payout_amount.TabIndex = 19;
      this.lbl_report_event_payout_amount.Text = "Payout Amount";
      // 
      // txt_report_event_payout_amount
      // 
      this.txt_report_event_payout_amount.Location = new System.Drawing.Point(11, 109);
      this.txt_report_event_payout_amount.Name = "txt_report_event_payout_amount";
      this.txt_report_event_payout_amount.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_payout_amount.TabIndex = 18;
      // 
      // lbl_report_event_machine_number
      // 
      this.lbl_report_event_machine_number.AutoSize = true;
      this.lbl_report_event_machine_number.Location = new System.Drawing.Point(228, 55);
      this.lbl_report_event_machine_number.Name = "lbl_report_event_machine_number";
      this.lbl_report_event_machine_number.Size = new System.Drawing.Size(88, 13);
      this.lbl_report_event_machine_number.TabIndex = 17;
      this.lbl_report_event_machine_number.Text = "Machine Number";
      // 
      // txt_report_event_machine_number
      // 
      this.txt_report_event_machine_number.Location = new System.Drawing.Point(228, 69);
      this.txt_report_event_machine_number.Name = "txt_report_event_machine_number";
      this.txt_report_event_machine_number.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_machine_number.TabIndex = 16;
      // 
      // lbl_report_event_serial_number
      // 
      this.lbl_report_event_serial_number.AutoSize = true;
      this.lbl_report_event_serial_number.Location = new System.Drawing.Point(11, 55);
      this.lbl_report_event_serial_number.Name = "lbl_report_event_serial_number";
      this.lbl_report_event_serial_number.Size = new System.Drawing.Size(73, 13);
      this.lbl_report_event_serial_number.TabIndex = 15;
      this.lbl_report_event_serial_number.Text = "Serial Number";
      // 
      // txt_report_event_serial_number
      // 
      this.txt_report_event_serial_number.Location = new System.Drawing.Point(11, 69);
      this.txt_report_event_serial_number.Name = "txt_report_event_serial_number";
      this.txt_report_event_serial_number.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_serial_number.TabIndex = 14;
      // 
      // lbl_report_event_vendor_id
      // 
      this.lbl_report_event_vendor_id.AutoSize = true;
      this.lbl_report_event_vendor_id.Location = new System.Drawing.Point(228, 17);
      this.lbl_report_event_vendor_id.Name = "lbl_report_event_vendor_id";
      this.lbl_report_event_vendor_id.Size = new System.Drawing.Size(52, 13);
      this.lbl_report_event_vendor_id.TabIndex = 13;
      this.lbl_report_event_vendor_id.Text = "VendorID";
      // 
      // txt_report_event_vendor_id
      // 
      this.txt_report_event_vendor_id.Location = new System.Drawing.Point(228, 31);
      this.txt_report_event_vendor_id.Name = "txt_report_event_vendor_id";
      this.txt_report_event_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_vendor_id.TabIndex = 12;
      // 
      // lbl_report_event_event_id
      // 
      this.lbl_report_event_event_id.AutoSize = true;
      this.lbl_report_event_event_id.Location = new System.Drawing.Point(11, 17);
      this.lbl_report_event_event_id.Name = "lbl_report_event_event_id";
      this.lbl_report_event_event_id.Size = new System.Drawing.Size(46, 13);
      this.lbl_report_event_event_id.TabIndex = 11;
      this.lbl_report_event_event_id.Text = "EventID";
      // 
      // txt_report_event_event_id
      // 
      this.txt_report_event_event_id.Location = new System.Drawing.Point(11, 31);
      this.txt_report_event_event_id.Name = "txt_report_event_event_id";
      this.txt_report_event_event_id.Size = new System.Drawing.Size(199, 20);
      this.txt_report_event_event_id.TabIndex = 10;
      // 
      // pnl_session_end
      // 
      this.pnl_session_end.Controls.Add(this.gbox_session_end);
      this.pnl_session_end.Location = new System.Drawing.Point(0, 3);
      this.pnl_session_end.Name = "pnl_session_end";
      this.pnl_session_end.Size = new System.Drawing.Size(465, 219);
      this.pnl_session_end.TabIndex = 2;
      // 
      // gbox_session_end
      // 
      this.gbox_session_end.Controls.Add(this.lbl_session_end_credits_balance);
      this.gbox_session_end.Controls.Add(this.txt_session_end_credits_balance);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_bill_in);
      this.gbox_session_end.Controls.Add(this.txt_session_end_bill_in);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_games_won);
      this.gbox_session_end.Controls.Add(this.txt_session_end_games_won);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_games_played);
      this.gbox_session_end.Controls.Add(this.txt_session_end_games_played);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_credits_won);
      this.gbox_session_end.Controls.Add(this.txt_session_end_credits_won);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_credits_played);
      this.gbox_session_end.Controls.Add(this.txt_session_end_credits_played);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_session_id);
      this.gbox_session_end.Controls.Add(this.txt_session_end_session_id);
      this.gbox_session_end.Controls.Add(this.lbl_session_end_vendor_id);
      this.gbox_session_end.Controls.Add(this.txt_session_end_vendor_id);
      this.gbox_session_end.Location = new System.Drawing.Point(13, 5);
      this.gbox_session_end.Name = "gbox_session_end";
      this.gbox_session_end.Size = new System.Drawing.Size(438, 202);
      this.gbox_session_end.TabIndex = 2;
      this.gbox_session_end.TabStop = false;
      this.gbox_session_end.Text = "Session End";
      // 
      // lbl_session_end_credits_balance
      // 
      this.lbl_session_end_credits_balance.AutoSize = true;
      this.lbl_session_end_credits_balance.Location = new System.Drawing.Point(228, 135);
      this.lbl_session_end_credits_balance.Name = "lbl_session_end_credits_balance";
      this.lbl_session_end_credits_balance.Size = new System.Drawing.Size(76, 13);
      this.lbl_session_end_credits_balance.TabIndex = 39;
      this.lbl_session_end_credits_balance.Text = "Credit Balance";
      // 
      // txt_session_end_credits_balance
      // 
      this.txt_session_end_credits_balance.Location = new System.Drawing.Point(228, 149);
      this.txt_session_end_credits_balance.Name = "txt_session_end_credits_balance";
      this.txt_session_end_credits_balance.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_credits_balance.TabIndex = 38;
      // 
      // lbl_session_end_bill_in
      // 
      this.lbl_session_end_bill_in.AutoSize = true;
      this.lbl_session_end_bill_in.Location = new System.Drawing.Point(11, 135);
      this.lbl_session_end_bill_in.Name = "lbl_session_end_bill_in";
      this.lbl_session_end_bill_in.Size = new System.Drawing.Size(32, 13);
      this.lbl_session_end_bill_in.TabIndex = 37;
      this.lbl_session_end_bill_in.Text = "Bill In";
      // 
      // txt_session_end_bill_in
      // 
      this.txt_session_end_bill_in.Location = new System.Drawing.Point(11, 149);
      this.txt_session_end_bill_in.Name = "txt_session_end_bill_in";
      this.txt_session_end_bill_in.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_bill_in.TabIndex = 36;
      // 
      // lbl_session_end_games_won
      // 
      this.lbl_session_end_games_won.AutoSize = true;
      this.lbl_session_end_games_won.Location = new System.Drawing.Point(228, 95);
      this.lbl_session_end_games_won.Name = "lbl_session_end_games_won";
      this.lbl_session_end_games_won.Size = new System.Drawing.Size(66, 13);
      this.lbl_session_end_games_won.TabIndex = 35;
      this.lbl_session_end_games_won.Text = "Games Won";
      // 
      // txt_session_end_games_won
      // 
      this.txt_session_end_games_won.Location = new System.Drawing.Point(228, 109);
      this.txt_session_end_games_won.Name = "txt_session_end_games_won";
      this.txt_session_end_games_won.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_games_won.TabIndex = 34;
      // 
      // lbl_session_end_games_played
      // 
      this.lbl_session_end_games_played.AutoSize = true;
      this.lbl_session_end_games_played.Location = new System.Drawing.Point(11, 95);
      this.lbl_session_end_games_played.Name = "lbl_session_end_games_played";
      this.lbl_session_end_games_played.Size = new System.Drawing.Size(75, 13);
      this.lbl_session_end_games_played.TabIndex = 33;
      this.lbl_session_end_games_played.Text = "Games Played";
      // 
      // txt_session_end_games_played
      // 
      this.txt_session_end_games_played.Location = new System.Drawing.Point(11, 109);
      this.txt_session_end_games_played.Name = "txt_session_end_games_played";
      this.txt_session_end_games_played.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_games_played.TabIndex = 32;
      // 
      // lbl_session_end_credits_won
      // 
      this.lbl_session_end_credits_won.AutoSize = true;
      this.lbl_session_end_credits_won.Location = new System.Drawing.Point(228, 55);
      this.lbl_session_end_credits_won.Name = "lbl_session_end_credits_won";
      this.lbl_session_end_credits_won.Size = new System.Drawing.Size(65, 13);
      this.lbl_session_end_credits_won.TabIndex = 31;
      this.lbl_session_end_credits_won.Text = "Credits Won";
      // 
      // txt_session_end_credits_won
      // 
      this.txt_session_end_credits_won.Location = new System.Drawing.Point(228, 69);
      this.txt_session_end_credits_won.Name = "txt_session_end_credits_won";
      this.txt_session_end_credits_won.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_credits_won.TabIndex = 30;
      // 
      // lbl_session_end_credits_played
      // 
      this.lbl_session_end_credits_played.AutoSize = true;
      this.lbl_session_end_credits_played.Location = new System.Drawing.Point(11, 55);
      this.lbl_session_end_credits_played.Name = "lbl_session_end_credits_played";
      this.lbl_session_end_credits_played.Size = new System.Drawing.Size(74, 13);
      this.lbl_session_end_credits_played.TabIndex = 29;
      this.lbl_session_end_credits_played.Text = "Credits Played";
      // 
      // txt_session_end_credits_played
      // 
      this.txt_session_end_credits_played.Location = new System.Drawing.Point(11, 69);
      this.txt_session_end_credits_played.Name = "txt_session_end_credits_played";
      this.txt_session_end_credits_played.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_credits_played.TabIndex = 28;
      // 
      // lbl_session_end_session_id
      // 
      this.lbl_session_end_session_id.AutoSize = true;
      this.lbl_session_end_session_id.Location = new System.Drawing.Point(228, 17);
      this.lbl_session_end_session_id.Name = "lbl_session_end_session_id";
      this.lbl_session_end_session_id.Size = new System.Drawing.Size(55, 13);
      this.lbl_session_end_session_id.TabIndex = 27;
      this.lbl_session_end_session_id.Text = "SessionID";
      // 
      // txt_session_end_session_id
      // 
      this.txt_session_end_session_id.Location = new System.Drawing.Point(228, 31);
      this.txt_session_end_session_id.Name = "txt_session_end_session_id";
      this.txt_session_end_session_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_session_id.TabIndex = 26;
      // 
      // lbl_session_end_vendor_id
      // 
      this.lbl_session_end_vendor_id.AutoSize = true;
      this.lbl_session_end_vendor_id.Location = new System.Drawing.Point(11, 17);
      this.lbl_session_end_vendor_id.Name = "lbl_session_end_vendor_id";
      this.lbl_session_end_vendor_id.Size = new System.Drawing.Size(52, 13);
      this.lbl_session_end_vendor_id.TabIndex = 25;
      this.lbl_session_end_vendor_id.Text = "VendorID";
      // 
      // txt_session_end_vendor_id
      // 
      this.txt_session_end_vendor_id.Location = new System.Drawing.Point(11, 31);
      this.txt_session_end_vendor_id.Name = "txt_session_end_vendor_id";
      this.txt_session_end_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_end_vendor_id.TabIndex = 24;
      // 
      // pnl_account_status
      // 
      this.pnl_account_status.Controls.Add(this.gbox_account_status);
      this.pnl_account_status.Location = new System.Drawing.Point(0, 3);
      this.pnl_account_status.Name = "pnl_account_status";
      this.pnl_account_status.Size = new System.Drawing.Size(465, 219);
      this.pnl_account_status.TabIndex = 2;
      // 
      // gbox_account_status
      // 
      this.gbox_account_status.Controls.Add(this.lbl_account_status_vendor_id);
      this.gbox_account_status.Controls.Add(this.txt_account_status_vendor_id);
      this.gbox_account_status.Controls.Add(this.lbl_account_status_account_id);
      this.gbox_account_status.Controls.Add(this.txt_account_status_account_id);
      this.gbox_account_status.Location = new System.Drawing.Point(13, 5);
      this.gbox_account_status.Name = "gbox_account_status";
      this.gbox_account_status.Size = new System.Drawing.Size(438, 202);
      this.gbox_account_status.TabIndex = 2;
      this.gbox_account_status.TabStop = false;
      this.gbox_account_status.Text = "Account Status";
      // 
      // lbl_account_status_vendor_id
      // 
      this.lbl_account_status_vendor_id.AutoSize = true;
      this.lbl_account_status_vendor_id.Location = new System.Drawing.Point(228, 17);
      this.lbl_account_status_vendor_id.Name = "lbl_account_status_vendor_id";
      this.lbl_account_status_vendor_id.Size = new System.Drawing.Size(52, 13);
      this.lbl_account_status_vendor_id.TabIndex = 11;
      this.lbl_account_status_vendor_id.Text = "VendorID";
      // 
      // txt_account_status_vendor_id
      // 
      this.txt_account_status_vendor_id.Location = new System.Drawing.Point(228, 31);
      this.txt_account_status_vendor_id.Name = "txt_account_status_vendor_id";
      this.txt_account_status_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.txt_account_status_vendor_id.TabIndex = 10;
      // 
      // lbl_account_status_account_id
      // 
      this.lbl_account_status_account_id.AutoSize = true;
      this.lbl_account_status_account_id.Location = new System.Drawing.Point(11, 17);
      this.lbl_account_status_account_id.Name = "lbl_account_status_account_id";
      this.lbl_account_status_account_id.Size = new System.Drawing.Size(58, 13);
      this.lbl_account_status_account_id.TabIndex = 9;
      this.lbl_account_status_account_id.Text = "AccountID";
      // 
      // txt_account_status_account_id
      // 
      this.txt_account_status_account_id.Location = new System.Drawing.Point(11, 31);
      this.txt_account_status_account_id.Name = "txt_account_status_account_id";
      this.txt_account_status_account_id.Size = new System.Drawing.Size(199, 20);
      this.txt_account_status_account_id.TabIndex = 8;
      // 
      // pnl_Report_Meters
      // 
      this.pnl_Report_Meters.Controls.Add(this.gbox_Meters);
      this.pnl_Report_Meters.Location = new System.Drawing.Point(0, 3);
      this.pnl_Report_Meters.Name = "pnl_Report_Meters";
      this.pnl_Report_Meters.Size = new System.Drawing.Size(458, 206);
      this.pnl_Report_Meters.TabIndex = 0;
      // 
      // gbox_Meters
      // 
      this.gbox_Meters.Controls.Add(this.lbl_meters_xml);
      this.gbox_Meters.Controls.Add(this.txt_meters_xml);
      this.gbox_Meters.Controls.Add(this.lbl_meters_machine_number);
      this.gbox_Meters.Controls.Add(this.txt_meters_machine_number);
      this.gbox_Meters.Controls.Add(this.lbl_meters_serialnumber);
      this.gbox_Meters.Controls.Add(this.txt_meters_serialnumber);
      this.gbox_Meters.Controls.Add(this.lbl_meters_vendorid);
      this.gbox_Meters.Controls.Add(this.txt_meters_vendorID);
      this.gbox_Meters.Location = new System.Drawing.Point(13, 5);
      this.gbox_Meters.Name = "gbox_Meters";
      this.gbox_Meters.Size = new System.Drawing.Size(438, 202);
      this.gbox_Meters.TabIndex = 0;
      this.gbox_Meters.TabStop = false;
      this.gbox_Meters.Text = "Meters";
      // 
      // lbl_meters_xml
      // 
      this.lbl_meters_xml.AutoSize = true;
      this.lbl_meters_xml.Location = new System.Drawing.Point(18, 114);
      this.lbl_meters_xml.Name = "lbl_meters_xml";
      this.lbl_meters_xml.Size = new System.Drawing.Size(64, 13);
      this.lbl_meters_xml.TabIndex = 7;
      this.lbl_meters_xml.Text = "Meters XML";
      // 
      // txt_meters_xml
      // 
      this.txt_meters_xml.Location = new System.Drawing.Point(16, 133);
      this.txt_meters_xml.Multiline = true;
      this.txt_meters_xml.Name = "txt_meters_xml";
      this.txt_meters_xml.Size = new System.Drawing.Size(416, 56);
      this.txt_meters_xml.TabIndex = 6;
      // 
      // lbl_meters_machine_number
      // 
      this.lbl_meters_machine_number.AutoSize = true;
      this.lbl_meters_machine_number.Location = new System.Drawing.Point(18, 66);
      this.lbl_meters_machine_number.Name = "lbl_meters_machine_number";
      this.lbl_meters_machine_number.Size = new System.Drawing.Size(88, 13);
      this.lbl_meters_machine_number.TabIndex = 5;
      this.lbl_meters_machine_number.Text = "Machine Number";
      // 
      // txt_meters_machine_number
      // 
      this.txt_meters_machine_number.Location = new System.Drawing.Point(16, 85);
      this.txt_meters_machine_number.Name = "txt_meters_machine_number";
      this.txt_meters_machine_number.Size = new System.Drawing.Size(187, 20);
      this.txt_meters_machine_number.TabIndex = 4;
      // 
      // lbl_meters_serialnumber
      // 
      this.lbl_meters_serialnumber.AutoSize = true;
      this.lbl_meters_serialnumber.Location = new System.Drawing.Point(217, 19);
      this.lbl_meters_serialnumber.Name = "lbl_meters_serialnumber";
      this.lbl_meters_serialnumber.Size = new System.Drawing.Size(73, 13);
      this.lbl_meters_serialnumber.TabIndex = 3;
      this.lbl_meters_serialnumber.Text = "Serial Number";
      // 
      // txt_meters_serialnumber
      // 
      this.txt_meters_serialnumber.Location = new System.Drawing.Point(220, 37);
      this.txt_meters_serialnumber.Name = "txt_meters_serialnumber";
      this.txt_meters_serialnumber.Size = new System.Drawing.Size(212, 20);
      this.txt_meters_serialnumber.TabIndex = 2;
      // 
      // lbl_meters_vendorid
      // 
      this.lbl_meters_vendorid.AutoSize = true;
      this.lbl_meters_vendorid.Location = new System.Drawing.Point(16, 19);
      this.lbl_meters_vendorid.Name = "lbl_meters_vendorid";
      this.lbl_meters_vendorid.Size = new System.Drawing.Size(52, 13);
      this.lbl_meters_vendorid.TabIndex = 1;
      this.lbl_meters_vendorid.Text = "VendorID";
      // 
      // txt_meters_vendorID
      // 
      this.txt_meters_vendorID.Location = new System.Drawing.Point(16, 38);
      this.txt_meters_vendorID.Name = "txt_meters_vendorID";
      this.txt_meters_vendorID.Size = new System.Drawing.Size(187, 20);
      this.txt_meters_vendorID.TabIndex = 0;
      // 
      // cmb_sp
      // 
      this.cmb_sp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_sp.FormattingEnabled = true;
      this.cmb_sp.Items.AddRange(new object[] {
            "SessionStart",
            "SessionUpdate",
            "SessionEnd",
            "AccountStatus",
            "ReportEvent",
            "ReportMeters",
            "AccountInfo",
            "CardInfo"});
      this.cmb_sp.Location = new System.Drawing.Point(6, 83);
      this.cmb_sp.Name = "cmb_sp";
      this.cmb_sp.Size = new System.Drawing.Size(207, 21);
      this.cmb_sp.TabIndex = 0;
      this.cmb_sp.SelectedIndexChanged += new System.EventHandler(this.cmb_sp_SelectedIndexChanged);
      // 
      // lbl_sp
      // 
      this.lbl_sp.AutoSize = true;
      this.lbl_sp.Location = new System.Drawing.Point(6, 67);
      this.lbl_sp.Name = "lbl_sp";
      this.lbl_sp.Size = new System.Drawing.Size(90, 13);
      this.lbl_sp.TabIndex = 1;
      this.lbl_sp.Text = "Stored Procedure";
      // 
      // btn_send
      // 
      this.btn_send.Location = new System.Drawing.Point(238, 389);
      this.btn_send.Name = "btn_send";
      this.btn_send.Size = new System.Drawing.Size(119, 34);
      this.btn_send.TabIndex = 118;
      this.btn_send.Text = "Send";
      this.btn_send.UseVisualStyleBackColor = true;
      this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
      // 
      // btn_clear_log
      // 
      this.btn_clear_log.Location = new System.Drawing.Point(1041, 389);
      this.btn_clear_log.Name = "btn_clear_log";
      this.btn_clear_log.Size = new System.Drawing.Size(119, 34);
      this.btn_clear_log.TabIndex = 119;
      this.btn_clear_log.Text = "Clear Log";
      this.btn_clear_log.UseVisualStyleBackColor = true;
      this.btn_clear_log.Click += new System.EventHandler(this.btn_clear_log_Click);
      // 
      // gbox_session_start
      // 
      this.gbox_session_start.Controls.Add(this.lbl_session_start_vendor_session_id);
      this.gbox_session_start.Controls.Add(this.txt_session_start_vendor_session_id);
      this.gbox_session_start.Controls.Add(this.lbl_session_start_machine_number);
      this.gbox_session_start.Controls.Add(this.txt_session_start_machine_number);
      this.gbox_session_start.Controls.Add(this.lbl_session_start_serial_number);
      this.gbox_session_start.Controls.Add(this.txt_session_start_serial_number);
      this.gbox_session_start.Controls.Add(this.lbl_session_start_vendor_id);
      this.gbox_session_start.Controls.Add(this.txt_session_start_vendor_id);
      this.gbox_session_start.Controls.Add(this.lbl_session_start_account_id);
      this.gbox_session_start.Controls.Add(this.txt_session_start_account_id);
      this.gbox_session_start.Location = new System.Drawing.Point(10, 9);
      this.gbox_session_start.Name = "gbox_session_start";
      this.gbox_session_start.Size = new System.Drawing.Size(438, 202);
      this.gbox_session_start.TabIndex = 2;
      this.gbox_session_start.TabStop = false;
      this.gbox_session_start.Text = "Session Start";
      // 
      // lbl_session_start_vendor_session_id
      // 
      this.lbl_session_start_vendor_session_id.AutoSize = true;
      this.lbl_session_start_vendor_session_id.Location = new System.Drawing.Point(11, 95);
      this.lbl_session_start_vendor_session_id.Name = "lbl_session_start_vendor_session_id";
      this.lbl_session_start_vendor_session_id.Size = new System.Drawing.Size(92, 13);
      this.lbl_session_start_vendor_session_id.TabIndex = 9;
      this.lbl_session_start_vendor_session_id.Text = "Vendor SessionID";
      // 
      // txt_session_start_vendor_session_id
      // 
      this.txt_session_start_vendor_session_id.Location = new System.Drawing.Point(11, 109);
      this.txt_session_start_vendor_session_id.Name = "txt_session_start_vendor_session_id";
      this.txt_session_start_vendor_session_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_start_vendor_session_id.TabIndex = 8;
      // 
      // lbl_session_start_machine_number
      // 
      this.lbl_session_start_machine_number.AutoSize = true;
      this.lbl_session_start_machine_number.Location = new System.Drawing.Point(228, 55);
      this.lbl_session_start_machine_number.Name = "lbl_session_start_machine_number";
      this.lbl_session_start_machine_number.Size = new System.Drawing.Size(88, 13);
      this.lbl_session_start_machine_number.TabIndex = 7;
      this.lbl_session_start_machine_number.Text = "Machine Number";
      // 
      // txt_session_start_machine_number
      // 
      this.txt_session_start_machine_number.Location = new System.Drawing.Point(228, 69);
      this.txt_session_start_machine_number.Name = "txt_session_start_machine_number";
      this.txt_session_start_machine_number.Size = new System.Drawing.Size(199, 20);
      this.txt_session_start_machine_number.TabIndex = 6;
      // 
      // lbl_session_start_serial_number
      // 
      this.lbl_session_start_serial_number.AutoSize = true;
      this.lbl_session_start_serial_number.Location = new System.Drawing.Point(11, 55);
      this.lbl_session_start_serial_number.Name = "lbl_session_start_serial_number";
      this.lbl_session_start_serial_number.Size = new System.Drawing.Size(73, 13);
      this.lbl_session_start_serial_number.TabIndex = 5;
      this.lbl_session_start_serial_number.Text = "Serial Number";
      // 
      // txt_session_start_serial_number
      // 
      this.txt_session_start_serial_number.Location = new System.Drawing.Point(11, 69);
      this.txt_session_start_serial_number.Name = "txt_session_start_serial_number";
      this.txt_session_start_serial_number.Size = new System.Drawing.Size(199, 20);
      this.txt_session_start_serial_number.TabIndex = 4;
      // 
      // lbl_session_start_vendor_id
      // 
      this.lbl_session_start_vendor_id.AutoSize = true;
      this.lbl_session_start_vendor_id.Location = new System.Drawing.Point(228, 17);
      this.lbl_session_start_vendor_id.Name = "lbl_session_start_vendor_id";
      this.lbl_session_start_vendor_id.Size = new System.Drawing.Size(52, 13);
      this.lbl_session_start_vendor_id.TabIndex = 3;
      this.lbl_session_start_vendor_id.Text = "VendorID";
      // 
      // txt_session_start_vendor_id
      // 
      this.txt_session_start_vendor_id.Location = new System.Drawing.Point(228, 31);
      this.txt_session_start_vendor_id.Name = "txt_session_start_vendor_id";
      this.txt_session_start_vendor_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_start_vendor_id.TabIndex = 2;
      // 
      // lbl_session_start_account_id
      // 
      this.lbl_session_start_account_id.AutoSize = true;
      this.lbl_session_start_account_id.Location = new System.Drawing.Point(11, 17);
      this.lbl_session_start_account_id.Name = "lbl_session_start_account_id";
      this.lbl_session_start_account_id.Size = new System.Drawing.Size(58, 13);
      this.lbl_session_start_account_id.TabIndex = 1;
      this.lbl_session_start_account_id.Text = "AccountID";
      // 
      // txt_session_start_account_id
      // 
      this.txt_session_start_account_id.Location = new System.Drawing.Point(11, 31);
      this.txt_session_start_account_id.Name = "txt_session_start_account_id";
      this.txt_session_start_account_id.Size = new System.Drawing.Size(199, 20);
      this.txt_session_start_account_id.TabIndex = 0;
      // 
      // frm_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1176, 443);
      this.Controls.Add(this.btn_clear_log);
      this.Controls.Add(this.btn_send);
      this.Controls.Add(this.tab_simulator);
      this.Controls.Add(this.lbl_sim_status);
      this.Controls.Add(this.m_btn_start);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frm_simulator";
      this.Text = "Alesis Client Simulator";
      this.Load += new System.EventHandler(this.frm_simulator_Load);
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.tab_simulator.ResumeLayout(false);
      this.tab_parameters.ResumeLayout(false);
      this.tab_parameters.PerformLayout();
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.tab_statistics.ResumeLayout(false);
      this.tab_ws.ResumeLayout(false);
      this.gbox_log.ResumeLayout(false);
      this.gbox_ws.ResumeLayout(false);
      this.gbox_ws.PerformLayout();
      this.pnl_start_session.ResumeLayout(false);
      this.pnl_account_info.ResumeLayout(false);
      this.groupBox7.ResumeLayout(false);
      this.groupBox7.PerformLayout();
      this.gb_connection.ResumeLayout(false);
      this.gb_connection.PerformLayout();
      this.pnl_container.ResumeLayout(false);
      this.pnl_Card_Info.ResumeLayout(false);
      this.groupBox8.ResumeLayout(false);
      this.groupBox8.PerformLayout();
      this.pnl_session_update.ResumeLayout(false);
      this.gbox_session_update.ResumeLayout(false);
      this.gbox_session_update.PerformLayout();
      this.pnl_report_event.ResumeLayout(false);
      this.gbox_report_event.ResumeLayout(false);
      this.gbox_report_event.PerformLayout();
      this.pnl_session_end.ResumeLayout(false);
      this.gbox_session_end.ResumeLayout(false);
      this.gbox_session_end.PerformLayout();
      this.pnl_account_status.ResumeLayout(false);
      this.gbox_account_status.ResumeLayout(false);
      this.gbox_account_status.PerformLayout();
      this.pnl_Report_Meters.ResumeLayout(false);
      this.gbox_Meters.ResumeLayout(false);
      this.gbox_Meters.PerformLayout();
      this.gbox_session_start.ResumeLayout(false);
      this.gbox_session_start.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.TextBox txt_db_pass;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox txt_db_user;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txt_db_name;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox txt_vendor_id;
    private System.Windows.Forms.TextBox txt_ip_address_1;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.TextBox txt_card_track_data;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox txt_machine_num;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txt_ip_address_2;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox txt_encrypted_card_track_data;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Timer tmr_refresh;
    private System.Windows.Forms.Button m_btn_start;
    private System.Windows.Forms.Timer tmr_stats;
    private System.Windows.Forms.TextBox txt_num_terminals;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label lbl_statistics_1;
    private System.Windows.Forms.Label lbl_statistics_5;
    private System.Windows.Forms.Label lbl_statistics_avg;
    private System.Windows.Forms.Label lbl_head;
    private System.Windows.Forms.Label lbl_sim_status;
    private System.Windows.Forms.Label lbl_totals;
    private System.Windows.Forms.Label lbl_head_totals;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TabControl tab_simulator;
    private System.Windows.Forms.TabPage tab_parameters;
    private System.Windows.Forms.TabPage tab_statistics;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox txt_min_session_plays;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox txt_max_session_plays;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox txt_max_delay_between_sessions;
    private System.Windows.Forms.TextBox txt_min_delay_between_sessions;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.TextBox txt_max_delay_between_plays;
    private System.Windows.Forms.TextBox txt_min_delay_between_plays;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.ComboBox cmb_serial_num;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.CheckBox chk_report_jackpot;
    private System.Windows.Forms.CheckBox chk_account_status;
    private System.Windows.Forms.Button btn_set_terminals;
    private System.Windows.Forms.Button btn_set_accounts;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox txt_min_update_x_sec;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.TextBox txt_max_plays_x_update;
    private System.Windows.Forms.TabPage tab_ws;
    private System.Windows.Forms.GroupBox gbox_ws;
    private System.Windows.Forms.Panel pnl_container;
    private System.Windows.Forms.Panel pnl_session_update;
    private System.Windows.Forms.Panel pnl_session_end;
    private System.Windows.Forms.Panel pnl_account_status;
    private System.Windows.Forms.Panel pnl_report_event;
    private System.Windows.Forms.ComboBox cmb_sp;
    private System.Windows.Forms.Label lbl_sp;
    private System.Windows.Forms.Button btn_random;
    private System.Windows.Forms.Button btn_send;
    private System.Windows.Forms.Button btn_clear_log;
    private System.Windows.Forms.GroupBox gbox_session_end;
    private System.Windows.Forms.GroupBox gbox_session_update;
    private System.Windows.Forms.GroupBox gbox_account_status;
    private System.Windows.Forms.GroupBox gbox_report_event;
    private System.Windows.Forms.Label lbl_account_status_vendor_id;
    private System.Windows.Forms.TextBox txt_account_status_vendor_id;
    private System.Windows.Forms.Label lbl_account_status_account_id;
    private System.Windows.Forms.TextBox txt_account_status_account_id;
    private System.Windows.Forms.Label lbl_session_end_bill_in;
    private System.Windows.Forms.TextBox txt_session_end_bill_in;
    private System.Windows.Forms.Label lbl_session_end_games_won;
    private System.Windows.Forms.TextBox txt_session_end_games_won;
    private System.Windows.Forms.Label lbl_session_end_games_played;
    private System.Windows.Forms.TextBox txt_session_end_games_played;
    private System.Windows.Forms.Label lbl_session_end_credits_won;
    private System.Windows.Forms.TextBox txt_session_end_credits_won;
    private System.Windows.Forms.Label lbl_session_end_credits_played;
    private System.Windows.Forms.TextBox txt_session_end_credits_played;
    private System.Windows.Forms.Label lbl_session_end_session_id;
    private System.Windows.Forms.TextBox txt_session_end_session_id;
    private System.Windows.Forms.Label lbl_session_end_vendor_id;
    private System.Windows.Forms.TextBox txt_session_end_vendor_id;
    private System.Windows.Forms.Label lbl_session_update_bill_in;
    private System.Windows.Forms.TextBox txt_session_update_bill_in;
    private System.Windows.Forms.Label lbl_session_update_games_won;
    private System.Windows.Forms.TextBox txt_session_update_games_won;
    private System.Windows.Forms.Label lbl_session_update_games_played;
    private System.Windows.Forms.TextBox txt_session_update_games_played;
    private System.Windows.Forms.Label lbl_session_update_credits_won;
    private System.Windows.Forms.TextBox txt_session_update_credits_won;
    private System.Windows.Forms.Label lbl_session_update_credits_played;
    private System.Windows.Forms.TextBox txt_session_update_credits_played;
    private System.Windows.Forms.Label lbl_session_update_session_id;
    private System.Windows.Forms.TextBox txt_session_update_session_id;
    private System.Windows.Forms.Label lbl_session_update_vendor_id;
    private System.Windows.Forms.TextBox txt_session_update_vendor_id;
    private System.Windows.Forms.Label lbl_report_event_payout_amount;
    private System.Windows.Forms.TextBox txt_report_event_payout_amount;
    private System.Windows.Forms.Label lbl_report_event_machine_number;
    private System.Windows.Forms.TextBox txt_report_event_machine_number;
    private System.Windows.Forms.Label lbl_report_event_serial_number;
    private System.Windows.Forms.TextBox txt_report_event_serial_number;
    private System.Windows.Forms.Label lbl_report_event_vendor_id;
    private System.Windows.Forms.TextBox txt_report_event_vendor_id;
    private System.Windows.Forms.Label lbl_report_event_event_id;
    private System.Windows.Forms.TextBox txt_report_event_event_id;
    private System.Windows.Forms.GroupBox gbox_log;
    private System.Windows.Forms.RichTextBox txt_ws_log;
    private System.Windows.Forms.Label lbl_session_update_credits_balance;
    private System.Windows.Forms.TextBox txt_session_update_credits_balance;
    private System.Windows.Forms.Label lbl_session_end_credits_balance;
    private System.Windows.Forms.TextBox txt_session_end_credits_balance;
    private System.Windows.Forms.Label lbl_report_event_session_id;
    private System.Windows.Forms.TextBox txt_report_event_session_id;
    private System.Windows.Forms.Label lbl_ws_url;
    private System.Windows.Forms.TextBox txt_IPadress;
    private System.Windows.Forms.GroupBox gb_connection;
    private System.Windows.Forms.RadioButton rd_zsp;
    private System.Windows.Forms.RadioButton rd_s2s;
    private System.Windows.Forms.Panel pnl_Report_Meters;
    private System.Windows.Forms.GroupBox gbox_Meters;
    private System.Windows.Forms.Label lbl_meters_machine_number;
    private System.Windows.Forms.TextBox txt_meters_machine_number;
    private System.Windows.Forms.Label lbl_meters_serialnumber;
    private System.Windows.Forms.TextBox txt_meters_serialnumber;
    private System.Windows.Forms.Label lbl_meters_vendorid;
    private System.Windows.Forms.TextBox txt_meters_vendorID;
    private System.Windows.Forms.Label lbl_meters_xml;
    private System.Windows.Forms.TextBox txt_meters_xml;
    private System.Windows.Forms.RadioButton rd_ws;
    private System.Windows.Forms.Panel pnl_account_info;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.Label label30;
    private System.Windows.Forms.TextBox pnl_info_txt_track_data;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox pnl_info_txt_vendor_id;
    private System.Windows.Forms.Panel pnl_Card_Info;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.TextBox pnl_card_info_track_data;
    private System.Windows.Forms.Panel pnl_start_session;
    private System.Windows.Forms.GroupBox gbox_session_start;
    private System.Windows.Forms.Label lbl_session_start_vendor_session_id;
    private System.Windows.Forms.TextBox txt_session_start_vendor_session_id;
    private System.Windows.Forms.Label lbl_session_start_machine_number;
    private System.Windows.Forms.TextBox txt_session_start_machine_number;
    private System.Windows.Forms.Label lbl_session_start_serial_number;
    private System.Windows.Forms.TextBox txt_session_start_serial_number;
    private System.Windows.Forms.Label lbl_session_start_vendor_id;
    private System.Windows.Forms.TextBox txt_session_start_vendor_id;
    private System.Windows.Forms.Label lbl_session_start_account_id;
    private System.Windows.Forms.TextBox txt_session_start_account_id;
  }
}

