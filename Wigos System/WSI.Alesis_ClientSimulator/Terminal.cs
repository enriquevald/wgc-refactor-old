//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Terminal.cs
// 
//   DESCRIPTION: Implements terminal parameters and connection
//
//        AUTHOR: 
// 
// CREATION DATE: 30-APR-2010 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-APR-2010 MBF   First version
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Alesis_ClientSimulator
{
  public enum WCP_PlaySessionType
  {
    NONE = 0,
    CADILLAC_JACK = 1,
    WIN = 2,
    ALESIS = 3,
  }

  public class Terminal
  {
    private static Random m_random = new Random(Environment.TickCount);

    private System.Threading.Thread m_thread = null;

    private Random m_rng = new Random(m_random.Next());

    public String m_connection_string;
    public String m_vendor_id;
    public Int32 m_machine_id;
    public String m_serial_number;
    public String m_track_data;

    public Int64 session_id;

    public Int32 terminal_id;
    public String name;

    public bool simulating;
    private bool m_session_started;

    // Statistics
    public UInt64 m_sum_started;
    public UInt64 m_sum_plays;
    public UInt64 m_sum_updates;
    public UInt64 m_sum_finished;

    // SQL
    public UInt64 m_sql_net_time;
    public UInt64 m_sql_cpu_time;


    public UInt64 m_sum_interval_start;
    public UInt64 m_sum_interval_update;
    public UInt64 m_sum_interval_end;
    public int m_tick_update;

    public Int64 play_session_id;

    public Int32 total_played_count;
    public Decimal total_played_amount;
    public Int32 total_won_count;
    public Decimal total_won_amount;
    public Decimal initial_balance;
    public Decimal current_balance;

    private Int32 m_min_session_plays;
    private Int32 m_max_session_plays;
    private Int32 m_min_delay_between_plays;
    private Int32 m_max_delay_between_plays;
    private Int32 m_min_delay_between_sessions;
    private Int32 m_max_delay_between_sessions;

    private Int32 m_max_plays_x_update;
    private Int32 m_min_updates_per_sec;

    private Boolean m_exec_report_jackpot;
    private Boolean m_exec_account_status;

    public SqlConnection sql_connection;


    private static System.Threading.ReaderWriterLock m_rw_lock = new System.Threading.ReaderWriterLock();
    private static SqlConnection m_sql_connection;
    private static String m_unique_connection_string = null;

    public SqlConnection LockConnection()
    {
      if (String.IsNullOrEmpty(m_unique_connection_string))
      {
        return sql_connection;
      }


      m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

      if (m_sql_connection == null)
      {
        try
        {
          m_sql_connection = new SqlConnection(m_unique_connection_string);
          m_sql_connection.Open();

          return m_sql_connection;
        }
        catch (Exception)
        {
          try { m_sql_connection.Close(); }
          catch { ;}
          try { m_sql_connection = null; }
          catch { ;}
        }
      }

      if (m_sql_connection != null)
      {
        return m_sql_connection;
      }

      return null;
    }

    public void UnlockConnection(SqlConnection Conn)
    {
      if (String.IsNullOrEmpty(m_unique_connection_string))
      {
        return;
      }

      m_rw_lock.ReleaseWriterLock();
    }


    public void SetParameters(String ConnectionString,
                               String VendorID,
                               Int32 MachineID,
                               String SerialNumber,
                               String TrackData,
                               Int32 MinSessionPlays,
                               Int32 MaxSessionPlays,
                               Int32 MinDelayBetweenPlays,
                               Int32 MaxDelayBetweenPlays,
                               Int32 MinDelayBetweenSessions,
                               Int32 MaxDelayBetweenSessions,
                               Int32 MaxPlaysPerUpdate,
                               Int32 MinUpdatesPerSec,
                               Boolean ExecReportJackpot,
                               Boolean ExecAccountStatus)
    {
      if (String.IsNullOrEmpty(m_unique_connection_string))
      {
        //m_unique_connection_string = ConnectionString;
      }
      m_connection_string = ConnectionString;
      m_vendor_id = VendorID;
      m_machine_id = MachineID;
      m_serial_number = SerialNumber;
      m_track_data = TrackData;

      m_min_session_plays = MinSessionPlays;
      m_max_session_plays = MaxSessionPlays;
      m_min_delay_between_plays = MinDelayBetweenPlays;
      m_max_delay_between_plays = MaxDelayBetweenPlays;
      m_min_delay_between_sessions = MinDelayBetweenSessions;
      m_max_delay_between_sessions = MaxDelayBetweenSessions;


      m_max_plays_x_update = MaxPlaysPerUpdate;
      m_min_updates_per_sec = MinUpdatesPerSec;

      m_exec_report_jackpot = ExecReportJackpot;
      m_exec_account_status = ExecAccountStatus;
    }

    public Terminal()
    {
      sql_connection = null;
      m_connection_string = null;
      play_session_id = 0;
      total_played_count = 0;
      total_played_amount = 0;
      total_won_count = 0;
      total_won_amount = 0;
      current_balance = 0;

      m_sum_finished = 0;
      m_sum_started = 0;
      m_sum_plays = 0;
      m_sum_updates = 0;

      m_sum_interval_start = 0;
      m_sum_interval_update = 0;
      m_sum_interval_end = 0;
    }

    public static String EncryptTrackData(String InternalTrackData)
    {
      String _external_trackdata;
      Boolean rc;

      rc = WSI.Common.CardNumber.TrackDataToExternal(out _external_trackdata, InternalTrackData, 0);

      return _external_trackdata;
    }

    public static String DecryptTrackData(String ExternalTrackData)
    {
      String _internal_trackdata;
      Boolean rc;
      int card_type;

      rc = WSI.Common.CardNumber.TrackDataToInternal(ExternalTrackData, out _internal_trackdata, out card_type);

      return _internal_trackdata;
    }

    internal bool StartCardSession()
    {
      int _tick;

      _tick = Misc.GetTickCount();

      if (Procedures.StartCardSession(this) != StatusCode.Success)
      {
        return false;
      }

      m_sum_interval_start += (UInt64)Misc.GetElapsedTicks(_tick);
      this.m_sum_started++;

      this.initial_balance = this.current_balance;
      this.total_played_amount = 0;
      this.total_played_count = 0;
      this.total_won_amount = 0;
      this.total_won_count = 0;

      return true;
    }

    internal bool Connect()
    {
      if (!String.IsNullOrEmpty(m_unique_connection_string))
      {
        return true;
      }
      // Already connected.
      if (sql_connection != null)
      {
        if (sql_connection.State == System.Data.ConnectionState.Open)
        {
          return true;
        }
        sql_connection.Close();
        sql_connection.Dispose();
        sql_connection = null;
      }
      if (m_connection_string == null)
      {
        return false;
      }
      if (m_connection_string.Equals(""))
      {
        return false;
      }


      sql_connection = new SqlConnection(m_connection_string);
      if (sql_connection == null)
      {
        return false;
      }

      try
      {
        sql_connection.Open();
        sql_connection.StatisticsEnabled = true;


        //foreach (sql_connection.RetrieveStatistics();
      }
      catch
      {
        return false;
      }

      return true;
    }

    internal bool Play()
    {
      Decimal _bet_amount;
      int _tick;
      int _random;
      int _multiplier;
      Decimal _won;

      _bet_amount = 1;
      _random = this.m_rng.Next(0, 100);
      switch (_random)
      {
        case 0:
          _multiplier = 500;  // 1 x 500  = 500
          break;

        case 10:
        case 11:
          _multiplier = 100;  // 2 x 100  = 200
          break;

        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
        case 27:
        case 28:
        case 29:
          _multiplier = 10;  // 10 x 10  = 100
          break;

        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
        case 35:
        case 36:
        case 37:
        case 38:
        case 39:
        case 40:
        case 41:
        case 42:
        case 43:
        case 44:
        case 45:
        case 46:
        case 47:
        case 48:
        case 49:
          _multiplier = 5;  // 20 x 5  = 100
          break;

        default:
          _multiplier = 0;
          break;
      }

      _won = _bet_amount * _multiplier;

      if (!PlayAllowed(_bet_amount))
      {
        return false;
      }
      total_played_amount += _bet_amount;
      total_played_count += 1;

      if (_won > 0)
      {
        //
        // Extra Ball
        //
        _random = this.m_rng.Next(0, 10);
        switch (_random)
        {
          case 0:
            _bet_amount += _won;
            break;

          case 1:
            _bet_amount += _won / 2;
            break;

          case 2:
            _bet_amount += _won / 10;
            break;

          default:
            _bet_amount += 0;
            break;
        }

        total_won_amount += _won;
        total_won_count += 1;
      }

      current_balance = initial_balance - total_played_amount + total_won_amount;

      Boolean _update;

      _update = false;
      if (m_max_plays_x_update == 0)
      {
        _update = true;
      }
      if (!_update)
      {
        _update = (total_played_count % m_max_plays_x_update == 0);
      }
      if (!_update)
      {
        _update = (Misc.GetElapsedTicks(m_tick_update) >= m_min_updates_per_sec * 1000);
      }

      if (_update)
      {
        _tick = Misc.GetTickCount();

        if (Procedures.ReportPlay(this) != StatusCode.Success)
        {
          return false;
        }

        m_sum_interval_update += (UInt64)Misc.GetElapsedTicks(_tick);
        m_sum_updates++;
        m_tick_update = Misc.GetTickCount();
      }
      this.m_sum_plays++;

      return true;
    }

    internal bool PlayAllowed(Decimal BetAmount)
    {
      if (current_balance >= BetAmount)
      {
        return true;
      }
      return false;
    }

    void SimulateThread()
    {
      int _delay;
      int _max_plays;
      Random _rnd;
      int _play_delay;
      int _start_delay;

      _rnd = new Random(Environment.TickCount);

      _delay = 0;

      while (true)
      {
        try
        {
          System.Threading.Thread.Sleep(_delay);
        }
        catch
        {
          m_session_started = false;
          // Finish thread
          return;
        }
        _delay = 1000;

        if (!simulating)
        {
          continue;
        }
        if (!Connect())
        {
          continue;
        }

        _start_delay = m_min_delay_between_sessions;
        if (m_max_delay_between_sessions > m_min_delay_between_sessions)
        {
          _start_delay += _rnd.Next(m_max_delay_between_sessions - m_min_delay_between_sessions);
        }

        try
        {
          System.Threading.Thread.Sleep(_start_delay);
        }
        catch
        {
          // Finish thread
          return;
        }

        if (m_exec_account_status)
        {
          // Account is not free
          if (AccountStatus() != StatusCode.Success)
          {
            continue;
          }
        }

        if (!StartCardSession())
        {
          continue;
        }

        m_session_started = true;

        _max_plays = 1;

        try
        {
          _max_plays = m_min_session_plays;
          if (m_max_session_plays > m_min_session_plays)
          {
            _max_plays += _rnd.Next(m_max_session_plays - m_min_session_plays);
          }

          while (total_played_count < _max_plays)
          {
            if (!simulating)
            {
              break;
            }

            _play_delay = m_min_delay_between_plays;
            if (m_max_delay_between_plays > m_min_delay_between_plays)
            {
              _play_delay += _rnd.Next(m_max_delay_between_plays - m_min_delay_between_plays);
            }

            try
            {
              System.Threading.Thread.Sleep(_play_delay);
            }
            catch
            {
              // Finish thread
              return;
            }

            if (!Play())
            {
              break;
            }

            if (m_exec_report_jackpot)
            {
              // report events at the middle of the session 
              if (total_played_count == _max_plays / 2)
              {
                // Jackpot between 1.000 and 10.000
                if (!ReportEvents(_rnd.Next(1000, 10000)))
                {
                  break;
                }
              }
            }
          }
        }
        finally
        {
          EndCardSession();
          m_session_started = false;
          if (total_played_count >= _max_plays)
          {
            _delay = 0;
          }
        }
      }
    }

    internal StatusCode AccountStatus()
    {
      return Procedures.AccountStatus(this);
    }

    internal bool ReportEvents(decimal Jackpot)
    {
      if (Procedures.ReportEvent(this, SendEventCode.JackpotWon, Jackpot) != StatusCode.Success)
      {
        return false;
      }

      if (Procedures.ReportEvent(this, SendEventCode.KioskBlocked, Jackpot) != StatusCode.Success)
      {
        return false;
      }

      if (Procedures.ReportEvent(this, SendEventCode.CallAttendant, 0) != StatusCode.Success)
      {
        return false;
      }

      return true;
    }

    internal void EndCardSession()
    {
      int _tick;

      if (this.play_session_id != 0)
      {
        _tick = Misc.GetTickCount();

        if (Procedures.EndCardSession(this) == StatusCode.Success)
        {
          m_sum_interval_end += (UInt64)Misc.GetElapsedTicks(_tick);
          this.m_sum_finished++;
        }
        this.play_session_id = 0;
      }
    }

    internal void StartSimulation()
    {
      if (m_thread == null)
      {
        m_thread = new System.Threading.Thread(new System.Threading.ThreadStart(SimulateThread));
        m_thread.Start();
      }
      simulating = true;
    }

    internal void StopSimulation()
    {
      simulating = false;
    }

    internal void Shutdown()
    {
      StopSimulation();
      m_thread.Interrupt();

      if (m_thread != null)
      {
        if (!m_thread.Join(1000))
        {
          m_thread.Abort();
        }
        m_thread = null;
      }

      if (sql_connection != null)
      {
        sql_connection.Close();
        sql_connection.Dispose();
        sql_connection = null;
      }
    }

    internal bool Connected()
    {
      if (sql_connection != null)
      {
        return sql_connection.State == System.Data.ConnectionState.Open;
      }
      return false;
    }

    internal bool SessionStarted()
    {
      return m_session_started;
    }

  }
}
