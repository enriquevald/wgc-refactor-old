//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Simulator.cs
// 
//   DESCRIPTION: Implements simulation threads
//
//        AUTHOR: 
// 
// CREATION DATE: 04-MAY-2010 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2010 AJ   First version
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections;

using System.Text;

namespace WSI.Alesis_ClientSimulator
{
  public static class Simulator
  {
    public struct Statistics
    {
      public int interval;
      
      public int num_terminals;
      public int num_connected;
      public int num_started;

      public UInt64 sum_started;
      public UInt64 sum_plays;
      public UInt64 sum_updates;
      public UInt64 sum_finished;

      public UInt64 sum_interval_start;
      public UInt64 sum_interval_update;
      public UInt64 sum_interval_end;

      public UInt64 sum_sql_net;
      public UInt64 sum_sql_cpu;


    };
    
    static ArrayList m_terminals = new ArrayList();
    
    public static void Start (int NumTerminals, 
                              String ConnectionString, 
                              String VendorID, 
                              Int32  MachineID, 
                              String SerialNumber, 
                              String TrackData,
                              Int32  MinSessionPlays,
                              Int32  MaxSessionPlays,
                              Int32  MinDelayBetweenPlays,
                              Int32  MaxDelayBetweenPlays,
                              Int32  MinDelayBetweenSessions,
                              Int32  MaxDelayBetweenSessions,
                              Int32 MaxPlaysPerUpdate,
                              Int32 MinUpdatesPerSec,
                              Boolean ExecReportJackpot,
                              Boolean ExecAccountStatus)
    {
      int    _idx;
      UInt64 _track_id;
      String _track_data;
      Int32  _machine_id;
      String _serial_number;
      String _external_track_data;

      // Stop
      Stop ();
      
      // Create missing terminals
      while ( m_terminals.Count < NumTerminals )
      {
        Terminal _terminal;
        _terminal = new Terminal ();
        m_terminals.Add(_terminal);
      }
      
      // Remove terminals
      while ( m_terminals.Count > NumTerminals )
      {
        Terminal _terminal;
        _terminal = (Terminal) m_terminals[m_terminals.Count - 1];
        _terminal.Shutdown();
        m_terminals.Remove(_terminal);
        _terminal = null;
      }

      _idx = 0;
      foreach ( Terminal _terminal in m_terminals )
      {
        // Set Track Data
        _track_id   = 0;
        UInt64.TryParse(TrackData, out _track_id);
        _track_id  += (UInt64)_idx;
        _track_data = _track_id.ToString("0000000000000");

        _external_track_data = Terminal.EncryptTrackData(_track_data);
        
        // Machine ID
        _machine_id = MachineID + _idx;
        
        // Serial Number
        _serial_number = SerialNumber + "_" +_idx.ToString("000");
        
        _terminal.SetParameters (ConnectionString, 
                                 VendorID,
                                 _machine_id,
                                 _serial_number,
                                 _external_track_data,
                                 MinSessionPlays,
                                 MaxSessionPlays,
                                 MinDelayBetweenPlays,
                                 MaxDelayBetweenPlays,
                                 MinDelayBetweenSessions,
                                 MaxDelayBetweenSessions,
                                 MaxPlaysPerUpdate,
                                 MinUpdatesPerSec,
                                 ExecReportJackpot,
                                 ExecAccountStatus);
        _idx++;
      }
      
      System.Threading.Thread _thread;
      _thread = new System.Threading.Thread(new System.Threading.ThreadStart(StartSimThread));
      _thread.Start ();
    }
    
    private static void StartSimThread ()
    {
      foreach (Terminal _terminal in m_terminals)
      {
        _terminal.StartSimulation ();
      }
    }

    public static void Stop ()
    {
      foreach (Terminal _terminal in m_terminals)
      {
        _terminal.StopSimulation ();
      }
    }
    
    public static Statistics QueryCounters ()
    {
      Statistics _stats;
      
      _stats = new Statistics ();
      _stats.num_terminals = m_terminals.Count;
      _stats.num_connected = 0;
      _stats.num_started   = 0;
      _stats.sum_started   = 0;
      _stats.sum_plays = 0;
      _stats.sum_updates = 0;
      _stats.sum_finished = 0;

      _stats.sum_sql_cpu = 0;
      _stats.sum_sql_net = 0;


      _stats.sum_interval_start = 0;
      _stats.sum_interval_update = 0;
      _stats.sum_interval_end = 0;

      foreach (Terminal _terminal in m_terminals)
      {
        if ( _terminal.Connected () )
        {
          _stats.num_connected += 1;
        }
        if ( _terminal.SessionStarted () )
        {
          _stats.num_started += 1;
        }
        _stats.sum_started   += _terminal.m_sum_started;
        _stats.sum_plays += _terminal.m_sum_plays;
        _stats.sum_updates += _terminal.m_sum_updates;

        _stats.sum_finished  += _terminal.m_sum_finished;

        _stats.sum_interval_start   += _terminal.m_sum_interval_start;
        _stats.sum_interval_update  += _terminal.m_sum_interval_update;
        _stats.sum_interval_end     += _terminal.m_sum_interval_end;

        _stats.sum_sql_cpu += _terminal.m_sql_cpu_time;
        _stats.sum_sql_net += _terminal.m_sql_net_time;
      }
      
      return _stats;
    }
    
    public static Statistics DiffCounters (Statistics NewValues, Statistics OldValues, int Milliseconds)
    {
      Simulator.Statistics _diff;
      
      _diff = new Simulator.Statistics ();
      _diff.interval      = Milliseconds;
      _diff.num_connected = NewValues.num_connected;
      _diff.num_terminals = NewValues.num_terminals;
      _diff.num_started   = NewValues.num_started;
      _diff.sum_finished  = NewValues.sum_finished - OldValues.sum_finished;
      _diff.sum_plays     = NewValues.sum_plays - OldValues.sum_plays;
      _diff.sum_updates   = NewValues.sum_updates - OldValues.sum_updates;

      _diff.sum_started   = NewValues.sum_started  - OldValues.sum_started;

      _diff.sum_interval_start = NewValues.sum_interval_start - OldValues.sum_interval_start;
      _diff.sum_interval_update  = NewValues.sum_interval_update  - OldValues.sum_interval_update;
      _diff.sum_interval_end   = NewValues.sum_interval_end   - OldValues.sum_interval_end;

      _diff.sum_sql_cpu = NewValues.sum_sql_cpu - OldValues.sum_sql_cpu;
      _diff.sum_sql_net = NewValues.sum_sql_net - OldValues.sum_sql_net;

      return _diff;
    }

    public static void WaitForTerminalFinish()
    {
      Boolean _still_simulating;

      do
      {
        _still_simulating = false;
        foreach (Terminal _terminal in m_terminals)
        {
          if (_terminal.SessionStarted())
          {
            System.Threading.Thread.Sleep(100);
            _still_simulating = true;
            break;
          }
        }
      }
      while (_still_simulating);
    }
  }
}
