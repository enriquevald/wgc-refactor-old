﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WSI.Cashier.WebApi.Client.Tests
{
  [TestClass]
  public class SessionTests
  {
    [TestMethod]
    public void OpenSessionTest()
    {
      SessionServices service = new SessionServices();
      var sessionOpened = service.OpenSession("WebApiSession14", DateTime.Now);
      service.Dispose();
    }

    [TestMethod]
    public void GetSessionListTest()
    {
      SessionServices service = new SessionServices();
      var sessions = service.GetSessions();
      service.Dispose();
    }
  }
}
