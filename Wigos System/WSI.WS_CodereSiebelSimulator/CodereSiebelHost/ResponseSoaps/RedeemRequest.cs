﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using HttpClient;
using System.Text.RegularExpressions;
using System.Configuration;

namespace CodereSiebelHost.ResponseSoaps
{

  public class Voucher
  {
    public String VoucherId;
    public Boolean Reedemed;
  }

  public class RedeemRequest : IResponse
  {


    public string GetSoapResponse(RequestParameters Params)
    {
      String _response;
      String _error_spcCode;
      String _error_spcMessage;
      String _records;
      String _xml_format;
      LoyTransaction[] _loyalty_transaction;
      Int32 _read_vouchers_one_time;

      _read_vouchers_one_time = Int32.Parse(ConfigurationManager.AppSettings["ReadVouchersOneTime"].ToString());

      _response = String.Empty;
      _error_spcCode = String.Empty;
      _error_spcMessage = String.Empty;
      _records = String.Empty;
      _loyalty_transaction = null;

      if (Params.AccountId != String.Empty)
      {
        Random _rnd_monto = new Random();
        Random _rnd_point = new Random();
        Random _rnd_voucher = new Random();

        _error_spcCode = "OK";
        _error_spcMessage = String.Empty;

        LoyTransaction _xlist;
        //List<String> _vouchers_id;
        //_vouchers_id = GetVouchersId();
        if (_read_vouchers_one_time == 0)
        {
          if (!Program.ReadVouchersFromFile())
          {
            return String.Empty;
          }
        }

        //_loyalty_transaction = new LoyTransaction[Program.m_matches_vouchers.Count];
        _loyalty_transaction = new LoyTransaction[Program.m_matches_vouchers.Length];

        for (Int32 _idx = 0; _idx < Program.m_matches_vouchers.Length; _idx++)
        {
          _xlist = new LoyTransaction();
          _xlist.MemberNumber = Params.AccountId;
          //_xlist.Monto = (_rnd_monto.Next(10, 500) * 10).ToString();
          switch (Program.m_monto_test_index)
          { 
            case 0:
              _xlist.Monto = _rnd_monto.Next(10, 50).ToString();
              break;
            case 1:
              _xlist.Monto = _rnd_monto.Next(10, 50).ToString() + "." + _rnd_monto.Next(1, 50).ToString("00");
              break;
            case 2:
            default:
              _xlist.Monto = _rnd_monto.Next(10, 50).ToString() + "," + _rnd_monto.Next(51, 99).ToString("00");
              break;
          }
          _xlist.Points = _rnd_point.Next(10, 1000).ToString();
          _xlist.ProductName = "Fondos Disponibles 50";


          if (!this.UseRedeemVoucherGroups())
          {
            if (_rnd_monto.Next(1, 10) < 6)
            {
              _xlist.ProductType = "Fondo Disponible";
            }
            else
            {
              _xlist.ProductType = "Efectivo";
            }
          }
          else
          {
              _xlist.ProductType = "Fondo Disponible Grupo " + _rnd_monto.Next(1, 9).ToString();
          }

          _xlist.TransacCanjeada = "N";
          _xlist.TransactionDate = String.Format("{0:dd/MM/yyyy HH:mm:ss}", new DateTime());
          _xlist.TransactionNumber = Program.m_matches_vouchers[_idx].ToString().Replace(":0\r", "");// _vouchers_id[_idx];

          _loyalty_transaction[_idx] = _xlist;
        }

        Program.m_monto_test_index = (Program.m_monto_test_index + 1) % 3;
        _records = "1";

        try
        {
          _xml_format = String.Empty;
          if (Format_ListOfLoyTransactionIo(_loyalty_transaction, out _xml_format))
          {
            //_response = System.IO.File.ReadAllText(Program.m_path + "ReedemResponse.xml");
            _response = Program.M_REEDEM_RESPONSE_XML;
            _response = _response.Replace("&p0", _error_spcCode).Replace("&p1", _error_spcMessage).Replace("&p2", _xml_format);
          }
        }
        catch (Exception ex)
        {
          Console.WriteLine("Exception: " + ex.Message);
        }
      }
      else
      {
        try
        {
          //_response = System.IO.File.ReadAllText(Program.m_path + "ReedemResponseError.xml");
          _response = Program.M_REEDEM_RESPONSE_ERROR_XML;
        }
        catch (Exception ex)
        {
          Console.WriteLine("Exception: " + ex.Message);
        }
      }

      return _response;
      //throw new NotImplementedException();

    }



    public string GetSoapError(int ErrorCode)
    {
      //String _soap = System.IO.File.ReadAllText(Program.m_path + "ReedemResponse.xml");
      String _soap = Program.M_REEDEM_RESPONSE_XML;

      _soap = _soap.Replace("&p0", "KO");
      _soap = _soap.Replace("&p1", "Ha ocurrido un error en la busqueda del Miembro. Por favor pongase en contacto con el administrador.");
      _soap = _soap.Replace("&p2", "");

      return _soap;
    }


    #region Private Methods
    private List<String> GetVouchersId()
    {
      List<String> _voucher_list = new List<String>();
      try
      {
        MatchCollection _matches;
        _matches = Regex.Matches(File.ReadAllText(Program.m_path + "VoucherIds.txt"), @"(\d-\d\d\d\d\d\d\d\d:0)");      
        foreach (Match ItemMatch in _matches)
        {
          Console.WriteLine(ItemMatch);
          _voucher_list.Add(ItemMatch.ToString());
        }

        return _voucher_list;

      }
      catch (Exception e)
      {
        throw e;
      }
    } // GetVouchersId

    private Boolean Format_ListOfLoyTransactionIo(LoyTransaction[] ListOfLoyTransactionIo, out String XmlFormat)
    {
      Boolean _rc;
      StringBuilder _sb;

      XmlFormat = String.Empty;
      _rc = false;

      try
      {
        _sb = new StringBuilder();

        foreach (LoyTransaction _voucher in ListOfLoyTransactionIo)
        {
          _sb.Append("<LoyTransaction>");
          _sb.Append("<TransacCanjeada>");
          _sb.Append(_voucher.TransacCanjeada);
          _sb.Append("</TransacCanjeada>");
          _sb.Append("<Monto>");
          _sb.Append(_voucher.Monto);
          _sb.Append("</Monto>");
          _sb.Append("<MemberNumber>");
          _sb.Append(_voucher.MemberNumber);
          _sb.Append("</MemberNumber>");
          _sb.Append("<Points>");
          _sb.Append(_voucher.Points);
          _sb.Append("</Points>");
          _sb.Append("<ProductName>");
          _sb.Append(_voucher.ProductName);
          _sb.Append("</ProductName>");
          _sb.Append("<ProductType>");
          _sb.Append(_voucher.ProductType);
          _sb.Append("</ProductType>");
          _sb.Append("<TransactionDate>");
          _sb.Append(_voucher.TransactionDate);
          _sb.Append("</TransactionDate>");
          _sb.Append("<TransactionNumber>");
          _sb.Append(_voucher.TransactionNumber);
          _sb.Append("</TransactionNumber>");
          _sb.Append("</LoyTransaction>");
        }
        XmlFormat = _sb.ToString();
        _rc = true;
      }
      catch
      {
        _rc = false;
      }

      return _rc;
    }

    private Boolean UseRedeemVoucherGroups()
    {
      IEnumerable<String> _lines;
      String[] _separators;

      _lines = File.ReadLines(Program.m_path + "Errors.txt");
      foreach (String _line in _lines)
      {
        _separators = _line.Split(':');
        if ((_separators[0]) == "UseGroups")
        {
          if (Int32.Parse(_separators[1]) > 0)
          {
            return true;
          }
          else
          {
            return false;
          }
        }
      } // foreach

      return false;
    } // UseRedeemVoucherGroups

    #endregion
  }

  public class LoyTransaction
  {

    private string montoField;

    private string transacCanjeadaField;

    private string memberNumberField;

    private string pointsField;

    private string productNameField;

    private string productTypeField;

    private string transactionDateField;

    private string transactionNumberField;

    public string Monto
    {
      get
      {
        return this.montoField;
      }
      set
      {
        this.montoField = value;
      }
    }

    public string TransacCanjeada
    {
      get
      {
        return this.transacCanjeadaField;
      }
      set
      {
        this.transacCanjeadaField = value;
      }
    }

    public string MemberNumber
    {
      get
      {
        return this.memberNumberField;
      }
      set
      {
        this.memberNumberField = value;
      }
    }

    public string Points
    {
      get
      {
        return this.pointsField;
      }
      set
      {
        this.pointsField = value;
      }
    }

    public string ProductName
    {
      get
      {
        return this.productNameField;
      }
      set
      {
        this.productNameField = value;
      }
    }

    public string ProductType
    {
      get
      {
        return this.productTypeField;
      }
      set
      {
        this.productTypeField = value;
      }
    }

    public string TransactionDate
    {
      get
      {
        return this.transactionDateField;
      }
      set
      {
        this.transactionDateField = value;
      }
    }

    public string TransactionNumber
    {
      get
      {
        return this.transactionNumberField;
      }
      set
      {
        this.transactionNumberField = value;
      }
    }
  } // LoyTransaction
}