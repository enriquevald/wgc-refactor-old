﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using HttpClient;

namespace CodereSiebelHost.ResponseSoaps
{
  public class AnonymousAccRequest : IResponse
  {
    #region public methods

    // PURPOSE : Get soap response
    //
    //  PARAMS :
    //      - INPUT:
    //        - Params : input params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : string representation of soap response
    public string GetSoapResponse(RequestParameters Params)
    {
      Random _rndMember;
      String _response;
      String _error_spcCode;
      String _error_spcMessage;

      _rndMember = new Random();
      _response = String.Empty;
      _error_spcCode = String.Empty;
      _error_spcMessage = String.Empty;

      if ((Params.AccountId != String.Empty) && (Params.TrackData != String.Empty) && (Params.SiteId != String.Empty))
      {
        _error_spcCode = "0";
        _error_spcMessage = "Se ha creado el socio anónimo correctamente.";
      }
      else
      {
        _error_spcCode = "-1";
        _error_spcMessage = "Error - AccountId:" + Params.AccountId.ToString() + " TrackData:" + Params.TrackData.ToString() + " SiteId:" + Params.SiteId.ToString();
      }

      try
      {
        _response = System.IO.File.ReadAllText(Program.m_path + "AnonymousAccResponse.xml");
        _response = _response.Replace("&p0", _rndMember.Next(100000, 300000).ToString()).Replace("&p1", _error_spcCode).Replace("&p2", _error_spcMessage);
      }
      catch (Exception _ex)
      {
        Console.WriteLine("Exception: " + _ex.Message);
        _response = _response.Replace("&p0", String.Empty).Replace("&p1", "-1").Replace("&p2", _ex.Message);
      }

      return _response;
    }

    // PURPOSE : Get soap error
    //
    //  PARAMS :
    //      - INPUT:
    //        - ErrorCode : input params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : string representation of soap error
    public string GetSoapError(Int32 ErrorCode)
    {
      String _soap;

      try
      {
        _soap = System.IO.File.ReadAllText(Program.m_path + "AnonymousAccResponse.xml");
        _soap = _soap.Replace("&p0", String.Empty);
        _soap = _soap.Replace("&p1", "-1");
        _soap = _soap.Replace("&p2", "Error");

        return _soap;
      }
      catch (Exception _ex)
      {
        throw _ex;
      }
    } // GetSoapError

    #endregion // public methods

  } // PointsRequest
}
