﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using HttpClient;
using System.Text.RegularExpressions;
using System.Configuration;

namespace CodereSiebelHost.ResponseSoaps
{
  public class ConfirmRequest : IResponse
  {
    String _input_value;

    public string GetSoapResponse(RequestParameters Params)
    {
      String _response;
      String _error_spcCode;
      String _error_spcMessage;
      String _records;
      Int32 _update_status_vouchers;

      _response = String.Empty;
      _error_spcCode = String.Empty;
      _error_spcMessage = String.Empty;
      _records = String.Empty;
      _input_value = Params.VoucherId;
      _update_status_vouchers = Int32.Parse(ConfigurationManager.AppSettings["UpdateStatusVouchers"].ToString());

      if (Params.VoucherId != String.Empty)
      {
        if (Regex.IsMatch(File.ReadAllText(Program.m_path + "VoucherIds.txt"), _input_value+":0"))
        {
          _error_spcCode = "OK";
          _error_spcMessage = "La transaccion ha sido canjeada con exito";

          if (_update_status_vouchers == 1)
          {
            File.WriteAllText(Program.m_path + "VoucherIds.txt", Regex.Replace(File.ReadAllText(Program.m_path + "VoucherIds.txt"), "(" + _input_value + ":0)", _input_value + ":1"));
          }
        }
        else
        {
          _error_spcCode = "KO";
          _error_spcMessage = "No existe transaccion con ese numero. Por favor contacte con el Administrador";
        }
      }
      else
      {
        //Error
        _error_spcCode = "KO";
        _error_spcMessage = "No existe transaccion con ese numero. Por favor contacte con el Administrador";
      }

      try
      {
        //_response = System.IO.File.ReadAllText(Program.m_path + "ConfirmResponse.xml");
        _response = Program.M_CONFIRM_RESPONSE_XML;
        _response = _response.Replace("&p0", _error_spcCode).Replace("&p1", _error_spcMessage);
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception: " + ex.Message);
      }

      return _response;

    }
    
    public string GetSoapError(int ErrorCode)
    {
      //String _soap = System.IO.File.ReadAllText(Program.m_path + "ConfirmResponse.xml");
      String _soap = Program.M_CONFIRM_RESPONSE_XML;
      _soap = _soap.Replace("&p0", "KO");
      switch (ErrorCode)
      {
        case 1: _soap = _soap.Replace("&p1", "Ha ocurrido un error en la busqueda del Miembro. Por favor pongase en contacto con el administrador."); break;
        case 2: _soap = _soap.Replace("&p1", "No existe transacción con ese numero. Por favor contacte con el Administrador"); break;
        case 3: _soap = _soap.Replace("&p1", "Esa transacción ya ha sido canjeada. Por favor pongase en contacto con el administrador."); break;
        default: _soap = _soap.Replace("&p1", "Ha ocurrido un error en la busqueda del Miembro. Por favor pongase en contacto con el administrador."); break;
      }
      return _soap;
    }

  }
}
