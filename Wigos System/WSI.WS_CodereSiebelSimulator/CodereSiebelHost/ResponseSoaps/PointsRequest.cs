﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using HttpClient;

namespace CodereSiebelHost.ResponseSoaps
{
  public class PointsRequest : IResponse
  {
    #region public methods

    // PURPOSE : Get soap response
    //
    //  PARAMS :
    //      - INPUT:
    //        - Params : input params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : string representation of soap response
    public string GetSoapResponse(RequestParameters Params)
    {
      Random _rndMember;
      String _response;
      String _error_spcCode;
      String _error_spcMessage;
      String _puntos_spcMiembro;
      String _records;

      _rndMember = new Random();
      _response = String.Empty;
      _error_spcCode = String.Empty;
      _error_spcMessage = String.Empty;
      _records = String.Empty;
      _puntos_spcMiembro = String.Empty;

      if (Params.AccountId != String.Empty)
      {
        _error_spcCode = "OK";
        _error_spcMessage = String.Empty;
        _puntos_spcMiembro = _rndMember.Next(10, 1000).ToString();
      }
      else
      {
        _error_spcCode = "KO";
        _error_spcMessage = "Ha ocurrido un error en la busqueda del Miembro. Por favor pongase en contacto con el Administrador.";
        _puntos_spcMiembro = String.Empty;
      }

      _records = "1";

      try
      {
        //_response = System.IO.File.ReadAllText(Program.m_path + "GetPointsResponse.xml");
        _response = Program.M_GET_POINTS_RESPONSE_XML;
        _response = _response.Replace("&p0", _error_spcCode).Replace("&p1", _error_spcMessage).Replace("&p2", _puntos_spcMiembro);
      }
      catch (Exception _ex)
      {
        Console.WriteLine("Exception: " + _ex.Message);
      }
      
      return _response;
     }

    // PURPOSE : Get soap error
    //
    //  PARAMS :
    //      - INPUT:
    //        - ErrorCode : input params
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : string representation of soap error
    public string GetSoapError(Int32 ErrorCode)
    {
      String _soap;

      try
      {
        //_soap = System.IO.File.ReadAllText(Program.m_path + "GetPointsResponse.xml");
        _soap = Program.M_GET_POINTS_RESPONSE_XML;
        _soap = _soap.Replace("&p0", "KO");
        _soap = _soap.Replace("&p1", "Ha ocurrido un error en la busqueda del Miembro. Por favor pongase en contacto con el administrador.");
        _soap = _soap.Replace("&p2", "");

        return _soap;
      }
      catch (Exception _ex)
      {
        throw _ex;
      }
    } // GetSoapError

    #endregion // public methods

  } // PointsRequest
}
