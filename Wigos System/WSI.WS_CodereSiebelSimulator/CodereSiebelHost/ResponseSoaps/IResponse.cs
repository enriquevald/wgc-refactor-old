﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodereSiebelHost.ResponseSoaps
{
  public interface IResponse
  {
    String GetSoapResponse(RequestParameters Params);
    String GetSoapError(Int32 ErrorCode);
  }
}
