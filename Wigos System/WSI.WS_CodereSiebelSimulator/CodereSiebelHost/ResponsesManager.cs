﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Reflection;
using CodereSiebelHost.ResponseSoaps;
using HttpClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;

namespace CodereSiebelHost
{
  public static class ResponsesManager
  {

    #region CONSTANTS

    public const String ERRORS_FILE = "Errors.txt";

    #endregion // CONSTANTS

    #region public methods

    public static String ManageRequest(HttpListenerRequest Request)
    {
      Int32 _error_code;
      String _response_soap;

      _response_soap = String.Empty;

      // Parse Request
      RequestParameters _request_params = SoapRequestParser.Parse(Request);
      
      System.Threading.Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["ResponseDelay"].ToString()));

      if (IsRequestError(_request_params.m_req_type, out _error_code))
      {
        // Create Response
        _response_soap = FillErrorSoap(_request_params.m_req_type, _error_code);
        
        // Write Console
        //Console.WriteLine("<-- Message response: " + _request_params.m_req_type.ToString());
        //Console.WriteLine("    Date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff"));
        //Console.WriteLine("    Status: KO");

        // Write Log
        //Program.WriteLog(_response_soap, Program.LOGFILE.Output);

      }
      else
      {
        // Create Response
        _response_soap = FillSoap(_request_params);
        
        // Write Console
       // Console.WriteLine("<-- Message response: " + _request_params.m_req_type.ToString());
       // Console.WriteLine("    Date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff"));
       // Console.WriteLine("    Status: OK");

        // Write Log
        //Program.WriteLog(_response_soap, Program.LOGFILE.Output);

      }

      if (CheckAndResetFile())
      {
       // Console.WriteLine("Reinitialized VouchersId. Date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff"));
      }

      return _response_soap;
    } // ManageRequest

    public static Boolean IsRequestError(TYPE_REQUEST Type, out Int32 ErrorCode)
    {
      IEnumerable<String> _lines;
      String[] _separators;

      _lines = File.ReadLines(Program.m_path + ERRORS_FILE);
      foreach (String _line in _lines)
      {
        _separators = _line.Split(':');
        if (Type.ToString() == (_separators[0]))
        {
          if (Int32.Parse(_separators[1]) > 0)
          {
            ErrorCode = Int32.Parse(_separators[1]);

            return true;
          }
          else
          {
            ErrorCode = 0;

            return false;
          }
        }
      } // foreach
      ErrorCode = 0;

      return false;
    } // IsRequestError

    #endregion

    #region private methods

    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT:
    //        - RequestParams
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : 
    private static String FillSoap(RequestParameters RequestParams)
    {
      Type _response_type;
      ConstructorInfo _response_constructor;
      IResponse Response;

      try
      {
        _response_type = Type.GetType("CodereSiebelHost.ResponseSoaps." + RequestParams.m_req_type.ToString());
        _response_constructor = _response_type.GetConstructor(new Type[] { });
        Response = (IResponse)_response_constructor.Invoke(new object[] { });
        
        return (Response.GetSoapResponse(RequestParams));
      }
      catch (Exception ex)
      {
        Console.WriteLine("FillSoap.Exception: " + ex.Message);
        
        throw ex;
      }
    } // FillSoap

    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT:
    //        - ReqType : 
    //        - ErrorCode :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String : 
    private static String FillErrorSoap(TYPE_REQUEST ReqType, Int32 ErrorCode)
    {
      Type _response_type;
      ConstructorInfo _response_constructor;
      IResponse Response;

      _response_type = Type.GetType("CodereSiebelHost.ResponseSoaps." + ReqType.ToString());
      _response_constructor = _response_type.GetConstructor(new Type[] { });
      Response = (IResponse)_response_constructor.Invoke(new object[] { });

      return (Response.GetSoapError(ErrorCode));
    } // FillErrorSoap

    private static Boolean CheckAndResetFile()
    {
      try
      {
        MatchCollection _matches = Regex.Matches(File.ReadAllText(Program.m_path + "VoucherIds.txt"), @"(\d-\d\d\d\d\d\d\d\d:0)");
        if (_matches.Count < 3)
        {
          File.WriteAllText(Program.m_path + "VoucherIds.txt", Regex.Replace(File.ReadAllText(Program.m_path + "VoucherIds.txt"), ":1", ":0"));
          return true;
        }
        return false;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    #endregion

  } // ResponsesManager
} // CodereSiebelHost
