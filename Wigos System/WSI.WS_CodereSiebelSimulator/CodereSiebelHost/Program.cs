﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading;
using System.Linq;
using System.Xml;
using System.IO;
using SimpleWebServer;
using System.Configuration;
using CodereSiebelHost;
using System.Reflection;
using System.Text.RegularExpressions;

namespace HttpClient
{
  public class Program
  {
    #region Constants
    
    public const String m_logfile_output = "LogOutput.log";
    public const String m_logfile_input = "LogInput.log";

    #endregion // Constants

    #region Members

    public static String m_logfile_path;
    public static String m_path;
    public static String[] m_monto_test_array;
    public static Int32 m_monto_test_index;

    public static String M_GET_POINTS_RESPONSE_XML = "";
    public static String M_REEDEM_RESPONSE_XML = "";
    public static String M_CONFIRM_RESPONSE_XML = "";
    public static String M_REEDEM_RESPONSE_ERROR_XML = "";

    //public static MatchCollection m_matches_vouchers;
    public static String[] m_matches_vouchers;

    #endregion // Members

    #region Enum
    public enum LOGFILE
    {
     Input = 0,
     Output = 1
    }
    #endregion

    static void Main(string[] args)
    {
      String _address;
      WebServer _ws;
      Int32 _read_vouchers_one_time;

      m_monto_test_index = 0;
      
      m_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\DataFiles\\";
      m_logfile_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\LogFiles\\";

      if (!ReadTemplates())
      {
        return;
      }
      _read_vouchers_one_time = Int32.Parse(ConfigurationManager.AppSettings["ReadVouchersOneTime"].ToString());

      if (_read_vouchers_one_time == 1)
      {
        if (!ReadVouchersFromFile())
        {
          return;
        }
      }

      //In Init, Check all Voucher Id's is not used
      File.WriteAllText(Program.m_path + "VoucherIds.txt", Regex.Replace(File.ReadAllText(Program.m_path + "VoucherIds.txt"), ":1", ":0"));
      _address = ConfigurationManager.AppSettings["Url"].ToString();
      _ws = new WebServer(SendResponse, _address);
      _ws.Run();
      Console.WriteLine("Codere Siebel Host has been initialized. Press Escape to Exit");
      Console.WriteLine("Address: "+_address);
      while (Console.ReadKey().Key != ConsoleKey.Escape)
      {

      }
      _ws.Stop();
      Console.WriteLine("Codere Siebel Host has been stopped");
      System.Threading.Thread.Sleep(1500);

    }

    public static String SendResponse(HttpListenerRequest request)
    {
      String _response;
      _response =  ResponsesManager.ManageRequest(request);
      
      return _response;
    }

    public static void WriteLog(String Text, LOGFILE InputOutput)
    {
      String _file_path;

      _file_path = String.Empty;
      switch (InputOutput)
      {
        case LOGFILE.Input: _file_path = Program.m_logfile_input; 
          break;
        case LOGFILE.Output: _file_path = Program.m_logfile_output; 
          break;
      }
      try
      {
        File.AppendAllText(Program.m_logfile_path + _file_path, "[" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff") + "]\r\n");
        File.AppendAllText(Program.m_logfile_path + _file_path, Text + "\r\n");
      }
      catch (Exception _ex)
      {
        Console.WriteLine("Error in log file: " + _ex.Message);
      }
    }

    public static Boolean ReadTemplates()
    {
      try
      {
        M_REEDEM_RESPONSE_XML = System.IO.File.ReadAllText(Program.m_path + "ReedemResponse.xml");
        M_GET_POINTS_RESPONSE_XML = System.IO.File.ReadAllText(Program.m_path + "GetPointsResponse.xml");
        M_CONFIRM_RESPONSE_XML = System.IO.File.ReadAllText(Program.m_path + "ConfirmResponse.xml");
        M_REEDEM_RESPONSE_ERROR_XML = System.IO.File.ReadAllText(Program.m_path + "ReedemResponseError.xml");
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
        
        return false;
      }

      return true;
    } // ReadTemplates


    public static Boolean ReadVouchersFromFile()
    {
      MatchCollection _match_collection;

      try
      {
        _match_collection = Regex.Matches(File.ReadAllText(Program.m_path + "VoucherIds.txt"), @"(\d-\d\d\d\d\d\d\d\d:0)");
        
        Program.m_matches_vouchers = new String[_match_collection.Count];
        
        for (Int32 _i = 0; _i < _match_collection.Count; _i++)
        {
          Program.m_matches_vouchers[_i] = _match_collection[_i].Value;
        }
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);

        return false;
      }
      
      return true;
    } // ReadVouchersFromFile

  } // class
}


  
