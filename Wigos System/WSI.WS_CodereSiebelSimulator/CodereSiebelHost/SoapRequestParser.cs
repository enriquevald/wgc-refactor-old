﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using HttpClient;

namespace CodereSiebelHost
{

  #region ENUMS

  public enum TYPE_REQUEST
  {
    PointsRequest = 0,
    RedeemRequest = 1,
    ConfirmRequest = 2,
    AnonymousAccRequest = 3
  }

  #endregion // ENUMS

  public class RequestParameters
  {
    #region Member

    public TYPE_REQUEST m_req_type;
    public String AccountId;
    public String VoucherId;
    public String TrackData;
    public String SiteId;

    #endregion // Member
  }

  public static class SoapRequestParser
  {

    #region public methods

    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT:
    //        - Request : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - RequestParameters :
    public static RequestParameters Parse(HttpListenerRequest Request)
    {
      String _request_formatted;
      RequestParameters _request_parameters;
      Boolean _request_parse_ok;
      XmlReader _xml_reader;

      _request_formatted = GetRequestPostData(Request);
      _request_parse_ok = false;
      _request_parameters = new RequestParameters();

      try
      {
        _xml_reader = XmlReader.Create(new StringReader(_request_formatted));
        while (!_request_parse_ok && _xml_reader.Read())
        {
          switch (_xml_reader.NodeType)
          {
            case XmlNodeType.Element:
              switch (_xml_reader.Name)
              {
                case "CDR_spcLOY_spcMember_spcPoint_Input":
                  _request_parameters.m_req_type = TYPE_REQUEST.PointsRequest;
                  _xml_reader.Read();
                  if (_xml_reader.Name == "Numero_spcMiembro")
                  {
                    _xml_reader.Read();
                    _request_parameters.AccountId = _xml_reader.Value;
                    _request_parameters.VoucherId = String.Empty;
                    _request_parameters.TrackData = String.Empty;
                    _request_parameters.SiteId = String.Empty;
                    _request_parse_ok = true;
                  }

                  break;
                case "CDR_spcLOY_spcMember_spcRedepcion_spcWorkflow_Input":
                //case "CDR_spcLOY_spcMember_spcRedepcion_Input": // Case necesario para antiguas versiones.
                  _request_parameters.m_req_type = TYPE_REQUEST.RedeemRequest;
                  _xml_reader.Read();
                  if (_xml_reader.Name == "Numero_spcMiembro")
                  {
                    _xml_reader.Read();
                    _request_parameters.AccountId = _xml_reader.Value;
                    _request_parameters.VoucherId = String.Empty;
                    _request_parameters.TrackData = String.Empty;
                    _request_parameters.SiteId = String.Empty;
                    _request_parse_ok = true;
                  }

                  break;
                case "CDR_spcLOY_spcCanjear_spcRedepcion_spcWorkflow_Input":
                  _request_parameters.m_req_type = TYPE_REQUEST.ConfirmRequest;
                  _xml_reader.Read();
                  if (_xml_reader.Name == "CDR_spcTransaccion_spcNumber")
                  {
                    _xml_reader.Read();
                    _request_parameters.VoucherId = _xml_reader.Value;
                    _request_parameters.AccountId = String.Empty;
                    _request_parameters.TrackData = String.Empty;
                    _request_parameters.SiteId = String.Empty;
                    _request_parse_ok = true;
                  }

                  break;
                case "CDR_spcAnon_spcUser_1_Input":
                  _xml_reader.Read();
                  if (_xml_reader.Name == "CDREntradaAnonimoIO")
                  {
                    _request_parameters.m_req_type = TYPE_REQUEST.AnonymousAccRequest;
                    _request_parameters.AccountId = String.Empty;
                    _request_parameters.VoucherId = String.Empty;
                    _request_parameters.TrackData = String.Empty;
                    _request_parameters.SiteId = String.Empty;

                    _xml_reader.Read();
                    if (_xml_reader.Name == "Entrada")
                    {
                      for (int i = 0; i < 3; i++)
                      {
                        // Reads label open
                        _xml_reader.Read();

                        switch (_xml_reader.Name)
                        {
                          case "CuentaWIN":
                            _xml_reader.Read();
                            _request_parameters.AccountId = _xml_reader.Value;
                            break;
                          case "NumeroTarjeta":
                            _xml_reader.Read();
                            _request_parameters.TrackData = _xml_reader.Value;
                            break;
                          case "CodigoSala":
                            _xml_reader.Read();
                            _request_parameters.SiteId = _xml_reader.Value;
                            break;
                        }

                        // Reads label close
                        _xml_reader.Read();
                      }
                    }
                    _request_parse_ok = true;
                  }

                  break;
                default:

                  break;
              } // switch

              break;
            case XmlNodeType.XmlDeclaration:
            case XmlNodeType.Text:
            case XmlNodeType.ProcessingInstruction:
            case XmlNodeType.Comment:
            case XmlNodeType.EndElement:
            default:

              break;
          } // switch
        } // while

        //Write Console
       // Console.WriteLine("--> Message received: " + _request_parameters.m_req_type.ToString());
        //Console.WriteLine("    Date: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff"));
       // Console.WriteLine("    IncCustomerNumber: " + _request_parameters.AccountId.ToString());
       // Console.WriteLine("    VoucherId: " + _request_parameters.VoucherId.ToString());

        //Write Log
       // Program.WriteLog(_request_formatted, Program.LOGFILE.Input);

        return _request_parameters;
      }
      catch
      {
        throw new ArgumentException("Error al parsear la información de entrada.");
      }

    } // RequestParameters

    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT:
    //        - Request : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String :
    public static string GetRequestPostData(HttpListenerRequest Request)
    {
      if (!Request.HasEntityBody)
      {

        return null;
      }
      using (Stream body = Request.InputStream) // here we have data
      {
        using (StreamReader reader = new StreamReader(body, Request.ContentEncoding))
        {

          return reader.ReadToEnd();
        }
      }
    } // GetRequestPostData

    #endregion // public methods

  } // class SoapRequestParser
}
