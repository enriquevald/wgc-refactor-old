﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WSI.Common;

namespace MGT
{
  public static class Configuration
  {

    #region Attributes

    private static String m_service_name = "Unknown";

    private static String m_version = "CC.BBB";

    #endregion

    #region Public Methods

    public static void CreateConnection()
    {
      StringBuilder _xml;

      _xml = new StringBuilder();
      _xml.AppendLine("<SiteConfig>");
      _xml.AppendLine("    <DBPrincipal>WIN-SERVER-4</DBPrincipal>");
      _xml.AppendLine("    <DBMirror>WIN-SERVER-4</DBMirror>");
      _xml.AppendLine("    <DBId>0</DBId>");
      _xml.AppendLine("</SiteConfig>");

      if (!ConfigurationFile.Init("MGT.cfg", _xml.ToString()))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");

        Thread.Sleep(2500);
      }
    } 
    #endregion

  }
}
