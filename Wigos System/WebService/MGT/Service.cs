﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ServiceForMGT.cs
// 
//   DESCRIPTION: Service class (WCF) that implement the web methods requested by MGT
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 25-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-NOV-2015 FAV    First release.
//------------------------------------------------------------------------------

using MGT.Converters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WSI.Common;

namespace MGT
{
  public class Service : IUIF
  {
    #region Constructor

    public Service()
    {
      // TODO: Esto lo deberia hacer el Servicio de Windows una sola vez en el OnStart
      Configuration.CreateConnection();
      Resource.Init();
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Returns Patron Data using the UIFPatron objects structure.
    /// </summary>
    /// <param name="pIsAcctNotCardStripe">True: pData parameter contains Patron/Player ID. False: pData parameter contains the mag stripe contents of a player card</param>
    /// <param name="pData">Contains either mag stripe or Patron / Player ID data</param>
    /// <param name="pPINNumber">Optional, based upon system requirements</param>
    /// <returns></returns>
    public UIF.Template.UIFPatron GetPatron(bool pIsAcctNotCardStripe, string pData, string pPINNumber)
    {

        MGT.UIF.Template.UIFPatron _patron = new UIF.Template.UIFPatron();

        try
        {

            Int64 _account_id = 0;
            if (pIsAcctNotCardStripe)
                Int64.TryParse(pData, out _account_id);
            else 
               _account_id = Decode(pData);

            if (_account_id <= 0)
                return null;

            CardData _card = new CardData();
            if (CardData.DB_CardGetAllData(_account_id, _card))
            {
            

                if (_card.PlayerTracking.Pin != pPINNumber)
                    return null;
                
                MGT.UIF.Template.UIFAddress _adddress = new UIF.Template.UIFAddress();
                MGT.UIF.Template.UIFBalance _balance = new UIF.Template.UIFBalance();
                MGT.UIF.Template.UIFAccount _account = new UIF.Template.UIFAccount();
            MGT.UIF.Template.UIFPatronInfo _patron_info = new UIF.Template.UIFPatronInfo();
            MGT.UIF.Template.UIFIdentityInfo _identity_info = new UIF.Template.UIFIdentityInfo(); 

                _adddress.AddressLine1 = _card.PlayerTracking.HolderAddress01;
                _adddress.AddressLine2 = _card.PlayerTracking.HolderAddress02;
                _adddress.Zip = _card.PlayerTracking.HolderZip;
                _adddress.City = _card.PlayerTracking.HolderCity;
                _adddress.State = _card.PlayerTracking.HolderAddress03; 
            _adddress.Country = CardData.CountriesString(_card.PlayerTracking.HolderAddressCountry);

            _balance.Available = _card.CurrentBalance;
            _balance.AvailableToday = _card.CurrentBalance;

            _account.AccountID = _account_id;
            _account.EnrollDate = _card.PlayerTracking.CardCreationDate;     
                _account.Anniversary = _card.PlayerTracking.HolderWeddingDate;

            _patron_info.Birthdate = _card.PlayerTracking.BeneficiaryBirthDate;
            _patron_info.CardStripe = _card.TrackData;
            _patron_info.CardStripeResolved = _card.TrackData;
                _patron_info.FirstName = _card.PlayerTracking.HolderName3;
                _patron_info.Gender = CardData.GenderString(_card.PlayerTracking.HolderGender);
                _patron_info.IsCustom = false; // CHECK
                _patron_info.LastName = _card.PlayerTracking.HolderName1;
                _patron_info.MiddleInitial = _card.PlayerTracking.HolderName2;
                _patron_info.NickName = "";
            _patron_info.PatronID = _card.AccountId;
            _patron_info.Status = CardData.MaritalStatusString((ACCOUNT_MARITAL_STATUS)_card.PlayerTracking.HolderMaritalStatus);

            _identity_info.IdentityType = UIF.Template.IdentityType.PIN;
            _identity_info.IdentityData = pPINNumber;
  
            
            _patron.Account = _account;
            _patron.Balances.SetValue(_balance, 0);
            _patron.Addresses.SetValue(_balance, 0);
            _patron.PatronInfo = _patron_info;
            }

        }
        catch (Exception ex)
        {
            Log.Exception(ex);
        }

        return _patron;
    }

    /// <summary>
    /// Returns Play Data using the MetricGroup / Metric objects structure.
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pFromDate">Start of period for Play rollup. May include Time when a granularity of Detail is specified.</param>
    /// <param name="pThruDate">End of period for Play rollup. May include Time when a granularity of Detail is specified.</param>
    /// <param name="pCasinoID">Optional, based upon system requirements.</param>
    /// <param name="pGamingActivityGranularity">An enumeration value specifying whether results should be drawn from Day, Detail, or Trip.</param>
    /// <returns></returns>
    public UIF.Template.MetricGroup GetPlay(long pPatronID, DateTime pFromDate, DateTime pThruDate, long pCasinoID, UIF.Template.MetricGranularity pGamingActivityGranularity)
    {
      UIF.Template.MetricGroup _metric_group = new UIF.Template.MetricGroup();
      try
      {
        Int32 _closing_time;
        String _site_id;

        PlaySessions _play_sessions = new PlaySessions();
        GamingActivity _gaming_activity = new GamingActivity(_play_sessions);

        Int32.TryParse(GeneralParam.Value("WigosGUI", "ClosingTime"), out _closing_time);
        _site_id =  GeneralParam.Value("Site", "Identifier");


        _gaming_activity.GetPlay(pPatronID, pFromDate, pThruDate, pGamingActivityGranularity, _site_id, _closing_time, out _metric_group);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return _metric_group;
    }

    /// <summary>
    /// Returns an array of UIFPatronOption objects using a LIKE style lookup of first and last name
    /// </summary>
    /// <param name="pFirstName">Full or partial first name for lookup.  (i.e. John, or J)</param>
    /// <param name="pLastName">Full or partial last name for lookup.  (i.e. Smith, or Smi)</param>
    /// <returns></returns>
    public UIF.Template.UIFPatronOption[] GetPatronsByName(string pFirstName, string pLastName)
    {

      DataTable _data_table;
      AccountFilters _account_filters;

      _account_filters = new AccountFilters();
      _account_filters.FirstName = pFirstName;
      _account_filters.LastName = pLastName;

      _data_table = _account_filters.GetPlayerData();
      return MGTParserPatron.GetUIFPatronOptionArray(_data_table);
    }

    /// <summary>
    /// Returns an array of UIFPatronOption objects using a lookup by date of birth
    /// </summary>
    /// <param name="pDateOfBirth">Player Birthdate</param>
    /// <returns></returns>
    public UIF.Template.UIFPatronOption[] GetPatronsByBirthdate(DateTime pDateOfBirth)
    {
      DataTable _data_table;
      AccountFilters _account_filters;

      _account_filters = new AccountFilters();
      _account_filters.BirthDate = pDateOfBirth;

      _data_table = _account_filters.GetPlayerData();
      return MGTParserPatron.GetUIFPatronOptionArray(_data_table);
    }

    /// <summary>
    /// Returns an array of UIFPatronOption objects using a lookup by phone number
    /// </summary>
    /// <param name="pPhoneNumber">Player Phone Number</param>
    /// <returns></returns>
    public UIF.Template.UIFPatronOption[] GetPatronsByPhone(string pPhoneNumber)
    {
      DataTable _data_table;
      AccountFilters _account_filters;

      _account_filters = new AccountFilters();
      _account_filters.PhoneNumber = pPhoneNumber;

      _data_table = _account_filters.GetPlayerData();
      return MGTParserPatron.GetUIFPatronOptionArray(_data_table);
    }

    /// <summary>
    /// Performs a balance adjustment
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pRedemptionType">An enumeration indicating the balance targeted for adjustment</param>
    /// <param name="pAdjustmentType">An enumeration specifying whether the adjustment is a Credit or Debit</param>
    /// <param name="pAmount">The amount to be adjusted</param>
    /// <param name="pReason">A comment for audit trails</param>
    /// <param name="pSubType">Optional indicator for audit trails</param>
    /// <returns></returns>
    public string AdjustPatronBalance(long pPatronID, UIF.Template.RedemptionType pRedemptionType, UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, string pSubType)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Performs a balance adjustment with the inclusion of an Expiration date and optional Start date.  May also be utilized in lieu of AdjustPatronBalance when a User ID is required.
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pRedemptionType">An enumeration indicating the balance targeted for adjustment</param>
    /// <param name="pAdjustmentType">An enumeration specifying whether the adjustment is a Credit or Debit</param>
    /// <param name="pAmount">The amount to be adjusted</param>
    /// <param name="pReason">A comment for audit trails</param>
    /// <param name="pExpirationDate">The date on which the adjustment expires</param>
    /// <param name="pUserID">Any required user id to perform the adjustment</param>
    /// <param name="pSubType">Optional indicator for audit trails</param>
    /// <param name="pStartDate">Optional, the date on which the adjustment should become available</param>
    /// <returns>Return value is a numeric system transaction identifier.  All other values indicate a failure, and may optionally include error information</returns>
    public string AdjustPatronBalanceWExp(long pPatronID, UIF.Template.RedemptionType pRedemptionType, UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, DateTime pExpirationDate, string pUserID, string pSubType, DateTime pStartDate)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Performs PIN Validation. MUST return a value of “VALID” for valid PIN.  All other values indicate an invalid PIN, and may optionally include error information
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pPINNumber">Player PIN</param>
    /// <returns></returns>
    public string ValidatePIN(long pPatronID, string pPINNumber)
    {

      var _check_status = PinCheckStatus.ERROR; 

      try
      {
        if (pPatronID > 0) { 
          _check_status = Accounts.DB_CheckAccountPIN(pPatronID, pPINNumber);
        }
      }
      catch (Exception ex)
      {
          Log.Exception(ex);
      }

      return _check_status.ToString();

    }

    /// <summary>
    /// Performs redemption of a system Redemption Item. Return value is a numeric system transaction identifier.  All other values indicate a failure, and may optionally include error information.
    /// </summary>
    /// <param name="pRedemptionType">An enumeration indicating the balance targeted for adjustment</param>
    /// <param name="pPlayerTrackingItemCode">The system identifier of the Redemption Item</param>
    /// <param name="pQuantity">The quantity to be redeemed</param>
    /// <param name="pAmount">The amount to be redeemed.  May represent simply a calculated amount, or the total value allotted to the redemption.</param>
    /// <param name="pReason">A comment for audit trails</param>
    /// <param name="pUserID">Any required user id to perform the adjustment</param>
    /// <param name="pWorkstation">The device at which the redemption occurs</param>
    /// <returns></returns>
    public string RedeemItem(UIF.Template.RedemptionType pRedemptionType, string pPlayerTrackingItemCode, double pQuantity, double pAmount, string pReason, string pUserID, string pWorkstation)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Returns an array of available Redemption Items using UIFRedemptionItem objects
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <returns></returns>
    public UIF.Template.UIFRedemptionItem[] GetRedemptionItems(long pPatronID)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Settles a Redemption when the system does not auto-settle.  (Rarely used)	Return value is a Boolean indicating success or failure
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pIssuanceID">The system transaction identifier returned by the redemption transaction</param>
    /// <param name="pAmount">The revised amount to be applied to the redemption transaction</param>
    /// <param name="pUserID">Any required user id to settled the redemption</param>
    /// <returns></returns>
    public bool Settle(long pPatronID, string pIssuanceID, decimal pAmount, string pUserID)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Returns the Patron / Player ID from the supplied mag stripe content
    /// </summary>
    /// <param name="pCardStripe">The contents of the mag stripe from a player card swipe</param>
    /// <returns></returns>
    public long Decode(string pCardStripe)
    {
      Int64 _account_id;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (Accounts.GetAccountIdFromExternalTrackData(pCardStripe, _db_trx.SqlTransaction, out _account_id))
          {
            return _account_id;
          }
          else
          {
            throw new Exception("Doesn't exist a valid Account ID for the card stripe passed");
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return 0;
    }

    /// <summary>
    /// Initiates bulk deployment of Gaming Activity data for a given period.	Return value is a Boolean indicating success or failure
    /// </summary>
    /// <param name="pImportType"></param>
    /// <param name="pImportSubType"></param>
    /// <param name="pLocationID"></param>
    /// <param name="pStartDate"></param>
    /// <param name="pEndDate"></param>
    /// <returns></returns>
    public bool ImportData(string pImportType, string pImportSubType, string pLocationID, DateTime pStartDate, DateTime pEndDate)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Returns an array of UIFBalance objects
    /// </summary>
    /// <param name="pPatronID">Player ID</param>
    /// <param name="pRedemptionType">An enumeration indicating a specific balance to be returned. A value of None indicates that ALL balances should be returned.</param>
    /// <returns></returns>
    public UIF.Template.UIFBalance[] GetPatronBalances(long pPatronID, UIF.Template.RedemptionType pRedemptionType)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Returns the added Patron using the UIFPatron objects structure.  
    ///(Return object should contain all related system identifiers created during the enrollment of the patron and exclude any supplied data that is not supported.)
    /// </summary>
    /// <param name="pPatron">A UIFPatron object structure containing all data necessary for enrollment</param>
    /// <returns></returns>
    public UIF.Template.UIFPatron AddPatron(UIF.Template.UIFPatron pPatron)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Returns the updated data set using the UIFPatron objects structure.  
    /// (Return object should include only data which was updated and exclude any supplied data that is not supported.)
    /// </summary>
    /// <param name="pPatron"></param>
    /// <returns></returns>
    public UIF.Template.UIFPatron UpdatePatron(UIF.Template.UIFPatron pPatron)
    {
      throw new NotImplementedException();
    }

    #endregion

  }
}
