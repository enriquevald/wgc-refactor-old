﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: UIFProxy.cs
// 
//   DESCRIPTION: Proxy class generated with the wsdl and xsd files provided by MGT 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 25-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-NOV-2015 FAV    First release. Using vsvcutil.exe tool
//------------------------------------------------------------------------------

namespace MGT.UIF.Template
{
  using System.Runtime.Serialization;


  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFPatron", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFPatron : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private MGT.UIF.Template.UIFAccount AccountField;

    private MGT.UIF.Template.UIFAddress[] AddressesField;

    private MGT.UIF.Template.UIFBalance[] BalancesField;

    private MGT.UIF.Template.UIFContactInfo[] ContactInfosField;

    private MGT.UIF.Template.UIFCustomInfo[] CustomInfosField;

    private MGT.UIF.Template.UIFIdentification[] IdentificationsField;

    private MGT.UIF.Template.UIFPatronInfo PatronInfoField;

    private MGT.UIF.Template.UIFTierRanking TierRankingField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFAccount Account
    {
      get
      {
        return this.AccountField;
      }
      set
      {
        this.AccountField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFAddress[] Addresses
    {
      get
      {
        return this.AddressesField;
      }
      set
      {
        this.AddressesField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFBalance[] Balances
    {
      get
      {
        return this.BalancesField;
      }
      set
      {
        this.BalancesField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFContactInfo[] ContactInfos
    {
      get
      {
        return this.ContactInfosField;
      }
      set
      {
        this.ContactInfosField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFCustomInfo[] CustomInfos
    {
      get
      {
        return this.CustomInfosField;
      }
      set
      {
        this.CustomInfosField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFIdentification[] Identifications
    {
      get
      {
        return this.IdentificationsField;
      }
      set
      {
        this.IdentificationsField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFPatronInfo PatronInfo
    {
      get
      {
        return this.PatronInfoField;
      }
      set
      {
        this.PatronInfoField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFTierRanking TierRanking
    {
      get
      {
        return this.TierRankingField;
      }
      set
      {
        this.TierRankingField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFAccount", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFAccount : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private long AccountIDField;

    private System.DateTime AnniversaryField;

    private System.DateTime EnrollDateField;

    private bool IsSharedField;

    private System.DateTime LastActivityDateField;

    private string LocationIDField;

    private long SecondaryIDField;

    private string SourceField;

    private string StatusField;

    private string ZipField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long AccountID
    {
      get
      {
        return this.AccountIDField;
      }
      set
      {
        this.AccountIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime Anniversary
    {
      get
      {
        return this.AnniversaryField;
      }
      set
      {
        this.AnniversaryField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime EnrollDate
    {
      get
      {
        return this.EnrollDateField;
      }
      set
      {
        this.EnrollDateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsShared
    {
      get
      {
        return this.IsSharedField;
      }
      set
      {
        this.IsSharedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime LastActivityDate
    {
      get
      {
        return this.LastActivityDateField;
      }
      set
      {
        this.LastActivityDateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string LocationID
    {
      get
      {
        return this.LocationIDField;
      }
      set
      {
        this.LocationIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long SecondaryID
    {
      get
      {
        return this.SecondaryIDField;
      }
      set
      {
        this.SecondaryIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Source
    {
      get
      {
        return this.SourceField;
      }
      set
      {
        this.SourceField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Status
    {
      get
      {
        return this.StatusField;
      }
      set
      {
        this.StatusField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Zip
    {
      get
      {
        return this.ZipField;
      }
      set
      {
        this.ZipField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFPatronInfo", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFPatronInfo : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private System.DateTime BirthdateField;

    private string CardStripeField;

    private string CardStripeResolvedField;

    private string FirstNameField;

    private string GenderField;

    private MGT.UIF.Template.UIFIdentityInfo[] IdentityInfosField;

    private bool IsCustomField;

    private string LastNameField;

    private string MiddleInitialField;

    private string NickNameField;

    private long PatronIDField;

    private string StatusField;

    private string SuffixField;

    private double SystemCardTierField;

    private string SystemCardTierDescriptionField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime Birthdate
    {
      get
      {
        return this.BirthdateField;
      }
      set
      {
        this.BirthdateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardStripe
    {
      get
      {
        return this.CardStripeField;
      }
      set
      {
        this.CardStripeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardStripeResolved
    {
      get
      {
        return this.CardStripeResolvedField;
      }
      set
      {
        this.CardStripeResolvedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string FirstName
    {
      get
      {
        return this.FirstNameField;
      }
      set
      {
        this.FirstNameField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Gender
    {
      get
      {
        return this.GenderField;
      }
      set
      {
        this.GenderField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFIdentityInfo[] IdentityInfos
    {
      get
      {
        return this.IdentityInfosField;
      }
      set
      {
        this.IdentityInfosField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsCustom
    {
      get
      {
        return this.IsCustomField;
      }
      set
      {
        this.IsCustomField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string LastName
    {
      get
      {
        return this.LastNameField;
      }
      set
      {
        this.LastNameField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string MiddleInitial
    {
      get
      {
        return this.MiddleInitialField;
      }
      set
      {
        this.MiddleInitialField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string NickName
    {
      get
      {
        return this.NickNameField;
      }
      set
      {
        this.NickNameField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long PatronID
    {
      get
      {
        return this.PatronIDField;
      }
      set
      {
        this.PatronIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Status
    {
      get
      {
        return this.StatusField;
      }
      set
      {
        this.StatusField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Suffix
    {
      get
      {
        return this.SuffixField;
      }
      set
      {
        this.SuffixField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double SystemCardTier
    {
      get
      {
        return this.SystemCardTierField;
      }
      set
      {
        this.SystemCardTierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SystemCardTierDescription
    {
      get
      {
        return this.SystemCardTierDescriptionField;
      }
      set
      {
        this.SystemCardTierDescriptionField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFTierRanking", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFTierRanking : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string CardRankSourceField;

    private double CardTierField;

    private string CardTierBasisField;

    private string CardTierDescField;

    private int CardTierFromFlagsField;

    private string CardTierMustMaintainField;

    private System.DateTime CardTierPeriodEndField;

    private System.DateTime CardTierPeriodExpirationField;

    private System.DateTime CardTierPeriodStartField;

    private int CardTierPointsField;

    private string CardTierToNextLevelField;

    private int EvaluationPeriodPointsField;

    private bool IsNullEntityField;

    private bool IsPreferredField;

    private string LocalTierField;

    private string NextCardTierField;

    private long PatronIDField;

    private string SiteCodeField;

    private int SiteIDField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardRankSource
    {
      get
      {
        return this.CardRankSourceField;
      }
      set
      {
        this.CardRankSourceField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double CardTier
    {
      get
      {
        return this.CardTierField;
      }
      set
      {
        this.CardTierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardTierBasis
    {
      get
      {
        return this.CardTierBasisField;
      }
      set
      {
        this.CardTierBasisField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardTierDesc
    {
      get
      {
        return this.CardTierDescField;
      }
      set
      {
        this.CardTierDescField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public int CardTierFromFlags
    {
      get
      {
        return this.CardTierFromFlagsField;
      }
      set
      {
        this.CardTierFromFlagsField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardTierMustMaintain
    {
      get
      {
        return this.CardTierMustMaintainField;
      }
      set
      {
        this.CardTierMustMaintainField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime CardTierPeriodEnd
    {
      get
      {
        return this.CardTierPeriodEndField;
      }
      set
      {
        this.CardTierPeriodEndField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime CardTierPeriodExpiration
    {
      get
      {
        return this.CardTierPeriodExpirationField;
      }
      set
      {
        this.CardTierPeriodExpirationField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime CardTierPeriodStart
    {
      get
      {
        return this.CardTierPeriodStartField;
      }
      set
      {
        this.CardTierPeriodStartField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public int CardTierPoints
    {
      get
      {
        return this.CardTierPointsField;
      }
      set
      {
        this.CardTierPointsField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardTierToNextLevel
    {
      get
      {
        return this.CardTierToNextLevelField;
      }
      set
      {
        this.CardTierToNextLevelField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public int EvaluationPeriodPoints
    {
      get
      {
        return this.EvaluationPeriodPointsField;
      }
      set
      {
        this.EvaluationPeriodPointsField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsNullEntity
    {
      get
      {
        return this.IsNullEntityField;
      }
      set
      {
        this.IsNullEntityField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsPreferred
    {
      get
      {
        return this.IsPreferredField;
      }
      set
      {
        this.IsPreferredField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string LocalTier
    {
      get
      {
        return this.LocalTierField;
      }
      set
      {
        this.LocalTierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string NextCardTier
    {
      get
      {
        return this.NextCardTierField;
      }
      set
      {
        this.NextCardTierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long PatronID
    {
      get
      {
        return this.PatronIDField;
      }
      set
      {
        this.PatronIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SiteCode
    {
      get
      {
        return this.SiteCodeField;
      }
      set
      {
        this.SiteCodeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public int SiteID
    {
      get
      {
        return this.SiteIDField;
      }
      set
      {
        this.SiteIDField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFAddress", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFAddress : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string AddressLine1Field;

    private string AddressLine2Field;

    private string AddressTypeField;

    private string CityField;

    private string CountryField;

    private bool IsBadField;

    private bool IsLocalField;

    private bool IsPreferredField;

    private bool OkayToSendField;

    private string StateField;

    private string ZipField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AddressLine1
    {
      get
      {
        return this.AddressLine1Field;
      }
      set
      {
        this.AddressLine1Field = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AddressLine2
    {
      get
      {
        return this.AddressLine2Field;
      }
      set
      {
        this.AddressLine2Field = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AddressType
    {
      get
      {
        return this.AddressTypeField;
      }
      set
      {
        this.AddressTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string City
    {
      get
      {
        return this.CityField;
      }
      set
      {
        this.CityField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Country
    {
      get
      {
        return this.CountryField;
      }
      set
      {
        this.CountryField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsBad
    {
      get
      {
        return this.IsBadField;
      }
      set
      {
        this.IsBadField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsLocal
    {
      get
      {
        return this.IsLocalField;
      }
      set
      {
        this.IsLocalField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsPreferred
    {
      get
      {
        return this.IsPreferredField;
      }
      set
      {
        this.IsPreferredField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool OkayToSend
    {
      get
      {
        return this.OkayToSendField;
      }
      set
      {
        this.OkayToSendField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string State
    {
      get
      {
        return this.StateField;
      }
      set
      {
        this.StateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Zip
    {
      get
      {
        return this.ZipField;
      }
      set
      {
        this.ZipField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFBalance", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFBalance : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private decimal AvailableField;

    private decimal AvailableTodayField;

    private decimal BalanceField;

    private string BalanceSubTypeField;

    private string BalanceTypeField;

    private decimal EarnedField;

    private decimal EarnedTodayField;

    private decimal RedeemedField;

    private decimal RedeemedTodayField;

    private MGT.UIF.Template.UIFBalance[] SubBalancesField;

    private decimal ValueField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Available
    {
      get
      {
        return this.AvailableField;
      }
      set
      {
        this.AvailableField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal AvailableToday
    {
      get
      {
        return this.AvailableTodayField;
      }
      set
      {
        this.AvailableTodayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Balance
    {
      get
      {
        return this.BalanceField;
      }
      set
      {
        this.BalanceField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string BalanceSubType
    {
      get
      {
        return this.BalanceSubTypeField;
      }
      set
      {
        this.BalanceSubTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string BalanceType
    {
      get
      {
        return this.BalanceTypeField;
      }
      set
      {
        this.BalanceTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Earned
    {
      get
      {
        return this.EarnedField;
      }
      set
      {
        this.EarnedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal EarnedToday
    {
      get
      {
        return this.EarnedTodayField;
      }
      set
      {
        this.EarnedTodayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Redeemed
    {
      get
      {
        return this.RedeemedField;
      }
      set
      {
        this.RedeemedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal RedeemedToday
    {
      get
      {
        return this.RedeemedTodayField;
      }
      set
      {
        this.RedeemedTodayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFBalance[] SubBalances
    {
      get
      {
        return this.SubBalancesField;
      }
      set
      {
        this.SubBalancesField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Value
    {
      get
      {
        return this.ValueField;
      }
      set
      {
        this.ValueField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFContactInfo", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFContactInfo : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string CategoryField;

    private string ContactInfoField;

    private string ContactSubTypeField;

    private string ContactTypeField;

    private string InteractionIDField;

    private bool IsBadField;

    private bool IsEmptyField;

    private bool IsPreferredField;

    private bool IsValidatedField;

    private bool OkayToContactField;

    private bool OptInField;

    private string SecurityInfoField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Category
    {
      get
      {
        return this.CategoryField;
      }
      set
      {
        this.CategoryField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ContactInfo
    {
      get
      {
        return this.ContactInfoField;
      }
      set
      {
        this.ContactInfoField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ContactSubType
    {
      get
      {
        return this.ContactSubTypeField;
      }
      set
      {
        this.ContactSubTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ContactType
    {
      get
      {
        return this.ContactTypeField;
      }
      set
      {
        this.ContactTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string InteractionID
    {
      get
      {
        return this.InteractionIDField;
      }
      set
      {
        this.InteractionIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsBad
    {
      get
      {
        return this.IsBadField;
      }
      set
      {
        this.IsBadField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsEmpty
    {
      get
      {
        return this.IsEmptyField;
      }
      set
      {
        this.IsEmptyField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsPreferred
    {
      get
      {
        return this.IsPreferredField;
      }
      set
      {
        this.IsPreferredField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsValidated
    {
      get
      {
        return this.IsValidatedField;
      }
      set
      {
        this.IsValidatedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool OkayToContact
    {
      get
      {
        return this.OkayToContactField;
      }
      set
      {
        this.OkayToContactField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool OptIn
    {
      get
      {
        return this.OptInField;
      }
      set
      {
        this.OptInField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SecurityInfo
    {
      get
      {
        return this.SecurityInfoField;
      }
      set
      {
        this.SecurityInfoField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFCustomInfo", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFCustomInfo : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string CustomInfoField;

    private string CustomInfoNameField;

    private string CustomInfoTypeField;

    private string CustomInfoValueField;

    private string DataTypeField;

    private string ValueDataTypeField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CustomInfo
    {
      get
      {
        return this.CustomInfoField;
      }
      set
      {
        this.CustomInfoField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CustomInfoName
    {
      get
      {
        return this.CustomInfoNameField;
      }
      set
      {
        this.CustomInfoNameField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CustomInfoType
    {
      get
      {
        return this.CustomInfoTypeField;
      }
      set
      {
        this.CustomInfoTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CustomInfoValue
    {
      get
      {
        return this.CustomInfoValueField;
      }
      set
      {
        this.CustomInfoValueField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string DataType
    {
      get
      {
        return this.DataTypeField;
      }
      set
      {
        this.DataTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ValueDataType
    {
      get
      {
        return this.ValueDataTypeField;
      }
      set
      {
        this.ValueDataTypeField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFIdentification", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFIdentification : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private MGT.UIF.Template.UIFAddress AddressField;

    private MGT.UIF.Template.UIFAttribute[] AttributesField;

    private string IdentificationDataField;

    private MGT.UIF.Template.IdentificationType IdentificationTypeField;

    private string IdentifierField;

    private string JurisdictionField;

    private MGT.UIF.Template.UIFPatronInfo PatronInfoField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFAddress Address
    {
      get
      {
        return this.AddressField;
      }
      set
      {
        this.AddressField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFAttribute[] Attributes
    {
      get
      {
        return this.AttributesField;
      }
      set
      {
        this.AttributesField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string IdentificationData
    {
      get
      {
        return this.IdentificationDataField;
      }
      set
      {
        this.IdentificationDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.IdentificationType IdentificationType
    {
      get
      {
        return this.IdentificationTypeField;
      }
      set
      {
        this.IdentificationTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Identifier
    {
      get
      {
        return this.IdentifierField;
      }
      set
      {
        this.IdentifierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Jurisdiction
    {
      get
      {
        return this.JurisdictionField;
      }
      set
      {
        this.JurisdictionField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFPatronInfo PatronInfo
    {
      get
      {
        return this.PatronInfoField;
      }
      set
      {
        this.PatronInfoField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFAttribute", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFAttribute : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string AttributeField;

    private string AttributeTypeField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Attribute
    {
      get
      {
        return this.AttributeField;
      }
      set
      {
        this.AttributeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AttributeType
    {
      get
      {
        return this.AttributeTypeField;
      }
      set
      {
        this.AttributeTypeField = value;
      }
    }
  }

  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "IdentificationType", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public enum IdentificationType : int
  {

    [System.Runtime.Serialization.EnumMemberAttribute()]
    None = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    DriversLicense = 1,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    StateID = 2,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Passport = 3,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    StudentID = 4,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    CreditCard = 5,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    DebitCard = 6,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    MagStripe = 7,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    SmartCard = 8,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Description = 9,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Custom = 10,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Generic = 11,
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFIdentityInfo", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFIdentityInfo : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string IdentityDataField;

    private MGT.UIF.Template.IdentityType IdentityTypeField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string IdentityData
    {
      get
      {
        return this.IdentityDataField;
      }
      set
      {
        this.IdentityDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.IdentityType IdentityType
    {
      get
      {
        return this.IdentityTypeField;
      }
      set
      {
        this.IdentityTypeField = value;
      }
    }
  }

  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "IdentityType", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public enum IdentityType : int
  {

    [System.Runtime.Serialization.EnumMemberAttribute()]
    None = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    PIN = 1,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Password = 2,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    SecurityQuestion = 3,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    ScanFinger = 4,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    ScanRetina = 5,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    ScanFacial = 6,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    ProximityCard = 7,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    ProximityDevice = 8,
  }

  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "MetricGranularity", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public enum MetricGranularity : int
  {

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Day = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Detail = 1,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Trip = 2,
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "MetricGroup", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class MetricGroup : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private System.DateTime EndDateField;

    private string GranularityField;

    private bool IsByMetaTypeField;

    private string MetaTypeField;

    private MGT.UIF.Template.Metric[] MetricsField;

    private long PatronIDField;

    private string SourceIDField;

    private string SourceTypeField;

    private System.DateTime StartDateField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime EndDate
    {
      get
      {
        return this.EndDateField;
      }
      set
      {
        this.EndDateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Granularity
    {
      get
      {
        return this.GranularityField;
      }
      set
      {
        this.GranularityField = value;
      }
    }

    /// <summary>
    /// This Boolean flag is an indicator of how the Metric objects contained by the MetricGroup are described.
    ///                                                 IsByMetaType = False
    /// 
    ///                                                                FIELD:                    METATYPE:
    ///                                                                SlotIn                    Empty
    ///                                                                SlotOut                   Empty
    ///                                                                SlotEP                    Empty
    ///                                                                PitIn                     Empty
    ///                                                                PitOut                    Empty
    ///                                                                Etc.
    ///                
    ///                                                IsByMetaType = True
    ///                                                                FIELD:                     METATYPE:
    ///                                                                In                         Slot
    ///                                                                Out                        Slot
    ///                                                                EP                         Slot
    ///                                                                In                         Pit
    ///                                                                Out                        Pit
    ///                                                                Etc.
    /// </summary>
    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsByMetaType
    {
      get
      {
        return this.IsByMetaTypeField;
      }
      set
      {
        this.IsByMetaTypeField = value;
      }
    }

    /// <summary>
    /// Represents what type of data is contained in the MetricGroup.  Possible fields could be GamingActivity or Play.   
    /// (The MetricGroup and Metric objects are designed to support any type of metric data.)
    /// </summary>
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string MetaType
    {
      get
      {
        return this.MetaTypeField;
      }
      set
      {
        this.MetaTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.Metric[] Metrics
    {
      get
      {
        return this.MetricsField;
      }
      set
      {
        this.MetricsField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long PatronID
    {
      get
      {
        return this.PatronIDField;
      }
      set
      {
        this.PatronIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SourceID
    {
      get
      {
        return this.SourceIDField;
      }
      set
      {
        this.SourceIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SourceType
    {
      get
      {
        return this.SourceTypeField;
      }
      set
      {
        this.SourceTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime StartDate
    {
      get
      {
        return this.StartDateField;
      }
      set
      {
        this.StartDateField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "Metric", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class Metric : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private string FieldField;

    private string MetaTypeField;

    private decimal ValueField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Field
    {
      get
      {
        return this.FieldField;
      }
      set
      {
        this.FieldField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string MetaType
    {
      get
      {
        return this.MetaTypeField;
      }
      set
      {
        this.MetaTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Value
    {
      get
      {
        return this.ValueField;
      }
      set
      {
        this.ValueField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFPatronOption", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFPatronOption : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private System.DateTime AnniversaryField;

    private System.DateTime BDayField;

    private bool BadAddField;

    private double CardTierField;

    private string CardTierDescField;

    private string CityField;

    private double CompsEarnedTodayField;

    private string Email1Field;

    private string Email2Field;

    private System.DateTime EnrollDateField;

    private string FirstField;

    private string GenderField;

    private bool IsCustomField;

    private bool IsNullEntityField;

    private string LastField;

    private string NickNameField;

    private long PatronIDField;

    private double PointsEarnedTodayField;

    private string StateField;

    private string ZipField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime Anniversary
    {
      get
      {
        return this.AnniversaryField;
      }
      set
      {
        this.AnniversaryField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime BDay
    {
      get
      {
        return this.BDayField;
      }
      set
      {
        this.BDayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool BadAdd
    {
      get
      {
        return this.BadAddField;
      }
      set
      {
        this.BadAddField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double CardTier
    {
      get
      {
        return this.CardTierField;
      }
      set
      {
        this.CardTierField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CardTierDesc
    {
      get
      {
        return this.CardTierDescField;
      }
      set
      {
        this.CardTierDescField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string City
    {
      get
      {
        return this.CityField;
      }
      set
      {
        this.CityField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double CompsEarnedToday
    {
      get
      {
        return this.CompsEarnedTodayField;
      }
      set
      {
        this.CompsEarnedTodayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Email1
    {
      get
      {
        return this.Email1Field;
      }
      set
      {
        this.Email1Field = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Email2
    {
      get
      {
        return this.Email2Field;
      }
      set
      {
        this.Email2Field = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.DateTime EnrollDate
    {
      get
      {
        return this.EnrollDateField;
      }
      set
      {
        this.EnrollDateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string First
    {
      get
      {
        return this.FirstField;
      }
      set
      {
        this.FirstField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Gender
    {
      get
      {
        return this.GenderField;
      }
      set
      {
        this.GenderField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsCustom
    {
      get
      {
        return this.IsCustomField;
      }
      set
      {
        this.IsCustomField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool IsNullEntity
    {
      get
      {
        return this.IsNullEntityField;
      }
      set
      {
        this.IsNullEntityField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Last
    {
      get
      {
        return this.LastField;
      }
      set
      {
        this.LastField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string NickName
    {
      get
      {
        return this.NickNameField;
      }
      set
      {
        this.NickNameField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public long PatronID
    {
      get
      {
        return this.PatronIDField;
      }
      set
      {
        this.PatronIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double PointsEarnedToday
    {
      get
      {
        return this.PointsEarnedTodayField;
      }
      set
      {
        this.PointsEarnedTodayField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string State
    {
      get
      {
        return this.StateField;
      }
      set
      {
        this.StateField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Zip
    {
      get
      {
        return this.ZipField;
      }
      set
      {
        this.ZipField = value;
      }
    }
  }

  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "RedemptionType", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public enum RedemptionType : int
  {

    [System.Runtime.Serialization.EnumMemberAttribute()]
    None = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Comp = 1,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Point = 2,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Cash = 3,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    PromoCredits = 4,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Other = 5,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Point2 = 6,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Point3 = 7,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Point4 = 8,
  }

  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "AdjustmentType", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public enum AdjustmentType : int
  {

    [System.Runtime.Serialization.EnumMemberAttribute()]
    None = 0,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Debit = 1,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Credit = 2,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Redeem = 3,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Multiply = 4,

    [System.Runtime.Serialization.EnumMemberAttribute()]
    Requirement = 5,
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFRedemptionItem", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFRedemptionItem : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private decimal AmountField;

    private double AmountRedeemedField;

    private bool AvailableField;

    private string DescriptionField;

    private string DisclaimerField;

    private double DollarLimitField;

    private string ItemIDField;

    private string ItemTypeField;

    private string OutletField;

    private MGT.UIF.Template.UIFRedemptionParameter[] RedemptionParametersField;

    private int SiteIDField;

    private string SystemItemIDField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal Amount
    {
      get
      {
        return this.AmountField;
      }
      set
      {
        this.AmountField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double AmountRedeemed
    {
      get
      {
        return this.AmountRedeemedField;
      }
      set
      {
        this.AmountRedeemedField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public bool Available
    {
      get
      {
        return this.AvailableField;
      }
      set
      {
        this.AvailableField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Description
    {
      get
      {
        return this.DescriptionField;
      }
      set
      {
        this.DescriptionField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Disclaimer
    {
      get
      {
        return this.DisclaimerField;
      }
      set
      {
        this.DisclaimerField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double DollarLimit
    {
      get
      {
        return this.DollarLimitField;
      }
      set
      {
        this.DollarLimitField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ItemID
    {
      get
      {
        return this.ItemIDField;
      }
      set
      {
        this.ItemIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ItemType
    {
      get
      {
        return this.ItemTypeField;
      }
      set
      {
        this.ItemTypeField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Outlet
    {
      get
      {
        return this.OutletField;
      }
      set
      {
        this.OutletField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.UIFRedemptionParameter[] RedemptionParameters
    {
      get
      {
        return this.RedemptionParametersField;
      }
      set
      {
        this.RedemptionParametersField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public int SiteID
    {
      get
      {
        return this.SiteIDField;
      }
      set
      {
        this.SiteIDField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SystemItemID
    {
      get
      {
        return this.SystemItemIDField;
      }
      set
      {
        this.SystemItemIDField = value;
      }
    }
  }

  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
  [System.Runtime.Serialization.DataContractAttribute(Name = "UIFRedemptionParameter", Namespace = "http://schemas.datacontract.org/2004/07/MGT.UIF.Template")]
  public partial class UIFRedemptionParameter : object, System.Runtime.Serialization.IExtensibleDataObject
  {

    private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

    private double AmountRequiredField;

    private MGT.UIF.Template.RedemptionType RedemptionTypeField;

    public System.Runtime.Serialization.ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public double AmountRequired
    {
      get
      {
        return this.AmountRequiredField;
      }
      set
      {
        this.AmountRequiredField = value;
      }
    }

    [System.Runtime.Serialization.DataMemberAttribute()]
    public MGT.UIF.Template.RedemptionType RedemptionType
    {
      get
      {
        return this.RedemptionTypeField;
      }
      set
      {
        this.RedemptionTypeField = value;
      }
    }
  }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName = "IUIF")]
public interface IUIF
{

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPatron", ReplyAction = "http://tempuri.org/IUIF/GetPatronResponse")]
  MGT.UIF.Template.UIFPatron GetPatron(bool pIsAcctNotCardStripe, string pData, string pPINNumber);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPlay", ReplyAction = "http://tempuri.org/IUIF/GetPlayResponse")]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFPatron))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFAccount))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFAddress[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFAddress))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFBalance[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFBalance))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFContactInfo[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFContactInfo))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFCustomInfo[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFCustomInfo))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFIdentification[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFIdentification))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFAttribute[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFAttribute))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.IdentificationType))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFPatronInfo))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFIdentityInfo[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFIdentityInfo))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.IdentityType))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFTierRanking))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.MetricGranularity))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.MetricGroup))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.Metric[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.Metric))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFPatronOption[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFPatronOption))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.RedemptionType))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.AdjustmentType))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFRedemptionItem[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFRedemptionItem))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFRedemptionParameter[]))]
  [System.ServiceModel.ServiceKnownTypeAttribute(typeof(MGT.UIF.Template.UIFRedemptionParameter))]
  MGT.UIF.Template.MetricGroup GetPlay(long pPatronID, System.DateTime pFromDate, System.DateTime pThruDate, long pCasinoID = 0, MGT.UIF.Template.MetricGranularity pGamingActivityGranularity = MGT.UIF.Template.MetricGranularity.Day) ;

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPatronsByName", ReplyAction = "http://tempuri.org/IUIF/GetPatronsByNameResponse")]
  MGT.UIF.Template.UIFPatronOption[] GetPatronsByName(string pFirstName, string pLastName);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPatronsByBirthdate", ReplyAction = "http://tempuri.org/IUIF/GetPatronsByBirthdateResponse")]
  MGT.UIF.Template.UIFPatronOption[] GetPatronsByBirthdate(System.DateTime pDateOfBirth);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPatronsByPhone", ReplyAction = "http://tempuri.org/IUIF/GetPatronsByPhoneResponse")]
  MGT.UIF.Template.UIFPatronOption[] GetPatronsByPhone(string pPhoneNumber);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/AdjustPatronBalance", ReplyAction = "http://tempuri.org/IUIF/AdjustPatronBalanceResponse")]
  string AdjustPatronBalance(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType, MGT.UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, string pSubType);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/AdjustPatronBalanceWExp", ReplyAction = "http://tempuri.org/IUIF/AdjustPatronBalanceWExpResponse")]
  string AdjustPatronBalanceWExp(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType, MGT.UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, System.DateTime pExpirationDate, string pUserID, string pSubType, System.DateTime pStartDate);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/ValidatePIN", ReplyAction = "http://tempuri.org/IUIF/ValidatePINResponse")]
  string ValidatePIN(long pPatronID, string pPINNumber);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/RedeemItem", ReplyAction = "http://tempuri.org/IUIF/RedeemItemResponse")]
  string RedeemItem(MGT.UIF.Template.RedemptionType pRedemptionType, string pPlayerTrackingItemCode, double pQuantity, double pAmount, string pReason, string pUserID, string pWorkstation);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetRedemptionItems", ReplyAction = "http://tempuri.org/IUIF/GetRedemptionItemsResponse")]
  MGT.UIF.Template.UIFRedemptionItem[] GetRedemptionItems(long pPatronID);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/Settle", ReplyAction = "http://tempuri.org/IUIF/SettleResponse")]
  bool Settle(long pPatronID, string pIssuanceID, decimal pAmount, string pUserID);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/Decode", ReplyAction = "http://tempuri.org/IUIF/DecodeResponse")]
  long Decode(string pCardStripe);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/ImportData", ReplyAction = "http://tempuri.org/IUIF/ImportDataResponse")]
  bool ImportData(string pImportType, string pImportSubType, string pLocationID, System.DateTime pStartDate, System.DateTime pEndDate);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/GetPatronBalances", ReplyAction = "http://tempuri.org/IUIF/GetPatronBalancesResponse")]
  MGT.UIF.Template.UIFBalance[] GetPatronBalances(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/AddPatron", ReplyAction = "http://tempuri.org/IUIF/AddPatronResponse")]
  MGT.UIF.Template.UIFPatron AddPatron(MGT.UIF.Template.UIFPatron pPatron);

  [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IUIF/UpdatePatron", ReplyAction = "http://tempuri.org/IUIF/UpdatePatronResponse")]
  MGT.UIF.Template.UIFPatron UpdatePatron(MGT.UIF.Template.UIFPatron pPatron);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public interface IUIFChannel : IUIF, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class UIFClient : System.ServiceModel.ClientBase<IUIF>, IUIF
{

  public UIFClient()
  {
  }

  public UIFClient(string endpointConfigurationName) :
    base(endpointConfigurationName)
  {
  }

  public UIFClient(string endpointConfigurationName, string remoteAddress) :
    base(endpointConfigurationName, remoteAddress)
  {
  }

  public UIFClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
    base(endpointConfigurationName, remoteAddress)
  {
  }

  public UIFClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
    base(binding, remoteAddress)
  {
  }

  public MGT.UIF.Template.UIFPatron GetPatron(bool pIsAcctNotCardStripe, string pData, string pPINNumber)
  {
    return base.Channel.GetPatron(pIsAcctNotCardStripe, pData, pPINNumber);
  }

  public MGT.UIF.Template.MetricGroup GetPlay(long pPatronID, System.DateTime pFromDate, System.DateTime pThruDate, long pCasinoID, MGT.UIF.Template.MetricGranularity pGamingActivityGranularity)
  {
    return base.Channel.GetPlay(pPatronID, pFromDate, pThruDate, pCasinoID, pGamingActivityGranularity);
  }

  public MGT.UIF.Template.UIFPatronOption[] GetPatronsByName(string pFirstName, string pLastName)
  {
    return base.Channel.GetPatronsByName(pFirstName, pLastName);
  }

  public MGT.UIF.Template.UIFPatronOption[] GetPatronsByBirthdate(System.DateTime pDateOfBirth)
  {
    return base.Channel.GetPatronsByBirthdate(pDateOfBirth);
  }

  public MGT.UIF.Template.UIFPatronOption[] GetPatronsByPhone(string pPhoneNumber)
  {
    return base.Channel.GetPatronsByPhone(pPhoneNumber);
  }

  public string AdjustPatronBalance(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType, MGT.UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, string pSubType)
  {
    return base.Channel.AdjustPatronBalance(pPatronID, pRedemptionType, pAdjustmentType, pAmount, pReason, pSubType);
  }

  public string AdjustPatronBalanceWExp(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType, MGT.UIF.Template.AdjustmentType pAdjustmentType, decimal pAmount, string pReason, System.DateTime pExpirationDate, string pUserID, string pSubType, System.DateTime pStartDate)
  {
    return base.Channel.AdjustPatronBalanceWExp(pPatronID, pRedemptionType, pAdjustmentType, pAmount, pReason, pExpirationDate, pUserID, pSubType, pStartDate);
  }

  public string ValidatePIN(long pPatronID, string pPINNumber)
  {
    return base.Channel.ValidatePIN(pPatronID, pPINNumber);
  }

  public string RedeemItem(MGT.UIF.Template.RedemptionType pRedemptionType, string pPlayerTrackingItemCode, double pQuantity, double pAmount, string pReason, string pUserID, string pWorkstation)
  {
    return base.Channel.RedeemItem(pRedemptionType, pPlayerTrackingItemCode, pQuantity, pAmount, pReason, pUserID, pWorkstation);
  }

  public MGT.UIF.Template.UIFRedemptionItem[] GetRedemptionItems(long pPatronID)
  {
    return base.Channel.GetRedemptionItems(pPatronID);
  }

  public bool Settle(long pPatronID, string pIssuanceID, decimal pAmount, string pUserID)
  {
    return base.Channel.Settle(pPatronID, pIssuanceID, pAmount, pUserID);
  }

  public long Decode(string pCardStripe)
  {
    return base.Channel.Decode(pCardStripe);
  }

  public bool ImportData(string pImportType, string pImportSubType, string pLocationID, System.DateTime pStartDate, System.DateTime pEndDate)
  {
    return base.Channel.ImportData(pImportType, pImportSubType, pLocationID, pStartDate, pEndDate);
  }

  public MGT.UIF.Template.UIFBalance[] GetPatronBalances(long pPatronID, MGT.UIF.Template.RedemptionType pRedemptionType)
  {
    return base.Channel.GetPatronBalances(pPatronID, pRedemptionType);
  }

  public MGT.UIF.Template.UIFPatron AddPatron(MGT.UIF.Template.UIFPatron pPatron)
  {
    return base.Channel.AddPatron(pPatron);
  }

  public MGT.UIF.Template.UIFPatron UpdatePatron(MGT.UIF.Template.UIFPatron pPatron)
  {
    return base.Channel.UpdatePatron(pPatron);
  }
}
