﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingActivity.cs
// 
//   DESCRIPTION: Classes that implement and load the MetricGroup object, about gaming activity, requested by MGT
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 04-DIC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-DIC-2015 FAV    First release.
//------------------------------------------------------------------------------
using MGT.UIF.Template;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;

namespace MGT.Converters
{
  public class GamingActivity
  {
    IPlaySessions m_play_sessions;

    #region Constructor
    public GamingActivity(IPlaySessions PlaySessions)
    {
      m_play_sessions = PlaySessions;
    }
    #endregion

    #region Public Methods
    public void GetPlay(Int64 AccountId, DateTime DateFrom, DateTime DateTo, MetricGranularity pGamingActivityGranularity, 
                        String SiteID, Int32 ClosingTime, out MetricGroup MetricGroup)
    {
      List<Metric> _metric_list = new List<Metric>();

      // Build the MetricGroup using the information sent by MGT
      MetricGroup = new MetricGroup()
      {
        EndDate = DateTo,
        Granularity = pGamingActivityGranularity.ToString(),
        IsByMetaType = false, // If is 'false' then all metric objects are by example--> Field:SlotIn, MetaType:Empty ; Field:SlotOut, MetaType: Empty
        MetaType = "GamingActivity",
        PatronID = AccountId,
        SourceID = SiteID,
        SourceType = "Site",
        StartDate = DateFrom
      };

      switch (pGamingActivityGranularity)
      {
        case MetricGranularity.Day:
        case MetricGranularity.Trip:

          // It should use the Closing time in the days parameters.
          DateFrom = DateFrom.Date.AddHours(ClosingTime);
          DateTo = DateTo.Date.AddHours(ClosingTime);
          break;
        case MetricGranularity.Detail:
          // I will use the time passed in the date parameters
          break;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        PlaySessionSummaryInfo _play_session_info;

        if (m_play_sessions.GetPlaysSessionsSummaryByAccountId(AccountId, DateFrom, DateTo, null, out _play_session_info, _db_trx.SqlTransaction))
        {
          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotIn, _play_session_info.PlayedAmount));
          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotOut, _play_session_info.WonAmount));

          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotCompErn, _play_session_info.ComputedPoints));
          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotPtsErn, _play_session_info.AwardedPoints));
          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotPtsRed, _play_session_info.ReCashIn));
        }

        if (m_play_sessions.GetPlaysSessionsSummaryByAccountId(AccountId, DateFrom, DateTo, 
          new List<PlaySessionType>() { PlaySessionType.HANDPAY_JACKPOT, PlaySessionType.HANDPAY_SITE_JACKPOT},
          out _play_session_info, _db_trx.SqlTransaction))
        {
          _metric_list.Add(GetMetricDetail(MGTFieldName.SlotJP, _play_session_info.WonAmount));
        }

        _metric_list.Add(GetMetricDetail(MGTFieldName.TotDays, m_play_sessions.GetDaysWithPlayByAccountId(AccountId, DateFrom, DateTo, _db_trx.SqlTransaction)));

        _db_trx.Commit();
      }

      MetricGroup.Metrics = _metric_list.ToArray();

    }

    #endregion

    #region Private Methods

    private Metric GetMetricDetail(MGTFieldName MGTFieldName, Decimal Value)
    {
      Metric _metric = new Metric();

      _metric.MetaType = String.Empty; // MetaType is empty
      _metric.Field = MGTFieldName.ToString();
      _metric.Value = Value;
      return _metric;
    }

    #endregion
  }

  public class PlaySessionSummaryInfo
  {
    #region Properties
    public Int64 AccountId { get; set; }

    public Int32 PlayedCount { get; set; }

    public Decimal PlayedAmount { get; set; }

    public Int32 WonCount { get; set; }

    public Decimal WonAmount { get; set; }

    public Decimal CashIn { get; set; }

    public Decimal CashOut { get; set; }

    public Decimal ComputedPoints { get; set; }

    public Decimal AwardedPoints { get; set; }

    public Decimal ReCashIn { get; set; }

    public DateTime? FinishedDate { get; set; }

    #endregion
  }

  public class PlaySessions : IPlaySessions
  {
    #region Public Methods

    /// <summary>
    /// Gets a summary of 'Play  by account Id from 'Play sessions'
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="DateFrom"></param>
    /// <param name="DateTo"></param>
    /// <param name="TypeList"></param>
    /// <param name="PlaySessionSummary"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean GetPlaysSessionsSummaryByAccountId(Int64 AccountId, DateTime DateFrom, DateTime DateTo, List<PlaySessionType> TypeList,
                                                      out PlaySessionSummaryInfo PlaySessionSummary, SqlTransaction Trx)
    {
      StringBuilder _sb;
      PlaySessionSummary = new PlaySessionSummaryInfo();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT     SUM(PS_PLAYED_COUNT)  AS PS_PLAYED_COUNT                    ");
        _sb.AppendLine("           , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT                   ");
        _sb.AppendLine("           , SUM(PS_WON_AMOUNT)    AS PS_WON_AMOUNT                      ");
        _sb.AppendLine("           , SUM(PS_WON_COUNT)     AS PS_WON_COUNT                       ");
        _sb.AppendLine("           , SUM(PS_CASH_IN)       AS PS_CASH_IN                         ");
        _sb.AppendLine("           , SUM(PS_CASH_OUT)      AS PS_CASH_OUT                        ");
        _sb.AppendLine("           , SUM(PS_COMPUTED_POINTS)      AS PS_COMPUTED_POINTS          ");
        _sb.AppendLine("           , SUM(PS_AWARDED_POINTS)      AS PS_AWARDED_POINTS            ");
        _sb.AppendLine("           , SUM(PS_RE_CASH_IN)      AS PS_RE_CASH_IN                    ");
        _sb.AppendLine("    FROM     PLAY_SESSIONS                                               ");
        _sb.AppendLine("   WHERE     PS_ACCOUNT_ID  =  @pAccountId                               ");
        _sb.AppendLine("     AND     PS_STATUS      <> @pStatusOpened                            ");
        _sb.AppendLine("     AND	   PS_FINISHED    >= @pDateFrom                                ");
        _sb.AppendLine("     AND	   PS_FINISHED    <  @pDateTo                                  ");
        if (TypeList != null && TypeList.Count > 0)
        {
          _sb.AppendLine("     AND	   PS_TYPE        IN (" + GetPlaySessionTypeIDList(TypeList) + ")");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              PlaySessionSummaryInfo _item = new PlaySessionSummaryInfo()
              {
                AccountId = AccountId,
                CashIn = (Decimal)_reader["PS_CASH_IN"],
                CashOut = (Decimal)_reader["PS_CASH_OUT"],
                PlayedAmount = (Decimal)_reader["PS_PLAYED_AMOUNT"],
                PlayedCount = (Int32)_reader["PS_PLAYED_COUNT"],
                WonAmount = (Decimal)_reader["PS_WON_AMOUNT"],
                WonCount = (Int32)_reader["PS_WON_COUNT"],
                ComputedPoints = (Decimal)_reader["PS_COMPUTED_POINTS"],
                AwardedPoints = (Decimal)_reader["PS_AWARDED_POINTS"],
                ReCashIn = (Decimal)_reader["PS_RE_CASH_IN"],
              };

              PlaySessionSummary = _item;
            }

            return true;
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2)
        {
          Log.Error(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    /// <summary>
    /// It gets the number of days with activity of an Account by Id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="DateFrom"></param>
    /// <param name="DateTo"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Int32 GetDaysWithPlayByAccountId(Int64 AccountId, DateTime DateFrom, DateTime DateTo, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT     CONVERT(VARCHAR(10),PS_FINISHED,112)                        ");
        _sb.AppendLine("           , SUM(PS_PLAYED_AMOUNT) AS PS_PLAYED_AMOUNT                   ");
        _sb.AppendLine("    FROM     PLAY_SESSIONS                                               ");
        _sb.AppendLine("   WHERE     PS_ACCOUNT_ID  =  @pAccountId                               ");
        _sb.AppendLine("     AND     PS_STATUS      <> @pStatusOpened                            ");
        _sb.AppendLine("     AND	   PS_FINISHED    >= @pDateFrom                                ");
        _sb.AppendLine("     AND	   PS_FINISHED    <  @pDateTo                                  ");
        _sb.AppendLine("GROUP BY     CONVERT(VARCHAR(10),PS_FINISHED,112)                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              using (DataTable dt = new DataTable())
              {
                dt.Load(_reader);
                return dt.Rows.Count;
              }
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number != -2)
        {
          Log.Error(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Gets a string of enum values (Integers) for the enum list passed like a parameter
    /// </summary>
    /// <param name="TypeList"></param>
    /// <returns></returns>
    private String GetPlaySessionTypeIDList(List<PlaySessionType> TypeList)
    {
      String _types = string.Empty;
      foreach (PlaySessionType _type in TypeList)
      {
        _types += (int)_type + ", ";
      }
      return _types.Trim().Trim(',');
    }

    #endregion
  }

  public interface IPlaySessions
    {
      Boolean GetPlaysSessionsSummaryByAccountId(Int64 AccountId, DateTime DateFrom, DateTime DateTo, List<PlaySessionType> TypeList,
                                                      out PlaySessionSummaryInfo PlaySessionSummary, SqlTransaction Trx);

      Int32 GetDaysWithPlayByAccountId(Int64 AccountId, DateTime DateFrom, DateTime DateTo, SqlTransaction Trx);
    }
}
