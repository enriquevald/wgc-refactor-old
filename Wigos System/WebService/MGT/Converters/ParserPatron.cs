﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ParserPatron.cs
// 
//   DESCRIPTION: Class used for parse account datatable to MGT UIFPatron / UIFPatronOption classes
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 01-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-DEC-2015 ETP    First release.
//------------------------------------------------------------------------------


using MGT.UIF.Template;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;

namespace MGT
{
  static class MGTParserPatron
  {
    #region public functions

    /// <summary>
    ///Gets UIfPatronOptionArray by DataTable.
    /// </summary>
    /// <param name="Data">DataTabel to parse</param>
    /// <returns>UIFPatronOption Array</returns>
    public static UIF.Template.UIFPatronOption[] GetUIFPatronOptionArray(DataTable Data)
    {
      List<UIFPatronOption> _patron_option_list;
      _patron_option_list = new List<UIFPatronOption>();

      foreach (DataRow row in Data.Rows)
      {
        UIFPatronOption _patron_option;

        _patron_option = new UIFPatronOption()
       {
         //  Anniversary
         //  BadAdd
         BDay = row["AC_HOLDER_BIRTH_DATE"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["AC_HOLDER_BIRTH_DATE"],
        
         //  CardTier
         //  CardTierDesc
         City = row["AC_HOLDER_CITY"] == DBNull.Value ? null : (String)row["AC_HOLDER_CITY"],
        
         //   CompsEarnedToday
         Email1 = row["AC_HOLDER_EMAIL_01"] == DBNull.Value ? null : (String)row["AC_HOLDER_EMAIL_01"],
         Email2 = row["AC_HOLDER_EMAIL_02"] == DBNull.Value ? null : (String)row["AC_HOLDER_EMAIL_02"],
        
         EnrollDate = (DateTime)row["AC_CREATED"],
         First = row["AC_HOLDER_NAME3"] == DBNull.Value ? null : (String)row["AC_HOLDER_NAME3"],
         Gender = GetGenderByInt(row["AC_HOLDER_GENDER"]),
         //   IsCustom

         //   IsNullEntity
         Last = row["AC_HOLDER_NAME1"] == DBNull.Value ? null : (String)row["AC_HOLDER_NAME1"],
         NickName = row["AC_HOLDER_NICKNAME"] == DBNull.Value ? null : (String)row["AC_HOLDER_NICKNAME"],
         PatronID = (long)row["AC_ACCOUNT_ID"],

         //   PointsEarnedToday
         State = row["AC_HOLDER_STATE"] == DBNull.Value ? null : (String)row["AC_HOLDER_STATE"],
         Zip = row["AC_HOLDER_ZIP"] == DBNull.Value ? null : (String)row["AC_HOLDER_ZIP"]
       };

        _patron_option_list.Add(_patron_option);
      }

      return _patron_option_list.ToArray();
    }
    #endregion


    #region private functions
    /// <summary>
    /// Function that returns String by Wigos BD Data (Int).
    /// </summary>
    /// <param name="Gender">Gender</param>
    /// <returns>Gender string</returns>
    private static String GetGenderByInt(Object Gender)
    {
      String _str_gender;
      GENDER _i_gender;
     
      _i_gender = Gender == DBNull.Value ? GENDER.UNKNOWN : (GENDER)Gender;
     
      switch (_i_gender)
      {
        case GENDER.MALE:
          _str_gender = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE");
          break;
        case GENDER.FEMALE:
          _str_gender = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE");
          break;
        default:
          _str_gender = null;
          break;
      }

      return _str_gender;
    }


  }
    #endregion
}
