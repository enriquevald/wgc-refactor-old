﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MGT
{
  public enum MGTFieldName
  { 
    SlotIn,
    SlotOut, 
    SlotJP, 
    SlotWin,
    SlotEP,
    SlotCompErn,
    SlotPtsErn,
    SlotCompRed,
    SlotPtsRed,
    SlotDays, 
    SlotTImePlayed,

    PitIn, 
    PitOut, 
    PitJP, 
    PitWin, 
    PitEP, 
    PitCompErn, 
    PitPtsErn, 
    PitCompRed,
    PitPtsRed,
    PitDays,
    PitAvgWager,
    PitTimePlayed,

    OtherIn, 
    OtherOut, 
    OtherJP, 
    OtherWin, 
    OtherEP, 
    OtherCompErn, 
    OtherPtsErn, 
    OtherCompRed, 
    OtherPtsRed,
    OtherDays, 
    OtherTimePlayed,

    TotDays,
    TotalCompsRed,
    TotalPtsRed 
  }

}
