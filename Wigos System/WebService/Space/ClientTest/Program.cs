﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml.Serialization;

namespace ClientTest
{
  class Program
  {
    static void Main(string[] args)
    {
      Parameters _parameters;
      WigosServiceClient.WigosServiceClient _canjes;
      WigosServiceClient.CreditRequest _request;
      WigosServiceClient.CreditResponse _response;
      EndpointAddressBuilder _endpoint;

      // Create xml entrance
      // WriteXML();

      if (!File.Exists("ClientTest.xml"))
      {
        Console.WriteLine("Not exists ClientTest.xml");
        PrintClientTest();

        return;
      }

      _parameters = new Parameters();
      _parameters = ReadXML();

      try
      {
        _canjes = new WigosServiceClient.WigosServiceClient();
        _endpoint = new EndpointAddressBuilder(_canjes.Endpoint.Address);
        _endpoint.Uri = new Uri(_parameters.Uri);
        _response = new WigosServiceClient.CreditResponse();
        _request = new WigosServiceClient.CreditRequest();

        //Create EndPoint
        //_endpoint.Headers.Add(AddressHeader.CreateAddressHeader("UserId", String.Empty, "resu@user"));
        //_endpoint.Headers.Add(AddressHeader.CreateAddressHeader("Password", String.Empty, "2016@reSu"));
        _endpoint.Headers.Add(AddressHeader.CreateAddressHeader("UserId", String.Empty, _parameters.UserId));
        _endpoint.Headers.Add(AddressHeader.CreateAddressHeader("Password", String.Empty, _parameters.Password));
        _canjes.Endpoint.Address = _endpoint.ToEndpointAddress();

        //Create Request
        //_request.AccountId = 1002302;
        //_request.ExternalCustomerId = 0; // Numero en SPACE
        //_request.Trackdata = "";
        //_request.AmountCents = 100;
        //_request.CreditType = WigosServiceClient.SharedDefinitionsCreditType.Cashable;       
        //_request.NonCashableGroupId = 1;
        //_request.SiteId = 302;
        //_request.TransactionId = Guid.NewGuid().ToString();

        _request.AccountId = _parameters.AccountId;
        _request.ExternalCustomerId = _parameters.ExternalCustomerId;
        _request.Trackdata = _parameters.Trackdata;
        _request.AmountCents = _parameters.AmountCents;
        _request.CreditType = _parameters.CreditType;
        _request.NonCashableGroupId = _parameters.NonCashableGroupId;
        _request.SiteId = _parameters.SiteId;
        _request.TransactionId = _parameters.TransactionId;

        _response = _canjes.Credit(_request);

        Console.WriteLine(String.Format("Response Status[{0}] TransactionId[{1}]", _response.Status, _response.TransactionId));
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }

      Console.ReadKey();
    } // Main

    /// <summary>
    /// Print ClienteTest.xml example
    /// </summary>
    private static void PrintClientTest()
    {
      Console.WriteLine("EXAMPLE:");
      Console.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
      Console.WriteLine("<Parameters xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
      Console.WriteLine("  <TransactionId>JCA-1</TransactionId>");
      Console.WriteLine("  <ExternalCustomerId>2</ExternalCustomerId>");
      Console.WriteLine("  <Trackdata>123track</Trackdata>");
      Console.WriteLine("  <AccountId>1</AccountId>");
      Console.WriteLine("  <SiteId>302</SiteId>");
      Console.WriteLine("  <CreditType>Cashable</CreditType>");
      Console.WriteLine("  <AmountCents>100</AmountCents>");
      Console.WriteLine("  <NonCashableGroupId>0</NonCashableGroupId>");
      Console.WriteLine("  <Uri>http://winsystemsintl.com:7774//WigosService</Uri>");
      Console.WriteLine("  <UserId>resu@user</UserId>");
      Console.WriteLine("  <Password>2016@reSu</Password>");
      Console.WriteLine("</Parameters>");
    } // PrintClientTest

    /// <summary>
    /// Write XML
    /// </summary>
    public static void WriteXML()
    {
      try
      {
        Parameters _parameters = new Parameters();
        _parameters.AccountId = 1;
        _parameters.ExternalCustomerId = 2;
        _parameters.Trackdata = "123track";
        _parameters.AmountCents = 100;
        _parameters.CreditType = WigosServiceClient.SharedDefinitionsCreditType.Cashable;
        _parameters.NonCashableGroupId = 0;
        _parameters.SiteId = 302;
        _parameters.TransactionId = "JCA-1";
        _parameters.Uri = "http://winsystemsintl.com:7774//WigosService";
        _parameters.UserId = "resu@user";
        _parameters.Password = "2016@reSu";
        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(Parameters));
        System.IO.StreamWriter file = new System.IO.StreamWriter("ClientTest.xml");
        writer.Serialize(file, _parameters);
        file.Close();
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } // WriteXML

    /// <summary>
    /// Read XML
    /// </summary>
    public static Parameters ReadXML()
    {

      Parameters _parameters = new Parameters();

      try
      {
        System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Parameters));
        System.IO.StreamReader file = new System.IO.StreamReader("ClientTest.xml");
        _parameters = (Parameters)reader.Deserialize(file);
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }

      return _parameters;
    } // ReadXML

  } // Program

  [MessageContract]
  [DataContract]
  [Serializable]
  public class Parameters
  {
    #region Fields & Properties
    [MessageBodyMember(Order = 0)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    [XmlElementAttribute(IsNullable = false)]
    public String TransactionId { get; set; }                     // Identificador único de la transacción (podría ser el voucher-id de Space)

    [MessageBodyMember(Order = 1)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int64 ExternalCustomerId { get; set; }                 // Número de cliente en Space (0: si no se conoce)

    [MessageBodyMember(Order = 2)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public String Trackdata { get; set; }                         // Número de tarjeta 01234567890123456789 ("": si no se conoce)

    [MessageBodyMember(Order = 3)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int64 AccountId { get; set; }                          // Número de cuenta (0: si no se conoce)  

    [MessageBodyMember(Order = 4)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int32 SiteId { get; set; }                             // Identificador de la sala en la que se va a llevar a cabo la transacción

    [MessageBodyMember(Order = 5)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public WigosServiceClient.SharedDefinitionsCreditType CreditType { get; set; }  // Indica el tipo de crédito "Cashable", "NonCashable", "Tables"

    [MessageBodyMember(Order = 6)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Decimal AmountCents { get; set; }                      // Indica la cantidad a transferir

    [MessageBodyMember(Order = 7)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public Int32 NonCashableGroupId { get; set; }                // Opcional, si se informa, en el caso de NonCashable indica el tipo de promoción (restricción por máquina)

    [MessageBodyMember(Order = 8)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public String Uri { get; set; }                              // Uri

    [MessageBodyMember(Order = 9)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public String UserId { get; set; }                           // UserId

    [MessageBodyMember(Order = 10)]
    [XmlElementAttribute(IsNullable = false)]
    [DataMember(IsRequired = true, EmitDefaultValue = false)]
    public String Password { get; set; }                         // Password

    #endregion
  } // Parameters
}
