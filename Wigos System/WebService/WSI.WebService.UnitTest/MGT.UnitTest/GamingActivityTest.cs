﻿////------------------------------------------------------------------------------
//// Date        Author Description
//// ----------- ------ ----------------------------------------------------------
//// 29-JAN-2018 AGS    Bug 31349:WIGOS-7723 [Ticket #11857] Error al Convertir el valor del paremtero de Int64 a Int32
////------------------------------------------------------------------------------

using System;
using NUnit.Framework;
using MGT.Converters;
using NSubstitute;
using MGT.UIF.Template;
using WSI.Common;
using System.Collections.Generic;

namespace WSI.WebService.UnitTest.MGT
{
  [TestFixture]
  public class GamingActivityTest
  {
    Int32 m_closing_time;
    String m_site_id;
    DateTime m_end_date;
    DateTime m_start_date;
    Int64 m_account_id;

    [SetUp]
    public void Init()
    {
      m_account_id = 1;
      m_closing_time = 8; //08:00
      m_site_id = "1";
      m_end_date = DateTime.Now;
      m_start_date = m_end_date.AddDays(-7);
    }

    [Test]
    public void CheckGetDaysWithPlayInGetPlay()
    {
      MetricGroup _metric_gropup;

      //Arrange
      IPlaySessions _plays_sessions_mock = Substitute.For<IPlaySessions>();
      _plays_sessions_mock.GetDaysWithPlayByAccountId(m_account_id, m_start_date, m_end_date, null).ReturnsForAnyArgs(15);

      //Act
      GamingActivity _gaming_activity = new GamingActivity(_plays_sessions_mock);
      _gaming_activity.GetPlay(1, m_start_date, m_end_date, MetricGranularity.Day, m_site_id, m_closing_time, out _metric_gropup);

      //Assert
      Assert.IsTrue(_metric_gropup.Metrics[0].Field == "TotDays" && _metric_gropup.Metrics[0].Value == 15, "");
    }

    [Test]
    public void CheckGetPlaysSessionsSummaryInGetPlay()
    {
      MetricGroup _metric_gropup;
      PlaySessionSummaryInfo _play_session_summary;

      //Arrange
      IPlaySessions _plays_sessions_mock = Substitute.For<IPlaySessions>();
      _plays_sessions_mock.GetPlaysSessionsSummaryByAccountId(m_account_id, m_start_date, m_end_date, null, out _play_session_summary, null).ReturnsForAnyArgs(x =>
          {
            x[4] = GetValuesForTesting();
            return true;
          });

      //Act
      GamingActivity _gaming_activity = new GamingActivity(_plays_sessions_mock);
      _gaming_activity.GetPlay(1, m_start_date, m_end_date, MetricGranularity.Day, m_site_id, m_closing_time, out _metric_gropup);

      //Assert
      Assert.IsTrue(_metric_gropup.Metrics[0].Field == "SlotIn" && _metric_gropup.Metrics[0].Value == 100, "");
      Assert.IsTrue(_metric_gropup.Metrics[1].Field == "SlotOut" && _metric_gropup.Metrics[1].Value == 10, "");
    }

    /// <summary>
    /// It simulates information returned by the database loaded in the PlaySessionSummaryInfo object
    /// </summary>
    /// <returns></returns>
    private PlaySessionSummaryInfo GetValuesForTesting()
    {
      PlaySessionSummaryInfo _play_session_summary = new PlaySessionSummaryInfo()
      {
        AccountId = 1,
        PlayedAmount = 100,
        WonAmount = 10,
      };

      return _play_session_summary;
    }

  }
}
