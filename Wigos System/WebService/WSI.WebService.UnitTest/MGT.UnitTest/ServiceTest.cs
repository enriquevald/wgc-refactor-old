﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WSI.Common;
using NSubstitute;
using MGT;

namespace WSI.WebService.UnitTest.MGT.UnitTest
{
  [TestFixture]
  public class ServiceTest
  {
    [Test]
    public void DecodeTest()
    {
      Assert.Fail();
    }
    [Test]
    public void ValidatePIN_WithNoPatronIDTest()
    {
      var _service = Substitute.For<IUIF>();

      _service.ValidatePIN(0, "1111").Returns(PinCheckStatus.ERROR.ToString()); 

      Assert.AreEqual(PinCheckStatus.ERROR.ToString(), _service.ValidatePIN(0, "1111"));
      
    }
  }
}

