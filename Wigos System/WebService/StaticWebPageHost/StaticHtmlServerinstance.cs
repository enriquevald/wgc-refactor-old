﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Management.Instrumentation;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;

namespace WSI.Hosting
{
  public class StaticHtmlServerinstance
  {
    private readonly string m_file_path;
    private readonly string m_wcp_address;
    private readonly int m_port;
    private readonly Func<string,Exception, bool> m_func;
    private readonly bool m_restrict_subnet;
    private readonly string m_subnet_restriction_mask;

    public StaticHtmlServerinstance(string FilePath, string WcpAddress, int Port, Func<string,Exception, bool> Func, bool RestrictSubnet, string SubnetRestrictionMask)
    {
      m_file_path = FilePath;
      m_wcp_address = WcpAddress;
      m_port = Port;
      m_func = Func;
      m_restrict_subnet = RestrictSubnet;
      m_subnet_restriction_mask = SubnetRestrictionMask;
    }

    //AGP 07/SEO/2017 Restructured to easy debugging
    public IDisposable StartServer()
    {
      Exception fakeException = new Exception("Fake exception on logging.");
      m_func("Inside StartServer() function", fakeException);
      string strURL = "";

      Action<IAppBuilder> _serverconfig = _builder =>
      {
        m_func(string.Format("Path:{0}", m_file_path), fakeException);

        var _fs = new PhysicalFileSystem(m_file_path);
        var _op = new FileServerOptions
        {
          EnableDefaultFiles = true,
          FileSystem = _fs
        };
        _op.StaticFileOptions.FileSystem = _fs;
        _op.StaticFileOptions.ServeUnknownFileTypes = true;
        _op.DefaultFilesOptions.DefaultFileNames = new[]
        {"index.html", "index.htm", "default.html", "default.htm", "default.jpg", "default.png"};
        if (m_restrict_subnet)
        {
          _op.StaticFileOptions.OnPrepareResponse = (_context) =>
          {
            IPAddress _ipaddr_client = IPAddress.Parse(_context.OwinContext.Request.RemoteIpAddress);
            IPAddress _ipaddr_svr = IPAddress.Parse(_context.OwinContext.Request.LocalIpAddress);
            IPAddress _ipsubnet = IPAddress.Parse(m_subnet_restriction_mask);
            if (!_ipaddr_svr.IsInSameSubnet(_ipaddr_client, _ipsubnet))
            {
              _context.OwinContext.Response.StatusCode = 401;
              m_func("We are in the same subnet", fakeException);
            }

          };
        }
        _builder.Use((_owin_context, _next) =>
        {
          try
          {
          return _next().ContinueWith(_x =>
          {
            if (_owin_context == null)
                m_func("owin_context is null", fakeException);
            else
                m_func("owin_context is not empty", fakeException);

            if (_owin_context.Response.StatusCode == 404)
            {
              _owin_context.Response.Redirect("/");
              m_func("Redirecting to root (/)", fakeException);
            }
          });
          }
          catch (Exception _ex)
          {
            m_func(string.Format("error calling website http://{0}:{1}", m_wcp_address, m_port), _ex);
            return null;
          }
        });
        _builder.UseFileServer(_op);
      };

      strURL = "http://" + m_wcp_address + ":" + m_port.ToString() + "/";
      m_func("Path to start web:" + strURL, fakeException);

      m_func("Quitting from StartServer at the end of function", fakeException);

        return Microsoft.Owin.Hosting.WebApp.Start("http://" + m_wcp_address + ":" + m_port.ToString() + "/",
        _serverconfig);
    }
  }

  public static class IPAddressExtensions
  {
    public static IPAddress GetBroadcastAddress(this IPAddress address, IPAddress subnetMask)
    {
      byte[] ipAdressBytes = address.GetAddressBytes();
      byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

      if (ipAdressBytes.Length != subnetMaskBytes.Length)
        throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

      byte[] broadcastAddress = new byte[ipAdressBytes.Length];
      for (int i = 0; i < broadcastAddress.Length; i++)
      {
        broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
      }
      return new IPAddress(broadcastAddress);
    }

    public static IPAddress GetNetworkAddress(this IPAddress address, IPAddress subnetMask)
    {
      byte[] ipAdressBytes = address.GetAddressBytes();
      byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

      if (ipAdressBytes.Length != subnetMaskBytes.Length)
        throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

      byte[] broadcastAddress = new byte[ipAdressBytes.Length];
      for (int i = 0; i < broadcastAddress.Length; i++)
      {
        broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
      }
      return new IPAddress(broadcastAddress);
    }

    public static bool IsInSameSubnet(this IPAddress address2, IPAddress address, IPAddress subnetMask)
    {
      IPAddress network1 = address.GetNetworkAddress(subnetMask);
      IPAddress network2 = address2.GetNetworkAddress(subnetMask);

      return network1.Equals(network2);
    }
  }
}
