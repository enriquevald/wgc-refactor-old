//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_PSAClientReportesDiarios.cs
// 
//   DESCRIPTION: Procedures the Reportes diarios for PSA.
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 13-Nov-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-Nov-2012 DDM    First release 
// 16-JUL-2013 JCA    Change name to new Service
// 10-NOV-2014 AMF    Fixed Bug PSA-5: Only send current month
// 01-DEC-2014 JCA    Fixed Bug WIG-1780: Change "Sended" ==> "Sent"
// 16-NOV-2015 DLL    Change limit daily repot to 120 month
// 14-APR-2016 JCA    Bug 11881:PSA_Client: Error en el envio de constancias a Alesis (Problemas de comunicaci�n entre WebServices)
// 24-OCT-2016 FGB    PBI 11428: PSA Client-Alesis: LOG when deleting records for forwarding (call and ACK)
// 23-MAR-2018 JML    Bug 32051:WIGOS-9173 Discrepancy in prizes reported to PSA - San Luis Potosi (074)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.PSA_Client.WS_PSA;

namespace WSI.PSA_Client
{
  public static class PSA_ClientReportesDiarios
  {
    #region Enums
    public enum PSA_TYPE
    {
      WINPSA = 1,
      ALESIS = 2,
    }
    #endregion

    #region Members
    private const Int32 WAIT_HINT_ERROR = (5 * 60 * 1000);  // 5 minutes    
    private const Int32 WAIT_HINT_OK = (30 * 60 * 1000);    // 30 minutes
    private const Int32 NUMBER_OF_RETRY_FOR_GUI = 3;

    private const String SERVICE_NAME = "WWP PSA Send Reports";
    #endregion

    #region Events
    public static Boolean TrustAllCertificateCallback(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    } // TrustAllCertificateCallback
    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Initializes Get information to PSA and Starts PeriodicJobThread thread 
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    static public void Init()
    {
      Thread _thread;

      ServicePointManager.ServerCertificateValidationCallback += TrustAllCertificateCallback;

      _thread = new Thread(PeriodicJobThread);
      _thread.Name = "PeriodicJobThread";
      _thread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Periodic thread
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    static private void PeriodicJobThread()
    {
      Int32 _process_wait_hint;
      Int32 _num_of_month_to_report;
      DateTime _now;
      DateTime _last_month;
      DateTime _date_init_PSAClient;
      String _str_date_init_PSAClient;
      Boolean _all_reports_sent;
      Boolean _all_reports_month;

      Boolean _monitorizacion_sent;
      DateTime _next_shipping_reports_date;
      DateTime _next_shipping_monitorizacion_date;
      Int32 _loop_wait_hint;
      Boolean _allow_send_monitorizacion;
      int _tick_log;
      PSA_TYPE _psa_type;

      _process_wait_hint = 0;
      _loop_wait_hint = 0;
      _next_shipping_reports_date = DateTime.MinValue;
      _next_shipping_monitorizacion_date = DateTime.MinValue;
      _allow_send_monitorizacion = false;
      _tick_log = Misc.GetTickCount() - 4 * 60 * 60 * 1000;
      _date_init_PSAClient = WGDB.Now;

      while (true)
      {
        try
        {
          Thread.Sleep(_loop_wait_hint + 250); // add an extra 250 milliseconds offset 
          _loop_wait_hint = WAIT_HINT_ERROR;
          _now = WGDB.Now;

          //
          // 0 - Check that is configured the PSAClient
          //
          if (String.IsNullOrEmpty(GeneralParam.GetString(PSACommon.GP_PSA_GROUP, "ServerAddress1")) && String.IsNullOrEmpty(GeneralParam.GetString(PSACommon.GP_PSA_GROUP, "ServerAddress2")))
          {
            // AJQ 16-APR-2013, Avoid logging ...
            if (Misc.GetElapsedTicks(_tick_log) >= 4 * 60 * 60 * 1000)
            {
              Log.Message(" PSAClient Remote Addresses not defined");
              _tick_log = Misc.GetTickCount();
            }

            continue;
          }

          _psa_type = (PSA_TYPE)GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, "PSAType", (int)PSA_TYPE.WINPSA);
          //_psa_type = PSA_TYPE.ALESIS;

          //
          // 1 - Calculate date          
          //
          _str_date_init_PSAClient = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, "StartDateToReport");
          if (!String.IsNullOrEmpty(_str_date_init_PSAClient))
          {
            if (DateTime.TryParse(_str_date_init_PSAClient, out _date_init_PSAClient))
            {
              if (_now < _date_init_PSAClient)
              {
                Log.Warning(" Reporting start at: " + _date_init_PSAClient.ToString());

                continue;
              }
            }
            else
            {
              Log.Error(" Error in parse StartDateToReport: " + _str_date_init_PSAClient);

              continue;
            }
          }

          //
          // 2 - Reportes diarios
          //
          if (_next_shipping_reports_date <= _now)
          {
            _process_wait_hint = WAIT_HINT_ERROR;
            _num_of_month_to_report = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, "NumberOfPreviousMonthsToReport", 1, 0, 120);
            _last_month = new DateTime(_now.Year, _now.Month, 1);
            _last_month = _last_month.AddMonths(_num_of_month_to_report * -1);
            _all_reports_sent = true;

            // Send reportes
            for (Int32 _idx = 0; _idx < _num_of_month_to_report + 1; _idx++)
            {
              _all_reports_month = false;

              if (_psa_type == PSA_TYPE.WINPSA)
              {
                if (!SendReportesDiarios(_last_month, ref _all_reports_month))
                {
                  Log.Error(" Call SendReportesDiarios  Date: " + _last_month.Date.ToString(PSACommon.DAY_DATE_FORMAT));
                }
              }
              else
              {
                if (!SendReportesDiariosAlesis(_last_month, _date_init_PSAClient, ref _all_reports_month))
                {
                  Log.Error(" Call SendReportesDiariosAlesis  Date: " + _last_month.Date.ToString(PSACommon.DAY_DATE_FORMAT));
                }
              }

              _last_month = _last_month.AddMonths(1);
              _all_reports_sent = (_all_reports_sent && _all_reports_month);
            }

            if (_all_reports_sent)
            {
              _process_wait_hint = WAIT_HINT_OK;
            }

            _now = WGDB.Now; // Calculate next shipping date based in the current time after sent report
            _next_shipping_reports_date = _now.AddMilliseconds(_process_wait_hint);
          }

          //
          // 3 - Send monitorizacion diaria
          //
          if (_psa_type == PSA_TYPE.WINPSA)
          {
            _allow_send_monitorizacion = GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, "AllowSendMonitorizacion", false);

            if (_allow_send_monitorizacion && _next_shipping_monitorizacion_date <= _now)
            {
              _process_wait_hint = WAIT_HINT_ERROR;
              _monitorizacion_sent = false;

              if (!SendMonitorizacion(ref _monitorizacion_sent))
              {
                Log.Error(" Call SendMonitorizacion  ");
              }

              if (_monitorizacion_sent)
              {
                _process_wait_hint = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, "MonitorizacionWaitMinutesForShipping", 15);
                _process_wait_hint = _process_wait_hint * 60 * 1000;
              }

              _now = WGDB.Now; // Calculate next shipping date based in the current time after sent DailyReport
              _next_shipping_monitorizacion_date = _now.AddMilliseconds(_process_wait_hint);
            }

            //test
#if DEBUG
            {
              Log.Message("");
              Log.Message("   Before SLEEP. ");
              Log.Message("   _now = " + _now.ToString());
              Log.Message("   _next_shipping_monitorizacion_date = " + _next_shipping_monitorizacion_date.ToString());
              Log.Message("   _next_shipping_reports_date = " + _next_shipping_reports_date.ToString());
              Log.Message("");
            }
#endif
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while (true)

    } // PeriodicJobThread

    //------------------------------------------------------------------------------
    // PURPOSE: Send Reportes for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT:
    //          - Month
    //
    //      - OUTPUT:
    //          - SentAllReportes
    //        
    // RETURNS:
    //
    //   NOTES:
    //   Send Report , If thre's any error sleep WAIT_FOR_NEXT_TRY
    private static Boolean SendReportesDiarios(DateTime Month, ref Boolean SentAllReportes)
    {
      PSA_Respuesta _respuesta;
      String _codigo_respuesta;
      String _codigo_respuesta_desc;
      String _id_sesion;
      Int32 _expiracion_conexion;
      Int32 _max_num_reintentos_reenvio;
      Int32 _max_num_regs_pag_reporte;
      String _checksum;
      DiaPendiente[] _dias_pendientes;
      List<DateTime> _lst_dias_pendientes;
      WSI.PSA_Client.WS_PSA.BindingHTTPS _ws_psa;
      String _login;
      String _password;
      String _operador;
      String _clave_establecimiento;
      String _clave_linea_negocio;
      String _server_address1;
      String _server_address2;
      String _version;

      try
      {
        SentAllReportes = false;

        // 0 - Read GP
        if (!PSACommon.GetAndCheckPSAServiceConnectionData(out _login, out _password, out _server_address1, out  _server_address2,
                                                      out _operador, out _clave_establecimiento, out _clave_linea_negocio, out _version))
        {
          Log.Error(" Call PSACommon.GetPSAServiceConnectionData");

          return false;
        }

        // 1 - Begin Connection
        if (!PSAClient.SetConnectionToWSPSA(_operador, _login, _password, _server_address1, _server_address2, out _codigo_respuesta,
                                  out _codigo_respuesta_desc, out _id_sesion, out _expiracion_conexion, out _max_num_reintentos_reenvio,
                                  out _max_num_regs_pag_reporte, out _ws_psa))
        {
          Log.Error(" Can't connect. " + PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

          //_str_alarm = Resource.String("STR_PSA_SEND_REPORTS_CANT_CONNECT");
          //Alarm.Register(AlarmSourceCode.PSASendReports
          //              , 0
          //              , Environment.MachineName + @"\\" + SERVICE_NAME
          //              , AlarmCode.Unknown
          //              , _str_alarm); 

          return false;
        }

        // 2 - Get pending reports
        _respuesta = _ws_psa.PSA_ConsultaReportesPendientes(_id_sesion, Month.ToString(PSACommon.DAY_DATE_FORMAT),
                                                            out _checksum, out _dias_pendientes);
        if (_respuesta.CodigoRespuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Error(" Call PSAService.PSA_ConsultaReportesPendientes. " + PSACommon.FormatFromErrorOfPSARespuesta(_respuesta));

          return false;
        }

        // TODO : Haria falta verificar el checksum de PSA_ConsultaReportesPendientes para saber que es realmente lo que pedimos..???
        //String _cheksum_dias_pendientes;
        //PSACommon.ObjectSerializableToXmlAndCheckSum(_dias_pendientes, out _xml_object_dummy, out _cheksum_dias_pendientes);

        //if (_checksum != _cheksum_dias_pendientes)
        //{
        //Log.Error(" The returned data do not correspond to requests! Request month: " + LastMonth.ToString(PSACommon.DAY_DATE_FORMAT));

        //WaitHint = WAIT_FOR_NEXT_TRY;

        //return false;

        //}

        PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);

        _lst_dias_pendientes = BL_PSAReportesDiarios.CleanPendingDays(_dias_pendientes, _clave_establecimiento);

        if (_lst_dias_pendientes.Count == 0)
        {
          // Note! 
          // One login can have more than one Establecimiento
          // If there isn't pending reports for this establecimiento, ClaveEsteblecimiento won't sent.

          // If there isn't reports, to sleep!
          SentAllReportes = true;

          return true;
        }

        // 3 - Send reporte diario the month
        foreach (DateTime _dia_pendiente in _lst_dias_pendientes)
        {
          SendReporteDiario(_login,
                             _password,
                             _operador,
                             _clave_establecimiento,
                             _clave_linea_negocio,
                             _server_address1,
                             _server_address2,
                             _version,
                             _max_num_reintentos_reenvio,
                             _max_num_regs_pag_reporte,
                             _dia_pendiente);
        } // foreach DiaPendiente

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        // Alarm?
      }

      return false;
    } // SendReportesDiarios

    //------------------------------------------------------------------------------
    // PURPOSE: Send Reportes for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT:
    //          - Month
    //          - Date Init to send reports
    //
    //      - OUTPUT:
    //          - SentAllReportes
    //        
    // RETURNS:
    //
    //   NOTES:
    //   Send Report , If thre's any error sleep WAIT_FOR_NEXT_TRY
    private static Boolean SendReportesDiariosAlesis(DateTime Month, DateTime DateInitPSAClient, ref Boolean SentAllReportes)
    {
      Int32 _max_num_reintentos_reenvio;
      Int32 _max_num_regs_pag_reporte;
      List<DateTime> _lst_dias_pendientes;
      DateTime _first_register;
      DateTime _last_register;
      DateTime _first_day_report;
      DateTime _last_day_report;
      Int32 _num_registers;
      Double _initial_balance;
      Double _final_balance;
      Int32 _num_constancies;

      try
      {
        SentAllReportes = false;

        _max_num_reintentos_reenvio = 1;
        _max_num_regs_pag_reporte = Int32.MaxValue;

        // Read GP
        if (!PSACommon.CheckAlesisServiceConnectionData())
        {
          Log.Error(" Call PSACommon.CheckAlesisServiceConnectionData");

          return false;
        }

        _first_day_report = new DateTime(Month.Year, Month.Month, 1, 0, 0, 0);
        _last_day_report = new DateTime(Month.Year, Month.Month, 1, 23, 59, 59).AddMonths(1);
        _last_day_report = _last_day_report.AddDays(-1);

        _num_registers = 0;
        _initial_balance = 0;
        _final_balance = 0;
        _num_constancies = 0;

        // Get pending days
        if (!GetDiasPendientesAlesis(_last_day_report, _first_day_report, DateInitPSAClient, out _lst_dias_pendientes))
        {
          Log.Error(" Call GetDiasPendientes");

          return false;
        }

        // Send reporte diario the month
        foreach (DateTime _dia_pendiente in _lst_dias_pendientes)
        {
          if (!SendReporteDiarioAlesis(_max_num_reintentos_reenvio,
                                       _max_num_regs_pag_reporte,
                                       _dia_pendiente,
                                       out _first_register,
                                       out _last_register,
                                       out _num_registers,
                                       out _initial_balance,
                                       out _final_balance,
                                       out _num_constancies
                                       ))
          {
            // Status = 2 = Error
            UpdatePSAClientDailyReport(2, _first_register, _last_register, _dia_pendiente, _num_registers, _initial_balance, _final_balance, _num_constancies);
            SentAllReportes = false;

            return false;
          }
          else
          {
            // Status = 1 = OK
            UpdatePSAClientDailyReport(1, _first_register, _last_register, _dia_pendiente, _num_registers, _initial_balance, _final_balance, _num_constancies);
          }
        } // foreach DiaPendiente

        SentAllReportes = true;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SendReportesDiariosAlesis

    //------------------------------------------------------------------------------
    // PURPOSE: Send Reporte Diario for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT:
    //          - Login
    //          - Password
    //          - Operador
    //          - ClaveEstablecimiento
    //          - ClaveLineaNegocio
    //          - ServerAddress1
    //          - ServerAddress2
    //          - Version
    //          - MaxNumReintentosReenvio
    //          - MaxNumRegsPagReporte
    //          - JornadaPSA
    //      - OUTPUT: 
    //          - None
    //
    // RETURNS:
    //        
    //   NOTES:
    //
    private static Boolean SendReporteDiario(String Login,
                                             String Password,
                                             String Operador,
                                             String ClaveEstablecimiento,
                                             String ClaveLineaNegocio,
                                             String ServerAddress1,
                                             String ServerAddress2,
                                             String Version,
                                             Int32 MaxNumReintentosReenvio,
                                             Int32 MaxNumRegsPagReporte,
                                             DateTime JornadaPSA)
    {
      String _codigo_respuesta;
      String _codigo_respuesta_desc;
      String _id_sesion;
      Int32 _expiracion_conexion;
      Int32 _max_num_regs_pag_reporte;
      Int32 _num_of_records_to_send;
      Int32 _num_retry_ws_psa;
      String _checksum;
      WSI.PSA_Client.WS_PSA.BindingHTTPS _ws_psa;
      List<Registro_L007> _registros;
      String _id_transaccion;
      BalanceLinea _balance_linea;
      Int64 _id_secuencia;
      String _xml_object_dummy;
      DateTime _now;
      List<PSASendObject> _registros_paginated;
      int _wait_hint;
      PSA_Mensaje_Reporte_diario _reporte_diario;
      DateTime _date_first_register;
      DateTime _date_last_register;

      _id_sesion = "";
      _ws_psa = new WSI.PSA_Client.WS_PSA.BindingHTTPS();
      _registros_paginated = new List<PSASendObject>();

      try
      {
        // 1 - Get pending day   
        _num_of_records_to_send = 0;
        _balance_linea = new BalanceLinea();
        _registros = new List<Registro_L007>();

        for (Int32 _idx = 1; _idx <= NUMBER_OF_RETRY_FOR_GUI; _idx++)
        {
          // Changed to Old to perform a test.
          if (DB_PSAReportesDiarios.GetRegistrosAndBalanceL007(JornadaPSA,
                                                               MaxNumRegsPagReporte,
                                                               WSI.Common.PSA_TYPE.WINPSA,
                                                               out _registros_paginated,
                                                               out _balance_linea,
                                                               out _num_of_records_to_send,
                                                               out _reporte_diario,
                                                               out _date_first_register,
                                                               out _date_last_register))
          {
            break;
          }

          Log.Error(String.Format(" Call DB_PSAReportesDiarios.GetRegistrosL007. Attempt: {0} Date: {1}", _idx.ToString(), JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)));
          if (_idx == NUMBER_OF_RETRY_FOR_GUI)
          {
            Alarm.Register(AlarmSourceCode.PSASendReports
                         , 0
                         , SERVICE_NAME
                         , AlarmCode.WWPClientPSA_ErrorGettingDataDailyReport
                         , Resource.String(Alarm.ResourceId(AlarmCode.WWPClientPSA_ErrorGettingDataDailyReport), JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)));

            // If I can't get reports, no longer send report.
            return false;
          }

          Thread.Sleep(5000);
        } // for _idx < NUMBER_OF_RETRY_FOR_GUI

        _wait_hint = 0;

        // 2 - Send pending day
        for (_num_retry_ws_psa = 1; _num_retry_ws_psa <= MaxNumReintentosReenvio; _num_retry_ws_psa++)
        {
          // If session is open, must be closed!!
          PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
          _id_secuencia = 1;

          // 2.0 - Connection
          Thread.Sleep(_wait_hint);
          _wait_hint = 60000;

          if (!PSAClient.SetConnectionToWSPSA(Operador, Login, Password, ServerAddress1, ServerAddress2, out _codigo_respuesta,
                             out _codigo_respuesta_desc, out _id_sesion, out _expiracion_conexion, out MaxNumReintentosReenvio,
                             out _max_num_regs_pag_reporte, out _ws_psa))
          {
            Log.Error(" Can't connect. Attempt: " + _num_retry_ws_psa +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            _wait_hint = 120000;
            // ANG : If I can't connect, no longer trying to send EstablecimientoReport!
            continue;
          }

          // 2.1 Fase 1
          _codigo_respuesta = _ws_psa.PSA_ReporteFase1_Contexto(_id_sesion, Version, JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT), ClaveEstablecimiento,
                                                                 1, ClaveLineaNegocio, null, out _codigo_respuesta_desc, out _id_transaccion);

          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Error(" Call PSAService.PSA_ReporteFase1_Contexto. Attempt: " + _num_retry_ws_psa + " Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT) +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 2.2 - Fase 2
          if (!PSACommon.ObjectSerializableToXmlAndCheckSum(_balance_linea, null, out _xml_object_dummy, out _checksum))
          {
            Log.Error(" Not is serializable the object Balance or can't obtain checksum in PSACommon.ObjectSerializableToXmlAndCheckSum ");

            continue;
          }

          _codigo_respuesta = _ws_psa.PSA_ReporteFase2_ReferenciaLinea(_id_sesion, _id_transaccion, _id_secuencia, ClaveLineaNegocio, _num_of_records_to_send,
                                                                       _checksum, _balance_linea, out _codigo_respuesta_desc);

          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Error(" Call PSAService.PSA_ReporteFase2_ReferenciaLinea. Attempt: " + _num_retry_ws_psa + " Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT) +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 2.3 - Fase 3
          _id_secuencia++;
          _now = WGDB.Now;

          foreach (PSASendObject _registro_paginated in _registros_paginated)
          {
            _codigo_respuesta = _ws_psa.PSA_ReporteFase3_RegistrosLinea(_id_sesion, _id_transaccion, _id_secuencia, ClaveLineaNegocio, _now.ToString(PSACommon.HOUR_DATE_FORMAT)
                                                                        , _registro_paginated.Checksum, _registro_paginated.Xml, out _codigo_respuesta_desc);

            if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
            {
              Log.Error(" Call PSAService.PSA_ReporteFase3_RegistrosLinea. Attempt: " + _num_retry_ws_psa +
                        PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

              break;
            }
            else
            {
              _id_secuencia++;
            }
          } // foreach PSASendObject (PSA_ReporteFase3_RegistrosLinea)

          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            continue;
          }

          // 2.4 - Fase 4
          _codigo_respuesta = _ws_psa.PSA_ReporteFase4_TransaccionCompletada(_id_sesion, _id_transaccion, _id_secuencia, out _codigo_respuesta_desc);
          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Error(" Failed transaction. PSAService.PSA_ReporteFase4_TransaccionCompletada Attempt: " + _num_retry_ws_psa + " Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT) +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));
          }
          else
          {
            return true;
          }
        } // for _num_retry_ws_psa

        if (_num_retry_ws_psa > MaxNumReintentosReenvio)
        {
          Log.Error(" Failed send registros to the date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT));
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        // Alarm?
      }
      finally
      {
        PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
      }

      return false;
    } // SendReporteDiario

    //------------------------------------------------------------------------------
    // PURPOSE: Send Reporte Diario for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT:
    //          - MaxNumReintentosReenvio
    //          - MaxNumRegsPagReporte
    //          - JornadaPSA
    //      - OUTPUT: 
    //          - FirstRegister,
    //          - LastRegister,
    //          - NumRegisters,
    //          - InitialBalance,
    //          - FinalBalance,
    //
    // RETURNS:
    //        
    //   NOTES:
    //
    private static Boolean SendReporteDiarioAlesis(Int32 MaxNumReintentosReenvio,
                                                   Int32 MaxNumRegsPagReporte,
                                                   DateTime JornadaPSA,
                                                   out DateTime FirstRegister,
                                                   out DateTime LastRegister,
                                                   out Int32 NumRegisters,
                                                   out Double InitialBalance,
                                                   out Double FinalBalance,
                                                   out Int32 NumConstancies
                                                   )
    {
      Int32 _num_of_records_to_send;
      Int32 _num_retry_ws_alesis;
      BalanceLinea _balance_linea;
      List<PSASendObject> _registros_paginated;
      int _wait_hint;
      PSA_Mensaje_Reporte_diario _reporte_diario;
      List<Registro_L007> _registro_constancias;
      Boolean _is_ok;

      FirstRegister = JornadaPSA;
      LastRegister = JornadaPSA;
      NumRegisters = 0;
      InitialBalance = 0;
      FinalBalance = 0;
      NumConstancies = 0;
      _is_ok = true;

      try
      {
        // 1 - Get pending day   
        _reporte_diario = new PSA_Mensaje_Reporte_diario();
        _registro_constancias = new List<Registro_L007>();
        _registros_paginated = new List<PSASendObject>();
        _num_of_records_to_send = 0;
        _balance_linea = new BalanceLinea();

        for (Int32 _idx = 1; _idx <= NUMBER_OF_RETRY_FOR_GUI; _idx++)
        {
          if (DB_PSAReportesDiarios.GetRegistrosAndBalanceL007(JornadaPSA,
                                                               MaxNumRegsPagReporte,
                                                               WSI.Common.PSA_TYPE.ALESIS,
                                                               out _registros_paginated,
                                                               out _balance_linea,
                                                               out _num_of_records_to_send,
                                                               out _reporte_diario,
                                                               out FirstRegister,
                                                               out LastRegister))
          {
            break;
          }

          Log.Error(String.Format(" Call DB_PSAReportesDiariosAlesis.GetRegistrosL007. Attempt: {0} Date: {1}", _idx.ToString(), JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)));
          if (_idx == NUMBER_OF_RETRY_FOR_GUI)
          {
            Alarm.Register(AlarmSourceCode.PSASendReports
                         , 0
                         , SERVICE_NAME
                         , AlarmCode.WWPClientPSA_ErrorGettingDataDailyReport
                         , Resource.String(Alarm.ResourceId(AlarmCode.WWPClientPSA_ErrorGettingDataDailyReport), JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)));

            // If I can't get reports, no longer send report.
            return false;
          }

          Thread.Sleep(5000);
        } // for _idx < NUMBER_OF_RETRY_FOR_GUI

        if (_reporte_diario.Registros_L007.Count == 0)
        {
          Log.Message("Sent PSAService.AlesisSendRegisters Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT));

          return true;
        }

        _wait_hint = 0;

        // 2 - Send pending day
        for (_num_retry_ws_alesis = 1; _num_retry_ws_alesis <= MaxNumReintentosReenvio; _num_retry_ws_alesis++)
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 60000;

          NumRegisters = _reporte_diario.Registros_L007.Count;
          InitialBalance = Convert.ToDouble(_balance_linea.BalanceLineaNegocio.BaliniLinea);
          FinalBalance = Convert.ToDouble(_balance_linea.BalanceLineaNegocio.BalfinLinea);

          _is_ok = Alesis_ClientReportesDiarios.ReportesDiariosAlesis.AlesisDeleteRegistersL007(JornadaPSA, FirstRegister, LastRegister);
          if (!_is_ok)
          {
            Log.Error(" Failed transaction. PSAService.ReportesDiariosAlesis.AlesisDeleteRegistersL007: "
                                          + "Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)
                                          + " First reg.: " + FirstRegister.ToString(PSACommon.HOUR_DATE_FORMAT)
                                          + " Last reg.: " + LastRegister.ToString(PSACommon.HOUR_DATE_FORMAT));
            return false;
          }

          _is_ok = Alesis_ClientReportesDiarios.ConstanciasDiariasAlesis.AlesisDeleteRegisters(JornadaPSA, FirstRegister, LastRegister);
          if (!_is_ok)
          {
            Log.Error(" Failed transaction. PSAService.ConstanciasDiariasAlesis.AlesisDeleteRegisters: "
                                          + "Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)
                                          + " First reg.: " + FirstRegister.ToString(PSACommon.HOUR_DATE_FORMAT)
                                          + " Last reg.: " + LastRegister.ToString(PSACommon.HOUR_DATE_FORMAT));
            return false;
          }

          if (!Alesis_ClientReportesDiarios.ReportesDiariosAlesis.AlesisSendRegisters(JornadaPSA, FirstRegister, LastRegister, _reporte_diario.Registros_L007, out _registro_constancias))
          {
            Log.Error(" Failed transaction. PSAService.AlesisSendRegisters Attempt: " + _num_retry_ws_alesis
                                          + " Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT)
                                          + " First reg.: " + FirstRegister.ToString(PSACommon.HOUR_DATE_FORMAT)
                                          + " Last reg.: " + LastRegister.ToString(PSACommon.HOUR_DATE_FORMAT));

            //14-04-2016  JCA  Francesc Aguilar requiere que se envien las constancias aun habiendo existido un error en el envio de los registros.
            //                 Esto es debido al problema de conexi�n existente en las salas de WinPot.
            if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, "SendAlesisConstanciasOnError", false))
            {
              AlesisSendConstancias(JornadaPSA, _num_retry_ws_alesis, _registro_constancias);
            }
          }
          else
          {
            if (AlesisSendConstancias(JornadaPSA, _num_retry_ws_alesis, _registro_constancias))
            {
              NumConstancies = _registro_constancias.Count;

              return true;
            }
          }
        } // for _num_retry_ws_psa

        NumConstancies = _registro_constancias.Count;

        if (_num_retry_ws_alesis > MaxNumReintentosReenvio)
        {
          Log.Error(" Failed send registros to the date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT));
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SendReporteDiario

    private static Boolean AlesisSendConstancias(DateTime JornadaPSA, Int32 NumRetry, List<Registro_L007> RegistroConstancias)
    {
      if (!Alesis_ClientReportesDiarios.ConstanciasDiariasAlesis.AlesisSendRegisters(JornadaPSA, RegistroConstancias))
      {
        Log.Error(" Failed transaction. PSAService.AlesisSendConstancias Attempt: " + NumRetry + " Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT));
      }
      else
      {
        Log.Message("Sent PSAService.AlesisSendRegisters Date: " + JornadaPSA.ToString(PSACommon.DAY_DATE_FORMAT));

        return true;
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get pending days
    //
    //  PARAMS:
    //      - INPUT: 
    //          - LastReportdate
    //          - FirstReportDate
    //          - DateInitPSAClient 
    //
    //      - OUTPUT:
    //          - LstDiasPendientes
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    private static Boolean GetDiasPendientesAlesis(DateTime LastReportdate, DateTime FirstReportDate, DateTime DateInitPSAClient, out List<DateTime> LstDiasPendientes)
    {
      StringBuilder _sb;
      Int32 _last_day;
      Int32 _closing_time;
      DateTime _tmp_date;
      DateTime _jornada_psa;
      DateTime _current_date;
      Int32 _before_send_wait_hours;

      _sb = new StringBuilder();
      LstDiasPendientes = new List<DateTime>();

      _current_date = WGDB.Now;
      _before_send_wait_hours = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, "BeforeSendWaitHours", 4, 1, 96); // up to four days

      try
      {
        // Se a�aden todos los d�as del mes
        if (FirstReportDate.Month == WGDB.Now.Month && FirstReportDate.Year == WGDB.Now.Year)
        {
          //Se debe enviar los registros hasta el d�a anterior, el d�a en curso se env�a el siguiente d�a.
          //Es por temas de jornada
          _last_day = WGDB.Now.Day - 1;
        }
        else
        {
          _last_day = DateTime.DaysInMonth(FirstReportDate.Year, FirstReportDate.Month);
        }

        // Recuperamos la hora de cierre
        _closing_time = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, "ClosingTime", 0);

        for (int i = 1; i <= _last_day; i++)
        {
          _jornada_psa = new DateTime(FirstReportDate.Year, FirstReportDate.Month, i, _closing_time, 0, 0);

          if (DateInitPSAClient <= _jornada_psa.AddDays(1).AddHours(_before_send_wait_hours) &&
            _current_date > _jornada_psa.AddDays(1).AddHours(_before_send_wait_hours))
          {
            LstDiasPendientes.Add(_jornada_psa);
            //LstDiasPendientes.Add(new DateTime(FirstReportDate.Year, FirstReportDate.Month, i, _closing_time, 0, 0));
          }
        }

        _sb.AppendLine("  SELECT   PCDR_DATE_REPORT ");
        _sb.AppendLine("         , PCDR_STATUS ");
        _sb.AppendLine("    FROM   PSA_CLIENT_DAILY_REPORT ");
        _sb.AppendLine("   WHERE   PCDR_DATE_REPORT <= @pLastReportDate ");
        _sb.AppendLine("     AND   PCDR_DATE_REPORT >= @pFirstReportDate ");
        _sb.AppendLine("ORDER BY   PCDR_DATE_REPORT ASC ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pLastReportDate", SqlDbType.DateTime).Value = LastReportdate;
            _sql_cmd.Parameters.Add("@pFirstReportDate", SqlDbType.DateTime).Value = FirstReportDate;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                //Si el d�a leido de bd su status = 1 (OK), se quita de la lista inicial
                if (_reader.GetInt32(1) == 1)
                {
                  _tmp_date = _reader.GetDateTime(0);
                  _tmp_date = new DateTime(_tmp_date.Year, _tmp_date.Month, _tmp_date.Day, _closing_time, 0, 0);
                  LstDiasPendientes.Remove(_tmp_date);
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetDiasPendientes

    //------------------------------------------------------------------------------
    // PURPOSE: Update table PSA_CLIENT_DAILY_REPORT
    //
    //  PARAMS:
    //      - INPUT:
    //          - Status
    //          - FirstRegister
    //          - LastRegister
    //          - ReportDate
    //          - NumRegisters   
    //          - InitialBalance
    //          - FinalBalance
    //          - NumConstancies
    //
    //      - OUTPUT: 
    //          - None
    //
    // RETURNS:
    //        
    //   NOTES:
    //
    private static Boolean UpdatePSAClientDailyReport(Int16 Status, DateTime FirstRegister, DateTime LastRegister, DateTime ReportDate,
                                                       Int32 NumRegisters, Double InitialBalance, Double FinalBalance, Int32 NumConstancies)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("IF NOT EXISTS ( SELECT   1                                           ");
      _sb.AppendLine("                  FROM   PSA_CLIENT_DAILY_REPORT                     ");
      _sb.AppendLine("                 WHERE   PCDR_DATE_REPORT = @pRegisterDate           ");
      _sb.AppendLine("              )                                                      ");
      _sb.AppendLine("BEGIN                                                                ");
      _sb.AppendLine("    INSERT INTO PSA_CLIENT_DAILY_REPORT (PCDR_DATE_REPORT            ");
      _sb.AppendLine("                                       , PCDR_STATUS                 ");
      _sb.AppendLine("                                       , PCDR_NUM_SEND               ");
      _sb.AppendLine("                                       , PCDR_NUM_REGISTERS          ");
      _sb.AppendLine("                                       , PCDR_INITIAL_BALANCE        ");
      _sb.AppendLine("                                       , PCDR_FINAL_BALANCE          ");
      _sb.AppendLine("                                       , PCDR_DATE_SEND              ");
      _sb.AppendLine("                                       , PCDR_DATE_FIRST_REGISTER    ");
      _sb.AppendLine("                                       , PCDR_DATE_LAST_REGISTER     ");
      _sb.AppendLine("                                       , PCDR_NUM_CONSTANCIES        ");
      _sb.AppendLine("                                        )                            ");
      _sb.AppendLine("                                 VALUES (@pRegisterDate              ");
      _sb.AppendLine("                                       , @pStatus                    ");
      _sb.AppendLine("                                       , 1                           ");
      _sb.AppendLine("                                       , @pNumRegisters              ");
      _sb.AppendLine("                                       , @pInitialBalance            ");
      _sb.AppendLine("                                       , @pFinalBalance              ");
      _sb.AppendLine("                                       , GETDATE()                   ");
      _sb.AppendLine("                                       , @pDateFirst                 ");
      _sb.AppendLine("                                       , @pDateLast                  ");
      _sb.AppendLine("                                       , @pNumConstancies            ");
      _sb.AppendLine("                                        )                            ");
      _sb.AppendLine("END                                                                  ");
      _sb.AppendLine("ELSE                                                                 ");
      _sb.AppendLine("BEGIN                                                                ");
      _sb.AppendLine("    UPDATE   PSA_CLIENT_DAILY_REPORT                                 ");
      _sb.AppendLine("       SET   PCDR_STATUS              = @pStatus                     ");
      _sb.AppendLine("           , PCDR_NUM_SEND            = ISNULL(PCDR_NUM_SEND, 0) + 1 ");
      _sb.AppendLine("           , PCDR_NUM_REGISTERS       = @pNumRegisters               ");
      _sb.AppendLine("           , PCDR_INITIAL_BALANCE     = @pInitialBalance             ");
      _sb.AppendLine("           , PCDR_FINAL_BALANCE       = @pFinalBalance               ");
      _sb.AppendLine("           , PCDR_DATE_SEND           = GETDATE()                    ");
      _sb.AppendLine("           , PCDR_DATE_FIRST_REGISTER = @pDateFirst                  ");
      _sb.AppendLine("           , PCDR_DATE_LAST_REGISTER  = @pDateLast                   ");
      _sb.AppendLine("           , PCDR_NUM_CONSTANCIES     = @pNumConstancies             ");
      _sb.AppendLine("     WHERE   PCDR_DATE_REPORT         = @pRegisterDate               ");
      _sb.AppendLine("END                                                                  ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;
          _sql_cmd.Parameters.Add("@pNumRegisters", SqlDbType.Int).Value = NumRegisters;
          _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = InitialBalance;
          _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = FinalBalance;
          _sql_cmd.Parameters.Add("@pDateFirst", SqlDbType.DateTime).Value = FirstRegister;
          _sql_cmd.Parameters.Add("@pDateLast", SqlDbType.DateTime).Value = LastRegister;
          _sql_cmd.Parameters.Add("@pRegisterDate", SqlDbType.DateTime).Value = ReportDate;
          _sql_cmd.Parameters.Add("@pNumConstancies", SqlDbType.Int).Value = NumConstancies;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            _sb.Length = 0;
            _sb.AppendFormat("Can't update Registers, UpdatePSAClientDailyReport Reporte_Diario: {0}.", ReportDate.ToString());
            _sb.AppendLine();

            Log.Error(_sb.ToString());

            return false;
          }

          _db_trx.Commit();

          return true;
        }
      }
    } // UpdatePSAClientDailyReport

    //------------------------------------------------------------------------------
    // PURPOSE: Send Monitorizaci�n Diaria for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Month
    //
    //      - OUTPUT:
    //          - SentAllMonitorizacion
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    private static Boolean SendMonitorizacion(ref bool SentAllMonitorizacion)
    {
      PSA_Respuesta _respuesta;
      String _codigo_respuesta;
      String _codigo_respuesta_desc;
      String _id_sesion;
      Int32 _expiracion_conexion;
      Int32 _max_num_reintentos_reenvio;
      Int32 _max_num_regs_pag_reporte;
      String _checksum;
      WSI.PSA_Client.WS_PSA.BindingHTTPS _ws_psa;
      String _login;
      String _password;
      String _operador;
      String _clave_establecimiento;
      String _clave_linea_negocio;
      String _server_address1;
      String _server_address2;
      String _version;
      Establecimientos _establecimientos;
      Establecimiento _establecimiento;
      Boolean _sent_all_sessions;
      Boolean _sent_all_statistics;

      Int32 _num_of_records_to_send;
      List<PSASendObject> _list_objects_to_send;

      try
      {
        SentAllMonitorizacion = false;

        // 0 - Read GP
        if (!PSACommon.GetAndCheckPSAServiceConnectionData(out _login, out _password, out _server_address1, out  _server_address2,
                                                   out _operador, out _clave_establecimiento, out _clave_linea_negocio, out _version))
        {
          Log.Error(" Call PSACommon.GetPSAServiceConnectionData");

          return false;
        }

        // 1 - Begin Connection
        if (!PSAClient.SetConnectionToWSPSA(_operador, _login, _password, _server_address1, _server_address2, out _codigo_respuesta,
                                            out _codigo_respuesta_desc, out _id_sesion, out _expiracion_conexion,
                                            out _max_num_reintentos_reenvio, out _max_num_regs_pag_reporte, out _ws_psa))
        {
          Log.Error(" Can't connect. " + PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

          return false;
        }

        // 2 - Get pending reports
        _establecimiento = null;
        _respuesta = _ws_psa.PSA_ConsultaUltimosRegsMonitorizacion(_id_sesion, out _checksum, out _establecimientos);
        if (_respuesta.CodigoRespuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
        {
          Log.Error(" Call PSAService.PSA_ConsultaReportesPendientes. " +
                    PSACommon.FormatFromErrorOfPSARespuesta(_respuesta));

          return false;
        }

        PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);

        // search establecimiento
        foreach (Establecimiento _idx_establecimiento in _establecimientos.Establecimiento)
        {
          if (_idx_establecimiento.ClaveEstablecimiento == _clave_establecimiento)
          {
            _establecimiento = _idx_establecimiento;
            break;
          }
        }

        if (_establecimiento == null)
        {
          Log.Error(" Function SendMonitorizaci�n. Not found ClaveEstablecimiento");

          return false;
        }

        // 3 - SESSIONS      
        _list_objects_to_send = new List<PSASendObject>();
        _sent_all_sessions = true;
        _num_of_records_to_send = 0;
        for (Int32 _idx = 1; _idx <= NUMBER_OF_RETRY_FOR_GUI; _idx++)
        {
          // Changed to Old to perform a test.
          if (DB_PSAMonitorizacionDiaria.GetPlaySessions(_establecimiento.MaximaSesionJuego,
                                                         _establecimiento.MaximoTimestampSesionJuego,
                                                         _max_num_regs_pag_reporte,
                                                         out _list_objects_to_send,
                                                         out _num_of_records_to_send))
          {
            break;
          }

          Log.Error(String.Format(" Call DB_PSAMonitorizacionDiaria.GetPlaySessions. Attempt: {0}", _idx.ToString()));
          if (_idx == NUMBER_OF_RETRY_FOR_GUI)
          {
            Alarm.Register(AlarmSourceCode.PSASendReports
               , 0
               , SERVICE_NAME
               , AlarmCode.WWPClientPSA_ErrorGettingDataPlaySessions
               , Resource.String(Alarm.ResourceId(AlarmCode.WWPClientPSA_ErrorGettingDataPlaySessions), _establecimiento.ClaveEstablecimiento));

            // If I can't get reports, no longer send report.
            return false;
          }

          Thread.Sleep(5000);
        } // for _idx < NUMBER_OF_RETRY_FOR_GUI

        if (_list_objects_to_send.Count == 0)
        {
          //  If there isn't reports , to sleep!
          _sent_all_sessions = true;
        }
        else
        {
          if (!SendSesionDiaria(_list_objects_to_send,
                                _login,
                                _password,
                                _operador,
                                _clave_establecimiento,
                                _clave_linea_negocio,
                                _server_address1,
                                _server_address2,
                                _version,
                                _max_num_reintentos_reenvio,
                                _num_of_records_to_send))
          {
            _sent_all_sessions = false;
          }

          // if you send TOTAL_RECORDS_TO_SEND records, sent_all_session is false and will sleep WAIT_HINT_ERROR (5min)
          _sent_all_sessions = (_sent_all_sessions && (_num_of_records_to_send != PSACommon.TOTAL_RECORDS_TO_SEND));
        }

        // 4 - ESTATISTICS 
        _list_objects_to_send = new List<PSASendObject>();
        _sent_all_statistics = true;
        _num_of_records_to_send = 0;

        for (Int32 _idx = 1; _idx <= NUMBER_OF_RETRY_FOR_GUI; _idx++)
        {
          // Get a list of pages to send to PSA
          if (DB_PSAMonitorizacionDiaria.GetStatistic(_establecimiento.MaximaEstadisticaJuego,
                                                       _establecimiento.MaximoTimestampEstadisticaJuego,
                                                       _max_num_regs_pag_reporte,
                                                       out _list_objects_to_send,
                                                       out _num_of_records_to_send))
          {
            break;
          }

          Log.Error(String.Format(" Call DB_PSAMonitorizacionDiaria.GetStatistic. Attempt: {0}", _idx.ToString()));
          if (_idx == NUMBER_OF_RETRY_FOR_GUI)
          {
            Alarm.Register(AlarmSourceCode.PSASendReports
            , 0
            , SERVICE_NAME
            , AlarmCode.WWPClientPSA_ErrorGettingDataStatistics
            , Resource.String(Alarm.ResourceId(AlarmCode.WWPClientPSA_ErrorGettingDataStatistics), _establecimiento.ClaveEstablecimiento));

            // If I can't get statistics, no longer send report.
            return false;
          }

          Thread.Sleep(5000);
        } // for _idx < NUMBER_OF_RETRY_FOR_GUI

        if (_list_objects_to_send.Count == 0)
        {
          // If there isn't reports, to sleep!
          _sent_all_statistics = true;
        }
        else
        {
          if (!SendEstadisticaDiaria(_list_objects_to_send,
                                      _establecimiento.MaximoTimestampEstadisticaJuego,
                                      _login,
                                      _password,
                                      _operador,
                                      _clave_establecimiento,
                                      _clave_linea_negocio,
                                      _server_address1,
                                      _server_address2,
                                      _version,
                                      _max_num_reintentos_reenvio,
                                      _num_of_records_to_send))
          {
            _sent_all_statistics = false;
          }

          // If _nom_of_records_to_send is not equals TOTAL_RECORDS_TO_SEND still records to send
          // sleeps WAIT_HINT_ERROR and sent-it.
          _sent_all_statistics = _sent_all_statistics && (_num_of_records_to_send != PSACommon.TOTAL_RECORDS_TO_SEND);
        }

        SentAllMonitorizacion = (_sent_all_sessions && _sent_all_statistics);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        // Alarm?
      }

      return false;
    } // SendMonitorizacion

    //------------------------------------------------------------------------------
    // PURPOSE: Send Session for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT:
    //          - SessionsToSend
    //          - Login
    //          - Password
    //          - Operador
    //          - ClaveEstablecimiento
    //          - ClaveLineaNegocio
    //          - ServerAddress1
    //          - ServerAddress2
    //          - Version
    //          - MaxNumReintentosReenvio
    //
    //      - OUTPUT:
    //  
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //  
    private static Boolean SendSesionDiaria(List<PSASendObject> SessionsToSend,
                                            String Login,
                                            String Password,
                                            String Operador,
                                            String ClaveEstablecimiento,
                                            String ClaveLineaNegocio,
                                            String ServerAddress1,
                                            String ServerAddress2,
                                            String Version,
                                            Int32 MaxNumReintentosReenvio,
                                            Int32 CountRegistrosSessiones)
    {
      String _codigo_respuesta;
      String _codigo_respuesta_desc;
      String _id_sesion;
      Int32 _expiracion_conexion;
      Int32 _max_num_regs_pag_reporte;
      Int32 _num_retry_ws_psa;
      WSI.PSA_Client.WS_PSA.BindingHTTPS _ws_psa;
      String _id_transaccion;
      Int64 _id_secuencia;
      DateTime _now;
      List<RegistroMonitorizacionSesiones> _list_sessions;
      DateTime _jornada_dummy;

      _id_sesion = "";
      _ws_psa = new WSI.PSA_Client.WS_PSA.BindingHTTPS();

      try
      {
        _jornada_dummy = WGDB.Now;

        // 1 - Sending
        for (_num_retry_ws_psa = 1; _num_retry_ws_psa <= MaxNumReintentosReenvio; _num_retry_ws_psa++)
        {
          // If session is open, must be closed!!
          PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
          _id_secuencia = 1;

          //1.0 Connection (Start session)
          if (!PSAClient.SetConnectionToWSPSA(Operador, Login, Password, ServerAddress1, ServerAddress2, out _codigo_respuesta,
                            out _codigo_respuesta_desc, out _id_sesion, out _expiracion_conexion, out MaxNumReintentosReenvio,
                            out _max_num_regs_pag_reporte, out _ws_psa))
          {
            Log.Error(" Can't connect. Attempt: " + _num_retry_ws_psa +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 1.1 - Fase 1
          _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase1_Contexto(_id_sesion, Version, _jornada_dummy.ToString(PSACommon.DAY_DATE_FORMAT), ClaveEstablecimiento,
                                                                       "1", "0", CountRegistrosSessiones, 0, out _codigo_respuesta_desc, out _id_transaccion);
          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Error(" Call PSAService.PSA_MonitorizacionFase1_Contexto. Attempt: " + _num_retry_ws_psa +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 1.2 - Fase 2A                   
          _now = WGDB.Now;
          foreach (PSASendObject _paginated_sessions in SessionsToSend)
          {
            _list_sessions = (List<RegistroMonitorizacionSesiones>)_paginated_sessions.ListObject;
            _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase2A_RegistrosSesionesJuego(_id_sesion, _id_transaccion, _id_secuencia, _now.ToString(PSACommon.HOUR_DATE_FORMAT),
                                                                                        _paginated_sessions.Checksum, _list_sessions.ToArray(), out _codigo_respuesta_desc);

            if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
            {
              Log.Error(" Call PSAService.PSA_MonitorizacionFase2A_RegistrosSesionesJuego. Attempt: " + _num_retry_ws_psa
                          + PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

              break;
            }

            _id_secuencia++;
          } // foreach (PSASendObject _paginated_sessions in _paginated_sessions)

          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            continue;
          }

          // 2.3 - Fase 3 
          _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase3_TransaccionCompletada(_id_sesion, _id_transaccion, _id_secuencia, out _codigo_respuesta_desc);
          if (_codigo_respuesta == PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            return true;
          }

          // It's an error (next retry?)
          Log.Error(" Failed transaction.PSAService.PSA_MonitorizacionFase3_TransaccionCompletada Attempt: " + _num_retry_ws_psa +
                    PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));
        } // for _num_retry_ws_psa 

        if (_num_retry_ws_psa > MaxNumReintentosReenvio)
        {
          Log.Error(" Failed send sessions ");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
      }

      return false;

    } // SendSesionDiaria

    //------------------------------------------------------------------------------
    // PURPOSE: Send Monitorizaci�n Diaria for one operador-establecimiento in one date
    //
    //  PARAMS:
    //      - INPUT: 
    //          - StatisticsToSend
    //          - LastTimestamp
    //          - Login
    //          - Password
    //          - Operador
    //          - ClaveEstablecimiento
    //          - ClaveLineaNegocio
    //          - ServerAddress1
    //          - ServerAddress2
    //          - Version
    //          - MaxNumReintentosReenvio
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    private static Boolean SendEstadisticaDiaria(List<PSASendObject> StatisticsPagesToSend,
                                                 Int64 LastTimestamp,
                                                 String Login,
                                                 String Password,
                                                 String Operador,
                                                 String ClaveEstablecimiento,
                                                 String ClaveLineaNegocio,
                                                 String ServerAddress1,
                                                 String ServerAddress2,
                                                 String Version,
                                                 Int32 MaxNumReintentosReenvio,
                                                 Int32 CountRegistrosEstadisticas)
    {
      String _codigo_respuesta;
      String _codigo_respuesta_desc;
      String _id_sesion;
      Int32 _expiracion_conexion;
      Int32 _max_num_regs_pag_reporte;
      Int32 _num_retry_ws_psa;
      WSI.PSA_Client.WS_PSA.BindingHTTPS _ws_psa;
      String _id_transaccion;
      Int64 _id_secuencia;
      DateTime _now;
      List<RegistroMonitorizacionEstadisticas> _list_statistics;
      DateTime _jornada_dummy;

      _id_sesion = "";
      _ws_psa = new WSI.PSA_Client.WS_PSA.BindingHTTPS();

      try
      {
        _jornada_dummy = WGDB.Now;

        // 1 - Sending
        for (_num_retry_ws_psa = 1; _num_retry_ws_psa <= MaxNumReintentosReenvio; _num_retry_ws_psa++)
        {
          // If session is open, must be closed!!
          PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
          _id_secuencia = 1;

          //1.0 Connection (Start session)
          if (!PSAClient.SetConnectionToWSPSA(Operador, Login, Password, ServerAddress1, ServerAddress2, out _codigo_respuesta,
                            out _codigo_respuesta_desc, out _id_sesion, out _expiracion_conexion, out MaxNumReintentosReenvio,
                            out _max_num_regs_pag_reporte, out _ws_psa))
          {
            Log.Error(" Can't connect. Attempt: " + _num_retry_ws_psa +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 1.1 - Fase 1
          _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase1_Contexto(_id_sesion, Version, _jornada_dummy.ToString(PSACommon.DAY_DATE_FORMAT), ClaveEstablecimiento,
                                                                       "0", "1", 0, CountRegistrosEstadisticas, out _codigo_respuesta_desc, out _id_transaccion);
          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            Log.Error(" Call PSAService.PSA_MonitorizacionFase1_Contexto. Attempt: " + _num_retry_ws_psa +
                      PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

            continue;
          }

          // 1.2 - Fase 2b                   
          _now = WGDB.Now;
          foreach (PSASendObject _stadistics_page in StatisticsPagesToSend)
          {
            _list_statistics = (List<RegistroMonitorizacionEstadisticas>)_stadistics_page.ListObject;
            _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase2B_RegistrosEstadisticasJuego(_id_sesion, _id_transaccion, _id_secuencia, _now.ToString(PSACommon.HOUR_DATE_FORMAT),
                                                                                        _stadistics_page.Checksum, _list_statistics.ToArray(), out _codigo_respuesta_desc);

            if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
            {
              Log.Error(" Call PSAService.PSA_MonitorizacionFase2B_RegistrosEstadisticasJuego. Attempt: " + _num_retry_ws_psa +
                       PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));

              break;
            }

            _id_secuencia++;
          } // foreach (PSASendObject _paginated_statistics in StatisticsToSend.ListObjectsToSend)

          if (_codigo_respuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            continue;
          }

          // 2.3 - Fase 3 
          _codigo_respuesta = _ws_psa.PSA_MonitorizacionFase3_TransaccionCompletada(_id_sesion, _id_transaccion, _id_secuencia, out _codigo_respuesta_desc);
          if (_codigo_respuesta == PSACommon.ResponseCode.PSA_RC_OK.ToString())
          {
            return true;
          }

          // It's an error (next retry?)
          Log.Error(" Failed transaction.PSAService.PSA_MonitorizacionFase3_TransaccionCompletada Attempt: " + _num_retry_ws_psa + " Date: " +
                    PSACommon.FormatFromErrorOfPSARespuesta(_codigo_respuesta, _codigo_respuesta_desc));
        } // for _num_retry_ws_psa 

        if (_num_retry_ws_psa > MaxNumReintentosReenvio)
        {
          Log.Error(" Failed send statistics. ");
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        PSAClient.CloseSessionPSA(_id_sesion, _ws_psa);
      }

      return false;
    } // SendEstadisticaDiaria

  } // WWP_PSAClientReportesDiarios
}
