//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DB_PSA.cs
// 
//   DESCRIPTION: Class to manage reportes diarios
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 13-Nov-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-Nov-2012 DDM   Initial version.
// 15-NOV-2012 ANG   Expand initial version 
// 15-NOV-2012 ANG   Write FromSerializedRegisterToPSAMessage
// 15-NOV-2012 ANG   Write SerializeListPagingAndChecksum
// 23-NOV-2012 ANG   Improve Performance for SerializeListPagingAndChecksum.
// 04-FEV-2013 ANG   Fix bug #560 "Not found ClaveEstablecimiento" from log.
// 13-Mar-2013 ANG   Add suport to report by Working Day
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using WSI.Common;
using WSI.PSA_Client.WS_PSA;

namespace WSI.PSA_Client
{
  [Serializable]
  [System.Xml.Serialization.XmlRoot(ElementName = "PSA_Mensaje_Reporte")]
  public class PSA_Mensaje_Reporte_diario
  {
    [System.Xml.Serialization.XmlElementAttribute(ElementName = "Registro_L007")]
    public List<Registro_L007> Registros_L007;
  }

  [Serializable]
  public class Registro_L007
  {
    public Int64 NoRegistro;
    public String ClavesublineaNegocio;
    public String ClaveCombinacion;
    public String ClaveTipoPago;
    public String IdEvento;
    public String NoComprobante;
    public Decimal SaldoInicial;
    public Decimal SaldoPromocion;
    public String NoRegistroCaja;
    public String FormaPago;
    public DateTime FechaHora;
    public Decimal MontoPremioNoReclamado;
    public Registro_Linea_Constancia Constancia;
    public String NumTransaccion;
    public Decimal MontoApostado;
    public Decimal MontoRecaudado;
    public Decimal MontoBolsa;
    public Decimal MontoReserva;
    public Decimal MontoPremioLinea;
    public Decimal MontoPremioSorteo;
    public Decimal MontoPremioEspecial;

    [System.Xml.Serialization.XmlIgnore()]
    public Decimal ISR;
  }

  [Serializable]
  public class Registro_Linea_Constancia
  {
    public Registro_Linea_Constancia_General General;
    public Registro_Linea_Constancia_Jugador Jugador;
  }

  [Serializable]
  public class Registro_Linea_Constancia_General
  {
    public Decimal MontoPremio;
    public Decimal ISRretenido;
    public String NoMaquina;
  }

  [Serializable]
  public class Registro_Linea_Constancia_Jugador
  {
    public Registro_Linea_Constancia_Jugador_General General;
    public Registro_Linea_Constancia_Jugador_Domicilio Domicilio;
  }

  [Serializable]
  public class Registro_Linea_Constancia_Jugador_General
  {
    public String rfc;
    public String NombreJugador;
    public String CURP;
    public String DocumentoID;
    public String NoDocumentoID;
  }

  [Serializable]
  public class Registro_Linea_Constancia_Jugador_Domicilio
  {
    public String calle;
    public String municipio;
    public String estado;
    public String pais;
    public String codigoPostal;
    public String noExterior;
    public String noInterior;
    public String colonia;
    public String localidad;
    public String referencia;
  }

  public class BL_PSAReportesDiarios
  {
    //----------------------------------------------------------------------------------
    // PURPOSE: Remove reports with tipo_carencia = reporte_informado_con_0_registros
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DiasPendientes[] array
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - DiasPendientes without reportes_informado_con_0_registros.
    //
    //   NOTES :
    //           Days with all reportes in "Reporte_informado_Con_0_Registros will be removed"
    public static List<DateTime> CleanPendingDays(DiaPendiente[] DiasPendientes, String ClaveEstablecimiento)
    {
      List<DateTime> _dias;
      DateTime _jornada_psa;
      EstablecimientoPendiente _establecimiento;
      ReportePendiente _reporte;
      DateTime _current_date;
      Int32 _before_send_wait_hours;
      Int32 _closing_time;

      _dias = new List<DateTime>(DiasPendientes.Length);
      _current_date = WGDB.Now;
      _before_send_wait_hours = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_BEFORE_SEND_WAIT_HOURS, 4, 1, 96); // up to four days

      _closing_time = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_CLOSING_TIME, 0);

      foreach (DiaPendiente _dia in DiasPendientes)
      {
        _reporte = null;
        _establecimiento = null;

        if (_dia == null)
        {
          continue;
        }

        try
        {
          _jornada_psa = DateTime.ParseExact(_dia.Fecha, PSACommon.DAY_DATE_FORMAT, null);
        }
        catch (Exception _ex)
        {
          Log.Error(" The date pending (DiaPendiente) has a bad format");
          Log.Exception(_ex);

          continue;
        }

        // default is 00:00. If you change the hour of day of the psa, should add the hours!!

        _jornada_psa = new DateTime(_jornada_psa.Year, _jornada_psa.Month, _jornada_psa.Day, _closing_time, 0, 0);

        // TODO: Check Date! with BeforeSendWhaitHours
        if (_current_date < _jornada_psa.AddDays(1).AddHours(_before_send_wait_hours))
        {
          // Current day is always pending in PSA establecimientos.
          // Check that ClaveEstablecimiento exists in server to make sure
          // that client ClaveEstablecimiento value is right.

          if (!LookForEstablecimiento(ClaveEstablecimiento, _dia.EstablecimientosPendientes, out _establecimiento))
          {
            Log.Error(" Function CleanPendingDays. Not found ClaveEstablecimiento");
          }

          continue;
        }

        if (!LookForEstablecimiento(ClaveEstablecimiento, _dia.EstablecimientosPendientes, out _establecimiento))
        {
          continue;
        }

        // check TIPO_CARENCIA
        foreach (ReportePendiente _reporte_pendiente in _establecimiento.ReportesPendientes)
        {
          if (_reporte_pendiente.TipoCarencia != null && _reporte_pendiente.TipoCarencia != PSACommon.TIPO_CARENCIA.REPORTE_INFORMADO_CON_0_REGISTROS)
          {
            _reporte = _reporte_pendiente;

            break;
          }
        }

        if (_reporte == null)
        {
          continue;
        }

        _dias.Add(_jornada_psa);

      } // End Foreach DiaPendiente

      _dias.Sort();

      return _dias;
    }

    //----------------------------------------------------------------------------------
    // PURPOSE: Look for establecimiento in establecimientos list.
    //
    //  PARAMS:
    //      - INPUT: 
    //          - ClaveEstablecimiento to search
    //          - List of EstablecimientoPendiente 
    //
    //      - OUTPUT:
    //          - Founded establecimiento
    //
    // RETURNS:
    //          - True if can found establecimiento
    //          - False if can't found establecimiento
    //
    //   NOTES :
    //
    private static Boolean LookForEstablecimiento(String ClaveEstablecimiento,
                                                  EstablecimientoPendiente[] EstablecimientosPendientes,
                                                  out EstablecimientoPendiente Establecimiento)
    {
      Boolean _is_found;
      _is_found = false;
      Establecimiento = null;

      foreach (EstablecimientoPendiente _est in EstablecimientosPendientes)
      {
        if (_est.ClaveEstablecimiento == ClaveEstablecimiento)
        {
          Establecimiento = _est;
          _is_found = true;

          break;
        }
      }

      return _is_found;
    } // LookForEstablecimiento
  } // BL_PSAReportesDiarios 
}
