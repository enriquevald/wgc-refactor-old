//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DB_PSAMonitorizacionDiaria.cs
// 
//   DESCRIPTION: Class to manage Querys that make reference to monitorización the PSA
// 
//        AUTHOR: Dani Domínguez
// 
// CREATION DATE: 11-Dec-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-DEC-2012 DDM    Initial version.
// 13-MAR-2013 ANG    Add suport to report by Working Day
// 16-NOV-2015 DLL    Change limit daily report to 120 month
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.PSA_Client.WS_PSA;

namespace WSI.PSA_Client
{
  class DB_PSAMonitorizacionDiaria
  {
    #region Constants

    // PlaySessions
    private const int SQL_COL_PS_PLAY_SESSION_ID = 0;
    private const int SQL_COL_PS_TIMESTAMP = 1;
    private const int SQL_COL_PS_STARTED = 2;
    private const int SQL_COL_PS_TERMINAL_NAME = 3;
    private const int SQL_COL_PS_PROVIDER_NAME = 4;
    private const int SQL_COL_PS_STATUS = 5;
    private const int SQL_COL_PS_INITIAL_BALANCE = 6;
    private const int SQL_COL_PS_FINAL_BALANCE = 7;
    private const int SQL_COL_PS_PLAYED_COUNT = 8;
    private const int SQL_COL_PS_WON_COUNT = 9;
    private const int SQL_COL_PS_REDEEMABLE_CASH_IN = 10;
    private const int SQL_COL_PS_REDEEMABLE_CASH_OUT = 11;
    private const int SQL_COL_PS_REDEEMABLE_PLAYED = 12;
    private const int SQL_COL_PS_REDEEMABLE_WON = 13;
    private const int SQL_COL_PS_NON_REDEEMABLE_CASH_IN = 14;
    private const int SQL_COL_PS_NON_REDEEMABLE_CASH_OUT = 15;
    private const int SQL_COL_PS_NON_REDEEMABLE_PLAYED = 16;
    private const int SQL_COL_PS_NON_REDEEMABLE_WON = 17;
    private const int SQL_COL_PS_PROMO_RE_CASH_IN = 18;
    private const int SQL_COL_PS_PROMO_RE_CASH_OUT = 19;
    private const int SQL_COL_PS_FINISHED = 20;

    //Statistic 
    private const int SQL_COL_SPH_DAY = 0;
    private const int SQL_COL_SPH_TIMESTAMP = 1;
    private const int SQL_COL_SPH_TERMINAL_NAME = 2;
    private const int SQL_COL_SPH_PROVIDER_NAME = 3;
    private const int SQL_COL_SPH_GAME_NAME = 4;
    private const int SQL_COL_SPH_PLAYED_COUNT = 5;
    private const int SQL_COL_SPH_WON_COUNT = 6;
    private const int SQL_COL_SPH_PLAYED_AMOUNT = 7;
    private const int SQL_COL_SPH_WON_AMOUNT = 8;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Read Play sessions
    //
    //  PARAMS:
    //      - INPUT: 
    //          - LastSessionId
    //          - LastTimestamp
    //          - ItemsPerPage
    //
    //      - OUTPUT:
    //          - list SessionPagesToSend
    //          - CountSessionsToSend
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    public static Boolean GetPlaySessions(Int64 LastSessionId, Int64 LastTimestamp, Int32 ItemsPerPage, out List<PSASendObject> SessionPagesToSend, out Int32 CountSessionsToSend)
    {
      DateTime _date_from;
      DateTime _now;
      StringBuilder _sb;
      RegistroMonitorizacionSesiones _session;
      Int32 _no_registro;
      List<RegistroMonitorizacionSesiones> _sessions;
      PSASendObject _session_page;
      Int32 _num_of_month_to_report;

      SessionPagesToSend = new List<PSASendObject>();
      CountSessionsToSend = 0;

      try
      {
        _sb = new StringBuilder();
        _no_registro = 0;
        _sessions = new List<RegistroMonitorizacionSesiones>();
        _num_of_month_to_report = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_NUMBER_OF_PREVIOUS_MONTHS_TO_REPORT, 1, 0, 120);
        _now = WGDB.Now;
        _date_from = new DateTime(_now.Year, _now.Month, 1);
        _date_from = _date_from.AddMonths(-_num_of_month_to_report);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendFormat(" SELECT TOP {0}  PS_PLAY_SESSION_ID             ", PSACommon.TOTAL_RECORDS_TO_SEND); // 0
          _sb.AppendLine("        , CAST(ISNULL(PS_TIMESTAMP, 0) as BIGINT) "); // 1
          _sb.AppendLine("        , PS_STARTED                              "); // 2
          _sb.AppendLine("        , ISNULL(TE_NAME, TE_TERMINAL_ID)         "); // 3
          _sb.AppendLine("        , TE_PROVIDER_ID                          "); // 4
          _sb.AppendLine("        , PS_STATUS                               "); // 5
          _sb.AppendLine("                                                  ");
          _sb.AppendLine("        , PS_INITIAL_BALANCE + PS_CASH_IN         "); // 6  Bear in mind midlle recharges  PS_CASH_IN
          _sb.AppendLine("        , PS_FINAL_BALANCE                        "); // 7
          _sb.AppendLine("        , PS_PLAYED_COUNT                         "); // 8
          _sb.AppendLine("        , PS_WON_COUNT                            "); // 9

          //_sb.AppendLine("        , PS_REDEEMABLE_CASH_IN   "); PS_REDEEMABLE_CASH_IN = PS_RE_CASH_IN + PS_PROMO_RE_CASH_IN
          //_sb.AppendLine("        , PS_REDEEMABLE_CASH_OUT  "); PS_REDEEMABLE_CASH_OUT = PS_RE_CASH_OUT + PS_PROMO_RE_CASH_OUT

          _sb.AppendLine("        , PS_RE_CASH_IN                           "); // 10
          _sb.AppendLine("        , PS_RE_CASH_OUT                          "); // 11
          _sb.AppendLine("        , PS_REDEEMABLE_PLAYED                    "); // 12
          _sb.AppendLine("        , PS_REDEEMABLE_WON                       "); // 13
          _sb.AppendLine("                                                  ");
          _sb.AppendLine("        , PS_NON_REDEEMABLE_CASH_IN               "); // 14
          _sb.AppendLine("        , PS_NON_REDEEMABLE_CASH_OUT              "); // 15
          _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED                "); // 16
          _sb.AppendLine("        , PS_NON_REDEEMABLE_WON                   "); // 17
          _sb.AppendLine("                                                  ");
          _sb.AppendLine("        , PS_PROMO_RE_CASH_IN                     "); // 18
          _sb.AppendLine("        , PS_PROMO_RE_CASH_OUT                    "); // 19
          _sb.AppendLine("        , PS_FINISHED                             "); // 20

          if (LastTimestamp > 0)
          {
            _sb.AppendLine("   FROM   PLAY_SESSIONS WITH( INDEX(IX_PS_TIMESTAMP))        ");
            _sb.AppendLine("  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID  ");
            _sb.AppendLine("  WHERE   CAST(ISNULL(PS_TIMESTAMP, 0) as BIGINT) > @pLastTimeStampSessions             ");
            _sb.AppendLine("    AND   PS_STARTED  >= @pIniDate                           ");
          }
          else if (LastSessionId > 0)
          {
            _sb.AppendLine("   FROM   PLAY_SESSIONS WITH( INDEX(PK_play_sessions))       ");
            _sb.AppendLine("  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID  ");
            _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID > @pLastSessionId               ");
            _sb.AppendLine("    AND   PS_STARTED  >= @pIniDate ");
          }
          else
          {
            _sb.AppendLine("   FROM   PLAY_SESSIONS                                      ");
            _sb.AppendLine("  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID  ");
            _sb.AppendLine("  WHERE   PS_STARTED  >= @pIniDate                           ");
          }

          _sb.AppendFormat("    AND   TE_TERMINAL_TYPE IN ({0})                          ", Misc.GamingTerminalTypeListToString());
          _sb.AppendLine("    ORDER BY PS_TIMESTAMP ASC                                  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandTimeout = 300; // 5min
            _sql_cmd.Parameters.Add("@pLastTimeStampSessions", SqlDbType.BigInt).Value = LastTimestamp;
            _sql_cmd.Parameters.Add("@pLastSessionId", SqlDbType.BigInt).Value = LastSessionId;
            _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = _date_from;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if ((_no_registro % ItemsPerPage) == 0 && _no_registro != 0)
                {
                  _session_page = new PSASendObject(_sessions);
                  SessionPagesToSend.Add(_session_page);
                  _sessions = new List<RegistroMonitorizacionSesiones>();
                }

                _no_registro++;

                _session = new RegistroMonitorizacionSesiones();
                _session.ContadorJugadasGanadorasSpecified = true;
                _session.ContadorJugadasSpecified = true;
                _session.EstatusSpecified = true;
                _session.IdSesionSpecified = true;
                _session.NoRegistroSpecified = true;
                _session.TimestampSpecified = true;

                _session.NoRegistro = _no_registro;
                _session.IdSesion = _reader.GetInt64(SQL_COL_PS_PLAY_SESSION_ID);
                _session.FechaHoraInicio = _reader.GetDateTime(SQL_COL_PS_STARTED).ToString(PSACommon.HOUR_DATE_FORMAT);

                if (!_reader.IsDBNull(SQL_COL_PS_FINISHED))
                {
                  _session.FechaHoraCierre = _reader.GetDateTime(SQL_COL_PS_FINISHED).ToString(PSACommon.HOUR_DATE_FORMAT);
                }

                if (!_reader.IsDBNull(SQL_COL_PS_PROVIDER_NAME))
                {
                  _session.ProveedorTerminal = _reader.GetString(SQL_COL_PS_PROVIDER_NAME);
                }
                else
                {
                  _session.ProveedorTerminal = PSACommon.EMPTY_STRING;
                }

                _session.NombreTerminal = _reader.GetString(SQL_COL_PS_TERMINAL_NAME);
                _session.Timestamp = _reader.GetInt64(SQL_COL_PS_TIMESTAMP);
                _session.Estatus = _reader.GetInt32(SQL_COL_PS_STATUS);

                _session.BalanceInicial = _reader.GetDecimal(SQL_COL_PS_INITIAL_BALANCE).ToString(PSACommon.MONEDA_FORMAT);
                if (!_reader.IsDBNull(SQL_COL_PS_FINAL_BALANCE))
                {
                  _session.BalanceFinal = _reader.GetDecimal(SQL_COL_PS_FINAL_BALANCE).ToString(PSACommon.MONEDA_FORMAT);
                }

                _session.ContadorJugadas = _reader.GetInt32(SQL_COL_PS_PLAYED_COUNT);
                _session.ContadorJugadasGanadoras = _reader.GetInt32(SQL_COL_PS_WON_COUNT);

                _session.CashInRedimible = _reader.GetDecimal(SQL_COL_PS_REDEEMABLE_CASH_IN).ToString(PSACommon.MONEDA_FORMAT);
                _session.CashOutRedimible = _reader.GetDecimal(SQL_COL_PS_REDEEMABLE_CASH_OUT).ToString(PSACommon.MONEDA_FORMAT);
                _session.RedimibleJugado = _reader.GetDecimal(SQL_COL_PS_REDEEMABLE_PLAYED).ToString(PSACommon.MONEDA_FORMAT);
                _session.RedimibleGanado = _reader.GetDecimal(SQL_COL_PS_REDEEMABLE_WON).ToString(PSACommon.MONEDA_FORMAT);

                _session.CashInNoRedimible = _reader.GetDecimal(SQL_COL_PS_NON_REDEEMABLE_CASH_IN).ToString(PSACommon.MONEDA_FORMAT);
                _session.CashOutNoRedimible = _reader.GetDecimal(SQL_COL_PS_NON_REDEEMABLE_CASH_OUT).ToString(PSACommon.MONEDA_FORMAT);
                _session.NoRedimibleJugado = _reader.GetDecimal(SQL_COL_PS_NON_REDEEMABLE_PLAYED).ToString(PSACommon.MONEDA_FORMAT);
                _session.NoRedimibleGanado = _reader.GetDecimal(SQL_COL_PS_NON_REDEEMABLE_WON).ToString(PSACommon.MONEDA_FORMAT);

                _session.CashInPromocionalRedimible = _reader.GetDecimal(SQL_COL_PS_PROMO_RE_CASH_IN).ToString(PSACommon.MONEDA_FORMAT);
                _session.CashOutPromocionalRedimible = _reader.GetDecimal(SQL_COL_PS_PROMO_RE_CASH_OUT).ToString(PSACommon.MONEDA_FORMAT);

                _sessions.Add(_session);
              } // while (_reader.Read())
            }
          }
        } // using DB_TRX

        if (_sessions.Count > 0)
        {
          _session_page = new PSASendObject(_sessions);
          SessionPagesToSend.Add(_session_page);
        }

        CountSessionsToSend = _no_registro;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetPlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE: Read Statistic
    //
    //  PARAMS:
    //      - INPUT: 
    //          - LastDay
    //          - LastTimestamp
    //          - ItemsPerPage
    //
    //      - OUTPUT:
    //          - List StatisticPagesToSend
    //          - CountSentStadistics
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    public static Boolean GetStatistic(DateTime? DateFrom, Int64 LastTimestamp, Int32 ItemsPerPage, out List<PSASendObject> StatisticPagesToSend, out Int32 CountSentStadistics)
    {
      DateTime _min_date_monitorizacion;
      DateTime _2007_opening;
      DateTime _now;
      StringBuilder _sb;
      Int32 _no_registro;
      Int32 _num_of_month_to_report;
      RegistroMonitorizacionEstadisticas _statistic;
      List<RegistroMonitorizacionEstadisticas> statistics;
      PSASendObject _statistic_page;
      Int32 _closing_time;

      StatisticPagesToSend = new List<PSASendObject>();
      CountSentStadistics = 0;

      _closing_time = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_CLOSING_TIME, 0);

      try
      {
        _now = WGDB.Now;
        _num_of_month_to_report = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_NUMBER_OF_PREVIOUS_MONTHS_TO_REPORT, 1, 0, 120);
        _min_date_monitorizacion = new DateTime(_now.Year, _now.Month, 1);
        _min_date_monitorizacion = _min_date_monitorizacion.AddMonths(_num_of_month_to_report * -1);

        if (LastTimestamp > 0 || !DateFrom.HasValue)
        {
          DateFrom = _min_date_monitorizacion;
        }

        _sb = new StringBuilder();
        _no_registro = 0;
        _2007_opening = new DateTime(2007, 1, 1, _closing_time, 0, 0);

        statistics = new List<RegistroMonitorizacionEstadisticas>();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Todo Andreu
          if (DateFrom < _min_date_monitorizacion)
          {
            DateFrom = _min_date_monitorizacion;
          }

          _sb.Length = 0;

          // Get title of the most played game 
          _sb.AppendLine(" SELECT   DATEADD(DAY, DATEDIFF(HOUR, @p2007Opening, SPH_BASE_HOUR) / 24, @p2007Opening) as DATE           ");
          _sb.AppendLine("        , SPH_TERMINAL_ID         as TERMINAL_ID                                                           ");
          _sb.AppendLine("        , SPH_GAME_ID             as GAME_ID                                                               ");
          _sb.AppendLine("        , SPH_GAME_NAME           as GAME_NAME                                                             ");
          _sb.AppendLine("        , SUM(SPH_PLAYED_COUNT)   as PLAYED_COUNT                                                          ");
          _sb.AppendLine("   INTO   #SPH_COUNT_PLAYED                                                                                ");
          _sb.AppendLine("   FROM   SALES_PER_HOUR WITH(index ( PK_sales_per_hour))                                                  ");
          _sb.AppendLine("  WHERE   SPH_BASE_HOUR >=  @pIniDate                                                                      ");
          _sb.AppendLine("  GROUP   BY DATEADD(DAY, DATEDIFF(HOUR, @p2007Opening, SPH_BASE_HOUR) / 24, @p2007Opening)                ");
          _sb.AppendLine("        , SPH_TERMINAL_ID                                                                                  ");
          _sb.AppendLine("        , SPH_GAME_ID                                                                                      ");
          _sb.AppendLine("        , SPH_GAME_NAME                                                                                    ");
          _sb.AppendLine("  ORDER   BY DATE, SPH_TERMINAL_ID, SUM(SPH_PLAYED_COUNT) DESC                                             ");
          _sb.AppendLine("                                                                                                           ");

          // Get stadistics data
          _sb.AppendFormat(" SELECT   TOP {0} DATEADD(DAY, DATEDIFF(HOUR, @p2007Opening, SPH_BASE_HOUR) / 24, @p2007Opening) as SPH_DAY", PSACommon.TOTAL_RECORDS_TO_SEND); //0
          _sb.AppendLine("        , CAST(MAX(SPH_TIMESTAMP) AS BIGINT)  as SPHTimeStamp                                              "); //1
          _sb.AppendLine("        , ISNULL(TE_NAME, TE_TERMINAL_ID)     as TerminalName                                              "); //2
          _sb.AppendLine("        , TE_PROVIDER_ID                                                                                   "); //3
          _sb.AppendLine("        , (SELECT   TOP 1  GAME_NAME                                                                       "); //4
          _sb.AppendLine("             FROM   #SPH_COUNT_PLAYED                                                                      ");
          _sb.AppendLine("            WHERE   TERMINAL_ID = SPH_TERMINAL_ID                                                          ");
          _sb.AppendLine("              and   DATE = DATEADD(DAY, DATEDIFF(HOUR, @p2007Opening, SPH_BASE_HOUR) / 24, @p2007Opening)  ");
          _sb.AppendLine("        ) as GAME_NAME                                                                                     ");
          _sb.AppendLine("        , SUM(SPH_PLAYED_COUNT)  as PlayedCount                                                            "); //5
          _sb.AppendLine("        , SUM(SPH_WON_COUNT)     as WonCount                                                               "); //6
          _sb.AppendLine("        , SUM(SPH_PLAYED_AMOUNT) as PlayedAmount                                                           "); //7
          _sb.AppendLine("        , SUM(SPH_WON_AMOUNT)    as WonAmount                                                              "); //8
          _sb.AppendLine("   FROM   SALES_PER_HOUR with(index(PK_sales_per_hour))                                                    ");
          _sb.AppendLine("  INNER   JOIN TERMINALS ON te_terminal_id = SPH_TERMINAL_ID                                               ");
          _sb.AppendLine("  WHERE   SPH_BASE_HOUR >= @pIniDate                                                                       ");
          _sb.AppendFormat("  AND   TE_TERMINAL_TYPE IN ({0})                                                                        ", Misc.GamingTerminalTypeListToString());
          _sb.AppendLine("  GROUP   BY  DATEADD(DAY, DATEDIFF(HOUR, @p2007Opening, SPH_BASE_HOUR) / 24, @p2007Opening)               ");
          _sb.AppendLine("        , TE_PROVIDER_ID                                                                                   ");
          _sb.AppendLine("        , SPH_TERMINAL_ID                                                                                  ");
          _sb.AppendLine("        , ISNULL(TE_NAME, TE_TERMINAL_ID)                                                                  ");
          _sb.AppendLine(" HAVING   MAX(CAST(ISNULL(SPH_TIMESTAMP, 0) as BIGINT)) > @pLastTimeStampStatistic                         ");
          _sb.AppendLine(" ORDER    BY SPHTimeStamp                                                                                  ");
          _sb.AppendLine("                                                                                                           ");
          _sb.AppendLine(" DROP TABLE #SPH_COUNT_PLAYED                                                                              ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandTimeout = 300; // 5min

            _sql_cmd.Parameters.Add("@pLastTimeStampStatistic", SqlDbType.BigInt).Value = LastTimestamp;
            _sql_cmd.Parameters.Add("@p2007Opening", SqlDbType.DateTime).Value = _2007_opening;
            _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = DateFrom;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (_no_registro % ItemsPerPage == 0 && _no_registro > 0)
                {
                  _statistic_page = new PSASendObject(statistics);
                  StatisticPagesToSend.Add(_statistic_page);
                  statistics = new List<RegistroMonitorizacionEstadisticas>();
                }

                _no_registro++;
                _statistic = new RegistroMonitorizacionEstadisticas();
                _statistic.ContadorJugadasGanadorasSpecified = true;
                _statistic.ContadorJugadasSpecified = true;
                _statistic.NoRegistroSpecified = true;
                _statistic.TimestampSpecified = true;

                _statistic.NoRegistro = _no_registro;
                _statistic.NombreTerminal = _reader.GetString(SQL_COL_SPH_TERMINAL_NAME);

                if (!_reader.IsDBNull(SQL_COL_SPH_PROVIDER_NAME))
                {
                  _statistic.ProveedorTerminal = _reader.GetString(SQL_COL_SPH_PROVIDER_NAME);
                }
                else
                {
                  _statistic.ProveedorTerminal = PSACommon.EMPTY_STRING;
                }

                _statistic.Juego = _reader.GetString(SQL_COL_SPH_GAME_NAME);
                _statistic.ContadorJugadas = _reader.GetInt64(SQL_COL_SPH_PLAYED_COUNT);
                _statistic.ContadorJugadasGanadoras = _reader.GetInt64(SQL_COL_SPH_WON_COUNT);
                _statistic.ImporteJugado = _reader.GetDecimal(SQL_COL_SPH_PLAYED_AMOUNT).ToString(PSACommon.MONEDA_FORMAT);
                _statistic.ImporteGanado = _reader.GetDecimal(SQL_COL_SPH_WON_AMOUNT).ToString(PSACommon.MONEDA_FORMAT);

                _statistic.Fecha = _reader.GetDateTime(SQL_COL_SPH_DAY).ToString(PSACommon.DAY_DATE_FORMAT);
                _statistic.Timestamp = _reader.GetInt64(SQL_COL_SPH_TIMESTAMP);

                statistics.Add(_statistic);
              }
            }
          }
        } // using DB_TRX

        if (statistics.Count > 0)
        {
          _statistic_page = new PSASendObject(statistics);

          StatisticPagesToSend.Add(_statistic_page);
        }

        CountSentStadistics = _no_registro;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetStatistic
  }
}