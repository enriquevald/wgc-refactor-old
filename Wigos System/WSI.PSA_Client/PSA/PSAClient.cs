//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_PSAClientReportesDiarios.cs
// 
//   DESCRIPTION: Common Procedures to the PSA Service
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 13-Nov-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-Dec-2012 DDM    First release 
// 25-JUL-2013 JCA    Add Proxy Settings
// 03-OCT-2013 ANG    Remove 'GlobalProxySelection is Obsolete' warning.
// 12-DIC-2013 JCA    Change Proxy to use in with all connections, set Null if not defined
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.PSA_Client;
using WSI.Common;

namespace WSI.PSA_Client
{
  public static class PSAClient
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Close session from psa.
    //
    //  PARAMS:
    //      - INPUT:
    //          - IdSesion
    //          - PSAService
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    //   NOTES:
    //
    public static void CloseSessionPSA(String IdSesion, WSI.PSA_Client.WS_PSA.BindingHTTPS WsPSA)
    {
      String _codigo_repuesta_desc;
      try
      {
        if (!String.IsNullOrEmpty(IdSesion))
        {
          // Be polite, try to close the opened session
          // Don't care about the return code
          WsPSA.PSA_CierreSesion(IdSesion, out _codigo_repuesta_desc);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }// CloseSessionPSA

    //------------------------------------------------------------------------------
    // PURPOSE: Sets connection and start session to PSA web service.
    //
    //  PARAMS:
    //      - INPUT:
    //          - IdOperador
    //          - Login
    //          - Password
    //          - ServerAddress1
    //          - ServerAddress2
    //
    //      - OUTPUT:
    //          - CodigoRespuesta
    //          - CodigoRespuestaDesc
    //          - IdSesion
    //          - ExpiracionConnexion
    //          - MaxNumReintentosReenvio
    //          - MaxNumRegsPagReporte
    //          - WsPSA
    //
    // RETURNS:
    //
    //   NOTES:
    //          Return True If you started session. False otherwise
    public static Boolean SetConnectionToWSPSA(String IdOperador,
                                               String Login,
                                               String Password,
                                               String ServerAddress1,
                                               String ServerAddress2,
                                               out String CodigoRespuesta,
                                               out String CodigoRespuestaDesc,
                                               out String IdSesion,
                                               out Int32 ExpiracionConnexion,
                                               out Int32 MaxNumReintentosReenvio,
                                               out Int32 MaxNumRegsPagReporte,
                                               out WSI.PSA_Client.WS_PSA.BindingHTTPS WsPSA)
    {
      String _proxy_address;
      //String _proxyuser;
      //String _proxypassword;
      //String _proxydomain;

      WsPSA = new WSI.PSA_Client.WS_PSA.BindingHTTPS();
      CodigoRespuesta = "";
      CodigoRespuestaDesc = "";
      IdSesion = "";
      MaxNumReintentosReenvio = 0;
      MaxNumRegsPagReporte = 0;
      ExpiracionConnexion = 0;

      //_proxyuser = "";
      //_proxypassword = "";
      //_proxydomain = "";

      //Create Proxy to connect

      // JCA IF NECESSARY USE PROXY
      // TEST THIS WITH PROXY INSTALLED IN X.COTS PC
      _proxy_address = WSI.Common.GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_PROXY_ADDRESS);
      //_proxyuser = WSI.Common.GeneralParam.GetString("PSAClient", "ProxyUser");
      //_proxypassword = WSI.Common.GeneralParam.GetString("PSAClient", "ProxyPassword");
      //_proxydomain = WSI.Common.GeneralParam.GetString("PSAClient", "ProxyDomain");

      if (!String.IsNullOrEmpty(_proxy_address))
      {
        System.Net.WebProxy _proxy_object;
        _proxy_object = new System.Net.WebProxy(_proxy_address);

        //JCA the user running the service must have navigation permissions otherwise:
        //_proxy_object.Credentials = new System.Net.NetworkCredential(_proxyuser, _proxypassword, _proxydomain);

        _proxy_object.BypassProxyOnLocal = true;

        // ANG : Change from GlobalProxySelection.Select to WebRequest.DefaultWebProxy 
        // in order to avoid 'GlobalProxySelection is Obsolete' warning.
        // The proxy connection has been re-tested and runs ok.
        System.Net.WebRequest.DefaultWebProxy = _proxy_object;

        WsPSA.Proxy = _proxy_object;
        //Log.Warning("PROXY[" + _proxyaddress + "]");
      }
      else
      {
        WsPSA.Proxy = null;
        //Log.Warning("PROXY[NOT USED]");
      }

      try
      {
        if (ServerAddress1 != "")
        {
          WsPSA.Url = ServerAddress1;  //"http://win-server-6:8090/PSAService.svc";//"http://localhost:62471/PSAService.svc"; 
          CodigoRespuesta = WsPSA.PSA_InicioSesion(Login, IdOperador, Password, out CodigoRespuestaDesc, out IdSesion,
                                                   out ExpiracionConnexion, out MaxNumReintentosReenvio,
                                                   out MaxNumRegsPagReporte);
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" Not connected to ServerAddress1: " + ServerAddress1);
        Log.Exception(_ex);
      }

      try
      {
        if (String.IsNullOrEmpty(CodigoRespuesta) || CodigoRespuesta != PSACommon.ResponseCode.PSA_RC_OK.ToString())
        {
          if (ServerAddress2 != "")
          {
            WsPSA.Url = ServerAddress2;
            CodigoRespuesta = WsPSA.PSA_InicioSesion(Login, IdOperador, Password, out CodigoRespuestaDesc, out IdSesion,
                                                     out ExpiracionConnexion, out MaxNumReintentosReenvio,
                                                     out MaxNumRegsPagReporte);
          }
        }

        if (CodigoRespuesta == PSACommon.ResponseCode.PSA_RC_OK.ToString())
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        //TODO: generar alarma
        Log.Error(" Not connected to ServerAddress2: " + ServerAddress2);
        Log.Exception(_ex);
      }

      return false;

    } // SetConnectionToWSPSA
  }

  public class PSASendObject
  {
    private String m_checksum;
    private String m_xml_obj;
    private Object m_list_object;

    public String Xml
    {
      get { return m_xml_obj; }
      set { m_xml_obj = value; }
    }
    public String Checksum
    {
      get { return m_checksum; }
      set { m_checksum = value; }
    }
    public Object ListObject
    {
      get { return m_list_object; }
      set { m_list_object = value; }
    }

    public PSASendObject(String XmlObject, String CheckSumObject, Object Object)
    {
      this.Xml = XmlObject;
      this.Checksum = CheckSumObject;
      this.m_list_object = Object;
    }

    public PSASendObject(String XmlObject, String CheckSumObject)
    {
      this.Xml = XmlObject;
      this.Checksum = CheckSumObject;
    }

    public PSASendObject(Object Object)
    {
      String _dummy_xml;
      String _checksum;

      this.m_list_object = Object;
      PSACommon.ObjectSerializableToXmlAndCheckSum(Object, null, out _dummy_xml, out _checksum);
      this.Checksum = _checksum;
    }

    public PSASendObject()
    {
      this.Xml = String.Empty;
      this.Checksum = String.Empty;
    }

  } // PSASendObject
}
