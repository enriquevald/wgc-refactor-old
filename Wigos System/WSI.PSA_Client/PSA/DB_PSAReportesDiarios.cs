//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DB_PSA.cs
// 
//   DESCRIPTION: Class to manage Querys that make reference to PSA
// 
//        AUTHOR: Dani Dom�nguez and Artur Nebot
// 
// CREATION DATE: 12-Nov-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-Nov-2012 DDM    Initial version.
// 14-Nov-2012 ANG    Expand and implement initial version.
// 10-Dec-2012 DDM    Fixed Bug #438. Error in Balance calculation
// 01-FEB-2013 ANG    Fixed Bug #560. Final balance was wrong, now ingresos - salidas
// 01-MAR-2013 JCA    Fixed Bug #647. Number Total Registers was wrong.
// 17-MAY-2013 JCA    Fixed Bug #792. Value of field <NoRegistroCaja> was wrong.
// 03-SEP-2013 RCI    Fixed Bug WIG-183: Recharge for points: invalid cashier movements
// 05-SEP-2013 ANG    Fixed Bug WIG-192: Errors with calculation of the initial balance with different currencies.
// 25-SEP-2013 ANG    Support for 'Prize In Kind' used by Cash Desk Draws
// 26-SEP-2013 ANG    Support for differents kinds of PaymentType ( BankCard, Check and Cash )
// 02-OCT-2013 ANG    Fix bug without affectation in PSA, In AccountMovements() TotalPrizeInKind was wrong. ( Not used by PSA )
// 04-OCT-2013 ANG    Fix bug WIG-260.
// 22-OCT-2013 ANG    Fix Bug WIG-294.
// 28-OCT-2013 ANG    Fix Bug WIG-298,WIG-332.
// 29-OCT-2013 ANG    Reject undone operations and undo operation from Cashier Movements.
// 18-NOV-2013 ANG    Move AccountMovements function to XMLReports.
// 08-AUG-2014 DDM    Fix bug WIG-1172.
// 06-AUG-2015 DDM    Botched Job for AlesisWinpot.
// 21-OCT-2016 FGB    PBI 10327: PSA Client-Alesis: Obtaining data for the withhold and check the results
// 02-AUG-2017 JML    PBI 29080: WIGOS-4025 PSA Client - New mode "Daily report offset"
// 10-APR-2018 JML    Feature 32223:WIGOS-9856 Parametrizaci�n de la inclusi�n del impuesto sobre erogaciones en el c�lculo del monto diario de salidas de caja que se informa en los balances diarios de caja reportados al PSA
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.PSA_Client.WS_PSA;

namespace WSI.PSA_Client
{
  public static class DB_PSAReportesDiarios
  {
    #region Constants
    private enum L007_UNR_property
    {
      NoReportar = -1,
      MontoPremioSorteo = 0,
      MontoPremioEspecial = 1,
    }

    private enum Prize_Register
    {
      MontoPremioLinea = 0,
      MontoPremioSorteo = 1,
    }
    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get Enable Daily Report Offset
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Daily Report Offset are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnableDailyReportOffset()
    {
      return GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_ENABLE_DAILY_REPORT_OFFSET, false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get data of the registro for psa
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DateFrom    
    //          - ItemsPerPage
    //          - PSAType
    //
    //      - OUTPUT:
    //          - ListObjectsToSend 
    //          - Balance    
    //          - CountRegistrosToSend
    //          - ReporteDiario
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    public static Boolean GetRegistrosAndBalanceL007(DateTime DateFrom,
                                                     Int32 ItemsPerPage,
                                                     PSA_TYPE PSAType,
                                                     out List<PSASendObject> ListObjectsToSend,
                                                     out BalanceLinea Balance,
                                                     out Int32 CountRegistrosToSend,
                                                     out PSA_Mensaje_Reporte_diario ReporteDiario,
                                                     out DateTime DateFirstRegister,
                                                     out DateTime DateLastRegister)
    {
      Registro_L007 _registro;
      DataSet _ds;
      DateTime _date_to;
      Decimal _ingresos;
      Decimal _salidas;
      Decimal _bal_ini_linea;

      Int32 _no_registro;
      String _clave_sublinea_negocio;
      String _xml;
      String _checksum;
      PSASendObject _send_object;
      L007_UNR_property _prop_monto_premio_UNR;
      Int32 _curp_mode;
      Boolean _report_expired_prize;
      Prize_Register _prize_register;
      Boolean _report_refunds;
      Decimal _prize;
      String _cashier_movs_table;

      Balance = new BalanceLinea();
      ReporteDiario = new PSA_Mensaje_Reporte_diario();
      ReporteDiario.Registros_L007 = new List<Registro_L007>();
      ListObjectsToSend = new List<PSASendObject>();
      CountRegistrosToSend = 0;
      if (IsEnableDailyReportOffset())
      {
        DateFrom = DateFrom.AddDays(-1);
      }
      _date_to = DateFrom.AddDays(1);

      DateFirstRegister = _date_to;
      DateLastRegister = DateFrom;

      try
      {
        _clave_sublinea_negocio = WSI.Common.GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_CLAVE_SUBLINEA_NEGOCIO);
        _curp_mode = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_CONSTANCIAS_REPORTAR_CURP, 0);
        _report_expired_prize = GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_REPORT_EXPIRED_PRIZE, true);

        // JML 9-OCT-2014: New parameter to put the prize in "Linea"(0) or "Sorteo"(1) field
        _prize_register = (Prize_Register)(WSI.Common.GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_FIELD_TO_REPORT_PRIZE, 0));
        // JML 9-OCT-2014: New parameter to report refunds: 0-No report, 1-Report in "MontoApostado" field
        _report_refunds = GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_REPORT_REFUNDS, false);

        //Get Account movements
        _ds = GetAccountMovementsPSA(DateFrom, _date_to, out _bal_ini_linea);
        if (_ds == null)
        {
          return false;
        }

        _cashier_movs_table = GetTableNameCashierMovements();
        _no_registro = 0;

        foreach (DataRow _row in _ds.Tables[_cashier_movs_table].Rows)
        {
          // DDM 08-AUG-2018: Fix bug WIG-1172.
          if ((ReporteDiario.Registros_L007.Count >= ItemsPerPage) && (PSAType == PSA_TYPE.WINPSA))
          {
            PSACommon.ObjectSerializableToXmlAndCheckSum(ReporteDiario, null, out _xml, out _checksum);
            _send_object = new PSASendObject(_xml, _checksum);
            ListObjectsToSend.Add(_send_object);
            ReporteDiario.Registros_L007 = new List<Registro_L007>();
          }

          _no_registro++;

          _registro = new Registro_L007();
          _registro.ISR = PSACommon.ConvertWithTwoDecimals(_row["ISR1"]);
          _registro.NoRegistro = _no_registro;
          _registro.ClavesublineaNegocio = _clave_sublinea_negocio;
          _registro.ClaveCombinacion = WSI.Common.GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_CLAVE_COMBINACION);

          _registro.ClaveTipoPago = _row["ClaveTipoPago"].ToString();
          _registro.FormaPago = _row["FormaPago"].ToString();

          _registro.IdEvento = _row["NoCuenta"].ToString();
          _registro.FechaHora = (DateTime)_row["FechaHora"];

          if (DateFirstRegister > _registro.FechaHora)
          {
            DateFirstRegister = _registro.FechaHora;
          }

          if (DateLastRegister < _registro.FechaHora)
          {
            DateLastRegister = _registro.FechaHora;
          }

          _registro.NumTransaccion = _row["IdOperacion"].ToString();
          _registro.NoRegistroCaja = PSACommon.ConvertToPSAString(_row["CashierName"]);
          _registro.SaldoInicial = PSACommon.ConvertWithTwoDecimals(_row["Deposito"]);

          _registro.MontoPremioLinea = PSACommon.EMPTY_DECIMAL;
          _registro.MontoPremioSorteo = PSACommon.EMPTY_DECIMAL;

          _prize = PSACommon.ConvertWithTwoDecimals(_row["Premio"]);
          _prize += PSACommon.ConvertWithTwoDecimals(_row["RecargaPorPuntosPremio"]);

          switch (_prize_register)
          {
            case Prize_Register.MontoPremioSorteo:
              _registro.MontoPremioSorteo = _prize;
              break;
            case Prize_Register.MontoPremioLinea:
            default:
              _registro.MontoPremioLinea = _prize;
              break;
          }

          //Get comprobante
          _registro.NoComprobante = GetNumeroComprobante(_row);

          _registro.MontoPremioNoReclamado = PSACommon.ConvertWithTwoDecimals(_row["PremioCaducado"]);
          if (!_report_expired_prize && _row["PremioCaducado"] != DBNull.Value && (Decimal)_row["PremioCaducado"] != 0)
          {
            _row.Delete();
            _no_registro--;

            continue;
          }

          _registro.SaldoPromocion = PSACommon.ConvertWithTwoDecimals(_row["SaldoPromocional"]);
          _registro.MontoApostado = PSACommon.EMPTY_DECIMAL;
          _registro.MontoRecaudado = PSACommon.EMPTY_DECIMAL;
          _registro.MontoBolsa = PSACommon.EMPTY_DECIMAL;
          _registro.MontoReserva = PSACommon.EMPTY_DECIMAL;

          _prop_monto_premio_UNR = (L007_UNR_property)GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_FIELD_TO_REPORT_UNR, 0);
          switch (_prop_monto_premio_UNR)
          {

            case L007_UNR_property.NoReportar:
              _registro.MontoPremioEspecial = PSACommon.EMPTY_DECIMAL;
              break;

            case L007_UNR_property.MontoPremioEspecial:
              _registro.MontoPremioEspecial = PSACommon.ConvertWithTwoDecimals(_row["PremioEspecie"]);

              break;

            case L007_UNR_property.MontoPremioSorteo:
            default:
              _registro.MontoPremioSorteo += PSACommon.ConvertWithTwoDecimals(_row["PremioEspecie"]);
              break;
          }

          if (_report_refunds)
          {
            _registro.MontoApostado = PSACommon.ConvertWithTwoDecimals(_row["Devolucion"]);
          }
          else if (XMLReport.GetPSAClient_EspecialMode() == WSI.Common.XMLReport.PSAClient_EspecialMode.AlesisWinpot)
          {
            // This especial mode  does not support:
            //    - General parameter "ReportRefund" = 1
            //    - General parameter "FieldToReportPrize" = 1 (It supports only send/report the prize award in the "MontoPremioEspecial", FieldToReportPrize" = 0 )
            // TODO: Otra opci�n de "IFO" a estudiar: if (_registro.MontoApostado == PSACommon.EMPTY_DECIMAL) && especialmode.AlesisWinpot....
            if (_registro.MontoPremioEspecial > 0)
            {
              // Premio Sorteo: premio bruto - tax
              _registro.MontoApostado = (PSACommon.ConvertWithTwoDecimals(_row["PremioEspecie"])
                                          - PSACommon.ConvertWithTwoDecimals(_row["ISR1"]));
            }
            else
            {
              _registro.MontoApostado = _registro.SaldoInicial;
              _registro.MontoRecaudado = _registro.SaldoInicial;
            }
          }

          // Not required!!
          //_registro.TipoCambio = 10;
          if ((Boolean)_row["Constancia"])
          {
            _registro.Constancia = new Registro_Linea_Constancia();
            _registro.Constancia.General = new Registro_Linea_Constancia_General();
            _registro.Constancia.General.ISRretenido = PSACommon.ConvertWithTwoDecimals(_row["ISR1"]);
            _registro.Constancia.General.MontoPremio = PSACommon.ConvertWithTwoDecimals(_row["Premio"]);

            _registro.Constancia.Jugador = new Registro_Linea_Constancia_Jugador();
            _registro.Constancia.Jugador.General = new Registro_Linea_Constancia_Jugador_General();
            _registro.Constancia.Jugador.General.NombreJugador = PSACommon.ConvertToPSAString(_row["Constancia.Nombre"]);
            _registro.Constancia.Jugador.General.rfc = PSACommon.ConvertToPSAString(_row["Constancia.RFC"]);
            _registro.Constancia.Jugador.General.DocumentoID = PSACommon.ConvertToPSAString(_row["Constancia.DocumentoID"]);
            _registro.Constancia.Jugador.General.NoDocumentoID = PSACommon.ConvertToPSAString(_row["Constancia.NoDocumentoID"]);

            if (_curp_mode == 1)
            {
              _registro.Constancia.Jugador.General.CURP = PSACommon.ConvertToPSAString(_row["Constancia.CURP"]);
            }

            // 21-OCT-2016 FGB    PBI 10327: PSA Client-Alesis: Obtaining data for the withhold and check the results
            if (HasToReportMajorPrizeAdditionalFields())
            {
              _registro.Constancia.Jugador.Domicilio = new Registro_Linea_Constancia_Jugador_Domicilio();
              _registro.Constancia.Jugador.Domicilio.calle = PSACommon.ConvertToPSAString(_row["Constancia.calle"]);
              _registro.Constancia.Jugador.Domicilio.municipio = PSACommon.ConvertToPSAString(_row["Constancia.municipio"]);
              _registro.Constancia.Jugador.Domicilio.estado = PSACommon.ConvertToPSAString(_row["Constancia.estado"]);
              _registro.Constancia.Jugador.Domicilio.pais = PSACommon.ConvertToPSAString(_row["Constancia.pais"]);
              _registro.Constancia.Jugador.Domicilio.noExterior = PSACommon.ConvertToPSAString(_row["Constancia.noExterior"]);
              _registro.Constancia.Jugador.Domicilio.noInterior = PSACommon.ConvertToPSAString(_row["Constancia.noInterior"]);
              _registro.Constancia.Jugador.Domicilio.colonia = PSACommon.ConvertToPSAString(_row["Constancia.colonia"]);
              _registro.Constancia.Jugador.Domicilio.referencia = PSACommon.ConvertToPSAString(_row["Constancia.referencia"]);

              _registro.Constancia.Jugador.Domicilio.codigoPostal = PSACommon.ConvertToPSAString(_row["Constancia.codigoPostal"]);
              _registro.Constancia.Jugador.Domicilio.localidad = PSACommon.ConvertToPSAString(_row["Constancia.localidad"]);
            }
          }

          ReporteDiario.Registros_L007.Add(_registro);
        }

        _ds.AcceptChanges();

        // calculate balance 
        if (_ds.Tables[_cashier_movs_table].Rows.Count == 0)
        {
          // Not have movements for that day!!
          Balance = GetBlankBalance(_clave_sublinea_negocio);

          DateFirstRegister = DateFrom;
          DateLastRegister = DateFrom;

          return true;
        }

        if (ReporteDiario.Registros_L007.Count > 0)
        {
          PSACommon.ObjectSerializableToXmlAndCheckSum(ReporteDiario, null, out _xml, out _checksum);
          _send_object = new PSASendObject(_xml, _checksum);
          ListObjectsToSend.Add(_send_object);
        }

        //CountRegistrosToSend = ReporteDiario.Registros_L007.Count;
        //Fixed Bug #647 Number Total Registers was wrong
        //Have to send the total records in FASE2 and not number of the current page.
        CountRegistrosToSend = _ds.Tables[_cashier_movs_table].Rows.Count;

        // Compute inputs & outputs
        ComputeInputsAndOutputs(_ds.Tables[_cashier_movs_table], out _ingresos, out _salidas);

        Balance.BalanceLineaNegocio = new BalanceLineaNegocio();
        Balance.BalanceLineaNegocio.BaliniLinea = _bal_ini_linea.ToString(PSACommon.MONEDA_FORMAT);
        Balance.BalanceLineaNegocio.BalfinLinea = ((Decimal)(_ingresos - _salidas) + _bal_ini_linea).ToString(PSACommon.MONEDA_FORMAT);
        Balance.BalanceLineaNegocio.IngresosCaja = _ingresos.ToString(PSACommon.MONEDA_FORMAT);
        Balance.BalanceLineaNegocio.SalidasCaja = _salidas.ToString(PSACommon.MONEDA_FORMAT);

        Balance.BalanceSublineasNegocio = new BalanceSubLineaNegocio[1];
        Balance.BalanceSublineasNegocio[0] = new BalanceSubLineaNegocio();
        Balance.BalanceSublineasNegocio[0].ClavesublineaNegocio = _clave_sublinea_negocio;
        Balance.BalanceSublineasNegocio[0].BaliniSubLinea = Balance.BalanceLineaNegocio.BaliniLinea;
        Balance.BalanceSublineasNegocio[0].BalfinSubLinea = Balance.BalanceLineaNegocio.BalfinLinea;

        return true;
      }
      catch (Exception _ex)
      {
        // Malformed invalid cast...
        Log.Exception(_ex);
      }

      return false;
    } // GetRegistrosAndBalanceL007
    #endregion

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE: Generate the Caja Unica report from a two dates range
    //
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DataSet containing all the report data
    //
    public static DataSet GetAccountMovementsPSA(DateTime FromDate, DateTime ToDate, out Decimal InitialBalance)
    {
      return WSI.Common.XMLReport.AccountMovementsPSA(FromDate, ToDate, out InitialBalance);
    }

    /// <summary>
    /// Returns the numero de comprobante
    /// </summary>
    /// <param name="Row"></param>
    /// <returns></returns>
    public static String GetNumeroComprobante(DataRow Row)
    {
      String NumeroComprobante;

      if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_REGISTRO_L007_MODIFY_NOCOMPROBANTE_BY_FOLIO, false))
      {
        NumeroComprobante = PSACommon.ConvertToPSAString(Row["Folio"]);
      }
      else
      {
        NumeroComprobante = PSACommon.ConvertToPSAString(Row["VoucherId"]);
      }

      return NumeroComprobante;
    }

    /// <summary>
    /// Returns if we must report the Additional Fields for Major Prizes.
    /// </summary>
    /// <returns></returns>
    public static Boolean HasToReportMajorPrizeAdditionalFields()
    {
      return WSI.Common.XMLReport.HasToReportMajorPrizeAdditionalFields();
    }

    /// <summary>
    /// Return BlankBalance, initialized with 0 values.
    /// </summary>
    /// <param name="SublineaNegocio"></param>
    /// <returns></returns>
    private static BalanceLinea GetBlankBalance(String SublineaNegocio)
    {
      BalanceLinea _balance;
      String _zero_value;

      _balance = new BalanceLinea();
      _zero_value = ((Decimal)0).ToString(PSACommon.MONEDA_FORMAT);

      _balance.BalanceLineaNegocio = new BalanceLineaNegocio();
      _balance.BalanceLineaNegocio.BaliniLinea = _zero_value;
      _balance.BalanceLineaNegocio.BalfinLinea = _zero_value;
      _balance.BalanceLineaNegocio.IngresosCaja = _zero_value;
      _balance.BalanceLineaNegocio.SalidasCaja = _zero_value;

      _balance.BalanceSublineasNegocio = new BalanceSubLineaNegocio[1];
      _balance.BalanceSublineasNegocio[0] = new BalanceSubLineaNegocio();
      _balance.BalanceSublineasNegocio[0].ClavesublineaNegocio = SublineaNegocio;
      _balance.BalanceSublineasNegocio[0].BaliniSubLinea = _zero_value;
      _balance.BalanceSublineasNegocio[0].BalfinSubLinea = _zero_value;

      return _balance;
    }

    /// <summary>
    /// Returns the table name of Cashier movements
    /// </summary>
    /// <returns></returns>
    private static String GetTableNameCashierMovements()
    {
      return WSI.Common.XMLReport.GetTableNameCashierMovements();
    }

    /// <summary>
    /// Compute inputs and outputs
    /// </summary>
    /// <param name="DataTableMovs"></param>
    /// <param name="Ingresos"></param>
    /// <param name="Salidas"></param>
    private static void ComputeInputsAndOutputs(DataTable DataTableMovs, out Decimal Ingresos, out Decimal Salidas)
    {
      Object _obj_compute;
      Object _obj_compute_isr1;
      Object _obj_compute_isr2;
      Object _obj_compute_isr3;
      Object _obj_compute_decimal_rounding;
      Object _obj_compute_recharge_points_prize;
      Object _obj_compute_cancelation_a;
      Object _obj_compute_cancelation_b;

      Ingresos = 0;
      Salidas = 0;

      if (DataTableMovs == null)
      {
        return;
      }

      _obj_compute = DataTableMovs.Compute("SUM(Deposito)", "Deposito is not null");
      if (_obj_compute != DBNull.Value)
      {
        Ingresos = (Decimal)_obj_compute;
      }

      _obj_compute = DataTableMovs.Compute("SUM(Premio)", "Premio IS NOT NULL");
      if (_obj_compute != DBNull.Value)
      {
        Salidas = (Decimal)_obj_compute;
      }

      _obj_compute_isr1 = DBNull.Value;
      // DHA 29-NOV-2013: Compute ISR1/ISR2 as 'Salidas', it depends of General Params configuration
      if (GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_SALIDAS_ISR1, 1) == 0)
      {
        _obj_compute_isr1 = DataTableMovs.Compute("SUM(ISR1)", "ISR1 IS NOT NULL");
      }

      if (_obj_compute_isr1 != DBNull.Value)
      {
        Salidas -= (Decimal)_obj_compute_isr1;
      }

      _obj_compute_isr2 = DBNull.Value;
      if (GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_SALIDAS_ISR2, 1) == 0)
      {
        _obj_compute_isr2 = DataTableMovs.Compute("SUM(ISR2)", "ISR2 IS NOT NULL");
      }

      if (_obj_compute_isr2 != DBNull.Value)
      {
        Salidas -= (Decimal)_obj_compute_isr2;
      }

      _obj_compute_isr3 = DBNull.Value;
      if (GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_SALIDAS_ISR3, 1) == 0)
      {
        _obj_compute_isr3 = DataTableMovs.Compute("SUM(ISR3)", "ISR3 IS NOT NULL");
      }

      if (_obj_compute_isr3 != DBNull.Value)
      {
        Salidas -= (Decimal)_obj_compute_isr3;
      }

      // SGB 17-MAR-2015: Cancelation controls
      _obj_compute_cancelation_a = DBNull.Value;
      if (GeneralParam.GetBoolean("Cashier", "CancellationAsUndoOperation"))
      {
        _obj_compute_cancelation_a = DataTableMovs.Compute("SUM(CancelacionEmpresaA)", "CancelacionEmpresaA IS NOT NULL");
      }

      if (_obj_compute_cancelation_a != DBNull.Value)
      {
        Salidas -= (Decimal)_obj_compute_cancelation_a;
        Ingresos -= (Decimal)_obj_compute_cancelation_a;
      }

      _obj_compute_cancelation_b = DBNull.Value;
      if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_SALIDAS_INCLUIR_CANCELACIONES_EMPRESAB))
      {
        _obj_compute_cancelation_b = DataTableMovs.Compute("SUM(CancelacionEmpresaB)", "CancelacionEmpresaB IS NOT NULL");
      }

      if (_obj_compute_cancelation_b != DBNull.Value)
      {
        Salidas += (Decimal)_obj_compute_cancelation_b;
      }

      // DDM 28-MAY-2014: Compute 'Decimal rounding', it depends of GP.
      // AJQ 05-JUN-2014: Also include 'RedondeoPorRetencion'
      _obj_compute_decimal_rounding = DBNull.Value;
      if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_SALIDAS_REDONDEO, false))
      {
        Decimal _rounding1;
        Decimal _rounding2;

        _rounding1 = 0;
        _rounding2 = 0;

        try
        {
          _rounding1 = (Decimal)DataTableMovs.Compute("SUM(RedondeoPorDecimales)", "RedondeoPorDecimales IS NOT NULL");
        }
        catch
        {
        }

        try
        {
          _rounding2 = (Decimal)DataTableMovs.Compute("SUM(RedondeoPorRetencion)", "RedondeoPorRetencion IS NOT NULL");
        }
        catch
        {
        }

        _obj_compute_decimal_rounding = (_rounding1 + _rounding2);
      }

      if (_obj_compute_decimal_rounding != DBNull.Value)
      {
        Salidas += (Decimal)_obj_compute_decimal_rounding;
      }

      // DHA 29-MAY-2014: Compute 'Recharge for point - price'
      _obj_compute_recharge_points_prize = DataTableMovs.Compute("SUM(RecargaPorPuntosPremio)", "RecargaPorPuntosPremio IS NOT NULL");
      if (_obj_compute_recharge_points_prize != DBNull.Value)
      {
        Salidas += (Decimal)_obj_compute_recharge_points_prize;
      }

      _obj_compute = DataTableMovs.Compute("SUM(Devolucion)", "Devolucion IS NOT NULL");
      if (_obj_compute != DBNull.Value)
      {
        Salidas = Salidas + (Decimal)_obj_compute;
      }
    }
    #endregion
  } // DB_PSAReportesDiarios
}