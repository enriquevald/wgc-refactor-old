//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DB_PSA.cs
// 
//   DESCRIPTION: Common process from PSA
// 
//        AUTHOR: Dani Domínguez and Artur Nebot
// 
// CREATION DATE: 13-Nov-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-Nov-2012 DDM    Initial version.
// 14-NOV-2012 ANG    Expand and implement initial version.
// 28-NOV-2012 ANG    Write AlarmClock() to compute remaining time
// 02-AUG-2017 JML    29080: WIGOS-4025 PSA Client - New mode "Daily report offset"
// 10-APR-2018 JML    Feature 32223:WIGOS-9856 Parametrización de la inclusión del impuesto sobre erogaciones en el cálculo del monto diario de salidas de caja que se informa en los balances diarios de caja reportados al PSA
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using WSI.Common;

namespace WSI.PSA_Client
{
  public static class PSACommon
  {
    #region Constants

    public const String DAY_DATE_FORMAT = "yyyy-MM-dd";
    public const String HOUR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public const String MONEDA_FORMAT = "000000000000.00";//"#,##0.00";
    public const String EMPTY_STRING = " ";
    
    public const String GP_PSA_GROUP = "PSAClient";
    public const String GP_ALESIS_DELETE_MESSAGE = "PrintAlesisDeleteMsg";
    public const String GP_ALESIS_INSERT_MESSAGE = "PrintAlesisInsertMsg";
    public const String GP_LAST_DAY_AS_NEXT_MONTH = "LastDayAsNextMonth";
    public const String GP_ESTABLECIMIENTO_ID = "EstablecimientoId";
    public const String GP_OPERADOR_ID = "OperadorId";
    public const String GP_PASSWORD = "Password";
    public const String GP_SERVER_ADDRESS1 = "ServerAddress1";
    public const String GP_SERVER_ADDRESS2 = "ServerAddress2";
    public const String GP_PROXY_ADDRESS = "ProxyAddress";

    public const String GP_CLAVE_ESTABLECIMIENTO = "ClaveEstablecimiento";
    public const String GP_LOGIN = "Login";
    public const String GP_OPERADOR = "Operador";
    public const String GP_VERSION = "Version";

    public const String GP_CLAVE_LINEA_NEGOCIO = "ClaveLineaNegocio";
    public const String GP_CLAVE_SUBLINEA_NEGOCIO = "ClaveSublineaNegocio";
    public const String GP_CLAVE_COMBINACION = "ClaveCombinacion";

    public const String GP_FIELD_TO_REPORT_UNR = "FieldToReportUNR";

    public const String GP_BEFORE_SEND_WAIT_HOURS = "BeforeSendWaitHours";
    public const String GP_CLOSING_TIME = "ClosingTime";

    public const String GP_NUMBER_OF_PREVIOUS_MONTHS_TO_REPORT = "NumberOfPreviousMonthsToReport";

    public const String GP_CONSTANCIAS_REPORTAR_CURP = "Constancias.ReportarCURP";
    public const String GP_REPORT_EXPIRED_PRIZE = "ReportExpiredPrize";
    public const String GP_FIELD_TO_REPORT_PRIZE = "FieldToReportPrize";
    public const String GP_REPORT_REFUNDS = "ReportRefunds";
    public const String GP_REGISTRO_L007_MODIFY_NOCOMPROBANTE_BY_FOLIO = "RegistroL007.ModifyNoComprobanteByFolio";
    public const String GP_SALIDAS_ISR1 = "Salidas.ISR1";
    public const String GP_SALIDAS_ISR2 = "Salidas.ISR2";
    public const String GP_SALIDAS_ISR3 = "Salidas.ISR3";
    public const String GP_SALIDAS_INCLUIR_CANCELACIONES_EMPRESAB = "Salidas.IncluirCancelacionesEmpresaB";
    public const String GP_SALIDAS_REDONDEO = "Salidas.Redondeo";
    public const String GP_ENABLE_DAILY_REPORT_OFFSET = "EnableDailyReportOffset"; 

    // Max number of monitorizacion records to send!
    public const Int32 TOTAL_RECORDS_TO_SEND = 50000;

    // Check if its behavior like return Decimal.Round(0, 2);
    public const Decimal EMPTY_DECIMAL = 0.00m;

    #endregion

    #region Structs
    public struct TIPO_CARENCIA
    {
      public static String REPORTE_NO_RECIBIDO = "Reporte_no_Recibido";
      public static String REPORTE_INFORMADO_CON_0_REGISTROS = "Reporte_Informado_Con_0_Registros";
    }
    #endregion

    #region Enums

    public enum ResponseCode
    {
      PSA_RC_OK,
      PSA_RC_SESSION_TIMEOUT,
      PSA_RC_BAD_SEQID,
      PSA_RC_BAD_MESSAGE_TYPE,
      PSA_RC_BAD_CHECKSUM,
      PSA_RC_BAD_MESSAGE_DATA,
      PSA_RC_INCOHERENCE_WITH_DB_DATA,
      PSA_RC_INTERNAL_PSA_ERROR,
      PSA_RC_BAD_LOGIN,
      PSA_RC_TOO_MANY_RETRYS,
      PSA_RC_PERIOD_EXPIRED
    }

    #endregion

    // Delegate with Transformation function Signature
    // For use as param in ObjectSerializableToXmlAndCheckSum()
    public delegate String PerformXMLAdaptationForPSA(String Xml);

    //------------------------------------------------------------------------------
    // PURPOSE: Get PSA Service connection Data from General Params.
    //
    //  PARAMS:
    //      - INPUT: 
    //          - XmlObject
    //
    //      - OUTPUT:
    //          - XmlStr
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static bool GetAndCheckPSAServiceConnectionData(out String Login
                                                         , out String Password
                                                         , out String ServerAddress1
                                                         , out String ServerAddress2
                                                         , out String Operador
                                                         , out String ClaveEstablecimiento
                                                         , out String ClaveLineaNegocio
                                                         , out String Version)
    {
      Login = String.Empty;
      Password = String.Empty;
      ServerAddress1 = String.Empty;
      ServerAddress2 = String.Empty;
      Operador = String.Empty;
      ClaveEstablecimiento = String.Empty;
      ClaveLineaNegocio = String.Empty;
      Version = String.Empty;

      try
      {
        ServerAddress1 = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_SERVER_ADDRESS1);
        ServerAddress2 = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_SERVER_ADDRESS1);

        if (String.IsNullOrEmpty(ServerAddress1) && String.IsNullOrEmpty(ServerAddress2))
        {
          Log.Error(" PSA ServerAddress not set");
          return false;
        }

        Login = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_LOGIN);
        Password = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_PASSWORD);
        Operador = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_OPERADOR);
        Version = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_VERSION);

        if (String.IsNullOrEmpty(Login))
        {
          Log.Error(String.Format(" {0}.Login missing", GP_PSA_GROUP));
          return false;
        }

        if (String.IsNullOrEmpty(Password))
        {
          Log.Error(String.Format(" {0}.Password missing", GP_PSA_GROUP));
          return false;
        }

        if (String.IsNullOrEmpty(Operador))
        {
          Log.Error(String.Format(" {0}.Operador missing", GP_PSA_GROUP));
          return false;
        }

        // TODO si hay una de las dos direcciones comprobar que hay login, pasword,version y operador. Mostrar log.
        if (!GetClaveEstablecimiento(out ClaveEstablecimiento))
        {
          return false;
        }

        ClaveLineaNegocio = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_CLAVE_LINEA_NEGOCIO);
        if (String.IsNullOrEmpty(ClaveLineaNegocio))
        {
          Log.Error(String.Format(" {0}.ClaveLineaNegocio missing", GP_PSA_GROUP));
          return false;
        }

        if (String.IsNullOrEmpty(WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_CLAVE_SUBLINEA_NEGOCIO)))
        {
          Log.Error(String.Format(" {0}.ClaveSublineaNegocio missing", GP_PSA_GROUP));
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetPSAServiceConnectionData

    //------------------------------------------------------------------------------
    // PURPOSE: Check General Params.
    //
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static bool CheckAlesisServiceConnectionData()
    {
      String _tmp = String.Empty;

      try
      {
        _tmp = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_SERVER_ADDRESS1);
        if (String.IsNullOrEmpty(_tmp))
        {
          Log.Error(String.Format(" {0}.ServerAddress1 missing", GP_PSA_GROUP));

          return false;
        }

        _tmp = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_SERVER_ADDRESS1);
        if (String.IsNullOrEmpty(_tmp))
        {
          Log.Error(String.Format(" {0}.ServerAddress2 missing", GP_PSA_GROUP));

          return false;
        }

        _tmp = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_OPERADOR_ID);
        if (String.IsNullOrEmpty(_tmp))
        {
          Log.Error(String.Format(" {0}.OperadorId missing", GP_PSA_GROUP));

          return false;
        }

        _tmp = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_ESTABLECIMIENTO_ID);
        if (String.IsNullOrEmpty(_tmp))
        {
          Log.Error(String.Format(" {0}.EstablecimientoId missing", GP_PSA_GROUP));

          return false;
        }

        _tmp = WSI.Common.GeneralParam.GetString(GP_PSA_GROUP, GP_PASSWORD);
        if (String.IsNullOrEmpty(_tmp))
        {
          Log.Error(String.Format(" {0}.Password missing", GP_PSA_GROUP));

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CheckAlesisServiceConnectionData

    //------------------------------------------------------------------------------
    // PURPOSE: Get Clave Establecimiento
    //          Get ClaveEstablecimiento value from General Params, 
    //          if this value is null , fet Site.Identifier.
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - String ClaveEstablecimiento
    //
    // RETURNS:
    //          - True: OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static Boolean GetClaveEstablecimiento(out String ClaveEstablecimiento)
    {
      Int32 _clave;
      ClaveEstablecimiento = String.Empty;

      _clave = WSI.Common.GeneralParam.GetInt32(GP_PSA_GROUP, GP_CLAVE_ESTABLECIMIENTO, 0);

      if (_clave == 0)
      {
        _clave = WSI.Common.GeneralParam.GetInt32("Site", "Identifier", 0);
      }

      if (_clave > 999 || _clave <= 0)
      {
        Log.Error(" Wrong ClaveEstablecimiento:" + _clave.ToString());

        return false;
      }

      ClaveEstablecimiento = _clave.ToString("000");

      return true;
    } // GetClaveEstablecimiento

    //------------------------------------------------------------------------------
    // PURPOSE: Get XML Serialization from Object
    //
    //  PARAMS:
    //      - INPUT: 
    //          - XmlObject
    //
    //      - OUTPUT:
    //          - XmlStr
    //
    // RETURNS:
    //          - True: Validate OK 
    //          - False: otherwise
    //
    //   NOTES:
    public static Boolean ObjectSerializableToXml(Object XmlObject, out String XmlStr)
    {
      System.Xml.Serialization.XmlSerializer _xml_ser;
      StreamWriter _stream_writer;
      StreamReader _stream_reader;

      XmlStr = String.Empty;

      try
      {
        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        ns.Add(String.Empty, String.Empty);

        using (MemoryStream _memory_stream = new System.IO.MemoryStream())
        {
          _stream_writer = new System.IO.StreamWriter(_memory_stream);
          _stream_reader = new System.IO.StreamReader(_memory_stream);

          _xml_ser = new System.Xml.Serialization.XmlSerializer(XmlObject.GetType());
          _xml_ser.Serialize(_stream_writer, XmlObject, ns);

          _memory_stream.Position = 0;
          XmlStr = _stream_reader.ReadToEnd();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ObjectSerializableToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Get Checksum from Object
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Object to get checksum
    //
    //      - OUTPUT:
    //          - String with checksum of object.
    //
    // RETURNS:
    //          - True: Ok 
    //          - False: Error serializing object.
    //
    //   NOTES:
    public static Boolean ObjectSerializableToXmlAndCheckSum(Object ObjectToChecksum, out String XmlObject, out String CheckSum)
    {
      return ObjectSerializableToXmlAndCheckSum(ObjectToChecksum, null, out XmlObject, out CheckSum);
    } // ObjectSerializableToXmlAndCheckSum

    //------------------------------------------------------------------------------
    // PURPOSE: Get Checksum from Object
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Object to get checksum
    //          - Transform Function.
    //            Function to custom default Xml Serialization.
    //
    //      - OUTPUT:
    //          - String with checksum of object.
    //
    // RETURNS:
    //          - True: Ok 
    //          - False: Error serializing object.
    //
    //   NOTES:
    public static Boolean ObjectSerializableToXmlAndCheckSum(Object ObjectToSerialize
                                                           , PerformXMLAdaptationForPSA TransformXMLObject
                                                           , out String XmlObject
                                                           , out String CheckSum)
    {
      CheckSum = String.Empty;
      XmlObject = String.Empty;

      if (!ObjectSerializableToXml(ObjectToSerialize, out XmlObject))
      {
        return false;
      }

      if (TransformXMLObject != null)
      {
        // Invoke function ( if exists ) it knows how to 
        // transform the default Xml to desired custom Xml. 
        XmlObject = TransformXMLObject.Invoke(XmlObject);
      }

      CheckSum = CalculateMD5Hash(XmlObject);

      return true;
    } // ObjectSerializableToXmlAndCheckSum

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate MD5 hash.
    //
    //  PARAMS:
    //      - INPUT: 
    //          - String to get hash
    //
    //      - OUTPUT:
    //          - XmlStr
    //
    // RETURNS:
    //         Hash from string
    //
    //   NOTES:
    public static String CalculateMD5Hash(String TextToChecksum)
    {
      MD5 _md5;
      byte[] _hash;

      _md5 = MD5.Create();
      _hash = _md5.ComputeHash(Encoding.UTF8.GetBytes(TextToChecksum));

      return BitConverter.ToString(_hash).Replace("-", "").ToLowerInvariant();
    } // CalculateMD5Hash

    /// <summary>
    /// Returns the type decimal value with two decimal
    ///   If Null or empty return PSACommon.EMPTY_DECIMAL
    /// </summary>
    /// <param name="ObjectValue"></param>
    /// <returns></returns>
    
    public static Decimal ConvertWithTwoDecimals(Object ObjectValue)
    {
      return ((ObjectValue != DBNull.Value) ? (Decimal.Round((Decimal)ObjectValue, 2)) : PSACommon.EMPTY_DECIMAL);
    }

    /// <summary>
    /// Convert String to PSA. 
    ///   If Null or empty return PSACommon.EMPTY_STRING
    /// </summary>
    /// <param name="ObjectValue"></param>
    /// <returns></returns>
    public static String ConvertToPSAString(Object ObjectValue)
    {
      String _str_PSA;

      _str_PSA = ((ObjectValue != DBNull.Value) ? ObjectValue.ToString() : PSACommon.EMPTY_STRING);
      if (String.IsNullOrEmpty(_str_PSA))
      {
        _str_PSA = PSACommon.EMPTY_STRING;
      }

      return _str_PSA;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Function that sets the format from response code to view in log
    //
    //  PARAMS:
    //      - INPUT: 
    //          - PSA_Respuesta
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //          - String with format
    //
    //   NOTES:
    //
    public static String FormatFromErrorOfPSARespuesta(WSI.PSA_Client.WS_PSA.PSA_Respuesta Response)
    {
      return FormatFromErrorOfPSARespuesta(Response.CodigoRespuesta, Response.CodigoRespuestaDescripcion);
    } // FormatFromErrorOfResponseCode

    //------------------------------------------------------------------------------
    // PURPOSE: Function that sets the format from response code to view in log
    //
    //  PARAMS:
    //      - INPUT: 
    //          - ResponseCode
    //          - ResponseCodeDescription
    //
    //      - OUTPUT:
    //          - None
    //
    // RETURNS:
    //          - String with format
    //
    //   NOTES:
    //
    public static String FormatFromErrorOfPSARespuesta(String ResponseCode, String ResponseCodeDescription)
    {
      StringBuilder _sb;

      if (String.IsNullOrEmpty(ResponseCode))
      {
        return String.Empty;
      }

      _sb = new StringBuilder();
      _sb.AppendLine();
      _sb.AppendLine("  - CodigoRespuesta: " + ResponseCode);
      _sb.AppendLine("  - CodigoRespuestaDescripcion: " + ResponseCodeDescription);
      _sb.AppendLine();

      return _sb.ToString();
    } // FormatFromErrorOfResponseCode
  } // PSACommon
}
