//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for PSA Client to run as a service
// 
//        AUTHOR: Joaquin Calero
// 
// CREATION DATE: 16-JUL-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-JUL-2013 JCA    First release (Header).
// 16-NOV-2015 DLL   Change limit daily repot to 120 month
//------------------------------------------------------------------------------

using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Net;
using WSI.Common;
using System.Diagnostics;

public partial class PSA_Client : CommonService
{
  private const string VERSION = "18.070";
  private const string SVC_NAME = "PSA_Client";
  private const string PROTOCOL_NAME = "PSA CLIENT";
  private const int SERVICE_PORT = 13080;
  private const Boolean STAND_BY = true;
  private const string LOG_PREFIX = "PSA_Client";
  private const string CONFIG_FILE = "WSI.PSA_Client_Config";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT;

  public static void Main(String[] Args)
  {
    PSA_Client _this_service;

    _this_service = new PSA_Client();
    _this_service.Run(Args);
  } // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    if (Debugger.IsAttached)
    {
      m_stand_by = false;
    }
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
  } // SetParameters

  protected override void OnServiceBeforeInit()
  {
    
  } // OnServiceBeforeInit

  //------------------------------------------------------------------------------
  // PURPOSE : Starts the service.
  //
  //  PARAMS :
  //      - INPUT :
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  protected override void OnServiceRunning(IPEndPoint ListeningOn)
  {
    //Int32 _site_id;
    //GetInfo _get_info;

    //// Get Site data
    //while (WSI.WWP.Site.Id == 0)
    //{
    //  _get_info = new GetInfo();

    //  Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _site_id);
    //  WSI.WWP.Site.Id = _site_id;
    //  WSI.WWP.Site.Name = Misc.ReadGeneralParams("Site", "Name");
    //  WSI.WWP.Site.ServerMacAddress = _get_info.GetMACAddress();

    //  if (WSI.WWP.Site.Id == 0)
    //  {
    //    Log.Error("SiteId not configured.");

    //    if (m_ev_stop.WaitOne(60000))
    //    {
    //      return;
    //    }
    //  }
    //}

    //Start PSA send Reportes
    WSI.PSA_Client.PSA_ClientReportesDiarios.Init();

  } // OnServiceRunning

  //------------------------------------------------------------------------------
  // PURPOSE : Stops the service.
  //
  //  PARAMS :
  //      - INPUT :
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  protected override void OnServiceStopped()
  {

  }

} // PSA_SiteService


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under system account
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }

  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //Assembly asembl;
    //asembl = Assembly.GetExecutingAssembly();
    //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

    //The service has a fixed name.
    serviceInstaller.ServiceName = "PSA_Client";
  }

  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //Assembly asembl;
    //asembl = Assembly.GetExecutingAssembly();
    //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

    //The service has a fixed name.
    serviceInstaller.ServiceName = "PSA_Client";
  }
}
