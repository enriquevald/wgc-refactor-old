//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: Alesis_ClientReportesDiarios.cs
//
//   DESCRIPTION: Alesis Client Functions
//
//        AUTHOR: Alberto Marcos
//
// CREATION DATE: 07-OCT-2014
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-OCT-2014 AMF    First release
// 02-SEP-2015 JCA    Add GP LastDayAsNextMonth
// 12-APR-2016 JCA    Bug 11737:PSA_Client: Error en el borrado de registros, no se tiene en cuenta el par�metro PSAClient.LastDayAsNextMonth
// 03-MAR-2016 DDM    Product Backlog Item 10292: PSAClient-Alesis-Palacio: Get operations of external database
//                    and modifications (print xml, constancias LastDayAsNextMonth)
// 21-OCT-2016 FGB    PBI 10327: PSA Client-Alesis: Obtaining data for the withhold and check the results
// 24-OCT-2016 FGB    PBI 11428: PSA Client-Alesis: LOG when deleting records for forwarding (call and ACK)
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using WSI.Common.Utilities;

namespace WSI.PSA_Client
{
  class Alesis_ClientReportesDiarios
  {
    #region Constants
    public const String HOUR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    #endregion

    #region Members
    //Masks to say what Report XML to log
    [Flags]
    public enum MASK_LOG_MSG_ALESIS
    {
      // MASK_STATUS
      NONE = 0x0000,
      REPORTES_DIARIOS = 0x0001,
      CONSTANCIAS = 0x0002
    }
    #endregion

    public static class ReportesDiariosAlesis
    {
      /// <summary>
      /// Returns if the Mask indicates that we have to log the XML messages when we are adding registers to Alesis
      /// </summary>
      /// <returns></returns>
      private static Boolean HasToLogXMLMessagesOnAddRegisters()
      {
        Int32 _print_msg_alesis;

        _print_msg_alesis = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_ALESIS_INSERT_MESSAGE, 0);

        return ((_print_msg_alesis & (Int32)MASK_LOG_MSG_ALESIS.REPORTES_DIARIOS) != 0);
      }

      /// <summary>
      /// Returns if the Mask indicates that we have to log the XML messages when we are deleting registers from Alesis
      /// </summary>
      /// <returns></returns>
      private static Boolean HasToLogXMLMessagesOnDeleteRegisters()
      {
        Int32 _print_delete_msg_alesis;

        _print_delete_msg_alesis = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_ALESIS_DELETE_MESSAGE, 0);

        return ((_print_delete_msg_alesis & (Int32)MASK_LOG_MSG_ALESIS.REPORTES_DIARIOS) != 0);
      }

      //------------------------------------------------------------------------------
      // PURPOSE: Send registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - List<PSASendObject>
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      public static Boolean AlesisSendRegisters(DateTime ReportDate, DateTime DateFrom, DateTime DateTo, List<Registro_L007> Registros, out List<Registro_L007> RegistroConstancias)
      {
        String _token;
        String _establecimiento_id;
        String _operador_id;

        RegistroConstancias = new List<Registro_L007>();

        try
        {
          _establecimiento_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_ESTABLECIMIENTO_ID);
          _operador_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_OPERADOR_ID);

          if (AlesisGetToken(_operador_id, _establecimiento_id, out _token))
          {
            if (AlesisWSAddRegisters(_token, _operador_id, _establecimiento_id, ReportDate, Registros, DateFrom, DateTo, out RegistroConstancias))
            {
              return true;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Error("Error in AlesisSendRegisters(List<Registro_L007> Registros)");
          Log.Exception(_ex);
        }

        return false;
      } //AlesisSendRegisters

      /// <summary>
      /// Adds a year to the date in DEBUG mode for making tests, and later know what registers to delete
      /// </summary>
      /// <param name="DateToSend"></param>
      /// <returns></returns>
      public static DateTime GetDateTimeToSendToAlesis(DateTime DateToSend)
      {
#if DEBUG
        return DateToSend.AddYears(+1);
#else
        return DateToSend;
#endif
      }

      //------------------------------------------------------------------------------
      // PURPOSE: Communicate with the webservice to Add registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Row: Register to add
      //      - OUTPUT:
      //          - Constancias
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean AlesisWSAddRegisters(String Token, String OperadorID, String EstablecimientoID, DateTime ReportDate, List<Registro_L007> Registros, DateTime DateFrom, DateTime DateTo, out List<Registro_L007> RegistroConstancias)
      {
        String _request;
        String _response;
        DateTime _report_date;
        DateTime _date_from;
        DateTime _date_to;

        WS_Alesis.WebServiceLineasdeNegocio7 _ws_alesis;

        RegistroConstancias = new List<Registro_L007>();

        if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_LAST_DAY_AS_NEXT_MONTH, false))
        {
          ReportDate = ReportDate.AddDays(1);
        }

        //Get the dates value to send to Alesis
        _report_date = GetDateTimeToSendToAlesis(ReportDate);
        _date_from = GetDateTimeToSendToAlesis(DateFrom);
        _date_to = GetDateTimeToSendToAlesis(DateTo);
        _request = String.Empty;

        try
        {
          //Get XML
          _request = GenerateAddRegistersXML(Token, OperadorID, EstablecimientoID, Registros, RegistroConstancias, _report_date);

          if (HasToLogXMLMessagesOnAddRegisters())
          {
            Log.Message(_request);
          }

          _ws_alesis = new WS_Alesis.WebServiceLineasdeNegocio7();
          try
          {
            _ws_alesis.Url = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS1);
            _response = _ws_alesis.PkstL007Add(_request);
          }
          catch (Exception _ex)
          {
            Log.Message(_ex.Message);

            //Maybe the sending went OK, but waiting for the reply occurs a TimeOut exception.
            // So now we ask (with Alesis Summary) the number or records in Alesis
            // If in the Alesis Summary we have the same number of records as we sent, then everything is OK.
            return CheckAlesisResponse(_ws_alesis, _report_date, Token, OperadorID, EstablecimientoID, _date_from, _date_to, Registros.Count);
          }

          if ((String.IsNullOrEmpty(_response)) || ProcessResponse(_response, Registros.Count))
          {
            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          if (!String.IsNullOrEmpty(_request))
          {
            Log.Message(_request);
          }
        }

        return false;
      } //AlesisWSAddRegisters

      /// <summary>
      /// Generate XML for adding registers
      /// </summary>
      /// <param name="Token"></param>
      /// <param name="OperadorID"></param>
      /// <param name="EstablecimientoID"></param>
      /// <param name="Registros"></param>
      /// <param name="RegistroConstancias"></param>
      /// <param name="ReportDate"></param>
      /// <returns></returns>
      private static String GenerateAddRegistersXML(String Token, String OperadorID, String EstablecimientoID, List<Registro_L007> Registros, List<Registro_L007> RegistroConstancias, DateTime ReportDate)
      {
        StringBuilder _sb;
        String _tmp;
        String _tmp2;
        DateTime _fecha_hora;

        _sb = new StringBuilder();

        _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        _sb.AppendLine("<Datos_Insertar>");
        _sb.AppendLine(String.Format("<Token SalaID=\"{0}\" OperID=\"{1}\" ID=\"{2}\" />", EstablecimientoID, OperadorID, Token));
        _sb.AppendLine(String.Format("<Periodo Mes=\"{0}\" A�o=\"{1}\" />", ReportDate.Month.ToString().PadLeft(2, '0'), ReportDate.Year));

        foreach (Registro_L007 _registro in Registros)
        {
          _tmp = "<Dato";
          _tmp += String.Format(" ClaveCombinacion=\"{0}\"", _registro.ClaveCombinacion);
          _tmp += String.Format(" ClaveSublineaNegocio=\"{0}\"", _registro.ClavesublineaNegocio);
          _tmp += String.Format(" ClaveTipoPago=\"{0}\"", _registro.ClaveTipoPago);

          if (_registro.Constancia != null)
          {
            RegistroConstancias.Add(_registro);

            _tmp += String.Format(" curp=\"{0}\"", _registro.Constancia.Jugador.General.CURP);
            _tmp += String.Format(" ExpidioConstancia=\"SI\"");
          }
          else
          {
            _tmp += " curp=\"\"";
            _tmp += String.Format(" ExpidioConstancia=\"NO\"");
          }

          _fecha_hora = GetDateTimeToSendToAlesis(_registro.FechaHora);
          _tmp += String.Format(" FechaHora=\"{0}\"", _fecha_hora.ToString(HOUR_DATE_FORMAT));

          _tmp += String.Format(" FormaPago=\"{0}\"", _registro.FormaPago);
          _tmp += String.Format(" IdEvento=\"{0}\"", _registro.IdEvento);

          if (WSI.Common.XMLReport.GetPSAClient_EspecialMode() == XMLReport.PSAClient_EspecialMode.AlesisWinpot && _registro.ISR != PSACommon.EMPTY_DECIMAL)
          {
            _tmp += String.Format(" isr=\"{0}\"", _registro.ISR);
          }
          else if (WSI.Common.XMLReport.GetPSAClient_EspecialMode() == XMLReport.PSAClient_EspecialMode.Default && _registro.Constancia != null)
          {
            _tmp += String.Format(" isr=\"{0}\"", _registro.Constancia.General.ISRretenido);
          }
          else
          {
            _tmp += " isr=\"0\"";
          }

          _tmp += String.Format(" MontoApostado=\"{0}\"", _registro.MontoApostado);
          _tmp += String.Format(" MontoBolsa=\"{0}\"", _registro.MontoBolsa);
          _tmp += String.Format(" MontoPremioEspecial=\"{0}\"", _registro.MontoPremioEspecial);
          _tmp += String.Format(" MontoPremioLinea=\"{0}\"", _registro.MontoPremioLinea);
          _tmp += String.Format(" MontoPremioNoReclamado=\"{0}\"", _registro.MontoPremioNoReclamado);
          _tmp += String.Format(" MontoPremioSorteo=\"{0}\"", _registro.MontoPremioSorteo);
          _tmp += String.Format(" MontoRecaudado=\"{0}\"", _registro.MontoRecaudado);
          _tmp += String.Format(" MontoReserva=\"{0}\"", _registro.MontoReserva);
          _tmp += String.Format(" NoComprobante=\"{0}\"", _registro.NoComprobante);

          if (_registro.Constancia != null)
          {
            _tmp2 = String.Format(" NombreJugador=\"{0}\"", ConstanciasDiariasAlesis.GetValue(_registro.Constancia.Jugador.General.NombreJugador, 40, true));
            _tmp += _tmp2.Replace("&", "&amp;");
          }
          else
          {
            _tmp += " NombreJugador=\"\"";
          }

          _tmp += String.Format(" NoRegistroCaja=\"{0}\"", ConstanciasDiariasAlesis.GetValue(_registro.NoRegistroCaja, 50, true));
          _tmp += String.Format(" NumTransaccion=\"{0}\"", _registro.NumTransaccion);

          if (_registro.Constancia != null)
          {
            _tmp += String.Format(" rfc=\"{0}\"", _registro.Constancia.Jugador.General.rfc);
          }
          else
          {
            _tmp += " rfc=\"\"";
          }

          _tmp += String.Format(" SaldoInicial=\"{0}\"", _registro.SaldoInicial);

          if (WSI.Common.XMLReport.GetPSAClient_EspecialMode() == XMLReport.PSAClient_EspecialMode.AlesisWinpot)
          {
            _tmp += String.Format(" SaldoPromocion=\"{0}\"", _registro.SaldoPromocion);
          }
          else
          {
            _tmp += " SaldoPromocion=\"0\"";
          }

          // Not requiered!!
          //_registro.TipoCambio = 10;
          _tmp += " TipoCambio=\"0\"";
          _tmp += " />";

          _sb.AppendLine(_tmp);
        }

        _sb.AppendLine("</Datos_Insertar>");

        return _sb.ToString();
      }

      /// <summary>
      /// Generate XML for adding registers/// 
      /// </summary>
      /// <param name="Token"></param>
      /// <param name="OperadorID"></param>
      /// <param name="EstablecimientoID"></param>
      /// <param name="ReportDate"></param>
      /// <param name="DateFrom"></param>
      /// <param name="DateTo"></param>
      /// <returns></returns>
      private static String GenerateDeleteRegistersXML(String Token, String OperadorID, String EstablecimientoID, DateTime ReportDate, DateTime DateFrom, DateTime DateTo)
      {
        String _tmp;
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        _sb.AppendLine("<Datos_Eliminar>");
        _tmp = String.Format("<Token SalaID=\"{0}\" OperID=\"{1}\" ID=\"{2}\" />", EstablecimientoID, OperadorID, Token);
        _sb.AppendLine(_tmp);

        _tmp = String.Format("<Periodo Mes=\"{0}\" A�o=\"{1}\" Fecha_Inicial=\"{2}\" Fecha_Final=\"{3}\" />"
                            , GetDateTimeToSendToAlesis(ReportDate).Month.ToString()
                            , GetDateTimeToSendToAlesis(ReportDate).Year.ToString()
                            , GetDateTimeToSendToAlesis(DateFrom).ToString(HOUR_DATE_FORMAT)
                            , GetDateTimeToSendToAlesis(DateTo).ToString(HOUR_DATE_FORMAT));

        _sb.AppendLine(_tmp);
        _sb.AppendLine("</Datos_Eliminar>");

        return _sb.ToString();
      }

      /// <summary>
      /// Checks the Alesis response, verifying that the number of records in Alesis are the same that the records sent.
      /// </summary>
      /// <param name="WSAlesis"></param>
      /// <param name="ReportDate"></param>
      /// <param name="Token"></param>
      /// <param name="OperadorID"></param>
      /// <param name="EstablecimientoID"></param>
      /// <param name="InitialDateTime"></param>
      /// <param name="FinalDateTime"></param>
      /// <param name="NumRecordsSentToAlesis"></param>
      /// <returns></returns>
      private static Boolean CheckAlesisResponse(WS_Alesis.WebServiceLineasdeNegocio7 WSAlesis, DateTime ReportDate, String Token, String OperadorID, String EstablecimientoID, DateTime InitialDateTime, DateTime FinalDateTime, Int32 NumRecordsSentToAlesis)
      {
        StringBuilder _sb;
        String _request;
        String _response;
        String _token;

        try
        {
          //Solicitamos token nuevo
          if (AlesisGetToken(OperadorID, EstablecimientoID, out _token))
          {
            _sb = new StringBuilder();
            _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            _sb.AppendLine("<Resumen>");
            _sb.AppendLine(String.Format("<Token SalaID=\"{0}\" OperID=\"{1}\" ID=\"{2}\" />", EstablecimientoID, OperadorID, _token));
            _sb.AppendLine(String.Format("<Periodo Mes=\"{0}\" A�o=\"{1}\" Fecha_Inicial=\"{2}\" Fecha_Final=\"{3}\" />", ReportDate.Month.ToString(), ReportDate.Year.ToString(), InitialDateTime.ToString(), FinalDateTime.ToString()));
            _sb.AppendLine("</Resumen>");

            //Solicitamos summary a Alesis
            _request = _sb.ToString();
            _response = WSAlesis.PkstL007Summary(_request);

            if (!String.IsNullOrEmpty(_response))
            {
              //Comparamos numero de registros enviados con los que retorna Alesis
              return CompareAlesisSummaryWithNumberRecordsSentToAlesis(_response, NumRecordsSentToAlesis);
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      }

      /// <summary>
      /// Compares the number of records of Alesis with the records sent
      /// </summary>
      /// <param name="ResponseXML">XML replied from Alesis with the Alesis Summary</param>
      /// <param name="NumRecordsSentToAlesis">Number of records sent to Alesis</param>
      /// <returns></returns>
      private static Boolean CompareAlesisSummaryWithNumberRecordsSentToAlesis(String ResponseXML, Int32 NumRecordsSentToAlesis)
      {
        XmlDocument _doc;
        XmlNodeList _childs;
        XmlNode _node;
        XmlAttributeCollection _att_collection;

        Int32 _alesis_num_records;

        _doc = new XmlDocument();

        try
        {
          _doc.LoadXml(ResponseXML);

          _childs = _doc.ChildNodes;
          _node = _childs.Item(1);
          _att_collection = _node.Attributes;

          if (_att_collection["TipoOperacion"].Value.ToLower() != "error")
          {
            if (_node.ChildNodes.Count > 0)
            {
              if (_node.ChildNodes[0].ChildNodes.Count > 0 && _node.ChildNodes[0].ChildNodes[0].Attributes.Count > 0)
              {
                //Gets the number of records in Alesis
                _alesis_num_records = Int32.Parse(_node.ChildNodes[0].ChildNodes[0].Attributes["Transacciones"].Value);

                return (_alesis_num_records == NumRecordsSentToAlesis);
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      }

      //------------------------------------------------------------------------------
      // PURPOSE: Logic to delete registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Report Date
      //          - Date From
      //          - Data To
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      public static Boolean AlesisDeleteRegistersL007(DateTime ReportDate, DateTime DateFrom, DateTime DateTo)
      {
        String _token;
        String _establecimiento_id;
        String _operador_id;

        _establecimiento_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_ESTABLECIMIENTO_ID);
        _operador_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_OPERADOR_ID);

        try
        {
          if (AlesisGetToken(_operador_id, _establecimiento_id, out _token))
          {
            if (AlesisWSDeleteRegistersL007(_token, _operador_id, _establecimiento_id, ReportDate, DateFrom, DateTo))
            {
              return true;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Error("Error in AlesisWSDeleteRegisters(DateTime DateFrom, DateTime DateTo)");
          Log.Exception(_ex);
        }

        return false;

      } //AlesisDeleteRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Communicate with the webservice to delete registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Token
      //          - OperadorID
      //          - EstablecimientoID
      //          - ReportDate
      //          - DateFrom
      //          - DateTo
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean AlesisWSDeleteRegistersL007(String Token, String OperadorID, String EstablecimientoID, DateTime ReportDate, DateTime DateFrom, DateTime DateTo)
      {
        String _request;
        String _response;
        WS_Alesis.WebServiceLineasdeNegocio7 _ws_alesis;
        XmlDocument _alesis_delete_service_result;
        Int32 _nbr_errores;
        Int32 _registros_afectados;
        XmlNode _node;

        try
        {
          if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_LAST_DAY_AS_NEXT_MONTH, false))
          {
            ReportDate = ReportDate.AddDays(1);
          }

          _request = GenerateDeleteRegistersXML(Token, OperadorID, EstablecimientoID, ReportDate, DateFrom, DateTo);

          if (HasToLogXMLMessagesOnDeleteRegisters())
          {
            //Log message
            Log.Message(_request);
          }

          _ws_alesis = new WS_Alesis.WebServiceLineasdeNegocio7();
          _ws_alesis.Url = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS1);

          _response = _ws_alesis.PkstL007Delete(_request);

          if (HasToLogXMLMessagesOnDeleteRegisters())
          {
            //Log answer
            Log.Message(_response + " " + _request);
          }

          _alesis_delete_service_result = new XmlDocument();

          _alesis_delete_service_result.LoadXml(_response);

          // Remove XML declaration
          if (_alesis_delete_service_result.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
          {
            _alesis_delete_service_result.RemoveChild(_alesis_delete_service_result.FirstChild);
          }

          _node = _alesis_delete_service_result.GetElementsByTagName("ColeccionRespuesta").Item(0);

          _nbr_errores = Int32.Parse(XML.GetAttributeValue(_node, "NoErrores"));
          _registros_afectados = Int32.Parse(XML.GetAttributeValue(_node, "RegistrosAfectados"));

          if (_nbr_errores == 0)
          {
            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      } //AlesisWSDeleteRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Verify if response token is OK
      //
      //  PARAMS:
      //      - INPUT:
      //          - ResponseXML
      //          - NumRegisters
      //      - OUTPUT:
      //
      // RETURNS:
      //      - True or False
      //   NOTES:
      //
      private static Boolean ProcessResponse(String ResponseXML, Int32 NumRegisters)
      {
        XmlDocument _doc;
        XmlNodeList _childs;
        XmlNode _node;
        XmlAttributeCollection _att_collection;

        String _no_errors;
        String _affected_registers;

        try
        {
          _doc = new XmlDocument();
          _doc.LoadXml(ResponseXML);

          _childs = _doc.ChildNodes;
          _node = _childs.Item(1);
          _att_collection = _node.Attributes;

          if (_att_collection["TipoOperacion"].Value.ToLower() != "error")
          {
            if (_node.ChildNodes.Count > 0)
            {
              if (_node.ChildNodes[0].ChildNodes.Count > 0 && _node.ChildNodes[0].ChildNodes[0].Attributes.Count > 0)
              {
                _no_errors = _node.ChildNodes[0].Attributes[0].Value;
                _affected_registers = _node.ChildNodes[0].Attributes[1].Value;

                //Log.Message("IsResponseOK NoErrors[" + _no_errors + "]");
                //Log.Message("IsResponseOK Affected Registers[" + _affected_registers + "]");

                if ((Convert.ToInt32(_no_errors) == 0) && ((Convert.ToInt32(_affected_registers) == NumRegisters)))
                {
                  return true;
                }
              }
            }
          }
          else
          {
            _node = _node.ChildNodes[1];
            Log.Error(_node.InnerXml);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        Log.Error("IsResponseOK not obtained [" + ResponseXML + "]");

        return false;
      }

      //------------------------------------------------------------------------------
      // PURPOSE: Get token
      //
      //  PARAMS:
      //      - INPUT:
      //          - OperadorId
      //          - EstablecimientoId
      //      - OUTPUT:
      //          - Token
      // RETURNS:
      //
      //   NOTES:
      //
      internal static Boolean AlesisGetToken(String OperadorID, String EstablecimientoID, out String Token)
      {
        WS_Alesis.WebServiceLineasdeNegocio7 _ws_alesis;
        StringBuilder _sb;
        String _tmp;
        String _password_Alesis;
        String _url_Alesis;
        String _request;
        String _token_response;

        Token = String.Empty;

        _password_Alesis = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_PASSWORD);
        _url_Alesis = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS1);

        try
        {
          _tmp = String.Format("<Datos OperID=\"{0}\" SalaID=\"{1}\" Linea=\"{2}\" Password=\"{3}\" />", OperadorID, EstablecimientoID, "L007", _password_Alesis);
          _sb = new StringBuilder();
          _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
          _sb.AppendLine("<Genera_Token>");
          _sb.AppendLine(_tmp);
          _sb.AppendLine("</Genera_Token>");

          _ws_alesis = new WS_Alesis.WebServiceLineasdeNegocio7();
          _ws_alesis.Url = _url_Alesis;
          _request = _sb.ToString();
          _token_response = _ws_alesis.PkstCreateToken(_request);

          if (GetResponseToken(_token_response, out Token))
          {
            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      } //AlesisGetToken

      //------------------------------------------------------------------------------
      // PURPOSE: Get response token
      //
      //  PARAMS:
      //      - INPUT:
      //          - ResponseXML
      //      - OUTPUT:
      //          - Token
      //
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean GetResponseToken(String ResponseXML, out String Token)
      {
        XmlDocument _doc;
        XmlNodeList _childs;
        XmlNode _node;
        XmlAttributeCollection _att_collection;

        Token = String.Empty;

        try
        {
          _doc = new XmlDocument();
          _doc.LoadXml(ResponseXML);

          _childs = _doc.ChildNodes;
          _node = _childs.Item(1);
          _att_collection = _node.Attributes;

          if (_att_collection["TipoOperacion"].Value.ToLower() != "error")
          {
            if (_node.ChildNodes.Count > 0)
            {
              if (_node.ChildNodes[0].ChildNodes.Count > 0 && _node.ChildNodes[0].ChildNodes[0].Attributes.Count > 0)
              {
                Token = _node.ChildNodes[0].ChildNodes[0].Attributes[0].Value;

                return true;
              }
            }
          }
          else
          {
            _node = _node.ChildNodes[1];
            Log.Error(_node.InnerXml);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        Log.Error("GetResponseToken Token not obtained [" + ResponseXML + "]");

        return false;
      } //GetResponseToken
    } // ReportesDiariosAlesis

    public static class ConstanciasDiariasAlesis
    {
      /// <summary>
      /// Returns if the Mask indicates that we have to log the XML messages when we are adding registers to Alesis
      /// </summary>
      /// <returns></returns>
      private static Boolean HasToLogXMLMessagesOnAddRegisters()
      {
        Int32 _print_msg_alesis;

        _print_msg_alesis = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_ALESIS_INSERT_MESSAGE, 0);

        return ((_print_msg_alesis & (Int32)MASK_LOG_MSG_ALESIS.CONSTANCIAS) != 0);
      }

      /// <summary>
      /// Returns if the Mask indicates that we have to log the XML messages when we are deleting registers from Alesis
      /// </summary>
      /// <returns></returns>
      private static Boolean HasToLogXMLMessagesOnDeleteRegisters()
      {
        Int32 _print_delete_msg_alesis;

        _print_delete_msg_alesis = GeneralParam.GetInt32(PSACommon.GP_PSA_GROUP, PSACommon.GP_ALESIS_DELETE_MESSAGE, 0);

        return ((_print_delete_msg_alesis & (Int32)MASK_LOG_MSG_ALESIS.CONSTANCIAS) != 0);
      }

      //------------------------------------------------------------------------------
      // PURPOSE: Send registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - List<PSASendObject>
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      public static Boolean AlesisSendRegisters(DateTime ReportDate, List<Registro_L007> Registros)
      {
        String _token;
        String _establecimiento_id;
        String _operador_id;

        try
        {
          if (Registros.Count == 0)
          {
            return true;
          }

          _establecimiento_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_ESTABLECIMIENTO_ID);
          _operador_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_OPERADOR_ID);

          if (AlesisGetToken(out _token, _operador_id, _establecimiento_id))
          {
            if (AlesisWSAddRegisters(_token, _operador_id, _establecimiento_id, ReportDate, Registros))
            {
              return true;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Error("Error in AlesisSendRegisters(List<Registro_Linea_Constancia> Constancias)");
          Log.Exception(_ex);
        }

        return false;

      } //AlesisSendRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Communicate with the webservice to Add registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Row: Register to add
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean AlesisWSAddRegisters(String Token, String OperadorID, String EstablecimientoID, DateTime ReportDate, List<Registro_L007> Registros)
      {
        String _tmp;
        String _fecha_constancia;
        String _request_add;
        String _response_add;
        StringBuilder _sb;
        WS_AlesisConstancias.WebServiceConstancias _ws_alesis_constancias;

        _sb = new StringBuilder();
        _tmp = String.Empty;

        try
        {
          if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_LAST_DAY_AS_NEXT_MONTH, false))
          {
            ReportDate = ReportDate.AddDays(1);
          }

          _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
          _sb.AppendLine("<ConstanciaRetencion>");
          _tmp = String.Format("<Token SalaID=\"{0}\" OperID=\"{1}\" ID=\"{2}\" />", EstablecimientoID, OperadorID, Token);
          _sb.AppendLine(_tmp);

          _tmp = String.Format("<Periodo Mes=\"{0}\" A�o=\"{1}\" />", ReportesDiariosAlesis.GetDateTimeToSendToAlesis(ReportDate).Month.ToString().PadLeft(2, '0')
                                                                    , ReportesDiariosAlesis.GetDateTimeToSendToAlesis(ReportDate).Year);

          _sb.AppendLine(_tmp);

          foreach (Registro_L007 _registro in Registros)
          {
            _tmp = "<Jugador";

            _tmp += String.Format(" rfc=\"{0}\"", _registro.Constancia.Jugador.General.rfc);
            _tmp += String.Format(" CURP=\"{0}\"", _registro.Constancia.Jugador.General.CURP);
            _tmp += String.Format(" NombreJugador=\"{0}\"", GetValue(_registro.Constancia.Jugador.General.NombreJugador, 100, true));
            _tmp += String.Format(" NoMaquina=\"{0}\"", GetValue(_registro.Constancia.General.NoMaquina, 30, true));
            _tmp += String.Format(" DocumentoID=\"{0}\"", _registro.Constancia.Jugador.General.DocumentoID);
            _tmp += String.Format(" NoDocumentoID=\"{0}\"", _registro.Constancia.Jugador.General.NoDocumentoID);
            _tmp += ">";

            _tmp += "<Domicilio";

            if (_registro.Constancia.Jugador.Domicilio == null)
            {
              _registro.Constancia.Jugador.Domicilio = new Registro_Linea_Constancia_Jugador_Domicilio();
            }

            _tmp += String.Format(" calle=\"{0}\"", GetValue(_registro.Constancia.Jugador.Domicilio.calle, 50, true));
            _tmp += String.Format(" noExterior=\"{0}\"", _registro.Constancia.Jugador.Domicilio.noExterior);
            _tmp += String.Format(" noInterior=\"{0}\"", _registro.Constancia.Jugador.Domicilio.noInterior);
            _tmp += String.Format(" colonia=\"{0}\"", _registro.Constancia.Jugador.Domicilio.colonia);
            _tmp += String.Format(" localidad=\"{0}\"", _registro.Constancia.Jugador.Domicilio.localidad);
            _tmp += String.Format(" municipio=\"{0}\"", _registro.Constancia.Jugador.Domicilio.municipio);
            _tmp += String.Format(" estado=\"{0}\"", _registro.Constancia.Jugador.Domicilio.estado);
            _tmp += String.Format(" pais=\"{0}\"", _registro.Constancia.Jugador.Domicilio.pais);
            _tmp += String.Format(" codigoPostal=\"{0}\"", _registro.Constancia.Jugador.Domicilio.codigoPostal);
            _tmp += "/>";

            _fecha_constancia = ReportesDiariosAlesis.GetDateTimeToSendToAlesis(_registro.FechaHora).ToString(HOUR_DATE_FORMAT);

            _tmp += "<Constancia";

            _tmp += String.Format(" IdEvento=\"{0}\"", _registro.IdEvento);
            _tmp += String.Format(" FechaConstancia=\"{0}\"", _fecha_constancia);

            _tmp += String.Format(" MontoPremio=\"{0}\"", _registro.Constancia.General.MontoPremio);
            _tmp += String.Format(" ISRretenido=\"{0}\"", _registro.Constancia.General.ISRretenido);
            _tmp += " ClavelineaNegocio=\"L007\"";
            _tmp += String.Format(" ClavesublineaNegocio=\"{0}\"", _registro.ClavesublineaNegocio);
            _tmp += String.Format(" Combinacion=\"{0}\"", _registro.ClaveCombinacion);
            _tmp += String.Format(" TipoPago=\"{0}\"", _registro.ClaveTipoPago);
            _tmp += "/>";
            _tmp += "</Jugador>";

            _sb.AppendLine(_tmp);
          }

          _sb.AppendLine("</ConstanciaRetencion>");

          _request_add = _sb.ToString();

          if (HasToLogXMLMessagesOnAddRegisters())
          {
            Log.Message(_request_add);
          }

          _ws_alesis_constancias = new WS_AlesisConstancias.WebServiceConstancias();
          _ws_alesis_constancias.Url = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS2);
          _response_add = _ws_alesis_constancias.ConstanciasAdd(_request_add);
          if ((String.IsNullOrEmpty(_response_add)) || IsResponseOK(_response_add, Registros.Count))
          {
            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Message(_sb.ToString());
        }

        return false;
      } //AlesisWSAddRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Return format value
      //
      //  PARAMS:
      //      - INPUT:
      //          - Value
      //          - Length
      //          - Amp
      //
      //      - OUTPUT:
      //
      // RETURNS:
      //      - String
      //   NOTES:
      //
      public static String GetValue(String Value, int Length, Boolean Amp)
      {
        if (String.IsNullOrEmpty(Value))
        {
          return String.Empty;
        }
        else
        {
          Value = Value.Trim().PadRight(Length, ' ').Substring(0, Length).Trim();
          if (Amp)
          {
            Value = Value.Replace("&", "&amp;");
          }
        }

        return Value;
      } // GetValue

      //------------------------------------------------------------------------------
      // PURPOSE: Logic to delete registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Report Date
      //          - Date From
      //          - Data To
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      public static Boolean AlesisDeleteRegisters(DateTime ReportDate, DateTime DateFrom, DateTime DateTo)
      {
        String _token;
        String _establecimiento_id;
        String _operador_id;

        _establecimiento_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_ESTABLECIMIENTO_ID);
        _operador_id = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_OPERADOR_ID);

        try
        {
          if (AlesisGetToken(out _token, _operador_id, _establecimiento_id))
          {
            if (AlesisWSDeleteRegisters(_token, _operador_id, _establecimiento_id, ReportDate, DateFrom, DateTo))
            {
              return true;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Error("Error in AlesisWSDeleteRegisters(DateTime DateFrom, DateTime DateTo)");
          Log.Exception(_ex);
        }

        return false;

      } //AlesisDeleteRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Communicate with the webservice to delete registers
      //
      //  PARAMS:
      //      - INPUT:
      //          - Token
      //          - OperadorID
      //          - EstablecimientoID
      //          - ReportDate
      //          - DateFrom
      //          - DateTo
      //      - OUTPUT:
      //
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean AlesisWSDeleteRegisters(String Token, String OperadorID, String EstablecimientoID, DateTime ReportDate, DateTime DateFrom, DateTime DateTo)
      {
        String _tmp;
        StringBuilder _sb;
        String _request_delete;
        String _response_delete;
        WS_AlesisConstancias.WebServiceConstancias _ws_alesis_constancias;
        XmlDocument _alesis_delete_service_result;
        Int32 _nbr_errores;
        Int32 _registros_afectados;
        XmlNode _node;

        try
        {
          if (GeneralParam.GetBoolean(PSACommon.GP_PSA_GROUP, PSACommon.GP_LAST_DAY_AS_NEXT_MONTH, false))
          {
            ReportDate = ReportDate.AddDays(1);
          }

          _sb = new StringBuilder();
          _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
          _sb.AppendLine("<Datos_Eliminar>");

          _tmp = String.Format("<Token SalaID=\"{0}\" OperID=\"{1}\" ID=\"{2}\" />", EstablecimientoID, OperadorID, Token);
          _sb.AppendLine(_tmp);

          _tmp = String.Format("<Periodo Mes=\"{0}\" A�o=\"{1}\" Fecha_Inicial=\"{2}\" Fecha_Final=\"{3}\" />"
                              , ReportesDiariosAlesis.GetDateTimeToSendToAlesis(ReportDate).Month.ToString()
                              , ReportesDiariosAlesis.GetDateTimeToSendToAlesis(ReportDate).Year.ToString()
                              , ReportesDiariosAlesis.GetDateTimeToSendToAlesis(DateFrom).ToString(HOUR_DATE_FORMAT)
                              , ReportesDiariosAlesis.GetDateTimeToSendToAlesis(DateTo).ToString(HOUR_DATE_FORMAT));
          _sb.AppendLine(_tmp);

          _sb.AppendLine("</Datos_Eliminar>");

          _request_delete = _sb.ToString();

          if (HasToLogXMLMessagesOnDeleteRegisters())
          {
            //Log message to delete
            Log.Message(_request_delete);
          }

          _ws_alesis_constancias = new WS_AlesisConstancias.WebServiceConstancias();
          _ws_alesis_constancias.Url = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS2);

          _response_delete = _ws_alesis_constancias.ConstanciasDelete(_request_delete);

          if (HasToLogXMLMessagesOnDeleteRegisters())
          {
            //Log answer to deletion
            Log.Message(_response_delete + " " + _request_delete);
          }

          _alesis_delete_service_result = new XmlDocument();

          _alesis_delete_service_result.LoadXml(_response_delete);

          // Remove XML declaration
          if (_alesis_delete_service_result.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
          {
            _alesis_delete_service_result.RemoveChild(_alesis_delete_service_result.FirstChild);
          }

          _node = _alesis_delete_service_result.GetElementsByTagName("ColeccionRespuesta").Item(0);

          _nbr_errores = Int32.Parse(XML.GetAttributeValue(_node, "NoErrores"));
          _registros_afectados = Int32.Parse(XML.GetAttributeValue(_node, "RegistrosAfectados"));

          if (_nbr_errores == 0)
          {
            return true;
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      } //AlesisWSDeleteRegisters

      //------------------------------------------------------------------------------
      // PURPOSE: Verify that response token is OK
      //
      //  PARAMS:
      //      - INPUT:
      //          - ResponseXML
      //          - NumRegisters
      //      - OUTPUT:
      //
      // RETURNS:
      //      - True or False
      //   NOTES:
      //
      private static Boolean IsResponseOK(String ResponseXML, Int32 NumRegisters)
      {
        XmlDocument _doc;
        XmlNodeList _childs;
        XmlNode _node;
        XmlAttributeCollection _att_collection;

        String _no_errors;
        String _affected_registers;

        try
        {
          _doc = new XmlDocument();
          _doc.LoadXml(ResponseXML);

          _childs = _doc.ChildNodes;
          _node = _childs.Item(1);
          _att_collection = _node.Attributes;

          if (_att_collection["TipoOperacion"].Value.ToLower() != "error")
          {
            if (_node.ChildNodes.Count > 0)
            {
              if ((_node.ChildNodes[0].ChildNodes.Count > 0) && (_node.ChildNodes[0].ChildNodes[0].Attributes.Count > 0))
              {
                _no_errors = _node.ChildNodes[0].Attributes[0].Value;
                _affected_registers = _node.ChildNodes[0].Attributes[1].Value;

                //Log.Message("IsResponseOK NoErrors[" + _no_errors + "]");
                //Log.Message("IsResponseOK Affected Registers[" + _affected_registers + "]");

                if ((Convert.ToInt32(_no_errors) == 0) && ((Convert.ToInt32(_affected_registers) == NumRegisters)))
                {
                  return true;
                }
              }
            }
          }
          else
          {
            _node = _node.ChildNodes[1];
            Log.Error(_node.InnerXml);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        Log.Error("IsResponseOK not obtained [" + ResponseXML + "]");

        return false;
      } //IsResponseOK

      //------------------------------------------------------------------------------
      // PURPOSE: Get token
      //
      //  PARAMS:
      //      - INPUT:
      //          - OperadorId
      //          - EstablecimientoId
      //      - OUTPUT:
      //          - Token
      // RETURNS:
      //
      //   NOTES:
      //
      internal static Boolean AlesisGetToken(out String Token, String OperadorID, String EstablecimientoID)
      {
        WS_AlesisConstancias.WebServiceConstancias _ws_alesis_constancias;
        StringBuilder _sb;
        String _tmp;
        String _password_Alesis;
        String _url_Alesis;
        String _token_response;

        Token = String.Empty;

        _password_Alesis = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_PASSWORD);
        _url_Alesis = GeneralParam.GetString(PSACommon.GP_PSA_GROUP, PSACommon.GP_SERVER_ADDRESS2);

        try
        {
          _sb = new StringBuilder();
          _sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
          _sb.AppendLine("<Genera_Token>");
          _tmp = String.Format("<Datos OperID=\"{0}\" SalaID=\"{1}\" Linea=\"{2}\" Password=\"{3}\" />", OperadorID, EstablecimientoID, "L007", _password_Alesis);
          _sb.AppendLine(_tmp);
          _sb.AppendLine("</Genera_Token>");

          _ws_alesis_constancias = new WS_AlesisConstancias.WebServiceConstancias();
          _ws_alesis_constancias.Url = _url_Alesis;
          _token_response = _ws_alesis_constancias.PkstCreateToken(_sb.ToString());

          if (GetResponseToken(_token_response, out Token))
          {
            return true;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;

      } //GetToken

      //------------------------------------------------------------------------------
      // PURPOSE: Get response token
      //
      //  PARAMS:
      //      - INPUT:
      //          - ResponseXML
      //      - OUTPUT:
      //          - Token
      //
      // RETURNS:
      //
      //   NOTES:
      //
      private static Boolean GetResponseToken(String ResponseXML, out String Token)
      {
        XmlDocument _doc;
        XmlNodeList _childs;
        XmlNode _node;
        XmlAttributeCollection _att_collection;

        Token = String.Empty;

        try
        {
          _doc = new XmlDocument();
          _doc.LoadXml(ResponseXML);

          _childs = _doc.ChildNodes;
          _node = _childs.Item(1);
          _att_collection = _node.Attributes;

          if (_att_collection["TipoOperacion"].Value.ToLower() != "error")
          {
            if (_node.ChildNodes.Count > 0)
            {
              if (_node.ChildNodes[0].ChildNodes.Count > 0 && _node.ChildNodes[0].ChildNodes[0].Attributes.Count > 0)
              {
                Token = _node.ChildNodes[0].ChildNodes[0].Attributes[0].Value;

                return true;
              }
            }
          }
          else
          {
            _node = _node.ChildNodes[1];
            Log.Error(_node.InnerXml);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        Log.Error("GetResponseToken Token not obtained [" + ResponseXML + "]");

        return false;
      } //GetResponseToken

    } // ConstanciasDiariasAlesis

  } // Alesis_ClientReportesDiarios

} // WSI.PSA_Client