using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace WSI.Codere
{
  public interface ICodere
  {
    Boolean GetBalance(out Int64 BalanceCents);
  }

  public class Codere : ICodere
  {
    private String m_method_url = String.Empty;
    private String m_config = String.Empty;
    private String m_mac_address = String.Empty;

    //------------------------------------------------------------------------------
    // PURPOSE : Retrieves the configuration
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private Boolean Configuration()
    {
      String _phoenix_path;

      try
      {
        // URL 
        m_method_url = Environment.GetEnvironmentVariable("LKT_PHOENIX_URL");
        if (String.IsNullOrEmpty(m_method_url))
        {
          m_method_url = @"http://extranet.codere.com/demo/methods.aspx/GetBalance";
        }

        // MAC Address
        m_mac_address = Environment.GetEnvironmentVariable("LKT_PHOENIX_MAC");
        if (String.IsNullOrEmpty(m_mac_address))
        {
          m_mac_address = "00:00:00:00:00:00";
        }

        // Config file
        _phoenix_path = Environment.GetEnvironmentVariable("LKT_PHOENIX_PATH");
        if (String.IsNullOrEmpty(_phoenix_path))
        {
          _phoenix_path = @"\BGT\config\Phoenix.conf";
        }

        using (FileStream _fs = new FileStream(_phoenix_path, FileMode.Open, FileAccess.Read))
        {
          using (StreamReader _tr = new StreamReader(_fs))
          {
            m_config = _tr.ReadToEnd();
          }
        }

        return true;
      }
      catch
      {
        ;
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get the Balance calling the WebService Method: GetBalance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //        - BalanceCents
    //
    // RETURNS :
    //      - TRUE, on success
    //      - FALSE, otherwise
    //
    //   NOTES :
    //
    public bool GetBalance(out Int64 BalanceCents)
    {
      String _data;
      Byte[] _bin_data;
      byte[] _buffer;
      String _txt;
      HttpWebRequest _request;
      HttpWebResponse _response;
      String _key;
      String _value;
      String[] _kv_split;

      BalanceCents = 0;

      try
      {
        // Read configuration file
        if (!Configuration())
        {
          return false;
        }

        _data = "{'xmlString':'" + m_config + "','macAddress':'" + m_mac_address + "'}";
        _bin_data = Encoding.UTF8.GetBytes(_data);

        _request = (HttpWebRequest)HttpWebRequest.Create(m_method_url);
        _request.Method = WebRequestMethods.Http.Post;
        _request.Accept = "application/json";
        _request.ContentType = "application/json;charset=utf-8";
        _request.ContentLength = _bin_data.Length;
        _request.GetRequestStream().Write(_bin_data, 0, _bin_data.Length);

        _response = (HttpWebResponse)_request.GetResponse();

        _buffer = new Byte[_response.ContentLength];

        _response.GetResponseStream().Read(_buffer, 0, _buffer.Length);

        _txt = Encoding.UTF8.GetString(_buffer);

        //
        // Parse
        // 
        if (_txt.IndexOf("{") >= 0)
        {
          _txt = _txt.Substring(_txt.IndexOf("{") + 1);
        }
        if (_txt.LastIndexOf("}") >= 0)
        {
          _txt = _txt.Substring(0, _txt.LastIndexOf("}"));
        }
        _txt = _txt.Trim();

        _kv_split = _txt.Split(new String[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
        if (_kv_split.Length < 2)
        {
          return false;
        }

        _key = _kv_split[0];
        _key = _key.Replace("\"", "");
        _key = _key.Replace("\'", "");
        _key = _key.Trim();

        if (_key == "d")
        {
          _value = _kv_split[1];
          _value = _value.Replace("\"", "");
          _value = _value.Replace("\'", "");
          _value = _value.Replace(",", "");
          _value = _value.Replace(".", "");
          _value = _value.Trim();

          if (Int64.TryParse(_value, out BalanceCents))
          {
            return true;
          }
        }
      }
      catch
      {
        ;
      }

      return false;
    } // GetBalance
  }
}
