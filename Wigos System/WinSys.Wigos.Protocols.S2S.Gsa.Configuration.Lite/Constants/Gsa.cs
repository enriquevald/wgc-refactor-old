﻿namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.Lite.Constants
{
	// TODO: Refactor. Basic replica from WinSys.Wigos.Gsa.S2S.Configuration but in .NET 2.0 to integrate with Wigos
	
	public static class Gsa
	{
		public const string GroupKey = "GSA";

		public const string ConfigurationIdAutoIncreaseable = "S2S.ConfigurationIdAutoIncreaseable";
		public const bool ConfigurationIdAutoIncreaseableDefault = false;
	}
}