﻿using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Configuration.Lite.GeneralParams
{
	// TODO: Refactor. Basic replica from WinSys.Wigos.Gsa.S2S.Configuration but in .NET 2.0 to integrate with Wigos

	public static class Gsa
	{
		public static bool IsConfigurationIdAutoIncreaseable
		{
			get
			{
				return GeneralParam.GetBoolean(
					Constants.Gsa.GroupKey,
					Constants.Gsa.ConfigurationIdAutoIncreaseable,
					Constants.Gsa.ConfigurationIdAutoIncreaseableDefault);
			}
		}
	}
}