﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wigos.GUI.Adapter.Common.Base
{
    public interface ITranslator<TSource, TDest>
    {
      TDest Translate(TSource source);
    }
}
