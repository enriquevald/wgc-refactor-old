namespace IDCardScanner
{
  partial class form_id_card_scanner
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.btn_scan = new System.Windows.Forms.Button();
      this.tmr_time = new System.Windows.Forms.Timer(this.components);
      this.m_img_photo = new System.Windows.Forms.PictureBox();
      this.m_img_x = new System.Windows.Forms.PictureBox();
      this.m_img_back = new System.Windows.Forms.PictureBox();
      this.txt_all_fields = new System.Windows.Forms.TextBox();
      this.m_img_front = new System.Windows.Forms.PictureBox();
      this.chk_auto_detect = new System.Windows.Forms.CheckBox();
      this.cmb_region = new System.Windows.Forms.ComboBox();
      this.chk_regions = new System.Windows.Forms.CheckedListBox();
      this.btn_move_up = new System.Windows.Forms.Button();
      this.btn_move_down = new System.Windows.Forms.Button();
      this.cmb_country = new System.Windows.Forms.ComboBox();
      this.cmb_state = new System.Windows.Forms.ComboBox();
      this.gb_state = new System.Windows.Forms.GroupBox();
      this.lbl_state = new System.Windows.Forms.Label();
      this.lbl_country = new System.Windows.Forms.Label();
      this.lbl_region = new System.Windows.Forms.Label();
      this.gb_regions = new System.Windows.Forms.GroupBox();
      this.btn_process = new System.Windows.Forms.Button();
      this.gb_photo = new System.Windows.Forms.GroupBox();
      this.gp_front_side = new System.Windows.Forms.GroupBox();
      this.gp_back_side = new System.Windows.Forms.GroupBox();
      this.gb_data = new System.Windows.Forms.GroupBox();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_x)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_back)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_front)).BeginInit();
      this.gb_state.SuspendLayout();
      this.gb_regions.SuspendLayout();
      this.gb_photo.SuspendLayout();
      this.gp_front_side.SuspendLayout();
      this.gp_back_side.SuspendLayout();
      this.gb_data.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_scan
      // 
      this.btn_scan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.btn_scan.Location = new System.Drawing.Point(0, 127);
      this.btn_scan.Name = "btn_scan";
      this.btn_scan.Size = new System.Drawing.Size(200, 48);
      this.btn_scan.TabIndex = 0;
      this.btn_scan.Text = "Scan";
      this.btn_scan.UseVisualStyleBackColor = true;
      this.btn_scan.Click += new System.EventHandler(this.btn_scan_Click);
      // 
      // tmr_time
      // 
      this.tmr_time.Enabled = true;
      this.tmr_time.Tick += new System.EventHandler(this.tmr_time_Tick);
      // 
      // m_img_photo
      // 
      this.m_img_photo.Location = new System.Drawing.Point(5, 14);
      this.m_img_photo.Name = "m_img_photo";
      this.m_img_photo.Size = new System.Drawing.Size(80, 100);
      this.m_img_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.m_img_photo.TabIndex = 16;
      this.m_img_photo.TabStop = false;
      // 
      // m_img_x
      // 
      this.m_img_x.Location = new System.Drawing.Point(0, 0);
      this.m_img_x.MaximumSize = new System.Drawing.Size(200, 125);
      this.m_img_x.Name = "m_img_x";
      this.m_img_x.Size = new System.Drawing.Size(200, 125);
      this.m_img_x.TabIndex = 17;
      this.m_img_x.TabStop = false;
      this.m_img_x.Click += new System.EventHandler(this.m_img_x_Click);
      this.m_img_x.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
      // 
      // m_img_back
      // 
      this.m_img_back.Location = new System.Drawing.Point(8, 14);
      this.m_img_back.Name = "m_img_back";
      this.m_img_back.Size = new System.Drawing.Size(200, 125);
      this.m_img_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.m_img_back.TabIndex = 19;
      this.m_img_back.TabStop = false;
      this.m_img_back.DoubleClick += new System.EventHandler(this.m_img_back_DoubleClick);
      // 
      // txt_all_fields
      // 
      this.txt_all_fields.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_all_fields.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_all_fields.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_all_fields.Location = new System.Drawing.Point(3, 16);
      this.txt_all_fields.Multiline = true;
      this.txt_all_fields.Name = "txt_all_fields";
      this.txt_all_fields.Size = new System.Drawing.Size(424, 271);
      this.txt_all_fields.TabIndex = 20;
      // 
      // m_img_front
      // 
      this.m_img_front.Location = new System.Drawing.Point(8, 14);
      this.m_img_front.MaximumSize = new System.Drawing.Size(200, 125);
      this.m_img_front.Name = "m_img_front";
      this.m_img_front.Size = new System.Drawing.Size(200, 125);
      this.m_img_front.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.m_img_front.TabIndex = 21;
      this.m_img_front.TabStop = false;
      this.m_img_front.DoubleClick += new System.EventHandler(this.m_img_front_DoubleClick);
      // 
      // chk_auto_detect
      // 
      this.chk_auto_detect.AutoSize = true;
      this.chk_auto_detect.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.chk_auto_detect.Location = new System.Drawing.Point(216, 4);
      this.chk_auto_detect.Name = "chk_auto_detect";
      this.chk_auto_detect.Size = new System.Drawing.Size(113, 21);
      this.chk_auto_detect.TabIndex = 22;
      this.chk_auto_detect.Text = "Auto-Detect";
      this.chk_auto_detect.UseVisualStyleBackColor = true;
      this.chk_auto_detect.CheckedChanged += new System.EventHandler(this.chk_auto_detect_CheckedChanged);
      // 
      // cmb_region
      // 
      this.cmb_region.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_region.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.cmb_region.FormattingEnabled = true;
      this.cmb_region.Location = new System.Drawing.Point(73, 20);
      this.cmb_region.Name = "cmb_region";
      this.cmb_region.Size = new System.Drawing.Size(203, 32);
      this.cmb_region.TabIndex = 24;
      this.cmb_region.SelectedIndexChanged += new System.EventHandler(this.cmb_region_SelectedIndexChanged);
      // 
      // chk_regions
      // 
      this.chk_regions.BackColor = System.Drawing.SystemColors.Control;
      this.chk_regions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.chk_regions.FormattingEnabled = true;
      this.chk_regions.Location = new System.Drawing.Point(6, 13);
      this.chk_regions.Name = "chk_regions";
      this.chk_regions.Size = new System.Drawing.Size(188, 130);
      this.chk_regions.TabIndex = 26;
      this.chk_regions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chk_regions_ItemCheck);
      // 
      // btn_move_up
      // 
      this.btn_move_up.Location = new System.Drawing.Point(200, 89);
      this.btn_move_up.Name = "btn_move_up";
      this.btn_move_up.Size = new System.Drawing.Size(75, 25);
      this.btn_move_up.TabIndex = 27;
      this.btn_move_up.Text = "Move Up";
      this.btn_move_up.UseVisualStyleBackColor = true;
      this.btn_move_up.Click += new System.EventHandler(this.btn_move_up_Click);
      // 
      // btn_move_down
      // 
      this.btn_move_down.Location = new System.Drawing.Point(200, 118);
      this.btn_move_down.Name = "btn_move_down";
      this.btn_move_down.Size = new System.Drawing.Size(75, 25);
      this.btn_move_down.TabIndex = 28;
      this.btn_move_down.Text = "Move Down";
      this.btn_move_down.UseVisualStyleBackColor = true;
      this.btn_move_down.Click += new System.EventHandler(this.btn_move_down_Click);
      // 
      // cmb_country
      // 
      this.cmb_country.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_country.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.cmb_country.FormattingEnabled = true;
      this.cmb_country.Location = new System.Drawing.Point(73, 61);
      this.cmb_country.Name = "cmb_country";
      this.cmb_country.Size = new System.Drawing.Size(203, 32);
      this.cmb_country.TabIndex = 29;
      this.cmb_country.SelectedIndexChanged += new System.EventHandler(this.cmb_country_SelectedIndexChanged);
      // 
      // cmb_state
      // 
      this.cmb_state.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmb_state.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
      this.cmb_state.FormattingEnabled = true;
      this.cmb_state.Location = new System.Drawing.Point(73, 104);
      this.cmb_state.Name = "cmb_state";
      this.cmb_state.Size = new System.Drawing.Size(203, 32);
      this.cmb_state.TabIndex = 30;
      this.cmb_state.SelectedIndexChanged += new System.EventHandler(this.cmb_state_SelectedIndexChanged);
      // 
      // gb_state
      // 
      this.gb_state.Controls.Add(this.lbl_state);
      this.gb_state.Controls.Add(this.lbl_country);
      this.gb_state.Controls.Add(this.lbl_region);
      this.gb_state.Controls.Add(this.cmb_region);
      this.gb_state.Controls.Add(this.cmb_state);
      this.gb_state.Controls.Add(this.cmb_country);
      this.gb_state.Location = new System.Drawing.Point(208, 24);
      this.gb_state.Name = "gb_state";
      this.gb_state.Size = new System.Drawing.Size(282, 151);
      this.gb_state.TabIndex = 31;
      this.gb_state.TabStop = false;
      // 
      // lbl_state
      // 
      this.lbl_state.AutoSize = true;
      this.lbl_state.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.lbl_state.Location = new System.Drawing.Point(6, 112);
      this.lbl_state.Name = "lbl_state";
      this.lbl_state.Size = new System.Drawing.Size(46, 17);
      this.lbl_state.TabIndex = 33;
      this.lbl_state.Text = "State";
      // 
      // lbl_country
      // 
      this.lbl_country.AutoSize = true;
      this.lbl_country.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.lbl_country.Location = new System.Drawing.Point(6, 69);
      this.lbl_country.Name = "lbl_country";
      this.lbl_country.Size = new System.Drawing.Size(64, 17);
      this.lbl_country.TabIndex = 32;
      this.lbl_country.Text = "Country";
      // 
      // lbl_region
      // 
      this.lbl_region.AutoSize = true;
      this.lbl_region.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
      this.lbl_region.Location = new System.Drawing.Point(6, 28);
      this.lbl_region.Name = "lbl_region";
      this.lbl_region.Size = new System.Drawing.Size(59, 17);
      this.lbl_region.TabIndex = 31;
      this.lbl_region.Text = "Region";
      // 
      // gb_regions
      // 
      this.gb_regions.Controls.Add(this.chk_regions);
      this.gb_regions.Controls.Add(this.btn_move_up);
      this.gb_regions.Controls.Add(this.btn_move_down);
      this.gb_regions.Location = new System.Drawing.Point(208, 24);
      this.gb_regions.Name = "gb_regions";
      this.gb_regions.Size = new System.Drawing.Size(282, 151);
      this.gb_regions.TabIndex = 32;
      this.gb_regions.TabStop = false;
      // 
      // btn_process
      // 
      this.btn_process.Location = new System.Drawing.Point(125, 131);
      this.btn_process.Name = "btn_process";
      this.btn_process.Size = new System.Drawing.Size(75, 25);
      this.btn_process.TabIndex = 33;
      this.btn_process.Text = "Process";
      this.btn_process.UseVisualStyleBackColor = true;
      this.btn_process.Visible = false;
      this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
      // 
      // gb_photo
      // 
      this.gb_photo.Controls.Add(this.m_img_photo);
      this.gb_photo.Location = new System.Drawing.Point(8, 176);
      this.gb_photo.Name = "gb_photo";
      this.gb_photo.Size = new System.Drawing.Size(90, 120);
      this.gb_photo.TabIndex = 34;
      this.gb_photo.TabStop = false;
      this.gb_photo.Text = "Photo";
      // 
      // gp_front_side
      // 
      this.gp_front_side.Controls.Add(this.m_img_front);
      this.gp_front_side.Location = new System.Drawing.Point(539, 176);
      this.gp_front_side.Name = "gp_front_side";
      this.gp_front_side.Size = new System.Drawing.Size(215, 145);
      this.gp_front_side.TabIndex = 35;
      this.gp_front_side.TabStop = false;
      this.gp_front_side.Text = "Front side";
      // 
      // gp_back_side
      // 
      this.gp_back_side.Controls.Add(this.m_img_back);
      this.gp_back_side.Location = new System.Drawing.Point(539, 320);
      this.gp_back_side.Name = "gp_back_side";
      this.gp_back_side.Size = new System.Drawing.Size(215, 145);
      this.gp_back_side.TabIndex = 36;
      this.gp_back_side.TabStop = false;
      this.gp_back_side.Text = "Back side";
      // 
      // gb_data
      // 
      this.gb_data.Controls.Add(this.txt_all_fields);
      this.gb_data.Location = new System.Drawing.Point(103, 176);
      this.gb_data.Name = "gb_data";
      this.gb_data.Size = new System.Drawing.Size(430, 290);
      this.gb_data.TabIndex = 37;
      this.gb_data.TabStop = false;
      this.gb_data.Text = "Data";
      // 
      // form_id_card_scanner
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(494, 176);
      this.Controls.Add(this.btn_scan);
      this.Controls.Add(this.gb_photo);
      this.Controls.Add(this.gb_data);
      this.Controls.Add(this.gp_back_side);
      this.Controls.Add(this.gp_front_side);
      this.Controls.Add(this.btn_process);
      this.Controls.Add(this.m_img_x);
      this.Controls.Add(this.chk_auto_detect);
      this.Controls.Add(this.gb_regions);
      this.Controls.Add(this.gb_state);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "form_id_card_scanner";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "  ID Card Scanner";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_id_scan_FormClosing);
      ((System.ComponentModel.ISupportInitialize)(this.m_img_photo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_x)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_back)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.m_img_front)).EndInit();
      this.gb_state.ResumeLayout(false);
      this.gb_state.PerformLayout();
      this.gb_regions.ResumeLayout(false);
      this.gb_photo.ResumeLayout(false);
      this.gp_front_side.ResumeLayout(false);
      this.gp_back_side.ResumeLayout(false);
      this.gb_data.ResumeLayout(false);
      this.gb_data.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_scan;
    private System.Windows.Forms.Timer tmr_time;
    private System.Windows.Forms.PictureBox m_img_photo;
    private System.Windows.Forms.PictureBox m_img_x;
    private System.Windows.Forms.PictureBox m_img_back;
    private System.Windows.Forms.TextBox txt_all_fields;
    private System.Windows.Forms.PictureBox m_img_front;
    private System.Windows.Forms.CheckBox chk_auto_detect;
    private System.Windows.Forms.ComboBox cmb_region;
    private System.Windows.Forms.CheckedListBox chk_regions;
    private System.Windows.Forms.Button btn_move_up;
    private System.Windows.Forms.Button btn_move_down;
    private System.Windows.Forms.ComboBox cmb_country;
    private System.Windows.Forms.ComboBox cmb_state;
    private System.Windows.Forms.GroupBox gb_state;
    private System.Windows.Forms.Label lbl_state;
    private System.Windows.Forms.Label lbl_country;
    private System.Windows.Forms.Label lbl_region;
    private System.Windows.Forms.GroupBox gb_regions;
    private System.Windows.Forms.Button btn_process;
    private System.Windows.Forms.GroupBox gb_photo;
    private System.Windows.Forms.GroupBox gp_front_side;
    private System.Windows.Forms.GroupBox gp_back_side;
    private System.Windows.Forms.GroupBox gb_data;
  }
}