using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

using System.Runtime.InteropServices;
using X = SCANWLib;
using Y = SCANWLib;
using Z = SCANWEXLib;
using System.Reflection;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;

namespace IDCardScanner
{


  public class ScannedPersonalInfo
  {
    public String InternalID;
    public String DocumentType;
    public String DocumentState;
    public String DocumentID;
    public DateTime DocumentExpirationDate = DateTime.MinValue;
    public DateTime DocumentIssueDate = DateTime.MinValue;
    public String CountryISO2;
    public String CountryISO3;
    public String CountryName;

    public String Name1;
    public String Name2;
    public String Name3;
    public String Name4;
    public DateTime BirthDate = DateTime.MinValue;
    public String Gender;

    public String FatherName;
    public String MotherName;

    public String Nationality;


    public String[] Address = new String[6];
    public String City;
    public String ZipCode;

    public String photo_filename; // Photo
    public String front_filename; // Anverso
    public String back_filename; // Reverso

    public Image image_x;
    public Image image_front;
    public Image image_back;
    public Image image_photo;

    public ScannedPersonalInfo()
    {
      DocumentID = null;
    }
  }

  public delegate void ScannedPersonalInfoHandler(ScannedPersonalInfoEvent Event);

  public class ScannedPersonalInfoEvent
  {
    public Boolean running;
    public int running_status;
    public string running_description;

    public Boolean error;
    public String err_msg;
    public ScannedPersonalInfo info;
  }

  public class Snapshell2
  {
    #region constants
    //private const string DEFAULT_LICENSE = "JMDJYKFEBMDH5UM4";
    private static String LICENSE_SNAPSHELL;
    private static String LICENSE_SDK;
    private static String m_user_country_iso2 = null;
    private static String m_user_config = null;
    private static SaveConfiguration m_save_handler = null;
    private static Boolean m_start = false;
    private static ManualResetEvent m_ev_start = new ManualResetEvent(false);
    

    [DllImport("IdCard.dll")]
    public static extern short SetRegion(int Region);

    //[DllImport("IdCard.dll")]
    //public static extern short SetRegionDetectionSequence(int region0, int region1, int region2, int region3, int region4, int region5, int region6);

    [DllImport("IdCard.dll")]
    public static extern void SetDatesFormat(short val);

    //[DllImport("IdCard.dll")]
    //public static extern short GetField(short filedIndex, char* szBuf);
    [DllImport("SLib.dll")]
    public static extern short SetScannerResolution(short resolution);


    #endregion

    #region enums

    enum REGIONS
    {
      USA = 0,
      CANADA = 1,
      SOUTH_AMERICA = 2,
      EUROPE = 3,
      AUSTRALIA = 4,
      ASIA = 5,
      AFRICA = 7
    };

    enum COUNTRIES
    {
      FRANCE = 90,
      GERMANY = 144,
      SPAIN = 150
    };

    #endregion

    #region private members

    private DataSet m_dataset = null;

    private Image m_image_x = null;
    private String m_image_x_path = null;


    private bool m_initialized = false;
    private static Snapshell2 m_self_pointer = null;


    private X.SLibExClass m_scanner = null;
    private Y.IdDataClass m_id_data = null;

    private Z.IdDataClass m_ex_id_data = null;
    private Z.SLibExClass m_ex_scanner = null;

    private int m_state_id = 150;

    private String m_tmp_folder;
    private String m_tmp_prefix;
    private String m_tmp_pattern;

    private Control m_control = null;
    private ScannedPersonalInfoHandler m_handler = null;
    private ScanProcessStatusHandler m_status_handler = null;

    private AutoResetEvent m_scan_requested = new AutoResetEvent(false);
    private AutoResetEvent m_process_requested = new AutoResetEvent(false);
    private AutoResetEvent m_settings_requested = new AutoResetEvent(false);
   
    private static Boolean m_is_scanning = false;
  
    private enum WAIT_STATUS
    {
      WAIT_TIMEOUT = 0,
      SCAN_REQUESTED = 1,
      PROCESS_REQUESTED = 2,
      SETTINGS_REQUESTED = 3,
      ERROR = 100,
    }


    static private void IDCardScannerBackgroundThread()
    {
      String _err;
      Snapshell2 _class;

      _class = new Snapshell2();
      _class.Init(out _err);
      _class.Loop();

    }

    public delegate Boolean SaveConfiguration(String ConfigString);

    static public void Init(String LicenseSnapshell, String LicenseSDK, String CountryIso2, String Config, SaveConfiguration SaveConfigHandler)
    {
      Thread _thread;

      if (!String.IsNullOrEmpty(LicenseSnapshell))
      {
        LICENSE_SNAPSHELL = LicenseSnapshell;
      }

      LICENSE_SDK = LICENSE_SNAPSHELL;
      if (!String.IsNullOrEmpty(LicenseSDK))
      {
        LICENSE_SDK = LicenseSDK;
      }
      if (!String.IsNullOrEmpty(CountryIso2))
      {
        m_user_country_iso2 = CountryIso2;
      }
      if (!String.IsNullOrEmpty(Config))
      {
        m_user_config = Config;
      }
      if (SaveConfigHandler != null)
      {
        m_save_handler = SaveConfigHandler;
      }
      _thread = new Thread(IDCardScannerBackgroundThread);
      _thread.SetApartmentState(ApartmentState.STA);
      _thread.Start();

      _thread = new Thread(IDCardScannerForegroundThread);
      _thread.SetApartmentState(ApartmentState.STA);
      _thread.Start();
    }

    static private void IDCardScannerForegroundThread()
    {
      Application.Run(new form_id_card_scanner());
    }

    public static DataSet DataSet()
    {
      if (m_self_pointer != null)
      {
        if (m_self_pointer.m_dataset != null)
        {
          return m_self_pointer.m_dataset;
        }
      }
      return null;
    }

    public delegate void InvokeDelegate();


    public enum SLIB_ERR
    {
      SLIB_ERR_NONE = 1,
      SLIB_ERR_INVALID_SCANNER = -1,
      SLIB_ERR_SCANNER_GENERAL_FAIL = -2,
      SLIB_ERR_CANCELED_BY_USER = -3,
      SLIB_ERR_SCANNER_NOT_FOUND = -4,
      SLIB_ERR_HARDWARE_ERROR = -5,
      SLIB_ERR_PAPER_FED_ERROR = -6,
      SLIB_ERR_SCANABORT = -7,
      SLIB_ERR_NO_PAPER = -8,
      SLIB_ERR_PAPER_JAM = -9,
      SLIB_ERR_FILE_IO_ERROR = -10,
      SLIB_ERR_PRINTER_PORT_USED = -11,
      SLIB_ERR_OUT_OF_MEMORY = -12,
      SLIB_ERR_BAD_WIDTH_PARAM = -2,
      SLIB_ERR_BAD_HEIGHT_PARAM = -3,
      SLIB_ERR_BAD_PARAM = -2,
      SLIB_LIBRARY_ALREADY_INITIALIZED = -13,
      SLIB_ERR_DRIVER_NOT_FOUND = -14,
      SLIB_ERR_SCANNER_BUSSY = -15,
      SLIB_ERR_IMAGE_CONVERSION = -16,
      SLIB_UNLOAD_FAILED_BAD_PARENT = -17,
      SLIB_NOT_INITILIZED = -18,
      SLIB_LIBRARY_ALREADY_USED_BY_OTHER_APP = -19,
      SLIB_CONFLICT_WITH_INOUTSCAN_PARAM = -20,
      SLIB_CONFLICT_WITH_SCAN_SIZE_PARAM = -21,
      SLIB_NO_SUPPORT_MULTIPLE_DEVICES = -22,
      SLIB_ERR_CAM_ALREADY_ASSIGNED = -23,
      SLIB_ERR_NO_FREE_CAM_FOUND = -24,
      SLIB_ERR_CAM_NOT_FOUND = -25,
      SLIB_ERR_CAM_NOT_ASSIGNED_TO_THIS_APP = -26,
      GENERAL_ERR_PLUG_NOT_FOUND = -200,
      ERR_SCANNER_ALREADY_IN_USE = -201,
      SLIB_ERR_SCANNER_ALREADY_IN_USE = -202,
      SLIB_ERR_CANNOT_OPEN_TWAIN_SOURCE = -203,
      SLIB_ERR_NO_TWAIN_INSTALLED = -204,
      SLIB_ERR_NO_NEXT_VALUE = -205
    };

    public enum ID_ERR
    {
      ID_ERR_NONE = 1,
      ID_ERR_STATE_NOT_SUPORTED = -2,
      ID_ERR_BAD_PARAM = -3,
      ID_ERR_NO_MATCH = -4,
      ID_ERR_FILE_OPEN = -5,
      ID_BAD_DESTINATION_FILE = -6,
      ID_ERR_FEATURE_NOT_SUPPORTED = -7,
      ID_ERR_COUNTRY_NOT_INIT = -8,
      ID_ERR_NO_NEXT_COUNTRY = -9,
      ID_ERR_STATE_NOT_INIT = -10,
      ID_ERR_NO_NEXT_STATE = -11,
      ID_ERR_CANNOT_DELETE_DESTINATION_IMAGE = -12,
      ID_ERR_CANNOT_COPY_TO_DESTONATION = -13,
      ID_ERR_FACE_IMAGE_NOT_FOUND = -14,
      // state auto-detect error
      ID_ERR_STATE_NOT_RECOGNIZED = -15,
      ID_ERR_USA_TEMPLATES_NOT_FOUND = -16,
      ID_ERR_WRONG_TEMPLATE_FILE = -17,
      ID_ERR_REGION_NOT_INIT = -18,
      ID_ERR_AUTO_DETECT_NOT_SUPPORTED = -19,
      ID_ERR_COMPARE_NO_TEXT = -20,
      ID_ERR_COMPARE_NO_BARCODE = -21,
      ID_ERR_COMPARE_BC_LIB_NOT_FOUND = -22,
      ID_ERR_COMPARE_LICENSE_DONT_MATCH_BC_LIBRARY = -23,
      ID_ERR_DM_LIBRARY_NOT_FOUND = -24,
      ID_ERR_DM_LIBRARY_NOT_LOADED = -25,
      ID_ERR_DM_WM_NOT_FOUND = -26,
      ID_ERR_DM_WM_NOT_AUTHENTICATED = -27
    };

    private bool SLIB_HasError(int ErrCode, out SLIB_ERR Error, out String ErrMsg)
    {
      Error = (SLIB_ERR)ErrCode;
      ErrMsg = Error.ToString() + " (" + ErrCode.ToString() + ")";

      if (Error == SLIB_ERR.SLIB_ERR_NONE) return false;
      if (Error == SLIB_ERR.SLIB_LIBRARY_ALREADY_INITIALIZED) return false;
      if (Error >= 0) return false;

      return true;
    }

    private bool ID_HasError(int ErrCode, out String ErrMsg)
    {
      ID_ERR _err;

      _err = (ID_ERR)ErrCode;
      ErrMsg = _err.ToString() + " (" + ErrCode.ToString() + ")";

      if (_err == ID_ERR.ID_ERR_NONE) return false;
      if (_err >= 0) return false;


      return true;
    }

    #endregion

    public Snapshell2()
    {
      m_self_pointer = this;
    }

    private void DeleteTempFiles(String Folder, String Pattern)
    {
      String[] _files;
      try
      {
        _files = Directory.GetFiles(Folder, Pattern);
        foreach (String _file in _files)
        {
          try
          {
            File.Delete(_file);
          }
          catch
          {
            ;
          }
        }
      }
      catch
      {
        ;
      }
    }

    private delegate void InvokeNotify(ScannedPersonalInfoEvent Event);
    private delegate void InvokeNotifyStatus(ScanProcessStatus Status);


    public enum ScanProcessStatus
    {
      None = 0,
      Idle = 1,

      ScanStarted = 10,
      ScanRunning = 11,
      ScanDone = 12,
      ScanFailed = 13,

      DetectStarted = 20,
      DetectRunning = 21,
      DetectDone = 22,
      DetectFailed = 25,

      ProcessStarted = 30,
      ProcessDone = 31,
      ProcessFailed = 35,
    }

    public delegate void ScanProcessStatusHandler(ScanProcessStatus Status);

    private void NotifyStatus(ScanProcessStatus Status)
    {
      try
      {
        Control _ctrl;
        ScanProcessStatusHandler _handler;

        _ctrl = m_control;
        _handler = m_status_handler;

        if (_handler == null)
        {
          return;
        }

        if (_ctrl != null)
        {
          if (_ctrl.InvokeRequired)
          {
            _ctrl.BeginInvoke(new ScanProcessStatusHandler(NotifyStatus), Status);

            return;
          }
        }

        _handler(Status);
      }
      catch
      { }
    }

    private void Notify(ScannedPersonalInfoEvent Event)
    {
      try
      {
        Control _ctrl;
        ScannedPersonalInfoHandler _handler;

        _ctrl = m_control;
        _handler = m_handler;
        if (_handler == null)
        {
          return;
        }

        if (_ctrl != null)
        {
          if (_ctrl.InvokeRequired)
          {
            _ctrl.BeginInvoke(new InvokeNotify(Notify), Event);

            return;
          }
        }

        _handler(Event);
      }
      catch { }
    }

    Boolean SlibInit(out String ErrorMsg)
    {
      ErrorMsg = String.Empty;

      try
      {
        if (m_scanner == null)
        {
          m_scanner = new X.SLibExClass();
        }

        if (m_scanner == null)
        {
          ErrorMsg = "Unable to create 'SLibExClass'";

          return false;
        }

        SLIB_ERR _slib_err;
        if (SLIB_HasError(m_scanner.InitLibrary(LICENSE_SNAPSHELL), out _slib_err, out ErrorMsg))
        {
          m_scanner = null;

          try
          {
            // UnInit the library
            Z.SLibExClass _scanner;
            _scanner = new SCANWEXLib.SLibExClass();
            _scanner.UnInit();
          }
          catch
          { }

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMsg = "EXCEPTION: " + _ex.Message;

        return false;
      }
    }

    public static Boolean RegisterHandlers(Control Ctrl, ScannedPersonalInfoHandler Handler, ScanProcessStatusHandler HandlerStatus)
    {
      try
      {
        Snapshell2 _class;

        _class = m_self_pointer;
        if (_class == null)
        {
          return false;
        }
        if (!_class.m_initialized)
        {
          return false;
        }

        _class.m_control = Ctrl;
        _class.m_handler = Handler;
        _class.m_status_handler = HandlerStatus;

        return true;
      }
      catch
      {
        return false;
      }
    }

    public static void OpenSettings()
    {
      ShowDialog();
    }

    public static void InitializeCheckScan()
    {
      m_start = true;
      m_ev_start.Set();
    }

    public static void StopCheckScan()
    {
      m_start = false;
      m_ev_start.Reset();
    }

    public static bool Scan(Control Ctrl, ScannedPersonalInfoHandler Handler, ScanProcessStatusHandler HandlerStatus)
    {
      try
      {
        Snapshell2 _class;

        _class = m_self_pointer;
        if (_class == null)
        {
          return false;
        }
        if (!_class.m_initialized)
        {
          return false;
        }
        //ETP Flag for avoid starting a new scan if the program is scaning.
        if (m_is_scanning)
        {
          return false;
        }
       
        _class.m_control = Ctrl;
        _class.m_handler = Handler;
        _class.m_status_handler = HandlerStatus;
      
        m_is_scanning = true;
        return _class.RequestScan();
      }
      catch
      {
        m_is_scanning = false;
        return false;
      }
    }

    public static bool Process(Control Ctrl, ScannedPersonalInfoHandler Handler, ScanProcessStatusHandler HandlerStatus)
    {
      try
      {
        Snapshell2 _class;

        _class = m_self_pointer;
        if (_class == null)
        {
          return false;
        }
        if (!_class.m_initialized)
        {
          return false;
        }

        _class.m_control = Ctrl;
        _class.m_handler = Handler;
        _class.m_status_handler = HandlerStatus;

        return _class.RequestProcess();
      }
      catch
      {
        return false;
      }
    }

    private WAIT_STATUS WaitScanRequested()
    {
      try
      {
        SLIB_ERR _slib_err;
        String _str_error;
        WaitHandle[] _handles;
        int _idx;
        int _pit;

        _handles = new WaitHandle[3] { m_scan_requested, m_process_requested, m_settings_requested };

        if (m_save_handler != null)
        {
          String _new_config;

          _new_config = ConfigToString();
          if (_new_config != m_user_config)
          {
            if (m_save_handler(_new_config))
            {
              m_user_config = _new_config;
            }
          }
        }

        while (true)
        {
          if (!m_start)
          {
            m_ev_start.WaitOne();
          }

          _idx = WaitHandle.WaitAny(_handles, 500);
          switch (_idx)
          {
            case 0: return WAIT_STATUS.SCAN_REQUESTED;
            case 1: return WAIT_STATUS.PROCESS_REQUESTED;
            case 2: return WAIT_STATUS.SETTINGS_REQUESTED;

            case WaitHandle.WaitTimeout:
              {
                // - Check "PaperInTray"
                _pit = m_scanner.PaperInTray;
                if (SLIB_HasError(_pit, out _slib_err, out _str_error))
                {
                  return WAIT_STATUS.ERROR;
                }

                if (_pit == 1)
                {
                  RequestScan();
                }
              }
              break;

            default:
              return WAIT_STATUS.ERROR;
          }
        } // while (true)
      }
      catch
      {
        return WAIT_STATUS.ERROR;
      }

    }


    public void Loop()
    {
      int _wait_hint;
      String _error_txt;
      Boolean _first;
      ScannedPersonalInfo _spinfo;
      ScannedPersonalInfoEvent _ev;
      String _err_msg;

      _wait_hint = 0;
      _first = true;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 100;

        if (!SlibInit(out _err_msg))
        {
          _wait_hint = 10000;

          continue;
        }

        if (_first)
        {
          CreateTable();
          _first = false;
        }

        try
        {
          WAIT_STATUS _request;

          _request = WaitScanRequested();
          switch (_request)
          {
            case WAIT_STATUS.SCAN_REQUESTED:
              NotifyStatus(ScanProcessStatus.ScanStarted);
              NotifyStatus(ScanProcessStatus.ScanRunning);           
              if (!Scan(out _spinfo, out _error_txt))
              {                
                NotifyStatus(ScanProcessStatus.ScanFailed);

                continue;
              }
              NotifyStatus(ScanProcessStatus.ScanDone);

              _ev = new ScannedPersonalInfoEvent();
              _ev.info = new ScannedPersonalInfo();
              _ev.info.image_x = m_image_x;
              Notify(_ev);

              RequestProcess();

              _wait_hint = 0;

              break;

            case WAIT_STATUS.PROCESS_REQUESTED:
              NotifyStatus(ScanProcessStatus.ProcessStarted);
              if (!Process(out _spinfo, out _error_txt))
              {
                NotifyStatus(ScanProcessStatus.ProcessFailed);
                m_is_scanning = false;
                continue;
              }
              NotifyStatus(ScanProcessStatus.ProcessDone);

              _ev = new ScannedPersonalInfoEvent();
              _ev.error = false;
              _ev.running = false;
              _ev.info = _spinfo;
              Notify(_ev);
              m_is_scanning = false;
                         
              _wait_hint = 0;

              break;

            case WAIT_STATUS.SETTINGS_REQUESTED:
              if (m_control != null)
              {
                m_control.Visible = true;
              }
              break;

            default:
            case WAIT_STATUS.ERROR:
              // Do nothing
              break;
          }
        }
        catch
        {
          _wait_hint = 30000;
        }
      }
    }


    public bool RequestScan()
    {
      return m_scan_requested.Set();
    }
    public bool RequestProcess()
    {
      return m_process_requested.Set();
    }

    private void CreateTable()
    {
      int _r_id;
      int _c_id;
      int _s_id;
      int _order;
      String _r_name;
      String _c_name;
      String _s_name;
      String _c_iso2;
      String _c_iso3;

      DataTable _config;
      DataTable _states;
      DataTable _countries;
      DataTable _regions;
      DataSet _ds;
      int _user_region_id;


      _user_region_id = -1;

      _config = new DataTable("Config");
      _config.Columns.Add("AutoDetect", Type.GetType("System.Boolean"));
      _config.Columns.Add("StateID", Type.GetType("System.Int32"));
      DataRow _dr_c = _config.NewRow();
      _dr_c["AutoDetect"] = true;
      _dr_c["StateID"] = -1;
      _config.Rows.Add(_dr_c);

      _regions = new DataTable("Regions");
      _regions.Columns.Add("Region.ID", Type.GetType("System.Int32"));
      _regions.Columns.Add("Region.Name", Type.GetType("System.String"));
      _regions.Columns.Add("Region.AutoSupported", Type.GetType("System.Boolean"));
      _regions.Columns.Add("Region.AutoEnabled", Type.GetType("System.Boolean"));
      _regions.Columns.Add("Region.Order", Type.GetType("System.Int32"));

      _countries = new DataTable("Countries");
      _countries.Columns.Add("Region.ID", Type.GetType("System.Int32"));
      _countries.Columns.Add("Country.ID", Type.GetType("System.Int32"));
      _countries.Columns.Add("Country.Name", Type.GetType("System.String"));
      _countries.Columns.Add("Country.ISO2", Type.GetType("System.String"));
      _countries.Columns.Add("Country.ISO3", Type.GetType("System.String"));

      _states = new DataTable("States");
      _states.Columns.Add("Region.ID", Type.GetType("System.Int32"));
      _states.Columns.Add("Country.ID", Type.GetType("System.Int32"));
      _states.Columns.Add("State.ID", Type.GetType("System.Int32"));
      _states.Columns.Add("State.Name", Type.GetType("System.String"));
      _states.Columns.Add("State.Enabled", Type.GetType("System.Boolean"));

      _ds = new DataSet();
      _ds.Tables.Add(_config);
      _ds.Tables.Add(_regions);
      _ds.Tables.Add(_countries);
      _ds.Tables.Add(_states);
      _ds.Relations.Add("R2C", _regions.Columns["Region.ID"], _countries.Columns["Region.ID"]);
      _ds.Relations.Add("R2S", _regions.Columns["Region.ID"], _states.Columns["Region.ID"]);
      _ds.Relations.Add("C2S", _countries.Columns["Country.ID"], _states.Columns["Country.ID"]);

      _r_id = m_ex_id_data.RegionGetFirst();
      _order = 0;
      while (_r_id >= 0)
      {
        m_ex_id_data.RegionGetNameById(_r_id, out _r_name);

        DataRow _dr_region = _regions.NewRow();
        Boolean _auto = false;

        _dr_region["Region.ID"] = _r_id;
        _dr_region["Region.Name"] = _r_name;
        _auto = (_r_id == 6) ? false : true;
        _dr_region["Region.AutoSupported"] = _auto;
        _dr_region["Region.AutoEnabled"] = _auto;
        if (_auto)
        {
          _dr_region["Region.Order"] = _order++;
        }
        else
        {
          _dr_region["Region.Order"] = 0;
        }
        _regions.Rows.Add(_dr_region);

        _c_id = m_ex_id_data.RegionGetFirstCountry(_r_id);
        while (_c_id >= 0)
        {
          m_id_data.Id2Country(_c_id, out _c_name);

          m_ex_id_data.Id2Country2(_c_id, out _c_iso2);
          if (_c_iso2.Contains("����"))
          {
            _c_iso2 = "--";
          }
          m_ex_id_data.Id2Country3(_c_id, out _c_iso3);
          if (_c_iso3.Contains("����"))
          {
            _c_iso3 = "---";
          }

          if (_c_iso2 == m_user_country_iso2)
          {
            _user_region_id = _r_id;
          }

          DataRow _dr_country;
          _dr_country = _countries.NewRow();
          _dr_country["Region.ID"] = _r_id;
          _dr_country["Country.ID"] = _c_id;
          _dr_country["Country.Name"] = _c_name;
          _dr_country["Country.ISO2"] = _c_iso2;
          _dr_country["Country.ISO3"] = _c_iso3;
          _countries.Rows.Add(_dr_country);

          _s_id = m_id_data.GetFirstStateByCountry(_c_id);
          while (_s_id >= 0)
          {
            m_id_data.Id2State(_s_id, out _s_name);

            DataRow _dr_state = _states.NewRow();
            _dr_state["Region.ID"] = _r_id;
            _dr_state["Country.ID"] = _c_id;
            _dr_state["State.ID"] = _s_id;
            _dr_state["State.Name"] = _s_name;
            _dr_state["State.Enabled"] = true;

            _states.Rows.Add(_dr_state);

            _s_id = m_id_data.GetNextStateByCountry(_c_id);
          }
          _c_id = m_ex_id_data.RegionGetNextCountry(_r_id);
        }

        _r_id = m_ex_id_data.RegionGetNext();
      }

      m_dataset = _ds;

      if (String.IsNullOrEmpty(m_user_config))
      {
        // Try using the 'Region' of the user country
        if (_user_region_id >= 0)
        {
          _order = 1;
          foreach (DataRow _dr_region in _regions.Rows)
          {
            if (_user_region_id == (int)_dr_region["Region.ID"])
            {
              _dr_region["Region.Order"] = 0;
              _dr_region["Region.AutoEnabled"] = true;
            }
            else
            {
              _dr_region["Region.Order"] = _order++;
              _dr_region["Region.AutoEnabled"] = false;
            }
          }
        }
      }
      else
      {
        StringToConfig(m_user_config);
      }
    }

    private String ConfigToString()
    {
      StringBuilder _sb;
      int _auto;
      int _state_id;
      DataRow _config;
      String _regions;
      int _region_id;

      try
      {
        if (m_dataset == null)
        {
          return String.Empty;
        }

        _sb = new StringBuilder();

        _auto = ((Boolean)(m_dataset.Tables["Config"].Rows[0]["AutoDetect"])) ? 1 : 0;
        _state_id = (int)(m_dataset.Tables["Config"].Rows[0]["StateID"]);
        _regions = "";

        DataRow[] _dr_regions;

        _dr_regions = m_dataset.Tables["Regions"].Select("[Region.AutoEnabled]=1", "[Region.Order] Asc, [Region.ID] Asc");
        foreach (DataRow _dr_region in _dr_regions)
        {
          _region_id = (int)_dr_region["Region.ID"];
          if (_regions.Length > 0)
          {
            _regions = _regions + ", ";
          }
          _regions = _regions + _region_id.ToString();
        }

        _sb.AppendLine(String.Format("{0}: {1}", "AutoDetect", _auto));
        _sb.AppendLine(String.Format("{0}: {1}", "Regions", _regions));
        _sb.AppendLine(String.Format("{0}: {1}", "StateID", _state_id));

        return _sb.ToString();
      }
      catch
      { }

      return String.Empty;
    }

    private void StringToConfig(String Config)
    {
      if (String.IsNullOrEmpty(Config))
      {
        return;
      }
      if (m_dataset == null)
      {
        return;
      }


      String[] _lines;
      String[] _kv;
      Dictionary<String, String> _dic;
      int _idx;
      String _key;
      String _value;
      int _order;
      _dic = new Dictionary<string, string>();

      _lines = Config.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
      foreach (String _line in _lines)
      {
        _idx = _line.IndexOf(':');
        if (_idx > 0)
        {
          _key = _line.Substring(0, _idx).Trim();
          _value = _line.Substring(1 + _idx).Trim();
          _dic.Add(_key, _value);
        }
      }

      DataRow _dr_c;
      _dr_c = m_dataset.Tables["Config"].Rows[0];
      _dr_c["AutoDetect"] = (int.Parse(_dic["AutoDetect"]) == 1);
      _dr_c["StateID"] = int.Parse(_dic["StateID"]);
      _value = _dic["Regions"];
      _lines = _value.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries);

      _order = 10;
      foreach (DataRow _r in m_dataset.Tables["Regions"].Rows)
      {
        _r["Region.AutoEnabled"] = false;
        _r["Region.Order"] = _order++;
      }

      _order = 0;
      foreach (String _reg in _lines)
      {
        int _region_id;
        _region_id = int.Parse(_reg);
        DataRow[] _dx;
        _dx = m_dataset.Tables["Regions"].Select("Region.ID = " + _region_id.ToString());
        if (_dx != null)
        {
          if (_dx.Length > 0)
          {
            _dx[0]["Region.AutoEnabled"] = true;
            _dx[0]["Region.Order"] = _order++;
          }
        }
      }
    }




    public bool Init(out String ErrorMsg)
    {
      ErrorMsg = String.Empty;

      try
      {
        if (m_initialized)
        {
          // Already initialized
          return true;
        }

        try
        {
          m_tmp_folder = Path.GetTempPath();
        }
        catch
        {
          m_tmp_folder = ".";
        }
        m_tmp_prefix = "ScannerTmpFile_";
        m_tmp_pattern = m_tmp_prefix + "*";
        DeleteTempFiles(".", m_tmp_pattern);
        DeleteTempFiles(m_tmp_folder, m_tmp_pattern);

        SlibInit(out ErrorMsg);


        m_id_data = new Y.IdDataClass();
        if (m_id_data == null)
        {
          ErrorMsg = "Unable to create 'IdDataClass'";

          return false;
        }
        m_ex_scanner = new Z.SLibExClass();
        m_ex_id_data = new Z.IdDataClass();

        if (ID_HasError(m_id_data.InitLibrary(LICENSE_SDK), out ErrorMsg))
        {
          return false;
        }

        m_initialized = true;

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMsg = _ex.Message;

        return false;
      }
    }

    public Boolean AutoDetect(out int StateID)
    {
      DataRow[] _dr_regions;
      int _region_id;
      Boolean _enabled;
      int _state_id;
      int _angle;
      String ErrorMsg;
      Boolean _failed;

      StateID = -1;
      _failed = true;

      try
      {
        NotifyStatus(ScanProcessStatus.DetectStarted);

        _enabled = (Boolean)m_dataset.Tables["Config"].Rows[0]["AutoDetect"];
        if (!_enabled)
        {
          _state_id = (int)m_dataset.Tables["Config"].Rows[0]["StateID"];
          if (_state_id >= 0)
          {
            StateID = _state_id;
            _failed = false;

            return true;
          }
          return false;
        }

        NotifyStatus(ScanProcessStatus.DetectRunning);

        _dr_regions = m_dataset.Tables["Regions"].Select("[Region.AutoEnabled]=1", "[Region.Order] Asc, [Region.ID] Asc");
        foreach (DataRow _dr_region in _dr_regions)
        {
          _region_id = (int)_dr_region["Region.ID"];

          if (ID_HasError(SetRegion(_region_id), out ErrorMsg))
          {
            continue;
          }

          if (ID_HasError(m_ex_id_data.EnableAllStatesInRegion(_region_id, 1), out ErrorMsg))
          {
            continue;
          }

          if (ID_HasError(_state_id = m_id_data.AutoDetectStateEx(null, out _angle), out ErrorMsg))
          {
            continue;
          }
          else
          {
            if (_angle == 2)
            {
              //ETP Bug 3958: flag indicating that we need to rotate image in form Thread.
              m_form.NeedRotate=true;
            }         

            StateID = _state_id;

            _failed = false;

            return true;
          }
        } // foreach

        ErrorMsg = "No Match";

        return false;
      }
      catch
      {
        StateID = -1;
      }
      finally
      {
        if (_failed)
        {
          NotifyStatus(ScanProcessStatus.DetectFailed);
        }
        else
        {
          NotifyStatus(ScanProcessStatus.DetectDone);
        }
      }

      return false;
    }

    /// <summary>
    /// Process last image scanned
    /// </summary>
    /// <param name="NewId">returns if we are processing a new id, compared with last process</param>
    /// <param name="info">processed info</param>
    /// <param name="Result">result returned by ProcState()</param>
    /// <returns></returns>
    public bool Scan(out ScannedPersonalInfo PersonalInfo, out string ErrorMsg)
    {
      SLIB_ERR _slib_error;
      String _tmp_filename;
      Boolean _failed;

      ErrorMsg = String.Empty;
      PersonalInfo = null;

      m_image_x = null;
      m_image_x_path = null;
      _failed = true;

      try
      {
        if (!m_initialized)
        {
          ErrorMsg = "Not initialized.";

          return false;
        }

        if (!SlibInit(out ErrorMsg))
        {
          return false;
        }

        DeleteTempFiles(m_tmp_folder, m_tmp_pattern);

        if (SLIB_HasError(SetScannerResolution(600), out _slib_error, out ErrorMsg))
        {
          return false;
        }

        _tmp_filename = Path.Combine(m_tmp_folder, m_tmp_prefix + Path.GetRandomFileName());
        if (SLIB_HasError(m_scanner.ScanToFile(_tmp_filename), out _slib_error, out ErrorMsg))
        {
          return false;
        }

        m_image_x = Image.FromFile(_tmp_filename);
        m_image_x_path = _tmp_filename;

        return true;
      }
      catch (Exception _ex)
      {
        m_image_x = null;
        m_image_x_path = null;

        ErrorMsg = _ex.Message;

        return false;
      }
      finally
      {
      }
    }


    public bool Process(out ScannedPersonalInfo PersonalInfo, out string ErrorMsg)
    {
      bool _found;
      string _photo_filename;
      ScannedPersonalInfo _info;

      ErrorMsg = String.Empty;
      PersonalInfo = null;

      try
      {
        _info = new ScannedPersonalInfo();

        _found = AutoDetect(out m_state_id);
        if (!_found)
        {
          return false;
        }

        String _state;

        if (ID_HasError(m_id_data.Id2State(m_state_id, out _state), out ErrorMsg))
        {
          return false;
        }

        SetDatesFormat((short)7);

        String _card_type;
        _card_type = m_ex_id_data.CardType;

        if (ID_HasError(m_id_data.ProcState(null, (int)m_state_id), out ErrorMsg))
        {
          NotifyStatus(ScanProcessStatus.DetectFailed);

          return false;
        }

        if (ID_HasError(m_id_data.RefreshData(), out ErrorMsg))
        {
          NotifyStatus(ScanProcessStatus.DetectFailed);

          return false;
        }

        // Front / Back
        if (m_ex_id_data.DocType.Contains("FRONT"))
        {
          _info.image_front = m_image_x;
        }
        if (m_ex_id_data.DocType.Contains("BACK"))
        {
          _info.image_back = m_image_x;
        }

        _photo_filename = Path.Combine(m_tmp_folder, m_tmp_prefix + Path.GetRandomFileName());
        if (ID_HasError(m_id_data.GetFaceImage(null, _photo_filename, (int)m_state_id), out ErrorMsg))
        {
          // continue without the "photo"
        }
        else
        {
          _info.photo_filename = _photo_filename;
          _info.image_photo = Image.FromFile(_photo_filename);

          if (_info.image_front == null && _info.image_back == null)
          {
            _info.image_front = m_image_x;
          }
        }

        _info.DocumentType = m_ex_id_data.TemplateType;
        _info.DocumentState = StringTrimOrNull(_state);
        _info.DocumentID = StringTrimOrNull(m_id_data.Id);

        // "Clean" DocumentID
        String _clean_id;
        _clean_id = "";
        foreach (Char _c in _info.DocumentID)
        {
          if (Char.IsLetterOrDigit(_c)) _clean_id = _clean_id + _c;
        }
        _info.DocumentID = _clean_id;

        if (_clean_id.Length <= 5)
        {
          NotifyStatus(ScanProcessStatus.DetectFailed);

          return false;
        }

        ParseDateTime(m_ex_id_data.ExpirationDate4, m_id_data.ExpirationDate, out _info.DocumentExpirationDate);
        ParseDateTime(m_ex_id_data.IssueDate4, m_id_data.IssueDate, out _info.DocumentIssueDate);

        // Internal ID
        _info.InternalID = _info.DocumentType + ":" + _info.DocumentID;
        _info.InternalID.Replace(" ", "");
        _info.InternalID.Replace("-", "");

        if (!String.IsNullOrEmpty(m_ex_id_data.NameLast1) && !String.IsNullOrEmpty(m_ex_id_data.NameLast1))
        {
          _info.Name1 = StringTrimOrNull(m_ex_id_data.NameLast1);
          _info.Name2 = StringTrimOrNull(m_ex_id_data.NameLast2);
          _info.Name3 = StringTrimOrNull(m_ex_id_data.NameFirst_NonMRZ);
          _info.Name4 = StringTrimOrNull(m_ex_id_data.NameMiddle_NonMRZ);
        }
        else
        {
          _info.Name1 = StringTrimOrNull(m_id_data.NameLast);
          _info.Name2 = StringTrimOrNull("");
          _info.Name3 = StringTrimOrNull(m_id_data.NameFirst);
          _info.Name4 = StringTrimOrNull(m_id_data.NameMiddle);
        }

        _info.FatherName = StringTrimOrNull(m_ex_id_data.FatherName);
        _info.MotherName = StringTrimOrNull(m_ex_id_data.MotherName);


        _info.Gender = StringTrimOrNull(m_id_data.Sex);
        ParseDateTime(m_ex_id_data.DateOfBirth4, m_id_data.DateOfBirth, out _info.BirthDate);

        _info.Nationality = StringTrimOrNull(m_ex_id_data.Nationality);

        _info.Address[0] = StringTrimOrNull(m_id_data.Address);
        _info.Address[1] = StringTrimOrNull(m_id_data.Address2);
        _info.Address[2] = StringTrimOrNull(m_id_data.Address3);
        _info.Address[3] = StringTrimOrNull(m_id_data.Address4);
        _info.Address[4] = StringTrimOrNull(m_id_data.Address5);
        _info.Address[5] = StringTrimOrNull(m_ex_id_data.Address6);

        _info.City = StringTrimOrNull(m_id_data.City);
        _info.ZipCode = StringTrimOrNull(m_id_data.Zip);

        _info.CountryISO2 = StringTrimOrNull(m_ex_id_data.CountryISO2);
        _info.CountryISO3 = StringTrimOrNull(m_ex_id_data.CountryISO3);
        _info.CountryName = StringTrimOrNull(m_ex_id_data.CountryShort);

        PersonalInfo = _info;

        NotifyStatus(ScanProcessStatus.ProcessDone);

        return true;
      }
      catch (Exception _ex)
      {
        NotifyStatus(ScanProcessStatus.DetectFailed);

        ErrorMsg = _ex.Message;

        return false;
      }

    }


    private String StringTrimOrNull(String Value)
    {
      if (!String.IsNullOrEmpty(Value))
      {
        Value = Value.Trim();

        if (!String.IsNullOrEmpty(Value))
        {
          return Value;
        }
      }

      return null;
    }

    // Parse a DateTime from the ID_DATA library
    private Boolean ParseDateTime(String FormatYYYYMMDD, String FormatYYMMDD, out DateTime Value)
    {
      if (!String.IsNullOrEmpty(FormatYYYYMMDD))
      {
        FormatYYYYMMDD = FormatYYYYMMDD.Trim();

        try
        {
          Value = new DateTime(int.Parse(FormatYYYYMMDD.Substring(0, 4)),
                               int.Parse(FormatYYYYMMDD.Substring(5, 2)),
                               int.Parse(FormatYYYYMMDD.Substring(8, 2)));

          return true;
        }
        catch
        { }
      }

      if (!String.IsNullOrEmpty(FormatYYMMDD))
      {
        FormatYYMMDD = FormatYYMMDD.Trim();

        if (DateTime.TryParse(FormatYYMMDD, out Value))
        {
          return true;
        }
      }

      Value = DateTime.MinValue;

      return false;
    }



    private static form_id_card_scanner m_form = null;
    internal static void SetForm(form_id_card_scanner form_id_card_scanner)
    {
      m_form = form_id_card_scanner;
    }

    public static void ShowDialog()
    {
      if (m_form == null) return;

      if (m_form.InvokeRequired)
      {
        m_form.BeginInvoke(new InvokeDelegate(ShowDialog));

        return;
      }
      m_form.Visible = true;
      m_form.BringToFront();
      m_form.TopLevel = true;
      m_form.TopMost = true;
    }
  }





}
