using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IDCardScanner
{
  public partial class form_id_card_scanner : Form
  {
    private bool m_processing = false;
    private String m_status = "";
    private bool m_vertical = false;
    private IDCardScanner.ScannedPersonalInfo m_last_info = new IDCardScanner.ScannedPersonalInfo();
    private DataSet m_dataset = null;

    private Boolean m_open_settings = false;
    private Boolean m_need_rotate;

    private static Control m_remote_control = null;
    private static ScannedPersonalInfoHandler m_remote_handler = null;

    public static void RegisterHandler(Control Ctrl, ScannedPersonalInfoHandler Handler)
    {
      if (Handler == null)
      {
        // Unregister
        m_remote_control = null;
        m_remote_handler = null;
      }
      else
      {
        m_remote_handler = Handler;
        m_remote_control = Ctrl;
      }
    }

    public form_id_card_scanner()
    {
      InitializeComponent();

      txt_all_fields.BackColor = this.BackColor;
      txt_all_fields.ForeColor = this.ForeColor;
      m_img_x.ErrorImage = null;

      IDCardScanner.Snapshell2.SetForm(this);
      m_need_rotate = false;
    }

    public Boolean NeedRotate
    {
      get
      {
        return m_need_rotate;
      }
      set
      {
        m_need_rotate = value;
      }
    }

    private void StatusHandler(IDCardScanner.Snapshell2.ScanProcessStatus Status)
    {
      Size _size;
      Boolean _visible;

      _size = new Size(500, 200);
      _visible = (m_remote_control != null && m_remote_handler != null);

      switch (Status)
      {
        case IDCardScanner.Snapshell2.ScanProcessStatus.ScanStarted:
          m_img_x.Image = null;
          m_vertical = true;

          m_status = "Scanning ...";
          m_processing = true;

          break;

        case IDCardScanner.Snapshell2.ScanProcessStatus.DetectStarted:
          m_status = "Detecting ..."; 
          m_processing = true;
          m_vertical = true;
          break;

        case IDCardScanner.Snapshell2.ScanProcessStatus.DetectDone:
        case IDCardScanner.Snapshell2.ScanProcessStatus.ProcessStarted:
          m_status = "Processing ..."; 
          m_processing = true;
          m_vertical = false;
          break;

        
        case IDCardScanner.Snapshell2.ScanProcessStatus.DetectFailed:
        case IDCardScanner.Snapshell2.ScanProcessStatus.ProcessFailed:
          m_status = "Failed!"; 
          m_processing = false;
          m_img_x.Refresh();
          break;

        case IDCardScanner.Snapshell2.ScanProcessStatus.ProcessDone:
          m_status = "OK"; 
          m_processing = false;
          m_img_x.Refresh();
          break;
      }

      if (m_status.Contains("..."))
      {
        _size = new Size(this.m_img_x.Width+2, this.m_img_x.Height+2);
        this.FormBorderStyle = FormBorderStyle.None;
        
        if (!this.Visible)
        {
          this.Visible = _visible;
        }
      }
      else
      {
        this.Visible = (m_status != "OK") && _visible;
        this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      }
      this.Size = _size;
      
      btn_process.Enabled = (m_img_x.Image != null) && (m_processing == false);
      btn_scan.Enabled = (chk_regions.Items.Count > 0) && (m_processing == false);

      gp_back_side.Visible = (_size.Height >= 520);
      gp_front_side.Visible = (_size.Height >= 520);
      gb_photo.Visible = (_size.Height >= 520);
      gb_data.Visible = (_size.Height >= 520);

      {
        Control _ctrl;
        _ctrl = m_remote_control;
        if (_ctrl != null && this.Visible )
        {
          this.Location = new Point(_ctrl.Location.X + _ctrl.Width / 2 - this.Width / 2,
                                    _ctrl.Location.Y + _ctrl.Height / 2 - this.Height / 2);
          this.BringToFront();

          this.TopLevel = true;
          this.TopMost = true;
        }
      }
    }

    private void ScannedPersonalInfoHandler(IDCardScanner.ScannedPersonalInfoEvent Event)
    {
      if (Event.info.image_x != null)
      {
        try
        {
          m_img_x.Image = Event.info.image_x;
        }
        catch (Exception _ex)
        {
          ;
        }
      }

      if (Event.running)
      {
        return;
      }

      if (Event.error)
      {
        return;
      }

      if (Event.info.DocumentID == null)
      {
        return;
      }

      if (m_last_info.InternalID == Event.info.InternalID)
      {
        // Merge info          
        Merge(Event.info, m_last_info);
      }
      else
      {
        m_img_back.Image = null;
        m_img_front.Image = null;
        m_img_photo.Image = null;
        txt_all_fields.Text = String.Empty;
      }

      m_last_info = Event.info;

      if (m_last_info.image_photo != null)
      {
        m_img_photo.Image = m_last_info.image_photo;
      }
      if (m_last_info.image_front != null)
      {
        m_img_front.Image = m_last_info.image_front;
      }
      if (m_last_info.image_back != null)
      {
        m_img_back.Image = m_last_info.image_back;
      }
      txt_all_fields.Text = DictionaryToString(ToDictionary(m_last_info));

      if (m_remote_control != null && m_remote_handler != null)
      {
        ScannedPersonalInfoEvent _ev;
        _ev = new ScannedPersonalInfoEvent();
        _ev.info = m_last_info;

        NotifyToRemote(_ev);
      }

      
    }

    private void NotifyToRemote(ScannedPersonalInfoEvent Event)
    {
      try
      {
        Control _ctrl;
        ScannedPersonalInfoHandler _hdlr;

        _ctrl = m_remote_control;
        _hdlr = m_remote_handler;

        if (_ctrl == null) return;
        if (_hdlr == null) return;

        if (_ctrl.InvokeRequired)
        {
          _ctrl.BeginInvoke(new ScannedPersonalInfoHandler(NotifyToRemote), Event);

          return;
        }

        _hdlr(Event);
      }
      catch
      { }
      finally
      { }
    }

    private void MergeDates(ref DateTime A, DateTime B)
    {
      if (A == DateTime.MinValue) { A = B; }
    }
    private void MergeStrings(ref String A, String B)
    {
      if (String.IsNullOrEmpty(A))
      {
        A = B;

        return;
      }

      if (String.IsNullOrEmpty(B))
      {
        return;
      }

      if (A.Length < B.Length)
      {
        A = B;

        return;
      }
    }

    private void Merge(IDCardScanner.ScannedPersonalInfo A, IDCardScanner.ScannedPersonalInfo B)
    {
      int _i;

      for (_i = 0; _i < A.Address.Length; _i++)
      {
        MergeStrings(ref A.Address[_i], B.Address[_i]);
      }

      MergeDates(ref A.BirthDate, B.BirthDate);
      MergeStrings(ref A.City, B.City);
      MergeStrings(ref A.CountryISO2, B.CountryISO2);
      MergeStrings(ref A.CountryISO3, B.CountryISO3);
      MergeStrings(ref A.CountryName, B.CountryName);

      MergeDates(ref A.DocumentExpirationDate, B.DocumentExpirationDate);
      // MergeStrings(ref A.DocumentID, B.DocumentID);
      MergeDates(ref A.DocumentIssueDate, B.DocumentIssueDate);
      MergeStrings(ref A.DocumentState, B.DocumentState);
      MergeStrings(ref A.DocumentType, B.DocumentType);
      MergeStrings(ref A.front_filename, B.front_filename);
      MergeStrings(ref A.Gender, B.Gender);

      if (!String.IsNullOrEmpty(A.Name1) && !String.IsNullOrEmpty(B.Name1))
      {
        if (A.Name1.Length > B.Name1.Length)
        {
          if (A.Name1.Contains(B.Name1) && A.Name1.Contains(B.Name2))
          {
            // B is OK
            A.Name1 = B.Name1;
            A.Name2 = B.Name2;
            A.Name3 = B.Name3;
            A.Name4 = B.Name4;
          }
        }
      }

      MergeStrings(ref A.Nationality, B.Nationality);
      MergeStrings(ref A.photo_filename, B.photo_filename);
      MergeStrings(ref A.ZipCode, B.ZipCode);

      if (A.image_photo == null)
      {
        A.image_photo = B.image_photo;
      }
      if (A.image_front == null)
      {
        A.image_front = B.image_front;
      }
      if (A.image_back == null)
      {
        A.image_back = B.image_back;
      }
    }

    private Dictionary<String, String> ToDictionary(IDCardScanner.ScannedPersonalInfo A)
    {
      Dictionary<String, String> _d;
      int _i;

      _d = new Dictionary<string, string>();

      for (_i = 0; _i < A.Address.Length; _i++)
      {
        _d.Add("Address" + _i.ToString(), A.Address[_i]);
      }
      _d.Add("BirthDate", (A.BirthDate == DateTime.MinValue) ? "" : A.BirthDate.ToString("yyyy-MM-dd"));
      _d.Add("City", A.City);
      _d.Add("CountryISO2", A.CountryISO2);
      _d.Add("CountryISO3", A.CountryISO3);
      _d.Add("CountryName", A.CountryName); 

      _d.Add("DocumentExpirationDate", (A.DocumentExpirationDate == DateTime.MinValue) ? "" : A.DocumentExpirationDate.ToString("yyyy-MM-dd"));
      _d.Add("DocumentID", A.DocumentID);
      _d.Add("DocumentIssueDate", (A.DocumentIssueDate == DateTime.MinValue) ? "" : A.DocumentIssueDate.ToString("yyyy-MM-dd"));
      _d.Add("DocumentState", A.DocumentState);
      _d.Add("DocumentType", A.DocumentType);
      _d.Add("front_filename", A.front_filename);
      _d.Add("Gender", A.Gender);

      _d.Add("Name1", A.Name1);
      _d.Add("Name2", A.Name2);
      _d.Add("Name3", A.Name3);
      _d.Add("Name4", A.Name4);

      _d.Add("FatherName", A.FatherName);
      _d.Add("MotherName", A.MotherName);

      _d.Add("Nationality", A.Nationality);
      _d.Add("photo_filename", A.photo_filename);
      _d.Add("ZipCode", A.ZipCode);

      return _d;
    }

    String DictionaryToString(Dictionary<String, String> D)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      foreach (String _key in D.Keys)
      {
        if (String.IsNullOrEmpty(D[_key]))
        {
          continue;
        }
        if (_key.Contains("filename"))
        {
          continue;
        }
        _sb.AppendLine(String.Format("{0,25} {1}", _key + ":", D[_key]));
      }
      return _sb.ToString();
    }

    internal class RowContainer
    {
      private int col_index = 0;
      DataRow m_row;

      internal RowContainer(DataRow Row, String ColName)
      {
        m_row = Row;
        col_index = Row.Table.Columns[ColName].Ordinal;
      }

      public DataRow Row
      {
        get
        {
          return m_row;
        }
      }

      public override String ToString()
      {
        if (m_row != null)
        {
          return (String)m_row[col_index];
        }
        else
        {
          return String.Empty;
        }
      }
    }

    int m_pos;
    private bool m_handlers_registered = false;

    private void tmr_time_Tick(object sender, EventArgs e)
    {
      if (m_dataset == null)
      {
        m_dataset = IDCardScanner.Snapshell2.DataSet();
        if ( m_dataset != null)
        {
          PopulateControls();
        }
        return;
      }
      if (!m_handlers_registered)
      { 
        m_handlers_registered = IDCardScanner.Snapshell2.RegisterHandlers(this, this.ScannedPersonalInfoHandler, this.StatusHandler);

        return;
      }

      if (!this.Visible) return;

      if (m_processing)
      {
        if (m_vertical)
        {
          m_pos = m_pos + m_img_x.Width / 20;
          if (m_pos > m_img_x.Width)
          {
            m_pos = 0;
          }
        }
        else
        {
          m_pos = m_pos + m_img_x.Height / 20;
          if (m_pos > m_img_x.Height)
          {
            m_pos = 0;
          }
        }

        //m_img_x.Paint += new PaintEventHandler(panel1_Paint);
        m_img_x.Refresh();
      }
      else
      {
        m_pos = 0;
      }
    }



    private void PopulateControls()
    {
      if (m_dataset == null)
      {
        return;
      }

      RegionsToCheckedList();
      MoveItem(0);
      RegionsToCombo();

      if (chk_regions.Items.Count > 0)
      {
        chk_auto_detect.Checked = true;
      }

      RefreshAutoCheck();
    }

    private void panel1_Paint(object sender, PaintEventArgs e)
    {
      try
      {
        if (!this.Visible) return;
        if (!this.m_img_x.Visible) return;

        int _x;
        _x = m_pos;

        if (m_img_x.Image != null)
        {
          try
          {
            //ETP Bug 3958: When m_need_rotate is true we need to rotate.
            if (m_need_rotate)
            {
              m_img_x.Image.RotateFlip(RotateFlipType.Rotate180FlipNone);
              m_need_rotate = false;
            }
            e.Graphics.DrawImage(m_img_x.Image, 0, 0, m_img_x.Width, m_img_x.Height);
          }
          catch (Exception _ex)
          {
            ;
          }
        }
        else
        {
          e.Graphics.FillRectangle(Brushes.Black, 0, 0, m_img_x.Width, m_img_x.Height);
        }
        if (m_processing)
        {
          if (m_vertical)
          {
            for (int _i = 0; _i < 2; _i++)
            {
              e.Graphics.DrawLine(Pens.Green, (m_pos + _i) % m_img_x.Width, 0, (m_pos + _i) % m_img_x.Width, m_img_x.Height);
            }
          }
          else
          {
            for (int _i = 0; _i < 2; _i++)
            {
              e.Graphics.DrawLine(Pens.Green, 0, (m_pos + _i) % m_img_x.Height, m_img_x.Width, (m_pos + _i) % m_img_x.Height);
            }
          }
        }

        SizeF _sizef;
        SizeF _sizef2;


        _sizef = e.Graphics.MeasureString(m_status, this.Font);
        _sizef2 = new SizeF(_sizef.Width * 1.1f, _sizef.Height * 1.1f);

        e.Graphics.FillRectangle(Brushes.Black,
                                 m_img_x.Width / 2 - _sizef2.Width / 2,
                                 m_img_x.Height / 2 - _sizef2.Height / 2,
                                 _sizef2.Width, _sizef2.Height);

        e.Graphics.DrawString(m_status, this.Font, Brushes.White,
                              m_img_x.Width / 2 - _sizef.Width / 2, m_img_x.Height / 2 - _sizef.Height / 2);
      }
      catch (Exception _ex)
      {
        ;
      }
    }

    private void MoveItem(int Delta)
    {
      RowContainer _r1;
      Boolean _checked;
      int _idx1;

      try
      {

        _idx1 = chk_regions.SelectedIndex;
        if (_idx1 < 0)
        {
          return;
        }

        _r1 = (RowContainer)chk_regions.Items[_idx1];
        _checked = chk_regions.CheckedItems.Contains(_r1);

        if ((_idx1 > 0 && Delta < 0) || (_idx1 < (chk_regions.Items.Count - 1) && Delta > 0))
        {
          chk_regions.Items.Remove(_r1);
          chk_regions.Items.Insert(_idx1 + Delta, _r1);
          chk_regions.SetItemChecked(_idx1 + Delta, _checked);
          chk_regions.SelectedIndex = _idx1 + Delta;
        }
      }
      finally
      {
        _idx1 = 0;
        foreach (Object _item in chk_regions.Items)
        {
          _r1 = (RowContainer)_item;
          _r1.Row["Region.Order"] = _idx1++;
        }
        if (Delta == 0)
        {
          UpdateRegionsAutoEnabled(-1, false);
        }
      }
    }

    private void btn_move_up_Click(object sender, EventArgs e)
    {
      MoveItem(-1);
    }
    private void btn_move_down_Click(object sender, EventArgs e)
    {
      MoveItem(+1);
    }
    private void RegionsToCheckedList()
    {
      DataRow[] _regions;

      _regions = m_dataset.Tables["Regions"].Select("", "Region.Order Asc");

      chk_regions.Items.Clear();
      foreach (DataRow _row in _regions)
      {
        if ((Boolean)_row["Region.AutoSupported"])
        {
          chk_regions.Items.Add(new RowContainer(_row, "Region.Name"), (Boolean)_row["Region.AutoEnabled"]);
          cmb_region.Items.Add(new RowContainer(_row, "Region.Name"));
        }
      }
      foreach (DataRow _row in _regions)
      {
        if (!(Boolean)_row["Region.AutoSupported"])
        {
          cmb_region.Items.Add(new RowContainer(_row, "Region.Name"));
        }
      }


    }
    private void UpdateLabelCombo(ComboBox C, Label L)
    {
      if (C.Items.Count > 0)
      {
        C.SelectedIndex = 0;
      }
      C.Enabled = (C.Items.Count > 1);
      L.Enabled = (C.Items.Count > 1);
    }
    private void RegionsToCombo()
    {
      DataRow[] _regions;

      try
      {
        cmb_region.Items.Clear();
        if (m_dataset == null)
        {
          return;
        }
        _regions = m_dataset.Tables["Regions"].Select("", "Region.AutoSupported Desc, Region.Order Asc");
        foreach (DataRow _row in _regions)
        {
          cmb_region.Items.Add(new RowContainer(_row, "Region.Name"));
        }
      }
      finally
      {
        UpdateLabelCombo(cmb_region, lbl_region);
      }
    }
    private void CountriesToCombo(int RegionID)
    {
      DataRow[] _items;
      ComboBox _combo;
      Label _label;

      _combo = cmb_country;
      _label = lbl_country;

      try
      {
        _combo.Items.Clear();

        if (m_dataset == null)
        {
          return;
        }

        _items = m_dataset.Tables["Countries"].Select("Region.ID = " + RegionID.ToString(), "Country.Name Asc");

        foreach (DataRow _row in _items)
        {
          _combo.Items.Add(new RowContainer(_row, "Country.Name"));
        }
      }
      finally
      {
        UpdateLabelCombo(_combo, _label);
      }
    }
    private void StatesToCombo(int CountryID)
    {
      DataRow[] _items;
      ComboBox _combo;
      Label _label;

      _combo = cmb_state;
      _label = lbl_state;

      try
      {
        _combo.Items.Clear();
        if (m_dataset == null)
        {
          return;
        }
        _items = m_dataset.Tables["States"].Select("Country.ID = " + CountryID.ToString(), "State.Name Asc");

        foreach (DataRow _row in _items)
        {
          _combo.Items.Add(new RowContainer(_row, "State.Name"));
        }
      }
      finally
      {
        UpdateLabelCombo(_combo, _label);
      }
    }
    private void RefreshAutoCheck()
    {
      gb_state.Visible = !(chk_auto_detect.Checked);
      gb_regions.Visible = (chk_auto_detect.Checked);

      if (m_dataset == null)
      {
        return;
      }

      m_dataset.Tables["Config"].Rows[0]["AutoDetect"] = (chk_auto_detect.Checked);
      if (!chk_auto_detect.Checked)
      {
        RowContainer _state;
        _state = (RowContainer)cmb_state.SelectedItem;
        if (_state != null)
        {
          m_dataset.Tables["Config"].Rows[0]["StateID"] = _state.Row["State.ID"];
        }
      }
    }
    private void chk_auto_detect_CheckedChanged(object sender, EventArgs e)
    {
      RefreshAutoCheck();
    }
    private void cmb_region_SelectedIndexChanged(object sender, EventArgs e)
    {
      RowContainer _parent;

      _parent = (RowContainer)cmb_region.SelectedItem;
      if (_parent == null) return;
      CountriesToCombo((int)_parent.Row["Region.ID"]);
    }
    private void cmb_country_SelectedIndexChanged(object sender, EventArgs e)
    {
      RowContainer _parent;

      _parent = (RowContainer)cmb_country.SelectedItem;
      if (_parent == null) return;
      StatesToCombo((int)_parent.Row["Country.ID"]);
    }
    private void UpdateRegionsAutoEnabled(int Index, Boolean NewChecked)
    {
      RowContainer _r1;

      foreach (Object _item in chk_regions.Items)
      {
        _r1 = (RowContainer)_item;
        _r1.Row["Region.AutoEnabled"] = false;
      }
      foreach (Object _item in chk_regions.CheckedItems)
      {
        _r1 = (RowContainer)_item;
        _r1.Row["Region.AutoEnabled"] = true;
      }

      if (Index >= 0 && Index < chk_regions.Items.Count)
      {
        _r1 = (RowContainer)chk_regions.Items[Index];
        _r1.Row["Region.AutoEnabled"] = NewChecked;
      }
    }
    private void chk_regions_ItemCheck(object sender, ItemCheckEventArgs e)
    {
      UpdateRegionsAutoEnabled(e.Index, e.NewValue == CheckState.Checked);
    }
    private void cmb_state_SelectedIndexChanged(object sender, EventArgs e)
    {
      RowContainer _r1;

      _r1 = (RowContainer)cmb_state.SelectedItem;
      if (_r1 == null)
      {
        return;
      }
      m_dataset.Tables["Config"].Rows[0]["StateID"] = _r1.Row["State.ID"];
    }
    private void btn_scan_Click(object sender, EventArgs e)
    {
      IDCardScanner.Snapshell2.Scan(this, this.ScannedPersonalInfoHandler, this.StatusHandler);
    }

    private void btn_process_Click(object sender, EventArgs e)
    {
      IDCardScanner.Snapshell2.Process(this, this.ScannedPersonalInfoHandler, this.StatusHandler);
    }

    private void m_img_front_DoubleClick(object sender, EventArgs e)
    {
      RotateImage(sender);
    }

    private void m_img_back_DoubleClick(object sender, EventArgs e)
    {
      RotateImage(sender);
    }

    private void RotateImage(Object PBox)
    {
      try
      {
        PictureBox _pbox;

        if (PBox == null) return;
        if (!(PBox is PictureBox)) return;
        _pbox = (PictureBox)PBox;
        if (_pbox.Image == null) return;
        _pbox.Image.RotateFlip(RotateFlipType.Rotate180FlipNone);
      }
      catch
      { };
    }

    private void frm_id_scan_FormClosing(object sender, FormClosingEventArgs e)
    {
      switch (e.CloseReason)
      {
        case CloseReason.UserClosing:
          e.Cancel = true;
          this.Visible = false;
          break;

        default:
          break;
      }

    }

    private void m_img_x_Click(object sender, EventArgs e)
    {
      btn_scan_Click(sender, e);

    }
  }
}
