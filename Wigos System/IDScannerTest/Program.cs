﻿using System;
using WSI.IDScanner;
using WSI.IDScanner.Model;

namespace IDScannerTest
{
  class Program
  {
    static void Main()
    {
      using (IDScanner _idscanner = new IDScanner())
      {
        _idscanner.RefreshAllConnectors();
        string[] _connectors = _idscanner.MissingDrivers();

        //_idscanner.InstallAllMissing();
        _idscanner.InitializeAll();
        _idscanner.AttatchToAllAvailable(EventHandler);
        //_idscanner.ForceScan();
        Console.ReadLine();
      }
    }

    private static void EventHandler(IDScannerInfoEvent Event)
    {
      Console.WriteLine(Event.info.Fields.Count+@" items");
      foreach (var _field in Event.info.Fields)
      {
        Console.WriteLine(_field.Key + @"=" + _field.Value);
      }
    }
  }
}
