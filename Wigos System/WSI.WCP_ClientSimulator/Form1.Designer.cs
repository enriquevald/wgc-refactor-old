namespace WSI.WCP_ClientSimulator
{
  partial class frm_client_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.components = new System.ComponentModel.Container();
      this.btn_connect = new System.Windows.Forms.Button();
      this.lbl_wcp_service_ip_address = new System.Windows.Forms.Label();
      this.btn_start_rnd_simulation = new System.Windows.Forms.Button();
      this.lbl_simulation_plays = new System.Windows.Forms.Label();
      this.txt_pending_plays = new System.Windows.Forms.TextBox();
      this.txt_num_terminals = new System.Windows.Forms.TextBox();
      this.lbl_num_terminals = new System.Windows.Forms.Label();
      this.lbl_simulation = new System.Windows.Forms.Label();
      this.cmb_service_address = new System.Windows.Forms.ComboBox();
      this.lbl_trx = new System.Windows.Forms.Label();
      this.tmr_refresh = new System.Windows.Forms.Timer(this.components);
      this.chk_sim_plays = new System.Windows.Forms.CheckBox();
      this.cmb_prefix = new System.Windows.Forms.ComboBox();
      this.lbl_terminals_conn = new System.Windows.Forms.Label();
      this.grp_terminals_counter = new System.Windows.Forms.GroupBox();
      this.lbl_terminals_start_session = new System.Windows.Forms.Label();
      this.lbl_terminals_enrolled = new System.Windows.Forms.Label();
      this.txt_base_card = new System.Windows.Forms.TextBox();
      this.lbl_player_base_card = new System.Windows.Forms.Label();
      this.chk_time_plays = new System.Windows.Forms.CheckBox();
      this.txt_time_plays = new System.Windows.Forms.TextBox();
      this.lbl_time_bet_plays = new System.Windows.Forms.Label();
      this.grp_db_params = new System.Windows.Forms.GroupBox();
      this.btn_set_accounts = new System.Windows.Forms.Button();
      this.txt_db_host = new System.Windows.Forms.TextBox();
      this.lbl_db_host = new System.Windows.Forms.Label();
      this.txt_db_id = new System.Windows.Forms.TextBox();
      this.lbl_db_id = new System.Windows.Forms.Label();
      this.tmr_machine_meters = new System.Windows.Forms.Timer(this.components);
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.button2 = new System.Windows.Forms.Button();
      this.textBox3 = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.button4 = new System.Windows.Forms.Button();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.splitContainer4 = new System.Windows.Forms.SplitContainer();
      this.txt_statistics = new System.Windows.Forms.TextBox();
      this.btn_clr_statistics = new System.Windows.Forms.Button();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.cmb_mode = new System.Windows.Forms.ComboBox();
      this.grp_terminals_counter.SuspendLayout();
      this.grp_db_params.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.splitContainer4.Panel1.SuspendLayout();
      this.splitContainer4.Panel2.SuspendLayout();
      this.splitContainer4.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_connect
      // 
      this.btn_connect.Location = new System.Drawing.Point(110, 165);
      this.btn_connect.Name = "btn_connect";
      this.btn_connect.Size = new System.Drawing.Size(63, 23);
      this.btn_connect.TabIndex = 5;
      this.btn_connect.Text = "Connect";
      this.btn_connect.UseVisualStyleBackColor = true;
      this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
      // 
      // lbl_wcp_service_ip_address
      // 
      this.lbl_wcp_service_ip_address.AutoSize = true;
      this.lbl_wcp_service_ip_address.Location = new System.Drawing.Point(9, 47);
      this.lbl_wcp_service_ip_address.Name = "lbl_wcp_service_ip_address";
      this.lbl_wcp_service_ip_address.Size = new System.Drawing.Size(112, 13);
      this.lbl_wcp_service_ip_address.TabIndex = 3;
      this.lbl_wcp_service_ip_address.Text = "WCP Service Address";
      // 
      // btn_start_rnd_simulation
      // 
      this.btn_start_rnd_simulation.Location = new System.Drawing.Point(884, 109);
      this.btn_start_rnd_simulation.Name = "btn_start_rnd_simulation";
      this.btn_start_rnd_simulation.Size = new System.Drawing.Size(143, 24);
      this.btn_start_rnd_simulation.TabIndex = 32;
      this.btn_start_rnd_simulation.Text = "Start/Stop Simulation";
      this.btn_start_rnd_simulation.UseVisualStyleBackColor = true;
      this.btn_start_rnd_simulation.Click += new System.EventHandler(this.btn_start_rnd_simulation_Click);
      // 
      // lbl_simulation_plays
      // 
      this.lbl_simulation_plays.AutoSize = true;
      this.lbl_simulation_plays.Location = new System.Drawing.Point(876, 0);
      this.lbl_simulation_plays.Name = "lbl_simulation_plays";
      this.lbl_simulation_plays.Size = new System.Drawing.Size(150, 13);
      this.lbl_simulation_plays.TabIndex = 36;
      this.lbl_simulation_plays.Text = "Simulation Plays (per Terminal)";
      // 
      // txt_pending_plays
      // 
      this.txt_pending_plays.Location = new System.Drawing.Point(894, 14);
      this.txt_pending_plays.MaxLength = 8;
      this.txt_pending_plays.Name = "txt_pending_plays";
      this.txt_pending_plays.Size = new System.Drawing.Size(123, 20);
      this.txt_pending_plays.TabIndex = 37;
      this.txt_pending_plays.Text = "2";
      this.txt_pending_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // txt_num_terminals
      // 
      this.txt_num_terminals.Location = new System.Drawing.Point(12, 167);
      this.txt_num_terminals.MaxLength = 5;
      this.txt_num_terminals.Name = "txt_num_terminals";
      this.txt_num_terminals.Size = new System.Drawing.Size(87, 20);
      this.txt_num_terminals.TabIndex = 4;
      this.txt_num_terminals.Text = "10";
      this.txt_num_terminals.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // lbl_num_terminals
      // 
      this.lbl_num_terminals.AutoSize = true;
      this.lbl_num_terminals.Location = new System.Drawing.Point(9, 151);
      this.lbl_num_terminals.Name = "lbl_num_terminals";
      this.lbl_num_terminals.Size = new System.Drawing.Size(80, 13);
      this.lbl_num_terminals.TabIndex = 41;
      this.lbl_num_terminals.Text = "Num. Terminals";
      // 
      // lbl_simulation
      // 
      this.lbl_simulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_simulation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
      this.lbl_simulation.Location = new System.Drawing.Point(844, 82);
      this.lbl_simulation.Name = "lbl_simulation";
      this.lbl_simulation.Size = new System.Drawing.Size(228, 28);
      this.lbl_simulation.TabIndex = 44;
      this.lbl_simulation.Text = "Stopped";
      this.lbl_simulation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // cmb_service_address
      // 
      this.cmb_service_address.FormattingEnabled = true;
      this.cmb_service_address.Items.AddRange(new object[] {
            "192.168.1.12:13000",
            "192.168.1.8:13000",
            "192.168.1.30:13000",
            "192.168.1.4:13000",
            "192.168.1.7:13000",
            "192.168.1.5:13000",
            "80.32.96.92:13000"});
      this.cmb_service_address.Location = new System.Drawing.Point(12, 62);
      this.cmb_service_address.Name = "cmb_service_address";
      this.cmb_service_address.Size = new System.Drawing.Size(138, 21);
      this.cmb_service_address.TabIndex = 2;
      this.cmb_service_address.Text = "192.168.1.12:13000";
      // 
      // lbl_trx
      // 
      this.lbl_trx.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_trx.ForeColor = System.Drawing.Color.Blue;
      this.lbl_trx.Location = new System.Drawing.Point(915, 139);
      this.lbl_trx.Name = "lbl_trx";
      this.lbl_trx.Size = new System.Drawing.Size(195, 39);
      this.lbl_trx.TabIndex = 47;
      this.lbl_trx.Text = "Avg: 10.00 Trx/s \r\nNow: 10.00 Trx/s";
      this.lbl_trx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tmr_refresh
      // 
      this.tmr_refresh.Enabled = true;
      this.tmr_refresh.Interval = 1000;
      this.tmr_refresh.Tick += new System.EventHandler(this.tmr_refresh_Tick);
      // 
      // chk_sim_plays
      // 
      this.chk_sim_plays.AutoSize = true;
      this.chk_sim_plays.Checked = true;
      this.chk_sim_plays.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_sim_plays.Location = new System.Drawing.Point(873, 19);
      this.chk_sim_plays.Name = "chk_sim_plays";
      this.chk_sim_plays.Size = new System.Drawing.Size(15, 14);
      this.chk_sim_plays.TabIndex = 48;
      this.chk_sim_plays.UseVisualStyleBackColor = true;
      this.chk_sim_plays.CheckedChanged += new System.EventHandler(this.chk_sim_plays_CheckedChanged);
      // 
      // cmb_prefix
      // 
      this.cmb_prefix.FormattingEnabled = true;
      this.cmb_prefix.Items.AddRange(new object[] {
            "ANDREU",
            "ALBERTO",
            "RONALD",
            "TONI",
            "MERCE",
            "FAT1",
            "FAT2"});
      this.cmb_prefix.Location = new System.Drawing.Point(12, 19);
      this.cmb_prefix.Name = "cmb_prefix";
      this.cmb_prefix.Size = new System.Drawing.Size(138, 21);
      this.cmb_prefix.TabIndex = 1;
      this.cmb_prefix.Text = "ANDREU";
      this.cmb_prefix.SelectedIndexChanged += new System.EventHandler(this.cmb_prefix_SelectedIndexChanged);
      this.cmb_prefix.TextChanged += new System.EventHandler(this.cmb_prefix_TextChanged);
      // 
      // lbl_terminals_conn
      // 
      this.lbl_terminals_conn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_terminals_conn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
      this.lbl_terminals_conn.Location = new System.Drawing.Point(12, 29);
      this.lbl_terminals_conn.Name = "lbl_terminals_conn";
      this.lbl_terminals_conn.Size = new System.Drawing.Size(189, 23);
      this.lbl_terminals_conn.TabIndex = 53;
      this.lbl_terminals_conn.Text = "0 Connected";
      this.lbl_terminals_conn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // grp_terminals_counter
      // 
      this.grp_terminals_counter.Controls.Add(this.lbl_terminals_start_session);
      this.grp_terminals_counter.Controls.Add(this.lbl_terminals_enrolled);
      this.grp_terminals_counter.Controls.Add(this.lbl_terminals_conn);
      this.grp_terminals_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.grp_terminals_counter.ForeColor = System.Drawing.Color.Black;
      this.grp_terminals_counter.Location = new System.Drawing.Point(15, 209);
      this.grp_terminals_counter.Name = "grp_terminals_counter";
      this.grp_terminals_counter.Size = new System.Drawing.Size(216, 111);
      this.grp_terminals_counter.TabIndex = 54;
      this.grp_terminals_counter.TabStop = false;
      this.grp_terminals_counter.Text = " Terminals ";
      // 
      // lbl_terminals_start_session
      // 
      this.lbl_terminals_start_session.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_terminals_start_session.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
      this.lbl_terminals_start_session.Location = new System.Drawing.Point(12, 77);
      this.lbl_terminals_start_session.Name = "lbl_terminals_start_session";
      this.lbl_terminals_start_session.Size = new System.Drawing.Size(189, 23);
      this.lbl_terminals_start_session.TabIndex = 55;
      this.lbl_terminals_start_session.Text = "0 Playing";
      this.lbl_terminals_start_session.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_terminals_enrolled
      // 
      this.lbl_terminals_enrolled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_terminals_enrolled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
      this.lbl_terminals_enrolled.Location = new System.Drawing.Point(12, 52);
      this.lbl_terminals_enrolled.Name = "lbl_terminals_enrolled";
      this.lbl_terminals_enrolled.Size = new System.Drawing.Size(189, 23);
      this.lbl_terminals_enrolled.TabIndex = 54;
      this.lbl_terminals_enrolled.Text = "0 Enrolled";
      this.lbl_terminals_enrolled.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // txt_base_card
      // 
      this.txt_base_card.Location = new System.Drawing.Point(12, 107);
      this.txt_base_card.MaxLength = 18;
      this.txt_base_card.Name = "txt_base_card";
      this.txt_base_card.Size = new System.Drawing.Size(138, 20);
      this.txt_base_card.TabIndex = 3;
      this.txt_base_card.Text = "100000000000000000";
      // 
      // lbl_player_base_card
      // 
      this.lbl_player_base_card.AutoSize = true;
      this.lbl_player_base_card.Location = new System.Drawing.Point(12, 91);
      this.lbl_player_base_card.Name = "lbl_player_base_card";
      this.lbl_player_base_card.Size = new System.Drawing.Size(56, 13);
      this.lbl_player_base_card.TabIndex = 55;
      this.lbl_player_base_card.Text = "Base Card";
      // 
      // chk_time_plays
      // 
      this.chk_time_plays.AutoSize = true;
      this.chk_time_plays.Checked = true;
      this.chk_time_plays.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chk_time_plays.Location = new System.Drawing.Point(873, 59);
      this.chk_time_plays.Name = "chk_time_plays";
      this.chk_time_plays.Size = new System.Drawing.Size(15, 14);
      this.chk_time_plays.TabIndex = 60;
      this.chk_time_plays.UseVisualStyleBackColor = true;
      this.chk_time_plays.CheckedChanged += new System.EventHandler(this.chk_time_plays_CheckedChanged);
      // 
      // txt_time_plays
      // 
      this.txt_time_plays.Location = new System.Drawing.Point(894, 54);
      this.txt_time_plays.MaxLength = 8;
      this.txt_time_plays.Name = "txt_time_plays";
      this.txt_time_plays.Size = new System.Drawing.Size(123, 20);
      this.txt_time_plays.TabIndex = 59;
      this.txt_time_plays.Text = "50";
      this.txt_time_plays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // lbl_time_bet_plays
      // 
      this.lbl_time_bet_plays.AutoSize = true;
      this.lbl_time_bet_plays.Location = new System.Drawing.Point(876, 40);
      this.lbl_time_bet_plays.Name = "lbl_time_bet_plays";
      this.lbl_time_bet_plays.Size = new System.Drawing.Size(173, 13);
      this.lbl_time_bet_plays.TabIndex = 58;
      this.lbl_time_bet_plays.Text = "Time between Plays (millisecconds)";
      // 
      // grp_db_params
      // 
      this.grp_db_params.Controls.Add(this.btn_set_accounts);
      this.grp_db_params.Controls.Add(this.txt_db_host);
      this.grp_db_params.Controls.Add(this.lbl_db_host);
      this.grp_db_params.Controls.Add(this.txt_db_id);
      this.grp_db_params.Controls.Add(this.lbl_db_id);
      this.grp_db_params.Location = new System.Drawing.Point(390, 14);
      this.grp_db_params.Name = "grp_db_params";
      this.grp_db_params.Size = new System.Drawing.Size(185, 103);
      this.grp_db_params.TabIndex = 125;
      this.grp_db_params.TabStop = false;
      this.grp_db_params.Text = "Database Parameters";
      // 
      // btn_set_accounts
      // 
      this.btn_set_accounts.Location = new System.Drawing.Point(52, 72);
      this.btn_set_accounts.Name = "btn_set_accounts";
      this.btn_set_accounts.Size = new System.Drawing.Size(85, 23);
      this.btn_set_accounts.TabIndex = 3;
      this.btn_set_accounts.Text = "Set Accounts";
      this.btn_set_accounts.UseVisualStyleBackColor = true;
      this.btn_set_accounts.Click += new System.EventHandler(this.btn_set_accounts_Click);
      // 
      // txt_db_host
      // 
      this.txt_db_host.Location = new System.Drawing.Point(59, 20);
      this.txt_db_host.MaxLength = 18;
      this.txt_db_host.Name = "txt_db_host";
      this.txt_db_host.Size = new System.Drawing.Size(121, 20);
      this.txt_db_host.TabIndex = 1;
      // 
      // lbl_db_host
      // 
      this.lbl_db_host.AutoSize = true;
      this.lbl_db_host.Location = new System.Drawing.Point(10, 23);
      this.lbl_db_host.Name = "lbl_db_host";
      this.lbl_db_host.Size = new System.Drawing.Size(47, 13);
      this.lbl_db_host.TabIndex = 10;
      this.lbl_db_host.Text = "DB Host";
      // 
      // txt_db_id
      // 
      this.txt_db_id.Location = new System.Drawing.Point(59, 44);
      this.txt_db_id.MaxLength = 18;
      this.txt_db_id.Name = "txt_db_id";
      this.txt_db_id.Size = new System.Drawing.Size(72, 20);
      this.txt_db_id.TabIndex = 2;
      // 
      // lbl_db_id
      // 
      this.lbl_db_id.AutoSize = true;
      this.lbl_db_id.Location = new System.Drawing.Point(22, 47);
      this.lbl_db_id.Name = "lbl_db_id";
      this.lbl_db_id.Size = new System.Drawing.Size(34, 13);
      this.lbl_db_id.TabIndex = 11;
      this.lbl_db_id.Text = "DB Id";
      // 
      // tmr_machine_meters
      // 
      this.tmr_machine_meters.Interval = 250;
      this.tmr_machine_meters.Tick += new System.EventHandler(this.tmr_machine_meters_Tick);
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
      this.label8.Location = new System.Drawing.Point(95, 14);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(229, 16);
      this.label8.TabIndex = 79;
      this.label8.Text = "Messages to validate or cancel ticket";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label9.Location = new System.Drawing.Point(157, 74);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(127, 13);
      this.label9.TabIndex = 78;
      this.label9.Text = "(Only for validation check)";
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(103, 43);
      this.textBox1.MaxLength = 18;
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(51, 20);
      this.textBox1.TabIndex = 77;
      // 
      // textBox2
      // 
      this.textBox2.Location = new System.Drawing.Point(104, 69);
      this.textBox2.MaxLength = 18;
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new System.Drawing.Size(50, 20);
      this.textBox2.TabIndex = 76;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(45, 73);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 16);
      this.label10.TabIndex = 75;
      this.label10.Text = "Amount";
      // 
      // button2
      // 
      this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.button2.Location = new System.Drawing.Point(290, 33);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(123, 23);
      this.button2.TabIndex = 74;
      this.button2.Text = "IsValidTicket";
      this.button2.UseVisualStyleBackColor = true;
      // 
      // textBox3
      // 
      this.textBox3.Location = new System.Drawing.Point(160, 43);
      this.textBox3.MaxLength = 18;
      this.textBox3.Name = "textBox3";
      this.textBox3.Size = new System.Drawing.Size(121, 20);
      this.textBox3.TabIndex = 3;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(5, 46);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(96, 16);
      this.label11.TabIndex = 2;
      this.label11.Text = "Ticket Number";
      // 
      // button4
      // 
      this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.button4.Location = new System.Drawing.Point(291, 60);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(123, 23);
      this.button4.TabIndex = 0;
      this.button4.Text = "CancelTicket";
      this.button4.UseVisualStyleBackColor = true;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.splitContainer4);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(1145, 393);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Statistics";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // splitContainer4
      // 
      this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer4.Location = new System.Drawing.Point(3, 3);
      this.splitContainer4.Name = "splitContainer4";
      this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer4.Panel1
      // 
      this.splitContainer4.Panel1.Controls.Add(this.txt_statistics);
      // 
      // splitContainer4.Panel2
      // 
      this.splitContainer4.Panel2.Controls.Add(this.btn_clr_statistics);
      this.splitContainer4.Size = new System.Drawing.Size(1139, 387);
      this.splitContainer4.SplitterDistance = 281;
      this.splitContainer4.TabIndex = 0;
      // 
      // txt_statistics
      // 
      this.txt_statistics.Dock = System.Windows.Forms.DockStyle.Fill;
      this.txt_statistics.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_statistics.Location = new System.Drawing.Point(0, 0);
      this.txt_statistics.Multiline = true;
      this.txt_statistics.Name = "txt_statistics";
      this.txt_statistics.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txt_statistics.Size = new System.Drawing.Size(1139, 281);
      this.txt_statistics.TabIndex = 0;
      this.txt_statistics.WordWrap = false;
      // 
      // btn_clr_statistics
      // 
      this.btn_clr_statistics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_clr_statistics.Location = new System.Drawing.Point(1052, 3);
      this.btn_clr_statistics.Name = "btn_clr_statistics";
      this.btn_clr_statistics.Size = new System.Drawing.Size(84, 23);
      this.btn_clr_statistics.TabIndex = 110;
      this.btn_clr_statistics.Text = "Clear Statistics";
      this.btn_clr_statistics.UseVisualStyleBackColor = true;
      this.btn_clr_statistics.Click += new System.EventHandler(this.btn_clr_statistics_Click);
      // 
      // tabControl1
      // 
      this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Location = new System.Drawing.Point(0, 326);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(1153, 419);
      this.tabControl1.TabIndex = 122;
      // 
      // cmb_mode
      // 
      this.cmb_mode.FormattingEnabled = true;
      this.cmb_mode.Items.AddRange(new object[] {
            "CASHLESS",
            "TITO"});
      this.cmb_mode.Location = new System.Drawing.Point(183, 20);
      this.cmb_mode.Name = "cmb_mode";
      this.cmb_mode.Size = new System.Drawing.Size(121, 21);
      this.cmb_mode.TabIndex = 138;
      // 
      // frm_client_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1156, 742);
      this.Controls.Add(this.cmb_mode);
      this.Controls.Add(this.grp_db_params);
      this.Controls.Add(this.tabControl1);
      this.Controls.Add(this.chk_time_plays);
      this.Controls.Add(this.txt_time_plays);
      this.Controls.Add(this.lbl_time_bet_plays);
      this.Controls.Add(this.txt_base_card);
      this.Controls.Add(this.lbl_player_base_card);
      this.Controls.Add(this.grp_terminals_counter);
      this.Controls.Add(this.cmb_prefix);
      this.Controls.Add(this.chk_sim_plays);
      this.Controls.Add(this.lbl_trx);
      this.Controls.Add(this.cmb_service_address);
      this.Controls.Add(this.btn_start_rnd_simulation);
      this.Controls.Add(this.lbl_simulation);
      this.Controls.Add(this.lbl_num_terminals);
      this.Controls.Add(this.txt_num_terminals);
      this.Controls.Add(this.txt_pending_plays);
      this.Controls.Add(this.lbl_simulation_plays);
      this.Controls.Add(this.lbl_wcp_service_ip_address);
      this.Controls.Add(this.btn_connect);
      this.Name = "frm_client_simulator";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "WSI.WCP.ClientSimulator ";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_client_simulator_FormClosed);
      this.grp_terminals_counter.ResumeLayout(false);
      this.grp_db_params.ResumeLayout(false);
      this.grp_db_params.PerformLayout();
      this.tabPage2.ResumeLayout(false);
      this.splitContainer4.Panel1.ResumeLayout(false);
      this.splitContainer4.Panel1.PerformLayout();
      this.splitContainer4.Panel2.ResumeLayout(false);
      this.splitContainer4.ResumeLayout(false);
      this.tabControl1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }



    #endregion

    private System.Windows.Forms.Button btn_connect;
    private System.Windows.Forms.Label lbl_wcp_service_ip_address;
    private System.Windows.Forms.Button btn_start_rnd_simulation;
    private System.Windows.Forms.Label lbl_simulation_plays;
    private System.Windows.Forms.TextBox txt_pending_plays;
    private System.Windows.Forms.TextBox txt_num_terminals;
    private System.Windows.Forms.Label lbl_num_terminals;
    private System.Windows.Forms.Label lbl_simulation;
    private System.Windows.Forms.ComboBox cmb_service_address;
    private System.Windows.Forms.Label lbl_trx;
    private System.Windows.Forms.Timer tmr_refresh;
    private System.Windows.Forms.CheckBox chk_sim_plays;
    private System.Windows.Forms.ComboBox cmb_prefix;
    private System.Windows.Forms.Label lbl_terminals_conn;
    private System.Windows.Forms.GroupBox grp_terminals_counter;
    private System.Windows.Forms.Label lbl_terminals_start_session;
    private System.Windows.Forms.Label lbl_terminals_enrolled;
    private System.Windows.Forms.TextBox txt_base_card;
    private System.Windows.Forms.Label lbl_player_base_card;
    private System.Windows.Forms.CheckBox chk_time_plays;
    private System.Windows.Forms.TextBox txt_time_plays;
    private System.Windows.Forms.Label lbl_time_bet_plays;
    private System.Windows.Forms.GroupBox grp_db_params;
    private System.Windows.Forms.TextBox txt_db_id;
    private System.Windows.Forms.Label lbl_db_id;
    private System.Windows.Forms.Button btn_set_accounts;
    private System.Windows.Forms.TextBox txt_db_host;
    private System.Windows.Forms.Label lbl_db_host;
    private System.Windows.Forms.Timer tmr_machine_meters;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.TextBox textBox3;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.SplitContainer splitContainer4;
    private System.Windows.Forms.TextBox txt_statistics;
    private System.Windows.Forms.Button btn_clr_statistics;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.ComboBox cmb_mode;
  }
}

