using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WSI.WCP;
using WSI.Common;
using System.Threading;

namespace WSI.WCP_ClientSimulator
{
  class EgmMachine : SimulatedTerminal
  {
    public static int m_instances = 0;
    public int m_instance_id;

    public static Queue<string> m_global_ticket = new Queue<string>();



    public Int64 promo_nr_cents = 0;
    public Int64 promo_re_cents;
    public Int64 re_cents = 1000;

    public Int64 meter_played;
    public Int64 meter_re_played;
    public Int64 meter_nr_played;
    public Int64 meter_won;
    public Int64 meter_nplayed;
    public Int64 meter_nwon;


    public Int64 m_session_id = 0;
    public Int64 m_session_in_bill = 0;
    public Int64 m_session_in_ticket_re = 0;
    public Int64 m_session_in_ticket_promo_re = 0;
    public Int64 m_session_in_ticket_promo_nr = 0;
    public Int64 m_session_out_ticket_re = 0;
    public Int64 m_session_out_ticket_promo_re = 0;
    public Int64 m_session_out_ticket_promo_nr = 0;

    public Int64 m_session_played = 0;
    public Int64 m_session_played_nr = 0;
    public Int64 m_session_won = 0;
    public Int64 m_session_num_plays = 0;
    public Int64 m_session_num_won = 0;

    public int m_last_bill = 0;
    public int m_tick_money_in = 0;
    public int m_delay_money_in = 0;
    private int m_tick_last_play = 0;
    public int tick_cashout = 0;

    Random m_random;

    public EgmMachine()
    {
      m_instance_id = 1 + Interlocked.Increment(ref m_instances);
      m_random = new Random(m_instance_id * Misc.GetTickCount());
    }

    void CreateLocalSession()
    {
      lock (this)
      {
        if (m_session_id == 0)
        {
          m_session_id = -1;
        }
      }
    }

    public void ProcessCashOut()
    {
      Int64[] _cents;
      TITO_TICKET_TYPE[] _type = new TITO_TICKET_TYPE[] { TITO_TICKET_TYPE.CASHABLE, TITO_TICKET_TYPE.PROMO_REDEEM, TITO_TICKET_TYPE.PROMO_NONREDEEM };

      _cents = new long[3];

      if (Misc.GetElapsedTicks(tick_cashout) < 10)
      {
        return;
      }
      if (IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_TITO_MsgValidationNumber))
      {
        return;
      }
      if (IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_TITO_MsgCreateTicket))
      {
        return;
      }


      if (!CashOut(out _cents[0], out _cents[1], out _cents[2]))
      {
        ReportPlay();

        return;
      }

      for (int _i = 0; _i < _cents.Length; _i++)
      {
        if (_cents[_i] <= 0)
        {
          continue;
        }

        WCP_Message _wcp_request;
        WCP_TITO_MsgValidationNumber _request;

        _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgValidationNumber);
        _request = (WCP_TITO_MsgValidationNumber)_wcp_request.MsgContent;
        _request.TicketType = (Int32)_type[_i];
        _request.AmountCents = _cents[_i];

        tick_cashout = Misc.GetTickCount();
        SendRequest(_wcp_request);
      }
    }






    private void ReportPlay()
    {
      if (Misc.GetElapsedTicks(m_tick_last_play) < 100)
      {
        return;
      }
      if (IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_MsgReportPlay))
      {
        return;
      }
      WCP_Message _wcp_request;
      WCP_MsgReportPlay _request;

      _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgReportPlay);
      _request = (WCP_MsgReportPlay)_wcp_request.MsgContent;

      _request.CardSessionId = 0;
      _request.Denomination = 1;
      _request.GameId = "EGM-001";
      _request.PayoutPercentage = 95.00;
      _request.PlayedAmount = 100;
      _request.SessionBalanceAfterPlay = 0;
      _request.SessionBalanceBeforePlay = 0;
      _request.TransactionId = Interlocked.Increment(ref this.transaction_id);
      _request.WonAmount = 100;


      m_tick_last_play = Misc.GetTickCount();
      SendRequest(_wcp_request);
    }


    void BillIn(Int64 Bill)
    {
      lock (this)
      {
        CreateLocalSession();
        AddCredit(Bill, 0, 0);
        m_session_in_bill += Bill;
      }
    }

    void AddCredit(Int64 Redimible, Int64 PromoRedimible, Int64 PromoNoRedimible)
    {
      lock (this)
      {
        CreateLocalSession();
        re_cents += Redimible;
        promo_re_cents += PromoRedimible;
        promo_nr_cents += PromoNoRedimible;

        m_tick_money_in = Misc.GetTickCount();
        m_delay_money_in = m_random.Next(50000);
      }
    }

    Boolean CashOut(out Int64 Redimible, out Int64 PromoRedimible, out Int64 PromoNoRedimible)
    {
      Redimible = 0;
      PromoRedimible = 0;
      PromoNoRedimible = 0;

      if (Misc.GetElapsedTicks(m_tick_money_in) < m_delay_money_in)
      {
        return false;
      }

      lock (this)
      {
        if (re_cents + promo_re_cents + promo_nr_cents > 0)
        {
          CreateLocalSession();
          Redimible = re_cents; re_cents = 0;
          PromoRedimible = promo_re_cents; promo_re_cents = 0;
          PromoNoRedimible = promo_nr_cents; promo_nr_cents = 0;

          return true;
        }
      }

      return false;
    }

    void Play(Int64 Bet, Int64 Won)
    {
      Int64 _bet;
      Int64 _delta;

      lock (this)
      {
        CreateLocalSession();

        _bet = Bet;
        _delta = Math.Min(promo_nr_cents, _bet); promo_nr_cents -= _delta; _bet -= _delta; meter_nr_played += _delta;
        m_session_played_nr += _delta;


        _delta = Math.Min(promo_re_cents, _bet); promo_re_cents -= _delta; _bet -= _delta; meter_re_played += _delta;
        _delta = Math.Min(re_cents, _bet); re_cents -= _delta; _bet -= _delta; meter_re_played += _delta;

        re_cents += Won;

        meter_nplayed += 1;
        meter_played += Bet;
        m_session_played += Bet;
        m_session_num_plays += 1;

        if (Won > 0)
        {
          meter_won += Won;
          meter_nwon += 1;

          m_session_won += Won;
          m_session_num_won += 1;
        }
      }
    }



    protected override void ProcessResponse(WCP_Message WcpRequest, WCP_Message WcpResponse)
    {
      if (WcpResponse.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_OK
          && WcpResponse.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        return;
      }

      switch (WcpResponse.MsgHeader.MsgType)
      {
        case WCP_MsgTypes.WCP_MsgReportPlay: break;
        case WCP_MsgTypes.WCP_TITO_MsgCancelTicketReply: break;


        case WCP_MsgTypes.WCP_TITO_MsgValidationNumberReply:
          {
            WCP_TITO_MsgValidationNumber _request;
            WCP_TITO_MsgValidationNumberReply _response;

            _request = (WCP_TITO_MsgValidationNumber)WcpRequest.MsgContent;
            _response = (WCP_TITO_MsgValidationNumberReply)WcpResponse.MsgContent;

            WCP_Message _new_request;
            WCP_TITO_MsgCreateTicket _create_ticket;

            _new_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgCreateTicket);

            _create_ticket = (WCP_TITO_MsgCreateTicket)_new_request.MsgContent;

            _create_ticket.TransactionId = this.transaction_id;
            this.transaction_id++;

            _create_ticket.ValidationType = (int)TITO_VALIDATION_TYPE.SYSTEM;
            _create_ticket.Date = DateTime.Now;
            _create_ticket.SystemId = _response.system_id;
            _create_ticket.ValidationNumberSequence = _response.validation_number_sequence;
            _create_ticket.AmountCents = _request.AmountCents;
            _create_ticket.ExpirationDate = _create_ticket.Date.AddDays(30).Date;

            SendRequest(_new_request);
          }
          break;

        case WCP_MsgTypes.WCP_TITO_MsgCreateTicketReply:
          {
            WCP_TITO_MsgCreateTicket _request;
            Int64 _number;
            _request = (WCP_TITO_MsgCreateTicket)WcpRequest.MsgContent;
            _number = (_request.SystemId % 100) * 10000000000000000L + _request.ValidationNumberSequence % 10000000000000000L;
            lock (m_global_ticket)
            {
              m_global_ticket.Enqueue(_number.ToString("000000000000000000"));
            }
          }
          break;


        case WCP_MsgTypes.WCP_TITO_MsgIsValidTicketReply:
          {
            WCP_TITO_MsgIsValidTicket _request;
            WCP_TITO_MsgIsValidTicketReply _response;
            Int64 _re;
            Int64 _promo_re;
            Int64 _promo_nr;

            _request = (WCP_TITO_MsgIsValidTicket)WcpRequest.MsgContent;
            _response = (WCP_TITO_MsgIsValidTicketReply)WcpResponse.MsgContent;

            _re = 0;
            _promo_re = 0;
            _promo_nr = 0;

            switch (_response.ValidateStatus)
            {
              case (int)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_CASHABLE: _re = _response.AmountCents; break;
              case (int)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_NONRESTRICTED_PROMOTIONAL_TICKET: _promo_re = _response.AmountCents; break;
              case (int)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_RESTRICTED_PROMOTIONAL_TICKET: _promo_nr = _response.AmountCents; break;
              default:
                break;
            }

            if (_re + _promo_re + _promo_nr == 0)
            {
              return;
            }

            AddCredit(_re, _promo_re, _promo_nr);

            m_session_in_ticket_re += _re;
            m_session_in_ticket_promo_re += _promo_re;
            m_session_in_ticket_promo_nr += _promo_nr;

            WCP_Message _wcp_request;
            WCP_TITO_MsgCancelTicket _cancel;

            _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgCancelTicket);
            _cancel = (WCP_TITO_MsgCancelTicket)_wcp_request.MsgContent;

            _wcp_request.MsgContent = _cancel;
            _cancel.StopCancellation = false;
            _cancel.SystemId = _request.SystemId;
            _cancel.ValidationNumberSequence = _request.ValidationNumberSequence;

            SendRequest(_wcp_request);
          }
          break;


      } // switch
    }

    public void MoneyToEgm()
    {
      Boolean _ticket_was_inserted;
      String _str_validation_number;

      if (IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_TITO_MsgIsValidTicket))
      {
        return;
      }

      _ticket_was_inserted = false;

      _str_validation_number = null;
      if (!_ticket_was_inserted)
      {
        lock (m_global_ticket)
        {
          if (m_global_ticket.Count > 0)
          {
            _str_validation_number = m_global_ticket.Dequeue();
          }
        }
      }

      if (String.IsNullOrEmpty(_str_validation_number))
      {
        if (Misc.GetElapsedTicks(m_last_bill) > 1000)
        {
          BillIn(10000);
          m_last_bill = Misc.GetTickCount();

          return;
        }
      }
      else
      {
        WCP_Message _wcp_request;
        WCP_TITO_MsgIsValidTicket _request;
        Int64 _validation_number;

        _validation_number = Int64.Parse(_str_validation_number);

        _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_TITO_MsgIsValidTicket);
        _request = (WCP_TITO_MsgIsValidTicket)_wcp_request.MsgContent;
        _wcp_request.MsgContent = _request;

        _request.AmountCents = 0;
        _request.SystemId = (int)(_validation_number / 10000000000000000);
        _request.ValidationNumberSequence = _validation_number % 10000000000000000;

        SendRequest(_wcp_request);
      }
    }

  }
}
