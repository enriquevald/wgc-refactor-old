using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.WCP_ClientSimulator
{
  class SasMeter
  {
    const Int64 MAX_CYCLE = 1000000000000000000L;
    private static Boolean m_extended_meters = false;

    private int m_meter_code = 0;
    private int m_game_id = 0;
    private Int64 m_value = 0L;

     internal  static List<int> SasGameMeterCodes()
    { 
      int [] _game_meters = new int [] { 0, 1, 2, 5, 6, 7, 28, 29, 30, 31, 32, 33, 34, 127 };

      List<int> _meters;

      _meters = new List<int>();

      foreach (int _code in _game_meters)
      {
        _meters.Add(_code);
      }

      return _meters;
    }

     internal static List<int> SasMachineMeterCodes()
    {
      List<int> _meters;

      _meters = new List<int>();

      for (int _code = 0; _code <= 189; _code++)
      {
        if ((_code > 57 && _code < 62)
            || (_code > 120 && _code < 127)
            || (_code > 147 && _code < 160)
            || (_code > 177 && _code < 184))
        {
          continue;
        }

        _meters.Add(_code);
      }

      return _meters;
    }


    private static int MinMeterSize(int MeterCode)
    {
      switch (MeterCode)
      {
        case 0x000D:
        case 0x000E:
        case 0x000F:
        case 0x0010:

        case 0x0080:
        case 0x0082:
        case 0x0084:
        case 0x0086:
        case 0x0088:
        case 0x008A:
        case 0x008C:
        case 0x008E:
        case 0x0090:
        case 0x0092:

        case 0x00A0:
        case 0x00A2:
        case 0x00A4:
        case 0x00A6:
        case 0x00A8:
        case 0x00AA:
        case 0x00AC:
        case 0x00AE:

        case 0x00B0:
        case 0x00B8:
        case 0x00BA:
          return 5;

        default:
          if (MeterCode >= 0 && MeterCode <= 255) return 4;
          if (MeterCode >= 0x10000 && MeterCode < 0x20000) return 5;
          return 0;
      }
    }

    private static Int64 GetCycle(int NumBCDs)
    {
      Int64 _cycle;

      if (NumBCDs <= 0) return 0;

      _cycle = 1;
      while (NumBCDs > 0)
      {
        _cycle *= 100;
      }

      return _cycle;
    }





    public SasMeter(int MeterCode)
    {
      m_meter_code = MeterCode;
      m_value = 0;

    }


    public void MeterSize (out int NumBCD, out Int64 Cycle)
    {
      NumBCD = MinMeterSize(this.m_meter_code);
      if (m_extended_meters)
      { 
        NumBCD += 2;
      }
      Cycle  = GetCycle (NumBCD);
    }




    public void Increment(Int64 Delta)
    {
      m_value = (m_value + Delta) % MAX_CYCLE;
      if (m_value < 0) m_value = (m_value + MAX_CYCLE) % MAX_CYCLE;
    }
  }

  class SasMeterList : Dictionary<int, SasMeter>
  {
    public void Increment(String MeterName, Int64 Delta)
    {
      int _meter_code;

      _meter_code = 0; // TODO: get the meter code
      Increment(_meter_code, Delta);
    }
    public void Increment(int MeterCode, Int64 Delta)
    {
      lock (this)
      {
        SasMeter _meter;

        if (this.ContainsKey(MeterCode))
        {
          _meter = this[MeterCode];
        }
        else
        {
          _meter = new SasMeter(MeterCode);
          this.Add(MeterCode, _meter);
        }
        _meter.Increment(Delta);

      }
    }

    internal static SasMeterList MachineMeters
    {
      get
      {
        SasMeterList _list;

        _list = new SasMeterList();

        foreach (int _code in SasMeter.SasMachineMeterCodes())
        {
          _list.Add(_code, new SasMeter(_code));
        }

        return _list;
      }
    }

    internal static SasMeterList GameMeters
    {
      get
      {
        SasMeterList _list;

        _list = new SasMeterList();

        foreach (int _code in SasMeter.SasGameMeterCodes())
        {
          _list.Add(_code, new SasMeter(_code));
        }

        return _list;
      }
    }
  };



  class SasMeterCollection : Dictionary<int, SasMeterList>
  {
    private void AddMachineMeters()
    {
      this.Add(0, SasMeterList.MachineMeters);
    }

    private void AddGameMeters(int GameCode)
    {
      this.Add(GameCode, SasMeterList.GameMeters);
    }


    public void Increment(int MeterCode, Int64 Delta)
    {
      this[0][MeterCode].Increment(Delta);
    }

    public void Increment(int GameCode, int MeterCode, Int64 Delta)
    {
      if (GameCode <= 0) return;

      this[0][MeterCode].Increment(Delta);
      
      if (GameCode <= 0) return;

      this[GameCode][MeterCode].Increment(Delta);
    }



    public void Play(int GameCode, Int64 BetCents, Int64 WonCents, Int64 JackpotCents)
    {
      if (BetCents <= 0) return;

      lock (this)
      {
        this.Increment(GameCode, 0, BetCents);
        this.Increment(GameCode, 5, 1);
        if (WonCents > 0)
        {
          this.Increment(GameCode, 1, WonCents);
          this.Increment(GameCode, 6, 1);
        }
        else
        {
          this.Increment(GameCode, 7, 1);
        }

        if (JackpotCents > 0)
        {
          this.Increment(GameCode, 2, JackpotCents);
        }

        if (WonCents + JackpotCents > 0)
        {
          this.Increment(GameCode, 34, WonCents + JackpotCents);
        }
      }
    }

    public void ToCurrentCredits(Int64 ReCents, Int64 PromoReCents, Int64 PromoNrCents)
    {
      lock (this)
      {
        this.Increment(12, ReCents + PromoReCents);
        this.Increment(27, PromoNrCents);
      }
    }

    public void BillIn(Int64 Cents)
    {
      Decimal _denom;

      _denom = 0.01m * Cents;
      lock (this)
      {
        this.Increment(11, Cents);
        this.Increment(62, 1);
        this.Increment(63, Cents);
        for (int _meter = 64; _meter <= 87; _meter++)
        {
          if (SAS_Meter.GetBillDenomination(_meter) == _denom)
          {
            this.Increment(_meter, Cents);
            break;
          }
        }
      }
    
    }


  };


  class SimWassEgm
  {
    private static int m_num_instances = 0;

    private SasMeterList m_meters = new SasMeterList();


    int tick_door_event = WSI.Common.Misc.GetTickCount();
    Boolean door_opended = false;

    Random m_random = new Random(Interlocked.Increment(ref m_num_instances));


    Int64 current_cents = 0;

    void MoneyIn()
    {
      Decimal _denom;

      int _bill_meter_code;

      _bill_meter_code = m_random.Next(64, 88);
      _denom = SAS_Meter.GetBillDenomination(_bill_meter_code);


      current_cents += (Int64)(_denom * 100.0m);

      lock (this.m_meters)
      {
        this.m_meters.Increment(_bill_meter_code, 1);
      }
    }

    void Play()
    {
      int[] paytable = new int[] 
      { 
        25, 10, 5, 2, 2, 1, 0, 0, 0, 0,
         0, 10, 5, 2, 2, 1, 0, 0, 0, 0,
         0,  0, 5, 2, 2, 1, 0, 0, 0, 0,
         0,  0, 5, 2, 2, 1, 0, 0, 0, 0,
         0,  0, 5, 2, 2, 1, 0, 0, 0, 0,
         0,  0, 0, 0, 0, 0, 0, 0, 0, 0,
         0,  0, 0, 0, 0, 0, 0, 0, 0, 0,
         0,  0, 0, 0, 0, 0, 0, 0, 0, 0,
         0,  0, 0, 0, 0, 0, 0, 0, 0, 0,
         0,  0, 0, 0, 0, 0, 0, 0, 0, 0,
      };

      Int64 _bet;
      Int64 _won;

      _bet = Math.Max(0, Math.Min(10, current_cents));
      if (_bet <= 0) return;

      _won = _bet * paytable[m_random.Next(0, paytable.Length)];

      lock (this.m_meters)
      {
        this.m_meters.Increment("Played.Count", 1);
        this.m_meters.Increment("Played.Cents", _bet);
        if (_won > 0)
        {
          this.m_meters.Increment("Won.Count", 1);
          this.m_meters.Increment("Won.Cents", _won);
        }
      }


    }





    void Iter()
    {
      if (door_opended)
      {
        if (Misc.GetElapsedTicks(tick_door_event) >= 5000)
        {
          door_opended = false;
          tick_door_event = WSI.Common.Misc.GetTickCount();
        }
      }
      else
      {
        if (Misc.GetElapsedTicks(tick_door_event) >= 1000)

          tick_door_event = WSI.Common.Misc.GetTickCount();

        if (m_random.Next(0, 100) == 0)
        {
          door_opended = true;
        }
      }

      // Money In

      // Play

      // Money Out



    }


  }
}
