//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  Terminals Simulator
// 
//   DESCRIPTION:  
//
//   AUTHOR     :  
// 
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-DIC-2013 JRM    Added TITO messages simulation and individual calls 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.WCP;
using WSI.Common;
using System.Threading;
using System.IO;
using WSI.WCP.WCP_Messages.Meters;
using WSI.Common.TITO;


namespace WSI.WCP_ClientSimulator
{
  public partial class frm_client_simulator : Form
  {

    #region Local Variables

    ArrayList GLB_Terminals = new ArrayList();

    Boolean sim_enabled = false;
    Boolean tito_sim_enabled = true;


    DateTime sim_started;
    DateTime sim_end;


    Random tito_rnd_generator;
    Random tito_rnd_occurrence;

    String m_service_address;

    Hashtable m_ht_meters = new Hashtable(3000);
    Hashtable m_ht_fund_transfer_meters = new Hashtable(3000);

    Boolean m_db_connected = false;
    Int32 m_db_id = 0;
    String m_db_host = "";

    int[,] m_tito_sas_codes;

    // EGM
    DataTable m_dt_accounts = null;
    SqlDataAdapter m_adap_accounts = null;
    SqlCommand m_cmd_reset_accounts = null;


    //////SqlCommand m_cmd_set_balance_accounts = null;

    #endregion

    #region Structures

    enum TITO_Simulator_Actions
    {
      BILLS_IN = 0,
      PLAY = 1,
      TICKET_OUT = 2,
      TICKET_IN = 3,
      START_CARD_SESSION = 4,
      END_CARD_SESSION = 5,
      EGM_PARAMS = 6,
      GET_PARAMETERS = 7,
      SAS_METERS = 8,
      NO_ACTION = 9
    }

    enum TITO_COORDINATOR_STATUS
    {
      SENT_IS_VALID = 0,
      IS_VALID = 1
    }

    #endregion

    delegate void SetTextCallback(SimulatedTerminal Terminal, String Xml);

    void UpdateStats()
    {
      txt_statistics.Text = SimulatedTerminal.StatsString();
    }


    void MessageReceived(SimulatedTerminal Terminal, String Xml)
    {
      try
      {
        WCP_Message _wcp_response;
        _wcp_response = WCP_Message.CreateMessage(Xml);

        if (_wcp_response.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_OK
            && _wcp_response.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
        {
          if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_SERVER_SESSION_NOT_VALID
              || _wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID)
          {
            Terminal.enrolled = false;
          }
          return;
        }

        switch (_wcp_response.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgEnrollTerminalReply:
            {
              WCP_MsgEnrollTerminalReply reply;
              reply = (WCP_MsgEnrollTerminalReply)_wcp_response.MsgContent;

              Terminal.terminal_session_id = _wcp_response.MsgHeader.TerminalSessionId;
              Terminal.sequence_id = reply.LastSequenceId + 1;
              Terminal.transaction_id = reply.LastTransactionId + 1;
              Terminal.stacker_id = reply.LastTransactionId + 1;

              Terminal.enrolled = true;

              SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgGetParameters);

              // Send Machine Meters
              Terminal.FundTransferMetersChanged = true;
            }
            break;

          case WCP_MsgTypes.WCP_MsgGetParametersReply:
            {

              WCP_MsgGetParametersReply reply;
              reply = (WCP_MsgGetParametersReply)_wcp_response.MsgContent;

              Terminal.terminal_id = (Int32)reply.MachineId;
              Terminal.terminal_system_id = reply.SystemId;
            }
            break;

          case WCP_MsgTypes.WCP_MsgStartCardSessionReply:
            {
              WCP_MsgStartCardSessionReply reply;
              reply = (WCP_MsgStartCardSessionReply)_wcp_response.MsgContent;

              Terminal.play_session_id = reply.CardSessionId;

              Terminal.credit_cents += reply.CardBalance;
              Terminal.ToGmCount++;
              Terminal.ToGmCount %= 100000000;
              Terminal.ToGmCents += reply.CardBalance;
              Terminal.ToGmCents %= 10000000000;
              Terminal.FundTransferMetersChanged = true;
              Terminal.player_name = reply.HolderName;
            }
            break;

          case WCP_MsgTypes.WCP_MsgEndSessionReply:
            {
              WCP_MsgEndSessionReply reply;
              reply = (WCP_MsgEndSessionReply)_wcp_response.MsgContent;

              // RCI 28-JUL-2010: Print started CardSessions correctly.
              Terminal.FromGmCount++;
              Terminal.FromGmCount %= 100000000;
              Terminal.FromGmCents += Terminal.credit_cents;
              Terminal.FromGmCents %= 10000000000;
              Terminal.FundTransferMetersChanged = true;
              Terminal.play_session_id = 0;
              Terminal.credit_cents = 0;
              Terminal.player_name = "";
            }
            break;

          case WCP_MsgTypes.WCP_MsgReportPlayReply:
            {
              if (!tito_sim_enabled)
              {
                WCP_MsgReportPlayReply reply;
                reply = (WCP_MsgReportPlayReply)_wcp_response.MsgContent;
              }
              else
              {
                EgmMachine egm;
                egm = (EgmMachine)Terminal;
                egm.ResponseReceived(_wcp_response);
              }
            }
            break;

          case WCP_MsgTypes.WCP_MsgAddCreditReply:
            {
              WCP_MsgAddCreditReply reply;
              reply = (WCP_MsgAddCreditReply)_wcp_response.MsgContent;
              Terminal.credit_cents = reply.SessionBalanceAfterAdd;
            }
            break;

          //SSC 01-MAR-2012: Added Money Event
          case WCP_MsgTypes.WCP_MsgMoneyEvent:
            {
              WCP_MsgMoneyEvent reply;
              reply = (WCP_MsgMoneyEvent)_wcp_response.MsgContent;
            }


            break;

          //
          // PromoBOX replies -------------------------------------------------------------------------------
          //

          //SSC 15-MAY-2012: Added ResourceGet
          case WCP_MsgTypes.WCP_WKT_MsgGetResourceReply:
            {
              WCP_WKT_MsgGetResourceReply reply;
              reply = (WCP_WKT_MsgGetResourceReply)_wcp_response.MsgContent;
            }
            break;

          //SSC 21-MAY-2012: Added PlayerRequestGift
          case WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGiftReply:
            {
              WCP_WKT_MsgPlayerRequestGiftReply reply;
              reply = (WCP_WKT_MsgPlayerRequestGiftReply)_wcp_response.MsgContent;
            }
            break;

          // JCM 24-05-2012
          case WCP_MsgTypes.WCP_WKT_MsgGetPlayerGiftsReply:
            {
              WCP_WKT_MsgGetPlayerGiftsReply reply;
              reply = (WCP_WKT_MsgGetPlayerGiftsReply)_wcp_response.MsgContent;
            }
            break;

          case WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromosReply:
            {
              WCP_WKT_MsgGetPlayerPromosReply reply;
              reply = (WCP_WKT_MsgGetPlayerPromosReply)_wcp_response.MsgContent;
            }
            break;

          // JCM 30-05-2012
          case WCP_MsgTypes.WCP_WKT_MsgGetParameters:
            {
              WCP_WKT_MsgGetParameters reply;
              reply = (WCP_WKT_MsgGetParameters)_wcp_response.MsgContent;
            }
            break;

          // TJG 15-MAI-2013
          case WCP_MsgTypes.WCP_WKT_MsgGetPlayerDrawsReply:
            {
              WCP_WKT_MsgGetPlayerDrawsReply reply;
              reply = (WCP_WKT_MsgGetPlayerDrawsReply)_wcp_response.MsgContent;
            }
            break;

          // TJG 15-MAI-2013
          case WCP_MsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbersReply:
            {
              WCP_WKT_MsgRequestPlayerDrawNumbersReply reply;
              reply = (WCP_WKT_MsgRequestPlayerDrawNumbersReply)_wcp_response.MsgContent;
            }
            break;


          case WCP_MsgTypes.WCP_MsgKeepAliveReply:
            break;

          case WCP_MsgTypes.WCP_MsgLockCardReply:
            {
              WCP_MsgLockCardReply reply;
              reply = (WCP_MsgLockCardReply)_wcp_response.MsgContent;
            }
            break;

          //
          // Mobile Bank Replies -------------------------------------------------------------------------------
          //

          case WCP_MsgTypes.WCP_MsgMobileBankCardCheckReply:
            {
              WCP_MsgMobileBankCardCheckReply reply;
              reply = (WCP_MsgMobileBankCardCheckReply)_wcp_response.MsgContent;
              Terminal.mb_authorized_amount = reply.MobileBankAuthorizedAmountx100;
            }
            break;

          case WCP_MsgTypes.WCP_MsgMobileBankCardTransferReply:
            {
              WCP_MsgMobileBankCardTransferReply reply;
              reply = (WCP_MsgMobileBankCardTransferReply)_wcp_response.MsgContent;

              // ACC 17-MAR-2010  The StartCardSession refresh the balance.

            }
            break;

          // RCI 28-JUL-2010: Print enrolled terminals correctly.
          case WCP_MsgTypes.WCP_MsgUnenrollTerminalReply:
            {
              Terminal.enrolled = false;
            }
            break;


          case WCP_MsgTypes.WCP_TITO_MsgEgmParametersReply:
            {
              WCP_TITO_MsgEgmParametersReply _reply;
              _reply = (WCP_TITO_MsgEgmParametersReply)_wcp_response.MsgContent;


            }
            break;


          case WCP_MsgTypes.WCP_MsgCashSessionPendingClosingReply:
            {
              WCP_MsgCashSessionPendingClosingReply _reply;
              _reply = (WCP_MsgCashSessionPendingClosingReply)_wcp_response.MsgContent;
            }
            break;


          case WCP_MsgTypes.WCP_TITO_MsgValidationNumberReply:
          case WCP_MsgTypes.WCP_TITO_MsgCreateTicketReply:
          case WCP_MsgTypes.WCP_TITO_MsgIsValidTicketReply:
          case WCP_MsgTypes.WCP_TITO_MsgCancelTicketReply:
            {
              EgmMachine egm;
              egm = (EgmMachine)Terminal;
              egm.ResponseReceived(_wcp_response);
            }
            break;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
      }
    }

    public frm_client_simulator()
    {
      Thread _thread;

      InitializeComponent();
      cmb_mode.SelectedIndex = 0;

      txt_db_id.Text = "0";

      //CreateTerminals(1);

      LoadGameMeters();
      LoadFundTransferMeters();

      // RCI 03-AUG-2010: Load last used data.
      LoadLastUsedData();

      if (!ConnectDB())
      {
        return;
      }

      _thread = new Thread(SimulationThread);
      _thread.Name = "SimulationThread";
      _thread.Start();
    }

    #region Constants

    private const int CARD_TYPE_PLAYER = 0;
    private const int CARD_TYPE_MOBILE_BANK = 1;

    private const String DIR_METERS = "meters";

    #endregion

    #region Enums

    #endregion

    #region Simulation Thread



    public void CashlessStep(SimulatedTerminal T)
    {
      int _elapsed;

      _elapsed = Environment.TickCount - T.m_tick_last_send;

      if (_elapsed > 0)
      {
        if (_elapsed < 100)
        {
          return;
        }
      }
      
      if (T.IsThereAnyMsgPendingReply())
      {
        return;
      }

      if (T.play_session_id == 0)
      {
        // Not in session
        SendRequestFrom(T, WCP_MsgTypes.WCP_MsgGetCardType);

        return;
      }

      if (T.pending_plays > 0)
      {
        if (T.IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_MsgReportPlay))
        {
          return;
        }

        SendRequestFrom(T, WCP_MsgTypes.WCP_MsgReportPlay);
        T.pending_plays--;

        return;
      }

      if (T.IsThereAnyMsgPendingReply(WCP_MsgTypes.WCP_MsgEndSession))
      {
        return;
      }

      SendRequestFrom(T, WCP_MsgTypes.WCP_MsgEndSession);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Simulation thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SimulationThread()
    {
      long _interval;
      int _wait_timeout;
      int _tick_alive;

      sim_end = DateTime.Now;
      sim_started = DateTime.Now;
      _wait_timeout = 100;

      int _num_cashouts_per_iter;

      _tick_alive = Misc.GetTickCount();


      Boolean _cashless;

      _cashless = true;



      while (true)
      {
        try
        {
          if (_wait_timeout > 0) System.Threading.Thread.Sleep(_wait_timeout);

          if (Misc.GetElapsedTicks(_tick_alive) >= 5000)
          {
            _tick_alive = Misc.GetTickCount();

            foreach (SimulatedTerminal _t in GLB_Terminals)
            {
              if (Misc.GetElapsedTicks(_t.m_tick_last_send) >= 30000)
              {
                if (_t.IsConnected && _t.enrolled)
                {
                  SendRequestFrom(_t, WCP_MsgTypes.WCP_MsgKeepAlive);

                  break;
                }
              }
            }
          }

          if (sim_enabled)
          {
            if (_cashless)
            {
              foreach (SimulatedTerminal _t in GLB_Terminals)
              {
                CashlessStep(_t);
              }
            }
            else
            {

              sim_end = DateTime.Now;

              _wait_timeout = 0;



              if (GLB_Terminals != null)
              {
                if (tito_sim_enabled)
                {
                  SimulatedTerminal _t;

                  _num_cashouts_per_iter = 0;
                  foreach (EgmMachine egm in GLB_Terminals)
                  {
                    _t = (SimulatedTerminal)egm;

                    if (!_t.enrolled)
                    {
                      SimStep(_t);

                      continue;
                    }
                    if (_t.transaction_id <= 0)
                    {
                      SimStep(_t);

                      continue;
                    }

                    if (egm.re_cents + egm.promo_re_cents + egm.promo_nr_cents > 0)
                    {
                      //if (_num_cashouts_per_iter < 1)
                      {
                        egm.ProcessCashOut();
                        _num_cashouts_per_iter++;
                      }
                      continue;
                    }

                    egm.MoneyToEgm();
                  }

                  continue;
                }
                foreach (SimulatedTerminal st in GLB_Terminals)
                {
                  _interval = Misc.GetElapsedTicks(st.last_play_tick);

                  if (st.time_between_plays > _interval)
                  {
                    continue;
                  }

                  SimStep(st);
                }
              }
            }
          }
          else
          {
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }

      } // while

    } // SimulationThread


    #endregion // Simulation Thread

    #region File GameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Save Game Meters. One file per ALL Terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SaveGameMeters()
    {
      TerminalGameMeters _meters;
      TimeSpan _ticks0;
      TimeSpan _ticks1;
      TimeSpan _time_close;
      TimeSpan _time_save;
      Boolean _add;
      String _file;
      StringBuilder _line;
      StreamWriter _sw;

      try
      {
        if (GLB_Terminals != null)
        {
          _ticks1 = Performance.GetTickCount();

          foreach (SimulatedTerminal _st in GLB_Terminals)
          {
            _add = false;
            _meters = GetGameMeters(_st.terminal_external_id);
            if (_meters == null)
            {
              // Not found. Is new Meters.
              _add = true;
              _meters = new TerminalGameMeters();
              _meters.terminal = _st.terminal_external_id;
            }
            _meters.played_count = _st.PlayedCount;
            _meters.played_cents = _st.PlayedCents;
            _meters.won_count = _st.WonCount;
            _meters.won_cents = _st.WonCents;
            _meters.jackpot_cents = _st.JackpotCents;

            if (_add)
            {
              AddGameMeters(_meters);
            }
          }
          _ticks0 = Performance.GetTickCount();

          _file = DIR_METERS + "\\" + "GAME_METERS";
          _sw = new StreamWriter(_file);

          _line = new StringBuilder();

          foreach (TerminalGameMeters _gm in m_ht_meters.Values)
          {
            try
            {
              _line.Remove(0, _line.Length);

              _line.Append(_gm.terminal);
              _line.Append(",");
              _line.Append(_gm.played_count);
              _line.Append(",");
              _line.Append(_gm.played_cents);
              _line.Append(",");
              _line.Append(_gm.won_count);
              _line.Append(",");
              _line.Append(_gm.won_cents);
              _line.Append(",");
              _line.Append(_gm.jackpot_cents);

              _sw.WriteLine(_line.ToString());
            }
            catch
            {
            }
          }

          _ticks1 = Performance.GetTickCount();
          _sw.Close();
          _time_close = Performance.GetElapsedTicks(_ticks1);

          _time_save = Performance.GetElapsedTicks(_ticks0);

          Log.Message("Times in ms. Close: " + _time_close.TotalMilliseconds + ", Save: " + _time_save.TotalMilliseconds);
        }
      }
      catch
      {
      }
    } // SaveGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Load Game Meters. One file per ALL Terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadGameMeters()
    {
      String _file;
      String _line;
      String[] _fields;
      StreamReader _sr;
      TerminalGameMeters _meters;
      Int64 _played_count;
      Int64 _played_cents;
      Int64 _won_count;
      Int64 _won_cents;
      Int64 _jackpot_cents;

      try
      {
        Directory.CreateDirectory(DIR_METERS);
        _file = DIR_METERS + "\\" + "GAME_METERS";

        if (!File.Exists(_file))
        {
          return;
        }

        using (_sr = new StreamReader(_file))
        {
          while ((_line = _sr.ReadLine()) != null)
          {
            _fields = _line.Split(',');

            _meters = new TerminalGameMeters();
            _meters.terminal = _fields[0].Trim();

            if (!Int64.TryParse(_fields[1], out _played_count))
            {
              _meters = null;
              continue;
            }
            _meters.played_count = _played_count;

            if (!Int64.TryParse(_fields[2], out _played_cents))
            {
              _meters = null;
              continue;
            }
            _meters.played_cents = _played_cents;

            if (!Int64.TryParse(_fields[3], out _won_count))
            {
              _meters = null;
              continue;
            }
            _meters.won_count = _won_count;

            if (!Int64.TryParse(_fields[4], out _won_cents))
            {
              _meters = null;
              continue;
            }
            _meters.won_cents = _won_cents;

            if (!Int64.TryParse(_fields[5], out _jackpot_cents))
            {
              _meters = null;
              continue;
            }
            _meters.jackpot_cents = _jackpot_cents;

            AddGameMeters(_meters);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Add the terminal game meters to the list of game meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalGameMeter Meter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AddGameMeters(TerminalGameMeters Meter)
    {
      try
      {
        if (m_ht_meters.Contains(Meter.terminal))
        {
          m_ht_meters.Remove(Meter.terminal);
        }
        m_ht_meters.Add(Meter.terminal, Meter);
      }
      catch
      {
      }
    } // AddGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Get the terminal game meters from the list of game meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TerminalName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private TerminalGameMeters GetGameMeters(String TerminalName)
    {
      try
      {
        if (m_ht_meters.Contains(TerminalName))
        {
          return (TerminalGameMeters)m_ht_meters[TerminalName];
        }
      }
      catch
      {
      }

      return null;
    } // GetGameMeters

    #endregion // File GameMeters

    #region File FundTransferMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Save FundTransfer Meters. One file per ALL Terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SaveFundTransferMeters()
    {
      TerminalFundTransferMeters _meters;
      TimeSpan _ticks0;
      TimeSpan _ticks1;
      TimeSpan _time_close;
      TimeSpan _time_save;
      Boolean _add;
      String _file;
      StringBuilder _line;
      StreamWriter _sw;

      try
      {
        if (GLB_Terminals != null)
        {
          _ticks1 = Performance.GetTickCount();

          foreach (SimulatedTerminal _st in GLB_Terminals)
          {
            _add = false;
            _meters = GetFundTransferMeters(_st.terminal_external_id);
            if (_meters == null)
            {
              // Not found. Is new Meters.
              _add = true;
              _meters = new TerminalFundTransferMeters();
              _meters.terminal = _st.terminal_external_id;
            }
            _meters.to_gm_count = _st.ToGmCount;
            _meters.to_gm_cents = _st.ToGmCents;
            _meters.from_gm_count = _st.FromGmCount;
            _meters.from_gm_cents = _st.FromGmCents;

            if (_add)
            {
              AddFundTransferMeters(_meters);
            }
          }
          _ticks0 = Performance.GetTickCount();

          _file = DIR_METERS + "\\" + "FUND_TRANSFER_METERS";
          _sw = new StreamWriter(_file);

          _line = new StringBuilder();

          foreach (TerminalFundTransferMeters _gm in m_ht_fund_transfer_meters.Values)
          {
            try
            {
              _line.Remove(0, _line.Length);

              _line.Append(_gm.terminal);
              _line.Append(",");
              _line.Append(_gm.to_gm_count);
              _line.Append(",");
              _line.Append(_gm.to_gm_cents);
              _line.Append(",");
              _line.Append(_gm.from_gm_count);
              _line.Append(",");
              _line.Append(_gm.from_gm_cents);

              _sw.WriteLine(_line.ToString());
            }
            catch
            {
            }
          }

          _ticks1 = Performance.GetTickCount();
          _sw.Close();
          _time_close = Performance.GetElapsedTicks(_ticks1);

          _time_save = Performance.GetElapsedTicks(_ticks0);

          Log.Message("Times in ms. Close: " + _time_close.TotalMilliseconds + ", Save: " + _time_save.TotalMilliseconds);
        }
      }
      catch
      {
      }
    } // SaveFundTransferMeters

    private void StartPlaySimulation(Boolean AuxSimEnabled)
    {
      int _offset_tick;
      int _idx_terminal;

      // Declare and set bills array element values for tito simulation
      m_tito_sas_codes = new int[9, 2] {  
                                          {64, 1,},
  	                                      {65, 2,},
                                          {66, 5,},
                                          {67, 10,},
                                          {68, 20,},
                                          {69, 25,},
                                          {70, 50,},
                                          {71, 100,},
                                          {72, 200,}
                                       };

      // Setup random number generators
      tito_rnd_generator = new Random((int)DateTime.Now.Ticks);
      tito_rnd_occurrence = new Random((int)DateTime.Now.Ticks + 5045564); // Same seeds give same random number or very similar


      if (AuxSimEnabled)
      {
        if (GLB_Terminals != null)
        {
          // Distribute GameMeters
          _idx_terminal = 0;
          _offset_tick = 15000 / GLB_Terminals.Count;

          foreach (SimulatedTerminal st in GLB_Terminals)
          {
            st.last_sent_message_validation_number = 0;
            st.last_game_meters_tick = Environment.TickCount - 15000 + (_offset_tick * _idx_terminal);
            _idx_terminal++;
          }

          // Distribute MachineMeters
          _idx_terminal = 0;
          _offset_tick = 30000 / GLB_Terminals.Count;

          foreach (SimulatedTerminal st in GLB_Terminals)
          {
            st.last_machine_meters_tick = Environment.TickCount - 30000 + (_offset_tick * _idx_terminal);
            _idx_terminal++;
          }

          // Distribute ReportEventList
          _idx_terminal = 0;
          _offset_tick = 15000 / GLB_Terminals.Count;

          foreach (SimulatedTerminal st in GLB_Terminals)
          {
            st.last_report_event_list_tick = Environment.TickCount - 15000 + (_offset_tick * _idx_terminal);
            _idx_terminal++;
          }

          // Distribute UpdateCardSession
          _idx_terminal = 0;
          _offset_tick = 30000 / GLB_Terminals.Count;

          foreach (SimulatedTerminal st in GLB_Terminals)
          {
            st.last_update_card_session_tick = Environment.TickCount - 30000 + (_offset_tick * _idx_terminal);
            _idx_terminal++;
          }

          // Distribute Plays
          _idx_terminal = 0;
          _offset_tick = int.Parse(txt_time_plays.Text) / GLB_Terminals.Count;

          foreach (SimulatedTerminal st in GLB_Terminals)
          {
            st.step = SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_GET_CARD_TYPE;
            if (chk_sim_plays.Checked == true)
            {
              st.pending_plays = int.Parse(txt_pending_plays.Text);
            }
            else
            {
              st.pending_plays = int.MaxValue;
            }

            if (chk_time_plays.Checked == true)
            {
              st.time_between_plays = int.Parse(txt_time_plays.Text);
              st.last_play_tick = Environment.TickCount - st.time_between_plays + (_offset_tick * _idx_terminal);
              _idx_terminal++;
            }
            else
            {
              st.time_between_plays = 0;
            }
          }
        }
      }

      sim_enabled = AuxSimEnabled;
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Load FundTransfer Meters. One file per ALL Terminals.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadFundTransferMeters()
    {
      String _file;
      String _line;
      String[] _fields;
      StreamReader _sr;
      TerminalFundTransferMeters _meters;
      Int64 _to_gm_count;
      Int64 _to_gm_cents;
      Int64 _from_gm_count;
      Int64 _from_gm_cents;

      try
      {
        Directory.CreateDirectory(DIR_METERS);
        _file = DIR_METERS + "\\" + "FUND_TRANSFER_METERS";

        if (!File.Exists(_file))
        {
          return;
        }

        using (_sr = new StreamReader(_file))
        {
          while ((_line = _sr.ReadLine()) != null)
          {
            _fields = _line.Split(',');

            _meters = new TerminalFundTransferMeters();
            _meters.terminal = _fields[0].Trim();

            if (!Int64.TryParse(_fields[1], out _to_gm_count))
            {
              _meters = null;
              continue;
            }
            _meters.to_gm_count = _to_gm_count;

            if (!Int64.TryParse(_fields[2], out _to_gm_cents))
            {
              _meters = null;
              continue;
            }
            _meters.to_gm_cents = _to_gm_cents;

            if (!Int64.TryParse(_fields[3], out _from_gm_count))
            {
              _meters = null;
              continue;
            }
            _meters.from_gm_count = _from_gm_count;

            if (!Int64.TryParse(_fields[4], out _from_gm_cents))
            {
              _meters = null;
              continue;
            }
            _meters.from_gm_cents = _from_gm_cents;

            AddFundTransferMeters(_meters);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadFundTransferMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Add the terminal game meters to the list of game meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalGameMeter Meter
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void AddFundTransferMeters(TerminalFundTransferMeters Meter)
    {
      try
      {
        if (m_ht_fund_transfer_meters.Contains(Meter.terminal))
        {
          m_ht_fund_transfer_meters.Remove(Meter.terminal);
        }
        m_ht_fund_transfer_meters.Add(Meter.terminal, Meter);
      }
      catch
      {
      }
    } // AddFundTransferMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Get the terminal game meters from the list of game meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - String TerminalName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private TerminalFundTransferMeters GetFundTransferMeters(String TerminalName)
    {
      try
      {
        if (m_ht_fund_transfer_meters.Contains(TerminalName))
        {
          return (TerminalFundTransferMeters)m_ht_fund_transfer_meters[TerminalName];
        }
      }
      catch
      {
      }

      return null;
    } // GetFundTransferMeters

    #endregion // File FundTransferMeters


    #region Button Events

    private void btn_connect_Click(object sender, EventArgs e)
    {
      int _num_terminals;

      if (!Int32.TryParse(txt_num_terminals.Text, out _num_terminals))
      {
        MessageBox.Show("Num. Terminals must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (_num_terminals > 10000)
      {
        MessageBox.Show("Num. Terminals must be less or equal than 10000.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      // RCI 03-AUG-2010: Save last used data.
      SaveLastUsedData();

      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        try
        {
          if (t.IsConnected)
          {
            t.Disconnect();
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      CreateTerminals(_num_terminals);

      this.Cursor = Cursors.WaitCursor;

      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        t.m_remote = cmb_service_address.Text;
        ThreadPool.QueueUserWorkItem(new WaitCallback(WorkItemTerminalConnect), t);
      }

      this.Cursor = Cursors.Default;
    }

    private void WorkItemTerminalConnect(Object Terminal)
    {
      SimulatedTerminal _terminal;

      try
      {
        _terminal = (SimulatedTerminal)Terminal;
        _terminal.Connect(_terminal.m_remote);
        Thread.Sleep(10);
        SendRequestFrom(_terminal, WCP_MsgTypes.WCP_MsgEnrollTerminal);
      }
      catch
      {
      }
    }

    //SSC 15-05-2012:
    private void btn_get_resource_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgGetResource);
      }
    }

    //SSC 21-05-2012
    private void btn_player_request_gift_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGift);
      }
    }
    //JCM 24-05-2012
    private void bt_Get_Gifts_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgGetPlayerGifts);
      }
    }
    //JCM 30-05-2012
    private void bt_Get_Params_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgGetParameters);
      }
    }

    private void btn_player_draws_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgGetPlayerDraws);
      }
    }

    private void btn_player_draw_numbers_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbers);
      }
    }

    private void btn_open_cashier_terminal_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        t.stacker_id++;
      }
    }


    private void btn_start_rnd_simulation_Click(object sender, EventArgs e)
    {
      Boolean _aux_sim_enabled;

      int _tick = Misc.GetTickCount();

      foreach (SimulatedTerminal st in GLB_Terminals)
      {
        if (tito_sim_enabled)
        {
          ((EgmMachine)st).tick_cashout = _tick;
          _tick += 10;
        }
      }

      m_service_address = cmb_service_address.Text;

      _aux_sim_enabled = !sim_enabled;

      if (_aux_sim_enabled)
      {
        lbl_simulation.Text = "Started";
        lbl_simulation.ForeColor = Color.FromArgb(0, 192, 0);


        //SimulationCreateValidTicketsList();
        sim_started = DateTime.Now;

        // Create 2 stackers for each terminal if they are not already created

        ////try
        ////{
        ////  _sql_conn = WGDB.Connection();
        ////  _sql_trx = _sql_conn.BeginTransaction();

        ////  CreateTwoStackersForEachTerminal(_sql_trx);

        ////  _sql_trx.Commit();
        ////}
        ////catch (Exception)
        ////{
        ////  return;
        ////}
      }
      else
      {
        lbl_simulation.Text = "Stopped";
        lbl_simulation.ForeColor = Color.FromArgb(255, 128, 128);
      }

      StartPlaySimulation(_aux_sim_enabled);

    }

    // Non transaction related buttons


    #endregion

    #region Timers


    private void tmr_machine_meters_Tick(object sender, EventArgs e)
    {
      long _interval;

      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        if (!t.enrolled)
        {
          return;
        }

        _interval = Misc.GetElapsedTicks(t.last_machine_meters_tick);

        if (_interval > 30000)
        {
          SendRequestFrom(t, WCP_MsgTypes.WCP_MsgReportMachineMeters);
        }
      }
    }

    public void SimStep(SimulatedTerminal Terminal)
    {
      long _interval;

      if (!Terminal.IsConnected)
      {
        Terminal.enrolled = false;
        Terminal.Connect(m_service_address);

        return;
      }

      if (!Terminal.enrolled)
      {
        SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgEnrollTerminal);

        return;
      }

      _interval = Misc.GetElapsedTicks(Terminal.last_game_meters_tick);

      if (_interval > 15000)
      {
        SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgReportGameMeters);

        return;
      }

      _interval = Misc.GetElapsedTicks(Terminal.last_machine_meters_tick);

      if (_interval > 30000)
      {
        SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgReportMachineMeters);
        return;
      }

      _interval = Misc.GetElapsedTicks(Terminal.last_report_event_list_tick);

      _interval = Misc.GetElapsedTicks(Terminal.last_update_card_session_tick);

      if (_interval > 30000 && Terminal.play_session_id > 0)
      {
        SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgUpdateCardSession);

        return;
      }

      switch (Terminal.step)
      {
        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_GET_CARD_TYPE:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgGetCardType);
            Terminal.step++;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_START_CARD_SESSION:
          {
            if (Terminal.play_session_id == 0)
            {
              SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgStartCardSession);
            }
            else
            {
              if (Terminal.pending_plays > 0)
              {
                SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgReportPlay);
                Terminal.pending_plays--;
              }
            }
            Terminal.step++;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_PLAY:
          {
            if (Terminal.pending_plays > 0)
            {
              SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgReportPlay);
              Terminal.pending_plays--;
            }

            if (Terminal.pending_plays <= 0)
            {
              Terminal.step++;
            }
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_CHANGE_STACKER:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgCashSessionPendingClosing);
            Terminal.step++;
          }
          break;
        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_EGM:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_TITO_MsgEgmParameters);

            Terminal.step++;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_BILL_IN:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgMoneyEvent);

            Terminal.step++;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_PLAY_SESSION_INFO:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgPlaySessionMeters);
            Terminal.step = SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_GET_PARAMETERS;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_TICKET_IN:
          {

            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_TITO_MsgIsValidTicket);

            Terminal.step++;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_GET_PARAMETERS:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgGetParameters);
            Terminal.step++;
          }

          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_TICKET_OUT:
          {
            Ticket _ticket;
            _ticket = new Ticket();


            ///Terminal.card_balance = 0;
            ///
            if (Terminal.terminal_id > 0)
            {
              SendRequestFrom(Terminal, WCP_MsgTypes.WCP_TITO_MsgCreateTicket);
            }
            else
            {
              SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgGetParameters);
            }

            Terminal.step++;

          }
          break;
        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_SAS_METERS:
          {
            SendRequestFrom(Terminal, WCP_MsgTypes.WCP_MsgSasMeters);
            Terminal.step++;
          }

          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_END_CARD_SESSION:
          {
            if (chk_sim_plays.Checked == true)
            {
              Terminal.pending_plays = int.Parse(txt_pending_plays.Text);
            }
            else
            {
              Terminal.pending_plays = int.MaxValue;
            }

            // End Session and start over
            Terminal.step = SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_BILL_IN;
          }
          break;

        case SimulatedTerminal.WCP_SimulationSteps.WCP_STEP_FINISHED:
          break;

        default:
          break;
      }

    }
    #endregion

    #region Messages

    void SendRequestFrom(SimulatedTerminal Terminal, WCP_MsgTypes MsgType)
    {
      SimulatedTerminal.SendRequestFrom(Terminal, MsgType);
    }

    #endregion

    private void CreateTerminals(int NumTerminals)
    {
      int _num_terminals;
      SimulatedTerminal t;

      _num_terminals = NumTerminals;

      while (_num_terminals > GLB_Terminals.Count)
      {
        if (tito_sim_enabled)
        {
          t = new EgmMachine();
        }
        else
        {
          t = new SimulatedTerminal();
        }

        if (GLB_Terminals.Count <= 999)
        {
          t.terminal_external_id = cmb_prefix.Text.ToUpper() + "_TERMINAL_" + GLB_Terminals.Count.ToString("000");
        }
        else
        {
          t.terminal_external_id = cmb_prefix.Text.ToUpper() + "_TERMINAL_" + GLB_Terminals.Count.ToString("0000");
        }

        GLB_Terminals.Add(t);

        t.idx = GLB_Terminals.Count - 1;

        UInt64 _track_value;
        String _internal_track;

        _track_value = UInt64.Parse(txt_base_card.Text) + (UInt64)t.idx;
        _internal_track = _track_value.ToString("0000000000000");
        CardNumber.TrackDataToExternal(out  t.external_track, _internal_track, WSI.Common.CardTrackData.CardsType.PLAYER);
      }
      while (_num_terminals < GLB_Terminals.Count)
      {
        GLB_Terminals.RemoveAt(GLB_Terminals.Count - 1);
      }

      cmb_prefix_TextChanged(null, null);
    }

    private void tmr_refresh_Tick(object sender, EventArgs e)
    {
      Int32 num_terminals_conn;
      Int32 num_terminals_enrolled;
      Int32 num_terminals_session_started;

      UpdateStats();

      num_terminals_conn = 0;
      num_terminals_enrolled = 0;
      num_terminals_session_started = 0;
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        if (t.IsConnected)
        {
          num_terminals_conn++;

          if (t.enrolled)
          {
            num_terminals_enrolled++;
          }

          if (t.play_session_id > 0)
          {
            num_terminals_session_started++;
          }
        }
        else
        {
          t.enrolled = false;
          t.play_session_id = 0;
        }
      }

      if (num_terminals_conn > 0)
      {
        lbl_terminals_conn.Text = num_terminals_conn.ToString() + " Connected";
        lbl_terminals_conn.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_conn.Text = "0 Connected";
        lbl_terminals_conn.ForeColor = Color.FromArgb(255, 128, 128);
      }

      if (num_terminals_enrolled > 0)
      {
        lbl_terminals_enrolled.Text = num_terminals_enrolled.ToString() + " Enrolled";
        lbl_terminals_enrolled.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_enrolled.Text = "0 Enrolled";
        lbl_terminals_enrolled.ForeColor = Color.FromArgb(255, 128, 128);
      }

      if (num_terminals_session_started > 0)
      {
        lbl_terminals_start_session.Text = num_terminals_session_started.ToString() + " Started Session";
        lbl_terminals_start_session.ForeColor = Color.FromArgb(0, 192, 0);
      }
      else
      {
        lbl_terminals_start_session.Text = "0 Started Session";
        lbl_terminals_start_session.ForeColor = Color.FromArgb(255, 128, 128);
      }

    }

    private void chk_sim_plays_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_sim_plays.Checked == true)
      {
        txt_pending_plays.Enabled = true;
      }
      else
      {
        txt_pending_plays.Enabled = false;
      }
    }

    private void cmb_prefix_SelectedIndexChanged(object sender, EventArgs e)
    {
      cmb_prefix_TextChanged(sender, e);
    }

    void cmb_prefix_TextChanged(object sender, System.EventArgs e)
    {

      int idx;
      SimulatedTerminal t0;

      lock (GLB_Terminals)
      {
        idx = 0;
        foreach (SimulatedTerminal t in GLB_Terminals)
        {
          t.terminal_external_id = cmb_prefix.Text.ToUpper() + "_TERMINAL_" + idx.ToString("000");
          idx += 1;
        }
      }

      if (GLB_Terminals.Count > 0)
      {
        t0 = (SimulatedTerminal)GLB_Terminals[0];
      }
    }

    private void chk_time_plays_CheckedChanged(object sender, EventArgs e)
    {
      if (chk_time_plays.Checked == true)
      {
        txt_time_plays.Enabled = true;
      }
      else
      {
        txt_time_plays.Enabled = false;
      }
    }

    private void btn_clr_statistics_Click(object sender, EventArgs e)
    {
      SimulatedTerminal.ClearStatistics();
    }

    private void btn_set_accounts_Click(object sender, EventArgs e)
    {
      DataRow _row;
      Int32 _num_terms;
      UInt64 _base_card;
      UInt64 _track;
      String _trackdata;
      String _trackdata_min;
      String _trackdata_max;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;

      if (!Int32.TryParse(txt_num_terminals.Text, out _num_terms))
      {
        MessageBox.Show("Num. Terminals must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      if (!UInt64.TryParse(txt_base_card.Text, out _base_card))
      {
        MessageBox.Show("Base Card must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      if (!ConnectDB())
      {
        return;
      }

      SaveLastUsedData();

      InitSQLAccounts();

      for (UInt32 _idx = 0; _idx < _num_terms; _idx++)
      {
        _row = m_dt_accounts.NewRow();

        _track = _base_card + _idx;
        _trackdata = _track.ToString("0000000000000");
        _row[0] = _trackdata;

        m_dt_accounts.Rows.Add(_row);
      }

      _track = _base_card;
      _trackdata_min = _track.ToString("0000000000000");
      _track = _base_card + (UInt32)_num_terms - 1;
      _trackdata_max = _track.ToString("0000000000000");

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        m_adap_accounts.InsertCommand.Connection = _sql_conn;
        m_adap_accounts.InsertCommand.Transaction = _sql_trx;

        m_adap_accounts.Update(m_dt_accounts);

        m_cmd_reset_accounts.Connection = _sql_conn;
        m_cmd_reset_accounts.Transaction = _sql_trx;

        m_cmd_reset_accounts.Parameters[0].Value = _trackdata_min;
        m_cmd_reset_accounts.Parameters[1].Value = _trackdata_max;

        m_cmd_reset_accounts.ExecuteNonQuery();

        _sql_trx.Commit();

        MessageBox.Show("Accounts set.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_dt_accounts.Clear();

        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { ; }
          }
        }
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { ; }
          _sql_conn.Dispose();
          _sql_conn = null;
        }
      }
    } // btn_set_accounts_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Connect to DB - Pool
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True/False: Indicate if can connect.
    //   NOTES :
    //
    private Boolean ConnectDB()
    {
      Int32 _db_id;
      String _db_host;
      Boolean _must_connect;

      if (!Int32.TryParse(txt_db_id.Text, out _db_id))
      {
        MessageBox.Show("DB Id must be an integer greater than 0.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
      _db_host = txt_db_host.Text.Trim().ToUpper();
      if (_db_host == "")
      {
        MessageBox.Show("DB Host must be a not empty string.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }

      _must_connect = !m_db_connected || (m_db_id != _db_id || m_db_host != _db_host);

      if (_must_connect)
      {
        //m_db_connected = true;
        //m_db_id = _db_id;
        //m_db_host = txt_db_host.Text.Trim();
        //WGDB.Init(m_db_id, m_db_host, m_db_host);
        //WGDB.SetApplication("WCP_ClientSimulator", "18.001");
        //// TODO: ConnectAs call Exit(), if something goes wrong... so the application ends. Maybe it's better
        //// to test the connection and return here with error, but not exit!
        //WGDB.ConnectAs("WGROOT");
      }

      return true;
    } // ConnectDB

    //------------------------------------------------------------------------------
    // PURPOSE : Init the data needed for set accounts:
    //           - The DataTable for insert accounts.
    //           - The SQL Adapter to insert accounts.
    //           - The UPDATE commands to reset accounts.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void InitSQLAccounts()
    {
      SqlCommand _sql_cmd_insert;
      StringBuilder _sql_query;

      // If already init... return.
      if (m_dt_accounts != null)
      {
        return;
      }

      // Init the DataTable for insert accounts.
      m_dt_accounts = new DataTable("ACCOUNTS");
      m_dt_accounts.Columns.Add("TRACKDATA", Type.GetType("System.String"));


      // Init the SQL Adapter to insert accounts.
      _sql_query = new StringBuilder();
      _sql_query.AppendLine("INSERT INTO ACCOUNTS ");
      _sql_query.AppendLine("           ( AC_TYPE ");
      _sql_query.AppendLine("           , AC_BLOCKED ");
      _sql_query.AppendLine("           , AC_TRACK_DATA) ");
      _sql_query.AppendLine("VALUES     ( 2, 0, @pTrackData )");

      _sql_cmd_insert = new SqlCommand(_sql_query.ToString());
      _sql_cmd_insert.Parameters.Add("@pTrackData", SqlDbType.NVarChar).SourceColumn = "TRACKDATA";

      m_adap_accounts = new SqlDataAdapter();
      m_adap_accounts.SelectCommand = null;
      m_adap_accounts.InsertCommand = _sql_cmd_insert;
      m_adap_accounts.UpdateCommand = null;
      m_adap_accounts.DeleteCommand = null;
      m_adap_accounts.ContinueUpdateOnError = true;

      m_adap_accounts.UpdateBatchSize = 500;
      m_adap_accounts.InsertCommand.UpdatedRowSource = UpdateRowSource.None;


      // Init the UPDATE commands to reset accounts.
      _sql_query = new StringBuilder();
      _sql_query.AppendLine("UPDATE ACCOUNTS ");
      _sql_query.AppendLine("   SET AC_LAST_TERMINAL_ID         = AC_CURRENT_TERMINAL_ID ");
      _sql_query.AppendLine("     , AC_LAST_TERMINAL_NAME       = AC_CURRENT_TERMINAL_NAME ");
      _sql_query.AppendLine("     , AC_LAST_PLAY_SESSION_ID     = AC_CURRENT_PLAY_SESSION_ID ");
      _sql_query.AppendLine("     , AC_CURRENT_TERMINAL_ID      = NULL ");
      _sql_query.AppendLine("     , AC_CURRENT_TERMINAL_NAME    = NULL ");
      _sql_query.AppendLine("     , AC_CURRENT_PLAY_SESSION_ID  = NULL ");
      _sql_query.AppendLine("     , AC_TYPE                     = 2 ");
      _sql_query.AppendLine(" WHERE AC_TRACK_DATA              >= @pTrackDataMin ");
      _sql_query.AppendLine("   AND AC_TRACK_DATA              <= @pTrackDataMax ");
      _sql_query.AppendLine("   AND AC_CURRENT_TERMINAL_ID IS NOT NULL ");

      m_cmd_reset_accounts = new SqlCommand(_sql_query.ToString());
      m_cmd_reset_accounts.Parameters.Add("@pTrackDataMin", SqlDbType.NVarChar);
      m_cmd_reset_accounts.Parameters.Add("@pTrackDataMax", SqlDbType.NVarChar);


      //////_sql_query = new StringBuilder();
      //////_sql_query.AppendLine("UPDATE ACCOUNTS ");
      //////_sql_query.AppendLine("   SET AC_BALANCE         = 10000 ");
      //////_sql_query.AppendLine("     , AC_INITIAL_CASH_IN = 10000 ");
      //////_sql_query.AppendLine("     , AC_LAST_ACTIVITY   = GETDATE() ");
      //////_sql_query.AppendLine("     , AC_CREATED         = GETDATE() ");
      //////_sql_query.AppendLine(" WHERE AC_TRACK_DATA     >= @pTrackDataMin ");
      //////_sql_query.AppendLine("   AND AC_TRACK_DATA     <= @pTrackDataMax ");

      //////m_cmd_set_balance_accounts = new SqlCommand(_sql_query.ToString());
      //////m_cmd_set_balance_accounts.Parameters.Add("@pTrackDataMin", SqlDbType.NVarChar);
      //////m_cmd_set_balance_accounts.Parameters.Add("@pTrackDataMax", SqlDbType.NVarChar);

    } // InitSQLAccounts

    //------------------------------------------------------------------------------
    // PURPOSE : Load last used data: Test Case, base card, etc.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void LoadLastUsedData()
    {
      String _file;
      String _line;
      String[] _fields;
      StreamReader _sr;
      String _test_case;
      String _wcp_service_address;

      try
      {
        _file = "LAST_SESSION";

        if (!File.Exists(_file))
        {
          return;
        }

        using (_sr = new StreamReader(_file))
        {
          if ((_line = _sr.ReadLine()) != null)
          {
            _fields = _line.Split(',');

            _test_case = _fields[0].Trim().ToUpper();
            _wcp_service_address = _fields[1].Trim();

            txt_base_card.Text = _fields[2].Trim();
            txt_num_terminals.Text = _fields[3].Trim();
            txt_db_host.Text = _fields[5].Trim();
            txt_db_id.Text = _fields[6].Trim();
            if (!cmb_prefix.Items.Contains(_test_case))
            {
              cmb_prefix.Items.Add(_test_case);
            }
            cmb_prefix.Text = _test_case;

            if (!cmb_service_address.Items.Contains(_wcp_service_address))
            {
              cmb_service_address.Items.Add(_wcp_service_address);
            }
            cmb_service_address.Text = _wcp_service_address;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // LoadLastUsedData

    //------------------------------------------------------------------------------
    // PURPOSE : Save last used data: Test Case, base card, etc.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void SaveLastUsedData()
    {
      String _file;
      StringBuilder _line;
      StreamWriter _sw;

      try
      {
        _file = "LAST_SESSION";
        _sw = new StreamWriter(_file);

        _line = new StringBuilder();
        _line.Append(cmb_prefix.Text);
        _line.Append(",");
        _line.Append(cmb_service_address.Text);
        _line.Append(",");
        _line.Append(txt_base_card.Text);
        _line.Append(",");
        _line.Append(txt_num_terminals.Text);
        _line.Append(",");
        _line.Append("True");
        _line.Append(",");
        _line.Append(txt_db_host.Text);
        _line.Append(",");
        _line.Append(txt_db_id.Text);
        _line.Append(",");
        _sw.WriteLine(_line.ToString());

        _sw.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    private void frm_client_simulator_FormClosed(object sender, FormClosedEventArgs e)
    {
      Environment.Exit(0);
    }


    private void btn_report_cash_session_pending_closing_Click(object sender, EventArgs e)
    {
      foreach (SimulatedTerminal t in GLB_Terminals)
      {
        SendRequestFrom(t, WCP_MsgTypes.WCP_MsgCashSessionPendingClosing);
      }
    }

  }

  class Statistics
  {
    public double time_max;
    public double time_sum;




    public Int64 count;
    public Int64 errors_count;
    public String name;
    public Int64 timeouts;
    public Int64 sent;
  }

  class TerminalGameMeters
  {
    public String terminal;
    public Int64 played_count;
    public Int64 played_cents;
    public Int64 won_count;
    public Int64 won_cents;
    public Int64 jackpot_cents;
  }

  class TerminalFundTransferMeters
  {
    public String terminal;
    public Int64 to_gm_count;
    public Int64 to_gm_cents;
    public Int64 from_gm_count;
    public Int64 from_gm_cents;
  }
}