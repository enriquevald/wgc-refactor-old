using System;
using System.IO;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using WSI.Common;
using WSI.WCP;
using System.Collections.Generic;
using System.Threading;
using System.Collections;
using System.Windows.Forms;

namespace WSI.WCP_ClientSimulator
{
  public delegate void OnMessageReceived(SimulatedTerminal Sender, String Xml);

  public class SimulatedTerminal
  {
    private List<WCP_Message> m_msg_pending_reply = new List<WCP_Message>();
    private Hashtable m_msg_last_send = new Hashtable();
    private Random m_rng = new Random();
    public int m_tick = Environment.TickCount;


    static Hashtable m_statistics = new Hashtable();
    static Int32 m_statistics_started_tick;
    static long m_statistics_last_interval;


    protected virtual void ProcessResponse(WCP_Message Request, WCP_Message Response)
    {
      return;
    }

    private void CommonProcessResponse(WCP_Message Request, WCP_Message Response)
    {
      if (Response.MsgHeader.ResponseCode != WCPPlayerRequestResponseCodes.WCP_RC_OK
          && Response.MsgHeader.ResponseCode != WCPPlayerRequestResponseCodes.WCP_RC_WKT_OK)
      {
        if (Response.MsgHeader.ResponseCode == WCPPlayerRequestResponseCodes.WCP_RC_SERVER_SESSION_NOT_VALID
            || Response.MsgHeader.ResponseCode == WCPPlayerRequestResponseCodes.WCP_RC_TERMINAL_SESSION_NOT_VALID)
        {
          this.Disconnect();
        }
        return;
      }

      switch (Response.MsgHeader.MsgType)
      {
        case WCPPlayerRequestMsgTypes.WCP_MsgEnrollTerminalReply:
          {
            WCP_MsgEnrollTerminalReply _reply;

            _reply = (WCP_MsgEnrollTerminalReply)Response.MsgContent;

            this.sequence_id = Math.Max(1, Math.Max(this.sequence_id, _reply.LastSequenceId + 1));
            this.transaction_id = Math.Max(1, Math.Max(this.transaction_id, _reply.LastTransactionId + 1));
            this.terminal_session_id = Response.MsgHeader.TerminalSessionId;
            this.enrolled = true;

            SendRequestFrom(this, WCPPlayerRequestMsgTypes.WCP_MsgGetParameters);

          }
          break;


        case WCPPlayerRequestMsgTypes.WCP_MsgGetParametersReply:
          {
            WCP_MsgGetParametersReply _reply;
            _reply = (WCP_MsgGetParametersReply)Response.MsgContent;
            this.terminal_id = (Int32)_reply.MachineId;
            this.terminal_system_id = _reply.SystemId;
          }
          break;


        case WCPPlayerRequestMsgTypes.WCP_MsgStartCardSessionReply:
          {
            WCP_MsgStartCardSessionReply _reply;
            _reply = (WCP_MsgStartCardSessionReply)Response.MsgContent;

            this.play_session_id = _reply.CardSessionId;
            this.player_name = _reply.HolderName;
            this.credit_cents += _reply.CardBalance;

            this.ToGmCount++;
            this.ToGmCount %= 100000000;
            this.ToGmCents += _reply.CardBalance;
            this.ToGmCents %= 10000000000;
            this.FundTransferMetersChanged = true;

            this.pending_plays = 10 + m_rng.Next(20);
          }
          break;


        case WCPPlayerRequestMsgTypes.WCP_MsgReportPlayReply:
          break;

        case WCPPlayerRequestMsgTypes.WCP_MsgEndSessionReply:


          break;

        case WCPPlayerRequestMsgTypes.WCP_MsgKeepAliveReply:
          break;

        case WCPPlayerRequestMsgTypes.WCP_MsgGetCardTypeReply:
          {
            WCP_MsgGetCardTypeReply _reply;
            _reply = (WCP_MsgGetCardTypeReply)Response.MsgContent;

            if (_reply.CardType == WCP_CardTypes.CARD_TYPE_PLAYER
                || _reply.CardType == WCP_CardTypes.CARD_TYPE_PLAYER_PIN)
            {
              SendRequestFrom(this, WCPPlayerRequestMsgTypes.WCP_MsgStartCardSession);
            }
          }
          break;

        default:
          ProcessResponse(Request, Response);
          break;
      }
    }



    private void NewXmlStringReceived(Object Sender, String NewXmlString)
    {
      String _xml_string;
      String _xml_message;
      int _idx0;
      int _idx1;

      try
      {
        XmlBuffer.Append(NewXmlString);

        while (XmlBuffer.Length > 0)
        {
          _xml_string = XmlBuffer.ToString();
          XmlBuffer.Length = 0;

          // Starting Tag
          _idx0 = _xml_string.IndexOf("<WCP_Message>");
          if (_idx0 < 0)
          {
            _idx1 = Math.Min(XmlBuffer.Length, "<WCP_Message>".Length);
            _xml_string = _xml_string.Substring(_idx1);

            XmlBuffer.Append(_xml_string);

            return;
          }

          _idx1 = _xml_string.IndexOf("</WCP_Message>");
          if (_idx1 < 0)
          {
            return;
          }

          _idx1 = _idx1 + "</WCP_Message>".Length;

          _xml_message = _xml_string.Substring(_idx0, _idx1 - _idx0);
          XmlBuffer.Append(_xml_string.Substring(_idx1));

          try
          {
            WCP_Message _wcp_response;
            _wcp_response = WCP_Message.CreateMessage(_xml_message);
            ResponseReceived(_wcp_response);
          }
          catch
          { }
        }
      }
      catch
      { }
    }


    public static void SendRequestFrom(SimulatedTerminal Terminal, WCPPlayerRequestMsgTypes MsgType)
    {
      WCP_Message _wcp_request;

      try
      {
        Int64 _interval = Environment.TickCount - Terminal.m_tick_last_send;


        _wcp_request = WCP_Message.CreateMessage(MsgType);
        _wcp_request.MsgHeader.ServerId = "";
        _wcp_request.MsgHeader.ServerSessionId = 0;

        _wcp_request.MsgHeader.TerminalId = Terminal.terminal_external_id;
        _wcp_request.MsgHeader.TerminalSessionId = Terminal.terminal_session_id;
        _wcp_request.MsgHeader.SequenceId = Terminal.sequence_id;
        Terminal.sequence_id++;

        switch (MsgType)
        {
          case WCPPlayerRequestMsgTypes.WCP_MsgEnrollTerminal:
            {
              WCP_MsgEnrollTerminal _request;

              _request = (WCP_MsgEnrollTerminal)_wcp_request.MsgContent;

              _wcp_request.MsgHeader.ServerSessionId = 0;
              _wcp_request.MsgHeader.TerminalSessionId = 0;
              _wcp_request.MsgHeader.SequenceId = 0;
              _request.ProviderId = "WSI";

              _request.TerminalType = TerminalTypes.SAS_HOST;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgGetCardType:
            {
              WCP_MsgGetCardType _request;

              _request = (WCP_MsgGetCardType)_wcp_request.MsgContent;
              _request.SetTrackData(0, Terminal.external_track);
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgStartCardSession:
            {
              WCPPlayerRequestMsgStartCardSession _request;

              _request = (WCPPlayerRequestMsgStartCardSession)_wcp_request.MsgContent;
              _request.SetTransactionId(Terminal.transaction_id++);
              _request.SetTrackData(0, Terminal.external_track);
              _request.SetPin("0000");
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgEndSession:
            {
              WCPPlayerRequestMsgEndSession _request;

              _request = (WCPPlayerRequestMsgEndSession)_wcp_request.MsgContent;

              _request.CardSessionId = Terminal.play_session_id;
              _request.CardBalance = Terminal.credit_cents;
              _request.HasCounters = false;
              // RCI 09-AUG-2010: Add TransactionId to MsgEndSession.
              _request.TransactionId = Terminal.transaction_id;
              Terminal.transaction_id++;

              _request.HasCounters = true;
              _request.PlayedCount = Terminal.PlayedCount - Terminal.InitialSession_PlayedCount;
              _request.PlayedCents = Terminal.PlayedCents - Terminal.InitialSession_PlayedCents;
              _request.WonCount = Terminal.WonCount - Terminal.InitialSession_WonCount;
              _request.WonCents = Terminal.WonCents - Terminal.InitialSession_WonCents;
              _request.JackpotCents = Terminal.JackpotCents - Terminal.InitialSession_JackpotCents;

              Terminal.play_session_id = 0;
              Terminal.player_name = String.Empty;
              Terminal.credit_cents = 0;
              Terminal.FromGmCount++;
              Terminal.FromGmCount %= 100000000;
              Terminal.FromGmCents += Terminal.credit_cents;
              Terminal.FromGmCents %= 10000000000;
              Terminal.FundTransferMetersChanged = true;

            }
            break;
          case WCPPlayerRequestMsgTypes.WCP_MsgReportPlay:
            {
              WCP_MsgReportPlay request;
              Random _rnd;
              Int64 _jackpot_cents;

              request = (WCP_MsgReportPlay)_wcp_request.MsgContent;

              _rnd = new Random(DateTime.Now.Millisecond);
              request.CardSessionId = Terminal.play_session_id;
              request.Denomination = 25;
              request.GameId = "Game_" + _rnd.Next(10).ToString();
              request.PayoutPercentage = 98.76;
              request.PlayedAmount = 25;

              request.WonAmount = 0;
              if (_rnd.Next(100) < 25)
              {
                request.WonAmount = 10 * _rnd.Next(19);
              }

              // RCI 03-AUG-2010: If no money remains, won a huge amount to recover balance.
              if (Terminal.credit_cents - request.PlayedAmount <= 0)
              {
                request.WonAmount = 1000000;
              }

              if (request.WonAmount > 0)
              {
                Terminal.WonCount++;
                Terminal.WonCents += request.WonAmount;
              }

              _jackpot_cents = 0;

              if (_rnd.Next(20000) < 2)
              {
                _jackpot_cents = (175 * _rnd.Next(100));
              }

              request.SessionBalanceBeforePlay = Terminal.credit_cents;
              request.SessionBalanceAfterPlay = request.SessionBalanceBeforePlay - request.PlayedAmount + request.WonAmount;

              // Update Game Meters
              Terminal.PlayedCount++;
              Terminal.PlayedCents += request.PlayedAmount;

              Terminal.JackpotCents += _jackpot_cents;

              request.TransactionId = Terminal.transaction_id;
              Terminal.transaction_id++;

              Terminal.credit_cents -= request.PlayedAmount;
              Terminal.credit_cents += request.WonAmount;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgPlayConditions:
            {
              WCP_MsgPlayConditions request;
              Random rnd;

              request = (WCP_MsgPlayConditions)_wcp_request.MsgContent;

              rnd = new Random();
              request.CardSessionId = Terminal.play_session_id;
              request.GameId = "Game_" + rnd.Next(10).ToString();
              request.BetAmount = 25;
              request.SessionBalanceBeforePlay = Terminal.credit_cents;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgPlaySessionMeters:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_TITO_MsgIsValidTicket:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_TITO_MsgEgmParameters:
            {
              MessageBox.Show("Not implemented");

            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_TITO_MsgCreateTicket:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgSasMeters:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          //SSC 01-MAR-2012: Added Money Event
          case WCPPlayerRequestMsgTypes.WCP_MsgMoneyEvent:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgAddCredit:
            {
              MessageBox.Show("Not implemented");
            }
            break;

          //JCM 30-MAY-2012: Get Cashier Paremeters
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgGetParameters:
            {
              WCP_WKT_MsgGetParameters _request;

              _request = (WCP_WKT_MsgGetParameters)_wcp_request.MsgContent;
            }
            break;





          case WCPPlayerRequestMsgTypes.WCP_MsgUpdateCardSession:
            {
              WCP_MsgUpdateCardSession request;

              request = (WCP_MsgUpdateCardSession)_wcp_request.MsgContent;

              request.CardSessionId = Terminal.play_session_id;
              request.PlayedCount = Terminal.PlayedCount - Terminal.InitialSession_PlayedCount;
              request.PlayedCents = Terminal.PlayedCents - Terminal.InitialSession_PlayedCents;
              request.WonCount = Terminal.WonCount - Terminal.InitialSession_WonCount;
              request.WonCents = Terminal.WonCents - Terminal.InitialSession_WonCents;
              request.JackpotCents = Terminal.JackpotCents - Terminal.InitialSession_JackpotCents;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgReportEvent:
            {
              WCP_MsgReportEvent request;
              Random rnd;

              request = (WCP_MsgReportEvent)_wcp_request.MsgContent;

              request.EventType = Terminal.ev_type;
              request.EventDateTime = DateTime.Now;
              request.EventData = "";

              if ((Terminal.ev_type == WCP_EventTypesCodes.WCP_EV_DOOR_OPENED) || (Terminal.ev_type == WCP_EventTypesCodes.WCP_EV_DOOR_CLOSED))
              {
                rnd = new Random();
                if (rnd.Next(2) == 0)
                {
                  request.EventData = "Up Door";
                }
                else
                {
                  request.EventData = "Down Door";
                }
              }

              if (Terminal.ev_type == WCP_EventTypesCodes.WCP_EV_SHUTDOWN)
              {
                Terminal.ev_type = WCP_EventTypesCodes.WCP_EV_DOOR_OPENED;
              }
              else
              {
                Terminal.ev_type++;
              }
            }
            break;


          case WCPPlayerRequestMsgTypes.WCP_MsgReportEventList:
            {
              WCP_MsgReportEventList request;
              ArrayList device_status_list = new ArrayList();
              ArrayList operations_list = new ArrayList();

              WCP_DeviceStatus device_status;
              WCP_Operation operation;

              request = (WCP_MsgReportEventList)_wcp_request.MsgContent;

              device_status = new WCP_DeviceStatus();

              // Create one device status
              request.NumDeviceStatus = 1;
              device_status.Code = DeviceCodes.WCP_DEVICE_CODE_NOTE_ACCEPTOR;
              device_status.Status = DeviceStatus.WCP_DEVICE_STATUS_COIN_ACCEPTOR_JAM;
              device_status.Priority = DevicePriority.WCP_DEVICE_PRIORITY_ERROR;
              device_status.LocalTime = DateTime.Now;

              device_status_list.Add(device_status);
              request.DeviceStatusList = device_status_list;

              operation = new WCP_Operation();

              // Create one operation
              request.NumOperations = 1;
              operation.Code = OperationCodes.WCP_OPERATION_CODE_EVENT;
              operation.Data = 0x0001007A;
              operation.LocalTime = DateTime.Now;

              operations_list.Add(operation);
              request.OperationList = operations_list;
            }
            break;


          case WCPPlayerRequestMsgTypes.WCP_MsgReportMeters:
            {
              WCP_MsgReportMeters request;
              WCP_MoneyMeter money_meter_item;
              Random rnd;

              request = (WCP_MsgReportMeters)_wcp_request.MsgContent;

              rnd = new Random();
              for (int i = 0; i < 5; i++)
              {
                money_meter_item = new WCP_MoneyMeter();

                money_meter_item.CashType = WCP_CashTypes.CASH_TYPE_CASH_IN;
                money_meter_item.MoneyType = WCP_MoneyTypes.MONEY_TYPE_COIN;
                money_meter_item.Count = (6 - i);
                money_meter_item.Amount = (i + 1) * 500 * (6 - i);
                money_meter_item.FaceValue = (i + 1) * 500;

                request.MoneyMeters.Add(money_meter_item);
              }

              request.CashInAmount = 40000;
              request.CashOutAmount = 5000;
              request.PlayedAmount = 25000;
              request.PlayedCount = 15;
              request.WonCount = rnd.Next(10);
              request.WonAmount = request.WonCount * 10000;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgReportGameMeters:
            {
              WCP_MsgReportGameMeters request;

              request = (WCP_MsgReportGameMeters)_wcp_request.MsgContent;

              request.GameId = "WCP SIMULATION";
              request.GameBaseName = "WCP SIMULATION";
              request.DenominationCents = 1;
              request.PlayedCount = Terminal.PlayedCount;
              request.PlayedCountMaxValue = 99999999 + 1;
              request.PlayedCents = Terminal.PlayedCents;
              request.PlayedMaxValueCents = 999999999 + 1;
              request.WonCount = Terminal.WonCount;
              request.WonCountMaxValue = 99999999 + 1;
              request.WonCents = Terminal.WonCents;
              request.WonMaxValueCents = 999999999 + 1;
              request.JackpotCents = Terminal.JackpotCents;
              request.JackpotMaxValueCents = 999999999 + 1;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgReportMachineMeters:
            {
              WCP_MsgReportMachineMeters request;

              Terminal.FundTransferMetersChanged = false;
              request = (WCP_MsgReportMachineMeters)_wcp_request.MsgContent;

              request.PlayWonAvalaible = true;
              request.PlayedQuantity = Terminal.PlayedCount;
              request.PlayedMaxValueQuantity = 99999999 + 1;
              request.PlayedCents = Terminal.PlayedCents;
              request.PlayedMaxValueCents = 9999999999 + 1;
              request.WonQuantity = Terminal.WonCount;
              request.WonMaxValueQuantity = 99999999 + 1;
              request.WonCents = Terminal.WonCents;
              request.WonMaxValueCents = 9999999999 + 1;
              request.FundTransferType = WCP_FundTransferType.FUND_TRANSFER_AFT;
              request.ToGmQuantity = Terminal.ToGmCount;
              request.ToGmMaxValueQuantity = 99999999 + 1;
              request.ToGmCents = Terminal.ToGmCents;
              request.ToGmMaxValueCents = 9999999999 + 1;
              request.FromGmQuantity = Terminal.FromGmCount;
              request.FromGmMaxValueQuantity = 99999999 + 1;
              request.FromGmCents = Terminal.FromGmCents;
              request.FromGmMaxValueCents = 9999999999 + 1;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgReportHPCMeter:
            {
              WCP_MsgReportHPCMeter request;

              request = (WCP_MsgReportHPCMeter)_wcp_request.MsgContent;

              request.HandPaysCents = Terminal.PlayedCents;  // wrong (correct: hpc cents)
              request.HandPaysMaxValueCents = 999999 + 1;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgIntegrityCheck:
            {
              WCP_MsgIntegrityCheck request;

              request = (WCP_MsgIntegrityCheck)_wcp_request.MsgContent;

              request.Crc32 = 0;
            }
            break;

          // Mobile Bank Transactions
          case WCPPlayerRequestMsgTypes.WCP_MsgMobileBankCardCheck:
          case WCPPlayerRequestMsgTypes.WCP_MsgMobileBankCardTransfer:
          case WCPPlayerRequestMsgTypes.WCP_MsgCashSessionPendingClosing:
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgGetResource:
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgGetPlayerGifts:
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgPlayerRequestGift:
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgGetPlayerDraws:
          case WCPPlayerRequestMsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbers:
            {
              MessageBox.Show("Not implemented");

              return;
            }
            break;

          case WCPPlayerRequestMsgTypes.WCP_MsgGetParameters:
            break;

          default:
            break;
        }

        if (_wcp_request != null)
        {
          Terminal.SendRequest(_wcp_request);

          if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgReportPlay)
          {
            Terminal.last_play_tick = Environment.TickCount;
          }

          if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgReportGameMeters)
          {
            Terminal.last_game_meters_tick = Environment.TickCount;
          }

          if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgReportMachineMeters)
          {
            Terminal.last_machine_meters_tick = Environment.TickCount;
          }

          if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgReportEventList)
          {
            Terminal.last_report_event_list_tick = Environment.TickCount;
          }

          if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgUpdateCardSession)
          {
            Terminal.last_update_card_session_tick = Environment.TickCount;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
    }




    private void UpdateStatistics(WCP_Message Request, WCP_Message Response, Boolean RequestTimeout)
    {
      Statistics _stats = null;
      String _str_type;
      WCPPlayerRequestMsgTypes _msg_type;
      WCP_Message _wcp_msg;
      Int64 _elapsed;
      Boolean _ok;
      Boolean _timeout;

      Boolean _is_reply;

      _elapsed = 0;
      _ok = true;
      _is_reply = false;
      _wcp_msg = Request;
      _timeout = false;

      if (Response != null)
      {
        _wcp_msg = Response;
        _is_reply = true;
        _elapsed = Misc.GetElapsedTicks(Request.m_created_tick);

        if (Response.MsgHeader.ResponseCode != WCPPlayerRequestResponseCodes.WCP_RC_OK
            && Response.MsgHeader.ResponseCode != WCPPlayerRequestResponseCodes.WCP_RC_WKT_OK)
        {
          _ok = false;
        }
      }

      if (Response == null && RequestTimeout)
      {
        _is_reply = true;
        _timeout = true;
      }

      _msg_type = Request.MsgHeader.MsgType;
      _str_type = _msg_type.ToString();

      lock (m_statistics)
      {
        Monitor.Enter(m_statistics);

        if (m_statistics.Count == 0)
        {
          m_statistics_started_tick = Environment.TickCount;
        }

        if (_is_reply)
        {
          if (m_statistics.ContainsKey(_msg_type))
          {
            _stats = (Statistics)m_statistics[_msg_type];

            if (_timeout)
            {
              _stats.timeouts++;
            }
            else if (!_ok)
            {
              _stats.errors_count++;
            }
            else
            {
              _stats.count++;
              _stats.time_max = Math.Max(_stats.time_max, _elapsed);
              _stats.time_sum += _elapsed;
            }
          }
        }
        else
        {
          if (m_statistics.ContainsKey(_msg_type))
          {
            _stats = (Statistics)m_statistics[_msg_type];
          }
          else
          {
            _stats = new Statistics();
            _stats.name = _str_type;
            m_statistics.Add(_msg_type, _stats);

          }
          _stats.sent++;
        }
        Monitor.Exit(m_statistics);
      }
    }

    public static void ClearStatistics()
    {
      lock (m_statistics)
      {
        Monitor.Enter(m_statistics);
        m_statistics_last_interval = 1000;
        m_statistics.Clear();
        Monitor.Exit(m_statistics);
      }
    }


    public static String StatsString()
    {
      String _str;
      double _trx_sec;
      long _interval;
      Hashtable _cloned_statistics;

      _str = "";

      _str = _str + " WCP Simulation \r\n";
      _str = _str + " \r\n";

      _str = _str + " Results " + " \r\n";
      _str = _str + " Message ".PadRight(30)
                  + " Average (ms) ".PadRight(15)
                  + " Max (ms) ".PadRight(15)
                  + " Sent ".PadRight(15)
                  + " Received ".PadRight(15)
                  + " Errors ".PadRight(15)
                  + " Timeout ".PadRight(15)
                    + " Trx/s ".PadRight(15)
                  + " \r\n";

      _interval = Misc.GetElapsedTicks(m_statistics_started_tick);
      m_statistics_last_interval = _interval;


      lock (m_statistics)
      {
        _cloned_statistics = (Hashtable)m_statistics.Clone();
      }

      foreach (Statistics _stat in _cloned_statistics.Values)
      {
        _trx_sec = (double)(_stat.count - _stat.errors_count) * 1000 / _interval;

        double _avg;

        _avg = 0;
        if (_stat.count > 0)
        {
          _avg = _stat.time_sum / _stat.count;
        }


        _str = _str + " " + _stat.name.PadRight(30)
                    + _avg.ToString("0").PadRight(15)
                    + _stat.time_max.ToString("0").PadRight(15)
                    + _stat.sent.ToString().PadRight(15)
                    + _stat.count.ToString().PadRight(15)
                    + _stat.errors_count.ToString().PadRight(15)
                    + _stat.timeouts.ToString().PadRight(15)
                    + _trx_sec.ToString("0.00").PadRight(15)
                    + " \r\n";
      }

      return _str;
    }




    public void SendRequest(WCP_Message Request)
    {
      Request.MsgHeader.TerminalId = this.terminal_external_id;
      Request.MsgHeader.TerminalSessionId = this.terminal_session_id;
      Request.MsgHeader.SequenceId = Interlocked.Increment(ref this.sequence_id);
      lock (m_msg_pending_reply)
      {
        TimeoutMsgPendingReply();

        m_msg_pending_reply.Add(Request);

        if (m_msg_last_send.ContainsKey(Request.MsgHeader.MsgType))
        {
          m_msg_last_send[Request.MsgHeader.MsgType] = Environment.TickCount;
        }
        else
        {
          m_msg_last_send.Add(Request.MsgHeader.MsgType, Environment.TickCount);
        }
      }

      this.Send(Request.ToXml());
      this.m_tick_last_send = Environment.TickCount;

      UpdateStatistics(Request, null, false);
    }

    public void ResponseReceived(WCP_Message Response)
    {
      WCP_Message _request;

      lock (m_msg_pending_reply)
      {
        _request = null;
        foreach (WCP_Message _item in m_msg_pending_reply)
        {
          if (_item.MsgHeader.SequenceId == Response.MsgHeader.SequenceId)
          {
            _request = _item;

            break;
          }
        }
      }

      if (_request == null)
      {
        return;
      }

      UpdateStatistics(_request, Response, false);

      CommonProcessResponse(_request, Response);

      lock (m_msg_pending_reply)
      {
        m_msg_pending_reply.Remove(_request);
      }
    }

    public Boolean IsThereAnyMsgPendingReply(WCPPlayerRequestMsgTypes MsgType)
    {
      return (NumMsgPendingReply(MsgType, 1) > 0);
    }

    public Boolean IsThereAnyMsgPendingReply()
    {
      lock (m_msg_pending_reply)
      {
        return (m_msg_pending_reply.Count > 0);
      }
    }


    protected int NumMsgPendingReply(WCPPlayerRequestMsgTypes MsgType)
    {
      return NumMsgPendingReply(MsgType, int.MaxValue);
    }

    protected int NumMsgPendingReply(WCPPlayerRequestMsgTypes MsgType, int MaxNum)
    {
      int _num_pending;

      _num_pending = 0;
      lock (m_msg_pending_reply)
      {
        TimeoutMsgPendingReply();

        foreach (WCP_Message _msg in m_msg_pending_reply)
        {
          if (_msg.MsgHeader.MsgType == MsgType)
          {
            _num_pending++;
            if (_num_pending >= MaxNum)
            {
              break;
            }
          }
        }
      }

      return _num_pending;
    }

    void TimeoutMsgPendingReply()
    {
      lock (m_msg_pending_reply)
      {
        WCP_Message _request_timeout;

        _request_timeout = null;
        foreach (WCP_Message _request in m_msg_pending_reply)
        {
          if (Misc.GetElapsedTicks(_request.m_created_tick) > 30000)
          {
            _request_timeout = _request;

            break;
          }
        }

        if (_request_timeout != null)
        {
          UpdateStatistics(_request_timeout, null, true);

          m_msg_pending_reply.Remove(_request_timeout);
        }
      }
    }





    public enum WCP_SimulationSteps
    {
      WCP_STEP_GET_CARD_TYPE
    ,
      WCP_STEP_START_CARD_SESSION
    ,
      WCP_STEP_GET_PARAMETERS
    ,
      WCP_STEP_BILL_IN
    ,
      WCP_STEP_EGM
    ,
      WCP_STEP_TICKET_OUT
    ,
      WCP_STEP_TICKET_IN
    ,
      WCP_STEP_CANCEL_TICKET
    ,
      WCP_STEP_SAS_METERS
    ,
      WCP_STEP_PLAY
    ,
      WCP_STEP_CHANGE_STACKER
    ,
      WCP_STEP_PLAY_SESSION_INFO
    ,
      WCP_STEP_END_CARD_SESSION
    ,
      WCP_STEP_FINISHED

    , WCP_STEP_GET_VALIDATION_NUMBER
    }

    const String WCP_ServiceCertificateId = "Andreu Juli� Quiles"; // <-- AJQ :-)
    TcpClient tcp_client;
    SslStream ssl_stream;
    Byte[] buffer;

    public String m_remote;

    public int idx;

    public OnMessageReceived MessageReceived;

    public String terminal_external_id;
    public Int32 terminal_id;
    public Int64 terminal_session_id;

    public Int64 sequence_id;
    public Int64 transaction_id;
    public Int32 terminal_system_id;

    public Int64 stacker_id;
    public Ticket simulation_ticket;

    public String external_track;
    public Int64 play_session_id;
    public Int64 credit_cents;

    public DateTime last_sent_message;
    public Int32 next_stacker_to_insert;

    public WCP_EventTypesCodes ev_type;

    public String player_name;

    public Int64 mb_authorized_amount;

    public Int64 last_sent_message_validation_number;

    // Simulation variables
    public Boolean enrolled;

    public int pending_plays;
    public int time_between_plays;
    public int last_play_tick;
    public WCP_SimulationSteps step;
    public int m_tick_last_send;


    // Game Meters
    public int last_game_meters_tick;
    public Int64 PlayedCount;
    public Int64 PlayedCents;
    public Int64 WonCount;
    public Int64 WonCents;
    public Int64 JackpotCents;

    // Fund Transfer Meters
    public int last_machine_meters_tick;
    public Boolean FundTransferMetersChanged;
    public Int64 ToGmCount;
    public Int64 ToGmCents;
    public Int64 FromGmCount;
    public Int64 FromGmCents;

    public int last_report_event_list_tick;
    public int last_update_card_session_tick;

    public Int64 InitialSession_PlayedCount;
    public Int64 InitialSession_PlayedCents;
    public Int64 InitialSession_WonCount;
    public Int64 InitialSession_WonCents;
    public Int64 InitialSession_JackpotCents;

    public StringBuilder XmlBuffer = new StringBuilder(16 * 1024);


    public SimulatedTerminal()
    {
      buffer = new Byte[16 * 1024];

      enrolled = false;
      next_stacker_to_insert = 1;
      ev_type = WCP_EventTypesCodes.WCP_EV_DOOR_OPENED;
      step = WCP_SimulationSteps.WCP_STEP_GET_CARD_TYPE;

      this.MessageReceived = new OnMessageReceived(this.NewXmlStringReceived);
    }


    public void Send(String Xml)
    {
      try
      {
        Byte[] _data;
        _data = Encoding.UTF8.GetBytes(Xml);
        ssl_stream.Write(_data, 0, _data.Length);

        m_tick_last_send = Environment.TickCount;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    public Boolean IsConnected
    {
      get
      {
        Boolean _connected;

        _connected = false;

        try
        {
          if (tcp_client == null || tcp_client.Client == null || ssl_stream == null)
          {
            return false;
          }

          _connected = tcp_client.Client.Connected && ssl_stream.IsAuthenticated;
        }
        catch { }

        return _connected;
      }
    }

    public void Disconnect()
    {
      try
      {
        if (ssl_stream != null) try { ssl_stream.Close(); }
          catch { };
        if (tcp_client != null) try { tcp_client.Close(); }
          catch { };

        enrolled = false;
        ssl_stream = null;
        tcp_client = null;

      }
      catch
      {
      }
    }

    public void Connect(String WCP_ServiceAddress)
    {
      try
      {
        IPEndPoint wcp_service_ip;

        wcp_service_ip = Misc.IPEndPointParse(WCP_ServiceAddress);

        // Step 1.
        // Instantiate a TcpClient with the target server and port number
        tcp_client = new TcpClient();

        tcp_client.Connect(wcp_service_ip);

        // Step 3.
        // Specify the callback function that will act as the validation delegate. This lets you inspect the certificate to see if it meets your
        // validation requirements.
        RemoteCertificateValidationCallback callback1 = new RemoteCertificateValidationCallback(OnCertificateValidation);
        LocalCertificateSelectionCallback callback2 = new LocalCertificateSelectionCallback(OnLocalCertificateSelectionCallback);
        // Step 4.
        // Instantiate an SslStream with the NetworkStream returned from the TcpClient.
        ssl_stream = new SslStream(tcp_client.GetStream(), true, callback1, callback2);

        // Step 5.
        // As a client, you can authenticate the server and validate the results using the SslStream.
        // This is the host name of the server you are connecting to, which may or may not be the name used
        // to connect to the server when TcpClient is instantiated.

        ssl_stream.AuthenticateAsClient(WCP_ServiceCertificateId);
        tcp_client.ReceiveBufferSize = 4 * 1024;
        tcp_client.SendBufferSize = 4 * 1024;

        ssl_stream.BeginRead(this.buffer, 0, this.buffer.Length,
                              new AsyncCallback(ReceiveCallback), this);
      }
      catch
      {
      }
    }

    static void ReceiveCallback(IAsyncResult AsyncResult)
    {
      SimulatedTerminal _terminal;
      String _xml;
      Boolean _disconnect;

      int _num_rcvd_bytes;

      _terminal = (SimulatedTerminal)AsyncResult.AsyncState;
      _disconnect = true;

      try
      {
        if (!_terminal.IsConnected)
        {
          return;
        }

        _num_rcvd_bytes = _terminal.ssl_stream.EndRead(AsyncResult);
        if (_num_rcvd_bytes > 0)
        {
          _xml = Encoding.UTF8.GetString(_terminal.buffer, 0, _num_rcvd_bytes);
          _terminal.MessageReceived(_terminal, _xml);
          _terminal.ssl_stream.BeginRead(_terminal.buffer, 0, _terminal.buffer.Length,
                                         new AsyncCallback(ReceiveCallback), _terminal);
          _disconnect = false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_disconnect)
        {
          _terminal.Disconnect();
        }
      }
    }


    private static X509Certificate OnLocalCertificateSelectionCallback(Object sender,
                                                                        string targetHost,
                                                                        X509CertificateCollection localCertificates,
                                                                        X509Certificate remoteCertificate,
                                                                        string[] acceptableIssuers)
    {
      X509Certificate2 xx509;

      // Send this certificate as the client certificate
      xx509 = new X509Certificate2(@"WCP_ServerCertificate.pfx", "xixero08");

      return xx509;
    }


    // Check the certificate for errors and to make sure it meets your security policy.
    private static bool OnCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
      return true;
    }






  }
}
