﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
	/// <summary>
	/// Types in a container message
	/// </summary>
	public enum BusinessExceptionMessageItem
	{
		MessageProcessingServicesUnavailable
	}
}