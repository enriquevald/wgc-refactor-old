﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
	/// <summary>
	/// Type of containers
	/// </summary>
	public enum BusinessExceptionContainer
	{
		Ticket,
		Message
	}
}
