﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
	public enum ClientType
	{
		Egm,
		Kiosk,
		Cashier,
		S2SAll
	}
}