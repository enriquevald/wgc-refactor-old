﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
    public enum TicketAction
    {
        Issued,
        Redeemed,
        Ejected,
    }
}