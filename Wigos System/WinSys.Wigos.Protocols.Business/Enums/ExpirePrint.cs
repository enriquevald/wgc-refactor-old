﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
    public enum ExpirePrint
    {
        NumDays,
        EgmExpiry,
        EgmCageExpiry,
        None
    }
}
