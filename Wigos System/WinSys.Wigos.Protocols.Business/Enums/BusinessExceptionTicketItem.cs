﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
	/// <summary>
	/// Types in a container ticket
	/// </summary>
	public enum BusinessExceptionTicketItem
	{
		InvalidTicketAction,
		InvalidTicketIdentifier,
		TicketHasBeenRedeemed,
		TicketHasBeenVoided,
		TicketRedemptionVoidPending,
		TicketHasExpired,
		DuplicateTransactionId,
		JackpotCannotBeRedeemedAtThisClient,
		InvalidDateRangeSpecifiedForReport,
		UnsupportedTransferProtocol,
		InvalidClientTypeSpecified,

		PlayerCardRequiredButNotSupplied,
		IncorrectPlayerCard,

		TicketCommitNotAcknowledged,
		TicketIssueNotAcknowledged,
		InvalidTicketConfigurationParameters
	}
}