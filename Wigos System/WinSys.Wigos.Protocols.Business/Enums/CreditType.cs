﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
    public enum CreditType
    {
        Cashable,
        Promo,
        NonCash,
        PromoPool,
        NonCashPool
    }
}
