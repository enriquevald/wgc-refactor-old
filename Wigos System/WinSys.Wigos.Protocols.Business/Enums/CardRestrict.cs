﻿namespace WinSys.Wigos.Protocols.Business.Enums
{
    public enum CardRestrict
    {
        AnyCard,
        ThisCard,
        NoCard
    }
}