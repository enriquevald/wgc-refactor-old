﻿using System;

using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class IssueTicketDto
	{
		public string ClientId { get; set; }
		public ClientType ClientType { get; set; }
		public string ValidationNumber { get; set; }
		public long TicketAmount { get; set; }
		public string CurrencyId { get; set; }
		public bool IsJackpot { get; set; }
		public int TransferAction { get; set; }
		public long TransferAmount { get; set; }
		public long TransferException { get; set; }
		public DateTime TransferDateTime { get; set; }
		public long TransferSequence { get; set; }
		public CreditType CreditType { get; set; }
		public DateTime EgmDateTime { get; set; }
		public DateTime CageDateTime { get; set; }
		public int TransactionId { get; set; }
		public int PoolId { get; set; }
		public string CardId { get; set; }
		public CardRestrict CardRestrict { get; set; }
		public DateTime ExpireDateTime { get; set; }
		public int ConfigurationId { get; set; }
	}
}