﻿using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class RedeemTicketDto
	{
		public string ClientId { get; set; }
		public ClientType ClientType { get; set; }
		public string ValidationNumber { get; set; }
		public int TransactionId { get; set; }
		public string CardId { get; set; }
	}
}