﻿using System;

using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class TicketDto
	{
		public long Id { get; set; }
		public long ValidationNumber { get; set; }
		public decimal Amount { get; set; }
		public TITO_TICKET_STATUS Status { get; set; }
		public TITO_TICKET_TYPE Type { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}