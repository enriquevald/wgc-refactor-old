﻿using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class GetTicketConfigDto
	{
		public ClientType ClientType { get; set; }
		public string ClientId { get; set; }
	}
}