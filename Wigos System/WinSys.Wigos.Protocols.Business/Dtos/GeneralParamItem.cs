﻿namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class GeneralParamItem
	{
		public string GroupKey { get; set; }
		public string SubjectKey { get; set; }
		public string Value { get; set; }
	}
}