﻿using System;

using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class AuthorizeTicketDto
	{
		public string ClientId { get; set; }
		public ClientType ClientType { get; set; }
		public string ValidationNumber { get; set; }
		public long TicketAmount { get; set; }
		public string CurrencyId { get; set; }
		public bool IsJackpot { get; set; }
		public CreditType CreditType { get; set; }
		public DateTime EgmDateTime { get; set; }
		public DateTime CageDateTime { get; set; }
		public int TransactionId { get; set; }
		public int PoolId { get; set; }
		public string CardId { get; set; }
		public CardRestrict CardRestrict { get; set; }
	}
}
