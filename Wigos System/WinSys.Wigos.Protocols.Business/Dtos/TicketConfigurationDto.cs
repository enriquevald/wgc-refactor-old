﻿using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class TicketConfigurationDto
	{
		public string ClientId { get; set; }
		public ClientType ClientType { get; set; }
		public int ConfigurationId { get; set; }
		public int ExpireEgm { get; set; }
		public int ExpireCage { get; set; }
		public ExpirePrint ExpirePrint { get; set; }
		public bool IsRoundUp { get; set; }
		public int RoundTime { get; set; }
		public string Location { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string TitleCash { get; set; }
		public string TitlePromo { get; set; }
		public string TitleNonCash { get; set; }
		public string TitleJackpot { get; set; }
	}
}