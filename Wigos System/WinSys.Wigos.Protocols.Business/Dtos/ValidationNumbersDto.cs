﻿using System;
using System.Collections.Generic;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class ValidationNumbersDto
	{
		public List<string> ValidationNumbers { get; set; }
		public DateTime SeedDateTime { get; set; }
		public long Seed1 { get; set; }
		public long Seed2 { get; set; }
	}
}