﻿using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Dtos
{
	public class AckTicketDto
	{
		public string ClientId { get; set; }
		public ClientType TerminalType { get; set; }
		public int ConfigurationId { get; set; }
		public int TransactionId { get; set; }
	}
}