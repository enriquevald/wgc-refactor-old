﻿using System;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket is not expired
	/// </summary>
	public class TicketNotExpiredValidator : ITicketNotExpiredValidator
	{
		/// <summary>
		/// Validates that the ticket is not expired
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			if (item.Status == TITO_TICKET_STATUS.EXPIRED)
			{
				DateTime expirationLimitDate = this.GetCalculatedGracePeriodFromExpirationDate(item.ExpirationDate);

				if (expirationLimitDate == DateTime.MinValue || item.ExpirationDate > expirationLimitDate)
				{
					throw new BusinessException(
						BusinessExceptionContainer.Ticket,
						BusinessExceptionTicketItem.TicketHasExpired);
				}
			}
		}

		private DateTime GetCalculatedGracePeriodFromExpirationDate(DateTime expirationDate)
		{
			object gracePeriod = Misc.GetTITOPaymentGracePeriod();
			DateTime gracePeriodLimit = DateTime.MinValue;

			if (gracePeriod != null)
			{
				if (gracePeriod is Int32)
				{
					gracePeriodLimit = expirationDate.AddDays((Int32)gracePeriod);
				}
				else if (gracePeriod is DateTime)
				{
					DateTime gracePeriodDateTime = (DateTime)gracePeriod;
					gracePeriodLimit = new DateTime(
						expirationDate.Year,
						gracePeriodDateTime.Month,
						gracePeriodDateTime.Day);
				}
			}

			return (expirationDate > gracePeriodLimit) ? gracePeriodLimit.AddYears(1) : gracePeriodLimit;
		}
	}
}