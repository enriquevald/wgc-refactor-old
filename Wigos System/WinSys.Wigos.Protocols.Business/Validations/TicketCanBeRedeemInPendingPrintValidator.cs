﻿using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams;
using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket can be redeemed if its state is pending print
	/// </summary>
	public class TicketCanBeRedeemInPendingPrintValidator : ITicketCanBeRedeemInPendingPrintValidator
	{
		/// <summary>
		/// Validates that the ticket can be redeemed if its state is pending print
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			if (item.Status == TITO_TICKET_STATUS.PENDING_PRINT 
			    && !Tito.IsPendingPrintAllowToRedeem)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.TicketRedemptionVoidPending);	
			}
		}
	}
}
