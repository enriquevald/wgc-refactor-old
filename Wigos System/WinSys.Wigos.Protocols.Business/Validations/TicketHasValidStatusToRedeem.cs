﻿using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;

using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket has a valid status to redeem it
	/// </summary>
	public class TicketHasValidStatusToRedeem : ITicketHasValidStatusToRedeem
	{
		private readonly ITicketNotExpiredValidator ticketNotExpiredValidator;
		private readonly ITicketNotDiscardedValidator ticketNotDiscardedValidator;
		private readonly ITicketNotRedeemedValidator ticketNotRedeemedValidator;
		private readonly ITicketCanBeRedeemInPendingPrintValidator ticketCanBeRedeemInPendingPrintValidator;

		public TicketHasValidStatusToRedeem(
			ITicketNotExpiredValidator ticketNotExpiredValidator,
			ITicketNotDiscardedValidator ticketNotDiscardedValidator,
			ITicketNotRedeemedValidator ticketNotRedeemedValidator,
			ITicketCanBeRedeemInPendingPrintValidator ticketCanBeRedeemInPendingPrintValidator)
		{
			this.ticketNotExpiredValidator = ticketNotExpiredValidator;
			this.ticketNotDiscardedValidator = ticketNotDiscardedValidator;
			this.ticketNotRedeemedValidator = ticketNotRedeemedValidator;
			this.ticketCanBeRedeemInPendingPrintValidator = ticketCanBeRedeemInPendingPrintValidator;
		}

		/// <summary>
		/// Validates that the ticket has a valid status to redeem it
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			this.ticketNotExpiredValidator.Validate(item);
			this.ticketNotDiscardedValidator.Validate(item);
			this.ticketNotRedeemedValidator.Validate(item);
			this.ticketCanBeRedeemInPendingPrintValidator.Validate(item);

			if (item.Status != TITO_TICKET_STATUS.VALID
			    && item.Status != TITO_TICKET_STATUS.PENDING_PRINT
			    && item.Status != TITO_TICKET_STATUS.PENDING_CANCEL
			    && item.Status != TITO_TICKET_STATUS.EXPIRED)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.InvalidTicketAction);
			}
		}
	}
}