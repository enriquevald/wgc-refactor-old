﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket can be redeemed if its state is pending print
	/// </summary>
	public interface ITicketCanBeRedeemInPendingPrintValidator : IValidator<TicketDto>
	{
	}
}