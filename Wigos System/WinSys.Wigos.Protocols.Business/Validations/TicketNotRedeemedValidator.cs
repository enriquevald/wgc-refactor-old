﻿using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket is not in redeemed status
	/// </summary>
	public class TicketNotRedeemedValidator : ITicketNotRedeemedValidator
	{
		/// <summary>
		/// Validates that the ticket is not in redeemed status
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			if (item.Status == TITO_TICKET_STATUS.REDEEMED 
			    || item.Status == TITO_TICKET_STATUS.CANCELED)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.TicketHasBeenRedeemed);
			}
		}
	}
}