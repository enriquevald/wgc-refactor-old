﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket is not expired
	/// </summary>
	public interface ITicketNotExpiredValidator : IValidator<TicketDto>
	{
	}
}