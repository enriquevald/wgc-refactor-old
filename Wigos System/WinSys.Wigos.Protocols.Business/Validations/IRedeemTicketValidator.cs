﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket can be redeemed
	/// </summary>
	public interface IRedeemTicketValidator : IValidator<TicketDto>
	{
	}
}