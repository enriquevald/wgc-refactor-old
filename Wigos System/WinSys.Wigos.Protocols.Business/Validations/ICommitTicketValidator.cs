﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket can be commited
	/// </summary>
	public interface ICommitTicketValidator : IValidator<TicketDto>
	{
	}
}