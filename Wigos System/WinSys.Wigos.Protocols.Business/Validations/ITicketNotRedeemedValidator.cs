﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket is not in redeemed status
	/// </summary>
	public interface ITicketNotRedeemedValidator : IValidator<TicketDto>
	{
	}
}