﻿using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket can be redeemed
	/// </summary>
	public class RedeemTicketValidator : IRedeemTicketValidator
	{
		private readonly ITicketIdValidator ticketIdValidator;
		private readonly ITicketHasValidStatusToRedeem ticketHasValidStatusToRedeem;

		public RedeemTicketValidator(
			ITicketIdValidator ticketIdValidator,
			ITicketHasValidStatusToRedeem ticketHasValidStatusToRedeem)
		{
			this.ticketIdValidator = ticketIdValidator;
			this.ticketHasValidStatusToRedeem = ticketHasValidStatusToRedeem;
		}

		/// <summary>
		/// Validates that the ticket can be redeemed
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			// TODO: Refactor. Adapted validations from WSI.Cashier.TITO.BusinessLogic.RedeemTicketList. Try to unify almost validations for redeem

			this.ticketIdValidator.Validate(item);
			this.ticketHasValidStatusToRedeem.Validate(item);
		}
	}
}