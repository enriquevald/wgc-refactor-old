﻿using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket has a valid identification
	/// </summary>
	public class TicketIdValidator : ITicketIdValidator
	{
		/// <summary>
		/// Validates that the ticket has a valid identification
		/// </summary>
		/// <param name="item">Ticket to validate</param>
		/// <remarks>
		/// Exceptions:
		///		* BussinessException is thrown in case of any validation error
		/// </remarks>
		public void Validate(TicketDto item)
		{
			if (item == null 
			    || item.Id == 0 
			    || item.ValidationNumber == 0)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.InvalidTicketIdentifier);
			}
		}
	}
}