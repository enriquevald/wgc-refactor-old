﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket has a valid identification
	/// </summary>
	public interface ITicketIdValidator : IValidator<TicketDto>
	{
	}
}