﻿using WinSys.Wigos.Architecture.Validators;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Validations
{
	/// <summary>
	/// Validates that the ticket has a valid status to redeem it
	/// </summary>
	public interface ITicketHasValidStatusToRedeem : IValidator<TicketDto>
	{
	}
}