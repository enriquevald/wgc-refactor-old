﻿using System;
using System.Runtime.Serialization;
using WinSys.Wigos.Architecture.Exceptions;
using WinSys.Wigos.Protocols.Business.Enums;

namespace WinSys.Wigos.Protocols.Business.Exceptions
{
	/// <summary>
	/// Implements a business exception
	/// </summary>
	public class BusinessException : ContainerWithItemException<BusinessExceptionContainer, Enum>
	{
		public BusinessException()
			: base()
		{
		}

		public BusinessException(
			BusinessExceptionContainer sourceContainer,
			Enum sourceItem)
			: base(sourceContainer, sourceItem)
		{
		}

		public BusinessException(string message)
			: base(message)
		{
		}

		public BusinessException(
			BusinessExceptionContainer sourceContainer,
			Enum sourceItem,
			string message)
			: base(sourceContainer, sourceItem, message)
		{
		}

		public BusinessException(
			string message,
			Exception innerException)
			: base(message, innerException)
		{
		}

		public BusinessException(
			BusinessExceptionContainer sourceContainer,
			Enum sourceItem,
			string message,
			Exception innerException)
			: base(sourceContainer, sourceItem, message, innerException)
		{
		}

		public BusinessException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context)
		{
		}
	}
}