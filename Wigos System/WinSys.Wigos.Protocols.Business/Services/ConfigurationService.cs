﻿using System.Collections.Generic;
using System.Linq;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories;
using WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Services
{
	/// <summary>
	/// Implements ConfigurationService
	/// </summary>
	public class ConfigurationService : IConfigurationService
	{
		private readonly ISequenceRepository sequenceRepository;

		public ConfigurationService(ISequenceRepository sequenceRepository)
		{
			this.sequenceRepository = sequenceRepository;
		}

		/// <summary>
		/// Get the current value of ConfigurationId sequence
		/// </summary>
		/// <returns>Current value</returns>
		public int GetConfigurationIdCurrentSequence()
		{
			return (int)this.sequenceRepository.GetCurrentValue(SequenceId.S2SGsaConfigurationId);
		}

		/// <summary>
		/// Check if ConfigurationId must increment instead general params to save and set it if necesary
		/// </summary>
		/// <param name="generalParamValuesToSave">General param values to check</param>
		/// <remarks>generalParamValuesToSave must have in each item of the list three values: GroupKey, SubjectKey and Value</remarks>
		public void CheckAndSetConfigurationIdIncrement(IEnumerable<GeneralParamItem> generalParamValuesToSave)
		{
			if (Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.IsConfigurationIdAutoIncreaseable)
			{
				if (generalParamValuesToSave.Any(item => item.Value != GeneralParam.GetString(item.GroupKey, item.SubjectKey)))
				{
					this.GetNextConfigurationIdSequence();
				}
			}
		}		
		
        /// <summary>
        /// Get and set the next value secuence for ConfigurationId
        /// </summary>
        /// <returns></returns>
		private int GetNextConfigurationIdSequence()
		{
			return (int)this.sequenceRepository.GetNextValue(SequenceId.S2SGsaConfigurationId);
		}
	}
}