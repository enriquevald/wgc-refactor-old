﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using WinSys.Wigos.Architecture.Services;
using WinSys.Wigos.Protocols.Business.Dtos;
using WinSys.Wigos.Protocols.Business.Enums;
using WinSys.Wigos.Protocols.Business.Exceptions;
using WinSys.Wigos.Protocols.Business.Validations;
using WinSys.Wigos.Protocols.S2S.Gsa.Configuration.GeneralParams;
using WinSys.Wigos.Protocols.S2S.Gsa.Persistence;
using WSI.Common;
using WSI.Common.TITO;
using WSI.WCP;

using Common = WSI.Common;

namespace WinSys.Wigos.Protocols.Business.Services
{
	/// <summary>
	/// Implements  a ticket service
	/// </summary>
	public class TicketService : ITicketService
	{
		private const decimal CentsFactor = 100;

		private Random m_randSeed;
		private readonly object issueActionLock;
		private readonly IRedeemTicketValidator redeemTicketValidator;
		private readonly ICommitTicketValidator commitTicketValidator;
		private readonly IConfigurationService configurationService;
		private readonly ILogService logService;

		public TicketService(
			IRedeemTicketValidator redeemVoucherValidator,
			ICommitTicketValidator commitTicketValidator,
			IConfigurationService configurationService,
			ILogService logService)
		{
			this.redeemTicketValidator = redeemVoucherValidator;
			this.commitTicketValidator = commitTicketValidator;
			this.configurationService = configurationService;
			this.logService = logService;

			this.issueActionLock = new object();
		}

		/// <summary>
		/// Get the validation numbers for a specified clientId
		/// </summary>
		/// <param name="clientId">Client id to get the validation numbers</param>
		/// <returns>Validation numbers generated with its respective info data</returns>
		public ValidationNumbersDto GetValidationNumbers(string clientId)
		{
			List<string> validationIds = new List<string>();
			int numberOfTicketsToGenerate = Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.ValidationIdListSize;

			long seed1 = this.LongRandom();
			long seed2 = this.LongRandom();

			using (DB_TRX dbTrx = new DB_TRX())
			{
				int cashierTerminalId;
				this.GetOrCreateClientIdAsVirtualTerminal(clientId, ClientType.Egm, out cashierTerminalId, dbTrx.SqlTransaction);
				CashierSessionInfo cashierSessionInfo = new CashierSessionInfo
				{
					GamingDay = DateTime.Now,
					TerminalId = cashierTerminalId
				};
				for (int i = 0; i < numberOfTicketsToGenerate; i++)
				{
					long sequence;
					validationIds.Add((Common.TITO.ValidationNumberManager.GetValidationNumberSequence(Tito.TicketsSystemId, Cashier.Common_ComposeValidationNumber(out sequence, dbTrx.SqlTransaction, cashierSessionInfo.TerminalId))).ToString().PadLeft(16, '0'));
				}
				dbTrx.Commit();
			}

			return new ValidationNumbersDto()
			{
				ValidationNumbers = validationIds,
				Seed1 = seed1,
				Seed2 = seed2,
				SeedDateTime = DateTime.UtcNow.AddHours(Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.OffsetMinutesToExpireSeedValues)
			};
		}

		/// <summary>
		/// Get the configuration of the ticket for a especified parameters
		/// </summary>
		/// <param name="getTicketConfiguration">Input parameters to get a especied ticket configuration</param>
		/// <returns>Ticket configuration</returns>
		public TicketConfigurationDto GetTicketConfiguration(GetTicketConfigDto getTicketConfiguration)
		{
			int expireDays = Tito.CashableTicketsExpirationDays;

			return new TicketConfigurationDto
			{
				ClientId = getTicketConfiguration.ClientId,
				ClientType = getTicketConfiguration.ClientType,
				ConfigurationId = this.configurationService.GetConfigurationIdCurrentSequence(),
				ExpireEgm = expireDays,
				ExpireCage = expireDays,
				ExpirePrint = ExpirePrint.NumDays,
				IsRoundUp = false,
				RoundTime = 0,
				Location = Tito.TicketsLocation,
				AddressLine1 = Tito.TicketsAddress1,
				AddressLine2 = Tito.TicketsAddress2,
				TitleCash = Tito.CashableTicketsTitle,
				TitlePromo = Tito.PromotionalTicketsRedeemableTitle,
				TitleNonCash = Tito.PromotionalTicketsNonRedeemableTitle,
				TitleJackpot = Tito.CashableTicketsTitleJackpot
			};
		}

		/// <summary>
		/// Performs the respective issue action of an specified ticket info
		/// </summary>
		/// <param name="issueTicket">Ticket info for issue</param>
		/// <returns>Acknowledge info for the action performed</returns>
		public AckTicketDto IssueTicket(IssueTicketDto issueTicket)
		{
			AckTicketDto result;

			lock (this.issueActionLock)
			{
				using (DB_TRX dbTrx = new DB_TRX())
				{
					CashierSessionInfo csi;
					CashierMovementsTable cmt;
					AccountMovementsTable amt;

					int cashierTerminalId;
					int terminalIdFromClientId = this.GetOrCreateClientIdAsVirtualTerminal(
						issueTicket.ClientId,
						issueTicket.ClientType,
						out cashierTerminalId,
						dbTrx.SqlTransaction);

					long virtualAccount = Accounts.GetOrCreateVirtualAccount(
						terminalIdFromClientId,
						AccountType.ACCOUNT_VIRTUAL_TERMINAL,
						dbTrx.SqlTransaction);

					long playSessionId = 0;
					long moneyCollectionId = 0;
					MultiPromos.InSessionAction action;
					this.PrepareDataToCreateTicket(issueTicket, ref playSessionId, virtualAccount, ref moneyCollectionId, terminalIdFromClientId, dbTrx, out action,
						out csi, out cmt, out amt);

					long operationId;
					Operations.DB_InsertOperation(OperationCode.TRANSFER_CREDIT_OUT, virtualAccount, csi.CashierSessionId, 0, this.ConvertS2SToWigosAmount(issueTicket), 0,
						this.ConvertS2SToWigosAmount(issueTicket), 0, 0, 0, 0, string.Empty, out operationId, dbTrx.SqlTransaction);

					Ticket ticketToIssue = this.GetTicketInstanceToBeIssued(
						issueTicket: issueTicket,
						operationId: operationId,
						playSessionId: playSessionId,
						virtualAccount: virtualAccount,
						moneyCollectionId: moneyCollectionId,
						terminalIdFromClientId: terminalIdFromClientId,
						amountToSet: this.ConvertS2SToWigosAmount(issueTicket));

					if (ticketToIssue.DB_CreateTicket(
						dbTrx.SqlTransaction,
						CardTrackData: virtualAccount.ToString(),
						UpdateAccount: false,
						InitialBalance: 0,
						CashierMovementsTable: cmt,
						AccountMovementsTable: amt))
					{

						if (!amt.Add(0, virtualAccount, MovementType.TITO_TerminalToAccountCredit, 0, 0, ticketToIssue.Amount, ticketToIssue.Amount, string.Empty))
						{
							dbTrx.Rollback();
						}
						else
						{
							//Link added Account Movement to the PlaySession.
							amt.SetPlaySessionId(playSessionId);
						}

						if (ticketToIssue.TicketType.Equals(TITO_TICKET_TYPE.HANDPAY)
							|| ticketToIssue.TicketType.Equals(TITO_TICKET_TYPE.JACKPOT))
						{
							HANDPAY_TYPE handPayType;
							bool doesHandpayExists;
							this.GenerateHandPayForJackpotTicket(
								issueTicket,
								terminalIdFromClientId,
								dbTrx,
								ticketToIssue,
								out handPayType,
								out doesHandpayExists);
						}

						//End Session
						this.IssueTicketEndCardSession(issueTicket, virtualAccount, playSessionId, terminalIdFromClientId, dbTrx);

						dbTrx.Commit();
					}
					else
					{
						dbTrx.Rollback();
						throw new BusinessException(
							BusinessExceptionContainer.Ticket,
							BusinessExceptionTicketItem.InvalidTicketAction);
					}
				}

				result = new AckTicketDto
				{
					ClientId = issueTicket.ClientId,
					TerminalType = issueTicket.ClientType,
					ConfigurationId = this.configurationService.GetConfigurationIdCurrentSequence(),
					TransactionId = issueTicket.TransactionId,
				};
			}

			return result;
		}

		/// <summary>
		/// Performs the redeem action for a specified ticket parameters
		/// </summary>
		/// <param name="redeemTicket">Ticket parameters to perform the redeem action</param>
		/// <returns>Authorize info for the specified ticket</returns>
		public AuthorizeTicketDto ReddemTicket(RedeemTicketDto redeemTicket)
		{
			TicketDto ticketDto = null;
			Ticket ticket;
			int redeemTerminalId;
			using (DB_TRX transaction = new DB_TRX())
			{
				int cashierTerminalId;
				redeemTerminalId = this.GetOrCreateClientIdAsVirtualTerminal(
					redeemTicket.ClientId,
					redeemTicket.ClientType,
					out cashierTerminalId,
					transaction.SqlTransaction);
				ticket = this.GetTicketByValidationNumber(redeemTicket.ValidationNumber, transaction);
			}
			if (ticket != null)
			{
				ticketDto = new TicketDto
							{
								Id = ticket.TicketID,
								ValidationNumber = ticket.ValidationNumber,
								Amount = ticket.Amount,
								ExpirationDate = ticket.ExpirationDateTime,
								Status = ticket.Status,
								Type = ticket.TicketType
							};
			}

			this.redeemTicketValidator.Validate(ticketDto);
			this.UpdateTicketStatusForAuthorizeRedeem(ticket, redeemTerminalId);

			string clientId;
			CardData card;
			this.GetTicketExtraDataFromTicket(ticket, out clientId, out card);
			AuthorizeTicketDto result = new AuthorizeTicketDto
										{
											CageDateTime = ticket.ExpirationDateTime,
											CardId = card.TrackData, // TODO: Confirm.
											CardRestrict = CardRestrict.NoCard, // TODO: REVIEW.
											ClientId = clientId,
											ClientType = redeemTicket.ClientType, // TODO: REVIEW.
											CreditType = this.GetTicketCreditTypeFromTicket(ticket.TicketType),
											CurrencyId = ticket.Cur_0, // TODO: Confirm.
											EgmDateTime = ticket.ExpirationDateTime,
											IsJackpot = ticket.TicketType == TITO_TICKET_TYPE.JACKPOT, // TODO: Ask (1): transform ticketType to creditType(S2S). View GetTicketType()
											PoolId = 0, // TODO: REVIEW.
											TransactionId = redeemTicket.TransactionId, // TODO: REVIEW.
											TicketAmount = this.ConvertWigosToS2SAmount(ticket),
											ValidationNumber = redeemTicket.ValidationNumber,
										};
			return result;
		}

		/// <summary>
		/// Performs the commit action for a specified ticket parameters
		/// </summary>
		/// <param name="commitTicket">Ticket parameters to perform the commit action</param>
		/// <returns>Acknowledge info for the action performed</returns>
		public AckTicketDto CommitTicket(CommitTicketDto commitTicket)
		{
			TicketDto ticketDto = null;
			Ticket ticket;
			int commitTerminalId;
			using (DB_TRX transaction = new DB_TRX())
			{
				int cashierTerminalId;
				commitTerminalId = this.GetOrCreateClientIdAsVirtualTerminal(
					commitTicket.ClientId,
					commitTicket.ClientType,
					out cashierTerminalId,
					transaction.SqlTransaction);
				ticket = this.GetTicketByValidationNumber(commitTicket.ValidationNumber, transaction);
			}
			if (ticket != null)
			{
				ticketDto = new TicketDto
							{
								Id = ticket.TicketID,
								ValidationNumber = ticket.ValidationNumber,
								Amount = ticket.Amount,
								ExpirationDate = ticket.ExpirationDateTime,
								Status = ticket.Status,
								Type = ticket.TicketType
							};
			}

			this.commitTicketValidator.Validate(ticketDto);
			this.UpdateTicketStatusForCommit(ticket, commitTerminalId);

			AckTicketDto result = new AckTicketDto
								  {
									  ClientId = commitTicket.ClientId, // TODO: REVIEW.
									  TerminalType = commitTicket.ClientType, // TODO: REVIEW.
									  ConfigurationId = this.configurationService.GetConfigurationIdCurrentSequence(),
									  TransactionId = commitTicket.TransactionId // TODO: REVIEW.
								  };
			return result;
		}

		private decimal ConvertS2SToWigosAmount(IssueTicketDto ticketOut)
		{
			return ((decimal)ticketOut.TicketAmount) / TicketService.CentsFactor;
		}

		private long ConvertWigosToS2SAmount(Common.Ticket ticket)
		{
			return (long)(ticket.Amount * TicketService.CentsFactor);
		}

		private Ticket GetTicketInstanceToBeIssued(
			IssueTicketDto issueTicket,
			long operationId,
			long playSessionId,
			long virtualAccount,
			long moneyCollectionId,
			int terminalIdFromClientId,
			decimal amountToSet)
		{
			Ticket ticketToIssue = new Ticket
			{
				Address1 = Tito.TicketsAddress1,
				Address2 = Tito.TicketsAddress2,
				Amount = amountToSet,
				Amt_0 = amountToSet,
				Amt_1 = amountToSet,
				CreatedAccountID = virtualAccount,
				CreatedDateTime = issueTicket.TransferDateTime,
				CreatedTerminalId = terminalIdFromClientId,
				CreatedTerminalName = issueTicket.ClientId,
				CreatedTerminalType = (int)this.ClientTypeToWCPTerminalTypeAdapter((ClientType)issueTicket.ClientType),
				CreatedPlaySessionId = playSessionId,
				ExpirationDateTime = this.GetExpirationTicketDateTime(
					issueTicket.TransferDateTime,
					issueTicket.CreditType),
				LastActionDateTime = issueTicket.TransferDateTime,
				MoneyCollectionID = moneyCollectionId,
				Status = this.TicketActionToTitoTicketStatusAdapter((TicketAction)issueTicket.TransferAction),
				TicketType = this.CreditTypeToTitoTicketTypeAdapter(
						issueTicket.CreditType,
						issueTicket.IsJackpot),
				ValidationNumber = Convert.ToInt64(issueTicket.ValidationNumber),
				ValidationType = TITO_VALIDATION_TYPE.SYSTEM,
				Cur_0 = issueTicket.CurrencyId,
				Cur_1 = issueTicket.CurrencyId,
				TransactionId = operationId
			};
			return ticketToIssue;
		}

		private void PrepareDataToCreateTicket(
			IssueTicketDto issueTicket,
			ref long playSessionId,
			long virtualAccount,
			ref long moneyCollectionId,
			int terminalIdFromClientId,
			DB_TRX dbTrx,
			out MultiPromos.InSessionAction action,
			out CashierSessionInfo cashierSessionInfo,
			out CashierMovementsTable cashierMovements,
			out AccountMovementsTable accountMovements)
		{
			cashierSessionInfo = new CashierSessionInfo()
			{
				GamingDay = DateTime.Now,
				UserType = GU_USER_TYPE.SYS_TITO,
				TerminalId = terminalIdFromClientId,
				TerminalName = issueTicket.ClientId
			};

			cashierMovements = new CashierMovementsTable(cashierSessionInfo);
			accountMovements = new AccountMovementsTable(cashierSessionInfo);

			MoneyCollection.GetCashierMoneyCollectionId(
				cashierSessionInfo.CashierSessionId,
				out moneyCollectionId,
				dbTrx.SqlTransaction);
			MultiPromos.AccountBalance accountBalance = this.GetAccountBalanceFitToTicketType(
				this.ConvertS2SToWigosAmount(issueTicket),
				this.CreditTypeToTitoTicketTypeAdapter(
					issueTicket.CreditType,
					issueTicket.IsJackpot));
			MultiPromos.Trx_GetPlaySessionId(
				virtualAccount,
				terminalIdFromClientId,
				TerminalTypes.SAS_HOST,
				true,
				MultiPromos.StartSessionTransferMode.Default,
				accountBalance,
				out playSessionId,
				out action,
				dbTrx.SqlTransaction);
		}

		// TODO: Refactor. Make this function in a separated adapter class or if is necessary remove this call reference from businnes (Perform this at integration layer)
		private CreditType GetTicketCreditTypeFromTicket(TITO_TICKET_TYPE ticketType)
		{
			//TODO: Pending to refactor this method in an isolated class to manage this explicit conversion.
			CreditType creditType;
			switch (ticketType)
			{
				case TITO_TICKET_TYPE.CASHABLE:
					creditType = CreditType.Cashable;
					break;
				case TITO_TICKET_TYPE.PROMO_REDEEM:
					creditType = CreditType.Promo;
					break;
				case TITO_TICKET_TYPE.PROMO_NONREDEEM:
					creditType = CreditType.NonCash;
					break;
				case TITO_TICKET_TYPE.HANDPAY:
					creditType = CreditType.Cashable;
					break;
				default:
					creditType = CreditType.Cashable;
					break;
			}
			return creditType;
		}

		// TODO: Refactor. Make this function in a separated adapter class or if is necessary remove this call reference from businnes (Perform this at integration layer)
		private TITO_TICKET_STATUS TicketActionToTitoTicketStatusAdapter(TicketAction ticketAction)
		{
			TITO_TICKET_STATUS titoTicketStatus;

			switch (ticketAction)
			{
				case TicketAction.Issued:
				case TicketAction.Ejected:		//TODO: Seems not to be a matching ticket status on wigos, we assume default value as VALID.
					titoTicketStatus = TITO_TICKET_STATUS.VALID;
					break;
				case TicketAction.Redeemed:
					titoTicketStatus = TITO_TICKET_STATUS.REDEEMED;
					break;
				default:
					titoTicketStatus = TITO_TICKET_STATUS.VALID;
					break;
			}

			return titoTicketStatus;
		}

		private void IssueTicketEndCardSession(
			IssueTicketDto issueTicket,
			long virtualAccount,
			long playSessionId,
			int terminalIdFromClientId,
			DB_TRX dbTrx)
		{
			MultiPromos.EndSessionInput endSessionInput = new MultiPromos.EndSessionInput
			{
				AccountId = virtualAccount,
				CloseSessionComment = "Session closed in issueVoucher request ending.",
				IsTITO = true,
				PlaySessionId = playSessionId,
				MovementType = MovementType.EndCardSession,
				VirtualAccountId = virtualAccount,
				Meters = new MultiPromos.PlayedWonMeters(),
				FinalBalanceSelectedByUserAfterMismatch = false,
				HasMeters = false,
				IsMico2 = false
			};
			MultiPromos.EndSessionOutput endSessionOutput = new MultiPromos.EndSessionOutput();

			MultiPromos.Trx_OnEndCardSession(
				terminalIdFromClientId,
				TerminalTypes.SAS_HOST,
				issueTicket.ClientId,
				endSessionInput,
				out endSessionOutput,
				dbTrx.SqlTransaction);
		}

		private void GenerateHandPayForJackpotTicket(
			IssueTicketDto issueTicket,
			int terminalIdFromClientId,
			DB_TRX dbTrx,
			Ticket ticketToIssue,
			out HANDPAY_TYPE handpayType,
			out Boolean doesHandpayExists)
		{
			handpayType = ticketToIssue.TicketType.Equals(TITO_TICKET_TYPE.HANDPAY) ? HANDPAY_TYPE.CANCELLED_CREDITS : HANDPAY_TYPE.JACKPOT;

			if (!HandPay.DB_DoesHandPayExists(
					terminalIdFromClientId,
					ticketToIssue.TransactionId,
					handpayType,
					ticketToIssue.Amount,
					out doesHandpayExists,
					dbTrx.SqlTransaction))
			{
				Log.Error("IssueVoucher -->  DB_DoesHandPayExists failed!!!");
				dbTrx.Rollback();
			}

			if (!doesHandpayExists)
			{
				if (!HandPay.DB_InsertNewHandPay(
						ticketToIssue.TicketID,
						terminalIdFromClientId,
						ticketToIssue.TransactionId,
						ticketToIssue.Amt_0,
						handpayType,
						issueTicket.ClientId,
						Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.ProviderName,
						WGDB.Now,
						dbTrx.SqlTransaction))
				{
					Log.Error("IssueVoucher -->  DB_InsertNewHandPay failed!!!");

					dbTrx.Rollback();
				}
			}
			else
			{
				if (!WCP_HPCMeter.DB_UpdateHandpayTicketId(
						terminalIdFromClientId,
						ticketToIssue.TransactionId,
						handpayType,
						ticketToIssue.Amount,
						ticketToIssue.TicketID,
						dbTrx.SqlTransaction))
				{
					Log.Error("IssueVoucher -->  DB_UpdateHandpayTicketID failed!!!");
				}
			}
		}

		private MultiPromos.AccountBalance GetAccountBalanceFitToTicketType(
			decimal voucherAmount,
			TITO_TICKET_TYPE ticketType)
		{
			MultiPromos.AccountBalance accountBalanceToReturn = new MultiPromos.AccountBalance();

			if (ticketType.Equals(TITO_TICKET_TYPE.CASHABLE) || ticketType.Equals(TITO_TICKET_TYPE.JACKPOT)) accountBalanceToReturn.Redeemable = voucherAmount;
			if (ticketType.Equals(TITO_TICKET_TYPE.PROMO_NONREDEEM)) accountBalanceToReturn.PromoNotRedeemable = voucherAmount;
			if (ticketType.Equals(TITO_TICKET_TYPE.PROMO_REDEEM)) accountBalanceToReturn.PromoRedeemable = voucherAmount;

			return accountBalanceToReturn;
		}

		private int GetOrCreateClientIdAsVirtualTerminal(string clientId, Enums.ClientType clientType, out int cashierTerminalId, System.Data.SqlClient.SqlTransaction sqlTransaction)
		{
			int terminalId;
			Common.Terminal.TerminalInfo terminalInfo;

			if (!WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(clientId, out terminalInfo, sqlTransaction))
			{
				//Provider Validation.
				string providerName = Protocols.S2S.Gsa.Configuration.GeneralParams.Gsa.ProviderName;
				int providerId;
				if (!this.GetProviderId(providerName, out providerId))
				{
					Providers.DB_InsertProvider(providerName, sqlTransaction);
				}
				//Terminal Validation.
				WCP_TerminalType terminalType = this.ClientTypeToWCPTerminalTypeAdapter(clientType);
				string terminalName;
				TerminalStatus terminalStatus;
				string serverExternalId;
				Currency sasAccountingDenomCents;
				WCP_BusinessLogic.DB_InsertTerminal(clientId, clientId, terminalType, providerName, TerminalTypes.SAS_HOST, sqlTransaction);
				Misc.GetTerminalInfo(clientId, (int)terminalType, out terminalId, out providerName, out terminalName, out terminalStatus, out serverExternalId, out sasAccountingDenomCents, sqlTransaction);
			}
			else
			{
				terminalId = terminalInfo.TerminalId;
			}
			cashierTerminalId = Cashier.ReadTerminalId(sqlTransaction, clientId, WSI.Common.GU_USER_TYPE.SYS_TITO, false);

			return terminalId;
		}

		private Boolean GetProviderId(string providerName, out int providerId)
		{
			Boolean isProviderInDB = false;
			using (DB_TRX dbTransaction = new DB_TRX())
			{
				isProviderInDB = WSI.Common.Groups.GetProviderId(providerName, out providerId, dbTransaction.SqlTransaction);
			}
			return isProviderInDB;
		}

		// TODO: Refactor. Make this function in a separated adapter class or if is necessary remove this call reference from businnes (Perform this at integration layer)
		private WCP_TerminalType ClientTypeToWCPTerminalTypeAdapter(ClientType clientType)
		{
			WCP_TerminalType terminalType;
			switch (clientType)
			{
				default:
					terminalType = WCP_TerminalType.GamingTerminal;
					break;
			}
			return terminalType;
		}

		DateTime GetExpirationTicketDateTime(DateTime CreationDateTime, CreditType creditType)
		{
			DateTime expirationDateTime;
			switch (creditType)
			{
				case CreditType.Cashable:
					expirationDateTime = CreationDateTime.AddDays(Tito.CashableTicketsExpirationDays);
					break;
				case CreditType.Promo:
					expirationDateTime = CreationDateTime.AddDays(Tito.PromotionalTicketsRedeemableExpirationDays);
					break;
				case CreditType.NonCash:
					expirationDateTime = CreationDateTime.AddDays(Tito.PromotionalTicketsNonRedeemableExpirationDays);
					break;
				default:
					expirationDateTime = CreationDateTime.AddDays(Tito.CashableTicketsExpirationDays);
					break;
			}
			return expirationDateTime;
		}

		// TODO: Refactor. Make this function in a separated adapter class or if is necessary remove this call reference from businnes (Perform this at integration layer)
		private TITO_TICKET_TYPE CreditTypeToTitoTicketTypeAdapter(CreditType creditType, bool isJackpot)
		{
			TITO_TICKET_TYPE ticketType;
			if (!isJackpot)
			{
				switch (creditType)
				{
					case CreditType.Cashable:
						ticketType = TITO_TICKET_TYPE.CASHABLE;
						break;
					case CreditType.Promo:
						ticketType = TITO_TICKET_TYPE.PROMO_REDEEM;
						break;
					case CreditType.NonCash:
						ticketType = TITO_TICKET_TYPE.PROMO_NONREDEEM;
						break;
					default:
						ticketType = TITO_TICKET_TYPE.CASHABLE;
						break;
				}
			}
			else
			{
				ticketType = TITO_TICKET_TYPE.JACKPOT;
			}
			return ticketType;
		}
		private Random randSeed
		{
			get
			{
				if (this.m_randSeed == null)
				{
					this.m_randSeed = new Random();
				}
				return this.m_randSeed;
			}
		}

		private long LongRandom()
		{
			long min = 1;
			long max = 9999999999;
			long result = 0;
			try
			{
				result = (this.randSeed.Next((Int32)(min >> 32), (Int32)(max >> 32)) << 32) | (long)this.randSeed.Next((Int32)min, (Int32)max);
			}
			catch (Exception ex)
			{
				WSI.Common.Log.Exception(ex);
			}
			return result;
		}

		// TODO: Review. Check if is necesary use this function.
		private string GetValidationNumberCheckSummed(long seedValue1, long seedValue2, string validationNumber)
		{
			Int32 modValueChecksummed = 0;
			byte checkSumByte = 0x00;
			byte[] byteArrayValidationNumber = new byte[16];
			try
			{
				byte[] byteArrayBuffer = new byte[32];

				System.Buffer.BlockCopy(BitConverter.GetBytes(seedValue1), 0, byteArrayBuffer, 0, 8);
				System.Buffer.BlockCopy(BitConverter.GetBytes(seedValue2), 0, byteArrayBuffer, 8, 8);

				for (int i = 0; i < validationNumber.Length; i++)
				{
					byteArrayValidationNumber[i] = BitConverter.GetBytes(validationNumber[i])[0];
				}

				System.Buffer.BlockCopy(byteArrayValidationNumber, 0, byteArrayBuffer, 16, 16);

				for (int i = 0; i < 32; i++)
				{
					checkSumByte ^= byteArrayBuffer[i];
				}
				modValueChecksummed = this.ModCheckSumByteValue(checkSumByte);
			}
			catch (Exception ex)
			{
				WSI.Common.Log.Exception(ex);
			}
			return string.Format("{0}{1}", modValueChecksummed.ToString().PadLeft(2, '0'), validationNumber);
		}

		private Int32 ModCheckSumByteValue(byte checkSumByte)
		{
			Int32 returnValue = 0;
			try
			{
				returnValue = Convert.ToInt32(checkSumByte.ToString()) % 100;
			}
			catch (Exception ex)
			{
				this.logService.Exception(ex);
			}
			return returnValue;
		}

		/// <summary>
		/// Gets a ticket dto data list from data base by its validation number instead ticketId
		/// </summary>
		/// <param name="validationNumber">Validation number to filter</param>
		/// <param name="transaction">Sql transaction</param>
		/// <returns>Ticket list with coincidences in case of having been found, else returns an empty list</returns>
		private IEnumerable<TicketDto> GetTicketsByValidationNumber(string validationNumber, DB_TRX transaction)
		{
			const int TicketIdColumnIndex = 0;
			const int AmountColumnIndex = 2;
			const int ExpirationDateColumnIndex = 10;
			const int StatusColumnIndex = 3;
			const int TypeColumnIndex = 4;
			const int ValidationNumberColumnIndex = 1;

			string ticketCondition = string.Format("TI_VALIDATION_NUMBER = {0} ", validationNumber);
			DataTable ticketData = null;
			Ticket.LoadTicketsByCondition(ticketCondition, ref ticketData, transaction.SqlTransaction);

			List<TicketDto> result = new List<TicketDto>();
			foreach (DataRow row in ticketData.Rows)
			{
				result.Add(
					new TicketDto
					{
						Id = (long)row.ItemArray[TicketIdColumnIndex],
						Amount = (decimal)row.ItemArray[AmountColumnIndex],
						ExpirationDate = (DateTime)row.ItemArray[ExpirationDateColumnIndex],
						Status = (TITO_TICKET_STATUS)row.ItemArray[StatusColumnIndex],
						Type = (TITO_TICKET_TYPE)row.ItemArray[TypeColumnIndex],
						ValidationNumber = (long)row.ItemArray[ValidationNumberColumnIndex]
					});
			}

			return result;
		}

		/// <summary>
		/// Gets a ticket data from data base by its validation number instead ticketId
		/// </summary>
		/// <param name="validationNumber">Validation number to filter</param>
		/// <param name="transaction">Sql transaction</param>
		/// <returns>Ticket data in case of having been found</returns>
		/// <remarks>Thrown a invalid ticket action in case of can not get one specific ticket</remarks>
		private Ticket GetTicketByValidationNumber(string validationNumber, DB_TRX transaction)
		{
			Ticket resultTicket = null;
			IEnumerable<TicketDto> tickets = this.GetTicketsByValidationNumber(validationNumber, transaction);

			int ticketsCount = tickets.Count();
			if (ticketsCount == 1)
			{
				resultTicket = Ticket.LoadTicket(tickets.ElementAt(0).Id, transaction.SqlTransaction, false);
			}
			else if (ticketsCount > 1)
			{
				throw new BusinessException(
					BusinessExceptionContainer.Ticket,
					BusinessExceptionTicketItem.InvalidTicketAction);
			}

			return resultTicket;
		}

		/// <summary>
		/// Update the specified ticket with correct status for redeem
		/// </summary>
		/// <param name="ticket">Ticket to update</param>
		/// <param name="redeemTerminalId">Terminal Id of the redeem terminal</param>
		/// <remarks>This method update the ticket status, only if is different from the current one</remarks>
		private void UpdateTicketStatusForAuthorizeRedeem(Ticket ticket, int redeemTerminalId)
		{
			const string SetTicketPendingCancelCommand = "TITO_SetTicketPendingCancel";

			bool isTicketUpdated = false;

			using (DB_TRX transaction = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand(SetTicketPendingCancelCommand, transaction.SqlTransaction.Connection, transaction.SqlTransaction))
			{
				try
				{
					sqlCommand.CommandType = CommandType.StoredProcedure;
					sqlCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = redeemTerminalId;
					sqlCommand.Parameters.Add("@pTicketValidationNumber", SqlDbType.BigInt).Value = ticket.ValidationNumber;
					sqlCommand.Parameters.Add("@pDefaultAllowRedemption", SqlDbType.Bit).Value = true;
					sqlCommand.Parameters.Add("@pDefaultMaxAllowedTicketIn", SqlDbType.Money).Value = ticket.Amount;
					sqlCommand.Parameters.Add("@pDefaultMinAllowedTicketIn", SqlDbType.Money).Value = ticket.Amount;
					sqlCommand.Parameters.Add("@pTicketRejectReasonEgm", SqlDbType.BigInt).Value = DBNull.Value;
					sqlCommand.Parameters.Add("@pTicketRejectReasonWcp", SqlDbType.BigInt).Value = DBNull.Value;

					SqlParameter ticketId = sqlCommand.Parameters.Add("@pTicketId", SqlDbType.BigInt);
					ticketId.Direction = ParameterDirection.Output;
					sqlCommand.Parameters.Add("@pTicketType", SqlDbType.Int).Direction = ParameterDirection.Output;
					sqlCommand.Parameters.Add("@pTicketAmount", SqlDbType.Money).Direction = ParameterDirection.Output;
					sqlCommand.Parameters.Add("@pTicketExpiration", SqlDbType.DateTime).Direction = ParameterDirection.Output;
					sqlCommand.ExecuteNonQuery();

					if (ticketId.Value != null)
					{
						isTicketUpdated = true;
						transaction.Commit();
					}
					else
					{
						transaction.Rollback();
					}
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					throw new BusinessException(BusinessExceptionContainer.Message, BusinessExceptionMessageItem.MessageProcessingServicesUnavailable, ex.Message, ex);
				}

				if (!isTicketUpdated)
				{
					throw new BusinessException(BusinessExceptionContainer.Ticket, BusinessExceptionTicketItem.InvalidTicketAction);
				}
			}
		}

		/// <summary>
		/// Update the specified ticket with correct status for commit
		/// </summary>
		/// <param name="ticket">Ticket to update</param>
		/// <param name="commitTerminalId">Terminal Id of the commit terminal</param>
		private void UpdateTicketStatusForCommit(Ticket ticket, int commitTerminalId)
		{
			const string SetTicketCanceledCommand = "TITO_SetTicketCanceled";

			bool isTicketUpdated = false;

			using (DB_TRX transaction = new DB_TRX())
			using (SqlCommand sqlCommand = new SqlCommand(SetTicketCanceledCommand, transaction.SqlTransaction.Connection, transaction.SqlTransaction))
			{
				try
				{
					sqlCommand.CommandType = CommandType.StoredProcedure;
					sqlCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = commitTerminalId;
					sqlCommand.Parameters.Add("@pTicketValidationNumber", SqlDbType.BigInt).Value = ticket.ValidationNumber;
					sqlCommand.Parameters.Add("@MsgSequenceId", SqlDbType.BigInt).Value = DBNull.Value;
					sqlCommand.Parameters.Add("@pTicketRejectReasonEgm", SqlDbType.BigInt).Value = DBNull.Value;
					sqlCommand.Parameters.Add("@pTicketRejectReasonWcp", SqlDbType.BigInt).Value = DBNull.Value;

					SqlParameter ticketId = sqlCommand.Parameters.Add("@pTicketId", SqlDbType.BigInt);
					ticketId.Direction = ParameterDirection.Output;
					sqlCommand.Parameters.Add("@pTicketAmount", SqlDbType.Money).Direction = ParameterDirection.Output;
					sqlCommand.ExecuteNonQuery();

					if (ticketId.Value != null)
					{
						isTicketUpdated = true;
						transaction.Commit();
					}
					else
					{
						transaction.Rollback();
					}
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					throw new BusinessException(BusinessExceptionContainer.Message, BusinessExceptionMessageItem.MessageProcessingServicesUnavailable, ex.Message, ex);
				}

				if (!isTicketUpdated)
				{
					throw new BusinessException(BusinessExceptionContainer.Ticket, BusinessExceptionTicketItem.InvalidTicketAction);
				}
			}
		}

		/// <summary>
		/// Gets the extra data needed from the ticket
		/// </summary>
		/// <param name="ticket">Source ticket to get extra data</param>
		/// <param name="terminal">Out parameters filled with terminal data</param>
		/// <param name="card">Out parameters filled with card data</param>
		/// <remarks>Terminal external Id is parsed to normalize as S2S expected with a specific</remarks>>
		private void GetTicketExtraDataFromTicket(
			Ticket ticket,
			out string clientId,
			out CardData card)
		{
			const string defaultPrefix = "MAC_";
			const string defaultId = "MAC_0000-0000-0000";

			card = new CardData();
			clientId = null;

			using (DB_TRX transaction = new DB_TRX())
			{
				Common.Terminal.TerminalInfo terminal;

				if (ticket.CreatedTerminalType == (int)TITO_TERMINAL_TYPE.TERMINAL)
				{
					Common.Terminal.Trx_GetTerminalInfo(
						ticket.CreatedTerminalId,
						out terminal,
						transaction.SqlTransaction);
					clientId = terminal.ExternalId;
				}
				else if ((ticket.CreatedTerminalType == (int)TITO_TERMINAL_TYPE.CASHIER))
				{
					clientId = this.GetCashierTerminalName(transaction.SqlTransaction, ticket.CreatedTerminalId);
					if (clientId != null)
					{
						clientId = defaultPrefix + clientId.Replace(" ", string.Empty);
					}
				}
				if (clientId == null)
				{
					clientId = defaultId;
				}

				Common.CardData.DB_CardGetAllData(
					ticket.CreatedAccountID,
					card,
					GetAllData: true);
			}
		}

		private string GetCashierTerminalName(SqlTransaction sqlTransaction, long cashierTerminalId)
		{
			const string getCashierTerminalNameQuery = "SELECT [ct_name] FROM [cashier_terminals] WHERE [ct_cashier_id] = @pId";

			string result = null;

			using (SqlCommand sqlCommand = new SqlCommand(getCashierTerminalNameQuery, sqlTransaction.Connection, sqlTransaction))
			{
				sqlCommand.Parameters.Add("@pId", SqlDbType.BigInt).Value = cashierTerminalId;

				object queryResult = sqlCommand.ExecuteScalar();
				if (queryResult != DBNull.Value)
				{
					result = (string)queryResult;
				}
			}

			return result;
		}
	}
}