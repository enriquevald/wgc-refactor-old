﻿using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Services
{
	/// <summary>
	/// Implements  a ticket service
	/// </summary>
	public interface ITicketService
	{
		/// <summary>
		/// Get the validation numbers for a specified clientId
		/// </summary>
		/// <param name="clientId">Client id to get the validation numbers</param>
		/// <returns>Validation numbers generated with its respective info data</returns>
		ValidationNumbersDto GetValidationNumbers(string clientId);

		/// <summary>
		/// Get the configuration of the ticket for a especified parameters
		/// </summary>
		/// <param name="getTicketConfiguration">Input parameters to get a especied ticket configuration</param>
		/// <returns>Ticket configuration</returns>
		TicketConfigurationDto GetTicketConfiguration(GetTicketConfigDto getTicketConfiguration);

		/// <summary>
		/// Performs the respective issue action of an specified ticket info
		/// </summary>
		/// <param name="issueTicket">Ticket info for issue</param>
		/// <returns>Acknowledge info for the action performed</returns>
		AckTicketDto IssueTicket(IssueTicketDto issueTicket);

		/// <summary>
		/// Performs the redeem action for a specified ticket parameters
		/// </summary>
		/// <param name="redeemTicket">Ticket parameters to perform the redeem action</param>
		/// <returns>Authorize info for the specified ticket</returns>
		AuthorizeTicketDto ReddemTicket(RedeemTicketDto redeemTicket);

		/// <summary>
		/// Performs the commit action for a specified ticket parameters
		/// </summary>
		/// <param name="commitTicket">Ticket parameters to perform the commit action</param>
		/// <returns>Acknowledge info for the action performed</returns>
		AckTicketDto CommitTicket(CommitTicketDto commitTicket);
	}
}