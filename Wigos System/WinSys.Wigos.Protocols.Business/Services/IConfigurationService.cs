﻿using System.Collections.Generic;
using WinSys.Wigos.Protocols.Business.Dtos;

namespace WinSys.Wigos.Protocols.Business.Services
{
	/// <summary>
	/// Implements ConfigurationService
	/// </summary>
	public interface IConfigurationService
	{
		/// <summary>
		/// Get the current value of ConfigurationId sequence
		/// </summary>
		/// <returns>Current value</returns>
		int GetConfigurationIdCurrentSequence();

		/// <summary>
		/// Check if ConfigurationId must increment instead general params to save and set it if necesary
		/// </summary>
		/// <param name="generalParamValuesToSave">General param values to check</param>
		/// <remarks>generalParamValuesToSave must have in each item of the list three values: GroupKey, SubjectKey and Value</remarks>
		void CheckAndSetConfigurationIdIncrement(IEnumerable<GeneralParamItem> generalParamValuesToSave);
	}
}