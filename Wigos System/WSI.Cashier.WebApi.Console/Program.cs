﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Cashier.WebApi.Client;

namespace WSI.Cashier.WebApi.ConsoleClient
{
  class Program
  {
    static void Main(string[] args)
    {
      MovementServices service = new MovementServices();
      var movements = service.GetMovements();
      
      
      foreach(var item in movements)
      {
        Console.WriteLine(item.Id + " - " + item.MovementDate);
      }
      service.Dispose();
    }
  }
}
