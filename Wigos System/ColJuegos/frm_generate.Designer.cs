namespace Coljuegos
{
  partial class frm_generate
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_generate = new System.Windows.Forms.Button();
      this.lbl_result = new System.Windows.Forms.Label();
      this.dt_date = new System.Windows.Forms.DateTimePicker();
      this.btn_exit = new System.Windows.Forms.Button();
      this.lnk_file_name = new System.Windows.Forms.LinkLabel();
      this.lnk_folder_name = new System.Windows.Forms.LinkLabel();
      this.lbl_folder_name = new System.Windows.Forms.Label();
      this.lbl_file_name = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // btn_generate
      // 
      this.btn_generate.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_generate.Location = new System.Drawing.Point(343, 86);
      this.btn_generate.Name = "btn_generate";
      this.btn_generate.Size = new System.Drawing.Size(112, 41);
      this.btn_generate.TabIndex = 1;
      this.btn_generate.Text = "Generar";
      this.btn_generate.UseVisualStyleBackColor = true;
      this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
      // 
      // lbl_result
      // 
      this.lbl_result.AutoSize = true;
      this.lbl_result.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_result.Location = new System.Drawing.Point(28, 68);
      this.lbl_result.Name = "lbl_result";
      this.lbl_result.Size = new System.Drawing.Size(73, 22);
      this.lbl_result.TabIndex = 5;
      this.lbl_result.Text = "xResult";
      // 
      // dt_date
      // 
      this.dt_date.CustomFormat = "ddMMMM yyyy";
      this.dt_date.Font = new System.Drawing.Font("Arial", 14.25F);
      this.dt_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.dt_date.Location = new System.Drawing.Point(32, 21);
      this.dt_date.Name = "dt_date";
      this.dt_date.Size = new System.Drawing.Size(218, 29);
      this.dt_date.TabIndex = 0;
      this.dt_date.ValueChanged += new System.EventHandler(this.dt_date_ValueChanged);
      // 
      // btn_exit
      // 
      this.btn_exit.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_exit.Location = new System.Drawing.Point(343, 133);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.Size = new System.Drawing.Size(112, 41);
      this.btn_exit.TabIndex = 2;
      this.btn_exit.Text = "Salir";
      this.btn_exit.UseVisualStyleBackColor = true;
      this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // lnk_file_name
      // 
      this.lnk_file_name.AutoEllipsis = true;
      this.lnk_file_name.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lnk_file_name.Location = new System.Drawing.Point(112, 140);
      this.lnk_file_name.Name = "lnk_file_name";
      this.lnk_file_name.Size = new System.Drawing.Size(225, 22);
      this.lnk_file_name.TabIndex = 4;
      this.lnk_file_name.TabStop = true;
      this.lnk_file_name.Text = "xFileName";
      this.lnk_file_name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnk_file_name_LinkClicked);
      // 
      // lnk_folder_name
      // 
      this.lnk_folder_name.AutoEllipsis = true;
      this.lnk_folder_name.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lnk_folder_name.Location = new System.Drawing.Point(112, 105);
      this.lnk_folder_name.Name = "lnk_folder_name";
      this.lnk_folder_name.Size = new System.Drawing.Size(225, 22);
      this.lnk_folder_name.TabIndex = 3;
      this.lnk_folder_name.TabStop = true;
      this.lnk_folder_name.Text = "xFolderName";
      this.lnk_folder_name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnk_folder_name_LinkClicked);
      // 
      // lbl_folder_name
      // 
      this.lbl_folder_name.AutoSize = true;
      this.lbl_folder_name.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_folder_name.Location = new System.Drawing.Point(28, 105);
      this.lbl_folder_name.Name = "lbl_folder_name";
      this.lbl_folder_name.Size = new System.Drawing.Size(82, 22);
      this.lbl_folder_name.TabIndex = 6;
      this.lbl_folder_name.Text = "Carpeta:";
      // 
      // lbl_file_name
      // 
      this.lbl_file_name.AutoSize = true;
      this.lbl_file_name.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_file_name.Location = new System.Drawing.Point(28, 140);
      this.lbl_file_name.Name = "lbl_file_name";
      this.lbl_file_name.Size = new System.Drawing.Size(78, 22);
      this.lbl_file_name.TabIndex = 7;
      this.lbl_file_name.Text = "Archivo:";
      // 
      // frm_generate
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(467, 186);
      this.Controls.Add(this.lbl_file_name);
      this.Controls.Add(this.lbl_folder_name);
      this.Controls.Add(this.lnk_folder_name);
      this.Controls.Add(this.lnk_file_name);
      this.Controls.Add(this.btn_exit);
      this.Controls.Add(this.dt_date);
      this.Controls.Add(this.lbl_result);
      this.Controls.Add(this.btn_generate);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "frm_generate";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "xGenerate";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_generate_FormClosed);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_generate;
    private System.Windows.Forms.Label lbl_result;
    private System.Windows.Forms.DateTimePicker dt_date;
    private System.Windows.Forms.Button btn_exit;
    internal System.Windows.Forms.LinkLabel lnk_file_name;
    internal System.Windows.Forms.LinkLabel lnk_folder_name;
    private System.Windows.Forms.Label lbl_folder_name;
    private System.Windows.Forms.Label lbl_file_name;
  }
}

