//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_generate.cs
// 
//   DESCRIPTION : Generate daily report Coljuegos
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2015 AMF    First release
// 29-JAN-2016 AMF    PBI 8820: Separator
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

namespace Coljuegos
{

  public partial class frm_generate : Form
  {
    #region " Constants "

    private const String DAILY_REPORT_FORMAT = "F18";
    private const String CONTROL_REGISTER_INITIAL_OPERATOR = "RI";
    private const String CONTROL_REGISTER_INITIAL_LOCAL = "RC";
    private const String CONTROL_REGISTER_DETAIL = "RD";
    private const String CONTROL_REGISTER_FINAL_LOCAL = "RE";
    private const String CONTROL_REGISTER_FINAL_OPERATOR = "RF";

    #endregion " Constants "

    #region " Members "

    protected String m_service_name = "Unknown";
    protected String m_version = "CC.BBB";
    protected String m_file_path = "";
    protected String m_separator = ";";

    #endregion " Members "

    //------------------------------------------------------------------------------
    // PURPOSE : Main
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public frm_generate()
    {
      frm_login _frm;

      _frm = new frm_login();
      _frm.ShowDialog();

      InitializeComponent();
      this.Text = "Reporte diario - Generador - @" + WGDB.DataSource.ToString();
      VisibleResult(false);
      this.dt_date.Value = WGDB.Now;
    } // frm_generate

    //------------------------------------------------------------------------------
    // PURPOSE : Reset labels result
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void VisibleResult(Boolean VisibleValue)
    {
      lbl_result.Visible = VisibleValue;
      lbl_file_name.Visible = VisibleValue;
      lnk_file_name.Visible = VisibleValue;
      lbl_folder_name.Visible = VisibleValue;
      lnk_folder_name.Visible = VisibleValue;
    } // ResetResult

    //------------------------------------------------------------------------------
    // PURPOSE : Create conecction
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void CreateConnection()
    {
      StringBuilder _xml;

      _xml = new StringBuilder();
      _xml.AppendLine("<SiteConfig>");
      _xml.AppendLine("    <DBPrincipal>WIN-SERVER-4</DBPrincipal>");
      _xml.AppendLine("    <DBMirror>WIN-SERVER-4</DBMirror>");
      _xml.AppendLine("    <DBId>0</DBId>");
      _xml.AppendLine("</SiteConfig>");

      if (!ConfigurationFile.Init("Coljuegos.cfg", _xml.ToString()))
      {
        Log.Error(" Reading application configuration settings. Application stopped");

        return;
      }

      // Connect to DB
      WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
      WGDB.SetApplication(m_service_name, m_version);
      WGDB.ConnectAs("WGROOT");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");

        Thread.Sleep(5000);
      }
    } // CreateConnection

    //------------------------------------------------------------------------------
    // PURPOSE : execute global procedure
    //
    //  PARAMS :
    //      - INPUT :
    //                - String Sql
    //                - DataTable Dt
    //
    //      - OUTPUT :
    //
    // RETURNS : True or False
    //
    //   NOTES :
    //
    private Boolean ExecuteProcedure(String Sql, out DataTable Dt)
    {
      Dt = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(Sql, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, Dt);

              return true;
            }
          }
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
        lbl_result.Text = "ERROR: Obteniendo datos";
      }

      return false;
    } // ExecuteProcedure

    //------------------------------------------------------------------------------
    // PURPOSE : Generate CSV(;) with the data extract
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void GenerateCSV()
    {
      String _report_time;
      DateTime _report_date;
      String _sql_str;
      DataTable _dt;
      Int32 _num_columns = 0;
      Int32 _total_registers_detail = 0;
      Decimal _decimal_detail;
      String _type;
      StringBuilder sb = new StringBuilder();
      DirectoryInfo _info;

      try
      {
        CreateConnection();

        _report_time = GeneralParam.GetString("Interface.Coljuegos", "ReportTime", "00:00");
        _report_date = new DateTime(dt_date.Value.Year, dt_date.Value.Month, dt_date.Value.Day, int.Parse(_report_time.Substring(0, 2)), int.Parse(_report_time.Substring(3, 2)), 0);
        _sql_str = "EXECUTE SP_Coljuegos_ReporteDiario '" + _report_date.ToString("yyyy-MM-dd HH:mm:ss") + "'";

        if (!ExecuteProcedure(_sql_str, out _dt))
        {
          return;
        }

        foreach (DataRow _dr in _dt.Rows)
        {
          _num_columns = (Int32)_dr[0];
          _type = _dr[1].ToString();
          if (_type == CONTROL_REGISTER_DETAIL)
          {
            _total_registers_detail++;
          }

          for (int i = 1; i <= _num_columns; i++)
          {
            if (_type == CONTROL_REGISTER_DETAIL)
            {
              if ((i > 4) && (i < 12) && (i != 10))
              {
                _decimal_detail = (Decimal)_dr[i];
                sb.Append(Decimal.Round(_decimal_detail, 0));
              }
              else
              {
                sb.Append(_dr[i].ToString());
              }
            }
            else
            {
              sb.Append(_dr[i].ToString());
            }

            sb.Append(m_separator);
          }

          if (_type == CONTROL_REGISTER_INITIAL_OPERATOR)
          {
            sb.Append(DAILY_REPORT_FORMAT);
            sb.Append(m_separator);
          }
          else if (_type == CONTROL_REGISTER_FINAL_LOCAL)
          {
            sb.Append(_total_registers_detail.ToString());
            sb.Append(m_separator);
          }
          else if (_type == CONTROL_REGISTER_FINAL_OPERATOR)
          {
            sb.Append(DAILY_REPORT_FORMAT);
            sb.Append(m_separator);
            sb.Append(_total_registers_detail.ToString());
            sb.Append(m_separator);
          }

          sb.AppendLine();
        }

        using (StreamWriter outfile = new StreamWriter(m_file_path))
        {
          outfile.Write(sb.ToString());
        }

        VisibleResult(true);
        lbl_result.Text = String.Empty;
        _info = new DirectoryInfo(Path.GetDirectoryName(m_file_path));
        lnk_folder_name.Text = _info.Name.ToString();
        lnk_folder_name.Links[0].LinkData = Path.GetDirectoryName(m_file_path);
        lnk_file_name.Text = Path.GetFileName(m_file_path);
        lnk_file_name.Links[0].LinkData = m_file_path;

      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
        lbl_result.Text = "ERROR: Generando reporte diario";
        lbl_result.Visible = true;
      }
    } // GenerateCSV

    //------------------------------------------------------------------------------
    // PURPOSE : Generate Click
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_generate_Click(object sender, EventArgs e)
    {
      Stream myStream;
      SaveFileDialog sfd_generate = new SaveFileDialog();
      String _nit;
      String _contract;
      try
      {
        _nit = GeneralParam.GetString("Interface.Coljuegos", "Operador.NIT");
        _contract = GeneralParam.GetString("Interface.Coljuegos", "Operador.Contrato.Numero");
        //sfd_generate.Filter = "Excel Files (*.xlsx)|*.xlsx";
        sfd_generate.FilterIndex = 2;
        sfd_generate.RestoreDirectory = true;
        sfd_generate.FileName = String.Format("F18-{0}{1}-{2}", _nit, _contract, dt_date.Value.ToString("yyyyMMdd"));

        if (sfd_generate.ShowDialog() == DialogResult.OK)
        {
          if ((myStream = sfd_generate.OpenFile()) != null)
          {
            VisibleResult(false);
            m_file_path = sfd_generate.FileName;
            myStream.Close();
            lbl_result.Text = "Generando...";
            lbl_result.Visible = true;
            this.Cursor = Cursors.WaitCursor;
            GenerateCSV();
            this.Cursor = Cursors.Arrow;
          }
        }
      }
      catch
      {
        lbl_result.Text = "ERROR: Archivo en uso";
        lbl_result.Visible = true;
      }
    } // btn_generate_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Exit
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void btn_exit_Click(object sender, EventArgs e)
    {
      Environment.Exit(0);
    } // btn_exit_Click

    //------------------------------------------------------------------------------
    // PURPOSE : Exit
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    // // btn_exit_Click

    //------------------------------------------------------------------------------
    // PURPOSE : date value changed event
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - EventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void dt_date_ValueChanged(object sender, EventArgs e)
    {
      VisibleResult(false);
    } // dt_date_ValueChanged

    //------------------------------------------------------------------------------
    // PURPOSE : On form closed
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - FormClosedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void frm_generate_FormClosed(object sender, FormClosedEventArgs e)
    {
      Environment.Exit(0);
    } // frm_generate_FormClosed

    //------------------------------------------------------------------------------
    // PURPOSE : File name link
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - LinkLabelLinkClickedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void lnk_file_name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      String _target;

      try
      {
        _target = e.Link.LinkData.ToString();

        System.Diagnostics.Process.Start(_target);
      }
      catch
      {
      }
    } // lnk_file_name_LinkClicked

    //------------------------------------------------------------------------------
    // PURPOSE : Folder name link
    //
    //  PARAMS :
    //      - INPUT :
    //                - object sender
    //                - LinkLabelLinkClickedEventArgs e
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void lnk_folder_name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      String _target;

      try
      {
        _target = e.Link.LinkData.ToString();

        System.Diagnostics.Process.Start(_target);
      }
      catch
      {
      }
    } // lnk_folder_name_LinkClicked

  }
}