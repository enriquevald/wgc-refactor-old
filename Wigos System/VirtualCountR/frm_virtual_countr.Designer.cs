﻿namespace VirtualCountR
{
  partial class frm_virtual_countr
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cb_countr = new System.Windows.Forms.ComboBox();
      this.lbl_countr = new System.Windows.Forms.Label();
      this.btn_reset = new System.Windows.Forms.Button();
      this.btn_exit = new System.Windows.Forms.Button();
      this.gb_ticket_pay = new System.Windows.Forms.GroupBox();
      this.btn_cancel = new System.Windows.Forms.Button();
      this.txt_ticket_pay_info = new System.Windows.Forms.TextBox();
      this.lbl_ticket_pay_info = new System.Windows.Forms.Label();
      this.btn_validate_ticket = new System.Windows.Forms.Button();
      this.lbl_ticket_pay_num = new System.Windows.Forms.Label();
      this.txt_ticket_pay_num = new System.Windows.Forms.TextBox();
      this.gb_ticket_new = new System.Windows.Forms.GroupBox();
      this.txt_ticket_new_info = new System.Windows.Forms.TextBox();
      this.lbl_ticket_new_info = new System.Windows.Forms.Label();
      this.btn_create_ticket = new System.Windows.Forms.Button();
      this.lbl_ticket_new_monto = new System.Windows.Forms.Label();
      this.txt_ticket_new_monto = new System.Windows.Forms.TextBox();
      this.btn_payment = new System.Windows.Forms.Button();
      this.lbl_countr_error = new System.Windows.Forms.Label();
      this.lbl_payment_ok = new System.Windows.Forms.Label();
      this.lbl_payment_ko = new System.Windows.Forms.Label();
      this.gb_ticket_pay.SuspendLayout();
      this.gb_ticket_new.SuspendLayout();
      this.SuspendLayout();
      // 
      // cb_countr
      // 
      this.cb_countr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cb_countr.Font = new System.Drawing.Font("Arial", 14.25F);
      this.cb_countr.FormattingEnabled = true;
      this.cb_countr.Location = new System.Drawing.Point(110, 16);
      this.cb_countr.Name = "cb_countr";
      this.cb_countr.Size = new System.Drawing.Size(178, 30);
      this.cb_countr.TabIndex = 0;
      this.cb_countr.SelectedIndexChanged += new System.EventHandler(this.cb_countr_SelectedIndexChanged);
      // 
      // lbl_countr
      // 
      this.lbl_countr.AutoSize = true;
      this.lbl_countr.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_countr.Location = new System.Drawing.Point(12, 20);
      this.lbl_countr.Name = "lbl_countr";
      this.lbl_countr.Size = new System.Drawing.Size(79, 22);
      this.lbl_countr.TabIndex = 7;
      this.lbl_countr.Text = "CountR:";
      // 
      // btn_reset
      // 
      this.btn_reset.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_reset.Location = new System.Drawing.Point(410, 491);
      this.btn_reset.Name = "btn_reset";
      this.btn_reset.Size = new System.Drawing.Size(90, 30);
      this.btn_reset.TabIndex = 4;
      this.btn_reset.Text = "Reset";
      this.btn_reset.UseVisualStyleBackColor = true;
      this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
      // 
      // btn_exit
      // 
      this.btn_exit.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_exit.Location = new System.Drawing.Point(410, 530);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.Size = new System.Drawing.Size(90, 30);
      this.btn_exit.TabIndex = 5;
      this.btn_exit.Text = "Salir";
      this.btn_exit.UseVisualStyleBackColor = true;
      this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // gb_ticket_pay
      // 
      this.gb_ticket_pay.Controls.Add(this.btn_cancel);
      this.gb_ticket_pay.Controls.Add(this.txt_ticket_pay_info);
      this.gb_ticket_pay.Controls.Add(this.lbl_ticket_pay_info);
      this.gb_ticket_pay.Controls.Add(this.btn_validate_ticket);
      this.gb_ticket_pay.Controls.Add(this.lbl_ticket_pay_num);
      this.gb_ticket_pay.Controls.Add(this.txt_ticket_pay_num);
      this.gb_ticket_pay.Font = new System.Drawing.Font("Arial", 14.25F);
      this.gb_ticket_pay.Location = new System.Drawing.Point(16, 67);
      this.gb_ticket_pay.Name = "gb_ticket_pay";
      this.gb_ticket_pay.Size = new System.Drawing.Size(463, 198);
      this.gb_ticket_pay.TabIndex = 1;
      this.gb_ticket_pay.TabStop = false;
      this.gb_ticket_pay.Text = "Ticket a pagar:";
      // 
      // btn_cancel
      // 
      this.btn_cancel.Enabled = false;
      this.btn_cancel.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_cancel.Location = new System.Drawing.Point(360, 71);
      this.btn_cancel.Name = "btn_cancel";
      this.btn_cancel.Size = new System.Drawing.Size(90, 30);
      this.btn_cancel.TabIndex = 17;
      this.btn_cancel.Text = "Cancelar";
      this.btn_cancel.UseVisualStyleBackColor = true;
      this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
      // 
      // txt_ticket_pay_info
      // 
      this.txt_ticket_pay_info.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_ticket_pay_info.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_ticket_pay_info.Location = new System.Drawing.Point(75, 71);
      this.txt_ticket_pay_info.MaxLength = 1500;
      this.txt_ticket_pay_info.Multiline = true;
      this.txt_ticket_pay_info.Name = "txt_ticket_pay_info";
      this.txt_ticket_pay_info.ReadOnly = true;
      this.txt_ticket_pay_info.Size = new System.Drawing.Size(251, 108);
      this.txt_ticket_pay_info.TabIndex = 2;
      // 
      // lbl_ticket_pay_info
      // 
      this.lbl_ticket_pay_info.AutoSize = true;
      this.lbl_ticket_pay_info.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_ticket_pay_info.Location = new System.Drawing.Point(6, 74);
      this.lbl_ticket_pay_info.Name = "lbl_ticket_pay_info";
      this.lbl_ticket_pay_info.Size = new System.Drawing.Size(48, 22);
      this.lbl_ticket_pay_info.TabIndex = 16;
      this.lbl_ticket_pay_info.Text = "Info:";
      // 
      // btn_validate_ticket
      // 
      this.btn_validate_ticket.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_validate_ticket.Location = new System.Drawing.Point(360, 37);
      this.btn_validate_ticket.Name = "btn_validate_ticket";
      this.btn_validate_ticket.Size = new System.Drawing.Size(90, 30);
      this.btn_validate_ticket.TabIndex = 1;
      this.btn_validate_ticket.Text = "Validar";
      this.btn_validate_ticket.UseVisualStyleBackColor = true;
      this.btn_validate_ticket.Click += new System.EventHandler(this.btn_validate_ticket_Click);
      // 
      // lbl_ticket_pay_num
      // 
      this.lbl_ticket_pay_num.AutoSize = true;
      this.lbl_ticket_pay_num.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_ticket_pay_num.Location = new System.Drawing.Point(4, 39);
      this.lbl_ticket_pay_num.Name = "lbl_ticket_pay_num";
      this.lbl_ticket_pay_num.Size = new System.Drawing.Size(54, 22);
      this.lbl_ticket_pay_num.TabIndex = 14;
      this.lbl_ticket_pay_num.Text = "Num:";
      // 
      // txt_ticket_pay_num
      // 
      this.txt_ticket_pay_num.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_ticket_pay_num.Font = new System.Drawing.Font("Arial", 14.25F);
      this.txt_ticket_pay_num.Location = new System.Drawing.Point(75, 36);
      this.txt_ticket_pay_num.MaxLength = 20;
      this.txt_ticket_pay_num.Name = "txt_ticket_pay_num";
      this.txt_ticket_pay_num.Size = new System.Drawing.Size(251, 29);
      this.txt_ticket_pay_num.TabIndex = 0;
      // 
      // gb_ticket_new
      // 
      this.gb_ticket_new.Controls.Add(this.txt_ticket_new_info);
      this.gb_ticket_new.Controls.Add(this.lbl_ticket_new_info);
      this.gb_ticket_new.Controls.Add(this.btn_create_ticket);
      this.gb_ticket_new.Controls.Add(this.lbl_ticket_new_monto);
      this.gb_ticket_new.Controls.Add(this.txt_ticket_new_monto);
      this.gb_ticket_new.Enabled = false;
      this.gb_ticket_new.Font = new System.Drawing.Font("Arial", 14.25F);
      this.gb_ticket_new.Location = new System.Drawing.Point(16, 280);
      this.gb_ticket_new.Name = "gb_ticket_new";
      this.gb_ticket_new.Size = new System.Drawing.Size(463, 194);
      this.gb_ticket_new.TabIndex = 2;
      this.gb_ticket_new.TabStop = false;
      this.gb_ticket_new.Text = "Ticket nuevo:";
      // 
      // txt_ticket_new_info
      // 
      this.txt_ticket_new_info.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
      this.txt_ticket_new_info.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_ticket_new_info.Location = new System.Drawing.Point(75, 71);
      this.txt_ticket_new_info.MaxLength = 1500;
      this.txt_ticket_new_info.Multiline = true;
      this.txt_ticket_new_info.Name = "txt_ticket_new_info";
      this.txt_ticket_new_info.Size = new System.Drawing.Size(251, 108);
      this.txt_ticket_new_info.TabIndex = 2;
      // 
      // lbl_ticket_new_info
      // 
      this.lbl_ticket_new_info.AutoSize = true;
      this.lbl_ticket_new_info.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_ticket_new_info.Location = new System.Drawing.Point(6, 74);
      this.lbl_ticket_new_info.Name = "lbl_ticket_new_info";
      this.lbl_ticket_new_info.Size = new System.Drawing.Size(48, 22);
      this.lbl_ticket_new_info.TabIndex = 16;
      this.lbl_ticket_new_info.Text = "Info:";
      // 
      // btn_create_ticket
      // 
      this.btn_create_ticket.Font = new System.Drawing.Font("Verdana", 8.25F);
      this.btn_create_ticket.Location = new System.Drawing.Point(360, 37);
      this.btn_create_ticket.Name = "btn_create_ticket";
      this.btn_create_ticket.Size = new System.Drawing.Size(90, 30);
      this.btn_create_ticket.TabIndex = 1;
      this.btn_create_ticket.Text = "Crear";
      this.btn_create_ticket.UseVisualStyleBackColor = true;
      this.btn_create_ticket.Click += new System.EventHandler(this.btn_create_ticket_Click);
      // 
      // lbl_ticket_new_monto
      // 
      this.lbl_ticket_new_monto.AutoSize = true;
      this.lbl_ticket_new_monto.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_ticket_new_monto.Location = new System.Drawing.Point(4, 39);
      this.lbl_ticket_new_monto.Name = "lbl_ticket_new_monto";
      this.lbl_ticket_new_monto.Size = new System.Drawing.Size(67, 22);
      this.lbl_ticket_new_monto.TabIndex = 14;
      this.lbl_ticket_new_monto.Text = "Monto:";
      // 
      // txt_ticket_new_monto
      // 
      this.txt_ticket_new_monto.Font = new System.Drawing.Font("Arial", 14.25F);
      this.txt_ticket_new_monto.Location = new System.Drawing.Point(75, 36);
      this.txt_ticket_new_monto.MaxLength = 15;
      this.txt_ticket_new_monto.Name = "txt_ticket_new_monto";
      this.txt_ticket_new_monto.Size = new System.Drawing.Size(251, 29);
      this.txt_ticket_new_monto.TabIndex = 0;
      this.txt_ticket_new_monto.Validating += new System.ComponentModel.CancelEventHandler(this.txt_ticket_new_monto_Validating);
      // 
      // btn_payment
      // 
      this.btn_payment.Enabled = false;
      this.btn_payment.Font = new System.Drawing.Font("Arial", 14.25F);
      this.btn_payment.Location = new System.Drawing.Point(16, 491);
      this.btn_payment.Name = "btn_payment";
      this.btn_payment.Size = new System.Drawing.Size(156, 69);
      this.btn_payment.TabIndex = 3;
      this.btn_payment.Text = "Procesar";
      this.btn_payment.UseVisualStyleBackColor = true;
      this.btn_payment.Click += new System.EventHandler(this.btn_payment_Click);
      // 
      // lbl_countr_error
      // 
      this.lbl_countr_error.AutoSize = true;
      this.lbl_countr_error.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_countr_error.ForeColor = System.Drawing.Color.Red;
      this.lbl_countr_error.Location = new System.Drawing.Point(306, 20);
      this.lbl_countr_error.Name = "lbl_countr_error";
      this.lbl_countr_error.Size = new System.Drawing.Size(199, 22);
      this.lbl_countr_error.TabIndex = 8;
      this.lbl_countr_error.Text = "Seleccione un CountR";
      this.lbl_countr_error.Visible = false;
      // 
      // lbl_payment_ok
      // 
      this.lbl_payment_ok.AutoSize = true;
      this.lbl_payment_ok.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_payment_ok.ForeColor = System.Drawing.Color.Green;
      this.lbl_payment_ok.Location = new System.Drawing.Point(178, 493);
      this.lbl_payment_ok.Name = "lbl_payment_ok";
      this.lbl_payment_ok.Size = new System.Drawing.Size(93, 22);
      this.lbl_payment_ok.TabIndex = 9;
      this.lbl_payment_ok.Text = "Pagado !!";
      this.lbl_payment_ok.Visible = false;
      // 
      // lbl_payment_ko
      // 
      this.lbl_payment_ko.AutoSize = true;
      this.lbl_payment_ko.Font = new System.Drawing.Font("Arial", 14.25F);
      this.lbl_payment_ko.ForeColor = System.Drawing.Color.Red;
      this.lbl_payment_ko.Location = new System.Drawing.Point(178, 530);
      this.lbl_payment_ko.Name = "lbl_payment_ko";
      this.lbl_payment_ko.Size = new System.Drawing.Size(163, 22);
      this.lbl_payment_ko.TabIndex = 10;
      this.lbl_payment_ko.Text = "Error en el pago !!";
      this.lbl_payment_ko.Visible = false;
      // 
      // frm_virtual_countr
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(512, 570);
      this.Controls.Add(this.lbl_payment_ko);
      this.Controls.Add(this.lbl_payment_ok);
      this.Controls.Add(this.lbl_countr_error);
      this.Controls.Add(this.btn_payment);
      this.Controls.Add(this.gb_ticket_new);
      this.Controls.Add(this.gb_ticket_pay);
      this.Controls.Add(this.btn_exit);
      this.Controls.Add(this.btn_reset);
      this.Controls.Add(this.lbl_countr);
      this.Controls.Add(this.cb_countr);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "frm_virtual_countr";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Virtual CountR";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_virtual_countr_FormClosed);
      this.Load += new System.EventHandler(this.frm_virtual_countr_Load);
      this.gb_ticket_pay.ResumeLayout(false);
      this.gb_ticket_pay.PerformLayout();
      this.gb_ticket_new.ResumeLayout(false);
      this.gb_ticket_new.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cb_countr;
    private System.Windows.Forms.Label lbl_countr;
    private System.Windows.Forms.Button btn_reset;
    private System.Windows.Forms.Button btn_exit;
    private System.Windows.Forms.GroupBox gb_ticket_pay;
    private System.Windows.Forms.TextBox txt_ticket_pay_info;
    private System.Windows.Forms.Label lbl_ticket_pay_info;
    private System.Windows.Forms.Button btn_validate_ticket;
    private System.Windows.Forms.Label lbl_ticket_pay_num;
    private System.Windows.Forms.TextBox txt_ticket_pay_num;
    private System.Windows.Forms.GroupBox gb_ticket_new;
    private System.Windows.Forms.TextBox txt_ticket_new_info;
    private System.Windows.Forms.Label lbl_ticket_new_info;
    private System.Windows.Forms.Button btn_create_ticket;
    private System.Windows.Forms.Label lbl_ticket_new_monto;
    private System.Windows.Forms.TextBox txt_ticket_new_monto;
    private System.Windows.Forms.Button btn_payment;
    private System.Windows.Forms.Label lbl_countr_error;
    private System.Windows.Forms.Label lbl_payment_ok;
    private System.Windows.Forms.Label lbl_payment_ko;
    private System.Windows.Forms.Button btn_cancel;
  }
}

