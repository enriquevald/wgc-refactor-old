﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : frm_virtual_countr.cs
// 
//   DESCRIPTION : Simulator CountR
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-APR-2016 AMF    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.CountR;

namespace VirtualCountR
{
  public partial class frm_virtual_countr : Form
  {

    #region " Members "

    private String m_service_name = "Unknown";
    private String m_version = "CC.BBB";
    private String m_currency_ISOcode = "";

    private Ticket m_paid_ticket;
    private Ticket m_new_ticket;
    private Int32 m_countr_code = -1;
    private Int64 m_ticket_amount_cents = 0;
    private Decimal m_new_ticket_amount = 0;

    #endregion " Members "

    #region " Properties "

    protected String ServiceName
    {
      get { return m_service_name; }
      set { m_service_name = value; }
    } // ServiceName

    protected String Version
    {
      get { return m_version; }
      set { m_version = value; }
    } // Version

    protected Int32 CountrCode
    {
      get { return m_countr_code; }
      set { m_countr_code = value; }
    } // CountrCode

    protected Decimal NewTicketAmount
    {
      get { return m_new_ticket_amount; }
      set { m_new_ticket_amount = value; }
    } // NewTicketAmount

    protected Ticket PaidTicket
    {
      get { return m_paid_ticket; }
      set { m_paid_ticket = value; }
    } // PaidTicket

    protected Ticket NewTicket
    {
      get { return m_new_ticket; }
      set { m_new_ticket = value; }
    } // NewTicket

    protected String CurrencyISOCode
    {
      get
      {
        if (String.IsNullOrEmpty(m_currency_ISOcode))
        {
          m_currency_ISOcode = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN");
        }

        return m_currency_ISOcode;
      }
    } // CurrencyISOCode

    protected Int64 TicketAmountCents
    {
      get { return m_ticket_amount_cents; }
      set { m_ticket_amount_cents = value; }
    } // TicketAmountCents

    #endregion " Properties "

    #region " Public Methods "

    /// <summary>
    /// Main
    /// </summary>
    public frm_virtual_countr()
    {
      InitializeComponent();
      cls_virual_countr.CreateConnection(this.ServiceName, this.Version);
      cls_virual_countr.FillComboCountR(ref this.cb_countr);
    } // frm_virtual_countr

    #endregion " Public Methods "

    #region " Events "

    /// <summary>
    /// Button exit Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_exit_Click(object sender, EventArgs e)
    {
      Environment.Exit(0);
    } // btn_exit_Click

    /// <summary>
    /// Button reset click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_reset_Click(object sender, EventArgs e)
    {
      cb_countr.SelectedIndex = -1;
      cb_countr.Enabled = true;
      lbl_countr_error.Visible = false;
      this.CountrCode = -1;

      txt_ticket_pay_num.Text = String.Empty;
      txt_ticket_pay_info.Text = String.Empty;
      this.PaidTicket = null;

      txt_ticket_new_monto.Text = String.Empty;
      txt_ticket_new_info.Text = String.Empty;
      this.NewTicketAmount = 0;
      this.NewTicket = null;

      lbl_payment_ko.Visible = false;
      lbl_payment_ok.Visible = false;

    } // btn_reset_Click

    /// <summary>
    /// Button validate click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_validate_ticket_Click(object sender, EventArgs e)
    {
      Int32 _countr_id;
      ACKRedeemKiosk _ack_redeem;

      try
      {
        if (this.CountrCode == -1)
        {
          if (cb_countr.SelectedIndex == -1)
          {
            lbl_countr_error.Visible = true;

            return;
          }

          if (!Int32.TryParse(cb_countr.SelectedValue.ToString(), out _countr_id))
          {
            lbl_countr_error.Visible = true;

            return;
          }

          cb_countr.Enabled = false;
          this.CountrCode = _countr_id;
        }

        if (String.IsNullOrEmpty(txt_ticket_pay_num.Text))
        {
          txt_ticket_pay_info.Text = "Introducir Ticket";

          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          CountRBussinesLogic.CheckAccount(this.CountrCode, txt_ticket_pay_num.Text, out _ack_redeem, _db_trx.SqlTransaction);
        }

        if (_ack_redeem.Ack == CountRBussinesLogic.Ack.ACK)
        {
          txt_ticket_pay_info.Text = "Ticket válido \r\n \r\n";
          txt_ticket_pay_info.Text += "Monto: " + Currency.Format((_ack_redeem.AmountCents / 100), this.CurrencyISOCode) + "\r\n \r\n";
          txt_ticket_pay_info.Text += "Pulse Cancelar o Procesar";

          this.txt_ticket_pay_num.Enabled = false;
          this.btn_validate_ticket.Enabled = false;

          this.btn_cancel.Enabled = true;
          this.btn_payment.Enabled = true;
          this.btn_payment.Focus();

          this.TicketAmountCents = _ack_redeem.AmountCents;
        }
        else
        {
          txt_ticket_pay_info.Text = "Ticket no válido \r\n \r\n";
          txt_ticket_pay_info.Text += "Error: " + _ack_redeem.AckExtended;
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }
    } // btn_validate_ticket_Click

    /// <summary>
    /// Cancel validate ticket
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.cb_countr.Enabled = true;
      this.txt_ticket_pay_num.Enabled = true;
      this.btn_validate_ticket.Enabled = true;

      this.btn_cancel.Enabled = false;
      this.btn_payment.Enabled = false;

      this.lbl_payment_ko.Visible = false;

      this.NewTicketAmount = 0;
    } // btn_cancel_Click

    /// <summary>
    /// Button create ticket click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_create_ticket_Click(object sender, EventArgs e)
    {
      Int32 _countr_id;
      Decimal _amount_taxes = 0;

      try
      {
        if (String.IsNullOrEmpty(txt_ticket_new_monto.Text))
        {
          txt_ticket_new_info.Text = "Introducir monto válido";

          return;
        }

        if (this.CountrCode == -1)
        {
          if (cb_countr.SelectedIndex == -1)
          {
            lbl_countr_error.Visible = true;

            return;
          }

          if (!Int32.TryParse(cb_countr.SelectedValue.ToString(), out _countr_id))
          {
            lbl_countr_error.Visible = true;

            return;
          }

          cb_countr.Enabled = false;
          this.CountrCode = _countr_id;
        }

        if (this.NewTicketAmount <= 0)
        {
          txt_ticket_new_info.Text = "Introducir monto válido";

          return;
        }

        //this.NewTicket = CountRBussinesLogic.CreateTicket(this.CountrCode, this.NewTicketAmount);

        if (this.NewTicket != null)
        {
          if (this.NewTicket.Taxes != null)
          {
            foreach (CountRTaxes _taxe in this.NewTicket.Taxes)
            {
              _amount_taxes += _taxe.Amount;
            }
          }

          txt_ticket_new_info.Text = "Ticket válido \r\n \r\n";
          txt_ticket_new_info.Text += "ID: " + this.NewTicket.TicketID.ToString() + "\r\n";
          txt_ticket_new_info.Text += "Monto: " + Currency.Format(this.NewTicket.Amount, this.CurrencyISOCode) + "\r\n";
          txt_ticket_new_info.Text += "Impuestos: " + Currency.Format(_amount_taxes, this.CurrencyISOCode);
        }
        else
        {
          txt_ticket_new_info.Text = "Error creando Ticket";
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }
    } // btn_create_ticket_Click

    /// <summary>
    /// Button payment click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btn_payment_Click(object sender, EventArgs e)
    {
      ACKRedeemKiosk _ack_redeem;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!CountRBussinesLogic.StartPayment(this.CountrCode, txt_ticket_pay_num.Text, this.TicketAmountCents, out _ack_redeem, _db_trx.SqlTransaction))
          {
            lbl_payment_ko.Visible = true;

            return;
          }

          _db_trx.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (CountRBussinesLogic.EndPayment(this.CountrCode, txt_ticket_pay_num.Text, this.TicketAmountCents, out _ack_redeem, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            this.lbl_payment_ok.Visible = true;

            this.btn_payment.Enabled = false;
            this.btn_cancel.Enabled = false;

            return;
          }
        }

        lbl_payment_ko.Visible = true;
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
        lbl_payment_ko.Visible = true;
      }
    } // btn_payment_Click

    /// <summary>
    /// Combo CountR Changed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void cb_countr_SelectedIndexChanged(object sender, EventArgs e)
    {
      lbl_countr_error.Visible = false;
    } // cb_countr_SelectedIndexChanged

    /// <summary>
    /// Form closed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frm_virtual_countr_FormClosed(object sender, FormClosedEventArgs e)
    {
      Environment.Exit(0);
    } // frm_virtual_countr_FormClosed

    /// <summary>
    /// Validate ticket amount
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void txt_ticket_new_monto_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (String.IsNullOrEmpty(txt_ticket_new_monto.Text)) { return; }

        if (!Decimal.TryParse(txt_ticket_new_monto.Text, out m_new_ticket_amount))
        {
          txt_ticket_new_info.Text = "Introducir monto válido";
        }
        else
        {
          if (this.NewTicketAmount < 0)
          {
            txt_ticket_new_info.Text = "Introducir monto válido";

            return;
          }

          txt_ticket_new_info.Text = String.Empty;
          txt_ticket_new_monto.Text = Currency.Format(this.NewTicketAmount, this.CurrencyISOCode).ToString();
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }
    } // txt_ticket_new_monto_Validating

    /// <summary>
    /// Form Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frm_virtual_countr_Load(object sender, EventArgs e)
    {
      OperationVoucherParams.LoadData = true;
    } // frm_virtual_countr_Load

    #endregion " Events "
  }
}
