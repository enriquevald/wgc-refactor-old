﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : cls_virtual_countr.cs
// 
//   DESCRIPTION : Simulator CountR Class
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAY-2016 AMF    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;

namespace VirtualCountR
{
  class cls_virual_countr
  {
    #region " Public Methods "

    /// <summary>
    /// Fill CountR list into combo
    /// </summary>
    public static void FillComboCountR(ref System.Windows.Forms.ComboBox FillCombo)
    {
      StringBuilder _sb;
      DataTable _table;

      _table = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   CR_CODE        ");
        _sb.AppendLine("          , CR_NAME        ");
        _sb.AppendLine("     FROM   COUNTR         ");
        _sb.AppendLine("    WHERE   CR_ENABLED = 1 ");
        _sb.AppendLine(" ORDER BY   CR_NAME        ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _db_trx.Fill(_sql_da, _table);
            }
          }
        }

        FillCombo.DataSource = _table;
        FillCombo.DisplayMember = "CR_NAME";
        FillCombo.ValueMember = "CR_CODE";
        FillCombo.SelectedIndex = -1;
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }
    } // FillComboCountR

    /// <summary>
    /// Create DB connection 
    /// </summary>
    /// <param name="ServiceName"></param>
    /// <param name="Version"></param>
    public static void CreateConnection(String ServiceName, String Version)
    {
      StringBuilder _xml;

      try
      {
        _xml = new StringBuilder();
        _xml.AppendLine("<SiteConfig>");
        _xml.AppendLine("    <DBPrincipal>VSCAGE</DBPrincipal>");
        _xml.AppendLine("    <DBMirror>VSCAGE</DBMirror>");
        _xml.AppendLine("    <DBId>701</DBId>");
        _xml.AppendLine("</SiteConfig>");

        if (!ConfigurationFile.Init("VirtualCountR.cfg", _xml.ToString()))
        {
          Log.Error(" Reading application configuration settings. Application stopped");

          return;
        }

        // Connect to DB
        WGDB.Init(Convert.ToInt32(ConfigurationFile.GetSetting("DBId")), ConfigurationFile.GetSetting("DBPrincipal"), ConfigurationFile.GetSetting("DBMirror"));
        WGDB.SetApplication(ServiceName, Version);
        WGDB.ConnectAs("WGROOT");

        while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
        {
          Log.Warning("Waiting for database connection ...");

          Thread.Sleep(3000);
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }
    } // CreateConnection

    #endregion " Public Methods "
  }
}
