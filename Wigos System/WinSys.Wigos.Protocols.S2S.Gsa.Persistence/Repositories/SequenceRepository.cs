﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories
{
	/// <summary>
	/// Implements a sequence repository
	/// </summary>
	/// <remarks>Multithreading safe</remarks>
	public class SequenceRepository : ISequenceRepository
	{
		private const string SequenceIdParameter = @"@sequenceId";
		private const string InitialValueParameter = "@initialValue";
		private const string BlockSizeParameter = "@blockSize";
		private const string GetNextValueSql = @"SELECT seq_next_value FROM sequences WHERE seq_id = @sequenceId";
		private const string SetInitialValueSql = @"INSERT INTO sequences (seq_id, seq_next_value) VALUES (@sequenceId, @initialValue)";
		private const string IncrementNextValueSql = @"UPDATE sequences SET seq_next_value = (SELECT seq_next_value + @blockSize FROM sequences WHERE seq_id = @sequenceId) WHERE seq_id = @sequenceId";

		private readonly Dictionary<SequenceId, object> sequencesDictionary;

		public SequenceRepository()
		{
			this.sequencesDictionary = new Dictionary<SequenceId, object>();
		}

		#region ISequenceRepository

		/// <summary>
		/// Implements a sequence repository
		/// </summary>
		public long GetCurrentValue(SequenceId sequenceId)
		{
			return this.GetSequenceValue(sequenceId, blockSize: 1, isNextValue: false);
		}

		/// <summary>
		/// Get the next value of the specified sequence
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get value</param>
		/// <returns>Current value of the sequence</returns>
		/// <remarks>If the sequence Id specified doesn't exist, it's created</remarks>
		public long GetNextValue(SequenceId sequenceId)
		{
			return this.GetSequenceValue(sequenceId, blockSize: 1, isNextValue: true);
		}

		/// <summary>
		/// Get the next block value of the specified sequence
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get the block</param>
		/// <param name="blockSize">Block size</param>
		/// <returns>The first value of the block and you can use this first value up to <code>blockSize - 1</code></returns>
		/// <remarks>
		/// If the sequence Id specified doesn't exist, it's created.
		/// <code>BlockSize</code> equals to 1 is like a <code>GetNextValue</code> call.
		/// </remarks>
		public long GetNextBlockValue(
			SequenceId sequenceId, 
			int blockSize)
		{
			if (blockSize <= 0)
			{
				throw new ArgumentException("Parameter must be a value greater than 0.", "blockSize");
			}

			return this.GetSequenceValue(sequenceId, blockSize, isNextValue: true);
		}

		#endregion

		/// <summary>
		/// Gets the lock for the specified sequenceId
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get the lock</param>
		/// <returns></returns>
		private object GetSequenceLock(SequenceId sequenceId)
		{
			object result;

			lock (this.sequencesDictionary)
			{
				if (this.sequencesDictionary.ContainsKey(sequenceId))
				{
					result = this.sequencesDictionary[sequenceId];
				}
				else
				{
					result = new object();
					this.sequencesDictionary.Add(sequenceId, result);
				}
			}

			return result;
		}

		/// <summary>
		/// Initializes a specified sequenceId
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to initialize</param>
		/// <param name="initialValue">First value for the sequence</param>
		/// <param name="sqlTransaction">Sql transaction for the operation</param>
		/// <remarks>No multithreading safe</remarks>
		private void InitializeSequenceValueInDataBase(
			SequenceId sequenceId,
			long initialValue,
			SqlTransaction sqlTransaction)
		{
			using (SqlCommand sqlCommand = new SqlCommand(SequenceRepository.SetInitialValueSql, sqlTransaction.Connection, sqlTransaction))
			{
				sqlCommand.Parameters.Add(SequenceRepository.SequenceIdParameter, SqlDbType.Int).Value = sequenceId;
				sqlCommand.Parameters.Add(SequenceRepository.InitialValueParameter, SqlDbType.BigInt).Value = initialValue;

				sqlCommand.ExecuteNonQuery();
			}
		}

		/// <summary>
		/// Get the value for a specifed sequenceId
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to initialize</param>
		/// <param name="blockSize">Block size</param>
		/// <param name="isNextValue">Specifies is the next value or not to get it</param>
		/// <returns>If <code>isNextValue</code> is <code>true</code> then returns the next value, else not return current value of the specified sequence</returns>
		/// <remarks>This method manage the multithreading lock and database transaction performing the data access with other methods: InitializeSequenceValueInDataBase and GetSequenceValueFromDataBase</remarks>
		private long GetSequenceValue(
			SequenceId sequenceId, 
			int blockSize,
			bool isNextValue)
		{
			long result;

			lock (this.GetSequenceLock(sequenceId))
			{
				using (DB_TRX dataBaseTransaction = new DB_TRX())
				{
					try
					{
						long? valueSequence = this.GetSequenceValueFromDataBase(sequenceId, blockSize, dataBaseTransaction.SqlTransaction, isNextValue);
						if (valueSequence.HasValue)
						{
							result = valueSequence.Value;
						}
						else
						{
							result = 0;
							this.InitializeSequenceValueInDataBase(sequenceId, initialValue: 1, sqlTransaction: dataBaseTransaction.SqlTransaction);
						}

						dataBaseTransaction.Commit();
					}
					catch
					{
						dataBaseTransaction.Rollback();
						throw;
					}
				}
			}

			return result;
		}
		
		/// <summary>
		/// Get the value for a specifed sequenceId from the data base
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to initialize</param>
		/// <param name="blockSize">Block size</param>
		/// <param name="sqlTransaction">Sql transaction for the operation</param>
		/// <param name="isNextValue">Specifies is the next value or not to get it</param>
		/// <returns>The secuence value if has been initialized previously in data base, else returns null value</returns>
		/// <remarks>No multithreading safe</remarks>
		private long? GetSequenceValueFromDataBase(
			SequenceId sequenceId,
			int blockSize,
			SqlTransaction sqlTransaction,
			bool isNextValue)
		{
			long? result = null;

			StringBuilder sqlCommandBuilder = new StringBuilder(SequenceRepository.GetNextValueSql);
			if (isNextValue)
			{
				sqlCommandBuilder.Append(";" + SequenceRepository.IncrementNextValueSql);
			}

			using (SqlCommand sqlCommand = new SqlCommand(sqlCommandBuilder.ToString(), sqlTransaction.Connection, sqlTransaction))
			{
				sqlCommand.Parameters.Add(SequenceRepository.SequenceIdParameter, SqlDbType.Int).Value = (int)sequenceId;
				if (isNextValue)
				{
					sqlCommand.Parameters.Add(SequenceRepository.BlockSizeParameter, SqlDbType.Int).Value = blockSize;
				}
				using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
				{
					if (sqlDataReader.Read())
					{
						result = isNextValue ? sqlDataReader.GetInt64(0) : sqlDataReader.GetInt64(0) - 1;
					}
				}
			}

			return result;
		}
	}
}