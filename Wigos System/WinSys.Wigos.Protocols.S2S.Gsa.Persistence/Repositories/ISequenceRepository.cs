﻿using WSI.Common;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Persistence.Repositories
{
	/// <summary>
	/// Implements a sequence repository
	/// </summary>
	public interface ISequenceRepository
	{
		/// <summary>
		/// Get the current value of the specified sequence
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get value</param>
		/// <returns>Current value of the sequence</returns>
		/// <remarks>If the sequence Id specified doesn't exist, it's created</remarks>
		long GetCurrentValue(SequenceId sequenceId);

		/// <summary>
		/// Get the next value of the specified sequence
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get value</param>
		/// <returns>Current value of the sequence</returns>
		/// <remarks>If the sequence Id specified doesn't exist, it's created</remarks>
		long GetNextValue(SequenceId sequenceId);

		/// <summary>
		/// Get the next block value of the specified sequence
		/// </summary>
		/// <param name="sequenceId">Id of the sequence to get the block</param>
		/// <param name="blockSize"></param>
		/// <returns>The first value of the block and you can use this first value up to <code>blockSize - 1</code></returns>
		/// <remarks>
		/// If the sequence Id specified doesn't exist, it's created.
		/// <code>BlockSize</code> equals to 1 is like a <code>GetNextValue</code> call.
		/// </remarks>
		long GetNextBlockValue(SequenceId sequenceId, int blockSize);
	}
}