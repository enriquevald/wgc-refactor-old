﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace WinSys.Wigos.Protocols.S2S.Gsa.Persistence
{
	public static class Providers
	{
		public static Boolean DB_InsertProvider(string providerName, SqlTransaction sqlTransaction)
		{
			StringBuilder sqlSentence = new StringBuilder();
			try
			{
				sqlSentence.AppendLine("INSERT INTO providers([pv_name] ,[pv_hide] ,[pv_points_multiplier] ,[pv_3gs] ,[pv_3gs_vendor_id] ,[pv_3gs_vendor_ip] ,[pv_site_jackpot] ,[pv_only_redeemable] ,[pv_ms_sequence_id])");
				sqlSentence.AppendLine("VALUES(@pv_name ,0 ,1.00 ,0 ,NULL ,NULL ,1 ,0 ,1)");

				using (SqlCommand cmd = new SqlCommand(sqlSentence.ToString(), sqlTransaction.Connection, sqlTransaction))
				{
					cmd.Parameters.Add(new SqlParameter("pv_name", providerName));
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{

				WSI.Common.Log.Exception(ex);
			}
			return true;

		}
	}
}