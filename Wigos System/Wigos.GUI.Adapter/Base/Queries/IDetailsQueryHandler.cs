﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wigos.GUI.Adapter.Base.Queries
{
  public interface IDetailsQueryHandler<TQuery> : IDisposable
    where TQuery : IQuery
  {
    TQuery Execute();
  }
}
