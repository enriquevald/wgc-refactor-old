﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wigos.GUI.Adapter.Base.Commands
{
  public interface ICommandHandler<TCommand, TResult> : IDisposable
     where TCommand : Command
  {
    TResult Execute(TCommand command);
  }
}
