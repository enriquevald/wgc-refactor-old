﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wigos.GUI.Adapter.Base.Commands
{
  public abstract class Command : ICommand
  {
    public abstract long? Id { get; set; }

    public Command(long? id = null)
    {
      Id = id;
    }
  }
}
