﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wigos.GUI.Adapter.Base.Commands
{
  public interface ICommand
  {
    long? Id { get; }
  }
}
