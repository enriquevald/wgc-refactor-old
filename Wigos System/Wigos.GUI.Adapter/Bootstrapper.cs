﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

using Wigos.Cage.Factory.Sessions;
using Factory = Wigos.Cage.Factory;

namespace Wigos.GUI.Adapter
{
  public class Bootstrapper
  {
    public static void Initialize(IUnityContainer container)
    {
      // Initialize Wigos.Cage Factory
      Factory.Bootstrapper.Initialize(container);

      // Register translators
      RegisterTranslator(container);
    }

    /// <summary>
    /// Register translator types for Dependency Injection
    /// </summary>
    /// <param name="container"></param>
    private static void RegisterTranslator(IUnityContainer container)
    {
      //container.RegisterType<ISessionProxy, SessionProxy>();

      //container.RegisterInstance<ISessionProxy>()
    }
  }
}
