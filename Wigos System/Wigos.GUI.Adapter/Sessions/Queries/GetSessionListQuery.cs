﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wigos.Cage.Features.Sessions;
//using Wigos.Cage.Proxy.Sessions;
using Wigos.GUI.Adapter.Base.Queries;
using Wigos.GUI.Adapter.Common.Base;

using Wigos.Cage.Features.Interfaces.Dto.Sessions;
using Wigos.Cage.Factory.Sessions;

namespace Wigos.GUI.Adapter.Sessions.Queries
{
  public class GetSessionListQuery : IListQueryHandler<DataTable>
  {
    private ISessionFactory sessionFactory;
    private ITranslator<IEnumerable<SessionOutputDto>, DataTable> translator;

    public GetSessionListQuery(ISessionFactory _sessionFactory,
      ITranslator<IEnumerable<SessionOutputDto>, DataTable> _translator)
    {
      sessionFactory = _sessionFactory;
      translator = _translator;
    }

    public DataTable Execute()
    {
      DataTable sessionsDataTable;
      var listSessionFactory = sessionFactory.CreateListSession();
      var sessionList = listSessionFactory.Execute();

      if (sessionList.Status == Core.Entity.Result.StatusResult.Success)
      {
        sessionsDataTable = translator.Translate(sessionList.Dto);
      }
      else
      {
        //TODO: Log here
        throw new Exception(); //TODO: use WigosException that includes the list of errors
      }
      return sessionsDataTable;
    }

    public void Dispose()
    {
      //sessionProxy.Dispose();
    }

  }
}