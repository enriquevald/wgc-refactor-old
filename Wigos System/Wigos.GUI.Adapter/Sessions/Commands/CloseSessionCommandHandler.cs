﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wigos.Cage.Factory.Sessions;
using Wigos.Core.Entity;
using Wigos.GUI.Adapter.Base.Commands;
using Wigos.GUI.Adapter.Common.Base;
using Wigos.Cage.Features.Interfaces.Dto.Sessions;

namespace Wigos.GUI.Adapter.Sessions.Commands
{
  public class CloseSessionCommandHandler : ICommandHandler<CloseSessionCommand, Result>
  {
    private ISessionFactory sessionFactory;
    public CloseSessionCommandHandler(ISessionFactory _sessionFactory)
    {
      this.sessionFactory = _sessionFactory;
    }

    public Result Execute(CloseSessionCommand command)
    {
      var closeSessionFactory = sessionFactory.CreateCloseSession();
      var result = closeSessionFactory.Execute(command.Id.Value, command.CloseUserId);
      closeSessionFactory.Dispose();

      return result;
    }

    public void Dispose()
    {
      //  sessionProxy.Dispose();
    }
  }
}
