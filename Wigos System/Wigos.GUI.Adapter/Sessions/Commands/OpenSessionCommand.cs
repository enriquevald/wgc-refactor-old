﻿using System;
using Wigos.GUI.Adapter.Base.Commands;

namespace Wigos.GUI.Adapter.Sessions.Commands
{
  public class OpenSessionCommand : Command
  {
    public override long? Id { get; set; }
    public string Name { get; set; }
    public DateTime WorkingDate { get; set; }

    public OpenSessionCommand(string name, DateTime workingDate)
      : base()
    {
      Name = name;
      WorkingDate = workingDate;
    }

  }
}
