﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wigos.GUI.Adapter.Base.Commands;

namespace Wigos.GUI.Adapter.Sessions.Commands
{
  public class CloseSessionCommand : Command
  {
    public override long? Id { get; set; }

    public int CloseUserId { get; set; }
    

    public CloseSessionCommand(long? id, int closeUserId)
      : base()
    {      
      Id = id;
      CloseUserId = closeUserId;
      
    }    
  }
}
