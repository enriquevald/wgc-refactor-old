﻿using Wigos.Cage.Factory.Sessions;
using Wigos.Core.Entity;
using Wigos.GUI.Adapter.Base.Commands;

namespace Wigos.GUI.Adapter.Sessions.Commands
{
  public class OpenSessionCommandHandler : ICommandHandler<OpenSessionCommand, Result>
  {    
    private ISessionFactory sessionFactory;

    public OpenSessionCommandHandler(ISessionFactory _sessionFactory)
    {
      sessionFactory = _sessionFactory;      
    }

    public Result Execute(OpenSessionCommand command)
    {
      var openSessionFactory = sessionFactory.CreateOpenSession();
      var sessionInputDto = new Wigos.Cage.Features.Interfaces.Dto.Sessions.SessionInputDto() 
      {
        Name = command.Name,
        OpeningDate = command.WorkingDate
      };

      var result = openSessionFactory.Execute(sessionInputDto);

      openSessionFactory.Dispose();

      return result;
    }

    public void Dispose()
    {
      //proxy.Dispose();
      //sessionFactory.D
    }
  }
}
