﻿using System;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SQLite;

namespace WSI.Common.Test
{
  [TestClass]
  public class CurrencyMultisiteTests
  {
    private IDbConnection _con;

    [TestInitialize]
    public void Setup()
    {

      _con =
        new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");


      _con.Open();
      SQLiteCommand command = new SQLiteCommand("drop table exchange_rates", _con as SQLiteConnection);
      command.ExecuteNonQuery();
      command = new SQLiteCommand("drop table site_currencies", _con as SQLiteConnection);
      command.ExecuteNonQuery();
      command = new SQLiteCommand("CREATE TABLE exchange_rates (" +
                                  "ER_CHANGE decimal(6,10)," +
                                  "ER_ISO_CODE nvarchar(3)," +
                                  "ER_WORKING_DAY Date)", _con as SQLiteConnection);
      command.ExecuteNonQuery();
      command = new SQLiteCommand("CREATE TABLE site_currencies(" +
                                  "sc_site_id int," +
                                  "sc_iso_code nvarchar(3)," +
                                  "sc_type int," +
                                  "sc_description nvarchar(50)," +
                                  "sc_change decimal(16, 8))", _con as SQLiteConnection);
      command.ExecuteNonQuery();


      command = new SQLiteCommand("insert into site_currencies(" +
                                  "sc_site_id," +
                                  "sc_iso_code ," +
                                  "sc_type ," +
                                  "sc_description," +
                                  "sc_change )values(1,'mx',1,'desc',1)", _con as SQLiteConnection);

      command.ExecuteNonQuery();


      command = new SQLiteCommand("insert into exchange_rates (" +
                                  "ER_CHANGE," +
                                  "ER_ISO_CODE ," +
                                  "ER_WORKING_DAY) values(1,'mx','2010-01-01 00:00:00.000')", _con as SQLiteConnection);
      command.ExecuteNonQuery();




    }

    [TestCleanup]
    public void TearDown()
    {
      _con.Close();
    }


    [TestMethod]
    public void TestGetAllFilteredSitesCurrencies()
    {

      DataTable _dt;
      using (var _trx = _con.BeginTransaction())
      {
        Assert.IsTrue(CurrencyMultisite.GetAllFilteredSitesCurrencies(new DateTime(2010, 1, 1),
          new DateTime(2010, 1, 2), out _dt, _trx));
        Assert.AreEqual( 1,_dt.Rows.Count);
      }
    }
  }
}


