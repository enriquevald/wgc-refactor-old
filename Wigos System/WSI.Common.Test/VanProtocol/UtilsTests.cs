﻿using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common.TITO;

namespace WSI.Common.Test
{
  [TestClass]
  public class UtilsTests
  {
    private IDbConnection _con;

    [TestInitialize]
    public void Setup()
    {

      _con =
        new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");


      _con.Open();
      SQLiteCommand command = new SQLiteCommand("drop table if exists ACCOUNT_PROMOTIONS", _con as SQLiteConnection);
      command.ExecuteNonQuery();
      command = new SQLiteCommand("CREATE TABLE ACCOUNT_PROMOTIONS (" +
        "ACP_UNIQUE_ID long,"+
        "ACP_ACCOUNT_ID long,"+
        "ACP_BALANCE decimal(10,6),"+
        "ACP_CREDIT_TYPE int,"+
        "ACP_PROMO_NAME nvarchar(100)," +
        "ACP_PROMO_TYPE int,"+
        "ACP_PROMO_ID long,"+
        "ACP_ACTIVATION Date,"+
        "ACP_STATUS int)", _con as SQLiteConnection);
      command.ExecuteNonQuery();


      command = new SQLiteCommand("insert into ACCOUNT_PROMOTIONS (" +
        "ACP_UNIQUE_ID ," +
        "ACP_ACCOUNT_ID ," +
        "ACP_BALANCE ," +
        "ACP_CREDIT_TYPE ," +
        "ACP_PROMO_NAME ," +
        "ACP_PROMO_TYPE ," +
        "ACP_PROMO_ID ," +
        "ACP_ACTIVATION ," +
        "ACP_STATUS ) values(1," +
        "1,"+
        "10.66,"+
        "1,"+
        "'test promo'," +
        "1,"+
        "1,"+
        "'2010-01-01 00:00:00.000'," +
        "5)", _con as SQLiteConnection);

      command.ExecuteNonQuery();





    }

    [TestCleanup]
    public void TearDown()
    {
      _con.Close();
    }

    [TestMethod]
    public void TestLoadPromotionTable()
    {
      DataTable _dt;
      using (var _trx = _con.BeginTransaction())
      {
        Assert.IsTrue(Utils.LoadPromoTable(1, out _dt, _trx));
        Assert.AreEqual(_dt.Rows.Count, 1);
      }
    }



  }
}