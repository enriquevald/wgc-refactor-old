﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common.TITO;


namespace WSI.Common.Test
{
  [TestClass]
  public class CardDataTests
  {


    

    [TestMethod]
    public void TestLoadPromotionTable()
    {
      DataTable _dt;
      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_ut;" + "user=sa;password=Manager00;"))

      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          Assert.IsTrue(Utils.LoadPromoTable(1, out _dt, _trx));
          Assert.AreEqual(_dt.Rows.Count,1);
        }
      }
    }

    [TestMethod]
    public void TestLoadCardData()
    {
      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_ut;" + "user=sa;password=Manager00;"))

      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          GeneralParam.SetDataAvaiable();
          CardData cd = new CardData();

          Assert.IsTrue(CardData.DB_CardGetAllData(11111,
            cd,
            true,
            _trx));
          Assert.AreEqual(11111, cd.AccountId);
          Assert.IsNotNull(cd.PlayerTracking);
          Assert.AreEqual("ADAMSON", cd.PlayerTracking.HolderName1);
          Assert.AreEqual("", cd.PlayerTracking.HolderName2);
          Assert.AreEqual("SCOTT", cd.PlayerTracking.HolderName3);
          Assert.AreEqual("JAMES", cd.PlayerTracking.HolderName4);
          //Add more fields
        }
      }
    }

    [TestMethod]
    public void TestUpdateCardData()
    {
      long operid;
      using (
        IDbConnection _con =
          new SqlConnection("Data" +
                            " Source=VSCAGE;" + "Initial Catalog=wgdb_ut;" + "user=sa;password=Manager00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          
          GeneralParam.SetDataAvaiable();
          CardData _card_data = new CardData();

          Assert.IsTrue(CardData.DB_CardGetAllData(11111,
            _card_data,
            true,
            _trx));

          _card_data.PlayerTracking.HolderName1 = "TEST";
          _card_data.PlayerTracking.HolderName2 = "TESTERUS";
          _card_data.PlayerTracking.HolderName3 = "TESTER";
          _card_data.PlayerTracking.HolderName4 = "THOMAS";
          _card_data.PlayerTracking.HolderName = "TESTER THOMAS TEST TESTERUS";
          _card_data.PlayerTracking.HolderId = "IDNUM";
          _card_data.PlayerTracking.HolderIdType = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          _card_data.PlayerTracking.HolderBirthDate = new DateTime(1985, 10, 10);
          _card_data.PlayerTracking.BeneficiaryBirthDate = new DateTime(1985, 10, 10);
          _card_data.PlayerTracking.BeneficiaryGender = 0;
          _card_data.PlayerTracking.BeneficiaryId1 = "ID";
          _card_data.PlayerTracking.BeneficiaryId1Type = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          _card_data.PlayerTracking.BeneficiaryId2 = "ID2";
          _card_data.PlayerTracking.BeneficiaryId2Type = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          _card_data.PlayerTracking.BeneficiaryId3 = "ID3";
          _card_data.PlayerTracking.BeneficiaryId3Type = "ID3TYPE";
          _card_data.PlayerTracking.BeneficiaryName1 = "B1";
          _card_data.PlayerTracking.BeneficiaryName2 = "B2";
          _card_data.PlayerTracking.BeneficiaryName3 = "B3";
          _card_data.PlayerTracking.BeneficiaryName = "B1 B2 B3";
          _card_data.PlayerTracking.BeneficiaryOccupation = "AN OCCUPATION";
          _card_data.PlayerTracking.BeneficiaryOccupationId = 99;
          _card_data.PlayerTracking.BeneficiaryScannedDocs = new DocumentList();
          _card_data.PlayerTracking.CardCreationDate = DateTime.Now;
          _card_data.PlayerTracking.HolderAddress01 = "";
          _card_data.PlayerTracking.HolderAddress02 = "";
          _card_data.PlayerTracking.CardLevel = 1;

          _card_data.PlayerTracking.CurrentPoints = 0;
          _card_data.PlayerTracking.CurrentPointsBar = 0;
          _card_data.PlayerTracking.CurrentPointsCreditNR = 0;
          _card_data.PlayerTracking.HolderAddress03 = "";
          _card_data.PlayerTracking.HolderAddressCountry = 3;
          _card_data.PlayerTracking.HolderAddressValidation = ADDRESS_VALIDATION.MANUAL_ENTRY;
          _card_data.PlayerTracking.HolderBirthCountry = 3;
          _card_data.PlayerTracking.HolderCity = "CITY";
          _card_data.PlayerTracking.HolderComments = "";
          _card_data.PlayerTracking.HolderDocumentId1 = 1;
          _card_data.PlayerTracking.HolderDocumentId2 = 2;
          _card_data.PlayerTracking.HolderEmail01 = "s@s.s";
          _card_data.PlayerTracking.HolderEmail02 = "d@d.d";
          _card_data.PlayerTracking.HolderExtNumber = "1";
          _card_data.PlayerTracking.HolderFedEntity = 3;
          _card_data.PlayerTracking.HolderGender = 0;
          _card_data.PlayerTracking.HolderHasBeneficiary = false;
          _card_data.PlayerTracking.HolderId1 = "HID1";
          _card_data.PlayerTracking.HolderId1Type = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          _card_data.PlayerTracking.HolderId2 = "HID2";
          _card_data.PlayerTracking.HolderId2Type = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          _card_data.PlayerTracking.HolderId3 = "HID3";
          _card_data.PlayerTracking.HolderId3Type = "ID3TYPE";
          _card_data.PlayerTracking.HolderIsVIP = false;
          _card_data.PlayerTracking.HolderLevelEntered = new DateTime();
          _card_data.PlayerTracking.HolderLevelExpiration = new DateTime();
          _card_data.PlayerTracking.HolderLevelNotify = 1;
          _card_data.PlayerTracking.HolderMaritalStatus = 1;
          _card_data.PlayerTracking.HolderNationality = 3;
          _card_data.PlayerTracking.HolderOccupation = "OCCUPATION";
          _card_data.PlayerTracking.HolderOccupationId = 3;
          _card_data.PlayerTracking.HolderPhone01 = "666";
          _card_data.PlayerTracking.HolderPhone02 = "999";
          _card_data.PlayerTracking.HolderScannedDocs = new DocumentList();
          _card_data.PlayerTracking.HolderTwitter = "@TESTER";
          _card_data.PlayerTracking.HolderWeddingDate = new DateTime(2015, 10, 10);
          _card_data.PlayerTracking.HolderZip = "08ZIP";
          _card_data.PlayerTracking.LastAddedPoints = DateTime.Now;
          _card_data.PlayerTracking.LevelUpgradeDate = DateTime.Now;
          _card_data.PlayerTracking.Pin = "1234";
          _card_data.PlayerTracking.PinFailures = 1;
          _card_data.PlayerTracking.ShowCommentsOnCashier = false;
          _card_data.PlayerTracking.SubType = 1;
          _card_data.PlayerTracking.TruncatedPoints = 0;

          
          Assert.IsTrue(CardData.DB_UpdateDataCard(_card_data, new CashierSessionInfo(), false, out operid, _trx));
          //Add more fields
          _trx.Rollback();
        }
      }
    }
  }
}
