﻿using NUnit.Framework;
using LCDInTouch.Web.Classes;
using LCDInTouch.Web.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System;

namespace WSI.Common.Test.LCDInTouch
{
  [TestFixture]
  public class DrawControllerTest
  {
    IWebDriver _driver;

    [SetUp]
    public void Init()
    {
 
      var chromeService = ChromeDriverService.CreateDefaultService(@"C:\WebDriver\");
      var options = new ChromeOptions();

      options.AddArgument("--disable-extensions");
      options.AddArgument("no-sandbox");

      _driver = new ChromeDriver(chromeService, options);

    }

    [TearDown]
    public void Teardown()
    {
      _driver.Quit();
    }
    
    [Test]
    public void LCDDisplayTest_ShowWelcomeView()
    {
     
      _driver.Navigate().GoToUrl("http://localhost/LCDInTouch/");

      Thread.Sleep(2000);

      Assert.True(_driver.Title.Contains("Welcome"));
    }


    [Test]
    public void NoValidTest_LCDDisplayTest_ShowDataAccount01View()
    {
      //IWebElement _pin_button_one;
      //IWebElement _btn_ok;
     
      _driver.Navigate().GoToUrl("http://localhost/LCDInTouch/DataAccount/DataAccount01?IsTest=true");

      Thread.Sleep(2000);

      //if (CheckPinConfig())
      //{
      //  Boolean exists = _driver.FindElements(By.Id("1")).Count > 0;

      //  if (exists)
      //  {
      //    _pin_button_one = _driver.FindElement(By.Id("1"));
      //    _pin_button_one.Click();
      //    _pin_button_one.Click();
      //    _pin_button_one.Click();
      //    _pin_button_one.Click();

      //    _btn_ok = _driver.FindElement(By.Id("btn_ok"));
      //    _btn_ok.Click();
      //  }
      //}

      Thread.Sleep(2000);

      Assert.True(_driver.Title.Contains("DataAccount01"));
    }



    [Test]
    public void LCDDisplayTest_InsertValidCard()
    {
      _driver.Navigate().GoToUrl("http://localhost/LCDInTouch/Manager/Manager?FunctionCode=CARD_STATUS&Result=2&Data=1002305");

      Thread.Sleep(3000);
      
      Assert.True(_driver.Title.Contains("DataAccount"));
    }

    [Test]
    public void LCDDisplayTest_InsertNotValidCard()
    {
      _driver.Navigate().GoToUrl("http://localhost/LCDInTouch/Manager/Manager?FunctionCode=CARD_STATUS&Result=2&Data=");

      Thread.Sleep(3000);

      Assert.True(_driver.Title.Contains("CardError"));
    }

    //[Test]
    //public void LCDDisplayTest_BienvenidaDraw()
    //{

    //  IWebElement _ok_button;

    //  _driver.Navigate().GoToUrl("http://localhost/LCDInTouch/WelcomeDraw/WelcomeDrawStart");
      
    //  Thread.Sleep(3000);


    //  _ok_button = _driver.FindElement(By.Id("btn_ok_1"));
    //  _ok_button.Click();

      

    //  Assert.True(_driver.Title.Contains("WelcomeDraw"));

    //}
  }
}
