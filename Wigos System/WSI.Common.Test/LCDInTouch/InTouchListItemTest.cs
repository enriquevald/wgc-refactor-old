﻿using NUnit.Framework;
using WSI.Common.InTouch;
using Moq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;


namespace WSI.Common.Test.LCDInTouch
{

  [TestFixture]
  public class InTouchListItemTest 
  {

    private Mock<InTouchListItem> m_one_intouch_listitem = new Mock<InTouchListItem>();
    private Mock<InTouchListItem> m_two_intouch_listitem = new Mock<InTouchListItem>();

    private List<InTouchListItem> m_list_intouch_listitem = new List<InTouchListItem>();
    private InTouchListItem m_intouch = new InTouchListItem();

    [TestFixtureSetUp]
    public void Init(){

      m_one_intouch_listitem.Object.CreditsValue = 1;
      m_one_intouch_listitem.Object.EnableButton = "Boton 1";
      m_one_intouch_listitem.Object.ItemRedirect = "Redirect 1";
      m_one_intouch_listitem.Object.LevelValue = 20;
      m_one_intouch_listitem.Object.PrizeValue = "Value 1";
      m_one_intouch_listitem.Object.PromoGameId = 2;
      m_one_intouch_listitem.Object.PromoGameReturnUnitsId = 50;
      m_one_intouch_listitem.Object.PromoGameUrl = "http://wwww.testing.cat";
      m_one_intouch_listitem.Object.PromotionDate = WGDB.Now.ToString();
      m_one_intouch_listitem.Object.PromotionId = 1;
      m_one_intouch_listitem.Object.PromotionName = "Promotion 1";
      m_one_intouch_listitem.Object.PromotionServerDate = WGDB.Now;
      m_one_intouch_listitem.Object.PromotionType = 1;

      m_two_intouch_listitem.Object.CreditsValue = 2;
      m_two_intouch_listitem.Object.EnableButton = "Boton 2";
      m_two_intouch_listitem.Object.ItemRedirect = "Redirect 2";
      m_two_intouch_listitem.Object.LevelValue = 30;
      m_two_intouch_listitem.Object.PrizeValue = "Value 2";
      m_two_intouch_listitem.Object.PromoGameId = 3;
      m_two_intouch_listitem.Object.PromoGameReturnUnitsId = 60;
      m_two_intouch_listitem.Object.PromoGameUrl = "http://wwww.testing2.cat";
      m_two_intouch_listitem.Object.PromotionDate = WGDB.Now.ToString();
      m_two_intouch_listitem.Object.PromotionId = 2;
      m_two_intouch_listitem.Object.PromotionName = "Promotion 2";
      m_two_intouch_listitem.Object.PromotionServerDate = WGDB.Now;
      m_two_intouch_listitem.Object.PromotionType = 2;

      m_list_intouch_listitem.Add(m_one_intouch_listitem.Object);
      m_list_intouch_listitem.Add(m_two_intouch_listitem.Object);

    }

    [Test]
    public void IntouchTest_GetAllRewardsObject()
    {
      m_intouch = new InTouchListItem();
      List<InTouchListItem> _test;
      _test = new List<InTouchListItem>();
      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          _test = m_intouch.GetAllRewardItemList((SqlTransaction)_trx,1002305);

        }
        _con.Close();
      }
      Assert.IsTrue(_test.Count > 0);
    }

    [Test]
    public void IntouchTest_GetAllPointsObject()
    {
      m_intouch = new InTouchListItem();
      List<InTouchListItem> _test;
      _test = new List<InTouchListItem>();
      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
           _test = m_intouch.GetAllPointsItemList((SqlTransaction)_trx, "GIFTS.GI_POINTS_Level1");

        }
        _con.Close();
      }
      Assert.IsTrue(_test.Count > 0);
    }

    //[Test]
    //public void IntouchTest_GetObjectListByGameType()
    //{
    //  m_intouch = new InTouchPromoGame(m_one_promo_game.Object);
    //  List<InTouchPromoGame> _test;

    //  using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
    //  {
    //    _con.Open();
    //    using (var _trx = _con.BeginTransaction())
    //    {
    //      m_intouch.InsertPromoGame((SqlTransaction)_trx);
    //      _test = m_intouch.GetListInTouchPromoGameByType(InTouchPromoGame.GameType.PlayCash, (SqlTransaction)_trx);
          
    //      _trx.Rollback();
    //    }
    //    _con.Close();
    //  }

    //  Assert.IsTrue(_test.Count > 0);
    //  Assert.IsTrue(_test[0].Type == InTouchPromoGame.GameType.PlayCash);
    //}

    //[Test]
    //public void IntouchTest_GetObjectListByGameTypeReward()
    //{
    //  m_intouch = new InTouchPromoGame(m_two_promo_game.Object);
    //  List<InTouchPromoGame> _test;

    //  using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
    //  {
    //    _con.Open();
    //    using (var _trx = _con.BeginTransaction())
    //    {
    //      m_intouch.InsertPromoGame((SqlTransaction)_trx);
    //      _test = m_intouch.GetListInTouchPromoGameByType(InTouchPromoGame.GameType.PlayReward, (SqlTransaction)_trx);

    //      _trx.Rollback();
    //    }
    //    _con.Close();
    //  }

    //  Assert.IsTrue(_test.Count > 0);
    //  Assert.IsTrue(_test[0].Type == InTouchPromoGame.GameType.PlayReward);
    //}

    //[Test]
    //public void IntouchTest_UpdateObject()
    //{
    //  Boolean _is_updated;
      
    //  m_intouch = new InTouchPromoGame(m_one_promo_game.Object);
      
    //        using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
    //  {
    //    _con.Open();
    //    using (var _trx = _con.BeginTransaction())
    //    {
    //      m_list_promo_game = m_intouch.GetListInTouchPromoGame((SqlTransaction)_trx);

    //      m_one_promo_game.Object.Id = m_list_promo_game[0].Id;
    //      m_one_promo_game.Object.Name = "Updated test";

    //      m_intouch = new InTouchPromoGame(m_one_promo_game.Object);

    //      _is_updated = m_intouch.UpdatePromoGame((SqlTransaction)_trx);
    //      _trx.Rollback();
    //    }
    //    _con.Close();
    //  }

    //  Assert.IsTrue(_is_updated, "The Object is not Updated");
    //  Assert.AreNotEqual(m_intouch, m_list_promo_game[0]);
    //}


    //[Test]
    //public void IntouchTest_GetAllObject()
    //{
   
      
    //  m_intouch = new InTouchPromoGame(m_one_promo_game.Object);

    //  using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
    //  {
    //    _con.Open();
    //    using (var _trx = _con.BeginTransaction())
    //    {
    //      m_list_promo_game = m_intouch.GetListInTouchPromoGame((SqlTransaction)_trx);

    //      _trx.Rollback();

    //    }
    //    _con.Close();
    //  }

    //  Assert.IsTrue(m_list_promo_game.Count > 0);
    //}
 
  }
}
