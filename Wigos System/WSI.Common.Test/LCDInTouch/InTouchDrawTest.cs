﻿using System;
using NUnit.Framework;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common.Test.LCDInTouch
{
  [TestFixture]
  public class InTouchDrawTest
  {
    private String m_select_pending_play_draw_query;
    private String m_insert_pending_play_draw_query;
    private String m_sql_connection;

    [TestFixtureSetUp]
    public void Init()
    {
      m_select_pending_play_draw_query = "SELECT * FROM TERMINAL_DRAWS_RECHARGES WHERE TDR_ACCOUNT_ID = 9999999";
      m_insert_pending_play_draw_query = "INSERT INTO TERMINAL_DRAWS_RECHARGES VALUES (9999999, 0, 100, 80, NULL, NULL, 9999999, 99)";
      m_sql_connection                 = @"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_000;user=sa;password=ManagerU00;";
    }

    [Test]
    public void IntouchTest_HasPendingPlayDraws()
    {
      Boolean _has_pending_draws;

      using (IDbConnection _con = new SqlConnection(m_sql_connection))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          using (SqlCommand _cmd = new SqlCommand(m_insert_pending_play_draw_query, ((SqlTransaction)_trx).Connection, (SqlTransaction)_trx))
          {
            _has_pending_draws = _cmd.ExecuteNonQuery() == 1;                       
          }
          _trx.Rollback();
        }
        _con.Close();
      }

      Assert.IsTrue(_has_pending_draws);
    }

    [Test]
    public void IntouchTest_DontHasPendingPlayDraws()
    {
      Boolean _dont_has_pending_draws;

      using (IDbConnection _con = new SqlConnection(m_sql_connection))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {      
          using (SqlCommand _cmd = new SqlCommand(m_select_pending_play_draw_query, ((SqlTransaction)_trx).Connection, (SqlTransaction)_trx))
          {
            using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
            {
              _dont_has_pending_draws = _sql_reader.Read();
            }
          }
        }
        _con.Close();
      }

      Assert.IsFalse(_dont_has_pending_draws);
    }

  }
}
