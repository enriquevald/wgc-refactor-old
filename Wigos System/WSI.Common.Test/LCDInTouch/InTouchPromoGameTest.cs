﻿using System;
using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;


namespace WSI.Common.Test.LCDInTouch
{

  [TestFixture]
  public class InTouchPromoGameTest 
  {

    private Mock<PromoGame> m_one_promo_game = new Mock<PromoGame>();
    private Mock<PromoGame> m_two_promo_game = new Mock<PromoGame>();
    private Mock<PromoGame> m_third_promo_game = new Mock<PromoGame>();
    private List<PromoGame> m_list_promo_game = new List<PromoGame>();
    private PromoGame m_intouch = new PromoGame();

    [TestFixtureSetUp]
    public void Init(){
      
      m_one_promo_game.Object.Id = 1;
      m_one_promo_game.Object.Name = "Test 1";
      m_one_promo_game.Object.Type = PromoGame.GameType.PlayCash;
      m_one_promo_game.Object.Price = 1000;
      m_one_promo_game.Object.PriceUnits = WSI.Common.PrizeType.Redeemable;
      m_one_promo_game.Object.ReturnPrice = false;
      m_one_promo_game.Object.ReturnUnits = WSI.Common.PrizeType.NoRedeemable;
      m_one_promo_game.Object.PercentatgeCostReturn = 5;
      m_one_promo_game.Object.GameURL = "http://wwww.testing.cat";
      m_one_promo_game.Object.MandatoryIC = false;
      m_one_promo_game.Object.ShowBuyDialog = true;
      m_one_promo_game.Object.PlayerCanCancel = true;
      m_one_promo_game.Object.AutoCancel = 0;
      m_one_promo_game.Object.TransferScreen = true;
      m_one_promo_game.Object.TransferTimeOut = 0;
      m_one_promo_game.Object.LastModification =WGDB.Now;
      m_one_promo_game.Object.LastUpdate = WGDB.Now;

      m_two_promo_game.Object.Id = 2;
      m_two_promo_game.Object.Name = "Test 2";
      m_two_promo_game.Object.Type = PromoGame.GameType.PlayReward;
      m_two_promo_game.Object.Price = 50;
      m_two_promo_game.Object.PriceUnits = WSI.Common.PrizeType.Redeemable;
      m_two_promo_game.Object.ReturnPrice = true;
      m_two_promo_game.Object.ReturnUnits = WSI.Common.PrizeType.Redeemable;
      m_two_promo_game.Object.PercentatgeCostReturn = 2;
      m_two_promo_game.Object.GameURL = "http://wwww.testing2.cat";
      m_two_promo_game.Object.MandatoryIC = true;
      m_two_promo_game.Object.ShowBuyDialog = true;
      m_two_promo_game.Object.PlayerCanCancel = true;
      m_two_promo_game.Object.AutoCancel = 1;
      m_two_promo_game.Object.TransferScreen = true;
      m_two_promo_game.Object.TransferTimeOut = 5;
      m_two_promo_game.Object.LastModification = WGDB.Now;
      m_two_promo_game.Object.LastUpdate = WGDB.Now;

      m_third_promo_game.Object.Id = It.IsAny<Int64>();
      m_third_promo_game.Object.Name = It.IsAny<String>();
      m_third_promo_game.Object.Type = (PromoGame.GameType)It.IsAny<Int64>();
      m_third_promo_game.Object.Price = It.IsAny<Decimal>();
      m_third_promo_game.Object.PriceUnits = (WSI.Common.PrizeType)It.IsAny<Int32>();
      m_third_promo_game.Object.ReturnPrice = It.IsAny<Boolean>();
      m_third_promo_game.Object.ReturnUnits = (WSI.Common.PrizeType)It.IsAny<Int32>();
      m_third_promo_game.Object.PercentatgeCostReturn = It.IsAny<Decimal>();
      m_third_promo_game.Object.GameURL = It.IsAny<String>();
      m_third_promo_game.Object.MandatoryIC = It.IsAny<Boolean>();
      m_third_promo_game.Object.ShowBuyDialog = It.IsAny<Boolean>();
      m_third_promo_game.Object.PlayerCanCancel = It.IsAny<Boolean>();
      m_third_promo_game.Object.AutoCancel = It.IsAny<Int32>();
      m_third_promo_game.Object.TransferScreen = It.IsAny<Boolean>();
      m_third_promo_game.Object.TransferTimeOut = It.IsAny<Int32>();
      m_third_promo_game.Object.LastModification = It.IsAny<DateTime>();

      m_list_promo_game.Add(m_one_promo_game.Object);
      m_list_promo_game.Add(m_two_promo_game.Object);

    }

    [Test]
    public void IntouchTest_InsertObject()
    {
      Boolean _is_inserted;
      m_intouch = new PromoGame(m_one_promo_game.Object);

     using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))

      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          _is_inserted = m_intouch.InsertPromoGame((SqlTransaction) _trx );
          
          _trx.Rollback();

        }
        _con.Close();
      }
      
      Assert.IsTrue(_is_inserted);
    }

    [Test]
    public void IntouchTest_ErrorOnInsertObject()
    {
      Boolean _is_inserted;
      m_intouch = new PromoGame(m_third_promo_game.Object);

      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          _is_inserted = m_intouch.InsertPromoGame((SqlTransaction)_trx);

          _trx.Rollback();

        }
        _con.Close();
      }

      Assert.IsFalse(_is_inserted);
    }

    [Test]
    public void IntouchTest_GetObjectById()
    {
      Boolean _is_inserted;
      m_intouch = new PromoGame(m_one_promo_game.Object);
      PromoGame _test;

      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          _is_inserted = m_intouch.InsertPromoGame((SqlTransaction)_trx);

           _test = m_intouch.GetPromoGameById(m_one_promo_game.Object.Id);


          _trx.Rollback();

        }
        _con.Close();
      }

      Assert.IsNotNull(_test);
    }

    [Test]
    public void IntouchTest_GetObjectListByGameType()
    {
      m_intouch = new PromoGame(m_one_promo_game.Object);
      List<PromoGame> _test;

      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          m_intouch.InsertPromoGame((SqlTransaction)_trx);
          _test = new List<PromoGame>(); //m_intouch.GetListInTouchPromoGameByType(InTouchPromoGame.GameType.PlayCash, 0, (SqlTransaction)_trx);
          
          _trx.Rollback();
        }
        _con.Close();
      }

      Assert.IsTrue(_test.Count > 0);
      Assert.IsTrue(_test[0].Type == PromoGame.GameType.PlayCash);
    }

    [Test]
    public void IntouchTest_GetObjectListByGameTypeReward()
    {
      m_intouch = new PromoGame(m_two_promo_game.Object);
      List<PromoGame> _test;

      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          m_intouch.InsertPromoGame((SqlTransaction)_trx);
          _test = new List<PromoGame>();//m_intouch.GetListInTouchPromoGameByType(InTouchPromoGame.GameType.PlayReward, 0, (SqlTransaction)_trx);

          _trx.Rollback();
        }
        _con.Close();
      }

      Assert.IsTrue(_test.Count > 0);
      Assert.IsTrue(_test[0].Type == PromoGame.GameType.PlayReward);
    }

    [Test]
    public void IntouchTest_UpdateObject()
    {
      Boolean _is_updated;
      
      m_intouch = new PromoGame(m_one_promo_game.Object);
      
            using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          m_list_promo_game = m_intouch.GetListPromoGame((SqlTransaction)_trx);

          m_one_promo_game.Object.Id = m_list_promo_game[0].Id;
          m_one_promo_game.Object.Name = "Updated test";

          m_intouch = new PromoGame(m_one_promo_game.Object);

          _is_updated = m_intouch.UpdatePromoGame((SqlTransaction)_trx);
          _trx.Rollback();
        }
        _con.Close();
      }

      Assert.IsTrue(_is_updated, "The Object is not Updated");
      Assert.AreNotEqual(m_intouch, m_list_promo_game[0]);
    }


    [Test]
    public void IntouchTest_GetAllObject()
    {
   
      
      m_intouch = new PromoGame(m_one_promo_game.Object);

      using (IDbConnection _con = new SqlConnection(@"Data Source=VSCAGE\UNRANKED;Initial Catalog=wgdb_306;user=sa;password=ManagerU00;"))
      {
        _con.Open();
        using (var _trx = _con.BeginTransaction())
        {
          m_list_promo_game = m_intouch.GetListPromoGame((SqlTransaction)_trx);

          _trx.Rollback();

        }
        _con.Close();
      }

      Assert.IsTrue(m_list_promo_game.Count > 0);
    }
 
  }
}
