//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WXP_Listener.cs
// 
//   DESCRIPTION: Procedures for listener peru frames
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 07-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUL-2012 ACC    First release 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Configuration;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;

namespace WSI.WXP_Listener
{
  public static class WXP_Listener
  {
    // ----------------------- 1234567890123456
    static String LOGIN_USR = "uSeRNaMe0";
    static String LOGIN_PWD = "PaSWRD";
    const String DELIMITER = "|";
    // ----------------------- 1234567890123456
    static String LOGIN_KEY = "Login.Key=123456";
    static String LOGIN__IV = "Login.IV=1234567";
    static String DATA__KEY = "Data.Key=1234567";
    static String DATA___IV = "Data.IV=12345678";


    static RijndaelManaged m_aes;
    static ICryptoTransform m_aes_decryptor;


    static public void Init(int PortNumber,
                            String UserName, String Password,
                            String LoginKey, String Login_IV,
                            String DataKey, String DataIV)
    {
      LOGIN_USR = UserName.PadRight(9, '0').Substring(0, 9);
      LOGIN_PWD = Password.PadRight(6, '0').Substring(0, 6);
      LOGIN_KEY = LoginKey.PadRight(16, '0').Substring(0, 16);
      LOGIN__IV = Login_IV.PadRight(16, '0').Substring(0, 16);
      DATA__KEY = DataKey.PadRight(16, '0').Substring(0, 16);
      DATA___IV = DataIV.PadRight(16, '0').Substring(0, 16);

      Console.Error.WriteLine("WXP_Listener");
      Console.Error.WriteLine();
      Console.Error.WriteLine("- Login:");
      Console.Error.WriteLine("  - Username: " + LOGIN_USR);
      Console.Error.WriteLine("  - Password: " + LOGIN_PWD);
      Console.Error.WriteLine("  - Key:      " + LOGIN_KEY);
      Console.Error.WriteLine("  - IV:       " + LOGIN__IV);
      Console.Error.WriteLine();
      Console.Error.WriteLine("- Data:");
      Console.Error.WriteLine("  - Key:      " + DATA__KEY);
      Console.Error.WriteLine("  - IV:       " + DATA___IV);
      Console.Error.WriteLine();
      Console.Error.WriteLine();
      Console.Error.WriteLine("- Listening on port: " + PortNumber.ToString());
      Console.Error.WriteLine();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes the decryptor
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void InitCipher(String Key, String IV)
    {
      m_aes = (RijndaelManaged)RijndaelManaged.Create();
      m_aes.KeySize = 128;
      m_aes.BlockSize = 128;
      m_aes.Mode = CipherMode.CBC;
      m_aes.Padding = PaddingMode.PKCS7;
      m_aes_decryptor = m_aes.CreateDecryptor(Encoding.UTF8.GetBytes(Key), Encoding.UTF8.GetBytes(IV));
    }

    static private Byte[] DecryptFrame(Byte[] Buffer)
    {
      MemoryStream _ms;
      Byte[] _buffer;
      Byte[] _frame;
      int _num_read;

      _ms = new MemoryStream(Buffer);
      _buffer = new byte[256];
      using (CryptoStream _cs = new CryptoStream(_ms, m_aes_decryptor, CryptoStreamMode.Read))
      {
        _num_read = _cs.Read(_buffer, 0, _buffer.Length);
        _frame = new Byte[_num_read];
        Array.Copy(_buffer, 0, _frame, 0, _frame.Length);
        return _frame;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Listen on the given port
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Listen(int PortNumber)
    {
      TcpListener _listener;
      Byte [] _buffer;
      Byte[] _receive_buffer;
      Byte[] _frame;
      int _len;
      Boolean _authenticated;
      String _login;
      String _expected_login;

      _receive_buffer = new Byte[128];

      _expected_login = LOGIN_USR + DELIMITER + LOGIN_PWD;

      try 
      {
        _listener = new TcpListener(IPAddress.Any, PortNumber);
        _listener.Start();
      }
      catch (Exception _ex)
      {
        Console.Error.WriteLine(_ex.ToString());

        return;
      }

      while (true)
      {
        try 
        {
          using (TcpClient _client = _listener.AcceptTcpClient())
          {
            Console.Error.WriteLine("Client connected from: " + _client.Client.RemoteEndPoint.ToString());

            _authenticated = false;

            InitCipher(LOGIN_KEY, LOGIN__IV);
            
            while (_client.Connected)
            {
              try
              {
                _len = _client.Client.Receive(_receive_buffer);
                if (_len <= 0)
                {
                  break;
                }
                _buffer = new Byte[_len];
                Array.Copy(_receive_buffer, 0, _buffer, 0, _buffer.Length);
                _frame = DecryptFrame(_buffer);

                // Authentication
                if (!_authenticated)
                {
                  _login = Encoding.UTF8.GetString(_frame);

                  if (_login == _expected_login)
                  {
                    _login = LOGIN_USR;
                    _buffer = Encoding.UTF8.GetBytes(_login);
                    _client.Client.Send(_buffer);

                    _authenticated = true;

                    Console.Error.WriteLine("Client Authenticated!");

                    InitCipher(DATA__KEY, DATA___IV);
                  }
                  else
                  {
                    DateTime _now;

                    _now = DateTime.Now;

                    _login = _now.Day.ToString("00") + _now.Month.ToString("00") + (_now.Year % 100).ToString("00")
                           + _now.Hour.ToString("00") +_now.Minute.ToString("00") + _now.Second.ToString("00") + _now.Millisecond.ToString("000");
                    _login += DELIMITER;
                    _login += "0";
                    _buffer = Encoding.UTF8.GetBytes(_login);
                    _client.Client.Send(_buffer);

                    Console.Error.WriteLine("Authentication failed: " + _login + ", Expected: " + _expected_login);

                    try { _client.Client.Close(); }
                    catch { }
                    try { _client.Close(); }
                    catch { }
                  }

                  continue;
                }
                else
                {
                  DateTime _now;

                  _now = DateTime.Now;

                  _login = _now.Day.ToString("00") + _now.Month.ToString("00") + (_now.Year % 100).ToString("00")
                         + _now.Hour.ToString("00") + _now.Minute.ToString("00") + _now.Second.ToString("00") + _now.Millisecond.ToString("000");
                  _login += DELIMITER;
                  _login += _len.ToString();
                  _buffer = Encoding.UTF8.GetBytes(_login);
                  _client.Client.Send(_buffer);
                }

                // Crypted Frame Received!
                Console.Out.WriteLine("TRAMA:");
                Console.Out.WriteLine(BitConverter.ToString(_frame).Replace("-", ""));
                Console.Out.WriteLine();
                Console.Out.WriteLine(PeruFrameViewer.ToString(_frame, true, true));
                Console.Out.WriteLine();
              }
              catch
              { }
            }
            Console.Error.WriteLine("Client disconnected!");
          }
        }
        catch (Exception _ex)
        {        
          Console.Error.WriteLine(_ex.ToString());
        }
      }
    }
  }
}
