using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WSI.WXP_Listener
{
  class Program
  {
    static void Main(string[] args)
    {
      int _port;

      if (args.Length != 7)
      {
        Console.Out.WriteLine("Usage:");
        Console.Out.WriteLine(" WXP_Listener PortNumber UserName Password LoginKey LoginIV DataKey DataIV");
        Console.Out.WriteLine();

        Environment.Exit(0);
      }

      int.TryParse(args[0], out _port);
      WXP_Listener.Init(_port, args[1], args[2], args[3], args[4], args[5], args[6]);
      WXP_Listener.Listen(_port);
    }
  }
}
