using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WXP_Listener
{
  //------------------------------------------------------------------------------
  // PURPOSE: Class to create Frame Item
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  class PeruFrameItem
  {
    internal String m_contenido;
    internal String m_abreviatura;
    internal int m_longitud;

    //------------------------------------------------------------------------------
    // PURPOSE: PeruFrameItem
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Contenido
    //          - Abreviatura
    //          - Longitud
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal PeruFrameItem(String Contenido, String Abreviatura, int Longitud)
    {
      m_contenido = Contenido;
      m_abreviatura = Abreviatura;
      m_longitud = Longitud;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: PeruFrameItem
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Contenido
    //          - Longitud
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal PeruFrameItem(String Contenido, int Longitud)
    {
      m_contenido = Contenido;
      m_abreviatura = "";
      m_longitud = Longitud;
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to create Frame Item List
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  class PeruFrameItemList : List<PeruFrameItem>
  {
    private int m_header_length = 0;

    //------------------------------------------------------------------------------
    // PURPOSE: HeaderEnd
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void HeaderEnd()
    {
      m_header_length = 0;
      foreach (PeruFrameItem _item in this)
      {
        m_header_length += _item.m_longitud;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: HeaderLength
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public int HeaderLength
    {
      get
      {
        return m_header_length;
      }
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage Frames
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public class PeruFrameViewer
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Economical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Economical Frame
    //
    private static PeruFrameItemList EFrame()
    {
      PeruFrameItemList _frame = new PeruFrameItemList();

      _frame.Add(new PeruFrameItem("Cabecera", "E", 1));
      _frame.Add(new PeruFrameItem("Check de Redundancia C�clica", "CRC", 2));
      _frame.Add(new PeruFrameItem("Registro de Sala DGJCMT", "RSDGJCMT", 9));
      _frame.Add(new PeruFrameItem("Registro M�quina DGJCMT", "RMT", 8));
      _frame.Add(new PeruFrameItem("FECHA", 12));
      _frame.HeaderEnd();

      // Datos
      _frame.Add(new PeruFrameItem("ID Moneda (1=soles, 2=d�lares)", 1));
      _frame.Add(new PeruFrameItem("Denominaci�n", "D", 4));
      _frame.Add(new PeruFrameItem("COIN IN FINAL", "CIF", 10));
      _frame.Add(new PeruFrameItem("COIN OUT FINAL", "COF", 10));
      _frame.Add(new PeruFrameItem("PMA FINAL", "PMF", 10));
      _frame.Add(new PeruFrameItem("OTRO CONTADOR FINAL", "OPF", 10));
      _frame.Add(new PeruFrameItem("TIPO DE CAMBIO", "TC", 5));
      _frame.Add(new PeruFrameItem("Reserva 1", 10));

      return _frame;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Technical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Technical Frame
    //
    private static PeruFrameItemList TFrame()
    {
      PeruFrameItemList _frame = new PeruFrameItemList();

      _frame.Add(new PeruFrameItem("Cabecera", "T", 1));
      _frame.Add(new PeruFrameItem("Check de Redundancia C�clica", "CRC", 2));
      _frame.Add(new PeruFrameItem("Registro de Sala DGJCMT", "RSDGJCMT", 9));
      _frame.Add(new PeruFrameItem("Registro M�quina DGJCMT", "RMT", 8));
      _frame.Add(new PeruFrameItem("ID Colector", 12));
      _frame.Add(new PeruFrameItem("FECHA", 12));
      _frame.HeaderEnd();

      // Datos
      _frame.Add(new PeruFrameItem("P�rdida Comunicaci�n entre Colector y M�quina", 1));
      _frame.Add(new PeruFrameItem("P�rdida Comunicaci�n entre Colector y Sala", 1));
      _frame.Add(new PeruFrameItem("Restauraci�n por cortes de energ�a", 1));
      _frame.Add(new PeruFrameItem("Error en M�quina por Bater�a Baja en RAM", 1));
      _frame.Add(new PeruFrameItem("Apertura de Puerta de Caja L�gica", 1));
      _frame.Add(new PeruFrameItem("Falla de Memoria del Colector", 1));
      _frame.Add(new PeruFrameItem("Falla de la RAM de la M�quina", 1));
      _frame.Add(new PeruFrameItem("Cereo (borrado del contenido de la memoria)", 1));
      _frame.Add(new PeruFrameItem("Reserva 1", 1));
      _frame.Add(new PeruFrameItem("Reserva 2", 1));

      return _frame;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: ToBase16
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in Hexa
    //
    private static String ToBase16(Byte[] Item, int Start, int Count)
    {
      return BitConverter.ToString(Item, Start, Count).Replace("-", " ");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: ToBase02
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in Binary
    //
    private static String ToBase02(Byte[] Item, int Start, int Count)
    {
      String _base16;
      String[] _bits = new String[] { "0000", "0001", "0010", "0011",
                                      "0100", "0101", "0110", "0111",
                                      "1000", "1001", "1010", "1011",
                                      "1100", "1101", "1110", "1111"};
      Byte[] _nibbles;
      Byte _zero;
      Byte _character_A;
      StringBuilder _sb;

      _base16 = ToBase16(Item, Start, Count);
      _nibbles = Encoding.ASCII.GetBytes(_base16.Replace(" ", ""));
      _zero = Encoding.ASCII.GetBytes("0")[0];
      _character_A = Encoding.ASCII.GetBytes("A")[0];

      _sb = new StringBuilder();
      int _idx_nibble;
      _idx_nibble = 0;
      foreach (Byte _nibble in _nibbles)
      {
        int _idx;

        _idx = _nibble - _zero;
        if (_idx >= 0 && _idx < 10)
        {
          _sb.Append(_bits[_idx]);
          _idx_nibble++;
        }

        _idx = _nibble - _character_A + 10;
        if (_idx >= 10 && _idx < 16)
        {
          _sb.Append(_bits[_idx]);
          _idx_nibble++;
        }

        if (_idx_nibble % 2 == 0)
        {
          _sb.Append(" ");
        }
      }

      return _sb.ToString().TrimEnd();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Data from Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Frame Data
    //
    private static Byte[] Data(Byte[] Frame)
    {
      PeruFrameItemList _items;

      if (Frame[0] == 'E')
      {
        _items = EFrame();
      }
      else if (Frame[0] == 'T')
      {
        _items = TFrame();
      }
      else
      {
        return new Byte[0];
      }

      if (Frame.Length < _items.HeaderLength)
      {
        return new Byte[0];
      }

      Byte[] _data;

      _data = new Byte[Frame.Length - _items.HeaderLength];

      Array.Copy(Frame, _items.HeaderLength, _data, 0, _data.Length);

      return _data;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Frame to String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String Frame
    //
    public static String ToString(Byte[] Frame, Boolean Hexadecimal, Boolean Binary)
    {
      PeruFrameItemList _items;
      StringBuilder _sb;

      int _idx_read;

      _sb = new StringBuilder();

      if (Frame[0] == 'E')
      {
        _items = EFrame();
      }
      else if (Frame[0] == 'T')
      {
        _items = TFrame();
      }
      else
      {
        _sb.AppendLine(ToBase16(Frame, 0, Frame.Length));

        return _sb.ToString();
      }

      _idx_read = 0;
      foreach (PeruFrameItem _item in _items)
      {
        String _text;
        String _b16;
        String _b02;


        _text = Encoding.ASCII.GetString(Frame, _idx_read, _item.m_longitud);
        _b16 = ToBase16(Frame, _idx_read, _item.m_longitud);
        _b02 = ToBase02(Frame, _idx_read, _item.m_longitud);
        _idx_read += _item.m_longitud;

        if (_item.m_abreviatura == "CRC")
        {
          _text = "[" + _b16.Replace(" ", "") + "]";
        }

        _sb.Append(_item.m_contenido.PadRight(50, ' ') + " " + _item.m_abreviatura.PadRight(10, ' ') + " L" + _item.m_longitud.ToString("00") + "     " + _text.PadRight(12));
        if (Hexadecimal) _sb.Append(" [Hexa: " + _b16.Replace(" ", "").PadRight(24) + "]");
        if (Binary) _sb.Append(" [Bin: " + _b02 + "]");
        _sb.AppendLine();
      }

      return _sb.ToString();
    }
  }


}
