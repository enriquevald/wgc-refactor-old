using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net;

namespace WSI.IP_3GS
{
  class Program
  {
    static Boolean Ping(IPAddress IP)
    {
      using (Process _process = new Process())
      {
        String _out;
        String[] _lines;
        int _num_replies;

        _num_replies = 0;

        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.FileName = "PING";
        _process.StartInfo.Arguments = "-n 1 -w 1000 -4 " + IP.ToString();
        _process.Start();
        _out = _process.StandardOutput.ReadToEnd();
        _process.WaitForExit();

        _lines = _out.Split(new String[] { Environment.NewLine }, StringSplitOptions.None);

        foreach (String _line in _lines)
        {
          if (_line.Contains(IP.ToString()) && _line.Contains("bytes="))
          {
            _num_replies++;
          }
        }

        if (_num_replies > 0)
        {
          return true;
        }

        return false;
      }
    }

    static void InterfaceAddIp(String Interface, IPAddress IP, IPAddress SubnetMask)
    {
      using (Process _process = new Process())
      {
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.FileName = "NETSH";
        _process.StartInfo.Arguments = "INTERFACE IP ADD ADDRESS " + Interface + " " + IP.ToString() + " " + SubnetMask.ToString();
        _process.Start();
        String _out = _process.StandardOutput.ReadToEnd();
        _process.WaitForExit();
      }
    }

    static void InterfaceDelIp(String Interface, IPAddress IP)
    {
      using (Process _process = new Process())
      {
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.FileName = "NETSH";
        _process.StartInfo.Arguments = "INTERFACE IP DEL ADDRESS " + Interface + " " + IP.ToString();
        _process.Start();
        String _out = _process.StandardOutput.ReadToEnd();
        _process.WaitForExit();
      }
    }

    static Boolean SeekInterface(IPAddress IPPattern, out String Interface, out IPAddress ServerIP)
    {
      Boolean _new_interface;
      String _interface;
      int _idx0;
      int _idx1;
      IPAddress _ipa;
      String _aux;
      String[] _lines;
      
      Interface = "";
      ServerIP = IPAddress.None;

      _interface = "";

      using (Process _process = new Process())
      {
        String _out;
        
        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.FileName = "NETSH";
        _process.StartInfo.Arguments = "INT IP SHOW ADDRESSES";
        _process.Start();
        _out = _process.StandardOutput.ReadToEnd();
        _process.WaitForExit();

        _lines = _out.Split(new String[] { Environment.NewLine }, StringSplitOptions.None);
      }

      if ( _lines == null )
      {
        return false;
      }

      _new_interface = true;
      foreach (String _line in _lines)
      {
        // Empty line ...
        if (String.IsNullOrEmpty(_line))
        {
          _new_interface = true;
          continue;
        }

        if (_new_interface)
        {
          // Get the Interface name
          _idx0 = _line.IndexOf('"');
          _idx1 = _line.LastIndexOf('"');

          if (_idx0 >= 0 && _idx1 > _idx0)
          {
            _interface = _line.Substring(_idx0, _idx1 - _idx0 + 1);
            _new_interface = false;
          }
          continue;
        } // if ( _new_interface )


        if (!_line.Contains("IP"))
        {
          continue;
        }
        _idx0 = _line.IndexOf(':');
        if (_idx0 < 0)
        {
          continue;
        }

        // The line contains an IP ...
        _aux = _line.Substring(_idx0 + 1);
        _aux = _aux.Trim();
        if (!IPAddress.TryParse(_aux, out _ipa))
        {
          continue;
        }

        if (IPMatch(IPPattern, _ipa))
        {
          // Server IP matches pattern
          Interface = _interface;
          ServerIP = _ipa;

          return true;
        }
      }

      return false;
    }

    static Boolean ExistsIP(IPAddress IP, out Boolean Duplicated)
    {
      String[] _lines;

      Duplicated = false;

      using (Process _process = new Process())
      {
        String _out;

        _process.StartInfo.UseShellExecute = false;
        _process.StartInfo.RedirectStandardOutput = true;
        _process.StartInfo.RedirectStandardError = true;
        _process.StartInfo.CreateNoWindow = true;
        _process.StartInfo.FileName = "NETSH";
        _process.StartInfo.Arguments = "INTERFACE IP SHOW IPADDRESSES";
        _process.Start();
        _out = _process.StandardOutput.ReadToEnd();
        _process.WaitForExit();

        _lines = _out.Split(new String[] {Environment.NewLine}, StringSplitOptions.None);

        foreach (String _line in _lines)
        {
          if (_line.Contains(IP.ToString()))
          {
            // Search for string "Duplica".
            // In english OS version, it is "Duplicated". In Spanish, it is "Duplicado".
            if (_line.Contains("Duplica"))
            {
              Duplicated = true;
            }

            return true;
          }
        }
      }

      return false;
    }

    static Boolean IPMatch(IPAddress Pattern, IPAddress IP)
    {
      Byte[] _pattern;
      Byte[] _ip;
      IPAddress _masked;

      _pattern = Pattern.GetAddressBytes();
      _ip = IP.GetAddressBytes();

      if (_pattern.Length != _ip.Length)
      {
        return false;
      }

      for (int _idx = 0; _idx < _ip.Length; _idx++)
      {
        if ( _pattern[_idx] == 255 )
        {
          _ip[_idx] &= _pattern[_idx];
        }
        else
        {
          if ( _ip[_idx] != _pattern[_idx] )
          {
            return false;
          }
        }
      }

      _masked = new IPAddress(_ip);

      return (_masked.ToString() == IP.ToString());
    }

    static void Main(string[] args)
    {
      String _interface;
      IPAddress _ip_pattern;
      IPAddress _server_ip;
      IPAddress _3gs_ip;
      IPAddress _3gs_subnet;
      int _num_tries;
      Boolean _duplicated;
      String _cmd;
      Boolean _show_help;

      _cmd = "";
      _show_help = true;

      if (args.Length == 1)
      {
        _cmd = args[0].ToUpper();

        if (_cmd.Equals("ENABLE") || _cmd.Equals("DISABLE"))
        {
          _show_help = false;
        }
      }
      
      if (_show_help)
      {
        // Help
        Console.WriteLine("WSI.IP_3GS Usage:");
        Console.WriteLine("   WSI.IP_3GS <cmd>");
        Console.WriteLine("cmd: ");
        Console.WriteLine("  - Enable:  enables  the current server to receive incoming 3GS requests.");
        Console.WriteLine("  - Disable: disables the current server to receive incoming 3GS requests.");
        Console.WriteLine(" ");

        return;
      }

      // Seek Server IP
      _ip_pattern = IPAddress.Parse("10.255.1.255");
      if (SeekInterface(_ip_pattern, out _interface, out _server_ip))
      {
        Byte[] _ip_byte;

        _ip_byte = _server_ip.GetAddressBytes();

        _ip_byte[0] = 10;           // 10
        _ip_byte[1] = _ip_byte[1];  // Site 
        _ip_byte[2] = 3;            // 3
        _ip_byte[3] = 1;            // 1

        _3gs_ip = new IPAddress(_ip_byte);
        _3gs_subnet = IPAddress.Parse("255.255.255.0");
      }
      else
      {
        return;
      }

      if (_cmd.Equals("DISABLE"))
      {
        // Remove the 3GS IP
        InterfaceDelIp(_interface, _3gs_ip);

        return;
      }

      // Enable IP

      // Seek 3GS IP: 10.<Site>.3.1
      if (ExistsIP(_3gs_ip, out _duplicated))
      {
        if (_duplicated)
        {
          // Remove the duplicated 3GS IP. Have to add it again to enable it properly.
          InterfaceDelIp(_interface, _3gs_ip);
        }
        else
        {
          // Already exists
          return;
        }
      }

      _num_tries = 30;
      while (_num_tries > 0)
      {
        _num_tries--;

        if (Ping(_3gs_ip))
        {
          if (_num_tries > 0)
          {
            System.Threading.Thread.Sleep(1000);
          }
          continue;
        }

        InterfaceAddIp(_interface, _3gs_ip, _3gs_subnet);

        return;
      }
    }
  }
}
