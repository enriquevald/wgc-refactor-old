﻿namespace DTSXDeployment
{
  partial class frm_dtsx_deployment_main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.btn_deploy_packages = new System.Windows.Forms.Button();
      this.clbDTSX = new System.Windows.Forms.CheckedListBox();
      this.gbSelectAndDeploy = new System.Windows.Forms.GroupBox();
      this.btn_PkgVersion = new System.Windows.Forms.Button();
      this.fileBrowser = new System.Windows.Forms.OpenFileDialog();
      this.txt_folder = new System.Windows.Forms.TextBox();
      this.gbFolderBrowser = new System.Windows.Forms.GroupBox();
      this.rtbResults = new System.Windows.Forms.RichTextBox();
      this.gbDTSInfo = new System.Windows.Forms.GroupBox();
      this.gbSSISServerConnection = new System.Windows.Forms.GroupBox();
      this.gbSSISServerConnectionString = new System.Windows.Forms.GroupBox();
      this.txtSSISServerConnectionString = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.chkExecutePackage = new System.Windows.Forms.CheckBox();
      this.gbConnsStatus = new System.Windows.Forms.GroupBox();
      this.lblStatusSSIS = new System.Windows.Forms.Label();
      this.lblStatusDestination = new System.Windows.Forms.Label();
      this.lblStatusSource = new System.Windows.Forms.Label();
      this.btnTest = new System.Windows.Forms.Button();
      this.gbConnectionStrings = new System.Windows.Forms.GroupBox();
      this.txt_path_xml = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.gbIntegrationServicesServer = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.txtServerName = new System.Windows.Forms.TextBox();
      this.lbl_SSIS_Server_Info = new System.Windows.Forms.Label();
      this.btnGetServerInfo = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.rbFileSystem = new System.Windows.Forms.RadioButton();
      this.rbMSDB = new System.Windows.Forms.RadioButton();
      this.gbConfigSettings = new System.Windows.Forms.GroupBox();
      this.chkPasswordPackage = new System.Windows.Forms.CheckBox();
      this.gbScheduling = new System.Windows.Forms.GroupBox();
      this.txt_schedule = new System.Windows.Forms.MaskedTextBox();
      this.cbScheduling = new System.Windows.Forms.ComboBox();
      this.lblPackagePwd = new System.Windows.Forms.Label();
      this.txtPackagePassword = new System.Windows.Forms.TextBox();
      this.chkCreateJob = new System.Windows.Forms.CheckBox();
      this.chkRunJobOnCreate = new System.Windows.Forms.CheckBox();
      this.epError = new System.Windows.Forms.ErrorProvider(this.components);
      this.statusBar = new System.Windows.Forms.StatusStrip();
      this.txtInfoStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.tooltipControl = new System.Windows.Forms.ToolTip(this.components);
      this.gbOutputs = new System.Windows.Forms.GroupBox();
      this.tcTabs = new System.Windows.Forms.TabControl();
      this.tpOutputs_1 = new System.Windows.Forms.TabPage();
      this.tpOutputs_2 = new System.Windows.Forms.TabPage();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.dgvJobs_1 = new System.Windows.Forms.DataGridView();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.dgvJobs_2 = new System.Windows.Forms.DataGridView();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.dgvJobs_3 = new System.Windows.Forms.DataGridView();
      this.tabPage4 = new System.Windows.Forms.TabPage();
      this.dgvJobs_4 = new System.Windows.Forms.DataGridView();
      this.gbSelectAndDeploy.SuspendLayout();
      this.gbFolderBrowser.SuspendLayout();
      this.gbDTSInfo.SuspendLayout();
      this.gbSSISServerConnection.SuspendLayout();
      this.gbSSISServerConnectionString.SuspendLayout();
      this.gbConnsStatus.SuspendLayout();
      this.gbConnectionStrings.SuspendLayout();
      this.gbIntegrationServicesServer.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.gbConfigSettings.SuspendLayout();
      this.gbScheduling.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
      this.statusBar.SuspendLayout();
      this.gbOutputs.SuspendLayout();
      this.tcTabs.SuspendLayout();
      this.tpOutputs_1.SuspendLayout();
      this.tpOutputs_2.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_1)).BeginInit();
      this.tabPage2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_2)).BeginInit();
      this.tabPage3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_3)).BeginInit();
      this.tabPage4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_4)).BeginInit();
      this.SuspendLayout();
      // 
      // btn_deploy_packages
      // 
      this.btn_deploy_packages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_deploy_packages.Location = new System.Drawing.Point(117, 281);
      this.btn_deploy_packages.Name = "btn_deploy_packages";
      this.btn_deploy_packages.Size = new System.Drawing.Size(114, 29);
      this.btn_deploy_packages.TabIndex = 0;
      this.btn_deploy_packages.Text = "Deploy Packages!";
      this.btn_deploy_packages.UseVisualStyleBackColor = true;
      this.btn_deploy_packages.Click += new System.EventHandler(this.btn_deploy_packages_Click);
      // 
      // clbDTSX
      // 
      this.clbDTSX.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.clbDTSX.FormattingEnabled = true;
      this.clbDTSX.Location = new System.Drawing.Point(6, 19);
      this.clbDTSX.Name = "clbDTSX";
      this.clbDTSX.Size = new System.Drawing.Size(227, 259);
      this.clbDTSX.TabIndex = 1;
      // 
      // gbSelectAndDeploy
      // 
      this.gbSelectAndDeploy.Controls.Add(this.btn_PkgVersion);
      this.gbSelectAndDeploy.Controls.Add(this.clbDTSX);
      this.gbSelectAndDeploy.Controls.Add(this.btn_deploy_packages);
      this.gbSelectAndDeploy.Location = new System.Drawing.Point(18, 391);
      this.gbSelectAndDeploy.Name = "gbSelectAndDeploy";
      this.gbSelectAndDeploy.Size = new System.Drawing.Size(239, 316);
      this.gbSelectAndDeploy.TabIndex = 2;
      this.gbSelectAndDeploy.TabStop = false;
      this.gbSelectAndDeploy.Text = "Select And Deploy DTSX Files";
      // 
      // btn_PkgVersion
      // 
      this.btn_PkgVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btn_PkgVersion.Location = new System.Drawing.Point(6, 281);
      this.btn_PkgVersion.Name = "btn_PkgVersion";
      this.btn_PkgVersion.Size = new System.Drawing.Size(105, 29);
      this.btn_PkgVersion.TabIndex = 2;
      this.btn_PkgVersion.Text = "Package Version";
      this.tooltipControl.SetToolTip(this.btn_PkgVersion, "Gets version information about selected packages, be careful if any package has p" +
        "assword, it must be the same and specified in the Package Password field.");
      this.btn_PkgVersion.UseVisualStyleBackColor = true;
      this.btn_PkgVersion.Click += new System.EventHandler(this.btn_PkgVersion_Click);
      this.btn_PkgVersion.MouseLeave += new System.EventHandler(this.btn_PkgVersion_MouseLeave);
      this.btn_PkgVersion.MouseHover += new System.EventHandler(this.btn_PkgVersion_MouseHover);
      // 
      // txt_folder
      // 
      this.txt_folder.Location = new System.Drawing.Point(6, 19);
      this.txt_folder.Name = "txt_folder";
      this.txt_folder.Size = new System.Drawing.Size(1015, 20);
      this.txt_folder.TabIndex = 3;
      this.tooltipControl.SetToolTip(this.txt_folder, "Click to begin selecting your ETL\'s Folder.");
      this.txt_folder.MouseLeave += new System.EventHandler(this.txt_folder_MouseLeave);
      this.txt_folder.MouseHover += new System.EventHandler(this.txt_folder_MouseHover);
      this.txt_folder.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txt_folder_MouseUp);
      // 
      // gbFolderBrowser
      // 
      this.gbFolderBrowser.Controls.Add(this.txt_folder);
      this.gbFolderBrowser.Location = new System.Drawing.Point(18, 13);
      this.gbFolderBrowser.Name = "gbFolderBrowser";
      this.gbFolderBrowser.Size = new System.Drawing.Size(1033, 52);
      this.gbFolderBrowser.TabIndex = 4;
      this.gbFolderBrowser.TabStop = false;
      this.gbFolderBrowser.Text = "Select Folder Where Your DTSX Files Are...";
      this.tooltipControl.SetToolTip(this.gbFolderBrowser, "Click to begin selecting your ETL\'s Folder.");
      this.gbFolderBrowser.Leave += new System.EventHandler(this.gbFolderBrowser_Leave);
      this.gbFolderBrowser.MouseHover += new System.EventHandler(this.gbFolderBrowser_MouseHover);
      // 
      // rtbResults
      // 
      this.rtbResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rtbResults.Location = new System.Drawing.Point(3, 3);
      this.rtbResults.Name = "rtbResults";
      this.rtbResults.Size = new System.Drawing.Size(768, 259);
      this.rtbResults.TabIndex = 0;
      this.rtbResults.Text = "";
      // 
      // gbDTSInfo
      // 
      this.gbDTSInfo.Controls.Add(this.gbSSISServerConnection);
      this.gbDTSInfo.Controls.Add(this.gbConnsStatus);
      this.gbDTSInfo.Controls.Add(this.gbConnectionStrings);
      this.gbDTSInfo.Controls.Add(this.gbIntegrationServicesServer);
      this.gbDTSInfo.Controls.Add(this.gbConfigSettings);
      this.gbDTSInfo.Location = new System.Drawing.Point(18, 71);
      this.gbDTSInfo.Name = "gbDTSInfo";
      this.gbDTSInfo.Size = new System.Drawing.Size(1033, 314);
      this.gbDTSInfo.TabIndex = 6;
      this.gbDTSInfo.TabStop = false;
      this.gbDTSInfo.Text = "DTS Server Info";
      // 
      // gbSSISServerConnection
      // 
      this.gbSSISServerConnection.Controls.Add(this.gbSSISServerConnectionString);
      this.gbSSISServerConnection.Controls.Add(this.label2);
      this.gbSSISServerConnection.Controls.Add(this.chkExecutePackage);
      this.gbSSISServerConnection.Location = new System.Drawing.Point(340, 125);
      this.gbSSISServerConnection.Name = "gbSSISServerConnection";
      this.gbSSISServerConnection.Size = new System.Drawing.Size(687, 79);
      this.gbSSISServerConnection.TabIndex = 14;
      this.gbSSISServerConnection.TabStop = false;
      this.gbSSISServerConnection.Text = "SSIS Server Connection String";
      // 
      // gbSSISServerConnectionString
      // 
      this.gbSSISServerConnectionString.Controls.Add(this.txtSSISServerConnectionString);
      this.gbSSISServerConnectionString.Location = new System.Drawing.Point(6, 15);
      this.gbSSISServerConnectionString.Name = "gbSSISServerConnectionString";
      this.gbSSISServerConnectionString.Size = new System.Drawing.Size(675, 41);
      this.gbSSISServerConnectionString.TabIndex = 10;
      this.gbSSISServerConnectionString.TabStop = false;
      this.gbSSISServerConnectionString.Text = "SSIS Server Connection String";
      // 
      // txtSSISServerConnectionString
      // 
      this.txtSSISServerConnectionString.Location = new System.Drawing.Point(6, 15);
      this.txtSSISServerConnectionString.Name = "txtSSISServerConnectionString";
      this.txtSSISServerConnectionString.Size = new System.Drawing.Size(663, 20);
      this.txtSSISServerConnectionString.TabIndex = 7;
      this.txtSSISServerConnectionString.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtSSISServerConnectionString_MouseUp);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(16, 12);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(0, 13);
      this.label2.TabIndex = 6;
      // 
      // chkExecutePackage
      // 
      this.chkExecutePackage.AutoSize = true;
      this.chkExecutePackage.Location = new System.Drawing.Point(12, 57);
      this.chkExecutePackage.Name = "chkExecutePackage";
      this.chkExecutePackage.Size = new System.Drawing.Size(163, 17);
      this.chkExecutePackage.TabIndex = 12;
      this.chkExecutePackage.Text = "Execute Package After Load";
      this.tooltipControl.SetToolTip(this.chkExecutePackage, "As a testing purpouse option, this check allows to run package directly to check " +
        "if it works properly.");
      this.chkExecutePackage.UseVisualStyleBackColor = true;
      this.chkExecutePackage.MouseLeave += new System.EventHandler(this.chkExecutePackage_MouseLeave);
      this.chkExecutePackage.MouseHover += new System.EventHandler(this.chkExecutePackage_MouseHover);
      // 
      // gbConnsStatus
      // 
      this.gbConnsStatus.Controls.Add(this.lblStatusSSIS);
      this.gbConnsStatus.Controls.Add(this.lblStatusDestination);
      this.gbConnsStatus.Controls.Add(this.lblStatusSource);
      this.gbConnsStatus.Controls.Add(this.btnTest);
      this.gbConnsStatus.Location = new System.Drawing.Point(872, 227);
      this.gbConnsStatus.Name = "gbConnsStatus";
      this.gbConnsStatus.Size = new System.Drawing.Size(155, 81);
      this.gbConnsStatus.TabIndex = 14;
      this.gbConnsStatus.TabStop = false;
      this.gbConnsStatus.Text = "Connections Status";
      // 
      // lblStatusSSIS
      // 
      this.lblStatusSSIS.AutoSize = true;
      this.lblStatusSSIS.Location = new System.Drawing.Point(29, 50);
      this.lblStatusSSIS.Name = "lblStatusSSIS";
      this.lblStatusSSIS.Size = new System.Drawing.Size(31, 13);
      this.lblStatusSSIS.TabIndex = 3;
      this.lblStatusSSIS.Text = "SSIS";
      this.lblStatusSSIS.Visible = false;
      // 
      // lblStatusDestination
      // 
      this.lblStatusDestination.AutoSize = true;
      this.lblStatusDestination.Location = new System.Drawing.Point(53, 29);
      this.lblStatusDestination.Name = "lblStatusDestination";
      this.lblStatusDestination.Size = new System.Drawing.Size(60, 13);
      this.lblStatusDestination.TabIndex = 2;
      this.lblStatusDestination.Text = "Destination";
      this.lblStatusDestination.Visible = false;
      // 
      // lblStatusSource
      // 
      this.lblStatusSource.AutoSize = true;
      this.lblStatusSource.Location = new System.Drawing.Point(6, 29);
      this.lblStatusSource.Name = "lblStatusSource";
      this.lblStatusSource.Size = new System.Drawing.Size(41, 13);
      this.lblStatusSource.TabIndex = 1;
      this.lblStatusSource.Text = "Source";
      this.lblStatusSource.Visible = false;
      // 
      // btnTest
      // 
      this.btnTest.Location = new System.Drawing.Point(6, 11);
      this.btnTest.Name = "btnTest";
      this.btnTest.Size = new System.Drawing.Size(10, 15);
      this.btnTest.TabIndex = 0;
      this.btnTest.Text = "Test!";
      this.btnTest.UseVisualStyleBackColor = true;
      this.btnTest.Visible = false;
      this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
      // 
      // gbConnectionStrings
      // 
      this.gbConnectionStrings.Controls.Add(this.txt_path_xml);
      this.gbConnectionStrings.Controls.Add(this.label1);
      this.gbConnectionStrings.Location = new System.Drawing.Point(340, 13);
      this.gbConnectionStrings.Name = "gbConnectionStrings";
      this.gbConnectionStrings.Size = new System.Drawing.Size(687, 106);
      this.gbConnectionStrings.TabIndex = 13;
      this.gbConnectionStrings.TabStop = false;
      this.gbConnectionStrings.Text = "Path XML file";
      // 
      // txt_path_xml
      // 
      this.txt_path_xml.Location = new System.Drawing.Point(12, 34);
      this.txt_path_xml.Name = "txt_path_xml";
      this.txt_path_xml.Size = new System.Drawing.Size(669, 20);
      this.txt_path_xml.TabIndex = 7;
      this.tooltipControl.SetToolTip(this.txt_path_xml, "Click to begin selecting your ETL\'s Folder.");
      this.txt_path_xml.MouseLeave += new System.EventHandler(this.txt_folder_MouseLeave);
      this.txt_path_xml.MouseHover += new System.EventHandler(this.txt_path_xml_MouseHover);
      this.txt_path_xml.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txt_path_xml_MouseUp);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(16, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(0, 13);
      this.label1.TabIndex = 6;
      // 
      // gbIntegrationServicesServer
      // 
      this.gbIntegrationServicesServer.Controls.Add(this.groupBox2);
      this.gbIntegrationServicesServer.Controls.Add(this.lbl_SSIS_Server_Info);
      this.gbIntegrationServicesServer.Controls.Add(this.btnGetServerInfo);
      this.gbIntegrationServicesServer.Controls.Add(this.groupBox1);
      this.gbIntegrationServicesServer.Location = new System.Drawing.Point(6, 227);
      this.gbIntegrationServicesServer.Name = "gbIntegrationServicesServer";
      this.gbIntegrationServicesServer.Size = new System.Drawing.Size(860, 81);
      this.gbIntegrationServicesServer.TabIndex = 12;
      this.gbIntegrationServicesServer.TabStop = false;
      this.gbIntegrationServicesServer.Text = "Integration Services Setup";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.txtServerName);
      this.groupBox2.Location = new System.Drawing.Point(12, 16);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(317, 45);
      this.groupBox2.TabIndex = 6;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Server Name";
      // 
      // txtServerName
      // 
      this.txtServerName.Enabled = false;
      this.txtServerName.Location = new System.Drawing.Point(6, 16);
      this.txtServerName.Name = "txtServerName";
      this.txtServerName.Size = new System.Drawing.Size(278, 20);
      this.txtServerName.TabIndex = 1;
      this.tooltipControl.SetToolTip(this.txtServerName, "Integration Services Server name where ETL/s will be deployed.");
      this.txtServerName.MouseLeave += new System.EventHandler(this.txtServerName_MouseLeave);
      this.txtServerName.MouseHover += new System.EventHandler(this.txtServerName_MouseHover);
      // 
      // lbl_SSIS_Server_Info
      // 
      this.lbl_SSIS_Server_Info.AutoSize = true;
      this.lbl_SSIS_Server_Info.Location = new System.Drawing.Point(656, 16);
      this.lbl_SSIS_Server_Info.Name = "lbl_SSIS_Server_Info";
      this.lbl_SSIS_Server_Info.Size = new System.Drawing.Size(0, 13);
      this.lbl_SSIS_Server_Info.TabIndex = 0;
      // 
      // btnGetServerInfo
      // 
      this.btnGetServerInfo.Location = new System.Drawing.Point(543, 35);
      this.btnGetServerInfo.Name = "btnGetServerInfo";
      this.btnGetServerInfo.Size = new System.Drawing.Size(99, 28);
      this.btnGetServerInfo.TabIndex = 0;
      this.btnGetServerInfo.Text = "DTS Server Info";
      this.btnGetServerInfo.UseVisualStyleBackColor = true;
      this.btnGetServerInfo.Click += new System.EventHandler(this.btnGetServerInfo_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbFileSystem);
      this.groupBox1.Controls.Add(this.rbMSDB);
      this.groupBox1.Location = new System.Drawing.Point(353, 19);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(156, 42);
      this.groupBox1.TabIndex = 5;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Where Store Packages?";
      // 
      // rbFileSystem
      // 
      this.rbFileSystem.AutoSize = true;
      this.rbFileSystem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.rbFileSystem.Location = new System.Drawing.Point(6, 16);
      this.rbFileSystem.Name = "rbFileSystem";
      this.rbFileSystem.Size = new System.Drawing.Size(77, 17);
      this.rbFileSystem.TabIndex = 3;
      this.rbFileSystem.Text = "File System";
      this.tooltipControl.SetToolTip(this.rbFileSystem, "With this option, package will be saved on a physical location on hard drive.");
      this.rbFileSystem.UseVisualStyleBackColor = true;
      // 
      // rbMSDB
      // 
      this.rbMSDB.AutoSize = true;
      this.rbMSDB.Checked = true;
      this.rbMSDB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
      this.rbMSDB.Location = new System.Drawing.Point(89, 16);
      this.rbMSDB.Name = "rbMSDB";
      this.rbMSDB.Size = new System.Drawing.Size(55, 17);
      this.rbMSDB.TabIndex = 4;
      this.rbMSDB.TabStop = true;
      this.rbMSDB.Text = "MSDB";
      this.tooltipControl.SetToolTip(this.rbMSDB, "With this option, SSIS packages will be stored in the MSDB database.");
      this.rbMSDB.UseVisualStyleBackColor = true;
      // 
      // gbConfigSettings
      // 
      this.gbConfigSettings.Controls.Add(this.chkPasswordPackage);
      this.gbConfigSettings.Controls.Add(this.gbScheduling);
      this.gbConfigSettings.Controls.Add(this.lblPackagePwd);
      this.gbConfigSettings.Controls.Add(this.txtPackagePassword);
      this.gbConfigSettings.Controls.Add(this.chkCreateJob);
      this.gbConfigSettings.Controls.Add(this.chkRunJobOnCreate);
      this.gbConfigSettings.Location = new System.Drawing.Point(6, 19);
      this.gbConfigSettings.Name = "gbConfigSettings";
      this.gbConfigSettings.Size = new System.Drawing.Size(329, 107);
      this.gbConfigSettings.TabIndex = 11;
      this.gbConfigSettings.TabStop = false;
      this.gbConfigSettings.Text = "Configuration Settings";
      // 
      // chkPasswordPackage
      // 
      this.chkPasswordPackage.AutoSize = true;
      this.chkPasswordPackage.Location = new System.Drawing.Point(12, 15);
      this.chkPasswordPackage.Name = "chkPasswordPackage";
      this.chkPasswordPackage.Size = new System.Drawing.Size(140, 17);
      this.chkPasswordPackage.TabIndex = 15;
      this.chkPasswordPackage.Text = "w/o Password Package";
      this.chkPasswordPackage.UseVisualStyleBackColor = true;
      this.chkPasswordPackage.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
      // 
      // gbScheduling
      // 
      this.gbScheduling.Controls.Add(this.txt_schedule);
      this.gbScheduling.Controls.Add(this.cbScheduling);
      this.gbScheduling.Location = new System.Drawing.Point(162, 38);
      this.gbScheduling.Name = "gbScheduling";
      this.gbScheduling.Size = new System.Drawing.Size(161, 37);
      this.gbScheduling.TabIndex = 14;
      this.gbScheduling.TabStop = false;
      this.gbScheduling.Text = "Job Scheduling";
      // 
      // txt_schedule
      // 
      this.txt_schedule.Location = new System.Drawing.Point(91, 12);
      this.txt_schedule.Mask = "99";
      this.txt_schedule.Name = "txt_schedule";
      this.txt_schedule.Size = new System.Drawing.Size(64, 20);
      this.txt_schedule.TabIndex = 15;
      this.txt_schedule.ValidatingType = typeof(int);
      // 
      // cbScheduling
      // 
      this.cbScheduling.FormattingEnabled = true;
      this.cbScheduling.Location = new System.Drawing.Point(7, 12);
      this.cbScheduling.Name = "cbScheduling";
      this.cbScheduling.Size = new System.Drawing.Size(78, 21);
      this.cbScheduling.TabIndex = 0;
      // 
      // lblPackagePwd
      // 
      this.lblPackagePwd.AutoSize = true;
      this.lblPackagePwd.Location = new System.Drawing.Point(6, 31);
      this.lblPackagePwd.Name = "lblPackagePwd";
      this.lblPackagePwd.Size = new System.Drawing.Size(99, 13);
      this.lblPackagePwd.TabIndex = 13;
      this.lblPackagePwd.Text = "Package Password";
      // 
      // txtPackagePassword
      // 
      this.txtPackagePassword.Enabled = false;
      this.txtPackagePassword.Location = new System.Drawing.Point(6, 50);
      this.txtPackagePassword.Name = "txtPackagePassword";
      this.txtPackagePassword.PasswordChar = '*';
      this.txtPackagePassword.Size = new System.Drawing.Size(147, 20);
      this.txtPackagePassword.TabIndex = 12;
      this.txtPackagePassword.MouseLeave += new System.EventHandler(this.txtPackagePassword_MouseLeave);
      this.txtPackagePassword.MouseHover += new System.EventHandler(this.txtPackagePassword_MouseHover);
      // 
      // chkCreateJob
      // 
      this.chkCreateJob.AutoSize = true;
      this.chkCreateJob.Checked = true;
      this.chkCreateJob.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkCreateJob.Location = new System.Drawing.Point(206, 15);
      this.chkCreateJob.Name = "chkCreateJob";
      this.chkCreateJob.Size = new System.Drawing.Size(117, 17);
      this.chkCreateJob.TabIndex = 11;
      this.chkCreateJob.Text = "Create Job for DTS";
      this.tooltipControl.SetToolTip(this.chkCreateJob, "After deploy ETL into Integration Services of , creates a Job refered to the ETL." +
        "");
      this.chkCreateJob.UseVisualStyleBackColor = true;
      this.chkCreateJob.CheckedChanged += new System.EventHandler(this.chkCreateJob_CheckedChanged);
      this.chkCreateJob.MouseLeave += new System.EventHandler(this.chkCreateJob_MouseLeave);
      this.chkCreateJob.MouseHover += new System.EventHandler(this.chkCreateJob_MouseHover);
      // 
      // chkRunJobOnCreate
      // 
      this.chkRunJobOnCreate.AutoSize = true;
      this.chkRunJobOnCreate.Location = new System.Drawing.Point(181, 81);
      this.chkRunJobOnCreate.Name = "chkRunJobOnCreate";
      this.chkRunJobOnCreate.Size = new System.Drawing.Size(142, 17);
      this.chkRunJobOnCreate.TabIndex = 10;
      this.chkRunJobOnCreate.Text = "Run Job On Create DTS";
      this.tooltipControl.SetToolTip(this.chkRunJobOnCreate, "Run inmediatelly the job after to be created.");
      this.chkRunJobOnCreate.UseVisualStyleBackColor = true;
      this.chkRunJobOnCreate.MouseLeave += new System.EventHandler(this.chkRunJobOnCreate_MouseLeave);
      this.chkRunJobOnCreate.MouseHover += new System.EventHandler(this.chkRunJobOnCreate_MouseHover);
      // 
      // epError
      // 
      this.epError.ContainerControl = this;
      // 
      // statusBar
      // 
      this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtInfoStatus});
      this.statusBar.Location = new System.Drawing.Point(0, 710);
      this.statusBar.Name = "statusBar";
      this.statusBar.Size = new System.Drawing.Size(1056, 22);
      this.statusBar.TabIndex = 7;
      this.statusBar.Text = "statusStrip1";
      // 
      // txtInfoStatus
      // 
      this.txtInfoStatus.Name = "txtInfoStatus";
      this.txtInfoStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // gbOutputs
      // 
      this.gbOutputs.Controls.Add(this.tcTabs);
      this.gbOutputs.Location = new System.Drawing.Point(263, 391);
      this.gbOutputs.Name = "gbOutputs";
      this.gbOutputs.Size = new System.Drawing.Size(788, 316);
      this.gbOutputs.TabIndex = 8;
      this.gbOutputs.TabStop = false;
      this.gbOutputs.Text = "Outputs";
      // 
      // tcTabs
      // 
      this.tcTabs.Controls.Add(this.tpOutputs_1);
      this.tcTabs.Controls.Add(this.tpOutputs_2);
      this.tcTabs.Location = new System.Drawing.Point(6, 19);
      this.tcTabs.Name = "tcTabs";
      this.tcTabs.SelectedIndex = 0;
      this.tcTabs.Size = new System.Drawing.Size(782, 291);
      this.tcTabs.TabIndex = 0;
      // 
      // tpOutputs_1
      // 
      this.tpOutputs_1.Controls.Add(this.rtbResults);
      this.tpOutputs_1.Location = new System.Drawing.Point(4, 22);
      this.tpOutputs_1.Name = "tpOutputs_1";
      this.tpOutputs_1.Padding = new System.Windows.Forms.Padding(3);
      this.tpOutputs_1.Size = new System.Drawing.Size(774, 265);
      this.tpOutputs_1.TabIndex = 0;
      this.tpOutputs_1.Text = "Execution Results";
      this.tpOutputs_1.UseVisualStyleBackColor = true;
      // 
      // tpOutputs_2
      // 
      this.tpOutputs_2.Controls.Add(this.tabControl1);
      this.tpOutputs_2.Location = new System.Drawing.Point(4, 22);
      this.tpOutputs_2.Name = "tpOutputs_2";
      this.tpOutputs_2.Padding = new System.Windows.Forms.Padding(3);
      this.tpOutputs_2.Size = new System.Drawing.Size(774, 265);
      this.tpOutputs_2.TabIndex = 1;
      this.tpOutputs_2.Text = "Job Results";
      this.tpOutputs_2.UseVisualStyleBackColor = true;
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage3);
      this.tabControl1.Controls.Add(this.tabPage4);
      this.tabControl1.Location = new System.Drawing.Point(3, 6);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(771, 259);
      this.tabControl1.TabIndex = 0;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.dgvJobs_1);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(763, 233);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Job Details";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // dgvJobs_1
      // 
      this.dgvJobs_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvJobs_1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvJobs_1.Location = new System.Drawing.Point(3, 3);
      this.dgvJobs_1.Name = "dgvJobs_1";
      this.dgvJobs_1.Size = new System.Drawing.Size(757, 227);
      this.dgvJobs_1.TabIndex = 0;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.dgvJobs_2);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(763, 233);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Step Details";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // dgvJobs_2
      // 
      this.dgvJobs_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvJobs_2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvJobs_2.Location = new System.Drawing.Point(3, 3);
      this.dgvJobs_2.Name = "dgvJobs_2";
      this.dgvJobs_2.Size = new System.Drawing.Size(757, 227);
      this.dgvJobs_2.TabIndex = 1;
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.dgvJobs_3);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage3.Size = new System.Drawing.Size(763, 233);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Scheduling Details";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // dgvJobs_3
      // 
      this.dgvJobs_3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvJobs_3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvJobs_3.Location = new System.Drawing.Point(3, 3);
      this.dgvJobs_3.Name = "dgvJobs_3";
      this.dgvJobs_3.Size = new System.Drawing.Size(757, 227);
      this.dgvJobs_3.TabIndex = 7;
      // 
      // tabPage4
      // 
      this.tabPage4.Controls.Add(this.dgvJobs_4);
      this.tabPage4.Location = new System.Drawing.Point(4, 22);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage4.Size = new System.Drawing.Size(763, 233);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "Execution Details";
      this.tabPage4.UseVisualStyleBackColor = true;
      // 
      // dgvJobs_4
      // 
      this.dgvJobs_4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvJobs_4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvJobs_4.Location = new System.Drawing.Point(3, 3);
      this.dgvJobs_4.Name = "dgvJobs_4";
      this.dgvJobs_4.Size = new System.Drawing.Size(757, 227);
      this.dgvJobs_4.TabIndex = 2;
      // 
      // frm_dtsx_deployment_main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1056, 732);
      this.Controls.Add(this.gbOutputs);
      this.Controls.Add(this.statusBar);
      this.Controls.Add(this.gbDTSInfo);
      this.Controls.Add(this.gbFolderBrowser);
      this.Controls.Add(this.gbSelectAndDeploy);
      this.Name = "frm_dtsx_deployment_main";
      this.Text = "DTSX Deployer";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_dtsx_deployment_main_FormClosing);
      this.Shown += new System.EventHandler(this.frm_dtsx_deployment_main_Shown);
      this.gbSelectAndDeploy.ResumeLayout(false);
      this.gbFolderBrowser.ResumeLayout(false);
      this.gbFolderBrowser.PerformLayout();
      this.gbDTSInfo.ResumeLayout(false);
      this.gbSSISServerConnection.ResumeLayout(false);
      this.gbSSISServerConnection.PerformLayout();
      this.gbSSISServerConnectionString.ResumeLayout(false);
      this.gbSSISServerConnectionString.PerformLayout();
      this.gbConnsStatus.ResumeLayout(false);
      this.gbConnsStatus.PerformLayout();
      this.gbConnectionStrings.ResumeLayout(false);
      this.gbConnectionStrings.PerformLayout();
      this.gbIntegrationServicesServer.ResumeLayout(false);
      this.gbIntegrationServicesServer.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.gbConfigSettings.ResumeLayout(false);
      this.gbConfigSettings.PerformLayout();
      this.gbScheduling.ResumeLayout(false);
      this.gbScheduling.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
      this.statusBar.ResumeLayout(false);
      this.statusBar.PerformLayout();
      this.gbOutputs.ResumeLayout(false);
      this.tcTabs.ResumeLayout(false);
      this.tpOutputs_1.ResumeLayout(false);
      this.tpOutputs_2.ResumeLayout(false);
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_1)).EndInit();
      this.tabPage2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_2)).EndInit();
      this.tabPage3.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_3)).EndInit();
      this.tabPage4.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvJobs_4)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_deploy_packages;
    private System.Windows.Forms.CheckedListBox clbDTSX;
    private System.Windows.Forms.GroupBox gbSelectAndDeploy;
    private System.Windows.Forms.TextBox txt_folder;
    private System.Windows.Forms.GroupBox gbFolderBrowser;
    private System.Windows.Forms.RichTextBox rtbResults;
    private System.Windows.Forms.GroupBox gbDTSInfo;
    private System.Windows.Forms.Button btnGetServerInfo;
    private System.Windows.Forms.TextBox txtServerName;
    private System.Windows.Forms.ErrorProvider epError;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RadioButton rbFileSystem;
    private System.Windows.Forms.RadioButton rbMSDB;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox chkRunJobOnCreate;
    private System.Windows.Forms.Button btn_PkgVersion;
    private System.Windows.Forms.GroupBox gbConfigSettings;
    private System.Windows.Forms.CheckBox chkExecutePackage;
    private System.Windows.Forms.CheckBox chkCreateJob;
    private System.Windows.Forms.Label lblPackagePwd;
    private System.Windows.Forms.TextBox txtPackagePassword;
    private System.Windows.Forms.StatusStrip statusBar;
    private System.Windows.Forms.ToolStripStatusLabel txtInfoStatus;
    private System.Windows.Forms.GroupBox gbIntegrationServicesServer;
    private System.Windows.Forms.Label lbl_SSIS_Server_Info;
    private System.Windows.Forms.GroupBox gbConnectionStrings;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.ToolTip tooltipControl;
    private System.Windows.Forms.GroupBox gbConnsStatus;
    private System.Windows.Forms.Label lblStatusDestination;
    private System.Windows.Forms.Label lblStatusSource;
    private System.Windows.Forms.Button btnTest;
    private System.Windows.Forms.GroupBox gbScheduling;
    private System.Windows.Forms.ComboBox cbScheduling;
    private System.Windows.Forms.GroupBox gbSSISServerConnection;
    private System.Windows.Forms.GroupBox gbSSISServerConnectionString;
    private System.Windows.Forms.TextBox txtSSISServerConnectionString;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox chkPasswordPackage;
    private System.Windows.Forms.Label lblStatusSSIS;
    private System.Windows.Forms.GroupBox gbOutputs;
    private System.Windows.Forms.TabControl tcTabs;
    private System.Windows.Forms.TabPage tpOutputs_1;
    private System.Windows.Forms.TabPage tpOutputs_2;
    private System.Windows.Forms.DataGridView dgvJobs_1;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.DataGridView dgvJobs_2;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.DataGridView dgvJobs_3;
    private System.Windows.Forms.TabPage tabPage4;
    private System.Windows.Forms.DataGridView dgvJobs_4;
    private System.Windows.Forms.TextBox txt_path_xml;
    private System.Windows.Forms.MaskedTextBox txt_schedule;
    private System.Windows.Forms.OpenFileDialog fileBrowser;
  }
}

