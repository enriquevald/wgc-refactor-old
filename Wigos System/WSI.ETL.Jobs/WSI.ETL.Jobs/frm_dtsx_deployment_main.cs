﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: frm_dtsx_deployment_main.cs
// 
//      DESCRIPTION: Winform DTSX Deployment Main
// 
//           AUTHOR: Oscar Mas
// 
//    CREATION DATE: 06-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-SEP-2017 OMC    First release.
//------------------------------------------------------------------------------
using log4net;
using System;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Dts.Runtime;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.ServiceProcess;

namespace DTSXDeployment
{
  public partial class frm_dtsx_deployment_main : Form
  {
    public const int SELECTED_SECOND = 0;
    public const int SELECTED_MINUTE = 1;
    public const int SELECTED_HOUR = 2;
    public const string LOCAL_NAME = "(Local)";

    Package pkg;
    Microsoft.SqlServer.Dts.Runtime.Application app;
    MyEventListener evList;
    private readonly ILog m_logger;
    DTSExecResult pkgResults;
    public const string DTS_FOLDER_NAME = "ETLs";
    public const string FILE_SYSTEM = "File System";
    public const string MSDB = "MSDB";
    public string ssis_server;
    public string ssis_initialcatalog;
    public string ssis_user;
    public string ssis_pwd;

    #region "Constructors"
    public frm_dtsx_deployment_main()
    {
      InitializeComponent();

    }//frm_dtsx_deployment_main
    public frm_dtsx_deployment_main(ILog Logger)
      : this()
    {
      this.m_logger = Logger;
      app = new Microsoft.SqlServer.Dts.Runtime.Application();
      //src_conn_str.Text = GetConnectionStringByName("srcConn");
      //dest_conn_str.Text = GetConnectionStringByName("destConn");
      txt_folder.Text = GetAppProperty("DTSXFolder");
      if (!String.IsNullOrEmpty(txt_folder.Text))
      {
        LoadCheckListBox();
      }

      txt_path_xml.Text = GetAppProperty("DTSXXmlFile");
      if (!String.IsNullOrEmpty(txt_path_xml.Text))
      {
        SetEnvironmentPath();
      }

      txtSSISServerConnectionString.Text = GetConnectionStringByName("ssisConn");
      //LoadSSISConnectionString();
      //GetSSISServerName();
      LoadSchedulingComboBox();
      SetupGrids(new DataGridView[] { dgvJobs_1, dgvJobs_2, dgvJobs_3, dgvJobs_4 });
    } //frm_dtsx_deployment_main 
    #endregion

    #region "Private Methods"

    private void SetEnvironmentPath()
    {
      System.Environment.SetEnvironmentVariable("DTSConfigPath", txt_path_xml.Text, EnvironmentVariableTarget.Machine);
      RestartService("SQLAgent");
    }

    private void LoadSchedulingComboBox()
    {
      cbScheduling.Items.Clear();
      cbScheduling.Items.Add("Seconds");
      cbScheduling.Items.Add("Minutes");
      cbScheduling.Items.Add("Hours");

      cbScheduling.SelectedIndex = 1;
    } //LoadSchedulingComboBox
    private void LoadAndDeployPackages(string pkgLocation)
    {
      try
      {
        evList = new MyEventListener();
        try
        {
          pkg = app.LoadPackage(pkgLocation, null);
        }
        catch (Exception)
        {
          app.PackagePassword = txtPackagePassword.Text;
          pkg = app.LoadPackage(pkgLocation, null);
        }

        rtbResults.AppendText(Environment.NewLine + pkgLocation);
        rtbResults.AppendText(Environment.NewLine + "CONNECTIONS");

        pkg.PackagePassword = txtPackagePassword.Text;
        if (chkPasswordPackage.Checked) { pkg.ProtectionLevel = DTSProtectionLevel.EncryptAllWithPassword; } else { pkg.ProtectionLevel = DTSProtectionLevel.DontSaveSensitive; }

        if (pkg.Connections.Count > 0)
        {
          foreach (ConnectionManager item in pkg.Connections)
          {
            //if (item.Name.ToLower().Equals("target")) {
            //  item.ConnectionString = dest_conn_str.Text;
            //  item.Description = dest_conn_str.Text;

            //  if (chkPasswordPackage.Checked)
            //  {
            //    item.Properties["Password"].SetValue(item, ssis_pwd);//txtPackagePassword.Text);
            //  }
            //}
            //if (item.Name.ToLower().Equals("source"))
            //{
            //  item.ConnectionString = src_conn_str.Text;
            //  if (chkPasswordPackage.Checked)
            //  {
            //    item.Properties["Password"].SetValue(item, ssis_pwd);//txtPackagePassword.Text);

            //  }
            //}
            //rtbResults.AppendText(Environment.NewLine + string.Format("{0}: {1}", item.Name, item.ConnectionString));
            //m_logger.Info(string.Format(System.Reflection.MethodBase.GetCurrentMethod().Name + string.Format("    - {0}: {1}", item.Name, item.ConnectionString)));
          }
        }
        if (!pkg.ProtectionLevel.Equals(DTSProtectionLevel.EncryptAllWithPassword))
        {

          //string xmlPkg = string.Empty;
          app.SaveToXml(pkgLocation, pkg, evList);
          //System.Xml.XmlDocument dtsxUpdated = new System.Xml.XmlDocument();
          //dtsxUpdated.LoadXml(xmlPkg);
          //dtsxUpdated.Save(pkgLocation);
          pkg = app.LoadPackage(pkgLocation, evList);
        }

        if (!app.ExistsOnDtsServer(string.Format("{0}\\{1}\\{2}", GetParentFolderName(), DTS_FOLDER_NAME, GetFileName(pkgLocation)), txtServerName.Text))
        {
          m_logger.Info(string.Format(System.Reflection.MethodBase.GetCurrentMethod().Name + " - There is no: '{0}' named package in DTS Server.", pkgLocation));
        }
        else
        {
          m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + string.Format(" - Package already exists: '{0}' in DTS Server.", pkgLocation));
          if (MessageBox.Show(this, "Do you want to update package?", "DTSX Deployment Package Already Exists", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
          {
            app.RemoveFromDtsServer(string.Format("{0}\\{1}\\{2}", GetParentFolderName(), DTS_FOLDER_NAME, GetFileName(pkgLocation)), txtServerName.Text);
            rtbResults.AppendText(Environment.NewLine + "Package removed. " + string.Format("{0}\\{1}\\{2}", GetParentFolderName(), DTS_FOLDER_NAME, GetFileName(pkgLocation)));
          }
        }

        app.SaveToDtsServer(pkg, evList, string.Format("{0}\\{1}\\{2}", GetParentFolderName(), DTS_FOLDER_NAME, GetFileName(pkgLocation)), txtServerName.Text);
        if (rbFileSystem.Checked)
          rtbResults.AppendText(Environment.NewLine + string.Format("Package added to file system {0}. ", string.Format("{0}\\{1}\\{2}", GetParentFolderName(), DTS_FOLDER_NAME, GetFileName(pkgLocation))));
        else
          rtbResults.AppendText(Environment.NewLine + string.Format("Package added to SQL Server {0}. ", string.Format("{0}\\{1}", GetParentFolderName(), DTS_FOLDER_NAME)));

        if (chkExecutePackage.Checked)
        {
          pkgResults = new DTSExecResult();
          pkgResults = pkg.Execute();
          m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + string.Format(" - Package execution Results: '{0}'.", pkgResults.ToString()));
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
        rtbResults.AppendText(Environment.NewLine + String.Format(" - Error deploying {0} package: {1}.", GetFileName(pkgLocation), ex.Message));
      }
    } //LoadAndDeployPackages
    private string GetFileName(string fullPath)
    {
      System.IO.FileInfo fi = null;
      try
      {
        fi = new System.IO.FileInfo(fullPath);
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
      return fi.Name.Replace(fi.Extension, string.Empty);
    } //GetFileName
    private bool CreateFolderToStoreETLs()
    {
      bool _value = false;
      try
      {
        if (!string.IsNullOrEmpty(txtServerName.Text))
        {
          if (!app.FolderExistsOnDtsServer(string.Format("{0}\\{1}", GetParentFolderName(), DTS_FOLDER_NAME), txtServerName.Text))
          {
            app.CreateFolderOnDtsServer(GetParentFolderName(), DTS_FOLDER_NAME, txtServerName.Text);
            _value = true;
          }
          else
          {
            m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Folder Already exists on DTS Server.");
            _value = true;
          }
        }
        else
        {
          m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Server Name must be informed previous to run any action, in order to be able to refer actions to server.");
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
      return _value;
    } //CreateFolderToStoreETLs
    private void ChooseFolder(TextBox Sender)
    {
      try
      {
        CommonOpenFileDialog _fbdBrowser = new CommonOpenFileDialog();
        _fbdBrowser.IsFolderPicker = true;
        if (!String.IsNullOrEmpty(Sender.Text))
        {
          _fbdBrowser.InitialDirectory = Sender.Text;
        }

        if (_fbdBrowser.ShowDialog() == CommonFileDialogResult.Ok)
        {
          Sender.Text = _fbdBrowser.FileName;
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //ChooseFolder

    private void ChooseFile(TextBox Sender)
    {
      try
      {
        fileBrowser.Filter = "Text Files (*.dtsConfig)|*.dtsConfig";
        fileBrowser.FilterIndex = 1;

        if (fileBrowser.ShowDialog() == DialogResult.OK)
        {
          String current = System.Environment.GetEnvironmentVariable("DTSConfigPath", EnvironmentVariableTarget.Machine);

          Sender.Text = fileBrowser.FileName;
          SetEnvironmentPath();
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    }

    private void LoadCheckListBox()
    {
      try
      {
        if (!string.IsNullOrEmpty(txt_folder.Text))
        {
          clbDTSX.Items.Clear();
          clbDTSX.Items.AddRange(new System.IO.DirectoryInfo(txt_folder.Text).GetFiles("*.dtsx"));
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //LoadCheckListBox
    private string GetParentFolderName()
    {
      string _parent_folder_name = string.Empty;
      try
      {
        if (rbFileSystem.Checked)
        {
          _parent_folder_name = FILE_SYSTEM;
        }
        else if (rbMSDB.Checked)
        {
          _parent_folder_name = MSDB;
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
      return _parent_folder_name;
    } //GetParentFolderName
    private void SetupGrids(DataGridView[] adgv)
    {
      foreach (DataGridView dgv in adgv)
      {
        dgv.AllowUserToAddRows = false;
        dgv.AllowUserToDeleteRows = false;
        dgv.AllowUserToOrderColumns = false;
      }
    }
    private void CreateJobAndAppendDTS(System.Data.SqlClient.SqlConnection cnn, string dts_name)
    {
      System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
      DataSet ds = new DataSet();
      try
      {
        //Execution of job on lauch
        if (chkCreateJob.Checked)
        {
          StringBuilder sbJob = new StringBuilder();
          sbJob.AppendLine("USE [msdb];");
          sbJob.AppendLine("DECLARE @jobId BINARY(16)");
          sbJob.AppendLine("DECLARE @schedule_id INT");
          sbJob.AppendLine(string.Format("IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'jb_{0}')", dts_name));
          sbJob.AppendLine(string.Format("EXEC msdb.dbo.sp_delete_job @job_name = N'jb_{0}', @delete_unused_schedule=1;", dts_name));
          sbJob.AppendLine(string.Format("EXEC  msdb.dbo.sp_add_job @job_name=N'jb_{0}', ", dts_name));
          sbJob.AppendLine("		@enabled=1, ");
          sbJob.AppendLine("		@notify_level_eventlog=0, ");
          sbJob.AppendLine("		@notify_level_email=0, ");
          sbJob.AppendLine("		@notify_level_netsend=0, ");
          sbJob.AppendLine("		@notify_level_page=0, ");
          sbJob.AppendLine("		@delete_level=0, ");
          sbJob.AppendLine(string.Format("		@description=N'{0}', ", dts_name));
          sbJob.AppendLine("		@category_name=N'Data Collector', ");
          sbJob.AppendLine("		@owner_login_name=N'sa', @job_id = @jobId OUTPUT;");
          sbJob.AppendLine(string.Format("EXEC msdb.dbo.sp_add_jobserver @job_name=N'jb_{0}', @server_name = N'{1}';", dts_name, GetServerName(cnn.DataSource)));
          sbJob.AppendLine(string.Format("EXEC msdb.dbo.sp_add_jobstep @job_name=N'jb_{0}', @step_name=N'jb_step_{0}', ", dts_name));
          sbJob.AppendLine("		@step_id=1, ");
          sbJob.AppendLine("		@cmdexec_success_code=0, ");
          sbJob.AppendLine("		@on_success_action=1, ");
          sbJob.AppendLine("		@on_fail_action=2, ");
          sbJob.AppendLine("		@retry_attempts=0, ");
          sbJob.AppendLine("		@retry_interval=0, ");
          sbJob.AppendLine("		@os_run_priority=0, @subsystem=N'SSIS', ");
          if (chkPasswordPackage.Checked)
          {
            sbJob.AppendLine(string.Format(@"		@command=N'/DTS ""{0}\{1}"" /SERVER ""{2}""  /DECRYPT {3} /CHECKPOINTING OFF /REPORTING E', ", string.Format("\\{0}\\{1}", GetParentFolderName(), DTS_FOLDER_NAME), dts_name, txtServerName.Text, txtPackagePassword.Text));
          }
          else
          {
            sbJob.AppendLine(string.Format(@"		@command=N'/DTS ""{0}\{1}"" /SERVER ""{2}""  /CHECKPOINTING OFF /REPORTING E', ", string.Format("\\{0}\\{1}", GetParentFolderName(), DTS_FOLDER_NAME), dts_name, txtServerName.Text));
          }

          sbJob.AppendLine("		@database_name=N'master', ");
          sbJob.AppendLine("		@flags=0;");
          sbJob.AppendLine(string.Format("EXEC msdb.dbo.sp_update_job @job_name=N'jb_{0}', ", dts_name));
          sbJob.AppendLine("		@enabled=1, ");
          sbJob.AppendLine("		@start_step_id=1, ");
          sbJob.AppendLine("		@notify_level_eventlog=0, ");
          sbJob.AppendLine("		@notify_level_email=0, ");
          sbJob.AppendLine("		@notify_level_netsend=0, ");
          sbJob.AppendLine("		@notify_level_page=0, ");
          sbJob.AppendLine("		@delete_level=0, ");
          sbJob.AppendLine(string.Format("		@description=N'{0}', ", dts_name));
          sbJob.AppendLine("		@category_name=N'Data Collector', ");
          sbJob.AppendLine("		@owner_login_name=N'sa', ");
          sbJob.AppendLine("		@notify_email_operator_name=N'', ");
          sbJob.AppendLine("		@notify_netsend_operator_name=N'', ");
          sbJob.AppendLine("		@notify_page_operator_name=N'';");

          sbJob.AppendLine(AddJobSchedule(dts_name));


          sbJob.AppendLine("		@enabled=1, ");
          sbJob.AppendLine("		@freq_type=4, ");
          sbJob.AppendLine("		@freq_interval=1, ");

          sbJob.Append(AddSchedule());

          sbJob.AppendLine("		@freq_relative_interval=0, ");
          sbJob.AppendLine("		@freq_recurrence_factor=1, ");
          sbJob.AppendLine("		@active_start_date=20170904, ");
          sbJob.AppendLine("		@active_end_date=99991231, ");
          sbJob.AppendLine("		@active_start_time=0, ");
          sbJob.AppendLine("		@active_end_time=235959, ");
          sbJob.AppendLine("		@schedule_id = @schedule_id OUTPUT");
          //Execution of job on lauch
          if (chkRunJobOnCreate.Checked)
          {
            sbJob.AppendLine(string.Format("EXEC msdb.dbo.sp_start_job @job_name=N'jb_{0}'; ", dts_name));
          }

          using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sbJob.ToString(), cnn))
          {
            cnn.Open();
            cmd.ExecuteNonQuery();
          }

          sbJob = new StringBuilder();
          sbJob.AppendLine("WAITFOR DELAY '00:00:01';");
          sbJob.AppendLine("EXEC dbo.sp_help_job ");
          sbJob.AppendLine(string.Format(" @job_name=N'jb_{0}', ", dts_name));
          sbJob.AppendLine(" @job_aspect = N'ALL' ; ");


          using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sbJob.ToString(), cnn))
          {
            da.SelectCommand = cmd;
            if (!cnn.State.Equals(ConnectionState.Open))
              cnn.Open();
            da.Fill(ds);
          }

          dgvJobs_1.DataSource = ds.Tables[0];
          dgvJobs_2.DataSource = ds.Tables[1];
          dgvJobs_3.DataSource = ds.Tables[2];
          dgvJobs_4.DataSource = ds.Tables[3];
          rtbResults.AppendText(string.Format(Environment.NewLine + Environment.NewLine + " ** {0} ** :: {1}." + Environment.NewLine, dts_name.ToUpper(), ds.Tables[3].Rows[0]["last_outcome_message"].ToString()));
          m_logger.Info(string.Format(Environment.NewLine + Environment.NewLine + " ** {0} ** :: {1}." + Environment.NewLine, dts_name.ToUpper(), ds.Tables[3].Rows[0]["last_outcome_message"].ToString()));
          rtbResults.ScrollToCaret();
        }
        else
        {
          m_logger.Warn(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Application will not create SQL Server Agent Jobs due to its configuration.");
        }
      }
      catch (Exception ex)
      {
        rtbResults.AppendText(Environment.NewLine + "---------------------------------------------------------------");
        rtbResults.AppendText("ERROR: " + ex.Message);
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } // CreateJobAndAppendDTS

    private String AddSchedule()
    {
      StringBuilder _schedule;
      Int16 _interval;

      _schedule = new StringBuilder();

      if (String.IsNullOrEmpty(txt_schedule.Text))
      {
        throw new Exception("Empty Schedule interval");
      }


      switch ((cbScheduling.SelectedIndex))
      {
        case SELECTED_SECOND:
          Int16.TryParse(txt_schedule.Text, out _interval);
          if (_interval < 10)
          {
            _interval = 10;
          }
          _schedule.AppendLine("		@freq_subday_type=2, ");
          _schedule.AppendLine(String.Format("		@freq_subday_interval={0}, ", _interval.ToString()));
          break;

        case SELECTED_MINUTE:
          _schedule.AppendLine("		@freq_subday_type=4, ");
          _schedule.AppendLine(String.Format("		@freq_subday_interval={0}, ", txt_schedule.Text));
          break;
        case SELECTED_HOUR:
          _schedule.AppendLine("		@freq_subday_type=8, ");
          _schedule.AppendLine(String.Format("@freq_subday_interval={0}, ", txt_schedule.Text));
          break;



        default:
          throw new System.ArgumentException("Selected index not included in AddSchedule()");
      }

      return _schedule.ToString();

    } //AddSchedule

    private String AddJobSchedule(String Name)
    {
      String _job_schedule;

      switch ((cbScheduling.SelectedIndex))
      {
        case SELECTED_SECOND:
          _job_schedule = String.Format("EXEC msdb.dbo.sp_add_jobschedule @job_name=N'jb_{0}', @name=N'Second Default Schedule', ", Name);
          break;

        case SELECTED_MINUTE:
          _job_schedule = String.Format("EXEC msdb.dbo.sp_add_jobschedule @job_name=N'jb_{0}', @name=N'Minute Default Schedule', ", Name);
          break;

        case SELECTED_HOUR:
          _job_schedule = String.Format("EXEC msdb.dbo.sp_add_jobschedule @job_name=N'jb_{0}', @name=N'Hour Default Schedule', ", Name);
          break;
        default:
          throw new System.ArgumentException("Selected index not included");
      }

      return _job_schedule;
    }
    private void LoadSSISConnectionString()
    {
      string[] aux;
      string conn;
      try
      {
        conn = GetConnectionStringByName("ssisConn");
        if (!string.IsNullOrEmpty(conn))
        {
          aux = conn.Split(';');
          foreach (string item in aux)
          {
            switch (item.Split('=')[0])
            {
              case "Data Source":
                ssis_server = item.Split('=')[1];
                break;
              case "Initial Catalog":
                ssis_initialcatalog = item.Split('=')[1];
                break;
              case "User ID":
                ssis_user = item.Split('=')[1];
                break;
              case "Password":
                ssis_pwd = item.Split('=')[1];
                break;
              default:
                break;
            }
          }
        }
      }
      catch (Exception)
      {

        throw;
      }

    } //LoadConnectionString

    private string GetServerName(string server_or_serverinstance)
    {
      string _server = string.Empty;
      try
      {
        if (server_or_serverinstance.Contains(@"\"))
        {

          _server = server_or_serverinstance.Split('\\')[0];
        }
        else
        {
          _server = server_or_serverinstance;
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
      return _server;
    } //GetServerName
    private string GetConnectionStringByName(string name)
    {
      string returnValue = null;
      ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];
      if (settings != null)
        returnValue = settings.ConnectionString;
      return returnValue;
    } //GetConnectionStringByName    
    private void OpenFrmConnectionManager(string conn_name)
    {
      using (WSI.ETL.Jobs.frm_connection_manager frm = new WSI.ETL.Jobs.frm_connection_manager(conn_name))
      {
        frm.ShowDialog();
        if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
        {
          switch (conn_name.ToLower())
          {
            case "srcconn":
              //src_conn_str.Text = GetConnectionStringByName(conn_name);
              break;
            case "destconn":
              //dest_conn_str.Text = GetConnectionStringByName(conn_name);
              break;
            case "ssisconn":
              txtSSISServerConnectionString.Text = GetConnectionStringByName(conn_name);
              GetSSISServerName();
              break;
            default:
              break;
          }
          btnTest_Click(null, null);
        }
      }
    } //OpenFrmConnectionManager

    private void GetSSISServerName()
    {

      string _ssis_conn = GetConnectionStringByName("ssisConn");
      try
      {
        using (System.Data.SqlClient.SqlConnection c = new System.Data.SqlClient.SqlConnection(_ssis_conn))
        {
          if (c.State != ConnectionState.Open)
            c.Open();
          string _servername;
          if (c.DataSource.Contains(@"\"))
            _servername = c.DataSource.Split('\\')[0];
          else
            _servername = c.DataSource;

          txtServerName.Text = ((_servername.Equals(".") || _servername.Equals("(local)") || _servername.Equals("local") || _servername.Equals("localhost") || _servername.Equals("127.0.0.1")) ? Environment.MachineName : _servername);
        }
        btnGetServerInfo_Click(null, null);
        ConnectionOK(lblStatusSSIS, gbSSISServerConnectionString);
      }
      catch (Exception Ex)
      {
        ConnectionKO(lblStatusSSIS, gbSSISServerConnectionString);
        m_logger.Error(Ex.Message, Ex);
      }

    } //GetSSISServerName

    private void SaveAppProperty(String Property, String Value)
    {
      System.Configuration.Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
      configuration.AppSettings.Settings.Remove(Property);
      configuration.AppSettings.Settings.Add(Property, Value);
      configuration.Save(ConfigurationSaveMode.Modified, true);
      ConfigurationManager.RefreshSection("appSettings");
    }

    private String GetAppProperty(String Property)
    {
      return ConfigurationManager.AppSettings[Property];
    }

    #endregion

    #region "Events"
    private void btn_deploy_packages_Click(object sender, EventArgs e)
    {
      rtbResults.Clear();
      if (CreateFolderToStoreETLs())
      {
        foreach (System.IO.FileInfo item in clbDTSX.CheckedItems)
        {
          m_logger.Info(string.Format(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Package to be processed: '{0}'.", item.FullName));
          LoadAndDeployPackages(item.FullName);
          m_logger.Info(string.Format(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Package created: '{0}' in DTS Server.", item.FullName));
          using (System.Data.SqlClient.SqlConnection cnn = new System.Data.SqlClient.SqlConnection(txtSSISServerConnectionString.Text.Replace(txtServerName.Text, LOCAL_NAME)))
          {
            //cnn.InfoMessage += OnInfoMessageGenerated;
            CreateJobAndAppendDTS(cnn, GetFileName(item.FullName));
          }
          m_logger.Info(string.Format(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Job created: '{0}' in DTS Server.", item.FullName));
          rtbResults.AppendText(Environment.NewLine + "---------------------------------------------------------------");
          m_logger.Info("---------------------------------------------------------------");
        }
        m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Process finished.");
        MessageBox.Show(this, "Process finished.", "DTSX Deployment Message");
      }
      else
      {
        m_logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name + " - There was a problem creating folder in DTS Server.");
      }
    } //btn_deploy_packages_Click

    private void OnInfoMessageGenerated(object sender, System.Data.SqlClient.SqlInfoMessageEventArgs e)
    {
      rtbResults.AppendText(Environment.NewLine + string.Format("SQL Server output:     {0}", e.Message));
      m_logger.Info(Environment.NewLine + string.Format("SQL Server output:     {0}", e.Message));
      foreach (System.Data.SqlClient.SqlError err in e.Errors)
      {
        rtbResults.AppendText(Environment.NewLine + string.Format("Msg {0}, Level {1}, State {2}, Line {3}", err.Number, err.Class, err.State, err.LineNumber));
        rtbResults.AppendText(Environment.NewLine + err.Message);
        m_logger.InfoFormat("Msg {0}, Level {1}, State {2}, Line {3}", err.Number, err.Class, err.State, err.LineNumber);
        m_logger.InfoFormat("{0}", err.Message);
      }
    }
    private void OnStatementCompleted(object sender, StatementCompletedEventArgs e)
    {
      rtbResults.AppendText(Environment.NewLine + string.Format("SQL Server output:     ({0} row(s) affected)", e.RecordCount));
      m_logger.Info(Environment.NewLine + string.Format("SQL Server output:     ({0} row(s) affected)", e.RecordCount));
    }

    private void btn_PkgVersion_Click(object sender, EventArgs e)
    {
      try
      {

        app.PackagePassword = txtPackagePassword.Text;

        if (clbDTSX.CheckedItems.Count > 0)
        {
          rtbResults.AppendText(Environment.NewLine + "---------------------------------------------------------------");
          foreach (System.IO.FileInfo item in clbDTSX.CheckedItems)
          {
            try
            {
              pkg = app.LoadPackage(item.FullName, null);
              rtbResults.AppendText(Environment.NewLine + string.Format("Package '{0}' Version:     v{1}.{2}.{3}", GetFileName(item.FullName), pkg.VersionMajor, pkg.VersionMinor, pkg.VersionBuild));
            }
            catch (DtsRuntimeException DtsEx)
            {
              m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + DtsEx.Message);
              rtbResults.AppendText(Environment.NewLine + string.Format("ERROR :: Package '{0}': {1}.", GetFileName(item.FullName), DtsEx.Message));
            }
          }
        }
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //btn_PkgVersion_Click
    private void txt_folder_MouseUp(object sender, MouseEventArgs e)
    {
      try
      {
        ChooseFolder((TextBox)sender);
        LoadCheckListBox();
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //txt_folder_MouseUp
    private void btnGetServerInfo_Click(object sender, EventArgs e)
    {
      string _server_version;
      try
      {
        rtbResults.Clear();
        txtServerName.Validated += txtServerName_Validated;
        if (this.ValidateChildren())
        {
          if (!string.IsNullOrEmpty(txtServerName.Text))
          {
            app = new Microsoft.SqlServer.Dts.Runtime.Application();
            app.GetServerInfo(txtServerName.Text, out _server_version);
            rtbResults.AppendText(Environment.NewLine + _server_version);
          }
        }
      }
      catch (Exception ex)
      {
        rtbResults.AppendText(Environment.NewLine + "---------------------------------------------------------------");
        rtbResults.AppendText(Environment.NewLine + ex.Message);
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //btnGetServerInfo_Click
    private void txtServerName_Validated(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(txtServerName.Text))
      {
        epError.SetError(txtServerName, "Server Name must be informed!");
      }
      else
      {
        epError.SetError(txtServerName, string.Empty);
      }
    } //txtServerName_Validated 
    private void chkCreateJob_CheckedChanged(object sender, EventArgs e)
    {
      chkRunJobOnCreate.Enabled = chkCreateJob.Checked;
      gbScheduling.Enabled = chkCreateJob.Checked;
    } //chkCreateJob_CheckedChanged
    private void src_conn_str_MouseUp(object sender, MouseEventArgs e)
    {
      OpenFrmConnectionManager("srcConn");
    } //src_conn_str_MouseUp
    private void txtSSISServerConnectionString_MouseUp(object sender, MouseEventArgs e)
    {
      OpenFrmConnectionManager("ssisConn");
    } //txtSSISServerConnectionString_MouseUp
    private void dest_conn_str_MouseUp(object sender, MouseEventArgs e)
    {
      OpenFrmConnectionManager("destConn");
    } //dest_conn_str_MouseUp
    private void frm_dtsx_deployment_main_Shown(object sender, EventArgs e)
    {
      GetSSISServerName();
    }  //frm_dtsx_deployment_main_Shown
    private void frm_dtsx_deployment_main_FormClosing(object sender, FormClosingEventArgs e)
    {
      SaveAppProperty("DTSXXmlFile", txt_path_xml.Text);
      SaveAppProperty("DTSXFolder", txt_folder.Text);

    } //frm_dtsx_deployment_main_FormClosing

    private void txtPackagePassword_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Set the password for packages with encryption.";
    } //txtPackagePassword_MouseHover
    private void txtPackagePassword_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //txtPackagePassword_MouseLeave
    private void chkCreateJob_MouseHover(object sender, EventArgs e)
    {

      txtInfoStatus.Text = "After deploy ETL into Integration Services of " + txtServerName.Text + ", creates a Job refered to the ETL.";
    } //chkCreateJob_MouseHover
    private void chkCreateJob_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //chkCreateJob_MouseLeave
    private void chkRunJobOnCreate_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Run inmediatelly the job after to be created.";
    } //chkRunJobOnCreate_MouseHover
    private void chkRunJobOnCreate_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //chkRunJobOnCreate_MouseLeave
    private void chkExecutePackage_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "As a testing purpouse option, this check allows to run package directly to check if it works properly.";
    } //chkExecutePackage_MouseHover
    private void chkExecutePackage_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //chkExecutePackage_MouseLeave
    private void txtServerName_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Integration Services Server name where ETL/s will be deployed.";
    } //txtServerName_MouseHover
    private void txtServerName_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //txtServerName_MouseLeave
    private void txt_folder_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Click to begin selecting your ETL's Folder.";
    } //txt_folder_MouseHover

    private void txt_folder_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //txt_folder_MouseLeave
    private void gbFolderBrowser_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Click to begin selecting your ETL's Folder.";
    } //gbFolderBrowser_MouseHover
    private void gbFolderBrowser_Leave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //gbFolderBrowser_Leave
    private void btn_PkgVersion_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Gets version information about selected packages, be careful if any package has password, it must be the same and specified in the Package Password field.";
    } //btn_PkgVersion_MouseHover
    private void btn_PkgVersion_MouseLeave(object sender, EventArgs e)
    {
      txtInfoStatus.Text = string.Empty;
    } //btn_PkgVersion_MouseLeave

    private void btnTest_Click(object sender, EventArgs e)
    {
      //ConnectionResetStatusLabel(lblStatusSource, gbSourceConnectionString);
      //ConnectionResetStatusLabel(lblStatusDestination, gbTargetConnectionString);
      ConnectionResetStatusLabel(lblStatusSSIS, gbSSISServerConnectionString);
      //try
      //{
      //  using (System.Data.SqlClient.SqlConnection cnnS = new System.Data.SqlClient.SqlConnection(src_conn_str.Text))
      //  {
      //    //cnnS.InfoMessage += OnInfoMessageGenerated;
      //    cnnS.Open();
      //  }
      //  ConnectionOK(lblStatusSource, gbSourceConnectionString);
      //}
      //catch (Exception)
      //{
      //  ConnectionKO(lblStatusSource, gbSourceConnectionString);
      //}
      //try
      //{
      //  using (System.Data.SqlClient.SqlConnection cnnD = new System.Data.SqlClient.SqlConnection(dest_conn_str.Text))
      //  {
      //    //cnnD.InfoMessage += OnInfoMessageGenerated;
      //    cnnD.Open();
      //  }
      //  ConnectionOK(lblStatusDestination, gbTargetConnectionString);
      //}
      //catch (Exception)
      //{
      //  ConnectionKO(lblStatusDestination, gbTargetConnectionString);
      //}
      try
      {
        using (System.Data.SqlClient.SqlConnection cnnD = new System.Data.SqlClient.SqlConnection(txtSSISServerConnectionString.Text))
        {
          //cnnD.InfoMessage += OnInfoMessageGenerated;
          cnnD.Open();
        }
        ConnectionOK(lblStatusSSIS, gbSSISServerConnectionString);
      }
      catch (Exception)
      {
        ConnectionKO(lblStatusSSIS, gbSSISServerConnectionString);
      }
    } //btnTest_Click

    private void ConnectionOK(Label lbl, GroupBox gb)
    {
      lbl.ForeColor = Color.Green;
      lbl.BorderStyle = BorderStyle.Fixed3D;
      lbl.Visible = true;
      lbl.Font = new Font(Label.DefaultFont, FontStyle.Bold);
      gb.BackColor = Color.LightGreen;
    } //ConnectionOK

    private void ConnectionKO(Label lbl, GroupBox gb)
    {
      lbl.ForeColor = Color.Red;
      lbl.BorderStyle = BorderStyle.Fixed3D;
      lbl.Visible = true;
      lbl.Font = new Font(Label.DefaultFont, FontStyle.Bold);
      gb.BackColor = Color.LightCoral;
    } //ConnectionKO

    private void ConnectionResetStatusLabel(Label lbl, GroupBox gb)
    {
      lbl.ForeColor = Color.Black;
      lbl.Visible = false;
      lbl.Font = new Font(Label.DefaultFont, FontStyle.Regular);
      gb.BackColor = Color.Black;
    } //ConnectionResetStatusLabel

    private void checkBox1_CheckedChanged(object sender, EventArgs e)
    {
      txtPackagePassword.Enabled = chkPasswordPackage.Checked;
      if (!txtPackagePassword.Enabled)
      {
        txtPackagePassword.Text = string.Empty;
      }
    } // checkBox1_CheckedChanged

    private void txt_path_xml_MouseHover(object sender, EventArgs e)
    {
      txtInfoStatus.Text = "Click to begin selecting your config file path.";
    } // txt_path_xml_MouseHover

    private void txt_path_xml_MouseUp(object sender, MouseEventArgs e)
    {
      try
      {
        ChooseFile((TextBox)sender);
        //LoadCheckListBox();
      }
      catch (Exception ex)
      {
        m_logger.Error(System.Reflection.MethodBase.GetCurrentMethod().Name + " - ERROR: " + ex.Message);
      }
    } //txt_folder_MouseUp

    public void RestartService(string serviceName, int timeoutMilliseconds = 10000)
    {
      ServiceController itemservice = new ServiceController(serviceName);

      ServiceController[] scServices;
      scServices = ServiceController.GetServices();
      TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
      foreach (ServiceController item in scServices)
      {
        String test = item.ServiceName;
        if (test.Contains("SQLAgent"))
        {
          if (item.Status == ServiceControllerStatus.Running)
          {
            item.Stop();
            item.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
          }
          item.Start();
          item.WaitForStatus(ServiceControllerStatus.Running, timeout);

        }
      }


    }

    #endregion
  } //frm_dtsx_deployment_main : Form
  class MyEventListener : DefaultEvents
  {
    public override bool OnError(DtsObject source, int errorCode, string subComponent,
     string description, string helpFile, int helpContext, string idofInterfaceWithError)
    {
      // Add application-specific diagnostics here.  
      Console.WriteLine("Error in {0}/{1} : {2}", source, subComponent, description);
      return false;
    } //OnError
  }  //MyEventListener
} //DTSXDeployment