﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: SetBalance.cs
// 
//      DESCRIPTION: SetBalance Class for Web Service
// 
//           AUTHOR: Javi Barea
// 
//    CREATION DATE: 28-OCT-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-OCT-2016 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Net;
using WSI.Common;
using FB.Common.Request;
using FB.Common.Response;
using FB.Common;
using System.Data;
using LCD_GameGateway;

namespace FB.Service.Classes
{
  public class SetBalance : Base
  {
    #region " Constants "

    private const String GAMEGATEWAY_COMMAND = "<type>{0}</type><cmd_id>{1}</cmd_id><accountid>{2}</accountid><partnerid>{3}</partnerid>";

    #endregion 

    #region " Public Methods "

    /// <summary>
    /// Process Request (SetBalance & TrxCancel)
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    public GenericResponse ProcessRequest(SetBalanceRequest Request)
    {
      return ProcessSetBalanceRequest(Request);
    } // ProcessRequest
    public GenericResponse ProcessRequest(TrxCancelRequest Request)
    {
      return ProcessTrxCancelRequest(Request);
    } // ProcessRequest

    /// <summary>
    /// Check transaction type (SetBalance)
    /// </summary>
    /// <param name="TransactionType"></param>
    /// <returns></returns>
    public Boolean CheckTransactionType_SetBalance(FBTransactionType TransactionType)
    {
      return ( TransactionType == FBTransactionType.Sum
            || TransactionType == FBTransactionType.Subtract);
    } // CheckTransactionType_SetBalance

    /// <summary>
    /// Check transaction type (TrxCancel)
    /// </summary>
    /// <param name="TransactionType"></param>
    /// <returns></returns>
    public Boolean CheckTransactionType_TrxCancel(FBTransactionType TransactionType)
    {
      return (TransactionType == FBTransactionType.Rollback);
    } // CheckTransactionType_TrxCancel

    #endregion 

    #region "Protected Methods"

    /// <summary>
    /// Validate and authentication request data 
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    protected Boolean StartSession(SetBalanceRequest Request)
    {
      // Validation and authentication request
      if (!WigosAuthentication(Request))
      {
        return false;
      }

      // Request data validation
      if (!RequestDataValidation(Request))
      {
        return false;
      }

      // Start new transaction
      if (CheckTransactionType_SetBalance(Request.Code))
      {
        // Mark transaction to Pending
        if (!StartTransaction(Request.TransactionId))
        {
          Error.Code = FBErrorCode.TransactionError;
          Error.Message = "SetBalance.StartSession: Error on StartTransaction.";

          return false;
        }
      }

      return true;

    } // StartSession

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Process SetBalance request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private GenericResponse ProcessSetBalanceRequest(SetBalanceRequest Request)
    {
      try
      {
        if (!ProcessRequestInternal(Request, FBRequestType.SetBalance))
        {
          Log.Message(Error.Message);

          // If error code == TrxValidation, must not update status. Is transaction check 
          return RequestTransactionErrorResponse(Request.TransactionId, (Error.Code == FBErrorCode.TrxValidation));
        }

        // Return Success response
        return SuccessResponse();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestTransactionGenericErrorResponse();
    } // ProcessRollbackRequest

    /// <summary>
    /// PRocess TrxCancel request
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private GenericResponse ProcessTrxCancelRequest(TrxCancelRequest Request)
    {
      try
      {
        SetBalanceRequest _request;

        // Mapp TrxCancel to SetBalanceRequest
        _request = Request.ToSetBalanceRequest();

        if (!ProcessRequestInternal(_request, FBRequestType.TrxCancel))
        {
          Error.Code = FBErrorCode.TransactionError;
          Log.Message(Error.Message);

          return RequestTransactionRollbackErrorResponse(Request.TransactionId);
        }

        // Return Success response
        return SuccessResponse();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return RequestTransactionGenericErrorResponse();
    } // ProcessRollbackRequest

    /// <summary>
    /// Process Autenticate Request internal
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RequestType"></param>
    /// <returns></returns>
    private Boolean ProcessRequestInternal(SetBalanceRequest Request, FBRequestType RequestType)
    {
      try
      {
        // Check if is new transaction
        if (!CheckTransaction(Request, RequestType))
        {
          Error.Code = FBErrorCode.TrxValidation;

          return false;
        }

        // Initialize new transaction only if is not Rollback
        if (!InitializeNewTransaction(Request, RequestType))
        {
          Error.Code = FBErrorCode.TransactionError;

          return false;
        }

        // Start Session
        if (!StartSession(Request))
        {
          return false;
        }

        // Process request internal
        if (!SetBalanceInternal(Request))
        {
          if (!TryTransferBalanceFromEGM(Request))
          {
            return false;
          }
        }

        // Finish success FB transaction
        if (!this.EndSuccessTransaction(Request.TransactionId, Request.Code))
        {
          Error.Code = FBErrorCode.TransactionError;
          Error.Message = "EndSuccessTransaction: Error on change transaction status.";

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ProcessRequestInternal

    /// <summary>
    /// Try transfer balance from EGM
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean TryTransferBalanceFromEGM(SetBalanceRequest Request)
    {
      if (Error.Code != FBErrorCode.NotEnoughBalance)
      {
        return false;
      }

      if (GameGateway.IsInPlaySession(Request.CustomerId))
      {
        if (TransferCreditFromEGM_FB(Convert.ToInt64(Request.PosId), Request.CustomerId))
        {
          return SetBalanceInternal(Request);
        }
      }

      return false;
    } // TryTransferBalanceFromEGM

    /// <summary>
    /// Initialize transaction if request type is SetBalance
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RequestType"></param>
    /// <returns></returns>
    private Boolean InitializeNewTransaction(SetBalanceRequest Request, FBRequestType RequestType)
    {
      // Format amount request (Transaction must be created with correct amount)
      Request.Amount /= 100.00m; //amount in cent

      // Initialize new transactions
      if (RequestType != FBRequestType.SetBalance)
      {
        return true;
      }

      // Initialize FB transaction (Pending)
      if (!this.InitTransaction(Request))
      {
        Error.Message = "SetBalance.InitializeNewTransaction: Error on InitTransaction.";

        return false;
      }

      return true;
    } // InitializeNewTransaction

    /// <summary>
    /// Request data validation
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean RequestDataValidation(SetBalanceRequest Request)
    {
      // No validate request data if is trasaction rollback. This validate in Transaction Class.
      if (Request.Code == FBTransactionType.Rollback)
      {
        return true;
      }

      // AccountId
      if (Request.CustomerId <= 0)
      {
        Error.Code = FBErrorCode.InvalidAccount;
        Error.Message = "RequestDataValidation: AccountId is not valid.";

        Log.Message(Error.Message);

        return false;
      }

      // Pin
      if (PlayerInfoData.GetPinRequest() && String.IsNullOrEmpty(Request.Pin))
      {
        Error.Code = FBErrorCode.WrongAuth;
        Error.Message = "RequestDataValidation: Pin is not valid.";

        return false;
      }

      return true;

    } // RequestDataValidation

    /// <summary>
    /// Get/Set account balance internal
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean SetBalanceInternal(SetBalanceRequest Request)
    {
      PlayerInfoData _player_info;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Get account data
          if (!FBBusinessLogic.GetAccountData(Request.CustomerId, out _player_info, _db_trx.SqlTransaction))
          {
            Error = _player_info.Error;

            return false;
          }

          // Set PlayerInfo
          this.PlayerInfo = _player_info;

          // Request Account pin
          if (!CheckAccountPin(Request))
          {
            return false;
          }

          // Prepare AddAmount and SubAmount
          if (!CheckBalance(Request))
          {
            return false;
          }

          // Call to process credit operation
          if (!FBBusinessLogic.UpdateAccountBalance(Request, this.PlayerInfo, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Error.Code = FBErrorCode.GeneralError;
        Error.Message = "Error on SetBalanceInternal.";

        Log.Exception(_ex);
      }

      return false;

    } // SetBalanceInternal

    /// <summary>
    /// Check account pin
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean CheckAccountPin(SetBalanceRequest Request)
    {
      // No check in rollback
      if ( Request.Code == FBTransactionType.Rollback)
      {
        return true;
      }

      if (!PlayerInfoData.GetPinRequest())
      {
        return true;
      }

      // AccountPin
      if (Request.Pin != PlayerInfo.Pin) 
      {
        Error.Code = FBErrorCode.InvalidPin;
        Error.Message = "CheckAccountPin: Wrong Pin.";

        return false;
      }

      return true;
    } // CheckAccountPin

    /// <summary>
    /// Check balance 
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean CheckBalance(SetBalanceRequest Request)
    {
      switch (Request.Code)
      {
        case FBTransactionType.Subtract:
        {
          return CheckSubAmount(Request);
        }
        case FBTransactionType.Sum:
        {
          return CheckAddAmount(Request);
        }
        case FBTransactionType.Rollback:
        {
          // Checked in 'CheckTransactionAmounts'
          return true;
        }

        default:
        case FBTransactionType.None:
          Error.Code    = FBErrorCode.TransactionError;
          Error.Message = "Unknown transaction code.";
        break;
      }

      return false;

    } // CheckBalance

    /// <summary>
    /// Check Subtract Amount
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean CheckSubAmount(SetBalanceRequest Request)
    {
      // Amount
      if (PlayerInfo.Balance.TotalRedeemable < Request.Amount)
      {
        Error.Code    = FBErrorCode.NotEnoughBalance;
        Error.Message = "Not enough balance";

        return false;
      }

      // Points
      if (PlayerInfo.Points < Request.Points)
      {
        Error.Code = FBErrorCode.NotEnoughPoints;
        Error.Message = "Not enough points";

        return false;
      }

      return true;

    } // CheckSubAmount

    /// <summary>
    /// Check Add Amount
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean CheckAddAmount(SetBalanceRequest Request)
    {
      if (Request.Amount <= 0 && Request.Points <= 0)
      {
        Error.Code    = FBErrorCode.InvalidAmount;
        Error.Message = "Invalid amount";
      }

      return true;
    } // CheckAddAmount

    #region Gateway

    //------------------------------------------------------------------------------
    // PURPOSE: Transfer credit from EGM --> account --> GameGateWay
    // 

    /// <summary>
    /// Transfer credit from EGM --> account --> FB
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private Boolean TransferCreditFromEGM_FB(Int64 TerminalId, Int64 AccountId)
    {
      Int64 _time_out;
      Int64 _wcp_id;
      String _wcp_parameter;

      try
      {
        _time_out = GeneralParam.GetInt32("FB", "CreditTimeOut", 30, 10, 60);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Gateway.InsertGamegatewayCommand(TerminalId, out _wcp_id, _db_trx.SqlTransaction))
          {
            Error.Code    = FBErrorCode.GeneralError;
            Error.Message = "TransferCreditFromEGM_FB.InsertGamegatewayCommand";

            return false;
          }

          // Set wcp params
          _wcp_parameter = String.Format(GAMEGATEWAY_COMMAND, (Int16)GameGateway.TYPE_WCP.DEBIT, _wcp_id, AccountId, "NULL");

          if (!Gateway.UpdateGamegatewayCommand(_wcp_parameter, _wcp_id, true, GetCreditStatus.PENDING, _db_trx.SqlTransaction))
          {
            Error.Code    = FBErrorCode.GeneralError;
            Error.Message = "TransferCreditFromEGM_FB.UpdateGamegatewayCommand";

            return false;
          }

          _db_trx.Commit();
        }

        if (!Gateway.GetGamegatewayCommandResponse(_wcp_parameter, _wcp_id, _time_out))
        {
          Error.Code    = FBErrorCode.NotEnoughBalance;
          Error.Message = "TransferCreditFromEGM_FB. Not enough balance AccountId: " + AccountId.ToString();

          return false;
        }

        Error.Code = FBErrorCode.None;

        return true;     
      }
      catch (Exception _ex)
      {
        Error.Code = FBErrorCode.GeneralError;
        Error.Message = "Error TransferCreditFromEGM_FB";

        Log.Exception(_ex);
        Log.Message(Error.Message);
      }

      return false;
    } // TransferCreditFromEGM_FB

    #endregion

    #endregion

    #region " FB Transaction "

    /// <summary>
    /// Check transaction
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RequestType"></param>
    /// <returns></returns>
    private Boolean CheckTransaction(SetBalanceRequest Request, FBRequestType RequestType)
    {
      FBErrorResponse _error_response;

      // TransactionId
      if (String.IsNullOrEmpty(Request.TransactionId))
      {
        Error.Message = String.Format("SetBalance: TransactionId is null or empty.");

        return false;
      }

      // Check transaction by request type
      if(!CheckTransactionByRequestType(Request, RequestType))
      {
        return false;
      }
      
      // Check transaction
      if(!this.Transaction.CheckTransaction(Request, out _error_response))
      {
        Error = _error_response;

        return false;
      }

      return true;
    }

    /// <summary>
    /// Check transaction by request type
    /// </summary>
    /// <param name="Request"></param>
    /// <param name="RequestType"></param>
    /// <returns></returns>
    private Boolean CheckTransactionByRequestType(SetBalanceRequest Request, FBRequestType RequestType)
    {
      switch(RequestType)
      { 
        // Check SetBalance transaction
        case FBRequestType.SetBalance:
        {
          if (!CheckTransactionType_SetBalance(Request.Code))
          {
            Error.Message = String.Format("SetBalance: Invalid transaction code for SetBalance request. TransactionId {0}", Request.TransactionId);

            return false;
          }
        }
        break;

        // Check TrxCancel transaction
        case FBRequestType.TrxCancel:
        {
          if (!CheckTransactionType_TrxCancel(Request.Code))
          {
            Error.Message = String.Format("SetBalance: Invalid transaction code for TrxCancel request. TransactionId {0}", Request.TransactionId);

            return false;
          }
        }
        break;
      }

      return true;

    } // CheckTransactionByRequestType
    
    /// <summary>
    /// Base method for initializate transaction
    /// </summary>
    /// <param name="Request"></param>
    /// <returns></returns>
    private Boolean InitTransaction(SetBalanceRequest Request)
    {
      return this.Transaction.InitTransaction ( Request.TransactionId
                                              , Request.CustomerId
                                              , Request.Amount
                                              , Request.Points
                                              , Request.Code);

    } // InitTransaction
    
    /// <summary>
    /// Format amount request
    /// </summary>
    /// <param name="Request"></param>
    private void FormatAmountRequest(ref SetBalanceRequest Request)
    {
      Request.Amount /= 100.00m; //amount in cent
    } // FormatAmountRequest

    /// <summary>
    /// Start transaction: Update FB transaction status to FBTransactionStatus.Pending
    /// </summary>
    /// <param name="TransactionId"></param>
    /// <returns></returns>
    private Boolean StartTransaction(String TransactionId)
    {
      return this.Transaction.UpdateTransaction(TransactionId, FBTransactionStatus.Pending);
    } // StartTransaction
    private Boolean StartTransaction(String TransactionId, SqlTransaction SqlTrx)
    {
      return this.Transaction.UpdateTransaction(TransactionId, FBTransactionStatus.Pending, SqlTrx);
    } // StartTransaction

    /// <summary>
    /// End success transaction: Update FB transaction status to FBTransactionStatus.Done
    /// </summary>
    /// <param name="TransactionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean EndSuccessTransaction(String TransactionId, FBTransactionType Type)
    {
      FBTransactionStatus _trx_status;

      _trx_status = (Type == FBTransactionType.Rollback) ? FBTransactionStatus.Cancel : FBTransactionStatus.Done;

      return this.Transaction.UpdateTransaction(TransactionId, _trx_status);
    } // EndSuccessTransaction

    /// <summary>
    /// End error transaction: Update FB transaction status to FBTransactionStatus.Error
    /// </summary>
    /// <param name="TransactionId"></param>
    /// <returns></returns>
    private Boolean EndErrorTransaction(String TransactionId)
    {
      return this.Transaction.UpdateTransaction(TransactionId, FBTransactionStatus.Error);
    } // EndErrorTransaction

    #endregion

  }
}