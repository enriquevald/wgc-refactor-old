namespace LCD_Simulator
{
  partial class frm_lcd_simulator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_exit = new System.Windows.Forms.Button();
      this.tab_simulator = new System.Windows.Forms.TabControl();
      this.tb_interface_lcd = new System.Windows.Forms.TabPage();
      this.gb_error_msg = new System.Windows.Forms.GroupBox();
      this.cmb_type_msg = new System.Windows.Forms.ComboBox();
      this.btn_send_msg = new System.Windows.Forms.Button();
      this.txt_msg = new System.Windows.Forms.TextBox();
      this.gb_events = new System.Windows.Forms.GroupBox();
      this.lbl_tito_hide_CA = new System.Windows.Forms.Label();
      this.tb_tito_hide_CA = new System.Windows.Forms.TextBox();
      this.btn_cash_out = new System.Windows.Forms.Button();
      this.gb_mode = new System.Windows.Forms.GroupBox();
      this.tb_points = new System.Windows.Forms.TextBox();
      this.lbl_points = new System.Windows.Forms.Label();
      this.tb_cash = new System.Windows.Forms.TextBox();
      this.lbl_cash = new System.Windows.Forms.Label();
      this.rb_cashless = new System.Windows.Forms.RadioButton();
      this.rb_tito_mode = new System.Windows.Forms.RadioButton();
      this.gb_send_marquee_msg = new System.Windows.Forms.GroupBox();
      this.btn_marquee_send = new System.Windows.Forms.Button();
      this.txt_marquee_msg = new System.Windows.Forms.TextBox();
      this.gb_jackpot_configuration = new System.Windows.Forms.GroupBox();
      this.btn_jackpot_apply = new System.Windows.Forms.Button();
      this.tb_jackpot_interval = new System.Windows.Forms.TextBox();
      this.lb_interval = new System.Windows.Forms.Label();
      this.gb_card_operations = new System.Windows.Forms.GroupBox();
      this.btn_insert_card = new System.Windows.Forms.Button();
      this.btn_extract_card = new System.Windows.Forms.Button();
      this.tb_lcd_item_request = new System.Windows.Forms.TabPage();
      this.gb_requests = new System.Windows.Forms.GroupBox();
      this.lbl_name = new System.Windows.Forms.Label();
      this.btn_return_KO = new System.Windows.Forms.Button();
      this.btn_return_OK = new System.Windows.Forms.Button();
      this.lbl_item_amount = new System.Windows.Forms.Label();
      this.lbl_item_name = new System.Windows.Forms.Label();
      this.lbl_description = new System.Windows.Forms.Label();
      this.pb_item_image = new System.Windows.Forms.PictureBox();
      this.lbl_item_description = new System.Windows.Forms.Label();
      this.lbl_amount = new System.Windows.Forms.Label();
      this.lbl_timeout = new System.Windows.Forms.Label();
      this.btn_operator = new System.Windows.Forms.Button();
      this.btn_logger = new System.Windows.Forms.Button();
      this.pnl_operator = new System.Windows.Forms.Panel();
      this.tab_simulator.SuspendLayout();
      this.tb_interface_lcd.SuspendLayout();
      this.gb_error_msg.SuspendLayout();
      this.gb_events.SuspendLayout();
      this.gb_mode.SuspendLayout();
      this.gb_send_marquee_msg.SuspendLayout();
      this.gb_jackpot_configuration.SuspendLayout();
      this.gb_card_operations.SuspendLayout();
      this.tb_lcd_item_request.SuspendLayout();
      this.gb_requests.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_item_image)).BeginInit();
      this.pnl_operator.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_exit
      // 
      this.btn_exit.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.btn_exit.Location = new System.Drawing.Point(0, 51);
      this.btn_exit.Name = "btn_exit";
      this.btn_exit.Size = new System.Drawing.Size(469, 43);
      this.btn_exit.TabIndex = 2;
      this.btn_exit.Text = "Exit";
      this.btn_exit.UseVisualStyleBackColor = true;
      this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
      // 
      // tab_simulator
      // 
      this.tab_simulator.Controls.Add(this.tb_interface_lcd);
      this.tab_simulator.Controls.Add(this.tb_lcd_item_request);
      this.tab_simulator.Location = new System.Drawing.Point(1, 6);
      this.tab_simulator.Name = "tab_simulator";
      this.tab_simulator.SelectedIndex = 0;
      this.tab_simulator.Size = new System.Drawing.Size(468, 478);
      this.tab_simulator.TabIndex = 8;
      // 
      // tb_interface_lcd
      // 
      this.tb_interface_lcd.Controls.Add(this.gb_error_msg);
      this.tb_interface_lcd.Controls.Add(this.gb_events);
      this.tb_interface_lcd.Controls.Add(this.gb_mode);
      this.tb_interface_lcd.Controls.Add(this.gb_send_marquee_msg);
      this.tb_interface_lcd.Controls.Add(this.gb_jackpot_configuration);
      this.tb_interface_lcd.Controls.Add(this.gb_card_operations);
      this.tb_interface_lcd.Location = new System.Drawing.Point(4, 22);
      this.tb_interface_lcd.Name = "tb_interface_lcd";
      this.tb_interface_lcd.Padding = new System.Windows.Forms.Padding(3);
      this.tb_interface_lcd.Size = new System.Drawing.Size(460, 452);
      this.tb_interface_lcd.TabIndex = 0;
      this.tb_interface_lcd.Text = "Interface with LCD";
      this.tb_interface_lcd.UseVisualStyleBackColor = true;
      // 
      // gb_error_msg
      // 
      this.gb_error_msg.Controls.Add(this.cmb_type_msg);
      this.gb_error_msg.Controls.Add(this.btn_send_msg);
      this.gb_error_msg.Controls.Add(this.txt_msg);
      this.gb_error_msg.Location = new System.Drawing.Point(6, 138);
      this.gb_error_msg.Name = "gb_error_msg";
      this.gb_error_msg.Size = new System.Drawing.Size(448, 153);
      this.gb_error_msg.TabIndex = 11;
      this.gb_error_msg.TabStop = false;
      this.gb_error_msg.Text = "Send Message";
      // 
      // cmb_type_msg
      // 
      this.cmb_type_msg.FormattingEnabled = true;
      this.cmb_type_msg.Location = new System.Drawing.Point(6, 19);
      this.cmb_type_msg.Name = "cmb_type_msg";
      this.cmb_type_msg.Size = new System.Drawing.Size(141, 21);
      this.cmb_type_msg.TabIndex = 6;
      // 
      // btn_send_msg
      // 
      this.btn_send_msg.Location = new System.Drawing.Point(153, 17);
      this.btn_send_msg.Name = "btn_send_msg";
      this.btn_send_msg.Size = new System.Drawing.Size(116, 23);
      this.btn_send_msg.TabIndex = 5;
      this.btn_send_msg.Text = "Send";
      this.btn_send_msg.UseVisualStyleBackColor = true;
      this.btn_send_msg.Click += new System.EventHandler(this.btn_send_msg_Click);
      // 
      // txt_msg
      // 
      this.txt_msg.Location = new System.Drawing.Point(6, 46);
      this.txt_msg.Multiline = true;
      this.txt_msg.Name = "txt_msg";
      this.txt_msg.Size = new System.Drawing.Size(436, 101);
      this.txt_msg.TabIndex = 0;
      this.txt_msg.Text = "xMessage";
      // 
      // gb_events
      // 
      this.gb_events.Controls.Add(this.lbl_tito_hide_CA);
      this.gb_events.Controls.Add(this.tb_tito_hide_CA);
      this.gb_events.Controls.Add(this.btn_cash_out);
      this.gb_events.Location = new System.Drawing.Point(324, 297);
      this.gb_events.Name = "gb_events";
      this.gb_events.Size = new System.Drawing.Size(130, 85);
      this.gb_events.TabIndex = 13;
      this.gb_events.TabStop = false;
      this.gb_events.Text = "TITO Event";
      // 
      // lbl_tito_hide_CA
      // 
      this.lbl_tito_hide_CA.AutoSize = true;
      this.lbl_tito_hide_CA.Location = new System.Drawing.Point(15, 41);
      this.lbl_tito_hide_CA.Name = "lbl_tito_hide_CA";
      this.lbl_tito_hide_CA.Size = new System.Drawing.Size(98, 13);
      this.lbl_tito_hide_CA.TabIndex = 5;
      this.lbl_tito_hide_CA.Text = "Hide Call Attendant";
      // 
      // tb_tito_hide_CA
      // 
      this.tb_tito_hide_CA.Location = new System.Drawing.Point(6, 59);
      this.tb_tito_hide_CA.Name = "tb_tito_hide_CA";
      this.tb_tito_hide_CA.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.tb_tito_hide_CA.Size = new System.Drawing.Size(116, 20);
      this.tb_tito_hide_CA.TabIndex = 4;
      this.tb_tito_hide_CA.Text = "1";
      // 
      // btn_cash_out
      // 
      this.btn_cash_out.Location = new System.Drawing.Point(6, 15);
      this.btn_cash_out.Name = "btn_cash_out";
      this.btn_cash_out.Size = new System.Drawing.Size(116, 23);
      this.btn_cash_out.TabIndex = 3;
      this.btn_cash_out.Text = "Cash Out";
      this.btn_cash_out.UseVisualStyleBackColor = true;
      this.btn_cash_out.Click += new System.EventHandler(this.btn_cash_out_Click);
      // 
      // gb_mode
      // 
      this.gb_mode.Controls.Add(this.tb_points);
      this.gb_mode.Controls.Add(this.lbl_points);
      this.gb_mode.Controls.Add(this.tb_cash);
      this.gb_mode.Controls.Add(this.lbl_cash);
      this.gb_mode.Controls.Add(this.rb_cashless);
      this.gb_mode.Controls.Add(this.rb_tito_mode);
      this.gb_mode.Enabled = false;
      this.gb_mode.Location = new System.Drawing.Point(6, 297);
      this.gb_mode.Name = "gb_mode";
      this.gb_mode.Size = new System.Drawing.Size(312, 85);
      this.gb_mode.TabIndex = 12;
      this.gb_mode.TabStop = false;
      this.gb_mode.Text = "Mode";
      // 
      // tb_points
      // 
      this.tb_points.Location = new System.Drawing.Point(168, 18);
      this.tb_points.Name = "tb_points";
      this.tb_points.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.tb_points.Size = new System.Drawing.Size(138, 20);
      this.tb_points.TabIndex = 5;
      // 
      // lbl_points
      // 
      this.lbl_points.AutoSize = true;
      this.lbl_points.Location = new System.Drawing.Point(120, 23);
      this.lbl_points.Name = "lbl_points";
      this.lbl_points.Size = new System.Drawing.Size(42, 13);
      this.lbl_points.TabIndex = 4;
      this.lbl_points.Text = "Points :";
      // 
      // tb_cash
      // 
      this.tb_cash.Enabled = false;
      this.tb_cash.Location = new System.Drawing.Point(168, 52);
      this.tb_cash.Name = "tb_cash";
      this.tb_cash.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.tb_cash.Size = new System.Drawing.Size(138, 20);
      this.tb_cash.TabIndex = 3;
      // 
      // lbl_cash
      // 
      this.lbl_cash.AutoSize = true;
      this.lbl_cash.Location = new System.Drawing.Point(125, 54);
      this.lbl_cash.Name = "lbl_cash";
      this.lbl_cash.Size = new System.Drawing.Size(37, 13);
      this.lbl_cash.TabIndex = 2;
      this.lbl_cash.Text = "Cash :";
      // 
      // rb_cashless
      // 
      this.rb_cashless.AutoSize = true;
      this.rb_cashless.Location = new System.Drawing.Point(11, 52);
      this.rb_cashless.Name = "rb_cashless";
      this.rb_cashless.Size = new System.Drawing.Size(97, 17);
      this.rb_cashless.TabIndex = 1;
      this.rb_cashless.Text = "Cashless Mode";
      this.rb_cashless.UseVisualStyleBackColor = true;
      this.rb_cashless.CheckedChanged += new System.EventHandler(this.rb_mode_CheckedChanged);
      // 
      // rb_tito_mode
      // 
      this.rb_tito_mode.AutoSize = true;
      this.rb_tito_mode.Checked = true;
      this.rb_tito_mode.Location = new System.Drawing.Point(12, 21);
      this.rb_tito_mode.Name = "rb_tito_mode";
      this.rb_tito_mode.Size = new System.Drawing.Size(80, 17);
      this.rb_tito_mode.TabIndex = 0;
      this.rb_tito_mode.TabStop = true;
      this.rb_tito_mode.Text = "TITO Mode";
      this.rb_tito_mode.UseVisualStyleBackColor = true;
      this.rb_tito_mode.CheckedChanged += new System.EventHandler(this.rb_mode_CheckedChanged);
      // 
      // gb_send_marquee_msg
      // 
      this.gb_send_marquee_msg.Controls.Add(this.btn_marquee_send);
      this.gb_send_marquee_msg.Controls.Add(this.txt_marquee_msg);
      this.gb_send_marquee_msg.Location = new System.Drawing.Point(6, 80);
      this.gb_send_marquee_msg.Name = "gb_send_marquee_msg";
      this.gb_send_marquee_msg.Size = new System.Drawing.Size(448, 52);
      this.gb_send_marquee_msg.TabIndex = 10;
      this.gb_send_marquee_msg.TabStop = false;
      this.gb_send_marquee_msg.Text = "Send Marquee Message";
      // 
      // btn_marquee_send
      // 
      this.btn_marquee_send.Location = new System.Drawing.Point(365, 21);
      this.btn_marquee_send.Name = "btn_marquee_send";
      this.btn_marquee_send.Size = new System.Drawing.Size(75, 23);
      this.btn_marquee_send.TabIndex = 4;
      this.btn_marquee_send.Text = "Send";
      this.btn_marquee_send.UseVisualStyleBackColor = true;
      this.btn_marquee_send.Click += new System.EventHandler(this.btn_marquee_send_Click);
      // 
      // txt_marquee_msg
      // 
      this.txt_marquee_msg.Location = new System.Drawing.Point(6, 23);
      this.txt_marquee_msg.Name = "txt_marquee_msg";
      this.txt_marquee_msg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.txt_marquee_msg.Size = new System.Drawing.Size(353, 20);
      this.txt_marquee_msg.TabIndex = 0;
      this.txt_marquee_msg.Text = "xMarqueeMessage";
      this.txt_marquee_msg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // gb_jackpot_configuration
      // 
      this.gb_jackpot_configuration.Controls.Add(this.btn_jackpot_apply);
      this.gb_jackpot_configuration.Controls.Add(this.tb_jackpot_interval);
      this.gb_jackpot_configuration.Controls.Add(this.lb_interval);
      this.gb_jackpot_configuration.Location = new System.Drawing.Point(6, 388);
      this.gb_jackpot_configuration.Name = "gb_jackpot_configuration";
      this.gb_jackpot_configuration.Size = new System.Drawing.Size(448, 58);
      this.gb_jackpot_configuration.TabIndex = 9;
      this.gb_jackpot_configuration.TabStop = false;
      this.gb_jackpot_configuration.Text = "Jackpot Configuration";
      // 
      // btn_jackpot_apply
      // 
      this.btn_jackpot_apply.Location = new System.Drawing.Point(299, 23);
      this.btn_jackpot_apply.Name = "btn_jackpot_apply";
      this.btn_jackpot_apply.Size = new System.Drawing.Size(75, 23);
      this.btn_jackpot_apply.TabIndex = 2;
      this.btn_jackpot_apply.Text = "Apply";
      this.btn_jackpot_apply.UseVisualStyleBackColor = true;
      this.btn_jackpot_apply.Click += new System.EventHandler(this.btn_jackpot_apply_Click);
      // 
      // tb_jackpot_interval
      // 
      this.tb_jackpot_interval.Location = new System.Drawing.Point(153, 24);
      this.tb_jackpot_interval.Name = "tb_jackpot_interval";
      this.tb_jackpot_interval.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.tb_jackpot_interval.Size = new System.Drawing.Size(134, 20);
      this.tb_jackpot_interval.TabIndex = 1;
      // 
      // lb_interval
      // 
      this.lb_interval.AutoSize = true;
      this.lb_interval.Location = new System.Drawing.Point(70, 26);
      this.lb_interval.Name = "lb_interval";
      this.lb_interval.Size = new System.Drawing.Size(77, 13);
      this.lb_interval.TabIndex = 0;
      this.lb_interval.Text = "Interval (seg.) :";
      // 
      // gb_card_operations
      // 
      this.gb_card_operations.Controls.Add(this.btn_insert_card);
      this.gb_card_operations.Controls.Add(this.btn_extract_card);
      this.gb_card_operations.Location = new System.Drawing.Point(6, 6);
      this.gb_card_operations.Name = "gb_card_operations";
      this.gb_card_operations.Size = new System.Drawing.Size(448, 68);
      this.gb_card_operations.TabIndex = 8;
      this.gb_card_operations.TabStop = false;
      this.gb_card_operations.Text = "Card Operations";
      // 
      // btn_insert_card
      // 
      this.btn_insert_card.Location = new System.Drawing.Point(6, 19);
      this.btn_insert_card.Name = "btn_insert_card";
      this.btn_insert_card.Size = new System.Drawing.Size(214, 43);
      this.btn_insert_card.TabIndex = 0;
      this.btn_insert_card.Text = "Insert Card";
      this.btn_insert_card.UseVisualStyleBackColor = true;
      this.btn_insert_card.Click += new System.EventHandler(this.btn_insert_card_Click);
      // 
      // btn_extract_card
      // 
      this.btn_extract_card.Enabled = false;
      this.btn_extract_card.Location = new System.Drawing.Point(228, 19);
      this.btn_extract_card.Name = "btn_extract_card";
      this.btn_extract_card.Size = new System.Drawing.Size(214, 43);
      this.btn_extract_card.TabIndex = 1;
      this.btn_extract_card.Text = "Extract Card";
      this.btn_extract_card.UseVisualStyleBackColor = true;
      this.btn_extract_card.Click += new System.EventHandler(this.btn_extract_card_Click);
      // 
      // tb_lcd_item_request
      // 
      this.tb_lcd_item_request.Controls.Add(this.gb_requests);
      this.tb_lcd_item_request.Controls.Add(this.lbl_timeout);
      this.tb_lcd_item_request.Location = new System.Drawing.Point(4, 22);
      this.tb_lcd_item_request.Name = "tb_lcd_item_request";
      this.tb_lcd_item_request.Padding = new System.Windows.Forms.Padding(3);
      this.tb_lcd_item_request.Size = new System.Drawing.Size(460, 452);
      this.tb_lcd_item_request.TabIndex = 1;
      this.tb_lcd_item_request.Text = "LCD Item Request";
      this.tb_lcd_item_request.UseVisualStyleBackColor = true;
      // 
      // gb_requests
      // 
      this.gb_requests.Controls.Add(this.lbl_name);
      this.gb_requests.Controls.Add(this.btn_return_KO);
      this.gb_requests.Controls.Add(this.btn_return_OK);
      this.gb_requests.Controls.Add(this.lbl_item_amount);
      this.gb_requests.Controls.Add(this.lbl_item_name);
      this.gb_requests.Controls.Add(this.lbl_description);
      this.gb_requests.Controls.Add(this.pb_item_image);
      this.gb_requests.Controls.Add(this.lbl_item_description);
      this.gb_requests.Controls.Add(this.lbl_amount);
      this.gb_requests.Location = new System.Drawing.Point(2, 6);
      this.gb_requests.Name = "gb_requests";
      this.gb_requests.Size = new System.Drawing.Size(454, 443);
      this.gb_requests.TabIndex = 0;
      this.gb_requests.TabStop = false;
      this.gb_requests.Text = "Request";
      // 
      // lbl_name
      // 
      this.lbl_name.AutoSize = true;
      this.lbl_name.Location = new System.Drawing.Point(6, 63);
      this.lbl_name.Name = "lbl_name";
      this.lbl_name.Size = new System.Drawing.Size(38, 13);
      this.lbl_name.TabIndex = 0;
      this.lbl_name.Text = "Name:";
      this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btn_return_KO
      // 
      this.btn_return_KO.Location = new System.Drawing.Point(232, 15);
      this.btn_return_KO.Name = "btn_return_KO";
      this.btn_return_KO.Size = new System.Drawing.Size(168, 27);
      this.btn_return_KO.TabIndex = 12;
      this.btn_return_KO.Text = "Return KO";
      this.btn_return_KO.UseVisualStyleBackColor = true;
      this.btn_return_KO.Click += new System.EventHandler(this.btn_return_KO_Click);
      // 
      // btn_return_OK
      // 
      this.btn_return_OK.Location = new System.Drawing.Point(56, 15);
      this.btn_return_OK.Name = "btn_return_OK";
      this.btn_return_OK.Size = new System.Drawing.Size(168, 27);
      this.btn_return_OK.TabIndex = 11;
      this.btn_return_OK.Text = "Return OK";
      this.btn_return_OK.UseVisualStyleBackColor = true;
      this.btn_return_OK.Click += new System.EventHandler(this.btn_return_OK_Click);
      // 
      // lbl_item_amount
      // 
      this.lbl_item_amount.Location = new System.Drawing.Point(6, 120);
      this.lbl_item_amount.Name = "lbl_item_amount";
      this.lbl_item_amount.Size = new System.Drawing.Size(195, 22);
      this.lbl_item_amount.TabIndex = 16;
      // 
      // lbl_item_name
      // 
      this.lbl_item_name.Location = new System.Drawing.Point(6, 76);
      this.lbl_item_name.Name = "lbl_item_name";
      this.lbl_item_name.Size = new System.Drawing.Size(201, 22);
      this.lbl_item_name.TabIndex = 15;
      // 
      // lbl_description
      // 
      this.lbl_description.AutoSize = true;
      this.lbl_description.Location = new System.Drawing.Point(6, 253);
      this.lbl_description.Name = "lbl_description";
      this.lbl_description.Size = new System.Drawing.Size(63, 13);
      this.lbl_description.TabIndex = 14;
      this.lbl_description.Text = "Description:";
      this.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pb_item_image
      // 
      this.pb_item_image.Location = new System.Drawing.Point(214, 63);
      this.pb_item_image.Name = "pb_item_image";
      this.pb_item_image.Size = new System.Drawing.Size(234, 200);
      this.pb_item_image.TabIndex = 13;
      this.pb_item_image.TabStop = false;
      // 
      // lbl_item_description
      // 
      this.lbl_item_description.Location = new System.Drawing.Point(6, 266);
      this.lbl_item_description.Name = "lbl_item_description";
      this.lbl_item_description.Size = new System.Drawing.Size(442, 165);
      this.lbl_item_description.TabIndex = 0;
      // 
      // lbl_amount
      // 
      this.lbl_amount.AutoSize = true;
      this.lbl_amount.Location = new System.Drawing.Point(6, 107);
      this.lbl_amount.Name = "lbl_amount";
      this.lbl_amount.Size = new System.Drawing.Size(46, 13);
      this.lbl_amount.TabIndex = 0;
      this.lbl_amount.Text = "Amount:";
      this.lbl_amount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_timeout
      // 
      this.lbl_timeout.AutoSize = true;
      this.lbl_timeout.BackColor = System.Drawing.Color.White;
      this.lbl_timeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 48.25F);
      this.lbl_timeout.ForeColor = System.Drawing.Color.DarkRed;
      this.lbl_timeout.Location = new System.Drawing.Point(65, 146);
      this.lbl_timeout.Name = "lbl_timeout";
      this.lbl_timeout.Size = new System.Drawing.Size(325, 74);
      this.lbl_timeout.TabIndex = 10;
      this.lbl_timeout.Text = "TIMEOUT";
      // 
      // btn_operator
      // 
      this.btn_operator.Location = new System.Drawing.Point(0, 5);
      this.btn_operator.Name = "btn_operator";
      this.btn_operator.Size = new System.Drawing.Size(231, 43);
      this.btn_operator.TabIndex = 9;
      this.btn_operator.Text = "Operator Panel";
      this.btn_operator.UseVisualStyleBackColor = true;
      this.btn_operator.Click += new System.EventHandler(this.btn_operator_Click);
      // 
      // btn_logger
      // 
      this.btn_logger.Location = new System.Drawing.Point(239, 5);
      this.btn_logger.Name = "btn_logger";
      this.btn_logger.Size = new System.Drawing.Size(230, 43);
      this.btn_logger.TabIndex = 10;
      this.btn_logger.Text = "Open Log Folder";
      this.btn_logger.UseVisualStyleBackColor = true;
      this.btn_logger.Click += new System.EventHandler(this.btn_logger_Click);
      // 
      // pnl_operator
      // 
      this.pnl_operator.Controls.Add(this.btn_logger);
      this.pnl_operator.Controls.Add(this.btn_operator);
      this.pnl_operator.Controls.Add(this.btn_exit);
      this.pnl_operator.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_operator.Location = new System.Drawing.Point(0, 483);
      this.pnl_operator.Name = "pnl_operator";
      this.pnl_operator.Size = new System.Drawing.Size(469, 94);
      this.pnl_operator.TabIndex = 9;
      // 
      // frm_lcd_simulator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(469, 577);
      this.Controls.Add(this.pnl_operator);
      this.Controls.Add(this.tab_simulator);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "frm_lcd_simulator";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "LCD Simulator";
      this.Load += new System.EventHandler(this.frm_lcd_simulator_Load);
      this.tab_simulator.ResumeLayout(false);
      this.tb_interface_lcd.ResumeLayout(false);
      this.gb_error_msg.ResumeLayout(false);
      this.gb_error_msg.PerformLayout();
      this.gb_events.ResumeLayout(false);
      this.gb_events.PerformLayout();
      this.gb_mode.ResumeLayout(false);
      this.gb_mode.PerformLayout();
      this.gb_send_marquee_msg.ResumeLayout(false);
      this.gb_send_marquee_msg.PerformLayout();
      this.gb_jackpot_configuration.ResumeLayout(false);
      this.gb_jackpot_configuration.PerformLayout();
      this.gb_card_operations.ResumeLayout(false);
      this.tb_lcd_item_request.ResumeLayout(false);
      this.tb_lcd_item_request.PerformLayout();
      this.gb_requests.ResumeLayout(false);
      this.gb_requests.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_item_image)).EndInit();
      this.pnl_operator.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btn_exit;
    private System.Windows.Forms.TabControl tab_simulator;
    private System.Windows.Forms.TabPage tb_interface_lcd;
    private System.Windows.Forms.GroupBox gb_error_msg;
    private System.Windows.Forms.ComboBox cmb_type_msg;
    private System.Windows.Forms.Button btn_send_msg;
    private System.Windows.Forms.TextBox txt_msg;
    private System.Windows.Forms.GroupBox gb_events;
    private System.Windows.Forms.Label lbl_tito_hide_CA;
    private System.Windows.Forms.TextBox tb_tito_hide_CA;
    private System.Windows.Forms.Button btn_cash_out;
    private System.Windows.Forms.GroupBox gb_mode;
    private System.Windows.Forms.TextBox tb_points;
    private System.Windows.Forms.Label lbl_points;
    private System.Windows.Forms.TextBox tb_cash;
    private System.Windows.Forms.Label lbl_cash;
    private System.Windows.Forms.RadioButton rb_cashless;
    private System.Windows.Forms.RadioButton rb_tito_mode;
    private System.Windows.Forms.GroupBox gb_send_marquee_msg;
    private System.Windows.Forms.Button btn_marquee_send;
    private System.Windows.Forms.TextBox txt_marquee_msg;
    private System.Windows.Forms.GroupBox gb_jackpot_configuration;
    private System.Windows.Forms.Button btn_jackpot_apply;
    private System.Windows.Forms.TextBox tb_jackpot_interval;
    private System.Windows.Forms.Label lb_interval;
    private System.Windows.Forms.GroupBox gb_card_operations;
    private System.Windows.Forms.Button btn_insert_card;
    private System.Windows.Forms.Button btn_extract_card;
    private System.Windows.Forms.TabPage tb_lcd_item_request;
    private System.Windows.Forms.Button btn_operator;
    private System.Windows.Forms.Button btn_logger;
    private System.Windows.Forms.GroupBox gb_requests;
    private System.Windows.Forms.Label lbl_item_amount;
    private System.Windows.Forms.Label lbl_item_name;
    private System.Windows.Forms.Label lbl_description;
    private System.Windows.Forms.PictureBox pb_item_image;
    private System.Windows.Forms.Button btn_return_KO;
    private System.Windows.Forms.Button btn_return_OK;
    private System.Windows.Forms.Label lbl_item_description;
    private System.Windows.Forms.Label lbl_amount;
    private System.Windows.Forms.Label lbl_name;
    private System.Windows.Forms.Panel pnl_operator;
    private System.Windows.Forms.Label lbl_timeout;
  }
}

