//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_lcd_simulator.cs
// 
//   DESCRIPTION: LCD Simulator Form. 
// 
//        AUTHOR: Carlos C�ceres.
// 
// CREATION DATE: 20-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAY-2014 CCG    First release.
// 29-MAY-2014 JBP    Added message simulator.
// 03-JUN-2014 JBP    Added LCD->Simulator Interface.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using LCD_Display;

namespace LCD_Simulator
{
  public partial class frm_lcd_simulator : Form
  {
    #region Enums

    private enum TYPE_SIMULATOR_TAB
    {
      INTERFACE = 0,
      REQUESTS = 1
    }

    #endregion

    #region Private variables

    private ILCD m_ilcd;
    private Timer m_timeout_timer;

    #endregion

    #region Properties

    public ILCD iLCD
    {
      get { return this.m_ilcd; }
      set { this.m_ilcd = value; }
    }

    public Timer TimeoutTimer
    {
      get { return this.m_timeout_timer; }
      set { this.m_timeout_timer = value; }
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public frm_lcd_simulator()
    {
      InitializeComponent();
      InitializeControls();
    }

    #endregion

    #region Private Functions

    #region Simulator --> LCD

    //------------------------------------------------------------------------------
    // PURPOSE: Bind type message combobox
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void BindTypeMessageComboBox()
    {
      BindingList<ComboBoxItem> _message_types;

      _message_types = new BindingList<ComboBoxItem>();
      _message_types.Add(new ComboBoxItem(TYPE_MESSAGE.ERROR, "Error"));
      _message_types.Add(new ComboBoxItem(TYPE_MESSAGE.INFO, "Information"));
      _message_types.Add(new ComboBoxItem(TYPE_MESSAGE.WARNING, "Warning"));
      _message_types.Add(new ComboBoxItem(TYPE_MESSAGE.SUCCESS, "Success"));

      this.cmb_type_msg.ValueMember = null;
      this.cmb_type_msg.DisplayMember = "Name";
      this.cmb_type_msg.DataSource = _message_types;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert card event
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void InsertCard()
    {
      String _parameters;
      _parameters = String.Empty;

      try
      {
        this.EnableInsertCardButtons(false);
        this.iLCD.MsgInterface((int)(int)TYPE_EVENT.INSERT_CARD, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred inserting card into LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Extract card event
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void ExtractCard()
    {
      String _parameters;
      _parameters = String.Empty;

      try
      {
        this.EnableInsertCardButtons(true);
        this.iLCD.MsgInterface((int)TYPE_EVENT.EXTRACT_CARD, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred extracting card from LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set Jackpot Interval
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SetJackpotInterval()
    {
      String _parameters;
      _parameters = String.Empty;

      try
      {
        _parameters = this.tb_jackpot_interval.Text;
        this.iLCD.MsgInterface((int)TYPE_EVENT.JACKPOT_INTERVAL, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred setting Jackpot Interval in LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set TITO/CASHLES mode
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SetMode()
    {
      try
      {
        this.btn_cash_out.Enabled = this.rb_tito_mode.Checked;
        this.tb_cash.Enabled = !this.rb_tito_mode.Checked;
        this.tb_tito_hide_CA.Enabled = this.rb_tito_mode.Checked;


        //this.SetInitMode(this.rb_tito_mode.Checked); //TODO: JCA REVISAR    [Se tendr�a que cerrar el LCD y abrir con el nuevo modo????]
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred setting mode in LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send marquee message
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SendMarqueeMessage()
    {
      String _parameters;
      _parameters = String.Empty;

      try
      {
        _parameters = this.txt_marquee_msg.Text;
        this.iLCD.MsgInterface((int)TYPE_EVENT.MARQUEE_MSG, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred sending marquee message to LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send cashout message
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SendCashOut()
    {
      String _parameters;
      _parameters = String.Empty;


      try
      {
        _parameters = "" + (Convert.ToInt32(this.tb_tito_hide_CA.Text) * 1000);
        this.iLCD.MsgInterface((int)TYPE_EVENT.CASH_OUT_EVENT, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred sending CashOut to LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send message and show into LCD
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SendMessage()
    {
      String _parameters;
      _parameters = String.Empty;


      try
      {
        _parameters = (((ComboBoxItem)this.cmb_type_msg.SelectedItem).m_value).ToString();
        _parameters += "|" + this.txt_msg.Text;

        this.iLCD.MsgInterface((int)TYPE_EVENT.SHOW_MESSAGE, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred sending message to LCD: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Enable/Disable Insert/Extract card buttons
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void EnableInsertCardButtons(Boolean Enabled)
    {
      this.btn_insert_card.Enabled = Enabled;
      this.btn_extract_card.Enabled = !Enabled;
    }

    #endregion

    #region LCD --> Simulator

    //------------------------------------------------------------------------------
    // PURPOSE: Enable/Disable Response buttons
    //
    //  PARAMS:
    //      - INPUT:
    //           - Boolean: Enabled
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void EnalbeDisableRequest(Boolean Enabled, TYPE_SIMULATOR_TAB TabActive)
    {
      this.gb_requests.Enabled = Enabled;
      this.pnl_operator.Enabled = !Enabled;

      this.tab_simulator.SelectedTab = this.tab_simulator.TabPages[(Int32)TabActive];
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send response to LCD
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SendResponse(TYPE_RESPONSE Response)
    {
      String _parameters;
      _parameters = String.Empty;


      try
      {
        _parameters = ((TYPE_RESPONSE)Response).ToString();

        this.iLCD.MsgInterface((int)TYPE_EVENT.SEND_RESPONSE, _parameters);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred sending response: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      this.EnalbeDisableRequest(false, TYPE_SIMULATOR_TAB.INTERFACE);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get request form LCD
    //
    //  PARAMS:
    //      - INPUT:
    //           - LCD_ITEM: ItemRequest
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void GetRequest(LCD_ITEM ItemRequest)
    {
      try
      {
        this.lbl_timeout.SendToBack();

        this.lbl_item_name.Text = ItemRequest.Name;
        this.lbl_item_amount.Text = ItemRequest.Amount.ToString();
        this.lbl_item_description.Text = ItemRequest.Description;
        this.pb_item_image.Image = ItemRequest.Image;

        this.EnalbeDisableRequest(true, TYPE_SIMULATOR_TAB.REQUESTS);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred opening operator panel: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show request timeout label
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void RequestTimeout()
    {
      try
      {
        this.ShowTimeoutLabel();
        EnalbeDisableRequest(false, TYPE_SIMULATOR_TAB.REQUESTS);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred showing timeout panel: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show and start request timeout label
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void ShowTimeoutLabel()
    {
      try
      {
        this.TimeoutTimer = new Timer();
        this.TimeoutTimer.Tick += new EventHandler(this.TimeoutTimer_Tick);
        this.TimeoutTimer.Interval = 10000;

        this.TimeoutTimer.Start();
        this.lbl_timeout.BringToFront();

        this.EnalbeDisableRequest(false, TYPE_SIMULATOR_TAB.REQUESTS);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred showing timeout panel: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Hide request timeout label
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void HideTimeoutLabel()
    {
      try
      {
        this.lbl_timeout.SendToBack();
        this.TimeoutTimer.Stop();
        this.EnalbeDisableRequest(false, TYPE_SIMULATOR_TAB.REQUESTS);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred hidding timeout panel: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    #endregion

    #region Common

    //------------------------------------------------------------------------------
    // PURPOSE: Show operator panel
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void OpenOperatorPanel()
    {
      try
      {
        this.iLCD.MsgInterface((int)TYPE_EVENT.OPERATOR_PANEL, null);
      }
      catch (Exception _ex)
      {
        MessageBox.Show("And error ocurred opening operator panel: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load simulator
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void LoadSimulator()
    {
      try
      {
        LCD _lcd;
        _lcd = new LCD();

        
        this.iLCD = _lcd;
        this.iLCD.Init ();

        this.Location = new Point(this.Location.X + 1000, this.Location.Y + 50);//TODO: JCA REVISAR
      }
      catch (Exception _ex)
      {
        MessageBox.Show("An error ocurred loading simulator: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void InitializeControls()
    {
      BindTypeMessageComboBox();

      // Marquee Message
      this.txt_marquee_msg.Text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";

      // Message
      this.txt_msg.Text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";

      this.EnalbeDisableRequest(false, TYPE_SIMULATOR_TAB.INTERFACE);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Open log Folder
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void OpenLog()
    {
      try
      {
        if (Directory.Exists(Log.DirectoryPath))
        {
          Process.Start(Log.DirectoryPath);
        }
        else
        {
          MessageBox.Show("Not exist LOG file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      catch (Exception _ex)
      {
        MessageBox.Show("Error opening LOG file: " + _ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close application
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void CloseApplication()
    {
      Log.EndLCD();

      this.Close();
      Application.Exit();
    }

    #endregion

    #endregion

    #region Events

    private void frm_lcd_simulator_Load(object sender, EventArgs e)
    {
      if (Debugger.IsAttached == true)
      {
        this.gb_mode.Enabled = true;
      }
      this.LoadSimulator();
    }

    private void btn_insert_card_Click(object sender, EventArgs e)
    {
      this.InsertCard();
    }

    private void btn_extract_card_Click(object sender, EventArgs e)
    {
      this.ExtractCard();
    }

    private void btn_exit_Click(object sender, EventArgs e)
    {
      this.CloseApplication();
    }

    private void btn_jackpot_apply_Click(object sender, EventArgs e)
    {
      this.SetJackpotInterval();
    }

    private void rb_mode_CheckedChanged(object sender, EventArgs e)
    {
      this.SetMode();
    }

    private void btn_marquee_send_Click(object sender, EventArgs e)
    {
      this.SendMarqueeMessage();
    }

    private void btn_cash_out_Click(object sender, EventArgs e)
    {
      this.SendCashOut();
    }

    private void btn_send_msg_Click(object sender, EventArgs e)
    {
      this.SendMessage();
    }

    private void btn_logger_Click(object sender, EventArgs e)
    {
      this.OpenLog();
    }

    private void btn_operator_Click(object sender, EventArgs e)
    {
      this.OpenOperatorPanel();
    }

    private void btn_return_OK_Click(object sender, EventArgs e)
    {
      this.SendResponse(TYPE_RESPONSE.OK);
    }

    private void btn_return_KO_Click(object sender, EventArgs e)
    {
      this.SendResponse(TYPE_RESPONSE.KO);
    }

    private void TimeoutTimer_Tick(object sender, EventArgs e)
    {
      this.HideTimeoutLabel();
    }

    #endregion

    #region Class ComboBoxItem

    // Content item for the combo box
    private class ComboBoxItem
    {
      #region Public variables

      public String m_name;
      public TYPE_MESSAGE m_value;

      #endregion

      #region ComboBoxItem

      public ComboBoxItem(TYPE_MESSAGE Value, String Name)
      {
        m_name = Name;
        m_value = Value;
      }

      #endregion

      public override string ToString()
      {
        // Generates the text shown in the combo box
        return m_name;
      }
    }

    #endregion
  }
}