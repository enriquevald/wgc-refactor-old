//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_base.cs
// 
//   DESCRIPTION: Base form. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display
{
  public partial class frm_base : Form
  {
    #region Properties

    public Boolean IsMessageShowing
    {
      get { return this.pnl_msg.Visible; }
    }

    #endregion

    #region Public Functions

    public frm_base()
    {
      InitializeComponent();
      InitializeControls();
    }

    public void ShowMessage(TYPE_MESSAGE Type, String Message)
    {
      this.ShowMessage(Type, Message, Constants.DEFAULT_TIME_SHOWING_MESSAGE);
    }

    public void ShowMessage(TYPE_MESSAGE Type, String Message, Int32 TimeShowed)
    {
      this.pnl_msg.BindMessage(Type, Message, TimeShowed);            
    }

    #endregion

    #region Private Functions

    private void InitializeControls()
    {
      Log.InitLCD();
      this.Location = new Point(0, 0);
    }

    #endregion

    #region Overrides

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams cp = base.CreateParams;
        //JCA: SI NO SE VE EL VIDEO COMENTAR LA SIGUIENTE LINEA
        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        return cp;
      }
    }

    #endregion

    #region Events

    #endregion
  }
}
