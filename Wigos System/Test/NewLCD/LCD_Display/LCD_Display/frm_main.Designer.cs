namespace LCD_Display
{
  partial class frm_main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_video = new System.Windows.Forms.Panel();
      this.pnl_marquee_top = new System.Windows.Forms.Panel();
      this.lbl_msg = new System.Windows.Forms.Label();
      this.pnl_marquee_top.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_top
      // 
      this.btn_top.FlatAppearance.BorderSize = 0;
      this.btn_top.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_top.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_top.Click += new System.EventHandler(this.btn_top_Click);
      // 
      // btn_bot
      // 
      this.btn_bot.FlatAppearance.BorderSize = 0;
      this.btn_bot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_bot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_bot.Click += new System.EventHandler(this.btn_bot_Click);
      // 
      // pnl_video
      // 
      this.pnl_video.BackColor = System.Drawing.Color.Transparent;
      this.pnl_video.Location = new System.Drawing.Point(0, 0);
      this.pnl_video.Name = "pnl_video";
      this.pnl_video.Size = new System.Drawing.Size(800, 300);
      this.pnl_video.TabIndex = 2;
      // 
      // pnl_marquee_top
      // 
      this.pnl_marquee_top.BackColor = System.Drawing.Color.Gold;
      this.pnl_marquee_top.Controls.Add(this.lbl_msg);
      this.pnl_marquee_top.Location = new System.Drawing.Point(0, 0);
      this.pnl_marquee_top.Name = "pnl_marquee_top";
      this.pnl_marquee_top.Size = new System.Drawing.Size(800, 30);
      this.pnl_marquee_top.TabIndex = 0;
      // 
      // lbl_msg
      // 
      this.lbl_msg.AutoSize = true;
      this.lbl_msg.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg.Location = new System.Drawing.Point(800, 1);
      this.lbl_msg.Name = "lbl_msg";
      this.lbl_msg.Size = new System.Drawing.Size(112, 29);
      this.lbl_msg.TabIndex = 0;
      this.lbl_msg.Text = "xMessage";
      // 
      // frm_main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.ClientSize = new System.Drawing.Size(800, 300);
      this.Controls.Add(this.pnl_video);
      this.Controls.Add(this.pnl_marquee_top);
      this.Name = "frm_main";
      this.Load += new System.EventHandler(this.frm_main_Load);
      this.Controls.SetChildIndex(this.pnl_marquee_top, 0);
      this.Controls.SetChildIndex(this.pnl_video, 0);
      this.Controls.SetChildIndex(this.pnl_base, 0);
      this.pnl_marquee_top.ResumeLayout(false);
      this.pnl_marquee_top.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_video;
    private System.Windows.Forms.Panel pnl_marquee_top;
    private System.Windows.Forms.Label lbl_msg;

  }
}