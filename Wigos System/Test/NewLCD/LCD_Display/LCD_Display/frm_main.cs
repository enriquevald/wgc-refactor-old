//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: frm_main.cs
// 
//   DESCRIPTION: LCD Main form 
// 
//        AUTHOR: Carlos Cáceres y Javier Barea.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------  ------------------------------------------------------
// 20-MAY-2014 JBP & CCG  First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using QuartzTypeLib;
using LCD_Display.Controls;
using LCD_Display.Properties;

namespace LCD_Display
{
  public partial class frm_main : frm_base
  {
    #region Constants

    private const int DEFAULT_TIMER_INTERVAL_JACKPOT = 45000;

    #endregion

    #region Variables

    private Object m_simulator;
    private Video m_video;
    private uc_list m_list;

    private Boolean m_call_attendant;
    private ACCOUNT m_account;
    private LCD_ITEM m_item;

    private TYPE_CONTAINER m_current_container;
    private TYPE_PROMOTION m_promotion;
    private TYPE_GIFT m_gift;
    private TYPE_REDEEM m_redeem;
    private TYPE_MODE m_mode;

    private Int32 m_jackpot_interval    = 0;
    private Boolean m_is_jackpot_showed = false;        // To control if the jackpot control is showed by timer event, not by user.    
    private Boolean m_is_card_inserted  = false;        // To know if the card is inserted or not.
    private Boolean m_is_chashout       = false;        // To know is Cash Out is done.
    private Timer m_timer_show_jackpot  = new Timer();  // Timer to show jackpot control.
    private Timer m_timer_marquee       = new Timer();  // Timer to move the message label.
    private Timer m_timer_hide_CA       = new Timer();  // Timer to hide call attendant button.
    private String m_marquee_text       = string.Empty;

    #endregion

    #region Properties

    public uc_main MainBody
    {
      get { return (uc_main)this.pnl_base.Controls["uc_main"]; }
    }

    public Object SimulatorForm
    {
      get { return this.m_simulator; }
      set { this.m_simulator = value; }
    }

    public uc_transferring MainTransferring
    {
      get { return (uc_transferring)this.pnl_base.Controls["uc_transferring"]; }
    }

    public Int32 DefaultTimerIntervalJackport
    {
      get { return DEFAULT_TIMER_INTERVAL_JACKPOT; }
    }

    public TYPE_CONTAINER CurrentContainer
    {
      get { return this.m_current_container; }
      set { this.m_current_container = value; }
    }

    public Boolean IsJackpotShowed
    {
      get { return this.m_is_jackpot_showed; }
      set { this.m_is_jackpot_showed = value; }
    }

    public Boolean IsCardInserted
    {
      get { return this.m_is_card_inserted; }
      set { this.m_is_card_inserted = value; }
    }

    public Boolean CallAttendant
    {
      get { return this.m_call_attendant; }
      set { this.m_call_attendant = value; }
    }

    public ACCOUNT Account
    {
      get { return this.m_account; }
      set { this.m_account = value; }
    }

    public LCD_ITEM SelectedItem
    {
      get { return this.m_item; }
      set { this.m_item = value; }
    }

    public TYPE_PROMOTION Promotion
    {
      get { return this.m_promotion; }
      set { this.m_promotion = value; }
    }

    public TYPE_GIFT Gift
    {
      get { return this.m_gift; }
      set { this.m_gift = value; }
    }

    public TYPE_REDEEM Redeem
    {
      get { return this.m_redeem; }
      set { this.m_redeem = value; }
    }

    public TYPE_MODE Mode
    {
      get { return this.m_mode; }
      set { this.m_mode = value; }
    }

    public uc_list CurrentList
    {
      get { return this.m_list; }
      set { this.m_list = value; }
    }

    public Video MainVideo
    {
      get { return this.m_video; }
      set { this.m_video = value; }
    }

    public Int32 JackpotInterval
    {
      get { return m_jackpot_interval; }
      set { m_jackpot_interval = value; }
    }

    public String MarqueeText
    {
      get { return m_marquee_text; }
      set { m_marquee_text = value; }
    }

    public Timer TimerShowJackpot
    {
      get { return this.m_timer_show_jackpot; }
      set { this.m_timer_show_jackpot = value; }
    }

    public Timer TimerMarquee
    {
      get { return this.m_timer_marquee; }
      set { this.m_timer_marquee = value; }
    }

    public Timer TimerCallAttendant
    {
      get { return this.m_timer_hide_CA; }
      set { this.m_timer_hide_CA = value; }
    }

    public Boolean IsCashOut
    {
      get { return this.m_is_chashout; }
      set { this.m_is_chashout = value; }
    }

    #endregion

    #region Message Interface

    //------------------------------------------------------------------------------
    // PURPOSE: To set the open mode.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean IsTitoMode.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void InitializeMode(Boolean IsTitoMode)
    {
      if (IsTitoMode == true)
      {
        this.Mode = TYPE_MODE.TITO;
      }
      else
      {
        this.Mode = TYPE_MODE.CASHLESS;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To Insert a card.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void InsertCard()
    {
      try
      {
        if (this.MainBody != null)
        {
          this.btn_bot.Visible = true;
          this.BindContainer(TYPE_CONTAINER.MAIN);

          this.MainBody.SetButtonCallAttendantVisibility(true);
          this.MainBody.SetButtonAccountVisibility(true);

          this.IsCardInserted = true;
          this.DisableButtons();
          this.CloseVideo();
          this.ShowMessage(TYPE_MESSAGE.WELCOME, Resources.WelcomeMessage, 2500);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while inserting the card: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To Extract the card.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void ExtractCard()
    {
      try
      {
        if (this.IsCardInserted == true)
        {
          this.IsCashOut = false;
          this.IsCardInserted = false;
          this.TimerShowJackpot.Stop();
          this.CallAttendant = false;
          this.BindContainer(TYPE_CONTAINER.MAIN);
          this.InitVideo();

          this.ShowMessage(TYPE_MESSAGE.FAREWELL, Resources.FarewellMessage, 2500);          
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while extracting the card: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To show the marquee with the message.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Msg
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void MarqueeMsg(String Msg)
    {
      try
      {
        this.MarqueeText = Msg;

        if (this.CurrentContainer == TYPE_CONTAINER.MAIN)
        {
          if (this.pnl_video.Visible == true)
          {
            this.CheckToShowMsg();
          }
          else
          {
            this.MainBody.InitMarqueeMovement();
            this.MainBody.CheckToShowMsg();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while showing the marquee message: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set the Jackpot Timer Interval .
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void ShowJackpotInterval(Int32 Interval)
    {
      try
      {
        this.JackpotInterval = Interval * 1000;

        this.TimerShowJackpot.Stop();
        this.TimerShowJackpot.Interval = this.JackpotInterval;
        this.TimerShowJackpot.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while setting the jackpot interval: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To show the marquee with the message.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    internal void CashOutEvent(Int32 Interval)
    {
      try
      {
        this.IsCashOut = true;

        if (this.pnl_video.Visible == false)
        {
          this.InitHideCallAttendant(Interval);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while cash out event: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To show the operator panel.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Nothing
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Void
    internal void ShowOperatorPanel()
    {
      try
      {
        this.DisableButtons();
        this.CloseVideo();
        this.BindContainer(TYPE_CONTAINER.OPERATOR);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while cash out event: " + _ex.Message);
      }
    }

    #endregion

    #region Public Functions

    public frm_main()
    {
      InitializeComponent();
      InitializeControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize several controls
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void InitializeControls()
    {
      try
      {
        this.MainVideo = new Video(this.pnl_video);

        this.MainVideo.SetVideoBackground(this);
        this.BindContainer(TYPE_CONTAINER.MAIN);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while initializing the form controls : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To load the different controls.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TYPE_CONTAINER Container.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void BindContainer(TYPE_CONTAINER Container)
    {
      UserControl _uc;

      try
      {
        this.pnl_base.Controls.Clear();
        this.CurrentContainer = Container;
        this.CurrentList = null;

        _uc = new Controls.uc_main();

        switch (Container)
        {
          case TYPE_CONTAINER.MAIN:
            _uc = new uc_main();
            break;
          case TYPE_CONTAINER.LOGIN:
            _uc = new uc_login();
            break;
          case TYPE_CONTAINER.ACCOUNT_MENU:
            _uc = new uc_account_menu();
            break;
          case TYPE_CONTAINER.ACCOUNT_INFO:
            _uc = new uc_account_info();
            break;
          case TYPE_CONTAINER.RAFFLES:
            _uc = new uc_raffles();
            break;
          case TYPE_CONTAINER.PROMOTION_MENU:
            _uc = new uc_promotion_menu();
            break;
          case TYPE_CONTAINER.PROMOTIONS_LIST:
            _uc = new uc_promotion_list();
            break;
          case TYPE_CONTAINER.PROMOTION_DETAIL:
            _uc = new uc_promotion_detail();
            break;
          case TYPE_CONTAINER.JACKPOTS:
            _uc = new uc_jackpot();
            break;
          case TYPE_CONTAINER.GIFT_MENU:
            _uc = new uc_gift_menu();
            break;
          case TYPE_CONTAINER.GIFTS_LIST:
            _uc = new uc_gift_list();
            break;
          case TYPE_CONTAINER.GIFT_DETAIL:
            _uc = new uc_gift_detail();
            break;
          case TYPE_CONTAINER.REDEEM:
            _uc = new uc_redeem();
            break;
          case TYPE_CONTAINER.TRANSFERRING:
            _uc = new uc_transferring();
            break;
          case TYPE_CONTAINER.OPERATOR:
            _uc = new uc_operator();
            break;
          case TYPE_CONTAINER.STATUS:
            _uc = new uc_status();
            break;
          case TYPE_CONTAINER.LOG_VIEWER:
            _uc = new uc_log_viewer();
            break;
        }

        this.pnl_base.Controls.Add(_uc);
        this.ShowButtonsDisabled();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding the control: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To load the different controls for back button.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void BindBackContainer()
    {
      try
      {
        this.pnl_base.Controls.Clear();

        if (this.CurrentContainer == TYPE_CONTAINER.ACCOUNT_MENU)
        {
          this.CallAttendant = false;
        }

        switch (this.CurrentContainer)
        {
          case TYPE_CONTAINER.MAIN:
          case TYPE_CONTAINER.LOGIN:
          case TYPE_CONTAINER.ACCOUNT_MENU:
            this.BindContainer(TYPE_CONTAINER.MAIN);
            break;

          case TYPE_CONTAINER.ACCOUNT_INFO:
          case TYPE_CONTAINER.RAFFLES:
          case TYPE_CONTAINER.PROMOTION_MENU:
          case TYPE_CONTAINER.GIFT_MENU:
          case TYPE_CONTAINER.JACKPOTS:
            this.BindContainer(TYPE_CONTAINER.ACCOUNT_MENU);
            break;

          case TYPE_CONTAINER.PROMOTIONS_LIST:
            this.BindContainer(TYPE_CONTAINER.PROMOTION_MENU);
            break;
          case TYPE_CONTAINER.PROMOTION_DETAIL:
            this.BindContainer(TYPE_CONTAINER.PROMOTIONS_LIST);
            break;

          case TYPE_CONTAINER.GIFTS_LIST:
            this.BindContainer(TYPE_CONTAINER.GIFT_MENU);
            break;
          case TYPE_CONTAINER.GIFT_DETAIL:
            this.BindContainer(TYPE_CONTAINER.GIFTS_LIST);
            break;

          case TYPE_CONTAINER.REDEEM:
            if (this.Redeem == TYPE_REDEEM.PROMOTION)
            {
              this.BindContainer(TYPE_CONTAINER.PROMOTION_DETAIL);
            }
            else
            {
              this.BindContainer(TYPE_CONTAINER.GIFT_DETAIL);
            }
            break;

          case TYPE_CONTAINER.OPERATOR:
            // En caso de salir del modo operador, volvemos a mostrar el video.
            this.btn_bot.Visible = true;
            this.BindContainer(TYPE_CONTAINER.MAIN);
            this.InitVideo();
            break;

          case TYPE_CONTAINER.STATUS:
          case TYPE_CONTAINER.LOG_VIEWER:
            this.BindContainer(TYPE_CONTAINER.OPERATOR);
            break;

          default:
            this.BindContainer(TYPE_CONTAINER.MAIN);
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding (back) the control : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the jackpot timer.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void InitJackpotTimer(Int32 Interval)
    {
      try
      {
        this.JackpotInterval = Interval;
        this.TimerShowJackpot.Tick += new EventHandler(TimerTickShowJackpot);
        this.TimerShowJackpot.Interval = this.JackpotInterval;
        this.TimerShowJackpot.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while initializing the timer to show the Jackpot : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Timer tick event to show Jackpot.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void TimerTickShowJackpot(object sender, EventArgs e)
    {
      try
      {
        this.TimerShowJackpot.Stop();
        this.StopMarqueeTimer();
        this.IsJackpotShowed = true;
        this.BindContainer(TYPE_CONTAINER.JACKPOTS);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred during the timer tick to show jackpot event : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To hide marquee panel
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void HideMsgPanel()
    {
      try
      {
        this.pnl_marquee_top.Visible = false;
        this.TimerMarquee.Stop();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while hiding the marquee panel : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set enabled/disabled the back button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Enable.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void EnableDisableButtonBack(Boolean Enable)
    {
      this.btn_top.Enabled = Enable;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set enabled/disabled the Call Attendant button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Enable.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void EnableDisableButtonCallAttendant(Boolean Enable)
    {
      this.btn_bot.Enabled = Enable;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To close the video.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void CloseVideo()
    {
      try
      {
        if (this.MainBody != null)
        {
          this.MainBody.InitMarqueeMovement();
          this.MainBody.CheckToShowMsg();
        }

        this.HideMsgPanel();
        this.MainVideo.CloseVideo();
        this.InitJackpotTimer(this.DefaultTimerIntervalJackport);        
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while closing the video : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the video.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Enable.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void InitVideo()
    {
      try
      {
        this.m_video.SetVideoBackground(this);
        this.CheckToShowMsg();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while the video was initializing : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To check if it's necesary to show the marquee.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void CheckToShowMsg()
    {
      try
      {
        this.TimerMarquee.Stop();

        if (this.MarqueeText == string.Empty)
        {
          this.pnl_marquee_top.Visible = false;
        }
        else
        {
          this.pnl_marquee_top.Visible = true;
          this.pnl_marquee_top.BringToFront();
          this.lbl_msg.Text = this.MarqueeText;
        }

        this.TimerMarquee.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while checking to show the marquee message : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set buttons enabled/disabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void DisableButtons()
    {
      if (!this.IsCardInserted)
      {
        if (this.MainBody != null)
        {
          this.MainBody.DisableMainMenuButtons();
        }

        this.EnableDisableButtonBack(false);
        this.EnableDisableButtonCallAttendant(false);
      }
      else
      {
        if (this.MainBody != null)
        {
          this.MainBody.EnableMainMenuButtons();
        }

        this.EnableDisableButtonBack(true);
        this.EnableDisableButtonCallAttendant(true);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To move marquee message.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Label LblMsg.
    //          - Int32 ContainerWidth.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Point _point.
    //
    public Point MoveMsgLabel(Label LblMsg, Int32 ContainerWidth)
    {
      Point _point;

      try
      {
        if (LblMsg.Location.X == -(LblMsg.Width))
        {
          _point = new Point(ContainerWidth, LblMsg.Location.Y);
        }
        else
        {
          _point = new Point(LblMsg.Location.X - 1, LblMsg.Location.Y);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while moving the marquee label : " + _ex.Message);
        _point = new Point(LblMsg.Location.X, LblMsg.Location.Y); // leave the label at the same point
      }

      return _point;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Display frm_main
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void Display(Object Simulator)
    {
      this.SimulatorForm = Simulator;

      base.ShowDialog ();
    }

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the marquee movement Timer.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void InitMsgMovement()
    {
      try
      {
        this.TimerMarquee.Tick += new EventHandler(_timer_Tick_MoveLabel);
        this.TimerMarquee.Interval = 8;
        this.TimerMarquee.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while the timer to move the marquee was initializing : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Timer Tick to move marquee label.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    void _timer_Tick_MoveLabel(object sender, EventArgs e)
    {
      try
      {
        this.TimerMarquee.Stop();
        this.lbl_msg.Location = this.MoveMsgLabel(this.lbl_msg, this.Width);
        this.TimerMarquee.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred during the timer tick event to move the marquee : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To show disabled the main buttons.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void ShowButtonsDisabled()
    {
      if (this.CurrentContainer == TYPE_CONTAINER.MAIN
            && !this.IsMessageShowing)
      {
        if (this.IsJackpotShowed == true
              && this.Mode == TYPE_MODE.CASHLESS)
        {
          this.IsJackpotShowed = false;
          this.DisableButtons();
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize timer to hide the call attendant button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int32 Interval.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void InitHideCallAttendant(Int32 Interval)
    {
      try
      {
        this.TimerCallAttendant.Tick += new EventHandler(Tick_HideCallAttendant);
        this.TimerCallAttendant.Interval = Interval;
        this.TimerCallAttendant.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while initializing the timer to hide the call attendant button : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Timer Tick event to hide the call attendant button.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void Tick_HideCallAttendant(object sender, EventArgs e)
    {
      try
      {
        this.TimerCallAttendant.Stop();

        if (this.IsCardInserted == true)
        {
          this.btn_bot.Visible = false;

          if (this.MainBody != null)
          {
            this.MainBody.SetButtonCallAttendantVisibility(false);
          }
        }
        else
        {
          this.IsCashOut = false;
          this.InitVideo();
          this.CallAttendant = false;
          this.BindContainer(TYPE_CONTAINER.MAIN);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred during the timer tick event to hide call attendant button : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To stop marquee timer.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void StopMarqueeTimer()
    {
      try
      {
        if (this.m_marquee_text != String.Empty)
        {
          this.MainBody.TimerMoveLabel.Stop();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while stoping the marquee label timer: " + _ex.Message);
      }
    }

    #endregion

    #region Events

    private void frm_main_Load(object sender, EventArgs e)
    {
      this.pnl_marquee_top.BringToFront();
      this.InitMsgMovement();
      this.CheckToShowMsg();
    }

    private void btn_top_Click(object sender, EventArgs e)
    {
      this.BindBackContainer();
    }

    private void btn_bot_Click(object sender, EventArgs e)
    {
      this.CallAttendant = true;
      this.BindContainer(TYPE_CONTAINER.MAIN);
    }

    #endregion

    #region Overrides

    protected override void WndProc(ref Message m)
    {
      this.MainVideo.WndProc(ref m);

      if (this.CurrentList != null)
      {
        this.CurrentList.WndProcList(ref m);
      }

      base.WndProc(ref m);
    }

    #endregion
  }
}