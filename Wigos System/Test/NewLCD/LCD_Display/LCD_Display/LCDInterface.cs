using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LCD_Display
{
  public interface ILCD
  {
    //void Init(Object Simulator);
    void Init();
    void InitSimulator(Object Simulator);
    bool MsgInterface(int Type, string Parameters);
  }

  public class LCD : ILCD
  {
    #region Variables

    private frm_main m_form_main;

    #endregion

    #region Properties

    internal frm_main MainForm
    {
      get { return this.m_form_main; }
      set { this.m_form_main = value; }
    }

    #endregion 

    #region LCD Members

    public LCD()
    {
    }

    //public LCD(bool TitoMode)
    //{
    //  this.MainForm = new frm_main();
    //  this.MainForm.InitializeMode(TitoMode);
    //}

    #endregion

    #region ILCD Members

    public void Init()
    {
      Thread _thread;
      _thread = new Thread(MyThread);
      _thread.Start();
    }


    private void MyThread()
    {
      this.MainForm = new frm_main();
      this.MainForm.InitializeMode(false);
      this.MainForm.Display(null);
    }

    public void InitSimulator(Object Simulator)
    {
      this.MainForm.Display(Simulator);
    }

    public bool MsgInterface(int Type, string Parameters)
    {
      String[] _parameters;
      _parameters = Parameters.Split('|');

      try
      {
        switch ((TYPE_EVENT)Type)
        {
          case TYPE_EVENT.INSERT_CARD:
            this.MainForm.InsertCard();
            break;

          case TYPE_EVENT.EXTRACT_CARD:
            this.MainForm.ExtractCard();
            break;

          case TYPE_EVENT.MARQUEE_MSG:
            this.MainForm.MarqueeMsg(_parameters[0]);
            break;

          case TYPE_EVENT.JACKPOT_INTERVAL:
            this.MainForm.ShowJackpotInterval(Convert.ToInt32(_parameters[0]));
            break;

          case TYPE_EVENT.CASH_OUT_EVENT:
            this.MainForm.CashOutEvent(Convert.ToInt32(_parameters[0]));
            break;

          case TYPE_EVENT.SHOW_MESSAGE:
            this.MainForm.ShowMessage((TYPE_MESSAGE)Enum.Parse(typeof(TYPE_MESSAGE), _parameters[0]), _parameters[1]);
            break;

          case TYPE_EVENT.OPERATOR_PANEL:
            this.MainForm.ShowOperatorPanel();
            break;
          case TYPE_EVENT.SEND_RESPONSE:
            if (this.MainForm.MainTransferring != null)
            {
              this.MainForm.MainTransferring.GetResponse((TYPE_RESPONSE)Enum.Parse(typeof(TYPE_RESPONSE), _parameters[0]));//((TYPE_RESPONSE)Convert.ToInt32(_parameters[0])));
            }
          break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while calling an interface function: " + _ex.Message);

      }
      return false;
    }

    #endregion
  }  
}
