//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Common.cs
// 
//   DESCRIPTION: Common clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace LCD_Display
{
  #region Structs

  public class Constants
  {
    // Message
    internal const Int32 WM_APP               = 0x8000;
    internal const Int32 WM_MOUSECLICK        = 0x210;
    internal const Int32 WM_XBUTTONDBLCLK     = 0x020D;
    internal const Int32 WM_XBUTTONDOWN       = 0x020B;
    internal const Int32 WM_XBUTTONUP         = 0x020C;
    internal const Int32 WM_GRAPHNOTIFY       = WM_APP + 1;

    // Message Params
    internal const Int32 WM_MOUSE_LEFT_CLICK  = 0x201;
    internal const Int32 WM_MOUSE_RIGHT_CLICK = 0x204;
    internal const Int32 WM_MOUSE_MID_CLICK   = 0x207;    

    //LCD Costants
    internal const Int32 DEFAULT_TIME_SHOWING_MESSAGE = 10000;
  }

  public struct ACCOUNT
  {
    public String Name;
    public String Address;
    public String Telephone;
    public String Email;
    public Int32  RedeemPoints;
    public Int32  RedeemPromoPoints;
    public Int32  NonRedeemPoints;
    public String ActualLevel;
    public String NextLevel;
  }

  public struct LCD_ITEM
  {
    public Image Image;
    public String Name;
    public String Description;
    public Int32 Amount;
    public TYPE_ITEM Type;
  }

  #endregion

  #region Enums

  public enum TYPE_CONTAINER
  {
    OPERATOR          = -1,
    STATUS            = -2,
    LOG_VIEWER        = -3,
    MAIN              =  0,
    LOGIN             =  1,
    ACCOUNT_MENU      =  2,
    ACCOUNT_INFO      =  3,
    RAFFLES           =  4,
    JACKPOTS          =  5,
    PROMOTION_MENU    =  6,
    PROMOTIONS_LIST   =  7,
    PROMOTION_DETAIL  =  8,
    GIFT_MENU         =  9,
    GIFTS_LIST        = 10,
    GIFT_DETAIL       = 11,
    REDEEM            = 12,
    TRANSFERRING      = 13
  }

  public enum TYPE_EVENT
  {
    INSERT_CARD       = 0,
    EXTRACT_CARD      = 1,
    MARQUEE_MSG       = 2,
    JACKPOT_INTERVAL  = 3,
    CASH_OUT_EVENT    = 4,
    SHOW_MESSAGE      = 5,
    OPERATOR_PANEL    = 6,
    SEND_RESPONSE     = 7
  }

  public enum TYPE_PROMOTION
  {
    AVAILABLE = 0,
    OTHERS = 1
  }

  public enum TYPE_GIFT
  {
    AVAILABLE = 0,
    NEXTS = 1
  }

  public enum TYPE_REDEEM
  {
    PROMOTION = 0,
    GIFT = 1
  }

  public enum TYPE_ITEM
  {
    GIFT = 0,
    DRAW = 1
  }

  public enum TYPE_STATUS
  {
    OK            = 0,
    ERR           = 1,
    UNK           = 2,
    NOTINSTALLED  = 3,
    JAM           = 4,
    SN            = 5,
    OPEN          = 6,
    FULL          = 7
  }

  public enum TYPE_MODE
  { 
    TITO     = 0,
    CASHLESS = 1
  }

  public enum TYPE_MESSAGE
  {
    ERROR     = 0,
    WARNING   = 1,
    INFO      = 2,
    SUCCESS   = 3,
    WELCOME   = 4,
    FAREWELL  = 5
  }

  public enum TYPE_RESPONSE
  {
    OK      = 0,
    KO      = 1,
    TIMEOUT = 2
  }

  public enum TYPE_SIMULATOR_MEMBER
  {
    GET_REQUEST     = 0,
    REUQEST_TIMEOUT = 1
  }

  #endregion
}
