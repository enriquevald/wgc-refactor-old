//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Log.cs
// 
//   DESCRIPTION: Logger clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 02-JUN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUN-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LCD_Display
{
  public class Log
  {
    #region Constants

    private const String PATH_LOG = "LOG";

    #endregion

    #region Variables
    #endregion

    #region Properties

    public static String DirectoryPath
    {
      get { return PATH_LOG; }
    }

    public static String FilePath
    {
      get 
      {
        return DirectoryPath + "/" + "LCD_DISPLAY_" + DateTime.Now.ToString("yyyy_MM_dd") + ".LOG";
      }
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Add initialize INFO
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_MESSAGE:  Type
    //           - String:        Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void InitLCD()
    { 
      try
      {
        SetLogger();

        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "--------------------------------- Init LCD ---------------------------------");
        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "...");
      }
      catch(Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } 

    //------------------------------------------------------------------------------
    // PURPOSE: Add end INFO
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_MESSAGE:  Type
    //           - String:        Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void EndLCD()
    { 
      try
      {
        SetLogger();

        Add(TYPE_MESSAGE.INFO, "...");
        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
        Add(TYPE_MESSAGE.INFO, "--------------------------------- End LCD ----------------------------------");
        Add(TYPE_MESSAGE.INFO, "----------------------------------------------------------------------------");
      }
      catch(Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } 

    //------------------------------------------------------------------------------
    // PURPOSE: Add line into log file
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_MESSAGE:  Type
    //           - String:        Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - Boolean  
    //   NOTES:
    //
    public static void Add(TYPE_MESSAGE Type, String Message)
    { 
      try
      {
        if (Message.Length > 0)
        {
          using (StreamWriter _sw = File.AppendText(Log.FilePath))
          {
            _sw.WriteLine(  "{0} - {1}: {2}"
                          , DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")
                          , GetTypeErrorString(Type)
                          , Message );
            _sw.Flush();
          }
        }        
      }
      catch(Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    } 

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Set logger folder and file. 
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - Void
    //   NOTES:
    //
    private static void SetLogger()
    {
      try
      {
        if (!File.Exists(Log.FilePath))
        {
          if (!Directory.Exists(Log.PATH_LOG))
          {
            Directory.CreateDirectory(Log.PATH_LOG);
          }
        }
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set logger permissions
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - Void
    //   NOTES:
    //
    private static void SetLoggerPermissions()
    {
      String _dataPath;
      String _appFile;
      
      try
      {
        _dataPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        _appFile = Path.Combine(_dataPath, Directory.GetCurrentDirectory() + "\\" + PATH_LOG);
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get type error description
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_MESSAGE:  Type
    //      - OUTPUT:
    //             
    // RETURNS:
    //           - String: Type error description  
    //   NOTES:
    //
    private static String GetTypeErrorString(TYPE_MESSAGE Type)
    {
      String _type;

      switch (Type)
      {
        case TYPE_MESSAGE.ERROR:
          _type = "  Error";
          break;
        case TYPE_MESSAGE.WARNING:
          _type = "Warning";
          break;
        case TYPE_MESSAGE.INFO:
          _type = "   Info";
          break;
        default:
          _type = "Undefined";
          break;
      }

      return _type;
    }
    
    #endregion
  }
}
