//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Video.cs
// 
//   DESCRIPTION: Video Class. 
// 
//        AUTHOR: Carlos Cáceres & Javi Barea
// 
// CREATION DATE: 20-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------  ----------------------------------------------------------
// 20-MAY-2014 CCG & JBP  First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

using QuartzTypeLib;

namespace LCD_Display
{
  public class Video
  {
    #region Enums

    enum MediaStatus
    {
      None    = 0,
      Stopped = 1,
      Paused  = 2,
      Running = 3
    }

    #endregion

    #region Constants

    private const int EC_COMPLETE           = 0x01;
    private const int WS_CHILD              = 0x40000000;
    private const int WS_CLIPCHILDREN       = 0x2000000;

    #endregion


    #region Variables

    private FilgraphManager m_obj_filter_graph;
    private IBasicAudio     m_obj_basic_audio;
    private IVideoWindow    m_obj_video_window;
    private IMediaEvent     m_obj_media_event;
    private IMediaEventEx   m_obj_media_event_ex;
    private IMediaPosition  m_obj_media_position;
    private IMediaControl   m_obj_media_control;

    private MediaStatus m_current_status = MediaStatus.None;

    private Panel m_video_panel;
    private frm_main m_frm_main;

    #endregion


    #region Properties

    #region Video Properties

    private MediaStatus CurrentStatus
    {
      get { return this.m_current_status; }
      set { this.m_current_status = value; }
    }
    
    public FilgraphManager FilterGraph
    {
      get { return this.m_obj_filter_graph; }
      set { this.m_obj_filter_graph = value; }
    }

    public IBasicAudio BasicAudio
    {
      get { return this.m_obj_basic_audio; }
      set { this.m_obj_basic_audio = value; }
    }

    public IVideoWindow VideoWindow
    {
      get { return this.m_obj_video_window; }
      set { this.m_obj_video_window = value; }
    }

    public IMediaEvent MediaEvent
    {
      get { return this.m_obj_media_event; }
      set { this.m_obj_media_event = value; }
    }

    public IMediaEventEx MediaEventEx
    {
      get { return this.m_obj_media_event_ex; }
      set { this.m_obj_media_event_ex = value; }
    }

    public IMediaPosition MediaPosition
    {
      get { return this.m_obj_media_position; }
      set { this.m_obj_media_position = value; }
    }

    public IMediaControl MediaControl
    {
      get { return this.m_obj_media_control; }
      set { this.m_obj_media_control = value; }
    }

    #endregion 

    #region Form Properties

    public Panel VideoPanel
    {
      get { return this.m_video_panel; }
      set { this.m_video_panel = value; }
    }

    public frm_main MainForm
    {
      get { return this.m_frm_main; }
      set { this.m_frm_main = value; }
    }

    #endregion

    #endregion


    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Video constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Video Panel
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public Video(Panel Panel)
    {
      this.VideoPanel = Panel;

      InitializeObjects();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Video Window WndProc
    //
    //  PARAMS:
    //      - INPUT:
    //           - Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void WndProc(ref Message m)
    {
      try
      {
        if (this.VideoPanel.Visible)
        {
          switch (m.Msg)
          {
            case Constants.WM_GRAPHNOTIFY:
              RestartVideo();
              break;

            case Constants.WM_MOUSECLICK:

              switch (m.WParam.ToInt32())
              {
                case Constants.WM_MOUSE_LEFT_CLICK:
                  CloseVideo();
                  this.MainForm.HideMsgPanel();
                  this.MainForm.InitJackpotTimer(this.MainForm.DefaultTimerIntervalJackport);
                  if (this.MainForm.Mode == TYPE_MODE.CASHLESS)
                  {
                    this.MainForm.DisableButtons();
                  }
                  break;

                case Constants.WM_MOUSE_RIGHT_CLICK:
                  StopVideo();
                  break;

                case Constants.WM_MOUSE_MID_CLICK:
                  StartVideo(true);
                  break;
              }
              break;
          }
        }

        this.CheckForActions(m);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error in WndProc: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check for actions
    //
    //  PARAMS:
    //      - INPUT:
    //           - Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void CheckForActions(Message m)    
    {
      if (m.WParam.ToInt64() == Constants.WM_MOUSE_LEFT_CLICK)
      {
        this.MainForm.TimerShowJackpot.Stop();
        this.MainForm.TimerShowJackpot.Start();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set video background
    //
    //  PARAMS:
    //      - INPUT:
    //           - Main form
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void SetVideoBackground(frm_main FrmMain)
    {
      String _file;

      try
      {
        this.MainForm = FrmMain;
        _file = "video.wmv";

        if (!File.Exists("video.wmv"))
        {
          Log.Add(TYPE_MESSAGE.ERROR, "The video not exist");

          this.VideoPanel.Visible = false;
        }

        CleanUp();

        this.FilterGraph = new FilgraphManager();
        this.FilterGraph.RenderFile(_file);
        this.BasicAudio = this.FilterGraph as IBasicAudio;

        try
        {
          this.VideoWindow              = this.FilterGraph as IVideoWindow;
          this.VideoWindow.Owner        = (int)this.VideoPanel.Handle;
          this.VideoWindow.WindowStyle  = WS_CHILD | WS_CLIPCHILDREN;

          this.VideoWindow.SetWindowPosition( this.VideoPanel.ClientRectangle.Left
                                            , this.VideoPanel.ClientRectangle.Top
                                            , this.VideoPanel.ClientRectangle.Width
                                            , this.VideoPanel.ClientRectangle.Height);
        }
        catch (Exception _ex)
        {
          this.VideoWindow = null;

          Log.Add(TYPE_MESSAGE.ERROR, "An error ocurred setting window position: " + _ex.Message);
        }

        this.MediaEvent   = this.FilterGraph as IMediaEvent;
        this.MediaEventEx = this.FilterGraph as IMediaEventEx;

        this.MediaEventEx.SetNotifyWindow((int)this.MainForm.Handle, Constants.WM_GRAPHNOTIFY, 0);

        this.MediaPosition  = this.FilterGraph as IMediaPosition;
        this.MediaControl   = this.FilterGraph as IMediaControl;

        this.VideoPanel.BringToFront();

        StartVideo(true);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "An error ocurred setting video background: " + _ex.Message);
      }
    }

    #endregion


    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize all video Objects
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void InitializeObjects()
    {
      this.MediaControl   = null;
      this.MediaPosition  = null;
      this.MediaEvent     = null;
      this.MediaEventEx   = null;      
      this.VideoWindow    = null;
      this.BasicAudio     = null;
      this.FilterGraph    = null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Clean all
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void CleanUp()
    {
      try
      {
        if (this.MediaControl != null)
        {
          this.MediaControl.Stop();
        }

        this.CurrentStatus = MediaStatus.Stopped;

        if (this.MediaEventEx != null)
        {
          this.MediaEventEx.SetNotifyWindow(0, 0, 0);
        }

        if (this.VideoWindow != null)
        {
          this.VideoWindow.Visible  = 0;
          this.VideoWindow.Owner    = 0;
        }

        InitializeObjects();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "An error ocurred in CleanUp function: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Start background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Boleean: IsRestart
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void StartVideo(Boolean IsRestart)
    {
      try
      {
        if (IsRestart)
        {
          StopVideo();
        }

        this.VideoPanel.Visible = true;

        this.MediaControl.Run();
        this.CurrentStatus = MediaStatus.Running;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "An error ocurred starting video: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Stop background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void StopVideo()
    {
      this.MediaPosition.CurrentPosition = 0;
      this.MediaControl.Stop();
      this.CurrentStatus = MediaStatus.Stopped;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Pause background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void PauseVideo()
    {
      this.MediaControl.Pause();
      this.CurrentStatus = MediaStatus.Paused;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Stop/Start background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void StopStart()
    {
      switch (this.CurrentStatus)
      {
        case MediaStatus.Running:
          PauseVideo();
          break;
        case MediaStatus.Stopped:
        case MediaStatus.Paused:
          StartVideo(false);
          break;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void CloseVideo()
    {
      if (this.VideoPanel.Visible == true)
      {
        this.MediaPosition.CurrentPosition = 0;
        this.MediaControl.Stop();

        this.CurrentStatus = MediaStatus.Stopped;
        InitializeObjects();

        this.VideoPanel.Visible = false;
        this.VideoPanel.SendToBack();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Restat background video
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void RestartVideo()
    {
      int lEventCode;
      int lParam1, lParam2;

      while (true)
      {
        try
        {
          this.MediaEventEx.GetEvent(out lEventCode,
              out lParam1,
              out lParam2,
              0);

          this.MediaEventEx.FreeEventParams(lEventCode, lParam1, lParam2);

          if (((QuartzTypeLib.FilgraphManagerClass)(this.MediaEventEx)).CurrentPosition
           == ((QuartzTypeLib.FilgraphManagerClass)(this.MediaEventEx)).Duration)
          {
            StartVideo(true);
          }
        }
        catch
        {
          break;
        }
      }
    }

    #endregion

  }
}
