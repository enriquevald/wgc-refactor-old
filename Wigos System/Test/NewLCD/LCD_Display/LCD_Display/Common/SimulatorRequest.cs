//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SimulatorRequest.cs
// 
//   DESCRIPTION: Requests to Simulator
// 
//        AUTHOR: Javier Barea y Carlos Cáceres.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------  ------------------------------------------------------
// 03-JUN-2014 JBP & CCG  First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace LCD_Display
{
  class SimulatorRequest
  {
    #region Constants

    private const String SIMULATOR_NAME = "\\LCD_Simulator.exe";

    #endregion

    #region Properties

    private String SimulatorPath
    {
      get { return Directory.GetCurrentDirectory() + SIMULATOR_NAME; }
    }

    #endregion

    #region Public Functions

    public Boolean CallSimluatorMember(Object SimulatorForm, TYPE_SIMULATOR_MEMBER Member)
    {
      return CallSimluatorMember(SimulatorForm, Member, null);
    }

    public Boolean CallSimluatorMember(Object SimulatorForm, TYPE_SIMULATOR_MEMBER Member, Object[] Args)
    {
      String _member;

      try
      {
        _member = GetMember(Member);

        if (String.IsNullOrEmpty(_member))
        {
          Log.Add(TYPE_MESSAGE.WARNING, "Warning: Invoking inexistent member.");

          return false;
        }
        
        SimulatorForm.GetType().InvokeMember(   _member
                                              , BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic
                                              , null
                                              , SimulatorForm
                                              , Args);

        return true;        
      }
      catch(Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred invoking simulator member: " + _ex.Message);
      }

      return false;
    }

    private String GetMember(TYPE_SIMULATOR_MEMBER Member)
    {
      String _member; 

      switch(Member)
      {
        case TYPE_SIMULATOR_MEMBER.GET_REQUEST:
          _member = "GetRequest";
          break;
        case TYPE_SIMULATOR_MEMBER.REUQEST_TIMEOUT:
          _member = "RequestTimeout";
          break;
        default:
          _member = String.Empty;
          break;
      }

      return _member;
    }

    #endregion
  }
}