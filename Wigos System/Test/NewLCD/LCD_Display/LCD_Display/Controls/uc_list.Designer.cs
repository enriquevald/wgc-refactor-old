namespace LCD_Display.Controls
{
  partial class uc_list
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_item_list = new System.Windows.Forms.Panel();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_item_list);
      this.pnl_container.Size = new System.Drawing.Size(522, 218);
      // 
      // pnl_item_list
      // 
      this.pnl_item_list.AutoSize = true;
      this.pnl_item_list.Location = new System.Drawing.Point(0, 22);
      this.pnl_item_list.MinimumSize = new System.Drawing.Size(522, 185);
      this.pnl_item_list.Name = "pnl_item_list";
      this.pnl_item_list.Size = new System.Drawing.Size(522, 185);
      this.pnl_item_list.TabIndex = 2;
      this.pnl_item_list.DragOver += new System.Windows.Forms.DragEventHandler(this.pnl_item_list_DragOver);
      // 
      // uc_list
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_list";
      this.Size = new System.Drawing.Size(522, 218);
      this.Load += new System.EventHandler(this.uc_list_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_item_list;
  }
}
