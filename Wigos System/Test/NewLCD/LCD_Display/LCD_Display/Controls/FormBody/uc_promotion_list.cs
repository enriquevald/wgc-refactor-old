//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_promotion_list.cs
// 
//   DESCRIPTION: Promotion List Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_promotion_list : uc_body
  {
    #region Public Functions

    public uc_promotion_list()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    private void ShowPromotionList()
    {
      if (this.MainForm.Promotion == TYPE_PROMOTION.AVAILABLE)
      {
        this.pnl_promotion_list.BringToFront();
        this.pnl_promotion_list.Visible = true;
      }
      else
      {
        this.pnl_casino_list.BringToFront();
        this.pnl_casino_list.Visible = true;
      }
    }

    private void ShowDetail(Boolean IsCasinoPromotion )
    {
      if (IsCasinoPromotion == true)
      {
        this.MainForm.Promotion = TYPE_PROMOTION.OTHERS;
      }
      else 
      {
        this.MainForm.Promotion = TYPE_PROMOTION.AVAILABLE;
      }
      
      this.MainForm.BindContainer(TYPE_CONTAINER.PROMOTION_DETAIL);
    }

    #endregion

    #region Events

    private void btn_ok_available_1_Click(object sender, EventArgs e)
    {
      ShowDetail(false);
    }

    private void btn_ok_available_2_Click(object sender, EventArgs e)
    {
      ShowDetail(false);
    }

    private void btn_ok_available_3_Click(object sender, EventArgs e)
    {
      ShowDetail(false);
    }

    private void btn_ok_available_4_Click(object sender, EventArgs e)
    {
      ShowDetail(false);
    }

    private void btn_ok_casino_1_Click(object sender, EventArgs e)
    {
      ShowDetail(true);
    }

    private void btn_ok_casino_2_Click(object sender, EventArgs e)
    {
      ShowDetail(true);
    }

    private void btn_ok_casino_3_Click(object sender, EventArgs e)
    {
      ShowDetail(true);
    }

    private void btn_ok_casino_4_Click(object sender, EventArgs e)
    {
      ShowDetail(true);
    }

    private void uc_promotion_list_Load(object sender, EventArgs e)
    {
      ShowPromotionList();
    }

    #endregion
  }
}
