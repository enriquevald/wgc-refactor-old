namespace LCD_Display.Controls
{
  partial class uc_log_viewer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.wb_log_viewer = new System.Windows.Forms.WebBrowser();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.wb_log_viewer);
      // 
      // wb_log_viewer
      // 
      this.wb_log_viewer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.wb_log_viewer.Location = new System.Drawing.Point(0, 0);
      this.wb_log_viewer.MinimumSize = new System.Drawing.Size(20, 20);
      this.wb_log_viewer.Name = "wb_log_viewer";
      this.wb_log_viewer.Size = new System.Drawing.Size(595, 300);
      this.wb_log_viewer.TabIndex = 0;
      this.wb_log_viewer.Visible = false;
      this.wb_log_viewer.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.wb_log_viewer_DocumentCompleted);
      // 
      // uc_log_viewer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_log_viewer";
      this.Load += new System.EventHandler(this.uc_log_viewer_Load);
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.WebBrowser wb_log_viewer;
  }
}
