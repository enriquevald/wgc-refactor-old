namespace LCD_Display.Controls
{
  partial class uc_gift_menu
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_next_gifts = new LCD_Display.Controls.uc_button();
      this.btn_available_gifts = new LCD_Display.Controls.uc_button();
      this.pnl_buttons.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.Controls.Add(this.btn_next_gifts);
      this.pnl_buttons.Controls.Add(this.btn_available_gifts);
      this.pnl_buttons.Location = new System.Drawing.Point(147, 64);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(301, 173);
      this.pnl_buttons.TabIndex = 3;
      // 
      // btn_next_gifts
      // 
      this.btn_next_gifts.BackColor = System.Drawing.Color.Transparent;
      this.btn_next_gifts.BackgroundImage = global::LCD_Display.Properties.Resources.NextGiftButton;
      this.btn_next_gifts.FlatAppearance.BorderSize = 0;
      this.btn_next_gifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_next_gifts.Location = new System.Drawing.Point(15, 91);
      this.btn_next_gifts.Name = "btn_next_gifts";
      this.btn_next_gifts.Size = new System.Drawing.Size(270, 56);
      this.btn_next_gifts.TabIndex = 1;
      this.btn_next_gifts.UseVisualStyleBackColor = false;
      this.btn_next_gifts.Click += new System.EventHandler(this.btn_next_gifts_Click);
      // 
      // btn_available_gifts
      // 
      this.btn_available_gifts.BackColor = System.Drawing.Color.Transparent;
      this.btn_available_gifts.BackgroundImage = global::LCD_Display.Properties.Resources.AvailableGiftButton;
      this.btn_available_gifts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.btn_available_gifts.FlatAppearance.BorderSize = 0;
      this.btn_available_gifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_available_gifts.Location = new System.Drawing.Point(15, 26);
      this.btn_available_gifts.Name = "btn_available_gifts";
      this.btn_available_gifts.Size = new System.Drawing.Size(270, 56);
      this.btn_available_gifts.TabIndex = 0;
      this.btn_available_gifts.UseVisualStyleBackColor = false;
      this.btn_available_gifts.Click += new System.EventHandler(this.btn_available_gifts_Click);
      // 
      // uc_gift_menu
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnl_buttons);
      this.Name = "uc_gift_menu";
      this.Controls.SetChildIndex(this.pnl_container, 0);
      this.Controls.SetChildIndex(this.pnl_buttons, 0);
      this.pnl_buttons.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_buttons;
    private uc_button btn_next_gifts;
    private uc_button btn_available_gifts;
  }
}
