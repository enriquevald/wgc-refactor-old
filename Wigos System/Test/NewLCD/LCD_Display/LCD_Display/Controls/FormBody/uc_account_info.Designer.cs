namespace LCD_Display.Controls
{
  partial class uc_account_info
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_account_info));
      this.pnl_account_info = new System.Windows.Forms.Panel();
      this.lbl_points_3 = new System.Windows.Forms.Label();
      this.lbl_points_2 = new System.Windows.Forms.Label();
      this.lbl_points_1 = new System.Windows.Forms.Label();
      this.lbl_next_level = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.lbl_actual_level = new System.Windows.Forms.Label();
      this.lbl_level = new System.Windows.Forms.Label();
      this.lbl_non_redeem_points = new System.Windows.Forms.Label();
      this.lbl_redeem_promo_points = new System.Windows.Forms.Label();
      this.lbl_not_redeem = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lb_redeem = new System.Windows.Forms.Label();
      this.lbl_redeem_points = new System.Windows.Forms.Label();
      this.lbl_points = new System.Windows.Forms.Label();
      this.lbl_gifts = new System.Windows.Forms.Label();
      this.pnl_account_info.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_account_info
      // 
      this.pnl_account_info.BackgroundImage = global::LCD_Display.Properties.Resources.back_account_info;
      this.pnl_account_info.Controls.Add(this.lbl_points_3);
      this.pnl_account_info.Controls.Add(this.lbl_points_2);
      this.pnl_account_info.Controls.Add(this.lbl_points_1);
      this.pnl_account_info.Controls.Add(this.lbl_next_level);
      this.pnl_account_info.Controls.Add(this.label3);
      this.pnl_account_info.Controls.Add(this.label6);
      this.pnl_account_info.Controls.Add(this.lbl_actual_level);
      this.pnl_account_info.Controls.Add(this.lbl_level);
      this.pnl_account_info.Controls.Add(this.lbl_non_redeem_points);
      this.pnl_account_info.Controls.Add(this.lbl_redeem_promo_points);
      this.pnl_account_info.Controls.Add(this.lbl_not_redeem);
      this.pnl_account_info.Controls.Add(this.label2);
      this.pnl_account_info.Controls.Add(this.lb_redeem);
      this.pnl_account_info.Controls.Add(this.lbl_redeem_points);
      this.pnl_account_info.Controls.Add(this.lbl_points);
      this.pnl_account_info.Controls.Add(this.lbl_gifts);
      this.pnl_account_info.Location = new System.Drawing.Point(15, 13);
      this.pnl_account_info.Name = "pnl_account_info";
      this.pnl_account_info.Size = new System.Drawing.Size(565, 274);
      this.pnl_account_info.TabIndex = 3;
      // 
      // lbl_points_3
      // 
      this.lbl_points_3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_points_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_points_3.Location = new System.Drawing.Point(385, 142);
      this.lbl_points_3.Name = "lbl_points_3";
      this.lbl_points_3.Size = new System.Drawing.Size(71, 22);
      this.lbl_points_3.TabIndex = 22;
      this.lbl_points_3.Text = "Points";
      this.lbl_points_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_2
      // 
      this.lbl_points_2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_points_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_points_2.Location = new System.Drawing.Point(385, 118);
      this.lbl_points_2.Name = "lbl_points_2";
      this.lbl_points_2.Size = new System.Drawing.Size(71, 22);
      this.lbl_points_2.TabIndex = 21;
      this.lbl_points_2.Text = "Points";
      this.lbl_points_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points_1
      // 
      this.lbl_points_1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_points_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_points_1.Location = new System.Drawing.Point(385, 94);
      this.lbl_points_1.Name = "lbl_points_1";
      this.lbl_points_1.Size = new System.Drawing.Size(71, 22);
      this.lbl_points_1.TabIndex = 20;
      this.lbl_points_1.Text = "Points";
      this.lbl_points_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_next_level
      // 
      this.lbl_next_level.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_next_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_next_level.Location = new System.Drawing.Point(307, 228);
      this.lbl_next_level.Name = "lbl_next_level";
      this.lbl_next_level.Size = new System.Drawing.Size(221, 24);
      this.lbl_next_level.TabIndex = 19;
      this.lbl_next_level.Text = "xNextLevel";
      this.lbl_next_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label3
      // 
      this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(77, 228);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(205, 24);
      this.label3.TabIndex = 18;
      this.label3.Text = "Next Level";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.White;
      this.label6.Location = new System.Drawing.Point(81, 204);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(201, 24);
      this.label6.TabIndex = 17;
      this.label6.Text = "Actual Level";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_actual_level
      // 
      this.lbl_actual_level.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_actual_level.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_actual_level.Location = new System.Drawing.Point(307, 204);
      this.lbl_actual_level.Name = "lbl_actual_level";
      this.lbl_actual_level.Size = new System.Drawing.Size(221, 24);
      this.lbl_actual_level.TabIndex = 16;
      this.lbl_actual_level.Text = "xActualLevel";
      this.lbl_actual_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_level
      // 
      this.lbl_level.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_level.ForeColor = System.Drawing.Color.White;
      this.lbl_level.Image = ((System.Drawing.Image)(resources.GetObject("lbl_level.Image")));
      this.lbl_level.Location = new System.Drawing.Point(3, 167);
      this.lbl_level.Name = "lbl_level";
      this.lbl_level.Size = new System.Drawing.Size(279, 33);
      this.lbl_level.TabIndex = 15;
      this.lbl_level.Text = "Level";
      // 
      // lbl_non_redeem_points
      // 
      this.lbl_non_redeem_points.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_non_redeem_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_non_redeem_points.Location = new System.Drawing.Point(307, 142);
      this.lbl_non_redeem_points.Name = "lbl_non_redeem_points";
      this.lbl_non_redeem_points.Size = new System.Drawing.Size(72, 22);
      this.lbl_non_redeem_points.TabIndex = 14;
      this.lbl_non_redeem_points.Text = "xNR";
      this.lbl_non_redeem_points.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_redeem_promo_points
      // 
      this.lbl_redeem_promo_points.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_redeem_promo_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_redeem_promo_points.Location = new System.Drawing.Point(307, 118);
      this.lbl_redeem_promo_points.Name = "lbl_redeem_promo_points";
      this.lbl_redeem_promo_points.Size = new System.Drawing.Size(72, 22);
      this.lbl_redeem_promo_points.TabIndex = 13;
      this.lbl_redeem_promo_points.Text = "xPR";
      this.lbl_redeem_promo_points.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_not_redeem
      // 
      this.lbl_not_redeem.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_not_redeem.ForeColor = System.Drawing.Color.White;
      this.lbl_not_redeem.Location = new System.Drawing.Point(85, 142);
      this.lbl_not_redeem.Name = "lbl_not_redeem";
      this.lbl_not_redeem.Size = new System.Drawing.Size(197, 22);
      this.lbl_not_redeem.TabIndex = 12;
      this.lbl_not_redeem.Text = "Non-Redeemable";
      this.lbl_not_redeem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.White;
      this.label2.Location = new System.Drawing.Point(77, 118);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(205, 22);
      this.label2.TabIndex = 11;
      this.label2.Text = "Redeemable Promo.";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lb_redeem
      // 
      this.lb_redeem.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_redeem.ForeColor = System.Drawing.Color.White;
      this.lb_redeem.Location = new System.Drawing.Point(81, 94);
      this.lb_redeem.Name = "lb_redeem";
      this.lb_redeem.Size = new System.Drawing.Size(201, 22);
      this.lb_redeem.TabIndex = 10;
      this.lb_redeem.Text = "Redeemable";
      this.lb_redeem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lbl_redeem_points
      // 
      this.lbl_redeem_points.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_redeem_points.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
      this.lbl_redeem_points.Location = new System.Drawing.Point(307, 94);
      this.lbl_redeem_points.Name = "lbl_redeem_points";
      this.lbl_redeem_points.Size = new System.Drawing.Size(72, 22);
      this.lbl_redeem_points.TabIndex = 6;
      this.lbl_redeem_points.Text = "xR";
      this.lbl_redeem_points.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lbl_points
      // 
      this.lbl_points.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_points.ForeColor = System.Drawing.Color.White;
      this.lbl_points.Image = ((System.Drawing.Image)(resources.GetObject("lbl_points.Image")));
      this.lbl_points.Location = new System.Drawing.Point(3, 59);
      this.lbl_points.Name = "lbl_points";
      this.lbl_points.Size = new System.Drawing.Size(279, 33);
      this.lbl_points.TabIndex = 1;
      this.lbl_points.Text = "Points";
      // 
      // lbl_gifts
      // 
      this.lbl_gifts.Image = global::LCD_Display.Properties.Resources.AccountInfoTitle;
      this.lbl_gifts.Location = new System.Drawing.Point(12, 5);
      this.lbl_gifts.Name = "lbl_gifts";
      this.lbl_gifts.Size = new System.Drawing.Size(217, 48);
      this.lbl_gifts.TabIndex = 0;
      // 
      // uc_account_info
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnl_account_info);
      this.Name = "uc_account_info";
      this.Load += new System.EventHandler(this.uc_account_info_Load);
      this.Controls.SetChildIndex(this.pnl_container, 0);
      this.Controls.SetChildIndex(this.pnl_account_info, 0);
      this.pnl_account_info.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_account_info;
    private System.Windows.Forms.Label lbl_gifts;
    private System.Windows.Forms.Label lbl_points;
    private System.Windows.Forms.Label lbl_redeem_points;
    private System.Windows.Forms.Label lbl_non_redeem_points;
    private System.Windows.Forms.Label lbl_redeem_promo_points;
    private System.Windows.Forms.Label lbl_not_redeem;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lb_redeem;
    private System.Windows.Forms.Label lbl_level;
    private System.Windows.Forms.Label lbl_next_level;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label lbl_actual_level;
    private System.Windows.Forms.Label lbl_points_1;
    private System.Windows.Forms.Label lbl_points_3;
    private System.Windows.Forms.Label lbl_points_2;
  }
}
