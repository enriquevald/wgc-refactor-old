namespace LCD_Display.Controls
{
  partial class uc_promotion_list
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_casino_list = new System.Windows.Forms.Panel();
      this.btn_ok_casino_2 = new LCD_Display.Controls.uc_button();
      this.btn_ok_casino_3 = new LCD_Display.Controls.uc_button();
      this.btn_ok_casino_4 = new LCD_Display.Controls.uc_button();
      this.btn_ok_casino_1 = new LCD_Display.Controls.uc_button();
      this.pnl_promotion_list = new System.Windows.Forms.Panel();
      this.btn_ok_available_2 = new LCD_Display.Controls.uc_button();
      this.btn_ok_available_3 = new LCD_Display.Controls.uc_button();
      this.btn_ok_available_4 = new LCD_Display.Controls.uc_button();
      this.btn_ok_available_1 = new LCD_Display.Controls.uc_button();
      this.pnl_container.SuspendLayout();
      this.pnl_casino_list.SuspendLayout();
      this.pnl_promotion_list.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_casino_list);
      this.pnl_container.Controls.Add(this.pnl_promotion_list);
      // 
      // pnl_casino_list
      // 
      this.pnl_casino_list.BackgroundImage = global::LCD_Display.Properties.Resources.CasinoPromotionsList;
      this.pnl_casino_list.Controls.Add(this.btn_ok_casino_2);
      this.pnl_casino_list.Controls.Add(this.btn_ok_casino_3);
      this.pnl_casino_list.Controls.Add(this.btn_ok_casino_4);
      this.pnl_casino_list.Controls.Add(this.btn_ok_casino_1);
      this.pnl_casino_list.Location = new System.Drawing.Point(17, 16);
      this.pnl_casino_list.Name = "pnl_casino_list";
      this.pnl_casino_list.Size = new System.Drawing.Size(560, 269);
      this.pnl_casino_list.TabIndex = 6;
      this.pnl_casino_list.Visible = false;
      // 
      // btn_ok_casino_2
      // 
      this.btn_ok_casino_2.BackColor = System.Drawing.Color.White;
      this.btn_ok_casino_2.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_casino_2.FlatAppearance.BorderSize = 0;
      this.btn_ok_casino_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_casino_2.Location = new System.Drawing.Point(509, 162);
      this.btn_ok_casino_2.Name = "btn_ok_casino_2";
      this.btn_ok_casino_2.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_casino_2.TabIndex = 4;
      this.btn_ok_casino_2.UseVisualStyleBackColor = false;
      this.btn_ok_casino_2.Click += new System.EventHandler(this.btn_ok_casino_2_Click);
      // 
      // btn_ok_casino_3
      // 
      this.btn_ok_casino_3.BackColor = System.Drawing.Color.White;
      this.btn_ok_casino_3.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_casino_3.FlatAppearance.BorderSize = 0;
      this.btn_ok_casino_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_casino_3.Location = new System.Drawing.Point(509, 191);
      this.btn_ok_casino_3.Name = "btn_ok_casino_3";
      this.btn_ok_casino_3.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_casino_3.TabIndex = 3;
      this.btn_ok_casino_3.UseVisualStyleBackColor = false;
      this.btn_ok_casino_3.Click += new System.EventHandler(this.btn_ok_casino_3_Click);
      // 
      // btn_ok_casino_4
      // 
      this.btn_ok_casino_4.BackColor = System.Drawing.Color.White;
      this.btn_ok_casino_4.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_casino_4.FlatAppearance.BorderSize = 0;
      this.btn_ok_casino_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_casino_4.Location = new System.Drawing.Point(509, 220);
      this.btn_ok_casino_4.Name = "btn_ok_casino_4";
      this.btn_ok_casino_4.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_casino_4.TabIndex = 2;
      this.btn_ok_casino_4.UseVisualStyleBackColor = false;
      this.btn_ok_casino_4.Click += new System.EventHandler(this.btn_ok_casino_4_Click);
      // 
      // btn_ok_casino_1
      // 
      this.btn_ok_casino_1.BackColor = System.Drawing.Color.White;
      this.btn_ok_casino_1.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_casino_1.FlatAppearance.BorderSize = 0;
      this.btn_ok_casino_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_casino_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_casino_1.Location = new System.Drawing.Point(509, 133);
      this.btn_ok_casino_1.Name = "btn_ok_casino_1";
      this.btn_ok_casino_1.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_casino_1.TabIndex = 1;
      this.btn_ok_casino_1.UseVisualStyleBackColor = false;
      this.btn_ok_casino_1.Click += new System.EventHandler(this.btn_ok_casino_1_Click);
      // 
      // pnl_promotion_list
      // 
      this.pnl_promotion_list.BackgroundImage = global::LCD_Display.Properties.Resources.AvailablePromotionsList;
      this.pnl_promotion_list.Controls.Add(this.btn_ok_available_2);
      this.pnl_promotion_list.Controls.Add(this.btn_ok_available_3);
      this.pnl_promotion_list.Controls.Add(this.btn_ok_available_4);
      this.pnl_promotion_list.Controls.Add(this.btn_ok_available_1);
      this.pnl_promotion_list.Location = new System.Drawing.Point(17, 16);
      this.pnl_promotion_list.Name = "pnl_promotion_list";
      this.pnl_promotion_list.Size = new System.Drawing.Size(560, 269);
      this.pnl_promotion_list.TabIndex = 3;
      this.pnl_promotion_list.Visible = false;
      // 
      // btn_ok_available_2
      // 
      this.btn_ok_available_2.BackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_2.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_available_2.FlatAppearance.BorderSize = 0;
      this.btn_ok_available_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_available_2.Location = new System.Drawing.Point(509, 162);
      this.btn_ok_available_2.Name = "btn_ok_available_2";
      this.btn_ok_available_2.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_available_2.TabIndex = 4;
      this.btn_ok_available_2.UseVisualStyleBackColor = false;
      this.btn_ok_available_2.Click += new System.EventHandler(this.btn_ok_available_2_Click);
      // 
      // btn_ok_available_3
      // 
      this.btn_ok_available_3.BackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_3.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_available_3.FlatAppearance.BorderSize = 0;
      this.btn_ok_available_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_available_3.Location = new System.Drawing.Point(509, 191);
      this.btn_ok_available_3.Name = "btn_ok_available_3";
      this.btn_ok_available_3.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_available_3.TabIndex = 3;
      this.btn_ok_available_3.UseVisualStyleBackColor = false;
      this.btn_ok_available_3.Click += new System.EventHandler(this.btn_ok_available_3_Click);
      // 
      // btn_ok_available_4
      // 
      this.btn_ok_available_4.BackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_4.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_available_4.FlatAppearance.BorderSize = 0;
      this.btn_ok_available_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_available_4.Location = new System.Drawing.Point(509, 220);
      this.btn_ok_available_4.Name = "btn_ok_available_4";
      this.btn_ok_available_4.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_available_4.TabIndex = 2;
      this.btn_ok_available_4.UseVisualStyleBackColor = false;
      this.btn_ok_available_4.Click += new System.EventHandler(this.btn_ok_available_4_Click);
      // 
      // btn_ok_available_1
      // 
      this.btn_ok_available_1.BackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_1.BackgroundImage = global::LCD_Display.Properties.Resources.OkButtonSmall;
      this.btn_ok_available_1.FlatAppearance.BorderSize = 0;
      this.btn_ok_available_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_ok_available_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_ok_available_1.Location = new System.Drawing.Point(509, 133);
      this.btn_ok_available_1.Name = "btn_ok_available_1";
      this.btn_ok_available_1.Size = new System.Drawing.Size(25, 23);
      this.btn_ok_available_1.TabIndex = 1;
      this.btn_ok_available_1.UseVisualStyleBackColor = false;
      this.btn_ok_available_1.Click += new System.EventHandler(this.btn_ok_available_1_Click);
      // 
      // uc_promotion_list
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_promotion_list";
      this.Load += new System.EventHandler(this.uc_promotion_list_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_casino_list.ResumeLayout(false);
      this.pnl_promotion_list.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_promotion_list;
    private uc_button btn_ok_available_2;
    private uc_button btn_ok_available_3;
    private uc_button btn_ok_available_4;
    private uc_button btn_ok_available_1;
    private System.Windows.Forms.Panel pnl_casino_list;
    private uc_button btn_ok_casino_2;
    private uc_button btn_ok_casino_3;
    private uc_button btn_ok_casino_4;
    private uc_button btn_ok_casino_1;
  }
}
