namespace LCD_Display.Controls
{
  partial class uc_raffles
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_draws = new System.Windows.Forms.Panel();
      this.uc_list_item = new LCD_Display.Controls.uc_list();
      this.pnl_container.SuspendLayout();
      this.pnl_draws.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_draws);
      // 
      // pnl_draws
      // 
      this.pnl_draws.BackgroundImage = global::LCD_Display.Properties.Resources.DrawsBg;
      this.pnl_draws.Controls.Add(this.uc_list_item);
      this.pnl_draws.Location = new System.Drawing.Point(17, 14);
      this.pnl_draws.Name = "pnl_draws";
      this.pnl_draws.Size = new System.Drawing.Size(560, 275);
      this.pnl_draws.TabIndex = 0;
      // 
      // uc_list_item
      // 
      this.uc_list_item.BackColor = System.Drawing.Color.Transparent;
      this.uc_list_item.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.uc_list_item.ItemList = null;
      this.uc_list_item.Location = new System.Drawing.Point(0, 57);
      this.uc_list_item.Name = "uc_list_item";
      this.uc_list_item.Size = new System.Drawing.Size(560, 218);
      this.uc_list_item.TabIndex = 2;
      // 
      // uc_raffles
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_raffles";
      this.Load += new System.EventHandler(this.uc_raffles_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_draws.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_draws;
    private uc_list uc_list_item;

  }
}
