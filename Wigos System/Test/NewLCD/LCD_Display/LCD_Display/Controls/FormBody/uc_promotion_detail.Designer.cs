namespace LCD_Display.Controls
{
  partial class uc_promotion_detail
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_promotion_detail));
      this.pnl_promotion_available = new System.Windows.Forms.Panel();
      this.pnl_promotion_casino = new System.Windows.Forms.Panel();
      this.btn_redeem = new LCD_Display.Controls.uc_button();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_promotion_casino);
      this.pnl_container.Controls.Add(this.btn_redeem);
      this.pnl_container.Controls.Add(this.pnl_promotion_available);
      // 
      // pnl_promotion_available
      // 
      this.pnl_promotion_available.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnl_promotion_available.BackgroundImage")));
      this.pnl_promotion_available.Location = new System.Drawing.Point(17, 9);
      this.pnl_promotion_available.Name = "pnl_promotion_available";
      this.pnl_promotion_available.Size = new System.Drawing.Size(560, 206);
      this.pnl_promotion_available.TabIndex = 1;
      this.pnl_promotion_available.Visible = false;
      // 
      // pnl_promotion_casino
      // 
      this.pnl_promotion_casino.BackgroundImage = global::LCD_Display.Properties.Resources.CasinoPromotionDetail;
      this.pnl_promotion_casino.Location = new System.Drawing.Point(17, 47);
      this.pnl_promotion_casino.Name = "pnl_promotion_casino";
      this.pnl_promotion_casino.Size = new System.Drawing.Size(560, 206);
      this.pnl_promotion_casino.TabIndex = 0;
      this.pnl_promotion_casino.Visible = false;
      // 
      // btn_redeem
      // 
      this.btn_redeem.BackColor = System.Drawing.Color.Transparent;
      this.btn_redeem.BackgroundImage = global::LCD_Display.Properties.Resources.RedeemButton;
      this.btn_redeem.FlatAppearance.BorderSize = 0;
      this.btn_redeem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_redeem.Location = new System.Drawing.Point(161, 228);
      this.btn_redeem.Name = "btn_redeem";
      this.btn_redeem.Size = new System.Drawing.Size(272, 59);
      this.btn_redeem.TabIndex = 2;
      this.btn_redeem.UseVisualStyleBackColor = false;
      this.btn_redeem.Click += new System.EventHandler(this.btn_redeem_Click);
      // 
      // uc_promotion_detail
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_promotion_detail";
      this.Load += new System.EventHandler(this.uc_promotion_detail_Load);
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_promotion_available;
    private System.Windows.Forms.Panel pnl_promotion_casino;
    private uc_button btn_redeem;
  }
}
