//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_status.cs
// 
//   DESCRIPTION: Status Control. 
// 
//        AUTHOR: Carlos Cáceres.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_status : uc_body
  {
    #region Constants

    private readonly string StatusOK = "OK";
    private readonly string StatusERR = "ERROR";
    private readonly string StatusUNK = "UNK";
    private readonly string StatusNotInstalled = "NOT INSTALLED";
    private readonly string StatusJAM = "JAM";
    private readonly string StatusSN = "!SERIAL NUMBER";
    private readonly string StatusOPEN = "OPEN";
    private readonly string StatusFULL = "FULL";

    #endregion
    
    #region Public Functions

    public uc_status()
    {
      InitializeComponent();
      InitializeStatusInfo();
    }

    #endregion
    
    #region Private Functions
        
    //------------------------------------------------------------------------------
    // PURPOSE: Initialize status Info
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void InitializeStatusInfo()
    {
      try
      {
        this.lb_version.Text = "v18.846";
        this.GetStatusInfo(TYPE_STATUS.OK, this.lb_status_sas);
        this.GetStatusInfo(TYPE_STATUS.ERR, this.lb_status_wcp);
        this.GetStatusInfo(TYPE_STATUS.UNK, this.lb_status_wc2);
        this.GetStatusInfo(TYPE_STATUS.SN, this.lb_status_lector);
        this.GetStatusInfo(TYPE_STATUS.NOTINSTALLED, this.lb_status_bill);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while initializing the status information : " + _ex.Message);
      }
    }
        
    //------------------------------------------------------------------------------
    // PURPOSE: Get status info
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_STATUS: Status
    //           - Label:       LabelStatus
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void GetStatusInfo(TYPE_STATUS Status, Label LabelStatus)
    {
      switch (Status)
      {
        case TYPE_STATUS.OK:
          LabelStatus.Text = StatusOK;
          LabelStatus.BackColor = System.Drawing.Color.Green;
          break;

        case TYPE_STATUS.ERR:
          LabelStatus.Text = StatusERR;
          LabelStatus.BackColor = System.Drawing.Color.Red;
          break;

        case TYPE_STATUS.UNK:
          LabelStatus.Text = StatusUNK;
          LabelStatus.BackColor = System.Drawing.Color.Yellow;
          break;

        case TYPE_STATUS.NOTINSTALLED:
          LabelStatus.Text = StatusNotInstalled;
          LabelStatus.BackColor = System.Drawing.Color.Yellow;
          break;

        case TYPE_STATUS.JAM:
          LabelStatus.Text = StatusJAM;
          LabelStatus.BackColor = System.Drawing.Color.Red;
          break;

        case TYPE_STATUS.SN:
          LabelStatus.Text = StatusSN;
          LabelStatus.BackColor = System.Drawing.Color.Yellow;
          break;

        case TYPE_STATUS.OPEN:
          LabelStatus.Text = StatusOPEN;
          LabelStatus.BackColor = System.Drawing.Color.Yellow;
          break;

        case TYPE_STATUS.FULL:
          LabelStatus.Text = StatusFULL;
          LabelStatus.BackColor = System.Drawing.Color.Yellow;
          break;
      }
    }

    #endregion

    #region Events

    private void pnl_container_Paint(object sender, PaintEventArgs e)
    {
      this.MainForm.TimerShowJackpot.Stop();
    }

    #endregion
  }
}
