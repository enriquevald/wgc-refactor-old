namespace LCD_Display.Controls
{
  partial class uc_status
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lb_sas = new System.Windows.Forms.Label();
      this.lb_wc2 = new System.Windows.Forms.Label();
      this.lb_wcp = new System.Windows.Forms.Label();
      this.lb_status_wc2 = new System.Windows.Forms.Label();
      this.lb_status_wcp = new System.Windows.Forms.Label();
      this.lb_version = new System.Windows.Forms.Label();
      this.lb_status_lector = new System.Windows.Forms.Label();
      this.lb_status_bill = new System.Windows.Forms.Label();
      this.lb_lector = new System.Windows.Forms.Label();
      this.lb_bill = new System.Windows.Forms.Label();
      this.lb_status_sas = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.lb_status_lector);
      this.pnl_container.Controls.Add(this.lb_status_bill);
      this.pnl_container.Controls.Add(this.lb_lector);
      this.pnl_container.Controls.Add(this.lb_bill);
      this.pnl_container.Controls.Add(this.lb_version);
      this.pnl_container.Controls.Add(this.lb_status_sas);
      this.pnl_container.Controls.Add(this.lb_status_wcp);
      this.pnl_container.Controls.Add(this.lb_status_wc2);
      this.pnl_container.Controls.Add(this.lb_wcp);
      this.pnl_container.Controls.Add(this.lb_wc2);
      this.pnl_container.Controls.Add(this.lb_sas);
      this.pnl_container.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_container_Paint);
      // 
      // lb_sas
      // 
      this.lb_sas.AutoSize = true;
      this.lb_sas.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_sas.ForeColor = System.Drawing.Color.White;
      this.lb_sas.Location = new System.Drawing.Point(53, 3);
      this.lb_sas.Name = "lb_sas";
      this.lb_sas.Size = new System.Drawing.Size(112, 59);
      this.lb_sas.TabIndex = 0;
      this.lb_sas.Text = "SAS:";
      // 
      // lb_wc2
      // 
      this.lb_wc2.AutoSize = true;
      this.lb_wc2.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_wc2.ForeColor = System.Drawing.Color.White;
      this.lb_wc2.Location = new System.Drawing.Point(35, 121);
      this.lb_wc2.Name = "lb_wc2";
      this.lb_wc2.Size = new System.Drawing.Size(130, 59);
      this.lb_wc2.TabIndex = 1;
      this.lb_wc2.Text = "WC2:";
      // 
      // lb_wcp
      // 
      this.lb_wcp.AutoSize = true;
      this.lb_wcp.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_wcp.ForeColor = System.Drawing.Color.White;
      this.lb_wcp.Location = new System.Drawing.Point(35, 62);
      this.lb_wcp.Name = "lb_wcp";
      this.lb_wcp.Size = new System.Drawing.Size(132, 59);
      this.lb_wcp.TabIndex = 2;
      this.lb_wcp.Text = "WCP:";
      // 
      // lb_status_wc2
      // 
      this.lb_status_wc2.AutoSize = true;
      this.lb_status_wc2.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_status_wc2.ForeColor = System.Drawing.Color.Black;
      this.lb_status_wc2.Location = new System.Drawing.Point(170, 123);
      this.lb_status_wc2.MinimumSize = new System.Drawing.Size(285, 50);
      this.lb_status_wc2.Name = "lb_status_wc2";
      this.lb_status_wc2.Size = new System.Drawing.Size(285, 50);
      this.lb_status_wc2.TabIndex = 3;
      this.lb_status_wc2.Text = "xStatus";
      this.lb_status_wc2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lb_status_wcp
      // 
      this.lb_status_wcp.AutoSize = true;
      this.lb_status_wcp.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_status_wcp.ForeColor = System.Drawing.Color.Black;
      this.lb_status_wcp.Location = new System.Drawing.Point(170, 66);
      this.lb_status_wcp.MinimumSize = new System.Drawing.Size(285, 50);
      this.lb_status_wcp.Name = "lb_status_wcp";
      this.lb_status_wcp.Size = new System.Drawing.Size(285, 50);
      this.lb_status_wcp.TabIndex = 4;
      this.lb_status_wcp.Text = "xStatus";
      this.lb_status_wcp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lb_version
      // 
      this.lb_version.AutoSize = true;
      this.lb_version.Font = new System.Drawing.Font("Calibri", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_version.ForeColor = System.Drawing.Color.White;
      this.lb_version.Location = new System.Drawing.Point(462, 9);
      this.lb_version.Name = "lb_version";
      this.lb_version.Size = new System.Drawing.Size(144, 42);
      this.lb_version.TabIndex = 6;
      this.lb_version.Text = "xVersion";
      // 
      // lb_status_lector
      // 
      this.lb_status_lector.AutoSize = true;
      this.lb_status_lector.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_status_lector.ForeColor = System.Drawing.Color.Black;
      this.lb_status_lector.Location = new System.Drawing.Point(170, 180);
      this.lb_status_lector.MinimumSize = new System.Drawing.Size(285, 50);
      this.lb_status_lector.Name = "lb_status_lector";
      this.lb_status_lector.Size = new System.Drawing.Size(285, 50);
      this.lb_status_lector.TabIndex = 10;
      this.lb_status_lector.Text = "xStatus";
      this.lb_status_lector.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lb_status_bill
      // 
      this.lb_status_bill.AutoSize = true;
      this.lb_status_bill.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_status_bill.ForeColor = System.Drawing.Color.Black;
      this.lb_status_bill.Location = new System.Drawing.Point(170, 237);
      this.lb_status_bill.MinimumSize = new System.Drawing.Size(285, 50);
      this.lb_status_bill.Name = "lb_status_bill";
      this.lb_status_bill.Size = new System.Drawing.Size(285, 50);
      this.lb_status_bill.TabIndex = 9;
      this.lb_status_bill.Text = "xStatus";
      this.lb_status_bill.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lb_lector
      // 
      this.lb_lector.AutoSize = true;
      this.lb_lector.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_lector.ForeColor = System.Drawing.Color.White;
      this.lb_lector.Location = new System.Drawing.Point(3, 177);
      this.lb_lector.Name = "lb_lector";
      this.lb_lector.Size = new System.Drawing.Size(162, 59);
      this.lb_lector.TabIndex = 8;
      this.lb_lector.Text = "Lector:";
      // 
      // lb_bill
      // 
      this.lb_bill.AutoSize = true;
      this.lb_bill.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_bill.ForeColor = System.Drawing.Color.White;
      this.lb_bill.Location = new System.Drawing.Point(51, 236);
      this.lb_bill.Name = "lb_bill";
      this.lb_bill.Size = new System.Drawing.Size(114, 59);
      this.lb_bill.TabIndex = 7;
      this.lb_bill.Text = "Bill.:";
      // 
      // lb_status_sas
      // 
      this.lb_status_sas.AutoSize = true;
      this.lb_status_sas.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lb_status_sas.ForeColor = System.Drawing.Color.Black;
      this.lb_status_sas.Location = new System.Drawing.Point(170, 10);
      this.lb_status_sas.MinimumSize = new System.Drawing.Size(285, 50);
      this.lb_status_sas.Name = "lb_status_sas";
      this.lb_status_sas.Size = new System.Drawing.Size(285, 50);
      this.lb_status_sas.TabIndex = 4;
      this.lb_status_sas.Text = "xStatus";
      this.lb_status_sas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // uc_status
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_status";
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lb_wcp;
    private System.Windows.Forms.Label lb_wc2;
    private System.Windows.Forms.Label lb_sas;
    private System.Windows.Forms.Label lb_status_lector;
    private System.Windows.Forms.Label lb_status_bill;
    private System.Windows.Forms.Label lb_lector;
    private System.Windows.Forms.Label lb_bill;
    private System.Windows.Forms.Label lb_version;
    private System.Windows.Forms.Label lb_status_wcp;
    private System.Windows.Forms.Label lb_status_wc2;
    private System.Windows.Forms.Label lb_status_sas;

  }
}
