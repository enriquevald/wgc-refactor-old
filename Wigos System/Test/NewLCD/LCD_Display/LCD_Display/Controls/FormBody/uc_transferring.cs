//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_transferring.cs
// 
//   DESCRIPTION: Transferring Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 20-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAY-2014 CCG    First release.
// 03-JUN-2014 JBP    Added interface request with Simulator.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace LCD_Display.Controls
{
  public partial class uc_transferring : uc_body
  {
    #region Members

    private Timer m_timer = new Timer();

    #endregion

    #region Properties

    private Timer TransferringTimer
    {
      get { return this.m_timer; }
      set { this.m_timer = value; }
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public uc_transferring()
    {
      InitializeComponent();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get external response
    //
    //  PARAMS:
    //      - INPUT:
    //           - TYPE_RESPONSE: Response
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void GetResponse(TYPE_RESPONSE Response)
    {
      this.ShowResponse(Response);
    }

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Disable/enable base buttons
    //
    //  PARAMS:
    //      - INPUT:
    //           - Boolean: Enabled
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void DisableEnableBaseButtons(Boolean Enabled)
    {
      this.MainForm.btn_bot.Enabled = Enabled;
      this.MainForm.btn_top.Enabled = Enabled;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: End request
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void EndRequest(TYPE_RESPONSE Response)
    {
      try
      {
        this.TransferringTimer.Stop();
        DisableEnableBaseButtons(true);
        this.MainForm.TimerShowJackpot.Start();

        if (Response == TYPE_RESPONSE.OK)
        {
          this.MainForm.BindContainer(TYPE_CONTAINER.ACCOUNT_MENU);
        }
        else
        {
          if (this.MainForm.Redeem == TYPE_REDEEM.PROMOTION)
          {
            this.MainForm.BindContainer(TYPE_CONTAINER.PROMOTION_DETAIL);
          }
          else
          {
            this.MainForm.BindContainer(TYPE_CONTAINER.GIFT_DETAIL);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred in end request event : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Wait transferring
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void WaitTransferring()
    {
      try
      {
        this.TransferringTimer.Tick += new EventHandler(transferring_timer_Tick);
        this.TransferringTimer.Interval = (30000);
        this.TransferringTimer.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while waiting response : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show response
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void ShowResponse(TYPE_RESPONSE Response)
    {
      try
      {
        this.TransferringTimer.Stop();

        switch (Response)
        {
          case TYPE_RESPONSE.OK:
            this.MainForm.ShowMessage(TYPE_MESSAGE.SUCCESS, "The operation has been completed successfully");
            break;
          case TYPE_RESPONSE.KO:
            this.MainForm.ShowMessage(TYPE_MESSAGE.ERROR, "The operation has not been completed: REQUEST REFUSED");
            break;
          case TYPE_RESPONSE.TIMEOUT:
            this.MainForm.ShowMessage(TYPE_MESSAGE.ERROR, "The operation has not been completed: TIMEOUT");
            break;
        }

        this.EndRequest(Response);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while showing response: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send request
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    // 
    private void SendRequest(TYPE_SIMULATOR_MEMBER Member, TYPE_RESPONSE Response)
    {
      this.SendRequest(Member);
      this.ShowResponse(Response);
    }

    private void SendRequest(TYPE_SIMULATOR_MEMBER Member)
    {
      Object[] _args;

      try
      {
        this.GetArgs(Member, out _args);

        if (!new SimulatorRequest().CallSimluatorMember(this.MainForm.SimulatorForm, Member, _args))
        {
          ShowResponse(TYPE_RESPONSE.KO);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while sending request: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Send request
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void GetArgs(TYPE_SIMULATOR_MEMBER Member, out Object[] Args)
    {
      Args = null;

      try
      {
        switch (Member)
        {
          case TYPE_SIMULATOR_MEMBER.GET_REQUEST:
            Args = new Object[1];
            Args[0] = this.MainForm.SelectedItem;
            break;

          case TYPE_SIMULATOR_MEMBER.REUQEST_TIMEOUT:
            Args = null;
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred getting args for simulator member request" + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Start transfer
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void StartTransfer()
    {
      try
      {
        this.MainForm.TimerShowJackpot.Stop();
        this.DisableEnableBaseButtons(false);
        this.SendRequest(TYPE_SIMULATOR_MEMBER.GET_REQUEST);
        this.WaitTransferring();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while starting transfer: " + _ex.Message);
      }
    }

    #endregion

    #region Events

    private void uc_transferring_Load(object sender, EventArgs e)
    {
      StartTransfer();
    }

    private void transferring_timer_Tick(object sender, EventArgs e)
    {
      this.SendRequest(TYPE_SIMULATOR_MEMBER.REUQEST_TIMEOUT, TYPE_RESPONSE.TIMEOUT);
    }

    #endregion
  }
}
