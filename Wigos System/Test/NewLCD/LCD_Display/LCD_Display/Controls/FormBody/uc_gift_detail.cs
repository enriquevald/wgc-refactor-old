//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gift_detail.cs
// 
//   DESCRIPTION: Gift Detail Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_gift_detail : uc_body
  {
    #region Public Functions

    public uc_gift_detail()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    private void LoadGiftInformation()
    {
      try
      {
        this.lbl_gift_description.Text = this.MainForm.SelectedItem.Description;
        this.lbl_gift_name.Text = this.MainForm.SelectedItem.Name;
        this.pnl_gift_image.BackgroundImage = this.MainForm.SelectedItem.Image;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while loading gift information : " + _ex.Message);
      }
    }

    private void ShowRedeemButton()
    { 
      if (this.MainForm.Gift == TYPE_GIFT.NEXTS)
      {
        this.btn_redeem.Visible = false;
      }
    }

    #endregion

    #region Events

    private void uc_gift_detail_Load(object sender, EventArgs e)
    {
      LoadGiftInformation();
      ShowRedeemButton();
    }

    private void btn_redeem_Click(object sender, EventArgs e)
    {
      this.MainForm.Redeem = TYPE_REDEEM.GIFT;
      this.MainForm.BindContainer(TYPE_CONTAINER.REDEEM);
    }

    #endregion
  }
}
