//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_promotion_menu.cs
// 
//   DESCRIPTION: Promotion Menu Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_promotion_menu : uc_body
  {
    #region Public Functions

    public uc_promotion_menu()
    {      
      InitializeComponent();
    }

    #endregion

    #region Events

    private void btn_player_promotions_Click(object sender, EventArgs e)
    {
      this.MainForm.Promotion = TYPE_PROMOTION.AVAILABLE;
      this.MainForm.BindContainer(TYPE_CONTAINER.PROMOTIONS_LIST);
    }

    private void btn_casino_propotions_Click(object sender, EventArgs e)
    {
      this.MainForm.Promotion = TYPE_PROMOTION.OTHERS;
      this.MainForm.BindContainer(TYPE_CONTAINER.PROMOTIONS_LIST);
    }

    #endregion
  }
}
