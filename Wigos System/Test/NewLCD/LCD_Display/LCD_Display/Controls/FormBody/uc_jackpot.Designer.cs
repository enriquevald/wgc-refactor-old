namespace LCD_Display.Controls
{
  partial class uc_jackpot
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_jackpot = new System.Windows.Forms.Panel();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.lbl_jackpot_1 = new System.Windows.Forms.Label();
      this.lbl_jackpot = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.pnl_jackpot.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_jackpot);
      // 
      // pnl_jackpot
      // 
      this.pnl_jackpot.BackgroundImage = global::LCD_Display.Properties.Resources.back_jackpot;
      this.pnl_jackpot.Controls.Add(this.label3);
      this.pnl_jackpot.Controls.Add(this.label4);
      this.pnl_jackpot.Controls.Add(this.label5);
      this.pnl_jackpot.Controls.Add(this.label2);
      this.pnl_jackpot.Controls.Add(this.label1);
      this.pnl_jackpot.Controls.Add(this.lbl_jackpot_1);
      this.pnl_jackpot.Controls.Add(this.lbl_jackpot);
      this.pnl_jackpot.Location = new System.Drawing.Point(15, 13);
      this.pnl_jackpot.Name = "pnl_jackpot";
      this.pnl_jackpot.Size = new System.Drawing.Size(565, 274);
      this.pnl_jackpot.TabIndex = 10;
      // 
      // label3
      // 
      this.label3.Font = new System.Drawing.Font("Calibri", 41.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(248, 178);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(274, 56);
      this.label3.TabIndex = 16;
      this.label3.Text = "$850";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.label3.Click += new System.EventHandler(this.Control_Click);
      // 
      // label4
      // 
      this.label4.Font = new System.Drawing.Font("Calibri", 41.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.Color.White;
      this.label4.Location = new System.Drawing.Point(248, 58);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(274, 56);
      this.label4.TabIndex = 15;
      this.label4.Text = "$21,045";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.label4.Click += new System.EventHandler(this.Control_Click);
      // 
      // label5
      // 
      this.label5.Font = new System.Drawing.Font("Calibri", 41.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.White;
      this.label5.Location = new System.Drawing.Point(248, 118);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(274, 56);
      this.label5.TabIndex = 14;
      this.label5.Text = "$9,860";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.label5.Click += new System.EventHandler(this.Control_Click);
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.White;
      this.label2.Location = new System.Drawing.Point(15, 201);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(208, 31);
      this.label2.TabIndex = 13;
      this.label2.Text = "Mini Jackpot";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.label2.Click += new System.EventHandler(this.Control_Click);
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.White;
      this.label1.Location = new System.Drawing.Point(15, 141);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(208, 31);
      this.label1.TabIndex = 12;
      this.label1.Text = "Super Jackpot";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.label1.Click += new System.EventHandler(this.Control_Click);
      // 
      // lbl_jackpot_1
      // 
      this.lbl_jackpot_1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_jackpot_1.ForeColor = System.Drawing.Color.White;
      this.lbl_jackpot_1.Location = new System.Drawing.Point(15, 81);
      this.lbl_jackpot_1.Name = "lbl_jackpot_1";
      this.lbl_jackpot_1.Size = new System.Drawing.Size(208, 31);
      this.lbl_jackpot_1.TabIndex = 11;
      this.lbl_jackpot_1.Text = "Mega Jackpot";
      this.lbl_jackpot_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_jackpot_1.Click += new System.EventHandler(this.Control_Click);
      // 
      // lbl_jackpot
      // 
      this.lbl_jackpot.Image = global::LCD_Display.Properties.Resources.JackpotTitle;
      this.lbl_jackpot.Location = new System.Drawing.Point(0, 0);
      this.lbl_jackpot.Name = "lbl_jackpot";
      this.lbl_jackpot.Size = new System.Drawing.Size(228, 48);
      this.lbl_jackpot.TabIndex = 10;
      this.lbl_jackpot.Click += new System.EventHandler(this.Control_Click);
      // 
      // uc_jackpot
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_jackpot";
      this.Load += new System.EventHandler(this.uc_jackpot_Load);
      this.Click += new System.EventHandler(this.Control_Click);
      this.pnl_container.ResumeLayout(false);
      this.pnl_jackpot.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_jackpot;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lbl_jackpot_1;
    private System.Windows.Forms.Label lbl_jackpot;


  }
}
