namespace LCD_Display.Controls
{
  partial class uc_transferring
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uc_transferring));
      this.lbl_title = new System.Windows.Forms.Label();
      this.pb_transferring = new System.Windows.Forms.PictureBox();
      this.pnl_container.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_transferring)).BeginInit();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.lbl_title);
      this.pnl_container.Controls.Add(this.pb_transferring);
      // 
      // lbl_title
      // 
      this.lbl_title.AutoSize = true;
      this.lbl_title.Font = new System.Drawing.Font("Calibri", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_title.ForeColor = System.Drawing.Color.White;
      this.lbl_title.Location = new System.Drawing.Point(179, 170);
      this.lbl_title.Name = "lbl_title";
      this.lbl_title.Size = new System.Drawing.Size(236, 53);
      this.lbl_title.TabIndex = 1;
      this.lbl_title.Text = "Transferring";
      // 
      // pb_transferring
      // 
      this.pb_transferring.BackColor = System.Drawing.Color.Transparent;
      this.pb_transferring.Image = ((System.Drawing.Image)(resources.GetObject("pb_transferring.Image")));
      this.pb_transferring.InitialImage = null;
      this.pb_transferring.Location = new System.Drawing.Point(243, 66);
      this.pb_transferring.Name = "pb_transferring";
      this.pb_transferring.Size = new System.Drawing.Size(109, 90);
      this.pb_transferring.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pb_transferring.TabIndex = 0;
      this.pb_transferring.TabStop = false;
      // 
      // uc_transferring
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_transferring";
      this.Load += new System.EventHandler(this.uc_transferring_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pb_transferring)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pb_transferring;
    private System.Windows.Forms.Label lbl_title;

  }
}
