//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_account_menu.cs
// 
//   DESCRIPTION: Account Menu Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_account_menu : uc_body
  {
    #region Public Functions

    public uc_account_menu()
    {
      InitializeComponent();
    }

    #endregion

    #region Events

    private void btn_account_info_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.ACCOUNT_INFO);
    }

    private void btn_raffles_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.RAFFLES);
    }

    private void btn_promotions_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.PROMOTION_MENU);
    }

    private void btn_jackpot_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.JACKPOTS);
    }

    private void btn_gift_Click(object sender, EventArgs e)
    {
      this.MainForm.IsJackpotShowed = false;
      this.MainForm.BindContainer(TYPE_CONTAINER.GIFT_MENU);
    }

    #endregion
  }
}
