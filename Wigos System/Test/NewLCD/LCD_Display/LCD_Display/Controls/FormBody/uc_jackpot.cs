//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_jackpot.cs
// 
//   DESCRIPTION: Jackpot clase. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_jackpot : uc_body
  {
    #region Private Variables

    private Timer m_timer_close_jackpot = new Timer();
    
    #endregion
    
    #region Properties
    
    private Timer TimerCloseJackpot
    {
      get { return m_timer_close_jackpot;  }
      set { m_timer_close_jackpot = value; }
    }

    #endregion

    #region Public Functions

    public uc_jackpot()
    {
      InitializeComponent();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the timer to close the Jackpot
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void InitJackpotCloseTimer()
    {
      try
      {
        if (this.MainForm.IsJackpotShowed)
        {
          //to avoid multiple handlers' at the same time.
          this.TimerCloseJackpot.Tick -= new EventHandler(TimerCloseJackpotTick);
          this.TimerCloseJackpot.Tick += new EventHandler(TimerCloseJackpotTick);

          this.TimerCloseJackpot.Interval = 5000;
          this.TimerCloseJackpot.Start();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while initializing the timer to close Jackpot : " + _ex.Message);
      }
    }

    #endregion
        
    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: To manage the click behavior.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void ClickBehavior()
    {
      try
      {
        if (this.MainForm != null
             && this.MainForm.IsJackpotShowed)
        {
          if (!this.MainForm.IsCardInserted 
                && this.MainForm.Mode == TYPE_MODE.CASHLESS)
          {
            this.MainForm.InitVideo();
            this.MainForm.CallAttendant = false;
            this.MainForm.BindContainer(TYPE_CONTAINER.MAIN);
          }
          else
          {
            this.MainForm.CallAttendant = false;
            this.MainForm.TimerShowJackpot.Start();
            this.MainForm.BindContainer(TYPE_CONTAINER.MAIN);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred during Click Behavior event : " + _ex.Message);
      }
    }

    #endregion
    
    #region Events

    private void Control_Click(object sender, EventArgs e)
    {
      this.ClickBehavior();
    }

    private void uc_jackpot_Load(object sender, EventArgs e)
    {
      if (!this.MainForm.IsJackpotShowed)
      {        
        this.MainForm.TimerShowJackpot.Stop();
      }
      else
      {
        this.InitJackpotCloseTimer();
      }
    }

    private void TimerCloseJackpotTick(object sender, EventArgs e)
    {
      this.ClickBehavior();
      this.TimerCloseJackpot.Stop();
    }

    #endregion
  }
}
