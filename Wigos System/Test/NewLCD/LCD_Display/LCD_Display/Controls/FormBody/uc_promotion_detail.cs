//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_promotion_detail.cs
// 
//   DESCRIPTION: Promotion Detail Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_promotion_detail : uc_body
  {
    #region Public Functions

    public uc_promotion_detail()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    private void ShowPromotionDetail()
    {
      if (this.MainForm.Promotion == TYPE_PROMOTION.AVAILABLE)
      {
        this.pnl_promotion_available.BringToFront();
        this.pnl_promotion_available.Visible = true;
      }
      else
      {
        this.pnl_promotion_casino.BringToFront();
        this.pnl_promotion_casino.Visible = true;
        this.btn_redeem.Visible = false;
      }
    }

    #endregion

    #region Events

    private void uc_promotion_detail_Load(object sender, EventArgs e)
    {
      ShowPromotionDetail();
    }

    private void btn_redeem_Click(object sender, EventArgs e)
    {
      this.MainForm.Redeem = TYPE_REDEEM.PROMOTION;
      this.MainForm.BindContainer(TYPE_CONTAINER.REDEEM);
    }

    #endregion
  }
}
