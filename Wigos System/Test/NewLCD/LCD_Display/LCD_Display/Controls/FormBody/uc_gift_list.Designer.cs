namespace LCD_Display.Controls
{
  partial class uc_gift_list
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_loyalty_club = new System.Windows.Forms.Panel();
      this.uc_list_item = new LCD_Display.Controls.uc_list();
      this.lbl_gifts = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.pnl_loyalty_club.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_loyalty_club);
      // 
      // pnl_loyalty_club
      // 
      this.pnl_loyalty_club.BackgroundImage = global::LCD_Display.Properties.Resources.back_gifts;
      this.pnl_loyalty_club.Controls.Add(this.uc_list_item);
      this.pnl_loyalty_club.Controls.Add(this.lbl_gifts);
      this.pnl_loyalty_club.Location = new System.Drawing.Point(16, 13);
      this.pnl_loyalty_club.Name = "pnl_loyalty_club";
      this.pnl_loyalty_club.Size = new System.Drawing.Size(565, 274);
      this.pnl_loyalty_club.TabIndex = 0;
      // 
      // uc_list_item
      // 
      this.uc_list_item.BackColor = System.Drawing.Color.Transparent;
      this.uc_list_item.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.uc_list_item.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.uc_list_item.ItemList = null;
      this.uc_list_item.Location = new System.Drawing.Point(0, 56);
      this.uc_list_item.Name = "uc_list_item";
      this.uc_list_item.Size = new System.Drawing.Size(565, 218);
      this.uc_list_item.TabIndex = 1;
      // 
      // lbl_gifts
      // 
      this.lbl_gifts.Image = global::LCD_Display.Properties.Resources.LoyaltyTitle;
      this.lbl_gifts.Location = new System.Drawing.Point(0, 0);
      this.lbl_gifts.Name = "lbl_gifts";
      this.lbl_gifts.Size = new System.Drawing.Size(228, 57);
      this.lbl_gifts.TabIndex = 0;
      // 
      // uc_gift_list
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "uc_gift_list";
      this.Load += new System.EventHandler(this.uc_gift_list_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_loyalty_club.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_loyalty_club;
    private System.Windows.Forms.Label lbl_gifts;
    private uc_list uc_list_item;
  }
}
