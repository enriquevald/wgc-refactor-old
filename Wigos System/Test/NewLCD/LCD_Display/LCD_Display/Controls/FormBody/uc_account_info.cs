//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_account_info.cs
// 
//   DESCRIPTION: Account Info Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_account_info : uc_body
  {
    #region Public Functions

    public uc_account_info()
    {
      InitializeComponent();
    }

    public void SetAccountInfo()
    {
      ACCOUNT _account;

      try
      {
        _account = this.MainForm.Account;

        this.lbl_redeem_points.Text = _account.RedeemPoints.ToString();
        this.lbl_redeem_promo_points.Text = _account.RedeemPromoPoints.ToString();
        this.lbl_non_redeem_points.Text = _account.NonRedeemPoints.ToString();
        this.lbl_actual_level.Text = _account.ActualLevel;
        this.lbl_next_level.Text = _account.NextLevel;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while setting the account info : " + _ex.Message);
      }
    }

    #endregion

    #region Events

    private void uc_account_info_Load(object sender, EventArgs e)
    {
      this.SetAccountInfo();
    }

    #endregion
  }
}
