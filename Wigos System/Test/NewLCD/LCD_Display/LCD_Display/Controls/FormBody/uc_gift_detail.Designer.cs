namespace LCD_Display.Controls
{
  partial class uc_gift_detail
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_gift_detail = new System.Windows.Forms.Panel();
      this.lbl_gift_description = new System.Windows.Forms.Label();
      this.lbl_gift_name = new System.Windows.Forms.Label();
      this.pnl_gift_image = new System.Windows.Forms.Panel();
      this.btn_redeem = new LCD_Display.Controls.uc_button();
      this.pnl_container.SuspendLayout();
      this.pnl_gift_detail.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_gift_detail);
      // 
      // pnl_gift_detail
      // 
      this.pnl_gift_detail.BackgroundImage = global::LCD_Display.Properties.Resources.GiftDetail;
      this.pnl_gift_detail.Controls.Add(this.lbl_gift_description);
      this.pnl_gift_detail.Controls.Add(this.lbl_gift_name);
      this.pnl_gift_detail.Controls.Add(this.pnl_gift_image);
      this.pnl_gift_detail.Controls.Add(this.btn_redeem);
      this.pnl_gift_detail.Location = new System.Drawing.Point(17, 11);
      this.pnl_gift_detail.Name = "pnl_gift_detail";
      this.pnl_gift_detail.Size = new System.Drawing.Size(560, 275);
      this.pnl_gift_detail.TabIndex = 3;
      // 
      // lbl_gift_description
      // 
      this.lbl_gift_description.AutoSize = true;
      this.lbl_gift_description.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_gift_description.ForeColor = System.Drawing.Color.Maroon;
      this.lbl_gift_description.Location = new System.Drawing.Point(26, 92);
      this.lbl_gift_description.MaximumSize = new System.Drawing.Size(280, 175);
      this.lbl_gift_description.Name = "lbl_gift_description";
      this.lbl_gift_description.Size = new System.Drawing.Size(112, 23);
      this.lbl_gift_description.TabIndex = 7;
      this.lbl_gift_description.Text = "xDescription";
      // 
      // lbl_gift_name
      // 
      this.lbl_gift_name.AutoSize = true;
      this.lbl_gift_name.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_gift_name.ForeColor = System.Drawing.Color.Maroon;
      this.lbl_gift_name.Location = new System.Drawing.Point(25, 63);
      this.lbl_gift_name.MaximumSize = new System.Drawing.Size(200, 0);
      this.lbl_gift_name.Name = "lbl_gift_name";
      this.lbl_gift_name.Size = new System.Drawing.Size(98, 29);
      this.lbl_gift_name.TabIndex = 6;
      this.lbl_gift_name.Text = "xName";
      // 
      // pnl_gift_image
      // 
      this.pnl_gift_image.Location = new System.Drawing.Point(336, 7);
      this.pnl_gift_image.Name = "pnl_gift_image";
      this.pnl_gift_image.Size = new System.Drawing.Size(205, 180);
      this.pnl_gift_image.TabIndex = 5;
      // 
      // btn_redeem
      // 
      this.btn_redeem.BackColor = System.Drawing.Color.Transparent;
      this.btn_redeem.BackgroundImage = global::LCD_Display.Properties.Resources.ConfirmButtonNormal;
      this.btn_redeem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.btn_redeem.FlatAppearance.BorderSize = 0;
      this.btn_redeem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_redeem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_redeem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_redeem.Location = new System.Drawing.Point(336, 205);
      this.btn_redeem.Name = "btn_redeem";
      this.btn_redeem.Size = new System.Drawing.Size(205, 64);
      this.btn_redeem.TabIndex = 4;
      this.btn_redeem.UseVisualStyleBackColor = false;
      this.btn_redeem.Click += new System.EventHandler(this.btn_redeem_Click);
      // 
      // uc_gift_detail
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_gift_detail";
      this.Load += new System.EventHandler(this.uc_gift_detail_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_gift_detail.ResumeLayout(false);
      this.pnl_gift_detail.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_gift_detail;
    private uc_button btn_redeem;
    private System.Windows.Forms.Label lbl_gift_name;
    private System.Windows.Forms.Panel pnl_gift_image;
    private System.Windows.Forms.Label lbl_gift_description;
  }
}
