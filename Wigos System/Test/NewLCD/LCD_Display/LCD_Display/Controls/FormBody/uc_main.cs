//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_main.cs
// 
//   DESCRIPTION: User Control Main. 
// 
//        AUTHOR: Carlos Cáceres.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_main : uc_body
  {
    #region Public Variables

    public Timer m_timer_move_label = new Timer();

    #endregion

    #region Private Variables

    private Int16 m_enter_operator_mode_clicks = 0;

    #endregion
    
    #region Properties

    public Timer TimerMoveLabel
    { 
      get{ return this.m_timer_move_label; }
      set{ this.m_timer_move_label = value;}
    }

    #endregion
    
    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public uc_main()
    {
      InitializeComponent();    
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To Show the call attendant confirmation button.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void CallAttendant()
    {
      this.pnl_main_menu.Visible = false;
      this.pnl_main_menu.SendToBack();
      this.btn_confirm.Visible = true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To Show the calling attendant message.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void CallingAttendant()
    {
      this.pnl_calling_an_attendant.BringToFront();
      this.pnl_calling_an_attendant.Visible = true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To confirm the call to an attendant.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void ConfirmCall()
    {
      try
      {
        this.btn_confirm.Visible = false;
        System.Threading.Thread.Sleep(2500);

        this.pnl_calling_an_attendant.SendToBack();
        this.pnl_calling_an_attendant.Visible = false;

        this.pnl_main_menu.Visible = true;
        this.pnl_main_menu.BringToFront();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred confirming the call to an attendant: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize the marquee movement.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void InitMarqueeMovement()
    {
      try
      {
        if (this.TimerMoveLabel.Enabled == false)
        {
          //to avoid multiple handlers' at the same time.
          this.TimerMoveLabel.Tick -= new EventHandler(_timer_Tick_MoveLabel);
          this.TimerMoveLabel.Tick += new EventHandler(_timer_Tick_MoveLabel);

          this.TimerMoveLabel.Interval = 8;
          this.TimerMoveLabel.Start();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred initializing the marquee movement: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Timer tick for marquee movement.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - object sender.
    //          - EventArgs e.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    void _timer_Tick_MoveLabel(object sender, EventArgs e)
    {
      try
      {
        this.TimerMoveLabel.Stop();
        this.lbl_msg.Location = this.MainForm.MoveMsgLabel(this.lbl_msg, this.Width);
        this.TimerMoveLabel.Start();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred during the timer tick to move marquee: " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To check if the marquee needs to be showed.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void CheckToShowMsg()
    {
      try
      {
        this.TimerMoveLabel.Stop();

        if (this.MainForm.MarqueeText == string.Empty)
        {
          this.pnl_marquee_bot.Visible = false;
        }
        else
        {
          this.pnl_marquee_bot.Visible = true;
          this.lbl_msg.Text = this.MainForm.MarqueeText;
          this.TimerMoveLabel.Start();
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while checking to show marquee message: " + _ex.Message);
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE: To disable the main menu buttons.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void DisableMainMenuButtons()
    {
      // Change Background
      this.btn_account.BackgroundImage        = Resources.AccountButtonDisabled;
      this.btn_call_attendant.BackgroundImage = Resources.CallAttendantButtonBigDisabled;

      // Set buttons as Disabled
      this.btn_account.Enabled        = false;
      this.btn_call_attendant.Enabled = false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To enable the main menu buttons.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void EnableMainMenuButtons()
    {
      // Change Background
      this.btn_account.BackgroundImage        = Resources.AccountButton;
      this.btn_call_attendant.BackgroundImage = Resources.CallAttendantButtonBig;

      // Set buttons as Enabled
      this.btn_account.Enabled = true;
      this.btn_call_attendant.Enabled = true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set the call attendant button visibility
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Visible.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void SetButtonCallAttendantVisibility(Boolean Visible)
    {
      this.btn_call_attendant.Visible = Visible;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: To set the Account button visibility
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Visible.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void SetButtonAccountVisibility(Boolean Visible)
    {
      this.btn_account.Visible = Visible;
    }

    #endregion
    
    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: To initialize controls
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private void InitializeControls()
    {
      try
      {
        if (this.MainForm.IsCashOut == true)
        {
          this.SetButtonCallAttendantVisibility(false);
        }

        if(this.MainForm.Mode == TYPE_MODE.TITO && this.MainForm.IsCardInserted == false)
        {
          this.SetButtonAccountVisibility(false);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred Initializing the controls in UC Main: " + _ex.Message);
      }
    }

    #endregion
   
    #region Events

    private void btn_call_attendant_Click(object sender, EventArgs e)
    {
      CallAttendant();
    }

    private void btn_confirm_MouseDown(object sender, MouseEventArgs e)
    {
      CallingAttendant();
    }

    private void btn_confirm_MouseUp(object sender, MouseEventArgs e)
    {
      ConfirmCall();
    }

    private void btn_account_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.LOGIN);
    }

    private void uc_main_Load(object sender, EventArgs e)
    {
      this.InitializeControls();
      this.InitMarqueeMovement();
      this.CheckToShowMsg();

      if (this.MainForm.CallAttendant)
      {
        this.MainForm.CallAttendant = false;
        CallAttendant();
      }
    }

    private void pnl_operator_Click(object sender, EventArgs e)
    {
      this.m_enter_operator_mode_clicks++;

      if (this.m_enter_operator_mode_clicks == 4)
      {
        this.MainForm.BindContainer(TYPE_CONTAINER.OPERATOR);
      }
    }

    #endregion
  }
}
