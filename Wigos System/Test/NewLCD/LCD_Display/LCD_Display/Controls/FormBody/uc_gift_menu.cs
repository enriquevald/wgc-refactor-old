//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gift_menu.cs
// 
//   DESCRIPTION: Gift's Menu Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_gift_menu : uc_body
  {
    #region Public Functions

    public uc_gift_menu()
    {
      InitializeComponent();
    }

    #endregion

    #region Events

    private void btn_available_gifts_Click(object sender, EventArgs e)
    {
      this.MainForm.Gift = TYPE_GIFT.AVAILABLE;
      this.MainForm.BindContainer(TYPE_CONTAINER.GIFTS_LIST);
    }

    private void btn_next_gifts_Click(object sender, EventArgs e)
    {
      this.MainForm.Gift = TYPE_GIFT.NEXTS;
      this.MainForm.BindContainer(TYPE_CONTAINER.GIFTS_LIST);
    }

    #endregion
  }
}
