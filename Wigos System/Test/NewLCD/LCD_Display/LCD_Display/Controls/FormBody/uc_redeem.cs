//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_redeem.cs
// 
//   DESCRIPTION: Redeem Control. 
// 
//        AUTHOR: Carlos Cáceres
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_redeem : uc_body
  {
    #region Public Functions

    public uc_redeem()
    {
      InitializeComponent();
    }

    #endregion
    
    #region Private Functions

    private void ShowRedeem()
    {
      if (this.MainForm.Redeem == TYPE_REDEEM.PROMOTION)
      {
        if (this.MainForm.Promotion == TYPE_PROMOTION.AVAILABLE)
        {
          this.pnl_promotion_redeem.BringToFront();
          this.pnl_promotion_redeem.Visible = true;
        }
        else
        {
        this.pnl_casino_redeem.BringToFront();
        this.pnl_casino_redeem.Visible = true;
        }
      }
      else // TYPE_REDEEM.GIFT
      {
        this.pnl_promotion_redeem.Visible = false;
        this.pnl_casino_redeem.Visible    = false;
        this.pnl_gift_detail.Visible      = true;
        this.pnl_gift_detail.BringToFront();
      }
    }

    private void LoadGiftInformation()
    {
      try
      {
        this.lbl_gift_description.Text      = this.MainForm.SelectedItem.Description;
        this.lbl_gift_name.Text             = this.MainForm.SelectedItem.Name;
        this.pnl_gift_image.BackgroundImage = this.MainForm.SelectedItem.Image;
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while loading the gift information: " + _ex.Message);
      }
    }

    #endregion
    
    #region Events

    private void btn_confirm_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.TRANSFERRING);
    }

    private void uc_redeem_Load(object sender, EventArgs e)
    {
      this.ShowRedeem();
      this.LoadGiftInformation();
    }

    #endregion
  }
}
