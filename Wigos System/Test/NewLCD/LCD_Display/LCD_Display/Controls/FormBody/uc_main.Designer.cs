namespace LCD_Display.Controls
{
  partial class uc_main
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_calling_an_attendant = new System.Windows.Forms.Panel();
      this.btn_language_en = new LCD_Display.Controls.uc_button();
      this.btn_language_es = new LCD_Display.Controls.uc_button();
      this.btn_confirm = new LCD_Display.Controls.uc_button();
      this.pnl_main_menu = new System.Windows.Forms.Panel();
      this.btn_call_attendant = new LCD_Display.Controls.uc_button();
      this.btn_account = new LCD_Display.Controls.uc_button();
      this.pnl_operator = new System.Windows.Forms.Panel();
      this.pnl_marquee_bot = new System.Windows.Forms.Panel();
      this.lbl_msg = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.pnl_main_menu.SuspendLayout();
      this.pnl_marquee_bot.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_marquee_bot);
      this.pnl_container.Controls.Add(this.pnl_operator);
      this.pnl_container.Controls.Add(this.pnl_main_menu);
      this.pnl_container.Controls.Add(this.btn_confirm);
      this.pnl_container.Controls.Add(this.btn_language_en);
      this.pnl_container.Controls.Add(this.btn_language_es);
      this.pnl_container.Controls.Add(this.pnl_calling_an_attendant);
      // 
      // pnl_calling_an_attendant
      // 
      this.pnl_calling_an_attendant.BackgroundImage = global::LCD_Display.Properties.Resources.PleaseWait;
      this.pnl_calling_an_attendant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.pnl_calling_an_attendant.Location = new System.Drawing.Point(37, 112);
      this.pnl_calling_an_attendant.Name = "pnl_calling_an_attendant";
      this.pnl_calling_an_attendant.Size = new System.Drawing.Size(520, 77);
      this.pnl_calling_an_attendant.TabIndex = 9;
      this.pnl_calling_an_attendant.Visible = false;
      // 
      // btn_language_en
      // 
      this.btn_language_en.BackColor = System.Drawing.Color.Transparent;
      this.btn_language_en.BackgroundImage = global::LCD_Display.Properties.Resources.USAFlag;
      this.btn_language_en.FlatAppearance.BorderSize = 0;
      this.btn_language_en.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_language_en.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_language_en.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_language_en.Location = new System.Drawing.Point(524, 16);
      this.btn_language_en.Name = "btn_language_en";
      this.btn_language_en.Size = new System.Drawing.Size(55, 36);
      this.btn_language_en.TabIndex = 11;
      this.btn_language_en.UseVisualStyleBackColor = true;
      // 
      // btn_language_es
      // 
      this.btn_language_es.BackColor = System.Drawing.Color.Transparent;
      this.btn_language_es.BackgroundImage = global::LCD_Display.Properties.Resources.SpanishFlag;
      this.btn_language_es.FlatAppearance.BorderSize = 0;
      this.btn_language_es.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_language_es.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_language_es.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_language_es.Location = new System.Drawing.Point(461, 16);
      this.btn_language_es.Name = "btn_language_es";
      this.btn_language_es.Size = new System.Drawing.Size(55, 36);
      this.btn_language_es.TabIndex = 10;
      this.btn_language_es.UseVisualStyleBackColor = true;
      // 
      // btn_confirm
      // 
      this.btn_confirm.BackColor = System.Drawing.Color.Transparent;
      this.btn_confirm.BackgroundImage = global::LCD_Display.Properties.Resources.ConfirmButton;
      this.btn_confirm.FlatAppearance.BorderSize = 0;
      this.btn_confirm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_confirm.Location = new System.Drawing.Point(126, 114);
      this.btn_confirm.Name = "btn_confirm";
      this.btn_confirm.Size = new System.Drawing.Size(342, 73);
      this.btn_confirm.TabIndex = 12;
      this.btn_confirm.UseVisualStyleBackColor = true;
      this.btn_confirm.Visible = false;
      this.btn_confirm.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_confirm_MouseDown);
      this.btn_confirm.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_confirm_MouseUp);
      // 
      // pnl_main_menu
      // 
      this.pnl_main_menu.Controls.Add(this.btn_call_attendant);
      this.pnl_main_menu.Controls.Add(this.btn_account);
      this.pnl_main_menu.Location = new System.Drawing.Point(123, 72);
      this.pnl_main_menu.Name = "pnl_main_menu";
      this.pnl_main_menu.Size = new System.Drawing.Size(349, 157);
      this.pnl_main_menu.TabIndex = 13;
      // 
      // btn_call_attendant
      // 
      this.btn_call_attendant.BackColor = System.Drawing.Color.Transparent;
      this.btn_call_attendant.BackgroundImage = global::LCD_Display.Properties.Resources.CallAttendantButtonBig;
      this.btn_call_attendant.FlatAppearance.BorderSize = 0;
      this.btn_call_attendant.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_call_attendant.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_call_attendant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_call_attendant.Location = new System.Drawing.Point(3, 3);
      this.btn_call_attendant.Name = "btn_call_attendant";
      this.btn_call_attendant.Size = new System.Drawing.Size(342, 73);
      this.btn_call_attendant.TabIndex = 0;
      this.btn_call_attendant.UseVisualStyleBackColor = true;
      this.btn_call_attendant.Click += new System.EventHandler(this.btn_call_attendant_Click);
      // 
      // btn_account
      // 
      this.btn_account.BackColor = System.Drawing.Color.Transparent;
      this.btn_account.BackgroundImage = global::LCD_Display.Properties.Resources.AccountButton;
      this.btn_account.FlatAppearance.BorderSize = 0;
      this.btn_account.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_account.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_account.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_account.Location = new System.Drawing.Point(3, 82);
      this.btn_account.Name = "btn_account";
      this.btn_account.Size = new System.Drawing.Size(342, 73);
      this.btn_account.TabIndex = 1;
      this.btn_account.UseVisualStyleBackColor = true;
      this.btn_account.Click += new System.EventHandler(this.btn_account_Click);
      // 
      // pnl_operator
      // 
      this.pnl_operator.Location = new System.Drawing.Point(0, 0);
      this.pnl_operator.Name = "pnl_operator";
      this.pnl_operator.Size = new System.Drawing.Size(124, 73);
      this.pnl_operator.TabIndex = 14;
      this.pnl_operator.Click += new System.EventHandler(this.pnl_operator_Click);
      // 
      // pnl_marquee_bot
      // 
      this.pnl_marquee_bot.BackColor = System.Drawing.Color.Gold;
      this.pnl_marquee_bot.Controls.Add(this.lbl_msg);
      this.pnl_marquee_bot.Location = new System.Drawing.Point(0, 270);
      this.pnl_marquee_bot.Name = "pnl_marquee_bot";
      this.pnl_marquee_bot.Size = new System.Drawing.Size(595, 30);
      this.pnl_marquee_bot.TabIndex = 15;
      this.pnl_marquee_bot.Visible = false;
      // 
      // lbl_msg
      // 
      this.lbl_msg.AutoSize = true;
      this.lbl_msg.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_msg.Location = new System.Drawing.Point(595, 1);
      this.lbl_msg.Name = "lbl_msg";
      this.lbl_msg.Size = new System.Drawing.Size(112, 29);
      this.lbl_msg.TabIndex = 1;
      this.lbl_msg.Text = "xMessage";
      // 
      // uc_main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_main";
      this.Load += new System.EventHandler(this.uc_main_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_main_menu.ResumeLayout(false);
      this.pnl_marquee_bot.ResumeLayout(false);
      this.pnl_marquee_bot.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_calling_an_attendant;
    private LCD_Display.Controls.uc_button btn_language_en;
    private LCD_Display.Controls.uc_button btn_language_es;
    private LCD_Display.Controls.uc_button btn_confirm;
    private System.Windows.Forms.Panel pnl_main_menu;
    private LCD_Display.Controls.uc_button btn_call_attendant;
    private LCD_Display.Controls.uc_button btn_account;
    private System.Windows.Forms.Panel pnl_operator;
    private System.Windows.Forms.Panel pnl_marquee_bot;
    private System.Windows.Forms.Label lbl_msg;

  }
}
