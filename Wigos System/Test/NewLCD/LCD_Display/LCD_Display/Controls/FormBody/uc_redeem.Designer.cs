namespace LCD_Display.Controls
{
  partial class uc_redeem
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_promotion_redeem = new System.Windows.Forms.Panel();
      this.pnl_casino_redeem = new System.Windows.Forms.Panel();
      this.btn_confirm = new LCD_Display.Controls.uc_button();
      this.pnl_gift_detail = new System.Windows.Forms.Panel();
      this.lbl_gift_description = new System.Windows.Forms.Label();
      this.lbl_gift_name = new System.Windows.Forms.Label();
      this.pnl_gift_image = new System.Windows.Forms.Panel();
      this.pnl_container.SuspendLayout();
      this.pnl_gift_detail.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_gift_detail);
      this.pnl_container.Controls.Add(this.btn_confirm);
      this.pnl_container.Controls.Add(this.pnl_promotion_redeem);
      this.pnl_container.Controls.Add(this.pnl_casino_redeem);
      // 
      // pnl_promotion_redeem
      // 
      this.pnl_promotion_redeem.BackgroundImage = global::LCD_Display.Properties.Resources.PromotionDetail;
      this.pnl_promotion_redeem.Location = new System.Drawing.Point(17, 11);
      this.pnl_promotion_redeem.Name = "pnl_promotion_redeem";
      this.pnl_promotion_redeem.Size = new System.Drawing.Size(560, 206);
      this.pnl_promotion_redeem.TabIndex = 0;
      // 
      // pnl_casino_redeem
      // 
      this.pnl_casino_redeem.BackgroundImage = global::LCD_Display.Properties.Resources.CasinoPromotionDetail;
      this.pnl_casino_redeem.Location = new System.Drawing.Point(17, 11);
      this.pnl_casino_redeem.Name = "pnl_casino_redeem";
      this.pnl_casino_redeem.Size = new System.Drawing.Size(560, 206);
      this.pnl_casino_redeem.TabIndex = 1;
      // 
      // btn_confirm
      // 
      this.btn_confirm.BackColor = System.Drawing.Color.White;
      this.btn_confirm.BackgroundImage = global::LCD_Display.Properties.Resources.ConfirmButtonNormal;
      this.btn_confirm.FlatAppearance.BorderSize = 0;
      this.btn_confirm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_confirm.Location = new System.Drawing.Point(161, 227);
      this.btn_confirm.Name = "btn_confirm";
      this.btn_confirm.Size = new System.Drawing.Size(272, 59);
      this.btn_confirm.TabIndex = 2;
      this.btn_confirm.UseVisualStyleBackColor = true;
      this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
      // 
      // pnl_gift_detail
      // 
      this.pnl_gift_detail.BackgroundImage = global::LCD_Display.Properties.Resources.GiftDetail;
      this.pnl_gift_detail.Controls.Add(this.lbl_gift_description);
      this.pnl_gift_detail.Controls.Add(this.lbl_gift_name);
      this.pnl_gift_detail.Controls.Add(this.pnl_gift_image);
      this.pnl_gift_detail.Location = new System.Drawing.Point(17, 11);
      this.pnl_gift_detail.Name = "pnl_gift_detail";
      this.pnl_gift_detail.Size = new System.Drawing.Size(560, 210);
      this.pnl_gift_detail.TabIndex = 4;
      this.pnl_gift_detail.Visible = false;
      // 
      // lbl_gift_description
      // 
      this.lbl_gift_description.AutoSize = true;
      this.lbl_gift_description.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_gift_description.ForeColor = System.Drawing.Color.Maroon;
      this.lbl_gift_description.Location = new System.Drawing.Point(26, 92);
      this.lbl_gift_description.MaximumSize = new System.Drawing.Size(280, 130);
      this.lbl_gift_description.Name = "lbl_gift_description";
      this.lbl_gift_description.Size = new System.Drawing.Size(112, 23);
      this.lbl_gift_description.TabIndex = 7;
      this.lbl_gift_description.Text = "xDescription";
      // 
      // lbl_gift_name
      // 
      this.lbl_gift_name.AutoSize = true;
      this.lbl_gift_name.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_gift_name.ForeColor = System.Drawing.Color.Maroon;
      this.lbl_gift_name.Location = new System.Drawing.Point(25, 63);
      this.lbl_gift_name.MaximumSize = new System.Drawing.Size(200, 0);
      this.lbl_gift_name.Name = "lbl_gift_name";
      this.lbl_gift_name.Size = new System.Drawing.Size(98, 29);
      this.lbl_gift_name.TabIndex = 6;
      this.lbl_gift_name.Text = "xName";
      // 
      // pnl_gift_image
      // 
      this.pnl_gift_image.Location = new System.Drawing.Point(336, 7);
      this.pnl_gift_image.Name = "pnl_gift_image";
      this.pnl_gift_image.Size = new System.Drawing.Size(205, 180);
      this.pnl_gift_image.TabIndex = 5;
      // 
      // uc_redeem
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_redeem";
      this.Load += new System.EventHandler(this.uc_redeem_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_gift_detail.ResumeLayout(false);
      this.pnl_gift_detail.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnl_promotion_redeem;
    private System.Windows.Forms.Panel pnl_casino_redeem;
    private uc_button btn_confirm;
    private System.Windows.Forms.Panel pnl_gift_detail;
    private System.Windows.Forms.Label lbl_gift_description;
    private System.Windows.Forms.Label lbl_gift_name;
    private System.Windows.Forms.Panel pnl_gift_image;
  }
}
