namespace LCD_Display.Controls
{
  partial class uc_promotion_menu
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_buttons = new System.Windows.Forms.Panel();
      this.btn_casino_promotions = new LCD_Display.Controls.uc_button();
      this.btn_player_promotions = new LCD_Display.Controls.uc_button();
      this.pnl_container.SuspendLayout();
      this.pnl_buttons.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.pnl_buttons);
      // 
      // pnl_buttons
      // 
      this.pnl_buttons.Controls.Add(this.btn_casino_promotions);
      this.pnl_buttons.Controls.Add(this.btn_player_promotions);
      this.pnl_buttons.Location = new System.Drawing.Point(112, 49);
      this.pnl_buttons.Name = "pnl_buttons";
      this.pnl_buttons.Size = new System.Drawing.Size(371, 202);
      this.pnl_buttons.TabIndex = 0;
      // 
      // btn_casino_promotions
      // 
      this.btn_casino_promotions.BackColor = System.Drawing.Color.Transparent;
      this.btn_casino_promotions.BackgroundImage = global::LCD_Display.Properties.Resources.CasinoPromotionButton;
      this.btn_casino_promotions.FlatAppearance.BorderSize = 0;
      this.btn_casino_promotions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_casino_promotions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_casino_promotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_casino_promotions.Location = new System.Drawing.Point(15, 111);
      this.btn_casino_promotions.Name = "btn_casino_promotions";
      this.btn_casino_promotions.Size = new System.Drawing.Size(342, 73);
      this.btn_casino_promotions.TabIndex = 1;
      this.btn_casino_promotions.UseVisualStyleBackColor = false;
      this.btn_casino_promotions.Click += new System.EventHandler(this.btn_casino_propotions_Click);
      // 
      // btn_player_promotions
      // 
      this.btn_player_promotions.BackColor = System.Drawing.Color.Transparent;
      this.btn_player_promotions.BackgroundImage = global::LCD_Display.Properties.Resources.AvailablePromotionButton;
      this.btn_player_promotions.FlatAppearance.BorderSize = 0;
      this.btn_player_promotions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_player_promotions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_player_promotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_player_promotions.Location = new System.Drawing.Point(15, 23);
      this.btn_player_promotions.Name = "btn_player_promotions";
      this.btn_player_promotions.Size = new System.Drawing.Size(342, 73);
      this.btn_player_promotions.TabIndex = 0;
      this.btn_player_promotions.UseVisualStyleBackColor = false;
      this.btn_player_promotions.Click += new System.EventHandler(this.btn_player_promotions_Click);
      // 
      // uc_promotion_menu
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_promotion_menu";
      this.pnl_container.ResumeLayout(false);
      this.pnl_buttons.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_button btn_casino_promotions;
    private uc_button btn_player_promotions;
    private System.Windows.Forms.Panel pnl_buttons;
  }
}
