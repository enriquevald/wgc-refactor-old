//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_operator.cs
// 
//   DESCRIPTION: Operator Control. 
// 
//        AUTHOR: Carlos Cáceres.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_operator : uc_body
  {
    #region Public Functions

    public uc_operator()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    private void InitializeControls()
    {
      this.MainForm.btn_bot.Visible = false;
    }

    #endregion
    
    #region Events

    private void uc_operator_Load(object sender, EventArgs e)
    {
      this.MainForm.TimerShowJackpot.Stop();
      this.MainForm.EnableDisableButtonBack(true);
      InitializeControls();
    }

    private void btn_status_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.STATUS);
    }

    private void btn_log_Click(object sender, EventArgs e)
    {
      this.MainForm.BindContainer(TYPE_CONTAINER.LOG_VIEWER);
    }

    #endregion
  }
}
