namespace LCD_Display.Controls
{
  partial class uc_login
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txt_pin = new System.Windows.Forms.TextBox();
      this.btn_7 = new LCD_Display.Controls.uc_button();
      this.btn_2 = new LCD_Display.Controls.uc_button();
      this.btn_9 = new LCD_Display.Controls.uc_button();
      this.btn_4 = new LCD_Display.Controls.uc_button();
      this.btn_enter = new LCD_Display.Controls.uc_button();
      this.btn_5 = new LCD_Display.Controls.uc_button();
      this.btn_0 = new LCD_Display.Controls.uc_button();
      this.btn_8 = new LCD_Display.Controls.uc_button();
      this.btn_6 = new LCD_Display.Controls.uc_button();
      this.btn_3 = new LCD_Display.Controls.uc_button();
      this.btn_1 = new LCD_Display.Controls.uc_button();
      this.lbl_login = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.lbl_login);
      this.pnl_container.Controls.Add(this.txt_pin);
      this.pnl_container.Controls.Add(this.btn_7);
      this.pnl_container.Controls.Add(this.btn_2);
      this.pnl_container.Controls.Add(this.btn_9);
      this.pnl_container.Controls.Add(this.btn_4);
      this.pnl_container.Controls.Add(this.btn_enter);
      this.pnl_container.Controls.Add(this.btn_5);
      this.pnl_container.Controls.Add(this.btn_0);
      this.pnl_container.Controls.Add(this.btn_8);
      this.pnl_container.Controls.Add(this.btn_6);
      this.pnl_container.Controls.Add(this.btn_3);
      this.pnl_container.Controls.Add(this.btn_1);
      this.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill;
      // 
      // txt_pin
      // 
      this.txt_pin.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txt_pin.Font = new System.Drawing.Font("Arial", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txt_pin.ForeColor = System.Drawing.SystemColors.WindowFrame;
      this.txt_pin.Location = new System.Drawing.Point(119, 107);
      this.txt_pin.MaximumSize = new System.Drawing.Size(0, 46);
      this.txt_pin.MaxLength = 4;
      this.txt_pin.MinimumSize = new System.Drawing.Size(329, 46);
      this.txt_pin.Name = "txt_pin";
      this.txt_pin.PasswordChar = '*';
      this.txt_pin.Size = new System.Drawing.Size(329, 46);
      this.txt_pin.TabIndex = 25;
      this.txt_pin.Text = "_pin";
      this.txt_pin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.txt_pin.Click += new System.EventHandler(this.txt_pin_Enter);
      // 
      // btn_7
      // 
      this.btn_7.BackColor = System.Drawing.Color.White;
      this.btn_7.BackgroundImage = global::LCD_Display.Properties.Resources.Num7;
      this.btn_7.FlatAppearance.BorderSize = 0;
      this.btn_7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_7.Location = new System.Drawing.Point(253, 218);
      this.btn_7.Name = "btn_7";
      this.btn_7.Size = new System.Drawing.Size(61, 50);
      this.btn_7.TabIndex = 20;
      this.btn_7.UseVisualStyleBackColor = true;
      this.btn_7.Click += new System.EventHandler(this.btn_7_Click);
      // 
      // btn_2
      // 
      this.btn_2.BackColor = System.Drawing.Color.White;
      this.btn_2.BackgroundImage = global::LCD_Display.Properties.Resources.Num2;
      this.btn_2.FlatAppearance.BorderSize = 0;
      this.btn_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_2.Location = new System.Drawing.Point(253, 160);
      this.btn_2.Name = "btn_2";
      this.btn_2.Size = new System.Drawing.Size(61, 50);
      this.btn_2.TabIndex = 22;
      this.btn_2.UseVisualStyleBackColor = true;
      this.btn_2.Click += new System.EventHandler(this.btn_2_Click);
      // 
      // btn_9
      // 
      this.btn_9.BackColor = System.Drawing.Color.White;
      this.btn_9.BackgroundImage = global::LCD_Display.Properties.Resources.Num9;
      this.btn_9.FlatAppearance.BorderSize = 0;
      this.btn_9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_9.Location = new System.Drawing.Point(387, 218);
      this.btn_9.Name = "btn_9";
      this.btn_9.Size = new System.Drawing.Size(61, 50);
      this.btn_9.TabIndex = 24;
      this.btn_9.UseVisualStyleBackColor = true;
      this.btn_9.Click += new System.EventHandler(this.btn_9_Click);
      // 
      // btn_4
      // 
      this.btn_4.BackColor = System.Drawing.Color.White;
      this.btn_4.BackgroundImage = global::LCD_Display.Properties.Resources.Num4;
      this.btn_4.FlatAppearance.BorderSize = 0;
      this.btn_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_4.Location = new System.Drawing.Point(387, 160);
      this.btn_4.Name = "btn_4";
      this.btn_4.Size = new System.Drawing.Size(61, 50);
      this.btn_4.TabIndex = 23;
      this.btn_4.UseVisualStyleBackColor = true;
      this.btn_4.Click += new System.EventHandler(this.btn_4_Click);
      // 
      // btn_enter
      // 
      this.btn_enter.BackColor = System.Drawing.Color.White;
      this.btn_enter.BackgroundImage = global::LCD_Display.Properties.Resources.OkButton;
      this.btn_enter.FlatAppearance.BorderSize = 0;
      this.btn_enter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_enter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_enter.Location = new System.Drawing.Point(455, 160);
      this.btn_enter.Name = "btn_enter";
      this.btn_enter.Size = new System.Drawing.Size(78, 108);
      this.btn_enter.TabIndex = 21;
      this.btn_enter.UseVisualStyleBackColor = true;
      this.btn_enter.Click += new System.EventHandler(this.btn_enter_Click);
      // 
      // btn_5
      // 
      this.btn_5.BackColor = System.Drawing.Color.White;
      this.btn_5.BackgroundImage = global::LCD_Display.Properties.Resources.Num5;
      this.btn_5.FlatAppearance.BorderSize = 0;
      this.btn_5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_5.Location = new System.Drawing.Point(119, 218);
      this.btn_5.Name = "btn_5";
      this.btn_5.Size = new System.Drawing.Size(61, 50);
      this.btn_5.TabIndex = 15;
      this.btn_5.UseVisualStyleBackColor = true;
      this.btn_5.Click += new System.EventHandler(this.btn_5_Click);
      // 
      // btn_0
      // 
      this.btn_0.BackColor = System.Drawing.Color.White;
      this.btn_0.BackgroundImage = global::LCD_Display.Properties.Resources.Num0;
      this.btn_0.FlatAppearance.BorderSize = 0;
      this.btn_0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_0.Location = new System.Drawing.Point(119, 160);
      this.btn_0.Name = "btn_0";
      this.btn_0.Size = new System.Drawing.Size(61, 50);
      this.btn_0.TabIndex = 14;
      this.btn_0.UseVisualStyleBackColor = true;
      this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
      // 
      // btn_8
      // 
      this.btn_8.BackColor = System.Drawing.Color.White;
      this.btn_8.BackgroundImage = global::LCD_Display.Properties.Resources.Num8;
      this.btn_8.FlatAppearance.BorderSize = 0;
      this.btn_8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_8.Location = new System.Drawing.Point(320, 218);
      this.btn_8.Name = "btn_8";
      this.btn_8.Size = new System.Drawing.Size(61, 50);
      this.btn_8.TabIndex = 16;
      this.btn_8.UseVisualStyleBackColor = true;
      this.btn_8.Click += new System.EventHandler(this.btn_8_Click);
      // 
      // btn_6
      // 
      this.btn_6.BackColor = System.Drawing.Color.White;
      this.btn_6.BackgroundImage = global::LCD_Display.Properties.Resources.Num6;
      this.btn_6.FlatAppearance.BorderSize = 0;
      this.btn_6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_6.Location = new System.Drawing.Point(186, 218);
      this.btn_6.Name = "btn_6";
      this.btn_6.Size = new System.Drawing.Size(61, 50);
      this.btn_6.TabIndex = 18;
      this.btn_6.UseVisualStyleBackColor = true;
      this.btn_6.Click += new System.EventHandler(this.btn_6_Click);
      // 
      // btn_3
      // 
      this.btn_3.BackColor = System.Drawing.Color.White;
      this.btn_3.BackgroundImage = global::LCD_Display.Properties.Resources.Num3;
      this.btn_3.FlatAppearance.BorderSize = 0;
      this.btn_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_3.Location = new System.Drawing.Point(320, 160);
      this.btn_3.Name = "btn_3";
      this.btn_3.Size = new System.Drawing.Size(61, 50);
      this.btn_3.TabIndex = 17;
      this.btn_3.UseVisualStyleBackColor = true;
      this.btn_3.Click += new System.EventHandler(this.btn_3_Click);
      // 
      // btn_1
      // 
      this.btn_1.BackColor = System.Drawing.Color.White;
      this.btn_1.BackgroundImage = global::LCD_Display.Properties.Resources.Num1;
      this.btn_1.FlatAppearance.BorderSize = 0;
      this.btn_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
      this.btn_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_1.Location = new System.Drawing.Point(186, 160);
      this.btn_1.Name = "btn_1";
      this.btn_1.Size = new System.Drawing.Size(61, 50);
      this.btn_1.TabIndex = 19;
      this.btn_1.UseVisualStyleBackColor = true;
      this.btn_1.Click += new System.EventHandler(this.btn_1_Click);
      // 
      // lbl_login
      // 
      this.lbl_login.Image = global::LCD_Display.Properties.Resources.EnterPin;
      this.lbl_login.Location = new System.Drawing.Point(119, 29);
      this.lbl_login.Name = "lbl_login";
      this.lbl_login.Size = new System.Drawing.Size(329, 73);
      this.lbl_login.TabIndex = 26;
      // 
      // uc_login
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Name = "uc_login";
      this.Load += new System.EventHandler(this.uc_login_Load);
      this.pnl_container.ResumeLayout(false);
      this.pnl_container.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TextBox txt_pin;
    private LCD_Display.Controls.uc_button btn_7;
    private LCD_Display.Controls.uc_button btn_2;
    private LCD_Display.Controls.uc_button btn_9;
    private LCD_Display.Controls.uc_button btn_4;
    private LCD_Display.Controls.uc_button btn_enter;
    private LCD_Display.Controls.uc_button btn_5;
    private LCD_Display.Controls.uc_button btn_0;
    private LCD_Display.Controls.uc_button btn_8;
    private LCD_Display.Controls.uc_button btn_6;
    private LCD_Display.Controls.uc_button btn_3;
    private LCD_Display.Controls.uc_button btn_1;
    private System.Windows.Forms.Label lbl_login;


  }
}
