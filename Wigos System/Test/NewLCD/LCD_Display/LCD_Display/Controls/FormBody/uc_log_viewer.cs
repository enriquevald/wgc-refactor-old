//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_log_viewer.cs
// 
//   DESCRIPTION: Log viewer Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_log_viewer : uc_body
  {
    #region Constants

    private const Boolean LOGGER_ADD_COLOR_STATUS_ACTIVE  = true;
    private const Int32 LOGGER_IDX_CODE                   = 64;
    private const Int32 LOGGER_LEN_CODE                   = 5;
    private const Int32 LOGGER_IDX_SPECIAL_CODE           = 70;
    private const Int32 LOGGER_LEN_SPECIAL_CODE           = 2;
    private const String LOGGER_LEN_SPECIAL_CODE_VALUE_00 = "->";
    private const String LOGGER_LEN_SPECIAL_CODE_VALUE_01 = "**";

    // Colors
    private const String COLOR_LOGGER_ERROR       = "red";
    private const String COLOR_LOGGER_UNEXPECTED  = "orange";
    private const String COLOR_LOGGER_IMPORTANT   = "green";

    // Lines Split 
    private const Int16 DATA_ENTRY_INI    = 0;
    private const Int16 DATA_ENTRY_LEN    = 22;
    private const Int16 MODULE_ENTRY_INI  = DATA_ENTRY_LEN;
    private const Int16 MODULE_ENTRY_LEN  = 18;
    private const Int16 METHOD_ENTRY_INI  = MODULE_ENTRY_INI + MODULE_ENTRY_LEN;
    private const Int16 METHOD_ENTRY_LEN  = 26;
    private const Int16 DESC_ENTRY_INI    = METHOD_ENTRY_INI + METHOD_ENTRY_LEN;

    #endregion
    
    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public uc_log_viewer()
    {
      InitializeComponent();

      InitializeControls();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get log entry from log file
    //
    //  PARAMS:
    //      - INPUT:
    //           - String: Line
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - String
    //   NOTES:
    //
    public String GetLogEntry(String Line)
    {
      String _color;
      String _formated_line;
      String[] _entry;
      Int16 _idx;

      try
      {
        _formated_line = String.Empty;

        _idx = 0;
        _entry = new String[4];
        _color = SetColorStatus(Line);

        _entry[0] = Line.Substring(DATA_ENTRY_INI, DATA_ENTRY_LEN);
        _entry[1] = Line.Substring(MODULE_ENTRY_INI, MODULE_ENTRY_LEN);
        _entry[2] = Line.Substring(METHOD_ENTRY_INI, METHOD_ENTRY_LEN);
        _entry[3] = Line.Substring(DESC_ENTRY_INI, (Line.Length - 1) - DESC_ENTRY_INI);

        _formated_line = "<p>" + Environment.NewLine;

        foreach (String _tag in _entry)
        {
          _formated_line += "<label" + _color + ">" + _entry[_idx] + "</label><br>";
          _idx++;
        }

        _formated_line += "</p>";
        _formated_line += "<hr />";
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while getting the log entry : " + _ex.Message);
        _formated_line = String.Empty;
      }

      return _formated_line;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get all log viewer
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public Boolean GetLogViewer(out String LogHtml)
    {
      Char _sep;
      String _zip_logger;
      String _str_logger;
      String[] _log;

      _sep = (char)10;
      LogHtml = String.Empty;

      try
      {
        _zip_logger = "7b0HYBxJliUmL23Ke39K9UrX4HShCIBgEyTYkEAQ7MGIzeaS7B1pRyMpqyqBymVWZV1mFkDM7Z28995777333nvvvfe6O51OJ/ff/z9cZmQBbPbOStrJniGAqsgfP358Hz8i7u3c3Xl4d29n9166u/to//6j3Qdplj49fnP85Pj16e9//PIs5edp1maTrMl//5dldv06u8xT8+zQs5t+9/jVi0fplz95+uq7r87enKYnjz69t/dp+uzR7u798c5O+vLR7sFO+t1HO2nS63Jv95vt8t7uPelybwdd7qF5rNv733S3ezrSncGRHnzTXe6M97nLT4e6vLf3DXe5czD+dPN83vv0m+7ywfgAXQ72SDT4hnv8dLy3ma77977pLvdlkLv7g11+w6J5b+eedjnIsPe/adGkLpmwg3S9/01L5c6e8Otwj9+0UO7sqlDuxbv89NHONy6Uu0LW4R6/YZnce/hwM7t+Slz1TXdpNM/gKHe/YaHce/hA9cDwKL9hodx7eN9M5VCX37S93HuoqmdwkN+0qdx7qBZ6yIh8+o2byr2Hu75TEOvymzaVew93ZCoHB/lNW0r0qIMc6PGbtpR7B6oHhnv8pmWSetxkQj79xu3k3oH6H8Na4Ju2k+hyf6NIftN2Ej3ubO7xm5bIg/uqzwe6fPCN28m9g33VOwPM8+CbN5QHe8o8AxLy4Js3lAfqDQyP8hs3lA9UKIe064Nv3lA+ONC5HCTsN24oH3y6Odx68M1bygf70uUAYe8fCMe+fPXlmy9Pvnz++3/35KV0+7KupnnT/P6v8/oyr1/lv2idN63p8t5u+pNZWdVpscybVV5nsypdZXWWTqvFIlvOHqW7n45/aF3d/6F11elp79HOwaP7D9PVN99Tl34/m13t/vC6+uERMOxpZ5ck4BHJwCr9/Kuz3//Zqy9fvDl98VRk6Nnp73+S1bMvqmXRVvXv/yK/Or3Ml630RNK1fZTyB6/yaV5c5gQ8TV+fvn599uWL3//N2RenX371JpCrHZhX2PSfhWF1JutntasOCaWrg5+VrkJu/1ntKkrAn52ufnij+uFNFXXww+1r797d3R1ftXNE8Pqrl6evfvLs9ZevUn1erwnGZdGQBJ8QgGr5RVYspa/9+/cO0m086av8omjaukpnedqsCbmqof6LaYHeuclAh1AbT89enZ68+b21Q/6kqPNp+3ufkeYweMjgSG28ntZ5voSx33n3EOa3WM6qq0f37n36bvfg0/Qni9z9kaaRbvdvGme02+e/15t092B88GB3CGbEv5h0QHVgPn2SLqr1si2WF+l4HJuTrweX+KLIyvTFejHJ60fpF8cn6cOdJ7vbuycPn24/uffp8fsM4VWezYa7oiGUVTZ7/xHcCmw+G78Pqrejdgzow0e79y03vn55/Obk26fMGsKNzSprp/O8Pl6tXs/XLfHbktn/gbL/aZm+LapmWqVNnpIs/qN/cTrN65qkrYqRJehtgPdfT/Nl/rLOG2cy99N6+tnTe09PX736/c9e/OTx87OnJ8fPn6dbO+8ODh58unPw6cmdtCyzBfebpWcC6t7Tp/llMc0PHj1SeCFCe4/gMNwnTfNDkn7X4Q9V+qXbT28a53tJv4P59fkxJjsfAvc9pX9zV7cS0/cfwa3A9gT1Q6gyKP0EdP/R/X3LjT+70k+93Uec/8MUNurQDe+HJ2z3oeW+YWFTmF9/+gdY9WvDfX9h29CVlYpoV5uF7UPBxuTia1Nlk7CRY7v3wxG2nf1H9x5g/eCHJWza4T07vB+OsGm3+zeN832EzYP59ac/wqofBPf9hO2Grm4lFe8/gluB7crFB1FlSNgA9KGEkD8cYdvffbRzIxN+k8JmOvwhCxu6/UYtmwfz609/nFW/Ptz3FrZNXd1KKt5/BLcCG5GLr0+VDcJ2f0cyhj8cYUNvD29iwm9S2EyHP2Rhu09TtXPTON9T2AzMrz/9cVb9+nDfW9g2dXUrqXj/EdwKbEwuvjZVNgmbAn356oyWHF45mJQcLeiN+vd/Ttg8LcsQ6N4ucii7tIjn5UnQ8nkxqbP6Ot0irvn9DdAvvnx6+vz3f0jp0qfPn98ZotV7SSA9nMbZ8xF4vbqUFE0DevT6ue8nS37W9coPNTz1Ovxh65VvOjz1YH59Th+Qyq8N9/31yofGke8/gluBjamAr02VTXpFgf5c6pXbcOY3oVc+9QXvZ12vHNzCaf5G9cqBpAp/2HqFun3wTesVhfn1OX1AKr823PfXKxu6upUCeP8R3ApsTAV8baps0isK9OdSr9yGMz9Yr5CzQoGCqNBvfN1654fW14NeT1h8+NnoKTaqe5Ij/aH0tf9oz+Rjn736kvjoxVPhiu8WFxX11GZ1e5LVs9fUb1EttS/himePHtxPjx/tpF8+ChiE0GyraVX+/t+drn7/41XR7RTLOD+svFS3t5/r9VYgdA9r2O8piV/fFkqHP+z1Vu32G11v9WB+fe0csyQfAvc9beHmrm5ltN5/BLcC2zNbH0KVQVvogP6c2cJbcuY3YAtpCe2eyQmG2vXZ6e//Omu+XTXt7/9Ftcyv35D2as5JiSn9TuuaNHmdy2y3dTZ9O8vabNwXMlo4MNrtZ6mLvb1Hez/7Xezv/Ox2QUnfXRPv/Gx1QYT62R7Fpy6s+dnq4uDR7s/2KB4+4tzZz2IXlJ7b/VkWvfv3Hu2ZlPg32oXXhYbNP2tdUBJg59Heg5/lLu79LKtB6mL/Z1kNfkqiLUbjZ7ELLNiaufhZcYzvu952yXETEXkvK/iefmivQxNs/Oz6ob1ub8xp3+yHRmF+fc/IenHfENzb+KG37upWDuP7j+BWYMVlvDXM21F7ACjCsB+uHxpF4b0kcOdWfmi3H+vB/VD0itfbz03A3UPoZ3uxvNvhPePf/HAV3b0bFfr7KzqG+fVFb1BNfE24X0fRDXZ1K430/iO4Fdi4TvqaVNms6Bjoz62iu5kzvxlFd+9ndVFrQ2//b1B0Dx/t/nAVnXb4w1Z0DyXQ/kYVncL8+qI3oCa+Ntz3V3QburqVRnr/EdwKbEwnfW2qbFJ0CvTnUtHdhjO/CUVHkaITvJ91RRf09v8CRbf/Q1hCiXb4Q1Z0+7dJVL+nojMwv77oxdXE14f73opuU1e30kjvP4JbgY3opK9PlQ2KzgD9OVR0t+LMb0DRUTb6vskd/uwrOiSm33tUH6RXqEOX8fvh6ZWHsib+jeoVhfn1OX1AKr823PfXKxu6upUCeP8R3ApsTAV8baps0isK9OdSr9yGM78JvfJwKKX4hrTEF9l0XizzL3Iac/NdckCeUM5e6fetb6X6dSrfk9Q3eYvPyVu5LKp18yh9yWx394Repb92d/Ypz797/+DTnXSUfrda+l/u7dFXu3t7O6P0O9SLfvp+6AZ43nMzo+jy199ZL1bplqJ+Bxi8LLPrfCbopdtHKWFAnxJ6QIo/+FnHQlA4ZoYkRIREggrh4T4HfX5WMQIipDCapqiWP8fECVH5UArdf/CzsTLmdbH3aEcXkn4Wu9ildW/jf/8sdXFv/9H+N78oHXZBc/HNL+cGXezTovHP8ijIJdv/5leMwy7ICvxsj+Lhz8ZybtDF/fuP9t165+1916/ju0pvP0TfVTr8ofuu0u2NHsJ7+a4O5tf3pmKe34fAfU/fdXNXt3Iy338EtwLbczM/hCqDvqsD+nPmu96SMz/Yd+V+OCb+ofiuB7sHdw8OHh7EPNd7dw92SX4DxzX0P25E9vZuUcxzJeS6vtm9vhP0s4FE6JYJgXpeGVPnZxWfQbf1h06ZTV7rzeT5fwA=";

        // Expand Logger
        InflateString(_zip_logger, out _str_logger);

        if (_str_logger.Length > 0)
        {
          _log = _str_logger.Split(_sep);

          foreach (String _line in _log)
          {
            if (_line.Length < LOGGER_IDX_SPECIAL_CODE + LOGGER_LEN_SPECIAL_CODE)
            {
              continue;
            }

            LogHtml += GetLogEntry(_line);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set color status
    //
    //  PARAMS:
    //      - INPUT:
    //           - String: Line
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - String
    //   NOTES:
    //
    private String SetColorStatus(String Line)
    {
      Int32 _code;
      String _special_code;
      String _color;

      if (!Int32.TryParse(Line.Substring(DESC_ENTRY_INI, LOGGER_LEN_CODE), out _code))
      {
        _code = 0;
      }

      _color = "";

      switch (_code)
      {
        case 1:
          _special_code = Line.Substring(LOGGER_IDX_SPECIAL_CODE, LOGGER_LEN_SPECIAL_CODE);

          switch (_special_code)
          {
            case LOGGER_LEN_SPECIAL_CODE_VALUE_00:
            case LOGGER_LEN_SPECIAL_CODE_VALUE_01:
              _color = COLOR_LOGGER_IMPORTANT;
              break;
          }
          break;

        case 4:
        case 21:
          _color = COLOR_LOGGER_ERROR;
          break;

        case 31:
        case 32:
          _color = COLOR_LOGGER_UNEXPECTED;
          break;
      }


      if (_color != String.Empty)
      {
        _color = " style=\"color:" + _color + " \" ";
      }

      return _color;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Uncompress log file text
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public static void InflateString(String StringToInflate, out String InflatedString)
    {
      MemoryStream _mem_stream;
      DeflateStream _compress;
      Byte[] _logger;
      int _len;

      try
      {
        _logger = Convert.FromBase64String(StringToInflate);
        _mem_stream = new MemoryStream(_logger);

        // Decompressed
        _logger = new Byte[(64 * 1024)];
        _compress = new DeflateStream(_mem_stream, CompressionMode.Decompress, true);
        _len = _compress.Read(_logger, 0, _logger.Length);
        _compress.Close();
        _compress = null;

        InflatedString = Encoding.UTF8.GetString(_logger, 0, _len);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while getting the log entry : " + _ex.Message);
        InflatedString = String.Empty;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Show Log Viewer
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Boolean
    //   NOTES:
    //
    public Boolean ShowLogViewer()
    {
      String _log_path;
      String _log_temp_path;
      String _log_err_path;
      String _curDir;
      String _contenido;
      String _line;
      Boolean _reading;

      try
      {
        _curDir = Directory.GetCurrentDirectory();
        _log_path = String.Format(@"{0}\LogViewer\LogViewer.html", _curDir);
        _log_temp_path = String.Format(@"{0}\LogViewer\LogViewer_tmp.html", _curDir);
        _log_err_path = String.Format(@"{0}\Database\ERRORS.LOG", _curDir);

        GetLogViewer(out _contenido);

        using (StreamWriter _sw = new StreamWriter(_log_path))
        {
          using (StreamReader _sr = new StreamReader(_log_temp_path))
          {
            _reading = true;

            while (_reading)
            {
              _line = _sr.ReadLine();

              if (_line != null)
              {
                _line = _line.Replace("@LOG_LIST@", _contenido);
                _sw.WriteLine(_line);
              }
              else
              {
                _reading = false;
              }
            }
          }
        }

        this.wb_log_viewer.Url = new Uri("file:///" + _log_path);

        return true;
      }
      catch (Exception _ex)
      {
        Console.WriteLine(_ex.Message);
      }

      return false;
    }

    #endregion
  
    #region Private Functios

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize controls
    //
    //  PARAMS:
    //      - INPUT:
    //           - String: Line
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - String
    //   NOTES:
    //
    public void InitializeControls()
    {

    }

    private void wb_log_viewer_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      this.wb_log_viewer.Visible = true;
    }

    #endregion

    #region Events

    private void uc_log_viewer_Load(object sender, EventArgs e)
    {
      this.MainForm.TimerShowJackpot.Stop();
      ShowLogViewer();
    }

    #endregion
  }
}
