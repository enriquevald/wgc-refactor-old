//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_raffles.cs
// 
//   DESCRIPTION: Raffles Control. 
// 
//        AUTHOR: Carlos Cáceres.
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 CCG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_raffles : uc_body
  {
    #region Private Variables

    private const Int16 NUM_IMAGES = 3;

    #endregion
    
    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public uc_raffles()
    {
      InitializeComponent();
    }

    #endregion
    
    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Set raffle item list
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SetItem()
    {
      LCD_ITEM[] _item_list = new LCD_ITEM[NUM_IMAGES];
      LCD_ITEM _item;
      Int16 _idx;

      try
      {
        for (_idx = 0; _idx < NUM_IMAGES; _idx++)
        {
          _item = new LCD_ITEM();

          _item.Image = GetItemImage(_idx);
          _item.Name = "Draw " + (_idx + 1).ToString();
          _item.Amount = 500; //HardCode
          _item.Type = TYPE_ITEM.DRAW;

          _item_list[_idx] = _item;
          this.uc_list_item.BindSingleItem(_item);
        }

        this.uc_list_item.ItemList = _item_list;
        //this.uc_list_item.Bind();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred setting the item information : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get raffle item image 
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private Image GetItemImage(Int16 Index)
    {
      Image _image;

      try
      {
        switch (Index)
        {
          case 0: _image = Resources.DrawLayout_1; break;
          case 1: _image = Resources.DrawLayout_2; break;
          case 2: _image = Resources.DrawLayout_3; break;

          default:
            _image = Resources.DrawLayout_1;
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while getting the item image : " + _ex.Message);
        _image = null;
      }

      return _image;
    }

    #endregion
    
    #region Events

    private void uc_raffles_Load(object sender, EventArgs e)
    {
      SetItem();
    }

    #endregion
  }
}
