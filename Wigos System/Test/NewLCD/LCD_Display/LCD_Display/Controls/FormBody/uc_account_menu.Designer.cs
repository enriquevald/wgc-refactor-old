namespace LCD_Display.Controls
{
  partial class uc_account_menu
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_account_info = new LCD_Display.Controls.uc_button();
      this.btn_raffles = new LCD_Display.Controls.uc_button();
      this.btn_jackpot = new LCD_Display.Controls.uc_button();
      this.btn_promotions = new LCD_Display.Controls.uc_button();
      this.btn_gift = new LCD_Display.Controls.uc_button();
      this.pnl_account_menu = new System.Windows.Forms.Panel();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.btn_gift);
      this.pnl_container.Controls.Add(this.btn_jackpot);
      this.pnl_container.Controls.Add(this.btn_promotions);
      this.pnl_container.Controls.Add(this.btn_raffles);
      this.pnl_container.Controls.Add(this.btn_account_info);
      this.pnl_container.Controls.Add(this.pnl_account_menu);
      // 
      // btn_account_info
      // 
      this.btn_account_info.BackColor = System.Drawing.Color.White;
      this.btn_account_info.BackgroundImage = global::LCD_Display.Properties.Resources.AccountInfoButton;
      this.btn_account_info.FlatAppearance.BorderSize = 0;
      this.btn_account_info.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_account_info.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_account_info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_account_info.Location = new System.Drawing.Point(21, 58);
      this.btn_account_info.Name = "btn_account_info";
      this.btn_account_info.Size = new System.Drawing.Size(273, 58);
      this.btn_account_info.TabIndex = 1;
      this.btn_account_info.UseVisualStyleBackColor = true;
      this.btn_account_info.Click += new System.EventHandler(this.btn_account_info_Click);
      // 
      // btn_raffles
      // 
      this.btn_raffles.BackColor = System.Drawing.Color.White;
      this.btn_raffles.BackgroundImage = global::LCD_Display.Properties.Resources.RafflesButton;
      this.btn_raffles.FlatAppearance.BorderSize = 0;
      this.btn_raffles.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_raffles.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_raffles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_raffles.Location = new System.Drawing.Point(306, 58);
      this.btn_raffles.Name = "btn_raffles";
      this.btn_raffles.Size = new System.Drawing.Size(273, 58);
      this.btn_raffles.TabIndex = 2;
      this.btn_raffles.UseVisualStyleBackColor = true;
      this.btn_raffles.Click += new System.EventHandler(this.btn_raffles_Click);
      // 
      // btn_jackpot
      // 
      this.btn_jackpot.BackColor = System.Drawing.Color.White;
      this.btn_jackpot.BackgroundImage = global::LCD_Display.Properties.Resources.CasinoJackpotButton;
      this.btn_jackpot.FlatAppearance.BorderSize = 0;
      this.btn_jackpot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_jackpot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_jackpot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_jackpot.Location = new System.Drawing.Point(306, 124);
      this.btn_jackpot.Name = "btn_jackpot";
      this.btn_jackpot.Size = new System.Drawing.Size(273, 58);
      this.btn_jackpot.TabIndex = 4;
      this.btn_jackpot.UseVisualStyleBackColor = true;
      this.btn_jackpot.Click += new System.EventHandler(this.btn_jackpot_Click);
      // 
      // btn_promotions
      // 
      this.btn_promotions.BackColor = System.Drawing.Color.White;
      this.btn_promotions.BackgroundImage = global::LCD_Display.Properties.Resources.PromotionsButton;
      this.btn_promotions.FlatAppearance.BorderSize = 0;
      this.btn_promotions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_promotions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_promotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_promotions.Location = new System.Drawing.Point(21, 124);
      this.btn_promotions.Name = "btn_promotions";
      this.btn_promotions.Size = new System.Drawing.Size(273, 58);
      this.btn_promotions.TabIndex = 3;
      this.btn_promotions.UseVisualStyleBackColor = true;
      this.btn_promotions.Click += new System.EventHandler(this.btn_promotions_Click);
      // 
      // btn_gift
      // 
      this.btn_gift.BackColor = System.Drawing.Color.White;
      this.btn_gift.BackgroundImage = global::LCD_Display.Properties.Resources.LoyaltyClubButton;
      this.btn_gift.FlatAppearance.BorderSize = 0;
      this.btn_gift.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_gift.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_gift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_gift.Location = new System.Drawing.Point(21, 190);
      this.btn_gift.Name = "btn_gift";
      this.btn_gift.Size = new System.Drawing.Size(273, 58);
      this.btn_gift.TabIndex = 5;
      this.btn_gift.UseVisualStyleBackColor = true;
      this.btn_gift.Click += new System.EventHandler(this.btn_gift_Click);
      // 
      // pnl_account_menu
      // 
      this.pnl_account_menu.Location = new System.Drawing.Point(15, 13);
      this.pnl_account_menu.Name = "pnl_account_menu";
      this.pnl_account_menu.Size = new System.Drawing.Size(565, 274);
      this.pnl_account_menu.TabIndex = 6;
      // 
      // uc_account
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_account";
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private LCD_Display.Controls.uc_button btn_account_info;
    private LCD_Display.Controls.uc_button btn_raffles;
    private LCD_Display.Controls.uc_button btn_gift;
    private LCD_Display.Controls.uc_button btn_jackpot;
    private LCD_Display.Controls.uc_button btn_promotions;
    private System.Windows.Forms.Panel pnl_account_menu;
  }
}
