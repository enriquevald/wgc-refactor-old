//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_gift_menu.cs
// 
//   DESCRIPTION: Gift's List Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_gift_list : uc_body
  {
    #region Private Variables

    private const Int16 NUM_IMAGES = 3;

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public uc_gift_list()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Set gift image list
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SetImageList()
    {
      LCD_ITEM[] _item_list;
      LCD_ITEM _item;
      Int16 _idx;

      try
      {
        _item_list = new LCD_ITEM[NUM_IMAGES];

        for (_idx = 0; _idx < NUM_IMAGES; _idx++)
        {
          _item = new LCD_ITEM();

          _item.Image = GetItemImage(_idx);
          _item.Name = "Gift " + (_idx + 1).ToString();
          _item.Description = "Description item " + (_idx + 1).ToString() + ", Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
          _item.Type = TYPE_ITEM.GIFT;

          _item_list[_idx] = _item;
          this.uc_list_item.BindSingleItem(_item);
        }      

        this.uc_list_item.ItemList = _item_list;
        //this.uc_list_item.Bind();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while setting the image list : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get item image
    //
    //  PARAMS:
    //      - INPUT:
    //           - Index
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Image
    //   NOTES:
    //
    private Image GetItemImage(Int16 Index)
    {
      Image _image;

      try
      {
        switch (Index)
        {
          case 0: _image = (this.MainForm.Gift == TYPE_GIFT.AVAILABLE) ? Resources.GifLayout_2 : Resources.GifLayout_3; break;
          case 1: _image = (this.MainForm.Gift == TYPE_GIFT.AVAILABLE) ? Resources.GifLayout_1 : Resources.GifLayout_4; break;
          case 2: _image = (this.MainForm.Gift == TYPE_GIFT.AVAILABLE) ? Resources.GifLayout_4 : Resources.GifLayout_2; break;

          default:
            _image = Resources.GifLayout_1;
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while getting the gift image : " + _ex.Message);
        _image = null;
      }

      return _image;
    }

    #endregion

    #region Events

    private void uc_gift_list_Load(object sender, EventArgs e)
    {
      SetImageList();
    }

    #endregion
  }
}
