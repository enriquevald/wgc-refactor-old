namespace LCD_Display.Controls
{
  partial class uc_operator
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_log = new LCD_Display.Controls.uc_button();
      this.btn_status = new LCD_Display.Controls.uc_button();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.btn_log);
      this.pnl_container.Controls.Add(this.btn_status);
      // 
      // btn_log
      // 
      this.btn_log.BackColor = System.Drawing.Color.Transparent;
      this.btn_log.BackgroundImage = global::LCD_Display.Properties.Resources.LogButton;
      this.btn_log.FlatAppearance.BorderSize = 0;
      this.btn_log.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_log.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_log.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_log.Location = new System.Drawing.Point(126, 159);
      this.btn_log.Name = "btn_log";
      this.btn_log.Size = new System.Drawing.Size(342, 73);
      this.btn_log.TabIndex = 1;
      this.btn_log.UseVisualStyleBackColor = true;
      this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
      // 
      // btn_status
      // 
      this.btn_status.BackColor = System.Drawing.Color.Transparent;
      this.btn_status.BackgroundImage = global::LCD_Display.Properties.Resources.StatusButton;
      this.btn_status.FlatAppearance.BorderSize = 0;
      this.btn_status.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_status.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_status.Location = new System.Drawing.Point(126, 68);
      this.btn_status.Name = "btn_status";
      this.btn_status.Size = new System.Drawing.Size(342, 73);
      this.btn_status.TabIndex = 0;
      this.btn_status.UseVisualStyleBackColor = true;
      this.btn_status.Click += new System.EventHandler(this.btn_status_Click);
      // 
      // uc_operator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_operator";
      this.Load += new System.EventHandler(this.uc_operator_Load);
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_button btn_status;
    private uc_button btn_log;
  }
}
