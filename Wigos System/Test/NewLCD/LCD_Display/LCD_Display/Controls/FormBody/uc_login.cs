//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_login.cs
// 
//   DESCRIPTION: Login Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_login : uc_body
  {
    #region Public Functions

    public uc_login()
    {
      InitializeComponent();
    }

    #endregion

    #region Private Functions

    private void InitializeControls()
    {
      this.txt_pin.Text = String.Empty;
    }

    private void AddNumber(Int16 Number)
    {
      if (this.txt_pin.Text.Length < 4)
      {
        this.txt_pin.Text += Number.ToString();
      }
    }

    private void GetAccountInfo()
    {
      ACCOUNT _account;

      _account.Name = "Mr. John Smith";
      _account.Address = "933E Sahara Ave, Las Vegas, NV 89109";
      _account.Telephone = "555 67 78 43";
      _account.Email = "John@hotmail.com";
      _account.RedeemPoints = 1500;
      _account.RedeemPromoPoints = 2345;
      _account.NonRedeemPoints = 236;
      _account.ActualLevel = "Bronze";
      _account.NextLevel = "Silver";

      this.MainForm.Account = _account;
      this.MainForm.CallAttendant = true;
    }

    public Boolean CheckAccountPin()
    {
      try
      {
        if (this.txt_pin.Text == "1111")
        {
          GetAccountInfo();
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while cheking the account PIN : " + _ex.Message);
        return false;
      }

      return false;
    }

    #endregion

    #region Events

    #region uc_login_Load
    private void uc_login_Load(object sender, EventArgs e)
    {
      InitializeControls();
    }
    #endregion

    #region btn_0_Click
    private void btn_0_Click(object sender, EventArgs e)
    {
      AddNumber(0);
    }
    #endregion

    #region btn_1_Click
    private void btn_1_Click(object sender, EventArgs e)
    {
      AddNumber(1);
    }
    #endregion

    #region btn_2_Click
    private void btn_2_Click(object sender, EventArgs e)
    {
      AddNumber(2);
    }
    #endregion

    #region btn_3_Click
    private void btn_3_Click(object sender, EventArgs e)
    {
      AddNumber(3);
    }
    #endregion

    #region btn_4_Click
    private void btn_4_Click(object sender, EventArgs e)
    {
      AddNumber(4);
    }
    #endregion

    #region btn_5_Click
    private void btn_5_Click(object sender, EventArgs e)
    {
      AddNumber(5);
    }
    #endregion

    #region btn_6_Click
    private void btn_6_Click(object sender, EventArgs e)
    {
      AddNumber(6);
    }
    #endregion

    #region btn_7_Click
    private void btn_7_Click(object sender, EventArgs e)
    {
      AddNumber(0);
    }
    #endregion

    #region btn_8_Click
    private void btn_8_Click(object sender, EventArgs e)
    {
      AddNumber(8);
    }
    #endregion

    #region btn_9_Click
    private void btn_9_Click(object sender, EventArgs e)
    {
      AddNumber(9);
    }
    #endregion

    #region txt_pin_Enter
    private void txt_pin_Enter(object sender, EventArgs e)
    {
      this.txt_pin.Text = String.Empty;
    }
    #endregion

    #region btn_enter_Click
    private void btn_enter_Click(object sender, EventArgs e)
    {
      if (CheckAccountPin())
      {
        this.MainForm.BindContainer(TYPE_CONTAINER.ACCOUNT_MENU);
      }
    }
    #endregion

    #endregion
  }
}
