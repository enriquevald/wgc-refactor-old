namespace LCD_Display.Controls
{
  partial class uc_item
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_item = new LCD_Display.Controls.uc_button();
      this.lbl_amount = new System.Windows.Forms.Label();
      this.pnl_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_container
      // 
      this.pnl_container.Controls.Add(this.lbl_amount);
      this.pnl_container.Controls.Add(this.btn_item);
      this.pnl_container.Size = new System.Drawing.Size(236, 187);
      // 
      // btn_item
      // 
      this.btn_item.BackColor = System.Drawing.Color.Transparent;
      this.btn_item.BackgroundImage = global::LCD_Display.Properties.Resources.GifLayout_1;
      this.btn_item.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.btn_item.FlatAppearance.BorderSize = 0;
      this.btn_item.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_item.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_item.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_item.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btn_item.Location = new System.Drawing.Point(16, 1);
      this.btn_item.Name = "btn_item";
      this.btn_item.Size = new System.Drawing.Size(205, 183);
      this.btn_item.TabIndex = 5;
      this.btn_item.UseVisualStyleBackColor = false;
      this.btn_item.Click += new System.EventHandler(this.btn_item_Click);
      // 
      // lbl_amount
      // 
      this.lbl_amount.BackColor = System.Drawing.Color.White;
      this.lbl_amount.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_amount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.lbl_amount.Location = new System.Drawing.Point(41, 110);
      this.lbl_amount.Name = "lbl_amount";
      this.lbl_amount.Size = new System.Drawing.Size(178, 46);
      this.lbl_amount.TabIndex = 5;
      this.lbl_amount.Text = "xAmount";
      this.lbl_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lbl_amount.Click += new System.EventHandler(this.btn_item_Click);
      // 
      // uc_item
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "uc_item";
      this.Size = new System.Drawing.Size(236, 187);
      this.pnl_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private uc_button btn_item;
    private System.Windows.Forms.Label lbl_amount;

  }
}
