//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_button.cs
// 
//   DESCRIPTION: Button Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_button : Button
  {
    #region Members

    Image m_backgroud_image;

    #endregion

    #region Public Functions

    public uc_button()
    {
      InitializeComponent();
    }

    #endregion

    #region Events

    public void btn_MouseDown(object sender, MouseEventArgs e)
    {

      this.m_backgroud_image = this.BackgroundImage;

      if (this.BackgroundImage.Size == Resources.GifLayout_1.Size)
      {
        if (((uc_item)this.Parent.Parent).Item.Type != TYPE_ITEM.DRAW)
        {
          this.BackgroundImage = Resources.GifLayout;
        }
      }
      else
      {
        this.BackgroundImage = Resources.back_white;
      }
    }

    public void btn_MouseUp(object sender, MouseEventArgs e)
    {
      if (this.m_backgroud_image != null)
      {
        this.BackgroundImage = this.m_backgroud_image;
      }
      this.m_backgroud_image = null;
    }

    #endregion
  }
}
