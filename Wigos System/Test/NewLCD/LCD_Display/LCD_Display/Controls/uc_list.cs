//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_list.cs
// 
//   DESCRIPTION: List Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_list : uc_base
  {
    #region Members

    LCD_ITEM[] m_item_list;

    #endregion

    #region Public Properties

    public LCD_ITEM[] ItemList
    {
      get { return m_item_list; }
      set { m_item_list = value; }
    }

    #endregion

    #region Public Functions

    public uc_list()
    {
      InitializeComponent();      
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Bind items to list 
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void Bind()
    {
      Int32 _idx;
      uc_item _item;

      try
      {
        for (_idx = 0; _idx < this.ItemList.Length; _idx++)
        {
          _item = new uc_item();

          _item.Item = this.ItemList[_idx];
          _item.BindItem();
          _item.Dock = DockStyle.Right;

          this.pnl_item_list.Controls.Add(_item);
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding the items : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Bind single image to list
    //
    //  PARAMS:
    //      - INPUT:
    //           - LCD_ITEM: Item
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void BindSingleItem(LCD_ITEM Item)
    {
      uc_item _item = new uc_item();

      try
      {
        _item.Item = Item;
        _item.BindItem();
        _item.Dock = DockStyle.Right;

        this.pnl_item_list.Controls.Add(_item);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding the item : " + _ex.Message);
      }
    }

    #endregion

    #region Events

    private void pnl_item_list_DragOver(object sender, DragEventArgs e)
    {
      Point _location = new Point();

      _location = this.pnl_item_list.Location;
      _location.X -= 10;
      this.pnl_item_list.Location = _location;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: List Window WndProc
    //
    //  PARAMS:
    //      - INPUT:
    //           - Message
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void WndProcList(ref Message m)
    {
      try
      {
        switch (m.Msg)
        {
          //case Constants.WM_XBUTTONDOWN:

          //  break;
          case Constants.WM_XBUTTONDOWN:

            switch (m.WParam.ToInt32())
            {
              case Constants.WM_XBUTTONDOWN:
                break;

              case Constants.WM_MOUSE_LEFT_CLICK:

                break;

              case Constants.WM_MOUSE_RIGHT_CLICK:

                break;

              case Constants.WM_MOUSE_MID_CLICK:

                break;
            }
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error in WndProc: " + _ex.Message);
      }

    }

    #endregion

    private void uc_list_Load(object sender, EventArgs e)
    {
      this.MainForm.CurrentList = this;
    }
  }
}
