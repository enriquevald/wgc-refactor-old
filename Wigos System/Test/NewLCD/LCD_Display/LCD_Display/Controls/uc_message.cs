//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_message.cs
// 
//   DESCRIPTION: Message Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using LCD_Display.Properties;

namespace LCD_Display.Controls
{
  public partial class uc_message : UserControl
  {
    #region Constants

    private const Int32 MAX_LENGTH_FONT_SIZE_30   = 62;
    private const Int32 MAX_LENGTH_FONT_SIZE_18   = 220;
    private const Int32 MAX_LENGTH_FONT_SIZE_12   = 500;
    private const Int32 MAX_LENGTH_MESSAGE        = 999;


    #endregion

    #region Variables

    private Timer m_auto_close_message;

    #endregion

    #region Properties

    public Timer AutoCloseMessage
    {
      get { return this.m_auto_close_message; }
      set { this.m_auto_close_message = value; }
    }

    #endregion

    #region Public Functions

    public uc_message()
    {
      InitializeComponent();
      
      InitializeControls();
    }

    public void BindMessage(TYPE_MESSAGE Type, String Message, Int32 TimeShowed)
    {
      try
      {
        this.SetStyle(Type);
        this.SetMessage(Message);
        this.StartTimer(TimeShowed);
        
        this.Visible = true;
        this.BringToFront();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding the message : " + _ex.Message);
      }
    }

    #endregion  

    #region Private Functions

    private void InitializeControls()
    {
      
    }

    private void SetMessage(String Message)
    {      
      if (Message.Length > MAX_LENGTH_MESSAGE)
      {
        Message = Message.Substring(0, MAX_LENGTH_MESSAGE);
      }

      this.lbl_message.Text = Message;

      if (Message.Length < MAX_LENGTH_FONT_SIZE_30)
      {
        this.lbl_message.Font = new Font(this.lbl_message.Font.FontFamily, (float)30);
      }
      else if (Message.Length < MAX_LENGTH_FONT_SIZE_18)                                
      {
        this.lbl_message.Font = new Font(this.lbl_message.Font.FontFamily, (float)18);
      }
      else if (Message.Length < MAX_LENGTH_FONT_SIZE_12)
      {
        this.lbl_message.Font = new Font(this.lbl_message.Font.FontFamily, (float)12);
      }
      else
      {
        this.lbl_message.Font = new Font(this.lbl_message.Font.FontFamily, (float)9);
      }
    }

    private void StartTimer(Int32 TimeShowed)
    {
      this.AutoCloseMessage          = new Timer();
      this.AutoCloseMessage.Tick    += new EventHandler(AutoCloseMessage_Tick);
      this.AutoCloseMessage.Interval = TimeShowed;
      this.AutoCloseMessage.Start();
    }

    private void SetStyle(TYPE_MESSAGE Type)
    {
      switch (Type)
      {
        case TYPE_MESSAGE.ERROR:
          this.pnl_message.BackColor      = Color.FromArgb(255, 109, 99);
          this.lbl_message.ForeColor      = Color.Black;
          this.pnl_image.BackgroundImage  = Resources.ic_error;
          break;

        case TYPE_MESSAGE.WARNING:
          this.pnl_message.BackColor      = Color.FromArgb(255, 255, 45);
          this.pnl_image.BackgroundImage  = Resources.ic_warning;
          break;

        case TYPE_MESSAGE.INFO:
          this.pnl_message.BackColor      = Color.FromArgb(48, 186, 255);
          this.pnl_image.BackgroundImage  = Resources.ic_information;
          break;
        case TYPE_MESSAGE.SUCCESS:
          this.pnl_message.BackColor      = Color.FromArgb(130, 255, 130);
          this.pnl_image.BackgroundImage  = Resources.ic_success;
          break;
        case TYPE_MESSAGE.WELCOME:
          this.pnl_message.BackColor = Color.FromArgb(48, 186, 255);
          this.pnl_image.BackgroundImage = Resources.ic_information;
          break;
        case TYPE_MESSAGE.FAREWELL:
          this.pnl_message.BackColor = Color.FromArgb(48, 186, 255);
          this.pnl_image.BackgroundImage = Resources.ic_information;
          break;
      }
    }

    private void UnbindMessage()
    {
      this.SendToBack();   
      this.pnl_image.BackgroundImage = null;
      this.lbl_message.Text = String.Empty;
      this.BackColor        = Color.Transparent;
      this.Visible          = false;

      this.AutoCloseMessage.Dispose();
    }

    #endregion

    #region Events

    private void uc_message_Click(object sender, EventArgs e)
    {
      this.UnbindMessage();
    }

    private void AutoCloseMessage_Tick(object sender, EventArgs e)
    {
      this.UnbindMessage();
    }

    #endregion
  }
}
