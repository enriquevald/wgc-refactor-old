//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_base.cs
// 
//   DESCRIPTION: Base Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_base : UserControl
  {
    #region Variables 

    Timer m_base_timer;

    #endregion

    #region Properties

    public Timer BaseTimer
    {
      get { return this.m_base_timer; }
      set { this.m_base_timer = value; }
    }

    public frm_main MainForm
    {
      get { return ((frm_main)this.ParentForm); }
    }

    #endregion
    
    #region Public Functions

    public uc_base()
    {
      InitializeComponent();
    }

    #endregion

    private void uc_base_Load(object sender, EventArgs e)
    {
      this.BaseTimer = new Timer();
      this.BaseTimer.Interval = 15000;
      this.BaseTimer.Tick += new EventHandler(BaseTimer_Tick);
      this.BaseTimer.Start();
    }
        
    #region Private Functions
    
    #endregion
    
    #region Events

    void BaseTimer_Tick(object sender, EventArgs e)
    {
      //Log.Add(TYPE_MESSAGE.INFO, "... running ...");
    }

    #endregion
  }
}
