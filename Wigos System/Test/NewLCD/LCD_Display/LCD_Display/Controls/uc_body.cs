//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_body.cs
// 
//   DESCRIPTION: Body Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_body : uc_base
  {
    #region Properties

    #endregion

    #region Public Functions

    public uc_body()
    {
      InitializeComponent();
    }

    #endregion
    
    #region Private Functions

    #endregion
    
    #region Events

    #endregion
  }
}

