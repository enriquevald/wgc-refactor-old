namespace LCD_Display.Controls
{
  partial class uc_message
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnl_message = new System.Windows.Forms.Panel();
      this.pnl_msg_container = new System.Windows.Forms.Panel();
      this.lbl_message = new System.Windows.Forms.Label();
      this.pnl_image = new System.Windows.Forms.Panel();
      this.pnl_message.SuspendLayout();
      this.pnl_msg_container.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_message
      // 
      this.pnl_message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
      this.pnl_message.Controls.Add(this.pnl_msg_container);
      this.pnl_message.Controls.Add(this.pnl_image);
      this.pnl_message.Location = new System.Drawing.Point(69, 45);
      this.pnl_message.Name = "pnl_message";
      this.pnl_message.Size = new System.Drawing.Size(662, 211);
      this.pnl_message.TabIndex = 4;
      this.pnl_message.Click += new System.EventHandler(this.uc_message_Click);
      // 
      // pnl_msg_container
      // 
      this.pnl_msg_container.Controls.Add(this.lbl_message);
      this.pnl_msg_container.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_msg_container.Location = new System.Drawing.Point(136, 0);
      this.pnl_msg_container.Name = "pnl_msg_container";
      this.pnl_msg_container.Size = new System.Drawing.Size(526, 211);
      this.pnl_msg_container.TabIndex = 1;
      this.pnl_msg_container.Click += new System.EventHandler(this.uc_message_Click);
      // 
      // lbl_message
      // 
      this.lbl_message.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbl_message.ForeColor = System.Drawing.SystemColors.WindowText;
      this.lbl_message.Location = new System.Drawing.Point(16, 21);
      this.lbl_message.Name = "lbl_message";
      this.lbl_message.Size = new System.Drawing.Size(479, 168);
      this.lbl_message.TabIndex = 1;
      this.lbl_message.Text = "xMessage";
      this.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lbl_message.Click += new System.EventHandler(this.uc_message_Click);
      // 
      // pnl_image
      // 
      this.pnl_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.pnl_image.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnl_image.Location = new System.Drawing.Point(0, 0);
      this.pnl_image.Name = "pnl_image";
      this.pnl_image.Size = new System.Drawing.Size(130, 211);
      this.pnl_image.TabIndex = 0;
      this.pnl_image.Click += new System.EventHandler(this.uc_message_Click);
      // 
      // uc_message
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.LightGray;
      this.Controls.Add(this.pnl_message);
      this.Name = "uc_message";
      this.Size = new System.Drawing.Size(800, 300);
      this.Click += new System.EventHandler(this.uc_message_Click);
      this.pnl_message.ResumeLayout(false);
      this.pnl_msg_container.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    public System.Windows.Forms.Panel pnl_message;
    private System.Windows.Forms.Panel pnl_image;
    private System.Windows.Forms.Label lbl_message;
    private System.Windows.Forms.Panel pnl_msg_container;

  }
}
