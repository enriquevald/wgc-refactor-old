namespace LCD_Display.Controls
{
  partial class uc_button
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SuspendLayout();
      // 
      // uc_button
      // 
      this.BackColor = System.Drawing.Color.Transparent;
      this.FlatAppearance.BorderSize = 0;
      this.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.Size = new System.Drawing.Size(79, 30);
      this.UseVisualStyleBackColor = false;
      this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
      this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_MouseUp);
      this.ResumeLayout(false);

    }

    #endregion
  }
}
