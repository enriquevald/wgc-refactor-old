//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: uc_item.cs
// 
//   DESCRIPTION: Item Control. 
// 
//        AUTHOR: Javi Barea
// 
// CREATION DATE: 16-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-MAY-2014 JBP    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace LCD_Display.Controls
{
  public partial class uc_item : uc_control
  {
    #region Members

    LCD_ITEM m_item;

    #endregion
    
    #region Public Properties

    public LCD_ITEM Item
    {
      get { return this.m_item; }
      set { this.m_item = value; }
    }

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public uc_item()
    {
      InitializeComponent();
    }

    #endregion
    
    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE: Bind item properties
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    public void BindItem()
    {
      try
      {
        // Bind item image
        this.btn_item.BackgroundImage = this.Item.Image;

        //Bind item price 
        SetAmountText();
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while binding the item : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set event click funcionallity
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void ButtonClick()
    {
      try
      {
        switch (this.Item.Type)
        {
          case TYPE_ITEM.GIFT:
            LoadGift();
            break;

          case TYPE_ITEM.DRAW:
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred on button click event : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Load selected gift
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void LoadGift()
    {
      try
      {
        this.MainForm.SelectedItem = this.Item;
        this.MainForm.BindContainer(TYPE_CONTAINER.GIFT_DETAIL);
      }
      catch (Exception _ex)
      {
        Log.Add(TYPE_MESSAGE.ERROR, "Error ocurred while loading the gift : " + _ex.Message);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set item amount text
    //
    //  PARAMS:
    //      - INPUT:
    //           - Nothing
    //      - OUTPUT:
    //             
    // RETURNS:
    //      - Void
    //   NOTES:
    //
    private void SetAmountText()
    {
      String _amount_type = String.Empty;

      switch (this.Item.Type)
      {
        case TYPE_ITEM.GIFT:
          this.lbl_amount.Visible = false;
          break;

        case TYPE_ITEM.DRAW:
          this.lbl_amount.Text = this.Item.Amount.ToString() + " Tickets";
          break;
      }
    }

    #endregion

    
    #region Events

    private void btn_item_Click(object sender, EventArgs e)
    {
      ButtonClick();
    }

    #endregion
  }
}
