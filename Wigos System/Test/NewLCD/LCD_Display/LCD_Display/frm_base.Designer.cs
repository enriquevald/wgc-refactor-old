namespace LCD_Display
{
  partial class frm_base
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_base));
      this.pnl_menu = new System.Windows.Forms.Panel();
      this.pnl_logo_top = new System.Windows.Forms.Panel();
      this.pnl_menu_mid = new System.Windows.Forms.Panel();
      this.btn_bot = new LCD_Display.Controls.uc_button();
      this.btn_top = new LCD_Display.Controls.uc_button();
      this.pnl_logo_bot = new System.Windows.Forms.Panel();
      this.pnl_base = new System.Windows.Forms.Panel();
      this.pnl_msg = new LCD_Display.Controls.uc_message();
      this.pnl_menu.SuspendLayout();
      this.pnl_menu_mid.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnl_menu
      // 
      this.pnl_menu.BackColor = System.Drawing.Color.Transparent;
      this.pnl_menu.Controls.Add(this.pnl_logo_top);
      this.pnl_menu.Controls.Add(this.pnl_menu_mid);
      this.pnl_menu.Controls.Add(this.pnl_logo_bot);
      this.pnl_menu.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnl_menu.Location = new System.Drawing.Point(0, 0);
      this.pnl_menu.Name = "pnl_menu";
      this.pnl_menu.Size = new System.Drawing.Size(200, 300);
      this.pnl_menu.TabIndex = 0;
      // 
      // pnl_logo_top
      // 
      this.pnl_logo_top.BackgroundImage = global::LCD_Display.Properties.Resources.LogoCasino;
      this.pnl_logo_top.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.pnl_logo_top.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnl_logo_top.Location = new System.Drawing.Point(0, 0);
      this.pnl_logo_top.Name = "pnl_logo_top";
      this.pnl_logo_top.Size = new System.Drawing.Size(200, 130);
      this.pnl_logo_top.TabIndex = 0;
      // 
      // pnl_menu_mid
      // 
      this.pnl_menu_mid.Controls.Add(this.btn_bot);
      this.pnl_menu_mid.Controls.Add(this.btn_top);
      this.pnl_menu_mid.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_menu_mid.Location = new System.Drawing.Point(0, 130);
      this.pnl_menu_mid.Name = "pnl_menu_mid";
      this.pnl_menu_mid.Size = new System.Drawing.Size(200, 127);
      this.pnl_menu_mid.TabIndex = 0;
      // 
      // btn_bot
      // 
      this.btn_bot.BackColor = System.Drawing.Color.White;
      this.btn_bot.BackgroundImage = global::LCD_Display.Properties.Resources.CallAttendantButtonSmall;
      this.btn_bot.FlatAppearance.BorderSize = 0;
      this.btn_bot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_bot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_bot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_bot.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.btn_bot.Location = new System.Drawing.Point(31, 72);
      this.btn_bot.Name = "btn_bot";
      this.btn_bot.Size = new System.Drawing.Size(140, 47);
      this.btn_bot.TabIndex = 0;
      this.btn_bot.UseVisualStyleBackColor = false;
      // 
      // btn_top
      // 
      this.btn_top.BackColor = System.Drawing.Color.White;
      this.btn_top.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_top.BackgroundImage")));
      this.btn_top.FlatAppearance.BorderSize = 0;
      this.btn_top.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
      this.btn_top.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
      this.btn_top.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btn_top.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.btn_top.Location = new System.Drawing.Point(31, 17);
      this.btn_top.Name = "btn_top";
      this.btn_top.Size = new System.Drawing.Size(140, 47);
      this.btn_top.TabIndex = 0;
      this.btn_top.UseVisualStyleBackColor = false;
      // 
      // pnl_logo_bot
      // 
      this.pnl_logo_bot.BackgroundImage = global::LCD_Display.Properties.Resources.LogoWigos;
      this.pnl_logo_bot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.pnl_logo_bot.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnl_logo_bot.Location = new System.Drawing.Point(0, 257);
      this.pnl_logo_bot.Name = "pnl_logo_bot";
      this.pnl_logo_bot.Size = new System.Drawing.Size(200, 43);
      this.pnl_logo_bot.TabIndex = 0;
      // 
      // pnl_base
      // 
      this.pnl_base.BackColor = System.Drawing.Color.Transparent;
      this.pnl_base.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnl_base.Location = new System.Drawing.Point(205, 0);
      this.pnl_base.Name = "pnl_base";
      this.pnl_base.Size = new System.Drawing.Size(595, 300);
      this.pnl_base.TabIndex = 0;
      // 
      // pnl_msg
      // 
      this.pnl_msg.AutoCloseMessage = null;
      this.pnl_msg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
      this.pnl_msg.Location = new System.Drawing.Point(0, 0);
      this.pnl_msg.Name = "pnl_msg";
      this.pnl_msg.Size = new System.Drawing.Size(800, 300);
      this.pnl_msg.TabIndex = 0;
      this.pnl_msg.Visible = false;
      // 
      // frm_base
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = global::LCD_Display.Properties.Resources.Background;
      this.ClientSize = new System.Drawing.Size(800, 300);
      this.Controls.Add(this.pnl_base);
      this.Controls.Add(this.pnl_menu);
      this.Controls.Add(this.pnl_msg);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "frm_base";
      this.Text = "frm_base";
      this.pnl_menu.ResumeLayout(false);
      this.pnl_menu_mid.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    #endregion

    private System.Windows.Forms.Panel pnl_menu;
    private System.Windows.Forms.Panel pnl_logo_top;
    private System.Windows.Forms.Panel pnl_menu_mid;
    private System.Windows.Forms.Panel pnl_logo_bot;
    public LCD_Display.Controls.uc_button btn_top;
    public LCD_Display.Controls.uc_button btn_bot;
    public System.Windows.Forms.Panel pnl_base;
    internal LCD_Display.Controls.uc_message pnl_msg;
  }
}