using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;

using System.Data.SqlClient;


namespace Test
{
  public partial class Form1:Form
  {
    public Form1 ()
    {
      InitializeComponent ();
    }

    private void button1_Click (object sender, EventArgs e)
    {
      SqlConnection conn;
      String default_xml;
      String server1;
      String server2;
      String client_str;
      int client_id;

      try
      {
        default_xml = "<SiteConfig>" +
                      "<DBPrincipal>192.168.200.1</DBPrincipal>" +
                      "<DBMirror>192.168.200.2</DBMirror>" +
                      "<DBId>0</DBId>" +
                      "</SiteConfig>";

        if ( !ConfigurationFile.Init ("WSI.Configuration", default_xml) )
        {
          Log.Error (" Reading application configuration settings. Application stopped");

          return;
        }

        server1 = ConfigurationFile.GetSetting ("DBPrincipal");
        server2 = ConfigurationFile.GetSetting ("DBMirror");
        client_str = ConfigurationFile.GetSetting("DBId");
        client_id = Convert.ToInt32(client_str);

        WGDB.Init(client_id, server1, server2);

        WGDB.SetPasswords(client_id,
                           txt_old_root.Text,
                           txt_new_root.Text,
                           txt_old_gui.Text,
                           txt_new_gui.Text);

        MessageBox.Show ("Passwords changed.");


        WGDB.Init(client_id, server1, server2);

        MessageBox.Show ("DB Initialized ...");

        WGDB.ConnectAs ("WGROOT");
        conn = WGDB.Connection ();

        MessageBox.Show ("WGROOT connected!");

        WGDB.ConnectAs ("WGGUI");
        conn = WGDB.Connection ();

        MessageBox.Show ("WGGUI connected!");

        MessageBox.Show ("Done!");
      }
      
      catch ( Exception ex )
      {
        MessageBox.Show ("ERROR: " + ex.Message);
      }
    }

    private void Gen_Pass_Click(object sender, EventArgs e)
    {
      String default_xml;
      String server1;
      String server2;
      String client_str;
      int client_id;

      try
      {
        default_xml = "<SiteConfig>" +
                      "<DBPrincipal>vsDev01</DBPrincipal>" +
                      "<DBMirror>vsDev01</DBMirror>" +
                      "<DBId>0</DBId>" +
                      "</SiteConfig>";

        if (!ConfigurationFile.Init("", default_xml))
        {
          Log.Error(" Reading application configuration settings. Application stopped");

          return;
        }

        server1 = ConfigurationFile.GetSetting("DBPrincipal");
        server2 = ConfigurationFile.GetSetting("DBMirror");
        client_str = ConfigurationFile.GetSetting("DBId");
        client_id = Convert.ToInt32(client_str);

        WGDB.Init(client_id, server1, server2);

        WGDB.ConnectAs("WGROOT");

        StringBuilder _script;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          WGDB.GenerateScript(Int32.Parse(id_db.Text.ToString()), root_pass.Text.ToString(), gui_pass.Text.ToString(), out _script, _db_trx.SqlTransaction);
          _db_trx.SqlTransaction.Rollback();
        }

        MessageBox.Show(_script.ToString());

      }

      catch (Exception ex)
      {
        MessageBox.Show("ERROR: " + ex.Message);
      }

    }
  }
}