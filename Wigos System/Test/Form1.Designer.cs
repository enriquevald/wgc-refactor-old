namespace Test
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose (bool disposing)
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose ();
      }
      base.Dispose (disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent ()
    {
      this.button1 = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.txt_old_root = new System.Windows.Forms.TextBox();
      this.txt_new_root = new System.Windows.Forms.TextBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.txt_old_gui = new System.Windows.Forms.TextBox();
      this.txt_new_gui = new System.Windows.Forms.TextBox();
      this.Gen_Pass = new System.Windows.Forms.Button();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.id_db = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.gui_pass = new System.Windows.Forms.TextBox();
      this.root_pass = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(242, 233);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 6;
      this.button1.Text = "Change";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.txt_old_root);
      this.groupBox1.Controls.Add(this.txt_new_root);
      this.groupBox1.Location = new System.Drawing.Point(21, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(296, 100);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "WGROOT";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(28, 61);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(78, 13);
      this.label2.TabIndex = 7;
      this.label2.Text = "New Password";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(28, 23);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(72, 13);
      this.label1.TabIndex = 6;
      this.label1.Text = "Old Password";
      // 
      // txt_old_root
      // 
      this.txt_old_root.Location = new System.Drawing.Point(115, 19);
      this.txt_old_root.Name = "txt_old_root";
      this.txt_old_root.PasswordChar = '*';
      this.txt_old_root.Size = new System.Drawing.Size(161, 20);
      this.txt_old_root.TabIndex = 1;
      // 
      // txt_new_root
      // 
      this.txt_new_root.Location = new System.Drawing.Point(115, 57);
      this.txt_new_root.Name = "txt_new_root";
      this.txt_new_root.PasswordChar = '*';
      this.txt_new_root.Size = new System.Drawing.Size(161, 20);
      this.txt_new_root.TabIndex = 2;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Controls.Add(this.txt_old_gui);
      this.groupBox2.Controls.Add(this.txt_new_gui);
      this.groupBox2.Location = new System.Drawing.Point(21, 127);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(296, 100);
      this.groupBox2.TabIndex = 3;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "WGGUI";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(28, 61);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(78, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "New Password";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(28, 23);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(72, 13);
      this.label4.TabIndex = 6;
      this.label4.Text = "Old Password";
      // 
      // txt_old_gui
      // 
      this.txt_old_gui.Location = new System.Drawing.Point(115, 19);
      this.txt_old_gui.Name = "txt_old_gui";
      this.txt_old_gui.PasswordChar = '*';
      this.txt_old_gui.Size = new System.Drawing.Size(161, 20);
      this.txt_old_gui.TabIndex = 4;
      // 
      // txt_new_gui
      // 
      this.txt_new_gui.Location = new System.Drawing.Point(115, 57);
      this.txt_new_gui.Name = "txt_new_gui";
      this.txt_new_gui.PasswordChar = '*';
      this.txt_new_gui.Size = new System.Drawing.Size(161, 20);
      this.txt_new_gui.TabIndex = 5;
      // 
      // Gen_Pass
      // 
      this.Gen_Pass.Location = new System.Drawing.Point(115, 110);
      this.Gen_Pass.Name = "Gen_Pass";
      this.Gen_Pass.Size = new System.Drawing.Size(161, 20);
      this.Gen_Pass.TabIndex = 7;
      this.Gen_Pass.Text = "Generate Password";
      this.Gen_Pass.UseVisualStyleBackColor = true;
      this.Gen_Pass.Click += new System.EventHandler(this.Gen_Pass_Click);
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.label7);
      this.groupBox3.Controls.Add(this.id_db);
      this.groupBox3.Controls.Add(this.Gen_Pass);
      this.groupBox3.Controls.Add(this.label5);
      this.groupBox3.Controls.Add(this.label6);
      this.groupBox3.Controls.Add(this.root_pass);
      this.groupBox3.Controls.Add(this.gui_pass);
      this.groupBox3.Location = new System.Drawing.Point(21, 270);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(296, 140);
      this.groupBox3.TabIndex = 8;
      this.groupBox3.TabStop = false;
      // 
      // id_db
      // 
      this.id_db.Location = new System.Drawing.Point(115, 80);
      this.id_db.Name = "id_db";
      this.id_db.PasswordChar = '*';
      this.id_db.Size = new System.Drawing.Size(64, 20);
      this.id_db.TabIndex = 4;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(28, 83);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(16, 13);
      this.label5.TabIndex = 6;
      this.label5.Text = "Id";
      // 
      // gui_pass
      // 
      this.gui_pass.Location = new System.Drawing.Point(115, 50);
      this.gui_pass.Name = "gui_pass";
      this.gui_pass.PasswordChar = '*';
      this.gui_pass.Size = new System.Drawing.Size(161, 20);
      this.gui_pass.TabIndex = 5;
      // 
      // root_pass
      // 
      this.root_pass.Location = new System.Drawing.Point(115, 19);
      this.root_pass.Name = "root_pass";
      this.root_pass.PasswordChar = '*';
      this.root_pass.Size = new System.Drawing.Size(161, 20);
      this.root_pass.TabIndex = 4;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(28, 23);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(79, 13);
      this.label6.TabIndex = 6;
      this.label6.Text = "Root Password";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(28, 53);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(72, 13);
      this.label7.TabIndex = 7;
      this.label7.Text = "Gui Password";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(348, 433);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.button1);
      this.Name = "Form1";
      this.Text = "WSI.WGDB_Passwords";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txt_old_root;
    private System.Windows.Forms.TextBox txt_new_root;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox txt_old_gui;
    private System.Windows.Forms.TextBox txt_new_gui;
    private System.Windows.Forms.Button Gen_Pass;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox id_db;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox root_pass;
    private System.Windows.Forms.TextBox gui_pass;
  }
}

