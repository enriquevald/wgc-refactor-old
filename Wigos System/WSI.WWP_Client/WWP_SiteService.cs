//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: StandardService.cs
// 
//   DESCRIPTION: Procedures for WWP Site to run as a service
// 
//        AUTHOR: Agust� Poch (Header only)
// 
// CREATION DATE: 28-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2009 APB    First release (Header).
// 14-DEC-2011 ACC    The service WWP Site allows connections from iStats application in Standby mode.
// 26-FEB-2013 JCM    Added WWP_MultiSite Thread
// 16-JUL-2013 JCA    Delete PSA Client. The Code is in WSI.PSA_Client Project
// 01-FEB-2016 AMF    PBI 8272: Space Requests
//------------------------------------------------------------------------------

using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Net;
using WSI.Common;
using WSI.WWP_Client;
using System.Diagnostics;
using WSI.WWP_SiteService.MultiSite;

namespace WSI.WWP_SiteService
{
  public partial class WWP_SiteService : CommonService
  {
    private const string VERSION = "18.171";
    private const string SVC_NAME = "WWP_SiteService";
    private const string PROTOCOL_NAME = "WWP SITE";
    private const int SERVICE_PORT = 13002;
    private const Boolean STAND_BY = true;
    private const string LOG_PREFIX = "WWP_SITE";
    private const string CONFIG_FILE = "WSI.WWP_Client_Config";
    private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE;
    private static WWP_SiteService m_this_service = null;

    public static void Shutdown(String Reason)
    {
      m_this_service.RequestOrderlyShutdown(Reason);
    }

    public static void Main(String[] Args)
    {
      m_this_service = new WWP_SiteService();
      m_this_service.Run(Args);
    } // Main

    protected override void SetParameters()
    {
      m_service_name = SVC_NAME;
      m_protocol_name = PROTOCOL_NAME;
      m_version = VERSION;
      m_default_port = SERVICE_PORT;
      m_stand_by = STAND_BY;
      if (Debugger.IsAttached)
      {
        m_stand_by = false;
      }
      m_tsv_packet = TSV_PACKET;
      m_log_prefix = LOG_PREFIX;
      m_old_config_file = CONFIG_FILE;
      
      OperationVoucherParams.LoadData = true;
    } // SetParameters

    protected override void OnServiceBeforeInit()
    {
      //
      // ACC 14-DEC-2011 The service WWP Site allows connections from iStats application in Standby mode.
      //

      IPEndPoint _service_ip_endpoint;

      _service_ip_endpoint = new IPEndPoint(IPAddress.Parse(m_ip_address), m_default_port);

      WWP_Listener.Create(_service_ip_endpoint);

    } // OnServiceBeforeInit

    //------------------------------------------------------------------------------
    // PURPOSE : Starts the service.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    protected override void OnServiceRunning(IPEndPoint ListeningOn)
    {
      Int32 _site_id;
      GetInfo _get_info;

      // Get Site data
      while (WSI.WWP.Site.Id == 0)
      {
        _get_info = new GetInfo();

        Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _site_id);
        WSI.WWP.Site.Id = _site_id;
        WSI.WWP.Site.Name = Misc.ReadGeneralParams("Site", "Name");
        WSI.WWP.Site.ServerMacAddress = _get_info.GetMACAddress();

        if (WSI.WWP.Site.Id == 0)
        {
          Log.Error("SiteId not configured.");

          if (m_ev_stop.WaitOne(60000))
          {
            return;
          }
        }
      }

      // Start WWP Protocol
      WWP_ProcessCenterRequest.Init();
      WWP_Protocol.Init();
      WWP_SiteRequest.Init();

      // MultiSite Request Process
      MS_Request_Process.Init();

      // Multi Site Sync Thread
      WSI.WWP_SiteService.WWP_MultiSiteClient.Init();


    } // OnServiceRunning

    //------------------------------------------------------------------------------
    // PURPOSE : Stops the service.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    protected override void OnServiceStopped()
    {
      WWP_Protocol.Stop();
    }
  } // WWP_SiteService


  [RunInstallerAttribute(true)]
  public class ProjectInstaller : Installer
  {
    private ServiceInstaller serviceInstaller;
    private ServiceProcessInstaller processInstaller;

    public ProjectInstaller()
    {
      processInstaller = new ServiceProcessInstaller();
      serviceInstaller = new ServiceInstaller();
      // Service will run under system account
      processInstaller.Account = ServiceAccount.NetworkService;
      // Service will have Start Type of Manual
      serviceInstaller.StartType = ServiceStartMode.Manual;
      // Here we are going to hook up some custom events prior to the install and uninstall
      BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
      BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
      // serviceInstaller.ServiceName = servicename;
      Installers.Add(serviceInstaller);
      Installers.Add(processInstaller);
    }

    private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
    {
      //Assembly asembl;
      //asembl = Assembly.GetExecutingAssembly();
      //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

      //The service has a fixed name.
      serviceInstaller.ServiceName = "WWP_SiteService";
    }

    private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
    {
      //Assembly asembl;
      //asembl = Assembly.GetExecutingAssembly();
      //serviceInstaller.ServiceName = asembl.ManifestModule.Name;

      //The service has a fixed name.
      serviceInstaller.ServiceName = "WWP_SiteService";
    }
  }

}