//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_ProcessRequest.cs
// 
//   DESCRIPTION: WWP Process requests from WWP Server.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 11-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-FEB-2011 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WWP;
using System.Collections;
using WSI.WWP_SiteService;

namespace WSI.WWP_Client
{
  #region WWP_RequestTask CLASS

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_RequestTask
  // 
  //   DESCRIPTION : Class for encapsulate request messages in a one task.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 11-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public class WWP_RequestTask : Task
  {
    private WWP_Message m_wwp_request;
    private WWP_Message m_wwp_response;

    public WWP_Message WwpRequest
    {
      get { return m_wwp_request; }
      set { m_wwp_request = value; }
    }

    public WWP_Message WwpResponse
    {
      get { return m_wwp_response; }
      set { m_wwp_response = value; }
    }

    private int m_tick_arrived;
    private int m_tick_dequeued;
    private int m_tick_processed;

    //------------------------------------------------------------------------------
    // PURPOSE : Create a task
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpListener
    //          - WwpClient
    //          - XmlMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public WWP_RequestTask(WWP_Message WwpRequestMessage)
    {
      m_wwp_request = WwpRequestMessage;

      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;
      if (WwpRequestMessage.MsgHeader.MsgType != WWP_MsgTypes.WWP_MsgCreateAccounts)
      {
        m_wwp_response = WWP_Message.CreateMessage(m_wwp_request.MsgHeader.MsgType + 1);
        m_wwp_response.MsgHeader.PrepareReply(m_wwp_request.MsgHeader);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Execute task. (Pool with BatchUpdate = False)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public override void Execute()
    {
      try
      {
        WWP_ProcessCenterRequest.ProcessRequestMsgTask(this);
      }
      catch
      {
      }
      finally
      {
      }
    }
  }

  #endregion // WWP_RequestTask CLASS

  #region WWP_RequestWorkerPool CLASS

  public enum WWP_RequestPool
  {
    Default = 0,  //  10 Workers
  };

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_RequestWorkerPool
  // 
  //   DESCRIPTION : Class for create workers (in Pools) for process messages.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public static class WWP_RequestWorkerPool
  {
    private static WorkerPool m_pool_default = null;

    //------------------------------------------------------------------------------
    // PURPOSE : Return the Pool for this task.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Pool
    //
    private static WWP_RequestPool RequestPool(WWP_RequestTask Task)
    {
      WWP_MsgTypes _msg_type;
      String _tag0;
      String _tag1;
      String _str_value;
      int _idx0;
      int _idx1;

      _msg_type = WWP_MsgTypes.WWP_MsgUnknown;

      _tag0 = "<MsgType>";
      _tag1 = "</MsgType>";
      _idx0 = Task.WwpRequest.ToXml().IndexOf(_tag0);
      if (_idx0 >= 0)
      {
        _idx0 = _idx0 + _tag0.Length;
        _idx1 = Task.WwpRequest.ToXml().IndexOf(_tag1, _idx0);
        if (_idx1 - _idx0 > 0)
        {
          _str_value = Task.WwpRequest.ToXml().Substring(_idx0, _idx1 - _idx0);
          _str_value = _str_value.Trim();
          _msg_type = (WWP_MsgTypes)WWP_MsgHeader.Decode(_str_value, WWP_MsgTypes.WWP_MsgUnknown);
        }
      }

      switch (_msg_type)
      {
        case WWP_MsgTypes.WWP_MsgUnknown:
        default:
          {
            return WWP_RequestPool.Default;
          }
      } // switch
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Init all Worker Pools.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      int _pool_workers;

      _pool_workers = 10;
      if (Environment.GetEnvironmentVariable("WWP_REQUESTPOOL_DEFAULT") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WWP_REQUESTPOOL_DEFAULT"));
      }
      m_pool_default = new WorkerPool("WWP Request Pool Default", _pool_workers);

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue task into corresponding Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Enqueue(WWP_RequestTask Task)
    {
      WWP_RequestPool _pool;

      _pool = RequestPool(Task);

      switch (_pool)
      {
        case WWP_RequestPool.Default:
          m_pool_default.EnqueueTask(Task);
          break;

        default:
          Log.Error("Unknown WWP Request Pool: " + _pool.ToString());
          break;
      }
    }
  }

  #endregion // WWP_RequestWorkerPool CLASS

  #region WWP_ProcessRequest CLASS

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_ProcessRequest
  // 
  //   DESCRIPTION : Class for process request messages.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 11-FEB-2011
  // 
  //------------------------------------------------------------------------------
  public static class WWP_ProcessCenterRequest
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Init.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      WWP_RequestWorkerPool.Init();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process WWP Message received:
    //              - Create Task.
    //              - Enqueue task into the worker Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Enqueue(WWP_Message WwpMessage)
    {
      WWP_RequestTask _input_task;

      _input_task = new WWP_RequestTask(WwpMessage);

      WWP_RequestWorkerPool.Enqueue(_input_task);

    } // Enqueue

    //------------------------------------------------------------------------------
    // PURPOSE : Process specific actions for WWP Message.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpTask
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void ProcessRequestMsgTask(WWP_RequestTask WwpTask)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;

      _wwp_request = WwpTask.WwpRequest;
      _wwp_response = null;
      if (WwpTask.WwpResponse != null)
      {
        _wwp_response = WwpTask.WwpResponse;
        _wwp_response.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;
      }

      try
      {
        WWP_Process.CenterRequest (_wwp_request, _wwp_response);
      }
      catch (WWP_Exception wwp_ex)
      {
        try
        {
          if (_wwp_response == null)
          {
            _wwp_response = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgError);
          }
          _wwp_response.MsgHeader.ResponseCode = wwp_ex.ResponseCode;
          if (_wwp_request != null)
          {
            _wwp_response.MsgHeader.SequenceId = _wwp_request.MsgHeader.SequenceId;
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
        try
        {
          if (_wwp_response != null)
          {
            WWP_Protocol.SendReply(_wwp_response);
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }

    } // ProcessRequestMsgTask
  }

  #endregion // WWP_ProcessRequest CLASS
}
