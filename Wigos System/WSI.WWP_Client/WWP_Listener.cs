//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_Listener.cs
// 
//   DESCRIPTION: WWP_Listener class
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 30-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAR-2011 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.WWP;
using System.Data.SqlClient;
using System.Net;
using System.Collections;
using WSI.WWP_SiteService;

internal class WWP_Listener : SecureTcpServer
{
  static  WWP_Listener m_listener = null;

  //------------------------------------------------------------------------------
  // PURPOSE : We only have ONE WWP_Listener. This routine creates this one, if it doesn't exist.
  //
  //  PARAMS :
  //      - INPUT :
  //          - IPEndPoint ListenOn
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  static public void Create(IPEndPoint ListenOn)
  {
    if ( m_listener != null )
    {
      return;
    }
    m_listener = new WWP_Listener(ListenOn);
  } // Create

  private const String WWP_STX = "<WWP_Message>";
  private const String WWP_ETX = "</WWP_Message>";
  private const Int32 WWP_MESSAGE_MAX_LENGTH = 32 * 1024 * 1024;

  private WWP_Listener(IPEndPoint IPEndPoint)
    : base(IPEndPoint)
  {
    this.SetXmlServerParameters(WWP_STX, WWP_ETX, WWP_MESSAGE_MAX_LENGTH, Encoding.UTF8);

    WWP_WorkerPool.Init();
    
    this.Start();
  } // WWP_Listener

  //------------------------------------------------------------------------------
  // PURPOSE : This routine is called when a WWP_Message is received.
  //
  //  PARAMS :
  //      - INPUT :
  //          - SecureTcpClient SecureTcpClient
  //          - string XmlMessage
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //
  public override void XmlMesageReceived(SecureTcpClient SecureTcpClient, string XmlMessage)
  {
    try
    {
      WWP_Task _wwp_task;
      WWP_Message _wwp_message;
      WWP_MsgTypes _msg_type;

      _wwp_message = WWP_Message.CreateMessage(XmlMessage);

      _msg_type = _wwp_message.MsgHeader.MsgType;
      if (_msg_type.ToString().IndexOf("Reply") >= 0
          || _wwp_message.MsgHeader.MsgType == WWP_MsgTypes.WWP_MsgError)
      {
        // Ignore Reply & Error messages
        return;
      }

      _wwp_task = new WWP_Task(this, SecureTcpClient, _wwp_message);

      WWP_WorkerPool.Enqueue(_wwp_task);
    }
    catch (Exception ex)
    {
      Log.Exception(ex);
      Log.Message("EXEP Msg: " + XmlMessage);
    }
  } // XmlMesageReceived

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WWP_WorkerPool
  // 
  //   DESCRIPTION : Class for create workers (in Pools) for process messages.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  private static class WWP_WorkerPool
  {
    private static WorkerPool m_pool_default = null;

    //------------------------------------------------------------------------------
    // PURPOSE : Init all Worker Pools.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    internal static void Init()
    {
      int _pool_workers;

      _pool_workers = 10;
      if (Environment.GetEnvironmentVariable("WWP_POOL_DEFAULT") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WWP_POOL_DEFAULT"));
      }
      m_pool_default = new WorkerPool("WWP Pool Default", _pool_workers);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue task into corresponding Pool.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Task
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    internal static void Enqueue(WWP_Task Task)
    {
      WorkerPool _pool;

      // Select pool (for now only default pool exists)
      _pool = m_pool_default;

      // Enqueue to the selected pool
      _pool.EnqueueTask(Task);
    } // Enqueue

  } // Class WWP_WorkerPool

  //------------------------------------------------------------------------------
  // 
  //    CLASS NAME : WwpInputMsgProcessTask
  // 
  //   DESCRIPTION : Class for encapsulate messages in a one task with listener, wcp_client and request message.
  // 
  //        AUTHOR: Alberto Cuesta
  // 
  // CREATION DATE: 04-FEB-2011
  // 
  //------------------------------------------------------------------------------
  private class WWP_Task : Task
  {
    Object m_wwp_listener;
    Object m_wwp_client;
    String m_xml_message;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    protected WWP_Message m_wwp_request = null;
    protected WWP_Message m_wwp_response = null;

    #region Properties

    protected String XmlResponse
    {
      get
      {
        return m_wwp_response.ToXml();
      }
    }
    protected internal String XmlRequest
    {
      get
      {
        return m_xml_message;
      }
    }
    public Object WwpListener
    {
      get
      {
        return m_wwp_listener;
      }
    }
    public Object WwpClient
    {
      get
      {
        return m_wwp_client;
      }
    }
    protected internal WWP_Message Request
    {
      get
      {
        return m_wwp_request;
      }
    }
    protected internal WWP_Message Response
    {
      get
      {
        return m_wwp_response;
      }
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Create a task
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpListener
    //          - WwpClient
    //          - XmlMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public WWP_Task(Object WwpListener, Object WwpClient, WWP_Message Request)
    {
      m_wwp_listener = WwpListener;
      m_wwp_client = WwpClient;
      m_xml_message = "";
      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;

      m_wwp_request = Request;
      m_wwp_response = WWP_Message.CreateMessage(m_wwp_request.MsgHeader.MsgType + 1);
      m_wwp_response.MsgHeader.PrepareReply(m_wwp_request.MsgHeader);
    } // WWP_Task

    //------------------------------------------------------------------------------
    // PURPOSE : Execute task. (Pool with BatchUpdate = False)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public override void Execute()
    {
      Boolean _error;
      String  _error_msg;
      SecureTcpClient _client;

      _error = true;
      _error_msg = "Unknown error";

      _client = (SecureTcpClient)WwpClient;

      if ( !_client.IsConnected )
      {
        // Avoid processing, the client has closed the connection
        return;
      }

      try
      {
        WWP_Process.ClientRequest(Request, Response);
        _error = false;
      }
      catch ( Exception _ex )
      {
        _error_msg = _ex.Message;

        Log.Exception (_ex);
      }
      finally 
      {
        if (_error)
        {
          WWP_MsgError _content;

          _content = new WWP_MsgError();
          _content.Description = _error_msg;

          Response.MsgHeader.MsgType = WWP_MsgTypes.WWP_MsgError;
          Response.MsgContent = _content;
        }
        if (_client.IsConnected)
        {
          _client.Send(Response.ToXml());
        }
      }
    } // Execute

  } // Class WWP_Task

} // Class WWP_Listener
