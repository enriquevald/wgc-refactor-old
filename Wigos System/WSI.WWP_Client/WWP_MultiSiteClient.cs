//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WWP_MultiSiteClient.cs
//
//   DESCRIPTION : WWP_MultiSiteClient class
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 25-FEB-2013
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-FEB-2013 JCM    First release.
// 11-JUL-2013 JML    Changed Triggers status control. CheckTriggersStatus().
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.WWP_SiteService.MultiSite;
using System.Data;
using System.Collections;

namespace WSI.WWP_SiteService
{

  public static class WWP_MultiSiteClient
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_MultiSiteClient Class
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(MultiSiteThreads);
      _thread.Name = "MultiSiteThreads";
      _thread.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for MultiSite background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void MultiSiteThreads()
    {
      Thread _thread;
      ArrayList _threadList;
      TriggersStatus _triggers_status;
      Int32 _wait_hint;

      _threadList = new ArrayList();

      _thread = new Thread(MultiSiteTasksThread);
      _thread.Name = "MultiSiteTasksThread";
      _threadList.Add(_thread);

      _thread = new Thread(MultiSiteThread_jobs);
      _thread.Name = "MultiSiteThread.job";
      _threadList.Add(_thread);

      _thread = new Thread(UploadGamePlaySessionsThread);
      _thread.Name = "UploadGamePlaySessionsThread";
      _threadList.Add(_thread);

      _thread = new Thread(DownloadPersonalInfoAllThread);
      _thread.Name = "DownloadPersonalInfoAllThread";
      _threadList.Add(_thread);

      try
      {
        _wait_hint = 5 * 60 * 1000; // 5 minute

        while (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
        {
          Thread.Sleep(_wait_hint);

          _triggers_status = MultiSiteCommon.CheckTriggersStatus();
          if (_triggers_status != TriggersStatus.AllDisable)
          {
            Log.Error(" The triggers of MultiSite are enabled. CheckTriggersStatus " + _triggers_status.ToString());
          }

          continue;
        }

        for (Int32 _i = 0; _i < _threadList.Count; _i++)
        {
          _thread = (Thread)_threadList[_i];
          _thread.Start();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // MultiSiteThreads

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for MultiSite background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void MultiSiteTasksThread()
    {
      Int32 _wait_hint;
      Int32 _numpending_tasks_tick;
      Int32 _triggers_tick;
      TriggersStatus _triggers_status;

      _wait_hint = 0;
      _numpending_tasks_tick = Misc.GetTickCount();
      _triggers_tick = Misc.GetTickCount();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minute

            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllDisable)
            {
              Log.Error(" The triggers of MultiSite are enabled. CheckTriggersStatus " + _triggers_status.ToString());
            }

            continue;
          }

          if (Misc.GetElapsedTicks(_triggers_tick) > 15000)
          {
            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllEnabled )
            {
              Log.Error(" The triggers of MultiSite are disabled. CheckTriggersStatus " + _triggers_status.ToString() );
              _wait_hint = 1 * 60 * 1000; // 1 minute

              continue;
            }
            _triggers_tick = Misc.GetTickCount();
          }

          while (!WSI.WWP_Client.WWP_Protocol.IsConnected())
          {
            Thread.Sleep(_wait_hint);
            _wait_hint = 1000;
            // TODO: Update "pending" counters
          }

          if (Misc.GetElapsedTicks(_numpending_tasks_tick) > 15000)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!MultiSite_SiteConfiguration.Update_NumPendings(_db_trx.SqlTransaction))
              {
                Log.Error(" Not updated pending number from tasks. Update_NumPending()");
              }

              _db_trx.Commit();
            }
            _numpending_tasks_tick = Misc.GetTickCount();
          }

          //
          // TASKS SYNCHRONIZATION
          //

          // Download Configuration (general params, tasks, sequences)
          if (!MultiSite_SiteConfiguration.SynchConfiguration())
          {
            Log.Error("SynchConfiguration Failed.");
          }

          // Sync with Center Points 
          if (!MultiSite_SitePoints.SynchPoints())
          {
            Log.Error("Upload SynchPoints Failed.");
          }

          // Sync PENDING Accounts with Center 
          if (!MultiSiteAccounts.SyncPendingPersonalInfo_Upload())
          {
            Log.Error("Upload Personal_Info Failed");
          }

          // Download Points All
          if (CommonMultiSite.GetPlayerTrackingMode()  == PlayerTracking_Mode.Multisite)
          {
            if (!MultiSite_SitePoints.SiteTask_DownloadPoints(TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All))
            {
              Log.Error("DownloadPoints All Failed.");
            }
          }
          //
          // Generic Upload & Download
          //

          MultiSite_SiteGenericUpload.UploadAllTimestampTable();
          MultiSite_SiteGenericUpload.UploadAllSequenceTable();
          MultiSite_SiteGenericDownload.DownloadAllSequenceTable();
          MultiSite_SiteGenericDownload.DownloadAllTimeStampTable();


          // Purge old Accounts in MS_SITE_SYNCHRONIZE_ACCOUNTS table
          if (!MultiSiteCommon.Delete_AccountsToForceSync())
          {
            Log.Error("Delete_AccountsToForceSync Failed.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while(true)

    } //MultiSiteTasksThread

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for MultiSite background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void MultiSiteThread_jobs()
    {

      Int32 _wait_hint;
      Int32 _numpending_tasks_tick;
      Int32 _triggers_tick;
      TriggersStatus _triggers_status;
      Int32 _wait_hint_1;
      Int32 _wait_hint_2;

      _wait_hint = 1000;
      _numpending_tasks_tick = Misc.GetTickCount();
      _triggers_tick = Misc.GetTickCount();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minute

            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllDisable)
            {
              Log.Error(" The triggers of MultiSite are enabled. CheckTriggersStatus " + _triggers_status.ToString());
            }

            continue;
          }

          _wait_hint_1 = Int32.MaxValue;
          if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
          {
            MultiSiteCommon.Elp01_SplitRedeemNoredeemPlaySession(out _wait_hint_1);
          }

          if (Misc.GetElapsedTicks(_triggers_tick) > 15000)
          {
            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllEnabled)
            {
              Log.Error(" The triggers of MultiSite are disabled. CheckTriggersStatus " + _triggers_status.ToString());
              _wait_hint = 1 * 60 * 1000; // 1 minute

              continue;
            }
            _triggers_tick = Misc.GetTickCount();
          }

          // TASKS Fill table to upload         
          MultiSiteCommon.GroupPlaySessionPerGame(out _wait_hint_2);
          _wait_hint = MinNumber(_wait_hint_1, _wait_hint_2);

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while(True)

    } // MultiSiteThread_jobs

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for MultiSite background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void UploadGamePlaySessionsThread()
    {
      Int32 _wait_hint;
      Int32 _numpending_tasks_tick;
      Int32 _triggers_tick;
      TriggersStatus _triggers_status;
      Int32 _master_mode;

      _wait_hint = 0;
      _numpending_tasks_tick = Misc.GetTickCount();
      _triggers_tick = Misc.GetTickCount();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minute

            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllDisable)
            {
              Log.Error(" The triggers of MultiSite are enabled. CheckTriggersStatus " + _triggers_status.ToString());
            }

            continue;
          }

          if (Misc.GetElapsedTicks(_triggers_tick) > 15000)
          {
            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllEnabled)
            {
              Log.Error(" The triggers of MultiSite are disabled. CheckTriggersStatus " + _triggers_status.ToString());
              _wait_hint = 1 * 60 * 1000; // 1 minute

              continue;
            }
            _triggers_tick = Misc.GetTickCount();
          }

          while (!WSI.WWP_Client.WWP_Protocol.IsConnected())
          {
            Thread.Sleep(_wait_hint);
            _wait_hint = 1000;

            continue;
          }

          //
          // TASKS SYNCHRONIZATION
          //
          _master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0);
          if (_master_mode == 1) //  CODERE: Only editing Multisite!
          {
            if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions, MultiSite_SiteGenericUpload.Trx_UploadGamePlaySessions))
            {
              Log.Error("Upload Game Play Sessions Failed.");
            }
          }
          //else if (_master_mode == 2)// Only editing Site
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while(true)

    } // UploadGamePlaySessionsThread

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the minimim of two numbers
    //
    //  PARAMS :
    //      - INPUT :
    //            - Number1
    //            - Number2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32
    //
    private static Int32 MinNumber(Int32 Number1, Int32 Number2)
    {
      if (Number1 < Number2)
      {

        return Number1;
      }

      return Number2;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for MultiSite background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void DownloadPersonalInfoAllThread()
    {
      Int32 _wait_hint;
      Int32 _numpending_tasks_tick;
      Int32 _triggers_tick;
      TriggersStatus _triggers_status;

      _wait_hint = 0;
      _numpending_tasks_tick = Misc.GetTickCount();
      _triggers_tick = Misc.GetTickCount();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minute

            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllDisable)
            {
              Log.Error(" The triggers of MultiSite are enabled. CheckTriggersStatus " + _triggers_status.ToString());
            }

            continue;
          }

          if (Misc.GetElapsedTicks(_triggers_tick) > 15000)
          {
            _triggers_status = MultiSiteCommon.CheckTriggersStatus();
            if (_triggers_status != TriggersStatus.AllEnabled)
            {
              Log.Error(" The triggers of MultiSite are disabled. CheckTriggersStatus " + _triggers_status.ToString());
              _wait_hint = 1 * 60 * 1000; // 1 minute

              continue;
            }
            _triggers_tick = Misc.GetTickCount();
          }

          while (!WSI.WWP_Client.WWP_Protocol.IsConnected())
          {
            Thread.Sleep(_wait_hint);
            _wait_hint = 1000;

            continue;
          }

          //
          // TASKS SYNCHRONIZATION
          //
          // Download Accounts All
          if (!MultiSiteAccounts.SiteTask_DownloadAccounts(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All))
          {
            Log.Error("Download Personal Info All Failed.");
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while(true)

    } // DownloadPersonalInfoAllThread

  } // class
}
