//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_ProcessCenterRequest.cs
// 
//   DESCRIPTION: WWP Process requests from WWP Center Service.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 11-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-FEB-2011 ACC    First release.
// 01-FEB-2016 AMF    PBI 8272: Space Requests
// 01-MAR-2016 AMF    Bug 10146:Canjes SPACE: NonCashableGroupId 0 valid
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_SiteService.MultiSite;
using System.Data;
using System.Collections;

namespace WSI.WWP_SiteService
{
  static public partial class WWP_Process
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Process a Request from WWP Center Service and return Response.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WWP_Message Request
    //
    //      - OUTPUT :
    //          - WWP_Message Response
    //
    // RETURNS :
    //
    static public void CenterRequest(WWP_Message Request, WWP_Message Response)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _error;

      _sql_conn = null;
      _sql_trx = null;
      _error = true;
      _wwp_response = null;
      _wwp_request = Request;     
      
      if (Response != null)
      {
        _wwp_response = Response;
        _wwp_response.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;
      }

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();
        // Begin Transaction
        _sql_trx = _sql_conn.BeginTransaction();

        switch (_wwp_request.MsgHeader.MsgType)
        {
          case WWP_MsgTypes.WWP_MsgSqlCommand:
            WWP_ProcessRequest.SqlCommand(_wwp_request, _wwp_response, _sql_trx);
            break;

          case WWP_MsgTypes.WWP_MsgTable:
            MsgTableCenterRequest(_wwp_request, _wwp_response, _sql_trx);
            break;

          case WWP_MsgTypes.WWP_MsgCreateAccounts:
            MultiSite_Elp01SpaceAccountsDownload.CreateChangeSpaceAccount(((WWP_MsgCreateAccounts)Request.MsgContent).List);
            break;

          default:
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_ERROR);
        }

        // End Transaction
        _sql_trx.Commit();

        _error = false;
      }
      catch (WWP_Exception wwp_ex)
      {
        try
        {
          if (_wwp_response == null)
          {
            _wwp_response = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgError);
          }
          _wwp_response.MsgHeader.ResponseCode = wwp_ex.ResponseCode;
          if (_wwp_request != null)
          {
            _wwp_response.MsgHeader.SequenceId = _wwp_request.MsgHeader.SequenceId;
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_error && _sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          try { _sql_trx.Dispose(); }
          catch { }
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          try { _sql_conn.Dispose(); }
          catch { }
          _sql_conn = null;
        }
      }
    } // CenterRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Process a MsgTable Request from WWP Center Service and return Response.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WWP_Message WwpRequest
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - WWP_Message WwpResponse
    //
    // RETURNS :
    //
    static private void MsgTableCenterRequest(WWP_Message WwpRequest, WWP_Message WwpResponse, SqlTransaction SqlTrx)
    {
      WWP_MsgTable _request;
      WWP_MsgTableReply _response;

      _request = (WWP_MsgTable)WwpRequest.MsgContent;
      _response = (WWP_MsgTableReply)WwpResponse.MsgContent;

      WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;

      try
      {
        switch (_request.Type)
        {
          case WWP_TableTypes.TABLE_TYPE_MULTISITE_RESPONSE:
          {
            if (!MS_Request_Process.IncommingCenterRequestMessage(WwpRequest.MsgHeader.ResponseCode, _request.DataSet.Tables[0]))
            {
              Log.Error("Calling IncommingCenterRequestMessage.");

              return;
            }

            // Nothing data to response
            _response.Format = _request.Format;
            _response.DataSet = new DataSet("DataSet");
            WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
          }
          break;

          case WWP_TableTypes.TABLE_TYPE_SPACE:
          if (!ManageSpaceRequest(_request.DataSet.Tables[0].Rows[0], ref WwpResponse))
          {
            WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_ERROR_INTERNAL_SERVER_ERROR;

            Log.Error("Calling ManageSpaceRequest.");
          }
          break;

          default:
          break;
        }

      }
      catch (Exception _ex)
      {
        WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;

        Log.Exception(_ex);
      }

    } // MsgTableCenterRequest

    #region " Space Request "

    /// <summary>
    /// Manage Space Request
    /// </summary>
    /// <param name="SpaceRequest"></param>
    /// <param name="WwpResponse"></param>
    /// <returns></returns>
    private static Boolean ManageSpaceRequest(DataRow SpaceRequest, ref WWP_Message WwpResponse)
    {
      Int32 _restricted_to_group_id;
      Decimal _amount_to_add;
      Promotion.PROMOTION_TYPE _promotion_type;
      AccountPromotion _account_promotion;
      Int32 _aux;
      Int64 _account_id;
      String _elp_details;
      ArrayList _voucher_list;
      Int64 _operation_id;
      Boolean _promo_as_cash_in;
      CashierSessionInfo _session;
      DataTable _dt_account_promo;
      WWP_ResponseCodes _response_code;

      try
      {
        _amount_to_add = ((Decimal)(Int64)SpaceRequest["AMOUNT"] / 100);
        _restricted_to_group_id = (Int32)SpaceRequest["GROUP_ID"];
        _account_id = (Int64)SpaceRequest["ACCOUNT_ID"];
        _voucher_list = new ArrayList();
        _operation_id = 0;
        _promo_as_cash_in = false;
        _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
        _elp_details = Resource.String("STR_PROMO_EXTERNAL_DETAIL") + " " + (String)SpaceRequest["TRANSACTION_ID"];

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (ExistsElp01SpaceRequest(SpaceRequest, _db_trx.SqlTransaction, out _response_code))
          {
            WwpResponse.MsgHeader.ResponseCode = _response_code;

            return true;
          }

          // Cashable = 0
          // NonCashable = 1
          // Tables = 2
          if ((Int16)SpaceRequest["CREDIT_TYPE"] == 0)
          {
            _promotion_type = Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM;
          }
          else if ((Int16)SpaceRequest["CREDIT_TYPE"] == 1)
          {
            _promotion_type = Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM;

            if (_restricted_to_group_id >= 1)
            {
              if (_restricted_to_group_id <= 9)
            {
              _aux = (Int32)Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01;
              _aux += (_restricted_to_group_id - 1);
              _promotion_type = (Promotion.PROMOTION_TYPE)_aux;

              if (Promotion.ReadPromotionData(_db_trx.SqlTransaction, _promotion_type, out _account_promotion))
              {
                if (!_account_promotion.enabled)
                {
                  WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_ERROR_NON_CASHABLE_GROUP;
                    Log.Message("Error: Blocked promotion: " + _restricted_to_group_id.ToString());

                  return true;
                }
              }
            }
            else
            {
              WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_ERROR_NON_CASHABLE_GROUP;
                Log.Message("Error: NonCashableGroupId > 9");

              return true;
            }
          }
          }
          else
          {
            WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_ERROR_ACCOUNT_INFO;
            Log.Message("Error: CREDIT_TYPE Not Implemented");

            return true;
          }

          if (Accounts.ProcessRecharge(_session, _account_id, _amount_to_add, true, _promotion_type, _elp_details, _db_trx.SqlTransaction, out _voucher_list, out _operation_id, out _promo_as_cash_in, out _dt_account_promo))
          {
            if (InsertElp01SpaceRequest(SpaceRequest, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();

              WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_OK_TRANSFERRED;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ManageSpaceRequest

    /// <summary>
    /// Insert register in Elp01_space_requests
    /// </summary>
    /// <param name="SpaceRequest"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean InsertElp01SpaceRequest(DataRow SpaceRequest, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   ELP01_SPACE_REQUESTS ");
        _sb.AppendLine("             ( ES_ACCOUNT_ID        ");
        _sb.AppendLine("             , ES_TRANSACTION_ID    ");
        _sb.AppendLine("             , ES_CREDIT_TYPE       ");
        _sb.AppendLine("             , ES_AMOUNT            ");
        _sb.AppendLine("             , ES_GROUP_ID)         ");
        _sb.AppendLine("      VALUES ( @pAccountId          ");
        _sb.AppendLine("             , @pTransactionId      ");
        _sb.AppendLine("             , @pCreditType         ");
        _sb.AppendLine("             , @pAmount             ");
        _sb.AppendLine("             , @pGroupId)           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = SpaceRequest["ACCOUNT_ID"];
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar).Value = SpaceRequest["TRANSACTION_ID"];
          _sql_cmd.Parameters.Add("@pCreditType", SqlDbType.Int).Value = SpaceRequest["CREDIT_TYPE"];
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ((Decimal)(Int64)SpaceRequest["AMOUNT"] / 100);
          _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.Int).Value = SpaceRequest["GROUP_ID"];

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertElp01SpaceRequest

    /// <summary>
    /// Check if exists Space Request
    /// </summary>
    /// <param name="SpaceRequest"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="ResponseCode"></param>
    /// <returns></returns>
    private static Boolean ExistsElp01SpaceRequest(DataRow SpaceRequest, SqlTransaction SqlTrx, out WWP_ResponseCodes ResponseCode)
    {
      StringBuilder _sb;

      ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ES_ACCOUNT_ID                       ");
        _sb.AppendLine("        , ES_AMOUNT                           ");
        _sb.AppendLine("   FROM   ELP01_SPACE_REQUESTS                ");
        _sb.AppendLine("  WHERE   ES_TRANSACTION_ID = @pTransactionId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar).Value = SpaceRequest["TRANSACTION_ID"];

          using (SqlDataReader _dr = _sql_cmd.ExecuteReader())
          {
            if (_dr.Read())
            {
              if (_dr.GetInt64(0) == (Int64)SpaceRequest["ACCOUNT_ID"] && _dr.GetDecimal(1) == ((Decimal)(Int64)SpaceRequest["AMOUNT"] / 100))
              {
                ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_OK_ALREADY_TRANSFERED;
              }
              else
              {
                ResponseCode = WWP_ResponseCodes.WWP_RC_SPACE_ERROR_TRANSACTIONID_ALREADY_EXISTS;
              }

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        ResponseCode = WWP_ResponseCodes.WWP_RC_UNKNOWN;

        return true;
      }

      return false;
    } // ExistsElp01SpaceRequest

    #endregion " Space Request "

  } // Class WWP_Process

} // WSI.WWP_SiteService
