//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_Protocol.cs
// 
//   DESCRIPTION: WWP_Protocol class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-FEB-2011 ACC    First release.
// 06-JUL-2016 ETP    Fixed bug 14082: Fixed conection errors with windows update.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Threading;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_SiteService;

namespace WSI.WWP_Client
{
  public static class WWP_Protocol
  {
    #region Constants

    private const Int32 WWP_TIMEOUT = 30000;  // AJQ 19-ABR-2010, TCP is connected, so we wait a bit more ...
    private const Int32 WWP_PORT = 13002;     // AJQ 09-MAR-2011

    #endregion

    #region Enums

    private enum WWP_PROTOCOL_STATUS
    {
      UNKNOWN = 0,
      CONNECTED = 1,
      DISCONNECTED = 2,
    }

    #endregion

    #region Members

    private static WWP_PROTOCOL_STATUS m_status = WWP_PROTOCOL_STATUS.UNKNOWN;
    private static AutoResetEvent m_status_changed = new AutoResetEvent(true);

    private static WaitableQueue m_queue_receive;
    private static TCPClientSSL m_ssl_client;

    private static Thread m_thread;

    private static Int64 m_session_id = 0;
    private static Int64 m_sequence_id = 0;

    private static ReaderWriterLock m_lock;

    private static int m_keep_alive_interval = 30000;                                  // 30 seconds
    private static int m_connection_timeout = CommonMultiSite.WwpConnectionTimeout();  // 3 minutes
    private static int m_tick_last_msg_sent = Misc.GetTickCount();
    private static int m_tick_last_msg_received = Misc.GetTickCount();
    private static String m_last_connected_address = "";

    private static ArrayList m_current_trxs = new ArrayList();  // WWP_CurrentTrx

    // AJQ 21-MAR-2013, Statistics
    private static Int64 m_snd_count = 0;
    private static Int64 m_snd_bytes = 0;
    private static Int64 m_rcv_count = 0;
    private static Int64 m_rcv_bytes = 0;
    private static Int64 m_save_stats_control = -1;

    private static int m_show_stats = Misc.GetTickCount();
    //private static Int64 m_xsnd_bytes = 0;
    //private static Int64 m_xrcv_bytes = 0;

    private static String m_partial_msg = "";

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WWP_Protocol class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      // Init ReaderWriterLock
      m_lock = new ReaderWriterLock();

      // Init Queue
      m_queue_receive = new WaitableQueue();

      // Init TCPClientSSL
      m_ssl_client = new TCPClientSSL(m_queue_receive);

      m_thread = new Thread(WwpProtocolThread);
      m_thread.Name = "WwpProtocolThread";
      m_thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Stop WWP_Protocol class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Stop()
    {
      SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);
    }

    #endregion Public Functions

    #region Status Management

    //------------------------------------------------------------------------------
    // PURPOSE : Wait Status Changed
    //
    //  PARAMS :
    //      - INPUT :
    //          - MillisecondsTimeout
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static WWP_PROTOCOL_STATUS WaitStatusChanged(int MillisecondsTimeout)
    {
      m_status_changed.WaitOne(MillisecondsTimeout, false);

      return m_status;
    } // WaitStatusChanged

    //------------------------------------------------------------------------------
    // PURPOSE : Set Status
    //
    //  PARAMS :
    //      - INPUT :
    //          - NewStatus
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static WWP_PROTOCOL_STATUS SetStatus(WWP_PROTOCOL_STATUS NewStatus)
    {
      WWP_PROTOCOL_STATUS _old_status;

      if (m_lock == null)
      {
        return WWP_PROTOCOL_STATUS.DISCONNECTED;
      }

      m_lock.AcquireWriterLock(Timeout.Infinite);

      _old_status = m_status;
      try
      {
        if (NewStatus == WWP_PROTOCOL_STATUS.DISCONNECTED)
        {
          // always close the client
          try
          {
            m_ssl_client.Disconnect();
          }
          catch { ;}
        }

        if (_old_status != NewStatus)
        {
          AlarmCode _alarm_code;
          String _alarm_description;

          //JML 25-OCT-2013 Only if the new status is correctly save in DB, it set the status, write logs & alarms.....
          //                This is the solution for defect WIG-211

          if (SaveStatusChanged(NewStatus, m_last_connected_address))
          {
            m_status = NewStatus;

            Log.Message("WWP_PROTOCOL_STATUS changed: " + _old_status.ToString() + " --> " + m_status.ToString());

            m_status_changed.Set();

            if (_old_status != WWP_PROTOCOL_STATUS.UNKNOWN) // First time (UNKNOWN -> DISCONNECTED) No Log
            {
              // Alarm
              _alarm_code = (m_status == WWP_PROTOCOL_STATUS.CONNECTED ? AlarmCode.TerminalSystem_WwpConnectedToCenter : AlarmCode.TerminalSystem_WwpDisconnectedFromCenter);
              _alarm_description = Resource.String(Alarm.ResourceId(_alarm_code), m_last_connected_address);

              Alarm.Register(AlarmSourceCode.Service,
                             Site.Id,
                             Misc.ConcatProviderAndTerminal(Site.Id.ToString(), Site.Name),
                             _alarm_code,
                             _alarm_description);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        m_status = WWP_PROTOCOL_STATUS.DISCONNECTED;

        // On any error close the client.
        try
        {
          m_ssl_client.Disconnect();
        }
        catch { ;}

      }
      finally
      {
        m_lock.ReleaseWriterLock();
      }

      return _old_status;
    } // SetStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Is connected ?
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static Boolean IsConnected()
    {
      if (m_status == WWP_PROTOCOL_STATUS.CONNECTED)
      {
        if (m_ssl_client.IsConnected)
        {
          return true;
        }
      }
      return false;
    } // IsConnected

    #endregion

    #region Main Thread

    //------------------------------------------------------------------------------
    // PURPOSE : Obtain the list of configured Server IP Addresses.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - List<IPEndPoint>
    //
    private static List<IPEndPoint> GetIPEndPointList()
    {
      List<IPEndPoint> _ip_endpoint_list;
      String _host_addr;
      String _host;
      String _env;
      int _port;
      IPAddress[] _ip_list;
      String[] _part;
      String[] _address_list;

      _ip_endpoint_list = new List<IPEndPoint>();

      _address_list = new String[2];

      //_address_list[0] = Common.Misc.ReadGeneralParams("WWP", "ServerAddress1");
      //_address_list[1] = Common.Misc.ReadGeneralParams("WWP", "ServerAddress2");
      _address_list[0] = Common.Misc.ReadGeneralParams("MultiSite", "CenterAddress1");
      _address_list[1] = Common.Misc.ReadGeneralParams("MultiSite", "CenterAddress2");
      _env = Environment.GetEnvironmentVariable("WWP_HOST_ADDRESS");
      if (!String.IsNullOrEmpty(_env))
      {
        Log.Message("Variable WWP_HOST_ADDRESS = " + _env);

        _address_list[0] = _env;
        _address_list[1] = null;
      }

      for (int _idx = 0; _idx < 2; _idx++)
      {
        try
        {
          _host_addr = _address_list[_idx];
          if (String.IsNullOrEmpty(_host_addr))
          {
            continue;
          }
          _part = _host_addr.Split(new String[1] { ":" }, StringSplitOptions.RemoveEmptyEntries);
          _host = _part[0];
          _port = WWP_PORT;
          if (_part.Length > 1)
          {
            int.TryParse(_part[1], out _port);
          }
          if (_port == 0)
          {
            _port = WWP_PORT;
          }

          _ip_list = Dns.GetHostAddresses(_host);
          foreach (IPAddress _ip in _ip_list)
          {
            try
            {
              _ip_endpoint_list.Add(new IPEndPoint(_ip, _port));
            }
            catch
            { }
          }
        }
        catch
        { }
      }

      return _ip_endpoint_list;
    } // GetIPEndPointList

    //------------------------------------------------------------------------------
    // PURPOSE : Keep Alive message
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void KeepAliveWhileConnected()
    {
      long _elapsed_ticks;
      WWP_Message _WWP_request;
      String _xml_request;
      Byte[] _raw_request;

      while (m_status == WWP_PROTOCOL_STATUS.CONNECTED)
      {
        try
        {
          SaveStats();

          if (WaitStatusChanged(1000) != WWP_PROTOCOL_STATUS.CONNECTED)
          {
            return;
          }
          if (!IsConnected())
          {
            SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }
          // Check Last Received Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_received);
          if (_elapsed_ticks > m_connection_timeout)
          {
            SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          // Check Last Sent Message
          _elapsed_ticks = Misc.GetElapsedTicks(m_tick_last_msg_sent);
          if (_elapsed_ticks <= m_keep_alive_interval)
          {
            // A few time ago a message was sent: continue waiting
            continue;
          }

          _WWP_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgKeepAlive);
          _WWP_request.MsgHeader.SessionId = m_session_id;
          GetSequenceId(out _WWP_request.MsgHeader.SequenceId);

          _xml_request = _WWP_request.ToXml();
          _raw_request = Encoding.UTF8.GetBytes(_xml_request);

          if (!m_ssl_client.Send(_raw_request))
          {
            SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);

            return;
          }

          m_tick_last_msg_sent = Misc.GetTickCount();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);

          return;
        }
      }
    } // KeepAliveWhileConnected

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void WwpProtocolThread()
    {
      WWP_PROTOCOL_STATUS _wwp_status;
      List<IPEndPoint> _endpoints;
      int _wait_hint;
      int _num_tries;
      int _idx_ip;



      Log.Message("WWP Protocol Thread Started ...");

      _endpoints = new List<IPEndPoint>(); // Empty list
      _num_tries = 0;
      _idx_ip = 0;
      _wait_hint = 0;

      while (true)
      {
        try
        {
          SaveStats();

          _wwp_status = WaitStatusChanged(1000);

          switch (_wwp_status)
          {
            default:
            case WWP_PROTOCOL_STATUS.UNKNOWN:
              {
                SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);
              }
              break;

            case WWP_PROTOCOL_STATUS.CONNECTED:
              {
                // ONLY FOR TEST ----------------------------------------------------
                ////// Test RequestCenter
                ////if (true)
                ////{
                ////  WWP_SiteRequest.TestRequestCenter_SqlCommand();
                ////}
                ////if (true)
                ////{
                ////  WWP_SiteRequest.TestRequestCenter_Table();
                ////}
                // ONLY FOR TEST ----------------------------------------------------

                KeepAliveWhileConnected();
              }
              break;

            case WWP_PROTOCOL_STATUS.DISCONNECTED:
              {
                IPEndPoint _wwp_service_ipe;
                Boolean _disconnect;
                Boolean _enrolled;
                Boolean _msg_ok;
                int _idx_try;
                int _tick_disconnected;
                long _auto_kill;

                _disconnect = true;
                _enrolled = false;
                _msg_ok = false;

                _idx_try = 0;
                _tick_disconnected = Misc.GetTickCount();
                _auto_kill = GeneralParam.GetInt64("WWP", "AutoKill.OnDisconnected.Seconds", 120) * 1000;

                while (!_enrolled)
                {
                  Thread.Sleep(_wait_hint);
                  _wait_hint = 2 * 60 * 1000;   // 2 Minutes while Not MultiSiteMember Or No Remote IP's

                  // AJQ 16-APR-2013, Avoid connecting when the site is not a multisite-member.
                  if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
                  {
                    continue;
                  }

                  _endpoints = GetIPEndPointList();
                  if (_endpoints.Count == 0)
                  {
                    Log.Warning("WWP MultiSite Remote Addresses not defined");

                    continue;
                  }

                  _wait_hint = 10000;   // 10 Seconds after trying ALL the IP's

                  if (_idx_try > 0 && Misc.GetElapsedTicks(_tick_disconnected) > _auto_kill)
                  {
                    String _msg;
                    long _sec;

                    _sec = Misc.GetElapsedTicks(_tick_disconnected) / 1000;
                    _msg = String.Format("WWP - DISCONNECTED: {0} seconds,  Connection attempts: {1}", _sec, _idx_try);
                    Log.Warning(_msg);
                    WSI.WWP_SiteService.WWP_SiteService.Shutdown("WWP - DISCONNECTED");

                    return;
                  }
                  _idx_try++;

                  _idx_ip = _idx_ip % _endpoints.Count;
                  _wwp_service_ipe = _endpoints[_idx_ip];
                  _idx_ip++; // Set next IP 
                  _num_tries = (_num_tries + 1) % _endpoints.Count;
                  if (_num_tries != 0)
                  {
                    // We have more IP's to try: no wait between connections
                    _wait_hint = 0;
                  }

                  try
                  {
                    // Connect to the WWP_Server
                    m_ssl_client = new TCPClientSSL(m_queue_receive);
                    m_ssl_client.Connect(_wwp_service_ipe);
                    if (!m_ssl_client.IsConnected)
                    {
                      m_ssl_client.Disconnect();
                      m_ssl_client = null;

                      continue;
                    }

                    Log.Message("Connected to: " + _wwp_service_ipe.ToString());
                    _idx_ip--;      // Reuse current IP when get disconnected
                    _num_tries = 0; // Next time will be the first try to connect.
                    _wait_hint = 0; // Wait hint on next connection

                    _msg_ok = false;

                    // Enroll
                    WWP_Message _wwp_request;
                    WWP_Message _wwp_response;
                    String _xml_request;
                    Byte[] _raw_request;
                    IAsyncResult _result;

                    _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgEnrollSite);

                    //// Test (Simulate iPad)
                    //if (true)
                    //{
                    //  _wwp_request = WWP_Process.TestRequest();
                    //}
                    _xml_request = _wwp_request.ToXml();
                    _raw_request = Encoding.UTF8.GetBytes(_xml_request);

                    // AJQ 21-MAR-2013, Statistics
                    m_tick_last_msg_sent = Misc.GetTickCount();
                    m_snd_count += 1;
                    m_snd_bytes += (Int64)_raw_request.Length;

                    _result = m_ssl_client.BeginSend(_raw_request, new AsyncCallback(SendCallback), m_ssl_client);


                    // Response received
                    string _xml_response = "";

                    // Enrolled
                    _xml_response = ReciveSyncMessage();


                    _wwp_response = WWP_Message.CreateMessage(_xml_response);
                    if (_wwp_response.MsgHeader.ResponseCode == WWP_ResponseCodes.WWP_RC_OK)
                    {
                      WWP_MsgEnrollSiteReply _wwp_msg_reply;

                      _wwp_msg_reply = (WWP_MsgEnrollSiteReply)_wwp_response.MsgContent;
                      _msg_ok = true;

                      m_lock.AcquireWriterLock(Timeout.Infinite);
                      try
                      {
                        m_session_id = _wwp_response.MsgHeader.SessionId;
                        m_sequence_id = Math.Max(m_sequence_id, 1 + _wwp_msg_reply.LastSequenceId);
                      }
                      finally
                      {
                        m_lock.ReleaseWriterLock();
                      }
                      Log.Message("Enrolled, SessionId: " + _wwp_response.MsgHeader.SessionId);
                    }
                    else
                    {
                      Log.Message("Enroll failed, ResponseCode:" + _wwp_response.MsgHeader.ResponseCode.ToString());
                      // Server Not Authorized / Others ...
                      _wait_hint = 2 * 60 * 1000;
                    }

                    if (!_msg_ok)
                    {
                      continue;
                    }

                    // Start receiving data
                    m_ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), m_ssl_client);

                    _enrolled = true;
                    _disconnect = false;

                    m_last_connected_address = _wwp_service_ipe.ToString();
                    SetStatus(WWP_PROTOCOL_STATUS.CONNECTED);
                    _num_tries = 0; // Next time will be the first try to connect.
                    _wait_hint = 0; // Wait hint on next connection
                  }
                  catch (Exception ex)
                  {
                    Log.Exception(ex);
                  }
                  finally
                  {
                    if (_disconnect)
                    {
                      if (m_ssl_client != null)
                      {
                        m_ssl_client.Disconnect();
                      }
                    }
                  }
                }
              }
              break;
          };
        }
        catch
        {
          try
          {
            m_ssl_client.Disconnect();
          }
          catch { }

          SetStatus(WWP_PROTOCOL_STATUS.UNKNOWN);
        }

      } // while (true)

    } // WwpProtocolThread

    #endregion Main Thread

    #region Send Receive Functions

    //------------------------------------------------------------------------------
    // PURPOSE : SendCallback function
    //
    //  PARAMS :
    //      - INPUT :
    //          - Result
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void SendCallback(IAsyncResult Result)
    {
      try
      {
        TCPClientSSL _ssl_client;

        m_tick_last_msg_sent = Misc.GetTickCount();

        // Not update WwpStatus: Not considered connected until it's enrolled.

        _ssl_client = (TCPClientSSL)Result.AsyncState;
        _ssl_client.EndSend(Result);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    } // SendCallback

    // PURPOSE : ReceiveCallback function
    //
    //  PARAMS :
    //      - INPUT :
    //          - Result
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void ReceiveCallback(IAsyncResult Result)
    {
      m_tick_last_msg_received = Misc.GetTickCount();
    } // ReceiveCallback
    //------------------------------------------------------------------------------

    // PURPOSE : DummyReceiveCallback function
    //
    //  PARAMS :
    //      - INPUT :
    //          - Result
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void DummyReceiveCallback(IAsyncResult Result)
    {
    } // ReceiveCallback

    private static String ReciveSyncMessage()
    {
      StringBuilder _sb;
      String _message;
      Int32 _tick_start_receiving;
      IAsyncResult _result;
      Boolean _msg_ok = false;
      byte[] _raw_response;

      _sb = new StringBuilder();

      _tick_start_receiving = Misc.GetTickCount();

      try
      {


        while (Misc.GetElapsedTicks(_tick_start_receiving) < WWP_TIMEOUT)
        {
          _result = m_ssl_client.BeginReceive(new AsyncCallback(ReceiveCallback), m_ssl_client);
          if (_result.AsyncWaitHandle.WaitOne(WWP_TIMEOUT, false))
          {
            // Enrolled
            _raw_response = m_ssl_client.EndReceive(_result);

            // AJQ 21-MAR-2013, Statistics
            m_tick_last_msg_received = Misc.GetTickCount();
            m_rcv_count += 1;
            m_rcv_bytes += (Int64)_raw_response.Length;

            _sb.Append(Encoding.UTF8.GetString(_raw_response));

            if (_sb.ToString().EndsWith("</WWP_Message>"))
            {
              //Message completed
              _msg_ok = true;
              break;
            }
          }
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_msg_ok)
      {
        _message = _sb.ToString();
      }
      else
      {
        _message = String.Empty;
        Log.Error("ReciveSyncMessage: Message not Correct");
      }

      return _message;
    } //ReciveSyncMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueue received message 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Result
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void EnqueueReceivedMessage(IAsyncResult Result)
    {
      Boolean _disconnect;
      TCPClientSSL _ssl_client;
      Byte[] _raw_response;
      String _xml_response;
      WWP_Message _wwp_response;

      // Default: Error occurred -> Disconnect
      _disconnect = true;

      try
      {
        _ssl_client = (TCPClientSSL)Result.AsyncState;
        _raw_response = _ssl_client.EndReceive(Result);

        if (_raw_response == null)
        {
          return;
        }
        if (_raw_response.Length == 0)
        {
          return;
        }

        // AJQ 21-MAR-2013, Statistics
        m_tick_last_msg_received = Misc.GetTickCount();
        m_rcv_count += 1;
        m_rcv_bytes += (Int64)_raw_response.Length;

        _xml_response = Encoding.UTF8.GetString(_raw_response);

        m_partial_msg += _xml_response;

        int _idx_etx;

        _idx_etx = m_partial_msg.IndexOf("</WWP_Message>");
        if (_idx_etx < 0)
        {
          _ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), _ssl_client);
          _disconnect = false;

          return;
        }
        _idx_etx += "</WWP_Message>".Length;

        _xml_response = m_partial_msg.Substring(0, _idx_etx);

        if (_idx_etx < m_partial_msg.Length)
        {
          m_partial_msg = m_partial_msg.Substring(_idx_etx);
        }
        else
        {
          m_partial_msg = "";
        }


        _wwp_response = WWP_Message.CreateMessage(_xml_response);

        WwpProcessMessage(_wwp_response);

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_SITE_SESSION_NOT_VALID
            && _wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID)
        {
          _ssl_client.BeginReceive(new AsyncCallback(EnqueueReceivedMessage), _ssl_client);
          _disconnect = false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_disconnect)
        {
          try
          {
            SetStatus(WWP_PROTOCOL_STATUS.DISCONNECTED);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }
      }
    } // EnqueueReceivedMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Process received message 
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpResponseMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void WwpProcessMessage(WWP_Message WwpMessage)
    {
      if (WwpMessage.MsgHeader.MsgType.ToString().IndexOf("Reply") == -1
          && WwpMessage.MsgHeader.MsgType.ToString().IndexOf("WWP_MsgError") == -1)
      {
        // Request from server
        WWP_ProcessCenterRequest.Enqueue(WwpMessage);

        return;
      }

      if (WwpMessage.MsgHeader.MsgType.ToString().IndexOf("Reply") == -1)
      {
        return;
      }

      if (WwpMessage.MsgHeader.ResponseCode == WWP_ResponseCodes.WWP_RC_SITE_SESSION_NOT_VALID
          || WwpMessage.MsgHeader.ResponseCode == WWP_ResponseCodes.WWP_RC_INVALID_SITE_SESSION_ID)
      {
        return;
      }

      if (WwpMessage.MsgHeader.MsgType == WWP_MsgTypes.WWP_MsgKeepAliveReply)
      {
        return;
      }

      // Reply to Site Service requests.
      WWP_SiteRequest.EnqueueCenterReply(WwpMessage);

      return;

    } // WwpProcessMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Get Sequence Id
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - SequenceId
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void GetSequenceId(out Int64 SequenceId)
    {
      m_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        SequenceId = m_sequence_id++;
      }
      finally
      {
        m_lock.ReleaseWriterLock();
      }

    } // GetSequenceId

    //------------------------------------------------------------------------------
    // PURPOSE : Send request message 
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpResponseMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void SendMessage(WWP_Message Message)
    {
      String _xml;
      Byte[] _raw;

      m_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        _xml = Message.ToXml();
        _raw = Encoding.UTF8.GetBytes(_xml);
        m_ssl_client.Send(_raw);
        // AJQ 21-MAR-2013, Statistics
        m_tick_last_msg_sent = Misc.GetTickCount();
        m_snd_count += 1;
        m_snd_bytes += (Int64)_raw.Length;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_lock.ReleaseWriterLock();
      }
    } // SendReply

    //------------------------------------------------------------------------------
    // PURPOSE : Send request message 
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpResponseMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void SendReply(WWP_Message MessageToSend)
    {
      SendMessage(MessageToSend);
    } // SendReply

    //------------------------------------------------------------------------------
    // PURPOSE : Send request message 
    //
    //  PARAMS :
    //      - INPUT :
    //          - WwpResponseMessage
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void SendRequest(WWP_Message MessageToSend)
    {
      MessageToSend.MsgHeader.SessionId = m_session_id;
      SendMessage(MessageToSend);
    } // SendRequest

    #endregion


    //------------------------------------------------------------------------------
    // PURPOSE : Update WWP_STATUS table
    //
    //  PARAMS :
    //      - INPUT :
    //          - ConnectionType
    //          - NewStatus
    //          - RemoteAddress
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static Boolean SaveStatusChanged(WWP_PROTOCOL_STATUS NewStatus, String RemoteAddress)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        StringBuilder _sb;
        try
        {
          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   WWP_STATUS ");
          _sb.AppendLine("   SET   WWP_STATUS         = @pNewStatus     ");
          _sb.AppendLine("       , WWP_STATUS_CHANGED = GETDATE()       ");
          _sb.AppendLine("       , WWP_LAST_ADDRESS   = CASE WHEN (@pNewStatus = @pConnected) THEN @pRemoteAddress ELSE WWP_LAST_ADDRESS END");
          _sb.AppendLine(" WHERE   WWP_TYPE           = @pConnectionType");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pConnected", SqlDbType.Int).Value = (Int32)WWP_PROTOCOL_STATUS.CONNECTED;
            _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = (Int32)((NewStatus == WWP_PROTOCOL_STATUS.CONNECTED) ? WWP_PROTOCOL_STATUS.CONNECTED : WWP_PROTOCOL_STATUS.DISCONNECTED);
            _cmd.Parameters.Add("@pRemoteAddress", SqlDbType.NVarChar).Value = RemoteAddress.Trim();
            _cmd.Parameters.Add("@pConnectionType", SqlDbType.Int).Value = (Int32)WWP_CONNECTION_TYPE.MULTISITE;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        return false;
      }
    } // SaveStatus


    //------------------------------------------------------------------------------
    // PURPOSE : Update statistics about WWP
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void SaveStats()
    {
      try
      {
        //////if (Misc.GetElapsedTicks(m_show_stats) >= 1 * 60 * 60 * 1000) // AJQ 1 hour
        //////{
        //////  StringBuilder _stats;
        //////  Decimal _bw_download;
        //////  Decimal _bw_upload;
        //////  Decimal _delta_download;
        //////  Decimal _delta_upload;

        //////  long _ellapsed;

        //////  _delta_download = (Decimal) (m_rcv_bytes - m_xrcv_bytes) / 1024m;
        //////  _delta_upload = (Decimal) (m_snd_bytes - m_xsnd_bytes) / 1024m;
        //////  _ellapsed = Misc.GetElapsedTicks(m_show_stats) / 1000;

        //////  m_xrcv_bytes = m_rcv_bytes;
        //////  m_xsnd_bytes = m_snd_bytes;


        //////  _bw_download = (Decimal)_delta_download / (Decimal)_ellapsed;
        //////  _bw_upload = (Decimal)_delta_upload / (Decimal)_ellapsed;

        //////  _stats = new StringBuilder();
        //////  _stats.AppendFormat("WWP Network usage --- Received: {0,8:F} KB, Sent: {1,8:F} KB, Download: {2,8:F} KB/s, Upload: {3,8:F} KB/s", _delta_download, _delta_upload, _bw_download, _bw_upload);
        //////  Log.Message(_stats.ToString());
        //////  m_show_stats = Misc.GetTickCount();
        //////}

        Int64 _ctrl;
        _ctrl = m_snd_bytes + m_rcv_bytes + m_snd_count + m_rcv_count;
        if (_ctrl == m_save_stats_control)
        {
          return;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          StringBuilder _sb;

          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   WWP_STATUS ");
          _sb.AppendLine("   SET   WWP_SENT_BYTES        = @pSentBytes        ");
          _sb.AppendLine("       , WWP_SENT_MESSAGES     = @pSentMessages     ");
          _sb.AppendLine("       , WWP_RECEIVED_BYTES    = @pReceivedBytes    ");
          _sb.AppendLine("       , WWP_RECEIVED_MESSAGES = @pReceivedMessages ");
          _sb.AppendLine("       , WWP_LAST_SENT_MSG     = CASE WHEN (WWP_SENT_BYTES <> @pSentBytes) THEN GETDATE() ELSE WWP_LAST_SENT_MSG END ");
          _sb.AppendLine("       , WWP_LAST_RECEIVED_MSG = CASE WHEN (WWP_RECEIVED_BYTES <> @pReceivedBytes) THEN GETDATE() ELSE WWP_LAST_RECEIVED_MSG END ");
          _sb.AppendLine(" WHERE   WWP_TYPE              = @pConnectionType");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pConnectionType", SqlDbType.Int).Value = (Int32)WWP_CONNECTION_TYPE.MULTISITE;

            _cmd.Parameters.Add("@pSentBytes", SqlDbType.BigInt).Value = m_snd_bytes;
            _cmd.Parameters.Add("@pSentMessages", SqlDbType.BigInt).Value = m_snd_count;
            _cmd.Parameters.Add("@pReceivedBytes", SqlDbType.BigInt).Value = m_rcv_bytes;
            _cmd.Parameters.Add("@pReceivedMessages", SqlDbType.BigInt).Value = m_rcv_count;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              m_save_stats_control = _ctrl;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // SaveStatus

  } // class WWP_Protocol

} // WSI.WWP_Client
