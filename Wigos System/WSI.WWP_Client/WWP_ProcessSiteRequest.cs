//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_ProcessSiteRequest.cs
// 
//   DESCRIPTION: WWP Process requests from WWP Center Service.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 26-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-FEB-2013 ACC    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using System.Threading;
using System.Collections;
using WSI.WWP_Client;
using System.Data;

namespace WSI.WWP_Client
{
  class WWP_SiteRequestTask
  {
    public WWP_Message request;
    public WWP_Message response;
    public ManualResetEvent ev_wait_response;
  }

  static public class WWP_SiteRequest
  {
    #region Constants

    private const Int32 WWP_TIMEOUT = 30000;

    #endregion

    #region Members

    private static Dictionary<Int64, Object> m_running_requests = new Dictionary<Int64, Object>();
    private static ReaderWriterLock m_lock;
    private static WaitableQueue m_incomming_messages_waitable_queue;
    private static Thread m_incomming_messages_thread;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Init.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      // Init running list
      m_running_requests = new Dictionary<Int64, Object>();

      // Init ReaderWriterLock
      m_lock = new ReaderWriterLock();

      // Create queue incomming messages
      m_incomming_messages_waitable_queue = new WaitableQueue();

      // Start Thread
      m_incomming_messages_thread = new Thread(ProcessCenterReplyThread);
      m_incomming_messages_thread.Name = "ProcessCenterReplyThread";
      m_incomming_messages_thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Process a Site Request.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WWP_Message Request
    //
    //      - OUTPUT :
    //          - WWP_Message Response
    //
    // RETURNS :
    //          - True: successful
    //          - False: Error or Timeout
    //
    static public Boolean SendAndReceive(WWP_Message Request, out WWP_Message Response)
    {
      return SendAndReceive(Request, out Response, WWP_TIMEOUT);
    }

    static public Boolean SendAndReceive(WWP_Message Request, out WWP_Message Response, Int32 TrxTimeout)
    {
      WWP_SiteRequestTask _site_request;
      Boolean _added;
      Boolean _process_ok;

      Response = WWP_Message.CreateMessage(Request.MsgHeader.MsgType + 1);

      // Check is connected 
      if (!WWP_Protocol.IsConnected())
      {
        return false;
      }

      _process_ok = false;
      _site_request = new WWP_SiteRequestTask();
      _added = false;

      try
      {
        _site_request.request = Request;
        _site_request.ev_wait_response = new ManualResetEvent(false);

        // Get Sequence Id
        WWP_Protocol.GetSequenceId(out _site_request.request.MsgHeader.SequenceId);

        m_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          m_running_requests.Add(_site_request.request.MsgHeader.SequenceId, _site_request);
        }
        finally
        {
          m_lock.ReleaseWriterLock();
        }

        _added = true;

        WWP_Protocol.SendRequest(_site_request.request);

        if (_site_request.ev_wait_response.WaitOne(TrxTimeout))
        {
          _process_ok = true;

          Response = _site_request.response;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      if (_added)
      {
        m_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          m_running_requests.Remove(_site_request.request.MsgHeader.SequenceId);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          m_lock.ReleaseWriterLock();
        }
      }

      return _process_ok;

    } // SendAndReceive

    //------------------------------------------------------------------------------
    // PURPOSE : Process a Center Reply.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WWP_Message Message received
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void EnqueueCenterReply(WWP_Message Message)
    {
      m_incomming_messages_waitable_queue.Enqueue(Message);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Process a Site Replies thread.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void ProcessCenterReplyThread()
    {
      WWP_Message _message_received;
      WWP_SiteRequestTask _site_request_running;
      Int64 _key_to_remove;

      while (true)
      {
        // Dequeue new message
        _message_received = (WWP_Message)m_incomming_messages_waitable_queue.Dequeue();

        m_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          _key_to_remove = -1; 
          foreach (Int64 _key in m_running_requests.Keys)
          {
            _site_request_running = (WWP_SiteRequestTask) m_running_requests[_key];

            if (   _key == _message_received.MsgHeader.SequenceId
                && _site_request_running.request.MsgHeader.SessionId == _message_received.MsgHeader.SessionId)
            {
              _site_request_running.response = _message_received;
              _site_request_running.ev_wait_response.Set();
              _key_to_remove = _key;

              break;
            }
          }

          if (_key_to_remove > 0)
          {
            m_running_requests.Remove(_key_to_remove);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          m_lock.ReleaseWriterLock();
        }

      } // while

    } // ProcessCenterReplyThread

    //------------------------------------------------------------------------------
    // PURPOSE : To test client requests.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void TestRequestCenter_SqlCommand()
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgSqlCommand _wwp_cmd_req;
      WWP_MsgSqlCommandReply _wwp_cmd_reply;
      StringBuilder _sb;
      DataTable _table;

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
        _wwp_cmd_req = (WWP_MsgSqlCommand)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT GP_GROUP_KEY, GP_SUBJECT_KEY, ISNULL(GP_KEY_VALUE, '') AS GP_KEY_VALUE ");
        _sb.AppendLine("     FROM GENERAL_PARAMS  ");
        _sb.AppendLine("    WHERE GP_GROUP_KEY = @p_group_key  ");

        _wwp_cmd_req.SqlCommand = new SqlCommand(_sb.ToString());
        _wwp_cmd_req.SqlCommand.Parameters.Add("@p_group_key", System.Data.SqlDbType.NVarChar).Value = "Cashier";

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Timeout or Failed!");

          return;
        }

        switch (_wwp_response.MsgHeader.ResponseCode)
        {
          case WWP_ResponseCodes.WWP_RC_OK:
            _wwp_cmd_reply = (WWP_MsgSqlCommandReply)_wwp_response.MsgContent;
            _table = _wwp_cmd_reply.DataSet.Tables[0];
            break;

          case WWP_ResponseCodes.WWP_RC_INVALID_SITE_ID:
            // ...
            break;

          default:
            // ...
            break;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

    } // TestRequestCenter_SqlCommand

  }
}
