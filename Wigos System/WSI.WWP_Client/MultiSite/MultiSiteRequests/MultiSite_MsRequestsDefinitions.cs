//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_MsRequestsDefinitions.cs
// 
//   DESCRIPTION: Ms Requests definitions.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_Client;
using WSI.WWP_SiteService.MultiSite;

namespace WSI.WWP_SiteService.MultiSite
{
  public static class MS_Request_Definitions
  {

    // TODO ... defs, enums, constants, ...

  } // static class 
} // namespace

