//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_MsRequestsDatabase.cs
// 
//   DESCRIPTION: Ms Requests database.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_Client;
using WSI.WWP_SiteService.MultiSite;

namespace WSI.WWP_SiteService.MultiSite
{
  public static class MS_Request_Database
  {

    #region Public Methods


    // PURPOSE : Get MultiSites Requests
    //           and Update Requests Status to InProgress
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId : filter
    //
    //      - OUTPUT :
    //            - RequestDataRow : DataSet that contains three tables with results: Table1 contains Requests whose Priority is 0, Table2 contains Requests whose Priority is 1, Table 3 contains Requests whose Status is In Progress
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean MsRequest_GetResponseById(Int64 UniqueId, out DataRow RequestDataRow)
    {
      StringBuilder _sb;
      DataTable _request_datatable;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialization
      _nr = 0;
      _tick0 = Environment.TickCount;
      RequestDataRow = null;
      _request_datatable = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("       SELECT    MR_UNIQUE_ID                 ");
        _sb.AppendLine("               , MR_PRIORITY                  ");
        _sb.AppendLine("               , MR_REQUEST_TYPE              ");
        _sb.AppendLine("               , MR_INPUT_DATA                ");
        _sb.AppendLine("               , MR_OUTPUT_DATA               ");
        _sb.AppendLine("               , MR_NUM_TRIES                 ");
        _sb.AppendLine("               , MR_STATUS                    ");
        _sb.AppendLine("               , MR_STATUS_CHANGED            ");
        _sb.AppendLine("         FROM    MULTISITE_REQUESTS           ");
        _sb.AppendLine("        WHERE    MR_UNIQUE_ID = @pMrUniqueId  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pMrUniqueId", SqlDbType.BigInt).Value = UniqueId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _nr = _sql_da.Fill(_request_datatable);

              if (_request_datatable.Rows.Count > 0)
              {
                RequestDataRow = _request_datatable.Rows[0];

                return true;
              }
            }
          } // SqlCommand
        } // DB_TRX
      } // Try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;

    } // MsRequest_GetResponseById

    
    // PURPOSE : Get first 500 MultiSites Requests and Update Requests Status to InProgress
    //
    //  PARAMS :
    //      - INPUT:
    //            - None
    //
    //      - OUTPUT :
    //            - RequestDataSet : DataSet that contains three tables with results: Table1 contains Requests whose Priority is 0, Table2 contains Requests whose Priority is 1, Table 3 contains Requests whose Status is In Progress
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean MultiSiteRequest_PendingToInProgress(out DataSet RequestDataSet)
    {
      StringBuilder _sb;
      String _select_top;
      Int32 _timeout;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;
      Boolean _rc;

      _rc = false;
      RequestDataSet = new DataSet();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // Initialize parameters
        _nr = 0;
        _tick0 = Environment.TickCount;
        _timeout = GeneralParam.GetInt32("MultiSite", "RequestTimeOut", 30);
        _select_top = "5000";
        try
        {
          _sb = new StringBuilder();

          // Table 0: Get MultiSites Requests whose Status has been changed for more than 30'' (RequestTimeOut)
          _sb.AppendLine(" SELECT    MR_UNIQUE_ID            ");
          _sb.AppendLine("         , MR_PRIORITY             ");
          _sb.AppendLine("         , MR_REQUEST_TYPE         ");
          _sb.AppendLine("         , MR_INPUT_DATA           ");
          _sb.AppendLine("         , MR_OUTPUT_DATA          ");
          _sb.AppendLine("         , MR_NUM_TRIES            ");
          _sb.AppendLine("         , MR_STATUS               ");
          _sb.AppendLine("         , MR_STATUS_CHANGED       ");
          _sb.AppendLine("   FROM    MULTISITE_REQUESTS      ");
          _sb.AppendLine("  WHERE    MR_STATUS_CHANGED < DATEADD(ss, -" + _timeout.ToString() + ", GETDATE());  ");  // Select all timeout requests

          // Table 1: Update and Get first 500 Requests whose Status has Pending for minus than 30'' (RequestTimeOut)
          _sb.AppendLine(" UPDATE TOP (" + _select_top + ")  MULTISITE_REQUESTS ");
          _sb.AppendLine("       SET  MR_STATUS = @pInProgressStatus  ");
          _sb.AppendLine("         ,  MR_STATUS_CHANGED  = GETDATE()  ");
          _sb.AppendLine("    OUTPUT  INSERTED.MR_UNIQUE_ID           ");
          _sb.AppendLine("         ,  INSERTED.MR_PRIORITY            ");
          _sb.AppendLine("         ,  INSERTED.MR_REQUEST_TYPE        ");
          _sb.AppendLine("         ,  INSERTED.MR_INPUT_DATA          ");
          _sb.AppendLine("         ,  INSERTED.MR_OUTPUT_DATA         ");
          _sb.AppendLine("         ,  INSERTED.MR_NUM_TRIES           ");
          _sb.AppendLine("         ,  INSERTED.MR_STATUS              ");
          _sb.AppendLine("         ,  INSERTED.MR_STATUS_CHANGED      ");
          _sb.AppendLine("     WHERE  MR_STATUS_CHANGED >= CASE WHEN MR_NUM_TRIES > 1 THEN MR_STATUS_CHANGED ");
          _sb.AppendLine("                                      ELSE DATEADD(ss, -" + _timeout.ToString() + ", GETDATE())  ");
          _sb.AppendLine("                                 END");
          _sb.AppendLine("             AND MR_STATUS = @pPendingStatus; ");
          

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pInProgressStatus", SqlDbType.Int).Value = WWP_MSRequestStatus.MS_REQUEST_STATUS_INPROGRESS;
            _sql_cmd.Parameters.Add("@pPendingStatus", SqlDbType.Int).Value = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _nr = _sql_da.Fill(RequestDataSet);

              _db_trx.Commit();
              _rc = true;
            }
          } // SqlCommand
        } // Try
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
          Log.Warning(_process_name, 10000, _nr, _tick0);
        }
      }
      return _rc;     
    } // MultiSiteRequest_PendingToInProgress

    // PURPOSE : Update MultiSite Requests table for confirmation requests
    //
    //  PARAMS :
    //      - INPUT:
    //            - RequestDataTable : DataTable with multisite requests
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    public static Boolean Ms_UpdateConfirmationsRequestsToRetried(DataTable RequestDataTable)
    {
      StringBuilder _sb;
      Int32 _nr;
      Int32 _tick0;
      String _process_name;

      // Initialization
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        // Update Requests whose status is Pending and Priority = 0
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE    MULTISITE_REQUESTS                                   ");
        _sb.AppendLine("    SET    MR_NUM_TRIES         = @pMrNumTries                  ");
        _sb.AppendLine("         , MR_STATUS            = @pMrStatus                    ");
        _sb.AppendLine("         , MR_STATUS_CHANGED    = GETDATE()                     ");
        _sb.AppendLine("  WHERE    MR_UNIQUE_ID         = @pUniqueId                    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pMrNumTries", SqlDbType.Int).SourceColumn = "MR_NUM_TRIES";
            _sql_cmd.Parameters.Add("@pMrStatus", SqlDbType.Int).SourceColumn = "MR_STATUS";
            _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "MR_UNIQUE_ID";

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.UpdateCommand = _sql_cmd;
              _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.ContinueUpdateOnError = true;

              _nr = _sql_da.Update(RequestDataTable);

              // Always Commit and return true 

              _db_trx.Commit();

              return true;
            }
          }
        } // _db_trx
      } // Try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }


      return false;
    } // Ms_UpdateConfirmationsRequestsToRetried

    // PURPOSE : Update a MultiSite Requests
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId : Response to Update
    //            - OutputData : New Value
    //            - Status : New Value
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      -  True: OK
    //      -  False: Otherwise.
    public static Boolean MsRequest_UpdateResponse(Int64 UniqueId, String OutputData, WWP_MSRequestStatus Status)
    {
      StringBuilder _sb;
      Int32 _nr;
      Int32 _tick0;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE    MULTISITE_REQUESTS                     ");
        _sb.AppendLine("    SET    MR_OUTPUT_DATA       = @pMrOutputData  ");
        _sb.AppendLine("         , MR_STATUS            = @pMrStatus      ");
        _sb.AppendLine("         , MR_STATUS_CHANGED    = CASE WHEN MR_STATUS = @pMrStatus                ");
        _sb.AppendLine("                                       THEN MR_STATUS_CHANGED ELSE GETDATE() END  ");
        _sb.AppendLine("  WHERE    MR_UNIQUE_ID         = @pUniqueId      ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pMrOutputData", SqlDbType.NVarChar).Value = OutputData;
            _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = UniqueId;
            _sql_cmd.Parameters.Add("@pMrStatus", SqlDbType.Int).Value = (Int32)Status;

            _nr = _sql_cmd.ExecuteNonQuery();
            if (_nr == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        } // _db_trx
      } // Try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // MsRequest_UpdateResponses

    // PURPOSE : Delete a Site Requests
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId: Request Id for delete
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean MsRequest_DeleteResponseById(Int64 UniqueId)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM    MULTISITE_REQUESTS            ");
        _sb.AppendLine("       WHERE    MR_UNIQUE_ID = @pMrUniqueId   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            // Add parameters
            _sql_cmd.Parameters.Add("@pMrUniqueId", SqlDbType.BigInt).Value = UniqueId;

            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);

          } //SqlCommand _sql_cmd

          _db_trx.Commit();

          return true;
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // MsRequest_DeleteResponseById

    // PURPOSE : Delete a Site Requests that satisfying the next conditions:
    //           1) NUM_TRIES value is less than 2 (0 or 1) and STATUS_CHANGED value is 1 hour old
    //              OR
    //           2) NUM_TRIES value is greater than 1 (Confirmation type) and STATUS value is OK and STATUS_CHANGED value is 1 hour old
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean MsRequest_DeleteOldRequests()
    {
      StringBuilder _sb;
      Int32 _timeout;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;
      _timeout = 60;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM    MULTISITE_REQUESTS                 ");
        _sb.AppendLine("       WHERE  (      MR_NUM_TRIES <= 1  ");
        _sb.AppendLine("                AND  MR_STATUS_CHANGED < DATEADD(ss, -" + _timeout.ToString() + ", GETDATE()) ) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);

          } //SqlCommand _sql_cmd

          _db_trx.Commit();

          return true;
        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // MsRequest_DeleteResponsesTimeout

    #endregion

    #region Private Methods
    
    #endregion

  } // static class 
} // namespace

