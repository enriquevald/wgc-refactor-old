//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_MsRequestsProcess.cs
// 
//   DESCRIPTION: Ms Requests process.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
// 03-MAY-2013 XIT    Thread's logic added
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_Client;
using WSI.WWP_SiteService.MultiSite;
using System.Threading;

namespace WSI.WWP_SiteService.MultiSite
{
  #region WorkerDefinitions

  public class MultiSiteRequestTask : Task
  {
    private DataTable m_requests_list;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    public MultiSiteRequestTask(DataTable Requests)
    {
      m_requests_list = Requests.Copy();

      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;
    }

    public override void Execute()
    {
      try
      {
        MS_Request_Process.MultiSiteRequestProcess(m_requests_list);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _tx > 10000
            || _tt > 20000)
        {
          Log.Warning("MultiSite Request Process times: Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString());
        }
      }
    }
  }

  #endregion // WorkerDefinitions

  public static class MS_Request_Process
  {
    public static WorkerPool m_worker_pool_elp;

    // TODO: 
    //   - Init: Pool de trheads & Main thread
    //   - Thread: lee y procesa peticiones hacia WWP_Center
    //

    //------------------------------------------------------------------------------
    // PURPOSE : Init Main thread & WorkerPool for site requests.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    public static void Init()
    {
      Int32 _num_workers;
      Thread _thread;
      
      // 4 workers for process multisite requests
      _num_workers = GeneralParam.GetInt32("ExternalLoyaltyProgram.Mode01", "Site.NumWorkers", 4);
      if (_num_workers == 0)
      {
        _num_workers = 4;
      }     
      m_worker_pool_elp = new WorkerPool("ELP01_SiteRequests", _num_workers);

      // Start Main Thread
      _thread = new Thread(new ThreadStart(MultiSiteRequestsThread));
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Main Thread que procesa peticiones
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    private static void MultiSiteRequestsThread()
    {
      DataSet _requests_lists;
      DataTable _requests_processing;
      Int32 _wait_hint;
      Int32 _delete_old_request_tick = Environment.TickCount - 300000;
     
      _wait_hint = 250;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 250;

          if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false))
          {
            _wait_hint = 5 * 60 * 1000; // 5 minute

            continue;
          }

          while (!WSI.WWP_Client.WWP_Protocol.IsConnected())
          {
            Thread.Sleep(_wait_hint);
            _wait_hint = 1000;
          }

          //Recupera 2 datatables(0- peticiones con mas de 30 seg ,1- peticiones con menos de 30 seg a las que se les cambia el estado
          if (!MS_Request_Database.MultiSiteRequest_PendingToInProgress(out _requests_lists))
          {
            Log.Error("MultiSiteRequest_PendingToInProgress failed.");
            _wait_hint = 5000; // 5 secconds

            continue;
          }
          //Enqueue Task in only one workerPool
          if (_requests_lists.Tables[1].Rows.Count > 0)
          {
            m_worker_pool_elp.EnqueueTask(new MultiSiteRequestTask(_requests_lists.Tables[1]));
          }      

          //Actualiza informaci�n de las pendientes [status 2] para actualizar BD apropiadamente
          if (_requests_lists.Tables[0].Rows.Count > 0)
          {
            _requests_processing = _requests_lists.Tables[0].Copy();
            //Refresca numero de intentos y elimina las peticiones de m�s de 30''
            RefreshNumTriesAndSetStatusPending(ref _requests_processing);

            if (!MS_Request_Database.Ms_UpdateConfirmationsRequestsToRetried(_requests_processing))
            {
              Log.Error("Ms_UpdateConfirmationsRequestsToRetried failed.");
              _wait_hint = 5000;

              continue;
            }
          }

          if (Misc.GetElapsedTicks(_delete_old_request_tick) > 60000) // 1 minute
          {
            if (!MS_Request_Database.MsRequest_DeleteOldRequests())
            {
              Log.Error("MsRequest_DeleteOldRequests failed.");
            }
            _delete_old_request_tick = Environment.TickCount;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Fucntion that refreshes the values of the Table an Delete task more 30sec request
    //
    //  PARAMS :
    //      - INPUT : 
    //          - InputRequests: DataTable de peticiones.   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    private static void RefreshNumTriesAndSetStatusPending(ref DataTable Table)
    {
      DataRow _dr;
      Int32 _idx;

      _idx = Table.Rows.Count - 1;
      while (_idx >= 0)
      {
        _dr = Table.Rows[_idx];

        if ((Int32)_dr["MR_NUM_TRIES"] > 0)
        {
          _dr["MR_NUM_TRIES"] = (Int32)_dr["MR_NUM_TRIES"] - 1;
        }

        if ((Int32)_dr["MR_NUM_TRIES"] > 1)
        {
          _dr["MR_STATUS"] = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;

        }

        if ((Int32)_dr["MR_NUM_TRIES"] == 0)
        {
          if (!MS_Request_Database.MsRequest_DeleteResponseById((Int64)_dr["MR_UNIQUE_ID"]))
          {
            Log.Message("MsRequest_DeleteResponseById failed. Request Unique Id: " + ((Int64)_dr["MR_UNIQUE_ID"]).ToString());
          }
          else
          {
            Table.Rows.RemoveAt(_idx);
          }
        }
        _idx--;
      }

    } // RefreshPendingInfo


    //------------------------------------------------------------------------------
    // PURPOSE : Funci�n que procesa N petici�n
    //
    //  PARAMS :
    //      - INPUT : 
    //          - InputRequests: DataTable de peticiones.   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //
    public static void MultiSiteRequestProcess(DataTable InputRequests)
    {
      WWP_ResponseCodes _response_code;

      //   - Enviar N peticiones (Trx_MultiSiteRequest)
      Trx_MultiSiteRequest(InputRequests, 0, out _response_code);
   
    } // MultiSiteRequestProcess

    #region Transactions

    //------------------------------------------------------------------------------
    // PURPOSE : MultiSite request Trx
    //
    //  PARAMS :
    //      - INPUT : 
    //          - InputRequests   
    //          - Timeout   
    //
    //      - OUTPUT :  
    //          - ResponseCode 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Timeout or failed trx (disconnect?)
    //
    private static Boolean Trx_MultiSiteRequest(DataTable InputRequests, Int32 Timeout, out WWP_ResponseCodes ResponseCode)
    {
      DataTable _request_datatable;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;

      ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR;

      try
      {
        ////_request_datatable = new DataTable();
        ////_request_datatable.Columns.Add("MR_UNIQUE_ID", System.Type.GetType("System.Int64"));
        ////_request_datatable.Columns.Add("MR_PRIORITY", System.Type.GetType("System.Int32"));
        ////_request_datatable.Columns.Add("MR_REQUEST_TYPE", System.Type.GetType("System.Int32"));
        ////_request_datatable.Columns.Add("MR_INPUT_DATA", System.Type.GetType("System.String"));

        _request_datatable = InputRequests.DefaultView.ToTable(true, "MR_UNIQUE_ID", "MR_PRIORITY", "MR_REQUEST_TYPE", "MR_INPUT_DATA");

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_MULTISITE_REQUEST;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_datatable);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response, Timeout))
        {
          if (Timeout > 0)
          {
            Log.Message("Trx_MultiSiteRequest: Timeout or Failed! TableType: TABLE_TYPE_MULTISITE_REQUEST");
          }

          return false;
        }

        ResponseCode = _wwp_response.MsgHeader.ResponseCode;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_MultiSiteRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Incomming message from multisite
    //
    //  PARAMS :
    //      - INPUT : 
    //          - RequestDataTable   
    //
    //      - OUTPUT :  
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              failed 
    //
    public static Boolean IncommingCenterRequestMessage(WWP_ResponseCodes RequestRC, DataTable InputRequests)
    {
      DataRow _dr_incomming;
      DataRow _dr_db_request;

      // Inserttar respuesta en la tabla de peticiones [multisite_requests] del site.
      // Tambien se han de gestionar reintentos.
      //
      // Datos: 
      //  (Int64)RequestDataTable.Rows[0]["SR_MR_UNIQUE_ID"]
      //  (Int32)RequestDataTable.Rows[0]["SR_REQUEST_TYPE"]
      //  (String)RequestDataTable.Rows[0]["SR_INPUT_DATA"]
      //  (String)RequestDataTable.Rows[0]["SR_OUTPUT_DATA"]

      try
      {
        //delete if status == ok && outputelp01status = ok || error || alreadypaid (si timeout y reintentos >0 no borrar y poner en pendiente)
        //update response if int.MaxValue no restar reintentos sino si;

        if (RequestRC != WWP_ResponseCodes.WWP_RC_OK)
        {
          for (Int32 _idx = 0; _idx < InputRequests.Rows.Count; _idx++)
          {
            _dr_incomming = InputRequests.Rows[_idx];

            if (!MS_Request_Database.MsRequest_GetResponseById((Int64)_dr_incomming["SR_MR_UNIQUE_ID"], out _dr_db_request))
            {
              continue;
            }

            _dr_db_request["MR_STATUS"] = WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;

            // Update de _dr_db_request.
            if (!MS_Request_Database.MsRequest_UpdateResponse((Int64)_dr_db_request["SR_MR_UNIQUE_ID"], "", (WWP_MSRequestStatus)_dr_db_request["MR_STATUS"]))
            {
              Log.Message("MS_Request_Database.MsRequest_UpdateResponses: Failed! Request Unique Id: " + ((Int64)_dr_db_request["SR_MR_UNIQUE_ID"]).ToString());

              continue;
            }
          } // for
        }
        else
        {
          for (Int32 _idx = 0; _idx < InputRequests.Rows.Count; _idx++)
          {
            _dr_incomming = InputRequests.Rows[_idx];

            if (!MS_Request_Database.MsRequest_GetResponseById((Int64)_dr_incomming["SR_MR_UNIQUE_ID"], out _dr_db_request))
            {
              continue;
            }

            _dr_db_request["MR_STATUS"] = WWP_MSRequestStatus.MS_REQUEST_STATUS_OK;
            _dr_db_request["MR_OUTPUT_DATA"] = _dr_incomming["SR_OUTPUT_DATA"];

            switch ((WWP_MultiSiteRequestType)_dr_incomming["SR_REQUEST_TYPE"])
            {
              case WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01:
                {
                  Elp01OutputData _output_data = new Elp01OutputData();

                  _output_data = new Elp01OutputData();
                  _output_data.LoadXml(_dr_incomming["SR_OUTPUT_DATA"].ToString());
             
                  if (_output_data.ResponseCodes != ELP01_ResponseCodes.ELP01_RESPONSE_CODE_TIMEOUT)
                  {
                    if (_output_data.RequestType == ELP01_Requests.ELP01_REQUEST_CONFIRM_VOUCHER_REDEEM)
                    {
                      if (_output_data.ResponseCodes != ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK)
                      {
                        String _xml_error;
                        StringBuilder _description = new StringBuilder();
                        Elp01Requests.ConvertRowToXml(_dr_incomming, true, out _xml_error);
                        _description.AppendLine(Resource.String("STR_ELP_ALARM_REDEEM_VOUCHER"));
                        _description.AppendLine(_xml_error);
                        Alarm.Register(AlarmSourceCode.Service
                                     , 0
                                     , "WWP_SITE"
                                     , AlarmCode.ELP_ErrorConfirmationRedeem
                                     , _description.ToString());
                      }
                      else
                      {
                        if (!MS_Request_Database.MsRequest_DeleteResponseById((Int64)_dr_db_request["MR_UNIQUE_ID"]))
                        {
                          Log.Message("MS_Request_Database.MsRequest_DeleteResponseById: Failed! Request Unique Id: " + ((Int64)_dr_db_request["MR_UNIQUE_ID"]).ToString());
                        }
                      }
                    }
                  }
                }
                break;

              default:
                {
                  continue;
                }
              //Unreachable code 
              //break;

            } // switch

            // Update de _dr_db_request.
            if (!MS_Request_Database.MsRequest_UpdateResponse((Int64)_dr_db_request["MR_UNIQUE_ID"], (String)_dr_db_request["MR_OUTPUT_DATA"], (WWP_MSRequestStatus)_dr_db_request["MR_STATUS"]))
            {
              continue;
            }

          } // for
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // IncommingCenterRequestMessage


    #endregion // Transactions

  } // static class 
} // namespace

