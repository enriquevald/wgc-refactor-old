//-------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   MultiSite_SiteGenericDownload.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 09-MAY-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 09-MAY-2013  MPO    Initial version
// 05-DIC-2014  DCS    Added Task Download Lcd Messages
// 21-MAR-2016 JRC     PBI: 1792 Multiple Buckets: Desactivar Acumulación de Buckets si SPACE is enabled
// 29-AUG-2016 FAV     Bug 17138: Error saving Patterns (Multisite)
// -------------------------------------------------------------------
 
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using WSI.WWP;
using System.Data;
using WSI.WWP_Client;

namespace WSI.WWP_SiteService.MultiSite
{
  public static class MultiSite_SiteGenericDownload
  {

    private const Int32 MAX_RUNNING_TASK = 300000; // 5 min.
    private const Int32 DEFAULT_MAX_ROWS = 500;

    public delegate Boolean Trx_Download(Int64 LocalSequence,
                                         out Int32 RowsUpdatedInLocal,
                                         out Int64 LastSeqInCenter,
                                         SqlTransaction SqlTrx);

    #region Members

    private static Dictionary<String, Int32> m_last_sync_upload = new Dictionary<String, Int32>();
    private static Dictionary<String, Int32> m_wait_seconds_upload = new Dictionary<String, Int32>();
    private static Boolean m_init_sync = false;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Init time wait task
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void InitTimeWaitTask()
    {
      foreach (String _task in Enum.GetNames(typeof(TYPE_MULTISITE_SITE_TASKS)))
      {

        m_last_sync_upload.Add(_task, 0);
        m_wait_seconds_upload.Add(_task, 0);

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Download task data by Sequence
    //
    //  PARAMS :
    //      - INPUT :
    //            - Task tipe
    //            - Function that know how to get and handle task's data
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean DownloadSequenceTable(TYPE_MULTISITE_SITE_TASKS SiteTask, Trx_Download Trx_DownloadAndSave)
    {
      Int64 _ellapsed;
      String _task_name;
      StringBuilder _sb;
      // Object _count_pendings;
      SequenceId _type_sequence_to_search;
      Boolean _enabled_and_running;
      Boolean _is_running;
      Int32 _wait_seconds;
      Int32 _dummy_max_rows;
      Int64 _local_sequence;
      Int64 _remote_sequence;
      DataTable _dt_center_sequences;
      Int32 _updated_in_center;
      Int64 _last_seq_in_center;
      Int32 _task_start_tick;

      if (!m_init_sync)
      {
        InitTimeWaitTask();

        m_init_sync = true;
      }

      _task_name = Enum.GetName(SiteTask.GetType(), SiteTask);
      _ellapsed = Misc.GetElapsedTicks(m_last_sync_upload[_task_name]);

      if (_ellapsed < (m_wait_seconds_upload[_task_name] * 1000))
      {
        return true;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();

          switch (SiteTask)
          {
            case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames:
              _sb.AppendLine("SELECT COUNT(*) FROM MS_SITE_PENDING_PROVIDERS_GAMES");
              _type_sequence_to_search = SequenceId.ProvidersGames;

              break;

            case TYPE_MULTISITE_SITE_TASKS.DownloadProviders:
              _type_sequence_to_search = SequenceId.Providers;

              break;
            case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments:
              _sb.AppendLine("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_DOCUMENTS");
              _type_sequence_to_search = SequenceId.AccountDocuments;

              break;



            default:
              Log.Error(String.Format("Can't recognize the task {0} when calling DownloadSequenceTable", _task_name));

              return false;
          }

          //  using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          //  {
          //    _count_pendings = _cmd.ExecuteScalar();

          //    if (_count_pendings == null && _count_pendings == DBNull.Value)
          //    {
          //      Log.Error(String.Format(" Error on read pendings, task {0}", _task_name));

          //      return false;
          //    }

          //    if ((Int32)_count_pendings > 0)
          //    {
          //      //Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

          //      return true;
          //    }
          //  }

          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, -1, out _enabled_and_running, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting_flag on task {0}", _task_name));

            return false;
          }

          if (!_enabled_and_running)
          {
            //Log.Error("Sync Personal Info not enable");

            return true;
          }

          _db_trx.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask, out _is_running, out _wait_seconds, out _local_sequence, out _dummy_max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't read reporting_flag on task {0}", _task_name));

            return false;
          }

          m_wait_seconds_upload[_task_name] = _wait_seconds;

          if (!_is_running)
          {
            return false;
          }
        } // end using

        if (!MultiSiteCommon.Trx_GetCenterLastSequences(out _dt_center_sequences))
        {
          Log.Error(String.Format(" Could not get last center sequences on task {0}. Trx_GetCenterLastSequences.", _task_name));

          return false;
        }

        _remote_sequence = -1;
        foreach (DataRow _dr_seq in _dt_center_sequences.Rows)
        {
          if ((Int32)_dr_seq[0] == (Int32)_type_sequence_to_search)
          {
            _remote_sequence = (Int64)_dr_seq[1];

            break;
          }
        }

        if (_remote_sequence < 0)
        {
          Log.Error(String.Format(" Could not get last center sequence, task {0}", _task_name));

          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _remote_sequence, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set sequence in center, task {0}", _task_name));

            return false;
          }
          _db_trx.Commit();
        }

        _task_start_tick = Misc.GetTickCount();

        while (_local_sequence <= _remote_sequence)
        {
          if (Misc.GetElapsedTicks(_task_start_tick) > MAX_RUNNING_TASK)
          {
            return true;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Trx_DownloadAndSave(_local_sequence, out _updated_in_center, out _last_seq_in_center, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Error calling Site_DownloadAndSave, task {0}", _task_name));

              return false;
            }

            if (_updated_in_center == 0)
            {
              break;
            }

            _remote_sequence = Math.Max(_remote_sequence, _last_seq_in_center);
            _local_sequence = _last_seq_in_center;

            if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _remote_sequence, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Can't set sequence in center, task {0}", _task_name));

              return false;
            }

            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, _local_sequence, out _enabled_and_running, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));

              return false;
            }

            _db_trx.Commit();

            if (!_enabled_and_running)
            {
              //Log.Error("Sync Personal Info not enable");
              return true;
            }

          } // end using

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_upload[_task_name] = Misc.GetTickCount();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _enabled_and_running, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Download task data by Timestamp
    //
    //  PARAMS :
    //      - INPUT :
    //            - Task tipe
    //            - Function that know how to get and handle task's data
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS SiteTask, Trx_Download Trx_DownloadAndSave, out Int32 UpdatedInLocal)
    {
      Int64 _ellapsed;
      String _task_name;
      Boolean _enabled_and_running;
      Boolean _is_running;
      Int32 _wait_seconds;
      Int32 _max_rows;
      Int64 _local_timestamp;
      Int32 _updated_in_local;
      Int64 _last_timestamp_in_local;
      Int32 _task_start_tick;

      if (!m_init_sync)
      {
        InitTimeWaitTask();

        m_init_sync = true;
      }

      UpdatedInLocal = -1;

      _task_name = Enum.GetName(SiteTask.GetType(), SiteTask);
      _ellapsed = Misc.GetElapsedTicks(m_last_sync_upload[_task_name]);

      if (_ellapsed < (m_wait_seconds_upload[_task_name] * 1000))
      {
        return true;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, -1, out _enabled_and_running, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting_flag on task {0}", _task_name));

            return false;
          }

          if (!_enabled_and_running)
          {
            //Log.Error("Sync Personal Info not enable");

            return true;
          }

          _db_trx.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask, out _is_running, out _wait_seconds, out _local_timestamp, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't read reporting_flag on task {0}", _task_name));

            return false;
          }

          m_wait_seconds_upload[_task_name] = _wait_seconds;

          if (!_is_running)
          {
            return false;
          }
        } // end using        

        _task_start_tick = Misc.GetTickCount();

        while (true)
        {
          if (Misc.GetElapsedTicks(_task_start_tick) > MAX_RUNNING_TASK)
          {
            return true;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Trx_DownloadAndSave(_local_timestamp, out _updated_in_local, out _last_timestamp_in_local, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Error calling Site_DownloadAndSave, task {0}", _task_name));

              return false;
            }
            //&& (_updated_in_center  > 0 || _updated_in_center < _max_rows)
            UpdatedInLocal = _updated_in_local;
            if (_updated_in_local == 0)
            {
              break;
            }

            _local_timestamp = _last_timestamp_in_local;

            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, _local_timestamp, out _enabled_and_running, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));

              return false;
            }

            _db_trx.Commit();

            if (!_enabled_and_running)
            {
              //Log.Error("Sync Personal Info not enable");
              return true;
            }

          } // end using

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_upload[_task_name] = Misc.GetTickCount();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _enabled_and_running, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return false;
    }


    #region Trx Sequence - Delegates

    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for ProviderGames Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DownloadProviderGames(Int64 LocalSequence,
                                                    out Int32 RowsUpdatedInLocal,
                                                    out Int64 LastSeqInCenter,
                                                    SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_providers_games;
      DataTable _reply_providers_games;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_providers_games = new DataTable();
      _request_providers_games.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_providers_games.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_providers_games.Rows.Add(Site.Id, LocalSequence);

      _reply_providers_games = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PROVIDERS_GAMES;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_providers_games);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DownloadProviderGames: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DownloadProviderGames Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_providers_games = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_providers_games.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_ProvidersGames ");
        _sb.AppendLine("          @pPvId ");
        _sb.AppendLine("        , @pGameId ");
        _sb.AppendLine("        , @pGameName ");
        _sb.AppendLine("        , @pPayout1 ");
        _sb.AppendLine("        , @pPayout2 ");
        _sb.AppendLine("        , @pPayout3 ");
        _sb.AppendLine("        , @pPayout4 ");
        _sb.AppendLine("        , @pPayout5 ");
        _sb.AppendLine("        , @pPayout6 ");
        _sb.AppendLine("        , @pPayout7 ");
        _sb.AppendLine("        , @pPayout8 ");
        _sb.AppendLine("        , @pPayout9 ");
        _sb.AppendLine("        , @pPayout10 ");
        _sb.AppendLine("        , @pMsSequenceId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPvId", SqlDbType.Int).SourceColumn = "GM_PV_ID";
          _cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GM_GAME_ID";
          _cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).SourceColumn = "GM_GAME_NAME";
          _cmd.Parameters.Add("@pPayout1", SqlDbType.Money).SourceColumn = "GM_PAYOUT_1";
          _cmd.Parameters.Add("@pPayout2", SqlDbType.Money).SourceColumn = "GM_PAYOUT_2";
          _cmd.Parameters.Add("@pPayout3", SqlDbType.Money).SourceColumn = "GM_PAYOUT_3";
          _cmd.Parameters.Add("@pPayout4", SqlDbType.Money).SourceColumn = "GM_PAYOUT_4";
          _cmd.Parameters.Add("@pPayout5", SqlDbType.Money).SourceColumn = "GM_PAYOUT_5";
          _cmd.Parameters.Add("@pPayout6", SqlDbType.Money).SourceColumn = "GM_PAYOUT_6";
          _cmd.Parameters.Add("@pPayout7", SqlDbType.Money).SourceColumn = "GM_PAYOUT_7";
          _cmd.Parameters.Add("@pPayout8", SqlDbType.Money).SourceColumn = "GM_PAYOUT_8";
          _cmd.Parameters.Add("@pPayout9", SqlDbType.Money).SourceColumn = "GM_PAYOUT_9";
          _cmd.Parameters.Add("@pPayout10", SqlDbType.Money).SourceColumn = "GM_PAYOUT_10";
          _cmd.Parameters.Add("@pMsSequenceId", SqlDbType.BigInt).SourceColumn = "GM_MS_SEQUENCE_ID";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_providers_games);

            if (_num_updated != _reply_providers_games.Rows.Count)
            {
              Log.Error(String.Format("DEBUG: Trx_DownloadProviderGames: Try to update {0}, Updated {1}", _reply_providers_games.Rows.Count, _num_updated));

              return false;
            }
          }

          LastSeqInCenter = (Int64)_reply_providers_games.Compute("MAX(GM_MS_SEQUENCE_ID)", "");
          RowsUpdatedInLocal = _num_updated;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for Provider Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadProviders(Int64 LocalSequence,
                                                out Int32 RowsUpdatedInLocal,
                                                out Int64 LastSeqInCenter,
                                                SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_providers;
      DataTable _reply_providers;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_providers = new DataTable();
      _request_providers.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_providers.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_providers.Rows.Add(Site.Id, LocalSequence);

      _reply_providers = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PROVIDERS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_providers);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadProviders: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadProviders Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_providers = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_providers.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_Providers ");
        _sb.AppendLine("          @pId");
        _sb.AppendLine("        , @pName");
        _sb.AppendLine("        , @pHide");
        _sb.AppendLine("        , @pPointsMultiplier");
        _sb.AppendLine("        , @p3gs");
        _sb.AppendLine("        , @p3gsVendorId");
        _sb.AppendLine("        , @p3gsVendorIp");
        _sb.AppendLine("        , @pSiteJackpot");
        _sb.AppendLine("        , @pOnlyRedeemable");
        _sb.AppendLine("        , @pMsSequenceId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).SourceColumn = "PV_ID";
          _cmd.Parameters.Add("@pName", SqlDbType.VarChar).SourceColumn = "PV_NAME";
          _cmd.Parameters.Add("@pHide", SqlDbType.Bit).SourceColumn = "PV_HIDE";
          _cmd.Parameters.Add("@pPointsMultiplier", SqlDbType.Money).SourceColumn = "PV_POINTS_MULTIPLIER";
          _cmd.Parameters.Add("@p3gs", SqlDbType.Bit).SourceColumn = "PV_3GS";
          _cmd.Parameters.Add("@p3gsVendorId", SqlDbType.NVarChar).SourceColumn = "PV_3GS_VENDOR_ID";
          _cmd.Parameters.Add("@p3gsVendorIp", SqlDbType.NVarChar).SourceColumn = "PV_3GS_VENDOR_IP";
          _cmd.Parameters.Add("@pSiteJackpot", SqlDbType.Bit).SourceColumn = "PV_SITE_JACKPOT";
          _cmd.Parameters.Add("@pOnlyRedeemable", SqlDbType.Bit).SourceColumn = "PV_ONLY_REDEEMABLE";
          _cmd.Parameters.Add("@pMsSequenceId", SqlDbType.BigInt).SourceColumn = "PV_MS_SEQUENCE_ID";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_providers);

            if (_num_updated != _reply_providers.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadProviders: Try to update {0}, Updated {1}", _reply_providers.Rows.Count, _num_updated));

              return false;
            }
          }

          LastSeqInCenter = (Int64)_reply_providers.Compute("MAX(PV_MS_SEQUENCE_ID)", "");
          RowsUpdatedInLocal = _num_updated;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for MasterProfiles Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadMasterProfiles(Int64 LocalSequence,
                                                out Int32 RowsUpdatedInLocal,
                                                out Int64 LastSeqInCenter,
                                                SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_master_profiles;
      DataTable _reply_master_profiles;
      StringBuilder _sb;
      Int32 _num_updated;
      SqlParameter _procedure_return;


      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_master_profiles = new DataTable();
      _request_master_profiles.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_master_profiles.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_master_profiles.Rows.Add(Site.Id, LocalSequence);

      _reply_master_profiles = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_MASTER_PROFILES;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_master_profiles);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadMasterProfiles: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadMasterProfiles Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_master_profiles = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_master_profiles.Rows.Count == 0)
        {
          return true;
        }
        // TODO: Hacer el delete-insert de los perfiles. Hacer por procedure.  -----
        _sb = new StringBuilder();

        /// 1- Create table tmp
        _sb.AppendLine("IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#TEMP_MASTERS_PROFILES') AND type in (N'U'))");
        _sb.AppendLine("BEGIN                                 ");
        _sb.AppendLine("  DROP TABLE #TEMP_MASTERS_PROFILES   ");
        _sb.AppendLine("END                                   ");
        _sb.AppendLine("CREATE  TABLE #TEMP_MASTERS_PROFILES  ");
        _sb.AppendLine("      ( GUP_NAME         NVARCHAR(40) ");
        _sb.AppendLine("      , GUP_MAX_USERS    INT          ");
        _sb.AppendLine("      , GUP_TIMESTAMP    BIGINT       ");
        _sb.AppendLine("      , GUP_MASTER_ID    BIGINT       ");
        _sb.AppendLine("      , GPF_GUI_ID       INT          ");
        _sb.AppendLine("      , GPF_FORM_ID      INT          ");
        _sb.AppendLine("      , GPF_READ_PERM    BIT          ");
        _sb.AppendLine("      , GPF_WRITE_PERM   BIT          ");
        _sb.AppendLine("      , GPF_DELETE_PERM  BIT          ");
        _sb.AppendLine("      , GPF_EXECUTE_PERM BIT          ");
        _sb.AppendLine("      , GF_FORM_ORDER    INT          ");
        _sb.AppendLine("      , GF_NLS_ID        INT   )      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          if (_cmd.ExecuteNonQuery() != -1)
          {
            Log.Error(String.Format("Trx_DonwloadMasterProfiles: Error creating temporal table"));

            return false;
          }
        }

        /// 2- Meter la tabla que llega en una temporal (insert into select)
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT  INTO #TEMP_MASTERS_PROFILES ");
        _sb.AppendLine("VALUES ( @pName ");
        _sb.AppendLine("       , @pMaxUsers ");
        _sb.AppendLine("       , @pTimeStamp ");
        _sb.AppendLine("       , @pMasterId ");
        _sb.AppendLine("       , @pGuiId ");
        _sb.AppendLine("       , @pFormId ");
        _sb.AppendLine("       , @pReadPerm ");
        _sb.AppendLine("       , @pWritePerm ");
        _sb.AppendLine("       , @pDeletePerm ");
        _sb.AppendLine("       , @pExecutePerm ");
        _sb.AppendLine("       , @pFormOrder ");
        _sb.AppendLine("       , @pFormNLSId ) ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 40).SourceColumn = "GUP_NAME";
          _cmd.Parameters.Add("@pMaxUsers", SqlDbType.Int).SourceColumn = "GUP_MAX_USERS";
          _cmd.Parameters.Add("@pTimeStamp", SqlDbType.BigInt).SourceColumn = "GUP_TIMESTAMP";
          _cmd.Parameters.Add("@pMasterId", SqlDbType.BigInt).SourceColumn = "GUP_MASTER_ID";
          _cmd.Parameters.Add("@pGuiId", SqlDbType.Int).SourceColumn = "GPF_GUI_ID";
          _cmd.Parameters.Add("@pFormId", SqlDbType.Int).SourceColumn = "GPF_FORM_ID";
          _cmd.Parameters.Add("@pReadPerm", SqlDbType.Bit).SourceColumn = "GPF_READ_PERM";
          _cmd.Parameters.Add("@pWritePerm", SqlDbType.Bit).SourceColumn = "GPF_WRITE_PERM";
          _cmd.Parameters.Add("@pDeletePerm", SqlDbType.Bit).SourceColumn = "GPF_DELETE_PERM";
          _cmd.Parameters.Add("@pExecutePerm", SqlDbType.Bit).SourceColumn = "GPF_EXECUTE_PERM";
          _cmd.Parameters.Add("@pFormOrder", SqlDbType.Int).SourceColumn = "GF_FORM_ORDER";
          _cmd.Parameters.Add("@pFormNLSId", SqlDbType.Int).SourceColumn = "GF_NLS_ID";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_master_profiles);

            if (_num_updated != _reply_master_profiles.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadMasterProfiles: Try to Insert {0}, Inserted {1}", _reply_master_profiles.Rows.Count, _num_updated));

              return false;
            }
          }
        }

        /// 3- Llamar al procedure de andreu
        /// 3- Drop de la tabla temporal        
        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE Update_MasterProfiles ");
        _sb.AppendLine("@pReturn OUTPUT ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _procedure_return = _cmd.Parameters.Add("@pReturn", SqlDbType.Int);
          _procedure_return.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();

          if ((Int32)_procedure_return.Value != 1)
          {
            Log.Error(String.Format("Trx_DonwloadMasterProfiles: Error in procedure Update_MasterProfiles "));

            return false;
          }

          LastSeqInCenter = (Int64)_reply_master_profiles.Compute("MAX(GUP_TIMESTAMP)", "");
          RowsUpdatedInLocal = _num_updated;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }



    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for MasterUsers Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadCorporateUsers(Int64 LocalSequence,
                                                     out Int32 RowsUpdatedInLocal,
                                                     out Int64 LastSeqInCenter,
                                                     SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_master_users;
      DataTable _reply_master_users;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_master_users = new DataTable();
      _request_master_users.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_master_users.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_master_users.Rows.Add(Site.Id, LocalSequence);

      _reply_master_users = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_CORPORATE_USERS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_master_users);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadMasterUsers: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadMasterUsers Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_master_users = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_master_users.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_MasterUsers   ");
        _sb.AppendLine("          @ProfileMasterId     ");
        _sb.AppendLine("        , @UserName            ");
        _sb.AppendLine("        , @UserEnabled         ");
        _sb.AppendLine("        , @UserPassword        ");
        _sb.AppendLine("        , @UserNotValidBefore  ");
        _sb.AppendLine("        , @UserNotValidAfter   ");
        _sb.AppendLine("        , @UserLastChanged     ");
        _sb.AppendLine("        , @UserPasswordExp     ");
        _sb.AppendLine("        , @UserPwdChgReq       ");
        _sb.AppendLine("        , @UserLoginFailures   ");
        _sb.AppendLine("        , @UserFullName        ");
        _sb.AppendLine("        , @UserTimestamp       ");
        _sb.AppendLine("        , @UserType            ");
        _sb.AppendLine("        , @UserSalesLimit      ");
        _sb.AppendLine("        , @UserMbSalesLimit    ");
        _sb.AppendLine("        , @UserBlockReason     ");
        _sb.AppendLine("        , @UserMasterId        ");
        _sb.AppendLine("        , @UserEmployeeCode    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@ProfileMasterId", SqlDbType.BigInt).SourceColumn = "GUP_MASTER_ID";
          _cmd.Parameters.Add("@UserName", SqlDbType.NVarChar, 50).SourceColumn = "GU_USERNAME";
          _cmd.Parameters.Add("@UserEnabled", SqlDbType.Bit).SourceColumn = "GU_ENABLED";
          _cmd.Parameters.Add("@UserPassword", SqlDbType.Binary, 40).SourceColumn = "GU_PASSWORD";
          _cmd.Parameters.Add("@UserNotValidBefore", SqlDbType.DateTime).SourceColumn = "GU_NOT_VALID_BEFORE";
          _cmd.Parameters.Add("@UserNotValidAfter", SqlDbType.DateTime).SourceColumn = "GU_NOT_VALID_AFTER";
          _cmd.Parameters.Add("@UserLastChanged", SqlDbType.DateTime).SourceColumn = "GU_LAST_CHANGED";
          _cmd.Parameters.Add("@UserPasswordExp", SqlDbType.DateTime).SourceColumn = "GU_PASSWORD_EXP";
          _cmd.Parameters.Add("@UserPwdChgReq", SqlDbType.Bit).SourceColumn = "GU_PWD_CHG_REQ";
          _cmd.Parameters.Add("@UserLoginFailures", SqlDbType.Int).SourceColumn = "GU_LOGIN_FAILURES";
          _cmd.Parameters.Add("@UserFullName", SqlDbType.NVarChar, 50).SourceColumn = "GU_FULL_NAME";
          _cmd.Parameters.Add("@UserTimestamp", SqlDbType.BigInt).SourceColumn = "GU_TIMESTAMP";
          _cmd.Parameters.Add("@UserType", SqlDbType.SmallInt).SourceColumn = "GU_USER_TYPE";
          _cmd.Parameters.Add("@UserSalesLimit", SqlDbType.Money).SourceColumn = "GU_SALES_LIMIT";
          _cmd.Parameters.Add("@UserMbSalesLimit", SqlDbType.Money).SourceColumn = "GU_MB_SALES_LIMIT";
          _cmd.Parameters.Add("@UserBlockReason", SqlDbType.Int).SourceColumn = "GU_BLOCK_REASON";
          _cmd.Parameters.Add("@UserMasterId", SqlDbType.Int).SourceColumn = "GU_MASTER_ID";
          _cmd.Parameters.Add("@UserEmployeeCode", SqlDbType.NVarChar, 40).SourceColumn = "GU_EMPLOYEE_CODE";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_master_users);

            if (_num_updated != _reply_master_users.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadMasterUsers: Try to Insert {0}, Inserted {1}", _reply_master_users.Rows.Count, _num_updated));

              return false;
            }
            LastSeqInCenter = (Int64)_reply_master_users.Compute("MAX(GU_TIMESTAMP)", "");
            RowsUpdatedInLocal = _num_updated;
          }

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for AccountDocuments Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadAccountDocuments(Int64 LocalSequence,
                                                       out Int32 RowsUpdatedInLocal,
                                                       out Int64 LastSeqInCenter,
                                                       SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_account_documents;
      DataTable _reply_account_documents;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_account_documents = new DataTable();
      _request_account_documents.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_account_documents.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_account_documents.Rows.Add(Site.Id, LocalSequence);

      _reply_account_documents = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_ACCOUNTS_DOCUMENTS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_account_documents);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadAccountDocuments: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadAccountDocuments: ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_account_documents = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_account_documents.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_AccountDocuments ");
        _sb.AppendLine("          @pAccountId             ");
        _sb.AppendLine("        , @pCreated               ");
        _sb.AppendLine("        , @pModified              ");
        _sb.AppendLine("        , @pData                  ");
        _sb.AppendLine("        , @pSequenceId            ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AD_ACCOUNT_ID";
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).SourceColumn = "AD_CREATED";
          _cmd.Parameters.Add("@pModified", SqlDbType.DateTime).SourceColumn = "AD_MODIFIED";
          _cmd.Parameters.Add("@pData", SqlDbType.VarBinary).SourceColumn = "AD_DATA";
          _cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).SourceColumn = "AD_MS_SEQUENCE_ID";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_account_documents);

            if (_num_updated != _reply_account_documents.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadAccountDocuments: Try to update {0}, Updated {1}", _reply_account_documents.Rows.Count, _num_updated));

              return false;
            }
          }

          LastSeqInCenter = (Int64)_reply_account_documents.Compute("MAX(AD_MS_SEQUENCE_ID)", "");
          RowsUpdatedInLocal = _num_updated;

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_DonwloadAccountDocuments


    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for AlarmPatterns Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadAlarmPatterns(Int64 LocalSequence,
                                                out Int32 RowsUpdatedInLocal,
                                                out Int64 LastSeqInCenter,
                                                SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_alarm_patterns;
      DataTable _reply_alarm_patterns;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_alarm_patterns = new DataTable();
      _request_alarm_patterns.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_alarm_patterns.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_alarm_patterns.Rows.Add(Site.Id, LocalSequence);

      _reply_alarm_patterns = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_ALARM_PATTERN;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_alarm_patterns);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadAlarmPatterns: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadAlarmPatterns Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_alarm_patterns = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_alarm_patterns.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_AlarmPatterns ");
        _sb.AppendLine("          @pId                 ");
        _sb.AppendLine("        , @pName               ");
        _sb.AppendLine("        , @pDescription        ");
        _sb.AppendLine("        , @pActive             ");
        _sb.AppendLine("        , @pPattern            ");
        _sb.AppendLine("        , @pAlCode             ");
        _sb.AppendLine("        , @pAlName             ");
        _sb.AppendLine("        , @pAlDescription      ");
        _sb.AppendLine("        , @pAlSeverity         ");
        _sb.AppendLine("        , @pType               ");
        _sb.AppendLine("        , @pSource             ");
        _sb.AppendLine("        , @pLifeTime           ");
        _sb.AppendLine("        , @pLastFind           ");
        _sb.AppendLine("        , @pDetections         ");
        _sb.AppendLine("        , @pScheduleTimeFrom   ");
        _sb.AppendLine("        , @pScheduleTimeTo     ");
        _sb.AppendLine("        , @pTimestamp          ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.BigInt).SourceColumn = "PT_ID";
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar , 50).SourceColumn = "PT_NAME";
          _cmd.Parameters.Add("@pDescription", SqlDbType.NVarChar, 350).SourceColumn = "PT_DESCRIPTION";
          _cmd.Parameters.Add("@pActive", SqlDbType.Bit).SourceColumn = "PT_ACTIVE";
          _cmd.Parameters.Add("@pPattern", SqlDbType.Xml).SourceColumn = "PT_PATTERN";
          _cmd.Parameters.Add("@pAlCode", SqlDbType.Int).SourceColumn = "PT_AL_CODE";
          _cmd.Parameters.Add("@pAlName", SqlDbType.NVarChar, 50).SourceColumn = "PT_AL_NAME";
          _cmd.Parameters.Add("@pAlDescription", SqlDbType.NVarChar, 350).SourceColumn = "PT_AL_DESCRIPTION";
          _cmd.Parameters.Add("@pAlSeverity", SqlDbType.Int).SourceColumn = "PT_AL_SEVERITY";
          _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "PT_TYPE";
          _cmd.Parameters.Add("@pSource", SqlDbType.Int).SourceColumn = "PT_SOURCE";
          _cmd.Parameters.Add("@pLifeTime", SqlDbType.Int).SourceColumn = "PT_LIFE_TIME";
          _cmd.Parameters.Add("@pLastFind", SqlDbType.DateTime).SourceColumn = "PT_LAST_FIND";
          _cmd.Parameters.Add("@pDetections", SqlDbType.Int).SourceColumn = "PT_DETECTIONS";
          _cmd.Parameters.Add("@pScheduleTimeFrom", SqlDbType.Int).SourceColumn = "PT_SCHEDULE_TIME_FROM";
          _cmd.Parameters.Add("@pScheduleTimeTo", SqlDbType.Int).SourceColumn = "PT_SCHEDULE_TIME_TO";
          _cmd.Parameters.Add("@pTimestamp", SqlDbType.BigInt).SourceColumn = "PT_TIMESTAMP";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_alarm_patterns);

            if (_num_updated != _reply_alarm_patterns.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadAlarmPatterns: Try to Insert {0}, Inserted {1}", _reply_alarm_patterns.Rows.Count, _num_updated));

              return false;
            }
            LastSeqInCenter = (Int64)_reply_alarm_patterns.Compute("MAX(PT_TIMESTAMP)", "");
            RowsUpdatedInLocal = _num_updated;
          }

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for LcdMessages Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadLcdMessages(Int64 LocalSequence,
                                                out Int32 RowsUpdatedInLocal,
                                                out Int64 LastSeqInCenter,
                                                SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_lcd_messages;
      DataTable _reply_lcd_messages;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_lcd_messages = new DataTable();
      _request_lcd_messages.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_lcd_messages.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_lcd_messages.Rows.Add(Site.Id, LocalSequence);

      _reply_lcd_messages = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_LCD_MESSAGES;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_lcd_messages);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadLcdMessages: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadLcdMessages Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_lcd_messages = _wwp_cmd_reply.DataSet.Tables[0];

        if (_reply_lcd_messages.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_LcdMessages      ");
        _sb.AppendLine("          @pUniqueId              ");
        _sb.AppendLine("        , @pType                  ");
        _sb.AppendLine("        , @pSiteList              ");
        _sb.AppendLine("        , @pTerminalList          ");
        _sb.AppendLine("        , @pAccountList           ");
        _sb.AppendLine("        , @pEnabled               ");
        _sb.AppendLine("        , @pOrder                 ");
        _sb.AppendLine("        , @pScheduleStart         ");
        _sb.AppendLine("        , @pScheduleEnd           ");
        _sb.AppendLine("        , @pScheduleWeekday       ");
        _sb.AppendLine("        , @pSchedule1TimeFrom     ");
        _sb.AppendLine("        , @pSchedule1TimeTo       ");
        _sb.AppendLine("        , @pSchedule2Enabled      ");
        _sb.AppendLine("        , @pSchedule2TimeFrom     ");
        _sb.AppendLine("        , @pSchedule2TimeTo       ");
        _sb.AppendLine("        , @pMessage               ");
        _sb.AppendLine("        , @pResourceId            ");
        _sb.AppendLine("        , @pMasterSequenceId      ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "MSG_UNIQUE_ID";
          _cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "MSG_TYPE";
          _cmd.Parameters.Add("@pSiteList", SqlDbType.Xml).SourceColumn = "MSG_SITE_LIST";
          _cmd.Parameters.Add("@pTerminalList", SqlDbType.Xml).SourceColumn = "MSG_TERMINAL_LIST";
          _cmd.Parameters.Add("@pAccountList", SqlDbType.Xml).SourceColumn = "MSG_ACCOUNT_LIST";
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).SourceColumn = "MSG_ENABLED";
          _cmd.Parameters.Add("@pOrder", SqlDbType.Int).SourceColumn = "MSG_ORDER";
          _cmd.Parameters.Add("@pScheduleStart", SqlDbType.DateTime).SourceColumn = "MSG_SCHEDULE_START";
          _cmd.Parameters.Add("@pScheduleEnd", SqlDbType.DateTime).SourceColumn = "MSG_SCHEDULE_END";
          _cmd.Parameters.Add("@pScheduleWeekday", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE_WEEKDAY";
          _cmd.Parameters.Add("@pSchedule1TimeFrom", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE1_TIME_FROM";
          _cmd.Parameters.Add("@pSchedule1TimeTo", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE1_TIME_TO";
          _cmd.Parameters.Add("@pSchedule2Enabled", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE2_ENABLED";
          _cmd.Parameters.Add("@pSchedule2TimeFrom", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE2_TIME_FROM";
          _cmd.Parameters.Add("@pSchedule2TimeTo", SqlDbType.Int).SourceColumn = "MSG_SCHEDULE2_TIME_TO";
          _cmd.Parameters.Add("@pMessage", SqlDbType.Xml).SourceColumn = "MSG_MESSAGE";
          _cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).SourceColumn = "MSG_RESOURCE_ID";
          _cmd.Parameters.Add("@pMasterSequenceId", SqlDbType.BigInt).SourceColumn = "MSG_TIMESTAMP";
          
          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_lcd_messages);

            if (_num_updated != _reply_lcd_messages.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadLcdMessages: Try to Insert {0}, Inserted {1}", _reply_lcd_messages.Rows.Count, _num_updated));

              return false;
            }
            LastSeqInCenter = (Int64)_reply_lcd_messages.Compute("MAX(MSG_TIMESTAMP)", "");
            RowsUpdatedInLocal = _num_updated;
          }

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_DonwloadLcdMessages


    //------------------------------------------------------------------------------
    // PURPOSE : Specific function to handle data for BucketConfig Tasks
    //
    //  PARAMS :
    //      - INPUT :
    //            - Site Sequence
    //            - Local updated rows
    //            - Last know center sequence
    //            - DataBase transaction object
    //      - OUTPUT :
    //            - Succes as boolean data.
    // RETURNS :
    //
    //   NOTES :
    public static Boolean Trx_DonwloadBucketsConfig(Int64 LocalSequence,
                                                out Int32 RowsUpdatedInLocal,
                                                out Int64 LastSeqInCenter,
                                                SqlTransaction SqlTrx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      DataTable _request_buckets_config;
      DataTable _reply_buckets_config;
      DataTable _reply_bucket_levels_config;
      StringBuilder _sb;
      Int32 _num_updated;

      RowsUpdatedInLocal = 0;
      LastSeqInCenter = 0;

      _request_buckets_config = new DataTable();
      _request_buckets_config.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
      _request_buckets_config.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
      _request_buckets_config.Rows.Add(Site.Id, LocalSequence);

      _reply_buckets_config = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_BUCKETS_CONFIG;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_request_buckets_config);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_DonwloadBucketConfig: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_DonwloadBucketConfig Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply_buckets_config = _wwp_cmd_reply.DataSet.Tables[0];
        _reply_bucket_levels_config = _wwp_cmd_reply.DataSet.Tables[1];

        if (_reply_buckets_config.Rows.Count == 0)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_BucketsConfig   ");
        _sb.AppendLine("         @pBucket_id		      ");
        _sb.AppendLine("        ,@pName				        ");
        _sb.AppendLine("        ,@pEnabled			      ");
        _sb.AppendLine("        ,@pSystem_type		    ");
        _sb.AppendLine("        ,@pBucket_type		    ");
        _sb.AppendLine("        ,@pVisible_flags	    ");
        _sb.AppendLine("        ,@pOrder_on_reports	  ");
        _sb.AppendLine("        ,@pExpiration_days	  ");
        _sb.AppendLine("        ,@pExpiration_date	  ");
        _sb.AppendLine("        ,@pLevel_flags		    ");
        _sb.AppendLine("        ,@pK_factor			      ");
        _sb.AppendLine("        ,@pMaster_Sequence_ID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pBucket_id", SqlDbType.BigInt).SourceColumn = "BU_BUCKET_ID";
          _cmd.Parameters.Add("@pName", SqlDbType.Text).SourceColumn = "BU_NAME";
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit ).SourceColumn = "BU_ENABLED";
          _cmd.Parameters.Add("@pSystem_type", SqlDbType.Int).SourceColumn = "BU_SYSTEM_TYPE";
          _cmd.Parameters.Add("@pBucket_type", SqlDbType.Int).SourceColumn = "BU_BUCKET_TYPE";
          _cmd.Parameters.Add("@pVisible_flags", SqlDbType.BigInt).SourceColumn = "BU_VISIBLE_FLAGS";
          _cmd.Parameters.Add("@pOrder_on_reports", SqlDbType.Int).SourceColumn = "BU_ORDER_ON_REPORTS";
          _cmd.Parameters.Add("@pExpiration_days", SqlDbType.Int).SourceColumn = "BU_EXPIRATION_DAYS";
          _cmd.Parameters.Add("@pExpiration_date", SqlDbType.Text).SourceColumn = "BU_EXPIRATION_DATE";
          _cmd.Parameters.Add("@pLevel_flags", SqlDbType.Bit).SourceColumn = "BU_LEVEL_FLAGS";
          _cmd.Parameters.Add("@pK_factor", SqlDbType.Bit).SourceColumn = "BU_K_FACTOR";
          _cmd.Parameters.Add("@pMaster_Sequence_ID", SqlDbType.Bit).SourceColumn = "BU_TIMESTAMP";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_buckets_config);

            if (_num_updated != _reply_buckets_config.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadBucketConfig: Try to Insert {0}, Inserted {1}", _reply_buckets_config.Rows.Count, _num_updated));

              return false;
            }
            LastSeqInCenter = (Int64)_reply_buckets_config.Compute("MAX(BU_TIMESTAMP)", "");
            RowsUpdatedInLocal = _num_updated;
          }
        }

        //bucket levels 

        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   Update_BucketsLevelsConfig   ");
        _sb.AppendLine("         @pBucket_id		               ");
        _sb.AppendLine("        ,@pLevel_id				             ");
        _sb.AppendLine("        ,@pA_factor			               ");
        _sb.AppendLine("        ,@pB_factor		                 ");

        using (SqlCommand _cmd2 = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd2.Parameters.Add("@pBucket_id", SqlDbType.BigInt).SourceColumn = "BUL_BUCKET_ID";
          _cmd2.Parameters.Add("@pLevel_id", SqlDbType.BigInt).SourceColumn = "BUL_LEVEL_ID";
          _cmd2.Parameters.Add("@pA_factor", SqlDbType.Money).SourceColumn = "BUL_A_FACTOR";
          _cmd2.Parameters.Add("@pB_factor", SqlDbType.Money).SourceColumn = "BUL_B_FACTOR";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd2;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            _num_updated = _sql_adapt.Update(_reply_bucket_levels_config);

            if (_num_updated != _reply_bucket_levels_config.Rows.Count)
            {
              Log.Error(String.Format("Trx_DonwloadBucketConfig Levels: Try to Insert {0}, Inserted {1}", _reply_bucket_levels_config.Rows.Count, _num_updated));

              return false;
            }
            RowsUpdatedInLocal = _num_updated;
          }
        }
        return true;

        
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_DonwloadBucketConfig



    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Call to tasks downloaded by Sequence
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    public static void DownloadAllSequenceTable()
    {
      Int32 _master_mode;

      _master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0);

      if (_master_mode == 1)//  CODERE: Only editing Multisite!
      {
        if (!MultiSite_SiteGenericDownload.DownloadSequenceTable(TYPE_MULTISITE_SITE_TASKS.DownloadProviders, MultiSite_SiteGenericDownload.Trx_DonwloadProviders))
        {
          Log.Error("Download providers failed.");
        }
        if (!MultiSite_SiteGenericDownload.DownloadSequenceTable(TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames, MultiSite_SiteGenericDownload.Trx_DownloadProviderGames))
        {
          Log.Error("Download Terminals Connected Failed.");
        }
      }

      //// TODO
      if (!MultiSite_SiteGenericDownload.DownloadSequenceTable(TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments, MultiSite_SiteGenericDownload.Trx_DonwloadAccountDocuments))
      {
        Log.Error("Download Account Documents failed.");
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Call to tasks downloaded by TimeStamp
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    // RETURNS :
    //
    public static void DownloadAllTimeStampTable()
    {
      Int32 UpdatedInLocal;
      // tables master mode ????

      if (!MultiSite_SiteGenericDownload.DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles, MultiSite_SiteGenericDownload.Trx_DonwloadMasterProfiles, out UpdatedInLocal))
      {
        Log.Error("Download Masters Profiles Failed.");
      }
      else
      {
        // UpdatedInLocal is different the zero: 
        //                -If any problem occurs in the download task.
        //                    Or more probably:
        //                -If the task doesn't download all the profiles on the maximum execution time of the task (MAX_RUNNING_TASK).
        if (UpdatedInLocal == 0)
        {
          if (!MultiSite_SiteGenericDownload.DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers, MultiSite_SiteGenericDownload.Trx_DonwloadCorporateUsers, out UpdatedInLocal))
          {
            Log.Error("Download Corporate Users Failed.");
          }
        }
      }

      if (!MultiSite_SiteGenericDownload.DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns, MultiSite_SiteGenericDownload.Trx_DonwloadAlarmPatterns, out UpdatedInLocal))
      {
        Log.Error("Download Alarms Patterns Failed.");
      }

      if (!MultiSite_SiteGenericDownload.DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages , MultiSite_SiteGenericDownload.Trx_DonwloadLcdMessages , out UpdatedInLocal))
      {
        Log.Error("Download Lcd Messages Failed.");
      }

      if ((CommonMultiSite.GetPlayerTrackingMode() == PlayerTracking_Mode.Multisite) && (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0) == 0)) //Buckets SPACE	
      {
        if (!MultiSite_SiteGenericDownload.DownloadTimeStampTable(TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig, MultiSite_SiteGenericDownload.Trx_DonwloadBucketsConfig, out UpdatedInLocal))
        {
          Log.Error("Download Buckets Config Failed.");
        }
      }
    }
  }
}


