//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSiteCommon.cs
//
//   DESCRIPTION : MultiSiteCommon class
//
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 26-FEB-2013
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-FEB-2013 JCM    First release.
// 28-MAY-2013 JML    AgroupPlaySessionPerGame move here from a separete file (and class).
// 11-JUL-2013 JML    Procedure CheckTriggersStatus changed.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using WSI.WWP;
using WSI.WWP_Client;
using System.Data.SqlClient;

namespace WSI.WWP_SiteService.MultiSite
{
  #region ENUMS

  public enum TriggersStatus
  {
    AllDisable = 0,
    AllEnabled = 1,
    Error = 2
  }

  public enum SplitOrGameType
  {
    PerGame = 0,
    ELP01Split = 1
  }

  #endregion

  class MultiSiteCommon
  {
    const Int32 COUNTER_LOG_SPLIT_OR_GROUP = 300000; // 5 min
    static DateTime m_last_running_delete_synchronize_accounts = Misc.TodayOpening().AddDays(-1);
    static Int32 m_log_SlpitOrGroup_counter = Environment.TickCount - COUNTER_LOG_SPLIT_OR_GROUP;

    //------------------------------------------------------------------------------
    // PURPOSE : Get Center Last Sequences
    //
    //  PARAMS :
    //      - INPUT : 
    //          - SiteId 
    //
    //      - OUTPUT :  
    //          - ReplyCenterSequences 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    public static Boolean Trx_GetCenterLastSequences(out DataTable ReplyCenterSequences)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;

      ReplyCenterSequences = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_GET_CENTER_SEQUENCES;
        _wwp_cmd_req.DataSet = new DataSet();

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SynchPoints: Timeout or Failed! TableType: TABLE_TYPE_GET_CENTER_SEQUENCES.");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SynchPoints Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString() + ",  TableType: TABLE_TYPE_GET_CENTER_SEQUENCES.");

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        ReplyCenterSequences = _wwp_cmd_reply.DataSet.Tables[0];

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_GetCenterLastSequences

    //------------------------------------------------------------------------------
    // PURPOSE : Delete all accounts previous from today at openning
    //
    //  PARAMS :
    //      - INPUT : 
    //          
    //
    //      - OUTPUT :  
    //          
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    public static Boolean Delete_AccountsToForceSync()
    {
      StringBuilder _sb;

      // Today already run
      if (m_last_running_delete_synchronize_accounts >= Misc.TodayOpening())
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE FROM MS_SITE_SYNCHRONIZE_ACCOUNTS WHERE SSA_TODAY < @pToday");

        using (DB_TRX _trx = new DB_TRX())
        {
          using (System.Data.SqlClient.SqlCommand _sql_cmd = new System.Data.SqlClient.SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pToday", SqlDbType.DateTime).Value = Misc.TodayOpening();
            _sql_cmd.ExecuteNonQuery();
            _trx.Commit();

            m_last_running_delete_synchronize_accounts = Misc.TodayOpening();

            return true;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check if all triggers for Multisite are enable in MultiSite Center
    //
    //  PARAMS :
    //      - INPUT : 
    //          
    //
    //      - OUTPUT :  
    //          
    //
    // RETURNS :
    //          - TriggersStatus
    //               - AllEnabled: All triggers enabled
    //               - AllDisable: All triggers disable
    //               - Error:      There are enable & disable(or not exists trigger), or procedure error.
    //
    // HELP :  TODO  -- delete this help (select) when this function is in use.
    //    SELECT   SYS.SYSOBJECTS.NAME AS TABLENAME
    //           , SYS.TRIGGERS.NAME AS TRIGGERNAME
    //           , CASE SYS.TRIGGERS.IS_DISABLED WHEN 0 THEN 'ENABLED'
    //                                     ELSE 'DISABLED' END AS STATUS
    //      FROM   SYS.TRIGGERS 
    //INNER JOIN   SYS.SYSOBJECTS ON SYS.TRIGGERS.PARENT_ID = SYS.SYSOBJECTS.ID
    //  ORDER BY   SYS.SYSOBJECTS.NAME
    public static TriggersStatus CheckTriggersStatus()
    {
      StringBuilder _sb;
      Int32 _count;
      Int32 _count_total_triggers;
      TriggersStatus _status;

      _status = TriggersStatus.Error;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   COUNT(1)                                                     ");
        _sb.AppendLine("      FROM   SYS.TRIGGERS                                                 ");
        _sb.AppendLine("INNER JOIN   SYS.SYSOBJECTS ON SYS.TRIGGERS.PARENT_ID = SYS.SYSOBJECTS.ID ");
        _sb.AppendLine("     WHERE   SYS.TRIGGERS.IS_DISABLED = 1                                 ");
        _sb.AppendLine("       AND   SYS.TRIGGERS.NAME IN (@pTrigger1, @pTrigger2, @pTrigger3)    ");
        //_sb.AppendLine("       AND   SYS.TRIGGERS.NAME like '%MultiSite%'                         ");

        using (DB_TRX _trx = new DB_TRX())
        {
          using (System.Data.SqlClient.SqlCommand _sql_cmd = new System.Data.SqlClient.SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTrigger1", SqlDbType.NVarChar).Value = "Trigger_SiteToMultiSite_Points";
            _sql_cmd.Parameters.Add("@pTrigger2", SqlDbType.NVarChar).Value = "MultiSiteTrigger_SiteAccountUpdate";
            _sql_cmd.Parameters.Add("@pTrigger3", SqlDbType.NVarChar).Value = "MultiSiteTrigger_SiteAccountDocuments";
            
            _count = (Int32)_sql_cmd.ExecuteScalar();
            _count_total_triggers = _sql_cmd.Parameters.Count;
          }
        }

        if (_count == 0)
        {
          _status = TriggersStatus.AllEnabled;
        }
        else if (_count == _count_total_triggers)
        {
          _status = TriggersStatus.AllDisable;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _status;

    } // CheckTriggersStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Call SplitOrGroupPlaySession function which fill Table elp01_play_sessions for MultiSite
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - WaitHint
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Elp01_SplitRedeemNoredeemPlaySession(out Int32 WaitHint)
    {
      WaitHint = 500;
      return SplitOrGroupPlaySession(SplitOrGameType.ELP01Split, out WaitHint);
    } // Elp01_SplitRedeemNoredeemPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Call SplitOrGroupPlaySession function which fill Table Game_Play_Sessions for MultiSite
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - WaitHint
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean GroupPlaySessionPerGame(out Int32 WaitHint)
    {
      WaitHint = 500;
      return SplitOrGroupPlaySession(SplitOrGameType.PerGame, out WaitHint);
    } // GroupPlaySessionPerGame

    //------------------------------------------------------------------------------
    // PURPOSE : Depending of "Type" parameter:
    //           a) Fill Table Game_Play_Sessions for MultiSite 
    //            OR 
    //           b) Fill Table elp01_play_sessions for MultiSite
    //
    //  PARAMS :
    //      - INPUT :
    //          - Type
    //
    //      - OUTPUT :
    //          - WaitHint
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    private static Boolean SplitOrGroupPlaySession(SplitOrGameType Type, out Int32 WaitHint)
    {
      Int32 _return_value;
      StringBuilder _sb;
      DataTable _play_sessions;
      Int64 _play_session_id;
      String _type_where;
      PendingGamePlaySessionStatus _type_MPSStatus;
      String _type_sp_execute;
      String _type_log;

      if (Type == SplitOrGameType.PerGame)
      {
        _type_where = "   AND   PS_FINISHED < DATEADD(SS, -300, GETDATE())";
        _type_sp_execute = "InsertGamePlaySessions";
        _type_log = " Error in GroupPlaySessionPerGame. Not inserted GamePlaySession of the play session. SessionId:";
        _type_MPSStatus = PendingGamePlaySessionStatus.ELP01_Split_PS_Reddem_NoRedeem;
      }
      else
      {
        _type_where = "";
        _type_MPSStatus = PendingGamePlaySessionStatus.Initial;
        _type_sp_execute = "elp01_Split_Redeem_NoRedeem_PlaySessions";
        _type_log = " Error in SP elp01_Split_Redeem_NoRedeem_PlaySessions. Play session is not splitted. SessionId:";
      }

      WaitHint = 1000;
      _sb = new StringBuilder();
      _return_value = -1;

      try
      {
        _sb.AppendLine("  SELECT TOP 100  MPS_PLAY_SESSION_ID");
        _sb.AppendLine("    FROM   MS_PENDING_GAME_PLAY_SESSIONS");
        _sb.AppendLine("   INNER   JOIN PLAY_SESSIONS ON MPS_PLAY_SESSION_ID = PS_PLAY_SESSION_ID");
        _sb.AppendLine("   WHERE   PS_STATUS  <> @StatusOpen ");
        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") != 0)
        {
          _sb.AppendLine("     AND   MPS_STATUS = @MPSStatus");
        }
        _sb.AppendLine(_type_where);
        _sb.AppendLine("ORDER BY   MPS_PLAY_SESSION_ID");

        _play_sessions = new DataTable();
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@StatusOpen", SqlDbType.Int).Value = PlaySessionStatus.Opened;
            _cmd.Parameters.Add("@MPSStatus", SqlDbType.Int).Value = _type_MPSStatus;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (!_reader.IsDBNull(0))
                {
                  _play_session_id = _reader.GetInt64(0);

                  _sb.Length = 0;
                  _sb.AppendLine("EXECUTE " + _type_sp_execute + " @pPlaySession");

                  //
                  // Transaction per play session
                  //
                  using (DB_TRX _db_trx2 = new DB_TRX())
                  {
                    using (SqlCommand _cmd2 = new SqlCommand(_sb.ToString(), _db_trx2.SqlTransaction.Connection, _db_trx2.SqlTransaction))
                    {
                      _cmd2.Parameters.Add("@pPlaySession", SqlDbType.BigInt).Value = _play_session_id;
                      try
                      {
                        _return_value = _cmd2.ExecuteNonQuery();
                      }
                      catch (Exception _ex)
                      {
                        Log.Exception(_ex);
                        try { _db_trx2.Rollback(); }catch { }
                        
                        continue;
                      }
                      if (_return_value < 0)
                      {
                        try { _db_trx2.Rollback(); }catch { }
                        if (Misc.GetElapsedTicks(m_log_SlpitOrGroup_counter) > COUNTER_LOG_SPLIT_OR_GROUP)
                        {
                          Log.Error(_type_log + _play_session_id.ToString());
                          m_log_SlpitOrGroup_counter = Environment.TickCount;
                          //System.Threading.Thread.Sleep(300000); // Sleep 5 min
                        }

                        continue;
                      }
                    }
                    _db_trx2.Commit();

                  }//using (DB_TRX
                }
              }//while
            }
          }
        }//using (DB_TRX
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      return true;
    } // SplitOrGroupPlaySession


  } // class MultiSiteCommon
}
