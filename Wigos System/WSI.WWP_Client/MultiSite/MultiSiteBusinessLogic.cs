//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSiteBussinesLogic.cs
// 
//   DESCRIPTION: Class with common operations for Multisite.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WWP_SiteService
{
  class MultiSiteBusinessLogic
  {


  } // class MultiSiteBusinessLogic
} // namespace WSI.WWP_SiteService
