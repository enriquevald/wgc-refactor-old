//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_SiteAccounts.cs
//
//   DESCRIPTION : MultiSite_SiteAccounts class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAR-2013 DDM    First release.
// 04-MAR-2013 ANG    Expand initial version.
// 09-OCT-2015 FAV    Warnings for the Log file to find errors with data track equal to NULL
// 03-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronización MultiSite: Tablas Customer Buckets
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;
using WSI.WWP_Client;
using WSI.WWP_SiteService.MultiSite;

namespace WSI.WWP_SiteService
{
  public static class MultiSiteAccounts
  {

    #region Members

    // Pending Accounts ( Upload )
    static private Int32 m_last_sync_pending_personal_info_tick = 0;
    static private Int32 m_wait_seconds_pending_personal_info = 0;

    // Download Accounts ( Download - ALL)
    static private Int32 m_last_download_account_all_tick = 0;
    static private Int32 m_wait_seconds_download_account_all = 0;

    // Download Accounts ( Download - SITE)
    static private Int32 m_last_download_account_site_tick = 0;
    static private Int32 m_wait_seconds_download_account_site = 0;

    // Force to sync account by trackdata
    //static private Int32 m_last_sync_force_sync_tick = 0;
    //static private Int32 m_wait_seconds_sync_force_sync = 0;

    #endregion

    #region ENUMS

    // TODO ENUM in MultiSiteCommon.cs ??? --> TYPE_DATA_MULTISITE_SYNC ???
    public enum TypeForSynchronizeSelect
    {
      PendingAccounts = 1,
      SynchronizeAccounts = 2
    }

    #endregion

    #region "Sync pending accounts"

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize PENDING Accounts Personal Info
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //todo: Trx o Request..?
    public static Boolean SyncPendingPersonalInfo_Upload()
    {
      DataTable _request_personal_info;
      Int64 _sequence_id;
      Boolean _reporting;
      Boolean _is_task_enabled;
      Int32 _wait_seconds;
      Int64 _ellapsed;
      Int32 _max_rows;

      _ellapsed = Misc.GetElapsedTicks(m_last_sync_pending_personal_info_tick);
      if (_ellapsed < (m_wait_seconds_pending_personal_info * 1000))
      {
        return true;
      }

      _wait_seconds = 0;

      try
      {
        while (true)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

              return false;
            }

            if (!_is_task_enabled)
            {
              //Log.Error("Sync Loyalty not enable");

              return true;
            }

            _db_trx.Commit();
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!WWP_MultiSiteTask.Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo, out _reporting, out _wait_seconds, out _sequence_id, out _max_rows, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag= true. Read_ReportingFlagValues");

              return false;
            }
            if (!_reporting)
            {
              return false;
            }

            if (!Read_AccountsToUpload(_max_rows, _db_trx.SqlTransaction, out _request_personal_info))
            {
              Log.Error(" Could not read accounts to synchronize. Read_AccountsToSynchronize");

              return false;
            }
            /// TODO si se quita bucle sin fin.

            if (!Trx_SynchPendingPersonalInfo(_request_personal_info))
            {
              Log.Error(" Could not synchronize personal info. Trx_SynchPendingPersonalInfo");

              return false;
            }

            if (_request_personal_info.Rows.Count == 0)
            {
              return true;
            }

            if (!DeletePendingUploadAccounts(_request_personal_info, _db_trx.SqlTransaction))
            {
              Log.Error("Error removing sent data. RemoveSentData()");

              return false;
            }

            _db_trx.Commit();

          } // End using
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_pending_personal_info_tick = Environment.TickCount;
        m_wait_seconds_pending_personal_info = _wait_seconds;

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Ends Reporting and set last sequence id receivied
            if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");
            }
            else
            {
              _db_trx.Commit();
            }
          } // end using
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize PENDING Personal Info
    //           Upload changes from Site to multisite
    //  PARAMS :
    //      - INPUT : Datatable RequestPersonalInfo    
    //
    //      - OUTPUT : DataTable ReplyPersonalInfo
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //todo: Trx o Request..?
    private static Boolean Trx_SynchPendingPersonalInfo(DataTable RequestPersonalInfo)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_ACCOUNT_PERSONAL_INFO;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(RequestPersonalInfo);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SynchPendingPersonalInfo : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SynchPendingPersonalInfo : Failed in MultisiteAccounts. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    #endregion

    #region "Download accounts"

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize Personal Info
    //           Download changes from Multisite to Site
    //  PARAMS :
    //      - INPUT : Datatable RequestPersonalInfo    
    //
    //      - OUTPUT : DataTable ReplyPersonalInfo
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //todo: Trx o Request..?
    public static Boolean SiteTask_DownloadAccounts(TYPE_MULTISITE_SITE_TASKS SiteTask) // SITE || ALL
    {
      DataTable _request_accounts;
      DataTable _downloaded_accounts;
      Int64 _local_sequence_value;
      Boolean _reporting;
      Boolean _enabled_and_running;
      Int32 _wait_seconds;
      Int64 _ellapsed;
      Int64 _center_sequence_value;
      DataTable _dt_center_sequences;
      Int32 _dummy_max_rows;

      _ellapsed = Misc.GetElapsedTicks(m_last_download_account_site_tick);
      if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site)
      {
        _ellapsed = Misc.GetElapsedTicks(m_last_download_account_site_tick);
        if (_ellapsed < (m_wait_seconds_download_account_site * 1000))
        {
          return true;
        }
      }
      else
      {
        _ellapsed = Misc.GetElapsedTicks(m_last_download_account_all_tick);
        if (_ellapsed < (m_wait_seconds_download_account_all * 1000))
        {
          return true;
        }
      }

      _wait_seconds = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //If ELP is disabled --> download accounts only if there is any accounts to upload
          if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 0)
          {
            if (IsThereAnyAccountToUpload(_db_trx.SqlTransaction))
            {
              // Nothing to download
              return true;
            }
          }

          // set reporting flag
          // Confirm DownlaodPersonalInfo_All or Site or Card...
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, -1, out _enabled_and_running, _db_trx.SqlTransaction))
          {
            Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

            return false;
          }

          if (!_enabled_and_running)
          {
            //Log.Error("Sync Personal Info not enable");

            return true;
          }

          _db_trx.Commit();
        } // end using

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask, out _reporting, out _wait_seconds, out _local_sequence_value, out _dummy_max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Can't set reporting_flag= true. Set_ReportingFlag");

            return false;
          }
          if (!_reporting)
          {
            return false;
          }
        } // end using

        if (!MultiSiteCommon.Trx_GetCenterLastSequences(out _dt_center_sequences))
        {
          Log.Error(" Could not get last center sequences. Trx_GetCenterLastSequences.");

          return false;
        }

        _center_sequence_value = -1;
        foreach (DataRow _dr_seq in _dt_center_sequences.Rows)
        {
          if ((Int32)_dr_seq[0] == (Int32)WSI.Common.SequenceId.TrackAccountChanges)
          {
            _center_sequence_value = (Int64)_dr_seq[1];

            break;
          }
        }

        if (_center_sequence_value < 0)
        {
          Log.Error(" Could not get last center sequence. .SyncPersonalInfo_Download()");

          return false;
        }

        if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _center_sequence_value, _db_trx.SqlTransaction))
            {
              Log.Error("WWP_MultiSiteTask.Site_SetCenterSequence(Accounts) failed!");

              return false;
            }
            _db_trx.Commit();
          }
        }

        while (_local_sequence_value <= _center_sequence_value)
        {
          _request_accounts = new DataTable();
          _request_accounts.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
          _request_accounts.Columns.Add("SEQ_ID", System.Type.GetType("System.Int64"));
          _request_accounts.Columns.Add("ACCOUNT_ID", System.Type.GetType("System.Int64"));

          switch (SiteTask)
          {
            case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site:
              _request_accounts.Rows.Add(new Object[] { Site.Id, _local_sequence_value, 0 });

              break;
            case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All:
              _request_accounts.Rows.Add(new Object[] { 0, _local_sequence_value, 0 });

              break;
          }

          if (!Trx_DownloadPersonalInfo(_request_accounts, out _downloaded_accounts))
          {
            Log.Error(" Could not synchronize personal info. Trx_SynchPersonalInfo");

            return false;
          }

          if (_downloaded_accounts.Rows.Count == 0)
          {
            break;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Site_SaveDownloadedAccounts(_downloaded_accounts, _db_trx.SqlTransaction))
            {
              Log.Error("Can't update Multisite's data.  Update_DataFromMultisiteSP");

              return false;
            }

            _local_sequence_value = (Int64)_downloaded_accounts.Rows[_downloaded_accounts.Rows.Count - 1]["AC_MS_PERSONAL_INFO_SEQ_ID"];

            if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All)
            {
              _center_sequence_value = Math.Max(_center_sequence_value, _local_sequence_value);
              if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _center_sequence_value, _db_trx.SqlTransaction))
              {
                Log.Error("WWP_MultiSiteTask.Site_SetCenterSequence(Accounts) failed!");

                return false;
              }
            }

            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, _local_sequence_value, out _enabled_and_running, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

              return false;
            }

            _db_trx.Commit();

            if (!_enabled_and_running)
            {
              //Log.Error("Sync Personal Info not enable");
              return true;
            }

          } // end using

        } // while _sequence_id < _last_center_sequence_id

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site)
        {
          m_last_download_account_site_tick = Environment.TickCount;
          m_wait_seconds_download_account_site = _wait_seconds;
        }
        else
        {
          m_last_download_account_all_tick = Environment.TickCount;
          m_wait_seconds_download_account_all = _wait_seconds;
        }

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _enabled_and_running, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");
            }
            else
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      }
      return false;
    }


    ////------------------------------------------------------------------------------
    //// PURPOSE : Synchronize Personal Info
    ////
    ////  PARAMS :
    ////      - INPUT : Datatable RequestPersonalInfo    
    ////
    ////      - OUTPUT : DataTable ReplyPersonalInfo
    ////
    //// RETURNS :
    ////          - True:               Executed succesfully
    ////          - False:              Executed unsuccesfully
    private static Boolean Trx_DownloadPersonalInfo(DataTable RequestPersonalInfo, out DataTable ReplyPersonalInfo)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;

      ReplyPersonalInfo = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PERSONAL_INFO;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(RequestPersonalInfo);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SynchPoints: Timeout or Failed! TableType: " + WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PERSONAL_INFO.ToString());

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SynchPoints Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString() + ",  TableType: " + WWP_TableTypes.TABLE_TYPE_DOWNLOAD_PERSONAL_INFO.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        ReplyPersonalInfo = _wwp_cmd_reply.DataSet.Tables[0];

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

    #region "Force to sync accounts by Trackdata"

    //////public static Boolean ForceSyncPersonalInfo_Download()
    //////{

    //////  DataTable _request_track_data;
    //////  DataTable _reply_personal_info;
    //////  DataTable _reply_personal_points;
    //////  Int64 _sequence_id;
    //////  Boolean _reporting;
    //////  Boolean _is_task_enabled;
    //////  Int32 _wait_seconds;
    //////  Int64 _ellapsed;
    //////  Int32 _max_rows;

    //////  _ellapsed = Misc.GetElapsedTicks(m_last_sync_force_sync_tick);
    //////  if (_ellapsed < (m_wait_seconds_sync_force_sync * 1000))
    //////  {
    //////    return true;
    //////  }

    //////  _wait_seconds = 0;

    //////  try
    //////  {
    //////    using (DB_TRX _db_trx = new DB_TRX())
    //////    {
    //////      if (IsThereAnyAccountToUpload(_db_trx.SqlTransaction))
    //////      {
    //////        // Nothing to download
    //////        return true;
    //////      }

    //////      // set reporting flag
    //////      if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
    //////      {
    //////        Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

    //////        return false;
    //////      }

    //////      if (!_is_task_enabled)
    //////      {
    //////        //Log.Error("Sync Personal Info not enable");

    //////        return true;
    //////      }

    //////      _db_trx.Commit();
    //////    }

    //////    using (DB_TRX _db_trx = new DB_TRX())
    //////    {
    //////      if (!WWP_MultiSiteTask.Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card, out _reporting, out _wait_seconds, out _sequence_id, out _max_rows, _db_trx.SqlTransaction))
    //////      {
    //////        Log.Error(" Can't set reporting_flag= true. Read_ReportingFlagValues");

    //////        return false;
    //////      }
    //////      if (!_reporting)
    //////      {
    //////        return false;
    //////      }

    //////      if (!Read_AccountsToForceSync(_max_rows, _db_trx.SqlTransaction, out _request_track_data))
    //////      {
    //////        Log.Error("Error reading pending trackdata to sync. Read_AccountsToForceSync()");

    //////        return false;
    //////      }

    //////      // No data to upload.
    //////      if (_request_track_data.Rows.Count == 0)
    //////      {

    //////        return true;
    //////      }

    //////      if (!Trx_ForceToSyncAccount(_request_track_data, out _reply_personal_info, out _reply_personal_points))
    //////      {
    //////        Log.Error("Trx_ForceToSyncAccount :  Could not synchronize personal info.");

    //////        return false;
    //////      }


    //////      // Apply account changes to local
    //////      if (!Site_SaveDownloadedAccounts(_reply_personal_info, _db_trx.SqlTransaction))
    //////      {
    //////        Log.Error("Can't update Multisite's data.  Update_DataFromMultisiteSP");

    //////        return false;
    //////      }

    //////      // Update account points movements.
    //////      if (!MultiSite_SitePoints.Update_PointsAccountMovements(_reply_personal_points, _db_trx.SqlTransaction))
    //////      {
    //////        Log.Error("ForceSyncPersonalInfo_Download : Could not update the account points with pending movements. Update_PointsAccountMovements");

    //////        return false;
    //////      }

    //////      _sequence_id = -1;
    //////      if (_reply_personal_info.Rows.Count > 0)
    //////      {
    //////        _sequence_id = (Int64)_reply_personal_info.Rows[_reply_personal_info.Rows.Count - 1]["AC_MS_PERSONAL_INFO_SEQ_ID"];
    //////      }

    //////      _db_trx.Commit();

    //////      return true;

    //////    }
    //////  } // End for idx
    //////  catch (Exception _ex)
    //////  {
    //////    Log.Exception(_ex);
    //////  }
    //////  finally
    //////  {

    //////    m_last_sync_force_sync_tick = Environment.TickCount;
    //////    m_wait_seconds_sync_force_sync = _wait_seconds;

    //////    try
    //////    {
    //////      using (DB_TRX _db_trx = new DB_TRX())
    //////      {
    //////        // Ends Reporting and set last sequence id receivied
    //////        if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
    //////        {
    //////          Log.Error(" Can't set reporting_flag. Set_ReportingFlag");
    //////        }
    //////        else
    //////        {
    //////          _db_trx.Commit();
    //////        }
    //////      } // end using
    //////    }
    //////    catch (Exception _ex)
    //////    {
    //////      Log.Exception(_ex);
    //////    }

    //////  }
    //////  return false;
    //////}


    ////////------------------------------------------------------------------------------
    //////// PURPOSE : Force to  Synchronize accounts by Trackdata
    ////////  PARAMS :
    ////////      - INPUT :
    ////////
    ////////      - OUTPUT :
    ////////
    //////// RETURNS :
    ////////          - True:               Executed succesfully
    ////////          - False:              Executed unsuccesfully
    ////////todo: Trx o Request..?
    //////private static Boolean Trx_ForceToSyncAccount(DataTable RequestPersonalInfo, out DataTable AccountFields, out DataTable AccountPoints)
    //////{
    //////  WWP_Message _wwp_request;
    //////  WWP_Message _wwp_response;
    //////  WWP_MsgTable _wwp_cmd_req;
    //////  WWP_MsgTableReply _wwp_reply_ds;

    //////  AccountPoints = new DataTable();
    //////  AccountFields = new DataTable();

    //////  try
    //////  {
    //////    _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
    //////    _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
    //////    _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

    //////    _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_SYNC_PERSONAL_INFO;
    //////    _wwp_cmd_req.DataSet = new DataSet();
    //////    _wwp_cmd_req.DataSet.Tables.Add(RequestPersonalInfo);


    //////    if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
    //////    {
    //////      Log.Message("Trx_SynchPendingPersonalInfo : Timeout or Failed!");

    //////      return false;
    //////    }

    //////    if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
    //////    {
    //////      Log.Error("Trx_SynchPendingPersonalInfo : Failed in MultisiteAccounts. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

    //////      return false;
    //////    }

    //////    _wwp_reply_ds = (WWP_MsgTableReply)_wwp_response.MsgContent;
    //////    AccountFields = _wwp_reply_ds.DataSet.Tables[0];
    //////    AccountPoints = _wwp_reply_ds.DataSet.Tables[1];

    //////    return true;

    //////  }
    //////  catch (Exception _ex)
    //////  {
    //////    Log.Exception(_ex);
    //////  }

    //////  return false;

    //////}

    //////public static Boolean Read_AccountsToForceSync(Int32 MaxRows, SqlTransaction SqlTrx, out DataTable TrackData)
    //////{

    //////  StringBuilder _sb;
    //////  DateTime _today;

    //////  _today = Misc.TodayOpening();

    //////  TrackData = new DataTable("TRACKDATA");

    //////  try
    //////  {
    //////    _sb = new StringBuilder();

    //////    _sb.AppendLine("SELECT TOP (@pMAXRows) SSA_TRACK_DATA ");
    //////    _sb.AppendLine("  FROM   MS_SITE_SYNCHRONIZE_ACCOUNTS ");
    //////    _sb.AppendLine("       , ACCOUNTS ");
    //////    _sb.AppendLine(" WHERE SSA_TRACK_DATA = AC_TRACK_DATA ");
    //////    _sb.AppendLine("   AND AC_ACCOUNT_ID NOT IN (SELECT SPA_ACCOUNT_ID FROM MS_SITE_PENDING_ACCOUNTS) ");

    //////    // TODO : Select account trackdata from current date
    //////    using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
    //////    {
    //////      _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRows;

    //////      using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
    //////      {
    //////        _sql_da.Fill(TrackData);

    //////        return true;
    //////      }
    //////    }
    //////  }
    //////  catch (Exception _ex)
    //////  {
    //////    Log.Exception(_ex);
    //////  }

    //////  return false;

    //////} //Read_AccountsToSync 

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Update data from multisite into  multisitemember 
    //          Stored Procedure version!
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DataTable with multisite data. ( Datatable ACCOUNTS_SYNCHRONIZE ) 
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //          - True
    //          - False
    //
    //   NOTES:
    //   May be not an insert, 
    public static Boolean Site_SaveDownloadedAccounts(DataTable DownloadedAccounts, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();
      Int32 _num_updated;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_PersonalInfo");
        _sb.AppendLine("  @pAccountId 					   ");
        _sb.AppendLine(", @pTrackData              ");
        _sb.AppendLine(", @pHolderName             ");
        _sb.AppendLine(", @pHolderId               ");
        _sb.AppendLine(", @pHolderIdType           ");
        _sb.AppendLine(", @pHolderAddress01        ");
        _sb.AppendLine(", @pHolderAddress02        ");
        _sb.AppendLine(", @pHolderAddress03        ");
        _sb.AppendLine(", @pHolderCity             ");
        _sb.AppendLine(", @pHolderZip              ");
        _sb.AppendLine(", @pHolderEmail01          ");
        _sb.AppendLine(", @pHolderEmail02          ");
        _sb.AppendLine(", @pHolderTwitter          ");
        _sb.AppendLine(", @pHolderPhoneNumber01    ");
        _sb.AppendLine(", @pHolderPhoneNumber02    ");
        _sb.AppendLine(", @pHolderComments         ");
        _sb.AppendLine(", @pHolderID1              ");
        _sb.AppendLine(", @pHolderID2              ");
        _sb.AppendLine(", @pHolderDocumentID1      ");
        _sb.AppendLine(", @pHolderDocumentID2      ");
        _sb.AppendLine(", @pHolderName1            ");
        _sb.AppendLine(", @pHolderName2            ");
        _sb.AppendLine(", @pHolderName3            ");
        _sb.AppendLine(", @pHolderGender           ");
        _sb.AppendLine(", @pHolderMaritalStatus    ");
        _sb.AppendLine(", @pHolderBirthDate        ");
        _sb.AppendLine(", @pHolderWeddingDate      ");
        _sb.AppendLine(", @pHolderLevel            ");
        _sb.AppendLine(", @pHolderLevelNotify      ");
        _sb.AppendLine(", @pHolderLevelEntered     ");
        _sb.AppendLine(", @pHolderLevelExpiration  ");
        _sb.AppendLine(", @pPin                    ");
        _sb.AppendLine(", @pPinFailures            ");
        _sb.AppendLine(", @pPinLastModified        ");
        _sb.AppendLine(", @pBlocked                ");
        _sb.AppendLine(", @pActivated              ");
        _sb.AppendLine(", @pBlockReason            ");
        _sb.AppendLine(", @pHolderIsVip            ");
        _sb.AppendLine(", @pHolderTitle            ");
        _sb.AppendLine(", @pHolderName4            ");
        _sb.AppendLine(", @pHolderPhoneType01      ");
        _sb.AppendLine(", @pHolderPhoneType02      ");
        _sb.AppendLine(", @pHolderState            ");
        _sb.AppendLine(", @pHolderCountry          ");
        _sb.AppendLine(", @pPersonalInfoSeqId      ");
        _sb.AppendLine(", @pUserType ");
        _sb.AppendLine(", @pPointsStatus           ");
        _sb.AppendLine(", @pDeposit ");
        _sb.AppendLine(", @pCardPay ");
        _sb.AppendLine(", @pBlockDescription");
        _sb.AppendLine(", @pMSHash");
        _sb.AppendLine(", @pMSCreatedOnSiteId");
        _sb.AppendLine(", @pCreated");
        _sb.AppendLine(", @pExternalReference");
        _sb.AppendLine(", @pHolderOccupation");
        _sb.AppendLine(", @pHolderExtNum");
        _sb.AppendLine(", @pHolderNationality ");
        _sb.AppendLine(", @pHolderBirthCountry ");
        _sb.AppendLine(", @pHolderFedEntity");
        _sb.AppendLine(", @pHolderId1Type ");
        _sb.AppendLine(", @pHolderId2Type ");
        _sb.AppendLine(", @pHolderId3Type ");
        _sb.AppendLine(", @pHolderId3");
        _sb.AppendLine(", @pHolderHasBeneficiary ");
        _sb.AppendLine(", @pBeneficiaryName ");
        _sb.AppendLine(", @pBeneficiaryName1");
        _sb.AppendLine(", @pBeneficiaryName2");
        _sb.AppendLine(", @pBeneficiaryName3");
        _sb.AppendLine(", @pBeneficiaryBirthDate ");
        _sb.AppendLine(", @pBeneficiaryGender ");
        _sb.AppendLine(", @pBeneficiaryOccupation ");
        _sb.AppendLine(", @pBeneficiaryId1Type ");
        _sb.AppendLine(", @pBeneficiaryId1 ");
        _sb.AppendLine(", @pBeneficiaryId2Type ");
        _sb.AppendLine(", @pBeneficiaryId2 ");
        _sb.AppendLine(", @pBeneficiaryId3Type ");
        _sb.AppendLine(", @pBeneficiaryId3 ");
        _sb.AppendLine(", @pHolderOccupationId ");
        _sb.AppendLine(", @pBeneficiaryOccupationId ");
        _sb.AppendLine(", @pPlayerTrackingMode");
        _sb.AppendLine(", @pShowCommentsOnCashier ");
        _sb.AppendLine(", @pHolderAddressCountry ");
        _sb.AppendLine(", @pHolderAddressValidation ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // Parameters
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AC_ACCOUNT_ID";
          _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).SourceColumn = "AC_TRACK_DATA";
          _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_NAME";
          _cmd.Parameters.Add("@pHolderID", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID";
          _cmd.Parameters.Add("@pHolderIdType", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID_TYPE";
          _cmd.Parameters.Add("@pHolderAddress01", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ADDRESS_01";
          _cmd.Parameters.Add("@pHolderAddress02", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ADDRESS_02";
          _cmd.Parameters.Add("@pHolderAddress03", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ADDRESS_03";
          _cmd.Parameters.Add("@pHolderCity", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_CITY";
          _cmd.Parameters.Add("@pHolderZip", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ZIP";
          _cmd.Parameters.Add("@pHolderEmail01", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_EMAIL_01";
          _cmd.Parameters.Add("@pHolderEmail02", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_EMAIL_02";
          _cmd.Parameters.Add("@pHolderTwitter", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_TWITTER_ACCOUNT";
          _cmd.Parameters.Add("@pHolderPhonenumber01", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_PHONE_NUMBER_01";
          _cmd.Parameters.Add("@pHolderPhonenumber02", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_PHONE_NUMBER_02";
          _cmd.Parameters.Add("@pHolderComments", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_COMMENTS";
          _cmd.Parameters.Add("@pHolderID1", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ID1";
          _cmd.Parameters.Add("@pHolderID2", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_ID2";
          _cmd.Parameters.Add("@pHolderDocumentID1", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_DOCUMENT_ID1";
          _cmd.Parameters.Add("@pHolderDocumentID2", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_DOCUMENT_ID2";
          _cmd.Parameters.Add("@pHolderName1", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_NAME1";
          _cmd.Parameters.Add("@pHolderName2", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_NAME2";
          _cmd.Parameters.Add("@pHolderName3", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_NAME3";
          _cmd.Parameters.Add("@pHolderGender", SqlDbType.Int).SourceColumn = "AC_HOLDER_GENDER";
          _cmd.Parameters.Add("@pHolderMaritalStatus", SqlDbType.Int).SourceColumn = "AC_HOLDER_MARITAL_STATUS";
          _cmd.Parameters.Add("@pHolderBirthdate", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_BIRTH_DATE";
          _cmd.Parameters.Add("@pHolderWeddingDate", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_WEDDING_DATE";
          _cmd.Parameters.Add("@pHolderLevel", SqlDbType.Int).SourceColumn = "AC_HOLDER_LEVEL";
          _cmd.Parameters.Add("@pHolderLevelExpiration", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_LEVEL_EXPIRATION";
          _cmd.Parameters.Add("@pHolderLevelEntered", SqlDbType.DateTime).SourceColumn = "AC_HOLDER_LEVEL_ENTERED";
          _cmd.Parameters.Add("@pHolderLevelNotify", SqlDbType.Int).SourceColumn = "AC_HOLDER_LEVEL_NOTIFY";
          _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar).SourceColumn = "AC_PIN";
          _cmd.Parameters.Add("@pPinFailures", SqlDbType.Int).SourceColumn = "AC_PIN_FAILURES";
          _cmd.Parameters.Add("@pPinLastModified", SqlDbType.DateTime).SourceColumn = "AC_PIN_LAST_MODIFIED";
          _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).SourceColumn = "AC_BLOCKED";
          _cmd.Parameters.Add("@pActivated", SqlDbType.Bit).SourceColumn = "AC_ACTIVATED";
          _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).SourceColumn = "AC_BLOCK_REASON";
          _cmd.Parameters.Add("@pHolderIsVip", SqlDbType.Bit).SourceColumn = "AC_HOLDER_IS_VIP";
          _cmd.Parameters.Add("@pHolderTitle", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_TITLE";
          _cmd.Parameters.Add("@pHolderName4", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_NAME4";
          _cmd.Parameters.Add("@pHolderPhoneType01", SqlDbType.Int).SourceColumn = "AC_HOLDER_PHONE_TYPE_01";
          _cmd.Parameters.Add("@pHolderPhoneType02", SqlDbType.Int).SourceColumn = "AC_HOLDER_PHONE_TYPE_02";
          _cmd.Parameters.Add("@pHolderState", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_STATE";
          _cmd.Parameters.Add("@pHolderCountry", SqlDbType.NVarChar).SourceColumn = "AC_HOLDER_COUNTRY";
          _cmd.Parameters.Add("@pPersonalInfoSeqId", SqlDbType.BigInt).SourceColumn = "AC_MS_PERSONAL_INFO_SEQ_ID";
          _cmd.Parameters.Add("@pUserType", SqlDbType.Int).SourceColumn = "AC_USER_TYPE";
          _cmd.Parameters.Add("@pPointsStatus", SqlDbType.Int).SourceColumn = "AC_POINTS_STATUS";
          _cmd.Parameters.Add("@pDeposit", SqlDbType.Money).SourceColumn = "AC_DEPOSIT";
          _cmd.Parameters.Add("@pCardPay", SqlDbType.Bit).SourceColumn = "AC_CARD_PAID";
          _cmd.Parameters.Add("@pBlockDescription", SqlDbType.NVarChar, 256).SourceColumn = "AC_BLOCK_DESCRIPTION";
          _cmd.Parameters.Add("@pMSHash", SqlDbType.VarBinary).SourceColumn = "AC_MS_HASH";
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).SourceColumn = "AC_CREATED";
          _cmd.Parameters.Add("@pMSCreatedOnSiteId", SqlDbType.Int).SourceColumn = "AC_MS_CREATED_ON_SITE_ID";
          _cmd.Parameters.Add("@pExternalReference", SqlDbType.NVarChar, 50).SourceColumn = "AC_EXTERNAL_REFERENCE";

          _cmd.Parameters.Add("@pHolderOccupation", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_OCCUPATION";
          _cmd.Parameters.Add("@pHolderExtNum", SqlDbType.NVarChar, 10).SourceColumn = "AC_HOLDER_EXT_NUM";
          _cmd.Parameters.Add("@pHolderNationality", SqlDbType.Int).SourceColumn = "AC_HOLDER_NATIONALITY";
          _cmd.Parameters.Add("@pHolderBirthCountry", SqlDbType.Int).SourceColumn = "AC_HOLDER_BIRTH_COUNTRY";
          _cmd.Parameters.Add("@pHolderFedEntity", SqlDbType.Int).SourceColumn = "AC_HOLDER_FED_ENTITY";
          _cmd.Parameters.Add("@pHolderId1Type", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID1_TYPE";
          _cmd.Parameters.Add("@pHolderId2Type", SqlDbType.Int).SourceColumn = "AC_HOLDER_ID2_TYPE";
          _cmd.Parameters.Add("@pHolderId3Type", SqlDbType.NVarChar, 50).SourceColumn = "AC_HOLDER_ID3_TYPE";
          _cmd.Parameters.Add("@pHolderId3", SqlDbType.NVarChar, 20).SourceColumn = "AC_HOLDER_ID3";
          _cmd.Parameters.Add("@pHolderHasBeneficiary", SqlDbType.Bit).SourceColumn = "AC_HOLDER_HAS_BENEFICIARY";
          _cmd.Parameters.Add("@pBeneficiaryName", SqlDbType.NVarChar, 200).SourceColumn = "AC_BENEFICIARY_NAME";
          _cmd.Parameters.Add("@pBeneficiaryName1", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME1";
          _cmd.Parameters.Add("@pBeneficiaryName2", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME2";
          _cmd.Parameters.Add("@pBeneficiaryName3", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_NAME3";
          _cmd.Parameters.Add("@pBeneficiaryBirthDate", SqlDbType.DateTime).SourceColumn = "AC_BENEFICIARY_BIRTH_DATE";
          _cmd.Parameters.Add("@pBeneficiaryGender", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_GENDER";
          _cmd.Parameters.Add("@pBeneficiaryOccupation", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_OCCUPATION";
          _cmd.Parameters.Add("@pBeneficiaryId1Type", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_ID1_TYPE";
          _cmd.Parameters.Add("@pBeneficiaryId1", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID1";
          _cmd.Parameters.Add("@pBeneficiaryId2Type", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_ID2_TYPE";
          _cmd.Parameters.Add("@pBeneficiaryId2", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID2";
          _cmd.Parameters.Add("@pBeneficiaryId3Type", SqlDbType.NVarChar, 50).SourceColumn = "AC_BENEFICIARY_ID3_TYPE";
          _cmd.Parameters.Add("@pBeneficiaryId3", SqlDbType.NVarChar, 20).SourceColumn = "AC_BENEFICIARY_ID3";
          _cmd.Parameters.Add("@pHolderOccupationId", SqlDbType.Int).SourceColumn = "AC_HOLDER_OCCUPATION_ID";
          _cmd.Parameters.Add("@pBeneficiaryOccupationId", SqlDbType.Int).SourceColumn = "AC_BENEFICIARY_OCCUPATION_ID";
          _cmd.Parameters.Add("@pPlayerTrackingMode", SqlDbType.Int).Value = (Int32)CommonMultiSite.GetPlayerTrackingMode();
          _cmd.Parameters.Add("@pShowCommentsOnCashier", SqlDbType.Bit).SourceColumn = "AC_SHOW_COMMENTS_ON_CASHIER";
          _cmd.Parameters.Add("@pHolderAddressCountry", SqlDbType.Int).SourceColumn = "AC_HOLDER_ADDRESS_COUNTRY";
          _cmd.Parameters.Add("@pHolderAddressValidation", SqlDbType.Int).SourceColumn = "AC_HOLDER_ADDRESS_VALIDATION";

          CheckTrackDataWithNullValue(DownloadedAccounts);

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_adapt.UpdateBatchSize = 500;
            // TODO: ES NECESARIO QUE SE ACTUALICE TODAS LAS CUENTAS?
            _num_updated = _sql_adapt.Update(DownloadedAccounts);

            if (_num_updated != DownloadedAccounts.Rows.Count)
            {
              // log..

              return false;
            }

          }
        } // SqlCommand


        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    private static void CheckTrackDataWithNullValue(DataTable DownloadedAccounts)
    {
      try
      {
        foreach (DataRow row in DownloadedAccounts.Rows)
        {
          object value = row["AC_TRACK_DATA"];
          if (value == DBNull.Value)
          {
            Log.Warning(string.Format("MultiSiteAccounts.Site_SaveDownloadedAccounts: It has detected an Account with TrackData = null. AccountID:{0} Type:{1} UserType:{2} CreatedOnSite:{3}",
              row["AC_ACCOUNT_ID"].ToString(),
              row["AC_TYPE"].ToString(),
              row["AC_USER_TYPE"] == DBNull.Value ? "NULL" : row["AC_USER_TYPE"].ToString(),
              row["AC_MS_CREATED_ON_SITE_ID"] == DBNull.Value ? "NULL" : row["AC_MS_CREATED_ON_SITE_ID"].ToString()));
          }
        }
      }
      catch { }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Count pending accounts to synchronize
    //
    //  PARAMS:
    //      - INPUT: SqlTrx  -  SqlTransaction
    //          
    //      - OUTPUT: Boolean
    //
    // RETURNS:
    //      - False: if not accounts in pending file.
    //      - True : otherwise.
    //
    // NOTE:
    //      - On error, it will return that there are pending accounts
    //
    private static Boolean IsThereAnyAccountToUpload(SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _count;

      try
      {
        _count = -1;

        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   COUNT(*) ");
        _sb.AppendLine("  FROM   MS_SITE_PENDING_ACCOUNTS ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _count = (Int32)_cmd.ExecuteScalar();
        }

        if (_count == 0)
        {
          return false;
        }

        //Log.Warning(" Site pending accounts: " + _count.ToString() + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;

    }// IsThereAnyAccountToUpload


    private static Boolean DeletePendingUploadAccounts(DataTable SentAccounts, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _rows_affected;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("IF EXISTS (SELECT   1 ");
        _sb.AppendLine("             FROM   MS_SITE_PENDING_ACCOUNTS ");
        _sb.AppendLine("            WHERE   SPA_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("              AND   SPA_GUID = @pGuid) ");
        _sb.AppendLine("    DELETE   MS_SITE_PENDING_ACCOUNTS     ");
        _sb.AppendLine("     WHERE   SPA_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("       AND   SPA_GUID = @pGuid ");
        _sb.AppendLine("  ELSE ");
        _sb.AppendLine("  BEGIN ");
        _sb.AppendLine("    UPDATE   ACCOUNTS ");
        _sb.AppendLine("       SET   AC_MS_CHANGE_GUID = NEWID() ");
        _sb.AppendLine("     WHERE   AC_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("       AND   AC_MS_CHANGE_GUID IS NULL; ");

        _sb.AppendLine("    UPDATE   MS_SITE_PENDING_ACCOUNTS ");
        _sb.AppendLine("       SET   SPA_GUID       = (SELECT AC_MS_CHANGE_GUID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId) ");
        _sb.AppendLine("     WHERE   SPA_ACCOUNT_ID = @pAccountId; ");
        _sb.AppendLine("  END ");

        SentAccounts.AcceptChanges();
        foreach (DataRow _row in SentAccounts.Rows)
        {
          _row.SetModified();
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AC_ACCOUNT_ID";
          _cmd.Parameters.Add("@pGuid", SqlDbType.UniqueIdentifier).SourceColumn = "AC_MS_CHANGE_GUID";


          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {

            _sql_da.UpdateCommand = _cmd;
            _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _rows_affected = _sql_da.Update(SentAccounts);
          }

          // Rows_affected don't match if gui change...
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Select account data 
    //
    //  PARAMS:
    //      - INPUT: TypeSelect
    //               SqlTrx  -  SqlTransaction
    //          
    //      - OUTPUT: Accounts
    //
    // RETURNS:
    //      - True : select Ok
    //      - False: otherwise.
    //
    // NOTE:
    //      Valid input parameters:
    //        - TypeSelect : enum TypeForSelect
    //
    public static Boolean Read_AccountsToUpload(Int32 MaxRows, SqlTransaction SqlTrx, out DataTable Accounts)
    {
      StringBuilder _sb;
      DateTime _today;

      _today = Misc.TodayOpening();

      Accounts = new DataTable("ACCOUNTS");
      // DDM 14-08-2013 Fixed Bug WIG-117: Set datetime to UTC. Sites with timezone differents can change the birth date
      Accounts.Columns.Add("AC_HOLDER_BIRTH_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;
      Accounts.Columns.Add("AC_BENEFICIARY_BIRTH_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;
      Accounts.Columns.Add("AC_HOLDER_WEDDING_DATE", Type.GetType("System.DateTime")).DateTimeMode = DataSetDateTime.Utc;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("         AC_ACCOUNT_ID");
        _sb.AppendLine("       , AC_TRACK_DATA");
        _sb.AppendLine("       , AC_CREATED");
        _sb.AppendLine("       , AC_HOLDER_NAME");
        _sb.AppendLine("       , AC_HOLDER_ID");
        _sb.AppendLine("       , AC_HOLDER_ID_TYPE");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_01");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_02");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_03");
        _sb.AppendLine("       , AC_HOLDER_CITY");
        _sb.AppendLine("       , AC_HOLDER_ZIP");
        _sb.AppendLine("       , AC_HOLDER_EMAIL_01");
        _sb.AppendLine("       , AC_HOLDER_EMAIL_02");
        _sb.AppendLine("       , AC_HOLDER_TWITTER_ACCOUNT");
        _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_01");
        _sb.AppendLine("       , AC_HOLDER_PHONE_NUMBER_02");
        _sb.AppendLine("       , AC_HOLDER_COMMENTS");
        _sb.AppendLine("       , AC_HOLDER_ID1");
        _sb.AppendLine("       , AC_HOLDER_ID2");
        _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID1");
        _sb.AppendLine("       , AC_HOLDER_DOCUMENT_ID2");
        _sb.AppendLine("       , AC_HOLDER_NAME1");
        _sb.AppendLine("       , AC_HOLDER_NAME2");
        _sb.AppendLine("       , AC_HOLDER_NAME3");
        _sb.AppendLine("       , AC_HOLDER_GENDER");
        _sb.AppendLine("       , AC_HOLDER_MARITAL_STATUS");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE");
        _sb.AppendLine("       , AC_HOLDER_WEDDING_DATE");
        _sb.AppendLine("       , AC_HOLDER_LEVEL");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_EXPIRATION");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_ENTERED");
        _sb.AppendLine("       , AC_HOLDER_LEVEL_NOTIFY");
        _sb.AppendLine("       , AC_PIN");
        _sb.AppendLine("       , AC_PIN_FAILURES");
        _sb.AppendLine("       , AC_PIN_LAST_MODIFIED");
        _sb.AppendLine("       , AC_BLOCKED");
        _sb.AppendLine("       , AC_ACTIVATED");
        _sb.AppendLine("       , AC_BLOCK_REASON");
        _sb.AppendLine("       , AC_HOLDER_IS_VIP");
        _sb.AppendLine("       , AC_HOLDER_TITLE");
        _sb.AppendLine("       , AC_HOLDER_NAME4");
        _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_01");
        _sb.AppendLine("       , AC_HOLDER_PHONE_TYPE_02");
        _sb.AppendLine("       , AC_HOLDER_STATE");
        _sb.AppendLine("       , AC_HOLDER_COUNTRY");
        _sb.AppendLine("       , AC_USER_TYPE");
        _sb.AppendLine("       , AC_POINTS_STATUS");
        _sb.AppendLine("       , AC_DEPOSIT");
        _sb.AppendLine("       , AC_CARD_PAID");
        _sb.AppendLine("       , AC_BLOCK_DESCRIPTION");
        _sb.AppendLine("       , AC_EXTERNAL_REFERENCE");
        _sb.AppendLine("       , AC_HOLDER_OCCUPATION "); // new fields 02/07/2013
        _sb.AppendLine("       , AC_HOLDER_EXT_NUM ");
        _sb.AppendLine("       , AC_HOLDER_NATIONALITY ");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_COUNTRY ");
        _sb.AppendLine("       , AC_HOLDER_FED_ENTITY ");
        _sb.AppendLine("       , AC_HOLDER_ID1_TYPE ");
        _sb.AppendLine("       , AC_HOLDER_ID2_TYPE ");
        _sb.AppendLine("       , AC_HOLDER_ID3_TYPE ");
        _sb.AppendLine("       , AC_HOLDER_ID3 ");
        _sb.AppendLine("       , AC_HOLDER_HAS_BENEFICIARY ");
        _sb.AppendLine("       , AC_BENEFICIARY_NAME ");
        _sb.AppendLine("       , AC_BENEFICIARY_NAME1 ");
        _sb.AppendLine("       , AC_BENEFICIARY_NAME2 ");
        _sb.AppendLine("       , AC_BENEFICIARY_NAME3 ");
        _sb.AppendLine("       , AC_BENEFICIARY_BIRTH_DATE ");
        _sb.AppendLine("       , AC_BENEFICIARY_GENDER ");
        _sb.AppendLine("       , AC_BENEFICIARY_OCCUPATION ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID1_TYPE ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID1 ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID2_TYPE ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID2 ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID3_TYPE ");
        _sb.AppendLine("       , AC_BENEFICIARY_ID3 ");
        _sb.AppendLine("       , AC_HOLDER_OCCUPATION_ID");
        _sb.AppendLine("       , AC_BENEFICIARY_OCCUPATION_ID");

        _sb.AppendLine("       , AC_MS_HAS_LOCAL_CHANGES");
        _sb.AppendLine("       , AC_MS_CHANGE_GUID");
        _sb.AppendLine("       , AC_MS_CREATED_ON_SITE_ID");
        _sb.AppendLine("       , AC_MS_MODIFIED_ON_SITE_ID");
        _sb.AppendLine("       , AC_MS_LAST_SITE_ID");
        _sb.AppendLine("       , AC_MS_POINTS_SEQ_ID");
        _sb.AppendLine("       , AC_MS_POINTS_SYNCHRONIZED");
        _sb.AppendLine("       , AC_MS_PERSONAL_INFO_SEQ_ID");
        _sb.AppendLine("       , AC_SHOW_COMMENTS_ON_CASHIER");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_COUNTRY");
        _sb.AppendLine("       , AC_HOLDER_ADDRESS_VALIDATION");
        _sb.AppendLine("  FROM   ACCOUNTS");

        _sb.AppendLine(" INNER   JOIN MS_SITE_PENDING_ACCOUNTS     ON SPA_ACCOUNT_ID = AC_ACCOUNT_ID ");

        //_sb.AppendLine(" ORDER   BY AC_ACCOUNT_ID"); // No cal ordenar

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRows;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Accounts);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Read_AccountsToUpload


  }
}
