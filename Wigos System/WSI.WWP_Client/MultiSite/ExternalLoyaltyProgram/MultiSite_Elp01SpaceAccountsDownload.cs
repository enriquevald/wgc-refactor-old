//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_Elp01SpaceAccountsDownload.cs
//
//   DESCRIPTION : MultiSite_Elp01SpaceAccountsDownload class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2013 JML    First release.
// 09-OCT-2015 FAV    Warnings for the Log file to find errors with data track equal to NULL
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
//using WSI.WWP;
//using WSI.WWP_Client;
//using WSI.WWP_SiteService.MultiSite;
//using System.Threading;
using WSI.WWP;

namespace WSI.WWP_SiteService.MultiSite
{
  class MultiSite_Elp01SpaceAccountsDownload
  {

    #region Structures

    public struct ELP01_DOWNLOADED_SPACE_ACCOUNT
    {
      public Int64 account_id;
      public String track_data;
    }

    #endregion // Structures

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Quickly Insert/Update Space Account
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Lists of Structure with minimum space account data.
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //------------------------------------------------------------------------------
    public static void CreateChangeSpaceAccount(WWP_MsgCreateAccounts.AccountList AccountList)
    {
      ELP01_DOWNLOADED_SPACE_ACCOUNT _account;

      foreach (WWP_MsgCreateAccounts.AccountItem _account_item in AccountList)
      {
        _account.account_id = _account_item.account_id;
        _account.track_data = _account_item.trackdata;

        if (!ELP01_SaveDownloadedSpaceAccount(_account))
        {
          Log.Error("ELP01_SaveDownloadedSpaceAccount. Can't create account: " + _account.account_id.ToString() + ", Track: " + _account.track_data);

          continue;
        }
      } // foreach
    } // CreateChangeSpaceAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Quickly Insert/Update Space Account
    //
    //  PARAMS:
    //      - INPUT: 
    //          - Structure with minimum space account data.
    //          - SQL Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:  - True if OK, otherwise False
    //------------------------------------------------------------------------------
    public static Boolean ELP01_SaveDownloadedSpaceAccount(ELP01_DOWNLOADED_SPACE_ACCOUNT Account)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE @OtherAccountId as BIGINT ");
        _sb.AppendLine(" DECLARE @UserTypeSiteAccount as BIT ");
        _sb.AppendLine(" DECLARE @InternalTrackData NVARCHAR(50) ");
        _sb.AppendLine(" SET @InternalTrackData = dbo.TrackDataToInternal(@pTrackData) ");

        _sb.AppendLine(" SELECT ");
        _sb.AppendLine(" 	 @OtherAccountId = AC_ACCOUNT_ID ");
        _sb.AppendLine(" 	,@UserTypeSiteAccount = AC_USER_TYPE ");
        _sb.AppendLine(" FROM ACCOUNTS WHERE AC_TRACK_DATA = @InternalTrackData ");

        _sb.AppendLine(" SET @OtherAccountId = ISNULL(@OtherAccountId, 0) ");
        _sb.AppendLine(" SET @UserTypeSiteAccount = ISNULL(@UserTypeSiteAccount, 1) ");
        
        _sb.AppendLine(" IF @OtherAccountId <> 0 AND @OtherAccountId <> @pAccountId ");
        _sb.AppendLine(" BEGIN ");
        _sb.AppendLine(" 	IF @UserTypeSiteAccount = 0 BEGIN -- IS ANONYMOUS ");
        _sb.AppendLine(" 		SET @InternalTrackData = '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50)) ");
        _sb.AppendLine(" 	END ");
        _sb.AppendLine(" 	ELSE  ");
        _sb.AppendLine(" 	BEGIN -- NOT IS ANONYMOUS ");
        _sb.AppendLine(" 		  UPDATE   ACCOUNTS    ");
        _sb.AppendLine(" 		     SET   AC_TRACK_DATA =  '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50)) ");
        _sb.AppendLine(" 		   WHERE   AC_ACCOUNT_ID = @OtherAccountId ");
        _sb.AppendLine(" 	END ");
        _sb.AppendLine(" END ");

        _sb.AppendLine(" IF NOT  EXISTS ");
        _sb.AppendLine("       ( SELECT   1 ");
        _sb.AppendLine("           FROM   ACCOUNTS ");
        _sb.AppendLine("          WHERE   AC_ACCOUNT_ID = @pAccountId) ");
        _sb.AppendLine("     BEGIN ");
        _sb.AppendLine("         INSERT   INTO ACCOUNTS ");
        _sb.AppendLine("                ( AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA, AC_USER_TYPE, AC_HOLDER_LEVEL) ");
        _sb.AppendLine("         VALUES (   @pAccountId,   @pHolderName,       2,  @pBlocked,   @InternalTrackData, 1, 1) ");
        _sb.AppendLine("     END ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine("    BEGIN ");
        _sb.AppendLine("         UPDATE   ACCOUNTS ");
        _sb.AppendLine("            SET   AC_TRACK_DATA                = @InternalTrackData ");
        _sb.AppendLine("                , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID ");  // -- avoid trigger on update 
        _sb.AppendLine("          WHERE   AC_ACCOUNT_ID                = @pAccountId       ");
        _sb.AppendLine(" END ");

        if (Account.track_data == null)
        {
          Log.Warning(string.Format("MultiSite_Elp01SpaceAccountsDownload.ELP01_SaveDownloadedSpaceAccount: It has detected an Account with TrackData = null. AccountID:{0}", Account.account_id));
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Account.account_id;
            _sql_cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 200).Value = ELP01_DownloadSpaceAccount.HOLDER_NAME;
            _sql_cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = 0;
            _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50).Value = Account.track_data;

            if (_sql_cmd.ExecuteNonQuery() == 0)
            {
              return false;
            }
          } // Using SqlCommand
          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion // Public Methods
 
  }  // class
} // namespace
