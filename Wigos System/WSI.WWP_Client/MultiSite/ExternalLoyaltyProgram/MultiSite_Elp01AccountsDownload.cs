//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_Elp01AccountsDownload.cs
//
//   DESCRIPTION : MultiSite_Elp01AccountsDownload class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2013 JML    First release.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;


using System.Data;
using System.Data.SqlClient;
using WSI.Common;
//using WSI.WWP;
//using WSI.WWP_Client;
//using WSI.WWP_SiteService.MultiSite;
//using System.Threading;


namespace WSI.WWP_SiteService.MultiSite
{
  class MultiSite_Elp01AccountsDownload
  {

    #region Structures

    public struct ELP01_DOWNLOAD_ACCOUNT
    {
      public Int64 account_id;
      public String track_data;
    }

    #endregion

    #region Constants

    public const String HOLDER_NAME = "Temporary Space Name";

    #endregion
    
    #region Public Methods

    public static Boolean CreateSpaceAccount(ELP01_DOWNLOAD_ACCOUNT Account)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Site_SaveDownloadedAccounts(Account, _db_trx.SqlTransaction))
        {
          Log.Error("Site_SaveDownloadedAccounts. Can't created account.");

          return false;
        }

       _db_trx.Commit();

        return true;
      } // end using
    }

       //------------------------------------------------------------------------------
    // PURPOSE: Update data from multisite into  multisitemember 
    //          Stored Procedure version!
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DataTable with multisite data. ( Datatable ACCOUNTS_SYNCHRONIZE ) 
    //
    //      - OUTPUT:
    //          - 
    //
    // RETURNS:
    //          - True
    //          - False
    //
    //   NOTES:
    //   May be not an insert, 
    public static Boolean Site_SaveDownloadedAccounts(ELP01_DOWNLOAD_ACCOUNT Account, SqlTransaction SqlTrx)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();
          
        _sb.AppendLine(" INSERT   INTO ACCOUNTS ");
        _sb.AppendLine("        ( AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) ");
        _sb.AppendLine(" VALUES (   @pAccountId,   @pHolderName,       2,  @pBlocked,   @pTrackData) ");
        //_sb.AppendLine(" ");
        //_sb.AppendLine(" UPDATE   ACCOUNTS                                                                      ");
        //_sb.AppendLine("    SET   AC_TRACK_DATA                = @pTrackData                                    ");
        //_sb.AppendLine("        , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID -- avoid trigger on update   ");
        //_sb.AppendLine("  WHERE   AC_ACCOUNT_ID                = @pAccountId                                    ");
        //_sb.AppendLine(" ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Account.account_id;
          _sql_cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 200).Value = HOLDER_NAME;
          _sql_cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = 0;
          _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50).Value = Account.track_data;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Warning("UpdateElp01TrackData failed. AccountId: " + Account.account_id.ToString());

            return false;
          }
        } // SqlCommand

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion // Public Methods
 
  }  // class
} // namespace
