//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   MultiSite_SiteGenericUpload.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 17-APR-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 17-APR-2013  MPO    Initial version
// 27-FEB-2014  JML    ADD Cashier Sesions Gruped By Hour
// 14-MAR-2014  JML    WIG-735 solved
// 22-JUL-2014  AMF    Tito Columns.
// 14-JAN-2015  DCS    Added Task Upload Handpays
// 17-MAR-2015  ANM    Added Task Upload Sells & buys
// 27-MAR-2015  FOS    Added fields in terminals and terminals_connected tables
// 24-APR-2015  MPO    TFS-1258: Improve performance for timestamp tasks
// 05-APR-2016  RAB    PBI 10787 (Phase 1): GameTable: Historification of the movements of box (MultiSite)
// 31-MAR-2017  MS     WIGOS-411 Creditlines
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using WSI.WWP;
using WSI.WWP_Client;

namespace WSI.WWP_SiteService.MultiSite
{
  public static class MultiSite_SiteGenericUpload
  {

    private const Int32 MAX_RUNNING_TASK = 300000; // 5 min.
    private const Int32 DEFAULT_MAX_ROWS = 500;

    public delegate Boolean Trx_Upload(Int64 FromSequence,
                                       Int64 MaxRowsToUpload,
                                       out Int64 ToSequence,
                                       out Int32 RowsUpdatedInCenter,
                                       SqlTransaction SqlTrx);

    public delegate Boolean GetMaxLocalSequence(Int64 RemoteSequence,
                                                out Int64 MaxSequence,
                                                out Int32 Count,
                                                SqlTransaction SqlTrx);

    public delegate Boolean Trx_UploadPendings(Int32 MaxRowsToUpload,
                                               out Int32 RowsUpdatedInCenter,
                                               SqlTransaction SqlTrx);

    #region Members

    private static Dictionary<String, Int32> m_last_sync_upload = new Dictionary<String, Int32>();
    private static Dictionary<String, Int32> m_wait_seconds_upload = new Dictionary<String, Int32>();
    private static Boolean m_init_sync = false;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Init time wait task
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void InitTimeWaitTask()
    {
      foreach (String _task in Enum.GetNames(typeof(TYPE_MULTISITE_SITE_TASKS)))
      {

        m_last_sync_upload.Add(_task, 0);
        m_wait_seconds_upload.Add(_task, 0);

      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE :Generic upload
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteTask: 
    //          - GetMaxSequence: 
    //          - TrxSync: 
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS SiteTask, Trx_Upload TrxSync)
    {
      Int32 _max_rows = DEFAULT_MAX_ROWS;
      Int64 _local_sequence;
      Boolean _is_task_enabled;
      Boolean _is_task_running;
      Int64 _remote_sequence;
      Int32 _task_start_tick;
      Int64 _max_local_sequence;
      Int64 _ellapsed;
      String _task_name;
      Int32 _wait;
      Int64 _remote_sequence_commited;
      Int32 _rows_updated;
      Int32 _total_reg_to_send;

      if (!m_init_sync)
      {
        InitTimeWaitTask();

        m_init_sync = true;
      }

      _task_name = Enum.GetName(SiteTask.GetType(), SiteTask);
      _ellapsed = Misc.GetElapsedTicks(m_last_sync_upload[_task_name]);
      _remote_sequence = 0;
      _rows_updated = 0;

      if (_ellapsed < (m_wait_seconds_upload[_task_name] * 1000))
      {
        return true;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask,
                                                          out _is_task_running,
                                                          out _is_task_enabled,
                                                          out _wait,
                                                          out _local_sequence,
                                                          out _remote_sequence,
                                                          out _max_rows,
                                                          _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't read reporting_flag on task {0}", _task_name));

            return false;
          }

          m_wait_seconds_upload[_task_name] = _wait;

          if (!_is_task_enabled)
          {
            return true;
          }

          if (!MultiSite_SiteConfiguration.Update_NumPendingTimeStamp(SiteTask, out _max_local_sequence, out _total_reg_to_send, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Not updated pending number from tasks. Update_NumPendingTimeStamp() on task {0}", _task_name));

            return false;
          }

          // update the local_seq_id 
          if (!WWP_MultiSiteTask.Set_ReportingFlag(-1,
                                                   _total_reg_to_send,
                                                   SiteTask,
                                                   true,
                                                   _max_local_sequence,
                                                   out _is_task_enabled,
                                                   _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting_flag on task {0}", _task_name));

            return false;
          }

          if (!_is_task_enabled)
          {
            //Log.Error("Sync Points not enable .Set_ReportingFlag");

            return true;
          }

          _db_trx.Commit();

          // Commented: notifier a only change
          //// After commit because set running the task
          ////if (_total_reg_to_send == 0)
          ////{
          ////  return true;
          ////}

        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask,
                                                  out _is_task_running,
                                                  out _is_task_enabled,
                                                  out _wait,
                                                  out _local_sequence,
                                                  out _remote_sequence,
                                                  out _max_rows,
                                                  _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't read reporting flag values on task {0}", _task_name));

            return false;
          }

          if (!_is_task_running)
          {
            return true;
          }
        }

        _task_start_tick = Misc.GetTickCount();

        // Is equal when have a only change. Is more when have changes.
        while (_remote_sequence <= _max_local_sequence)
        {
          if (Misc.GetElapsedTicks(_task_start_tick) > MAX_RUNNING_TASK)
          {
            return true;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!TrxSync(_remote_sequence, _max_rows, out _remote_sequence_commited, out _rows_updated, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("MultiSite_SiteGenericUpload.TrxSync, task {0}", _task_name));

              break;
            }

            if (_rows_updated == 0)
            {
              break;
            }

            if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _remote_sequence_commited, _rows_updated, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("MultiSite_SiteGenericUpload.Site_SetCenterSequence(SiteTask), task {0}", _task_name));

              break;
            }

            _db_trx.Commit();
            _remote_sequence = _remote_sequence_commited;
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_upload[_task_name] = Misc.GetTickCount();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting_flag on task {0}", _task_name));
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return true;

    }

    public static Boolean UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS SiteTask, Trx_UploadPendings Trx_UploadPendingsTask)
    {
      Int32 _max_rows = DEFAULT_MAX_ROWS;
      Int64 _local_sequence;
      Boolean _is_task_enabled;
      Boolean _is_task_running;
      Int32 _task_start_tick;
      Int64 _ellapsed;
      String _task_name;
      Int32 _wait;
      Int32 _rows_updated_in_center;

      if (!m_init_sync)
      {
        InitTimeWaitTask();

        m_init_sync = true;
      }

      _task_name = Enum.GetName(SiteTask.GetType(), SiteTask);
      _ellapsed = Misc.GetElapsedTicks(m_last_sync_upload[_task_name]);

      if (_ellapsed < (m_wait_seconds_upload[_task_name] * 1000))
      {
        return true;
      }

      _task_start_tick = Misc.GetTickCount();

      try
      {
        while (true)
        {
          if (Misc.GetElapsedTicks(_task_start_tick) > MAX_RUNNING_TASK)
          {
            return true;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));

              return false;
            }

            if (!_is_task_enabled)
            {
              return true;
            }

            _db_trx.Commit();
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask, out _is_task_running, out _wait, out _local_sequence, out _max_rows, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Can't read reporting flag on task {0}", _task_name));

              return false;
            }

            m_wait_seconds_upload[_task_name] = _wait;

            if (!_is_task_running)
            {
              return false;
            }

            if (!Trx_UploadPendingsTask(_max_rows, out _rows_updated_in_center, _db_trx.SqlTransaction))
            {
              Log.Error(String.Format("Could not read and upload on task {0}", _task_name));

              return false;
            }

            if (_rows_updated_in_center == 0)
            {
              return true;
            }

            _db_trx.Commit();

          } // End using
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_upload[_task_name] = Misc.GetTickCount();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error(String.Format("Can't set reporting flag on task {0}", _task_name));
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return false;
    }

    #region Trx Sequence - Delegates

    public static Boolean Trx_UploadProvidersGames(Int32 MaxRowsToUpload, out Int32 RowsUpdatedInCenter, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _dt_pg;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      Int32 _rows_affected;

      RowsUpdatedInCenter = 0;

      _dt_pg = new DataTable();
      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   TOP (@pMAXRows) ");
      _sb.AppendLine("             PG_GAME_NAME ");
      _sb.AppendLine("           , PG_PAYOUT_1 ");
      _sb.AppendLine("           , PG_PAYOUT_2 ");
      _sb.AppendLine("           , PG_PAYOUT_3 ");
      _sb.AppendLine("           , PG_PAYOUT_4 ");
      _sb.AppendLine("           , PG_PAYOUT_5 ");
      _sb.AppendLine("           , PG_PAYOUT_6 ");
      _sb.AppendLine("           , PG_PAYOUT_7 ");
      _sb.AppendLine("           , PG_PAYOUT_8 ");
      _sb.AppendLine("           , PG_PAYOUT_9 ");
      _sb.AppendLine("           , PG_PAYOUT_10 ");
      _sb.AppendLine("      FROM   PROVIDERS_GAMES ");
      _sb.AppendLine("INNER JOIN   MS_SITE_PENDING_PROVIDERS_GAMES ");
      _sb.AppendLine("        ON   SPG_PV_ID   = PG_PV_ID ");
      _sb.AppendLine("       AND   SPG_GAME_ID = PG_GAME_ID ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpload;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            _dt_pg.Load(_reader);
          }
        }

        //if (_dt_pg.Rows.Count == 0)
        //{
        //  return true;
        //}

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_PROVIDERS_GAMES;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_dt_pg);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadProvidersGames : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadProvidersGames : Failed in MultisiteAccounts. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        _sb.Length = 0;
        _sb.AppendLine(" DELETE   MS_SITE_PENDING_PROVIDERS_GAMES");
        _sb.AppendLine("  WHERE   SPG_PV_ID   = @pProviderId ");
        _sb.AppendLine("   AND    SPG_GAME_ID = @pGameId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProviderId", SqlDbType.Int).SourceColumn = "PG_PV_ID";
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "PG_GAME_ID";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;

            _rows_affected = _sql_da.Update(_dt_pg);
          }
        }

        return true;
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction to upload game_play_sesions table
    //
    //  PARAMS :
    //      - INPUT :
    //          - MaxRowsToUpload:     
    //          - TrxSync: 
    //      - OUTPUT :
    //          - RowsUpdatedInCenter
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadGamePlaySessions(Int32 MaxRowsToUpload, out Int32 RowsUpdatedInCenter, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataSet _request;
      DataSet _reply;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      Int32 _rows_affected;
      Int32 _rows_deleted_gps;
      Int32 _rows_deleted_elp001_ps;


      RowsUpdatedInCenter = 0;
      try
      {
        _request = new DataSet();
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TOP (@pMAXRows) GPS_PLAY_SESSION_ID");
        _sb.AppendLine("       , GPS_GAME_ID");
        _sb.AppendLine("       , GPS_ACCOUNT_ID");
        _sb.AppendLine("       , GPS_TERMINAL_ID");
        _sb.AppendLine("       , GPS_PLAYED_COUNT");
        _sb.AppendLine("       , GPS_PLAYED_AMOUNT");
        _sb.AppendLine("       , GPS_WON_COUNT");
        _sb.AppendLine("       , GPS_WON_AMOUNT");
        _sb.AppendLine("       , GPS_PAYOUT");
        _sb.AppendLine("  FROM   GAME_PLAY_SESSIONS");
        _sb.AppendLine("SELECT   TOP (@pMAXRows) EPS_ID");
        _sb.AppendLine("       , EPS_TICKET_NUMBER");
        _sb.AppendLine("       , EPS_ACCOUNT_ID");
        _sb.AppendLine("       , EPS_SLOT_SERIAL_NUMBER");
        _sb.AppendLine("       , EPS_SLOT_HOUSE_NUMBER");
        _sb.AppendLine("       , EPS_VENUE_CODE");
        _sb.AppendLine("       , EPS_AREA_CODE");
        _sb.AppendLine("       , EPS_BANK_CODE");
        _sb.AppendLine("       , EPS_VENDOR_CODE");
        _sb.AppendLine("       , EPS_GAME_CODE");
        _sb.AppendLine("       , EPS_START_TIME");
        _sb.AppendLine("       , EPS_END_TIME");
        _sb.AppendLine("       , EPS_BET_AMOUNT");
        _sb.AppendLine("       , EPS_PAID_AMOUNT");
        _sb.AppendLine("       , EPS_GAMES_PLAYED");
        _sb.AppendLine("       , EPS_INITIAL_AMOUNT");
        _sb.AppendLine("       , EPS_ADITIONAL_AMOUNT");
        _sb.AppendLine("       , EPS_FINAL_AMOUNT");
        _sb.AppendLine("       , EPS_BET_COMB_CODE");
        _sb.AppendLine("       , EPS_KINDOF_TICKET");
        _sb.AppendLine("       , EPS_SEQUENCE_NUMBER");
        _sb.AppendLine("       , EPS_CUPON_NUMBER");
        _sb.AppendLine("       , EPS_DATE_UPDATED");
        _sb.AppendLine("       , EPS_DATE_INSERTED");
        _sb.AppendLine("  FROM   ELP01_PLAY_SESSIONS");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpload;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_request);
          }
        }

        _request.Tables[0].TableName = "GAME_PLAY_SESSIONS";
        _request.Tables[1].TableName = "ELP01_PLAY_SESSIONS";

        //if (_request.Tables["GAME_PLAY_SESSIONS"].Rows.Count + _request.Tables["ELP01_PLAY_SESSIONS"].Rows.Count == 0)
        //{
        //  return true;
        //}

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_GAME_PLAY_SESSIONS;
        _wwp_cmd_req.DataSet = _request;

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadGamePlaySessions : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadGamePlaySessions : Failed in MultiSite_SiteGenericUpload. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }
        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply = _wwp_cmd_reply.DataSet;
        RowsUpdatedInCenter = _reply.Tables["GAME_PLAY_SESSIONS"].Rows.Count + _reply.Tables["ELP01_PLAY_SESSIONS"].Rows.Count;


        // Delete game_play_sessions and elp01
        _reply.AcceptChanges();
        foreach (DataRow _row in _reply.Tables["GAME_PLAY_SESSIONS"].Rows)
        {
          _row.Delete();
        }

        foreach (DataRow _row in _reply.Tables["ELP01_PLAY_SESSIONS"].Rows)
        {
          _row.Delete();
        }

        _sb.Length = 0;
        _sb.AppendLine("DELETE   GAME_PLAY_SESSIONS");
        _sb.AppendLine(" WHERE   GPS_PLAY_SESSION_ID = @pPlaySessionId");
        _sb.AppendLine("   AND   GPS_GAME_ID         = @pGameId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).SourceColumn = "GPS_PLAY_SESSION_ID";
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GPS_GAME_ID";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _rows_deleted_gps = _reply.Tables["GAME_PLAY_SESSIONS"].Rows.Count;
            _rows_affected = _sql_da.Update(_reply.Tables["GAME_PLAY_SESSIONS"]);

            if (_rows_affected != _rows_deleted_gps)
            {
              Log.Warning(String.Format("Not deleted all records on tables GAME_PLAY_SESSIONS. Expected: {0}, Updated: {1}", _reply.Tables["GAME_PLAY_SESSIONS"].Rows.Count, _rows_affected));
            }
          }
        }
        _sb.Length = 0;
        _sb.AppendLine("DELETE   ELP01_PLAY_SESSIONS");
        _sb.AppendLine(" WHERE   EPS_ID              = @pEpsId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pEpsId", SqlDbType.BigInt).SourceColumn = "EPS_ID";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _rows_deleted_elp001_ps = _reply.Tables["ELP01_PLAY_SESSIONS"].Rows.Count;
            _rows_affected = _sql_da.Update(_reply.Tables["ELP01_PLAY_SESSIONS"]);

            if (_rows_affected != _rows_deleted_elp001_ps)
            {
              Log.Warning(String.Format("Not deleted all records on tables ELP001_PLAY_SESSIONS. Expected: {0}, Updated: {1}", _rows_deleted_elp001_ps, _rows_affected));
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadGamePlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction to upload ACCOUNT_DOCUMENTS table
    //
    //  PARAMS :
    //      - INPUT :
    //          - MaxRowsToUpload:     
    //          - TrxSync: 
    //      - OUTPUT :
    //          - RowsUpdatedInCenter
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadAccountDocuments(Int32 MaxRowsToUpload, out Int32 RowsUpdatedInCenter, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataSet _request;
      DataSet _reply;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      Int32 _rows_deleted;
      Int32 _rows_affected;

      RowsUpdatedInCenter = 0;
      try
      {
        _request = new DataSet();
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   TOP (@pMAXRows) ");
        _sb.AppendLine("           AD_ACCOUNT_ID ");
        _sb.AppendLine("         , AD_CREATED ");
        _sb.AppendLine("         , AD_MODIFIED ");
        _sb.AppendLine("         , AD_DATA ");
        _sb.AppendLine("         , AD_MS_SEQUENCE_ID ");
        _sb.AppendLine("    FROM   ACCOUNT_DOCUMENTS ");
        _sb.AppendLine("   INNER   JOIN MS_SITE_PENDING_ACCOUNT_DOCUMENTS ");
        _sb.AppendLine("      ON   AD_ACCOUNT_ID = PAD_ACCOUNT_ID ");
        _sb.AppendLine("ORDER BY   AD_ACCOUNT_ID ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpload;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_request);
          }
        }

        _request.Tables[0].TableName = "ACCOUNT_DOCUMENTS";

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_ACCOUNTS_DOCUMENTS;
        _wwp_cmd_req.DataSet = _request;

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadAccountDocuments : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadAccountDocuments : Failed in MultiSite_SiteGenericUpload. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }
        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply = _wwp_cmd_reply.DataSet;
        RowsUpdatedInCenter = _reply.Tables["ACCOUNT_DOCUMENTS"].Rows.Count;

        // - Performs the updating of the sequence in account_documents

        _reply.AcceptChanges();
        foreach (DataRow _row in _reply.Tables["ACCOUNT_DOCUMENTS"].Rows)
        {
          _row.Delete();
        }

        // - Delete the pendings returns the multisite.
        _sb.Length = 0;
        _sb.AppendLine("DELETE   MS_SITE_PENDING_ACCOUNT_DOCUMENTS ");
        _sb.AppendLine(" WHERE   PAD_ACCOUNT_ID    = @pAccountId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AD_ACCOUNT_ID";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {

            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _rows_deleted = _reply.Tables["ACCOUNT_DOCUMENTS"].Rows.Count;
            _rows_affected = _sql_da.Update(_reply.Tables["ACCOUNT_DOCUMENTS"]);

            if (_rows_affected != _rows_deleted)
            {
              Log.Warning(String.Format("Not deleted all records on tables MS_SITE_PENDING_ACCOUNT_DOCUMENTS. Expected: {0}, Updated: {1}", _rows_deleted, _rows_affected));
            }

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadAccountDocuments

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction to upload sells&buys table
    //
    //  PARAMS :
    //      - INPUT :
    //          - MaxRowsToUpload:     
    //          - TrxSync: 
    //      - OUTPUT :
    //          - RowsUpdatedInCenter
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadSellsAndBuys(Int32 MaxRowsToUpload, out Int32 RowsUpdatedInCenter, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataSet _request;
      DataSet _reply;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      Int32 _rows_affected;
      Int32 _rows_deleted;
      DataTable _dt;
      Object _list_operation_ids;

      RowsUpdatedInCenter = 0;
      try
      {
        _request = new DataSet();
        _dt = new DataTable();
        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @OperationIds VARCHAR(500)                          ");
        _sb.AppendLine("SELECT TOP (@pMAXRows) @OperationIds = coalesce(@OperationIds + ',', '')  ");
        _sb.AppendLine("       + convert(varchar(12),[mptwd_operation_id])          ");
        _sb.AppendLine("  FROM   ms_pending_task68_work_data                        ");
        _sb.AppendLine("SELECT   @OperationIds                         ");

        // TODO..
        //_sb.AppendLine("EXECUTE dbo.GetSalesAndPaymentsByOperationList @OperationIds");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpload;

          _list_operation_ids = _sql_cmd.ExecuteScalar();
        }

        if (_list_operation_ids != null && _list_operation_ids != DBNull.Value)
        {
          SharedBL02Common.GetSalesAndPaymentsByOperationListDirect((String)_list_operation_ids, out _dt);
        }   // if _list_operation_ids != null && _list_operation_ids != DBNull.Value
        _dt.TableName = "TASK68_WORK_DATA";
        _request.Tables.Add(_dt);

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_SELLS_AND_BUYS;
        _wwp_cmd_req.DataSet = _request;

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadSellsAndBuys : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadSellsAndBuys : Failed in MultiSite_SiteGenericUpload. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }
        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply = _wwp_cmd_reply.DataSet;
        RowsUpdatedInCenter = _reply.Tables[0].Rows.Count;


        // Delete sells&buys
        _reply.AcceptChanges();
        foreach (DataRow _row in _reply.Tables[0].Rows)
        {
          _row.Delete();
        }

        _sb.Length = 0;
        _sb.AppendLine("DELETE   MS_PENDING_TASK68_WORK_DATA");
        _sb.AppendLine(" WHERE   mptwd_operation_id = @pOperationId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "SB_SELL_BUYS_ID";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _rows_deleted = _reply.Tables[0].Rows.Count;
            _rows_affected = _sql_da.Update(_reply.Tables[0]);

            if (_rows_affected != _rows_deleted)
            {
              Log.Warning(String.Format("Not deleted all records on table MS_PENDING_TASK68_WORK_DATA. Expected: {0}, Updated: {1}", _reply.Tables[0].Rows.Count, _rows_affected));
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadSellsAndBuys


    #endregion

    #region Trx Timestamp - Delegates

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about areas
    //
    //  PARAMS :
    //      - INPUT :
    //          - LocalSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadAreas(Int64 FromSequence,
                                          Int64 MaxRowsToUpdate,
                                          out Int64 ToSequence,
                                          out Int32 RowsToUpdate,
                                          SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _areas;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _areas = new DataTable("AREAS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ AREAS
        ///

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           AR_AREA_ID");
        _sb.AppendLine("         , AR_NAME");
        _sb.AppendLine("         , AR_SMOKING");
        _sb.AppendLine("         , CAST(AR_TIMESTAMP as BIGINT) as AR_TIMESTAMP");
        _sb.AppendLine("    FROM   AREAS");
        _sb.AppendLine("   WHERE   AR_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   AR_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            _areas.Load(_reader);
          }
        }

        RowsToUpdate = _areas.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_areas.Compute("MAX(AR_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND AREAS TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_AREAS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_areas);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncAreas: Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncAreas: Failed in Multisite_SiteAreas. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadAreas


    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about banks
    //
    //  PARAMS :
    //      - INPUT :
    //          - LocalSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadBanks(Int64 FromSequence,
                                          Int64 MaxRowsToUpdate,
                                          out Int64 ToSequence,
                                          out Int32 RowsToUpdate,
                                          SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _banks;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _banks = new DataTable("BANKS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ BANKS
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           BK_BANK_ID");
        _sb.AppendLine("         , BK_AREA_ID");
        _sb.AppendLine("         , BK_NAME");
        _sb.AppendLine("         , CAST(BK_TIMESTAMP as BIGINT) as BK_TIMESTAMP");
        _sb.AppendLine("    FROM   BANKS");
        _sb.AppendLine("   WHERE   BK_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   BK_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _banks.Load(_sql_reader);
          }
        }

        RowsToUpdate = _banks.Rows.Count;

        if (_banks.Rows.Count > 0)
        {
          ToSequence = (Int64)_banks.Compute("MAX(BK_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND Banks TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_BANKS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_banks);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SyncBanks : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SyncBanks : Failed in Multisite_SiteBanks. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadBanks

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about play sessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadPlaySessions(Int64 FromSequence,
                                                 Int64 MaxRowsToUpdate,
                                                 out Int64 ToSequence,
                                                 out Int32 RowsToUpdate,
                                                 SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _play_sessions;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _play_sessions = new DataTable("PLAY_SESSIONS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ PLAY SESSIONS
        ///
        _sb = new StringBuilder();

        _sb.AppendLine("  ;WITH PSCTE AS");
        _sb.AppendLine("  (SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           PS_PLAY_SESSION_ID ");
        _sb.AppendLine("          ,PS_ACCOUNT_ID      ");
        _sb.AppendLine("          ,PS_TERMINAL_ID");
        _sb.AppendLine("          ,PS_TYPE");
        _sb.AppendLine("          ,PS_TYPE_DATA");
        _sb.AppendLine("          ,PS_STATUS");
        _sb.AppendLine("          ,PS_STARTED");
        _sb.AppendLine("          ,PS_INITIAL_BALANCE");
        _sb.AppendLine("          ,PS_PLAYED_COUNT");
        _sb.AppendLine("          ,PS_PLAYED_AMOUNT");
        _sb.AppendLine("          ,PS_WON_COUNT");
        _sb.AppendLine("          ,PS_WON_AMOUNT");
        _sb.AppendLine("          ,PS_CASH_IN");
        _sb.AppendLine("          ,PS_CASH_OUT");
        _sb.AppendLine("          ,PS_FINISHED");
        _sb.AppendLine("          ,PS_FINAL_BALANCE");
        _sb.AppendLine("          ,PS_LOCKED");
        _sb.AppendLine("          ,PS_STAND_ALONE");
        _sb.AppendLine("          ,PS_PROMO");
        _sb.AppendLine("          ,PS_WCP_TRANSACTION_ID");
        _sb.AppendLine("          ,PS_REDEEMABLE_CASH_IN");
        _sb.AppendLine("          ,PS_REDEEMABLE_CASH_OUT");
        _sb.AppendLine("          ,PS_REDEEMABLE_PLAYED");
        _sb.AppendLine("          ,PS_REDEEMABLE_WON");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_CASH_IN");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_CASH_OUT");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_PLAYED");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_WON");
        _sb.AppendLine("          ,PS_BALANCE_MISMATCH");
        _sb.AppendLine("          ,PS_SPENT_USED");
        _sb.AppendLine("          ,PS_CANCELLABLE_AMOUNT");
        _sb.AppendLine("          ,PS_REPORTED_BALANCE_MISMATCH");
        _sb.AppendLine("          ,PS_RE_CASH_IN");
        _sb.AppendLine("          ,PS_PROMO_RE_CASH_IN");
        _sb.AppendLine("          ,PS_RE_CASH_OUT");
        _sb.AppendLine("          ,PS_PROMO_RE_CASH_OUT");
        _sb.AppendLine("          ,PS_TIMESTAMP");
        _sb.AppendLine("          ,PS_RE_TICKET_IN");
        _sb.AppendLine("          ,PS_PROMO_RE_TICKET_IN");
        _sb.AppendLine("          ,PS_BILLS_IN_AMOUNT");
        _sb.AppendLine("          ,PS_RE_TICKET_OUT");
        _sb.AppendLine("          ,PS_PROMO_NR_TICKET_IN");
        _sb.AppendLine("          ,PS_PROMO_NR_TICKET_OUT");
        _sb.AppendLine("          ,PS_REDEEMABLE_PLAYED_ORIGINAL     ");
        _sb.AppendLine("          ,PS_REDEEMABLE_WON_ORIGINAL        ");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_PLAYED_ORIGINAL ");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_WON_ORIGINAL    ");
        _sb.AppendLine("          ,PS_PLAYED_COUNT_ORIGINAL          ");
        _sb.AppendLine("          ,PS_WON_COUNT_ORIGINAL             ");
        _sb.AppendLine("          ,PS_AUX_FT_RE_CASH_IN              ");
        _sb.AppendLine("          ,PS_AUX_FT_NR_CASH_IN              ");
        _sb.AppendLine("          ,PS_RE_FOUND_IN_EGM                ");
        _sb.AppendLine("          ,PS_NR_FOUND_IN_EGM                ");
        _sb.AppendLine("          ,PS_RE_REMAINING_IN_EGM            ");
        _sb.AppendLine("          ,PS_NR_REMAINING_IN_EGM            ");
        _sb.AppendLine("          ,PS_HANDPAYS_AMOUNT                ");
        _sb.AppendLine("          ,PS_HANDPAYS_PAID_AMOUNT           ");
        _sb.AppendLine("    FROM   PLAY_SESSIONS WITH( INDEX(IX_PS_TIMESTAMP)) ");
        _sb.AppendLine("   WHERE   PS_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   PS_TIMESTAMP ASC )");
        _sb.AppendLine("  SELECT   PS_PLAY_SESSION_ID ");
        _sb.AppendLine("          ,PS_ACCOUNT_ID      ");
        _sb.AppendLine("          ,PS_TERMINAL_ID");
        _sb.AppendLine("          ,PS_TYPE");
        _sb.AppendLine("          ,PS_TYPE_DATA");
        _sb.AppendLine("          ,PS_STATUS");
        _sb.AppendLine("          ,PS_STARTED");
        _sb.AppendLine("          ,PS_INITIAL_BALANCE");
        _sb.AppendLine("          ,PS_PLAYED_COUNT");
        _sb.AppendLine("          ,PS_PLAYED_AMOUNT");
        _sb.AppendLine("          ,PS_WON_COUNT");
        _sb.AppendLine("          ,PS_WON_AMOUNT");
        _sb.AppendLine("          ,PS_CASH_IN");
        _sb.AppendLine("          ,PS_CASH_OUT");
        _sb.AppendLine("          ,PS_FINISHED");
        _sb.AppendLine("          ,PS_FINAL_BALANCE");
        _sb.AppendLine("          ,PS_LOCKED");
        _sb.AppendLine("          ,PS_STAND_ALONE");
        _sb.AppendLine("          ,PS_PROMO");
        _sb.AppendLine("          ,PS_WCP_TRANSACTION_ID");
        _sb.AppendLine("          ,PS_REDEEMABLE_CASH_IN");
        _sb.AppendLine("          ,PS_REDEEMABLE_CASH_OUT");
        _sb.AppendLine("          ,PS_REDEEMABLE_PLAYED");
        _sb.AppendLine("          ,PS_REDEEMABLE_WON");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_CASH_IN");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_CASH_OUT");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_PLAYED");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_WON");
        _sb.AppendLine("          ,PS_BALANCE_MISMATCH");
        _sb.AppendLine("          ,PS_SPENT_USED");
        _sb.AppendLine("          ,PS_CANCELLABLE_AMOUNT");
        _sb.AppendLine("          ,PS_REPORTED_BALANCE_MISMATCH");
        _sb.AppendLine("          ,PS_RE_CASH_IN");
        _sb.AppendLine("          ,PS_PROMO_RE_CASH_IN");
        _sb.AppendLine("          ,PS_RE_CASH_OUT");
        _sb.AppendLine("          ,PS_PROMO_RE_CASH_OUT");
        _sb.AppendLine("          ,CAST(PS_TIMESTAMP as BIGINT) as PS_TIMESTAMP");
        _sb.AppendLine("          ,PS_RE_TICKET_IN");
        _sb.AppendLine("          ,PS_PROMO_RE_TICKET_IN");
        _sb.AppendLine("          ,PS_BILLS_IN_AMOUNT");
        _sb.AppendLine("          ,PS_RE_TICKET_OUT");
        _sb.AppendLine("          ,PS_PROMO_NR_TICKET_IN");
        _sb.AppendLine("          ,PS_PROMO_NR_TICKET_OUT");
        _sb.AppendLine("          ,PS_REDEEMABLE_PLAYED_ORIGINAL     ");
        _sb.AppendLine("          ,PS_REDEEMABLE_WON_ORIGINAL        ");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_PLAYED_ORIGINAL ");
        _sb.AppendLine("          ,PS_NON_REDEEMABLE_WON_ORIGINAL    ");
        _sb.AppendLine("          ,PS_PLAYED_COUNT_ORIGINAL          ");
        _sb.AppendLine("          ,PS_WON_COUNT_ORIGINAL             ");
        _sb.AppendLine("          ,PS_AUX_FT_RE_CASH_IN              ");
        _sb.AppendLine("          ,PS_AUX_FT_NR_CASH_IN              ");
        _sb.AppendLine("          ,PS_RE_FOUND_IN_EGM                ");
        _sb.AppendLine("          ,PS_NR_FOUND_IN_EGM                ");
        _sb.AppendLine("          ,PS_RE_REMAINING_IN_EGM            ");
        _sb.AppendLine("          ,PS_NR_REMAINING_IN_EGM            ");
        _sb.AppendLine("          ,PS_HANDPAYS_AMOUNT                ");
        _sb.AppendLine("          ,PS_HANDPAYS_PAID_AMOUNT           ");
        _sb.AppendLine("    FROM   PSCTE");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _play_sessions.Load(_sql_reader);
          }
        }

        RowsToUpdate = _play_sessions.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_play_sessions.Compute("MAX(PS_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND PLAY SESSIONS TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_PLAY_SESSIONS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_play_sessions);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SyncPlaySessions : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SyncPlaySessions : Failed in Multisite_SitePlaySessions. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadPlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about Sales per Hour
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadSPH(Int64 FromSequence,
                                        Int64 MaxRowsToUpdate,
                                        out Int64 ToSequence,
                                        out Int32 RowsToUpdate,
                                        SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _sph;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _sph = new DataTable("SPH");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ SALES PER HOUR
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           SPH_BASE_HOUR");
        _sb.AppendLine("         , SPH_TERMINAL_ID");
        _sb.AppendLine("         , ISNULL(TGT_TRANSLATED_GAME_ID, 0) AS SPH_GAME_ID");
        _sb.AppendLine("         , SPH_PLAYED_COUNT");
        _sb.AppendLine("         , SPH_PLAYED_AMOUNT");
        _sb.AppendLine("         , SPH_WON_COUNT");
        _sb.AppendLine("         , SPH_WON_AMOUNT");
        _sb.AppendLine("         , SPH_TERMINAL_NAME");
        _sb.AppendLine("         , SPH_GAME_NAME");
        _sb.AppendLine("         , CAST(SPH_TIMESTAMP as BIGINT) as SPH_TIMESTAMP");
        _sb.AppendLine("         , SPH_THEORETICAL_WON_AMOUNT");
        _sb.AppendLine("         , CASE tgt_payout_idx ");
        _sb.AppendLine("                WHEN  1 THEN pg_payout_1");
        _sb.AppendLine("                WHEN  2 THEN pg_payout_2");
        _sb.AppendLine("                WHEN  3 THEN pg_payout_3");
        _sb.AppendLine("                WHEN  4 THEN pg_payout_4");
        _sb.AppendLine("                WHEN  5 THEN pg_payout_5");
        _sb.AppendLine("                WHEN  6 THEN pg_payout_6");
        _sb.AppendLine("                WHEN  7 THEN pg_payout_7");
        _sb.AppendLine("                WHEN  8 THEN pg_payout_8");
        _sb.AppendLine("                WHEN  9 THEN pg_payout_9");
        _sb.AppendLine("                WHEN 10 THEN pg_payout_10");
        _sb.AppendLine("                ELSE CAST(0.00 AS MONEY)");
        _sb.AppendLine("           END AS SPH_PAYOUT");
        _sb.AppendLine("         , SPH_UNIQUE_ID");
        _sb.AppendLine("         , SPH_PROGRESSIVE_PROVISION_AMOUNT");
        _sb.AppendLine("         , SPH_JACKPOT_AMOUNT");
        _sb.AppendLine("         , SPH_PROGRESSIVE_JACKPOT_AMOUNT");
        _sb.AppendLine("         , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0");
        _sb.AppendLine("    FROM   SALES_PER_HOUR WITH( INDEX(IX_SPH_TIMESTAMP)) ");
        _sb.AppendLine("    LEFT   OUTER JOIN TERMINAL_GAME_TRANSLATION  ON TGT_SOURCE_GAME_ID = SPH_GAME_ID ");
        _sb.AppendLine("                                                AND TGT_TERMINAL_ID    = SPH_TERMINAL_ID ");
        _sb.AppendLine("    LEFT   OUTER JOIN PROVIDERS_GAMES ON PG_GAME_ID = TGT_TRANSLATED_GAME_ID ");
        _sb.AppendLine("   WHERE   SPH_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   SPH_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _sph.Load(_sql_reader);
          }
        }

        RowsToUpdate = _sph.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_sph.Compute("MAX(SPH_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND SALES PER HOUR TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_SALES_PER_HOUR;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_sph);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncSPH : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncSPH : Failed in Multisite_SiteSalesPerHour. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadSPH

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about terminals
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence: 
    //          - RowsToUpdate: 
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadTerminals(Int64 FromSequence,
                                              Int64 MaxRowsToUpdate,
                                              out Int64 ToSequence,
                                              out Int32 RowsToUpdate,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _terminals;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _terminals = new DataTable("TERMINALS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ TERMINALS
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           TE_TERMINAL_ID              ");
        _sb.AppendLine("         , TE_TYPE                     ");
        _sb.AppendLine("         , TE_SERVER_ID                ");
        _sb.AppendLine("         , TE_EXTERNAL_ID              ");
        _sb.AppendLine("         , TE_BLOCKED                  ");
        _sb.AppendLine("         , TE_ACTIVE                   ");
        _sb.AppendLine("         , TE_PROVIDER_ID              ");
        _sb.AppendLine("         , TE_CLIENT_ID                ");
        _sb.AppendLine("         , TE_BUILD_ID                 ");
        _sb.AppendLine("         , TE_TERMINAL_TYPE            ");
        _sb.AppendLine("         , TE_VENDOR_ID                ");
        _sb.AppendLine("         , TE_STATUS                   ");
        _sb.AppendLine("         , TE_RETIREMENT_DATE          ");
        _sb.AppendLine("         , TE_RETIREMENT_REQUESTED     ");
        _sb.AppendLine("         , TE_DENOMINATION             ");
        _sb.AppendLine("         , TE_MULTI_DENOMINATION       ");
        _sb.AppendLine("         , TE_PROGRAM                  ");
        _sb.AppendLine("         , TE_THEORETICAL_PAYOUT       ");
        _sb.AppendLine("         , TE_PROV_ID                  ");
        _sb.AppendLine("         , TE_BANK_ID                  ");
        _sb.AppendLine("         , TE_FLOOR_ID                 ");
        _sb.AppendLine("         , TE_GAME_TYPE                ");
        _sb.AppendLine("         , TE_ACTIVATION_DATE          ");
        _sb.AppendLine("         , TE_CURRENT_ACCOUNT_ID       ");
        _sb.AppendLine("         , TE_CURRENT_PLAY_SESSION_ID  ");
        _sb.AppendLine("         , TE_REGISTRATION_CODE        ");
        _sb.AppendLine("         , TE_SAS_FLAGS                ");
        _sb.AppendLine("         , TE_SERIAL_NUMBER            ");
        _sb.AppendLine("         , TE_CABINET_TYPE             ");
        _sb.AppendLine("         , TE_JACKPOT_CONTRIBUTION_PCT ");
        _sb.AppendLine("         , CAST(TE_TIMESTAMP AS BIGINT) AS TE_TIMESTAMP");
        _sb.AppendLine("         , TE_POSITION                 ");
        _sb.AppendLine("         , TE_MACHINE_ID               ");
        //FOS 27-03-2015
        _sb.AppendLine("         , TE_MASTER_ID                ");
        _sb.AppendLine("         , TE_CHANGE_ID                ");
        _sb.AppendLine("         , TE_BASE_NAME                ");
        _sb.AppendLine("    FROM   TERMINALS");
        _sb.AppendLine("   WHERE   TE_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   TE_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _terminals.Load(_sql_reader);

          }
        }

        RowsToUpdate = _terminals.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_terminals.Compute("MAX(TE_TIMESTAMP)", "");
          //return true;        
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND TERMINALS TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_TERMINALS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_terminals);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncTerminals : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncTerminals : Failed in MultiSite_SiteTerminals. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about Terminals connected
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadTerminalsConnected(Int64 FromSequence,
                                                       Int64 MaxRowsToUpdate,
                                                       out Int64 ToSequence,
                                                       out Int32 RowsToUpdate,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _terminals_connected;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _terminals_connected = new DataTable("TERMINALS_CONNECTED");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        //
        // READ TerminalsConnected
        //
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        //FOS 27-03-2015
        _sb.AppendLine("           TC_DATE");
        _sb.AppendLine("         , TC_MASTER_ID");
        _sb.AppendLine("         , TC_TERMINAL_ID");
        _sb.AppendLine("         , TC_STATUS");
        _sb.AppendLine("         , TC_CONNECTED");
        _sb.AppendLine("         , CAST(TC_TIMESTAMP as BIGINT) as TC_TIMESTAMP");
        _sb.AppendLine("    FROM   TERMINALS_CONNECTED");
        _sb.AppendLine("   WHERE   TC_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   TC_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _terminals_connected.Load(_sql_reader);
          }
        }

        RowsToUpdate = _terminals_connected.Rows.Count;

        if (_terminals_connected.Rows.Count > 0)
        {
          ToSequence = (Int64)_terminals_connected.Compute("MAX(TC_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        //
        // SEND TerminalsConnected TO CENTER
        //

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_TERMINALS_CONNECTION;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_terminals_connected);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_SyncTerminalsConnected : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_SyncTerminalsConnected : Failed in Multisite_SiteTerminalsConnected. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadTerminalsConnected

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about providers
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadProvider(Int64 FromSequence,
                                             Int64 MaxRowsToUpdate,
                                             out Int64 ToSequence,
                                             out Int32 RowsToUpdate,
                                             SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _providers;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _providers = new DataTable("PROVIDERS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ BANKS
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           PV_ID");
        _sb.AppendLine("         , PV_NAME");
        _sb.AppendLine("         , PV_HIDE");
        _sb.AppendLine("         , PV_POINTS_MULTIPLIER");
        _sb.AppendLine("         , PV_3GS");
        _sb.AppendLine("         , PV_3GS_VENDOR_ID");
        _sb.AppendLine("         , PV_3GS_VENDOR_IP");
        _sb.AppendLine("         , PV_SITE_JACKPOT");
        _sb.AppendLine("         , PV_ONLY_REDEEMABLE");
        _sb.AppendLine("         , CAST(PV_TIMESTAMP as BIGINT) as PV_TIMESTAMP");
        _sb.AppendLine("    FROM   PROVIDERS");
        _sb.AppendLine("   WHERE   PV_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   PV_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _providers.Load(_sql_reader);
          }
        }

        RowsToUpdate = _providers.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_providers.Compute("MAX(PV_TIMESTAMP)", "");
          //return true;
        }


        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND Banks TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_PROVIDERS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_providers);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncProvider : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncProvider : Failed in MultisiteAccounts. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadProvider

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about last activity
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadLastActivity(Int32 MaxRowsToUpdate,
                                                 out Int32 RowsUpdatedInCenter,
                                                 SqlTransaction SqlTrx)
    {

      StringBuilder _sb;
      DataTable _last_account_activity;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;

      DataSet _reply;
      WWP_MsgTableReply _wwp_cmd_reply;

      _last_account_activity = new DataTable("LAST_ACCOUNT_ACTIVITY");
      RowsUpdatedInCenter = 0;
      try
      {
        ///
        // READ LAST ACCOUNT ACTIVITY 
        ///
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   TOP (@pMAXRows) ");
        _sb.AppendLine("           LAA_ACCOUNT_ID ");
        _sb.AppendLine("         , AC_LAST_ACTIVITY AS LAA_LAST_ACTIVITY");
        _sb.AppendLine("         , AC_RE_BALANCE AS LAA_RE_BALANCE ");
        _sb.AppendLine("         , AC_PROMO_RE_BALANCE AS LAA_PROMO_RE_BALANCE ");
        _sb.AppendLine("         , AC_PROMO_NR_BALANCE AS LAA_PROMO_NR_BALANCE ");
        _sb.AppendLine("         , AC_IN_SESSION_RE_BALANCE AS LAA_IN_SESSION_RE_BALANCE ");
        _sb.AppendLine("         , AC_IN_SESSION_PROMO_RE_BALANCE AS LAA_IN_SESSION_PROMO_RE_BALANCE ");
        _sb.AppendLine("         , AC_IN_SESSION_PROMO_NR_BALANCE AS LAA_IN_SESSION_PROMO_NR_BALANCE ");
        _sb.AppendLine("         , CAST(LAA_TIMESTAMP AS BIGINT) AS LAA_TIMESTAMP ");
        _sb.AppendLine("    FROM   MS_SITE_PENDING_LAST_ACTIVITY ");
        _sb.AppendLine("    LEFT   JOIN ACCOUNTS ON AC_ACCOUNT_ID = LAA_ACCOUNT_ID ");
        _sb.AppendLine("   ORDER   BY LAA_TIMESTAMP ");
        _sb.AppendLine("         , LAA_ACCOUNT_ID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _last_account_activity.Load(_sql_reader);
          }
        }

        RowsUpdatedInCenter = _last_account_activity.Rows.Count;


        ///
        // SEND LAST ACTIVTY TO CENTER
        ///

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_LAST_ACTIVITY;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_last_account_activity);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadLastActivity : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadLastActivity : Failed in Multisite_SiteGenericUpload. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        _reply = _wwp_cmd_reply.DataSet;

        // - Performs the updating of the sequence in 

        _reply.AcceptChanges();
        foreach (DataRow _row in _reply.Tables["LAST_ACCOUNT_ACTIVITY"].Rows)
        {
          _row.Delete();
        }

        // - Delete the pendings returns the multisite.
        _sb.Length = 0;
        _sb.AppendLine("DELETE   MS_SITE_PENDING_LAST_ACTIVITY ");
        _sb.AppendLine(" WHERE   LAA_ACCOUNT_ID                = @pAccountId ");
        _sb.AppendLine("   AND   CAST(LAA_TIMESTAMP AS BIGINT) = @pTimestamp ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "LAA_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pTimestamp", SqlDbType.BigInt).SourceColumn = "LAA_TIMESTAMP";

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {

            _sql_da.DeleteCommand = _sql_cmd;
            _sql_da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.ContinueUpdateOnError = true;
            _sql_da.Update(_reply.Tables["LAST_ACCOUNT_ACTIVITY"]);

            // WIG-735 - Batch is not checked, because "timespamp" can change while data is uploading 

          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadLastActivity

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about casier sesions gruped by hour
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadCashierMovementsGrupedByHour(Int64 FromSequence,
                                                                 Int64 MaxRowsToUpdate,
                                                                 out Int64 ToSequence,
                                                                 out Int32 RowsToUpdate,
                                                                 SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _cashier_movements_grouped_by_hour;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _cashier_movements_grouped_by_hour = new DataTable("CASHIER_MOVEMENTS_GROUPED_BY_HOUR");

      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ CASHIER MOVEMENTS GROUPED BY HOUR
        ///
        _sb = new StringBuilder();

        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           CM_DATE ");
        _sb.AppendLine("         , CM_TYPE ");
        _sb.AppendLine("         , CM_SUB_TYPE ");
        _sb.AppendLine("         , CM_CURRENCY_ISO_CODE ");
        _sb.AppendLine("         , CM_CURRENCY_DENOMINATION ");
        _sb.AppendLine("         , CM_TYPE_COUNT ");
        _sb.AppendLine("         , CM_SUB_AMOUNT ");
        _sb.AppendLine("         , CM_ADD_AMOUNT ");
        _sb.AppendLine("         , CM_AUX_AMOUNT ");
        _sb.AppendLine("         , CM_INITIAL_BALANCE ");
        _sb.AppendLine("         , CM_FINAL_BALANCE ");
        _sb.AppendLine("         , CAST(CM_TIMESTAMP as BIGINT) AS CM_TIMESTAMP ");
        _sb.AppendLine("         , CM_UNIQUE_ID ");
        _sb.AppendLine("         , CM_CAGE_CURRENCY_TYPE ");
        _sb.AppendLine("    FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR ");
        _sb.AppendLine("   WHERE   CM_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   CM_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _cashier_movements_grouped_by_hour.Load(_sql_reader);
          }
        }

        RowsToUpdate = _cashier_movements_grouped_by_hour.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_cashier_movements_grouped_by_hour.Compute("MAX(CM_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND CASHIER MOVEMENTS GROUPED BY HOUR TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_CASHIER_SESIONS_GRUPED_BY_HOUR;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_cashier_movements_grouped_by_hour);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadCashierMovementsGrupedByHour : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadCashierMovementsGrupedByHour : Failed in Multisite_SiteCashierMovementsGrupedByHour. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_UploadCashierMovementsGrupedByHour

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about AlarmS
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadAlarms(Int64 FromSequence,
                                          Int64 MaxRowsToUpdate,
                                          out Int64 ToSequence,
                                          out Int32 RowsToUpdate,
                                          SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _alarms;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _alarms = new DataTable("Alarms");

      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ Alarms
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           AL_ALARM_ID");
        _sb.AppendLine("         , AL_SOURCE_CODE");
        _sb.AppendLine("         , AL_SOURCE_ID");
        _sb.AppendLine("         , AL_SOURCE_NAME");
        _sb.AppendLine("         , AL_ALARM_CODE");
        _sb.AppendLine("         , AL_ALARM_NAME");
        _sb.AppendLine("         , AL_ALARM_DESCRIPTION");
        _sb.AppendLine("         , AL_SEVERITY");
        _sb.AppendLine("         , AL_REPORTED");
        _sb.AppendLine("         , AL_DATETIME");
        _sb.AppendLine("         , AL_ACK_DATETIME");
        _sb.AppendLine("         , AL_ACK_USER_ID");
        _sb.AppendLine("         , AL_ACK_USER_NAME");
        _sb.AppendLine("         , CAST(AL_TIMESTAMP as BIGINT) AS AL_TIMESTAMP");
        _sb.AppendLine("    FROM   ALARMS");
        _sb.AppendLine("   WHERE   AL_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        //_sb.AppendLine("     AND   AL_REPORTED >= DATEADD(m, -1, GETDATE()) "); //max data for send, 1 month.
        _sb.AppendLine("ORDER BY   AL_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _alarms.Load(_sql_reader);
          }
        }

        RowsToUpdate = _alarms.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_alarms.Compute("MAX(AL_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        ///
        /// SEND Alarms 
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_ALARMS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_alarms);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadAlarms : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadAlarms: Failed in Multisite_SiteAlarms. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_UploadAlarms

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about Handpays
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence:    
    //          - RowsToUpdate:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadHandpays(Int64 FromSequence,
                                             Int64 MaxRowsToUpdate,
                                         out Int64 ToSequence,
                                         out Int32 RowsToUpdate,
                                             SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _handpays;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _handpays = new DataTable("Handpays");

      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ HANDPAYS
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP (@pMAXRows)");
        _sb.AppendLine("           HP_ID ");
        _sb.AppendLine("         , HP_TERMINAL_ID ");
        _sb.AppendLine("         , HP_GAME_BASE_NAME ");
        _sb.AppendLine("         , HP_DATETIME ");
        _sb.AppendLine("         , HP_PREVIOUS_METERS ");
        _sb.AppendLine("         , HP_AMOUNT ");
        _sb.AppendLine("         , HP_TE_NAME ");
        _sb.AppendLine("         , HP_TE_PROVIDER_ID ");
        _sb.AppendLine("         , HP_MOVEMENT_ID ");
        _sb.AppendLine("         , HP_TYPE ");
        _sb.AppendLine("         , HP_PLAY_SESSION_ID ");
        _sb.AppendLine("         , HP_SITE_JACKPOT_INDEX ");
        _sb.AppendLine("         , HP_SITE_JACKPOT_NAME ");
        _sb.AppendLine("         , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ");
        _sb.AppendLine("         , HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID ");
        _sb.AppendLine("         , HP_STATUS ");
        _sb.AppendLine("         , HP_SITE_JACKPOT_NOTIFIED ");
        _sb.AppendLine("         , HP_TICKET_ID ");
        _sb.AppendLine("         , HP_TRANSACTION_ID ");
        _sb.AppendLine("         , HP_CANDIDATE_PLAY_SESSION_ID ");
        _sb.AppendLine("         , HP_CANDIDATE_PREV_PLAY_SESSION_ID ");
        _sb.AppendLine("         , HP_LONG_POLL_1B_DATA ");
        _sb.AppendLine("         , HP_PROGRESSIVE_ID ");
        _sb.AppendLine("         , HP_LEVEL ");
        _sb.AppendLine("         , HP_STATUS_CHANGED ");
        _sb.AppendLine("         , HP_TAX_BASE_AMOUNT ");
        _sb.AppendLine("         , HP_TAX_AMOUNT ");
        _sb.AppendLine("         , HP_TAX_PCT ");
        _sb.AppendLine("         , CAST(HP_TIMESTAMP as BIGINT) AS HP_TIMESTAMP ");
        _sb.AppendLine("    FROM   HANDPAYS ");
        _sb.AppendLine("   WHERE   HP_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258 ");
        _sb.AppendLine("ORDER BY   HP_TIMESTAMP ASC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _handpays.Load(_sql_reader);
          }
        }

        RowsToUpdate = _handpays.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_handpays.Compute("MAX(HP_TIMESTAMP)", "");
          //return true;
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        ///
        /// SEND HANDPAYS
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_HANDPAYS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_handpays);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_UploadHandpays : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_UploadHandpays: Failed in Multisite_SiteHandpays. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_UploadHandpays

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about Creditlines
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence: 
    //          - RowsToUpdate: 
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadCreditlines(Int64 FromSequence,
                                              Int64 MaxRowsToUpdate,
                                              out Int64 ToSequence,
                                              out Int32 RowsToUpdate,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _credit_lines;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _credit_lines = new DataTable("CREDIT_LINES");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ CREDIT_LINES
        ///
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT TOP (@pMAXRows)                                                                                          ");
        _sb.AppendLine("        CL_ID                                                                                                   ");
        _sb.AppendLine("      , CL_ACCOUNT_ID                                                                                           ");
        _sb.AppendLine("      , CL_LIMIT_AMOUNT                                                                                         ");
        _sb.AppendLine("      , CL_TTO_AMOUNT                                                                                           ");
        _sb.AppendLine("      , CL_SPENT_AMOUNT                                                                                         ");
        _sb.AppendLine("      , CL_ISO_CODE                                                                                             ");
        _sb.AppendLine("      , CL_STATUS                                                                                               ");
        _sb.AppendLine("      , CL_IBAN                                                                                                 ");
        _sb.AppendLine("      , CL_XML_SIGNERS                                                                                          ");
        _sb.AppendLine("      , CL_CREATION                                                                                             ");
        _sb.AppendLine("      , CL_UPDATE                                                                                               ");
        _sb.AppendLine("      , CL_EXPIRED                                                                                              ");
        _sb.AppendLine("      , CL_LAST_PAYBACK                                                                                         ");
        _sb.AppendLine("      , CAST(CL_TIMESTAMP as BIGINT) AS CL_TIMESTAMP                                                            ");
        _sb.AppendLine("   FROM  CREDIT_LINES                                                                                           ");
        _sb.AppendLine("  WHERE   CL_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258   ");
        _sb.AppendLine("ORDER BY   CL_TIMESTAMP ASC                                                                                     ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _credit_lines.Load(_sql_reader);

          }
        }

        RowsToUpdate = _credit_lines.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_credit_lines.Compute("MAX(CL_TIMESTAMP)", "");
          //return true;        
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND CREDITLINES TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_CREDITLINES;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_credit_lines);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncCreditlines : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncCreditlines : Failed in MultiSite_CreditLines. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadCreditlines

    //------------------------------------------------------------------------------
    // PURPOSE : Transaction with MultiSite about Creditline Movements
    //
    //  PARAMS :
    //      - INPUT :
    //          - FromSequence: 
    //          - MaxRowsToUpdate: 
    //          - SqlTrx: 
    //      - OUTPUT :
    //          - ToSequence: 
    //          - RowsToUpdate: 
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    public static Boolean Trx_UploadCreditlineMovements(Int64 FromSequence,
                                              Int64 MaxRowsToUpdate,
                                              out Int64 ToSequence,
                                              out Int32 RowsToUpdate,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _credit_line_movements;
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      DataTable _sequence;

      _credit_line_movements = new DataTable("CREDIT_LINE_MOVEMENTS");
      ToSequence = 0;
      RowsToUpdate = 0;

      try
      {
        ///
        // READ CREDIT_LINE_MOVEMENTS
        ///
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT  TOP (@pMAXRows)                                                                                         ");
        _sb.AppendLine("        CLM_ID                                                                                                  ");
        _sb.AppendLine("      , CLM_CREDIT_LINE_ID                                                                                      ");
        _sb.AppendLine("      , CLM_OPERATION_ID                                                                                        ");
        _sb.AppendLine("      , CLM_TYPE                                                                                                ");
        _sb.AppendLine("      , CLM_OLD_VALUE                                                                                           ");
        _sb.AppendLine("      , CLM_NEW_VALUE                                                                                           ");
        _sb.AppendLine("      , CLM_CREATION                                                                                           ");
        _sb.AppendLine("      , CLM_CREATION_USER                                                                                       ");
        _sb.AppendLine("      , CAST(CLM_TIMESTAMP as BIGINT) AS CLM_TIMESTAMP                                                           ");
        _sb.AppendLine("   FROM  CREDIT_LINE_MOVEMENTS                                                                                  ");
        _sb.AppendLine("  WHERE   CLM_TIMESTAMP > CONVERT(BINARY(8),@pSequence) -- CONVERT NEEDED FOR IMPROVE THE PERFORMANCE TFS-1258  ");
        _sb.AppendLine("ORDER BY   CLM_TIMESTAMP ASC                                                                                    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRowsToUpdate;
          _sql_cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = FromSequence;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            _credit_line_movements.Load(_sql_reader);

          }
        }

        RowsToUpdate = _credit_line_movements.Rows.Count;

        if (RowsToUpdate > 0)
        {
          ToSequence = (Int64)_credit_line_movements.Compute("MAX(CLM_TIMESTAMP)", "");
          //return true;        
        }

        // ToSequence should be > or = than FromSequence !        
        ToSequence = Math.Max(ToSequence, FromSequence);

        //if (ToSequence == 0)
        //{
        //  return true;
        //}

        ///
        // SEND CREDITLINE MOVEMENTS TO CENTER
        ///

        _sequence = new DataTable("SEQUENCE");
        _sequence.Columns.Add("FIRST_SEQUENCE", typeof(Int64));
        _sequence.Columns.Add("LAST_SEQUENCE", typeof(Int64));
        _sequence.Rows.Add(FromSequence, ToSequence);
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;

        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_UPLOAD_CREDITLINE_MOVEMENTS;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(_credit_line_movements);
        _wwp_cmd_req.DataSet.Tables.Add(_sequence);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("TrxSyncCreditlineMovements : Timeout or Failed!");

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("TrxSyncCreditlineMovements : Failed in MultiSite_CreditLineMovements. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString());

          return false;

        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Trx_UploadCreditlineMovements


    #endregion

    public static void UploadAllTimestampTable()
    {

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadAreas, MultiSite_SiteGenericUpload.Trx_UploadAreas))
      {
        Log.Error("Upload Areas Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour, MultiSite_SiteGenericUpload.Trx_UploadSPH))
      {
        Log.Error("Upload SalesPerHour Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadBanks, MultiSite_SiteGenericUpload.Trx_UploadBanks))
      {
        Log.Error("Upload Banks Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions, MultiSite_SiteGenericUpload.Trx_UploadPlaySessions))
      {
        Log.Error("Upload PlaySessions Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadTerminals, MultiSite_SiteGenericUpload.Trx_UploadTerminals))
      {
        Log.Error("Upload Terminals Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected, MultiSite_SiteGenericUpload.Trx_UploadTerminalsConnected))
      {
        Log.Error("Upload Terminals Connected Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour, MultiSite_SiteGenericUpload.Trx_UploadCashierMovementsGrupedByHour))
      {
        Log.Error("Upload Cashier Sesions Gruped By Hour Failed.");
      }

      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadAlarms, MultiSite_SiteGenericUpload.Trx_UploadAlarms))
      {
        Log.Error("Upload Alarms failed.");
      }
      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadHandpays, MultiSite_SiteGenericUpload.Trx_UploadHandpays))
      {
        Log.Error("Upload Handpays failed.");
      }
      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadCreditLines, MultiSite_SiteGenericUpload.Trx_UploadCreditlines))
      {
        Log.Error("Upload Creditlines failed.");
      }
      if (!MultiSite_SiteGenericUpload.UploadTimestampTable(TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements, MultiSite_SiteGenericUpload.Trx_UploadCreditlineMovements))
      {
        Log.Error("Upload Creditline Movements failed.");
      }
    } //UploadAllTimestampTable

    public static void UploadAllSequenceTable()
    {
      //Int32 _master_mode;

      //_master_mode = GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0);

      //if (_master_mode == 1) //  CODERE: Only editing Multisite!
      //{
      //  if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions, MultiSite_SiteGenericUpload.Trx_UploadGamePlaySessions))
      //  {
      //    Log.Error("Upload Game Play Sessions Failed.");
      //  }
      //}
      //else if (_master_mode == 2)// Only editing Site
      //{
      //  //if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames, MultiSite_SiteGenericUpload.Trx_UploadProvidersGames))
      //  //{
      //  //  Log.Error("Upload Terminals Connected Failed.");
      //  //}
      //  //if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadProviders, MultiSite_SiteGenericUpload.Trx_UploadProviders))
      //  //{
      //  //  Log.Error("Upload Providers Failed.");
      //  //}
      //}
      //  
      if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments, MultiSite_SiteGenericUpload.Trx_UploadAccountDocuments))
      {
        Log.Error("Upload Account Documents Failed.");
      }
      //  
      if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadLastActivity, MultiSite_SiteGenericUpload.Trx_UploadLastActivity))
      {
        Log.Error("Upload last activity Failed.");
      }

      //      if (GeneralParam.GetInt32("MultiSite", "TablesMasterMode", 0) == 1) //  
      //      {
      if (!MultiSite_SiteGenericUpload.UploadSequenceTable(TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys, MultiSite_SiteGenericUpload.Trx_UploadSellsAndBuys))
      {
        Log.Error("Upload Sells & buys Failed.");
      }
      //      }

    }

  }
}
