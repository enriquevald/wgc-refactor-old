//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_SitePoints.cs
// 
//   DESCRIPTION: Process for Points sinchronize.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 08-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using WSI.WWP;
using WSI.WWP_Client;

namespace WSI.WWP_SiteService.MultiSite
{
  class MultiSite_SitePoints
  {
    #region Constants

    private const Int32 NUMBER_OF_RETRIES_FOR_SYNCH = 3;

    // COLUMNS OF MSITEACCOUNTS (Sended from MultiSite)
    private const int AM_MOVEMENT_ID = 0;
    private const int AM_STATUS = 1;      // 0: OK
    private const int AM_ACCOUNT_ID = 2;
    private const int AC_MS_POINTS_SEQ_ID = 3;
    private const int AC_POINTS = 4;
    private const int AC_POINTS_STATUS = 5;
    private const int AC_HOLDER_LEVEL = 6;
    private const int AC_HOLDER_LEVEL_ENTERED = 7;
    private const int AC_HOLDER_LEVEL_EXPIRATION = 8;
    private const int LOCAL_PENDING_POINTS = 9;

    #endregion

    #region Members

    static private Int32 m_last_sync_upload_points_tick = 0;
    static private Int32 m_last_sync_download_points_site_tick = 0;
    static private Int32 m_last_sync_download_points_all_tick = 0;

    static private Int32 m_wait_seconds_upload_points = 0;
    static private Int32 m_wait_seconds_download_points_site = 0;
    static private Int32 m_wait_seconds_download_points_all = 0;

    #endregion // Members

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Download points
    //
    //  PARAMS :
    //      - INPUT :
    //          - Multisite Task Type
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    public static Boolean SiteTask_DownloadPoints(TYPE_MULTISITE_SITE_TASKS SiteTask)
    {
      DataTable _request_datatable;
      DataTable _reply_account_movements;
      DataRow _dr;
      Int32 _site_id;
      Int64 _local_sequence_value;
      Boolean _reporting;
      Int64 _ellapsed;
      Int64 _wait_ticks;
      Int32 _wait_seconds;
      Boolean _is_task_enabled;
      DataTable _dt_center_sequences;
      Int64 _center_sequence_value;
      Int32 _max_rows;

      if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site)
      {
        _ellapsed = Misc.GetElapsedTicks(m_last_sync_download_points_site_tick);
        _wait_ticks = (m_wait_seconds_download_points_site * 1000);
      }
      else
      {
        _ellapsed = Misc.GetElapsedTicks(m_last_sync_download_points_all_tick);
        _wait_ticks = (m_wait_seconds_download_points_all * 1000);
      }

      if (_ellapsed < _wait_ticks)
      {
        return true;
      }

      //
      // Select "Last Known Sequence"
      // Request From "Sequence"
      // Save Changes
      // Save "Last Received Sequence" of this Site 
      //

      _wait_seconds = 0;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

            return false;
          }

          if (!_is_task_enabled)
          {
            //Log.Error("Sync Download Points not enable");

            return true;
          }

          _db_trx.Commit();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(SiteTask, out _reporting, out _wait_seconds, out _local_sequence_value, out _max_rows, _db_trx.SqlTransaction))
          {
            Log.Error(" Error Reading MS_SITE_SYNCH_CONTROL. Read_ReportingFlagValues.");

            return false;
          }

          if (!_reporting)
          {
            return true;
          }
        }

        if (!MultiSiteCommon.Trx_GetCenterLastSequences(out _dt_center_sequences))
        {
          Log.Error(" Could not get last center sequences. Trx_GetCenterLastSequences.");

          return false;
        }

        _center_sequence_value = -1;
        foreach (DataRow _dr_seq in _dt_center_sequences.Rows)
        {
          if ((Int32)_dr_seq[0] == (Int32)WSI.Common.SequenceId.TrackPointChanges)
          {
            _center_sequence_value = (Int64)_dr_seq[1];

            break;
          }
        }

        if (_center_sequence_value < 0)
        {
          Log.Error(" Could not get last center sequence. DownloadPouints() ");

          return false;
        }

        if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _center_sequence_value, _db_trx.SqlTransaction))
            {
              Log.Error("WWP_MultiSiteTask.Site_SetCenterSequence(Points) failed!");

              return false;
            }
            _db_trx.Commit();
          }
        }

        while (_local_sequence_value <= _center_sequence_value)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _site_id = 0;
            if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site)
            {
              _site_id = Site.Id;
            }

            _request_datatable = new DataTable();
            _request_datatable.Columns.Add("SITE_ID", System.Type.GetType("System.Int32"));
            _request_datatable.Columns.Add("POINTS_SEQ_ID", System.Type.GetType("System.Int64"));

            _dr = _request_datatable.NewRow();
            _dr[0] = _site_id;
            _dr[1] = _local_sequence_value;
            _request_datatable.Rows.Add(_dr);

            _reply_account_movements = new DataTable();
            if (!Trx_DownloadPoints(_request_datatable, out _reply_account_movements))
            {
              Log.Error(" Could not synchronize points. Trx_DownloadPoints");

              return false;
            }

            if (!Update_PointsAccountMovements(_reply_account_movements, _db_trx.SqlTransaction))
            {
              Log.Error(" Could not update the account points with pending movements. Update_PointsAccountMovements");

              return false;
            }

            if (_reply_account_movements.Rows.Count <= 0)
            {
              break;
            }

            _local_sequence_value = (Int64)_reply_account_movements.Rows[_reply_account_movements.Rows.Count - 1]["AC_MS_POINTS_SEQ_ID"];

            if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All)
            {
              if (_local_sequence_value > _center_sequence_value)
              {
                _center_sequence_value = Math.Max(_center_sequence_value, _local_sequence_value);
                if (!WWP_MultiSiteTask.Site_SetCenterSequence(SiteTask, _center_sequence_value, _db_trx.SqlTransaction))
                {
                  Log.Error("WWP_MultiSiteTask.Site_SetCenterSequence(Points) failed!");

                  return false;
                }
              }
            }

            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, true, _local_sequence_value, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

              return false;
            }

            _db_trx.Commit();

            if (!_is_task_enabled)
            {
              return true;
            }
          } // using
        } // while

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (SiteTask == TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site)
        {
          m_last_sync_download_points_site_tick = Environment.TickCount;
          m_wait_seconds_download_points_site = _wait_seconds;
        }
        else
        {
          m_last_sync_download_points_all_tick = Environment.TickCount;
          m_wait_seconds_download_points_all = _wait_seconds;
        }

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(SiteTask, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");
            }
            else
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      return false;

    } // DownloadPoints

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize points
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    static public Boolean SynchPoints()
    {
      DataTable _request_account_movements;
      DataTable _reply_account_movements;
      Int64 _sequence_id;
      Boolean _reporting;
      Int64 _ellapsed;
      Int32 _wait_seconds;
      Boolean _is_task_enabled;
      Int32 _max_rows;
      DataRow[] _reply_am_status_ok;
      DataRow[] _reply_am_status_not_ok;
      StringBuilder _sb_warning;

      _ellapsed = Misc.GetElapsedTicks(m_last_sync_upload_points_tick);
      if (_ellapsed < (m_wait_seconds_upload_points * 1000))
      {
        return true;
      }

      _wait_seconds = 0;
      try
      {
        while (true)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // set reporting flag
            if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
            {
              Log.Error(" Can't set reporting_flag. Set_ReportingFlag");

              return false;
            }

            if (!_is_task_enabled)
            {
              //Log.Error("Sync Points not enable .Set_ReportingFlag");

              return true;
            }

            _db_trx.Commit();
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!WWP_MultiSiteTask.Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements, out _reporting, out _wait_seconds, out _sequence_id, out _max_rows, _db_trx.SqlTransaction))
            {
              Log.Error(" Error Reading MS_SITE_SYNCH_CONTROL. Read_ReportingFlagValues.");

              return false;
            }
            if (!_reporting)
            {
              return false;
            }

            if (!Read_PendingAccountMovements(_max_rows, out _request_account_movements, _db_trx.SqlTransaction))
            {
              Log.Error(" Could not read the account movements. Read_PendingAccountMovements");

              return false;
            }
            /// TODO si se quita bucle sin fin.

            if (!Trx_SynchPoints(_request_account_movements, out _reply_account_movements))
            {
              Log.Error(" Could not synchronize points. Trx_SynchPoints");

              return false;
            }

            if (_request_account_movements.Rows.Count == 0)
            {
              return true;
            }

            _reply_am_status_ok = _reply_account_movements.Select("AM_STATUS = 0");

            if (!Update_PointsAccountMovements(_reply_am_status_ok, _db_trx.SqlTransaction))
            {
              Log.Error(" Could not update the account points with pending movements. Update_Account_Pending_Points");

              return false;
            }

            _db_trx.Commit();

            // uncheck of movement have benn uploaded             
            if (_reply_am_status_ok.Length != _request_account_movements.Rows.Count)
            {
              _sb_warning = new StringBuilder();

              _reply_am_status_not_ok = _reply_account_movements.Select("AM_STATUS <> 0");
              _sb_warning.AppendLine(" Not updated all movements. Number Movements with error status: " + _reply_am_status_not_ok.Length.ToString());
              if (_reply_am_status_not_ok.Length > 0)
              {
                _sb_warning.Append("         Sample movement --> MovementId:" + _reply_am_status_not_ok[0][AM_MOVEMENT_ID].ToString());
                _sb_warning.Append(", AccountId:" + _reply_am_status_not_ok[0][AM_ACCOUNT_ID].ToString());
              }
              Log.Warning(_sb_warning.ToString());

              // TODO:  Make log..
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_upload_points_tick = Environment.TickCount;
        m_wait_seconds_upload_points = _wait_seconds;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error(" Can't set reporting_flag. Set_ReportingFlag");
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return false;
    } // SynchPoints

    #endregion // Public Functions

    #region Database Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Update Accounts Points Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReplyAccountMovements 
    //          - Trx   
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:         Executed succesfully
    //          - False:        Executed unsuccesfully
    //
    static public Boolean Update_PointsAccountMovements(DataRow[] ReplyAccountMovements, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_PointsInAccount ");
        _sb.AppendLine("  @pMovementId ");
        _sb.AppendLine(", @pAccountId ");
        _sb.AppendLine(", @pStatus ");
        _sb.AppendLine(", @pPointsSequenceId ");
        _sb.AppendLine(", @pPoints ");
        _sb.AppendLine(", @pPlayerTrackingMode ");
        _sb.AppendLine(", @pBucketId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // Parameters
          _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt).SourceColumn = "AM_MOVEMENT_ID";
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AM_ACCOUNT_ID";
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "AM_STATUS";
          _cmd.Parameters.Add("@pPointsSequenceId", SqlDbType.BigInt).SourceColumn = "AC_MS_POINTS_SEQ_ID";
          _cmd.Parameters.Add("@pPoints", SqlDbType.Money).SourceColumn = "AC_POINTS";
          _cmd.Parameters.Add("@pPlayerTrackingMode", SqlDbType.Int).Value = (Int32)CommonMultiSite.GetPlayerTrackingMode();
          _cmd.Parameters.Add("@pBucketId", SqlDbType.BigInt).SourceColumn = "CBU_BUCKET_ID";

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
            _sql_adapt.UpdateBatchSize = 500;
            _sql_adapt.Update(ReplyAccountMovements);
          }
        } // SqlCommand

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Update_PointsAccountMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Update Accounts Points Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReplyAccountMovements
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:         Executed succesfully
    //          - False:        Executed unsuccesfully
    //
    static public Boolean Update_PointsAccountMovements(DataTable ReplyAccountMovements, SqlTransaction Trx)
    {
      return Update_PointsAccountMovements(ReplyAccountMovements.Select(""), Trx);
    } // Update_PointsAccountMovements


    //------------------------------------------------------------------------------
    // PURPOSE : Read Accounts Points Movements of Site.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True:         Executed succesfully
    //          - False:        Executed unsuccesfully
    //
    static public Boolean Read_PendingAccountMovements(Int32 MaxRows, out DataTable AccountMovements, SqlTransaction Trx)
    {
      StringBuilder _sb = new StringBuilder();
      AccountMovements = new DataTable("ACCOUNT_MOVEMENTS");

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT    TOP (@pMAXRows)      ");
        _sb.AppendLine("         AM_MOVEMENT_ID       ");
        _sb.AppendLine("       , AM_PLAY_SESSION_ID   ");
        _sb.AppendLine("       , AM_ACCOUNT_ID        ");
        _sb.AppendLine("       , AM_TERMINAL_ID       ");
        _sb.AppendLine("       , AM_WCP_SEQUENCE_ID   ");
        _sb.AppendLine("       , AM_WCP_TRANSACTION_ID");
        _sb.AppendLine("       , AM_DATETIME          ");
        _sb.AppendLine("       , AM_TYPE              ");
        _sb.AppendLine("       , AM_INITIAL_BALANCE   ");
        _sb.AppendLine("       , AM_SUB_AMOUNT        ");
        _sb.AppendLine("       , AM_ADD_AMOUNT        ");
        _sb.AppendLine("       , AM_FINAL_BALANCE     ");
        _sb.AppendLine("       , AM_CASHIER_ID        ");
        _sb.AppendLine("       , AM_CASHIER_NAME      ");
        _sb.AppendLine("       , AM_TERMINAL_NAME     ");
        _sb.AppendLine("       , AM_OPERATION_ID      ");
        _sb.AppendLine("       , AM_DETAILS           ");
        _sb.AppendLine("       , AM_REASONS           ");
        _sb.AppendLine(" FROM   ACCOUNT_MOVEMENTS AM ");
        _sb.AppendLine("INNER   JOIN MS_SITE_PENDING_ACCOUNT_MOVEMENTS MS_AM");
        _sb.AppendLine("   ON   MS_AM.SPM_MOVEMENT_ID = AM.AM_MOVEMENT_ID");
        _sb.AppendLine("ORDER BY   MS_AM.SPM_MOVEMENT_ID  ASC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMAXRows", SqlDbType.Int).Value = MaxRows;
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(AccountMovements);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Read_PendingAccountMovements

    #endregion // Database Functions

    #region Transactions

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize points (Upload movements)
    //
    //  PARAMS :
    //      - INPUT : 
    //          - RequestAccountMovements    
    //
    //      - OUTPUT :  
    //          - ReplyAccountMovements 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    private static Boolean Trx_SynchPoints(DataTable RequestAccountMovements, out DataTable ReplyAccountMovements)
    {
      return Trx_Points(WWP_TableTypes.TABLE_TYPE_POINTS_ACCOUNT_MOVEMENTS, RequestAccountMovements, out ReplyAccountMovements);

    } // Trx_SynchPoints

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize points (Download)
    //
    //  PARAMS :
    //      - INPUT : 
    //          - RequestDataTable    
    //
    //      - OUTPUT :  
    //          - ReplyAccountMovements 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    private static Boolean Trx_DownloadPoints(DataTable RequestDataTable, out DataTable ReplyAccountMovements)
    {
      return Trx_Points(WWP_TableTypes.TABLE_TYPE_DOWNLOAD_POINTS, RequestDataTable, out ReplyAccountMovements);

    } // Trx_DownloadPoints

    //------------------------------------------------------------------------------
    // PURPOSE : Synchronize points (Upload/Download)
    //
    //  PARAMS :
    //      - INPUT : 
    //          - TableType    
    //          - RequestAccountMovements    
    //
    //      - OUTPUT :  
    //          - ReplyAccountMovements 
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    private static Boolean Trx_Points(WWP_TableTypes TableType, DataTable RequestDataTable, out DataTable ReplyAccountMovements)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;

      ReplyAccountMovements = new DataTable();

      try
      {
        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.Type = TableType;
        _wwp_cmd_req.DataSet = new DataSet();
        _wwp_cmd_req.DataSet.Tables.Add(RequestDataTable);

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("Trx_Points: Timeout or Failed! TableType: " + TableType.ToString());

          return false;
        }

        if (_wwp_response.MsgHeader.ResponseCode != WWP_ResponseCodes.WWP_RC_OK)
        {
          Log.Error("Trx_Points Failed. ResponseCode: " + _wwp_response.MsgHeader.ResponseCode.ToString() + ",  TableType: " + TableType.ToString());

          return false;
        }

        _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
        ReplyAccountMovements = _wwp_cmd_reply.DataSet.Tables[0];

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_Points

    #endregion // Transactions
  }
}
