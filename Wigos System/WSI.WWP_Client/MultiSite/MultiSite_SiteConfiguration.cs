//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : MultiSite_SiteConfiguration.cs
//
//   DESCRIPTION : MultiSite_SiteConfiguration class
//
//        AUTHOR: Dani Dominguez
// 
// CREATION DATE: 05-MAY-2013
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-MAY-2013 DDM    First release.
//                    Added SynchLoyaltyProgram
// 18-ABR-2013 JML    Change SynchLoyaltyProgram by SynchConfiguration
// 04-AUG-2014 JBC/MPO    WIG-1131: Download for enable/disable document types
// 05-DIC-2014 DCS    Added Task Download Lcd Messages
// 14-JAN-2015 DCS    Added Task Upload Handpays
// 17-MAR-2015 ANM    Added Task Upload Sells & buys
// 28-APR-2015 DCS    Multisite Multicurrency -> Update currencies configuration
// 02-FEB-2015 FAV    Added lines in Log file for detect errors (Bug 8915:Error en el servicio WWP_site)
// 03-FEB-2016 JML    Bug 8943*: MultiSite MultiCurrency:Accounts Information: Identification types
// 03-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronización MultiSite: Tablas Customer Buckets
// 14-MAY-2016 ETP    Fixed Bug 14495:Ref.394: Picos de CPU a causa de query SQL
// 27-JUL-2016 FAV    Fixed Bug 16029: IdentificationTypes in MultiSite doesn't insert new records
// 17-NOV-2016 JML    Fixed Bug 15545: "WWP Site" service log: errors after connecting sites to a MultiSite environment
// 31-MAR-2017 MS     WIGOS-411 Creditlines
// 14-FEB-2018 JML    Fixed Bug 31531:WIGOS-7926 [Ticket #11970] Pagos manuales
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using WSI.Common;
using System.Data.SqlClient;
using WSI.WWP;
using WSI.WWP_Client;

namespace WSI.WWP_SiteService.MultiSite
{
  class MultiSite_SiteConfiguration
  {
    #region Constants

    const Int32 COLUMN_GP_GROUP_KEY = 0;
    const Int32 COLUMN_GP_SUBJECT_KEY = 1;
    const Int32 COLUMN_GP_KEY_VALUE = 2;
    const Int32 COLUMN_GP_MS_DOWNLOAD_TYPE = 3;

    const Int32 COLUMN_IDT_ID = 0;
    const Int32 COLUMN_IDT_ENABLED = 1;    
    const Int32 COLUMN_IDT_ORDER = 2;    
    const Int32 COLUMN_IDT_NAME = 3;    
    const Int32 COLUMN_IDT_COUNTRY_ISO_CODE_2 = 4;    

    #endregion // Constants

    #region Members

    static private Int32 m_last_sync_general_params_tick = 0;
    static private Int32 m_wait_seconds_general_params = 0;

    #endregion // Members

    //------------------------------------------------------------------------------
    // PURPOSE : Synch General Params from MultiSite 
    //
    //  PARAMS :
    //      - INPUT :
    //              
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    //
    public static Boolean SynchConfiguration()
    {
      DataSet _reply_configuration;
      Int64 _sequence_id;
      Boolean _reporting;
      Int64 _ellapsed;
      Int32 _wait_seconds;
      Boolean _is_task_enabled;
      Int32 _dummy_max_rows;

      _ellapsed = Misc.GetElapsedTicks(m_last_sync_general_params_tick);
      if (_ellapsed < (m_wait_seconds_general_params * 1000))
      {
        return true;
      }

      _wait_seconds = 0;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Set reporting flag
          if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration, true, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error("SynchConfiguration: Can't set reporting_flag. Set_ReportingFlag");

            return false;
          }

          if (!_is_task_enabled)
          {
            //Log.Error("Sync Configuration not enable");

            return true;
          }

          if (!Update_NumPendings(_db_trx.SqlTransaction))
          {
            Log.Error(" Not updated pending number from tasks. SynchTask.Update_NumPending()");

            return false;
          }

          _db_trx.Commit();

        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration, out _reporting, out _wait_seconds, out _sequence_id, out _dummy_max_rows, _db_trx.SqlTransaction))
          {
            Log.Error("SynchConfiguration: Error Reading MS_SITE_SYNCH_CONTROL. Read_ReportingFlagValues.");

            return false;
          }

          if (!_reporting)
          {
            return false;
          }

          if (!Trx_SynchConfiguration(out _reply_configuration, _db_trx.SqlTransaction))
          {
            Log.Warning(" SynchConfiguration.Trx_SynchConfiguration executed unsuccessfully");

            return false;
          }
          //
          // UPDATE Data
          //
          if (!Update_GeneralParams(_reply_configuration.Tables["GENERAL_PARAMS"], _db_trx.SqlTransaction))
          {
            Log.Warning(" SynchConfiguration.Update_GeneralParams executed unsuccessfully");

            return false;
          }

          //
          // UPDATE Identification Types
          //
          if (!Update_IdentificationTypes(_reply_configuration.Tables["IDENTIFICATION_TYPES"], _db_trx.SqlTransaction))
          {
            Log.Warning(" SynchConfiguration.Update_IdentificationTypes executed unsuccessfully");

            return false;
          }

          if (!Update_Tasks(_reply_configuration.Tables["TASKS"], _db_trx.SqlTransaction))
          {
            Log.Error(" Could not update tasks. Update_Tasks");

            return false;

          }

          //TODO: return interval, max rows, sequence..
          if (!Update_NumPendings(_db_trx.SqlTransaction))
          {
            Log.Error(" Not updated pending number from tasks. SynchTask.Update_NumPending()");

            return false;
          }

          _db_trx.Commit();

           return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_last_sync_general_params_tick = Environment.TickCount;
        m_wait_seconds_general_params = _wait_seconds;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!WWP_MultiSiteTask.Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration, false, -1, out _is_task_enabled, _db_trx.SqlTransaction))
          {
            Log.Error("SynchConfiguration: Can't set reporting_flag = false. Set_ReportingFlag");
          }
          else
          {
            _db_trx.Commit();
          }
        }
      }

      return false;

    }  // SynchConfiguration

    //------------------------------------------------------------------------------
    // PURPOSE : Update General Params from MultiSite 
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable:    
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    private static Boolean Update_GeneralParams(DataTable GeneralParams, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _rows_updated;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = @pGroupKey AND GP_SUBJECT_KEY =@pSubjectKey)");
        _sb.AppendLine("  INSERT INTO  GENERAL_PARAMS");
        _sb.AppendLine("              ( GP_GROUP_KEY ,GP_SUBJECT_KEY ,GP_KEY_VALUE)");
        _sb.AppendLine("       VALUES ( @pGroupKey   , @pSubjectKey  ,@pKeyValue  );");
        _sb.AppendLine("ELSE ");
        _sb.AppendLine("  UPDATE  GENERAL_PARAMS");
        _sb.AppendLine("     SET  GP_KEY_VALUE   = CASE @pMsDownloadType                                  ");
        _sb.AppendLine("                                WHEN @pTypeUpdate                                 ");  // Update Site:
        _sb.AppendLine("                                     THEN @pKeyValue                              ");  //      with value from Multisite   
        _sb.AppendLine("                                WHEN @pTypeUpdPrtyMultisite                       ");  // Update with Priority Multisite: 
        _sb.AppendLine("                                     THEN CASE WHEN ISNULL(@pKeyValue, '') = ''   ");  //      if MS value is null or empty
        _sb.AppendLine("                                                    THEN GP_KEY_VALUE             ");  //      leave actual value
        _sb.AppendLine("                                                    ELSE @pKeyValue END           ");  //      else put Multisite value
        _sb.AppendLine("                                WHEN @pTypeUpdPrtySite                            ");  // Update with Priority Site;
        _sb.AppendLine("                                     THEN CASE WHEN ISNULL(GP_KEY_VALUE, '') = '' ");  //       if Site value is null or empty 
        _sb.AppendLine("                                                    THEN @pKeyValue               ");  //       put Multisite value
        _sb.AppendLine("                                                    ELSE GP_KEY_VALUE END         ");  //       else leave actual value
        _sb.AppendLine("                                ELSE GP_KEY_VALUE                                 ");
        _sb.AppendLine("                           END                                                    ");
        _sb.AppendLine("   WHERE  GP_GROUP_KEY   = @pGroupKey ");
        _sb.AppendLine("     AND  GP_SUBJECT_KEY = @pSubjectKey ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGroupKey", SqlDbType.NVarChar, 50).SourceColumn = GeneralParams.Columns[COLUMN_GP_GROUP_KEY].ColumnName;
          _cmd.Parameters.Add("@pSubjectKey", SqlDbType.NVarChar, 50).SourceColumn = GeneralParams.Columns[COLUMN_GP_SUBJECT_KEY].ColumnName;
          _cmd.Parameters.Add("@pKeyValue", SqlDbType.NVarChar, 1024).SourceColumn = GeneralParams.Columns[COLUMN_GP_KEY_VALUE].ColumnName;
          _cmd.Parameters.Add("@pMsDownloadType", SqlDbType.Int).SourceColumn = GeneralParams.Columns[COLUMN_GP_MS_DOWNLOAD_TYPE].ColumnName;
          _cmd.Parameters.Add("@pTypeUpdate", SqlDbType.Int).Value = ParamatersTypeForSite.Download_Update;
          _cmd.Parameters.Add("@pTypeUpdPrtyMultisite", SqlDbType.Int).Value = ParamatersTypeForSite.Download_UpdatePriorityMultisite;
          _cmd.Parameters.Add("@pTypeUpdPrtySite", SqlDbType.Int).Value = ParamatersTypeForSite.Download_UpdatePrioritySite;

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _cmd;
            _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _da.UpdateBatchSize = 500;
            _rows_updated = _da.Update(GeneralParams);

            if (_rows_updated == GeneralParams.Rows.Count)
            {
              return true;
            }
            else
            {
              Log.Warning(string.Format(" Update_GeneralParams has returned false. Updated rows [{0}], Expected rows [{1}]", _rows_updated, GeneralParams.Rows.Count));
          }
        }
      }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // Update_GeneralParams


    //------------------------------------------------------------------------------
    // PURPOSE : Update Identification types enabled from MultiSite 
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable:    
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    private static Boolean Update_IdentificationTypes(DataTable IdentificationTypes, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _rows_updated;
      Boolean _is_enabled_multi_currency;
      Object _obj;

      _is_enabled_multi_currency = false;

      try
      {
        // Bug 15545:
        // It is necessary to inquiry the parameter with a "select", because it may have changed in the same transaction in which we are.
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CAST(CASE GP_KEY_VALUE WHEN 1 THEN 1 ELSE 0 END AS BIT) ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS                             ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY = 'MultiSite.Multicurrency'   ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'Enabled'                 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _obj = _sql_cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _is_enabled_multi_currency = (Boolean)_obj;
          }
        }

        if (_is_enabled_multi_currency)
        {
          return true;
        }

        if (IdentificationTypes == null)
        {
          return false;
        }
        _sb = new StringBuilder();
        _sb.AppendLine("IF NOT EXISTS (SELECT * FROM IDENTIFICATION_TYPES WHERE IDT_ID = @pID)");
        _sb.AppendLine("  INSERT INTO  IDENTIFICATION_TYPES");
        _sb.AppendLine("              ( IDT_ID ,IDT_ENABLED, IDT_ORDER, IDT_NAME, IDT_COUNTRY_ISO_CODE2)");
        _sb.AppendLine("       VALUES ( @pID ,@pEnabled, @pOrder, @pName, @pCountryIsoCode2 );");
        _sb.AppendLine("ELSE ");
        _sb.AppendLine("  UPDATE  IDENTIFICATION_TYPES ");
        _sb.AppendLine("     SET  IDT_ENABLED  = @pEnabled ");
        _sb.AppendLine("   WHERE  IDT_ID   = @pID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).SourceColumn = IdentificationTypes.Columns[COLUMN_IDT_ENABLED].ColumnName;
          _cmd.Parameters.Add("@pID", SqlDbType.Int).SourceColumn = IdentificationTypes.Columns[COLUMN_IDT_ID].ColumnName;
          _cmd.Parameters.Add("@pOrder", SqlDbType.Int).SourceColumn = IdentificationTypes.Columns[COLUMN_IDT_ORDER].ColumnName;
          _cmd.Parameters.Add("@pName", SqlDbType.NVarChar, 50).SourceColumn = IdentificationTypes.Columns[COLUMN_IDT_NAME].ColumnName;
          _cmd.Parameters.Add("@pCountryIsoCode2", SqlDbType.NVarChar, 2).SourceColumn = IdentificationTypes.Columns[COLUMN_IDT_COUNTRY_ISO_CODE_2].ColumnName;


          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _cmd;
            _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _da.UpdateBatchSize = 500;
            _rows_updated = _da.Update(IdentificationTypes);

            if (_rows_updated == IdentificationTypes.Rows.Count)
            {
              return true;
            }
            else
            {
              Log.Warning(string.Format(" Update_IdentificationTypes has returned false. Updated rows [{0}], Expected rows [{1}]", _rows_updated, IdentificationTypes.Rows.Count));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // Update_GeneralParams

    //------------------------------------------------------------------------------
    // PURPOSE : Request General Params from MultiSite 
    //
    //  PARAMS :
    //      - OUTPUT :
    //          - DataTable:    
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //   NOTES :
    private static Boolean Trx_SynchConfiguration(out DataSet ReplyConfiguration, SqlTransaction Trx)
    {
      WWP_Message _wwp_request;
      WWP_Message _wwp_response;
      WWP_MsgTable _wwp_cmd_req;
      WWP_MsgTableReply _wwp_cmd_reply;
      StringBuilder _sb;
      DataSet _request;

      _request = new DataSet();
      ReplyConfiguration = new DataSet();

      try
      {
        // DCS 19-MAY-2015
        //
        // To get the currencies use join with General Params because currency_exchange table is updated through window
        // "Cash -> Customize -> Bank Card / Check / Currency operations"  which not ever update the database and only General param is ever updated
        // 
        // Top 2 in the query was use to get only the first foreign currency. Updated in Center is only functional with one foreign currency at the moment
        //
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT ST_TASK_ID, ST_LOCAL_SEQUENCE_ID, ST_REMOTE_SEQUENCE_ID, ST_NUM_PENDING FROM MS_SITE_TASKS ");
        _sb.AppendLine(" ");
        _sb.AppendLine("     SELECT   CE_CURRENCY_ISO_CODE    ");        
        _sb.AppendLine("            , CASE T.SST_ID           ");
        _sb.AppendLine("                   WHEN 1 THEN 1      ");// NATIONAL CURRENCY  
        _sb.AppendLine("                          ELSE 0      ");// FOREIGN CURRENCY     
        _sb.AppendLine("                   END                ");       
        _sb.AppendLine("              AS CE_NATIONAL_CURRENCY ");      
        _sb.AppendLine("            , CE_DESCRIPTION          ");      
        _sb.AppendLine("            , CE_CHANGE               ");
        _sb.AppendLine("       FROM   CURRENCY_EXCHANGE       ");
        _sb.AppendLine(" INNER JOIN   (SELECT TOP 2 * FROM   dbo.SplitStringIntoTable((SELECT   GP_KEY_VALUE                                  ");
        _sb.AppendLine(" 								                                                 FROM   GENERAL_PARAMS                                ");
        _sb.AppendLine(" 													                                      WHERE   GP_GROUP_KEY = 'RegionalOptions'              ");
        _sb.AppendLine(" 														                                      AND   GP_SUBJECT_KEY = 'CurrenciesAccepted'),';',1) ");
        _sb.AppendLine(" 			 ) AS T                                                                       ");
        _sb.AppendLine(" 		ON   CE_CURRENCY_ISO_CODE = T.SST_VALUE                                         ");
        _sb.AppendLine("      WHERE   CE_TYPE = @pCurrencyType                                              ");
        
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = WSI.Common.CurrencyExchangeType.CURRENCY;
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(_request);
          }
        }
        _request.Tables[0].TableName = "TASKS";
        _request.Tables[1].TableName = "CURRENCIES";

        _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgTable);
        _wwp_cmd_req = (WWP_MsgTable)_wwp_request.MsgContent;
        _wwp_cmd_req.Type = WWP_TableTypes.TABLE_TYPE_CONFIGURATION;
        _wwp_cmd_req.Format = DataSetRawFormat.XML_ZLIB_BASE64;
        _wwp_cmd_req.DataSet = _request;

        if (!WWP_SiteRequest.SendAndReceive(_wwp_request, out _wwp_response))
        {
          Log.Message("WWP_SiteRequest.SendAndReceive Timeout or Failed!");

          return false;
        }

        switch (_wwp_response.MsgHeader.ResponseCode)
        {
          case WWP_ResponseCodes.WWP_RC_OK:
            _wwp_cmd_reply = (WWP_MsgTableReply)_wwp_response.MsgContent;
            ReplyConfiguration = _wwp_cmd_reply.DataSet;

            return true;
          //break;

          case WWP_ResponseCodes.WWP_RC_INVALID_SITE_ID:
            // ...
            break;

          default:
            // ...
            break;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // Trx_SynchConfiguration


    //------------------------------------------------------------------------------
    // PURPOSE : Update number of pending tasks
    //
    //  PARAMS :
    //      - INPUT : 
    //            - Trx 
    //      - OUTPUT :
    //            - ReplyTask
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    public static Boolean Update_NumPendings(SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        if (!MultiSite_SiteConfiguration.Update_NumPendingTimeStamp(Trx))
        {
          Log.Error(" Not updated pending number from tasks. Update_NumPending()");
        }

        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (ST_REMOTE_SEQUENCE_ID - ISNULL(ST_LOCAL_SEQUENCE_ID, 0))");
        _sb.AppendLine(" WHERE   ST_TASK_ID    IN (@pDownloadConfiguration, ");
        _sb.AppendLine("                           @pDownloadPoints_Site, ");
        _sb.AppendLine("                           @pDownloadPoints_All, ");
        _sb.AppendLine("                           @pDownloadPoints_Site, ");
        _sb.AppendLine("                           @pDownloadPersonalInfo_Site, ");
        _sb.AppendLine("                           @pDownloadPersonalInfo_All, ");
        _sb.AppendLine("                           @pDownloadProvidersGames, ");
        _sb.AppendLine("                           @pDownloadProviders,");
        _sb.AppendLine("                           @pUploadSellsAndBuys);");

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = 0 ");
        _sb.AppendLine(" WHERE   ST_NUM_PENDING < 0;");

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_MOVEMENTS) ");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadPointsMovements;"); //11

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNTS)");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadPersonalInfo;"); //12

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_DOCUMENTS)");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadAccountDocuments;"); //13

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_SITE_SYNCHRONIZE_ACCOUNTS) ");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pDownloadPersonalInfo_Card;"); //31

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM GAME_PLAY_SESSIONS) ");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadGamePlaySessions;"); //59

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_SITE_PENDING_LAST_ACTIVITY) ");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadLastActivity;"); //62

        _sb.AppendLine("UPDATE   MS_SITE_TASKS ");
        _sb.AppendLine("   SET   ST_NUM_PENDING = (SELECT COUNT(*) FROM MS_PENDING_TASK68_WORK_DATA) ");
        _sb.AppendLine(" WHERE   ST_TASK_ID     = @pUploadSellsAndBuys;"); //68

        // Replace one or more space with one space 
        String _sql = (new System.Text.RegularExpressions.Regex(@"(\s+|\n)")).Replace(_sb.ToString(), " ");

        using (SqlCommand _cmd = new SqlCommand(_sql, Trx.Connection, Trx))
        {

          foreach (Int32 _task_id in Enum.GetValues(typeof(TYPE_MULTISITE_SITE_TASKS)))
          {
            _cmd.Parameters.Add("@p" + Enum.GetName(typeof(TYPE_MULTISITE_SITE_TASKS), _task_id), SqlDbType.Int).Value = _task_id;
          }           

          _cmd.ExecuteNonQuery();

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Update_NumPending

    //------------------------------------------------------------------------------
    // PURPOSE : Update number of pending tasks
    //
    //  PARAMS :
    //      - INPUT : 
    //            - Trx 
    //      - OUTPUT :
    //            - ReplyTask
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    public static Boolean Update_NumPendingTimeStamp(SqlTransaction Trx)
    {
      Int32 _dummy_Int32;
      Int64 _dummy_Int64;

      return Update_NumPendingTimeStamp(TYPE_MULTISITE_SITE_TASKS.Unknown, out _dummy_Int64, out _dummy_Int32, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update number of pending tasks
    //
    //  PARAMS :
    //      - INPUT : 
    //            - TaskToUpd
    //            - Trx 
    //      - OUTPUT :
    //            - MaxSequenceId
    //            - Count
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    public static Boolean Update_NumPendingTimeStamp(TYPE_MULTISITE_SITE_TASKS TaskToUpd,
                                                     out Int64 MaxSequenceId,
                                                     out Int32 Count,
                                                     SqlTransaction Trx)
    {
      StringBuilder _sb;
      StringBuilder _sb_gen_upd;
      StringBuilder _sb_declare;
      DataTable _dt_gen_upd;
      String _str;
      SqlParameter _param_max_seq;
      SqlParameter _param_count;

      MaxSequenceId = 0;
      Count = 0;

      // It is necessary retrieve the max sequence ID although doesn't have any change
      _sb_declare = new StringBuilder();
      _sb_declare.AppendLine(" DECLARE @RemoteSeq AS BIGINT ");
      _sb_declare.AppendLine(" DECLARE @UpdateTask TABLE ( UP_TASK_ID   INT     ");
      _sb_declare.AppendLine("                           , UP_PENDING   INT     ");
      _sb_declare.AppendLine("                           , UP_LOCAL_SEQ BIGINT) ");
      _sb_declare.AppendLine(" ");

      _sb_gen_upd = new StringBuilder();
      _sb_gen_upd.AppendLine(" SET @RemoteSeq = ISNULL((SELECT   ST_REMOTE_SEQUENCE_ID  ");
      _sb_gen_upd.AppendLine("                            FROM   MS_SITE_TASKS  ");
      _sb_gen_upd.AppendLine("                           WHERE   ST_TASK_ID = #TASK_ID#), 0) ");

      _sb_gen_upd.AppendLine(" SELECT   @pMaxSeq = MAX(ISNULL(#FIELD#, 0)) ");
      _sb_gen_upd.AppendLine("        , @pCount = SUM(CASE WHEN #FIELD# > ISNULL(@RemoteSeq, 0) THEN 1 ELSE 0 END) ");
      _sb_gen_upd.AppendLine("   FROM   #TABLE#  ");
      _sb_gen_upd.AppendLine("  WHERE   #FIELD# >= CAST(ISNULL (@RemoteSeq,0) AS TIMESTAMP) ");
      _sb_gen_upd.AppendLine("    #MORE_WHERE# ");

      _sb_gen_upd.AppendLine("IF @pCount > 0");
      _sb_gen_upd.AppendLine("BEGIN");
      _sb_gen_upd.AppendLine("   INSERT INTO @UpdateTask(UP_TASK_ID, UP_PENDING, UP_LOCAL_SEQ) ");
      _sb_gen_upd.AppendLine("                    VALUES( #TASK_ID#,    @pCount,     @pMaxSeq)  ");
      _sb_gen_upd.AppendLine("END");
      _sb_gen_upd.AppendLine(" ");

      _dt_gen_upd = new DataTable();
      _dt_gen_upd.Columns.Add("", Type.GetType("System.String"));
      _dt_gen_upd.Columns.Add("", Type.GetType("System.String"));
      _dt_gen_upd.Columns.Add("", Type.GetType("System.Int32"));
      _dt_gen_upd.Columns.Add("", Type.GetType("System.String"));

      _dt_gen_upd.Rows.Add("SALES_PER_HOUR", "SPH_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour, "");
      _dt_gen_upd.Rows.Add("PLAY_SESSIONS", "PS_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions, "");
      _dt_gen_upd.Rows.Add("PROVIDERS", "PV_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadProviders, "");
      _dt_gen_upd.Rows.Add("BANKS", "BK_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadBanks, "");
      _dt_gen_upd.Rows.Add("AREAS", "AR_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadAreas, "");
      _dt_gen_upd.Rows.Add("TERMINALS", "TE_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadTerminals, "");
      _dt_gen_upd.Rows.Add("TERMINALS_CONNECTED", "TC_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected, "");
      _dt_gen_upd.Rows.Add("CASHIER_MOVEMENTS_GROUPED_BY_HOUR", "CM_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour, "");
      _dt_gen_upd.Rows.Add("ALARMS", "AL_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadAlarms, ""); // "AND AL_REPORTED >= DATEADD(m, -1, GETDATE())" );
      _dt_gen_upd.Rows.Add("HANDPAYS", "HP_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadHandpays, "");
      _dt_gen_upd.Rows.Add("CREDIT_LINES", "CL_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadCreditLines, "");
      _dt_gen_upd.Rows.Add("CREDIT_LINE_MOVEMENTS", "CLM_TIMESTAMP", TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements, "");

      _sb = new StringBuilder();
      _sb.AppendLine(_sb_declare.ToString());

      foreach (DataRow _dr in _dt_gen_upd.Rows)
      {
        _str = _sb_gen_upd.ToString();
        _str = _str.Replace("#TABLE#", _dr[0].ToString());
        _str = _str.Replace("#FIELD#", _dr[1].ToString());
        _str = _str.Replace("#TASK_ID#", _dr[2].ToString());
        _str = _str.Replace("#MORE_WHERE#", _dr[3].ToString());

        if (TaskToUpd == (TYPE_MULTISITE_SITE_TASKS)_dr[2])
        {
          _sb.Length = 0;
          _sb.AppendLine(_sb_declare.ToString());
          _sb.AppendLine(_str);
          break;
        }
        else
        {
          _sb.AppendLine(_str);
        }
      }

      _sb.AppendLine("     UPDATE   TASKS                                  ");
      _sb.AppendLine("        SET   ST_NUM_PENDING        = UP_PENDING     ");
      _sb.AppendLine("            , ST_LOCAL_SEQUENCE_ID  = UP_LOCAL_SEQ   ");
      _sb.AppendLine("       FROM   MS_SITE_TASKS AS TASKS                 ");
      _sb.AppendLine(" INNER JOIN   @UpdateTask ON ST_TASK_ID = UP_TASK_ID ");
      _sb.AppendLine("      WHERE   UP_PENDING > 0                         ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _param_max_seq = _cmd.Parameters.Add("@pMaxSeq", SqlDbType.BigInt);
          _param_max_seq.Direction = ParameterDirection.Output;

          _param_count = _cmd.Parameters.Add("@pCount", SqlDbType.Int);
          _param_count.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();

          if (TaskToUpd != TYPE_MULTISITE_SITE_TASKS.Unknown)
          {
            if (_param_max_seq.Value != DBNull.Value)
            {
              MaxSequenceId = (Int64)_param_max_seq.Value;
            }
            if (_param_count.Value != DBNull.Value)
            {
              Count = (Int32)_param_count.Value;
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update tasks from MultiSite
    //
    //  PARAMS :
    //      - INPUT : 
    //            - Tasks
    //            - Trx
    //      - OUTPUT :    
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    private static Boolean Update_Tasks(DataTable Tasks, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;
      try
      {
        // TODO : MAKE TO PROCEDURE
        _sb = new StringBuilder();
        _sb.AppendLine("IF @pTaskId IN(@pUploadPointsMovements,");
        _sb.AppendLine("               @pUploadPersonalInfo,");
        _sb.AppendLine("               @pDownloadConfiguration,");
        _sb.AppendLine("               @pDownloadPoints_All,");
        _sb.AppendLine("               @pDownloadPersonalInfo_All,");
        _sb.AppendLine("               @pUploadProvidersGames,");
        _sb.AppendLine("               @pDownloadProvidersGames,");
        _sb.AppendLine("               @pUploadGamePlaySessions,");
        _sb.AppendLine("               @pDownloadProviders,");
        _sb.AppendLine("               @pDownloadAccountDocuments,");
        _sb.AppendLine("               @pUploadAccountDocuments,");
        _sb.AppendLine("               @pUploadLastActivity,");
        _sb.AppendLine("               @pUploadSellsAndBuys)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS");
        _sb.AppendLine("     SET   ST_ENABLED            = @pEnabled ");
        _sb.AppendLine("         , ST_INTERVAL_SECONDS   = @pInterval");        
        _sb.AppendLine("         , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pForce = 1) THEN 0 ELSE @pSequenceRemoteFromMultiSite END ");// for local_sequence_id
        _sb.AppendLine("         , ST_MAX_ROWS_TO_UPLOAD = @pMaxRows  ");
        _sb.AppendLine("         , ST_START_TIME  	     = @pStartTime");
        _sb.AppendLine("  WHERE   ST_TASK_ID             = @pTaskId   ");
        _sb.AppendLine("END");
        
        //
        // TIMESTAMP IN SITE
        //
        _sb.AppendLine("ELSE IF @pTaskId IN (@pUploadSalesPerHour,");
        _sb.AppendLine("                     @pUploadPlaySessions,");
        _sb.AppendLine("                     @pUploadProviders,");
        _sb.AppendLine("                     @pUploadBanks,");
        _sb.AppendLine("                     @pUploadAreas,");
        _sb.AppendLine("                     @pUploadTerminals,");
        _sb.AppendLine("                     @pUploadTerminalsConnected,");
        _sb.AppendLine("                     @pUploadCashierMovementsGrupedByHour,");
        _sb.AppendLine("                     @pUploadAlarms,");
        _sb.AppendLine("                     @pUploadHandpays,");
        _sb.AppendLine("                     @pUploadCreditlines,");
        _sb.AppendLine("                     @pUploadCreditlineMovements)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS");
        _sb.AppendLine("     SET   ST_ENABLED            = @pEnabled ");
        _sb.AppendLine("         , ST_INTERVAL_SECONDS   = @pInterval");
        _sb.AppendLine("         , ST_REMOTE_SEQUENCE_ID = CASE WHEN (@pForce = 1) THEN 0 ELSE @pSequenceLocalFromMultiSite END ");// for remote_sequence_id
        _sb.AppendLine("         , ST_MAX_ROWS_TO_UPLOAD = @pMaxRows  ");
        _sb.AppendLine("         , ST_START_TIME  	     = @pStartTime");
        _sb.AppendLine("  WHERE   ST_TASK_ID             = @pTaskId   ");
        _sb.AppendLine("END");

        //
        // TIMESTAMP IN THE MULTISITE
        // 
        _sb.AppendLine("ELSE IF @pTaskId IN (@pDownloadMasterProfiles,@pDownloadCorporateUsers,@pDownloadAlarmPatterns,@pDownloadLcdMessages,@pDownloadBucketsConfig)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  UPDATE   MS_SITE_TASKS");
        _sb.AppendLine("     SET   ST_ENABLED            = @pEnabled ");
        _sb.AppendLine("         , ST_INTERVAL_SECONDS   = @pInterval");
        _sb.AppendLine("         , ST_NUM_PENDING        = @pNumPending ");
        _sb.AppendLine("         , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pForce = 1) THEN 0 ELSE @pSequenceRemoteFromMultiSite END ");// for local_sequence_id
        _sb.AppendLine("         , ST_REMOTE_SEQUENCE_ID = @pSequenceLocalFromMultiSite  ");// for remote_sequence_id
        _sb.AppendLine("         , ST_MAX_ROWS_TO_UPLOAD = @pMaxRows  ");
        _sb.AppendLine("         , ST_START_TIME  	     = @pStartTime");
        _sb.AppendLine("  WHERE   ST_TASK_ID             = @pTaskId   ");
        _sb.AppendLine("END");

        // Replace one or more space with one space 
        String _sql = (new System.Text.RegularExpressions.Regex(@"(\s+|\n)")).Replace(_sb.ToString(), " ");

        using (SqlCommand _cmd = new SqlCommand(_sql, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).SourceColumn = "ST_ENABLED";
          _cmd.Parameters.Add("@pInterval", SqlDbType.Int).SourceColumn = "ST_INTERVAL_SECONDS";
          _cmd.Parameters.Add("@pForce", SqlDbType.Bit).SourceColumn = "ST_FORCE_RESYNCH";
          _cmd.Parameters.Add("@pMaxRows", SqlDbType.Int).SourceColumn = "ST_MAX_ROWS_TO_UPLOAD";
          _cmd.Parameters.Add("@pStartTime", SqlDbType.Int).SourceColumn = "ST_START_TIME";
          _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).SourceColumn = "ST_TASK_ID";
          _cmd.Parameters.Add("@pSequenceRemoteFromMultiSite", SqlDbType.BigInt).SourceColumn = "ST_REMOTE_SEQUENCE_ID";
          _cmd.Parameters.Add("@pSequenceLocalFromMultiSite", SqlDbType.BigInt).SourceColumn = "ST_LOCAL_SEQUENCE_ID";
          _cmd.Parameters.Add("@pNumPending", SqlDbType.Int).SourceColumn = "ST_NUM_PENDING";

          foreach (Int32 _task_id in Enum.GetValues(typeof(TYPE_MULTISITE_SITE_TASKS)))
          {
            _cmd.Parameters.Add("@p" + Enum.GetName(typeof(TYPE_MULTISITE_SITE_TASKS), _task_id), SqlDbType.Int).Value = _task_id;
          }

          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
            _sql_adapt.UpdateBatchSize = 500;
            //_sql_adapt.ContinueUpdateOnError = true;
            _num_rows_updated = _sql_adapt.Update(Tasks);

            if (_num_rows_updated != Tasks.Rows.Count) 
            {
              Log.Warning(String.Format("Not updated all tasks. Expected: {0}, Updated: {1}", Tasks.Rows.Count, _num_rows_updated));
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // Update_Tasks
  }
}
