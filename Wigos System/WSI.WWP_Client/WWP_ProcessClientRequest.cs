//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_ProcessClientRequest.cs
// 
//   DESCRIPTION: WWP Process requests from clients as iPad terminals.
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 31-MAR-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-MAR-2011 AJQ    First release.
// 11-FEB-2013 ANG    Add support for blocked users.
//                    Update user's last activity.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WWP;

namespace WSI.WWP_SiteService
{
  static public partial class WWP_Process
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Process a Request from a client as iPad and return Response.
    //
    //  PARAMS :
    //      - INPUT :
    //          - WWP_Message Request
    //
    //      - OUTPUT :
    //          - WWP_Message Response
    //
    // RETURNS :
    //
    static public void ClientRequest(WWP_Message Request, WWP_Message Response)
    {
      Boolean _error;
      String _error_msg;
      String _username;
      String _password;
      Object _obj_user;
      Int32 _user_id;
      Int32 _profile_id;
      StringBuilder _sb;

      _username = Request.MsgHeader.Username;
      _password = Request.MsgHeader.Password;

      _error = true;
      _error_msg = "Unknown error";

      try
      {
        _sb = new StringBuilder();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine("SELECT   GU_USER_ID            ");
          _sb.AppendLine("       , GU_PROFILE_ID         ");
          _sb.AppendLine("  FROM   GUI_USERS             ");
          _sb.AppendLine(" WHERE   GU_USERNAME     = @p1 ");
          _sb.AppendLine("   AND   GU_BLOCK_REASON = @p2 ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = _username;
            _cmd.Parameters.Add("@p2", SqlDbType.Int).Value = GUI_USER_BLOCK_REASON.NONE;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_USER_NOT_AUTHORIZED);
              }
              _user_id = _reader.GetInt32(0);
              _profile_id = _reader.GetInt32(1);
            }
          }

          WSI.Common.PasswordPolicy _pwd;
          _pwd = new PasswordPolicy();
          if ( !_pwd.VerifyCurrentPassword (_user_id, _password, _db_trx.SqlTransaction ))
          {
            // username error
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_USER_WRONG_LOGIN);
          }

          // Get profile & Check profile form 
          _sb.Length = 0;
          _sb.AppendLine("SELECT   GPF_READ_PERM        ");
          _sb.AppendLine("  FROM   GUI_PROFILE_FORMS    ");
          _sb.AppendLine(" WHERE   GPF_PROFILE_ID = @p1 ");
          _sb.AppendLine("   AND   GPF_GUI_ID     = 104 ");
          _sb.AppendLine("   AND   GPF_FORM_ID    = 0   ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@p1", SqlDbType.Int).Value = _profile_id;

            _obj_user = _cmd.ExecuteScalar();

            if (_obj_user == null || !(Boolean)_obj_user)
            {
              // username error
              throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_USER_NOT_AUTHORIZED);
            }
          }

          // Update User Last Activity

          _sb.Length = 0;
          _sb.AppendLine("UPDATE   GUI_USERS                    ");
          _sb.AppendLine("   SET   GU_LAST_ACTIVITY = GETDATE() ");
          _sb.AppendLine(" WHERE   GU_USERNAME      = @p1       ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = _username;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_ERROR);
            }
          }

          _db_trx.Commit();
        } // DB_TRX

        using ( DB_TRX _db_trx = new DB_TRX())
        {
          switch (Request.MsgHeader.MsgType)
          {
            case WWP_MsgTypes.WWP_MsgSqlCommand:
              {
                WWP_ProcessRequest.SqlCommand(Request, Response, _db_trx.SqlTransaction);
                _db_trx.Rollback();
                _error = false;
              }
              break;

            default:
              _db_trx.Rollback();
              _error_msg = "Unexpected MsgType: " + Request.MsgHeader.MsgType.ToString();
              break;
          }
        }

      }
      catch (WWP_Exception _wwp_ex)
      {
        try
        {
          if (Response == null)
          {
            Response = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgError);
          }
          Response.MsgHeader.ResponseCode = _wwp_ex.ResponseCode;
          if (Response != null)
          {
            Response.MsgHeader.SequenceId = Request.MsgHeader.SequenceId;
          }
          _error = false;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }
      catch (Exception _ex)
      {
        _error_msg = _ex.Message;

        Log.Exception(_ex);
      }

      try
      {
        if (_error)
        {
          WWP_MsgError _content;

          _content = new WWP_MsgError();
          _content.Description = _error_msg;

          Response.MsgHeader.MsgType = WWP_MsgTypes.WWP_MsgError;
          Response.MsgContent = _content;
        }
      }
      catch
      {
      }
    } // ClientRequest

    //------------------------------------------------------------------------------
    // PURPOSE : To test client requests.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - WWP_Message
    //
    static public WWP_Message TestRequest()
    {
      WWP_Message _wwp_request;
      WWP_MsgSqlCommand _wwp_cmd;

      _wwp_request = WWP_Message.CreateMessage(WWP_MsgTypes.WWP_MsgSqlCommand);
      _wwp_cmd = (WWP_MsgSqlCommand)_wwp_request.MsgContent;
      _wwp_cmd.Format = DataSetRawFormat.XML_ZLIB_BASE64;
      //_wwp_cmd.SqlCommand = new SqlCommand("SELECT TOP 2 * FROM PLAY_SESSIONS WHERE PS_FINISHED >= @DateTime");
      //_wwp_cmd.SqlCommand.Parameters.Add("@BigInt", System.Data.SqlDbType.BigInt).Value = 1;
      //_wwp_cmd.SqlCommand.Parameters.Add("@Bit", System.Data.SqlDbType.Bit).Value = 0;
      //_wwp_cmd.SqlCommand.Parameters.Add("@DateTime", System.Data.SqlDbType.DateTime).Value = DateTime.Now.AddDays(-1000);
      //_wwp_cmd.SqlCommand.Parameters.Add("@Decimal", System.Data.SqlDbType.Decimal).Value = 1234.56789;
      //_wwp_cmd.SqlCommand.Parameters.Add("@Int", System.Data.SqlDbType.Int).Value = 1234;
      //_wwp_cmd.SqlCommand.Parameters.Add("@NVarChar", System.Data.SqlDbType.NVarChar).Value = "Texto 1234.56789 Special Chars: & < > ' \" ";

      String _sql_txt;
      // REAL SELECT 12 days
      //_sql_txt = "SELECT *    FROM      (        SELECT  PROVIDER              , BASE_DATE              , SUM(NUM_TERMINALS) AS NUM_TERMINALS              , SUM(PLAYED)        AS PLAYED              , SUM(PLAYED - WON)  AS NETWIN          FROM          ((  SELECT  TE_PROVIDER_ID AS PROVIDER                    , TC_DATE        AS BASE_DATE                    , COUNT(*)       AS NUM_TERMINALS                    , 0              AS PLAYED                    , 0              AS WON            FROM  TERMINALS_CONNECTED               , TERMINALS              WHERE  TC_DATE <= DATEADD(DAY, 0, CAST('2011/03/25 00:00:00' AS DATETIME))                 AND  TC_DATE >  DATEADD(DAY, -12, CAST('2011/03/25 00:00:00' AS DATETIME))                 AND  TC_CONNECTED = 1               AND  TC_TERMINAL_ID = TE_TERMINAL_ID                AND  TE_TYPE = 1                AND  TE_TERMINAL_TYPE IN (1, 3, 5)          GROUP BY  TE_PROVIDER_ID, TC_DATE           )               UNION           (              SELECT  PS_PROVIDER    AS PROVIDER                    , PS_BASE_DAY    AS BASE_DATE                    , 0              AS NUM_TERMINALS                    , SUM(PLAYED)    AS PLAYED                    , SUM(WON)       AS WON                FROM (   SELECT  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00') PS_BASE_DAY                               , ( SELECT  TE_PROVIDER_ID                                      FROM  TERMINALS                                     WHERE  TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID ) AS PS_PROVIDER                                , SUM(PS_TOTAL_CASH_IN)  AS PLAYED                                , SUM(PS_TOTAL_CASH_OUT) AS WON                            FROM  PLAY_SESSIONS                           WHERE  PS_STATUS <> 0 AND PS_PROMO = 0                            AND  PS_FINISHED <= DATEADD(DAY, 1, CAST('2011/03/25 08:00:00' AS DATETIME))                            AND  PS_FINISHED >  DATEADD(DAY, -11, CAST('2011/03/25 08:00:00' AS DATETIME))                       GROUP BY  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00')                               , PS_TERMINAL_ID                     )                      AS SUM_PLAY_SESSIONS            GROUP BY PS_PROVIDER, PS_BASE_DAY          ))           AS PROVIDERS_SALES        GROUP BY PROVIDER, BASE_DATE      )       AS PROVIDERS_STATS   WHERE NUM_TERMINALS > 0 ORDER BY PROVIDER, BASE_DATE";
      // SIMULATED SELECT 12 days
      //_sql_txt = "SELECT 'PROV_METRONIA_9999' AS PROVIDER, *    FROM      (        SELECT  BASE_DATE              , SUM(NUM_TERMINALS) AS NUM_TERMINALS              , SUM(PLAYED)        AS PLAYED              , SUM(PLAYED - WON)  AS NETWIN          FROM          ((  SELECT  TC_DATE        AS BASE_DATE                    , COUNT(*)       AS NUM_TERMINALS                    , 0              AS PLAYED                    , 0              AS WON            FROM  TERMINALS_CONNECTED               , TERMINALS              WHERE  TC_DATE <= DATEADD(DAY, 0, CAST('2011/03/25 00:00:00' AS DATETIME))                 AND  TC_DATE >  DATEADD(DAY, -12, CAST('2011/03/25 00:00:00' AS DATETIME))                 AND  TC_CONNECTED = 1               AND  TC_TERMINAL_ID = TE_TERMINAL_ID                AND  TE_TYPE = 1                AND  TE_TERMINAL_TYPE IN (1, 3, 5)          GROUP BY  TC_DATE           )               UNION           (              SELECT PS_BASE_DAY    AS BASE_DATE                    , 0              AS NUM_TERMINALS                    , SUM(PLAYED)    AS PLAYED                    , SUM(WON)       AS WON                FROM (   SELECT  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00') PS_BASE_DAY                               , SUM(PS_TOTAL_CASH_IN)  AS PLAYED                                , SUM(PS_TOTAL_CASH_OUT) AS WON                            FROM  PLAY_SESSIONS                           WHERE  PS_STATUS <> 0 AND PS_PROMO = 0                            AND  PS_FINISHED <= DATEADD(DAY, 1, CAST('2011/03/25 08:00:00' AS DATETIME))                            AND  PS_FINISHED >  DATEADD(DAY, -11, CAST('2011/03/25 08:00:00' AS DATETIME))                       GROUP BY  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00')                               , PS_TERMINAL_ID                     )                      AS SUM_PLAY_SESSIONS            GROUP BY PS_BASE_DAY          ))           AS PROVIDERS_SALES        GROUP by BASE_DATE      )       AS PROVIDERS_STATS   WHERE NUM_TERMINALS > 0 ORDER BY BASE_DATE";

      // REAL SELECT 49 days
      //_sql_txt = "  SELECT *    FROM      (        SELECT  PROVIDER              , BASE_DATE              , SUM(NUM_TERMINALS) AS NUM_TERMINALS              , SUM(PLAYED)        AS PLAYED              , SUM(PLAYED - WON)  AS NETWIN          FROM          ((  SELECT  TE_PROVIDER_ID AS PROVIDER                    , TC_DATE        AS BASE_DATE                    , COUNT(*)       AS NUM_TERMINALS                    , 0              AS PLAYED                    , 0              AS WON            FROM  TERMINALS_CONNECTED               , TERMINALS              WHERE  TC_DATE <= DATEADD(DAY, 6, CAST('2011/03/21 00:00:00' AS DATETIME))                 AND  TC_DATE >  DATEADD(DAY, -((7*6) + 1), CAST('2011/03/21 00:00:00' AS DATETIME))                 AND  TC_CONNECTED = 1               AND  TC_TERMINAL_ID = TE_TERMINAL_ID                AND  TE_TYPE = 1                AND  TE_TERMINAL_TYPE IN (1, 3, 5)          GROUP BY  TE_PROVIDER_ID, TC_DATE           )               UNION           (              SELECT  PS_PROVIDER    AS PROVIDER                    , PS_BASE_DAY    AS BASE_DATE                    , 0              AS NUM_TERMINALS                    , SUM(PLAYED)    AS PLAYED                    , SUM(WON)       AS WON                FROM (   SELECT  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00') PS_BASE_DAY                               , ( SELECT  TE_PROVIDER_ID                                      FROM  TERMINALS                                     WHERE  TE_TERMINAL_ID = PLAY_SESSIONS.PS_TERMINAL_ID ) AS PS_PROVIDER                                , SUM(PS_TOTAL_CASH_IN)  AS PLAYED                                , SUM(PS_TOTAL_CASH_OUT) AS WON                            FROM  PLAY_SESSIONS                           WHERE  PS_STATUS <> 0 AND PS_PROMO = 0                            AND  PS_FINISHED <= DATEADD(DAY, 7, CAST('2011/03/21 08:00:00' AS DATETIME))                            AND  PS_FINISHED >  DATEADD(DAY, -(7*6), CAST('2011/03/21 08:00:00' AS DATETIME))                       GROUP BY  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00')                               , PS_TERMINAL_ID                     )                      AS SUM_PLAY_SESSIONS            GROUP BY PS_PROVIDER, PS_BASE_DAY          ))           AS PROVIDERS_SALES        GROUP BY PROVIDER, BASE_DATE      )       AS PROVIDERS_STATS   WHERE NUM_TERMINALS > 0 ORDER BY PROVIDER, BASE_DATE";
      // SIMULATED SELECT 49 days

      _sql_txt = "  SELECT 'PROV_METRONIA_9999' AS PROVIDER, *    FROM      (        SELECT  BASE_DATE              , SUM(NUM_TERMINALS) AS NUM_TERMINALS              , SUM(PLAYED)        AS PLAYED              , SUM(PLAYED - WON)  AS NETWIN          FROM          ((  SELECT  TC_DATE        AS BASE_DATE                    , COUNT(*)       AS NUM_TERMINALS                    , 0              AS PLAYED                    , 0              AS WON            FROM  TERMINALS_CONNECTED               , TERMINALS              WHERE  TC_DATE <= DATEADD(DAY, 6, CAST('2011/03/21 00:00:00' AS DATETIME))                 AND  TC_DATE >  DATEADD(DAY, -((7*6) + 1), CAST('2011/03/21 00:00:00' AS DATETIME))                 AND  TC_CONNECTED = 1               AND  TC_TERMINAL_ID = TE_TERMINAL_ID                AND  TE_TYPE = 1                AND  TE_TERMINAL_TYPE IN (1, 3, 5)          GROUP BY  TC_DATE           )               UNION           (              SELECT  PS_BASE_DAY    AS BASE_DATE                    , 0              AS NUM_TERMINALS                    , SUM(PLAYED)    AS PLAYED                    , SUM(WON)       AS WON                FROM (   SELECT  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00') PS_BASE_DAY                              , SUM(PS_TOTAL_CASH_IN)  AS PLAYED                                , SUM(PS_TOTAL_CASH_OUT) AS WON                            FROM  PLAY_SESSIONS                           WHERE  PS_STATUS <> 0 AND PS_PROMO = 0                            AND  PS_FINISHED <= DATEADD(DAY, 7, CAST('2011/03/21 08:00:00' AS DATETIME))                            AND  PS_FINISHED >  DATEADD(DAY, -(7*6), CAST('2011/03/21 08:00:00' AS DATETIME))                       GROUP BY  DATEADD (DAY, DATEDIFF (DAY, '01/01/2007 08:00:00', PS_FINISHED), '01/01/2007 00:00:00')                               , PS_TERMINAL_ID                     )                      AS SUM_PLAY_SESSIONS            GROUP BY PS_BASE_DAY          ))           AS PROVIDERS_SALES        GROUP BY BASE_DATE      )       AS PROVIDERS_STATS   WHERE NUM_TERMINALS > 0 ORDER BY BASE_DATE";

      _wwp_cmd.SqlCommand = new SqlCommand(_sql_txt);

      return _wwp_request;
    } // TestRequest

  } // Class WWP_Process

} // WSI.WWP_SiteService
