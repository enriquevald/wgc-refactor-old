﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: NotificationAvilable.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Pablo Molina
// 
// CREATION DATE: 09-FEB-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2017 PDM    First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WinUp.Notifications
{
  public class NotificationAvilable
  {
    public NotificationAvilable()
    {
      Devices = new List<DeviceInfo>();
    }

    public Int64 Id { get; set; }
    public Int16 NroSegmentation { get; set; }
    public NotificationContent Content { get; set; }
    public List<DeviceInfo> Devices { get; set; }
  }

  public class NotificationContent
  {
    public string Title { get; set; }
    public string Message { get; set; }
    public string Target { get; set; }
    public Int64 TargetType { get; set; }
    public bool IsUrl { get; set; }
    public Int64 ImageId { get; set; }
    public string ImageExtension { get; set; }
  }

  public class DeviceInfo
  {
    public Int64 Id { get; set; }
    public string Token { get; set; }
  }

  public class NotificationsAvilables : List<NotificationAvilable>
  {

  }

  public class FCMData
  {
    public string Title { get; set; }
    public string Message { get; set; }
    public string Target { get; set; }
    public long TargetId { get; set; }
    public bool TargetIsURL { get; set; }
    public DateTime Date { get; set; }
    public string DateString { get; set; }
    public string Icon { get; set; }

    public string Image
    {
      get
      {
        if (!String.IsNullOrEmpty(ImageName))
        {
          return NotificationDispatcher.URL_RESOURCES + ImageName;
        }
        else
          return "";
      }
    }

    public long Status { get; set; }
    public long Id { get; set; }
    public string AppName { get; set; }
    public string[] Devices { get; set; }
    public long ImageId { get; set; }
    public string ImageExtension { get; set; }
    public string ImageName
    {
      get
      {
        if (ImageId > 0)
        {
          return "backend_" + ImageId + "_" + DateString + "." + this.ImageExtension;
        }
        else
          return "";
      }
    }
  }

  public class FCMResponse
  {
    public string status { get; set; }
    public string message { get; set; }
    public FCMResponseDetails FCMResponseDetails { get; set; }
    public List<FCMResponseDevicesDetails> FCMResponseDevicesDetailsList { get; set; }
  }

  public class FCMResponseDetails
  {
    public string multicast_id { get; set; }
    public int success { get; set; }
    public int failure { get; set; }
    public int canonical_ids { get; set; }
    public object results { get; set; }
  }

  public class FCMResponseDevicesDetails
  {
    public string error { get; set; }
    public string message_id { get; set; }
  }

}
