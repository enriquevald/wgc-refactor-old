﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Util.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Pablo Molina
// 
// CREATION DATE: 09-FEB-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2017 PDM    First release
//------------------------------------------------------------------------------
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WinUp.Notifications
{
  public static class Util
  {

    public enum NOTIFICATION_STATUS
    {
      TOSENT = 0,
      INPROCESS = 1,
      SEND_OK = 2,
      SEND_ERROR = 3
    }

    public enum NOTIFICATION_DEVICE_STATUS
    {
      TOSENT = 0,
      SEND_OK = 1,
      ERROR = 2
    }

    public static double GetCurrentTime()
    {
      TimeSpan _time;
      _time = new TimeSpan(WGDB.Now.Hour, WGDB.Now.Minute, WGDB.Now.Second);
      
      return _time.TotalSeconds;
    }

    public static bool IsCorrectDay(Int32 Weekdays)
    {

      String _str_binary;
      char[] _bit_array;
      DateTime dt;

      dt = DateTime.Now;

      _str_binary = Convert.ToString(Weekdays, 2);

      _str_binary = new string('0', 7 - _str_binary.Length) + _str_binary;

      _bit_array = _str_binary.ToCharArray();

      switch (dt.DayOfWeek)
      {
        case DayOfWeek.Friday:
          if (_bit_array[2] == '1') return true;
          break;
        case DayOfWeek.Monday:
          if (_bit_array[6] == '1') return true;
          break;
        case DayOfWeek.Saturday:
          if (_bit_array[1] == '1') return true;
          break;
        case DayOfWeek.Sunday:
          if (_bit_array[0] == '1') return true;
          break;
        case DayOfWeek.Thursday:
          if (_bit_array[3] == '1') return true;
          break;
        case DayOfWeek.Tuesday:
          if (_bit_array[5] == '1') return true;
          break;
        case DayOfWeek.Wednesday:
          if (_bit_array[4] == '1') return true;
          break;
        default:
          break;
      }

      return false;
    }

    public static void DecodeBirthdayFilter(Int32 SQLBirthdayFilter, out Int32 AgeFilter,
                                            out Int32 BirthdayFilter, out Int32 AgeFrom, out Int32 AgeTo)
    {
      AgeTo = 0;
      AgeFrom = 0;
      AgeFilter = 0;
      BirthdayFilter = SQLBirthdayFilter;
      if (SQLBirthdayFilter.ToString().Length > 1)
      {
        int _birthday_value;
        byte[] _bytes;

        _bytes = BitConverter.GetBytes(SQLBirthdayFilter);

        Int32.TryParse(_bytes[_bytes.Length - _bytes.Length].ToString(), out  _birthday_value);
        AgeFilter = _birthday_value;
        Int32.TryParse(_bytes[_bytes.Length - 1].ToString(), out  _birthday_value);
        BirthdayFilter = _birthday_value;
        Int32.TryParse(_bytes[_bytes.Length - 3].ToString(), out AgeFrom);
        Int32.TryParse(_bytes[_bytes.Length - 2].ToString(), out AgeTo);
      }
    }


    public static void DecodeCreatedAccountFilter(Int32 SQLCreatedAccountFilter, out Int32 CreatedAccountFilter,
                                        out Int32 WorkingDaysIncluded, out Int32 AnniversaryYearFrom, out Int32 AnniversaryYearTo)
    {
      WorkingDaysIncluded = 0;
      AnniversaryYearFrom = 0;
      AnniversaryYearTo = 0;
      CreatedAccountFilter = SQLCreatedAccountFilter;
      if (SQLCreatedAccountFilter.ToString().Length > 1)
      {
        int _filter_value;
        byte[] _bytes;

        _bytes = BitConverter.GetBytes(SQLCreatedAccountFilter);

        Int32.TryParse(_bytes[_bytes.Length - _bytes.Length].ToString(), out  _filter_value);
        CreatedAccountFilter = _filter_value;

        Int32.TryParse(_bytes[_bytes.Length - 3].ToString(), out WorkingDaysIncluded);
        Int32.TryParse(_bytes[_bytes.Length - 2].ToString(), out AnniversaryYearFrom);
        Int32.TryParse(_bytes[_bytes.Length - 1].ToString(), out AnniversaryYearTo);
      }
    }//DecodeCreatedAccountFilter


    public static string SerializeContentToJson(NotificationContent Content)
    {
      try
      {
        string json = JsonConvert.SerializeObject(Content, Formatting.Indented);
        return json;
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static NotificationContent DeserializeJsonToContent(String Content)
    {
      try
      {

        if (String.IsNullOrEmpty(Content)) { return null; }

        var content = JsonConvert.DeserializeObject<NotificationContent>(Content);
        return content;

      }
      catch (Exception)
      {
        return null;
      }
    }

    public static String CheckUrl(String Url)
    {

      if (Url.EndsWith("/"))
      {
        return Url;
      }
      else
      {
        return Url + "/";
      }
    }

    public static List<Dictionary<Int64, String>> GetDictionariesBySize(Dictionary<Int64, String> Dictionary, Int32 Size)
    {

      List<Dictionary<Int64, String>> _dictionaries;
      Dictionary<Int64, String> _dictionary;
      Boolean _createDic;
      Int32 _partialCount;

      _dictionaries = new List<Dictionary<Int64, String>>();
      _createDic = true;
      _dictionary = null;
      _partialCount = 0;

      if (Dictionary.Count <= Size)
      {
        _dictionaries.Add(Dictionary);
        return _dictionaries;
      }


      foreach (KeyValuePair<Int64, String> Item in Dictionary)
      {

        if (_createDic)
        {
          _dictionary = new Dictionary<Int64, String>();
          _dictionaries.Add(_dictionary);
        }

        _dictionary.Add(Item.Key, Item.Value);

        _partialCount++;

        if (_partialCount == Size)
        {
          _partialCount = 0;
          _createDic = true;
        }
        else
        {
          _createDic = false;
        }
      }

      return _dictionaries;
    }
  }
}
