﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Notification.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Pablo Molina
// 
// CREATION DATE: 09-FEB-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2017 PDM    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Timers;
using WSI.Common;
using WSI.WinUp.Notifications;

namespace WSI.WinUp.Notifications
{
  public class NotificationBuilder
  {

    static Thread syncThread = null;
    static System.Timers.Timer m_timer;
    static SimpleLogger _logger = new SimpleLogger("WINUP");
    public static void Init()
    {
      _logger.WriteLine("Init WinUp Notifications Builder Process");

      m_timer = new System.Timers.Timer();
      m_timer.Interval = 60000; // 1 min
      m_timer.Enabled = true;
      m_timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
      m_timer.Start();
      WSI.WinUp.Notifications.NotificationDispatcher.CompleteTaskEvent += NotificationDispatcher_CompleteTaskEvent;
    }

    protected static void timer_Elapsed(object sender, ElapsedEventArgs e)
    {
      syncThread = new Thread(new ThreadStart(RunProcess));
      syncThread.Start();
    }

    private static void RunProcess()
    {

      bool _result;

      _logger.WriteLine("WinUp Notification Process Builder running.");

      m_timer.Stop();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          var _notifications = GetNotifications(_db_trx.SqlTransaction);

          if (_notifications != null && _notifications.Count > 0)
          {
            if (GetAvilableDevices(_notifications, _db_trx.SqlTransaction))
            {
              foreach (var item in _notifications)
              {
                _result = InsertNotificationInTableToSent(item, _db_trx.SqlTransaction);

                if (!_result)
                {
                  _db_trx.SqlTransaction.Rollback();
                  _logger.WriteLine("WinUp Notification Builder Process finished with error.");
                  return;
                }
              }
            }
            else
            {
              _db_trx.SqlTransaction.Rollback();
              _logger.WriteLine("WinUp Notification Builder Process finished with error.");
              return;
            }

            _db_trx.SqlTransaction.Commit();
          }

        }

        _logger.WriteLine("WinUp Notification Builder Process finished.");

        //PDM 09-FEB-2017
        WSI.WinUp.Notifications.NotificationDispatcher.Init();

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification Builder Process finished with error. - " + _ex.Message);
        Log.Error("WinUp Notification Builder Process finished with error.");
        Log.Exception(_ex);
      }
    }

    static void NotificationDispatcher_CompleteTaskEvent(object sender, EventArgs e)
    {
      m_timer.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert the notificaction in a intermediate table
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Notification
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if the oparations was success
    //    - false: if the oparations was error
    private static bool InsertNotificationInTableToSent(NotificationAvilable Notification, SqlTransaction Trx)
    {

      StringBuilder _sql_insert;
      SqlCommand sql_command;
      Int32 _rows_affected;
      SqlParameter _param_id;
      Int64 _id;

      _sql_insert = new StringBuilder();

      try
      {
        _sql_insert.Append(" UPDATE mapp_notification  ");
        _sql_insert.Append(" SET  no_last_send_date=GETDATE()  ");
        _sql_insert.Append(" , no_sent=0 ");
        _sql_insert.Append(" WHERE no_id_notification=@id  ");

        sql_command = new SqlCommand(_sql_insert.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = Notification.Id;

        _rows_affected = sql_command.ExecuteNonQuery();

        if (_rows_affected > 0 && Notification.Devices.Count > 0)
        {
          _sql_insert.Length = 0;

          _sql_insert.Append("INSERT INTO mapp_notification_sent (");
          _sql_insert.Append(" ns_id_notification,");
          _sql_insert.Append(" ns_status,");
          _sql_insert.Append(" ns_created_date,");
          _sql_insert.Append(" ns_api_response,");
          _sql_insert.Append(" ns_data)");
          _sql_insert.Append(" VALUES (");
          _sql_insert.Append(" @ns_id_notification,0,GETDATE(),'',@data)");
          _sql_insert.Append("        SET @pId    = SCOPE_IDENTITY();   ");

          sql_command = new SqlCommand(_sql_insert.ToString());
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          sql_command.Parameters.Add("@ns_id_notification", SqlDbType.BigInt).Value = Notification.Id;
          sql_command.Parameters.Add("@data", SqlDbType.NVarChar).Value = Util.SerializeContentToJson(Notification.Content);

          _param_id = sql_command.Parameters.Add("@pId", SqlDbType.Int);
          _param_id.Direction = ParameterDirection.Output;

          _rows_affected = sql_command.ExecuteNonQuery();

          //SI HAY ENCABEZADO, GUARDO LOS DEVICES
          if (_rows_affected > 0 && _param_id.Value != DBNull.Value)
          {
            _id = Convert.ToInt64(_param_id.Value);

            _sql_insert.Length = 0;

            _sql_insert.Append(" INSERT INTO [mapp_notification_sent_device] ");
            _sql_insert.Append(" ([nsd_id_notification_sent]");
            _sql_insert.Append(" ,[nsd_id_device] ");
            _sql_insert.Append(" ,[nsd_device_token]");
            _sql_insert.Append(" ,[nsd_status]");
            _sql_insert.Append(" ,[nsd_retries])");
            _sql_insert.Append(" VALUES");
            _sql_insert.Append(" (@nsd_id_notification_sent");
            _sql_insert.Append(" ,@nsd_id_device");
            _sql_insert.Append(" ,@nsd_device_token");
            _sql_insert.Append(" ,0,0)");

            foreach (var item in Notification.Devices)
            {

              sql_command = new SqlCommand(_sql_insert.ToString());
              sql_command.Connection = Trx.Connection;
              sql_command.Transaction = Trx;

              sql_command.Parameters.Add("@nsd_id_notification_sent", SqlDbType.BigInt).Value = _id;
              sql_command.Parameters.Add("@nsd_id_device", SqlDbType.BigInt).Value = item.Id;
              sql_command.Parameters.Add("@nsd_device_token", SqlDbType.NVarChar).Value = item.Token;

              sql_command.ExecuteNonQuery();
            }

          }

        }

        return true;
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: InsertNotificationInTableToSent - " + _ex.Message);
        Log.Error("WinUp Notification. Exception in function: InsertNotificationInTableToSent");
        Log.Exception(_ex);
        return false;
      }
    }//InsertNotificationInTableToSent


    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices for all notifications
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Notifications
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if the oparations was success
    //    - false: if the oparations was error
    private static bool GetAvilableDevices(NotificationsAvilables Notifications, SqlTransaction Trx)
    {

      try
      {
        foreach (var item in Notifications)
        {
          switch (item.NroSegmentation)
          {
            case 0:
              item.Devices = GetDevicesByFilters(item.Id, Trx);
              break;
            case 1:
              item.Devices = GetDevicesByAccountId(item.Id, Trx);
              break;
            case 2:
              item.Devices = GetDevicesByGroupAccountId(item.Id, Trx);
              break;
            case 3:
              item.Devices = GetDevicesToUserNotRegisters(Trx);
              break;
            case 4:
              item.Devices = GetAllDevices(Trx);
              break;
            default:
              item.Devices = new List<DeviceInfo>();
              break;
          }
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }//GetAvilableDevices

    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices for multiples filters
    // 
    //  PARAMS:
    //      - INPUT:
    //        - IdNotification
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - List<DeviceInfo>: List of devices
    private static List<DeviceInfo> GetDevicesByFilters(Int64 IdNotification, SqlTransaction Trx)
    {

      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      List<DeviceInfo> _devices;
      DeviceInfo _device;
      Int32 _gender = 0;
      Int32 _birthday_filter = 0;
      Int32 _created_account_filter = 0;
      Int32 _level_filter = 0;
      Int32 _freq_filter_last_days = 0;
      Int32 _freq_filter_min_days = 0;
      Int32 _freq_filter_min_cash_in = 0;
      bool _is_vip;
      bool _has_rows;

      _sql_select = new StringBuilder();
      _devices = new List<DeviceInfo>();
      _has_rows = false;
      _is_vip = false;

      try
      {

        _sql_select.Append("SELECT ");
        _sql_select.Append("[ns_gender_filter]");
        _sql_select.Append(",[ns_birthday_filter]");
        _sql_select.Append(",[ns_created_account_filter]");
        _sql_select.Append(",[ns_level_filter]");
        _sql_select.Append(",[ns_freq_filter_last_days]");
        _sql_select.Append(",[ns_freq_filter_min_days]");
        _sql_select.Append(",[ns_freq_filter_min_cash_in]");
        _sql_select.Append(",[ns_is_vip]");
        _sql_select.Append(" FROM mapp_notification_segmentation ");
        _sql_select.Append(" WHERE ns_id_notification = @id");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = IdNotification;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {
          _has_rows = true;

          while (_sql_data_reader.Read())
          {
            _gender = _sql_data_reader["ns_gender_filter"] != DBNull.Value ? _sql_data_reader.GetInt32(0) : 0;
            _birthday_filter = _sql_data_reader["ns_birthday_filter"] != DBNull.Value ? _sql_data_reader.GetInt32(1) : 0;
            _created_account_filter = _sql_data_reader["ns_created_account_filter"] != DBNull.Value ? _sql_data_reader.GetInt32(2) : 0;
            _level_filter = _sql_data_reader["ns_level_filter"] != DBNull.Value ? _sql_data_reader.GetInt32(3) : 0;
            _freq_filter_last_days = _sql_data_reader["ns_freq_filter_last_days"] != DBNull.Value ? _sql_data_reader.GetInt32(4) : 0;
            _freq_filter_min_days = _sql_data_reader["ns_freq_filter_min_days"] != DBNull.Value ? _sql_data_reader.GetInt32(5) : 0;
            _freq_filter_min_cash_in = _sql_data_reader["ns_freq_filter_min_cash_in"] != DBNull.Value ? _sql_data_reader.GetInt32(6) : 0;
            _is_vip = _sql_data_reader["ns_is_vip"] != DBNull.Value ? _sql_data_reader.GetBoolean(7) : false;
          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();


        if (_has_rows)
        {
          var ids = GetIdAccountsByFilters(_gender, _birthday_filter, _created_account_filter, _level_filter, _freq_filter_last_days, _freq_filter_min_days, _freq_filter_min_cash_in, _is_vip, Trx);

          if (!String.IsNullOrEmpty(ids))
          {

            _sql_select.Length = 0;
            _sql_select.Append("SELECT nodev_id_device, nodev_last_generated_token FROM ");
            _sql_select.Append(" mapp_notification_device ");
            _sql_select.Append(" WHERE nodev_id_account IN  (" + ids + ")");

            sql_command = new SqlCommand(_sql_select.ToString());
            sql_command.Connection = Trx.Connection;
            sql_command.Transaction = Trx;

            _sql_data_reader = sql_command.ExecuteReader();


            if (_sql_data_reader.HasRows)
            {
              while (_sql_data_reader.Read())
              {
                _device = new DeviceInfo();
                _device.Id = _sql_data_reader.GetInt64(0);
                _device.Token = _device.Token = _sql_data_reader["nodev_last_generated_token"] != DBNull.Value ? _sql_data_reader.GetString(1).Trim() : "";
                if (!String.IsNullOrEmpty(_device.Token))
                {
                  _devices.Add(_device);
                }
              }
            }

            if (!_sql_data_reader.IsClosed)
              _sql_data_reader.Close();

          }

        }

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetDevicesByFilters - " + _ex.Message);
        Log.Error("WinUp Notification. Exception in function: GetDevicesByFilters");
        Log.Exception(_ex);
        throw new Exception();
      }

      return _devices;
    }//GetDevicesByFilters

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of accounts
    // 
    //  PARAMS:
    //      - INPUT:
    //        - GenderFilter
    //        - BirthdayFilter
    //        - CreateAccountFilter
    //        - LevelFilter
    //        - FreqFilterLastDays
    //        - FreqFilterMinDays
    //        - FreqFilterMinCashIn
    //        - IsVip
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - String: List of id accounts
    private static String GetIdAccountsByFilters(Int32 GenderFilter,
                                                  Int32 BirthdayFilter,
                                                  Int32 CreateAccountFilter,
                                                  Int32 LevelFilter,
                                                  Int32 FreqFilterLastDays,
                                                  Int32 FreqFilterMinDays,
                                                  Int32 FreqFilterMinCashIn,
                                                  Boolean IsVip, SqlTransaction Trx)
    {

      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      String _ids;
      Int32 _age_filter = 0;
      Int32 _birthday_age_from = 0;
      Int32 _birthday_age_to = 0;
      Int32 _created_account_working_days_included = 0;
      Int32 _created_account_anniversary_year_from = 0;
      Int32 _created_account_anniversary_year_to = 0;
      Int64 _account_id;
      DateTime _date_created;

      _sql_select = new StringBuilder();
      _ids = string.Empty;
      _account_id = 0;
      _date_created = DateTime.Now;

      try
      {
        Util.DecodeBirthdayFilter(BirthdayFilter, out _age_filter, out BirthdayFilter, out _birthday_age_from, out _birthday_age_to);
        Util.DecodeCreatedAccountFilter(CreateAccountFilter, out CreateAccountFilter, out _created_account_working_days_included, out _created_account_anniversary_year_from, out _created_account_anniversary_year_to);

        _sql_select.Length = 0;
        _sql_select.Append("SELECT ac_account_id,AC_CREATED FROM accounts ");

        _sql_select.Append(" WHERE 1=1 ");

        if (GenderFilter > 0)
        {
          _sql_select.Append(" AND (ac_holder_gender=" + GenderFilter + ")");
        }

        //public enum PROMOTION_BIRTHDAY_FILTER
        //{
        //  PROMOTION_BIRTHDAY_FILTER_NOT_SET = 0,
        //  PROMOTION_BIRTHDAY_FILTER_DAY_ONLY = 1,
        //  PROMOTION_BIRTHDAY_FILTER_WHOLE_MONTH = 2,
        //  PROMOTION_BIRTHDAY_FILTER_AGE_INCLUDED = 3,
        //  PROMOTION_BIRTHDAY_FILTER_AGE_EXCLUDED = 4
        //}

        //public enum PROMOTION_CREATED_ACCOUNT_FILTER
        //{
        //  PROMOTION_CREATED_ACCOUNT_FILTER_NOT_SET = 0,
        //  PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAY_ONLY = 1,
        //  PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAYS_INCLUDED = 2,
        //  PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_WHOLE_MONTH = 3,
        //  PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_DAY_ONLY = 4
        //}

        if (BirthdayFilter == 1)
        {
          _sql_select.Append(" AND (MONTH(ac_holder_birth_date) = MONTH(GETDATE()) AND DAY(ac_holder_birth_date) = DAY(GETDATE()))  ");
        }

        if (BirthdayFilter == 2)
        {
          _sql_select.Append(" AND (MONTH(ac_holder_birth_date) = MONTH(GETDATE())) ");
        }

        if (_age_filter == 3)
        {
          _sql_select.Append(" AND (datediff(yy, ac_holder_birth_date ,GETDATE()) >= " + _birthday_age_from + " AND datediff(yy, ac_holder_birth_date ,GETDATE()) <= " + _birthday_age_to + ")");
        }

        if (_age_filter == 4)
        {
          _sql_select.Append(" AND (datediff(yy, ac_holder_birth_date ,GETDATE()) <= " + _birthday_age_from + " OR datediff(yy, ac_holder_birth_date ,GETDATE()) >= " + _birthday_age_to + ")");
        }

        if (IsVip)
        {
          _sql_select.Append(" AND (ac_holder_is_vip=1) ");
        }

        if (LevelFilter != 0)
        {
          _sql_select.Append(GetLevelFilter(LevelFilter));
        }

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;


        if (FreqFilterLastDays > 0)
        {

          _sql_select.Append(" AND (ac_account_id IN (");
          _sql_select.Append("  SELECT AO_ACCOUNT_ID ");
          _sql_select.Append("  FROM ");
          _sql_select.Append("  ( ");
          _sql_select.Append("  SELECT  AO_ACCOUNT_ID, DATEDIFF (DAY, AO_DATETIME, @pTodayOpening ) FECHA , SUM(AO_AMOUNT) SUMA ");
          _sql_select.Append("   FROM   ACCOUNT_OPERATIONS AC with(index(IX_ao_code_date_account)) ");
          _sql_select.Append("   WHERE   AO_CODE IN (@pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo,@pNACashIn, @pNACashInPromo) ");
          _sql_select.Append("     AND   AO_DATETIME   >= DATEADD (DAY, -@pLastDays, @pTodayOpening ) ");
          _sql_select.Append("     AND   AO_DATETIME   <  @pTodayOpening  ");
          _sql_select.Append("     AND   AO_ACCOUNT_ID >0 ");
          _sql_select.Append("   GROUP BY DATEDIFF (DAY, AO_DATETIME, @pTodayOpening ) , AO_ACCOUNT_ID ");
          _sql_select.Append("   HAVING SUM(AO_AMOUNT)> @pMinDailyCashIn ");
          _sql_select.Append("  ) Z ");
          _sql_select.Append("  GROUP BY AO_ACCOUNT_ID ");
          _sql_select.Append("  HAVING COUNT(AO_ACCOUNT_ID)> @pMinDays ");
          _sql_select.Append("))");

          sql_command.Parameters.Add("@pMinDays", SqlDbType.Int).Value = FreqFilterMinDays;
          sql_command.Parameters.Add("@pLastDays", SqlDbType.Int).Value = FreqFilterLastDays;
          sql_command.Parameters.Add("@pMinDailyCashIn", SqlDbType.Money).Value = FreqFilterMinCashIn;
          sql_command.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();

          sql_command.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = OperationCode.CASH_IN;
          sql_command.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = OperationCode.PROMOTION;
          sql_command.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = OperationCode.MB_CASH_IN;
          sql_command.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = OperationCode.MB_PROMOTION;
          sql_command.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = OperationCode.NA_CASH_IN;
          sql_command.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = OperationCode.NA_PROMOTION;

          sql_command.CommandText = _sql_select.ToString();

        }

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {

            _account_id = _sql_data_reader.GetInt64(0);
            _date_created = _sql_data_reader.GetDateTime(1);

            if (IsCreatedAccountCompliant(DateTime.Now, _date_created, CreateAccountFilter, _created_account_working_days_included, _created_account_anniversary_year_from, _created_account_anniversary_year_to))
            {
              if (String.IsNullOrEmpty(_ids))
              {
                _ids = _account_id.ToString();
              }
              else
              {
                _ids += "," + _account_id.ToString();
              }

            }

          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

        return _ids;

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetIdAccountsByFilters - " + _ex.Message);

        Log.Error("WinUp Notification. Exception in function: GetIdAccountsByFilters");
        Log.Exception(_ex);
        throw new Exception();
      }
    }//GetIdAccountsByFilters

    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices for account id
    // 
    //  PARAMS:
    //      - INPUT:
    //        - IdNotification
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - List<DeviceInfo>: List of devices
    private static List<DeviceInfo> GetDevicesByAccountId(Int64 IdNotification, SqlTransaction Trx)
    {

      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      Int64 _id_account;
      List<DeviceInfo> _devices;
      DeviceInfo _device;

      _sql_select = new StringBuilder();
      _id_account = 0;
      _devices = new List<DeviceInfo>();

      try
      {

        _sql_select.Append("SELECT ns_id_account FROM ");
        _sql_select.Append(" mapp_notification_segmentation ");
        _sql_select.Append(" WHERE ns_id_notification = @id");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = IdNotification;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {
            _id_account = _sql_data_reader.GetInt64(0);
          }

        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();


        if (_id_account > 0)
        {
          _sql_select.Length = 0;
          _sql_select.Append("SELECT nodev_id_device, nodev_last_generated_token FROM ");
          _sql_select.Append(" mapp_notification_device ");
          _sql_select.Append(" WHERE nodev_id_account = @id_account");

          sql_command = new SqlCommand(_sql_select.ToString());
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          sql_command.Parameters.Add("@id_account", SqlDbType.BigInt).Value = _id_account;


          _sql_data_reader = sql_command.ExecuteReader();


          if (_sql_data_reader.HasRows)
          {
            while (_sql_data_reader.Read())
            {
              _device = new DeviceInfo();
              _device.Id = _sql_data_reader.GetInt64(0);
              _device.Token = _device.Token = _sql_data_reader["nodev_last_generated_token"] != DBNull.Value ? _sql_data_reader.GetString(1).Trim() : "";
              if (!String.IsNullOrEmpty(_device.Token))
              {
                _devices.Add(_device);
              }
            }
          }

          if (!_sql_data_reader.IsClosed)
            _sql_data_reader.Close();

        }

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetDevicesByAccountId - " + _ex.Message);

        Log.Error("WinUp Notification. Exception in function: GetDevicesByAccountId");
        Log.Exception(_ex);
        throw new Exception();
      }

      return _devices;

    }//GetDevicesByAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices for account id list
    // 
    //  PARAMS:
    //      - INPUT:
    //        - IdNotification
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - List<DeviceInfo>: List of devices
    private static List<DeviceInfo> GetDevicesByGroupAccountId(Int64 IdNotification, SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      String _ids_account;
      List<DeviceInfo> _devices;
      DeviceInfo _device;

      _sql_select = new StringBuilder();
      _ids_account = "";
      _devices = new List<DeviceInfo>();

      try
      {

        _sql_select.Append("SELECT ns_ids_accounts FROM ");
        _sql_select.Append(" mapp_notification_segmentation ");
        _sql_select.Append(" WHERE ns_id_notification = @id");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = IdNotification;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {
            _ids_account = _sql_data_reader.GetString(0);
          }

        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

        if (!String.IsNullOrEmpty(_ids_account))
        {
          _sql_select.Length = 0;
          _sql_select.Append("SELECT nodev_id_device, nodev_last_generated_token FROM ");
          _sql_select.Append(" mapp_notification_device ");
          _sql_select.Append(" WHERE nodev_id_account IN  (" + _ids_account + ")");

          sql_command = new SqlCommand(_sql_select.ToString());
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          _sql_data_reader = sql_command.ExecuteReader();


          if (_sql_data_reader.HasRows)
          {
            while (_sql_data_reader.Read())
            {
              _device = new DeviceInfo();
              _device.Id = _sql_data_reader.GetInt64(0);
              _device.Token = _device.Token = _sql_data_reader["nodev_last_generated_token"] != DBNull.Value ? _sql_data_reader.GetString(1).Trim() : "";
              if (!String.IsNullOrEmpty(_device.Token))
              {
                _devices.Add(_device);
              }
            }
          }

          if (!_sql_data_reader.IsClosed)
            _sql_data_reader.Close();

        }

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetDevicesByGroupAccountId - " + _ex.Message);

        Log.Error("WinUp Notification. Exception in function: GetDevicesByGroupAccountId");
        Log.Exception(_ex);
        throw new Exception();
      }

      return _devices;
    }//GetDevicesByGroupAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices when account id is 0
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - List<DeviceInfo>: List of devices
    private static List<DeviceInfo> GetDevicesToUserNotRegisters(SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      List<DeviceInfo> _devices;
      DeviceInfo _device;

      _sql_select = new StringBuilder();
      _devices = new List<DeviceInfo>();

      try
      {

        _sql_select.Append("SELECT nodev_id_device, nodev_last_generated_token FROM  ");
        _sql_select.Append(" mapp_notification_device ");
        _sql_select.Append(" WHERE  nodev_id_account=0");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        _sql_data_reader = sql_command.ExecuteReader();


        if (_sql_data_reader.HasRows)
        {
          while (_sql_data_reader.Read())
          {
            _device = new DeviceInfo();
            _device.Id = _sql_data_reader.GetInt64(0);
            _device.Token = _device.Token = _sql_data_reader["nodev_last_generated_token"] != DBNull.Value ? _sql_data_reader.GetString(1).Trim() : "";
            if (!String.IsNullOrEmpty(_device.Token))
            {
              _devices.Add(_device);
            }

          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetDevicesToUserNotRegisters");
        Log.Error("WinUp Notification. Exception in function: GetDevicesToUserNotRegisters");
        Log.Exception(_ex);
        throw new Exception();
      }

      return _devices;
    }//GetDevicesToUserNotRegisters

    //------------------------------------------------------------------------------
    // PURPOSE: Get all devices
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - List<DeviceInfo>: List of devices
    private static List<DeviceInfo> GetAllDevices(SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      List<DeviceInfo> _devices;
      DeviceInfo _device;

      _sql_select = new StringBuilder();
      _devices = new List<DeviceInfo>();

      try
      {

        _sql_select.Append("SELECT nodev_id_device, nodev_last_generated_token FROM ");
        _sql_select.Append(" mapp_notification_device ");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {
          while (_sql_data_reader.Read())
          {
            _device = new DeviceInfo();
            _device.Id = _sql_data_reader.GetInt64(0);
            _device.Token = _device.Token = _sql_data_reader["nodev_last_generated_token"] != DBNull.Value ? _sql_data_reader.GetString(1).Trim() : "";
            if (!String.IsNullOrEmpty(_device.Token))
            {
              _devices.Add(_device);
            }
          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetAllDevices");
        Log.Error("WinUp Notification. Exception in function: GetAllDevices");
        Log.Exception(_ex);
        throw new Exception();
      }

      return _devices;
    }//GetAllDevices

    //------------------------------------------------------------------------------
    // PURPOSE: Get the avilables notifications
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - NotificationsAvilables: List of notifications
    private static NotificationsAvilables GetNotifications(SqlTransaction Trx)
    {

      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      Int32 _weekdays;
      NotificationAvilable _notification;
      NotificationsAvilables _notifications;
      NotificationContent _item;
      Boolean _sent;

      _sql_select = new StringBuilder();
      _notifications = new NotificationsAvilables();
      _sent = false;

      try
      {

        //Obteniendo notificaciones =========================================================
        _logger.WriteLine("Getting WinUp Notifications - Start");

        _sql_select.Append("SELECT no_id_notification");
        _sql_select.Append(",no_title");
        _sql_select.Append(",no_message");
        _sql_select.Append(",no_target");
        _sql_select.Append(",no_target_identifier");
        _sql_select.Append(",no_target_isURL");
        _sql_select.Append(",no_last_send_date");
        _sql_select.Append(",ISNULL(no_segmentation,-1) as no_segmentation");
        _sql_select.Append(",ns_date_from");
        _sql_select.Append(",ns_date_to");
        _sql_select.Append(",ns_time_from");
        _sql_select.Append(",ns_time_to");
        _sql_select.Append(",ISNULL(ns_weekdays,0) as ns_weekdays");
        _sql_select.Append(",ISNULL(no_sent,0) as no_sent");

        _sql_select.Append(",ISNULL(IMG.[im_image_id], 0) as no_image_id");
        _sql_select.Append(",ISNULL(IMG.[im_file_name],'') as no_image_name ");

        _sql_select.Append(" FROM mapp_notification ");
        
        _sql_select.Append(" LEFT JOIN  MAPP_NOTIFICATION_SCHEDULE NS ");
        _sql_select.Append(" ON  MAPP_NOTIFICATION.NO_ID_NOTIFICATION = NS.NS_ID_NOTIFICATION ");

        _sql_select.Append(" LEFT JOIN  MAPP_IMAGES IMG ");
        _sql_select.Append(" ON  MAPP_NOTIFICATION.NO_IMAGE_ID = IMG.im_image_id ");

        _sql_select.Append(" WHERE ");
        _sql_select.Append(" (no_schedule = 1 AND ");
        _sql_select.Append(" no_status=1 AND ");
        _sql_select.Append(" (no_last_send_date IS NULL OR CAST(no_last_send_date AS DATE) < CAST(GETDATE() AS DATE)) AND ");
        _sql_select.Append(" CAST(ns_date_from AS DATE) <= CAST(GETDATE() AS DATE) AND ");
        _sql_select.Append(" CAST(ns_date_to AS DATE) >= CAST(GETDATE() AS DATE) AND ");
        _sql_select.Append(" ns_time_from <= @time_now ) ");

        _sql_select.Append(" OR (no_status = 1 AND no_sent = 1) ");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@time_now", SqlDbType.BigInt).Value = Util.GetCurrentTime();

        _sql_data_reader = sql_command.ExecuteReader();


        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {

            //Verifico que sea el dia de envio.
            _weekdays = _sql_data_reader.GetInt16(12);
            _sent = _sql_data_reader.GetBoolean(13);

            if (_sent || Util.IsCorrectDay(_weekdays))
            {

              _notification = new NotificationAvilable();
              _notification.Id = _sql_data_reader.GetInt64(0);
              _notification.NroSegmentation =  _sql_data_reader.GetInt16(7);
              _item = new NotificationContent();
              _item.Title = _sql_data_reader["no_title"] != DBNull.Value ? _sql_data_reader.GetString(1) : "";
              _item.Message = _sql_data_reader["no_message"] != DBNull.Value ? _sql_data_reader.GetString(2) : ""; ;
              _item.Target = _sql_data_reader["no_target"] != DBNull.Value ? _sql_data_reader.GetString(3) : "";
              _item.TargetType = _sql_data_reader["no_target_identifier"] != DBNull.Value ? _sql_data_reader.GetInt64(4) : 0;
              _item.IsUrl = _sql_data_reader["no_target_isURL"] != DBNull.Value ? _sql_data_reader.GetBoolean(5) : false;
              _item.ImageId = _sql_data_reader.GetInt64(14);
              _item.ImageExtension = String.IsNullOrEmpty(_sql_data_reader.GetString(15)) ? "" : System.IO.Path.GetExtension(_sql_data_reader.GetString(15));
              _notification.Content = _item;

              _notifications.Add(_notification);

            }

          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

        _logger.WriteLine("Getting WinUp Notifications - Finish. Status: Success");

        return _notifications;
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: GetNotifications");
        Log.Error("WinUp Notification. Exception in function: GetNotifications");
        Log.Exception(_ex);
        return null;
      }
    }//GetNotifications


    //------------------------------------------------------------------------------
    // PURPOSE: Get the string for a level filter
    // 
    //  PARAMS:
    //      - INPUT:
    //        - LevelFilter
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - string: part of sql where
    private static String GetLevelFilter(Int32 LevelFilter)
    {

      String _str_binary;
      char[] _bit_array;
      String _where;
      String _levels;

      _where = " AND (AC_HOLDER_LEVEL IN ({0}))";
      _levels = String.Empty;

      try
      {
        _str_binary = Convert.ToString(LevelFilter, 2);
        _str_binary = new string('0', 5 - _str_binary.Length) + _str_binary;

        _bit_array = _str_binary.ToCharArray();

        if (_bit_array[4] == '1')
        {
          //0
          _levels = "0";
        }

        if (_bit_array[3] == '1')
        {
          //1
          if (String.IsNullOrEmpty(_levels))
          {
            _levels = "1";
          }
          else
          {
            _levels += ",1";
          }

        }

        if (_bit_array[2] == '1')
        {
          //2
          if (String.IsNullOrEmpty(_levels))
          {
            _levels = "2";
          }
          else
          {
            _levels += ",2";
          }
        }

        if (_bit_array[1] == '1')
        {
          //3
          if (String.IsNullOrEmpty(_levels))
          {
            _levels = "3";
          }
          else
          {
            _levels += ",3";
          }
        }

        if (_bit_array[0] == '1')
        {
          //4
          if (String.IsNullOrEmpty(_levels))
          {
            _levels = "4";
          }
          else
          {
            _levels += ",4";
          }
        }

        _where = String.Format(_where, _levels);
        return _where;
      }
      catch (Exception)
      {
        return string.Empty;
      }

    }//GetLevelFilter


    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether the account holder qualifies for a frequency-based promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountData
    //        - FrequencyFilter
    //        - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder qualifies for the promotion
    //    - false: account holder qualifies for the promotion
    // 
    private static Boolean IsFrequencyAllowed(Int64 AccountId,
                                              Int32 FreqFilterLastDays,
                                              Int32 FreqFilterMinDays,
                                              Int32 FreqFilterMinCashIn)
    {
      StringBuilder _sb;
      Int32 _sql_value;
      Object _obj;

      if (FreqFilterLastDays == 0)
      {
        // No frequency filter set.
        return true;
      }

      try
      {

        using (DB_TRX _db_trx = new DB_TRX())
        {

          _sb = new StringBuilder();
          _sb.AppendLine("SELECT CASE WHEN (COUNT(*) >= @pMinDays) THEN 1 ELSE 0 END ");
          _sb.AppendLine("FROM ");
          _sb.AppendLine("( ");
          _sb.AppendLine("  SELECT   SUM(AO_AMOUNT) DAYLY_CASHIN ");
          _sb.AppendLine("         , DATEDIFF (DAY, AO_DATETIME, @pTodayOpening) NDAYS ");
          _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS with(index(IX_ao_code_date_account)) ");

          _sb.AppendLine("   WHERE   AO_CODE IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, ");
          _sb.AppendLine("                        @pNACashIn, @pNACashInPromo ) ");

          _sb.AppendLine("     AND   AO_DATETIME   >= DATEADD (DAY, -@pLastDays, @pTodayOpening) ");
          _sb.AppendLine("     AND   AO_DATETIME   <  @pTodayOpening ");
          _sb.AppendLine("     AND   AO_ACCOUNT_ID =  @pAccountId ");
          _sb.AppendLine("  GROUP BY DATEDIFF (DAY, AO_DATETIME, @pTodayOpening) ");
          _sb.AppendLine(") as X ");
          _sb.AppendLine("WHERE DAYLY_CASHIN >= @pMinDailyCashIn ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@pMinDays", SqlDbType.Int).Value = FreqFilterMinDays;
            _cmd.Parameters.Add("@pLastDays", SqlDbType.Int).Value = FreqFilterLastDays;
            _cmd.Parameters.Add("@pMinDailyCashIn", SqlDbType.Money).Value = FreqFilterMinCashIn;
            _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();

            _cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = OperationCode.CASH_IN;
            _cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = OperationCode.PROMOTION;
            _cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = OperationCode.MB_CASH_IN;
            _cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = OperationCode.MB_PROMOTION;
            _cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = OperationCode.NA_CASH_IN;
            _cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = OperationCode.NA_PROMOTION;

            _sql_value = 0;
            _obj = _cmd.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              _sql_value = (Int32)_obj;
            }

            return _sql_value == 1;
          }
        }
      }
      catch (Exception ex)
      {
        _logger.WriteLine("WinUp Notification. Exception in function: InFrequencyAllowed - " + ex.Message);
        Log.Exception(ex);

        return false;
      }
    } // IsFrequencyAllowed


    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether the account qualifies for a created account date-based
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - CreatedAccountDate: created account date
    //        - Filter: created account filter set on the promotion
    //        - DaysIncluded: offset days since account creation
    //        - AnniversaryYearFrom: anniversary year from
    //        - AnniversaryYearTo: anniversary year to
    //
    //      - OUTPUT: see Returns below
    //
    // RETURNS:
    //    - true: account qualifies for the notification
    //    - false: account qualifies for the notification
    // 

    private static Boolean IsCreatedAccountCompliant(DateTime DateToTest, DateTime CreatedAccountDate, Int32 Filter, Int32 DaysIncluded, Int32 AnniversaryYearFrom, Int32 AnniversaryYearTo)
    {
      Boolean _result = true;
      //Obtener la jornada en la que se creó la cuenta
      DateTime _createdAccountWorkingDate = Misc.Opening(CreatedAccountDate);
      //Obtener la jornada de la fecha a testear
      DateTime _dateToTestWorkingDate = Misc.Opening(DateToTest);

      switch (Filter)
      {
        case 0:
          _result = true;
          break;
        case 1: //CREATED_ACCOUNT_FILTER_WORKING_DAY_ONLY
          _result = CreatedAccountFilterWorkingDayRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, 0);
          break;
        case 2: //CREATED_ACCOUNT_FILTER_WORKING_DAYS_INCLUDED
          _result = CreatedAccountFilterWorkingDayRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, DaysIncluded);
          break;
        case 3: //CREATED_ACCOUNT_FILTER_ANNIVERSARY_WHOLE_MONTH
          _result = CreatedAccountFilterAnniversaryRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, true, AnniversaryYearFrom, AnniversaryYearTo);
          break;
        case 4://CREATED_ACCOUNT_FILTER_ANNIVERSARY_DAY_ONLY
          _result = CreatedAccountFilterAnniversaryRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, false, AnniversaryYearFrom, AnniversaryYearTo);
          break;
      }
      return _result;
    } // IsCreatedAcountCompliant

    //------------------------------------------------------------------------------
    // PURPOSE: Checks the resulting working days range against now.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - StartDate: created account working date
    //        - DaysIncluded: offset days since account creation
    //
    //      - OUTPUT: returns true or false based on analysis
    //
    // RETURNS:
    //    - true: working days range contains now
    //    - false: working days range doesn't
    // 
    private static Boolean CreatedAccountFilterWorkingDayRangeCompare(DateTime DateToTest, DateTime StartDate, Int32 DaysIncluded)
    {
      Boolean _result = false;
      DateTime _endDate = StartDate.AddDays(DaysIncluded);

      //don't want to deal with the time part 
      DateToTest = DateToTest - DateToTest.TimeOfDay;
      StartDate = StartDate - StartDate.TimeOfDay;
      _endDate = _endDate - _endDate.TimeOfDay;

      _result = DateToTest >= StartDate && DateToTest <= _endDate;
      return _result;
    }//CreatedAccountFilterWorkingDayRangeCompare

    //------------------------------------------------------------------------------
    // PURPOSE: Checks the resulting anniversary range against now.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - StartDate: created account working date
    //        - AnniversaryWholeMonth: check the whole month or the working day only
    //        - AnniversaryYearFrom: anniversary year from
    //        - AnniversaryYearTo: anniversary year from
    //
    //      - OUTPUT: returns true or false based on analysis
    //
    // RETURNS:
    //    - true: range contains now
    //    - false: range doesn't
    // 
    private static Boolean CreatedAccountFilterAnniversaryRangeCompare(DateTime DateToTest, DateTime StartDate, bool AnniversaryWholeMonth, Int32 AnniversaryYearFrom, Int32 AnniversaryYearTo)
    {
      Boolean _result = false;
      bool _isInAnniversary = false;
      //don't want to deal with the time part 
      DateToTest = DateToTest - DateToTest.TimeOfDay;
      StartDate = StartDate - StartDate.TimeOfDay;

      //Check the anniversary (month or day only), if the year is the same it isn't an anniversary
      if (AnniversaryWholeMonth)
        _isInAnniversary = StartDate.Month == DateToTest.Month && StartDate.Year != DateToTest.Year;
      else
        _isInAnniversary = (StartDate.Day == DateToTest.Day && StartDate.Month == DateToTest.Month) && StartDate.Year != DateToTest.Year;

      _result = _isInAnniversary;

      //Check the anniversary years range (only if necessary)
      if (_isInAnniversary && AnniversaryYearFrom != 0)
      {
        int _yearFrom = StartDate.Year + AnniversaryYearFrom;
        int _yearTo = StartDate.Year + AnniversaryYearTo;
        int _yearToTest = DateToTest.Year;
        _result = _yearToTest >= _yearFrom && _yearToTest <= _yearTo;
      }

      return _result;
    } //CreatedAccountFilterAnniversaryRangeCompare

    public static void Stop()
    {
      _logger.WriteLine("Stopping WinUp Notifications.");
      _logger.WriteLine("WinUp Notifications stopped.");
    }
  }
}
