﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: NotificationDispatcher.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Pablo Molina
// 
// CREATION DATE: 09-FEB-2017 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2017 PDM    First release
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Timers;
using WSI.Common;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace WSI.WinUp.Notifications
{
  public class NotificationDispatcher
  {
    static Thread syncThread = null;
    static System.Timers.Timer m_timer;
    static string APPNAME = "WINUP";
    static int MAX_RETRIES = 5;
    static int MAX_DEVICES_ALLOWED = 1000;
    static string URL_NOTIFICATIONS = "";
    public static event EventHandler CompleteTaskEvent;
    public static string URL_RESOURCES = "";
    static SimpleLogger _logger = new SimpleLogger("WINUP");

    public static void Init()
    {

      _logger.WriteLine("Init WinUp Notifications Dispatcher Process");

      if (m_timer == null)
      {

        m_timer = new System.Timers.Timer();
        m_timer.Interval = 10000; // 10 seg.
        m_timer.Enabled = true;
        m_timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        MAX_RETRIES = GeneralParam.GetInt32("WinUP", "Notifications.Sent.MaxRetries", 5);
        MAX_DEVICES_ALLOWED = GeneralParam.GetInt32("WinUP", "Notifications.Sent.MaxDevicesAllowed", 1000);
        URL_NOTIFICATIONS = Util.CheckUrl(GeneralParam.GetString("WinUP", "Notifications.Url", "http://localhost:20397/api/notification"));
        URL_RESOURCES = Util.CheckUrl(GeneralParam.GetString("WinUP", "Resources.Url", "http://localhost:20397/api/resources"));
      }

      m_timer.Start();

    }

    protected static void timer_Elapsed(object sender, ElapsedEventArgs e)
    {
      syncThread = new Thread(new ThreadStart(RunProcess));
      syncThread.Start();
    }

    protected static void OnCompleteTaskEvent()
    {
      EventHandler handler = CompleteTaskEvent;
      if (handler != null)
      {
        handler(null, EventArgs.Empty);
      }
    }

    private static void RunProcess()
    {
      bool _result;
      List<FCMData> fcmdata;

      _logger.WriteLine("WinUp Notification Dispatcher Process running.");

      m_timer.Stop();
      fcmdata = null;

      try
      {

        //Actualizo el estado
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (RefreshStatus(_db_trx.SqlTransaction))
          {
            _db_trx.Commit();
          }
        }

        //Obtengo las notificaciones y le cambio el estado a En Proceso.
        using (DB_TRX _db_trx = new DB_TRX())
        {
          fcmdata = GetNotificationsToSent(_db_trx.SqlTransaction);
          if (fcmdata != null)
          {
            _db_trx.Commit();
          }
          else
            _db_trx.Rollback();
        }

        //Envio las notitificaciones
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _result = true;

          if (fcmdata != null && fcmdata.Count > 0)
          {
            foreach (var item in fcmdata)
            {
              if (!SendNotification(item, _db_trx.SqlTransaction))
              {
                _result = false;
                Log.Error("WinUp Notification Dispatcher. Error to save notification_sent " + item.Id + " in SendNotification");
                break;
              }
            }
          }
          if (_result)
            _db_trx.Commit();
          else
            _db_trx.Rollback();
        }

        //Vuelvo a actualizar el estado
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (RefreshStatus(_db_trx.SqlTransaction))
          {
            _db_trx.Commit();
          }
        }

        //NADA MAS?????
        _logger.WriteLine("WinUp Notification Dispatcher Process success.");

        OnCompleteTaskEvent();
      }
      catch (Exception _ex)
      {
        _logger.WriteLine("WinUp Notification Dispacher Process finished with error.");
        Log.Exception(_ex);
      }

    }

    private static List<FCMData> GetNotificationsToSent(SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      FCMData _fcmdata;
      List<FCMData> _fcmdatalist;

      _sql_select = new StringBuilder();
      _fcmdatalist = new List<FCMData>();

      try
      {

        // ns_status 
        // 0- A Enviar 
        // 1- En Proceso 
        // 2- Envio correcto 
        // 3- Envio con errores 

        _sql_select.Append("SELECT ns_id_notification_sent, ns_status, ISNULL(ns_data,''), ns_created_date FROM mapp_notification_sent ");
        _sql_select.Append(" WHERE ns_status = 0 OR ns_status = 1");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {

            var data = Util.DeserializeJsonToContent(_sql_data_reader.GetString(2));

            if (data != null)
            {

              _fcmdata = new FCMData();
              _fcmdata.Title = data.Title;
              _fcmdata.Message = data.Message;
              _fcmdata.Target = data.Target;
              _fcmdata.TargetId = data.TargetType;
              _fcmdata.TargetIsURL = data.IsUrl;
              _fcmdata.ImageId = data.ImageId;
              _fcmdata.ImageExtension = data.ImageExtension;
              _fcmdata.Id = _sql_data_reader.GetInt64(0);
              _fcmdata.AppName = APPNAME;
              _fcmdata.Date = _sql_data_reader["ns_created_date"] != DBNull.Value ? _sql_data_reader.GetDateTime(3) : DateTime.Now;
              _fcmdata.DateString = _fcmdata.Date.ToString("yyyyMMddHHmmss");
              _fcmdata.Icon = null;
              _fcmdata.Status = _sql_data_reader.GetInt64(1);
              _fcmdatalist.Add(_fcmdata);

            }
          }

        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();


        _sql_select.Length = 0;
        _sql_select.Append(" UPDATE mapp_notification_sent SET ns_status=1 WHERE ns_status=0");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.ExecuteNonQuery();

        return _fcmdatalist;
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: GetNotificationsToSent");
        Log.Exception(_ex);
        return null;
      }
    }

    private static bool SendNotification(FCMData Data, SqlTransaction Trx)
    {

      string _result;
      string[] _devicesTokens;

      try
      {

        var _devices = GetDevices(Data.Id, Trx);

        if (_devices.Count == 0)
        {
          return SetStatusNotification(Util.NOTIFICATION_STATUS.SEND_ERROR, Data.Id, "Notification without devices.", Trx);
        }

        if (_devices.Count <= MAX_DEVICES_ALLOWED)
        {
          _devicesTokens = new string[_devices.Count];
          _devices.Values.CopyTo(_devicesTokens, 0);
          Data.Devices = _devicesTokens;

          _result = Send(Data);

          return ProcessUniqueReponse(Data, _devices, _result, Trx);

        }
        else
        {
          var _partial_list = Util.GetDictionariesBySize(_devices, 2);
          var _result_list = new List<String>();

          foreach (var item in _partial_list)
          {
            _devicesTokens = new string[item.Count];
            item.Values.CopyTo(_devicesTokens, 0);
            Data.Devices = _devicesTokens;
            _result = Send(Data);
            _result_list.Add(_result);
          }

          return ProcessMultiplesReponses(Data, _devices, _result_list, Trx);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: SendNotification");
        Log.Exception(_ex);
        return false;
      }

    }

    private static Boolean ProcessMultiplesReponses(FCMData Data, Dictionary<Int64, String> Devices, List<String> Responses, SqlTransaction Trx)
    {

      List<FCMResponse> _responses;
      FCMResponse _response;
      FCMResponseDetails _details;
      List<FCMResponseDevicesDetails> _devicesresponses;
      String _message_complete;
      int _success;
      int _failure;

      _response = null;
      _message_complete = String.Empty;
      _responses = new List<FCMResponse>();
      _success = 0;
      _failure = 0;

      try
      {

        foreach (var item in Responses)
        {
          _response = JsonConvert.DeserializeObject<FCMResponse>(item);

          if (!String.IsNullOrEmpty(_response.message) && _response.status == "Error")
          {
            return SaveError(Data, _response.message, Trx);
          }
          else
          {
            _response.FCMResponseDetails = JsonConvert.DeserializeObject<FCMResponseDetails>(_response.message);
            _response.FCMResponseDevicesDetailsList = JsonConvert.DeserializeObject<List<FCMResponseDevicesDetails>>(_response.FCMResponseDetails.results.ToString());
            _responses.Add(_response);
          }
        }

        //Junto todos los mensajes para el log en la tabla
        foreach (var item in Responses)
        {
          _message_complete += item;
        }

        _devicesresponses = new List<FCMResponseDevicesDetails>();

        //sumo el total de success y failures
        foreach (var item in _responses)
        {
          _success += item.FCMResponseDetails.success;
          _failure += item.FCMResponseDetails.failure;
          _devicesresponses.AddRange(item.FCMResponseDevicesDetailsList);
        }

        _response = new FCMResponse();
        _response.status = "OK";
        _response.message = _message_complete;

        _details = new FCMResponseDetails();
        _details.failure = _failure;
        _details.success = _success;
        _details.canonical_ids = 0;
        _details.multicast_id = "";

        return SaveResults(Data, Devices, _response, _details, _devicesresponses, Trx);

      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: ProcessMultiplesReponses");
        Log.Exception(_ex);
        return false;
      }

    }

    private static Boolean ProcessUniqueReponse(FCMData Data, Dictionary<Int64, String> Devices, String Response, SqlTransaction Trx)
    {

      FCMResponse _response;
      FCMResponseDetails _details;
      List<FCMResponseDevicesDetails> _devicesresponses;

      try
      {

        _response = JsonConvert.DeserializeObject<FCMResponse>(Response);

        if (!String.IsNullOrEmpty(_response.message) && _response.status == "Error")
        {
          return SaveError(Data, _response.message, Trx);
        }

        _details = JsonConvert.DeserializeObject<FCMResponseDetails>(_response.message);
        _devicesresponses = JsonConvert.DeserializeObject<List<FCMResponseDevicesDetails>>(_details.results.ToString());

        return SaveResults(Data, Devices, _response, _details, _devicesresponses, Trx);

      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: ProcessUniqueReponse");
        Log.Exception(_ex);
        return false;
      }

    }

    private static Boolean SaveResults(FCMData Data, Dictionary<Int64, String> Devices, FCMResponse FCMResponse, FCMResponseDetails FCMResponseDetails, List<FCMResponseDevicesDetails> FCMResponseDevicesDetailsList, SqlTransaction Trx)
    {
      Int32 _index;

      _index = 0;

      try
      {
        //Si la cantidad de succes es igual a la cantidad de dispositivos, marco la notiticacion como OK y a todos los dispositivos
        if (FCMResponseDetails.success == Devices.Count)
        {
          return MarkNotificationOK(Data.Id, FCMResponse.message, Trx);
        }

        //Si la respuesta del device me trajo un OK, lo guardo. En caso de error lo dejo asi, para volver a ser procesado si aun no llego a la cantidad de reenvios permitidos.
        var _array = FCMResponseDevicesDetailsList.ToArray();
        foreach (KeyValuePair<Int64, String> Item in Devices)
        {
          if (String.IsNullOrEmpty(_array[_index].error) && !String.IsNullOrEmpty(_array[_index].message_id))
          {
            if (!MarkDeviceOK(Item.Key, false, 0, Trx))
            {
              return false;
            }
          }
          _index++;
        }

        // Guardo la ultima respuesta de la api en la notificacion.
        return SetStatusNotification(Util.NOTIFICATION_STATUS.INPROCESS, Data.Id, FCMResponse.message, Trx);
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: SaveResults");
        Log.Exception(_ex);
        return false;
      }
    }


    private static bool MarkNotificationOK(Int64 NotificationSentId, String Message, SqlTransaction Trx)
    {
      if (SetStatusNotification(Util.NOTIFICATION_STATUS.SEND_OK, NotificationSentId, Message, Trx))
      {
        return MarkDeviceOK(0, true, NotificationSentId, Trx);
      }
      else
      {
        return false;
      }
    }

    private static bool MarkDeviceOK(Int64 DeviceSentId, Boolean AllDevices, Int64 NotificationSentId, SqlTransaction Trx)
    {
      StringBuilder _sql;
      SqlCommand sql_command;
      Int64 _id;

      _sql = new StringBuilder();
      _id = 0;

      try
      {

        _sql.Append("UPDATE mapp_notification_sent_device SET ");
        _sql.Append(" nsd_status = @status");

        if (AllDevices)
        {
          _sql.Append(" WHERE nsd_id_notification_sent=@id");
          _id = NotificationSentId;
        }
        else
        {
          _sql.Append(" WHERE nsd_id_notification_sent_devices=@id");
          _id = DeviceSentId;
        }

        sql_command = new SqlCommand(_sql.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@status", SqlDbType.BigInt).Value = Util.NOTIFICATION_DEVICE_STATUS.SEND_OK;
        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = _id;

        sql_command.ExecuteNonQuery();
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: MarkDeviceOK");
        Log.Exception(_ex);
        return false;
      }
    }

    private static Boolean RefreshStatus(SqlTransaction Trx)
    {

      StringBuilder _sql;
      SqlCommand sql_command;

      _sql = new StringBuilder();

      try
      {

        _sql.Append(" UPDATE mapp_notification_sent SET ns_status = @status  ");
        _sql.Append(" WHERE  ns_id_notification_sent NOT IN  ");
        _sql.Append(" (SELECT DISTINCT(nsd_id_notification_sent)  ");
        _sql.Append(" FROM mapp_notification_sent_device ");
        _sql.Append(" WHERE nsd_retries < " + MAX_RETRIES + " AND nsd_status=@status_device) ");
        _sql.Append(" AND ns_status = @current_status ");

        sql_command = new SqlCommand(_sql.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;
        sql_command.Parameters.Add("@status", SqlDbType.BigInt).Value = Util.NOTIFICATION_STATUS.SEND_ERROR;
        sql_command.Parameters.Add("@current_status", SqlDbType.BigInt).Value = Util.NOTIFICATION_STATUS.INPROCESS;
        sql_command.Parameters.Add("@status_device", SqlDbType.BigInt).Value = Util.NOTIFICATION_DEVICE_STATUS.TOSENT;

        sql_command.ExecuteNonQuery();

        _sql.Length = 0;
        _sql.Append("UPDATE mapp_notification_sent_device SET ");
        _sql.Append(" nsd_status = @new_status");
        _sql.Append(" WHERE nsd_status=@status ");
        _sql.Append("  AND nsd_retries = " + MAX_RETRIES + "");

        sql_command = new SqlCommand(_sql.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@status", SqlDbType.BigInt).Value = Util.NOTIFICATION_DEVICE_STATUS.TOSENT;
        sql_command.Parameters.Add("@new_status", SqlDbType.BigInt).Value = Util.NOTIFICATION_DEVICE_STATUS.ERROR;

        sql_command.ExecuteNonQuery();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: MarkDeviceOK");
        Log.Exception(_ex);
        return false;
      }

    }

    private static Boolean SaveError(FCMData Data, String MessageError, SqlTransaction Trx)
    {
      StringBuilder _sql;
      SqlCommand sql_command;

      _sql = new StringBuilder();

      try
      {

        _sql.Append("UPDATE mapp_notification_sent SET ");
        _sql.Append(" ns_api_response = ns_api_response + @message");
        _sql.Append(" WHERE ns_id_notification_sent=@id");

        sql_command = new SqlCommand(_sql.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@message", SqlDbType.NVarChar).Value = MessageError;
        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = Data.Id;

        sql_command.ExecuteNonQuery();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: SaveError");
        Log.Exception(_ex);
        return false;
      }
    }

    private static string Send(FCMData Data)
    {

      try
      {

        var output = JsonConvert.SerializeObject(Data);
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL_NOTIFICATIONS);
        httpWebRequest.ContentType = "application/json; charset=utf-8";
        httpWebRequest.Method = "POST";
        httpWebRequest.Accept = "application/json; charset=utf-8";

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
          streamWriter.Write(output);
          streamWriter.Flush();
          streamWriter.Close();
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
          var result = streamReader.ReadToEnd();
          return result;
        }

      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: Send");
        Log.Exception(_ex);
      }

      return "";
    }

    private static bool SetStatusNotification(WSI.WinUp.Notifications.Util.NOTIFICATION_STATUS Status, Int64 IdNotification, String Message, SqlTransaction Trx)
    {
      StringBuilder _sql;
      SqlCommand sql_command;

      _sql = new StringBuilder();
      try
      {

        _sql.Append("UPDATE mapp_notification_sent SET ");
        _sql.Append(" ns_status = @status,");
        _sql.Append(" ns_api_response = ns_api_response + @message");
        _sql.Append(" WHERE ns_id_notification_sent=@id");

        sql_command = new SqlCommand(_sql.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@status", SqlDbType.BigInt).Value = Status;
        sql_command.Parameters.Add("@message", SqlDbType.NVarChar).Value = Message;
        sql_command.Parameters.Add("@id", SqlDbType.BigInt).Value = IdNotification;

        sql_command.ExecuteNonQuery();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: SetStatusNotification");
        Log.Exception(_ex);
        return false;
      }
    }

    private static Dictionary<Int64, String> GetDevices(Int64 IdNotification, SqlTransaction Trx)
    {
      StringBuilder _sql_select;
      SqlCommand sql_command;
      SqlDataReader _sql_data_reader;
      Dictionary<Int64, String> _devices;

      _sql_select = new StringBuilder();
      _devices = new Dictionary<long, string>();

      try
      {

        _sql_select.Append(" SELECT nsd_id_notification_sent_devices AS ID, ISNULL(nsd_device_token,'') AS TOKEN");
        _sql_select.Append(" FROM mapp_notification_sent_device ");
        _sql_select.Append(" WHERE ");
        _sql_select.Append(" (nsd_status=0 OR nsd_status IS NULL) AND nsd_retries < " + MAX_RETRIES);
        _sql_select.Append(" AND nsd_id_notification_sent = " + IdNotification);
        _sql_select.Append(" ORDER BY [nsd_id_device] DESC");

        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        _sql_data_reader = sql_command.ExecuteReader();

        if (_sql_data_reader.HasRows)
        {

          while (_sql_data_reader.Read())
          {
            if (_sql_data_reader.GetString(1) != String.Empty)
            {
              _devices.Add(_sql_data_reader.GetInt64(0), _sql_data_reader.GetString(1));
            }
          }
        }

        if (!_sql_data_reader.IsClosed)
          _sql_data_reader.Close();

        _sql_select.Length = 0;
        _sql_select.Append(" UPDATE mapp_notification_sent_device SET nsd_retries=(ISNULL(nsd_retries,0)+1) ");
        _sql_select.Append(" WHERE ");
        _sql_select.Append(" (nsd_status=0 OR nsd_status IS NULL) AND nsd_retries < " + MAX_RETRIES);
        _sql_select.Append(" AND nsd_id_notification_sent = " + IdNotification);


        sql_command = new SqlCommand(_sql_select.ToString());
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.ExecuteNonQuery();

      }
      catch (Exception _ex)
      {
        Log.Error("WinUp Notification Dispatcher. Exception in function: GetDevices");
        Log.Exception(_ex);
      }

      return _devices;
    }
  }
}
