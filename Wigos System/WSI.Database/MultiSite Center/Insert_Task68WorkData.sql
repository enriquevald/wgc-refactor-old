--------------------------------------------------------------------------------
-- Copyright � 2015 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: [Insert_Task68WorkData]
-- 
--   DESCRIPTION: Insert data in Task68WorkData
-- 
--        AUTHOR: Alex de Nicol�s
-- 
-- CREATION DATE: 18-MAR-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 18-MAR-2015 ANM    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Task68WorkData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Insert_Task68WorkData]
GO

CREATE PROCEDURE [dbo].[Insert_Task68WorkData]
      @pSiteId              int
    , @pOperationId         bigint
    , @pDatetime            datetime
    , @pAccountId           bigint
    , @pOperType            int
    , @pTotalCashIn         money
    , @pCashInSplit1        money
    , @pCashInSplit2        money
    , @pCashInTax1          money
    , @pCashInTax2          money
    , @pTotalCashOut        money
    , @pDevolution          money
    , @pPrize               money
    , @pIsr1                money
    , @pIsr2                money
    , @pCashOutMoney        money
    , @pCashOutCheck        money
    , @pRfc                 nvarchar(20)
    , @pCurp                nvarchar(20)
    , @pName                nvarchar(200)
    , @pAddress             nvarchar(200)
    , @pCity                nvarchar(50)
    , @pPostalCode          nvarchar(10)
    , @pEvidenceRfc         nvarchar(20)
    , @pEvidenceCurp        nvarchar(20)
    , @pEvidenceName        nvarchar(200)
    , @pEvidenceAddres      nvarchar(200)
    , @pEvidenceCity        nvarchar(50)
    , @pEvidencePostalCode  nvarchar(10)
    , @pEvidence            bit
    , @pDocumentId          int
    , @pRetentionRounding   money
    , @pServiceCharge       money
    , @pDecimalRounding     money
    , @pCashierSessionId    bigint
    , @pPrizeAbove          money
    , @pCashierName         nvarchar(50)
    , @pPrizeExpired        money
    , @pVoucherId           bigint
    , @pFolio               bigint
    , @pPromotionalAmount   money
    , @pPaymentTypeCode     nvarchar(10)
    , @pPaymentType         nvarchar(50)
    , @pRechargePointsPrize money
    
AS
BEGIN   

  DECLARE @externalRef as numeric(15,0)
  DECLARE @externalCard as numeric(20,0)
      
  IF NOT EXISTS (SELECT 1 FROM Task68_work_data WHERE twd_site_id = @pSiteId And twd_operation_id = @pOperationId)
  BEGIN

    SELECT @externalCard = ET_EXTERNAL_CARD_ID
          , @externalRef = ET_EXTERNAL_ACCOUNT_ID
    FROM   ELP01_TRACKDATA
    WHERE  ET_AC_ACCOUNT_ID = @pAccountId
     
    INSERT INTO   Task68_work_data
                ( twd_site_id
                , twd_operation_id
                , twd_datetime
                , twd_account_id
                , twd_external_reference
                , twd_external_card_number
                , twd_operation_type
                , twd_total_cash_in
                , twd_cash_in_split1
                , twd_cash_in_split2
                , twd_cash_in_tax1
                , twd_cash_in_tax2
                , twd_total_cash_out
                , twd_devolution
                , twd_prize
                , twd_isr1
                , twd_isr2
                , twd_cash_out_money
                , twd_cash_out_check
                , twd_rfc
                , twd_curp
                , twd_name
                , twd_address
                , twd_city
                , twd_postal_code
                , twd_evidence_rfc
                , twd_evidence_curp
                , twd_evidence_name
                , twd_evidence_addres
                , twd_evidence_city
                , twd_evidence_postal_code
                , twd_evidence
                , twd_document_id
                , twd_retention_rounding
                , twd_service_charge
                , twd_decimal_rounding
                , twd_cashier_session_id
                , twd_prize_above
                , twd_cashier_name
                , twd_prize_expired
                , twd_voucher_id
                , twd_folio
                , twd_promotional_amount
                , twd_payment_type_code
                , twd_payment_type
                , twd_recharge_points_prize
                )
       VALUES   ( @pSiteId
                , @pOperationId
                , @pDatetime
                , @pAccountId
                , @externalRef
                , @externalCard
                , @pOperType
                , @pTotalCashIn
                , @pCashInSplit1
                , @pCashInSplit2
                , @pCashInTax1
                , @pCashInTax2
                , @pTotalCashOut
                , @pDevolution
                , @pPrize
                , @pIsr1
                , @pIsr2
                , @pCashOutMoney
                , @pCashOutCheck
                , @pRfc
                , @pCurp
                , @pName
                , @pAddress
                , @pCity
                , @pPostalCode
                , @pEvidenceRfc
                , @pEvidenceCurp
                , @pEvidenceName
                , @pEvidenceAddres
                , @pEvidenceCity
                , @pEvidencePostalCode
                , @pEvidence
                , @pDocumentId
                , @pRetentionRounding
                , @pServiceCharge
                , @pDecimalRounding
                , @pCashierSessionId
                , @pPrizeAbove
                , @pCashierName
                , @pPrizeExpired
                , @pVoucherId
                , @pFolio
                , @pPromotionalAmount
                , @pPaymentTypeCode
                , @pPaymentType
                , @pRechargePointsPrize )
  END
  ELSE
  BEGIN
          UPDATE   TASK68_WORK_DATA
             SET   TWD_SENT_TO_SPACE = 0 
           WHERE   TWD_SITE_ID = @pSiteId 
             AND   TWD_OPERATION_ID = @pOperationId
  END

END
GO