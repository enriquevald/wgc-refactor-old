--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_Games
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_Games for increasing the sequence
-- 
--        AUTHOR: Dani Dom�nguez
-- 
-- CREATION DATE: 09-MAY-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-MAY-2013 DDM    First release.
-- 29-MAY-2013 DDM    Change table name, ProvidersGames to Games.
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_Games]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_Games]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_Games] ON [dbo].[games]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence13Value AS BIGINT
    DECLARE @SiteId          AS INT
    DECLARE @PvId            AS INT
    DECLARE @GameId          as INT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.GM_SITE_ID
            , INSERTED.GM_PV_ID
            , INSERTED.GM_GAME_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @SiteId, @PvId, @GameId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
		
        -- Providers Games
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 13

        SELECT   @Sequence13Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 13

        UPDATE   GAMES
           SET   GM_MS_SEQUENCE_ID = @Sequence13Value
         WHERE   GM_SITE_ID        = @SiteId
           AND   GM_PV_ID          = @PvId
           and   GM_GAME_ID        = @GameId

        FETCH NEXT FROM InsertedCursor INTO @SiteId, @PvId, @GameId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor   

END
GO