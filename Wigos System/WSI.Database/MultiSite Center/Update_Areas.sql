  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Areas.sql
  --
  --   DESCRIPTION: Insert_Areas
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -------------------------------------------------------------------------------- 
CREATE PROCEDURE Update_Areas
            @pSiteId  INT  
           ,@pAreaid INT
           ,@pName nvarchar(50)
		   ,@pSmoking bit
           

AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   AREAS
            WHERE   AR_AREA_ID =  @pAreaid 
              AND   AR_SITE_ID =  @pSiteId)
UPDATE AREAS
   SET AR_NAME    =    @pName 
      ,AR_SMOKING =    @pSmoking
            WHERE   AR_AREA_ID =  @pAreaid 
              AND   AR_SITE_ID =  @pSiteId

ELSE
INSERT INTO AREAS
           (AR_AREA_ID
           ,AR_NAME
           ,AR_SMOKING
           ,AR_SITE_ID)
     VALUES
           (@pAreaid
           ,@pName
           ,@pSmoking
           ,@pSiteId)

END -- Update_Areas
GO



