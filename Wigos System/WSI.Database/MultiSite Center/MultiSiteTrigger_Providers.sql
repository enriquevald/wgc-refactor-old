--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_Providers
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_Providers for increasing the sequence
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 13-MAY-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 13-MAY-2013 AMF    First release.
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_Providers]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_Providers]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_Providers] ON [dbo].[providers]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence14Value AS BIGINT
    DECLARE @SiteId          AS INT
    DECLARE @PvId            AS INT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.PV_SITE_ID
            , INSERTED.PV_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @SiteId, @PvId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
        -- Providers
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 14

        SELECT   @Sequence14Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 14

        UPDATE   PROVIDERS
           SET   PV_MS_SEQUENCE_ID = @Sequence14Value
         WHERE   PV_SITE_ID        = @SiteId
           AND   PV_ID             = @PvId

        FETCH NEXT FROM InsertedCursor INTO @SiteId, @PvId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END
GO