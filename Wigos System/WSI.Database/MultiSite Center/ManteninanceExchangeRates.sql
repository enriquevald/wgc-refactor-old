--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: ManteinanceExchangeRates
--
--   DESCRIPTION: Stored Procedure that manteinance table exchange rates
--
--        AUTHOR: Didac Campanals Subirats
--
-- CREATION DATE: 27-ABR-2015
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-ABR-2015 DCS    First release.  
-- 19-MAY-2015 DCS    Update with last changes --> Only update last 30 days  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ManteinanceExchangeRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ManteinanceExchangeRates]
GO

CREATE PROCEDURE [dbo].[ManteinanceExchangeRates]        
AS
BEGIN 

DECLARE @pIsoCode as NVARCHAR(3)
DECLARE @pFirstDateIsoCode as DATETIME
DECLARE @pNowDate as DATETIME
DECLARE @pNumRows as BIGINT
DECLARE @pMaxDaysUpdated as BIGINT

	-- This variable indicates the maximum number of days to update, back today.
	SET @pMaxDaysUpdated = 30

	SET @pNowDate = DATEADD(HOUR, - DATEPART(HOUR, GETDATE()) , DATEADD(HOUR, DATEDIFF(HOUR, 0, GETDATE()), 0))

	DECLARE   CursIsoCode CURSOR FOR 
	 SELECT   CUR_ISO_CODE
	   FROM   CURRENCIES		 
	        
	  SET NOCOUNT ON;
	 OPEN CursIsoCode
	FETCH NEXT FROM CursIsoCode INTO @pIsoCode
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT   @pFirstDateIsoCode = MIN(ER_WORKING_DAY)
					 , @pNumRows = COUNT (*)
			FROM   EXCHANGE_RATES
		 WHERE   ER_ISO_CODE = @pIsoCode
		   AND   ER_WORKING_DAY >= (SELECT DATEADD(DAY,-@pMaxDaysUpdated,DATEADD(HOUR, - DATEPART(HOUR, GETDATE()) , DATEADD(HOUR, DATEDIFF(HOUR, 0, GETDATE()), 0))))
		
		IF (@pNumRows > 0 )
		BEGIN
			
			IF ( SELECT (DATEDIFF(DAY, @pFirstDateIsoCode, @pNowDate ) + 1) - @pNumRows ) > 0 
			BEGIN

			WITH DATES  AS
			(
					SELECT   @pFirstDateIsoCode  AS CALENDARDATE
			 UNION ALL
					SELECT   DATEADD (DAY, 1, CALENDARDATE) AS CALENDARDATE
						FROM   DATES
					 WHERE   DATEADD (DAY, 1, CALENDARDATE) <= @pNowDate
			) 
					INSERT   INTO EXCHANGE_RATES
					SELECT   @pIsoCode AS ER_ISO_CODEY
								 , AA.CALENDARDATE AS ER_WORKING_DAY
								 , ISNULL ( ER_CHANGE
													, ( SELECT   TOP 1 ER_CHANGE 
												 				FROM   EXCHANGE_RATES AS ER2 
															 WHERE   ER2.ER_ISO_CODE = @pIsoCode 
																 AND   ER2.ER_WORKING_DAY < AA.CALENDARDATE 
															 ORDER   BY ER_WORKING_DAY DESC
														 )
													) AS ER_CHANGE
								 , GETDATE() AS ER_DATE_UPDATED
						FROM   DATES AS AA 
			 LEFT JOIN   EXCHANGE_RATES AS ER 
		  				ON   DATEADD (DAY, DATEDIFF(DD, 0, AA.CALENDARDATE), 0) =  DATEADD (DAY, DATEDIFF(DD, 0, ER_WORKING_DAY), 0)
						 AND   ER_ISO_CODE = @pIsoCode
					 WHERE   ER_ISO_CODE IS NULL

			OPTION (MAXRECURSION 32767)
			
				END -- DATEDIFF
			END -- NumRows

		FETCH NEXT FROM CursIsoCode INTO @pIsoCode

	END -- WHILE

CLOSE CursIsoCode
DEALLOCATE CursIsoCode

END -- PROCEDURE
GO
