  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_TerminalGameTranslation.sql
  --
  --   DESCRIPTION: Insert_TerminalGameTranslation
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -- 24-ARP-2013 ANG    Fix Pk, unexpected behavior in update.
  -- 08-MAY-2013 ANG    Add tgt_payout_idx field 
  -- 13-MAY-2013 ANG    Add tgt_translated_game_id field
  -------------------------------------------------------------------------------- 
  
CREATE PROCEDURE Update_TerminalGameTranslation
            @pSiteId  INT  
          , @pTerminalId int
          , @pSourceGameId int
          , @pTargetGameId int
          , @pPayoutIdx int
          , @pTranslatedGameId int
          , @pCreated datetime
          , @pUpdated datetime
AS
BEGIN    
IF EXISTS (SELECT   1 
             FROM   terminal_game_translation
            WHERE   [tgt_SITE_id]         = @pSiteId
              AND   [tgt_terminal_id]     = @pTerminalId 
              AND   [tgt_source_game_id]  = @pSourceGameId)
              
UPDATE terminal_game_translation
   SET      [tgt_target_game_id]      = @pTargetGameId
           ,[tgt_created]             = @pCreated
           ,[tgt_updated]             = @pUpdated
           ,[tgt_payout_idx]          = @pPayoutIdx
           ,[tgt_translated_game_id]  = @pTranslatedGameId
    WHERE   [tgt_SITE_id]         = @pSiteId
      AND   [tgt_terminal_id]     = @pTerminalId 
      AND   [tgt_source_game_id]  = @pSourceGameId

ELSE
INSERT INTO terminal_game_translation
           ([tgt_terminal_id]
           ,[tgt_source_game_id]
           ,[tgt_target_game_id]
           ,[tgt_payout_idx]
           ,[tgt_translated_game_id]
           ,[tgt_created]
           ,[tgt_updated]
           ,[tgt_SITE_id]
           )
     VALUES 
           (@pTerminalId
           ,@pSourceGameId
           ,@pTargetGameId
           ,@pPayoutIdx
           ,@pTranslatedGameId
           ,@pCreated
           ,@pUpdated
           ,@pSiteId
           )

END -- Insert Terminal-Game Translation
GO




