﻿  --------------------------------------------------------------------------------
  -- Copyright © 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_Creditlines.sql
  --
  --   DESCRIPTION: Update_Creditlines
  --
  --        AUTHOR: Mark Stansfield
  --
  -- CREATION DATE: 31-MAR-2017
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 31-MAR-2017 MS     First release.  
  -------------------------------------------------------------------------------- 
CREATE PROCEDURE Update_Creditlines
           @pSiteId  INT  
          ,@pId INT
          ,@pAccountId INT
          ,@pLimitAmount MONEY
          ,@pTTOAmount MONEY
          ,@pSpentAmount MONEY
          ,@pISOCode nvarchar(3)
          ,@pStatus INT
          ,@pIBAN nvarchar(34)
          ,@pXMLSigners XML
          ,@pCreation DATETIME
          ,@pUpdate DATETIME
          ,@pExpired DATETIME
          ,@pLastPayback DATETIME
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   CREDIT_LINES
            WHERE   CL_ID =  @pId 
              AND   CL_SITE_ID =  @pSiteId)
              
UPDATE CREDIT_LINES
   SET CL_LIMIT_AMOUNT   = @pLimitAmount
      ,CL_TTO_AMOUNT     = @pTTOAmount
      ,CL_SPENT_AMOUNT   = @pSpentAmount
      ,CL_ISO_CODE       = @pISOCode
      ,CL_STATUS         = @pStatus
      ,CL_IBAN           = @pIBAN
      ,CL_XML_SIGNERS    = @pXMLSigners
      ,CL_CREATION       = @pCreation
      ,CL_UPDATE         = @pUpdate
      ,CL_EXPIRED        = @pExpired
      ,CL_LAST_PAYBACK   = @pLastPayback  
            WHERE   CL_ID =  @pId  
              AND   CL_SITE_ID    =  @pSiteId


ELSE
INSERT INTO CREDIT_LINES
           (CL_SITE_ID
           ,CL_ID
           ,CL_ACCOUNT_ID
           ,CL_LIMIT_AMOUNT
           ,CL_TTO_AMOUNT
           ,CL_SPENT_AMOUNT
           ,CL_ISO_CODE
           ,CL_STATUS
           ,CL_IBAN
           ,CL_XML_SIGNERS
           ,CL_CREATION
           ,CL_UPDATE  
           ,CL_EXPIRED 
           ,CL_LAST_PAYBACK)
     VALUES 
           (@pSiteId 
           ,@pId
           ,@pAccountID
           ,@pLimitAmount
           ,@pTTOAmount
           ,@pSpentAmount
           ,@pISOCode
           ,@pStatus
           ,@pIBAN
           ,@pXMLSigners
           ,@pCreation
           ,@pUpdate
           ,@pExpired
           ,@pLastPayback)  
END -- Update_Creditlines
GO



