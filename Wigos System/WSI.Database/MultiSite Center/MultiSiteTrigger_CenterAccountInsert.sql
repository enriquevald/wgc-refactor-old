--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountInsert.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountInsert and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when center receive a new account, mark it to synchronize.
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountInsert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountInsert]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountInsert] ON [dbo].[accounts]
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence10Value AS BIGINT
    DECLARE @Sequence11Value AS BIGINT
    DECLARE @AccountId       AS BIGINT

    RAISERROR ('Row Inserted!', 0, 0) WITH LOG

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AC_ACCOUNT_ID 
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN

        -- Personal Info
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 10

        -- Points
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 11

        SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10
        SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

        UPDATE   ACCOUNTS
           SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
               , AC_MS_POINTS_SEQ_ID        = @Sequence11Value 
         WHERE   AC_ACCOUNT_ID              = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END
GO