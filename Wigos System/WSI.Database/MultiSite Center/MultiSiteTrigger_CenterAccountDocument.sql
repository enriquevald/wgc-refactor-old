--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_CenterAccountDocument
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_CenterAccountDocument for increasing the sequence
-- 
--        AUTHOR: Jos� Mart�nez L�pez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
-- 22-MAR-2016 JML    Bug 10880:MS-Meier: Not upload account_documents
--------------------------------------------------------------------------------  

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_CenterAccountDocument]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument] ON [dbo].[ACCOUNT_DOCUMENTS]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence15Value AS BIGINT
    DECLARE @AccountId       AS BIGINT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AD_ACCOUNT_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
        -- Account Documents
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 15

        SELECT   @Sequence15Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 15

        UPDATE   ACCOUNT_DOCUMENTS
           SET   AD_MS_SEQUENCE_ID = @Sequence15Value
         WHERE   AD_ACCOUNT_ID     = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END   -- MultiSiteTrigger_CenterAccountDocument
GO