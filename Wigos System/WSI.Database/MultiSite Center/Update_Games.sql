  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Games.sql
  --
  --   DESCRIPTION: Insert_Games
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -------------------------------------------------------------------------------- 
CREATE PROCEDURE Update_Games
           @pSiteId  INT  
          ,@pGameId INT
          ,@pName nvarchar(50)
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   games
            WHERE   gm_game_id =  @pGameId 
              AND   gm_site_id =  @pSiteId)
              
UPDATE games
   SET gm_name =@pName
            WHERE   gm_game_id =  @pGameId 
              AND   gm_site_id =  @pSiteId

ELSE
INSERT INTO games
           ( gm_site_id
            ,gm_game_id
            ,gm_name)
     VALUES 
           (@pSiteId
           ,@pGameId
           ,@pName)


END -- Update_Games
GO




