  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_TerminalsConnected
  --
  --   DESCRIPTION: Stored Procedure that inserted or updated the terminals connected
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 03-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 03-MAY-2013 DDM    First release.  
  -- 27-MAR-2015 FOS		Added column TC_MASTER_ID
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalsConnected]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Update_TerminalsConnected]
GO

  
 CREATE PROCEDURE [dbo].[Update_TerminalsConnected]
  @pSiteId     INT  
, @pMasterId   INT 
, @pDate       DATETIME
, @pTerminalId INT
, @pStatus     INT
, @pConnected  BIT
, @pTimeStamp  BIGINT
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   TERMINALS_CONNECTED
              WHERE   TC_SITE_ID     = @pSiteId 
                AND   TC_DATE        = @pDate
                AND   TC_MASTER_ID   = @pMasterId)
                
    UPDATE   TERMINALS_CONNECTED
       SET   TC_STATUS      = @pStatus
           , TC_CONNECTED   = @pConnected
           , TC_TIMESTAMP   = @pTimeStamp
     WHERE   TC_SITE_ID     = @pSiteId 
       AND   TC_DATE        = @pDate
       AND   TC_MASTER_ID   = @pMasterId

  ELSE

    INSERT INTO   TERMINALS_CONNECTED
                ( TC_SITE_ID
                , TC_DATE
                , TC_MASTER_ID
                , TC_TERMINAL_ID
                , TC_STATUS
                , TC_CONNECTED
                , TC_TIMESTAMP)
         VALUES    
                ( @pSiteId    
                , @pDate      
                , @pMasterId
                , @pTerminalId
                , @pStatus    
                , @pConnected 
                , @pTimeStamp )

END -- Insert_Terminals_connected
GO
