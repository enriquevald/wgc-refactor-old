--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME:		Update_SiteHandpays.sql
--
--   DESCRIPTION:		Update_SiteHandpays
--
--   AUTHOR:				Didac Campanals Subirats
--
--   CREATION DATE: 07-JAN-2015
--
-- REVISION HISTORY:
--
-- Date         Author     Description
-- -----------  ---------  ----------------------------------------------------------
-- 07-JAN-2015  DCS        First release.  
-- ------------ ---------  ---------------------------------------------------------- 
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SiteHandpays]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_SiteHandpays]
GO

CREATE PROCEDURE   [dbo].[Update_SiteHandpays]
                   @pSiteid													INT
								 , @pHandpayId											BIGINT
								 , @pTerminalId											INT
								 , @pGameBaseName										NVARCHAR(50)
                 , @pDatetime												DATETIME
								 , @pPreviousMeters									DATETIME
								 , @pAmount													MONEY
								 , @pTeName													NVARCHAR(50)
								 , @pTeProviderId										NVARCHAR(50)
								 , @pMovementId											BIGINT
								 , @pType														INT
								 , @pPlaySessionId									BIGINT
								 , @pSiteJackpotIndex								INT
								 , @pSiteJackpotName								NVARCHAR(50)
								 , @pSiteJackpotAwardedOnTerminalId	INT
								 , @pSiteJackpotAwardedToAccountId	BIGINT
								 , @pStatus													INT
								 , @pSiteJackpotNotified						BIT
								 , @pTicketId												BIGINT
								 , @pTransactionId									BIGINT
								 , @pCandidatePlaySessionId					BIGINT
								 , @pCandidatePrevPlaySessionId			BIGINT
								 , @pLongPoll1bData									XML
								 , @pProgressiveId									BIGINT
								 , @pLevel													INT
								 , @pStatusChanged									DATETIME
								 , @pTaxBaseAmount									MONEY
								 , @pTaxAmount											MONEY
								 , @pTaxPct													DECIMAL								 
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   HANDPAYS 
              WHERE   HP_SITE_ID  = @pSiteId 
                AND   HP_ID = @pHandpayId)
                
/****** Update site Handpays ******/
	BEGIN                
		     UPDATE   HANDPAYS
			      SET		HP_TERMINAL_ID													= @pTerminalId
								, HP_GAME_BASE_NAME												= @pGameBaseName
								, HP_DATETIME															= @pDatetime
								, HP_PREVIOUS_METERS											= @pPreviousMeters
								, HP_AMOUNT																= @pAmount
     						, HP_TE_NAME															= @pTeName
 								, HP_TE_PROVIDER_ID												= @pTeProviderId
 								, HP_MOVEMENT_ID													= @pMovementId
								, HP_TYPE																	= @pType
								, HP_PLAY_SESSION_ID											= @pPlaySessionId
								, HP_SITE_JACKPOT_INDEX										= @pSiteJackpotIndex
								, HP_SITE_JACKPOT_NAME										= @pSiteJackpotName
								, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID	= @pSiteJackpotAwardedOnTerminalId
								, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID		= @pSiteJackpotAwardedToAccountId
								, HP_STATUS																= @pStatus
								, HP_SITE_JACKPOT_NOTIFIED								= @pSiteJackpotNotified
								, HP_TICKET_ID														= @pTicketId
								, HP_TRANSACTION_ID												= @pTransactionId
								, HP_CANDIDATE_PLAY_SESSION_ID						= @pCandidatePlaySessionId
								, HP_CANDIDATE_PREV_PLAY_SESSION_ID				= @pCandidatePrevPlaySessionId
								, HP_LONG_POLL_1B_DATA										= @pLongPoll1bData
								, HP_PROGRESSIVE_ID												= @pProgressiveId
								, HP_LEVEL																= @pLevel
								, HP_STATUS_CHANGED												= @pStatusChanged
								, HP_TAX_BASE_AMOUNT											= @pTaxBaseAmount
								, HP_TAX_AMOUNT														= @pTaxAmount
								, HP_TAX_PCT															= @pTaxPct				       
				  WHERE   HP_SITE_ID															= @pSiteId 
			      AND   HP_ID																		= @pHandpayId
  END
ELSE
	BEGIN  
/****** Insert site Handpays ******/  

    INSERT INTO   HANDPAYS
								( HP_SITE_ID
								, HP_ID
								, HP_TERMINAL_ID
								, HP_GAME_BASE_NAME
								, HP_DATETIME
								, HP_PREVIOUS_METERS
								, HP_AMOUNT
								, HP_TE_NAME
								, HP_TE_PROVIDER_ID
								, HP_MOVEMENT_ID
								, HP_TYPE
								, HP_PLAY_SESSION_ID
								, HP_SITE_JACKPOT_INDEX	
								, HP_SITE_JACKPOT_NAME
								, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID
								, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID
								, HP_STATUS
								, HP_SITE_JACKPOT_NOTIFIED
								, HP_TICKET_ID
								, HP_TRANSACTION_ID
								, HP_CANDIDATE_PLAY_SESSION_ID
								, HP_CANDIDATE_PREV_PLAY_SESSION_ID
								, HP_LONG_POLL_1B_DATA
								, HP_PROGRESSIVE_ID
								, HP_LEVEL
								, HP_STATUS_CHANGED
								, HP_TAX_BASE_AMOUNT
								, HP_TAX_AMOUNT
								, HP_TAX_PCT
								)
				 VALUES ( @pSiteid  
								, @pHandpayId  
								, @pTerminalId  
								, @pGameBaseName  
								, @pDatetime  
								, @pPreviousMeters  
								, @pAmount  
								, @pTeName  
								, @pTeProviderId  
								, @pMovementId  
								, @pType  
								, @pPlaySessionId  
								, @pSiteJackpotIndex  
								, @pSiteJackpotName  
								, @pSiteJackpotAwardedOnTerminalId  
								, @pSiteJackpotAwardedToAccountId  
								, @pStatus  
								, @pSiteJackpotNotified  
								, @pTicketId  
								, @pTransactionId  
								, @pCandidatePlaySessionId  
								, @pCandidatePrevPlaySessionId  
								, @pLongPoll1bData  
								, @pProgressiveId  
								, @pLevel  
								, @pStatusChanged  
								, @pTaxBaseAmount 
								, @pTaxAmount
								, @pTaxPct
								)
														
	END
	      
END -- Update_SiteHandpays

GO
 