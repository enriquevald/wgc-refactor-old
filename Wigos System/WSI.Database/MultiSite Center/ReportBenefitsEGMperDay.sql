 ----------------------------------------------------------------------------------------------------------
---- Report to get the benefits bu EGM   								                                              -----
---- Author: Xavi Guirado																			                                        -----
---- Date: 23/Nov/2017																				                                        -----
-----------------------------------------------------------------------------------------------------------	


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportBenefitsEGMperDay]') AND type in (N'P', N'PC'))
 DROP PROCEDURE [dbo].ReportBenefitsEGMperDay
GO

CREATE PROCEDURE [dbo].[ReportBenefitsEGMperDay]
 (@pFrom	DATETIME)
AS

BEGIN

DECLARE @tsmh_from DATETIME;
DECLARE @tsmh_to DATETIME;

SET @tsmh_from = DATEADD(DAY, -1, @pFrom);
SET @tsmh_to = @pFrom;

	  SELECT 
		tsmh_terminal_id																										AS Terminal,
		--MAX(Games.gm_name)																										AS Juego,
		''																														AS Juego,
		'C1'																													AS Tipo_C,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			/ NULLIF(SUM (CASE WHEN TSMH_METER_CODE = 5				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END),0)			AS Promedio_Apuesta,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)			AS Apuestas,
		SUM((CASE WHEN TSMH_METER_CODE = 1							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
				+ (CASE WHEN TSMH_METER_CODE = 2					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Pagos,
		SUM(CASE WHEN TSMH_METER_CODE = 2							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)			AS JackPot,		
		'1'																														AS Dias,	
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			As Beneficios,
		SUM (CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Promedio_Diario,
		NULLIF(
		CONVERT(DECIMAL(16,2), SUM((CASE WHEN TSMH_METER_CODE = 1	THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))*100.00
			/ NULLIF(SUM(CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END),0)),0)		AS Payout_Real,
		MAX(terminals.te_theoretical_payout)																					AS Payout_Teo,
		'0,00'																													AS Promocion,
		SUM (CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Benef_Real,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Promo_Diaio_real	
		
	FROM terminal_sas_meters_history
	INNER JOIN terminals ON tsmh_terminal_id = terminals.te_terminal_id
--	LEFT JOIN games ON tsmh_game_id = games.gm_game_id

	WHERE 
			 TSMH_DATETIME >= @tsmh_from 
			AND TSMH_DATETIME <= @tsmh_to
	AND tsmh_meter_code IN (0,1,2,5)
	AND tsmh_type = 20

	GROUP BY tsmh_datetime, tsmh_site_id, tsmh_terminal_id

END -- ReporteBeneficioMaquinaPorDia

GO

GRANT EXECUTE ON ReportBenefitsEGMperDay TO [wggui] WITH GRANT OPTION
GO

--//GENERIC REPORT
BEGIN
  DECLARE @rtc_store_name	NVARCHAR(200)
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)

  SET @rtc_store_name = 'ReportBenefitsEGMperDay'
	
  SET @rtc_report_name = '<LanguageResources><NLS09><Label>Report EGM Benefits</Label></NLS09><NLS10><Label>Reporte Beneficios por Terminal</Label></NLS10></LanguageResources>'
	
  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
	  <ReportToolDesignSheetsDTO>
		<LanguageResources>
		  <NLS09>
			<Label>Report EGM Benefits</Label>
		  </NLS09>
		  <NLS10>
			<Label>Reporte de Beneficios Terminal</Label>
		  </NLS10>
		</LanguageResources>
		<Columns>    
		  <ReportToolDesignColumn>
			<Code>Terminal</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Terminal</Label>
			  </NLS09>
			  <NLS10>
				<Label>Terminal</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>   
		  <ReportToolDesignColumn>
			<Code>Juego</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Game</Label>
			  </NLS09>
			  <NLS10>
				<Label>Juego</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Tipo_C</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Type C</Label>
			  </NLS09>
			  <NLS10>
				<Label>Tipo C</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>     
		  <ReportToolDesignColumn>
			<Code>Promedio_Apuesta</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Average bet</Label>
			  </NLS09>
			  <NLS10>
				<Label>Promodio Apuesta</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>      
		  <ReportToolDesignColumn>
			<Code>Apuestas</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Bets</Label>
			  </NLS09>
			  <NLS10>
				<Label>Apuestas</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>     
		  <ReportToolDesignColumn>
			<Code>Pagos</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Payments</Label>
			  </NLS09>
			  <NLS10>
				<Label>Pagos</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>JackPot</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>JackPot</Label>
			  </NLS09>
			  <NLS10>
				<Label>JackPot</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Dias</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Days</Label>
			  </NLS09>
			  <NLS10>
				<Label>D�as</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>
		  <ReportToolDesignColumn>
			<Code>Beneficios</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Benefits</Label>
			  </NLS09>
			  <NLS10>
				<Label>Beneficios</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Promedio_Diario</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Daily average</Label>
			  </NLS09>
			  <NLS10>
				<Label>Promedio Diario</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Payout_Real</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>% Real</Label>
			  </NLS09>
			  <NLS10>
				<Label>% Real</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Payout_Teo</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>% Theo</Label>
			  </NLS09>
			  <NLS10>
				<Label>% Teo</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>    
		  <ReportToolDesignColumn>
			<Code>Promocion</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Promotion</Label>
			  </NLS09>
			  <NLS10>
				<Label>Promocion</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>
		  <ReportToolDesignColumn>
			<Code>Benef_Real</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Real Benef.</Label>
			  </NLS09>
			  <NLS10>
				<Label>Benef.Real</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>
		  <ReportToolDesignColumn>
			<Code>Promo_Diaio_real</Code>
			<Width>300</Width>
			<EquityMatchType>Equality</EquityMatchType>
			<LanguageResources>
			  <NLS09>
				<Label>Real daily prom.</Label>
			  </NLS09>
			  <NLS10>
				<Label>Promo. Diario Real</Label>
			  </NLS10>
			</LanguageResources>
		  </ReportToolDesignColumn>
		</Columns>
	  </ReportToolDesignSheetsDTO>
	</ArrayOfReportToolDesignSheetsDTO>
	'
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
  <FilterType>CustomFilters</FilterType>
  <Filters>
    <ReportToolDesignFilter>
      <TypeControl>uc_date_picker</TypeControl>
      <TextControl>
        <LanguageResources>
          <NLS09>
            <Label>Date</Label>
          </NLS09>
          <NLS10>
            <Label>Fecha</Label>
          </NLS10>
        </LanguageResources>
      </TextControl>
      <ParameterStoredProcedure>
        <Name>@pFrom</Name>
        <Type>DateTime</Type>
      </ParameterStoredProcedure>
      <Methods>
        <Method>
          <Name>SetFormat</Name>
          <Parameters>1,0</Parameters>
        </Method>
      </Methods>
      <Value>TodayOpening() - 1</Value>
    </ReportToolDesignFilter>
  </Filters>
</ReportToolDesignFilterDTO>'
  
  IF NOT EXISTS(SELECT 1 
				FROM report_tool_config 
				WHERE rtc_store_name = @rtc_store_name)
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
    VALUES	/*RTC_REPORT_TOOL_ID */
			/*RTC_FORM_ID*/        (@rtc_form_id,
			/*RTC_LOCATION_MENU*/   2, 
            /*RTC_REPORT_NAME*/     @rtc_report_name,
            /*RTC_STORE_NAME*/      @rtc_store_name,
            /*RTC_DESIGN_FILTER*/   @rtc_design_filter,
            /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
            /*RTC_MAILING*/         0,
            /*RTC_STATUS*/          1,
            /*RTC_MODE*/            36,
			/*RTC_HTML_HEADER*/		NULL,
		    /*RTC_HTML_FOOTER*/		NULL)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
        , RTC_REPORT_NAME   = /*RTC_REPORT_NAME*/     @rtc_report_name
        , RTC_MODE_TYPE     =  /*RTC_MODE*/ 36
      WHERE RTC_STORE_NAME = @rtc_store_name
  END
END
GO
