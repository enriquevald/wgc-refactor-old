--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSite_UpdateCurrencies
--
--   DESCRIPTION: Stored Procedure that insert or update currencies in Multisite Multicurrency from sites
--
--        AUTHOR: Didac Campanals Subirats
--
-- CREATION DATE: 28-APR-2015
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 28-APR-2015 DCS    First release.  
-- 06-MAY-2015 DCS    Update with last changes --> Only one foreign currency 
-- 19-MAY-2015 DCS    Update with last changes --> 3 Response code (New national, change national, update foreign)
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSite_UpdateCurrencies]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSite_UpdateCurrencies]
GO
CREATE PROCEDURE [dbo].[MultiSite_UpdateCurrencies]        
          @pIsoCode                 NVARCHAR(3)
        , @pSiteId                  INT
        , @pType                    INT
        , @pDescription             NVARCHAR(MAX)
        , @pForeignCurrencyType     INT
        , @pNationalCurrencyType    INT
        , @pChange                  DECIMAL(16,8)
        , @pResponseCode			INT               OUTPUT
        , @pBeforeCurrencyIsoCode   NVARCHAR(3)		  OUTPUT
  AS
BEGIN 

SET @pBeforeCurrencyIsoCode = (SELECT SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_TYPE = @pType)

SET @pResponseCode = 0         

------------------------------------------
-- INSERT NEW NATIONAL CURRENCY IN CURRENCIES
------------------------------------------
IF NOT EXISTS( SELECT TOP 1 CUR_ISO_CODE FROM CURRENCIES WHERE CUR_ISO_CODE = @pIsoCode) AND (@pType = @pNationalCurrencyType)
BEGIN
    INSERT   INTO CURRENCIES (CUR_ISO_CODE, CUR_DESCRIPTION)    
                       VALUES(@pIsoCode   , @pDescription  )      
 
    SET @pResponseCode = 1   -- IS NEW NATIONAL CURRENCY!   
 END  
 
 
----------------------------------------------------
-- INSERT OR UPDATE CURRENCY IN SITE_CURRENCIES
----------------------------------------------------
IF NOT EXISTS ( SELECT TOP 1 SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_TYPE = @pType)  
BEGIN
  -- NEW NATIONAL CURRENCY IN SITE_CURRENCIES
  INSERT INTO   SITE_CURRENCIES  
              ( SC_SITE_ID            
              , SC_ISO_CODE           
              , SC_DESCRIPTION        
              , SC_TYPE               
              , SC_CHANGE             
              )                       
       VALUES ( @pSiteId              
              , @pIsoCode             
              , @pDescription         
              , @pType                
              , @pChange              
              )

  IF ( @pType = @pNationalCurrencyType)
    SET @pResponseCode = @pResponseCode + 2 --IS NEW SITE NATIONAL CURRENCY
  ELSE
    SET @pResponseCode = @pResponseCode + 4 --IS NEW SITE FOREIGN CURRENCY
END
ELSE
BEGIN
  IF NOT EXISTS ( SELECT TOP 1 SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_ISO_CODE = @pIsoCode AND SC_TYPE = @pType)  
  BEGIN
    IF ( @pType = @pNationalCurrencyType)
      SET @pResponseCode = @pResponseCode + 2 --IS UPDATED SITE NATIONAL CURRENCY
    ELSE
      SET @pResponseCode = @pResponseCode + 4 --IS UPDATED SITE FOREIGN CURRENCY
  END
      
  UPDATE   SITE_CURRENCIES           
     SET   SC_ISO_CODE = @pIsoCode 
         , SC_DESCRIPTION = @pDescription
         , SC_CHANGE = @pChange      
   WHERE   SC_SITE_ID = @pSiteId     
     AND   SC_TYPE = @pType 
      
END  

SELECT @pResponseCode, @pBeforeCurrencyIsoCode
END
GO


