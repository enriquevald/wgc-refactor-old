  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_SiteAlarms.sql
  --
  --   DESCRIPTION: Update_SiteAlarms
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 15-ABR-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author    Description
  -- ----------- --------- ----------------------------------------------------------
  -- 15-ABR-2014 JML & JMM First release.  
  -------------------------------------------------------------------------------- 
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SiteAlarms]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_SiteAlarms]
GO
/****** Insert/Update site alarms ******/
CREATE PROCEDURE   [dbo].[Update_SiteAlarms]
                   @pSiteid             INT
                 , @pAlarmId            BIGINT
                 , @pSourceCode         INT
                 , @pSourceId           BIGINT 
                 , @pSourceName         NVARCHAR(100)
                 , @pAlarmCode          INT
                 , @pAlarmName          NVARCHAR(50)
                 , @pAlarmDescription   NVARCHAR(MAX)
                 , @pSeverity           INT
                 , @pReported           DATETIME
                 , @pDatetime           DATETIME
                 , @pAckDatetime        DATETIME
                 , @pAckUserId          INT
                 , @pAckUserName        NVARCHAR(50)
                 , @pTimestamp          BIGINT
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   SITE_ALARMS 
              WHERE   SA_SITE_ID  = @pSiteId 
                AND   SA_ALARM_ID = @pAlarmId
                AND   SA_REPORTED = @pReported)
     UPDATE   SITE_ALARMS
        SET   SA_SITE_ID           = @pSiteid
            , SA_ALARM_ID          = @pAlarmId
            , SA_SOURCE_CODE       = @pSourceCode
            , SA_SOURCE_ID         = @pSourceId
            , SA_SOURCE_NAME       = @pSourceName
            , SA_ALARM_CODE        = @pAlarmCode
            , SA_ALARM_NAME        = @pAlarmName
            , SA_ALARM_DESCRIPTION = @pAlarmDescription
            , SA_SEVERITY          = @pSeverity
            , SA_REPORTED          = @pReported
            , SA_DATETIME          = @pDatetime
            , SA_ACK_DATETIME      = @pAckDatetime
            , SA_ACK_USER_ID       = @pAckUserId
            , SA_ACK_USER_NAME     = @pAckUserName
            , SA_TIMESTAMP         = @pTimestamp
      WHERE   SA_SITE_ID  = @pSiteId
        AND   SA_ALARM_ID = @pAlarmId
        AND   SA_REPORTED = @pReported
  ELSE
    INSERT INTO   SITE_ALARMS
                ( SA_SITE_ID
                , SA_ALARM_ID
                , SA_SOURCE_CODE
                , SA_SOURCE_ID
                , SA_SOURCE_NAME
                , SA_ALARM_CODE
                , SA_ALARM_NAME
                , SA_ALARM_DESCRIPTION
                , SA_SEVERITY
                , SA_REPORTED
                , SA_DATETIME
                , SA_ACK_DATETIME
                , SA_ACK_USER_ID
                , SA_ACK_USER_NAME
                , SA_TIMESTAMP )
         VALUES ( @pSiteid
                , @pAlarmId
                , @pSourceCode
                , @pSourceId
                , @pSourceName
                , @pAlarmCode
                , @pAlarmName
                , @pAlarmDescription
                , @pSeverity
                , @pReported
                , @pDatetime
                , @pAckDatetime
                , @pAckUserId
                , @pAckUserName
                , @pTimestamp)      
END -- Update_SiteAlarms
GO
 