﻿  --------------------------------------------------------------------------------
  -- Copyright © 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_CreditlineMovements.sql
  --
  --   DESCRIPTION: Update_CreditlineMovements
  --
  --        AUTHOR: Mark Stansfield
  --
  -- CREATION DATE: 31-MAR-2017
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 31-MAR-2017 MS     First release.  
  -------------------------------------------------------------------------------- 
CREATE PROCEDURE Update_CreditlineMovements
           @pSiteId  INT  
          ,@pId BIGINT
          ,@pCreditlineId BIGINT
          ,@pOperationId BIGINT
          ,@pType INT
          ,@pOldvalue XML
          ,@pNewValue XML
          ,@pCreation DATETIME
          ,@pCreationUser NVARCHAR(50)
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   CREDIT_LINE_MOVEMENTS
            WHERE   CLM_ID =  @pId 
              AND   CLM_SITE_ID =  @pSiteId)
              
UPDATE CREDIT_LINE_MOVEMENTS
   SET CLM_CREDIT_LINE_ID    = @pCreditlineId
      ,CLM_OPERATION_ID      = @pOperationId
      ,CLM_TYPE              = @pType
      ,CLM_OLD_VALUE          = @pOldvalue
      ,CLM_NEW_VALUE          = @pNewValue
      ,CLM_CREATION           = @pCreation
      ,CLM_CREATION_USER      = @pCreationUser
            WHERE   CLM_ID      =  @pId  
              AND   CLM_SITE_ID =  @pSiteId


ELSE

INSERT INTO CREDIT_LINE_MOVEMENTS
           (CLM_SITE_ID
           ,CLM_ID
           ,CLM_CREDIT_LINE_ID
           ,CLM_OPERATION_ID  
           ,CLM_TYPE          
           ,CLM_OLD_VALUE      
           ,CLM_NEW_VALUE      
           ,CLM_CREATION       
           ,CLM_CREATION_USER)  
     VALUES 
           (@pSiteId 
           ,@pID
           ,@pCreditlineId
           ,@pOperationId
           ,@pType
           ,@pOldvalue
           ,@pNewValue
           ,@pCreation
           ,@pCreationUser)  
END -- Update_CreditlineMovements
GO



