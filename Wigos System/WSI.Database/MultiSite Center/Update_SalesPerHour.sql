  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_SalesPerHour.sql
  --
  --   DESCRIPTION: Insert_SalesPerHour
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -- 30-05-2013  JML    Add translated game & Payout
  -- 01-SEP-2014 AMF    Progressive Jackpot
  -------------------------------------------------------------------------------- 

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SalesPerHour]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_SalesPerHour]
GO
  
CREATE PROCEDURE Update_SalesPerHour
  @pSiteId											INT 
, @pBaseHour										DATETIME
, @pTerminalId									INT
, @pGameId											INT
, @pPlayedCount									BIGINT
, @pPlayedAmount								MONEY
, @pWonCount										BIGINT
, @pWonAmount										MONEY
, @pTerminalName								NVARCHAR(50)
, @pGameName										NVARCHAR(50)
, @pTimeStamp										BIGINT
, @pTheoreticalWonAmount				MONEY
, @pPayout											MONEY
, @pUniqueId										BIGINT
, @pProgressiveProvisionAmount	MONEY
, @pJackpotAmount								MONEY
, @pProgressiveJackpotAmount		MONEY
, @pProgressiveJackpotAmount0		MONEY

AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   SALES_PER_HOUR
              WHERE   sph_unique_id   = @pUniqueId
                AND   SPH_SITE_ID     = @pSiteId)

     UPDATE   SALES_PER_HOUR
        SET   SPH_BASE_HOUR										 = @pBaseHour
            , SPH_PLAYED_COUNT							   = @pPlayedCount
            , SPH_PLAYED_AMOUNT								 = @pPlayedAmount
            , SPH_WON_COUNT										 = @pWonCount
            , SPH_WON_AMOUNT									 = @pWonAmount
            , SPH_TERMINAL_NAME								 = @pTerminalName
            , SPH_GAME_NAME										 = @pGameName
            , SPH_TIMESTAMP										 = @pTimeStamp
            , SPH_THEORETICAL_WON_AMOUNT			 = @pTheoreticalWonAmount
            , SPH_PAYOUT											 = @pPayout
            , SPH_TERMINAL_ID									 = @pTerminalId
            , SPH_GAME_ID											 = @pGameId
            ,	SPH_PROGRESSIVE_PROVISION_AMOUNT = @pProgressiveProvisionAmount
            , SPH_JACKPOT_AMOUNT							 = @pJackpotAmount
            , SPH_PROGRESSIVE_JACKPOT_AMOUNT	 = @pProgressiveJackpotAmount
            , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0 = @pProgressiveJackpotAmount0
      WHERE   sph_unique_id     = @pUniqueId
        AND   SPH_SITE_ID       = @pSiteId
        
  ELSE
  
    INSERT INTO   SALES_PER_HOUR
                ( SPH_BASE_HOUR
                , SPH_TERMINAL_ID
                , SPH_GAME_ID
                , SPH_PLAYED_COUNT
                , SPH_PLAYED_AMOUNT
                , SPH_WON_COUNT
                , SPH_WON_AMOUNT
                , SPH_SITE_ID
                , SPH_TERMINAL_NAME
                , SPH_GAME_NAME
                , SPH_TIMESTAMP
                , SPH_THEORETICAL_WON_AMOUNT
                , SPH_PAYOUT 
                , SPH_UNIQUE_ID
                , SPH_PROGRESSIVE_PROVISION_AMOUNT
                , SPH_JACKPOT_AMOUNT
                , SPH_PROGRESSIVE_JACKPOT_AMOUNT
                , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
                )
         VALUES
                ( @pBaseHour
                , @pTerminalId
                , @pGameId
                , @pPlayedCount
                , @pPlayedAmount
                , @pWonCount
                , @pWonAmount
                , @pSiteId
                , @pTerminalName 
                , @pGameName
                , @pTimeStamp
                , @pTheoreticalWonAmount
                , @pPayout 
                , @pUniqueId 
                , @pProgressiveProvisionAmount
                , @pJackpotAmount
                , @pProgressiveJackpotAmount
                , @pProgressiveJackpotAmount0               
                )

END -- Update_SalesPerHour
GO


