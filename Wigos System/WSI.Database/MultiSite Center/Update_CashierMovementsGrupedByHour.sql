--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_CashierMovementsGrupedByHour.sql
--
--   DESCRIPTION: Update_CashierMovementsGrupedByHour
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 27-FEB-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-FEB-2014 JML    First release.
-- 05-APR-2016 RAB    PBI 10787 (Phase 1): GameTable: Historification of the movements of box (MultiSite)
-------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_CashierMovementsGrupedByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_CashierMovementsGrupedByHour]
GO
 
CREATE PROCEDURE  [dbo].[Update_CashierMovementsGrupedByHour]
                  @pSiteId               INT  
                , @pDate                 DATETIME
                , @pType                 INT
                , @pSubType              INT
                , @pCurrencyIsoCode      NVARCHAR(3)
                , @pCurrencyDenomination MONEY
                , @pTypeCount            INT
                , @pSubAmount            MONEY
                , @pAddAmount            MONEY
                , @pAuxAmount            MONEY
                , @pInitialBalance       MONEY
                , @pFinalBalance         MONEY
                , @pTimeStamp            BIGINT
                , @pUniqueId             BIGINT
								, @pCageCurrencyType		 INT
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
              WHERE   CM_SITE_ID							= @pSiteId 
                AND   CM_UNIQUE_ID						= @pUniqueId)
  BEGIN
      UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR                   
         SET   CM_DATE                  = @pDate                   
             , CM_TYPE                  = @pType                   
             , CM_SUB_TYPE              = @pSubType                
             , CM_CURRENCY_ISO_CODE     = @pCurrencyIsoCode        
             , CM_CURRENCY_DENOMINATION = @pCurrencyDenomination   
             , CM_TYPE_COUNT            = @pTypeCount              
             , CM_SUB_AMOUNT            = @pSubAmount              
             , CM_ADD_AMOUNT            = @pAddAmount              
             , CM_AUX_AMOUNT            = @pAuxAmount              
             , CM_INITIAL_BALANCE       = @pInitialBalance         
             , CM_FINAL_BALANCE         = @pFinalBalance           
             , CM_TIMESTAMP             = @pTimeStamp
						 , CM_CAGE_CURRENCY_TYPE		= ISNULL(@pCageCurrencyType, 0)        
       WHERE   CM_SITE_ID								= @pSiteId                        
         AND   CM_UNIQUE_ID							= @pUniqueId
  END
  ELSE
  BEGIN
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
                  ( CM_SITE_ID
                  , CM_DATE
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_TYPE_COUNT
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_TIMESTAMP 
                  , CM_UNIQUE_ID
									, CM_CAGE_CURRENCY_TYPE )
          VALUES
                  ( @pSiteId    
                  , @pDate  
                  , @pType 
                  , @pSubType 
                  , @pCurrencyIsoCode 
                  , @pCurrencyDenomination 
                  , @pTypeCount 
                  , @pSubAmount 
                  , @pAddAmount 
                  , @pAuxAmount 
                  , @pInitialBalance 
                  , @pFinalBalance 
                  , @pTimeStamp  
                  , @pUniqueId
									, ISNULL(@pCageCurrencyType, 0))
  END
END
GO 