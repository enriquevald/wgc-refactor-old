  --------------------------------------------------------------------------------
  -- Copyright � 2014 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_TerminalsForOldVersion.sql
  --
  --   DESCRIPTION: Insert_Terminals For Old Version DDBB
  --
  --        AUTHOR: Alberto Marcos
  --
  -- CREATION DATE: 02-OCT-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 02-OCT-2014 AMF    First release.  
  -- 27-MAR-2015 FOS    Added columns TE_POSITION y TE_MACHINE_ID
  -------------------------------------------------------------------------------- 
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalsForOldVersion]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_TerminalsForOldVersion]
GO
  
CREATE PROCEDURE Update_TerminalsForOldVersion
       @pSiteId	INT		
      ,@pTerminalId INT                  
      ,@pType   INT				    
      ,@pServerId   INT					
      ,@pBaseName   NVARCHAR(50)			
      ,@pExternalId   NVARCHAR(40)			
      ,@pBlocked   BIT					
      ,@pActive   BIT					
      ,@pProviderId   NVARCHAR(50)			
      ,@pClientId   SMALLINT				
      ,@pBuildId   SMALLINT				
      ,@pTerminalType   SMALLINT				
      ,@pVendorId   NVARCHAR(50)			
      ,@pStatus   INT					
      ,@pRetirementDate  DATETIME				
      ,@pRetirementRequested  DATETIME				
      ,@pDenomination  MONEY					
      ,@pMultiDenomination  NVARCHAR(40)			
      ,@pProgram  NVARCHAR(40)			
      ,@pTheoreticalPayout  MONEY					
      ,@pProvId  INT					
      ,@pBankId  INT					
      ,@pFloorId  NVARCHAR(20)			
      ,@pGameType  INT					
      ,@pActivationDate  DATETIME				
      ,@pCurrentAccountId  BIGINT			    	
      ,@pCurrentPlaySessionId  BIGINT				    
      ,@pRegistrationCode  NVARCHAR(50)			
      ,@pSasFlags  INT					
      ,@pSerialNumber  NVARCHAR(50)			
      ,@pCabinetType  NVARCHAR(50)			
      ,@pJackpotContributionPct  NUMERIC	
      ,@pPosition  INT
      ,@pMachineId  NVARCHAR(50)
AS													
BEGIN												  
	IF EXISTS(SELECT   1   
             FROM   TERMINALS
            WHERE   TE_TERMINAL_ID = @pTerminalId
              AND   TE_SITE_ID		  = @pSiteId)
                
   UPDATE   TERMINALS
      SET   TE_TYPE                     = @pType 
          , TE_SERVER_ID								=	@pServerId 
          , TE_BASE_NAME								=	@pBaseName 
          , TE_EXTERNAL_ID							=	@pExternalId 
          , TE_BLOCKED									=	@pBlocked 
          , TE_ACTIVE									  =	@pActive 
          , TE_PROVIDER_ID							=	@pProviderId 
          , TE_CLIENT_ID								=	@pClientId 
          , TE_BUILD_ID								  =	@pBuildId 
          , TE_TERMINAL_TYPE						=	@pTerminalType 
          , TE_VENDOR_ID								=	@pVendorId 
          , TE_STATUS									  =	@pStatus 
          , TE_RETIREMENT_DATE					=	@pRetirementDate
          , TE_RETIREMENT_REQUESTED		  =	@pRetirementRequested
          , TE_DENOMINATION						  =	@pDenomination
          , TE_MULTI_DENOMINATION			  =	@pMultiDenomination
          , TE_PROGRAM									=	@pProgram
          , TE_THEORETICAL_PAYOUT			  =	@pTheoreticalPayout
          , TE_PROV_ID									=	@pProvId
          , TE_BANK_ID									=	@pBankId
          , TE_FLOOR_ID								  =	@pFloorId
          , TE_GAME_TYPE								=	@pGameType
          , TE_ACTIVATION_DATE					=	@pActivationDate
          , TE_CURRENT_ACCOUNT_ID			  =	@pCurrentAccountId
          , TE_CURRENT_PLAY_SESSION_ID  =	@pCurrentPlaySessionId
          , TE_REGISTRATION_CODE				=	@pRegistrationCode
          , TE_SAS_FLAGS								=	@pSasFlags
          , TE_SERIAL_NUMBER						=	@pSerialNumber
          , TE_CABINET_TYPE						  =	@pCabinetType
          , TE_JACKPOT_CONTRIBUTION_PCT =	@pJackpotContributionPct
					, TE_POSITION									=	@pPosition
          , TE_MACHINE_ID								= @pMachineId
    WHERE   TE_TERMINAL_ID = @pTerminalId
  		AND   TE_SITE_ID		 = @pSiteId
                            
ELSE
  INSERT INTO   TERMINALS
							( TE_TERMINAL_ID
              , TE_TYPE
              , TE_SERVER_ID
              , TE_BASE_NAME
              , TE_EXTERNAL_ID
              , TE_BLOCKED
              , TE_ACTIVE
              , TE_PROVIDER_ID
              , TE_CLIENT_ID
              , TE_BUILD_ID
              , TE_TERMINAL_TYPE
              , TE_VENDOR_ID
              , TE_STATUS
              , TE_RETIREMENT_DATE
              , TE_RETIREMENT_REQUESTED
              , TE_DENOMINATION
              , TE_MULTI_DENOMINATION
              , TE_PROGRAM
              , TE_THEORETICAL_PAYOUT
              , TE_PROV_ID
              , TE_BANK_ID
              , TE_FLOOR_ID
              , TE_GAME_TYPE
              , TE_ACTIVATION_DATE
              , TE_CURRENT_ACCOUNT_ID
              , TE_CURRENT_PLAY_SESSION_ID
              , TE_REGISTRATION_CODE
              , TE_SAS_FLAGS
              , TE_SERIAL_NUMBER
              , TE_CABINET_TYPE
              , TE_JACKPOT_CONTRIBUTION_PCT
              , TE_SITE_ID 
              , TE_POSITION
              , TE_MACHINE_ID
              , TE_MASTER_ID)
       VALUES
              ( @pTerminalId
              , @pType
              , @pServerId 
              , @pBaseName 
              , @pExternalId 
              , @pBlocked 
              , @pActive 
              , @pProviderId 
              , @pClientId 
              , @pBuildId 
              , @pTerminalType 
              , @pVendorId 
              , @pStatus 
              , @pRetirementDate
              , @pRetirementRequested
              , @pDenomination
              , @pMultiDenomination
              , @pProgram
              , @pTheoreticalPayout
              , @pProvId
              , @pBankId
              , @pFloorId
              , @pGameType
              , @pActivationDate
              , @pCurrentAccountId
              , @pCurrentPlaySessionId
              , @pRegistrationCode
              , @pSasFlags
              , @pSerialNumber
              , @pCabinetType
              , @pJackpotContributionPct
              , @pSiteId
              , @pPosition
              , @pMachineId
              , @pTerminalId)
                                                                  
                                          
END --Procedure InsertTerminalsForOldVersion
GO