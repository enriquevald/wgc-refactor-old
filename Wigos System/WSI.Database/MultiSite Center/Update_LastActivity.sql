  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_LastActivity.sql
  --
  --   DESCRIPTION: Update account activity 
  --
  --        AUTHOR: Joan Marc Pepi�
  --
  -- CREATION DATE: 13-AUG-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 13-AUG-2013 JPJ    First release.  
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_LastActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_LastActivity]
GO

CREATE   PROCEDURE [dbo].[Update_LastActivity]
	 @pSite_Id											INT
	,@pAccount_Id										BIGINT
	,@pLast_Activity								DATETIME
	,@pRE_Balance										MONEY
	,@pPromo_RE_Balance							MONEY
	,@pPromo_NR_Balance							MONEY
	,@pIn_Session_RE_Balance				MONEY
	,@pIn_Session_Promo_RE_Balance	MONEY
	,@pIn_Session_Promo_NR_Balance	MONEY
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   LAST_ACCOUNT_ACTIVITY
              WHERE   LAA_SITE_ID			         = @pSite_Id
                AND   LAA_ACCOUNT_ID	         = @pAccount_Id)
  BEGIN

     UPDATE   LAST_ACCOUNT_ACTIVITY
        SET   LAA_LAST_ACTIVITY									= @pLast_Activity				
						, LAA_RE_BALANCE										= @pRE_Balance					
						, LAA_PROMO_RE_BALANCE							= @pPromo_RE_Balance				
						, LAA_PROMO_NR_BALANCE							= @pPromo_NR_Balance 			
						, LAA_IN_SESSION_RE_BALANCE					= @pIn_Session_RE_Balance		
						, LAA_IN_SESSION_PROMO_RE_BALANCE		= @pIn_Session_Promo_RE_Balance	
						, LAA_IN_SESSION_PROMO_NR_BALANCE		= @pIn_Session_Promo_NR_Balance	
			WHERE   LAA_SITE_ID		= @pSite_Id
		    AND   LAA_ACCOUNT_ID  = @pAccount_Id
  END      
  ELSE
  BEGIN
    INSERT INTO   LAST_ACCOUNT_ACTIVITY
                ( LAA_SITE_ID
                , LAA_ACCOUNT_ID
                , LAA_LAST_ACTIVITY
							  , LAA_RE_BALANCE
							  , LAA_PROMO_RE_BALANCE
							  , LAA_PROMO_NR_BALANCE
								, LAA_IN_SESSION_RE_BALANCE
								, LAA_IN_SESSION_PROMO_RE_BALANCE
								, LAA_IN_SESSION_PROMO_NR_BALANCE)
         VALUES
                ( @pSite_Id
                , @pAccount_Id
                , @pLast_Activity				
								, @pRE_Balance					
								, @pPromo_RE_Balance				
								, @pPromo_NR_Balance 			
								, @pIn_Session_RE_Balance		
								, @pIn_Session_Promo_RE_Balance	
								, @pIn_Session_Promo_NR_Balance	)
  END

  UPDATE   ACCOUNTS
     SET   AC_LAST_ACTIVITY = CASE WHEN @pLast_Activity > AC_LAST_ACTIVITY THEN @pLast_Activity ELSE AC_LAST_ACTIVITY END
   WHERE   AC_ACCOUNT_ID = @pAccount_Id
   
END -- Update_LastActivity
GO