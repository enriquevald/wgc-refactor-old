--------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Providers.sql
  --
  --   DESCRIPTION: Insert_Providers
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -------------------------------------------------------------------------------- 

CREATE PROCEDURE Update_Providers
                 @pSiteId  INT  
               , @pProviderId INT
               , @pName NVARCHAR(50)
               , @pHide BIT
               , @pPointsMultiplier MONEY
               , @p3gs BIT
               , @p3gsVendorId NVARCHAR(50)
               , @p3gsVendorIp NVARCHAR(50)
               , @pSiteJackpot BIT
               , @pOnlyRedeemable BIT 
AS
BEGIN   
IF EXISTS (SELECT 1 FROM PROVIDERS WHERE PV_ID = @pProviderId AND PV_SITE_ID = @pSiteId)
  BEGIN   
	  UPDATE   PROVIDERS
         SET   PV_NAME =              @pName				
             , PV_HIDE =		        @pHide		  
             , PV_POINTS_MULTIPLIER =	@pPointsMultiplier  
             , PV_3GS	=		        @p3gs	  
             , PV_3GS_VENDOR_ID =		@p3gsVendorId	  
             , PV_3GS_VENDOR_IP =		@p3gsVendorIp	  
             , PV_SITE_JACKPOT =		@pSiteJackpot	  
             , PV_ONLY_REDEEMABLE =	@pOnlyRedeemable	
       WHERE   PV_ID      =  @pProviderId 
         AND   PV_SITE_ID =  @pSiteId
  END
ELSE
  BEGIN   

    INSERT INTO   PROVIDERS
                ( PV_SITE_ID
                , PV_ID
                , PV_NAME
                , PV_HIDE
                , PV_POINTS_MULTIPLIER
                , PV_3GS
                , PV_3GS_VENDOR_ID
                , PV_3GS_VENDOR_IP
                , PV_SITE_JACKPOT
                , PV_ONLY_REDEEMABLE)
         VALUES ( @pSiteId              
                , @pProviderId			  
                , @pName				  
                , @pHide		  
                , @pPointsMultiplier  
                , @p3gs	  
                , @p3gsVendorId	  
                , @p3gsVendorIp	  
                , @pSiteJackpot	  
                , @pOnlyRedeemable)		  
  END
END -- Update_Providers
GO
