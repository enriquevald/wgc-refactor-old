  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PlaySessions.sql
  --
  --   DESCRIPTION: Update_PlaySessions
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -- 22-JUL-2014 AMF    Tito Columns.
  -- 23-JAN-2017 JML    Missing columns. Fixed Bug 8574: MultiSite: When transferring promotional credits not redeemable, Ticket-in is not updated in play sessions
  -------------------------------------------------------------------------------- 
   
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PlaySessions]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].Update_PlaySessions
GO  

CREATE PROCEDURE Update_PlaySessions 
  @pPlaySessionId BIGINT
, @pAccountId BIGINT
, @pTerminalId INT 
, @pType INT 
, @pTypedata XML 
, @pStatus INT 
, @pStarted DATETIME 
, @pInitialBalance MONEY 
, @pPlayedCount INT 
, @pPlayedAmount MONEY 
, @pWonCount INT 
, @pWonAmount MONEY 
, @pCashin MONEY 
, @pCashout MONEY 
, @pFinished DATETIME 
, @pFinalBalance MONEY 
, @pLocked DATETIME 
, @pStandalone BIT 
, @pPromo BIT 
, @pWcpTransactionId BIGINT 
, @pRedeemableCashin MONEY 
, @pRedeemableCashout MONEY 
, @pRedeemablePlayed MONEY 
, @pRedeemableWon MONEY 
, @pNonRedeemableCashin MONEY 
, @pNonRedeemableCashout MONEY 
, @pNonRedeemablePlayed MONEY 
, @pNonRedeemableWon MONEY 
, @pBalanceMismatch BIT 
, @pSpentUsed MONEY 
, @pCancellableAmount MONEY 
, @pReportedBalanceMismatch MONEY 
, @pReCashin MONEY 
, @pPromoReCashin MONEY 
, @pReCashout MONEY 
, @pPromoReCashout MONEY 
, @pSiteId INT 
, @pTimeStamp Bigint
, @pReTicketIn MONEY
, @pPromoReTicketIn MONEY
, @pBillsInAmount MONEY
, @pReTicketOut MONEY
, @pPromoNrTicketIn MONEY
, @pPromoNrTicketOut MONEY
, @pRedeemablePlayedOriginal MONEY
, @pRedeemableWonOriginal MONEY
, @pNonRedeemablePlayedOriginal MONEY
, @pNonRedeemableWonOriginal MONEY
, @pPlayedCountOriginal INT
, @pWonCountOriginal INT
, @pAuxFtReCashIn MONEY
, @pAuxFtNrCashIn MONEY
, @pReFoundInEgm MONEY
, @pNrFoundInEgm MONEY
, @pReRemainingInEgm MONEY
, @pNrRemainingInEgm MONEY
, @pHandpaysAmount MONEY
, @pHandpaysPaidAmount MONEY

AS
BEGIN  

	IF EXISTS (SELECT  1 
				 FROM  PLAY_SESSIONS
				WHERE   PS_SITE_ID         =  @pSiteId 
				  AND   PS_PLAY_SESSION_ID =  @pPlaySessionId)

	BEGIN
	UPDATE   PLAY_SESSIONS
	   SET   PS_ACCOUNT_ID                = @pAccountId  
		   , PS_TERMINAL_ID               = @pTerminalId    
		   , PS_TYPE                      = @pType  
		   , PS_TYPE_DATA                 = @pTypedata  
		   , PS_STATUS                    = @pStatus  
		   , PS_STARTED                   = @pStarted  
		   , PS_INITIAL_BALANCE           =	@pInitialBalance  
		   , PS_PLAYED_COUNT              = @pPlayedCount  
		   , PS_PLAYED_AMOUNT             = @pPlayedAmount  
		   , PS_WON_COUNT                 = @pWonCount  
		   , PS_WON_AMOUNT                = @pWonAmount  
		   , PS_CASH_IN                   = @pCashin  
		   , PS_CASH_OUT                  = @pCashout  
		   , PS_FINISHED                  = @pFinished  
		   , PS_FINAL_BALANCE             = @pFinalBalance  
		   , PS_LOCKED                    = @pLocked  
		   , PS_STAND_ALONE               = @pStandalone  
		   , PS_PROMO                     = @pPromo  
		   , PS_WCP_TRANSACTION_ID        = @pWcpTransactionId  
		   , PS_REDEEMABLE_CASH_IN        = @pRedeemableCashin  
		   , PS_REDEEMABLE_CASH_OUT       = @pRedeemableCashout  
		   , PS_REDEEMABLE_PLAYED         = @pRedeemablePlayed  
		   , PS_REDEEMABLE_WON            =	@pRedeemableWon  
		   , PS_NON_REDEEMABLE_CASH_IN    = @pNonRedeemableCashin  
		   , PS_NON_REDEEMABLE_CASH_OUT   = @pNonRedeemableCashout  
		   , PS_NON_REDEEMABLE_PLAYED     = @pNonRedeemablePlayed  
		   , PS_NON_REDEEMABLE_WON        = @pNonRedeemableWon  
		   , PS_BALANCE_MISMATCH          = @pBalanceMismatch  
		   , PS_SPENT_USED                = @pSpentUsed  
		   , PS_CANCELLABLE_AMOUNT        = @pCancellableAmount  
		   , PS_REPORTED_BALANCE_MISMATCH = @pReportedBalanceMismatch  
		   , PS_RE_CASH_IN                = @pReCashin  
		   , PS_PROMO_RE_CASH_IN          = @pPromoReCashin  
		   , PS_RE_CASH_OUT               = @pReCashout  
		   , PS_PROMO_RE_CASH_OUT         = @pPromoReCashout
		   , PS_TIMESTAMP                 = @pTimeStamp 
			 , PS_RE_TICKET_IN							= @pReTicketIn
			 , PS_PROMO_RE_TICKET_IN				= @pPromoReTicketIn
			 , PS_BILLS_IN_AMOUNT						= @pBillsInAmount
			 , PS_RE_TICKET_OUT							= @pReTicketOut
			 , PS_PROMO_NR_TICKET_IN				= @pPromoNrTicketIn
			 , PS_PROMO_NR_TICKET_OUT				= @pPromoNrTicketOut
       , PS_REDEEMABLE_PLAYED_ORIGINAL     = @pRedeemablePlayedOriginal     
       , PS_REDEEMABLE_WON_ORIGINAL        = @pRedeemableWonOriginal        
       , PS_NON_REDEEMABLE_PLAYED_ORIGINAL = @pNonRedeemablePlayedOriginal  
       , PS_NON_REDEEMABLE_WON_ORIGINAL    = @pNonRedeemableWonOriginal     
       , PS_PLAYED_COUNT_ORIGINAL          = @pPlayedCountOriginal          
       , PS_WON_COUNT_ORIGINAL             = @pWonCountOriginal   
       , PS_AUX_FT_RE_CASH_IN              = @pAuxFtReCashIn
       , PS_AUX_FT_NR_CASH_IN              = @pAuxFtNrCashIn          
       , PS_RE_FOUND_IN_EGM                = @pReFoundInEgm                 
       , PS_NR_FOUND_IN_EGM                = @pNrFoundInEgm                 
       , PS_RE_REMAINING_IN_EGM            = @pReRemainingInEgm             
       , PS_NR_REMAINING_IN_EGM            = @pNrRemainingInEgm             
       , PS_HANDPAYS_AMOUNT                = @pHandpaysAmount               
       , PS_HANDPAYS_PAID_AMOUNT           = @pHandpaysPaidAmount           
	 WHERE   PS_SITE_ID                   = @pSiteId 
	   AND   PS_PLAY_SESSION_ID           = @pPlaySessionId
	END
	ELSE
	BEGIN
	INSERT INTO   PLAY_SESSIONS
				( PS_PLAY_SESSION_ID 
				, PS_ACCOUNT_ID
				, PS_TERMINAL_ID
				, PS_TYPE
				, PS_TYPE_DATA
				, PS_STATUS
				, PS_STARTED
				, PS_INITIAL_BALANCE
				, PS_PLAYED_COUNT
				, PS_PLAYED_AMOUNT
				, PS_WON_COUNT
				, PS_WON_AMOUNT
				, PS_CASH_IN
				, PS_CASH_OUT
				, PS_FINISHED
				, PS_FINAL_BALANCE
				, PS_LOCKED
				, PS_STAND_ALONE
				, PS_PROMO
				, PS_WCP_TRANSACTION_ID
				, PS_REDEEMABLE_CASH_IN
				, PS_REDEEMABLE_CASH_OUT
				, PS_REDEEMABLE_PLAYED
				, PS_REDEEMABLE_WON
				, PS_NON_REDEEMABLE_CASH_IN
				, PS_NON_REDEEMABLE_CASH_OUT
				, PS_NON_REDEEMABLE_PLAYED
				, PS_NON_REDEEMABLE_WON
				, PS_BALANCE_MISMATCH
				, PS_SPENT_USED
				, PS_CANCELLABLE_AMOUNT
				, PS_REPORTED_BALANCE_MISMATCH
				, PS_RE_CASH_IN
				, PS_PROMO_RE_CASH_IN
				, PS_RE_CASH_OUT
				, PS_PROMO_RE_CASH_OUT
				, PS_SITE_ID
				, PS_TIMESTAMP
				, PS_RE_TICKET_IN 
				, PS_PROMO_RE_TICKET_IN 
				, PS_BILLS_IN_AMOUNT 
				, PS_RE_TICKET_OUT 
				, PS_PROMO_NR_TICKET_IN 
				, PS_PROMO_NR_TICKET_OUT
        , PS_REDEEMABLE_PLAYED_ORIGINAL      
        , PS_REDEEMABLE_WON_ORIGINAL         
        , PS_NON_REDEEMABLE_PLAYED_ORIGINAL  
        , PS_NON_REDEEMABLE_WON_ORIGINAL     
        , PS_PLAYED_COUNT_ORIGINAL           
        , PS_WON_COUNT_ORIGINAL 
        , PS_AUX_FT_RE_CASH_IN  
        , PS_AUX_FT_NR_CASH_IN               
        , PS_RE_FOUND_IN_EGM                 
        , PS_NR_FOUND_IN_EGM                 
        , PS_RE_REMAINING_IN_EGM             
        , PS_NR_REMAINING_IN_EGM             
        , PS_HANDPAYS_AMOUNT                 
        , PS_HANDPAYS_PAID_AMOUNT            
        )
		 VALUES
				( @pPlaySessionId
				, @pAccountId  
				, @pTerminalId    
				, @pType  
				, @pTypedata  
				, @pStatus  
				, @pStarted  
				, @pInitialBalance  
				, @pPlayedCount  
				, @pPlayedAmount  
				, @pWonCount  
				, @pWonAmount  
				, @pCashin  
				, @pCashout  
				, @pFinished  
				, @pFinalBalance  
				, @pLocked  
				, @pStandalone  
				, @pPromo  
				, @pWcpTransactionId  
				, @pRedeemableCashin  
				, @pRedeemableCashout  
				, @pRedeemablePlayed  
				, @pRedeemableWon  
				, @pNonRedeemableCashin  
				, @pNonRedeemableCashout  
				, @pNonRedeemablePlayed  
				, @pNonRedeemableWon  
				, @pBalanceMismatch  
				, @pSpentUsed  
				, @pCancellableAmount  
				, @pReportedBalanceMismatch  
				, @pReCashin  
				, @pPromoReCashin  
				, @pReCashout  
				, @pPromoReCashout    
				, @pSiteId
				, @pTimeStamp   
				, @pReTicketIn
				, @pPromoReTicketIn
				, @pBillsInAmount
				, @pReTicketOut
				, @pPromoNrTicketIn
				, @pPromoNrTicketOut
        , @pRedeemablePlayedOriginal     
        , @pRedeemableWonOriginal        
        , @pNonRedeemablePlayedOriginal  
        , @pNonRedeemableWonOriginal     
        , @pPlayedCountOriginal          
        , @pWonCountOriginal   
        , @pAuxFtReCashIn
        , @pAuxFtNrCashIn                    
        , @pReFoundInEgm                 
        , @pNrFoundInEgm                 
        , @pReRemainingInEgm             
        , @pNrRemainingInEgm             
        , @pHandpaysAmount               
        , @pHandpaysPaidAmount                  
        )
	END
	
END
GO