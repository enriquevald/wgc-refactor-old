  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: MultiSite_CashierMovementsGroupedByHour
  --
  --   DESCRIPTION: Stored Procedure that gets the cashier movements by hour and site
  --
  --        AUTHOR: Joan Marc Pepi� Jamison
  --
  -- CREATION DATE: 17-MAR-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 17-MAR-2014 JPJ    First release.
  -- 05-APR-2016 RAB    PBI 10787 (Phase 1): GameTable: Historification of the movements of box (MultiSite) 
  -------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSite_CashierMovementsGroupedByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]
GO
CREATE PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]        
         @pSiteId    NVARCHAR(MAX)
        ,@pDateFrom  DATETIME
        ,@pDateTo    DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

  IF (@pSiteId IS NOT NULL)
 			SET @_sql = 'SELECT   CM_SITE_ID'
  ELSE
			SET @_sql = 'SELECT   NULL'
 
			SET @_sql   = @_sql + '
													, CM_TYPE
													, CM_SUB_TYPE
													, CM_TYPE_COUNT							
													, CM_CURRENCY_ISO_CODE
													, CM_CURRENCY_DENOMINATION
													, CM_SUB_AMOUNT
													, CM_ADD_AMOUNT
													, CM_AUX_AMOUNT
													, CM_INITIAL_BALANCE
													, CM_FINAL_BALANCE
													, CM_CAGE_CURRENCY_TYPE
										 FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
										WHERE   CM_DATE >= ''' + CONVERT(VARCHAR, @pDateFrom, 21) + '''  
											AND   CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
  	
		IF (@pSiteId IS NOT NULL)
				SET @_sql = @_sql + ' AND CM_SITE_ID IN (' + @pSiteId + ')'
  		
    EXEC sp_executesql @_sql
					
END -- MultiSite_CashierMovementsGroupedByHour
GO

GRANT EXECUTE ON [dbo].[MultiSite_CashierMovementsGroupedByHour] TO [wggui] WITH GRANT OPTION
GO