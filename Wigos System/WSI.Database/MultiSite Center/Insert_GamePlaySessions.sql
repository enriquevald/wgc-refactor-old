--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Insert_GamePlaySessions
-- 
--   DESCRIPTION: Insert Game Play Sessions
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 03-JUN-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-JUN-2013 AMF    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_GamePlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Insert_GamePlaySessions
GO
CREATE PROCEDURE Insert_GamePlaySessions
  @pSiteId INT
, @pPlaySessionId BIGINT
, @pGameId INT
, @pAccountId BIGINT
, @pTerminalId INT
, @pPlayedCount INT
, @pPlayedAmount MONEY
, @pWonCount INT
, @pWonAmount MONEY
, @pPayout MONEY
AS
BEGIN   		  
  IF NOT EXISTS(SELECT   1 
				          FROM   GAME_PLAY_SESSIONS 
			           WHERE   GPS_PLAY_SESSION_ID = @pPlaySessionId
				           AND   GPS_GAME_ID         = @pGameId 
				           AND   GPS_SITE_ID         = @pSiteId)
              
  BEGIN
  INSERT INTO  GAME_PLAY_SESSIONS
  		       ( GPS_SITE_ID     
  		       , GPS_PLAY_SESSION_ID
 		         , GPS_GAME_ID      
		         , GPS_ACCOUNT_ID   
		         , GPS_TERMINAL_ID  
		         , GPS_PLAYED_COUNT 
		         , GPS_PLAYED_AMOUNT
 		         , GPS_WON_COUNT    
 		         , GPS_WON_AMOUNT   
 		         , GPS_PAYOUT )     
       VALUES                       
	           ( @pSiteId           
	           , @pPlaySessionId    
	           , @pGameId           
	           , @pAccountId        
	           , @pTerminalId       
	           , @pPlayedCount      
	           , @pPlayedAmount     
	           , @pWonCount         
	           , @pWonAmount        
	           , @pPayout )
  END
END -- Insert_GamePlaySessions
GO