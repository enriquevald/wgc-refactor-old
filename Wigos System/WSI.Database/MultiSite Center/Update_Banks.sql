  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_Banks.sql
  --
  --   DESCRIPTION: Update_Banks
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -------------------------------------------------------------------------------- 
CREATE PROCEDURE Update_Banks
           @pSiteId  INT  
          ,@pBankId INT
          ,@pAreaId INT
          ,@pName nvarchar(50)
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   BANKS
            WHERE   BK_BANK_ID =  @pBankId 
              AND   BK_SITE_ID =  @pSiteId)
              
UPDATE BANKS
   SET BK_AREA_ID =@pAreaId
      ,BK_NAME =@pName
            WHERE   BK_BANK_ID =  @pBankId 
              AND   BK_SITE_ID =  @pSiteId


ELSE
INSERT INTO BANKS
           (BK_BANK_ID
           ,BK_SITE_ID
           ,BK_AREA_ID
           ,BK_NAME)
     VALUES 
           (@pBankId 
           ,@pSiteId
           ,@pAreaId
           ,@pName)


END -- Update_Banks
GO


