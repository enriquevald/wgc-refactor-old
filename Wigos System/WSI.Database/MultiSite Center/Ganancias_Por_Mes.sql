--//STORED PROCEDUDE
/****** Object:  StoredProcedure [dbo].[sp_GetTerminals]    Script Date: 11/17/2017 13:58:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ganancias_Por_Mes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Ganancias_Por_Mes]
GO

CREATE PROCEDURE Ganancias_Por_Mes
	@pMonth			INT,
	@pYear			INT
AS
	BEGIN
		SET NOCOUNT ON;
		------------------------------------------------------------------------------
		-- DECLARATIONS.
		DECLARE @param_datetime							DATETIME;
		DECLARE @string_datetime						VARCHAR(100);
		DECLARE @tsmh_from								DATETIME;
		DECLARE @tsmh_from_days_to_substract			INT;
		DECLARE @tsmh_to								DATETIME;
		DECLARE @cantidad_de_puestos					INT;
		------------------------------------------------------------------------------
		-- SET INITIAL VALUES.
		SET @string_datetime = CONVERT(VARCHAR(4),@pYear)+'-'+CONVERT(VARCHAR(2),@pMonth)+'-01';
		SET @param_datetime = CONVERT(DATETIME,@string_datetime, 120);
		SET @tsmh_from_days_to_substract = DATEPART(DAY,@param_datetime)-1;
		SET @tsmh_from			= DATEADD(DAY, -@tsmh_from_days_to_substract, @param_datetime);
		SET @tsmh_to			= DATEADD(month, ((YEAR(@param_datetime) - 1900) * 12) + MONTH(@param_datetime), -1);
		SET @cantidad_de_puestos = (SELECT COUNT(TE_TERMINAL_ID) FROM TERMINALS);
		------------------------------------------------------------------------------
		SELECT 
				REPLACE(CONVERT(VARCHAR(11), tsmh_datetime, 105), '-', '/')																			AS 'D�a'
			,	MAX(ST_NAME)																														AS 'Sala'
			,	COUNT (DISTINCT TSMH_TERMINAL_ID)																												AS 'Puestos_Operativos'
			,	@cantidad_de_puestos																												AS 'Cant_de_Puestos'
			,	SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )		 							AS 'Apostado'
			,	(   SUM (CASE WHEN TSMH_METER_CODE = 1			THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
				  + SUM (CASE WHEN TSMH_METER_CODE = 2			THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END ))									AS 'Pagado'
			,	(       SUM (CASE WHEN TSMH_METER_CODE = 0		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
				 - (	SUM (CASE WHEN TSMH_METER_CODE = 1		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
					+	SUM (CASE WHEN TSMH_METER_CODE = 2		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )))									AS 'Ganancia' -- Ganancia = Apostado - Pagado
			,	CASE WHEN SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END ) <> 0 THEN
					  CONVERT(DECIMAL(19,2), (( SUM (CASE WHEN TSMH_METER_CODE = 1				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
					+ SUM (CASE WHEN TSMH_METER_CODE = 2				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )))
					/ CONVERT(DECIMAL, SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )) * 100.00)
				ELSE
					0.00
				END																																	AS 'Porcentaje_de_Pago' -- (Pagado/Apostado)*100
		 FROM		  TERMINAL_SAS_METERS_HISTORY
			LEFT JOIN TERMINALS 
				ON	TE_TERMINAL_ID	= TSMH_TERMINAL_ID
				AND	TE_SITE_ID	= TSMH_SITE_ID
			LEFT JOIN SITES		
				ON	ST_SITE_ID		= TE_SITE_ID 
				AND ST_SITE_ID		= TSMH_SITE_ID	
		 WHERE   
				TSMH_TYPE			= 20
			AND	TSMH_METER_CODE   IN (0,1,2,3,4,5,11,36,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,128,130,132,134,136)
			AND TSMH_DATETIME >= @tsmh_from 
			AND TSMH_DATETIME <= @tsmh_to
		 GROUP BY 
			  CONVERT(VARCHAR(11), tsmh_datetime, 105)
			, TSMH_SITE_ID
	END
GO

GRANT EXECUTE ON [Ganancias_Por_Mes] TO [wggui] WITH GRANT OPTION
GO

--//GENERIC REPORT
BEGIN
	DECLARE @rtc_store_name	NVARCHAR(200)
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)

  SET @rtc_store_name = 'Ganancias_Por_Mes'
	
  SET @rtc_report_name = '<LanguageResources><NLS09><Label>Data Report :: Earnings of Month</Label></NLS09><NLS10><Label>Informe de datos :: Ganancias del Mes</Label></NLS10></LanguageResources>'
	
  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
					<ReportToolDesignSheetsDTO>
						<LanguageResources>
							<NLS09>
								<Label>Monthly Report</Label>
							</NLS09>
							<NLS10>
								<Label>Reporte mensual</Label>
							</NLS10>
						</LanguageResources>
						<Columns>    
							<ReportToolDesignColumn>
								<Code>DIA</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Day</Label>
									</NLS09>
									<NLS10>
										<Label>D�a</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>   
							<ReportToolDesignColumn>
								<Code>Sala</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Site</Label>
									</NLS09>
									<NLS10>
										<Label>Sala</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>    
							<ReportToolDesignColumn>
								<Code>Puestos_Operativos</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Operative positions</Label>
									</NLS09>
									<NLS10>
										<Label>Puestos Operativos</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>     
							<ReportToolDesignColumn>
								<Code>Cant_de_Puestos</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Number of positions</Label>
									</NLS09>
									<NLS10>
										<Label>Cant. de Puestos</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>      
							<ReportToolDesignColumn>
								<Code>Apostado</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Bet</Label>
									</NLS09>
									<NLS10>
										<Label>Apostado</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>     
							<ReportToolDesignColumn>
								<Code>Pagado</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Paid</Label>
									</NLS09>
									<NLS10>
										<Label>Pagado</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>    
							<ReportToolDesignColumn>
								<Code>Ganancia</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Benefit</Label>
									</NLS09>
									<NLS10>
										<Label>Ganancia</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>    
							<ReportToolDesignColumn>
								<Code>Porcentaje_de_Pago</Code>
								<Width>300</Width>
								<EquityMatchType>Equality</EquityMatchType>
								<LanguageResources>
									<NLS09>
										<Label>Payment Percentage</Label>
									</NLS09>
									<NLS10>
										<Label>Porcentaje de Pago</Label>
									</NLS10>
								</LanguageResources>
							</ReportToolDesignColumn>
						</Columns>
					</ReportToolDesignSheetsDTO>
				</ArrayOfReportToolDesignSheetsDTO>'
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
					<Variables>
						<VariableDTO>
							<Name>[site]</Name>
							<Type>GeneralParam</Type>
							<MetaDataOne>Site</MetaDataOne>
							<MetaDataTwo>Name</MetaDataTwo>
						</VariableDTO>
						<VariableDTO>
							<Name>[customerName]</Name>
							<Type>GeneralParam</Type>
							<MetaDataOne>Cashier</MetaDataOne>
							<MetaDataTwo>Split.A.CompanyName</MetaDataTwo>
						</VariableDTO>
						<VariableDTO>
							<Name>[userName]</Name>
							<Type>CurrentUser</Type>
							<MetaDataOne>UserName</MetaDataOne>
							<MetaDataTwo />
						</VariableDTO>
						<VariableDTO>
							<Name>[pMonth]</Name>
							<Type>Parameter</Type>
							<MetaDataOne>@param_datetime</MetaDataOne>
							<MetaDataTwo>MMMM</MetaDataTwo>
						</VariableDTO>
						<VariableDTO>
							<Name>[pYear]</Name>
							<Type>Parameter</Type>
							<MetaDataOne>@param_datetime</MetaDataOne>
							<MetaDataTwo>yyyy</MetaDataTwo>
						</VariableDTO>
					</Variables>
					<ShowPrint>true</ShowPrint>
					<FilterType>MonthYear</FilterType>
					<FilterText>
						<LanguageResources>
							<NLS09>
								<Label>Months And Year</Label>
							</NLS09>
							<NLS10>
								<Label>Meses Y A�o</Label>
							</NLS10>
						</LanguageResources>
					</FilterText>
					<Filters>
						<ReportToolDesignFilter>
							<TypeControl>uc_date_picker</TypeControl>
							<TextControl>
								<LanguageResources>
									<NLS09>
										<Label>Month</Label>
									</NLS09>
									<NLS10>
										<Label>Mes</Label>
									</NLS10>
								</LanguageResources>
							</TextControl>
							<ParameterStoredProcedure>
								<Name>@param_datetime</Name>
								<Type>DateTime</Type>
							</ParameterStoredProcedure>
							<Methods>
								<Method>
									<Name>SetFormat</Name>
									<Parameters>14,0</Parameters>
								</Method>
							</Methods>
							<Propertys>
								<Property>
									<Name>ShowUpDown</Name>
									<Value>True</Value>
								</Property>
							</Propertys>
							<Value>TodayOpening()</Value>
						</ReportToolDesignFilter>
					</Filters>
				</ReportToolDesignFilterDTO>'
  
  IF NOT EXISTS(SELECT 1 
				FROM report_tool_config 
				WHERE rtc_store_name = @rtc_store_name)
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
    VALUES	/*RTC_REPORT_TOOL_ID */
			/*RTC_FORM_ID*/        (@rtc_form_id,
			/*RTC_LOCATION_MENU*/   2, 
            /*RTC_REPORT_NAME*/     @rtc_report_name,
            /*RTC_STORE_NAME*/      @rtc_store_name,
            /*RTC_DESIGN_FILTER*/   @rtc_design_filter,
            /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
            /*RTC_MAILING*/         0,
            /*RTC_STATUS*/          1,
            /*RTC_MODE*/            36,
			/*RTC_HTML_HEADER*/		NULL,
		    /*RTC_HTML_FOOTER*/		NULL)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
        , RTC_REPORT_NAME   = /*RTC_REPORT_NAME*/     @rtc_report_name
        , RTC_MODE_TYPE     =  /*RTC_MODE*/ 36
      WHERE RTC_STORE_NAME = @rtc_store_name
  END
END
GO

