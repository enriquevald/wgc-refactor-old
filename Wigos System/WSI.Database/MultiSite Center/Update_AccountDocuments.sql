--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AccountDocuments.sql
--
--   DESCRIPTION: Update Account Documents in Multisite
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 02-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AccountDocuments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_AccountDocuments]
GO

CREATE PROCEDURE Update_AccountDocuments
                 @pAccountId BIGINT
               , @pCreated DATETIME
               , @pModified DATETIME
               , @pData VARBINARY(MAX)
AS
BEGIN   
IF EXISTS (SELECT 1 FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId)
  BEGIN   
	  UPDATE   ACCOUNT_DOCUMENTS
       SET   AD_CREATED        = @pCreated				
           , AD_MODIFIED       = @pModified		  
           , AD_DATA           = @pData  
     WHERE   AD_ACCOUNT_ID     = @pAccountId 
  END
ELSE
  BEGIN   
  
    INSERT INTO   ACCOUNT_DOCUMENTS
                ( AD_ACCOUNT_ID
                , AD_CREATED
                , AD_MODIFIED
                , AD_DATA )
         VALUES ( @pAccountId 
                , @pCreated
                , @pModified
                , @pData )

  END
END -- Update_AccountDocuments
GO
 