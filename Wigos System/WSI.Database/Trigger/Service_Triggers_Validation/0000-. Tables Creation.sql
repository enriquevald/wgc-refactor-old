-------------------------------------------
-- Drop and create table ErrorHandling
-------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ErrorHandling]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[ErrorHandling](
		[pkErrorHandlingID] [INT] 			IDENTITY(1,1) 		NOT NULL,
		[Error_Number] 		[INT] 								NOT NULL,
		[Error_Message] 	[VARCHAR](4000) 						NULL,
		[Error_Severity] 	[SMALLINT] 							NOT NULL,
		[Error_State] 		[SMALLINT] 							NOT NULL	DEFAULT 1,
		[Error_Procedure] 	[VARCHAR](200) 						NOT NULL,
		[Error_Line] 		[INT] 								NOT NULL	DEFAULT 0,
		[UserName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[HostName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[Time_Stamp] 		[DATETIME] 							NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[pkErrorHandlingID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO