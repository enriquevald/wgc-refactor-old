/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'EnableTriggers_By_Service'.
Is implemented to set Enabled or Disabled the group of Triggers linked to the service name passed.

Current Supported Values for @ServiceName: 
	- InHouseAPI.
	- AGG.
	- The rest of values are controlled to print advertisement with a 'not found message'.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
   @ServiceName: Service to affect its own triggers.
*/
/* Stored Procedure EnableTriggers_By_Service */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggers_By_Service]') AND type IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[EnableTriggers_By_Service]
GO

CREATE PROCEDURE [dbo].[EnableTriggers_By_Service] 
 @ServiceName VARCHAR(100)
AS
	BEGIN
	DECLARE @trigger_status_text VARCHAR(100);
	DECLARE @service_exists BIT = 0;
		BEGIN TRY
			SET NOCOUNT ON;
			/* InHouseAPI */
			IF(@ServiceName = 'InHouseAPI')
				BEGIN
					DECLARE @INHOUSEAPI BIT = ISNULL(( SELECT gp_key_value FROM general_params WHERE gp_group_key = 'InHouseAPI' AND gp_subject_key = 'Enabled'), 0);
					SET @service_exists = 1;
					EXEC EnableTriggerByNameAndTable @TableName = N'PLAY_SESSIONS', @ProcName = N'InHouseAPI_Play_Sessions_Insert', @Enable = @INHOUSEAPI
					EXEC EnableTriggerByNameAndTable @TableName = N'CUSTOMER_VISITS', @ProcName = N'InHouseAPI_Customer_Visits_Insert', @Enable = @INHOUSEAPI
					/* Action Performed Message  */
					IF @INHOUSEAPI = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';
				END
			/* Future Services Added Section ...*/
			/*
			----------------------------------------------------------------------------------------
			ELSE IF(@ServiceName = 'XXX')
				BEGIN
					/* Do Some Stuff... */
					IF @XXX = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';				
				END
			----------------------------------------------------------------------------------------
			*/
			IF (@service_exists = 1)
				SELECT @ServiceName + @trigger_status_text;
			ELSE
				SELECT 'ServiceName "' + @ServiceName + '" not found!';
		END TRY
		BEGIN CATCH
			EXEC dbo.spErrorHandling
		END CATCH
	END
GO