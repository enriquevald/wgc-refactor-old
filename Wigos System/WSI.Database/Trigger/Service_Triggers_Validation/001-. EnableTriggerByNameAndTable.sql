/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'EnableTriggerByNameAndTable'.
Is implemented to Enable or Disable a Trigger based on received parameters.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
   @TableName: Table Affected Name.
 , @ProcName: Trigger to be Enabled/Disabled Name.
 , @Enable: Bit to set Enabled/Disabled The Trigger.
*/
/* Stored Procedure EnableTriggerByNameAndTable */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggerByNameAndTable]') AND type IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[EnableTriggerByNameAndTable]
GO

CREATE PROCEDURE [dbo].[EnableTriggerByNameAndTable] 
   @TableName AS VARCHAR(150)
 , @ProcName AS VARCHAR(200)
 , @Enable AS BIT
AS
	BEGIN
		BEGIN TRY
			SET NOCOUNT ON;
			DECLARE @sql NVARCHAR(MAX);
			IF @Enable = 1 
				BEGIN
					SET @sql = 'ENABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
				END
			ELSE 
				BEGIN
					SET @sql = 'DISABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
				END
			IF EXISTS (SELECT * FROM sys.objects WHERE type = 'TR' AND name = @ProcName)
			EXEC sp_executesql @sql;
		END TRY
		BEGIN CATCH
			EXEC dbo.spErrorHandling
		END CATCH
	END
GO