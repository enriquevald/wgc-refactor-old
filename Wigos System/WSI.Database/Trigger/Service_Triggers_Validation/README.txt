This scripts folder allows to, given service name, enable/disable Database associated table triggers based on activation service flag on GeneralParams.
	- For example, InHouseAPI, setting InHouseAPI.Enabled=1, all table triggers become enabled otherwise if are set to 0 become disabled.

Legend:
.\0000-. Tables Creation.sql
		--> Creates table ErrorHandling (Is the same as AGG, allows to reuse same structure, if exists doesn't DROP TABLE).
.\000-. spErrorHandling.sql
		--> Drop/Create stored procedure to be able to inform ErrorHandling table. (Is the same as AGG, allows to reuse same structure).
.\001-. EnableTriggerByNameAndTable.sql
		--> Drop/Create stored procedure to Enable/Disable Triggers params based.
.\002-. EnableTriggers_By_Service.sql
		--> Drop/Create stored procedure to Enable/Disable Triggers aggregated by ServiceName.
.\999-. SampleCallToSetTriggers.sql
		--> Sample code to use that!


Prefix number, determines the execution order of scripts, are dependant!!