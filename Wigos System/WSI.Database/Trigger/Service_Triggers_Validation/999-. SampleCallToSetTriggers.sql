/*
----------------------------------------------------------------------------------------------------------------
Call to Set Enabled or Disabled the group of Triggers linked to the service name passed.

Current Supported Values for @ServiceName: 
	- InHouseAPI.
	- AGG.
	- The rest of values are controlled to print advertisement with a 'not found message'.
	


Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       Help Call.

Requeriments:

Parameters:
   @ServiceName: Service to affect its own triggers.
*/
/* Stored Procedure Call EnableTriggers_By_Service */
EXEC	[dbo].[EnableTriggers_By_Service] @ServiceName = N'InHouseAPI'