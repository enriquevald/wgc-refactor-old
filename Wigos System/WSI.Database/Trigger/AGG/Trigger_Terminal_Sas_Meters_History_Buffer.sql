--------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Trigger_Terminal_Sas_Meters_History_Buffer.sql
-- 
--   DESCRIPTION: Audit Script
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 07-SEP-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-SEP-2017 ETP    First release.
--------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_TSMH_Buffer_AGG]'))
BEGIN
    DROP TRIGGER Trigger_TSMH_Buffer_AGG
END

GO
CREATE TRIGGER Trigger_TSMH_Buffer_AGG on TERMINAL_SAS_METERS_HISTORY
   AFTER INSERT, UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    IF EXISTS (SELECT GP_KEY_VALUE FROM   GENERAL_PARAMS WHERE   GP_GROUP_KEY = 'AGG' AND   GP_SUBJECT_KEY = 'Buffering.Enabled' AND GP_KEY_VALUE = '1')
    BEGIN	

        
        INSERT INTO   BUFFER_TERMINAL_SAS_METERS_HISTORY
                    ( BTSMH_TERMINAL_ID
                    , BTSMH_METER_CODE
                    , BTSMH_GAME_ID
                    , BTSMH_DENOMINATION
                    , BTSMH_TYPE
                    , BTSMH_DATETIME )
             SELECT   TSMH_TERMINAL_ID
                    , TSMH_METER_CODE
                    , TSMH_GAME_ID
                    , TSMH_DENOMINATION
                    , TSMH_TYPE
                    , TSMH_DATETIME
               FROM   INSERTED;
    END   
	 
    SET NOCOUNT OFF;    
END
GO







