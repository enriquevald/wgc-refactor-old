--------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Trigger_Alarm_Buffer.sql
-- 
--   DESCRIPTION: Audit Script
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 07-SEP-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-SEP-2017 ETP    First release.
--------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_Alarms_Buffer_AGG]'))
BEGIN
    DROP TRIGGER Trigger_Alarms_Buffer_AGG
END

GO
CREATE TRIGGER Trigger_Alarms_Buffer_AGG on ALARMS
   AFTER INSERT, UPDATE
AS 
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    Declare @_alarm_category as int
    
    set @_alarm_category = 1
    
    IF EXISTS (SELECT GP_KEY_VALUE FROM   GENERAL_PARAMS WHERE   GP_GROUP_KEY = 'AGG' AND   GP_SUBJECT_KEY = 'Buffering.Enabled' AND GP_KEY_VALUE = '1')
    BEGIN	
    
        INSERT INTO   BUFFER_ALARMS
                    ( BA_ID )
             SELECT   AL_ALARM_ID
               FROM   INSERTED
               WHERE  AL_ALARM_CODE IN (    SELECT    DISTINCT(ALCC_ALARM_CODE)
                                              FROM    ALARM_CATEGORIES  
                                        INNER JOIN    ALARM_CATALOG_PER_CATEGORY
                                                ON    ALCC_CATEGORY = ALC_CATEGORY_ID
                                               AND    ALC_ALARM_GROUP_ID = @_alarm_category  );
    END    

    SET NOCOUNT OFF;
END
GO








