--------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Trigger_Terminal_Sas_Meters_Buffer.sql
-- 
--   DESCRIPTION: Audit Script
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 07-SEP-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-SEP-2017 ETP    First release.
--------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_TSM_Buffer_AGG]'))
BEGIN
    DROP TRIGGER Trigger_TSM_Buffer_AGG
END

GO
CREATE TRIGGER [dbo].[Trigger_TSM_Buffer_AGG] on [dbo].[terminal_sas_meters]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT GP_KEY_VALUE FROM   GENERAL_PARAMS WHERE  GP_GROUP_KEY = 'AGG' AND   GP_SUBJECT_KEY = 'Buffering.Enabled' AND GP_KEY_VALUE = '1')
	BEGIN	

		UPDATE   BUFFER_TERMINAL_SAS_METERS
		   SET   BTSM_TERMINAL_ID = BTSM_TERMINAL_ID
		  FROM   BUFFER_TERMINAL_SAS_METERS B
    INNER JOIN   INSERTED 
		    ON   B.BTSM_TERMINAL_ID   =   INSERTED.TSM_TERMINAL_ID 
		   AND   B.BTSM_METER_CODE    =   INSERTED.TSM_METER_CODE 
		   AND   B.BTSM_GAME_ID       =   INSERTED.TSM_GAME_ID 
		   AND   B.BTSM_DENOMINATION  =   INSERTED.TSM_DENOMINATION

    	INSERT INTO   BUFFER_TERMINAL_SAS_METERS
					( BTSM_TERMINAL_ID
					, BTSM_METER_CODE
					, BTSM_GAME_ID
					, BTSM_DENOMINATION )
			 SELECT   TSM_TERMINAL_ID
					, TSM_METER_CODE
					, TSM_GAME_ID
					, TSM_DENOMINATION
			   FROM   INSERTED
	LEFT OUTER JOIN   BUFFER_TERMINAL_SAS_METERS B 
	             ON   B.BTSM_TERMINAL_ID  =  INSERTED.TSM_TERMINAL_ID 
				AND   B.BTSM_METER_CODE   =  INSERTED.TSM_METER_CODE 
                AND   B.BTSM_GAME_ID      =  INSERTED.TSM_GAME_ID 
                AND   B.BTSM_DENOMINATION =  INSERTED.TSM_DENOMINATION
		      WHERE   B.BTSM_TERMINAL_ID   IS   NULL
			    AND   B.BTSM_METER_CODE    IS   NULL
                AND   B.BTSM_GAME_ID       IS   NULL
			    AND   B.BTSM_DENOMINATION  IS   NULL			   
    END    
    
    SET NOCOUNT OFF;
END
GO