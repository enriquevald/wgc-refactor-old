--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_SiteAccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
-- 22-MAY-2013 DDM    Fixed bug #793
-- 12-AGO-2013 JML    Add Last Activity
-- 13-FEB-2014 JPJ    Added restriction with virtual accounts
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              AS varbinary(20)
    DECLARE @hash1              AS varbinary(20)
    DECLARE @value              AS nvarchar(max)
    DECLARE @changed            AS bit
    DECLARE @updated            AS BIT    
-- For Last Activity 
    DECLARE @LastActyvity0          AS DATETIME
    DECLARE @ReBalance0             AS MONEY
    DECLARE @PromoREBalance0        AS MONEY
    DECLARE @PromoNRBalance0        AS MONEY
    DECLARE @SessionReBalance0      AS MONEY
    DECLARE @SessionPromoREBalance0 AS MONEY
    DECLARE @SessionPromoNRBalance0 AS MONEY
    DECLARE @LastActyvity1          AS DATETIME
    DECLARE @ReBalance1             AS MONEY
    DECLARE @PromoREBalance1        AS MONEY
    DECLARE @PromoNRBalance1        AS MONEY
    DECLARE @SessionReBalance1      AS MONEY
    DECLARE @SessionPromoREBalance1 AS MONEY
    DECLARE @SessionPromoNRBalance1 AS MONEY
    
    IF (UPDATE(AC_MS_CHANGE_GUID))  
         return

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

  IF   UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
    OR UPDATE (AC_HOLDER_OCCUPATION_ID)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION_ID)
    OR UPDATE (AC_SHOW_COMMENTS_ON_CASHIER)
    OR UPDATE (AC_HOLDER_ADDRESS_COUNTRY)
    OR UPDATE (AC_HOLDER_ADDRESS_VALIDATION)
        SET @updated = 1;      
  -- For Last Activity  
  IF   UPDATE (AC_LAST_ACTIVITY)
    OR UPDATE (AC_RE_BALANCE)
    OR UPDATE (AC_PROMO_RE_BALANCE)
    OR UPDATE (AC_PROMO_NR_BALANCE)
    OR UPDATE (AC_IN_SESSION_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_NR_BALANCE)
        SET @updated = 2;
        
  IF (@updated = 0) RETURN

  DECLARE PersonalInfoCursor CURSOR FOR 
   SELECT   AC_ACCOUNT_ID
          , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                              + ISNULL(AC_HOLDER_NAME,       '')
                              + ISNULL(AC_HOLDER_ID,         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                              + ISNULL(AC_HOLDER_ADDRESS_01, '')
                              + ISNULL(AC_HOLDER_ADDRESS_02, '')
                              + ISNULL(AC_HOLDER_ADDRESS_03, '')
                              + ISNULL(AC_HOLDER_CITY,       '')
                              + ISNULL(AC_HOLDER_ZIP,        '')
                              + ISNULL(AC_HOLDER_EMAIL_01,   '')
                              + ISNULL(AC_HOLDER_EMAIL_02,   '')
                              + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                              + ISNULL(AC_HOLDER_COMMENTS,        '')
                              + ISNULL(AC_HOLDER_ID1, '')
                              + ISNULL(AC_HOLDER_ID2, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                              + ISNULL(AC_HOLDER_NAME1, '')
                              + ISNULL(AC_HOLDER_NAME2, '')
                              + ISNULL(AC_HOLDER_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                              + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                              + ISNULL(AC_HOLDER_TITLE                        , '')
                              + ISNULL(AC_HOLDER_NAME4                        , '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                              + ISNULL(AC_HOLDER_STATE                        , '')
                              + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                              + ISNULL(AC_BLOCK_DESCRIPTION, '')
                              + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                              + ISNULL(AC_EXTERNAL_REFERENCE, '')
                              + ISNULL(AC_HOLDER_OCCUPATION, '')
                              + ISNULL(AC_HOLDER_EXT_NUM, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_FED_ENTITY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                              + ISNULL(AC_HOLDER_ID3_TYPE, '')
                              + ISNULL(AC_HOLDER_ID3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                              + ISNULL(AC_BENEFICIARY_NAME, '')
                              + ISNULL(AC_BENEFICIARY_NAME1, '')
                              + ISNULL(AC_BENEFICIARY_NAME2, '')
                              + ISNULL(AC_BENEFICIARY_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')  
                              + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID1, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID2, '')
                              + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')        
                              + ISNULL(AC_BENEFICIARY_ID3, '') 
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_OCCUPATION_ID), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_OCCUPATION_ID), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_SHOW_COMMENTS_ON_CASHIER), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ADDRESS_COUNTRY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ADDRESS_VALIDATION), '')
                              ) 
          , ISNULL(AC_LAST_ACTIVITY, GETDATE())
          , ISNULL(AC_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_NR_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_NR_BALANCE, 0)
     FROM   INSERTED
    WHERE   AC_TRACK_DATA not like '%-NEW-%'
      AND   AC_TYPE NOT IN (4, 5) -- Virtual accounts don't have to be sent to the Center
      
  SET NOCOUNT ON;

  OPEN PersonalInfoCursor

  FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                          , @hash1
                                          , @LastActyvity1
                                          , @ReBalance1
                                          , @PromoREBalance1
                                          , @PromoNRBalance1
                                          , @SessionReBalance1
                                          , @SessionPromoREBalance1
                                          , @SessionPromoNRBalance1

  WHILE @@FETCH_STATUS = 0
  BEGIN

    SELECT   @hash0                  = AC_MS_HASH          
           , @LastActyvity0          = AC_LAST_ACTIVITY
           , @ReBalance0             = AC_RE_BALANCE
           , @PromoREBalance0        = AC_PROMO_RE_BALANCE
           , @PromoNRBalance0        = AC_PROMO_NR_BALANCE
           , @SessionReBalance0      = AC_IN_SESSION_RE_BALANCE
           , @SessionPromoREBalance0 = AC_IN_SESSION_PROMO_RE_BALANCE
           , @SessionPromoNRBalance0 = AC_IN_SESSION_PROMO_NR_BALANCE
      FROM   DELETED 
     WHERE   AC_ACCOUNT_ID = @AccountId
		 
    SELECT   @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

    IF @changed = 1
    BEGIN
      DECLARE @new_id as uniqueidentifier
           
      SET @new_id = NEWID()
           
      -- Personal Info
      UPDATE   ACCOUNTS
         SET   AC_MS_HASH         = @hash1
             , AC_MS_CHANGE_GUID  = @new_id
       WHERE   AC_ACCOUNT_ID      = @AccountId
            
      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
        INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
      ELSE 
        UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId

    END

    SELECT @changed = CASE WHEN (    @LastActyvity0          = @LastActyvity1          
                                 AND @ReBalance0             = @ReBalance1             
                                 AND @PromoREBalance0        = @PromoREBalance1        
                                 AND @PromoNRBalance0        = @PromoNRBalance1        
                                 AND @SessionReBalance0      = @SessionReBalance1      
                                 AND @SessionPromoREBalance0 = @SessionPromoREBalance1 
                                 AND @SessionPromoNRBalance0 = @SessionPromoNRBalance1 ) THEN 0
                           ELSE 1 END

    IF (@changed = 1)
    BEGIN

      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_LAST_ACTIVITY WHERE LAA_ACCOUNT_ID = @AccountId)
        INSERT INTO  MS_SITE_PENDING_LAST_ACTIVITY (LAA_ACCOUNT_ID) VALUES (@AccountId )
      ELSE 
        UPDATE MS_SITE_PENDING_LAST_ACTIVITY SET LAA_ACCOUNT_ID = @AccountId WHERE LAA_ACCOUNT_ID = @AccountId

    END

    FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                            , @hash1
                                            , @LastActyvity1
                                            , @ReBalance1
                                            , @PromoREBalance1
                                            , @PromoNRBalance1
                                            , @SessionReBalance1
                                            , @SessionPromoREBalance1
                                            , @SessionPromoNRBalance1

  END

  CLOSE PersonalInfoCursor
  DEALLOCATE PersonalInfoCursor

END
GO


--
-- Disable/enable triggers for site members
--

--IF EXISTS (SELECT   1
--             FROM   GENERAL_PARAMS 
--            WHERE   GP_GROUP_KEY   = N'Site' 
--              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
--              AND   GP_KEY_VALUE   = N'1')
--BEGIN
--      EXEC MultiSiteTriggersEnable 1
--END
--ELSE
--BEGIN
--      EXEC MultiSiteTriggersEnable 0
--END
--GO 
