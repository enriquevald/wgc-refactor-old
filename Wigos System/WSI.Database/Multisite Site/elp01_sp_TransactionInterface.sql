  /****** Object:  StoredProcedure [dbo].[elp01_sp_TransactionInterface]    Script Date: 05/14/2013 11:51:55 ******/
  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: elp01_sp_TransactionInterface.sql
  --
  --   DESCRIPTION: elp01_sp_TransactionInterface
  --
  --        AUTHOR: Jordi Vera
  --
  -- CREATION DATE: 14-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 14-MAY-2013 JVV     First release.  
  -------------------------------------------------------------------------------- 
    
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[elp01_sp_TransactionInterface]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[elp01_sp_TransactionInterface]
GO
 CREATE PROCEDURE [dbo].[elp01_sp_TransactionInterface]
        @pElpPlaySessionId  BIGINT
       ,@pElpAccountId			BIGINT
       ,@pElpTerminalId			INT    
       ,@pElpStarted				DATETIME  
       ,@pElpFinished				DATETIME  
       ,@pElpPlayed					MONEY  
       ,@pElpWon						MONEY 
       ,@pElpPlayedCount		INT 
       ,@pElpCashin					MONEY
       ,@pElpCashout				MONEY
       ,@pKindof_ticket			INT
	     ,@pGameCode					INT

  AS
BEGIN          
		DECLARE @ticketNumber		AS BIGINT
		DECLARE @sequenceNumber AS BIGINT		
		DECLARE @teSerialnumber AS NVARCHAR(50)
		DECLARE @teFloorId			AS NVARCHAR(20)
		DECLARE @teBankId				AS INT
		DECLARE @teProvId				AS INT
		DECLARE @bkAreaId				AS INT
		DECLARE @SiteId 				AS INT
		DECLARE @AcUserType			AS INT
    		 		
		 		
  		/********** EXTERNAL DATA **********/
  	 --General params
  	 SELECT  @SiteId = CAST(GP_KEY_VALUE AS INT) 
		  FROM   GENERAL_PARAMS 
		 WHERE   GP_GROUP_KEY   = 'Site' 
		   AND   GP_SUBJECT_KEY = 'Identifier'
		   AND   ISNUMERIC(GP_KEY_VALUE) = 1	
		--Accounts 
		SELECT	 @AcUserType = AC_USER_TYPE
		 FROM		 ACCOUNTS 
		WHERE		 AC_ACCOUNT_ID = @pElpAccountId;			
		--Calculate TicketNumber / SequenceNumber
		SET @ticketNumber = @pElpPlaySessionId;	 		
		SET @sequenceNumber = @ticketNumber;		
		--Terminals
		SELECT @teSerialnumber = TE_SERIAL_NUMBER
					,@teFloorId = TE_FLOOR_ID
					,@teBankId = TE_BANK_ID
					,@teProvId = TE_PROV_ID
			FROM TERMINALS
		WHERE  TE_TERMINAL_ID = @pElpTerminalId;
		--Banks
		SELECT @bkAreaId = BK_AREA_ID 
		  FROM BANKS
		WHERE BK_BANK_ID = @teBankId;
		
				 
		/********** EXTERNAL DATA **********/
		
        IF (@teProvId IS NOT NULL AND ISNULL(@AcUserType,0) = 1) 
        BEGIN         
          INSERT INTO elp01_play_sessions
           ([eps_ticket_number]
           ,[eps_account_id]
           ,[eps_slot_serial_number]
           ,[eps_slot_house_number]
           ,[eps_venue_code]
           ,[eps_area_code]
           ,[eps_bank_code]
           ,[eps_vendor_code]
           ,[eps_game_code]
           ,[eps_start_time]
           ,[eps_end_time]
           ,[eps_bet_amount]
           ,[eps_paid_amount]
           ,[eps_games_played]
           ,[eps_initial_amount]
           ,[eps_aditional_amount]
           ,[eps_final_amount]
           ,[eps_bet_comb_code]
           ,[eps_kindof_ticket]
           ,[eps_sequence_number]
           ,[eps_cupon_number]
           ,[eps_date_updated]
           ,[eps_date_inserted])
     VALUES
           ( @ticketNumber
           , @pElpAccountId  
           , @teSerialnumber
           , ISNULL(@teFloorId,' ')
           , @SiteId
           , @bkAreaId
           , @teBankId
           , @teProvId		
           , @pGameCode	
           , @pElpStarted
           , @pElpFinished	
           , @pElpPlayed
           , @pElpWon
           , @pElpPlayedCount
           , @pElpCashin
           , 0 --AditionalAmount
           , @pElpCashout
           , NULL --BetComboCode 
           , @pKindof_ticket
           , @sequenceNumber
           , NULL --CuponNumber
           , GETDATE()
           , GETDATE()
            )                 
            
       END      
 END --elp01_sp_TransactionInterface
