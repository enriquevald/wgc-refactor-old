--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.  
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
-- 03-JUL-2013 DDM    Added new fields about Money Laundering
-- 11-NOV-2013 JPJ    Added new fields AC_HOLDER_OCCUPATION_ID and AC_BENEFICIARY_OCCUPATION_ID
-- 23-APR-2014 SMN		
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId                  bigint
, @pTrackData                  nvarchar(50)
,	@pHolderName                 nvarchar(200)
,	@pHolderId                   nvarchar(20)
, @pHolderIdType               int
, @pHolderAddress01            nvarchar(50)
, @pHolderAddress02            nvarchar(50)
, @pHolderAddress03            nvarchar(50)
, @pHolderCity                 nvarchar(50)
, @pHolderZip                  nvarchar(10) 
, @pHolderEmail01              nvarchar(50)
,	@pHolderEmail02              nvarchar(50)
,	@pHolderTwitter              nvarchar(50)
,	@pHolderPhoneNumber01        nvarchar(20)
, @pHolderPhoneNumber02        nvarchar(20)
, @pHolderComments             nvarchar(100)
, @pHolderId1                  nvarchar(20)
, @pHolderId2                  nvarchar(20)
, @pHolderDocumentId1          bigint
, @pHolderDocumentId2          bigint
, @pHolderName1                nvarchar(50)
,	@pHolderName2                nvarchar(50)
,	@pHolderName3                nvarchar(50)
, @pHolderGender               int
, @pHolderMaritalStatus        int
, @pHolderBirthDate            datetime
, @pHolderWeddingDate          datetime
, @pHolderLevel                int
, @pHolderLevelNotify          int
, @pHolderLevelEntered         datetime
,	@pHolderLevelExpiration      datetime
,	@pPin                        nvarchar(12)
, @pPinFailures                int
, @pPinLastModified            datetime
, @pBlocked                    bit
, @pActivated                  bit
, @pBlockReason                int
, @pHolderIsVip                int
, @pHolderTitle                nvarchar(15)                       
, @pHolderName4                nvarchar(50)                       
, @pHolderPhoneType01          int
, @pHolderPhoneType02          int
, @pHolderState                nvarchar(50)                    
, @pHolderCountry              nvarchar(50)                
, @pPersonalInfoSequenceId     bigint                     
, @pUserType                   int
, @pPointsStatus               int
, @pDeposit                    money
, @pCardPay                    bit
, @pBlockDescription           nvarchar(256) 
, @pMSHash                     varbinary(20) 
, @pMSCreatedOnSiteId          int
, @pCreated                    datetime
, @pExternalReference          nvarchar(50) 
, @pHolderOccupation           nvarchar(50) 	
, @pHolderExtNum               nvarchar(10) 
, @pHolderNationality          Int 
, @pHolderBirthCountry         Int 
, @pHolderFedEntity            Int 
, @pHolderId1Type              Int -- RFC
, @pHolderId2Type              Int -- CURP
, @pHolderId3Type              nvarchar(50) 
, @pHolderId3                  nvarchar(20) 
, @pHolderHasBeneficiary       bit  
, @pBeneficiaryName            nvarchar(200) 
, @pBeneficiaryName1           nvarchar(50) 
, @pBeneficiaryName2           nvarchar(50) 
, @pBeneficiaryName3           nvarchar(50) 
, @pBeneficiaryBirthDate       Datetime 
, @pBeneficiaryGender          int 
, @pBeneficiaryOccupation      nvarchar(50) 
, @pBeneficiaryId1Type         int   -- RFC
, @pBeneficiaryId1             nvarchar(20) 
, @pBeneficiaryId2Type         int   -- CURP
, @pBeneficiaryId2             nvarchar(20) 
, @pBeneficiaryId3Type         nvarchar(50) 
, @pBeneficiaryId3             nvarchar(20)
, @pHolderOccupationId         int
, @pBeneficiaryOccupationId    int
, @pPlayerTrackingMode         int 
, @pShowCommentsOnCashier      bit
, @pHolderAddressCountry       int
, @pHolderAddressValidation    int
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT
DECLARE @UserTypeSiteAccount as BIT

SELECT
       @pOtherAccountId = AC_ACCOUNT_ID 
      ,@UserTypeSiteAccount = AC_USER_TYPE
FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData

SET @pOtherAccountId = ISNULL(@pOtherAccountId, 0)
SET @UserTypeSiteAccount = ISNULL(@UserTypeSiteAccount, 1)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
	   IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
			AND @UserTypeSiteAccount = 0 -- IS ANONYMOUS
       BEGIN
			SET @pTrackData = '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
       END
       ELSE 
       BEGIN -- NOT IS ANONYMOUS OR ELP IS NOT ENABLED
			  UPDATE   ACCOUNTS   
				 SET   AC_TRACK_DATA =  '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
			   WHERE   AC_ACCOUNT_ID = @pOtherAccountId
       END


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL ELSE @pHolderLevel END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_NOTIFY ELSE @pHolderLevelNotify END
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_ENTERED ELSE @pHolderLevelEntered END
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @pBlocked 
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = CASE WHEN @pBlocked = 0 THEN 0 ELSE (dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason) END 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID   = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				          = @pUserType
         , AC_POINTS_STATUS             = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_POINTS_STATUS ELSE @pPointsStatus END
         , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription                
         , AC_CREATED                   = @pCreated
         , AC_MS_CREATED_ON_SITE_ID     = @pMSCreatedOnSiteId
         , AC_MS_HASH                   = @pMSHash
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
         , AC_SHOW_COMMENTS_ON_CASHIER  = @pShowCommentsOnCashier
         , AC_HOLDER_ADDRESS_COUNTRY    = @pHolderAddressCountry
         , AC_HOLDER_ADDRESS_VALIDATION = @pHolderAddressValidation
   WHERE   AC_ACCOUNT_ID                = @pAccountId 

END
GO

 