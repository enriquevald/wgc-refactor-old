 --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountInsert.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_SiteAccountInsert and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when center receive a new account, mark it to synchronize.
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--

 ------IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountInsert]') AND type in (N'TR'))
------DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountInsert]
------GO
------
------CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountInsert] ON [dbo].[accounts]
------   AFTER INSERT
------NOT FOR REPLICATION
------AS 
------BEGIN
------    DECLARE @Sequence10Value AS BIGINT
------    DECLARE @Sequence11Value AS BIGINT
------    DECLARE @AccountId       AS BIGINT
------    DECLARE @NewId as uniqueidentifier
------    
------    DECLARE InsertedCursor CURSOR FOR 
------     SELECT   INSERTED.AC_ACCOUNT_ID 
------            , INSERTED.AC_MS_CHANGE_GUID            
------       FROM   INSERTED
------      WHERE   INSERTED.AC_TRACK_DATA NOT LIKE '%-NEW-%' 
------
------    SET NOCOUNT ON;
------
------    OPEN InsertedCursor
------
------    FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
------    
------    WHILE @@FETCH_STATUS = 0
------    BEGIN       
------      
------      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
------      BEGIN
------        INSERT INTO   MS_SITE_PENDING_ACCOUNTS 
------                    ( SPA_ACCOUNT_ID
------                    , SPA_GUID)
------             VALUES ( @AccountId
------                    , @NewId )
------      END
------
------      FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
------    END
------
------    CLOSE InsertedCursor
------    DEALLOCATE InsertedCursor
------
------END
