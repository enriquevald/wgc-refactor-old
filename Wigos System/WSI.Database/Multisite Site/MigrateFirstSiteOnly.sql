

 USE [wgdb_000]

--
-- Upload local accounts
--
UPDATE   ACCOUNTS SET AC_MS_CHANGE_GUID = NEWID ()
DELETE   MS_SITE_PENDING_ACCOUNTS
INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID)
     SELECT   AC_ACCOUNT_ID, AC_MS_CHANGE_GUID 
       FROM   ACCOUNTS;

--
-- Upload points that count for level(from last year till now)
--
-- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 

DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS (SPM_MOVEMENT_ID)
     SELECT   AM_MOVEMENT_ID FROM ACCOUNT_MOVEMENTS 
      WHERE   AM_TYPE in (36, 72, 73, 68, 1102, 1202, 1302, 1402, 1502)
        AND   AM_DATETIME >= GETDATE () - 400 --- More than one year


-- 36 PointsAwarded
-- 72 ImportedPointsForLevel
-- 73 ImportedPointsHistory
-- 68 ManuallyAddedPointsForLevel 
-- 1102 MULTIPLE_BUCKETS_END_SESSION + PuntosNivel
-- 1202 MULTIPLE_BUCKETS_Expired + PuntosNivel
-- 1302 MULTIPLE_BUCKETS_Manual_Add + PuntosNivel
-- 1402 MULTIPLE_BUCKETS_Manual_Sub + PuntosNivel
-- 1502 MULTIPLE_BUCKETS_Manual_Set + PuntosNivel




--
-- Initial number of points (when joined to multisite)
--
DECLARE @BucketId_PuntosCanje BIGINT
SET @BucketId_PuntosCanje = 1

INSERT INTO   ACCOUNT_MOVEMENTS
                  ( AM_ACCOUNT_ID, AM_DATETIME, AM_TYPE, AM_INITIAL_BALANCE, AM_SUB_AMOUNT,        AM_ADD_AMOUNT,   AM_FINAL_BALANCE )
      SELECT		AC_ACCOUNT_ID,   GETDATE(),     101,                  0,             0, ISNULL(CBU_VALUE, 0), ISNULL(CBU_VALUE, 0) 
         FROM   ACCOUNTS LEFT JOIN CUSTOMER_BUCKET ON ac_account_id = cbu_customer_id AND cbu_bucket_id = @BucketId_PuntosCanje 
        WHERE   ISNULL(CBU_VALUE, 0) > 0
