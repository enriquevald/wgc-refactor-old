       --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Providers.sql
  --
  --   DESCRIPTION: Insert_Providers
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 21-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 21-MAY-2013 JML    First release. Different from the center.
  -- 17-SEP-2014 JML    Fixed Bug #WIG-1244
  -------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Providers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Providers]
GO

CREATE PROCEDURE [dbo].[Update_Providers]
                   @pProviderId INT
                 , @pName NVARCHAR(50)
                 , @pHide BIT
                 , @pPointsMultiplier MONEY
                 , @p3gs BIT
                 , @p3gsVendorId NVARCHAR(50)
                 , @p3gsVendorIp NVARCHAR(50)
                 , @pSiteJackpot BIT
                 , @pOnlyRedeemable BIT 
                 , @pMsSequenceId BIGINT		   
AS
BEGIN 

  DECLARE @pOtherProviderId as BIGINT

  SET @pOtherProviderId = (SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @pName)
    
  IF (@pOtherProviderId IS NOT NULL AND @pOtherProviderId <> @pProviderId)
  BEGIN  
    UPDATE   PROVIDERS
       SET   PV_NAME =  'CHANGING-' + CAST( @pOtherProviderId AS NVARCHAR)
     WHERE   PV_ID   = @pOtherProviderId	     
  END
   
  IF EXISTS (SELECT 1 FROM PROVIDERS WHERE PV_ID = @pProviderId)
  BEGIN
    UPDATE   PROVIDERS
       SET   PV_NAME           = @pName				
           , PV_HIDE           = @pHide		  
           , PV_MS_SEQUENCE_ID = @pMsSequenceId
     WHERE   PV_ID             = @pProviderId        
  END
  ELSE
  BEGIN
    SET IDENTITY_INSERT PROVIDERS ON
    
    INSERT INTO   PROVIDERS
                ( PV_ID
                , PV_NAME
                , PV_HIDE
                , PV_MS_SEQUENCE_ID)
         VALUES ( @pProviderId			  
                , @pName				  
                , @pHide		  
                , @pMsSequenceId)
    
    SET IDENTITY_INSERT PROVIDERS OFF
  END
END 
GO