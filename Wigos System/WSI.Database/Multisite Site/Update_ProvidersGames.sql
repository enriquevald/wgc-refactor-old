--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_ProvidersGames.sql
--
--   DESCRIPTION: Update Table Providers_Games in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 08-MAY-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAY-2013 JML    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_ProvidersGames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsInAccount]
GO

CREATE PROCEDURE Update_ProvidersGames
                 @pPvId          INT
               , @pGameId        INT
               , @pGameName      NVARCHAR(50)
               , @pPayout1       MONEY
               , @pPayout2       MONEY
               , @pPayout3       MONEY
               , @pPayout4       MONEY
               , @pPayout5       MONEY
               , @pPayout6       MONEY
               , @pPayout7       MONEY
               , @pPayout8       MONEY
               , @pPayout9       MONEY
               , @pPayout10      MONEY
               , @pMsSequenceId  BIGINT
AS
BEGIN   
IF EXISTS (SELECT 1 FROM PROVIDERS_GAMES WHERE PG_PV_ID = @pPvId AND PG_GAME_ID = @pGameId)
  BEGIN
	 UPDATE   PROVIDERS_GAMES
        SET   PG_GAME_NAME      = @pGameName
            , PG_PAYOUT_1       = @pPayout1
            , PG_PAYOUT_2       = @pPayout2
            , PG_PAYOUT_3       = @pPayout3
            , PG_PAYOUT_4       = @pPayout4
            , PG_PAYOUT_5       = @pPayout5
            , PG_PAYOUT_6       = @pPayout6
            , PG_PAYOUT_7       = @pPayout7
            , PG_PAYOUT_8       = @pPayout8
            , PG_PAYOUT_9       = @pPayout9
            , PG_PAYOUT_10      = @pPayout10
            , PG_MS_SEQUENCE_ID = @pMsSequenceId
      WHERE   PG_PV_ID   =  @pPvId 
        AND   PG_GAME_ID =  @pGameId
  END
ELSE
  BEGIN
    INSERT INTO   PROVIDERS_GAMES
                ( PG_PV_ID
                , PG_GAME_ID
                , PG_GAME_NAME
                , PG_PAYOUT_1
                , PG_PAYOUT_2
                , PG_PAYOUT_3
                , PG_PAYOUT_4
                , PG_PAYOUT_5
                , PG_PAYOUT_6
                , PG_PAYOUT_7
                , PG_PAYOUT_8
                , PG_PAYOUT_9
                , PG_PAYOUT_10
                , PG_MS_SEQUENCE_ID)
         VALUES
                ( @pPvId
                , @pGameId
                , @pGameName
                , @pPayout1
                , @pPayout2
                , @pPayout3
                , @pPayout4
                , @pPayout5
                , @pPayout6
                , @pPayout7
                , @pPayout8
                , @pPayout9
                , @pPayout10
                , @pMsSequenceId)
  END
END -- Update_ProvidersGames
GO

 