 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateAccount]
GO
CREATE PROCEDURE [dbo].[CreateAccount]
@AccountId    BIGINT       OUTPUT
AS
BEGIN
	DECLARE @SiteId            as INT
	DECLARE @NewTrackData      as NVARCHAR(50)

	SELECT   @SiteId = CAST(GP_KEY_VALUE AS INT) 
	  FROM   GENERAL_PARAMS 
	 WHERE   GP_GROUP_KEY   = 'Site' 
	   AND   GP_SUBJECT_KEY = 'Identifier'
	   AND   ISNUMERIC(GP_KEY_VALUE) = 1

	IF ( ISNULL(@SiteId, 0) <= 0 ) 
		RAISERROR ('SiteId not configured!', 20, 0) WITH LOG

	IF @SiteId > 999
		RAISERROR ('SiteId is greater than 999', 20, 0) WITH LOG

		
    SET @NewTrackData = '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
		
	UPDATE   SEQUENCES 
	   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
	 WHERE   SEQ_ID         = 4

	IF @@ROWCOUNT = 0 
	BEGIN
		RAISERROR ('Sequence #4 does not exist!', 20, 0) WITH LOG
	END

	SELECT @AccountId = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4

	SET @AccountId = 1000000 + @AccountId * 1000 + @SiteId % 1000
    
	INSERT INTO ACCOUNTS (AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) VALUES (@AccountId, 2, 1, @NewTrackData)

END
GO

GRANT EXECUTE ON [dbo].[CreateAccount] TO [wggui] WITH GRANT OPTION
GO
