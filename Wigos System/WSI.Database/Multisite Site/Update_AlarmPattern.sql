--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AlarmPatterns.sql
--
--   DESCRIPTION: Update Patterns in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 16-MAY-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAY-2014 JML    First release.  
-- 08-JUL-2014 JPJ    Added multilanguage support
-- 06-AUG-2014 DCS    Fixed Bug WIG-1162: Alarm catalog without update
-- 18-SEP-2014 JPJ    Mailing alarm functionality
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AlarmPatterns]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_AlarmPatterns]
GO 

CREATE PROCEDURE   [dbo].[Update_AlarmPatterns]
                    @pId                 BIGINT
                  , @pName               NVARCHAR(50)
                  , @pDescription        NVARCHAR(350)
                  , @pActive             BIT
                  , @pPattern            XML
                  , @pAlCode             INT
                  , @pAlName             NVARCHAR(50)
                  , @pAlDescription      NVARCHAR(350)
                  , @pAlSeverity         INT
                  , @pType               INT
                  , @pSource             INT
                  , @pLifeTime           INT
                  , @pLastFind           DATETIME
                  , @pDetections         INT
                  , @pScheduleTimeFrom   INT
                  , @pScheduleTimeTo     INT
                  , @pTimestamp          BIGINT

AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM PATTERNS WHERE PT_ID = @pId)
  
  BEGIN
    SET IDENTITY_INSERT PATTERNS ON

    INSERT INTO   PATTERNS ( PT_ID
                           , PT_NAME
                           , PT_DESCRIPTION
                           , PT_ACTIVE
                           , PT_PATTERN
                           , PT_AL_CODE
                           , PT_AL_NAME
                           , PT_AL_DESCRIPTION
                           , PT_AL_SEVERITY
                           , PT_TYPE
                           , PT_SOURCE
                           , PT_LIFE_TIME
                           , PT_LAST_FIND
                           , PT_DETECTIONS
                           , PT_SCHEDULE_TIME_FROM
                           , PT_SCHEDULE_TIME_TO
                           , PT_TIMESTAMP 
                           , PT_ACTIVE_MAILING )
                    VALUES ( @pId 
                           , @pName
                           , @pDescription
                           , @pActive
                           , @pPattern
                           , @pAlCode
                           , @pAlName
                           , @pAlDescription
                           , @pAlSeverity
                           , @pType
                           , @pSource
                           , @pLifeTime
                           , NULL
                           , 0
                           , @pScheduleTimeFrom
                           , @pScheduleTimeTo
                           , @pTimestamp 
                           , 0 ) 

    SET IDENTITY_INSERT PATTERNS OFF
    
		-- English language
	  INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE,  ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,         9,                1,         @pAlName,  @pAlDescription,  1           )
	  
		-- Spanish language
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,        10,               1,         @pAlName,  @pAlDescription,  1           )
    
    -- Insert alarm in his category 
		INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME)
																			VALUES (@pAlCode,        44,            1,         GetDate()    )
  
  END
  ELSE
  BEGIN
    UPDATE   PATTERNS
       SET   PT_NAME               = @pName
           , PT_DESCRIPTION        = @pDescription
           , PT_ACTIVE             = @pActive
           , PT_PATTERN            = @pPattern
           , PT_AL_CODE            = @pAlCode
           , PT_AL_NAME            = @pAlName
           , PT_AL_DESCRIPTION     = @pAlDescription
           , PT_AL_SEVERITY        = @pAlSeverity
           , PT_TYPE               = @pType
           , PT_SOURCE             = @pSource
           , PT_LIFE_TIME          = @pLifeTime
           , PT_SCHEDULE_TIME_FROM = @pScheduleTimeFrom
           , PT_SCHEDULE_TIME_TO   = @pScheduleTimeTo
           , PT_TIMESTAMP          = @pTimestamp
     WHERE   PT_ID  = @pId
     
     UPDATE ALARM_CATALOG 
			 SET   ALCG_DESCRIPTION			= @pAlDescription
					 , ALCG_NAME						= @pAlName
		 WHERE   ALCG_ALARM_CODE			= @pAlCode   
		 
  END

END
GO 
