--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSiteTriggersEnable
--
--   DESCRIPTION: Enable and disable the triggers of Multisite
--
--        AUTHOR: Dani Dom�nguez and Andreu Juli�
--
-- CREATION DATE: 02-APR-2013
--
-- REVISION HISTORY:
--
-- Date        Author    Description
-- ----------- --------- ----------------------------------------------------------
-- 08-MAR-2013 AJQ & DDM First release.  
-- 04-JUL-2013 JML       Add control of trigger on account_documents
-----------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTriggersEnable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSiteTriggersEnable]
GO

CREATE PROCEDURE [dbo].[MultiSiteTriggersEnable] 
@Enable as bit
WITH EXECUTE AS OWNER
AS
BEGIN
	
  SET NOCOUNT ON;
	
  IF @Enable = 1 
  BEGIN
    ENABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate    ON ACCOUNTS;
    ENABLE  TRIGGER Trigger_SiteToMultiSite_Points        ON ACCOUNT_MOVEMENTS;
    ENABLE  TRIGGER MultiSiteTrigger_SiteAccountDocuments ON ACCOUNT_DOCUMENTS;
  END
  ELSE 
  BEGIN
    DISABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate    ON ACCOUNTS;
    DISABLE  TRIGGER Trigger_SiteToMultiSite_Points        ON ACCOUNT_MOVEMENTS;
    DISABLE  TRIGGER MultiSiteTrigger_SiteAccountDocuments ON ACCOUNT_DOCUMENTS;
  END
	
END

GO

GRANT EXECUTE ON [dbo].[MultiSiteTriggersEnable] TO [wggui] WITH GRANT OPTION

GO