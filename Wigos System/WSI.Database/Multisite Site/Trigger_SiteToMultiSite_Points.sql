--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Trigger_SiteToMultiSite_Points.sql
-- 
--   DESCRIPTION: Procedures for trigger Trigger_SiteToMultiSite_Points and related issues in Site
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 25-FEB-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 25-FEB-2013 JML    First release.
-- 02-DIC-2013 JML    Add movements 68 & 72
-- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets.
-- 03-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronizaci�n MultiSite: Tablas Customer Buckets
-------------------------------------------------------------------------------
--
--   NOTES :
--
-- Movements related with points:
--     

-- Movements related with points:
--   36 -> PointsAwarded                    Redemption Points
--   37 -> PointsToGiftRequest              Redemption Points
--   38 -> PointsToNotRedeemable            Redemption Points
--   39 -> PointsGiftDelivery
--   40 -> PointsExpired                    Redemption Points
--   41 -> PointsToDrawTicketPrint          Redemption Points
--   42 -> PointsGiftServices
--   46 -> PointsToRedeemable               Redemption Points
--   50 -> CardAdjustment                   Redemption Points
--   60 -> PromotionPoint                   Redemption Points
--   61 -> CancelPromotionPoint             Redemption Points
--   62 -> ManualHolderLevelChanged         Level Points
--   66 -> CancelGiftInstance               Redemption Points
--   67 -> PointsStatusChanged
--   68 -> ManuallyAddedPointsForLevel      Level Points
--   71 -> ImportedPointsOnlyForRedeem      Redemption Points
--   72 -> ImportedPointsForLevel           Level Points
--   73 -> ImportedPointsHistory
--   86 -> ExternalSystemPointsSubstract
--  101 -> MultiSiteCurrentLocalPoints
-- 11xx -> MULTIPLE_BUCKETS_End_Session
-- 12xx -> MULTIPLE_BUCKETS_Expired
-- 13xx -> MULTIPLE_BUCKETS_Manual_Add
-- 14xx -> MULTIPLE_BUCKETS_Manual_Sub
-- 15xx -> MULTIPLE_BUCKETS_Manual_Set


IF  EXISTS (SELECT  * 
              FROM  sys.objects 
             WHERE  object_id = OBJECT_ID(N'[dbo].[Trigger_SiteToMultiSite_Points]') 
               AND  type in (N'TR'))
DROP TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
  IF NOT EXISTS (SELECT   *
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
        WHERE   ( (AM_TYPE IN (36, 37, 38, 39, 40, 41, 42, 46, 50, 60, 61, 62, 66, 67, 68, 71, 72, 73, 86, 101)) --, 84
                  or (AM_TYPE BETWEEN 1100 AND 1599) ) --Buckets movements
   
    SET NOCOUNT OFF
END -- [Trigger_SiteToMultiSite_Points]

GO
 
--
-- Disable/enable triggers for site members
--

--IF EXISTS (SELECT   *
--             FROM   GENERAL_PARAMS 
--            WHERE   GP_GROUP_KEY   = N'Site' 
--              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
--              AND   GP_KEY_VALUE   = N'1')
--BEGIN
--  EXEC MultiSiteTriggersEnable 1
--END
--ELSE
--BEGIN
--  EXEC MultiSiteTriggersEnable 0
--END
--
--GO
