--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_MasterProfiles.sql
--
--   DESCRIPTION: Update Master Profiles in the Site
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 21-JUN-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-JUN-2013 DDM    First release.  
-- 25-JUN-2013 JML    Add insertion of the form in the site
-- 26-JUN-2013 JML    Add "order" and "NLS" of the form
-- 09-JUL-2013 JML    Change GUP_MASTER_SECUENCE_ID by GUP_MASTER_SEQUENCE_ID
--                    Add condition for GUI_FORMS (defect WIG-46)
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_MasterProfiles]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_MasterProfiles]
GO 

CREATE PROCEDURE [dbo].[Update_MasterProfiles]
  @pReturn INT OUTPUT
AS
BEGIN

  DECLARE @MasterId           AS BIGINT
  DECLARE @ProfileName        AS NVARCHAR(40)
  DECLARE @ProfileId          AS BIGINT
  DECLARE @MaxUsers           AS INT
  DECLARE @MasterTimeStamp    AS BIGINT
  
  DECLARE @ProfileGuiId       AS INT
  DECLARE @ProfileFormId      AS INT
  DECLARE @ProfileReadPerm    AS BIT
  DECLARE @ProfileWritePerm   AS BIT
  DECLARE @ProfileDeletePerm  AS BIT
  DECLARE @ProfileExecutePerm AS BIT

  DECLARE @FormOrder          AS INT
  DECLARE @FormNLSId          AS INT

  SET @pReturn = 0

  -- (Defect WIG-46) Added the condition "IS NOT NULL GPF_GUI_ID", because when the profile is left without permissions tries to insert with nulls. 
  DECLARE FormsCursor CURSOR FOR SELECT DISTINCT GPF_GUI_ID, GPF_FORM_ID, GF_FORM_ORDER, GF_NLS_ID FROM #TEMP_MASTERS_PROFILES WHERE GPF_GUI_ID IS NOT NULL
  OPEN    FormsCursor

  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  WHILE @@FETCH_STATUS = 0
  BEGIN
	  IF NOT EXISTS (SELECT 1 FROM GUI_FORMS WHERE GF_GUI_ID = @ProfileGuiId AND GF_FORM_ID = @ProfileFormId )
    BEGIN
		  INSERT INTO GUI_FORMS (GF_GUI_ID,     GF_FORM_ID,     GF_FORM_ORDER, GF_NLS_ID  ) 
                     VALUES (@ProfileGuiId, @ProfileFormId, @FormOrder,    @FormNLSId )
	  END
	  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  END

  CLOSE      FormsCursor
  DEALLOCATE FormsCursor

  DECLARE ProfilesCursor CURSOR FOR SELECT DISTINCT GUP_MASTER_ID, GUP_NAME, GUP_MAX_USERS, GUP_TIMESTAMP  FROM #TEMP_MASTERS_PROFILES 
  OPEN    ProfilesCursor

  FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @ProfileId = NULL
    SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @MasterId )

    IF @ProfileId IS NULL 
    BEGIN
      SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_NAME = @ProfileName )
    END

    IF @ProfileId IS NULL 
    BEGIN
      -- is new profile
      SET @ProfileId = ISNULL((SELECT MAX(GUP_PROFILE_ID) FROM GUI_USER_PROFILES), 0) + 1
      
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME) 
                             VALUES (@ProfileId,     @ProfileName)
    END

    UPDATE   GUI_USER_PROFILES 
       SET   GUP_MAX_USERS          = ISNULL(@MaxUsers, 0) 
           , GUP_NAME               = @ProfileName 
           , GUP_MASTER_ID          = @MasterId
           , GUP_MASTER_SEQUENCE_ID = @MasterTimeStamp
     WHERE   GUP_PROFILE_ID = @ProfileId

    DELETE   GUI_PROFILE_FORMS
     WHERE   GPF_PROFILE_ID = @ProfileId
    
    INSERT   INTO GUI_PROFILE_FORMS 
		       ( GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM )
		       
	  SELECT   @ProfileId,     GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM 
	    FROM   #TEMP_MASTERS_PROFILES
	   WHERE   GUP_NAME = @ProfileName
	     AND   GPF_READ_PERM IS NOT NULL

    FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  END
  CLOSE      ProfilesCursor
  DEALLOCATE ProfilesCursor

  DROP TABLE #TEMP_MASTERS_PROFILES
  
  SET @pReturn = 1
 
END
GO