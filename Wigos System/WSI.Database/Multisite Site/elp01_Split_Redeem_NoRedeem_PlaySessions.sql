  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: elp01_Split_Redeem_NoRedeem_PlaySessions.sql
  --
  --   DESCRIPTION: Split Play Session Redeem - Non Redeem in ELP01 proc�s
  --
  --        AUTHOR: Jordi Vera
  --
  -- CREATION DATE: 10-SEP-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 10-SEP-2013 JVV     First release.  
  -------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[elp01_Split_Redeem_NoRedeem_PlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[elp01_Split_Redeem_NoRedeem_PlaySessions]
GO
CREATE PROCEDURE [dbo].[elp01_Split_Redeem_NoRedeem_PlaySessions] 
  @pPlaySessionId BIGINT
AS
BEGIN
  
  DECLARE @RedeemablePlayed             AS MONEY
  DECLARE @NonRedeemablePlayed          AS MONEY
  DECLARE @AccountId                    AS BIGINT
  DECLARE @TerminalId                   AS INT
  DECLARE @PlayedCount                  AS INT
  DECLARE @PlayedAmount                 AS MONEY
  DECLARE @WonCount                     AS INT
  DECLARE @WonAmount                    AS MONEY
  DECLARE @RedeemableWon                AS MONEY
  DECLARE @RedeemableCashIn             AS MONEY
  DECLARE @RedeemableCashOut            AS MONEY
  DECLARE @NonRedeemableWon             AS MONEY
  DECLARE @NonRedeemableCashIn          AS MONEY
  DECLARE @NonRedeemableCashOut         AS MONEY
  DECLARE @GameId                       AS INT
  DECLARE @PsStarted                    AS DATETIME
  DECLARE @PsFinished                   AS DATETIME
  DECLARE @TranslatedGameId             AS INT
   
  IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
  BEGIN

 SELECT TOP 1 @TranslatedGameId = ISNULL(TGT_TRANSLATED_GAME_ID,0)
								FROM   PLAYS WITH( INDEX(IX_plays))
								LEFT   OUTER JOIN TERMINAL_GAME_TRANSLATION ON   TGT_SOURCE_GAME_ID = PL_GAME_ID 
																			AND  TGT_TERMINAL_ID    = PL_TERMINAL_ID			
								WHERE   PL_PLAY_SESSION_ID = @pPlaySessionId
   SELECT 
         PS_PLAY_SESSION_ID
        ,PS_ACCOUNT_ID
        ,PS_TERMINAL_ID
        ,PS_PLAYED_COUNT
        ,PS_PLAYED_AMOUNT
        ,PS_WON_COUNT
        ,PS_WON_AMOUNT
        ,PS_REDEEMABLE_PLAYED
        ,PS_REDEEMABLE_WON
        ,PS_REDEEMABLE_CASH_IN
        ,PS_REDEEMABLE_CASH_OUT
        ,PS_NON_REDEEMABLE_PLAYED  
        ,PS_NON_REDEEMABLE_WON
        ,PS_NON_REDEEMABLE_CASH_IN
        ,PS_NON_REDEEMABLE_CASH_OUT
         , CASE WHEN (ISNULL(@TranslatedGameId,0)= 0)
                       THEN 
						   (SELECT TOP 1 ISNULL(TGT_TRANSLATED_GAME_ID,0)
									FROM   TERMINAL_GAME_TRANSLATION WHERE TGT_TERMINAL_ID = PS_TERMINAL_ID
									ORDER BY TGT_TRANSLATED_GAME_ID DESC)
                       ELSE @TranslatedGameId 
                       END AS PL_GAME_ID
        ,PS_STARTED
        ,PS_FINISHED
        INTO #ELP01_PLAY_SESSIONS
        FROM PLAY_SESSIONS  PS
        WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId AND PS_STATUS <> 0
         GROUP   BY 
           PS_PLAY_SESSION_ID
         , PS_ACCOUNT_ID
         , PS_TERMINAL_ID
         , PS_PLAYED_COUNT
         , PS_PLAYED_AMOUNT
         , PS_WON_COUNT
         , PS_WON_AMOUNT
         , PS_REDEEMABLE_PLAYED 
         , PS_REDEEMABLE_WON 
         , PS_REDEEMABLE_CASH_IN
         , PS_REDEEMABLE_CASH_OUT
         , PS_NON_REDEEMABLE_PLAYED
         , PS_NON_REDEEMABLE_WON
         , PS_NON_REDEEMABLE_CASH_IN
         , PS_NON_REDEEMABLE_CASH_OUT
         , PS_STARTED
         , PS_FINISHED  
    
    DECLARE    Elp01_Play_Sessions_Cursor CURSOR FOR 
     SELECT    PS_PLAY_SESSION_ID
        ,PS_REDEEMABLE_PLAYED
        ,PS_NON_REDEEMABLE_PLAYED  
        ,PS_ACCOUNT_ID
        ,PS_TERMINAL_ID
        ,PS_PLAYED_COUNT
        ,PS_PLAYED_AMOUNT
        ,PS_WON_COUNT
        ,PS_WON_AMOUNT      
        ,PS_REDEEMABLE_WON
        ,PS_REDEEMABLE_CASH_IN
        ,PS_REDEEMABLE_CASH_OUT    
        ,PS_NON_REDEEMABLE_WON
        ,PS_NON_REDEEMABLE_CASH_IN
        ,PS_NON_REDEEMABLE_CASH_OUT
        ,ISNULL(PL_GAME_ID,0)    
        ,PS_STARTED
        ,PS_FINISHED
       FROM   #ELP01_PLAY_SESSIONS

    SET NOCOUNT ON;

    OPEN Elp01_Play_Sessions_Cursor

    FETCH NEXT FROM Elp01_Play_Sessions_Cursor INTO @pPlaySessionId, @RedeemablePlayed, @NonRedeemablePlayed, @AccountId, @TerminalId, @PlayedCount, @PlayedAmount, @WonCount, @WonAmount, @RedeemableWon, @RedeemableCashIn, @RedeemableCashOut, @NonRedeemableWon, @NonRedeemableCashIn, @NonRedeemableCashOut, @GameId, @PsStarted, @PsFinished

    WHILE @@FETCH_STATUS = 0
    BEGIN  
        IF ((@RedeemablePlayed > 0 OR @RedeemableWon > 0) OR (@RedeemableCashIn <> @RedeemableCashOut))
        BEGIN
            EXEC dbo.elp01_sp_TransactionInterface   
                                @pElpPlaySessionId = @pPlaySessionId
                                ,@pElpAccountId = @AccountId
                                ,@pElpTerminalId = @TerminalId 
                                ,@pElpStarted= @PsStarted
                                ,@pElpFinished = @PsFinished
                                ,@pElpPlayed = @RedeemablePlayed
                                ,@pElpWon = @RedeemableWon
                                ,@pElpPlayedCount = @PlayedCount
                                ,@pElpCashin = @RedeemableCashIn
                                ,@pElpCashout = @RedeemableCashOut
                                ,@pKindof_ticket = 0
                                ,@pGameCode = @GameId;
                                                                  
        END
        IF ((@NonRedeemablePlayed > 0 OR @NonRedeemableWon > 0) OR (@NonRedeemableCashIn <> @NonRedeemableCashOut))
        BEGIN
              EXEC dbo.elp01_sp_TransactionInterface   
                                 @pElpPlaySessionId = @pPlaySessionId
                                ,@pElpAccountId = @AccountId
                                ,@pElpTerminalId = @TerminalId 
                                ,@pElpStarted= @PsStarted
                                ,@pElpFinished = @PsFinished
                                ,@pElpPlayed = @NonRedeemablePlayed
                                ,@pElpWon = @NonRedeemableWon
                                ,@pElpPlayedCount = @PlayedCount
                                ,@pElpCashin = @NonRedeemableCashIn
                                ,@pElpCashout = @NonRedeemableCashOut
                                ,@pKindof_ticket = 1
                                ,@pGameCode = @GameId;                                            
        END 
   
    UPDATE MS_PENDING_GAME_PLAY_SESSIONS SET MPS_STATUS = 1 WHERE MPS_PLAY_SESSION_ID = @pPlaySessionId --CHANGE STATUS TO ALREADY SPLIT
    
    FETCH NEXT FROM Elp01_Play_Sessions_Cursor INTO @pPlaySessionId, @RedeemablePlayed, @NonRedeemablePlayed, @AccountId, @TerminalId, @PlayedCount, @PlayedAmount, @WonCount, @WonAmount, @RedeemableWon, @RedeemableCashIn, @RedeemableCashOut, @NonRedeemableWon, @NonRedeemableCashIn, @NonRedeemableCashOut, @GameId, @PsStarted, @PsFinished
    END

    CLOSE Elp01_Play_Sessions_Cursor
    DEALLOCATE Elp01_Play_Sessions_Cursor
    DROP TABLE #ELP01_PLAY_SESSIONS
  
  END
  
END -- elp01_Split_Redeem_NoRedeem_PlaySessions
GO 