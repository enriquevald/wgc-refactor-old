 --------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_LcdMessages.sql
--
--   DESCRIPTION: Update Lcd Messages in the Site
--
--        AUTHOR: Didac Campanals Subirats
--
-- CREATION DATE: 03-DIC-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-DIC-2014 DCS    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_LcdMessages]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_LcdMessages]
GO 

CREATE PROCEDURE   [dbo].[Update_LcdMessages]
										 @pUniqueId							BIGINT        
									 , @pType									INT
									 , @pSiteList             XML
									 , @pTerminalList         XML
									 , @pAccountList          XML
									 , @pEnabled              BIT
									 , @pOrder								INT
									 , @pScheduleStart        DATETIME
									 , @pScheduleEnd          DATETIME
									 , @pScheduleWeekday      INT
									 , @pSchedule1TimeFrom    INT
									 , @pSchedule1TimeTo      INT
									 , @pSchedule2Enabled     BIT
									 , @pSchedule2TimeFrom    INT
									 , @pSchedule2TimeTo      INT
									 , @pMessage              XML
									 , @pResourceId           BIGINT
									 , @pMasterSequenceId     BIGINT									 
                   
AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM LCD_MESSAGES WHERE MSG_UNIQUE_ID = @pUniqueId)
  
  BEGIN
    
    INSERT INTO   LCD_MESSAGES ( 
																 MSG_UNIQUE_ID 
															 , MSG_TYPE
															 , MSG_SITE_LIST
															 , MSG_TERMINAL_LIST
															 , MSG_ACCOUNT_LIST
															 , MSG_ENABLED
															 , MSG_ORDER
															 , MSG_SCHEDULE_START
															 , MSG_SCHEDULE_END
															 , MSG_SCHEDULE_WEEKDAY
															 , MSG_SCHEDULE1_TIME_FROM
															 , MSG_SCHEDULE1_TIME_TO
															 , MSG_SCHEDULE2_ENABLED
															 , MSG_SCHEDULE2_TIME_FROM
															 , MSG_SCHEDULE2_TIME_TO
															 , MSG_MESSAGE
															 , MSG_RESOURCE_ID
															 , MSG_MASTER_SEQUENCE_ID
															 )
                       VALUES  (
                                 @pUniqueId            
															 , @pType                
															 , @pSiteList            
															 , @pTerminalList        
															 , @pAccountList         
															 , @pEnabled             
															 , @pOrder               
															 , @pScheduleStart       
															 , @pScheduleEnd         
															 , @pScheduleWeekday     
															 , @pSchedule1TimeFrom   
															 , @pSchedule1TimeTo     
															 , @pSchedule2Enabled    
															 , @pSchedule2TimeFrom   
															 , @pSchedule2TimeTo      
															 , @pMessage             
															 , @pResourceId          
															 , @pMasterSequenceId    
															 ) 

  END
  ELSE
  BEGIN
    UPDATE   LCD_MESSAGES
      SET 
					 MSG_TYPE										= @pType
				 , MSG_SITE_LIST							= @pSiteList
				 , MSG_TERMINAL_LIST					= @pTerminalList
				 , MSG_ACCOUNT_LIST						= @pAccountList
				 , MSG_ENABLED								= @pEnabled
				 , MSG_ORDER									= @pOrder
				 , MSG_SCHEDULE_START					= @pScheduleStart
				 , MSG_SCHEDULE_END						= @pScheduleEnd
				 , MSG_SCHEDULE_WEEKDAY				= @pScheduleWeekday
				 , MSG_SCHEDULE1_TIME_FROM		= @pSchedule1TimeFrom
				 , MSG_SCHEDULE1_TIME_TO			= @pSchedule1TimeTo
				 , MSG_SCHEDULE2_ENABLED			= @pSchedule2Enabled
				 , MSG_SCHEDULE2_TIME_FROM		= @pSchedule2TimeFrom
				 , MSG_SCHEDULE2_TIME_TO			= @pSchedule2TimeTo
				 , MSG_MESSAGE								= @pMessage
				 , MSG_RESOURCE_ID						= @pResourceId
				 , MSG_MASTER_SEQUENCE_ID			= @pMasterSequenceId
   WHERE   MSG_UNIQUE_ID							= @pUniqueId
		 
  END
END
GO 
