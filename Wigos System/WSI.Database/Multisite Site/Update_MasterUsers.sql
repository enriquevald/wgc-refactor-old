--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: update_MasterUsers.sql
--
--   DESCRIPTION: Update Corporate Users in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 15-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[update_MasterUsers]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[update_MasterUsers]
GO 

CREATE PROCEDURE   [dbo].[update_MasterUsers]
                   @ProfileMasterId       BIGINT
                 , @UserName              NVARCHAR(50)
                 , @UserEnabled           BIT
                 , @UserPassword          BINARY(40)
                 , @UserNotValidBefore    DATETIME
                 , @UserNotValidAfter     DATETIME
                 , @UserLastChanged       DATETIME
                 , @UserPasswordExp       DATETIME
                 , @UserPwdChgReq         BIT
                 , @UserLoginFailures     INT
                 , @UserFullName          NVARCHAR(50)
                 , @UserTimestamp         BIGINT
                 , @UserType              SMALLINT
                 , @UserSalesLimit        MONEY
                 , @UserMbSalesLimit      MONEY
                 , @UserBlockReason       INT
                 , @UserMasterId          INT
                 , @UserEmployeeCode      NVARCHAR(40)
                 
AS
BEGIN

  DECLARE @UserId             AS BIGINT
  DECLARE @ProfileId          AS BIGINT
  

  SET @ProfileId = NULL
  SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @ProfileMasterId )
 
  SET @UserId = NULL
  SET @UserId = ( SELECT GU_USER_ID FROM GUI_USERS WHERE GU_USERNAME = @UserName )

  IF @UserId IS NULL AND @ProfileId IS NOT NULL
  BEGIN
    -- is new User
    SET @UserId = ISNULL((SELECT MAX(GU_USER_ID) FROM GUI_USERS), 0) + 1
    
    INSERT INTO GUI_USERS (GU_USER_ID, GU_PROFILE_ID, GU_USERNAME, GU_ENABLED,   GU_PASSWORD,   GU_NOT_VALID_BEFORE, GU_PWD_CHG_REQ, GU_USER_TYPE,  GU_BLOCK_REASON) 
                   VALUES (@UserId,    @ProfileId,    @UserName,   @UserEnabled, @UserPassword, @UserNotValidBefore, @UserPwdChgReq, @UserType, @UserBlockReason)
  END

  IF @UserId IS NOT NULL AND @ProfileId IS NOT NULL
  BEGIN
    UPDATE   GUI_USERS                    
       SET   GU_PROFILE_ID          = @ProfileId  
           , GU_USERNAME            = @UserName            
           , GU_ENABLED             = @UserEnabled         
           , GU_PASSWORD            = @UserPassword        
           , GU_NOT_VALID_BEFORE    = @UserNotValidBefore  
           , GU_NOT_VALID_AFTER     = @UserNotValidAfter   
           , GU_LAST_CHANGED        = @UserLastChanged     
           , GU_PASSWORD_EXP        = @UserPasswordExp     
           , GU_PWD_CHG_REQ         = @UserPwdChgReq
           , GU_LOGIN_FAILURES      = @UserLoginFailures
           , GU_FULL_NAME           = @UserFullName 
           , GU_USER_TYPE           = @UserType        
           , GU_SALES_LIMIT         = @UserSalesLimit      
           , GU_MB_SALES_LIMIT      = @UserMbSalesLimit    
           , GU_BLOCK_REASON        = @UserBlockReason     
           , GU_MASTER_ID           = @UserMasterId        
           , GU_MASTER_SEQUENCE_ID  = @UserTimestamp
           , GU_EMPLOYEE_CODE       = @UserEmployeeCode       
     WHERE   GU_USER_ID   = @UserId
  END
END
GO 

