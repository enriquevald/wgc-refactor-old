--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountDocuments.sql
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_SiteAccountDocuments for insert data into MS_SITE_PENDING_ACCOUNT_DOCUMENTS
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
-------------------------------------------------------------------------------
--

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountDocuments]'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountDocuments]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountDocuments]
ON [dbo].[account_documents]
AFTER UPDATE
NOT FOR REPLICATION
AS
  BEGIN
    IF ( UPDATE(AD_MS_SEQUENCE_ID) )
    BEGIN      
      RETURN
    END

    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_DOCUMENTS
                ( PAD_ACCOUNT_ID )
         SELECT   AD_ACCOUNT_ID 
           FROM   INSERTED 
          WHERE   AD_ACCOUNT_ID NOT IN (SELECT PAD_ACCOUNT_ID FROM MS_SITE_PENDING_ACCOUNT_DOCUMENTS)
   
    SET NOCOUNT OFF

  END -- [MultiSiteTrigger_SiteAccountDocuments]
GO


--
-- Disable/enable triggers for site members
--

--IF EXISTS (SELECT   1
--             FROM   GENERAL_PARAMS 
--            WHERE   GP_GROUP_KEY   = N'Site' 
--              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
--              AND   GP_KEY_VALUE   = N'1')
--BEGIN
--      EXEC MultiSiteTriggersEnable 1
--END
--ELSE
--BEGIN
--      EXEC MultiSiteTriggersEnable 0
--END
--GO