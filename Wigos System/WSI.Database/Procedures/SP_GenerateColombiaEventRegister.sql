﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateColombiaEventRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
GO

CREATE PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
(
   @pTerminal Integer, 
   @pEventId Integer,
   @pEndDayRegisterDatetime DateTime = NULL
) 
AS

BEGIN

DECLARE @DefaultPayout AS INTEGER
DECLARE @Now as Datetime

set @Now = GETDATE()

--If Terminal payout is not defined, it uses the GP default payout
SELECT   @DefaultPayout = GP_KEY_VALUE
  FROM   GENERAL_PARAMS
 WHERE   GP_SUBJECT_KEY = 'TerminalDefaultPayout' AND GP_GROUP_KEY = 'PlayerTracking'

--Gets and pivots the specified terminal meters
    SELECT   TE_TERMINAL_ID
           , TSM_METER_CODE
           , CASE WHEN TSM_METER_CODE IN (0,1,2,3,11) THEN TSM_METER_VALUE/100.0 ELSE TSM_METER_VALUE END AS METER_VALUE
           , CASE WHEN TE_THEORETICAL_PAYOUT IS NOT NULL THEN TE_THEORETICAL_PAYOUT * 100 ELSE @DefaultPayout END AS TE_THEORETICAL_PAYOUT
           , TE_REGISTRATION_CODE-- AS NUC
           , TE_EXTERNAL_ID --AS NUID
           , TE_SERIAL_NUMBER --AS SERIAL
      INTO   #TEMP 
      FROM   TERMINALS
 LEFT JOIN   TERMINAL_SAS_METERS ON TSM_TERMINAL_ID = TE_TERMINAL_ID
     WHERE   TE_TERMINAL_ID = CASE WHEN @pTerminal <> 0 THEN  @pTerminal ELSE TE_TERMINAL_ID END 
       AND   TE_STATUS = CASE WHEN @pEventId <> 9 THEN 0 ELSE TE_STATUS END 
       AND   TE_TERMINAL_TYPE = 5

IF @pEndDayRegisterDatetime IS NOT NULL and @pEventId = 0
BEGIN
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @pEndDayRegisterDatetime
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS
END
--Inserts the row
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @Now
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS

--Inserts the row
     DROP TABLE #TEMP
END 

GRANT EXECUTE ON [dbo].[SP_GenerateColombiaEventRegister] TO [wggui] WITH GRANT OPTION
GO