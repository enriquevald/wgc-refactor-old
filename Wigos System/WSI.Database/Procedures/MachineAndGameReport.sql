--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MachineAndGameReport.sql
-- 
--   DESCRIPTION: Procedure for Daily Machine Report 
-- 
--        AUTHOR: Jordi Masachs 
-- 
-- CREATION DATE: 30-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-DEC-2014 JMV    First release
-- 01-DEC-2015 DLL    Fixed Bug TFS-7220: Remove meter promotion redimible
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Show meter history grouped by machine and by name
-- 
--  PARAMS:
--      - INPUT:
--  @pDateFrom DATETIME,  
--  @pDateTo   DATETIME,
--  @pTotalOnTop BIT = 0 
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
-- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MachineAndGameReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[MachineAndGameReport]
GO  


CREATE PROCEDURE [dbo].[MachineAndGameReport]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pTotalOnTop BIT = 0 
AS

BEGIN

-- Cash in fields
DECLARE @_ticket_in_RE INT
DECLARE @_ticket_in_pro_RE INT
DECLARE @_ticket_in_pro_NR INT
DECLARE @_credit_from_bills INT

-- Cash out fields
DECLARE @_ticket_out_RE INT
DECLARE @_ticket_out_pro_NR INT

-- Set field codes
SET @_ticket_in_RE = 0x80
SET @_ticket_in_pro_RE = 0x84
SET @_ticket_in_pro_NR = 0x82
SET @_credit_from_bills = 0x0B
SET @_ticket_out_RE = 0x86
SET @_ticket_out_pro_NR = 0x88

-- Create temp table for whole report
CREATE TABLE #TT_MACHINEGAME (TERMINAL_ID INT, TERMINAL_NAME NVARCHAR(50), GAMENAME NVARCHAR(50), CASH_IN MONEY, CASH_OUT MONEY, HANDPAYS MONEY, NETWIN MONEY)

-- Create temp table for terminal - game relation
CREATE TABLE #TT_GAMES (TTG_TERMINAL_ID INT, TTG_GAMENAME NVARCHAR(50))
INSERT INTO #TT_GAMES

    SELECT TGT_TERMINAL_ID AS TTG_TERMINAL_ID, CASE WHEN TOTAL > 1 THEN 'Multigame' ELSE TTG_GAMENAME END AS TTG_GAMENAME
    FROM
    (
        SELECT TGT_TERMINAL_ID, TOTAL, (SELECT TOP 1 PG_GAME_NAME FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID AND TGT_TERMINAL_ID = XX.TGT_TERMINAL_ID) AS TTG_GAMENAME
        FROM
        (
            SELECT TGT_TERMINAL_ID, count(*) AS TOTAL FROM
            (
              SELECT DISTINCT TGT_TERMINAL_ID, TGT_TRANSLATED_GAME_ID, PG_GAME_NAME
                FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES
               WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID
            ) YY
            GROUP BY TGT_TERMINAL_ID
        ) AS XX
    ) AS HH

-- Insert into the result values
INSERT INTO #TT_MACHINEGAME
	SELECT TSMH_TERMINAL_ID AS TERMINAL_ID, TE_NAME AS TERMINAL_NAME, (CASE WHEN TTG_GAMENAME IS NOT NULL THEN TTG_GAMENAME ELSE 'UNKNOWN' END) AS GAME_NAME, 
	CASH_IN, CASH_OUT, ISNULL(HANDPAYS, 0) AS HANDPAYS, CASH_IN - CASH_OUT - ISNULL(HANDPAYS, 0) AS NETWIN 
	FROM  
	(	SELECT TSMH_TERMINAL_ID, 
			 SUM(CASE WHEN 
				TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_IN,
			 SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_ticket_out_RE, @_ticket_out_pro_NR) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_OUT
		FROM	TERMINAL_SAS_METERS_HISTORY WITH (INDEX(IX_tsmh_type_code_datetime))
		WHERE	TSMH_TYPE = 1  
				AND TSMH_DATETIME >= @pDateFrom AND TSMH_DATETIME < @pDateTo 
				AND TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills, @_ticket_out_RE, @_ticket_out_pro_NR) 	
		GROUP BY TSMH_TERMINAL_ID ) AS TSMH_SUM 
	LEFT JOIN TERMINALS ON TSMH_TERMINAL_ID = TE_TERMINAL_ID
  LEFT JOIN #TT_GAMES ON TSMH_TERMINAL_ID = TTG_TERMINAL_ID
	LEFT JOIN 
	(
		SELECT 	
			HP_TERMINAL_ID,
			SUM(HP_AMOUNT) as HANDPAYS
			FROM HANDPAYS
			WHERE HP_DATETIME >= @pDateFrom AND HP_DATETIME < @pDateTo
		GROUP BY HP_TERMINAL_ID
		) AS HP_SUM ON TSMH_TERMINAL_ID = HP_TERMINAL_ID

-- Prepare report values and view
SELECT BLANK, CTYPE, CONCEPT, 
(CASE WHEN CASH_IN IS NOT NULL THEN CASH_IN ELSE 0 END) AS CASH_IN, 
(CASE WHEN CASH_OUT IS NOT NULL THEN CASH_OUT ELSE 0 END) AS CASH_OUT, 
(CASE WHEN HANDPAYS IS NOT NULL THEN HANDPAYS ELSE 0 END) AS HANDPAYS, 
(CASE WHEN NETWIN IS NOT NULL THEN NETWIN ELSE 0 END) AS NETWIN FROM
(
	SELECT '' as BLANK, 1 AS CTYPE, 2 AS CTYPE2, TERMINAL_NAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY TERMINAL_NAME

	UNION

	SELECT '' as BLANK, 2 AS CTYPE, 3 AS CTYPE2, GAMENAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY GAMENAME

	UNION

	SELECT '' as BLANK, 3 AS CTYPE, 1 AS CTYPE2, '' AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN
	FROM #TT_MACHINEGAME
	) X

ORDER BY CASE @pTotalOnTop WHEN 0 THEN CTYPE ELSE CTYPE2 END

DROP TABLE #TT_MACHINEGAME

DROP TABLE #TT_GAMES

END
GO

GRANT EXECUTE ON [dbo].[MachineAndGameReport] TO [wggui] WITH GRANT OPTION
GO
