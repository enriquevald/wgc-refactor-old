/****** Object:  StoredProcedure [dbo].[]    Script Date: 11/27/2015 09:43:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountsData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAccountsData]
GO
--------------------------------------------------------------------------------
-- PURPOSE: Get Account data by filters. 
-- 
--  PARAMS:
--      - INPUT:   @FirstName               NVARCHAR(50)
--				   @LastName                NVARCHAR(50)
--				   @DocumentID              NVARCHAR(20)
--				   @YearBirthDate           INT         
--				   @MonthBirthDate          INT         
--				   @DayBirthDate            INT         
--				   @YearWeddingDate         INT         
--				   @MonthWeddingDate		INT         
--				   @DayWeddingDate			INT         
--				   @OnlyVipAccount          BIT         
--				   @Phone                   NVARCHAR(20)
--				   @Email					NVARCHAR(50)
--				   @Gender                  INT         
--      - OUTPUT:
--
-- RETURNS:
--  Accounts data of selected filters.
--   NOTES:
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[GetAccountsData]
       @FirstName                NVARCHAR(50)  = NULL
       ,@LastName                NVARCHAR(50)  = NULL
       ,@DocumentID              NVARCHAR(20)  = NULL
       ,@YearBirthDate           INT           = NULL
       ,@MonthBirthDate          INT           = NULL
       ,@DayBirthDate            INT           = NULL
       ,@YearWeddingDate         INT           = NULL
       ,@MonthWeddingDate        INT           = NULL
       ,@DayWeddingDate          INT           = NULL
       ,@OnlyVipAccount          BIT           = NULL
       ,@Phone                   NVARCHAR(20)  = NULL
       ,@Email                   NVARCHAR(50)  = NULL
       ,@Gender                  INT           = NULL
AS
BEGIN

DECLARE @SQLString NVARCHAR(MAX);
DECLARE @WHERECAUSE NVARCHAR(500);
DECLARE @WITHINDEX NVARCHAR(500);

SET @WHERECAUSE = ''
SET @WITHINDEX = ''

IF (ISNULL(@FirstName, '') <> '') BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13)
+ 'AND AC_HOLDER_NAME3 LIKE ''%' + @FirstName + '%'''
END

IF (ISNULL(@LastName, '') <> '') BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13)
+ 'AND ( AC_HOLDER_NAME1 LIKE ''%' + @LastName + '%'' OR  AC_HOLDER_NAME2 LIKE ''%' + @LastName + '%'')'
END


IF (ISNULL(@DocumentID, '') <> '') BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13)
+ 'AND ( AC_HOLDER_ID LIKE ''%' + @DocumentID + '%'' 
	OR AC_HOLDER_ID1 LIKE ''%' + @DocumentID + '%'' 
	OR AC_HOLDER_ID2 LIKE ''%' + @DocumentID + '%'''
END

IF (ISNULL(@YearBirthDate, 0) <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND YEAR(AC_HOLDER_BIRTH_DATE) = ' + Convert(NVARCHAR(4),@YearBirthDate)
SET @WITHINDEX = CHAR(13) + ' WITH(INDEX(IX_ac_holder_birth_date)) '
END

IF (ISNULL(@MonthBirthDate, 0) <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND MONTH(AC_HOLDER_BIRTH_DATE) = ' + Convert(NVARCHAR(2),@MonthBirthDate)
SET @WITHINDEX = CHAR(13) + ' WITH(INDEX(IX_ac_holder_birth_date)) '
END

IF (ISNULL(@DayBirthDate, 0) <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND DAY(AC_HOLDER_BIRTH_DATE) = ' + Convert(NVARCHAR(2),@DayBirthDate)
SET @WITHINDEX = CHAR(13) + ' WITH(INDEX(IX_ac_holder_birth_date)) '
END


IF (ISNULL(@YearWeddingDate, 0) <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND YEAR(AC_HOLDER_WEDDING_DATE) = ' + Convert(NVARCHAR(4),@YearWeddingDate)
END

IF (ISNULL(@MonthWeddingDate, '') <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND MONTH(AC_HOLDER_WEDDING_DATE) = ' + Convert(NVARCHAR(2),@MonthWeddingDate)
END

IF (ISNULL(@DayWeddingDate, '') <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND DAY(AC_HOLDER_WEDDING_DATE) = ' + Convert(NVARCHAR(2),@DayWeddingDate)
END


IF (@OnlyVipAccount = 1) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) +
'AND AC_HOLDER_IS_VIP = 1'
END


IF (ISNULL(@Phone, '') <> '') BEGIN
SET @WHERECAUSE = @WHERECAUSE
+ CHAR(13) +
'AND ( AC_HOLDER_PHONE_NUMBER_01 LIKE ''%' + @Phone + '%'' OR AC_HOLDER_PHONE_NUMBER_02 LIKE ''%' + @Phone + '%'')'

END

IF (ISNULL(@Email, '') <> '') BEGIN
SET @WHERECAUSE = @WHERECAUSE
+ CHAR(13) +
'AND ( AC_HOLDER_EMAIL_01 LIKE ''%' + @Phone + '%'' OR AC_HOLDER_EMAIL_02 LIKE ''%' + @Email + '%'')'
END

IF (ISNULL(@Gender, 0) <> 0) BEGIN
SET @WHERECAUSE = @WHERECAUSE + CHAR(13) + 'AND DAY(AC_HOLDER_GENDER) = ' + Convert(NVARCHAR(1),@Gender)
END



SET @SQLString =
'SELECT     AC_HOLDER_BIRTH_DATE
		  , AC_HOLDER_CITY
		  , AC_CREATED
		  , AC_HOLDER_NAME
		  , AC_HOLDER_NAME1
		  , AC_HOLDER_NAME2
		  , AC_HOLDER_NAME3
		  , AC_HOLDER_NAME4
		  , AC_HOLDER_NICKNAME
		  , AC_HOLDER_GENDER
		  , AC_ACCOUNT_ID
		  , AC_HOLDER_STATE
		  , AC_HOLDER_ZIP
		  , AC_HOLDER_EMAIL_01
		  , AC_HOLDER_EMAIL_02
		  , AC_HOLDER_PHONE_NUMBER_01
		  , AC_HOLDER_PHONE_NUMBER_02
		  , AC_HOLDER_DOCUMENT_ID1
		  , AC_HOLDER_DOCUMENT_ID2
   FROM ACCOUNTS' 
+ CHAR(13) + @WITHINDEX 
+ CHAR(13) + 'WHERE 0 = 0 ' 
+ @WHERECAUSE 
+ CHAR(13) + 'ORDER BY AC_HOLDER_NAME';

EXEC (@SQLString)

END  -- PROCEDURE [dbo].[GetAccountsData]