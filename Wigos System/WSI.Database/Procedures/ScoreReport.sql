﻿------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: ScoreReport.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
------------------------------------------------------------------------------------------------------------------------------------------------
-- 19-JUL-2017 JML    PBI 28626: WIGOS-3190 Score report - Report in GUI
-- 09-AUG-2017 RAB    Bug 29293: WIGOS-4303 'Hourly score report' reports an incorrect Drop
-- 17-AUG-2017 RAB    Bug 29297: WIGOS-4304 'Hourly score report' reports the Win/Loss for the hour of the fill, and the next ones which does not have fills
-- 17-AUG-2017 RAB    Bug 29384: WIGOS-4391 Cashiers 'Hourly drop' does not match with Drop in WigosGUI 'Hourly score report'
-- 17-AUG-2017 RAB    Bug 29332: WIGOS-4150 Score report - Close gaming table amounts are not considered on WinLoss "Final" column
-- 26-OCT-2017 RAB    Bug 30434: WIGOS-6141 Score report: "An error occurred while formatting row data" is displayed after search in score report
------------------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
 )
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols             AS NVARCHAR(MAX)
  DECLARE @query            AS NVARCHAR(MAX)
  DECLARE @NationalCurrency AS NVARCHAR(3)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyIsoCode')
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE AND GTSC_ISO_CODE = @NATIONALCURRENCY
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo         
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)                                            

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(CASE WHEN GTPM_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(GTPM_VALUE, GTPM_ISO_CODE, @NationalCurrency) ELSE  GTPM_VALUE END)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)                                                                                           
                                              
      --  SET WIN/LOSS WITH CURSORS      
      DECLARE @GT_SESSION_ID BIGINT
      DECLARE GT_SESSION_WIN_LOSS_CURSOR CURSOR GLOBAL
      FOR SELECT GAMING_TABLE_SESSION_ID
            FROM #VALUE_TABLE 
        GROUP BY GAMING_TABLE_SESSION_ID
                                                            
      OPEN GT_SESSION_WIN_LOSS_CURSOR
      FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      
      -- FIRST CURSOR: ITERANCE THROUGH ALL SESSIONS OF #VALUE_TABLE
      WHILE(@@FETCH_STATUS = 0)
        BEGIN        
        
          DECLARE @WIN_LOSS_HOUR INT
          DECLARE @WIN_LOSS_LAST_AMOUNT MONEY
          DECLARE @WIN_LOSS_CURRENT_AMOUNT MONEY
          DECLARE @WIN_LOSS_FINAL_AMOUNT MONEY
          DECLARE @WIN_LOSS_CLOSING_HOUR AS INT
          DECLARE @WIN_LOSS_CURRENT_HOUR AS INT
          DECLARE @WIN_LOSS_CLOSING_TABLE_AMOUNT MONEY
          DECLARE @CREDITS_SUB_FILLS MONEY     
          DECLARE @DROP_CURRENT_AMOUNT MONEY		
            
          DECLARE HOUR_WIN_LOSS_CURSOR CURSOR GLOBAL
          FOR SELECT HORA
                FROM #VALUE_TABLE
               WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID              
                                                            
          OPEN HOUR_WIN_LOSS_CURSOR
          FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR     
                                      
          -- WE HAVE THE HOUR THE TABLE WAS CLOSED                                                            
           SELECT   @WIN_LOSS_CURRENT_HOUR = DATEPART(HOUR, ISNULL(CLOSING_DATE, GETDATE()))
                  , @WIN_LOSS_CLOSING_HOUR = DATEPART(HOUR, CLOSING_DATE)
             FROM   #VALUE_TABLE
            WHERE   GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
         GROUP BY   CLOSING_DATE
                                      
          SET @WIN_LOSS_FINAL_AMOUNT = 0
          SET @WIN_LOSS_CLOSING_TABLE_AMOUNT = (SELECT FINAL_BALANCE
                                                  FROM #VALUE_TABLE
                                                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                              GROUP BY FINAL_BALANCE)                 
                                                          
          -- SECOND CURSOR: ITERANCE THE 24 HOURS OF THE PREVIOUS CURSOR SESSION
          WHILE(@@FETCH_STATUS = 0)
            BEGIN
              
              IF @WIN_LOSS_HOUR <> 9999
              BEGIN
                -- GET CURRENT WIN LOSS AMOUNT BY THE CURRENT TIME ITERANCE              
                SET @WIN_LOSS_CURRENT_AMOUNT = (SELECT ISNULL(GTWL_WIN_LOSS_AMOUNT, 0)
                                                  FROM GAMING_TABLES_WIN_LOSS
                                                 WHERE GTWL_GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND GTWL_DATETIME_HOUR = DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom))
                
                SET @CREDITS_SUB_FILLS = (SELECT SUM(CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_SUB_AMOUNT END -
                                                     CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_ADD_AMOUNT END)
                                            FROM CASHIER_MOVEMENTS
                                           WHERE CM_DATE BETWEEN DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom) AND DATEADD(HOUR, @WIN_LOSS_HOUR + 1, @pDateFrom)
                                             AND CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                                             AND CM_CAGE_CURRENCY_TYPE = @pChipRE
                                             AND CM_SESSION_ID = (SELECT CASHIER_SESSION_ID
                                                                    FROM #VALUE_TABLE
                                                                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                                                GROUP BY CASHIER_SESSION_ID))                                           
                
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) <= @WIN_LOSS_CURRENT_HOUR
                BEGIN
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + GT_DROP, @DROP_CURRENT_AMOUNT = GT_DROP
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = @WIN_LOSS_HOUR                              
                END                
                
                -- IF THE ITERATED TIME IS EQUAL TO THE CLOSURE OF THE GAMBLING TABLE WE SET THE VALUE OF WIN LOSS
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) = @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  SET @WIN_LOSS_FINAL_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + ISNULL(@WIN_LOSS_CLOSING_TABLE_AMOUNT, 0)
                END
                
                -- UPDATE WIN LOSS LAST AMOUNT WITH CURRENT WIN LOSS AMOUNT
                IF DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom) <= GETDATE()
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0)
                END
                ELSE
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = 0
                END
              END
                           
              FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR                         
            
            END       
            CLOSE HOUR_WIN_LOSS_CURSOR
            DEALLOCATE HOUR_WIN_LOSS_CURSOR
  
        FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      END
      CLOSE GT_SESSION_WIN_LOSS_CURSOR
      DEALLOCATE GT_SESSION_WIN_LOSS_CURSOR         

      UPDATE #VALUE_TABLE
          SET GT_WIN_LOSS = (SELECT ISNULL(SUM(CASE WHEN CM_TYPE = 191 THEN ISNULL(CM_SUB_AMOUNT, 0)
                                               WHEN CM_TYPE = 201 THEN ISNULL(CM_FINAL_BALANCE, 0)
                                               END ), 0)
                               FROM CASHIER_MOVEMENTS
                              WHERE CM_SESSION_ID = CASHIER_SESSION_ID
                                AND cm_type IN (191, 201)
                                AND cm_cage_currency_type = 1001)
        WHERE HORA = 9999

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_DROP / A.GT_WIN_LOSS * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_WIN_LOSS <> 0
 
      --  SET OCCUPATION BY HOUR 
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999
       
      -- SET HOUR = 0 WHEN WIN/LOSS IS NOT ENTER 
      DECLARE @Table_Name AS NVARCHAR(50)
      DECLARE @Hour       AS DATETIME
      
      DECLARE BLANK_HOUR_CURSOR CURSOR GLOBAL
        FOR SELECT   GT_NAME
                   , GTWL_DATETIME_HOUR
              FROM   GAMING_TABLES
        INNER JOIN   GAMING_TABLES_SESSIONS  ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
        INNER JOIN ( SELECT   GTWL_GAMING_TABLE_SESSION_ID, GTWL_DATETIME_HOUR, GTWL_WIN_LOSS_AMOUNT 
                       FROM   GAMING_TABLES_WIN_LOSS 
                      WHERE   GTWL_DATETIME_HOUR >= @pDateFrom
                        AND   GTWL_DATETIME_HOUR <= DATEADD(DAY, 1, @pDateFrom)
                      UNION
                     SELECT   GTWL_GAMING_TABLE_SESSION_ID, CONVERT(DATETIME, LEFT(CONVERT(NVARCHAR(20), GETDATE(), 120), 14)+'00:00', 120), NULL AS GTWL_WIN_LOSS_AMOUNT 
                       FROM   GAMING_TABLES_WIN_LOSS 
                      WHERE   GTWL_DATETIME_HOUR >= (SELECT MAX(GTWL_DATETIME_HOUR) FROM  GAMING_TABLES_WIN_LOSS WHERE   GTWL_DATETIME_HOUR >= @pDateFrom )
                        AND   GTWL_DATETIME_HOUR <= DATEADD(HOUR, -1, GETDATE())  
                    ) AS A    ON GTS_GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID
          GROUP BY   GT_NAME, GTWL_DATETIME_HOUR
            HAVING   MAX(GTWL_WIN_LOSS_AMOUNT) IS NULL
                                                            
      OPEN BLANK_HOUR_CURSOR
      FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour     
      WHILE(@@FETCH_STATUS = 0)
        BEGIN
          UPDATE   #VALUE_TABLE
             SET   GT_DROP = 0
                 , GT_WIN_LOSS = 0
                 , GT_HOLD = 0
                 , GT_OCCUPATION_NUMBER = 0
           WHERE   TABLE_NAME = @Table_Name
             AND   DATEADD(HOUR, HORA,  @pDateFrom) = @Hour
        
          FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour                          
        END       
      CLOSE BLANK_HOUR_CURSOR
      DEALLOCATE BLANK_HOUR_CURSOR           
            
            
      --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO
