
--------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportCashDeskDrawsGenerator.sql
-- 
--   DESCRIPTION: Procedure for Cash Desk Draws detailed Report (in Raffle generator mode)
-- 
--        AUTHOR: Marcos Mohedano
-- 
-- CREATION DATE: 05-SEP-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 05-SEP-2013 MMG    First release.
-- 03-OCT-2013 MMG    Added new parameter (Site ID filter).
-- 08-OCT-2013 MMG    Added use of a new Index (IX_SITE_DATETIME).
-- 09-OCT-2013 MMG    Changed filters order.
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash desk draws filtering by its parameters
-- 
--  PARAMS:
--      - INPUT:
--   @pDrawFrom	   DATETIME     
--   @pDrawTo		   DATETIME      
--   @pDrawId		   BIGINT		  
--   @pSqlAccount  NVARCHAR(MAX) 
--   @pSiteId      INT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDrawsGenerator]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDrawsGenerator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ReportCashDeskDrawsGenerator]  
  @pDrawFrom      DATETIME      = NULL,
  @pDrawTo        DATETIME      = NULL,
  @pDrawId        BIGINT        = NULL,
  @pSqlAccount    NVARCHAR(MAX) = NULL,
  @pSiteId        INT           = NULL

AS
BEGIN

      SET NOCOUNT ON;

	  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	  IF @pSqlAccount IS NOT NULL
	  BEGIN
	    INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		  IF @@ROWCOUNT > 500
		     ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	  END

	  IF    @pDrawId IS NOT NULL 
      --
      -- Filtering by Draw ID
      --
      BEGIN       

            IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		    IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

            SELECT 
               CD_SITE_ID
               , CD_DRAW_ID
               , CD_DRAW_DATETIME
               , CD_OPERATION_ID
               , CD_ACCOUNT_ID
               , CD_COMBINATION_BET
               , CD_COMBINATION_WON
               , CD_RE_BET
               , CD_RE_WON
               , CD_NR_WON
            FROM
               CASHDESK_DRAWS
               WITH(INDEX(IX_CD_DRAW_DATETIME_SITE_ACCOUNT))        
            WHERE    
                  CD_DRAW_ID = @pDrawId  
               AND 
                  CD_DRAW_DATETIME >= @pDrawFrom
               AND
                  CD_DRAW_DATETIME < @pDrawTo
			   AND
			      ((@pSiteId IS NULL) OR (CD_SITE_ID = @pSiteId))
			   AND   
				  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )        
         
            ORDER BY CD_DRAW_ID

            DROP TABLE #ACCOUNTS_TEMP

            RETURN
            
      END 

	  IF @pSiteId IS NOT NULL 
      --
      -- Filtering by Site ID, but not by draw ID 
      --
      BEGIN       

            IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		    IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

            SELECT 
               CD_SITE_ID
               , CD_DRAW_ID
               , CD_DRAW_DATETIME
               , CD_OPERATION_ID
               , CD_ACCOUNT_ID
               , CD_COMBINATION_BET
               , CD_COMBINATION_WON
               , CD_RE_BET
               , CD_RE_WON
               , CD_NR_WON
            FROM
               CASHDESK_DRAWS
               WITH(INDEX(IX_CD_SITE_DATETIME_ACCOUNT))        
            WHERE    
                cd_site_id = @pSiteId  
               AND 
                  CD_DRAW_DATETIME >= @pDrawFrom
               AND
                  CD_DRAW_DATETIME < @pDrawTo	
			   AND   
				  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )		  
         
            ORDER BY CD_DRAW_ID

            DROP TABLE #ACCOUNTS_TEMP

            RETURN
            
      END         

      IF    @pSqlAccount IS NOT NULL 
      --
      -- Filtering by account, but not by draw ID neither Site ID
      --
      BEGIN     

			IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		    IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()              

            SELECT 
               CD_SITE_ID
               , CD_DRAW_ID
               , CD_DRAW_DATETIME
               , CD_OPERATION_ID
               , CD_ACCOUNT_ID
               , CD_COMBINATION_BET
               , CD_COMBINATION_WON
               , CD_RE_BET
               , CD_RE_WON
               , CD_NR_WON             
            FROM
               CASHDESK_DRAWS
               WITH(INDEX(IX_CD_ACCOUNT_DATETIME))         
               INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
            WHERE    
                  CD_DRAW_DATETIME >= @pDrawFrom
               AND
                  CD_DRAW_DATETIME < @pDrawTo               

            ORDER BY CD_DRAW_ID

            DROP TABLE #ACCOUNTS_TEMP

            RETURN
            
      END 
           
      --
      -- Filtering by draw date, but not by account neither Site ID nor Draw ID
      --       
      IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
      IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

      SELECT 
       CD_SITE_ID
         , CD_DRAW_ID
         , CD_DRAW_DATETIME
         , CD_OPERATION_ID
         , CD_ACCOUNT_ID
         , CD_COMBINATION_BET
         , CD_COMBINATION_WON
         , CD_RE_BET
         , CD_RE_WON
         , CD_NR_WON
      FROM
         CASHDESK_DRAWS
         WITH(INDEX(IX_CD_DATETIME))   
      WHERE     
            CD_DRAW_DATETIME >= @pDrawFrom
         AND
            CD_DRAW_DATETIME < @pDrawTo	    
      
      ORDER BY CD_DRAW_ID

      DROP TABLE #ACCOUNTS_TEMP
	
      RETURN
  
END
GO

GRANT EXECUTE ON [dbo].[ReportCashDeskDrawsGenerator] TO [wggui] WITH GRANT OPTION
GO

