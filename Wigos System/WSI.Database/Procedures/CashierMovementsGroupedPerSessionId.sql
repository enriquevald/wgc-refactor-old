 --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGroupedPerSessionId.sql
-- 
--   DESCRIPTION: Group cashier sessions by ID
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 11-MAY-2017 RGR    Added permission
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGroupedPerSessionId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
        @pCashierSessionId BIGINT     
  AS
BEGIN          	
DECLARE @CashierSessionId NVARCHAR(MAX)
SET @CashierSessionId = CONVERT(VARCHAR, @pCashierSessionId)

	IF EXISTS (SELECT CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId)
	BEGIN
		DELETE FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId
	END
		
		INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID 
					( CM_SESSION_ID
					, CM_TYPE
					, CM_SUB_TYPE
					, CM_TYPE_COUNT
					, CM_CURRENCY_ISO_CODE
					, CM_CURRENCY_DENOMINATION
					, CM_SUB_AMOUNT
					, CM_ADD_AMOUNT
					, CM_AUX_AMOUNT
					, CM_INITIAL_BALANCE
					, CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
					)
		EXEC CashierMovementsGrouped_Select @CashierSessionId, NULL, NULL
		--UPDATE HISTORY FLAG IN CASHIER_SESSIONS
		UPDATE cashier_sessions SET cs_history = 1 WHERE cs_session_id = @pCashierSessionId

 END --CreateCashierMovementsGroupedPerSessionId

 GRANT EXECUTE ON CashierMovementsGroupedPerSessionId TO wggui WITH GRANT OPTION 
