USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_MesasTorneos]    Script Date: 12-09-2017 7:27:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_SIOC_MesasTorneos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT '' AS nombre,
		   '' AS nroTorneoDia,
		   '' AS realizado,
		   '' AS motivoNoRealizado,
		   '' AS fechaInicio,
		   '' AS fechaTermino,
		   '' AS nroDias,
		   '' AS tipoDocumentoAutorizacion,
		   '' AS nroDocumentoAutorizacion,
		   '' AS fechaDocumentoAutorizacion,
		   '' AS entregaCreditos,
		   '' AS entregaEfectivo,
		   '' AS inscripciones_nroInscritos,
		   '' AS inscripciones_nroReinscritos,
		   '' AS inscripciones_nroAddOn,
		   '' AS recaudacionEfectivo_valorInscripcion,
		   '' AS recaudacionEfectivo_valorReinscripcion,
		   '' AS recaudacionEfectivo_valorAddOn,
		   '' AS recaudacionEfectivo_aporteCasinoPremios,
		   '' AS recaudacionEfectivo_aporteCasinoOtrosPremios,
		   '' AS recaudacionEfectivo_otrosAportesPremios_nro,
		   '' AS recaudacionEfectivo_otrosAportesPremios_monto,
		   '' AS recaudacionCredito_aporteCasinoPremios,
		   '' AS recaudacionCredito_aporteCasinoOtrosPremios,
		   '' AS premiosEfectivo_montoPremiosPrincipales,
		   '' AS premiosCredito_montoPremiosPrincipales,
		   '' AS idTorneo
END


GO

