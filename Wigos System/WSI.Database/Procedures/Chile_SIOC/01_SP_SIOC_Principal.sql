USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_Principal]    Script Date: 12-09-2017 7:11:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SP_SIOC_Principal]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- NOMBRES SHEETS
SELECT 'infoGlobalDiario', 1
UNION
SELECT 'InformacionGlobal', 2
UNION
SELECT 'MaquinaMes', 3
UNION
SELECT 'MaquinaIngresos', 4
UNION
SELECT 'MaquinasPozos', 5
UNION
SELECT 'TicketExpirados', 6
UNION
SELECT 'MesasIngresos', 7
UNION
SELECT 'MesasPozos', 8
UNION
SELECT 'MesasTorneos', 9
UNION
SELECT 'MesasTorneosOtrosPremios', 10
UNION
SELECT 'MesasTorneosPremios', 11
UNION
SELECT 'BingoDiario', 12
UNION
SELECT 'BingoPozos', 13
ORDER BY 2 ASC


-------------- AJQ ------------
DECLARE @D0		AS DATETIME
DECLARE @D1		AS DATETIME
DECLARE @TODAY  AS DATETIME

SET @D0 = '2000-01-01T12:00:00';
SET @D0 = DATEADD(YEAR,  @Year  - 2000, @D0)
SET @D0 = DATEADD(MONTH, @Month -    1, @D0)
SET @D0 = dbo.Opening(0, @D0)
SET @D1 = DATEADD(MONTH,             1, @D0)

SET @TODAY = dbo.TodayOpening(0)
SET @D1 = case when (@D1 > @TODAY) then @TODAY else @D1 end

 
 --EXEC FIXIT_SP_300_EGM_DAY       @D0, @D1
 --EXEC FIXIT_SP_300_EGM_HP        @D0, @D1
 --EXEC FIXIT_SP_300_EGM_COLLECTED @D0, @D1
 --EXEC FIXIT_SP_300_EGM_WIN       @D0, @D1

-- infoGlobalDiario

EXEC SP_SIOC_infoGlobalDiario @Month, @Year

-- InformacionGlobal
EXEC SP_SIOC_InformacionGlobal @Month, @Year

-- MaquinaMes
EXEC SP_SIOC_MaquinaMes @D0, @D1

-- MaquinaIngresos
EXEC SP_SIOC_MaquinaIngresos @D0, @D1
 
-- MaquinasPozos
EXEC SP_SIOC_MaquinasPozos @Month, @Year

-- TicketExpirados
EXEC SP_SIOC_TicketExpirados @Month, @Year

-- MesasIngresos
EXEC SP_SIOC_MesasIngresos @Month, @Year

-- MesasPozos
EXEC SP_SIOC_MesasPozos @Month, @Year

-- MesasTorneos
EXEC SP_SIOC_MesasTorneos

-- MesasTorneosOtrosPremios
EXEC SP_SIOC_MesasTorneosOtrosPremios

-- MesasTorneosPremios
EXEC SP_SIOC_MesasTorneosPremios

-- BingoDiario
EXEC SP_SIOC_BingoDiario @Month, @Year

-- BingoPozos
EXEC SP_SIOC_BingoPozos

END


GO


