USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_InformacionGlobal]    Script Date: 12-09-2017 7:21:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_SIOC_InformacionGlobal]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

---InformacionGlobal
Select '' as pagoProvisionalMensual,
	   MAX(DATEPART(DAY,dt_val)) AS diasOperacion,
	   @Month as mes,
	   @Year as a�o
from ( 
--Matriz cruzada de valores numericos 
select 
dateadd(dd, value, dateadd(month, @Month - 1, dateadd(year, @Year - 1900, '1900.01.01 00:00:00.000'))) as dt_val 
from( 
select (v2 * 4 + v1) * 4 + v0 as value from 
(select 0 as v0 union select 1 union select 2 union select 3) as rs0 cross join 
(select 0 as v1 union select 1 union select 2 union select 3) as rs1 cross join 
(select 0 as v2 union select 1 union select 2 union select 3) as rs2 
) as rs 
---fin de matriz 
) as rs2 
where month(dt_val) = @Month

END


GO


