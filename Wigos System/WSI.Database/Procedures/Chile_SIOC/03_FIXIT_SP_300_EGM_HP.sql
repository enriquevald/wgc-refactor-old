USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[FIXIT_SP_300_EGM_HP]    Script Date: 12-09-2017 7:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		ANDREU
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FIXIT_SP_300_EGM_HP] 
	@D0 DATETIME, 
	@D1 DATETIME
AS
BEGIN
	SET NOCOUNT ON;

DROP TABLE TMP_300_B

select * 
INTO TMP_300_B
from
(

select te_name ASSET, te_provider_id PROVIDER, XXX.*, HP.MeterHPC * 0.01 MeterHPC, HP.MeterJackpot * 0.01 MeterJackpot
, XXX.sum_hpc + XXX.sum_jkp + XXX.sum_prog - (HP.MeterHPC + HP.MeterJackpot) * 0.01 DiffReported 
,   XXX.paid_sum_hpc + XXX.paid_sum_jkp + XXX.paid_sum_prog 
  + XXX.paid_sum_ma_hpc + XXX.paid_sum_ma_jkp + XXX.paid_sum_ma_prog
  + XXX.paid_sum_ma_ma 
  - (HP.MeterHPC + HP.MeterJackpot) * 0.01 DiffPaid 
from
(
select ISNULL(REPORTED.TID, PAID.TID) TID, ISNULL(REPORTED.Reportado, PAID.Pagado) FechaHora 
, ISNULL(REPORTED.num_hpc    , 0) num_hpc
, ISNULL(REPORTED.num_jkp    , 0) num_jkp
, ISNULL(REPORTED.num_prog   , 0) num_prog
, ISNULL(REPORTED.num_ma_hpc , 0) num_ma_hpc
, ISNULL(REPORTED.num_ma_jkp , 0) num_ma_jkp
, ISNULL(REPORTED.num_ma_prog, 0) num_ma_prog
, ISNULL(REPORTED.num_ma_ma  , 0) num_ma_ma
, ISNULL(REPORTED.sum_hpc    , 0) sum_hpc
, ISNULL(REPORTED.sum_jkp    , 0) sum_jkp
, ISNULL(REPORTED.sum_prog   , 0) sum_prog
, ISNULL(REPORTED.sum_ma_hpc , 0) sum_ma_hpc
, ISNULL(REPORTED.sum_ma_jkp , 0) sum_ma_jkp
, ISNULL(REPORTED.sum_ma_prog, 0) sum_ma_prog
, ISNULL(REPORTED.sum_ma_ma  , 0) sum_ma_ma

, ISNULL(PAID.num_hpc     , 0) paid_num_hpc
, ISNULL(PAID.num_jkp     , 0) paid_num_jkp
, ISNULL(PAID.num_prog    , 0) paid_num_prog
, ISNULL(PAID.num_ma_hpc  , 0) paid_num_ma_hpc
, ISNULL(PAID.num_ma_jkp  , 0) paid_num_ma_jkp
, ISNULL(PAID.num_ma_prog , 0) paid_num_ma_prog
, ISNULL(PAID.num_ma_ma   , 0) paid_num_ma_ma
, ISNULL(PAID.sum_hpc     , 0) paid_sum_hpc
, ISNULL(PAID.sum_jkp     , 0) paid_sum_jkp
, ISNULL(PAID.sum_prog    , 0) paid_sum_prog
, ISNULL(PAID.sum_ma_hpc  , 0) paid_sum_ma_hpc
, ISNULL(PAID.sum_ma_jkp  , 0) paid_sum_ma_jkp
, ISNULL(PAID.sum_ma_prog , 0) paid_sum_ma_prog
, ISNULL(PAID.sum_ma_ma   , 0) paid_sum_ma_ma

from
(
select hp_terminal_id TID, dbo.Opening(0, hp_datetime) Reportado
, SUM(case when hp_type = 0                                            then 1 else 0 end) num_hpc
, SUM(case when hp_type = 1 and isnull(hp_level,0)     in (0, 0x40)    then 1 else 0 end) num_jkp
, SUM(case when hp_type = 1 and isnull(hp_level,0) not in (0, 0x40)    then 1 else 0 end) num_prog
, SUM(case when hp_type = 1000                                         then 1 else 0 end) num_ma_hpc
, SUM(case when hp_type = 1001 and isnull(hp_level,0)     in (0, 0x40) then 1 else 0 end) num_ma_jkp
, SUM(case when hp_type = 1001 and isnull(hp_level,0) not in (0, 0x40) then 1 else 0 end) num_ma_prog
, SUM(case when hp_type = 1010                                         then 1 else 0 end) num_ma_ma

, SUM(case when hp_type = 0                                            then hp_amount else 0 end) sum_hpc
, SUM(case when hp_type = 1 and isnull(hp_level,0)     in (0, 0x40)    then hp_amount else 0 end) sum_jkp
, SUM(case when hp_type = 1 and isnull(hp_level,0) not in (0, 0x40)    then hp_amount else 0 end) sum_prog
, SUM(case when hp_type = 1000                                         then hp_amount else 0 end) sum_ma_hpc
, SUM(case when hp_type = 1001 and isnull(hp_level,0)     in (0, 0x40) then hp_amount else 0 end) sum_ma_jkp
, SUM(case when hp_type = 1001 and isnull(hp_level,0) not in (0, 0x40) then hp_amount else 0 end) sum_ma_prog
, SUM(case when hp_type = 1010                                         then hp_amount else 0 end) sum_ma_ma

from handpays 
where hp_datetime >= @D0
 and  hp_datetime <  @D1
 group by hp_terminal_id, dbo.Opening(0, hp_datetime) 

) REPORTED
 full outer join
(
select hp_terminal_id TID, dbo.Opening(0, hp_datetime) Pagado ---- AJQ deberiamos reportar por la fecha de pago
, SUM(case when hp_type = 0                                            then 1 else 0 end) num_hpc
, SUM(case when hp_type = 1 and isnull(hp_level,0)     in (0, 0x40)    then 1 else 0 end) num_jkp
, SUM(case when hp_type = 1 and isnull(hp_level,0) not in (0, 0x40)    then 1 else 0 end) num_prog
, SUM(case when hp_type = 1000                                         then 1 else 0 end) num_ma_hpc
, SUM(case when hp_type = 1001 and isnull(hp_level,0)     in (0, 0x40) then 1 else 0 end) num_ma_jkp
, SUM(case when hp_type = 1001 and isnull(hp_level,0) not in (0, 0x40) then 1 else 0 end) num_ma_prog
, SUM(case when hp_type = 1010                                         then 1 else 0 end) num_ma_ma

, SUM(case when hp_type = 0                                            then hp_amount else 0 end) sum_hpc
, SUM(case when hp_type = 1 and isnull(hp_level,0)     in (0, 0x40)    then hp_amount else 0 end) sum_jkp
, SUM(case when hp_type = 1 and isnull(hp_level,0) not in (0, 0x40)    then hp_amount else 0 end) sum_prog
, SUM(case when hp_type = 1000                                         then hp_amount else 0 end) sum_ma_hpc
, SUM(case when hp_type = 1001 and isnull(hp_level,0)     in (0, 0x40) then hp_amount else 0 end) sum_ma_jkp
, SUM(case when hp_type = 1001 and isnull(hp_level,0) not in (0, 0x40) then hp_amount else 0 end) sum_ma_prog
, SUM(case when hp_type = 1010                                         then hp_amount else 0 end) sum_ma_ma

from handpays 
where hp_status & 0x8000 = 0x8000
 and  hp_datetime >= @D0   ---- AJQ deberiamos reportar por la fecha de pago 
 and  hp_datetime <  @D1
 group by hp_terminal_id, dbo.Opening(0, hp_datetime)
 
 ) PAID on REPORTED.TID = PAID.TID and REPORTED.Reportado = PAID.Pagado
 
 ) XXX
 inner join terminals on te_terminal_id = TID 
 inner join 
 (
   select   tsmh_terminal_id TID
          , tsmh_datetime    FechaHora
          , SUM(case when (tsmh_meter_code = 2) then tsmh_meter_increment else 0 end) MeterJackpot
          , SUM(case when (tsmh_meter_code = 3) then tsmh_meter_increment else 0 end) MeterHPC
   from terminal_sas_meters_history
   where tsmh_datetime >= @D0 
     and tsmh_datetime <  @D1
	 and tsmh_denomination = 0
	 and tsmh_game_id      = 0
	 and tsmh_meter_code in (2, 3)
	 and tsmh_type         = 20
	 group by tsmh_terminal_id, tsmh_datetime 
 ) HP on HP.TID = XXX.TID and HP.FechaHora = XXX.FechaHora 

 ) ZZZ
order by 1, 2, 3, 4




 



END

GO


