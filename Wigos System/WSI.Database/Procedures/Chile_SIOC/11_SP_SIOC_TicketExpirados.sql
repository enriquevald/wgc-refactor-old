USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_TicketExpirados]    Script Date: 12-09-2017 7:24:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_SIOC_TicketExpirados]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @d0 as datetime
declare @d1 as datetime
declare @dia as int

set @d0 = dbo.Opening(0,'2000-01-01T12:00:00')
set @d0 = DATEADD(YEAR, @Year - 2000, @d0)
set @d0 = DATEADD(MONTH, @Month - 1,  @d0)
set @d1 = DATEADD(MONTH, 1,  @d0)


SELECT   DATEPART(DAY,  dbo.Opening(0,ti_expiration_datetime)) AS dia_numero
        , COUNT(1)       numero
        , ROUND(SUM(TI_AMOUNT),0) valorAsociado
INTO ##TMP_EXPIRED
    FROM   TICKETS
WHERE   ti_status = 3
    AND   ti_expiration_datetime >= @d0
    AND   ti_expiration_datetime < @d1
	AND   ti_type_id = 0 -- Redeemable, no incluimos los Promocionales (RE, NR)
	and   ti_amount > 0  -- Evita incluir tickets īrarosīcon monto 0
GROUP BY    DATEPART(DAY,  dbo.Opening(0,ti_expiration_datetime)) 


WHILE (@d0 < @d1)
BEGIN

SET @dia =  DATEPART(DAY,  @d0)
IF NOT EXISTS (SELECT 1 FROM ##TMP_EXPIRED WHERE dia_numero = @dia)
BEGIN
	INSERT INTO ##TMP_EXPIRED (dia_numero, numero, valorAsociado) values (@dia, 0, 0)
END

set @d0 = DATEADD(DAY, 1,  @d0)

END

SELECT * FROM ##TMP_EXPIRED ORDER BY 1

DROP TABLE ##TMP_EXPIRED

return


--select 1, 0, 0

---- TicketExpirados
--SELECT   RS3.dia_numero
--       , SUM(RS3.numero) as numero
--       , SUM(RS3.valorAsociado) as valorAsociado
--  FROM   (
--           SELECT   DATEPART(DAY,dt_val) AS dia_numero
--                  , 0 AS numero
--                  , 0 AS valorAsociado
--             FROM   (
--                      --Matriz cruzada de valores numericos 
--                      SELECT   DATEADD(dd, value, DATEADD(MONTH, @Month - 1, DATEADD(YEAR, @Year - 1900, '1900.01.01 00:00:00.000'))) as dt_val 
--                        FROM   ( 
--                                 select (v2 * 4 + v1) * 4 + v0 as value from 
--                                (select 0 as v0 union select 1 union select 2 union select 3) as rs0 cross join 
--                                (select 0 as v1 union select 1 union select 2 union select 3) as RS1 cross join 
--                                (select 0 as v2 union select 1 union select 2 union select 3) as RS2 
--                               ) as rs 
--                       ---fin de matriz 
--                    ) as RS2 
--            WHERE   MONTH(dt_val) = @Month
--            UNION
--           SELECT   DATEPART(DAY,  dbo.Opening(0,ti_expiration_datetime)) AS dia_numero
--                  , COUNT(1)
--                  , SUM(TI_AMOUNT)
--             FROM   TICKETS
--            WHERE   ti_status = 3
--              AND   ti_expiration_datetime >= @d0
--              AND   ti_expiration_datetime < @d1
--         GROUP BY    DATEPART(DAY,  dbo.Opening(0,ti_expiration_datetime)) 
--         ) as RS3
--GROUP BY RS3.dia_numero


END



GO


