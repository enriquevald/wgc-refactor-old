USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_MaquinaIngresos]    Script Date: 12-09-2017 8:12:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_SIOC_MaquinaIngresos]
  (@D0 DATETIME,
   @D1 DATETIME) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @D0 >= '2016-08-01T08:00:00' 
	BEGIN
		SELECT   MAQUINA				codigo
			   , MAQUINA				codigoSCJ
			   , ME_BILL				efectivo
			   , ME_TI_RE + ME_TI_PRE	ticketInTarjetaIn
			   , ME_TO_RE + ME_HPCC     ticketOutTarjetaOut                 -- ME_TO_RE + ME_HPCC     

			   , case when (ME_JKP >= TOT_RPT_PRO) THEN (ME_JKP - TOT_RPT_PRO) ELSE 0 END   montoPagoManualPorSistemaJuegoBase  -- ME_JKP - TOT_PRO       
			   , TOT_RPT_PRO            montoPagoManualPorSistemaProgresivo
			   , 0                      montoPagoManualPorError             -- No hay pagos por error, se introducen en el Excel
	   
			   , 0                      variacionPozo

			   , N_RPT_JKP              nroPagoManualPorSistemaJuegoBase
			   , N_RPT_PRO              nroPagoManualPorSistemaProgresivo
			   , 0                      nroPagoManualPorError

			   , ''                     fueraOperacionMinutos
			   , ''                     fueraOperacionMotivo
			   , DIA                    dia 

		FROM TMP_300_H 
		ORDER BY FECHA, MAQUINA
	RETURN
	END


SELECT   MAQUINA				codigo
       , MAQUINA				codigoSCJ
	   , CO_BILL				efectivo
	   , ME_TI_RE + ME_TI_PRE	ticketInTarjetaIn
	   , ME_TO_RE + TOT_HPC     ticketOutTarjetaOut                 -- ME_TO_RE + ME_HPCC     

       , TOT_JKP                montoPagoManualPorSistemaJuegoBase  -- ME_JKP - TOT_PRO       
	   , TOT_PRO                montoPagoManualPorSistemaProgresivo
	   , TOT_ERR                montoPagoManualPorError
	   
	   , 0                      variacionPozo

       , N_JKP                  nroPagoManualPorSistemaJuegoBase
	   , N_PRO                  nroPagoManualPorSistemaProgresivo
	   , N_ERR                  nroPagoManualPorError

       , ''                     fueraOperacionMinutos
       , ''                     fueraOperacionMotivo
       , DIA                    dia 

FROM TMP_300_H 
ORDER BY FECHA, MAQUINA

END -- SP_SIOC_MaquinaIngresos


GO

