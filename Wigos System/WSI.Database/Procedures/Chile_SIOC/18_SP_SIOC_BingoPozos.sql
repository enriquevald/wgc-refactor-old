USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_BingoPozos]    Script Date: 12-09-2017 7:29:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_SIOC_BingoPozos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT '' AS pozoNumero,
		   '' AS pozoAporteCasino,
		   '' AS pozoAportedeOtroPozo,
		   '' AS pozoPremiosEntregados,
		   '' AS pozoAporteaOtroPozo
END


GO

