USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_MaquinasPozos]    Script Date: 12-09-2017 8:11:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SP_SIOC_MaquinasPozos]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- infoGlobalDiario
SELECT   SUM(1)  AS nroMaquinasOperando,
         '' AS pozoTotalInicial,
         '' AS pozoTotalFinal,
         pgs_progressive_id as pozo_numero,
         pgl_level_id AS nivel_numero,
         '' AS nivel_aporteJugadoresFinal,
         DATEPART(DAY,tc_date) AS dia_numero
  FROM   progressives
 INNER   JOIN progressives_levels ON pgs_progressive_id = pgl_progressive_id
 INNER   JOIN terminal_groups on pgs_progressive_id = tg_element_id
   AND   tg_element_type = 6
 INNER   JOIN terminals_connected on tg_terminal_id = tc_terminal_id
   AND   tc_date >= dateadd(month, @Month - 1, dateadd(year, @Year - 1900, '1900.01.01 00:00:00.000'))
   AND   tc_date  < dateadd(month, @Month, dateadd(year, @Year - 1900, '1900.01.01 00:00:00.000'))   
GROUP BY pgs_progressive_id, pgl_level_id, pgl_contribution_pct, tc_date
ORDER BY tc_date, pgs_progressive_id, pgl_level_id


END



GO

