USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_MaquinaMes]    Script Date: 12-09-2017 7:22:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_SIOC_MaquinaMes]
  (@D0 DATETIME,
   @D1 DATETIME) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	------------- AJQ ---------
SELECT   ''						    montoMinimoPagoManual
       , Maquina					maquinaPorMes_codigo
       , Maquina					maquinaPorMes_codigoSC
	   , SUM(ME_GAMES_PLAYED)		maquinaPorMes_nroJugadas
	   , SUM(ME_CI)		            maquinaPorMes_totalJugado
	   , 0         		            maquinaPorMes_tarjetaInPromocional
	   , SUM(ME_TI_PNR)		        maquinaPorMes_ticketInPromocional
	   , SUM(ME_TO_PNR)		        maquinaPorMes_ticketOutPromocional
	
	   , 0                          retornos_fechaInicio
	   , 0                          retornos_fechaFin
	   , @D0                        contadores_fechaInicio
	   , @D1                        contadores_fechaFin
	   
	   , CAST (MAX(ISNULL(te_theoretical_payout,0)) AS NUMERIC(5,2)) retornos_retorno_retornoJugadorTeorico

	   , 0							entradas_inicial
	   , SUM(ME_CI)					entradas_final
	   , 0							salidas_inicial
	   , SUM(ME_CO)+SUM(ME_JKP)     salidas_final
		
		FROM TMP_300_H inner join terminals on te_terminal_id = TID
        GROUP BY Maquina
 
 ORDER BY Maquina


-------- MaquinaMes
------DECLARE @D0  AS DATETIME
------DECLARE @D1  AS DATETIME
------DECLARE @TC0 AS DATETIME
------DECLARE @TC1 AS DATETIME
------DECLARE @HH  AS INT

------SET @D0 = '2000-01-01T12:00:00';
------SET @D0 = DATEADD(YEAR,  @Year  - 2000, @D0)
------SET @D0 = DATEADD(MONTH, @Month -    1, @D0)
------SET @D0 = dbo.Opening(0, @D0)
------SET @D1 = DATEADD(MONTH,             1, @D0)

------SET @TC0 = '2000-01-01T00:00:00';
------SET @TC0 = DATEADD(YEAR,  @Year  - 2000, @TC0)
------SET @TC0 = DATEADD(MONTH, @Month -    1, @TC0)
------SET @TC1 = DATEADD(MONTH,             1, @TC0)

------SET @HH  = DATEDIFF (HOUR, @TC0, @D0)

------IF object_id('tempdb..##TempTable_SasMetersHistory') is not null  DROP TABLE #TempTable_SasMetersHistory


------SELECT * INTO #TempTable_SasMetersHistory FROM(
------SELECT   '' AS montoMinimoPagoManual,
------		 tsmh_terminal_id as tsmh_terminal_id,
------		 SUM(CASE WHEN tsmh_meter_code =   5 THEN tsmh_meter_increment ELSE 0 END) AS maquinaPorMes_nroJugadas,
------         SUM(CASE WHEN tsmh_meter_code =   0 THEN tsmh_meter_increment ELSE 0 END) * 0.01 AS maquinaPorMes_totalJugado,
------         SUM(CASE WHEN tsmh_meter_code = 162 THEN tsmh_meter_increment ELSE 0 END) * 0.01 AS maquinaPorMes_tarjetaInPromocional,
------         SUM(CASE WHEN tsmh_meter_code = 130 THEN tsmh_meter_increment ELSE 0 END) * 0.01 AS maquinaPorMes_ticketInPromocional,
------         SUM(CASE WHEN tsmh_meter_code = 136 THEN tsmh_meter_increment ELSE 0 END) * 0.01 AS maquinaPorMes_ticketOutPromocional,
------         0 AS retornos_fechaInicio,
------         0 AS retornos_fechaFin,
------         @D0  AS contadores_fechaInicio,
------         @D1  AS contadores_fechaFin,
------         0 AS entradas_inicial,
------         SUM(CASE WHEN tsmh_meter_code = 0 THEN tsmh_meter_increment ELSE 0 END) * 0.01  AS entradas_final,
------         0 AS salidas_inicial,
------         SUM(CASE WHEN tsmh_meter_code in ( 1, 2 ) THEN tsmh_meter_increment ELSE 0 END) * 0.01 AS salidas_final
------  FROM    terminal_sas_meters_history
------ WHERE   tsmh_type = 20
------   AND   tsmh_datetime >= @D0
------   AND   tsmh_datetime <  @D1
------   AND   tsmh_meter_code in (0,1,2,5,130,136,162)
------GROUP BY tsmh_terminal_id
------)X

------SELECT
------	ISNULL(#TempTable_SasMetersHistory.montoMinimoPagoManual, '') as montoMinimoPagoManual,
------	te_name AS maquinaPorMes_codigo,
------    te_name AS maquinaPorMes_codigoSC,
------    ISNULL(#TempTable_SasMetersHistory.maquinaPorMes_nroJugadas, 0) as maquinaPorMes_nroJugadas,
------    ISNULL(#TempTable_SasMetersHistory.maquinaPorMes_totalJugado,0) as maquinaPorMes_totalJugado,
------    ISNULL(#TempTable_SasMetersHistory.maquinaPorMes_tarjetaInPromocional,0) as maquinaPorMes_tarjetaInPromocional,
------    ISNULL(#TempTable_SasMetersHistory.maquinaPorMes_ticketInPromocional,0) as maquinaPorMes_ticketInPromocional,
------    ISNULL(#TempTable_SasMetersHistory.maquinaPorMes_ticketOutPromocional,0) as maquinaPorMes_ticketOutPromocional,
------    ISNULL(#TempTable_SasMetersHistory.retornos_fechaInicio,0) as retornos_fechaInicio,
------    ISNULL(#TempTable_SasMetersHistory.retornos_fechaFin, 0 ) as retornos_fechaFin,
------    ISNULL(#TempTable_SasMetersHistory.contadores_fechaInicio , @D0) as contadores_fechaInicio,
------	ISNULL(#TempTable_SasMetersHistory.contadores_fechaFin, @D1) as contadores_fechaFin,
------	ISNULL(te_theoretical_payout,0) AS retornos_retorno_retornoJugadorTeorico,
------	ISNULL(#TempTable_SasMetersHistory.entradas_inicial,0) as entradas_inicial,
------    ISNULL(#TempTable_SasMetersHistory.entradas_final,0) as entradas_final,
------    ISNULL(#TempTable_SasMetersHistory.salidas_inicial,0) as salidas_inicial,
------    ISNULL(#TempTable_SasMetersHistory.salidas_final,0) as salidas_final
        
------FROM Terminals 
------LEFT JOIN #TempTable_SasMetersHistory on te_terminal_id = #TempTable_SasMetersHistory.tsmh_terminal_id
------WHERE te_terminal_type = 5
------ORDER by maquinaPorMes_codigo

------DROP TABLE #TempTable_SasMetersHistory

END -- SP_SIOC_MaquinaMes


GO


