USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[FIXIT_SP_300_EGM_COLLECTED]    Script Date: 12-09-2017 7:15:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		ANDREU
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FIXIT_SP_300_EGM_COLLECTED] 
	@D0 DATETIME, 
	@D1 DATETIME
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @D0 DATETIME
--DECLARE @D1 DATETIME
DECLARE @NDAYS INT

SET @NDAYS = 10

--SET @D0 = '2016-01-01T08:00:00'
--SET @D1 = '2016-02-01T08:00:00'

DROP TABLE TMP_300_D

SELECT * 
INTO TMP_300_D
FROM
(
SELECT   MC.MC_COLLECTION_ID, MC.MC_TERMINAL_ID, CA.CGS_WORKING_DAY
       , MC.MC_DATETIME, MC.MC_EXTRACTION_DATETIME, MC.MC_COLLECTION_DATETIME, DBO.Opening (0, DATEADD(HOUR, -DATEPART(HOUR, MC.MC_COLLECTION_DATETIME), MC.MC_COLLECTION_DATETIME)) WORKING_DAY_COL    
       , ISNULL(CAST(MC.MC_COLLECTED_BILL_AMOUNT AS BIGINT),0) COL_BILL
       , ISNULL(MCD.COL_BILL_X_DENOM,0)                        COL_BILL_X_DENOM
       , ISNULL(CAST(MC.MC_EXPECTED_BILL_AMOUNT  AS BIGINT),0) EXP_BILL
       , ISNULL(MCD.EXP_BILL_X_DENOM,0)                        EXP_BILL_X_DENOM
FROM MONEY_COLLECTIONS MC
FULL OUTER JOIN
(
SELECT MCD_COLLECTION_ID
     , CAST (SUM (CASE WHEN (MCD_FACE_VALUE > 0) THEN (MCD_FACE_VALUE * MCD_NUM_COLLECTED) ELSE 0 END) AS BIGINT) COL_BILL_X_DENOM
     , CAST (SUM (CASE WHEN (MCD_FACE_VALUE > 0) THEN (MCD_FACE_VALUE * MCD_NUM_EXPECTED ) ELSE 0 END) AS BIGINT) EXP_BILL_X_DENOM
FROM MONEY_COLLECTION_DETAILS 
GROUP BY MCD_COLLECTION_ID
) MCD
ON MC.MC_COLLECTION_ID = MCD.MCD_COLLECTION_ID 
FULL OUTER JOIN
(
	SELECT CGM_MC_COLLECTION_ID, CA.CGS_WORKING_DAY 
	FROM CAGE_MOVEMENTS 
	INNER JOIN CAGE_SESSIONS CA ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID AND CGM_TYPE = 104 
	       AND CGS_WORKING_DAY >= @D0  
		   AND CGS_WORKING_DAY <  @D1 
) CA ON CA.CGM_MC_COLLECTION_ID = MC.MC_COLLECTION_ID 
WHERE MC.MC_TERMINAL_ID > 0 AND MC.MC_COLLECTION_DATETIME IS NOT NULL 
  AND MC.MC_COLLECTION_DATETIME >= @D0 - @NDAYS 
  AND MC.MC_COLLECTION_DATETIME <  @D1 + @NDAYS 
  AND CGS_WORKING_DAY >= @D0  
  AND CGS_WORKING_DAY <  @D1 
) XXXX

DROP TABLE TMP_300_E

SELECT * 
INTO TMP_300_E
FROM
(
SELECT MC_TERMINAL_ID TID, CGS_WORKING_DAY WORKING_DAY, SUM(COL_BILL_X_DENOM) COL_BILL 
FROM TMP_300_D 
GROUP BY MC_TERMINAL_ID, CGS_WORKING_DAY
--SELECT MC_TERMINAL_ID TID, WORKING_DAY_COL  WORKING_DAY, SUM(COL_BILL_X_DENOM) COL_BILL 
--FROM TMP_300_D 
--GROUP BY MC_TERMINAL_ID, WORKING_DAY_COL  
) XXXX


DROP TABLE TMP_300_F

SELECT * 
INTO TMP_300_F
FROM
(
SELECT TSMH_TERMINAL_ID TID, TSMH_DATETIME WORKING_DAY, CAST(SUM(TSMH_METER_INCREMENT) * 0.01 AS BIGINT) METERED
FROM TERMINAL_SAS_METERS_HISTORY 
 WHERE TSMH_TYPE        = 20
  AND TSMH_GAME_ID      = 0
  AND TSMH_DENOMINATION = 0
  AND TSMH_DATETIME    >= @D0
  AND TSMH_DATETIME     < @D1
  AND TSMH_METER_CODE   = 11
GROUP BY TSMH_TERMINAL_ID, TSMH_DATETIME
) XXXX

DROP TABLE TMP_300_G

SELECT * 
INTO TMP_300_G
FROM
(
SELECT  ISNULL(E.TID, F.TID)                 TID
      , ISNULL(E.WORKING_DAY, F.WORKING_DAY) WORKING_DAY    
	  , ISNULL(E.COL_BILL, 0)                COLLECTED_BILLS 
	  , ISNULL(F.METERED,  0)                METERD_BILLS 
FROM TMP_300_E E FULL OUTER JOIN TMP_300_F F ON E.TID = F.TID AND E.WORKING_DAY = F.WORKING_DAY  
) 
XXXX


END














----DROP TABLE TMP_300_C

----select te_name ASSET, TE_PROVIDER_ID Manufacturer,  YYY.* 
----INTO TMP_300_C
----from
----(
----select 
----  ISNULL(METER.Dia, COLLECTION.Dia) Dia
----, ISNULL(METER.TID, COLLECTION.TID) TID
----, CAST(ISNULL(METER.BillIn, 0)          as bigint)  BillIn
----, CAST(ISNULL(COLLECTION.Collected, 0)  as bigint)  Collected
----, CAST(ISNULL(COLLECTION.Collected, 0) - ISNULL(METER.BillIn, 0)  as bigint) DiffCollection

----from
----(
----select tsmh_datetime Dia, tsmh_terminal_id TID, tsmh_meter_increment * 0.01 BillIn 
----  from terminal_sas_meters_history
---- where tsmh_datetime    >= @d0
----   and tsmh_datetime     < @d1
----   and tsmh_game_id      = 0
----   and tsmh_denomination = 0
----   and tsmh_type         = 20
----   and tsmh_meter_code   = 11
----) METER
----full outer join
----(
----select *
----from
----(

----select  Jornada Dia
----      , mc_terminal_id TID
----	  , sUM(1)                        Collections
----      , sum(mc_collected_bill_amount) Collected
----      --, sum(mc_expected_bill_amount ) Expected
----      --, sum(mc_collected_bill_amount) - sum(mc_expected_bill_amount ) Diff
	  
----	  --, min(mc_datetime) mi_ins
----	  --, max(mc_datetime) ma_ins
----	  --, min(mc_collection_datetime) mi_col
----	  --, max(mc_collection_datetime) ma_col

----from
----(
----select *, 
----   dbo.Opening(0, 
----   dateadd(second, -datepart(second, mc_collection_datetime)
---- , dateadd(minute, -datepart(minute, mc_collection_datetime)
---- , dateadd(hour,   -datepart(hour,   mc_collection_datetime), mc_collection_datetime)))) Jornada
---- from
----money_collections
----where mc_datetime >= @D0 - 45
----  and mc_datetime  < @D1 + 45
----  and mc_terminal_id >= 10000
----) XXXX 
----where Jornada >= @D0
---- and  Jornada <  @D1
---- group by Jornada, mc_terminal_id
 
----) ZZZZ
----) COLLECTION

----on METER.Dia = COLLECTION.Dia and METER.TID = COLLECTION.TID 
----) YYY
----inner join terminals on te_terminal_id = TID
----order by 3, 1, 2


----END


GO


