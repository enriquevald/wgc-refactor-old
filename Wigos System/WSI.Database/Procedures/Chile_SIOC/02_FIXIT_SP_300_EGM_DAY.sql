USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[FIXIT_SP_300_EGM_DAY]    Script Date: 12-09-2017 7:13:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		ANDREU
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FIXIT_SP_300_EGM_DAY] 
	-- Add the parameters for the stored procedure here
	@D0 DATETIME, 
	@D1 DATETIME
AS
BEGIN
	SET NOCOUNT ON;

DROP TABLE TMP_300_A

select *
INTO TMP_300_A
from
(
select   Fecha, TID, Dia, AssetNumber, Manufacturer
       
	   , M0 CoinIn 
	   , M1 CoinOut
	   , M2 Jackpot
	   , M0-(M1+M2) Hold

	   , ' '        SPC1 

	   , M5 GamesPlayed
	   , M6 GamesWon

	   , case when (M5 > 0) then ROUND((100.0 * M6 / M5),2) else null end GamesWonPct
	   , case when (M5 > 0) then ROUND((M0 / M5),2) else null end         BetAvg
	   , case when (M0 > 0) then ROUND((100.0 * (M0-(M1+M2))/M0), 2)     else null end HoldPct	   
	   , ' '        SPC2

	   , M11_BILLS BillIn
	   , M128      CashableTicketIn
	   , M130      RestrictedTicketIn
	   , M132      NonRestrictedTicketIn
	   , M134      CashableTicketOut
	   , M136      RestrictedTicketOut
	   , M2        Jackpots
	   , M3        HPCanceledCredits

	   , ' '        SPC3
	   , (M11_BILLS + M128 + M130 + M132)							TotalIn
	   , (M134 + M136 + M2 + M3)									TotalOut
	   , (M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3) TotalWin
	   
	   , ' '        SPC4
	   , (M11_BILLS + M128 + 0 + M132)								TotalReIn
	   , (M134 + 0 + M2 + M3)										TotalReOut
	   , (M11_BILLS + M128 + 0 + M132) - (M134 + 0 + M2 + M3)       TotalReWin

	   , ' '        SPC5
	   , case when ( (M0-(M1+M2)) - ((M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3)) <> 0 ) then 'X' else ' ' END X
	   , (M0-(M1+M2)) - ((M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3)) XX
from
(
   SELECT 
         MIN(TE_TERMINAL_ID)          TID 
	   , MIN(TSMH_DATETIME)           Fecha   
       , DATEPART(DAY, tsmh_datetime) Dia   
       , MIN(te_name)                 AssetNumber
	   , MIN(te_provider_id)          Manufacturer

	   , SUM (CASE WHEN tsmh_meter_code = 0 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M0
	   , SUM (CASE WHEN tsmh_meter_code = 1 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M1
	   , SUM (CASE WHEN tsmh_meter_code = 2 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M2
	   , SUM (CASE WHEN tsmh_meter_code = 3 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M3
	   , SUM (CASE WHEN tsmh_meter_code = 4 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M4

	   , SUM (CASE WHEN tsmh_meter_code = 5 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END )   M5
	   , SUM (CASE WHEN tsmh_meter_code = 6 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END )   M6

	   , SUM (CASE WHEN tsmh_meter_code = 11 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END )  * 0.01 M11_BILLS
	   , SUM (CASE WHEN tsmh_meter_code = 128 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M128
	   , SUM (CASE WHEN tsmh_meter_code = 130 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M130
	   , SUM (CASE WHEN tsmh_meter_code = 132 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M132

	   , SUM (CASE WHEN tsmh_meter_code = 134 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M134
	   , SUM (CASE WHEN tsmh_meter_code = 136 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M136

	   , SUM (CASE WHEN tsmh_meter_code = 36 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) * 0.01 M36

	   , sum(case when (tsmh_meter_code in (11,128,130,132)) then ISNULL(tsmh_meter_increment, 0)  else 0 end ) CommputedTotalIn
       , sum(case when (tsmh_meter_code in (2,3,134,136)) then ISNULL(tsmh_meter_increment, 0)  else 0 end ) CommputedTotalOut

	   ,
	   ( SUM (CASE WHEN tsmh_meter_code = 0 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   - SUM (CASE WHEN tsmh_meter_code = 1 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   - SUM (CASE WHEN tsmh_meter_code = 2 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   ) * 0.01 MNET0

	   ,
	   ( SUM (CASE WHEN tsmh_meter_code =  11 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   + SUM (CASE WHEN tsmh_meter_code = 128 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   + SUM (CASE WHEN tsmh_meter_code = 130 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   + SUM (CASE WHEN tsmh_meter_code = 132 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   - SUM (CASE WHEN tsmh_meter_code = 134 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   - SUM (CASE WHEN tsmh_meter_code = 136 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
	   - SUM (CASE WHEN tsmh_meter_code =   3 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
) * 0.01	   AS MNET1
	   
 FROM TERMINAL_SAS_METERS_HISTORY H 
INNER JOIN TERMINALS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID 
	   WHERE tsmh_datetime >= @D0 AND tsmh_datetime < @D1
	     and tsmh_type         = 20 
		 and tsmh_denomination = 0
		 and tsmh_game_id      = 0
GROUP BY TE_TERMINAL_ID, TE_PROVIDER_ID, TE_NAME, TSMH_DATETIME
) XXX 
) XXXX 
order by 4, 1




END

GO


