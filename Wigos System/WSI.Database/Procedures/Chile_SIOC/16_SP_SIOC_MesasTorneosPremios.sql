USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[SP_SIOC_MesasTorneosPremios]    Script Date: 12-09-2017 7:28:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_SIOC_MesasTorneosPremios]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT '' AS Efectivo_lugar,
		   '' AS Efectivo_monto,
		   '' AS Credito_lugar,
		   '' AS Credito_monto,
		   '' AS idTurno
END


GO

