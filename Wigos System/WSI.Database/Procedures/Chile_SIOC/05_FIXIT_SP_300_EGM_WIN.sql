USE [wgdb_000]
GO

/****** Object:  StoredProcedure [dbo].[FIXIT_SP_300_EGM_WIN]    Script Date: 12-09-2017 7:16:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		ANDREU
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[FIXIT_SP_300_EGM_WIN] 
	-- Add the parameters for the stored procedure here
	@D0 DATETIME, 
	@D1 DATETIME
AS
BEGIN
	SET NOCOUNT ON;

--EXEC FIXIT_SP_300_EGM_DAY       @D0, @D1
--EXEC FIXIT_SP_300_EGM_HP        @D0, @D1
--EXEC FIXIT_SP_300_EGM_COLLECTED @D0, @D1
--EXEC FIXIT_SP_300_EGM_WIN       @D0, @D1


DROP TABLE TMP_300_H

SELECT * 
INTO TMP_300_H
FROM
(
SELECT TID, Fecha, Dia, AssetNumber Maquina, Manufacturer Fabricante
, CAST (ME_CI     AS BIGINT) ME_CI
, CAST (ME_CO     AS BIGINT) ME_CO
, CAST (ME_JKP    AS BIGINT) ME_JKP
, CAST (HOLD      AS BIGINT) HOLD

, ' '  SPC10
, CAST (ME_GAMES_PLAYED AS BIGINT) ME_GAMES_PLAYED 
, CAST (ME_GAMES_WON    AS BIGINT) ME_GAMES_WON    
, ' '  SPC15

, CAST (CO_BILL   AS BIGINT) CO_BILL
, CAST (ME_BILL   AS BIGINT) ME_BILL
, CAST ((CO_BILL - ME_BILL) AS BIGINT) EXTRA_BILL

, ' '  SPC20

, CAST (ME_TI_RE    AS BIGINT) ME_TI_RE   
, CAST (ME_TI_PNR   AS BIGINT) ME_TI_PNR   
, CAST (ME_TI_PRE   AS BIGINT) ME_TI_PRE   
, CAST (ME_TO_RE    AS BIGINT) ME_TO_RE   
, CAST (ME_TO_PNR   AS BIGINT) ME_TO_PNR   
, CAST (ME_HPCC     AS BIGINT) ME_HPCC     

, CAST (TOT_WIN     AS BIGINT) TOT_WIN 

, ' '  SPC30

, CAST (NumRptJKP      AS BIGINT) N_RPT_JKP
, CAST (NumRptPRO      AS BIGINT) N_RPT_PRO
, CAST (SumRptJKP      AS BIGINT) TOT_RPT_JKP
, CAST (SumRptPRO      AS BIGINT) TOT_RPT_PRO
 

, ' '  SPC31

, CAST (NumHPC      AS BIGINT) N_HPC
, CAST (NumJKP      AS BIGINT) N_JKP
, CAST (NumPRO      AS BIGINT) N_PRO
, CAST (NumERR      AS BIGINT) N_ERR
 
, CAST (SumHPC            AS BIGINT) TOT_HPC
, CAST (SumJKP            AS BIGINT) TOT_JKP
, CAST (SumPRO            AS BIGINT) TOT_PRO
, CAST (SumERR            AS BIGINT) TOT_ERR

, ' '  SPC40
, CAST (SUM_HP_PAID  AS BIGINT)     SUM_HP_PAID  
, CAST (SUM_HP_METER AS BIGINT)     SUM_HP_METER 
, CAST ((SUM_HP_PAID - SUM_HP_METER) AS BIGINT)     EXT_HP_PAID 

from
(

SELECT A.TID, A.Fecha, A.Dia, A.AssetNumber, A.Manufacturer
, A.CoinIn					ME_CI
, A.CoinOut                 ME_CO
, A.Jackpots                ME_JKP
, A.Hold                    HOLD

, A.GamesPlayed             ME_GAMES_PLAYED 
, A.GamesWon                ME_GAMES_WON

, A.BillIn					      ME_BILL
, (ISNULL(G.Collected_BILLS, 0))  CO_BILL

, A.CashableTicketIn	    ME_TI_RE
, A.RestrictedTicketIn      ME_TI_PNR
, A.NonRestrictedTicketIn   ME_TI_PRE
, A.CashableTicketOut       ME_TO_RE
, A.RestrictedTicketOut     ME_TO_PNR
, A.HPCanceledCredits       ME_HPCC
, A.TotalWin                TOT_WIN


, ISNULL(B.num_jkp,  0) NumRptJKP
, ISNULL(B.num_prog, 0) NumRptPRO
, ISNULL(B.sum_jkp,  0) SumRptJKP
, ISNULL(B.sum_prog, 0) SumRptPRO




, (ISNULL(B.paid_num_hpc,  0)  + ISNULL(B.paid_num_ma_hpc,  0))  NumHPC
, (ISNULL(B.paid_sum_hpc,  0)  + ISNULL(B.paid_sum_ma_hpc,  0))  SumHPC
, (ISNULL(B.paid_num_jkp,  0)  + ISNULL(B.paid_num_ma_jkp,  0))  NumJKP
, (ISNULL(B.paid_num_prog, 0)  + ISNULL(B.paid_num_ma_prog, 0))  NumPRO
, (ISNULL(B.paid_num_ma_ma,0))                                   NumERR
, (ISNULL(B.paid_sum_jkp,  0)  + ISNULL(B.paid_sum_ma_jkp,  0))  SumJKP
, (ISNULL(B.paid_sum_prog, 0)  + ISNULL(B.paid_sum_ma_prog, 0))  SumPRO
, (ISNULL(B.paid_sum_ma_ma,0))                                   SumERR

, ISNULL(A.Jackpots,0) + ISNULL(A.HPCanceledCredits,0)           SUM_HP_METER

, (ISNULL(B.paid_sum_hpc,  0)  + ISNULL(B.paid_sum_ma_hpc,  0))  
+ (ISNULL(B.paid_sum_jkp,  0)  + ISNULL(B.paid_sum_ma_jkp,  0))  
+ (ISNULL(B.paid_sum_prog, 0)  + ISNULL(B.paid_sum_ma_prog, 0))  
+ (ISNULL(B.paid_sum_ma_ma,0))                                   SUM_HP_PAID

from TMP_300_A A
full outer join TMP_300_B B on A.TID = B.TID and A.Fecha = B.FechaHora
full outer join TMP_300_G G on A.TID = G.TID and A.Fecha = G.WORKING_DAY

)
REPORT
) XXX

order by Fecha, Maquina

END

GO


