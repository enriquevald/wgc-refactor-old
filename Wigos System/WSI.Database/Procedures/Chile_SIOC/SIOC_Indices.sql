--OJO, ESTE INDICE ELIMINA UNO EXISTENTE PARA VOLVER A CREARLO CON UN CAMPO INCLUIDO NUEVO  (ti_amount)-------

USE [wgdb_XXX]
GO
/****** Object:  Index [IX_ti_status_expiration_datetime]    Script Date: 02/11/2015 10:54:11 ******/
DROP INDEX [IX_ti_status_expiration_datetime] ON [dbo].[tickets] WITH ( ONLINE = OFF )
GO
USE [wgdb_XXX]
GO
CREATE NONCLUSTERED INDEX [IX_ti_status_expiration_datetime] ON [dbo].[tickets] 
(
	[ti_status] ASC,
	[ti_expiration_datetime] ASC
)
INCLUDE ( [ti_amount]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
