﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_RestartAllTerminals]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_RestartAllTerminals]
GO

CREATE PROCEDURE [dbo].[SP_RestartAllTerminals] 
AS
BEGIN

  DECLARE @pTerminalId BIGINT  
  DECLARE CursorActiveTerminals CURSOR FOR SELECT TE_TERMINAL_ID 
                                             FROM TERMINALS 
                                            WHERE TE_TERMINAL_TYPE IN ( 1, 5, 101, 102, 105, 106, 108, 109, 110)  
                                              AND TE_STATUS = 0 

  OPEN CursorActiveTerminals
	  FETCH NEXT FROM CursorActiveTerminals INTO @pTerminalId

	  WHILE @@FETCH_STATUS = 0
	  BEGIN
		  EXEC SP_RestartTerminal @pTerminalId
  		  
		  FETCH NEXT FROM CursorActiveTerminals INTO @pTerminalId
	  END -- WHILE
  	
  CLOSE CursorActiveTerminals
  DEALLOCATE CursorActiveTerminals

END


GRANT EXECUTE ON [dbo].[SP_RestartAllTerminals] TO [wggui] WITH GRANT OPTION

GO