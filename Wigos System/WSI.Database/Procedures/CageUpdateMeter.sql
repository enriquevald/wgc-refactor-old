 --------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CageUpdateMeter.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: Sergi Mart�nez
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-SET-2014 SMN    First release.
-- 17-JUN-2015 RCI    Fixed Bug WIG-2443: Create METERS before update if they don't exist
-- 22-ABR-2016 JML    Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
-- ----------- ------ ----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateMeter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageUpdateMeter]
GO

/****** Object:  StoredProcedure [dbo].[CageUpdateMeter]    Script Date: 09/30/2014 17:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CageUpdateMeter]
	   	@pValueIn MONEY
    , @pValueOut MONEY
	  ,	@pSourceTagetId BIGINT
	  , @pConceptId BIGINT
	  , @pIsoCode VARCHAR(3)
	  , @pCageCurrencyType INT
	  , @pOperation INT
	  , @pSessionId BIGINT = NULL
	  , @pSessionGetMode INT
      
  AS
BEGIN 

	-- @pOperationId  =	0 - Set
	--				  =	1 - Update
	
	-- , @pCageSessionGetMode = 0 - Get cage session from parameters
	--						  = 1 - Get cage_sessions


	DECLARE @CageSessionId BIGINT
	DECLARE @GetFromMode INT
	DECLARE @out_CageMetersValue TABLE(CM_VALUE DECIMAL)
	DECLARE @out_CageSessionMetersValue TABLE (CSM_VALUE DECIMAL)

	-- Get Cage Session
	
	IF @pSessionGetMode = 0
		SET @CageSessionId = @pSessionId

	ELSE IF @pSessionGetMode = 1
	BEGIN
	
		-------------- TODO : GET FROM GP!! -------------- 
		--SELECT @GetFromMode = GP_KEY_VALUE FROM GENERAL_PARAMS 
		-- WHERE GP_GROUP_KEY = 'RegionalOptions' 
		--   AND GP_SUBJECT_KEY = 'CurrencyISOCode'
		SET @GetFromMode = 1
     
		IF @GetFromMode = 1
			SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
									FROM CAGE_SESSIONS
								ORDER BY cgs_cage_session_id DESC)
		ELSE
			SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
									FROM CAGE_SESSIONS
								ORDER BY cgs_cage_session_id ASC)
	END				   

  -- Create Global and CageSession Meters. It checks if they don't exist and create them if necessary
  -- RCI 17-JUN-2015: Fixed Bug WIG-2443: Create them before update
	EXEC CageCreateMeters @pSourceTagetId
	                    , @pConceptId
	                    , @CageSessionId  -- @pCageSessionId
	                    , 0     -- Create in cage_meters and cage_session_meters
	                    , NULL  --@CurrencyForced

	-- CAGE METERS --

	UPDATE CAGE_METERS SET                                                                  
 	       CM_VALUE_IN = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_IN, 0) + @pValueIn    
                              ELSE CM_VALUE_IN END
         , CM_VALUE_OUT = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_OUT, 0) + @pValueOut  
                               ELSE CM_VALUE_OUT END                                                                       
         , CM_VALUE = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE, 0) + @pValueIn - @pValueOut            
                           WHEN @pOperation = 0 THEN @pValueIn - @pValueOut                                
                           ELSE CM_VALUE END 
    OUTPUT  INSERTED.CM_VALUE INTO @out_CageMetersValue
 	 WHERE CM_SOURCE_TARGET_ID = @pSourceTagetId
 	   AND CM_CONCEPT_ID = @pConceptId                                                     
 	   AND CM_ISO_CODE = @pIsoCode                                                         
 	   AND CM_CAGE_CURRENCY_TYPE = @pCageCurrencyType                                                         

 	   
	-- CAGE SESSION METERS --

	IF @CageSessionId IS NOT NULL
	BEGIN
	 	UPDATE CAGE_SESSION_METERS SET                                                          
	 	       CSM_VALUE_IN = ISNULL(CSM_VALUE_IN, 0) + @pValueIn 
	 	     , CSM_VALUE_OUT = ISNULL(CSM_VALUE_OUT, 0) + @pValueOut                                  
	         , CSM_VALUE = ISNULL(CSM_VALUE, 0) + @pValueIn - @pValueOut                                      
	 	OUTPUT INSERTED.CSM_VALUE INTO @out_CageSessionMetersValue                                                            
	 	 WHERE CSM_SOURCE_TARGET_ID = @pSourceTagetId
	 	   AND CSM_CONCEPT_ID = @pConceptId                                                                                              
	 	   AND CSM_ISO_CODE = @pIsoCode                                                        
       AND CSM_CAGE_CURRENCY_TYPE = @pCageCurrencyType                                                         
	 	   AND CSM_CAGE_SESSION_ID = @CageSessionId 
	END                                                        
 
	SELECT  ISNULL((SELECT TOP 1 CM_VALUE FROM @out_CageMetersValue),0) as CM_VALUE
		  , ISNULL((SELECT TOP 1 CSM_VALUE FROM @out_CageSessionMetersValue),0) as CSM_VALUE
		  
 END  -- CageUpdateMeter
                                                                                       
 GO
 GRANT EXECUTE ON [dbo].[CageUpdateMeter] TO [wggui] WITH GRANT OPTION 
 GO
