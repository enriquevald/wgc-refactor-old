--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: GamingTables.sql
--
--   DESCRIPTION: Gaming Tables reports procedures and functions
--
--        AUTHOR: Ramon Moncl�s
--
-- CREATION DATE: 24-DEC-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 24-DEC-2013 RMS    First release.
-- 07-JAN-2014 RMS    Corrected order when groupped.
-- 09-JAN-2014 RMS    Added Stored procedure for chip operations reports
-- 31-JAN-2014 RMS    Added Stored procedure for gaming table session information
-- 03-MAR-2014 DLL    Fixed Bug WIG-685: procedure GT_Chips_Operations filter by movement, no by session
-- 07-MAR-2014 DLL    Fixed Bug WIG-704: procedure GT_Cancellations show terminal name not gaming table name
-- 10-APR-2014 DLL    Fixed Bug WIG-816: procedure GT_Session_Information incorrect cash amount when movement filter
-- 16-MAY-2014 DLL    Fixed Bug WIG-924: Delete ApplyExchange function. Values save in CM_AUX_AMOUNT
-- 16-JUN-2014 DLL    Fixed Bug WIG-1033: Income control report always 0 when filter per day
-- 27-OCT-2014 DLL    New calculation of drop
-- 03-DEC-2014 JMV    GT_Cancellations: added new parameter (WIG-1801). Optimization.
-- 25-AUG-2016 FGB    GT_Session_Information: Optimization.
-- 07-SEP-2016 FGB    GT_Session_Information: Optimization.
-- 12-SEP-2016 JML    Bug 17344: Gaming tables: Number of visits is not reported correctly when close the table.
-- 14-DIC-2016 ETP    Bug 21115: Corrected win formula.
-- 08-AGU-2016 DHA    Bug 29243:WIGOS-4259 [Ticket #7660] Income Control Report - Duplicated tips - Version 03.05.0037
-- 05-JUN-2018 AGS		Bug 32889:WIGOS-12142, WIGOS-12143, WIGOS-12146, WIGOS-12148 - Drop mesas
-- 19-JUN-2018 FJC    Bug: WIGOS-13040 When pressing Reset button on Cash movements coming from Chip purchase/sale report, two points appear
--------------------------------------------------------------------------------
-- USE [wgdb_000]
-- GO

-- SET ANSI_NULLS ON
-- GO

-- SET QUOTED_IDENTIFIER ON
-- GO

----------------------------------------------------------------------------------------------------------------
-------------------------- FUNCTIONS
----------------------------------------------------------------------------------------------------------------

-- Clean older functions if exists

IF OBJECT_ID (N'dbo.SplitStringIntoTable', N'TF') IS NOT NULL
    DROP FUNCTION DBO.SplitStringIntoTable;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP_GAMBLING_TABLES', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP_GAMBLING_TABLES;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP_CASHIER', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP_CASHIER;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_WIN]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION dbo.GT_Calculate_WIN;
GO

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

--IF OBJECT_ID (N'dbo.GT_Base_Report_Data_Player_Tracking', N'P') IS NOT NULL
--    DROP PROCEDURE dbo.GT_Base_Report_Data_Player_Tracking;
--GO

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;
GO

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;
GO

IF OBJECT_ID (N'dbo.GT_Cancellations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Cancellations;
GO

IF OBJECT_ID (N'dbo.GT_Get_Drop', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Get_Drop;
GO



-- Create functions

    -- PURPOSE: Apply exchange to national currency
    --
    -- PARAMS:
    --     - INPUT:
    --           - @pAmount
    --           - @pIsoCode
    --
    -- RETURNS:
    --     - DECIMAL(16,8): Operation result
    --

CREATE FUNCTION ApplyExchange
(
  @pAmount        MONEY,
  @pIsoCode       VARCHAR(3),
  @pMovementDate  DATETIME
)
RETURNS MONEY
AS
BEGIN
      DECLARE @Exchanged      MONEY
      DECLARE @Change         MONEY
      DECLARE @NumDecimals    INT

      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
          SELECT TOP 1
                  @Change = CEA_NEW_CHANGE,
                  @NumDecimals = CEA_NEW_NUM_DECIMALS
            FROM  CURRENCY_EXCHANGE_AUDIT WITH (INDEX(IX_CEA_ISO_CODE_TYPE_DATETIME))
           WHERE  CEA_TYPE = 0
             AND  CEA_CURRENCY_ISO_CODE = @pIsoCode
             AND  CEA_DATETIME <= @pMovementDate
           ORDER  BY CEA_DATETIME DESC

            SET @NumDecimals = ISNULL(@NumDecimals, 0)
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)
      END

      RETURN @Exchanged
END -- ApplyExchange
GO

GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO

/*
   PURPOSE: SPLITS STRING INTO TABLE

   PARAMS: @pString: String list of items separate by same delimiter. Ex: '1,2,3,4,5,6'
           @pDelimiter: Separator char on the string list. Ex: ','
           @pTrimItems: Default 1 - remove left and right spaces on each item

   RETURN:  TABLE with the selected values in row format: SST_ID, SST_VALUE

   NOTES: Select in must take the SST_VALUE to compare the item.
          Ex: WHERE FIELD_NAME IN ( SELECT SST_VALUE FROM dbo.SplitStringIntoTable(@ListItemsString,@Separator, DEFAULT) )
*/
CREATE FUNCTION SplitStringIntoTable
(
    @pString VARCHAR(4096),
    @pDelimiter VARCHAR(10),
    @pTrimItems BIT = 1
)
RETURNS
@ReturnTable TABLE
(
    [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
    [SST_VALUE] [VARCHAR](50) NULL
)
AS
BEGIN
        DECLARE @_ISPACES INT
        DECLARE @_PART VARCHAR(50)

        -- INITIALIZE SPACES
        SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)

        WHILE @_ISPACES > 0
        BEGIN
            SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))

            IF @pTrimItems = 1
             SET @_PART = LTRIM(RTRIM(@_PART))

            INSERT INTO @ReturnTable(SST_VALUE)
            SELECT @_PART

            SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))

            SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        END

        IF LEN(@pString) > 0
            INSERT INTO @ReturnTable
            SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END

    RETURN
END
GO

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type

   PARAMS:  @pOwnSalesAmount: Chips sales
            @pExternalSalesAmount: Chips sales from other cashiers

   RETURN:  Money: Drop
*/
CREATE FUNCTION GT_Calculate_DROP
(
    @pOwnSalesAmount        AS MONEY,
    @pExternalSalesAmount   AS MONEY,
    @pCollectedAmount       AS MONEY,
    @pIsIntegratedCashier   AS BIT
)
RETURNS MONEY
BEGIN
  DECLARE @drop as Money
    IF @pIsIntegratedCashier = 1
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount
    ELSE
     SET @drop = @pOwnSalesAmount + @pExternalSalesAmount + @pCollectedAmount

    IF @drop < 0
      SET @drop = 0

    RETURN @drop
END
GO

-- PERMISSIONS  GT_Calculate_DROP
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP] TO [wggui] WITH GRANT OPTION
GO

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type

   PARAMS:  @pCollectedAmount : total cash
			      @pCollectedAmountDropBox : total cash on dropbox
            @pCollectedChipsAmount: Chips on dropbox
            @pCollectedTicketsAmount: tickets on dropbox


   RETURN:  Money: Drop
*/
CREATE FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] (@pCollectedAmount AS MONEY, @pCollectedAmountDropBox AS MONEY, @pCollectedChipsAmountDropBox as MONEY, @pCollectedTicketsAmountDropBox as MONEY, @pIsIntegratedCashier AS BIT) RETURNS MONEY
  BEGIN
	DECLARE @drop as Money
    
    SET @drop = 0  
    
    IF @pIsIntegratedCashier = 1
    BEGIN
		  RETURN @drop
	  END
	        
    SET @drop = @pCollectedAmount + @pCollectedAmountDropBox + @pCollectedChipsAmountDropBox + @pCollectedTicketsAmountDropBox
    
    IF @drop < 0
      set @drop = 0

   RETURN @drop
  END
GO

-- PERMISSIONS  GT_Calculate_DROP_GAMBLING_TABLES
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] TO [wggui] WITH GRANT OPTION
GO

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type

   PARAMS:  @pOwnSalesAmount: Chips sales
            @pExternalSalesAmount: Chips sales from other cashiers

   RETURN:  Money: Drop
*/
CREATE FUNCTION [dbo].[GT_Calculate_DROP_CASHIER] ( @pOwnSalesAmount AS MONEY, @pExternalSalesAmount as MONEY) RETURNS MONEY
  BEGIN
  DECLARE @drop as Money
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount

   IF @drop < 0
     set @drop = 0

   RETURN @drop
  END
GO

-- PERMISSIONS  GT_Calculate_DROP_CASHIER
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP_CASHIER] TO [wggui] WITH GRANT OPTION
GO


/*
   PURPOSE: Calculates the WIN field in a gaming table or table type

   PARAMS:  @pChipsFinalAmount: Closing chips amounts, sended from CAGE
			      @pChipsInitialAmount: Opening chips amounts, sended from CAGE
			      @pChipsFillAmount: Chips sended from CAGE, excluding opening
			      @pChipsCreditAmount: Chips sended to CAGE, excluding closing
			      @pTipsAmount: Tips
			      @pOwnSales: Chips own sales
			      @pExternalSales: Chips external sales
			      @pCollectedAmount: Money collected
			      @pCollectedAmountDropBox : total cash on dropbox
            @pCollectedChipsAmount: Chips on dropbox
            @pCollectedTicketsAmount: tickets on dropbox

   RETURN:  Money: Win
*/
CREATE FUNCTION [dbo].[GT_Calculate_WIN] ( @pChipsFinalAmount AS MONEY, @pChipsInitialAmount AS MONEY, @pChipsFillAmount AS MONEY, @pChipsCreditAmount AS MONEY, @pTipsAmount AS MONEY , @pOwnSales AS MONEY, @pExternalSales AS MONEY, @pCollectedAmount AS MONEY, @pCollectedAmountDropBox AS MONEY, @pCollectedChipsAmountDropBox as MONEY, @pCollectedTicketsAmountDropBox as MONEY, @pIsIntegratedCashier AS BIT) RETURNS MONEY
  BEGIN

  DECLARE @win as Money
  
    SET @win = (@pChipsFinalAmount - @pChipsInitialAmount) + (@pChipsCreditAmount - @pChipsFillAmount) - @pTipsAmount
    
    -- Add Drop CashDesk    
    SET @win = @win + @pOwnSales + @pExternalSales
    
    IF @pIsIntegratedCashier = 1
    BEGIN
		  RETURN @win
	  END
    
    -- Add Drop CollectedAmount + DropBox
    SET @win = @win + @pCollectedAmount + @pCollectedAmountDropBox + @pCollectedChipsAmountDropBox + @pCollectedTicketsAmountDropBox
    
    RETURN @win
    
  END
GO


----------------------------------------------------------------------------------------------------------------
-------------------------- STORED PROCEDURE
----------------------------------------------------------------------------------------------------------------

/*----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.1    24-DEC-2013    RMS      Using GAMING_TABLE_SESSION
1.0.2    20-FEB-2014    RMS      Only get data of closed gaming table sessions
1.0.3    26-FEB-2015    ANM      Add sum in SESSION_HOURS to get average time later in code
1.0.4    06-JUN-2016    FOS      Calculate drop in gamblingtables and cashier
1.0.5    20-JUL-2016    RAB      Add MultiCurrency to report
1.0.6    05-APR-2017    JML      Add validated tickets 
1.0.7    23-FEB-2018    RGR      Bug 31477:WIGOS-8152 [Ticket #12340] Fallo: Reporte Control de Ingresos Agrupado por Fecha Version V03.06.0035

Requeriments:
   -- Functions:
         dbo.GT_Calculate_DROP( Amount )
         dbo.GT_Calculate_WIN ( AmountAdded, AmountSubtracted, CollectedAmount, TipsAmount )

Parameters:
     -- BASE TYPE:                  Indicates if data is based on table types (0) or the tables (1).
     -- TIME INTERVAL:              Range of time to group data between days (0), months (1) and years (2).
     -- DATE FROM:                  Start date for data collection. (If NULL then use first available date).
     -- DATE TO:                    End date for data collection. (If NULL then use current date).
     -- ONLY ACTIVITY:              If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
     -- ORDER BY:                   0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.
     -- VALID TYPES:                A string that contains the valid table types to list.
     -- SELECTED CURRENCY:          Selected currency in a frm_gaming_tables_income_report
     -- TOTAL TO SELECTED CURRENCY: 0 --> Option: Total to local iso code
                                    1 --> Option: By selected currency

Process: (From inside to outside)

   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.

   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.

   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.

 Results:

   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE SELECTED CATEGORIES
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

INSERT INTO @_SELECTED_CURRENCY SELECT * FROM dbo.SplitStringIntoTable(@pSelectedCurrency, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           CAST(@_DATE_TO AS DATETIME)
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(DISTINCT X.GTS_GAMING_TABLE_SESSION_ID) AS SESSION_SUM         
    INTO   #GT_TEMPORARY_REPORT_DATA
   FROM (
           -- CORE QUERY
         SELECT DISTINCT GTS_GAMING_TABLE_SESSION_ID,
				CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0
                            THEN ISNULL(GTS_COPY_DEALER_VALIDATED_AMOUNT, 0)
                            WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                            THEN SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))
                            END                                                           AS COPY_DEALER_VALIDATED_AMOUNT
                FROM   GAMING_TABLES_SESSIONS GTS
           LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                  ON   GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                 AND   GTSC_ISO_CODE IN ( SELECT SST_VALUE FROM @_SELECTED_CURRENCY) 
                 AND   GTSC_TYPE <> @_CHIP_COLOR
          INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                 AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
          INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                 AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
          INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1 -- Only closed sessions
            GROUP BY   GTS_GAMING_TABLE_SESSION_ID, CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                     , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                     , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                     , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                     , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                     , GTS_COPY_DEALER_VALIDATED_AMOUNT
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION
   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

--

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CHIPS OPERATIONS

Version   Date            User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0     09-JAN-2014     RMS       First Release
1.0.1     22-OCT-2015     CPC       New column DELTA
1.0.2     29-APR-2016     RAB       Add chips RE, NR, Color to operations
1.0.3     05-JUL-2016     RAB       Add CM_CURRENCY_ISO_CODE in group by
          24-NOV-2016     JML       Fixed Bug 20556: Duplicate (triplicate,...) amount

Requeriments:
   -- Functions:

 --Parameters:
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- STATUS:         The gaming table status tu filter (enabled or disabled) (-1 to show all).
   -- AREA:           Location Area of the gaming table.
   -- BANK:           Area bank of location.
   -- FLOOR:          Floor of location.
   -- CASHIERS:       Indicates if cashiers without gaming table must be included
   -- CASHIER GROUP NAME: Sets the name of the gaming table type column for cashiers.
   -- VALID TYPES:    A string that contains the valid table types to list.
   -- SELECTED CURRENCY: Selected currency in a form. Only to Option 2 in a form.
   -- SELECTED OPTION FORM: 0 --> Option: Total to local iso code
   --                       1 --> Option: By selected currency
  */

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Chips_Operations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GT_Chips_Operations]
GO


CREATE PROCEDURE [dbo].[GT_Chips_Operations]
(
    @pDateFrom              DATETIME
  , @pDateTo                DATETIME
  , @pStatus                INTEGER
  , @pArea                  NVARCHAR(50)
  , @pBank                  NVARCHAR(50)
  , @pOnlyTables            INTEGER
  , @pCashierGroupName      NVARCHAR(50)
  , @pValidTypes            NVARCHAR(50)
  , @pSelectedCurrency      NVARCHAR(3)
  , @pSelectedOptionForm    INTEGER
)
AS
BEGIN
  ----------------------------------------------------------------------------------------------------------------
  DECLARE @_DATE_FROM                  AS DATETIME
  DECLARE @_DATE_TO                    AS DATETIME
  DECLARE @_STATUS                     AS INTEGER
  DECLARE @_AREA                       AS NVARCHAR(50)
  DECLARE @_BANK                       AS NVARCHAR(50)
  DECLARE @_DELIMITER                  AS CHAR(1)
  DECLARE @_ONLY_TABLES                AS INTEGER
  DECLARE @_CASHIERS_NAME              AS NVARCHAR(50)
  DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE NVARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
  DECLARE @_TYPE_CHIPS_SALE_TOTAL      AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL  AS INTEGER
  DECLARE @_CHIP_RE                    AS INTEGER
  DECLARE @_CHIP_NR                    AS INTEGER
  DECLARE @_SELECTED_CURRENCY          AS NVARCHAR(3)
  DECLARE @_SELECTED_OPTION_FORM       AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE            AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE        AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_REMAINING_AMOUNT          AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT AS INTEGER
  ----------------------------------------------------------------------------------------------------------------

  -- Initialization --
  SET @_TYPE_CHIPS_SALE_TOTAL              = 303
  SET @_TYPE_CHIPS_PURCHASE_TOTAL          = 304
  SET @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE     = 309
  SET @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE = 310
  SET @_TYPE_CHIPS_SALE_REMAINING_AMOUNT          = 312
  SET @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT = 313

  SET @_DELIMITER            =   ','
  SET @_DATE_FROM            =   @pDateFrom
  SET @_DATE_TO              =   @pDateTo
  SET @_STATUS               =   ISNULL(@pStatus, -1)
  SET @_AREA                 =   ISNULL(@pArea, '')
  SET @_BANK                 =   ISNULL(@pBank, '')
  SET @_ONLY_TABLES          =   ISNULL(@pOnlyTables, 1)
  SET @_CHIP_RE              =   1001
  SET @_CHIP_NR              =   1002
  SET @_SELECTED_CURRENCY    = @pSelectedCurrency
  SET @_SELECTED_OPTION_FORM = @pSelectedOptionForm

  IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
    SET @_CASHIERS_NAME  = '---CASHIER---'
  END

  ----------------------------------------------------------------------------------------------------------------
  -- CHECK DATE PARAMETERS
  IF @_DATE_FROM IS NULL
  BEGIN
     -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
     SET @_DATE_FROM = CAST('' AS DATETIME)
  END

  IF @_DATE_TO IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
  END

  -- ASSIGN TYPES PARAMETER INTO TABLE
  INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

   ----------------------------------------------------------------------------------------------------------------
  -- MAIN QUERY
  -- Total to local isocode
  IF @_SELECTED_OPTION_FORM = 0
  BEGIN
     SELECT   0 AS TYPE_SESSION
            , CS_SESSION_ID AS SESSION_ID			
            , GT_NAME AS GT_NAME
            , CS_OPENING_DATE AS SESSION_DATE
            , GTT_NAME AS GTT_NAME
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
            , (SELECT ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0)
               FROM cashier_movements WHERE cm_session_id = cs_session_id ) AS REMINING
			, ''					 AS CURRENCY_ISO_CODE
			, MAX(CS_NAME) AS SESSION_NAME
			, CT_NAME AS CT_NAME
       INTO   #CHIPS_OPERATIONS_TABLE_OPTION_1
       FROM   CASHIER_SESSIONS
	    INNER   JOIN CASHIER_TERMINALS ON (cs_cashier_id = ct_cashier_id)
			 LEFT   JOIN GAMING_TABLES_SESSIONS ON (GTS_CASHIER_SESSION_ID = CS_SESSION_ID)
      INNER   JOIN GAMING_TABLES GT ON (GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID)
      INNER   JOIN GAMING_TABLES_TYPES ON (GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID)
      WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
        AND   GT_AREA_ID   = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
        AND   GT_BANK_ID   = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
        AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
        AND   CS_OPENING_DATE >= @_DATE_FROM
        AND   CS_OPENING_DATE <  @_DATE_TO
        AND   GT_HAS_INTEGRATED_CASHIER = 1
        AND   (ISNULL(GTS_TOTAL_SALES_AMOUNT, 0) + ISNULL(GTS_TOTAL_PURCHASE_AMOUNT, 0)) <> 0
    GROUP BY CT_NAME, GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
    ORDER BY GTT_NAME
		
    -- Check if cashiers must be visible
    IF @_ONLY_TABLES = 0
    BEGIN
      -- Select and join data to show cashiers
      -- Adding cashiers without gaming tables
      INSERT INTO   #CHIPS_OPERATIONS_TABLE_OPTION_1
           SELECT   1 AS TYPE_SESSION
                  , CM_SESSION_ID AS SESSION_ID				  
                  , CT_NAME as GT_NAME
                  , CS_OPENING_DATE AS SESSION_DATE
                  , @_CASHIERS_NAME as GTT_NAME
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    ELSE 0 END), 0) AS GTS_TOTAL_SALES_AMOUNT
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    ELSE 0 END), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    ELSE 0 END ), 0) 
                  - ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    ELSE 0 END ), 0) AS DELTA
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0) AS REMINING
				  , '' AS CURRENCY_ISO_CODE
				  , MAX(CS_NAME) AS SESSION_NAME
				  , CT_NAME as CT_NAME
     FROM   CASHIER_MOVEMENTS
	  INNER   JOIN CASHIER_SESSIONS        ON CS_SESSION_ID          = CM_SESSION_ID
    INNER   JOIN CASHIER_TERMINALS       ON CM_CASHIER_ID          = CT_CASHIER_ID
     LEFT   JOIN  GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
    WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
				 -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
                 -- 312 = CHIPS_SALE_REMAINING_AMOUNT;  313 = CHIPS_SALE_REMAINING_AMOUNT
      AND   CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE,
                        @_TYPE_CHIPS_SALE_REMAINING_AMOUNT, @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT)
      AND   CM_DATE >= @_DATE_FROM
      AND   CM_DATE <  @_DATE_TO
    GROUP   BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE
    END
    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_1 ORDER BY GTT_NAME, GT_NAME

    -- DROP TEMPORARY TABLE
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_1
  END

  -- By selected currency
  IF @_SELECTED_OPTION_FORM = 1
  BEGIN
   SELECT   0 AS TYPE_SESSION
          , CS_SESSION_ID AS SESSION_ID		  
          , GT_NAME AS GT_NAME
          , CS_OPENING_DATE AS SESSION_DATE
          , GTT_NAME AS GTT_NAME
          , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
          , ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
          , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
          , (SELECT ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0)
               FROM cashier_movements WHERE cm_session_id = cs_session_id ) AS REMINING
          , GTSC_ISO_CODE AS CURRENCY_ISO_CODE
		  , MAX(CS_NAME) AS SESSION_NAME
		  , CT_NAME AS CT_NAME
     INTO   #CHIPS_OPERATIONS_TABLE_OPTION_2
     FROM   CASHIER_SESSIONS
			  JOIN CASHIER_TERMINALS ON (cs_cashier_id = ct_cashier_id)
			  LEFT JOIN GAMING_TABLES_SESSIONS ON (GTS_CASHIER_SESSION_ID = CS_SESSION_ID)
			  LEFT JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON (GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID)
			  INNER JOIN GAMING_TABLES GT ON (GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID)
              INNER JOIN GAMING_TABLES_TYPES ON (GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID)
    WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
      AND   GT_AREA_ID = CASE WHEN @_AREA = '' THEN GT_AREA_ID ELSE @_AREA END
      AND   GT_BANK_ID = CASE WHEN @_BANK = '' THEN GT_BANK_ID ELSE @_BANK END
      AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
      AND   CS_OPENING_DATE >= @_DATE_FROM
      AND   CS_OPENING_DATE <  @_DATE_TO
      AND   GT_HAS_INTEGRATED_CASHIER = 1
      AND   GTSC_TYPE IN (@_CHIP_RE, @_CHIP_NR)
      AND   GTSC_ISO_CODE = @_SELECTED_CURRENCY
      AND   (ISNULL(GTS_TOTAL_SALES_AMOUNT, 0) + ISNULL(GTS_TOTAL_PURCHASE_AMOUNT, 0)) <> 0
    GROUP   BY CT_NAME, GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE, GTSC_ISO_CODE
    ORDER   BY GTT_NAME

  -- Check if cashiers must be visible
  IF @_ONLY_TABLES = 0
  BEGIN
    -- Select and join data to show cashiers
    -- Adding cashiers without gaming tables

    INSERT INTO #CHIPS_OPERATIONS_TABLE_OPTION_2
      SELECT   1 AS TYPE_SESSION
             , CM_SESSION_ID AS SESSION_ID			 
             , CT_NAME as GT_NAME
             , CS_OPENING_DATE AS SESSION_DATE
             , @_CASHIERS_NAME as GTT_NAME
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS GTS_TOTAL_SALES_AMOUNT
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS GTS_TOTAL_PURCHASE_AMOUNT

             -- CALCULATE DELTA
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0)
             - ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS DELTA

             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                               ELSE 0 END ), 0) AS REMINING

             , CM_CURRENCY_ISO_CODE AS CURRENCY_ISO_CODE
			 , MAX(CS_NAME) AS SESSION_NAME
			 , CT_NAME AS CT_NAME
        FROM   CASHIER_MOVEMENTS
			   INNER JOIN CASHIER_SESSIONS ON (CS_SESSION_ID = CM_SESSION_ID)
			   INNER JOIN CASHIER_TERMINALS ON (CM_CASHIER_ID = CT_CASHIER_ID)
               LEFT JOIN GAMING_TABLES_SESSIONS ON (GTS_CASHIER_SESSION_ID = CM_SESSION_ID)
			   LEFT JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON (GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID)
       WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
               -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
			   -- 312 = CHIPS_SALE_REMAINING_AMOUNT;  313 = CHIPS_SALE_REMAINING_AMOUNT
        AND   CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE,
                          @_TYPE_CHIPS_SALE_REMAINING_AMOUNT, @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT)
        AND   CM_DATE >= @_DATE_FROM
        AND   CM_DATE <  @_DATE_TO
        AND   CM_CURRENCY_ISO_CODE = @_SELECTED_CURRENCY
      GROUP   BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE, CM_CURRENCY_ISO_CODE
    END

    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_2 ORDER BY GTT_NAME,GT_NAME

    -- DROP TEMPORARY TABLE
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_2
 END

END  -- END PROCEDURE

GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO
--  /*
--  ----------------------------------------------------------------------------------------------------------------
--  BASIC REPORT QUERY FOR GAMING TABLES

--  Version  Date           User     Description
--  ----------------------------------------------------------------------------------------------------------------
--  1.0.1    24-DEC-2013    RMS      Using GAMING_TABLE_SESSION
--  1.0.2    20-FEB-2014    RMS      Only get data of closed gaming table sessions
--  1.0.3    26-FEB-2015    ANM      Add sum in SESSION_HOURS to get average time later in code
--  1.0.4    06-JUN-2016    FOS      Calculate drop in gamblingtables and cashier
--  1.0.5    18-JUL-2016    RAB      Add MultiCurrency to report

--  Requeriments:
--     -- Functions:
--           dbo.GT_Calculate_DROP( Amount )
--           dbo.GT_Calculate_WIN ( AmountAdded, AmountSubtracted, CollectedAmount, TipsAmount )

--  Parameters:
--     -- BASE TYPE:          Indicates if data is based on table types (0) or the tables (1).
--     -- TIME INTERVAL:      Range of time to group data between days (0), months (1) and years (2).
--     -- DATE FROM:          Start date for data collection. (If NULL then use first available date).
--     -- DATE TO:            End date for data collection. (If NULL then use current date).
--     -- ONLY ACTIVITY:      If true (1) results will only show tables or table types with activity inside the dates range,
--                            else (0) results will include all tables or table types.
--     -- ORDER BY:           0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.
--     -- VALID TYPES:        A string that contains the valid table types to list.
--     -- SELECTED CURRENCY:  Selected currency in a frm_gaming_tables_income_report

--  Process: (From inside to outside)

--     1- Core Query:
--           Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.

--     2- Specific report query:
--           Here we group by base type and time interval and includes the session information for the interval, generating a temporary
--           table.

--     3- Filter activity:
--           Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
--           join to filter tables or table types without activity.

--     4- Drop the temporary table.

--   Results:

--     We can create 6 different report, based on table types or tables and grouped by days, months and years.
--  ----------------------------------------------------------------------------------------------------------------
--  */

--CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
--(
--    @pBaseType            INTEGER
--  , @pTimeInterval        INTEGER
--  , @pDateFrom            DATETIME
--  , @pDateTo              DATETIME
--  , @pOnlyActivity        INTEGER
--  , @pOrderBy             INTEGER
--  , @pValidTypes          VARCHAR(4096)
--  , @pSelectedCurrency    NVARCHAR(3)
--)
--AS
--BEGIN
---- DECLARATIONS
--   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
--   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
--   -- @_DAY_VAR:            Variable to create the intervals
--   DECLARE @_DAY_VAR DATETIME

---- PARAMETERS
--   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
--   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
--   DECLARE @_DATE_FROM        AS   DATETIME
--   DECLARE @_DATE_TO          AS   DATETIME
--   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
--   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
--   DECLARE @_DELIMITER        AS   CHAR(1)
--   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
--   DECLARE @_SELECTED_CURRENCY AS  NVARCHAR(3)

------------------------------------------------------------------------------------------------------------------

---- Initialization --
--SET @_BASE_TYPE        =   @pBaseType
--SET @_TIME_INTERVAL    =   @pTimeInterval
--SET @_DATE_FROM        =   @pDateFrom
--SET @_DATE_TO          =   @pDateTo
--SET @_ONLY_ACTIVITY    =   @pOnlyActivity
--SET @_ORDER_BY         =   @pOrderBy
--SET @_DELIMITER        =   ','
--SET @_SELECTED_CURRENCY =  @pSelectedCurrency

------------------------------------------------------------------------------------------------------------------

---- CHECK DATE PARAMETERS
--IF @_DATE_FROM IS NULL
--BEGIN
--   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
--   SET @_DATE_FROM = CAST('' AS DATETIME)
--   SET @pDateFrom  = CAST('' AS DATETIME)
--END

--IF @_DATE_TO IS NULL
--BEGIN
--   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
--   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
--   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
--END

---- ASSIGN TYPES PARAMETER INTO TABLE
--INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

--IF @_TIME_INTERVAL <> -1
-- BEGIN
--   -- INTERVALS DATES AND TABLE PREPARATION
--   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
--   SET @_DATE_FROM = CASE
--                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
--                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
--                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
--                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
--                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
--                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
--                     END

--   SET @_DATE_TO = CASE
--                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
--                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
--                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
--                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
--                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
--                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
--                     END

--   -- PREPARE THE TIME INTERVALS TABLE
--   SET @_DAY_VAR = @_DATE_FROM
--   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
--   BEGIN
--         -- SET THE LINK FIELD FOR THE RESULTS
--         IF @_BASE_TYPE = 0
--          BEGIN
--            -- LINK WITH TABLE TYPES
--            INSERT INTO @_DAYS_AND_TABLES
--                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
--                   FROM   GAMING_TABLES_TYPES AS X
--                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
--          END
--         ELSE
--          BEGIN
--            -- LINK WITH TABLES
--            INSERT INTO @_DAYS_AND_TABLES
--                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
--                   FROM   GAMING_TABLES AS X
--              LEFT JOIN   GAMING_TABLES_TYPES AS Z
--                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
--                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
--          END

--          -- SET INCREMENT
--          SET @_DAY_VAR = CASE
--                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
--                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
--                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
--                          END
--   END

--END -- IF INTERVALS <> - 1
--ELSE
--BEGIN

--   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
--   IF @_BASE_TYPE = 0
--    BEGIN
--      -- LINK WITH TABLE TYPES
--      INSERT INTO   @_DAYS_AND_TABLES
--           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
--             FROM   GAMING_TABLES_TYPES AS X
--            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
--    END
--   ELSE
--    BEGIN
--      -- LINK WITH TABLES
--      INSERT INTO   @_DAYS_AND_TABLES
--           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
--             FROM   GAMING_TABLES AS X
--        LEFT JOIN   GAMING_TABLES_TYPES AS Z
--               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
--            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
--    END

--END

---- SELECT INTO TEMPORARY TABLE WITH ALL DATA
---- SPECIFIC REPORT QUERY
--  SELECT   X.TABLE_IDENTIFIER
--         , X.TABLE_NAME
--         , X.TABLE_TYPE
--         , X.TABLE_TYPE_NAME
--         , X.CM_DATE_ONLY
--         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
--         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
--         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
--         , SUM(X.BUY_IN) AS BUY_IN
--         , SUM(X.CHIPS_IN) AS CHIPS_IN
--         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
--         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
--         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
--         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
--         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
--              THEN 0
--              ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100)
--           END AS HOLD
--         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
--         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
--              THEN 0
--              ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
--           END AS PAYOUT
--         , SUM(X.NETWIN) AS NETWIN
--         , X.ISO_CODE

--   INTO #GT_TEMPORARY_REPORT_DATA

--   FROM (
--            -- CORE QUERY
--            SELECT   CASE
--                        WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
--                            DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
--                        WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
--                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
--                        WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
--                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
--                     END AS CM_DATE_ONLY
--                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER     -- GET THE BASE TYPE IDENTIFIER
--                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME                                         -- GET THE BASE TYPE IDENTIFIER NAME
--                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE                            -- TYPE
--                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME                                       -- TYPE NAME
--                   , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN
--                   , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
--                   , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
--                   , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
--                   , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
--                   , SUM(GTPS_NETWIN) AS NETWIN
--                   , GTPS_ISO_CODE AS ISO_CODE
--                   , GT_THEORIC_HOLD  AS THEORIC_HOLD
--                   , CS_OPENING_DATE AS OPEN_HOUR
--                   , CS_CLOSING_DATE AS CLOSE_HOUR
--                   , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
--              FROM   GT_PLAY_SESSIONS
--             INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
--             INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
--             INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
--             INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
--               AND   CS_OPENING_DATE >= @pDateFrom
--               AND   CS_OPENING_DATE < @pDateTo
--             WHERE   CS_STATUS = 1
--              AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
--              AND    GTPS_ISO_CODE IN (@pSelectedCurrency)
--             GROUP   BY   GTPS_GAMING_TABLE_ID
--                   , GT_NAME
--                   , CS_OPENING_DATE
--                   , CS_CLOSING_DATE
--                   , GTT_GAMING_TABLE_TYPE_ID
--                   , GTT_NAME
--                   , GT_THEORIC_HOLD
--                   , GTPS_ISO_CODE
--             -- END CORE QUERY
--        ) AS X
-- GROUP BY  X.TABLE_IDENTIFIER
--         , X.TABLE_NAME
--         , X.TABLE_TYPE
--         , X.TABLE_TYPE_NAME
--         , X.CM_DATE_ONLY
--         , X.ISO_CODE

--  -- Group by indentifier and time interval

--IF @_TIME_INTERVAL <> -1
--BEGIN
--   -- INTERVALS DATES AND TABLE FINAL PREPARATION

--   -- FILTER ACTIVITY
--   IF @_ONLY_ACTIVITY = 0
--    BEGIN
--      -- JOIN THE SELECT WITH ALL DATA
--      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
--               DT.TABLE_IDENT_NAME AS TABLE_NAME,
--               DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
--               DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
--               ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
--               ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
--               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
--               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
--               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
--               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
--               ISNULL(ZZ.HOLD, 0) AS HOLD,
--               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
--               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
--               ISNULL(ZZ.NETWIN, 0) AS NETWIN,
--               ISO_CODE,
--               ZZ.OPEN_HOUR,
--               ZZ.CLOSE_HOUR,
--               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
--               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
--               DATE_TIME
--        FROM   @_DAYS_AND_TABLES DT
--   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
--               ON (
--                  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
--                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
--                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
--                )
--         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
--       -- SET ORDER
--    ORDER BY   DT.TABLE_TYPE_IDENT ASC,
--               CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
--               DATE_TIME DESC;

--    END
--   ELSE
--    BEGIN
--      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
--      SELECT  DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
--              DT.TABLE_IDENT_NAME AS TABLE_NAME,
--              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
--              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
--              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
--              ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
--              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
--              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
--              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
--              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
--              ISNULL(ZZ.HOLD, 0) AS HOLD,
--              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
--              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
--              ISNULL(ZZ.NETWIN, 0) AS NETWIN,
--              ISO_CODE,
--              ZZ.OPEN_HOUR,
--              ZZ.CLOSE_HOUR,
--              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
--              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
--              DATE_TIME
--        FROM  @_DAYS_AND_TABLES DT
--  INNER JOIN  #GT_TEMPORARY_REPORT_DATA ZZ
--              ON(
--                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
--                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
--                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
--              )
--              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
--       -- SET ORDER
--   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
--              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
--              DATE_TIME DESC;

--    END -- IF ONLY_ACTIVITY

--END
--ELSE  -- ELSE WITHOUT INTERVALS
--BEGIN

--   -- FINAL WITHOUT INTERVALS

--      -- FILTER ACTIVITY
--   IF @_ONLY_ACTIVITY = 0
--    BEGIN
--            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
--            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
--                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
--                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
--                     DT.TABLE_TYPE_NAME,
--                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
--                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
--                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
--                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
--                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
--                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
--                     ISNULL(ZZ.HOLD, 0) AS HOLD,
--                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
--                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
--                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
--                     ISO_CODE,
--                     OPEN_HOUR,
--                     CLOSE_HOUR,
--                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
--                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
--              FROM   @_DAYS_AND_TABLES DT
--         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
--                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
--          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
--                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
--                     DATE_TIME DESC;

--     END
--    ELSE
--     BEGIN
--            -- JOIN DATA WITH ONLY ACTIVITY
--            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
--                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
--                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
--                     DT.TABLE_TYPE_NAME,
--                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
--                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
--                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
--                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
--                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
--                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
--                     ISNULL(ZZ.HOLD, 0) AS HOLD,
--                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
--                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
--                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
--                     ISO_CODE,
--                     OPEN_HOUR,
--                     CLOSE_HOUR,
--                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
--                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
--              FROM   @_DAYS_AND_TABLES DT
--        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
--                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
--          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
--                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
--                     DATE_TIME DESC;

--     END
-- END

---- ERASE THE TEMPORARY DATA
--DROP TABLE #GT_TEMPORARY_REPORT_DATA

--END -- END PROCEDURE
--GO

---- PERMISSIONS
--GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION
--GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLE SESSION INFORMATION

Version   Date            User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0     31-JAN-2014     RMS       First Release
1.0.1     27-APR-2016     RAB       Include differents chip types in a operations.
1.0.2     25-AUG-2016     FGB       GT_Session_Information: Optimization.
1.0.3     07-SEP-2016     FGB       GT_Session_Information: Optimization.

Requeriments:
   -- Functions:

Parameters:
   -- BASE TYPE:        To select if report data source is session or cashier movements.
                          0 - By Session
                          1 - By Cashier Movements
   -- DATE FROM:        Start date for data collection. (If NULL then use first available date).
   -- DATE TO:          End date for data collection. (If NULL then use current date).
   -- STATUS:           The gaming table session status (Opened or Closed) (-1 to show all).
   -- ENABLED:          The gaming table status (Enabled or Disabled) (-1 to show all).
   -- AREA:             Location Area of the gaming table.
   -- BANK:             Area bank of location.
   -- CHIPS ISO CODE:   ISO Code for chips in cage.
   -- CHIPS COINS CODE: Coins code for chips in cage.
   -- VALID TYPES:      A string that contains the valid table types to list.
*/

CREATE PROCEDURE [dbo].[GT_Session_Information]
(
    @pBaseType            INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pStatus              INTEGER
  , @pEnabled             INTEGER
  , @pAreaId              INTEGER
  , @pBankId              INTEGER
  , @pChipsISOCode        VARCHAR(50)
  , @pChipsCoinsCode      INTEGER
  , @pValidTypes          VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
DECLARE @_NATIONAL_CURRENCY AS  VARCHAR(3)

-- CHIP TYPES
DECLARE @_CHIPS_RE    AS INTEGER
DECLARE @_CHIPS_NR    AS INTEGER
DECLARE @_CHIPS_COLOR AS INTEGER

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL          AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL             AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN                     AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT                    AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT               AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION            AS   INTEGER
DECLARE @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    AS   INTEGER

DECLARE @_MOVEMENT_CLOSE_SESSION                 AS   INTEGER

----------------------------------------------------------------------------------------------------------------
-- Initialization --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)
SET @_CHIPS_RE         =   1001
SET @_CHIPS_NR         =   1002
SET @_CHIPS_COLOR      =   1003

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL          =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL             =   303
SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE =   310
SET @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    =   309
SET @_MOVEMENT_FILLER_IN                     =   2
SET @_MOVEMENT_FILLER_OUT                    =   3
SET @_MOVEMENT_CAGE_FILLER_OUT               =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION            =   201
SET @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    =   1000006 --- CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN + CageMeters.CageConceptId.Tips;

SET @_MOVEMENT_CLOSE_SESSION                 =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

-- GET NATIONAL CURRENCY
SELECT @_NATIONAL_CURRENCY = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode' 

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0
BEGIN
  -- REPORT BY GAMING TABLE SESSION
  SELECT    GTS_CASHIER_SESSION_ID
          , CS_NAME
          , CS_STATUS
          , GT_NAME
          , GTT_NAME
          , GT_HAS_INTEGRATED_CASHIER
          , CT_NAME
          , GU_USERNAME
          , CS_OPENING_DATE
          , CS_CLOSING_DATE
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_PURCHASE_AMOUNT     ELSE GTSC_TOTAL_PURCHASE_AMOUNT   END, 0)  )  AS GTS_TOTAL_PURCHASE_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_SALES_AMOUNT        ELSE GTSC_TOTAL_SALES_AMOUNT      END, 0)  )  AS GTS_TOTAL_SALES_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_INITIAL_CHIPS_AMOUNT      ELSE GTSC_INITIAL_CHIPS_AMOUNT    END, 0)  )  AS GTS_INITIAL_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FILLS_CHIPS_AMOUNT        ELSE GTSC_FILLS_CHIPS_AMOUNT      END, 0)  )  AS GTS_FILLS_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_CREDITS_CHIPS_AMOUNT      ELSE GTSC_CREDITS_CHIPS_AMOUNT    END, 0)  )  AS GTS_CREDITS_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FINAL_CHIPS_AMOUNT        ELSE GTSC_FINAL_CHIPS_AMOUNT      END, 0)  )  AS GTS_FINAL_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TIPS                      ELSE GTSC_TIPS                    END, 0)  )  AS GTS_TIPS               
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_COLLECTED_AMOUNT          ELSE GTSC_COLLECTED_AMOUNT        END, 0)  )  AS GTS_COLLECTED_AMOUNT   
          , GTS_CLIENT_VISITS
          , AR_NAME
          , BK_NAME
          , GTS_GAMING_TABLE_SESSION_ID
          , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
          , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END AS GTSC_ISO_CODE
          , dbo.GT_Calculate_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0), ISNULL(GTS_COLLECTED_AMOUNT, 0), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_COPY_DEALER_VALIDATED_AMOUNT ELSE GTSC_COPY_DEALER_VALIDATED_AMOUNT END, 0)  ) AS GTS_COPY_DEALER_VALIDATED_AMOUNT
   FROM         GAMING_TABLES_SESSIONS
   LEFT    JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT    JOIN GUI_USERS                           ON GU_USER_ID                  = CS_USER_ID
   LEFT    JOIN GAMING_TABLES                       ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT    JOIN GAMING_TABLES_TYPES                 ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT    JOIN AREAS                               ON GT_AREA_ID                  = AR_AREA_ID
   LEFT    JOIN BANKS                               ON GT_BANK_ID                  = BK_BANK_ID
   LEFT    JOIN CASHIER_TERMINALS                   ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CS_OPENING_DATE >= @_DATE_FROM
   AND   CS_OPENING_DATE < @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   GROUP BY GTS_CASHIER_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , GTS_GAMING_TABLE_SESSION_ID
         , GTS_OWN_SALES_AMOUNT
         , GTS_EXTERNAL_SALES_AMOUNT
         , GTS_COLLECTED_AMOUNT
         , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END 
     ORDER BY CS_OPENING_DATE

END
ELSE
BEGIN
  -- REPORT BY CASHIER MOVEMENTS
    SELECT   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL          THEN CM_ADD_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_PURCHASE_AMOUNT
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL             THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_SALES_AMOUNT
           , 0     AS CM_INITIAL_CHIPS_AMOUNT                                         --  Don't show initial chips amount for movements inquiry
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                      THEN CM_ADD_AMOUNT ELSE 0 END
                )  AS CM_FILLS_CHIPS_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                 = @_MOVEMENT_FILLER_OUT
                      THEN CM_SUB_AMOUNT
                      WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       THEN CM_INITIAL_BALANCE
                       ELSE 0
                       END
                )  AS CM_CREDITS_CHIPS_AMOUNT
           , 0     AS CM_FINAL_CHIPS_AMOUNT                                           --  Don't show final chips amount for movements inquiry
           , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_INITIAL_BALANCE
                      
                      WHEN CM_TYPE                  = @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS  
                      THEN CM_ADD_AMOUNT    
                      ELSE 0    
                 END
                )  AS CM_TIPS
           , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                          CASE WHEN CM_CURRENCY_ISO_CODE IS NULL
                                 OR (CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE
                                AND  CM_CAGE_CURRENCY_TYPE NOT IN (@_CHIPS_RE, @_CHIPS_NR, @_CHIPS_COLOR) ) THEN
                               CASE WHEN CM_TYPE  = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                                    ELSE 0
                               END
                               ELSE 0
                          END
                      WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                          CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                               WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                               ELSE 0
                          END
                      ELSE 0
                 END
                )  AS CM_COLLECTED_AMOUNT
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END AS CM_CURRENCY_ISO_CODE
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTS_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
           , 0 AS GTS_COPY_DEALER_VALIDATED_AMOUNT
      FROM         CASHIER_MOVEMENTS
     INNER   JOIN  CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
     INNER   JOIN  GAMING_TABLES_SESSIONS             ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
      LEFT   JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID 
                                                     AND GTSC_TYPE <> @_CHIPS_COLOR
                                                     AND gtsc_iso_code = cm_currency_iso_code 
                                                     AND gtsc_type = cm_cage_currency_type
      LEFT   JOIN  GUI_USERS                          ON GU_USER_ID                  = CS_USER_ID
      LEFT   JOIN  GAMING_TABLES                      ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
      LEFT   JOIN  GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
      LEFT   JOIN  AREAS                              ON GT_AREA_ID                  = AR_AREA_ID
      LEFT   JOIN  BANKS                              ON GT_BANK_ID                  = BK_BANK_ID
      LEFT   JOIN  CASHIER_TERMINALS                  ON GT_CASHIER_ID               = CT_CASHIER_ID
     WHERE   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
       AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
       AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
       AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
       AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
       AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
  GROUP BY   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END
  ORDER BY   CS_OPENING_DATE
  
END

END -- END PROCEDURE GT_Session_Information
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


CREATE PROCEDURE [dbo].[GT_Cancellations]
(
    @pCashierId           BIGINT,
    @pDateFrom            DATETIME,
    @pDateTo              DATETIME,
    @pType                INT, -- 0: only Sales ; 1: only Purchases ; -1: all
    @pUserId              INT,
    @pSelectedCurrency    NVARCHAR(3)
)
AS
BEGIN
      DECLARE @TYPE_CHIPS_SALE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE AS INT
      DECLARE @UNDO_STATUS AS INT
      DECLARE @NATIONAL_CURRENCY AS NVARCHAR(3)

      SET @TYPE_CHIPS_SALE = 300
      SET @TYPE_CHIPS_PURCHASE = 301
      SET @TYPE_CHIPS_SALE_TOTAL = 303
      SET @TYPE_CHIPS_PURCHASE_TOTAL = 304
      SET @TYPE_CHIPS_SALE_TOTAL_EXCHANGE = 309
      SET @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE = 310
      SET @UNDO_STATUS = 2
      SET @pType = ISNULL(@pType, -1)
      SET @NATIONAL_CURRENCY = (SELECT GP_KEY_VALUE
                                FROM GENERAL_PARAMS
                                WHERE GP_GROUP_KEY = 'RegionalOptions'
                                  AND GP_SUBJECT_KEY = 'CurrencyISOCode')

      SELECT
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_CASHIER_NAME
                  ELSE NULL
                  END AS ORIGEN,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_DATE
                  ELSE NULL
                  END AS CM_DATE,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN ABS(CM_SUB_AMOUNT)
                 WHEN CM_TYPE IN (@TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_PURCHASE_TOTAL) THEN ABS(CM_ADD_AMOUNT)
                 WHEN CM_TYPE IN (@TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN ABS(CM_INITIAL_BALANCE)
                 END AS AMOUNT,
            ABS(CASE WHEN CM_CURRENCY_DENOMINATION IS NULL OR CM_CURRENCY_DENOMINATION = 0 THEN 0
                      ELSE
                          CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END) THEN CM_SUB_AMOUNT
                                ELSE CM_ADD_AMOUNT
                                END / CM_CURRENCY_DENOMINATION
                      END) AS QUANTITY,
            CM_CURRENCY_DENOMINATION,
            CM.CM_MOVEMENT_ID,
            CM.CM_TYPE,
            CM.CM_USER_NAME,
            CM.CM_OPERATION_ID,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN AO.AO_DATETIME
                  ELSE NULL
                  END AS AO_DATETIME
      FROM CASHIER_MOVEMENTS CM
            LEFT JOIN ACCOUNT_OPERATIONS CANCEL ON CM.CM_OPERATION_ID          = CANCEL.AO_OPERATION_ID
            LEFT JOIN ACCOUNT_OPERATIONS AO     ON CANCEL.AO_UNDO_OPERATION_ID = AO.AO_OPERATION_ID
      WHERE (@pCashierId IS NULL OR @pCashierId = CM_CASHIER_ID)
              AND (@pUserId IS NULL OR @pUserId = CM_USER_ID)
              AND CM_UNDO_STATUS = @UNDO_STATUS
              AND CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE)
              AND CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
              AND ((         @pType = 1  AND CM.CM_TYPE IN (@TYPE_CHIPS_PURCHASE,
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_PURCHASE_TOTAL
                                                              ELSE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE END))
                         OR (@pType = 0  AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE,
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL
                                                              ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END))
                         OR (@pType = -1 AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE,
                                                            @TYPE_CHIPS_PURCHASE,
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL
                                                              ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END,
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_PURCHASE_TOTAL
                                                              ELSE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE END)))
              AND CM_CURRENCY_ISO_CODE IN (@pSelectedCurrency)
      ORDER BY CM.CM_MOVEMENT_ID

END --GT_CANCELLATIONS

GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Cancellations] TO [wggui] WITH GRANT OPTION
GO

/*
----------------------------------------------------------------------------------------------------------------
QUERY FOR GAMBLING TABLES: CHIPS DROP INFORMATION

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    06-FEB-2014    JVV      First Release

Parameters:
   -- GamblingTableId
   -- DateFrom
   -- DateTo
*/

CREATE PROCEDURE [dbo].[GT_Get_Drop]
 (
    @pGamblingTableId   BIGINT
  , @pDateFrom          DATETIME
  , @pDateTo            DATETIME
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_GAMBLING_TABLE_ID  AS   BIGINT
DECLARE @_DATE_FROM          AS   DATETIME
DECLARE @_DATE_TO            AS   DATETIME

-- CASHIER MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialization --

SET @_GAMBLING_TABLE_ID             =   @pGamblingTableId
SET @_DATE_FROM                     =   @pDateFrom
SET @_DATE_TO                       =   @pDateTo
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END
----------------------------------------------------------------------------------------------------------------

IF @_GAMBLING_TABLE_ID IS NOT NULL
  BEGIN
    -- DROP BY GAMING TABLE
    SELECT   @_GAMBLING_TABLE_ID  AS GAMBLING_TABLE_ID
           , CM_GAMING_TABLE_SESSION_ID AS GAMBLING_TABLE_SESSION_ID
           , CM_CASHIER_ID AS CASHIER_ID
           , CM_SESSION_ID AS CASHIER_SESSION_ID
           , CM_DATE AS DATE_DROP
            , CV_VOUCHER_ID AS VOUCHER_ID
           , CM_OPERATION_ID AS OPERATION_ID
           , CM_ACCOUNT_ID AS ACCOUNT_ID
           , CM_SUB_AMOUNT AS DROP_AMOUNT
      FROM   CASHIER_MOVEMENTS
      LEFT JOIN CASHIER_VOUCHERS ON CASHIER_VOUCHERS.CV_OPERATION_ID = CASHIER_MOVEMENTS.CM_OPERATION_ID AND CASHIER_VOUCHERS.cv_type = 8   --(enum CashierVoucherType.ChipsSale = 8)
     WHERE   CM_TYPE = @_CHIPS_MOVEMENT_SALES_TOTAL
       AND  (CM_GAMING_TABLE_SESSION_ID IN (SELECT   GTS_GAMING_TABLE_SESSION_ID
                                              FROM   GAMING_TABLES_SESSIONS
                                             WHERE   GTS_GAMING_TABLE_ID IN (@_GAMBLING_TABLE_ID))
                       OR CM_SESSION_ID IN (SELECT   GTS_CASHIER_SESSION_ID
                                              FROM   GAMING_TABLES_SESSIONS
                                             WHERE   GTS_GAMING_TABLE_ID IN (@_GAMBLING_TABLE_ID))
            )
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
  END

ELSE
  BEGIN
    SELECT   ISNULL((SELECT   GTS_GAMING_TABLE_ID
                       FROM   GAMING_TABLES_SESSIONS
                      WHERE   GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID )
           , (SELECT   GTS_GAMING_TABLE_ID
                FROM   GAMING_TABLES_SESSIONS
               WHERE   GTS_CASHIER_SESSION_ID = CM_SESSION_ID ))  AS  GAMBLING_TABLE_ID
           , CM_GAMING_TABLE_SESSION_ID AS GAMBLING_TABLE_SESSION_ID
           , CM_CASHIER_ID AS CASHIER_ID
           , CM_SESSION_ID AS CASHIER_SESSION_ID
           , CM_DATE AS DATE_DROP
           , CV_VOUCHER_ID AS VOUCHER_ID
           , CM_OPERATION_ID AS OPERATION_ID
           , CM_ACCOUNT_ID AS ACCOUNT_ID
           , CM_SUB_AMOUNT AS DROP_AMOUNT
      FROM   CASHIER_MOVEMENTS
      LEFT JOIN CASHIER_VOUCHERS ON CASHIER_VOUCHERS.CV_OPERATION_ID = CASHIER_MOVEMENTS.CM_OPERATION_ID AND CASHIER_VOUCHERS.cv_type = 8
     WHERE   CM_TYPE = @_CHIPS_MOVEMENT_SALES_TOTAL
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
  END

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Get_Drop] TO [wggui] WITH GRANT OPTION
GO

--OLD FUNCTION
IF OBJECT_ID (N'dbo.GT_Calculate_TIP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_TIP;
GO