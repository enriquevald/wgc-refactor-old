IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO
--EXEC	[dbo].[GetCashierTransactionsTaxes_Prueba3]
--		@pStartDatetime = N'2015-09-01 00:00:00.000',
--		@pEndDatetime = N'2016-01-01 00:00:00.000',
--		@pTerminalId = 13,
--		@pCmMovements = N'103, 304',
--		@pHpMovements = N'0,1,2,30, 1000, 1001, 11001, 11002, 1010',
--		@pPaymentUserId = 16,
--		@pAuthorizeUserId = null,  --18,

/*

----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CASHIER TRANSACTIONS TAXES

Version           Date                    User                    Description
----------------------------------------------------------------------------------------------------------------
1.0.0             22-OCT-2015             CPC                     New procedure
1.0.1             24-DEC-2015             JML                     New version
1.0.2             12-APR-2016             EOR					            Add Handpays Pending                     

Requirements:
   -- Functions:
         
Parameters:
   -- START DATE:            Start date for data collection. (If NULL then use first available date).
   -- END DATE:              End date for data collection. (If NULL then use current date).
   -- TERMINAL ID:           The terminal id for data collection (If NULL then all terminals)
   -- CASHIER MOVEMENTS:     Cashier movements in format '1,2,3,4'.  CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) 
   --                         TAX_ON_PRIZE1 = 6,
   --                         TAX_ON_PRIZE2 = 14,
   --                                                                                      
   --                         TITO_TICKET_CASHIER_PAID_CASHABLE = 103,
   --                         TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE = 124,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE = 125,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE = 126,
   --                                                                                      
   --                         CHIPS_PURCHASE_TOTAL = 304,
   --
   --                         HANDPAY = 30,
   --                         MANUAL_HANDPAY = 32,
   --
   -- HANDPAYS MOVEMENTS:    Handpays movements in format '1,2,3,4'. 
   -- PAYMENT USER ID:       The payment user id for data collection (If NULL then all payment users)
   -- AUTHORIZE USER ID:     The authorize user id for data collection (If NULL then all authorize users)
   
*/  
CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
  DECLARE @SelectPartToExecute INT -- 1:HP,  2:CM,  3:Both
      
  SET @_PAID = 32768
  
  SET @SelectPartToExecute = 3 

  IF @pHpMovements IS NOT NULL AND @pCmMovements IS NULL
  BEGIN
     SET @SelectPartToExecute = 2
  END
  ELSE IF @pHpMovements IS NULL AND @pCmMovements IS NOT NULL
  BEGIN
     SET @SelectPartToExecute = 1
  END
     
  
  SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 WHEN  38 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32, 38) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
' + CHAR(10) 

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 2
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , AM_TYPE                   AS TYPE2
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   ACCOUNT_MOVEMENTS WITH(INDEX( IX_am_operation_id)) ON AM_OPERATION_ID = AO_OPERATION_ID
INNER JOIN   HANDPAYS          WITH(INDEX( IX_HP_MOVEMENT_ID))       ON HP_MOVEMENT_ID  = AM_MOVEMENT_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
--------      + CASE WHEN @pWithTaxes = 1 THEN ' AND HP_TAX_AMOUNT > 0' ELSE ' AND HP_TAX_AMOUNT = 0' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + CHAR(10) 
END


IF @SelectPartToExecute = 3
BEGIN
  SET @_SQL = @_SQL + ' UNION ' + CHAR(10)
END

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 1
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS BEFORE_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE2
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
----     + CASE WHEN @pWithTaxes = 1 THEN ' AND CM_ADD_AMOUNT > 0' ELSE ' AND CM_ADD_AMOUNT = 0' END + CHAR(10) 
END
 
SET @_SQL = @_SQL + ' ORDER   BY DATETIME DESC ' + CHAR(10) 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL  
   
END  

GO
GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO