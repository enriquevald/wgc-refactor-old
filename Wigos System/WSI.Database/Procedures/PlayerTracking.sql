--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME : PlayerTracking.sql
-- 
--   DESCRIPTION : Procedures for Player Tracking
-- 
-- REVISION HISTORY :
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-SEP-2010 ACC    First release.
-- 21-SEP-2010 TJG    Factors to calculate points are obtained according to the 
--                    account's level and from the GENERAL_PARAMS
-- 30-SEP-2010 AJQ    Cancellable Operation Id is reset when played_amount > 0.
-- 20-JAN-2012 AJQ    Provider's points multiplier
-- 01-FEB-2012 AJQ    Re-calculate the non-redeemable amount after a cash-in-while-playing.
-- 22-FEB-2012 RCI    Read the new NonRedeemable2 amount.
-- 23-FEB-2012 AJQ & RCI    Recalculate NR1 and NR2.
-- 27-FEB-2012 RCI    BalanceParts: Fixed NR1 amount when exists a WonLock.
-- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE : Read PlaySession & Accounts & PlayerTracking data
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--          @TerminalId                 int            
--          @StandAlone                 bit
--          @AccountId                  bigint         
--          @InitialBalance             money          
--          @CashIn                     money
--          @PlayedAmount               money          
--          @WonAmount                  money          
--          @FinalBalance               money        
--          @InitialNonRedeemable       money        
--          @NonRedeemable2             money        
--          @AccountBalance             money          
--          @HolderName                 nvarchar (50)
--          @HolderLevel                int          
--          @MaxAllowedAccountBalance   numeric (20,6)
--          @StatusCode                 int            
--          @StatusText                 nvarchar (254) 
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadData]
GO
CREATE PROCEDURE [dbo].[PT_ReadData]
  @PlaySessionId              bigint
, @TerminalId                 int             OUTPUT
, @StandAlone                 bit             OUTPUT
, @AccountId                  bigint          OUTPUT
, @InitialBalance             money           OUTPUT
, @CashIn                     money           OUTPUT
, @PlayedAmount               money           OUTPUT
, @WonAmount                  money           OUTPUT
, @FinalBalance               money           OUTPUT
, @InitialNonRedeemable       money           OUTPUT
, @NonRedeemable2             money           OUTPUT
, @InitialCashIn              money           OUTPUT
, @PrizeLock                  money           OUTPUT
, @AccountBalance             money           OUTPUT
, @PointsBalance              money           OUTPUT
, @HolderName                 nvarchar (50)   OUTPUT
, @HolderLevel                int             OUTPUT
, @MaxAllowedAccountBalance   numeric (20,6)  OUTPUT
, @StatusCode                 int             OUTPUT
, @StatusText                 nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc int
	
  SET @StatusCode = 1
	SET @StatusText = 'PT_ReadData: Reading PlaySession...'
	
	--
	-- Select data from PLAY_SESSIONS & ACCOUNTS table
	--
  SELECT @TerminalId            = PS_TERMINAL_ID
       , @StandAlone            = PS_STAND_ALONE
       , @AccountId             = PS_ACCOUNT_ID
       , @InitialBalance        = PS_INITIAL_BALANCE + PS_CASH_IN
       , @CashIn                = PS_CASH_IN
       , @PlayedAmount          = PS_PLAYED_AMOUNT
       , @WonAmount             = PS_WON_AMOUNT
       , @FinalBalance          = PS_FINAL_BALANCE
       , @InitialNonRedeemable  = AC_INITIAL_NOT_REDEEMABLE
       , @NonRedeemable2        = AC_NOT_REDEEMABLE
       , @InitialCashIn         = AC_INITIAL_CASH_IN
       , @PrizeLock             = AC_NR_WON_LOCK
       , @AccountBalance        = AC_BALANCE
       , @PointsBalance         = AC_POINTS 
       , @HolderName            = ISNULL (AC_HOLDER_NAME, '')
       , @HolderLevel           = ISNULL (AC_HOLDER_LEVEL, 0)
    FROM PLAY_SESSIONS, ACCOUNTS
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId
     AND PS_ACCOUNT_ID      = AC_ACCOUNT_ID

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode  = 1
    SET @StatusText  = 'PT_ReadData: Invalid PlaySessionId.'
    GOTO ERROR_PROCEDURE
  END

  IF ( @HolderName = '' )
  BEGIN
    SET @HolderLevel = 0  
  END
  
  --- 
  --- Read Max Allowed Account Balance
  ---
  SELECT @MaxAllowedAccountBalance = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Cashier'
     AND GP_SUBJECT_KEY = 'MaxAllowedAccountBalance'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadData: Invalid Cashier.MaxAllowedAccountBalance general parameter.'
    GOTO ERROR_PROCEDURE
  END
    
  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadData: PlaySession read.'

ERROR_PROCEDURE:

END -- PT_ReadData
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable.
-- 
--  PARAMS :
--      - INPUT :
--          @Balance             money
--          @InitialCashIn       money
--          @NonRedeemable1      money
--          @NonRedeemable2      money
--          @WonLock             money
--
--      - OUTPUT :
--          TOTAL_BALANCE        money
--          TOTAL_REDEEMABLE     money
--          TOTAL_NON_REDEEMABLE money
--          PART_REFUND          money
--          PART_WON             money
--          PART_NR1             money
--          PART_NR2             money
--          PART_NR_WON          money
--          LOCK_BROKEN          bit
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_BalanceParts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_BalanceParts]
GO
CREATE PROCEDURE [dbo].[WSP_BalanceParts]
  @Balance        money
, @InitialCashIn  money
, @NonRedeemable1 money
, @NonRedeemable2 money
, @WonLock        money
AS
BEGIN
  DECLARE @tmp_balance          money
  DECLARE @part_nr1             money
  DECLARE @part_nr2             money
  DECLARE @part_nr_won          money
  DECLARE @lock_broken          bit
  DECLARE @part_dev             money
  DECLARE @part_won             money
  DECLARE @total_redeemable     money
  DECLARE @total_non_redeemable money
  DECLARE @part_nr1_total       money
  
  SET NOCOUNT ON;

  SET @tmp_balance = CASE WHEN ( @Balance < 0 ) THEN 0 ELSE @Balance END

  -- Non Redeemable
  --   NR Withold
  SET @part_nr1 = CASE WHEN (@tmp_balance < @NonRedeemable1) THEN @tmp_balance ELSE @NonRedeemable1 END
  SET @tmp_balance = @tmp_balance - @part_nr1
  --   NR Cover Coupon
  SET @part_nr2 = CASE WHEN (@tmp_balance < @NonRedeemable2) THEN @tmp_balance ELSE @NonRedeemable2 END
  SET @tmp_balance = @tmp_balance - @part_nr2
  --   NR Won
  SET @part_nr_won = 0
  SET @lock_broken = 0

  -- Redeemable
  --   Devolution
  SET @part_dev = CASE WHEN (@tmp_balance < @InitialCashIn) THEN @tmp_balance ELSE @InitialCashIn END
  SET @tmp_balance = @tmp_balance - @part_dev
  --   Won
  SET @part_won = @tmp_balance
   
  IF ( @WonLock <> 0 )
  BEGIN
    -- WonLock -> Won is Non Redeemable 
    SET @part_nr_won = @part_won
    SET @part_won    = 0
      IF ( @part_nr_won >= @WonLock ) SET @lock_broken = 1
  END

  SET @total_redeemable     = @part_dev + @part_won
  SET @part_nr1_total       = @part_nr1 + @part_nr_won
  SET @total_non_redeemable = @part_nr1_total + @part_nr2

  SELECT @Balance as TOTAL_BALANCE, @total_redeemable as TOTAL_REDEEMABLE, @total_non_redeemable as TOTAL_NON_REDEEMABLE, 
         @part_dev as PART_REFUND, @part_won as PART_WON, @part_nr1_total as PART_NR1, @part_nr2 as PART_NR2,
         @part_nr_won as PART_NR_WON, @lock_broken as LOCK_BROKEN

END -- WSP_BalanceParts
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable.
-- 
--  PARAMS :
--      - INPUT :
--          @AccountId           bigint
--
--      - OUTPUT :
--          TOTAL_BALANCE        money
--          TOTAL_REDEEMABLE     money
--          TOTAL_NON_REDEEMABLE money
--          PART_REFUND          money
--          PART_WON             money
--          PART_NR1             money
--          PART_NR2             money
--          PART_NR_WON          money
--          LOCK_BROKEN          bit
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_AccountBalanceParts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_AccountBalanceParts]
GO
CREATE PROCEDURE [dbo].[WSP_AccountBalanceParts]
  @AccountId bigint
AS
BEGIN
  DECLARE @balance         money
  DECLARE @initial_cash_in money
  DECLARE @non_redeemable1 money
  DECLARE @non_redeemable2 money
  DECLARE @won_lock        money

  SELECT   @balance         = AC_BALANCE
         , @initial_cash_in = AC_INITIAL_CASH_IN
         , @non_redeemable1 = AC_INITIAL_NOT_REDEEMABLE
         , @non_redeemable2 = AC_NOT_REDEEMABLE
         , @won_lock        = ISNULL (AC_NR_WON_LOCK, 0)
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID    = @AccountId

  EXEC WSP_BalanceParts @balance, @initial_cash_in, @non_redeemable1, @non_redeemable2, @won_lock

END -- WSP_AccountBalanceParts
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @Balance                money
--          @InitialCashIn          money
--          @InitialNonRedeemable   money
--          @PrizeLock              money
--
--      - OUTPUT :
--          @PartNonRedeemable      money
--          @PartRedeemable         money
--          @PartNonRedeemable1     money
--          @PartNonRedeemable2     money
--          @PartRefund             money
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_BalancePartsWhenPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
GO
CREATE PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
  @Balance                money
, @InitialCashIn          money
, @InitialNonRedeemable   money
, @NonRedeemable2         money
, @PrizeLock              money
, @PartNonRedeemable      money  OUTPUT
, @PartRedeemable         money  OUTPUT
, @PartNonRedeemable1     money  OUTPUT
, @PartNonRedeemable2     money  OUTPUT
, @PartRefund             money  OUTPUT
AS
BEGIN

  DECLARE @balance_parts TABLE
     (balance_out money, total_redeemable money, total_non_redeemable money, part_refund money, part_won money,
      part_nr1 money, part_nr2 money, part_nr_won money, lock_broken bit)

  INSERT INTO @balance_parts
    EXECUTE dbo.WSP_BalanceParts @Balance, @InitialCashIn, @InitialNonRedeemable, @NonRedeemable2, @PrizeLock

  SELECT   @PartNonRedeemable  = total_non_redeemable
         , @PartRedeemable     = total_redeemable
         , @PartNonRedeemable1 = part_nr1
         , @PartNonRedeemable2 = part_nr2
         , @PartRefund         = part_refund
    FROM   @balance_parts

END -- PT_BalancePartsWhenPlaying
GO

--------------------------------------------------------------------------------
-- PURPOSE : Read Point calculation factors
-- 
--  PARAMS :
--      - INPUT :
--          @HolderName
--          @HolderLevel
--
--      - OUTPUT :
--          @RedeemablePlayedTo1Point
--          @TotalPlayedTo1Point
--          @RedeemableSpentTo1Point
--          @StatusCode     
--          @StatusText     
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadPointFactors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadPointFactors]
GO
CREATE PROCEDURE [dbo].[PT_ReadPointFactors]
  @HolderLevel                          int
, @RedeemableSpentTo1Point              numeric (20,6)  OUTPUT
, @RedeemablePlayedTo1Point             numeric (20,6)  OUTPUT
, @TotalPlayedTo1Point                  numeric (20,6)  OUTPUT
, @StatusCode                           int             OUTPUT
, @StatusText                           nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc           int
  DECLARE @level_name   nvarchar (50)

  SET @StatusCode = 1
  SET @StatusText = 'PT_ReadPointFactors: Reading Point Calculation Factors...'

  SET @RedeemableSpentTo1Point  = 0.00
  SET @RedeemablePlayedTo1Point = 0.00       
  SET @TotalPlayedTo1Point      = 0.00

	--
	-- Select data from GENERAL_PARAMS table: 
	--      * PlayerTracking.LevelXX.RedeemablePlayedTo1Point
	--      * PlayerTracking.LevelXX.TotalPlayedTo1Point
	--      * PlayerTracking.LevelXX.RedeemableSpentTo1Point
	--

  IF ( @HolderLevel = 1 )
    SET @level_name = 'Level01'
  ELSE IF ( @HolderLevel = 2 )
    SET @level_name = 'Level02'
  ELSE IF ( @HolderLevel = 3 )
    SET @level_name = 'Level03'
  ELSE IF ( @HolderLevel = 4 )
    SET @level_name = 'Level04'
  ELSE
    BEGIN
      --- 
      --- Wong Holder's Level 
      ---
      SET @StatusCode = 1
      SET @StatusText = 'PT_ReadPointFactors: Invalid Holder Level.'

      GOTO ERROR_PROCEDURE
    END  

  --- 
  --- Read level's RedeemablePlayedTo1Point
  ---
  SELECT @RedeemablePlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemablePlayedTo1Point'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemablePlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's TotalPlayedTo1Point
  ---
  SELECT @TotalPlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.TotalPlayedTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.TotalPlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's RedeemableSpentTo1Point
  ---
  SELECT @RedeemableSpentTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemableSpentTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemableSpentTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadPointFactors: Point Calculation read.'

  GOTO EXIT_PROCEDURE

ERROR_PROCEDURE:
  SET @RedeemablePlayedTo1Point = 0.00
  SET @TotalPlayedTo1Point      = 0.00
  SET @RedeemableSpentTo1Point  = 0.00

EXIT_PROCEDURE:

END -- PT_ReadPointFactors
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @PlayedTotal            money
--          @PlayedRedeemable       money
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteJackpot_AccumulatePlayed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SiteJackpot_AccumulatePlayed]
GO
CREATE PROCEDURE [dbo].[SiteJackpot_AccumulatePlayed]
  @PlaySessionId          bigint
, @PlayedTotal            money
, @PlayedRedeemable       money
AS
BEGIN
  DECLARE @site_jackpot   bit

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @site_jackpot = ISNULL ( ( SELECT PV_SITE_JACKPOT FROM PROVIDERS     WHERE PV_ID = (
                                 SELECT TE_PROV_ID      FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                 SELECT PS_TERMINAL_ID  FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @site_jackpot = 1 )
  BEGIN
    --
    -- Update Site Jackpot Played amounts
    --
    UPDATE SITE_JACKPOT_PARAMETERS
       SET SJP_PLAYED = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @PlayedRedeemable ELSE @PlayedTotal END
     WHERE SJP_ENABLED = 1 
  END

END -- SiteJackpot_AccumulatePlayed
GO

--------------------------------------------------------------------------------
-- PURPOSE : Accumulate points for player when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

  -- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @stand_alone                       bit
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable0           money
  DECLARE @initial_non_redeemable1           money
  DECLARE @non_redeemable2_0                 money
  DECLARE @non_redeemable2_1                 money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @part_nr1_out                      money
  DECLARE @part_nr2_out                      money
  DECLARE @part_refund_out                   money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money

  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  DECLARE @points_multiplier                 money
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  DECLARE @cash_in_while_playing             money
  DECLARE @session_cash_in                   money
  DECLARE @total_balance                     money
  DECLARE @redeemable_from_cash_in           money
  
  -- 07-DEC-2013 JML: Add variables for type of virtual accounts
  DECLARE @account_virtual_cashier           int
  DECLARE @account_virtual_terminal          int
  -- 11-FEB-2016 JRC: bucket puntos de nivel 	
  DECLARE @bucket_puntos_nivel			     int
  

  SET @account_virtual_cashier = 4
  SET @account_virtual_terminal = 5


  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
      
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @stand_alone OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @session_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable0 OUTPUT, @non_redeemable2_0 OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance  OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @initial_non_redeemable1 = @initial_non_redeemable0
  SET @non_redeemable2_1       = @non_redeemable2_0

  --
  -- AJQ 01-FEB-2012: Re-calculate the non-redeemable amount after a cash-in-while-playing.
  --
  IF ( @stand_alone = 1 AND @initial_non_redeemable0 > 0 AND ISNULL (@prize_lock, 0) = 0 )
  BEGIN
    --
    -- SAS HOST and Account with NR and No PrizeLock
    --
        SET @cash_in_while_playing = @account_balance + @session_cash_in

    IF ( @cash_in_while_playing > 0 )
    BEGIN
      --
          -- Player added money while playing (already on the session and/or in the account)
          --
      SET @total_balance = @total_cash_out + @account_balance

      -- Compute "money while playing" part
      SET @redeemable_from_cash_in = @cash_in_while_playing
      IF ( @redeemable_from_cash_in > @total_balance )
        SET @redeemable_from_cash_in = @total_balance

      -- Remainig Balance without the "money while playing" part
      SET @total_balance = @total_balance - @redeemable_from_cash_in

      --
      -- Split the "remaining" balance
      --
      EXECUTE dbo.PT_BalancePartsWhenPlaying @total_balance, @initial_cash_in, @initial_non_redeemable0, @non_redeemable2_0, @prize_lock, 
                                             @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT,
                                             @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT

      --
      -- Update the Initial Non Redeemable
      -- 
      SET @initial_non_redeemable1 = @part_nr1_out

      UPDATE   ACCOUNTS
         SET   AC_INITIAL_NOT_REDEEMABLE = @initial_non_redeemable1
       WHERE   AC_ACCOUNT_ID             = @account_id
      
    END
  END
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable0, @non_redeemable2_0, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT,
                                        @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT  -- not used in the IN part
  --
  -- If played greater than the refundable part, then recalculate NR2.
  --
  IF @played_amount > @part_refund_out
  BEGIN
    SET @non_redeemable2_1 = @non_redeemable2_0 - (@played_amount - @part_refund_out)
    SET @non_redeemable2_1 = CASE WHEN ( @non_redeemable2_1 < 0 ) THEN 0 ELSE @non_redeemable2_1 END
    
    UPDATE   ACCOUNTS
       SET   AC_NOT_REDEEMABLE = @non_redeemable2_1
     WHERE   AC_ACCOUNT_ID     = @account_id
  END

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable1, @non_redeemable2_1, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT,
                                         @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
  -- JML 07-DEC-2013: Not Block virtual accounts 
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | 0x0200  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id 
       AND AC_TYPE NOT IN (@account_virtual_cashier, @account_virtual_terminal) 
  END
    
  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @points_multiplier = ISNULL ( ( SELECT PV_POINTS_MULTIPLIER FROM PROVIDERS     WHERE PV_ID = (
                                      SELECT TE_PROV_ID           FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                      SELECT PS_TERMINAL_ID       FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @points_multiplier <= 0 )
    GOTO EXIT_PROCEDURE

  -- Apply multiplier
  SET @won_points = ROUND (@won_points * @points_multiplier, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,


  -- 11-FEB-2016 JRC bucket puntos de nivel
  -- Bucket MULTIPLE_BUCKETS_END_SESSION + PuntosNivel = 1102
  SET @bucket_puntos_nivel = 1102
  
  
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, @bucket_puntos_nivel, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @PlaySessionId, @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on PlaySession when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger]
ON [dbo].[play_sessions]
AFTER UPDATE
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @old_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @old_status   = (SELECT PS_STATUS FROM DELETED)
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @old_status = 0 AND @new_status <> 0 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger
GO

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on PlaySession when a handpay is registered.
--           Play sessions are directly inserted with status = 1 (Closed).
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger_Insert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
ON [dbo].[play_sessions]
AFTER INSERT
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @new_status = 1 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger_Insert
GO

/****** PERMISSIONS ******/
GRANT EXECUTE ON OBJECT::dbo.WSP_AccountBalanceParts TO wggui;
GO
GRANT EXECUTE ON OBJECT::dbo.WSP_BalanceParts TO wggui;
GO
