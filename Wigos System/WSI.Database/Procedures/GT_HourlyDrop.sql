﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_HourlyDrop]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_HourlyDrop]
GO

/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR CASHIER & GUI DROP HOURLY FOR GAMBLING TABLES

Version     Date          User              Description
----------------------------------------------------------------------------------------------------------------
1.0.0       26-APR-2017   JML&RAB&DHA       New procedure
1.0.1       05-MAY-2017   DHA               PBI 27175:Drop gaming table information - Global drop hourly report (Filter)
----------------------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[GT_HourlyDrop]
(
    @pGamingTableSessionId	BIGINT,
    @pDateFrom				      DATETIME,
    @pDateTo				        DATETIME,
    @pIsoCode				        NVARCHAR(3),
    @pSaleChipsMov			    INT,
    @pValCopyDealerMov		  INT,
    @pRedeemableChipsType   INT,
    @pGamingTablesId				NVARCHAR(MAX)
)
AS
BEGIN

  DECLARE @NationalCurrency NVARCHAR(3)
  DECLARE @_DELIMITER       NVARCHAR(1)
  DECLARE @_GAMING_TABLE    TABLE(SST_ID INT, SST_VALUE VARCHAR(50))
  DECLARE @old_time AS DATETIME

  DECLARE @Date AS DATETIME
  DECLARE @GTSessionId AS BIGINT
  DECLARE @IsoCode AS NVARCHAR(3)
  DECLARE @OpeningDate AS DATETIME
  DECLARE @ClosingDate AS DATETIME



    
  SET @old_time = '2007-01-01T07:00:00'
  SET     @_DELIMITER = ','

  INSERT INTO @_GAMING_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pGamingTablesId, @_DELIMITER, DEFAULT)

  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

  DECLARE   @t_Hour_Iso 
    TABLE ( TEMP_SESSION_ID  BIGINT,
            TEMP_DATETIME    DATETIME, 
            TEMP_ISO_CODE    VARCHAR(3));  

  IF @pGamingTableSessionId IS NOT NULL
  BEGIN
        SELECT   @pDateFrom = CONVERT(DATETIME, CONVERT(NVARCHAR(13), CS_OPENING_DATE, 21) + ':00:00', 21)
               , @pDateTo = ISNULL(CS_CLOSING_DATE, GETDATE())
          FROM   GAMING_TABLES_SESSIONS 
    INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
         WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId
  END
  ELSE
  BEGIN
    IF @pDateFrom IS NULL
    BEGIN
      SET @pDateFrom = CONVERT(DATETIME, 0)
    END
            
    IF @pDateTo IS NULL
    BEGIN
      SET @pDateTo = GETDATE()
    END
  END


  DECLARE ISO_CODES_CURSOR CURSOR FOR
           SELECT   DISTINCT GTS_GAMING_TABLE_SESSION_ID
                  , ISNULL(GTSC_ISO_CODE, @NationalCurrency)
                  , CS_OPENING_DATE
                  , ISNULL(CS_CLOSING_DATE, GETDATE())
             FROM   GAMING_TABLES_SESSIONS 
       INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
        LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
            WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId)
                                                OR  (@pGamingTableSessionId IS NULL AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
              AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTSC_ISO_CODE) 
              AND  ISNULL(GTSC_TYPE, @pRedeemableChipsType) = @pRedeemableChipsType
              AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(@pGamingTablesId) = 0 )
                                               
            UNION
                                   
           SELECT   DISTINCT GTPM_GAMING_TABLE_SESSION_ID
                  , GTPM_ISO_CODE
                  , CS_OPENING_DATE
                  , ISNULL(CS_CLOSING_DATE, GETDATE())
             FROM   GT_PLAYERTRACKING_MOVEMENTS 
       INNER JOIN   GAMING_TABLES_SESSIONS ON GTPM_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
       INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
            WHERE   ((@pGamingTableSessionId IS NOT NULL AND GTPM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId OR @pGamingTableSessionId IS NULL)
                                                OR  (@pGamingTableSessionId IS NULL AND CS_OPENING_DATE >= @pDateFrom AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
              AND   GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
              AND   (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTPM_ISO_CODE)
              AND   ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(ISNULL(@pGamingTablesId, '')) = 0 )


  OPEN ISO_CODES_CURSOR   
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate

  WHILE @@FETCH_STATUS = 0   
  BEGIN   
    
    SET @Date = @pDateFrom
    
    IF (@OpeningDate > @pDateFrom)
    BEGIN
		  SET @Date = DATEADD(HOUR, DATEDIFF(HOUR, @old_time, @OpeningDate), @old_time) 
    END
    
    WHILE @Date < @ClosingDate AND @Date < @pDateTo
    BEGIN
      IF @Date >= CONVERT(NVARCHAR(13), @OpeningDate, 21) + ':00:00' AND @Date <= @ClosingDate
      BEGIN
        INSERT INTO @t_Hour_Iso
               VALUES(@GTSessionId, CONVERT(NVARCHAR(13), @Date, 21) + ':00:00', @IsoCode)
      END
      SET @Date = DATEADD(HH, 1, @Date)
    END
      
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate  
  END  

  CLOSE ISO_CODES_CURSOR   
  DEALLOCATE ISO_CODES_CURSOR
  
     SELECT   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE
            , ISNULL(SUM(GTPM_VALUE), 0) AS DROP_AMOUNT
            , CS_NAME
            , GT_NAME
       FROM   @t_Hour_Iso
  LEFT JOIN   GT_PLAYERTRACKING_MOVEMENTS ON TEMP_ISO_CODE = GTPM_ISO_CODE
                                         AND TEMP_DATETIME = CONVERT(NVARCHAR(13), GTPM_DATETIME, 21) + ':00:00'
                                         AND GTPM_GAMING_TABLE_SESSION_ID = TEMP_SESSION_ID
                                         AND GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
 INNER JOIN   GAMING_TABLES_SESSIONS      ON TEMP_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
 INNER JOIN   CASHIER_SESSIONS            ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
 INNER JOIN   GAMING_TABLES               ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID
   GROUP BY   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE                            
            , CS_NAME
            , GT_NAME
   ORDER BY   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE
               
END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_HourlyDrop] TO [wggui] WITH GRANT OPTION
GO