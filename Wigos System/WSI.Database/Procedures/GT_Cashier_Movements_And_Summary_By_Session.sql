﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Cashier_Movements_And_Summary_By_Session]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
GO

/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR CASHIER MOVEMENTS AND SUMMARY FOR GAMING TABLE SESSION

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       23-FEB-2017   JML       New procedure
1.0.0       06-APR-2017   JML       Bug 26677:WIGOS-1042 - Gaming table Fixed banking the amount of template is duplicated after fill it
1.0.1       18-APR-2017   JML       Bug 28835:WIGOS-3768 Tables - The credit operation is showed in Fill line on gaming table report
1.0.2       13-SEP-2017   JML       Bug 29740:WIGOS-3996 Wrong informed data movements in Gambling Table report when reopening session
----------------------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
(
  @pGtSessionId                  BigInt,

  @pCageChipColor                VARCHAR(3),-- X02
  @pOpeningCash                  INT,  
  @pCageCloseSession             INT,
  @pCageFillerIn                 INT,
  @pCageFillerOut                INT,
  @pChipsSale                    INT,
  @pChipsPurchase                INT,
  @pChipsSaleDevolutionForTito   INT,
  @pChipsSaleWithCashIn          INT,
  @pChipsSaleRegisterTotal       INT,
  @pFillerInClosingStock         INT, 
  @pFillerOutClosingStock        INT,  
  @pReopenCashier                INT,
  
  @pOwnSalesAmount       Money,
  @pExternalSalesAmount  Money,
  @pCollectedAmount      Money,
  @pIsIntegratedCashier  Bit,

  @pDrop                 Money        OUTPUT
)
AS
BEGIN
  DECLARE @Columns           AS VARCHAR(MAX)
  DECLARE @NationalCurrency  AS VARCHAR(5)
  DECLARE @HasInitial        AS INT
           
  SET @HasInitial = 0
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
          
      SELECT   CS_SESSION_ID
             , MAX(CM_DATE)                    AS DATE_MOV
             , CM_TYPE                         AS TYPE_MOV
             , ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS ID_MOV_22
             , REPLICATE('0', 50 - LEN(CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)) 
               + REPLICATE('0', 50 - LEN(CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)) AS ID_MOV
             , SUM(CM_SUB_AMOUNT)                                                                                          AS SUB_AMOUNT
             , SUM(CASE CM_TYPE WHEN @pCageCloseSession THEN CM_FINAL_BALANCE ELSE CM_ADD_AMOUNT END)                      AS ADD_AMOUNT
             , SUM(CASE CM_TYPE WHEN @pCageCloseSession THEN CM_FINAL_BALANCE ELSE CM_ADD_AMOUNT END) - SUM(CM_SUB_AMOUNT) AS CALCULATED_AMOUNT
             ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency
                WHEN @pCageChipColor THEN ''
                ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE
             ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE
             ,  0 OPENER_MOV
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTS_INITIAL_CHIPS_AMOUNT AS INITIAL_CHIPS_AMOUNT
        INTO   #TABLE_MOVEMENTS
        FROM   GAMING_TABLES_SESSIONS
  INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
  INNER JOIN   CASHIER_MOVEMENTS AS CM WITH(INDEX(IX_CM_SESSION_ID)) ON CM_SESSION_ID = CS_SESSION_ID
   LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
  INNER JOIN   GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
         AND   CM_TYPE IN (@pOpeningCash,
                           ---@pFillerInClosingStock,
                           @pCageFillerIn,
                           @pCageFillerOut,
                           @pCageCloseSession,
                           @pFillerOutClosingStock,
                           @pChipsSale,
                           @pChipsPurchase,
                           @pChipsSaleDevolutionForTito,
                           @pChipsSaleWithCashIn,
                           @pChipsSaleRegisterTotal, 
                           @pReopenCashier)
    GROUP BY   CS_SESSION_ID
             , CM_TYPE
             , CCMR.CGM_MOVEMENT_ID
             , REPLICATE('0', 50 - LEN(cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)))) + cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)) 
               + REPLICATE('0', 50 - LEN(cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50)))) + cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50))
             , CM.CM_CURRENCY_ISO_CODE
             , CM.CM_CAGE_CURRENCY_TYPE
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTS_INITIAL_CHIPS_AMOUNT
    ORDER BY   MAX(CM_DATE) DESC
          
  DECLARE @cursor_id_mov AS nvarchar(255)

  DECLARE _cursor CURSOR FOR SELECT MIN(ID_MOV) 
                               FROM #TABLE_MOVEMENTS 
                              WHERE TYPE_MOV IN (@pCageFillerIn , @pFillerInClosingStock) 
                                AND INITIAL_CHIPS_AMOUNT > 0
                              GROUP BY CS_SESSION_ID
  
  OPEN _cursor  
  FETCH NEXT FROM _cursor INTO @cursor_id_mov
  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    UPDATE #TABLE_MOVEMENTS
       SET OPENER_MOV = 1
     WHERE ID_MOV = @cursor_id_mov
     
     SET @HasInitial = @HasInitial + 1

    FETCH NEXT FROM _cursor INTO @cursor_id_mov 
  END   
  CLOSE _cursor;  
  DEALLOCATE _cursor;  

  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')

  SELECT @Columns = COALESCE(@Columns + ',', '') +  '[' + CURRENCY_TYPE + ']'                             
    FROM   (SELECT   DISTINCT ISNULL(CE_CURRENCY_ORDER, -1) CE_CURRENCY_ORDER
                   , CASE WHEN CURRENCY_TYPE > 1000 
                          THEN 0
                          ELSE 1 END AS ORDER_2
                   , CASE WHEN CURRENCY_ISO_CODE = '' THEN 'X02' ELSE CURRENCY_ISO_CODE END 
                   + ' ' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_TYPE      
              FROM   #TABLE_MOVEMENTS 
              LEFT   JOIN CURRENCY_EXCHANGE ON CURRENCY_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
             WHERE   CURRENCY_ISO_CODE IS NOT NULL
           ) AS COLS
  ORDER   BY CE_CURRENCY_ORDER ASC
        , ORDER_2 ASC
        , CURRENCY_TYPE ASC

  SET @Columns = ISNULL(@Columns,COALESCE('[' + @NationalCurrency + ']',''))                   

  SELECT * FROM   #TABLE_MOVEMENTS   

  IF (@HasInitial = 0) 
  BEGIN
        INSERT INTO #TABLE_MOVEMENTS
          SELECT   CS_SESSION_ID
                 , cs_opening_date
                 , @pCageFillerIn 
                 , 0
                 , REPLICATE('0', 100) 
                 , 0
                 , 0
                 , 0
                 , @NationalCurrency 
                 , 0
                 , 1 OPENER_MOV
                 , 0
                 , 0
                 , 0
            FROM   GAMING_TABLES_SESSIONS
      INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
           WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
  END

   EXEC ('-- INITIAL AMOUNT
        SELECT   ''INITIAL_AMOUNT''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV = 1
               ) AS T1
        PIVOT (
                SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
              ) AS PVT
        UNION   ALL
         -- TOTAL FILL IN
        SELECT   ''TOTAL_FILL_IN''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END 
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV <> 1
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
         UNION   ALL
          -- TOTAL WITHDRAW
        SELECT   ''TOTAL_WITHDRAW'' AS NAME_MOV
               , *
          FROM (  SELECT   SUB_AMOUNT AS AMOUNT
                         , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                           + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                    FROM   #TABLE_MOVEMENTS
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
   ')
  
   DROP TABLE #TABLE_MOVEMENTS      

  SELECT @pDrop = dbo.GT_Calculate_DROP(@pOwnSalesAmount, @pExternalSalesAmount, @pCollectedAmount, @pIsIntegratedCashier) 
 
END
GO
  
GRANT EXECUTE ON [dbo].[GT_Cashier_Movements_And_Summary_By_Session] TO [WGGUI] WITH GRANT OPTION
GO
