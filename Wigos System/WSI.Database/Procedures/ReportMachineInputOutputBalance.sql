﻿
--------------------------------------------------------------------------------
-- Copyright © 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportMachineInputOutputBalance.sql
-- 
--   DESCRIPTION: Report for report machine input output balance
-- 
--        AUTHOR: Fernando Jiménez
-- 
-- CREATION DATE: 20-AUG-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 20-AUG-2015 FJC    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportMachineInputOutputBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportMachineInputOutputBalance]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Andreu Julià & Fernando Jiménez >
-- Create date: <20-AUG-2015>
-- Description:	<Pantalla de cuadratura de entradas y salidas de máquina>
-- =============================================

CREATE PROCEDURE [dbo].[ReportMachineInputOutputBalance]
  @pDateFrom        DATETIME,
  @pDateTo          DATETIME,
  @pShowTerminal    BIT,
  @pShowImbalance   BIT,
  @pTerminalWhere   NVARCHAR(MAX) 
  
AS
BEGIN  
  
  DECLARE @_QUERY   NVARCHAR(MAX)
  
  SET @_QUERY = 
  '
    SELECT 
          P.PV_NAME
        , TOTAL.* 
      FROM
      (
        SELECT'
  IF (@pShowTerminal = 1)  --GROUP BY TERMINAL
  BEGIN
    SET @_QUERY = @_QUERY + '         
              [ProviderID]                                                                               [ProviderID]                    
            , [TerminalName]                                                                             [TerminalName]                  
            , T.TerminalID                                                                               [TerminalID]                    
            , ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)						                             [SYS.Total.IN]                  
            , ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)                                  [SYS.Total.OUT]                 
	          , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 																	  
	             (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                               [SYS.Total.DIFF]                
            , ISNULL([FTToEGM],0)												                                                 [METER.Total.IN]                
            , ISNULL([FTFromEGM],0)											                                                 [METER.Total.OUT]               
	          , ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0)					                                     [METER.Total.DIFF]              
            , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)			           [DIFF.Total.IN]                 
            , (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0))	       [DIFF.Total.OUT]                
            , ((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 					  
               ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))	   [DIFF.Total]                    
            , ISNULL([SYSToEGM],0)                                                                       [SYSToEGM]                      
            , ISNULL([SYSToEGM.Cancel],0)                                                                [SYSToEGM.Cancel]
            , ISNULL([SYSFromEGM],0)                                                                     [SYSFromEGM]
            , ISNULL([SYSFromEGM.Abandoned],0)                                                           [SYSFromEGM.Abandoned]
            , ISNULL([SYSToEGM.Count],0)                                                                 [SYSToEGM.Count]
            , ISNULL([SYSFromEGM.Count],0)                                                               [SYSFromEGM.Count]
            , ISNULL([SYSFromEGM.Abandoned.Count],0)                                                     [SYSFromEGM.Abandoned.Count]
            , ISNULL([SYSToEGM.Cancel.Count],0)                                                          [SYSToEGM.Cancel.Count]
            , ISNULL([FTToEGM],0)                                                                        [FTToEGM]
            , ISNULL([FTFromEGM],0)                                                                      [FTFromEGM]
            , ISNULL([FTToEGM.Count],0)                                                                  [FTToEGM.Count]
            , ISNULL([FTFromEGM.Count],0)                                                                [FTFromEGM.Count]'

  END
  ELSE                  -- GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY +' 
              [ProviderID]
            , NULL                                                                                       [TerminalName]
            , NULL                                                                                       [TerminalID]
            , SUM(ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0))						                         [SYS.Total.IN]
            , SUM(ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                             [SYS.Total.OUT]
	          , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 															   
	                (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)))                            [SYS.Total.DIFF]
            , SUM(ISNULL([FTToEGM],0))												                                           [METER.Total.IN]
            , SUM(ISNULL([FTFromEGM],0))											                                           [METER.Total.OUT]
	          , SUM(ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0))					                                 [METER.Total.DIFF]
            , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0))			       [DIFF.Total.IN]
            , SUM((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))   [DIFF.Total.OUT]
            , SUM(((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 
                  ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))) [DIFF.Total] 
            , SUM(ISNULL([SYSToEGM],0))                                                                  [SYSToEGM]
            , SUM(ISNULL([SYSToEGM.Cancel],0))                                                           [SYSToEGM.Cancel]
            , SUM(ISNULL([SYSFromEGM],0))                                                                [SYSFromEGM]
            , SUM(ISNULL([SYSFromEGM.Abandoned],0))                                                      [SYSFromEGM.Abandoned]
            , SUM(ISNULL([SYSToEGM.Count],0))                                                            [SYSToEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Count],0))                                                          [SYSFromEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Abandoned.Count],0))                                                [SYSFromEGM.Abandoned.Count]
            , SUM(ISNULL([SYSToEGM.Cancel.Count],0))                                                     [SYSToEGM.Cancel.Count]
            , SUM(ISNULL([FTToEGM],0))                                                                   [FTToEGM]
            , SUM(ISNULL([FTFromEGM],0))                                                                 [FTFromEGM]
            , SUM(ISNULL([FTToEGM.Count],0))                                                             [FTToEGM.Count]
            , SUM(ISNULL([FTFromEGM.Count],0))                                                           [FTFromEGM.Count]'
  END  

SET @_QUERY = @_QUERY + '    
      FROM
      (
          SELECT    TE_TERMINAL_ID [TerminalID]
                  , TE_PROV_ID     [ProviderID]
                  , TE_PROVIDER_ID [ProviderNAME]
                  , TE_NAME        [TerminalName]
           FROM     TERMINALS
           WHERE    ' + @pTerminalWhere  + '
       ) T
       
       LEFT JOIN
       (   
        SELECT   AM_TERMINAL_ID   TERMINALID                                                       
               , SUM (CASE WHEN (AM_TYPE = 5 and (am_operation_id=0)) THEN ISNULL(am_sub_amount, 0) ELSE 0 END) [SYSToEGM]
               , SUM (CASE WHEN (AM_TYPE = 6 and (am_operation_id=0)) THEN ISNULL(am_add_amount, 0) ELSE 0 END) [SYSFromEGM]
               , SUM (CASE WHEN (AM_TYPE = 5 AND am_sub_amount > 0) THEN 1 ELSE 0 END)  [SYSToEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 6 AND am_add_amount > 0) THEN 1 ELSE 0 END)  [SYSFromEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 56) THEN ISNULL(am_add_amount, 0) ELSE 0 END)[SYSToEGM.Cancel]
               , SUM (CASE WHEN (AM_TYPE = 56 AND am_add_amount > 0) THEN 1 ELSE 0 END) [SYSToEGM.Cancel.Count]
          FROM   ACCOUNT_MOVEMENTS 
         WHERE   AM_TYPE IN (5,6, 56) -- StartCardSession = 5,EndCardSession = 6,CancelStartCardSession
           AND   AM_DATETIME  >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME)  
           AND   AM_DATETIME  <  CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME)  
      GROUP BY   AM_TERMINAL_ID
      ) S   ON   T.TERMINALID = S.TERMINALID 
      LEFT JOIN
      (
      SELECT   MSH_TERMINAL_ID                   [TerminalID] 
             , SUM(ISNULL(MSH_TO_GM_AMOUNT,0))   [FTToEGM]
             , SUM(ISNULL(MSH_FROM_GM_AMOUNT,0)) [FTFromEGM]
             , SUM(ISNULL(MSH_TO_GM_COUNT,0))    [FTToEGM.Count]
             , SUM(ISNULL(MSH_FROM_GM_COUNT,0))  [FTFromEGM.Count]
        FROM   MACHINE_STATS_PER_HOUR 
       WHERE   MSH_BASE_HOUR >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
         AND   MSH_BASE_HOUR < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
    GROUP BY   MSH_TERMINAL_ID
      ) M ON   T.TERMINALID = M.TERMINALID 
      LEFT JOIN 
      (
      SELECT    HP_TERMINAL_ID  
             ,  SUM (HP_AMOUNT) [SYSFROMEGM.ABANDONED]
             ,  SUM (1) [SYSFROMEGM.ABANDONED.COUNT]
       FROM     HANDPAYS 
      WHERE     HP_DATETIME >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_DATETIME < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_TYPE = 2
   GROUP BY     HP_TERMINAL_ID
     ) H ON     T.TERMINALID = HP_TERMINAL_ID'
      
  IF (@pShowTerminal = 0)  --GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY + '
      GROUP BY [ProviderID]'
  END

  SET @_QUERY = @_QUERY + '      
      ) TOTAL 
   
   INNER JOIN  PROVIDERS P 
           ON  P.PV_ID = TOTAL.PROVIDERID'
  
  
  IF (@pShowImbalance = 1) -- SHOW IMBALANCE
  BEGIN
    SET @_QUERY = @_QUERY + '
      WHERE [DIFF.Total] <> 0'
  END
  
  SET @_QUERY = @_QUERY + '
    ORDER BY 1'
    
  EXEC (@_QUERY)
 
END
  
GO

GRANT EXECUTE ON [dbo].[ReportMachineInputOutputBalance] TO [wggui] WITH GRANT OPTION

GO