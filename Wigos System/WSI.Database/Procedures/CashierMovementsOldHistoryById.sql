 --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsOldHistoryById.sql
-- 
--   DESCRIPTION: Historicized cashier movements by Session ID
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsOldHistoryById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsOldHistoryById]
GO

CREATE PROCEDURE [dbo].[CashierMovementsOldHistoryById]
	@ElementsToProcess INT OUTPUT
AS
BEGIN

 DECLARE @Cs_session_id AS BIGINT
 DECLARE @init_time AS DATETIME

 SET @init_time =  GETDATE()
 	
 SELECT TOP 1000 CS_SESSION_ID, CS_STATUS
 INTO #TmpCashierSessionsCursor
 FROM CASHIER_SESSIONS WITH(INDEX(IX_CS_HISTORY))
 WHERE CS_HISTORY = 0 
 ORDER BY CS_STATUS, CS_SESSION_ID DESC
 
 SELECT @ElementsToProcess = COUNT(CS_SESSION_ID) FROM #TmpCashierSessionsCursor
 SET	@ElementsToProcess = ISNULL(@ElementsToProcess,0)
 
 IF @ElementsToProcess <= 0
	RETURN
	
 DECLARE  CashierSessionsCursor CURSOR FOR 
 SELECT cs_session_id FROM #TmpCashierSessionsCursor

  OPEN CashierSessionsCursor
  FETCH NEXT FROM CashierSessionsCursor INTO @Cs_session_id 

  WHILE @@FETCH_STATUS = 0
  BEGIN	    
	EXEC CashierMovementsGroupedPerSessionId @Cs_session_id
	FETCH NEXT FROM CashierSessionsCursor INTO @Cs_session_id		
	--RETURN if more than one minute work
	IF (DATEDIFF(MI, @init_time, GETDATE()) >= 1)
		RETURN	
  END

  CLOSE CashierSessionsCursor
  DEALLOCATE CashierSessionsCursor	
  DROP TABLE #TmpCashierSessionsCursor
  
END --CashierMovementsOldHistoryById

