--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME : MassiveRemoval.sql
--
--   DESCRIPTION : Procedures for massive removal (plays, and 3GS movements and audit).
--
--        AUTHOR: Raul Ruiz
--
-- CREATION DATE: 10-JAN-2013
--
-- REVISION HISTORY :
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 10-JAN-2013 RRB    First release.
-- 19-FEB-2013 AJQ    Rewritten

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Delete Plays from AccountMovements
--
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MassiveRemoval_AccountMovementsPlays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MassiveRemoval_AccountMovementsPlays]
GO
CREATE PROCEDURE MassiveRemoval_AccountMovementsPlays
AS
BEGIN
  DECLARE @num_rows_to_delete   BIGINT
  DECLARE @start_time           DATETIME
  DECLARE @iter_start           DATETIME
  DECLARE @days_interval        INT
  DECLARE @iter_deleted         INT
  DECLARE @iter_elapsed         INT
  DECLARE @total_deleted        INT
  DECLARE @total_elapsed        INT
  DECLARE @date_to              DATETIME

  DECLARE @KEEP_DAYS            INT
  DECLARE @ITER_MIN_ROWS        INT
  DECLARE @ITER_MAX_ROWS        INT
  DECLARE @ITER_MIN_TIME        INT
  DECLARE @ITER_MAX_TIME        INT
  DECLARE @TOTAL_MAX_ROWS       INT
  DECLARE @ROW_DELTA            INT
  DECLARE @DELAY                NCHAR(10)
  DECLARE @TIMEOUT              INT
  DECLARE @max_play             DATETIME
  
  SET @KEEP_DAYS         =     200  --- Keep at least half year
  SET @ITER_MIN_ROWS     =    1000
  SET @ITER_MAX_ROWS     =   50000
  SET @TOTAL_MAX_ROWS   =  5000000 --- 5 Mill
  SET @ROW_DELTA        =      500
  SET @ITER_MIN_TIME     =     500
  SET @ITER_MAX_TIME     =    1000
  SET @DELAY             =  N'00:00:01'
  SET @TIMEOUT           =  30 * 60 * 1000

  SET @start_time    = GETDATE()
  SET @date_to       = DATEADD (DAY, -@KEEP_DAYS, dbo.TodayOpening (0)) 
  SET @num_rows_to_delete = @ITER_MIN_ROWS 
  SET @total_deleted = 0

  SET @iter_deleted = -1
 
  WHILE (@iter_deleted <> 0)
  BEGIN
    SET @iter_deleted = 0
    SET @iter_start   = GETDATE ()

    BEGIN TRAN
    --
    -- Delete statement
    --
    DELETE   TOP (@num_rows_to_delete)
      FROM   ACCOUNT_MOVEMENTS
     WHERE   AM_TYPE     = 0     -- Movement Play
       AND   AM_DATETIME < @date_to

     SET @iter_deleted  = @@ROWCOUNT

    COMMIT TRAN

    SET @iter_elapsed  = DATEDIFF(MILLISECOND, @iter_start, GETDATE())
    SET @total_elapsed = DATEDIFF(MILLISECOND, @start_time, GETDATE())
    SET @total_deleted = @total_deleted + @iter_deleted

    IF @total_elapsed >= @TIMEOUT       BREAK
    IF @total_deleted >= @TOTAL_MAX_ROWS BREAK

    IF @iter_elapsed < @ITER_MIN_TIME       SET @num_rows_to_delete = @num_rows_to_delete + @ROW_DELTA
    IF @iter_elapsed > @ITER_MAX_TIME       SET @num_rows_to_delete = @num_rows_to_delete - @ROW_DELTA
    IF @num_rows_to_delete < @ITER_MIN_ROWS SET @num_rows_to_delete = @ITER_MIN_ROWS
    IF @num_rows_to_delete > @ITER_MAX_ROWS SET @num_rows_to_delete = @ITER_MAX_ROWS

    WAITFOR DELAY @DELAY 
  END

END

GO

--------------------------------------------------------------------------------
-- PURPOSE: Delete Audit3GS
--
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MassiveRemoval_Audit3gs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MassiveRemoval_Audit3gs]
GO
CREATE PROCEDURE MassiveRemoval_Audit3gs
AS
BEGIN
  DECLARE @num_rows_to_delete   BIGINT
  DECLARE @start_time           DATETIME
  DECLARE @iter_start           DATETIME
  DECLARE @days_interval        INT
  DECLARE @iter_deleted         INT
  DECLARE @iter_elapsed         INT
  DECLARE @total_deleted        INT
  DECLARE @total_elapsed        INT
  DECLARE @date_to              DATETIME

  DECLARE @KEEP_DAYS            INT
  DECLARE @ITER_MIN_ROWS        INT
  DECLARE @ITER_MAX_ROWS        INT
  DECLARE @ITER_MIN_TIME        INT
  DECLARE @ITER_MAX_TIME        INT
  DECLARE @TOTAL_MAX_ROWS       INT
  DECLARE @ROW_DELTA            INT
  DECLARE @DELAY                NCHAR(10)
  DECLARE @TIMEOUT              INT
  DECLARE @max_play             DATETIME
  
  SET @KEEP_DAYS         =     200  --- Keep at least half year
  SET @ITER_MIN_ROWS     =    1000
  SET @ITER_MAX_ROWS     =   50000
  SET @TOTAL_MAX_ROWS   =  5000000 --- 5 Mill
  SET @ROW_DELTA        =      500
  SET @ITER_MIN_TIME     =     500
  SET @ITER_MAX_TIME     =    1000
  SET @DELAY             =  N'00:00:01'
  SET @TIMEOUT           =  60 * 60 * 1000

  SET @start_time    = GETDATE()
  SET @date_to       = DATEADD (DAY, -@KEEP_DAYS, dbo.TodayOpening (0)) 
  SET @num_rows_to_delete = @ITER_MIN_ROWS 
  SET @total_deleted = 0

  SET @iter_deleted = -1
 
  WHILE (@iter_deleted <> 0)
  BEGIN
    SET @iter_deleted = 0
    SET @iter_start   = GETDATE ()

    BEGIN TRAN
    --
    -- Delete statement
    --
    DELETE   TOP (@num_rows_to_delete)
      FROM   AUDIT_3GS
     WHERE   A3GS_EXECUTION < @date_to

     SET @iter_deleted  = @@ROWCOUNT

    COMMIT TRAN

    SET @iter_elapsed  = DATEDIFF(MILLISECOND, @iter_start, GETDATE())
    SET @total_elapsed = DATEDIFF(MILLISECOND, @start_time, GETDATE())
    SET @total_deleted = @total_deleted + @iter_deleted

    IF @total_elapsed >= @TIMEOUT       BREAK
    IF @total_deleted >= @TOTAL_MAX_ROWS BREAK

    IF @iter_elapsed < @ITER_MIN_TIME       SET @num_rows_to_delete = @num_rows_to_delete + @ROW_DELTA
    IF @iter_elapsed > @ITER_MAX_TIME       SET @num_rows_to_delete = @num_rows_to_delete - @ROW_DELTA
    IF @num_rows_to_delete < @ITER_MIN_ROWS SET @num_rows_to_delete = @ITER_MIN_ROWS
    IF @num_rows_to_delete > @ITER_MAX_ROWS SET @num_rows_to_delete = @ITER_MAX_ROWS

    WAITFOR DELAY @DELAY 
  END

END

GO

--------------------------------------------------------------------------------
-- PURPOSE: Delete Plays
--
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MassiveRemoval_Plays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MassiveRemoval_Plays]
GO
CREATE PROCEDURE MassiveRemoval_Plays
AS
BEGIN
  DECLARE @num_rows_to_delete   BIGINT
  DECLARE @start_time           DATETIME
  DECLARE @iter_start           DATETIME
  DECLARE @play_id              BIGINT
  DECLARE @days_interval        INT
  DECLARE @iter_deleted         INT
  DECLARE @iter_elapsed         INT
  DECLARE @total_deleted        INT
  DECLARE @total_elapsed        INT
  DECLARE @date_to              DATETIME

  DECLARE @KEEP_DAYS            INT
  DECLARE @ITER_MIN_ROWS        INT
  DECLARE @ITER_MAX_ROWS        INT
  DECLARE @ITER_MIN_TIME        INT
  DECLARE @ITER_MAX_TIME        INT
  DECLARE @TOTAL_MAX_ROWS       INT
  DECLARE @ROW_DELTA            INT
  DECLARE @DELAY                NCHAR(10)
  DECLARE @TIMEOUT              INT
  DECLARE @max_play             DATETIME
  
  SET @KEEP_DAYS         =     200  --- Keep at least half year
  SET @ITER_MIN_ROWS     =    1000
  SET @ITER_MAX_ROWS     =   50000
  SET @TOTAL_MAX_ROWS   =  5000000 --- 5 Mill
  SET @ROW_DELTA        =      500
  SET @ITER_MIN_TIME     =     500
  SET @ITER_MAX_TIME     =    1000
  SET @DELAY             =  N'00:00:01'
  SET @TIMEOUT           =  60 * 60 * 1000

  SET @start_time    = GETDATE()
  SET @date_to       = DATEADD (DAY, -@KEEP_DAYS, dbo.TodayOpening (0)) 
  SET @num_rows_to_delete = @ITER_MIN_ROWS 
  SET @total_deleted = 0

  -- Last PlayId to be deleted
  SET @play_id = ( SELECT   MAX(PL_PLAY_ID)
                     FROM   PLAYS WITH (INDEX(IX_PLAYS))
                    WHERE   PL_PLAY_SESSION_ID = (
                            SELECT   TOP 1 PS_PLAY_SESSION_ID
                              FROM   PLAY_SESSIONS WITH (INDEX(IX_PS_FINISHED_STATUS))
                             WHERE   PS_FINISHED      < @date_to
                               AND   PS_PLAYED_COUNT  > 0
                            ORDER BY PS_FINISHED DESC
                            )
                 )
  SET @play_id = ISNULL (@play_id, 0)

  SET @iter_deleted = -1
 
  WHILE (@iter_deleted <> 0)
  BEGIN
    SET @iter_deleted = 0
    SET @iter_start   = GETDATE ()

    BEGIN TRAN
    --
    -- Delete statement
    --
    DELETE   TOP (@num_rows_to_delete)
      FROM   PLAYS
     WHERE   PL_PLAY_ID <= @play_id

     SET @iter_deleted  = @@ROWCOUNT

    COMMIT TRAN

    SET @iter_elapsed  = DATEDIFF(MILLISECOND, @iter_start, GETDATE())
    SET @total_elapsed = DATEDIFF(MILLISECOND, @start_time, GETDATE())
    SET @total_deleted = @total_deleted + @iter_deleted

    IF @total_elapsed >= @TIMEOUT        BREAK
    IF @total_deleted >= @TOTAL_MAX_ROWS BREAK

    IF @iter_elapsed < @ITER_MIN_TIME       SET @num_rows_to_delete = @num_rows_to_delete + @ROW_DELTA
    IF @iter_elapsed > @ITER_MAX_TIME       SET @num_rows_to_delete = @num_rows_to_delete - @ROW_DELTA
    IF @num_rows_to_delete < @ITER_MIN_ROWS SET @num_rows_to_delete = @ITER_MIN_ROWS
    IF @num_rows_to_delete > @ITER_MAX_ROWS SET @num_rows_to_delete = @ITER_MAX_ROWS

    WAITFOR DELAY @DELAY 
  END
END

GO


USE [msdb]
GO

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'MassiveRemoval')
  EXEC msdb.dbo.sp_delete_job @job_name=N'MassiveRemoval', @delete_unused_schedule=1
GO


BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 02/19/2013 17:04:09 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'MassiveRemoval', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SingleStep]    Script Date: 02/19/2013 17:04:10 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SingleStep', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC MassiveRemoval_AccountMovementsPlays
EXEC MassiveRemoval_Audit3GS
EXEC MassiveRemoval_Plays
', 
		@database_name=N'wgdb_000', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily Massive Removal', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20130201, 
		@active_end_date=99991231, 
		@active_start_time=60000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

