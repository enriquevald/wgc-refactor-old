IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReceptionCustomerVisits]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[ReceptionCustomerVisits]
GO

CREATE PROCEDURE [dbo].[ReceptionCustomerVisits] @pStartDate BIGINT, @pPlayersOnSite BIT
AS BEGIN

DECLARE @_QUERY   NVARCHAR(MAX)

SET @_QUERY = 
  '         SELECT                                                                                                                          
                ACCOUNT_PHOTO.APH_PHOTO                                                                                                
               --,ACCOUNTS.AC_HOLDER_NAME   
               ,ACCOUNTS.AC_HOLDER_NAME

               ,NULL AS AC_HOLDER_NAME12
               
               ,CAST(CUSTOMER_VISITS.CUT_GENDER AS VARCHAR) CUT_GENDER                                                                 
               ,ACCOUNTS.AC_HOLDER_BIRTH_DATE                                                                                          
               ,ADIC_DATA.CUE_DOCUMENT_TYPE                                                                                            
               ,ADIC_DATA.CUE_DOCUMENT_NUMBER                                                                                          
               ,CAST(ACCOUNTS.AC_HOLDER_LEVEL AS VARCHAR)   AC_HOLDER_LEVEL                                                            
               ,ADIC_DATA.ENTRANCES_COUNT                                                                                              
               ,ADIC_DATA.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                              
               ,ADIC_DATA.CUE_ENTRANCE_DATETIME
               ,ACCOUNTS.AC_ACCOUNT_ID   
			   ,CUE_EXIT_DATETIME                                                                                                                              
         FROM   CUSTOMER_VISITS                                                                                                        
   INNER JOIN  ACCOUNTS ON     CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNTS.AC_ACCOUNT_ID                                                
    LEFT JOIN  ACCOUNT_PHOTO ON    CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNT_PHOTO.APH_ACCOUNT_ID                                      
                                                                                                                                       
                                                                                                                                       
   INNER JOIN                                                                                                                          
               (                                          
                  SELECT                                                                                                               
                           CUSTOMER_ENTRANCES.CUE_VISIT_ID                                                                             
                          ,CAST(CUSTOMER_ENTRANCES.CUE_DOCUMENT_TYPE AS VARCHAR) CUE_DOCUMENT_TYPE                                     
                          ,CUSTOMER_ENTRANCES.CUE_DOCUMENT_NUMBER                                                                      
                          ,CUSTOMER_ENTRANCES.CUE_TICKET_ENTRY_PRICE_PAID                                                              
                          ,Z.ENTRANCES_COUNT                                                                                           
                          ,Z.CUE_ENTRANCE_DATETIME                                                                                     
                          ,Z.SUM_CUE_TICKET_ENTRY_PRICE_PAID  
						  ,CUSTOMER_ENTRANCES.CUE_EXIT_DATETIME 				                                                                      
                    FROM                                                                                                               
                          CUSTOMER_ENTRANCES                                                                                           
              INNER JOIN (                                                                                                             
                           SELECT                                                                                                      
                                    MAX(CUE_ENTRANCE_DATETIME) AS CUE_ENTRANCE_DATETIME                                                
                                   ,CUE_VISIT_ID                                                                                       
                                   ,COUNT(*) AS ENTRANCES_COUNT                                                                        
                                   ,SUM(CUE_TICKET_ENTRY_PRICE_PAID) AS SUM_CUE_TICKET_ENTRY_PRICE_PAID
                             FROM                                                                                                      
                                   CUSTOMER_ENTRANCES                                                                                  
                            WHERE                                                                                                      
                                   CUSTOMER_ENTRANCES.CUE_VISIT_ID IN (                                                                
                                                                       SELECT                                                          
                                                                               CUT_VISIT_ID                                            
                                                                         FROM                                                          
                                                                               CUSTOMER_VISITS                                         
                                                                        WHERE                                                          
                                                                               CUT_GAMING_DAY = ' + CAST(@pStartDate AS VARCHAR(50)) + ' 
                                                          )                                                                
                           GROUP BY CUSTOMER_ENTRANCES.CUE_VISIT_ID                  
                           ) Z                                                                                                         
                      ON CUSTOMER_ENTRANCES.CUE_ENTRANCE_DATETIME = Z.CUE_ENTRANCE_DATETIME                                            
                         AND CUSTOMER_ENTRANCES.CUE_VISIT_ID = Z.CUE_VISIT_ID                                                          
               ) ADIC_DATA                                                                                                             
                                                                                                                                       
           ON CUSTOMER_VISITS.CUT_VISIT_ID = ADIC_DATA.CUE_VISIT_ID                                                                    
                                                                                                                                       
        WHERE  CUT_GAMING_DAY = ' + CAST(@pStartDate AS VARCHAR(50))

		IF (@pPlayersOnSite = 1)
		BEGIN
			SET @_QUERY = @_QUERY + ' AND CUE_EXIT_DATETIME IS NULL '
		END
                                                                                       
        SET @_QUERY = @_QUERY + ' ORDER BY ADIC_DATA.CUE_ENTRANCE_DATETIME '

		EXEC (@_QUERY)

END

GO

GRANT EXECUTE ON RECEPTIONCUSTOMERVISITS TO WGGUI

GO