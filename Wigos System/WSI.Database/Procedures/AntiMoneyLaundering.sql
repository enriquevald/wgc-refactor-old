--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AntiMoneyLaundering.sql
-- 
--   DESCRIPTION: Procedures for anti-money laundering
--
--        AUTHOR: Andreu Julià
-- 
-- CREATION DATE: 04-SEP-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2013 AJQ    First

USE [wgdb_000]
GO

--
--
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_AccountCurrentMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_AccountCurrentMonth]
GO
CREATE PROCEDURE [dbo].[AML_AccountCurrentMonth]
  @AccountId  BIGINT,
  @SplitA     MONEY OUTPUT,
  @Prizes     MONEY OUTPUT
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @_today as DATETIME
  DECLARE @_day_1 as DATETIME
  DECLARE @_date1  as DATETIME
  DECLARE @_track_data as NVARCHAR(50)
  DECLARE @_track_data_external as NVARCHAR(50)
  DECLARE @_group_by_trackdata as INT
  DECLARE @_level as INT
  DECLARE @_group_by_mode as INT

  SET @SplitA = 0
  SET @Prizes = 0

  SET @_track_data = NULL

  SET @_group_by_trackdata  = 0

  SELECT   @_group_by_mode = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS 
   WHERE   GP_GROUP_KEY   = 'AntiMoneyLaundering' 
     AND   GP_SUBJECT_KEY = 'GroupByMode'
     AND   ISNUMERIC(GP_KEY_VALUE) = 1
  SET @_group_by_mode = ISNULL (@_group_by_mode, 0)

  IF @_group_by_mode <> 0 
  BEGIN
    -- Trackdata is needed
    SET @_group_by_trackdata = 1

    SELECT   @_track_data  = AC_TRACK_DATA
           , @_level       = ISNULL(AC_HOLDER_LEVEL, 0)
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID = @AccountId

    IF @_track_data IS NULL RETURN

    SET @_track_data_external = dbo.TrackDataToExternal(@_track_data)
    IF @_track_data_external = ''
      SET @_track_data_external = @_track_data

    IF @_group_by_mode = 1 AND @_level > 0 SET @_group_by_trackdata = 0  -- Personal account, don't group by 'trackdata'
    IF @_group_by_mode = 2 AND @_level = 0 SET @_group_by_trackdata = 0  -- Anonymous account, don't group by 'trackdata'
  END

  SET @_today = dbo.TodayOpening(0)
  SET @_day_1 = DATEADD (DAY, 1 - DATEPART(DAY, @_today), @_today)

  SET @_date1 = NULL

  IF @_group_by_trackdata = 0 
  BEGIN
    SELECT   @_date1 = MAX(AMD_DAY), @SplitA = SUM(AMD_SPLIT_A), @Prizes = SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (IX_amd_account_day))
     WHERE   AMD_ACCOUNT_ID = @AccountId
       AND   AMD_DAY       >= @_day_1 
  END
  ELSE
  BEGIN
    SELECT   @_date1 = MAX(AMD_DAY), @SplitA = SUM(AMD_SPLIT_A), @Prizes = SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (IX_amd_account_day))
     WHERE   AMD_ACCOUNT_ID = @AccountId
       AND   AMD_DAY       >= @_day_1 
       AND   AMD_TRACK_DATA = @_track_data_external
  END

  IF @_date1 IS NULL 
  BEGIN 
    SET @_date1  = @_day_1
    SET @SplitA = 0
    SET @Prizes = 0
  END
  ELSE
  BEGIN
    SET @_date1  = DATEADD(DAY, 1, @_date1)
    SET @SplitA = ISNULL(@SplitA, 0)
    SET @Prizes = ISNULL(@Prizes, 0)
  END

  IF @_group_by_trackdata = 0 
  BEGIN
    SELECT   @SplitA = @SplitA
                     + isnull(sum(case when (am_type =  1) then am_add_amount else 0 end ), 0)
                     - isnull(sum(case when (am_type in (77, 1000)) then am_sub_amount + am_add_amount else 0 end ), 0),
             @Prizes = @Prizes + isnull(sum(case when (am_type =  2) then am_sub_amount else 0 end ), 0)
      FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
     WHERE   AM_DATETIME >= @_date1
       AND   AM_TYPE IN (1, 2, 77, 1000)
       AND   AM_ACCOUNT_ID = @AccountId
  END
  ELSE
  BEGIN
    SELECT   @SplitA = @SplitA
                     + isnull(sum(case when (am_type =  1) then am_add_amount else 0 end ), 0)
                     - isnull(sum(case when (am_type in (77, 1000)) then am_sub_amount + am_add_amount else 0 end ), 0),
             @Prizes = @Prizes + isnull(sum(case when (am_type =  2) then am_sub_amount else 0 end ), 0)
      FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
     WHERE   AM_DATETIME >= @_date1
       AND   AM_TYPE IN (1, 2, 77, 1000)
       AND   AM_ACCOUNT_ID = @AccountId
       AND   AM_TRACK_DATA = @_track_data_external
  END
END
GO

--
--
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_CreateDailyMonthly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_CreateDailyMonthly]
GO
CREATE PROCEDURE [dbo].[AML_CreateDailyMonthly] 
  @Day DATETIME
AS
BEGIN
  DECLARE @Date1 DATETIME
  DECLARE @Date2 DATETIME
  DECLARE @Count DATETIME

  SET NOCOUNT ON;

  -- Delete any previous row of the given day  
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  SELECT @Count = COUNT(*) FROM AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  IF @Count > 0 
  BEGIN
    DELETE AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  END

    -- Generate 'day'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  INSERT INTO AML_DAILY (AMD_DAY, AMD_ACCOUNT_ID, AMD_TRACK_DATA, AMD_SPLIT_A, AMD_PRIZE)
  (
    SELECT   DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)      
           , AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')                            
           , SUM(CASE WHEN (AM_TYPE =  1) THEN AM_ADD_AMOUNT ELSE 0 END )
           - SUM(CASE WHEN (AM_TYPE IN (77, 1000)) THEN AM_SUB_AMOUNT + AM_ADD_AMOUNT ELSE 0 END )           
           , SUM(CASE WHEN (AM_TYPE =  2) THEN AM_SUB_AMOUNT ELSE 0 END )      
      FROM   ACCOUNT_MOVEMENTS  WITH (INDEX (IX_am_datetime))
     WHERE   AM_DATETIME >= @Date1
       AND   AM_DATETIME <  @Date2
       AND   AM_TYPE IN (1, 2, 77, 1000)
    GROUP BY AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')
           , DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)
  )

  -- First day of the month at '00:00:00'
  SET @Date1 = DATEADD (DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SELECT @Count = COUNT(*) FROM AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  IF @Count > 0
  BEGIN
    DELETE AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  END

  -- First day of the month at 'Opening'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD(DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  INSERT INTO AML_MONTHLY (AMM_MONTH, AMM_ACCOUNT_ID, AMM_TRACK_DATA, AMM_SPLIT_A, AMM_PRIZE)
  (
    SELECT   @Date1
           , AMD_ACCOUNT_ID
           , AMD_TRACK_DATA
           , SUM(AMD_SPLIT_A)
           , SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (PK_aml_daily))
     WHERE   AMD_DAY >= @Date1
       AND   AMD_DAY <  DATEADD(MONTH, 1, @Date1)
    GROUP BY AMD_ACCOUNT_ID, AMD_TRACK_DATA
  )
END
GO

--
--
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_DailyWork]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_DailyWork]
GO
CREATE PROCEDURE [dbo].[AML_DailyWork] 
AS
BEGIN
  DECLARE @_date0 DATETIME
  DECLARE @_date1 DATETIME
  DECLARE @_start DATETIME

  SET @_start = GETDATE ()
  SET @_date1 = (SELECT MAX(AMD_DAY) FROM AML_DAILY WITH (INDEX (PK_aml_daily)))
  SET @_date1 = DATEADD (DAY, 1, @_date1)

  IF @_date1 IS NULL 
  BEGIN
    SET @_date0 = dbo.Opening(0, '2013-07-01T23:59:59')  --- First Day 'AntiLavado'
    SET @_date1 = (SELECT   dbo.Opening(0, MIN(AM_DATETIME)) 
                     FROM   ACCOUNT_MOVEMENTS
                    WHERE   AM_DATETIME >= @_date0)
    IF @_date1 IS NULL RETURN
  END

  SET @_date0 = (SELECT dbo.TodayOpening(0))

  WHILE (@_date1 < @_date0)
  BEGIN
    EXEC AML_CreateDailyMonthly @_date1
    SET @_date1 = @_date1 + 1
    IF DATEDIFF (SECOND, @_start, GETDATE()) > 60 RETURN
  END
END
GO