CREATE PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_UpdateCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_StartCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint] OUTPUT,
	@PlayableBalance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_StartCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_SendEvent]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@EventId [bigint],
	@Amount [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_SendEvent]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_EndCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_EndCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_AccountStatus]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@MachineNumber [int],
	@Balance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_AccountStatus]
GO


