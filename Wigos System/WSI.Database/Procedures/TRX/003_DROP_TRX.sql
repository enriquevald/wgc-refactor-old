IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_UpdateCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_StartCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_StartCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_SendEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_SendEvent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_EndCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_EndCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_AccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_AccountStatus]
GO