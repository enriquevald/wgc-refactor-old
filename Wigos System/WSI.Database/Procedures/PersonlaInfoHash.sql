 --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: PersonlaInfoHash.sql
-- 
--   DESCRIPTION: Procedures for Hash of Personal info 
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 22-MAY-2013
-- 
-- REVISION HISTORY: 
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 22-MAY-2013 JML    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonlaInfoHash]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PersonlaInfoHash]
GO
CREATE PROCEDURE [dbo].[PersonlaInfoHash]
    @TableHash   NVARCHAR(50) 
  , @AccountId   BIGINT
  , @SelectHash  NVARCHAR(MAX) OUTPUT 
AS
BEGIN

  SET @SelectHash = N'SELECT   AC_ACCOUNT_ID
                             , HASHBYTES (''SHA1'',  ISNULL(AC_TRACK_DATA,        '''')
                                                   + ISNULL(AC_HOLDER_NAME,       '''')
                                                   + ISNULL(AC_HOLDER_ID,         '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '''')
                                                   + ISNULL(AC_HOLDER_ADDRESS_01, '''')
                                                   + ISNULL(AC_HOLDER_ADDRESS_02, '''')
                                                   + ISNULL(AC_HOLDER_ADDRESS_03, '''')
                                                   + ISNULL(AC_HOLDER_CITY,       '''')
                                                   + ISNULL(AC_HOLDER_ZIP,        '''')
                                                   + ISNULL(AC_HOLDER_EMAIL_01,   '''')
                                                   + ISNULL(AC_HOLDER_EMAIL_02,   '''')
                                                   + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '''')
                                                   + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '''')
                                                   + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '''')
                                                   + ISNULL(AC_HOLDER_COMMENTS,        '''')
                                                   + ISNULL(AC_HOLDER_ID1, '''')
                                                   + ISNULL(AC_HOLDER_ID2, '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '''')
                                                   + ISNULL(AC_HOLDER_NAME1, '''')
                                                   + ISNULL(AC_HOLDER_NAME2, '''')
                                                   + ISNULL(AC_HOLDER_NAME3, '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '''')
                                                   + ISNULL(AC_HOLDER_TITLE                        , '''')
                                                   + ISNULL(AC_HOLDER_NAME4                        , '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '''')
                                                   + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '''')
                                                   + ISNULL(AC_HOLDER_STATE                        , '''')
                                                   + ISNULL(AC_HOLDER_COUNTRY                      , '''') 
                                                   + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '''')
                                                   + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '''') ) 
                   FROM   ' + @TableHash + N'
                  WHERE   AC_TRACK_DATA NOT LIKE ''%-NEW-%'' '
  If (isnull(@AccountId, 0) > 0)
  BEGIN
    SET @SelectHash = @SelectHash + N'AND   AC_ACCOUNT_ID = ' + CONVERT(NVARCHAR, @AccountId)
  END

END -- PersonlaInfoHash