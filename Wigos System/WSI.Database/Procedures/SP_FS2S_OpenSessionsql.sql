﻿--------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SP_FS2S_OpenSession.sql
-- 
--   DESCRIPTION: Procedure for FS2S Open session
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 18-SEP-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 18-SEP-2017 ETP    First release.
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FS2S_OpenSession]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[SP_FS2S_OpenSession]
END

GO

CREATE  PROCEDURE [dbo].[SP_FS2S_OpenSession]
AS
BEGIN

 SELECT   SUM(ISNULL(AC_RE_RESERVED,0))*100                            AS TotalValueAccounts
        , 0.0                                                          AS TotalValueAccountsPromo 
   FROM   ACCOUNTS
   
END

GO

GRANT EXECUTE ON [dbo].[SP_FS2S_OpenSession] TO [wggui] WITH GRANT OPTION 

GO