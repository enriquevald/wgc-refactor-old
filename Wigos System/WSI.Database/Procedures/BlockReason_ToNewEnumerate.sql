CREATE FUNCTION BlockReason_ToNewEnumerate
(@pBlockReason INT) returns INT
AS
BEGIN
	DECLARE @Result INT
	
    IF @pBlockReason > 0 AND @pBlockReason < 256
              SET @Result  = POWER(2, @pBlockReason + 7) -- 2^(AC_BLOCK_REASON-1) x 256
    ELSE 
              SET @Result = @pBlockReason
              
	RETURN @Result
END -- BlockReason_ToNewEnumerate
GO

GRANT EXECUTE ON [dbo].[BlockReason_ToNewEnumerate] TO [wggui] WITH GRANT OPTION 