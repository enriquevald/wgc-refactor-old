--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: Logrand_SP.sql
--   DESCRIPTION: Creates a set of stored procedures for Logrand
--                  wsp_001_corte_por_caja
--                  wsp_001_ticket_por_ticket
--                  wsp_001_ficha_cliente
--                  wsp_001_clientes_movimientos_mayores
--        AUTHOR: Quim Morales
-- CREATION DATE: 23-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 23-SEP-2014 QMP    First release.
-- 15-JAN-2015 JMV    Modifications in wsp_001_ficha_cliente and wsp_001_account_points_query
-- 21-JAN-2015 JMV    Fixed Bug WIG-1952: Incompatible con SQL Server 2005
--------------------------------------------------------------------------------
USE [wgdb_000]
GO


--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: Corte_por_caja.sql
--   DESCRIPTION: Returns a summary of a day cash status 
--        AUTHOR: Joan Marc Pepi�
-- CREATION DATE: 29-AUG-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 29-AUG-2014 JPJ    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_corte_por_caja]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_corte_por_caja]
GO

CREATE PROCEDURE [dbo].[wsp_001_corte_por_caja]        
  @v_FechaINI DATE
AS
BEGIN 
  DECLARE @pStartGameDay                        DATETIME
  DECLARE @pEndGameDay                          DATETIME
  DECLARE @CASH_IN                              INT 
  DECLARE @TAX_ON_PRIZE1                        INT 
  DECLARE @PRIZES                               INT 
  DECLARE @CARD_IN                              INT 
  DECLARE @TAX_ON_PRIZE2                        INT 
  DECLARE @CARD_REPLACEMENT                     INT 
  DECLARE @CASH_IN_SPLIT2                       INT 
  DECLARE @MB_CASH_IN_SPLIT2                    INT 
  DECLARE @DEV_SPLIT2                           INT 
  DECLARE @CASH_IN_SPLIT1                       INT 
  DECLARE @DEV_SPLIT1                           INT 
  DECLARE @MB_CASH_IN_SPLIT1                    INT 
  DECLARE @NA_CASH_IN_SPLIT1                    INT 
  DECLARE @NA_CASH_IN_SPLIT2                    INT 
  DECLARE @TAX_RETURNING_ON_PRIZE_COUPON        INT 
  DECLARE @CURRENCY_EXCHANGE_SPLIT1             INT 
  DECLARE @CURRENCY_CARD_EXCHANGE_SPLIT1        INT 
  DECLARE @CURRENCY_EXCHANGE_SPLIT2             INT 
  DECLARE @CURRENCY_CARD_EXCHANGE_SPLIT2        INT 
  DECLARE @POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE INT 
  DECLARE @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 INT 
  DECLARE @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1  INT 
  DECLARE @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 INT 
  DECLARE @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2  INT 
  
  SET @CASH_IN                               =   4
  SET @TAX_ON_PRIZE1                         =   6
  SET @PRIZES                                =   8
  SET @CARD_IN                               =   9
  SET @TAX_ON_PRIZE2                         =  14
  SET @CARD_REPLACEMENT                      =  28
  SET @CASH_IN_SPLIT2                        =  34
  SET @MB_CASH_IN_SPLIT2                     =  35
  SET @DEV_SPLIT2                            =  36
  SET @CASH_IN_SPLIT1                        =  37
  SET @DEV_SPLIT1                            =  38
  SET @MB_CASH_IN_SPLIT1                     =  39
  SET @NA_CASH_IN_SPLIT1                     =  54
  SET @NA_CASH_IN_SPLIT2                     =  55
  SET @TAX_RETURNING_ON_PRIZE_COUPON         =  69
  SET @CURRENCY_EXCHANGE_SPLIT1              =  78
  SET @CURRENCY_CARD_EXCHANGE_SPLIT1         =  79
  SET @CURRENCY_EXCHANGE_SPLIT2              =  87
  SET @CURRENCY_CARD_EXCHANGE_SPLIT2         =  88
  SET @POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE  = 143
  SET @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1  = 147
  SET @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1   = 148
  SET @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2  = 149
  SET @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2   = 150

  SET @pStartGameDay = dbo.Opening(0, DATEADD(DAY, 1, @v_FechaINI))
  SET @pEndGameDay = DATEADD(DAY, 1, @pStartGameDay)

  SELECT   @v_FechaINI                               'Fecha'
         , CS_SESSION_ID                             'Shift'
         , GU_FULL_NAME                              'Cajera'
         , MontoEfectivo                             'MontoEfectivo'
         , MontoDolares                              'MontoDolares'
         , MontoTDC                                  'MontoTDC'
         , Vouchers
         , VouchersRECEIVED
         , VouchersNONEG
         , TotalIngresos                             'TotalIngresos'
         , Devoluciones                              'Devoluciones'
         , PremiosBruto - (RetencionF + RetencionE)  'Premios' 
         , RetencionF                                'RetencionF'
         , RetencionE                                'RetencionE' 
    FROM (
          SELECT   CS_SESSION_ID
                 , CS_USER_ID
                 , SUM(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT1, @MB_CASH_IN_SPLIT2,
                                              @CASH_IN_SPLIT1,    @CASH_IN_SPLIT2, 
                                              @NA_CASH_IN_SPLIT1, @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                            WHEN CM_TYPE IN ( @CURRENCY_EXCHANGE_SPLIT1, @CURRENCY_EXCHANGE_SPLIT2 ) THEN -CM_FINAL_BALANCE
                            ELSE 0
                       END) 'MontoEfectivo'
                 , SUM(CASE WHEN CM_TYPE IN ( @CURRENCY_EXCHANGE_SPLIT1 ) AND CM_CURRENCY_ISO_CODE = 'USD' THEN CM_INITIAL_BALANCE ELSE 0 END) 'MontoDolares'
                 , SUM(CASE WHEN CM_TYPE IN ( @CURRENCY_CARD_EXCHANGE_SPLIT1,        @CURRENCY_CARD_EXCHANGE_SPLIT2, 
                                              @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1, @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1,  
                                              @CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2, @CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 ) THEN CM_INITIAL_BALANCE ELSE 0 END) 'MontoTDC' 
                 , NULL 'Vouchers'
                 , NULL 'VouchersRECEIVED'
                 , NULL 'VouchersNONEG'
                 , SUM(
                      CASE WHEN CM_TYPE IN ( @CASH_IN, @CARD_IN, @CARD_REPLACEMENT) THEN CM_ADD_AMOUNT 
                           WHEN CM_TYPE IN ( @CURRENCY_EXCHANGE_SPLIT1 )            THEN CM_SUB_AMOUNT
                           ELSE 0
                      END)                                                                            'TotalIngresos'
                 , SUM(CASE WHEN CM_TYPE IN(@DEV_SPLIT1, @DEV_SPLIT2) THEN CM_SUB_AMOUNT ELSE 0 END)  'Devoluciones'
                 , SUM(
                     CASE WHEN CM_TYPE IN(@PRIZES, @TAX_RETURNING_ON_PRIZE_COUPON, @POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE) THEN CM_SUB_AMOUNT
                          ELSE 0
                     END)                                                                             'PremiosBruto' 
                 , SUM(CASE WHEN CM_TYPE IN ( @TAX_ON_PRIZE1 ) THEN CM_ADD_AMOUNT ELSE 0 END)         'RetencionF'
                 , SUM(CASE WHEN CM_TYPE IN ( @TAX_ON_PRIZE2 ) THEN CM_ADD_AMOUNT ELSE 0 END)         'RetencionE'  
            FROM   CASHIER_SESSIONS
           INNER   JOIN CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID ON CS_SESSION_ID = CM_SESSION_ID 
           WHERE   CS_STATUS = 1
             AND   CS_OPENING_DATE >= @pStartGameDay
             AND   CS_OPENING_DATE <  @pEndGameDay
        GROUP BY   CS_SESSION_ID, CS_USER_ID
      ) CashierSummaryBySession
        INNER JOIN GUI_USERS ON GU_USER_ID = CS_USER_ID
END -- wsp_001_corte_por_caja
GO

GRANT EXECUTE ON [dbo].[wsp_001_corte_por_caja] TO [wg_datareader] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: Resumen_ticketxticket.sql
--   DESCRIPTION: Returns ticket information
--        AUTHOR: Joan Marc Pepi�
-- CREATION DATE: 29-AUG-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 29-AUG-2014 JPJ    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_ticket_por_ticket]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_ticket_por_ticket]
GO

CREATE PROCEDURE [DBO].[wsp_001_ticket_por_ticket]
 (  
    @v_FolioAnterior  BIGINT
  , @v_CuantosMas        INT
 )
AS
BEGIN
  DECLARE @CashIn_A                             INT 
  DECLARE @CashOut_A                            INT 
  DECLARE @CASH_IN                              INT 
  DECLARE @PRIZES                               INT 
  DECLARE @CARD_IN                              INT 
  DECLARE @CARD_REPLACEMENT                     INT 
  DECLARE @DEV_SPLIT1                           INT 
  DECLARE @CANCEL_SPLIT1                        INT 
  DECLARE @CURRENCY_EXCHANGE_SPLIT1             INT 
  
  SET @CashIn_A                              =   1
  SET @CashOut_A                             =   3
  SET @CASH_IN                               =   4
  SET @PRIZES                                =   8
  SET @CARD_IN                               =   9
  SET @CARD_REPLACEMENT                      =  28
  SET @DEV_SPLIT1                            =  38  
  SET @CANCEL_SPLIT1                         =  40
  SET @CURRENCY_EXCHANGE_SPLIT1              =  78
        
    SELECT   CM_OPERATION_ID  'Tmp_Operation_Id'
           , SUM(CASE WHEN CM_TYPE IN(@DEV_SPLIT1, @CANCEL_SPLIT1, @PRIZES) THEN CM_SUB_AMOUNT ELSE 0 END) 'Credit'
           ,                   SUM(
                      CASE WHEN CM_TYPE IN ( @CASH_IN, @CARD_IN, @CARD_REPLACEMENT) THEN CM_ADD_AMOUNT 
                           WHEN CM_TYPE IN ( @CURRENCY_EXCHANGE_SPLIT1 )            THEN CM_SUB_AMOUNT
                           ELSE 0
                      END)     'Debit'
           , SUM(CASE WHEN CM_TYPE IN( @DEV_SPLIT1, @CANCEL_SPLIT1 )                THEN CM_SUB_AMOUNT ELSE 0 END) 'Devolucion'
           , SUM(CASE WHEN CM_TYPE IN( @PRIZES )                                    THEN CM_SUB_AMOUNT ELSE 0 END) 'Premio'
      INTO   #TEMP_CASHIER_MOVEMENTS    
      FROM   CASHIER_MOVEMENTS
  GROUP BY   CM_OPERATION_ID

    SELECT   TOP (@v_CuantosMas) 
             CV_VOUCHER_ID                           'Movimiento_id'
           , dbo.Opening(0, CV_DATETIME)             'Fecha'
           , CV_DATETIME                             'FechaHora'
           , CV_VOUCHER_ID                           'Folio1'
           , NULL                                    'Folio2'
           , CASE WHEN CV_TYPE IN (@CashIn_A)   THEN 'Carga'
                  WHEN CV_TYPE IN (@CashOut_A)  THEN 'Pago'
                                                ELSE ''                                    
             END                                     'Tipo_transaccion'
           , Credit                                  'Credit'
           , Debit                                   'Debit'
           , Devolucion                              'Devolucion'
           , Premio                                  'Premio'
           , dbo.TrackDataToExternal(AC_TRACK_DATA)  'TarjetaCliente'
           , AC_ACCOUNT_ID                           'Cliente_id'
           , AC_HOLDER_NAME3 + ' ' + AC_HOLDER_NAME4 'Nombres'
           , AC_HOLDER_NAME1 + ' ' + AC_HOLDER_NAME2 'Apellidos'
           , CV_SESSION_ID                           'Shift_id'
           , CV_USER_ID                              'Cajera_id'
           , GU_FULL_NAME                            'CajeraNombres'
           , NULL                                    'CajeraApellidos'
           , CV_CASHIER_ID                           'Caja_id'
           , CV_CASHIER_NAME                         'Caja_Name'
      FROM CASHIER_VOUCHERS
           INNER JOIN GUI_USERS ON GU_USER_ID    = CV_USER_ID
           INNER JOIN ACCOUNTS  ON AC_ACCOUNT_ID = CV_ACCOUNT_ID
           INNER JOIN #TEMP_CASHIER_MOVEMENTS ON Tmp_Operation_Id = CV_OPERATION_ID
     WHERE CV_VOUCHER_ID >= @v_FolioAnterior
           AND CV_TYPE IN (@CashIn_A, @CashOut_A)
  ORDER BY MOVIMIENTO_ID

  DROP TABLE #TEMP_CASHIER_MOVEMENTS
  
END -- wsp_001_ticket_por_ticket
GO

GRANT EXECUTE ON [dbo].[wsp_001_ticket_por_ticket] TO [wg_datareader] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: FICHA_CLIENTE.sql
--   DESCRIPTION: Returns client information
--        AUTHOR: Joan Marc Pepi�
-- CREATION DATE: 29-AUG-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 29-AUG-2014 JPJ    First release.
-- 04-NOV-2014 OPC    If both parameters are informed, check they match the same account
-- 15-JAN-2015 JMV    DUT: Logrand MKT19 - Interfaz Micros-WIGOS: Ajuste Stored Procedures
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_ficha_cliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_ficha_cliente]
GO

CREATE PROCEDURE [DBO].[wsp_001_ficha_cliente]
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_status INT
  DECLARE @_points MONEY
  DECLARE @_level INT
  DECLARE @_level_name NVARCHAR(50)
  DECLARE @_tbl_select TABLE (StatusSelect INT, BalancePoints MONEY)       

  SET @_account_id = @v_Cliente_Id

  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
  END

  IF @v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id
  BEGIN
    SET @_account_id = NULL
  END
  
  -- Obtain account points
  INSERT INTO   @_tbl_select
  EXECUTE   dbo.wsp_001_account_points_query NULL, @_account_id

  SELECT   @_status = StatusSelect
         , @_points = BalancePoints
  FROM     @_tbl_select
    
  -- Obtain level name from holder level
  SET @_level = ( SELECT AC_HOLDER_LEVEL 
                  FROM ACCOUNTS 
                  WHERE AC_ACCOUNT_ID = @_account_id )
                  
  SET @_level_name = ( SELECT GP_KEY_VALUE
                     FROM GENERAL_PARAMS
                     WHERE GP_SUBJECT_KEY LIKE 'Level%' + CAST(@_level AS NVARCHAR) + '.Name' )

  SELECT    AC_CREATED           'FechaAlta'
          , AC_HOLDER_BIRTH_DATE 'FechaNacimiento'          
          , AC_HOLDER_NAME3      'NombreCliente '
          , AC_HOLDER_NAME1      'Apellido1'
          , AC_HOLDER_NAME2      'Apellido2'
          , AC_HOLDER_LEVEL      'Nivel'
          , @_level_name         'NombreNivel'
          , AC_RE_BALANCE + AC_PROMO_RE_BALANCE 'BalanceCreditosRedimibles'
          , AC_PROMO_NR_BALANCE  'BalanceCreditosNoRedimibles'
          , AC_BALANCE           'BalanceCreditosTotal'
          , @_points             'BalancePuntos'
          , @_status             'EstadoPuntos'    
            
     FROM   ACCOUNTS
    WHERE   AC_ACCOUNT_ID = @_account_id
  
END -- wsp_001_ficha_cliente
GO

GRANT EXECUTE ON [dbo].[wsp_001_ficha_cliente] TO [wg_datareader] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: Resumen_movimientos_mayores.sql
--   DESCRIPTION: Returns transactions with determinate amounts
--        AUTHOR: Joan Marc Pepi�
-- CREATION DATE: 29-AUG-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 29-AUG-2014 JPJ    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_clientes_movimientos_mayores]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_clientes_movimientos_mayores]
GO

CREATE PROCEDURE [DBO].[wsp_001_clientes_movimientos_mayores]
 (  
    @v_FechaINI                 DATE,
    @v_FechaFIN                 DATE,
    @v_TotalCargado             MONEY
 )
AS
BEGIN

  -- Operation Codes
  -- CashIn operations
  DECLARE @CASH_IN                    INT 
  DECLARE @PROMOTION                  INT 
  DECLARE @MB_CASH_IN                 INT 
  DECLARE @MB_PROMOTION               INT 
  DECLARE @NA_CASH_IN                 INT  
  DECLARE @NA_PROMOTION               INT 
  -- CashOut operations
  DECLARE @CASH_OUT                   INT 
  DECLARE @_Fecha_Ini                 DATETIME
  DECLARE @_Fecha_Fin                 DATETIME
  
    -- CashIn operations
  SET @CASH_IN                     =   1
  SET @PROMOTION                   =   3
  SET @MB_CASH_IN                  =   4
  SET @MB_PROMOTION                =   5
  SET @NA_CASH_IN                  = 109 
  SET @NA_PROMOTION                = 110
  -- CashOut operations
  SET @CASH_OUT                    =   2
 
  

  SET @_Fecha_Ini = dbo.Opening(0, DATEADD(DAY, 1, @v_FechaINI))
  SET @_Fecha_Fin = dbo.Opening(0, DATEADD(DAY, 1, @v_FechaFIN))

  SELECT   AC_ACCOUNT_ID                          'Cliente_id'
         , dbo.TrackDataToExternal(AC_TRACK_DATA) 'TarjetaCliente'
         , AC_HOLDER_NAME                         'NombreCliente'
         , CV_VOUCHER_ID                          'Folio1'
         , AO_DATETIME                            'FechaHora'
         , dbo.Opening(0, AO_DATETIME)            'Fecha'
         , CASE WHEN AO_CODE IN ( @CASH_IN, @PROMOTION, @MB_CASH_IN, @MB_PROMOTION, @NA_CASH_IN, @NA_PROMOTION ) THEN AO_AMOUNT
                ELSE 0
           END                                    'Cargado'
         , CASE WHEN AO_CODE IN ( @CASH_OUT ) THEN AO_AMOUNT
                ELSE 0
           END                                    'Pagado'              
    FROM   ACCOUNT_OPERATIONS WITH(INDEX(IX_ao_code_date_account)) 
           INNER JOIN ACCOUNTS ON AC_ACCOUNT_ID =  AO_ACCOUNT_ID
           INNER JOIN CASHIER_VOUCHERS WITH(INDEX(IX_cv_operation_id_datetime)) ON CV_OPERATION_ID = AO_OPERATION_ID AND CV_TYPE IN (1, 3)
   WHERE   AO_AMOUNT    >  @v_TotalCargado
     AND   AO_DATETIME  >= @_Fecha_Ini 
     AND   AO_DATETIME  <  @_Fecha_Fin 
     AND   ISNULL(AO_UNDO_STATUS, 0) = 0           -- Only take in account Operations with UNDO_STATUS = None
     AND   AO_CODE IN (
                       @CASH_IN, @PROMOTION, 
                       @MB_CASH_IN, @MB_PROMOTION, 
                       @NA_CASH_IN, @NA_PROMOTION,
                       @CASH_OUT
                      )
END -- wsp_001_clientes_movimientos_mayores
GO

GRANT EXECUTE ON [dbo].[wsp_001_clientes_movimientos_mayores] TO [wg_datareader] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------  
-- Copyright � 2014 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_constancias
--   DESCRIPTION: Returns account major prizes between two dates.
--        AUTHOR: Omar P�rez Ciurana
-- CREATION DATE: 17-OCT-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 17-OCT-2014 OPC    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_constancias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_constancias]
GO

CREATE PROCEDURE [DBO].[wsp_001_constancias]
 (
   @v_FechaINI DATE,
   @v_FechaFIN DATE
 )
AS
BEGIN

  -- Members
  DECLARE @_fecha_ini  DATETIME
  DECLARE @_fecha_fin  DATETIME

  -- Set Members
  SET @_fecha_ini = dbo.Opening(0, DATEADD(DAY, 1, @v_FechaINI))  
  SET @_fecha_fin = dbo.Opening(0, DATEADD(DAY, 1, @v_FechaFIN))

  -- Query Select
  SELECT   AMP_OPERATION_ID          'Identificador'
         , AMP_DATETIME              'FechaHora'
         , AMP_ACCOUNT_ID            'IdCuenta'
         , AMP_PLAYER_NAME3          'Nombre'
         , AMP_PLAYER_NAME1          'Apellido1'
         , AMP_PLAYER_NAME2          'Apellido2'
         , AMP_PLAYER_ID1            'RFC'
         , AMP_PRIZE                 'Premio'
         , AMP_WITHOLDING_TAX1       'RetencionFederal'
         , AMP_WITHOLDING_TAX2       'RetencionEstatal'
    FROM   ACCOUNT_MAJOR_PRIZES
   WHERE   AMP_DATETIME >= @_fecha_ini
     AND   AMP_DATETIME  < @_fecha_fin

END --wsp_001_constancias
GO

GRANT EXECUTE ON [dbo].[wsp_001_constancias] TO [wg_datareader] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------  
-- Copyright � 2014 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_account_points_query
--   DESCRIPTION: Returns the balance points of the player.
--        AUTHOR: Omar P�rez Ciurana
-- CREATION DATE: 03-NOV-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-NOV-2014 OPC    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_account_points_query]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_account_points_query]
GO

CREATE PROCEDURE [DBO].[wsp_001_account_points_query]
 (
    @ExternalTrackData NVARCHAR(50),
    @AccountId BIGINT
 )
AS
BEGIN
  
  -- MEMBERS
  DECLARE @_account_id BIGINT
  
  DECLARE @_status INT
  DECLARE @_blocked BIT
  DECLARE @_points MONEY
  DECLARE @_status_points INT
  DECLARE @bucket_puntos_canje INT
  
  -- SET
  SET @_status = -1
  SET @_points = 0
  SET @bucket_puntos_canje = 1

  IF @ExternalTrackData IS NULL AND @AccountId IS NULL 
  BEGIN
    -- ERROR: Account not found
    SET @_status = 10
  END
  ELSE
  BEGIN
    SET @_account_id = @AccountId

    IF @ExternalTrackData IS NOT NULL
    BEGIN 
      SET @_account_id =  (
                           SELECT   AC_ACCOUNT_ID
                             FROM   ACCOUNTS
                            WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@ExternalTrackData) 
                          )
    END  

    IF @AccountId IS NOT NULL AND @AccountId <> @_account_id
    BEGIN
      -- ERROR: Account not found
      SET @_status = 10
    END
    ELSE
    BEGIN
      SELECT   @_blocked       = AC_BLOCKED
             , @_points        = DBO.GETBUCKETVALUE(@bucket_puntos_canje, AC_ACCOUNT_ID)
             , @_status_points = AC_POINTS_STATUS
        FROM   ACCOUNTS
       WHERE   @_account_id    = AC_ACCOUNT_ID
          
      IF @@rowcount = 0
      BEGIN
        -- ERROR: Account not found
        SET @_status = 10
      END
      ELSE
      BEGIN
        IF @_blocked = 1
        BEGIN
          -- ERROR: Account blocked
          SET @_status = 1
        END
        ELSE
        BEGIN
          IF @_status_points = 1
          BEGIN
            -- ERROR: Account can't redeem points
            SET @_status = 2
          END
          ELSE
          BEGIN
            -- OK
            SET @_status = 0
          END
        END
      END
    END
  END

  -- RETURN
  SELECT   @_status AS 'Status' 
         , @_points AS 'PointsBalance'
  
END -- wsp_001_account_points_query
GO

GRANT EXECUTE ON [dbo].[wsp_001_account_points_query] TO [wg_datareader] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------  
-- Copyright � 2014 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_account_points_substract
--   DESCRIPTION: Substract the indicated points on the parameter.
--        AUTHOR: Omar P�rez Ciurana
-- CREATION DATE: 03-NOV-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-NOV-2014 OPC    First release.
-- 12-JAN-2015 JMV    DUT: Logrand MKT19 - Interfaz Micros-WIGOS: Ajuste Stored Procedures
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_account_points_substract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_account_points_substract]
GO

CREATE PROCEDURE [DBO].[wsp_001_account_points_substract]
 (
    @ExternalTrackData NVARCHAR(50),
    @AccountId BIGINT,
    @PointsToSubstract INT,
    @Details NVARCHAR(255)
 )
AS
BEGIN
  
  -- MEMBERS
  DECLARE @_account_id BIGINT
  DECLARE @_status INT
  DECLARE @_points MONEY
  DECLARE @_tbl_select TABLE (StatusSelect INT, BalancePoints MONEY)
  DECLARE @bucket_puntos_canje INT
  
  -- SET MEMBERS
  SET @_status = -1
  SET @_points = 0
  SET @bucket_puntos_canje = 1
  
  IF @PointsToSubstract IS NULL
  BEGIN
    -- ERROR: PointsToSubstract must be > 0
    SET @_status = 11
  END
  ELSE
  BEGIN     
    IF @ExternalTrackData IS NULL AND @AccountId IS NULL 
    BEGIN
      -- ERROR: Account not found
      SET @_status = 10
    END 
    ELSE
    BEGIN
    
      BEGIN TRY
       BEGIN TRANSACTION
       
       SET @_account_id = @AccountId
       
        IF @ExternalTrackData IS NOT NULL
        BEGIN
          SET @_account_id =  (
                                SELECT   AC_ACCOUNT_ID
                                  FROM   ACCOUNTS
                                 WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@ExternalTrackData) 
                              )
        END   
       
       IF @AccountId IS NOT NULL AND @AccountId <> @_account_id
       BEGIN
        -- ERROR: Account not found
        SET @_status = 10      
       END
       ELSE
       BEGIN
        
        -- BLOCK CUSTOMER_BUCKET
        UPDATE   CUSTOMER_BUCKET
           SET   CBU_VALUE       = CBU_VALUE
         WHERE   CBU_CUSTOMER_ID = @_account_id
           AND   CBU_BUCKET_ID   = @bucket_puntos_canje
         
        -- EXECUTE PROCEDURE
        INSERT INTO   @_tbl_select
            EXECUTE   dbo.wsp_001_account_points_query NULL, @_account_id
        
        -- SELECT
        SELECT   @_status = StatusSelect
               , @_points = BalancePoints
          FROM   @_tbl_select
        
        IF @_status = 0
        BEGIN
        
          UPDATE   CUSTOMER_BUCKET
             SET   CBU_VALUE       = CBU_VALUE - @PointsToSubstract
           WHERE   CBU_CUSTOMER_ID = @_account_id
             AND   CBU_VALUE - @PointsToSubstract >= 0
             AND   CBU_BUCKET_ID   = @bucket_puntos_canje

          IF @@rowcount = 0
          BEGIN
            -- ERROR: Account has not enough points
            SET @_status = 3
            SET @_points = 0
          END
          ELSE
          BEGIN
            -- OK
            SET @_status = 0
            
              IF @ExternalTrackData IS NULL 
              BEGIN
                -- GET THE TRACK DATA AND ENCRYPT
                SELECT   @ExternalTrackData = AC_TRACK_DATA
                  FROM   ACCOUNTS
                 WHERE   AC_ACCOUNT_ID = @_account_id
                  
                -- ENCRYPTED TRACK DATA
                SET @ExternalTrackData = dbo.TrackDataToExternal(@ExternalTrackData)
              END
                      
            -- INSERT MOVEMENT
            INSERT INTO   ACCOUNT_MOVEMENTS
                        ( AM_PLAY_SESSION_ID
                        , AM_ACCOUNT_ID
                        , AM_TERMINAL_ID
                        , AM_WCP_SEQUENCE_ID
                        , AM_WCP_TRANSACTION_ID
                        , AM_DATETIME
                        , AM_TYPE
                        , AM_INITIAL_BALANCE
                        , AM_SUB_AMOUNT
                        , AM_ADD_AMOUNT
                        , AM_FINAL_BALANCE
                        , AM_CASHIER_ID
                        , AM_CASHIER_NAME
                        , AM_TERMINAL_NAME
                        , AM_OPERATION_ID
                        , AM_DETAILS
                        , AM_REASONS
                        , AM_UNDO_STATUS
                        , AM_TRACK_DATA)
                VALUES 
                        ( NULL
                        , @_account_id
                        , NULL
                        , NULL
                        , NULL
                        , GETDATE()
                        , 86
                        , @_points
                        , @PointsToSubstract
                        , 0
                        , (@_points - @PointsToSubstract)
                        , NULL
                        , NULL
                        , NULL
                        , 0
                        , @Details
                        , NULL
                        , NULL
                        , @ExternalTrackData)
            
            SET @_points = (@_points - @PointsToSubstract)
            
          END
        END  
       END
       
       COMMIT TRANSACTION
       
      END TRY
      BEGIN CATCH
      
        ROLLBACK

        -- ERROR: SQL error
        SET @_status = 12
        
      END CATCH
    END
  END
    
  -- RETURN
  SELECT @_status AS 'Status', @_points AS 'PointsBalance'
    
END -- wsp_001_account_points_substract
GO

GRANT EXECUTE ON [dbo].[wsp_001_account_points_substract] TO [wg_datareader] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_actualizacion_expediente
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_actualizacion_expediente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_pld_actualizacion_expediente]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_actualizacion_expediente]
 (  
    @v_NumeroTarjeta NVARCHAR (50)		= NULL
  , @v_Cliente_Id    BIGINT						= NULL
  , @v_Folio         UNIQUEIDENTIFIER = NULL
  , @v_Date          DATETIME					= NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @v_Cliente_Id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    BEGIN TRY 
      UPDATE   ACCOUNTS
         SET   AC_EXTERNAL_AML_FILE_SEQUENCE = @v_Folio,
               AC_EXTERNAL_AML_FILE_UPDATED  = @v_Date
       WHERE   AC_ACCOUNT_ID                 = @_account_id
       
      SELECT   0 AS  Estado -- Operaci�n realizada con �xito
    END TRY
    BEGIN CATCH
      SELECT   12 AS Estado -- Error: no se ha podido realizar la operaci�n (error de SQL)
    END CATCH    
  END
END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_actualizacion_expediente] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_ficha_cliente
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_ficha_cliente]') AND type in (N'P', N'PC'))

DROP PROCEDURE [dbo].[wsp_001_pld_ficha_cliente]
GO

CREATE PROCEDURE [dbo].wsp_001_pld_ficha_cliente
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT

  SET @_account_id = @v_Cliente_Id

  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
  END

  IF @v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id
  BEGIN
    SET @_account_id = NULL
  END
  
SELECT   ac_account_id AS numCuenta,
         ac_holder_name3 AS Nombre,
         ac_holder_name1 AS ApellidoPaterno,
         ac_holder_name2 AS ApellidoMaterno,
         ac_holder_gender AS Genero,
         ac_holder_birth_date AS FechaNacimiento,
         '' AS cveEstadoNac,
         '' AS descEstadoNac,
         ac_holder_id2 as Curp,
         ac_holder_email_01 AS CorreoPrincipal,
         ac_holder_email_02 AS CorreoAlterno,
         ac_holder_phone_number_01 as TelefonoMovil,
         ac_holder_phone_number_02 as TelefonoFijo,
         'F' as cveTipoPersona,
         'FISICA' as descTipoPersona,
         oc_code as cveActEconomica,
         ac_holder_id1 as RFC,
         '' as cveNacionalidad,
         co_name as descNacionalidad,
         ac_holder_address_01 as Calle,
         ac_holder_ext_num as NumExterior,
         '' as NumInterior,
         ac_holder_address_02 as Colonia,
         '' as cvePais,
         '' as descPais,
         '' as cveEstado,
         fs_name as descEstado,
         '' as cveMunicipio,
         ac_holder_city as descMunicipio,
         ac_holder_id_type as cveTipoIdentificacion,
         idt_name as descTipoIdentificacion,
         '' as AutoridadEmisora,
         ac_holder_id as NumIdentificacion
  FROM   accounts
  LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
  LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
  LEFT JOIN identification_types on ac_holder_id_type = idt_id
  LEFT JOIN occupations on oc_id = ac_holder_occupation_id
 WHERE   ac_account_id = @_account_id

END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_ficha_cliente] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_bloqueo_desbloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
-- 21-MAY-2015 RCI    WIG-2374: No block if account is in session and other minor changes.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]
 (  
    @v_Account_Id    BIGINT        = NULL
  , @v_Block         BIT           = NULL
  , @v_Block_Reason  INT           = NULL
  , @v_Desc          NVARCHAR(256) = NULL
  , @v_Movement_Type INT           = NULL
 )
AS
BEGIN
  
DECLARE @_balance        MONEY
DECLARE @_track_data     NVARCHAR(50)
DECLARE @rc              INT
DECLARE @blocked_changed BIT
DECLARE @OutputTable     TABLE ( BLOCKED_CHANGED BIT )

BEGIN TRY

  -- Block / Unblock
  UPDATE   ACCOUNTS
     SET   AC_BLOCKED           = @v_Block,
           AC_BLOCK_REASON      = CASE WHEN @v_Block = 0 THEN 0 ELSE AC_BLOCK_REASON | @v_Block_Reason END,
           AC_BLOCK_DESCRIPTION = @v_Desc
  OUTPUT   CASE WHEN DELETED.AC_BLOCKED = INSERTED.AC_BLOCKED THEN 0 ELSE 1 END AS BLOCKED_CHANGED
    INTO   @OutputTable
   WHERE   AC_ACCOUNT_ID        = @v_Account_Id
     AND (( @v_Block = 1 AND AC_CURRENT_PLAY_SESSION_ID IS NULL ) OR @v_Block = 0 )

  SET @rc = @@ROWCOUNT

  IF ( @rc <> 1 )
  BEGIN
    SELECT   13 AS  Estado -- Error: No se ha podido bloquear porque la cuenta est� en sesi�n
  END
  ELSE
  BEGIN
    SELECT   @blocked_changed = BLOCKED_CHANGED
      FROM   @OutputTable

    IF @blocked_changed = 1
    BEGIN

      SELECT   @_balance     = AC_BALANCE
             , @_track_data  = dbo.TrackDataToExternal(AC_TRACK_DATA)
        FROM   ACCOUNTS
       WHERE   AC_ACCOUNT_ID = @v_Account_Id

      -- Movement
      INSERT INTO   ACCOUNT_MOVEMENTS
                  ( AM_PLAY_SESSION_ID, AM_ACCOUNT_ID, AM_TERMINAL_ID, AM_WCP_SEQUENCE_ID, AM_WCP_TRANSACTION_ID, AM_DATETIME
                  , AM_TYPE, AM_INITIAL_BALANCE, AM_SUB_AMOUNT, AM_ADD_AMOUNT, AM_FINAL_BALANCE, AM_CASHIER_ID, AM_CASHIER_NAME
                  , AM_TERMINAL_NAME, AM_OPERATION_ID, AM_DETAILS, AM_REASONS, AM_UNDO_STATUS, AM_TRACK_DATA)
           VALUES 
                  ( NULL, @v_Account_Id, 0, NULL, NULL, GETDATE()
                  , @v_Movement_Type, @_balance, 0, 0, @_balance, NULL, NULL
                  , 'PLD Externo', 0, @v_Desc, NULL, NULL, @_track_data)  
    END

    -- Return
    SELECT   0 AS  Estado -- Operaci�n realizada con �xito
  END

END TRY
BEGIN CATCH
  SELECT   12 AS Estado -- Error: no se ha podido realizar la operaci�n (error de SQL)
END CATCH

END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_bloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_bloqueo_cuenta]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[wsp_001_pld_bloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_bloqueo_cuenta]
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
  , @v_Reason        NVARCHAR(256) = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @_account_id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    EXEC wsp_001_pld_bloqueo_desbloqueo_cuenta @_account_id, 1, 0x40000, @v_Reason, 75 -- MovementType.AccountBlocked
  END
END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_bloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_desbloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_desbloqueo_cuenta]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_pld_desbloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_desbloqueo_cuenta]
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
  , @v_Reason        NVARCHAR(256) = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @_account_id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    EXEC wsp_001_pld_bloqueo_desbloqueo_cuenta @_account_id, 0, 0, @v_Reason, 76 -- MovementType.AccountUnblocked
  END
END
GO
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_desbloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO
