------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: CageGetGlobalReportData.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
------------------------------------------------------------------------------------------------------------------------------------------------
-- 23-MAR-2016 JML    Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
-- 22-ABR-2016 JML    Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
-- 28-ABR-2016 JML    Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
-- 02-MAY-2016 JML    Product Backlog Item 10825:Gaming Tables (Fase 1): Items, tickets & chips (Cage)
-- 10-JUN-2016 JML    Product Backlog Item 13541:UNPLANNED - Temas varios Winions Sprint 25 -  Add closing stock window
-- 16-JUN-2016 JML    Product Backlog Item 14486: Review - Backward compatibility.
-- 10-JAN-2017 CCG    Bug 20565:Error resumen de boveda - arqueos.
-- 20-JUN-2017 LTC    Bug 28294:WIGOS-2846 Error to view 'cage session report'
-- 15-MAR-2017 DPC    Bug 31952:[WIGOS-7555]: Cage general report: Database connection error is diplayed when the option is selected.
-- 30-MAY-2018 EOR		Bug 32866: WIGOS-12245 [Ticket #14549] Fallo � Reporte Global de b�veda � Tarjeta Bancaria Version V03.08.0001
------------------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN  
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT
  DECLARE @CageCurrencyType_ColorChips INT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT   CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES 
    FROM ( SELECT   SST_VALUE AS CURRENCY_ISO_CODE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
            UNION ALL 
           SELECT   'X01' AS CURRENCY_ISO_CODE    -- for retrocompatibility
            UNION ALL 
           SELECT   'X02' AS CURRENCY_ISO_CODE ) AS CURRENCIES
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
  SET @CageCurrencyType_ColorChips = 1003
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    -- LTC 2017-JUN-20
    UPDATE #TMP_STOCK_ROWS 
    SET STOCK_ROW = CASE WHEN (LEN(STOCK_ROW) - LEN(REPLACE(STOCK_ROW, ';',''))) = 3 THEN STOCK_ROW + ';' + '-1' ELSE STOCK_ROW END
    FROM #TMP_STOCK_ROWS

    SELECT left(STOCK_ROW, 3) AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4),'.', '|'), ';', '.'), 4), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 3), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 2), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE,  
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CHIP_ID, 
       CH_NAME AS NAME, CH_DRAWING AS DRAWING
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS 
    LEFT JOIN chips ON ch_chip_id = CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT)

    SELECT   A.*
           , CE_CURRENCY_ORDER 
      FROM   #TMP_STOCK AS A
      LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY   CE_CURRENCY_ORDER
           , CGS_ISO_CODE
           , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                  WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                  ELSE 1 END
           , CGS_DENOMINATION DESC
           , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                  ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT  a.*
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END  
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE 
         
     -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END AS LIABILITY_NAME 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END   AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CSM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
		 INTO #TABLE_TEMP_3
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE 
                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                    
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                  AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
              AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                                 
                         , CGM_TYPE
                   ) AS T1        
          GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END, CGM_TYPE

              UNION ALL
              
               SELECT CASE TE_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TE_ISO_CODE, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
    
    -- TABLE[3]    
        SELECT * FROM #TABLE_TEMP_3
			    WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
        DROP TABLE #TABLE_TEMP_3
        
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID
         , CC.CC_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 
    --DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT a.* 
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CM.CM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END 
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC_DESCRIPTION AS LIABILITY_NAME 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CC_DESCRIPTION
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END

      
   -- TEMP TABLE[3]: Get session sumary
   SELECT   CMD_ISO_CODE
          , CAGE_CURRENCY_TYPE
		      , ORIGIN
		      , CMD_DEPOSITS
		      , CMD_WITHDRAWALS 
	   INTO   #TEMP_TABLE_3    
     FROM (
			      SELECT   CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                   , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                          ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                   , ORIGIN
                   , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                   , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM ( SELECT   CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                   ELSE CMD_QUANTITY END AS ORIGIN
                            , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                            , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                   ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                            , CMD_DENOMINATION
                            , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                               WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                               WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                   ELSE 0 END AS CMD_DEPOSITS
                            , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END 
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE

          -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                   
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
                            AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                             
                         , CGM_TYPE 
                    ) AS T1
           GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
        INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
        INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB



	SELECT   CMD_ISO_CODE AS ISO_CODE
           , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
           , ISNULL(ORIGIN, 0) AS ORIGIN
           , SUM(CMD_DEPOSITS) AS DEPOSITS
           , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS  
	  INTO   #TEMP_TABLE_5
	  FROM   #TEMP_TABLE_3 TEMP_3   		
 LEFT JOIN   CURRENCY_EXCHANGE CE ON TEMP_3.CMD_ISO_CODE = CE.CE_CURRENCY_ISO_CODE AND CE.CE_TYPE = 0
  GROUP BY   CAGE_CURRENCY_TYPE 
           , CE_CURRENCY_ORDER
           , CMD_ISO_CODE 
           , ORIGIN  
  ORDER BY   CAGE_CURRENCY_TYPE 
           , CE_CURRENCY_ORDER
           , CMD_ISO_CODE 
           , ORIGIN DESC 
		   
	-- TABLE[3]:
	  SELECT * FROM #TEMP_TABLE_5
	           WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
      DROP TABLE #TEMP_TABLE_3
			DROP TABLE #TEMP_TABLE_5
         
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END
         , CC.CC_DESCRIPTION
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 

  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored

  DROP TABLE #TMP_CURRENCIES_ISO_CODES
  DROP TABLE #TMP_CAGE_SESSIONS
  
                   
                   
END -- CageGetGlobalReportData

GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO