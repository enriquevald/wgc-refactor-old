﻿--------------------------------------------------------------------------------
-- Copyright © 2016 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GamingTables.sql
-- 
--   DESCRIPTION: Gaming Tables reports procedures and functions
-- 
--        AUTHOR: Dani Domínguez
-- 
-- CREATION DATE: 06-MAY-2016
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-MAY-2016 ETP    First release.
-- ----------- ------ ----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[[GamingDayFromDateTime]]') AND type in (N'P', N'PC'))
DROP FUNCTION [dbo].[GamingDayFromDateTime]
GO

CREATE FUNCTION [dbo].[GamingDayFromDateTime] 
(
			-- Add the parameters for the function here
			@DateTime DATETIME
)
RETURNS int
AS
BEGIN
			-- Declare the return variable here
			DECLARE @_open               AS DATETIME
			DECLARE @_gaming_day    AS INT
			
			SET @_open       = dbo.Opening (0, @DateTime)
			SET @_gaming_day = YEAR (@_open) * 10000 + MONTH(@_open) * 100 + DAY(@_open)

			RETURN @_gaming_day

END