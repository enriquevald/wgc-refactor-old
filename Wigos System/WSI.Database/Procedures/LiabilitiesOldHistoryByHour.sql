 
  --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: LiabilitiesOldHistoryByHour.sql
-- 
--   DESCRIPTION: Historify HOURLY_LIABILITIES in CASHIER_MOVEMENTS_GROUPED_BY_HOUR
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 08-APR-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-APR-2014 JVV    First release.
-- 04-NOV-2015 ETP	  GamingHall: Changed name of TITO.TITOMode General parameter for Site.SystemMode
--------------------------------------------------------------------------------

 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LiabilitiesOldHistoryByHour]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LiabilitiesOldHistoryByHour]
GO

 CREATE PROCEDURE [dbo].[LiabilitiesOldHistoryByHour]
      @HoursToProcess INT OUTPUT
AS
BEGIN

      DECLARE @Date0   DATETIME
      DECLARE @Date1   DATETIME

      DECLARE @MaxHoursPerIteration INT
      
      DECLARE @HoursDiff INT
      DECLARE @Counter INT

      DECLARE @LastHourHistorified DATETIME
      DECLARE @CMLastHourNotHistorified DATETIME
      DECLARE @CMFirstHour DATETIME

      DECLARE @Cs_session_id AS BIGINT
      
      DECLARE @CurrencyIsoCode AS NVARCHAR(20)
      
      DECLARE @IsTITO AS INT 
      DECLARE @TypeAccounts AS INT
      DECLARE @TypeAccountsInSession AS INT
      DECLARE @TypeTickets AS INT
      DECLARE @SubType AS INT

      SET NOCOUNT ON;

         -- Type and SubType settings
         SET @TypeAccounts = 0
      SET @TypeAccountsInSession = 1
      SET @TypeTickets = 2
      SET @SubType = 2
       
      SET  @MaxHoursPerIteration = 500 
      SET @HoursToProcess = -1
      
      -- Check if is Site is in TITO Mode
      SELECT  @IsTITO = GP_KEY_VALUE 
        FROM  GENERAL_PARAMS 
       WHERE  GP_GROUP_KEY   = 'Site' 
         AND  GP_SUBJECT_KEY = 'SystemMode'

      -- Default CURRENCY_ISO_CODE
      SELECT  @CurrencyIsoCode = GP_KEY_VALUE 
        FROM  GENERAL_PARAMS 
       WHERE  GP_GROUP_KEY   = 'RegionalOptions' 
         AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

      SELECT @LastHourHistorified = MIN(CM_DATE) FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE CM_SUB_TYPE = 2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

      IF @LastHourHistorified  IS NULL
            RETURN

      SELECT @CMFirstHour = MIN(HLB_DATETIME) FROM HOURLY_LIABILITIES
      SELECT @CMLastHourNotHistorified = MAX(HLB_DATETIME) FROM HOURLY_LIABILITIES WHERE HLB_DATETIME < @LastHourHistorified 

      SET @HoursDiff = ISNULL(DATEDIFF(HOUR, @CMFirstHour, @CMLastHourNotHistorified),0)
      SET @HoursToProcess = @HoursDiff

      IF @HoursDiff <= 0 
      BEGIN
            IF @CMLastHourNotHistorified IS NOT NULL
            BEGIN
                  SET @HoursDiff = 1
                  SET @HoursToProcess = @HoursDiff
            END
            ELSE
            BEGIN
                          -- end
                  RETURN
            END
      END
      
      -- Gets hour with no minutes or seconds
      SET @Date0 = DATEADD(HOUR, DATEDIFF(HOUR, 0, @CMLastHourNotHistorified), 0)

      IF @HoursDiff > @MaxHoursPerIteration 
            SET @HoursDiff = @MaxHoursPerIteration
      
      SET @Counter = @HoursDiff

      WHILE @Counter >= -1
      BEGIN

            SET @Date1 = DATEADD (HOUR, -@Counter, @Date0)
            
            DELETE FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                  WHERE CM_DATE     = @DATE1 
                    AND CM_SUB_TYPE =  2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES
      
      
                    -- If IS CASHLESS MODE
                    IF @IsTITO = 0
                    BEGIN
                      -- Insert Accounts/Accounts-In-Session
                 INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                             ( CM_DATE
                             , CM_TYPE
                             , CM_SUB_TYPE
                             , CM_TYPE_COUNT
                             , CM_CURRENCY_ISO_CODE
                             , CM_CURRENCY_DENOMINATION
                             , CM_SUB_AMOUNT
                             , CM_ADD_AMOUNT
                             , CM_AUX_AMOUNT
                             , CM_INITIAL_BALANCE
                             , CM_FINAL_BALANCE
                             )
                  SELECT  @Date1 HLB_DATETIME
                                        , @TypeAccounts                                       AS CM_TYPE
                                        , @SubType                                            AS CM_SUB_TYPE
                                        , 0                                                         AS TYPE_COUNT
                                        , @CurrencyIsoCode                             AS CM_CURRENCY_ISO_CODE
                                        , 0                                                         AS CM_CURRENCY_DENOMINATION
                                        , ISNULL(HLB_RE_BALANCE, 0)                    AS CM_SUB_AMOUNT
                                        , ISNULL(HLB_PROMO_RE_BALANCE, 0) AS CM_ADD_AMOUNT
                                        , ISNULL(HLB_PROMO_NR_BALANCE, 0) AS CM_AUX_AMOUNT
                                        , ISNULL(HLB_POINTS, 0)                        AS CM_INITIAL_BALANCE
                                        , ISNULL(HLB_NUM_ACCOUNTS, 0)           AS CM_FINAL_BALANCE
                  FROM     HOURLY_LIABILITIES
                  WHERE  HLB_DATETIME = @Date1
                    
                       UNION ALL
                          SELECT  @Date1 HLB_DATETIME 
                                        , @TypeAccountsInSession                              AS CM_TYPE
                                        , @SubType                                                         AS CM_SUB_TYPE
                                        , 0                                                                       AS TYPE_COUNT
                                        , @CurrencyIsoCode                                          AS CM_CURRENCY_ISO_CODE
                                        , 0                                                                       AS CM_CURRENCY_DENOMINATION
                                        , ISNULL(HLB_IN_SESSION_RE_TO_GM, 0)           AS CM_SUB_AMOUNT
                                        , ISNULL(HLB_IN_SESSION_PROMO_RE_TO_GM, 0)     AS CM_ADD_AMOUNT
                                        , ISNULL(HLB_IN_SESSION_PROMO_NR_TO_GM, 0)     AS CM_AUX_AMOUNT
                                        , ISNULL(HLB_POINTS, 0)                                     AS CM_INITIAL_BALANCE
                                        , ISNULL(HLB_NUM_ACCOUNTS_IN_SESSION, 0) AS CM_FINAL_BALANCE
                  FROM     HOURLY_LIABILITIES
                  WHERE  HLB_DATETIME = @Date1
                    END
           
                    -- If IS TITO MODE
                    ELSE   
                    BEGIN
                           -- Insert Tickets
                 INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                             ( CM_DATE
                             , CM_TYPE
                             , CM_SUB_TYPE
                             , CM_TYPE_COUNT
                             , CM_CURRENCY_ISO_CODE
                             , CM_CURRENCY_DENOMINATION
                             , CM_SUB_AMOUNT
                             , CM_ADD_AMOUNT
                             , CM_AUX_AMOUNT
                             , CM_INITIAL_BALANCE
                             , CM_FINAL_BALANCE
                             )
                  SELECT  @Date1 HLB_DATETIME
                                        , @TypeTickets                                        AS CM_TYPE
                                        , @SubType                                            AS CM_SUB_TYPE
                                        , 0                                                         AS TYPE_COUNT
                                        , @CurrencyIsoCode                             AS CM_CURRENCY_ISO_CODE
                                        , 0                                                         AS CM_CURRENCY_DENOMINATION
                                        , ISNULL(HLB_RE_BALANCE, 0)                    AS CM_SUB_AMOUNT
                                        , ISNULL(HLB_PROMO_RE_BALANCE, 0) AS CM_ADD_AMOUNT
                                        , ISNULL(HLB_PROMO_NR_BALANCE, 0) AS CM_AUX_AMOUNT
                                        , ISNULL(HLB_POINTS, 0)                        AS CM_INITIAL_BALANCE
                                        , ISNULL(HLB_NUM_ACCOUNTS, 0)           AS CM_FINAL_BALANCE
                  FROM     HOURLY_LIABILITIES
                  WHERE  HLB_DATETIME = @Date1
                    END 

                    DELETE FROM HOURLY_LIABILITIES WHERE HLB_DATETIME = @Date1

      SET @Counter = @Counter -1
      END

END --LiabilitiesOldHistoryByHour

