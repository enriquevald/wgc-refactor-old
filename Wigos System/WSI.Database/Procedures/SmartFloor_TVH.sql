--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmartFloor_TVH]') AND type in (N'P', N'PC'))
--  DROP PROCEDURE [dbo].[SmartFloor_TVH]
--GO

CREATE PROCEDURE [dbo].[SmartFloor_TVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
AS
BEGIN
  DECLARE @DateStartZero AS DATETIME  
  DECLARE @DateEndZero AS DATETIME  
  DECLARE @BetAvgMinHighRoller AS MONEY
  SET @DateStartZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateStart),0)) 
  SET @DateEndZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateEnd),0)) 
  
  --SET @BetAvgMinHighRoller = 1.6    --HARDCODED. This value must be obtained from a General Param.

  SELECT    TERMINALS.TE_TERMINAL_ID
          , CAST(CONVERT(NVARCHAR(8),@DateEnd,112) AS INTEGER)                                AS T_DATE
          , DATEPART(DW,@DateEnd)                                                             AS T_WEEKDAY
          , SPH.[APUESTA MEDIA]                                                               AS BET_AVG
          , SPH.JUGADO                                                                        AS PLAYED
          , SPH.PREMIOS                                                                       AS PRIZE
          , CASE WHEN (@IsTitoMode = 1) THEN 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON] + PS2.PS_CASH_IN
            ELSE 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON]
            END                                                                               AS COIN_IN          -- CASH IN (Recargas / Bill In)
            
          , SPH.[NET WIN]                                                                     AS NET_WIN
          , SPH.[THEORICAL WON]                                                               AS THEORETICAL_WIN
          , SPH.[NUM JUGADAS]                                                                 AS PLAYED_COUNT
          , TE_DENOMINATION                                                                   AS DENOMINATION     --¿TE_MULTI_DENOMINATION?
          , SPH.LOSS                                                                          AS LOSS
          , CAST(CAST(OCUP.OCCUPIED * 100 AS DECIMAL) / 
            (CAST( (OCUP.TERMINAL_COUNT_MASTER * 24 * 60 * 60) AS DECIMAL)) AS DECIMAL(10,3)) AS MACHINE_OCUPATTION
          --, PS2.GENDER_MALE
          --, PS2.GENDER_FEMALE
          --, PS2.GENDER_UNKNOWN
          --, PS2.ANONYMOUSS                                                                    AS ANONYMOUSS
          --, PS2.REGISTERED_LEVEL_SILVER                                                       AS REGISTERED_LEVEL_SILVER
          --, PS2.REGISTERED_LEVEL_GOLD                                                         AS REGISTERED_LEVEL_GOLD
          --, PS2.REGISTERED_LEVEL_PLATINUM                                                     AS REGISTERED_LEVEL_PLATINUM
          --, PS2.REGISTERED_LEVEL_WIN                                                          AS REGISTERED_LEVEL_WIN
          --, PS2.NEW_CLIENTS                                                                   AS NEW_CLIENTS
     FROM   TERMINALS

-- SALES PER HOUR
INNER JOIN  
        (
          SELECT  SPH_TERMINAL_ID  
                , SUM(CASE WHEN SALES_PER_HOUR_V2.SPH_PLAYED_COUNT > 0 THEN 
                    ROUND( SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT / 
                           SALES_PER_HOUR_V2.SPH_PLAYED_COUNT, 2)
                  ELSE
                    0                                                               
                  END)                                                              AS [APUESTA MEDIA]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT)                          AS [JUGADO]
                , SUM(SALES_PER_HOUR_V2.SPH_WON_AMOUNT)                             AS [PREMIOS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))   AS [NET WIN]
                , SUM(SALES_PER_HOUR_V2.SPH_THEORETICAL_WON_AMOUNT)                 AS [THEORICAL WON]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_COUNT)                           AS [NUM JUGADAS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))   AS [LOSS]
            FROM  SALES_PER_HOUR_V2
           WHERE  SPH_BASE_HOUR >= @DateStart 
             AND  SPH_BASE_HOUR < @DateEnd
        GROUP BY  SPH_TERMINAL_ID) SPH
              ON  TERMINALS.TE_TERMINAL_ID = SPH.SPH_TERMINAL_ID

 -- ACCOUNT_MOVEMENTS (PDTE REVISAR)
  LEFT JOIN
 			  ( 
  			SELECT  AM_TERMINAL_ID 
  				    , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [DEPOSIT A]         -- Recarga / Cash In
  				    , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [PRIZE COUPON]      -- Cupón Premio
  			  FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_AM_DATETIME) )                       
  			 WHERE  AM_DATETIME >= @DateStart                                                   
 				   AND  AM_DATETIME <  @DateEnd 
    	GROUP BY  AM_TERMINAL_ID) 
ACCOUNT_MOV ON  ACCOUNT_MOV.AM_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID

-- MACHINE OCUPATTION % (PDTE REVISAR)
LEFT JOIN 
        (
        SELECT  TE_TERMINAL_ID
              , TE_MASTER_ID
              , SUM(DATEDIFF (SECOND, PS_STARTED, PS_FINISHED))     AS OCCUPIED 
              , COUNT(DISTINCT TC_DATE)                             AS CONNECTED
              , CASE WHEN COUNT(DISTINCT TC_DATE) = 0 THEN 
                  CASE WHEN TERMINALS.TE_MASTER_ID IS NULL THEN 0
                  ELSE 1
                  END 
                  ELSE
                    COUNT(DISTINCT TC_DATE)
                  END                                               AS TERMINAL_COUNT_MASTER
          FROM  TERMINALS
     LEFT JOIN  PLAY_SESSIONS 
            ON  TE_TERMINAL_ID = PS_TERMINAL_ID 
           AND  PS_FINISHED >= @DateStart 
           AND  PS_FINISHED <  @DateEnd 
     LEFT JOIN  TERMINALS_CONNECTED  
            ON  TC_TERMINAL_ID = TE_TERMINAL_ID 
           AND  TC_DATE >= @DateStartZero
           AND  TC_DATE <  @DateEndZero
      GROUP BY  TE_TERMINAL_ID, TE_MASTER_ID
      ) OCUP
    ON OCUP.TE_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
    
-- ACCOUNTS (GENDER: MALE, FEMALE; USER TYPE: REGISTERED(level), ANONYMOUS, UNKNOWN)
LEFT JOIN 
  (
  SELECT 
        PS.PS_TERMINAL_ID AS PSI
      --, COUNT(PS.GENDER_MALE)               AS GENDER_MALE 
      --, COUNT(PS.GENDER_FEMALE)             AS GENDER_FEMALE
      --, COUNT(PS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
      --, COUNT(PS.ANONYMOUSS)                AS ANONYMOUSS
      --, COUNT(PS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
      --, COUNT(PS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
      --, COUNT(PS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
      --, COUNT(PS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
      --, COUNT(PS.NEW_CLIENTS)               AS NEW_CLIENTS 
      , SUM(PS.PS_CASH_IN)                  AS PS_CASH_IN
  FROM 
    (
      SELECT  PS_TERMINAL_ID
            , AC_ACCOUNT_ID
			      --, SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
         --   , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
         --   , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 0 THEN 1 
         --         END)                                                          AS ANONYMOUSS
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 1 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_SILVER
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 2 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_GOLD
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 3 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_PLATINUM
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 4 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_WIN
			      --, SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
			      , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
        FROM  PLAY_SESSIONS
  INNER JOIN  ACCOUNTS
          ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
       WHERE  PS_FINISHED >= @DATESTART                       
  			 AND  PS_FINISHED <  @DATEEND                       
  			 AND  PS_STATUS   <> 0
    GROUP BY  PS_TERMINAL_ID
            , AC_ACCOUNT_ID) PS
    GROUP BY PS_TERMINAL_ID
      ) PS2
   ON PS2.PSI = TERMINALS.TE_TERMINAL_ID

-- PROMOTIONS (ASSIGNED & GRANTED)
   SELECT   PM.PROMOTIONS_TOTAL
          , PMG.PROMOTIONS_GRANTED
          , PLAYERS2.GENDER_MALE
          , PLAYERS2.GENDER_FEMALE
          , PLAYERS2.GENDER_UNKNOWN
          , PLAYERS2.ANONYMOUSS
          , PLAYERS2.REGISTERED_LEVEL_SILVER
          , PLAYERS2.REGISTERED_LEVEL_GOLD
          , PLAYERS2.REGISTERED_LEVEL_PLATINUM
          , PLAYERS2.REGISTERED_LEVEL_WIN
          , PLAYERS2.NEW_CLIENTS
     FROM   
            (SELECT COUNT(*) AS PROMOTIONS_TOTAL
               FROM PROMOTIONS) PM
          , (SELECT COUNT(*) AS PROMOTIONS_GRANTED
               FROM ACCOUNT_PROMOTIONS) PMG
          
          , (SELECT   COUNT(PLAYERS.GENDER_MALE)               AS GENDER_MALE 
                    , COUNT(PLAYERS.GENDER_FEMALE)             AS GENDER_FEMALE
                    , COUNT(PLAYERS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
                    , COUNT(PLAYERS.ANONYMOUSS)                AS ANONYMOUSS
                    , COUNT(PLAYERS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
                    , COUNT(PLAYERS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
                    , COUNT(PLAYERS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
                    , COUNT(PLAYERS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
                    , COUNT(PLAYERS.NEW_CLIENTS)               AS NEW_CLIENTS 
                FROM 
                  (
                    SELECT  AC_ACCOUNT_ID
			                    , SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 0 THEN 1 
                                END)                                                          AS ANONYMOUSS
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 1 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_SILVER
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 2 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_GOLD
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 3 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_PLATINUM
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 4 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_WIN
			                    , SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
			                    , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
                      FROM  PLAY_SESSIONS
                INNER JOIN  ACCOUNTS
                        ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
                     WHERE  PS_FINISHED >= @DATESTART                       
  			               AND  PS_FINISHED <  @DATEEND                       
  			               AND  PS_STATUS   <> 0
                  GROUP BY  AC_ACCOUNT_ID) PLAYERS
                 ) PLAYERS2
               
END

