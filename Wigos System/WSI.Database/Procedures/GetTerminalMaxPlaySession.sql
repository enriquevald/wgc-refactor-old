﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTerminalMaxPlaySession]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[GetTerminalMaxPlaySession]
GO
 
CREATE FUNCTION [dbo].[GetTerminalMaxPlaySession](@TerminalID int)
RETURNS @MaxPs TABLE (
    lp_terminal_id        INT      NOT NULL
   ,lp_play_session_id    BIGINT
)
AS
BEGIN
   INSERT INTO @MaxPs (lp_terminal_id, lp_play_session_id)
   SELECT
		ps_terminal_id AS lp_terminal_id, ISNULL(MAX(ps_play_session_id),0) AS lp_play_session_id
   FROM play_sessions
   WHERE PS_PLAY_SESSION_ID >= (SELECT ISNULL(te_current_play_session_id,0) FROM terminals WHERE te_terminal_id = @terminalid)
		 AND ps_terminal_id = @terminalid
   GROUP BY ps_terminal_id;
 
   RETURN;
END;
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TerminalProc]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetTerminalAndMaxPlaySession]
GO 

CREATE PROC [dbo].[GetTerminalAndMaxPlaySession]
 @pTerminalID int

 AS
 BEGIN

;WITH TEMP AS
(
	SELECT     dbo.terminals.te_terminal_id,  dbo.terminals.te_name, dbo.terminals.te_external_id, dbo.terminals.te_blocked,
			   MAXSP.lp_play_session_id,
						dbo.terminals.te_iso_code
	FROM    dbo.Terminals OUTER APPLY dbo.GetTerminalMaxPlaySession(te_terminal_id) MAXSP
	WHERE te_terminal_id = @pTerminalID
)
SELECT TEMP.te_name TE_NAME,
	   TEMP.te_external_id TE_EXTERNAL_ID,
       TEMP.te_blocked TE_BLOCKED,
       ac.ac_track_data AC_TRACK_DATA,
       ac.ac_account_id AC_ACCOUNT_ID,
       ps.ps_play_session_id PS_PLAY_SESSION_ID,
       ps.ps_played_count PS_PLAYED_COUNT,
       ps.ps_played_amount PS_PLAYED_AMOUNT,
       ps.ps_won_count PS_WON_COUNT,
       ps.ps_won_amount PS_WON_AMOUNT,
       ac.ac_balance AC_BALANCE,
       ps.ps_type PS_TYPE,
       ps.ps_stand_alone PS_STAND_ALONE,
       ps.ps_status PS_STATUS,
       TEMP.te_iso_code TE_ISO_CODE
FROM TEMP
	 LEFT JOIN play_sessions ps ON temp.lp_play_session_id = ps.ps_play_session_id
	 LEFT JOIN accounts ac ON ac.ac_current_play_session_id = ps.ps_account_id
END