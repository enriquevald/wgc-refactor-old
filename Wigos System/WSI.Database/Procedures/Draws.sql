--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Draws.sql
-- 
--   DESCRIPTION: Procedures for Draws 
-- 
--        AUTHOR: Raul Ruiz
-- 
-- CREATION DATE: 28-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 28-MAR-2013 RRB    First release.
-- 03-APR-2013 RCI & JAR    Changed SP ReportDrawTickets to accept draw name and account filter.
-- 04-APR-2013 RCI    Accept draw id filter and return DR_CREDIT_TYPE.
-- 05-JUN-2013 DHA    Fixed Bug #819: Modifications in the query for filters with character '%'
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE : Obtain a report of draw tickets
-- 
--  PARAMS :
--      - INPUT :
--          @pDrawName    nvarchar(50)
--          @pSqlAccount  nvarchar(256)
--          @pTicketFrom  datetime
--          @pTicketTo    datetime
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportDrawTickets]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportDrawTickets]
GO
CREATE PROCEDURE [dbo].[ReportDrawTickets]
  @pDrawId      BIGINT        = NULL,
  @pDrawName    NVARCHAR(50)  = NULL,
  @pSqlAccount  NVARCHAR(MAX) = NULL,
  @pTicketFrom  DATETIME      = NULL,
  @pTicketTo    DATETIME      = NULL
AS
BEGIN
  SET NOCOUNT ON;

  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_HOLDER_LEVEL INT)
  IF @pSqlAccount IS NOT NULL
  BEGIN
    INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_HOLDER_LEVEL FROM ACCOUNTS ' + @pSqlAccount)
    IF @@ROWCOUNT > 500
      ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
  END

  IF @pDrawId IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_DRAW_ID  = @pDrawId
       AND   DT_CREATED >= @pTicketFrom
       AND   DT_CREATED  < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pDrawName IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DR_NAME    LIKE '%' + @pDrawName + '%' ESCAPE '\'
       AND   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED    < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pSqlAccount IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by account
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_TICKET_ACCOUNT_CREATED))
     INNER   JOIN DRAWS          ON DR_ID         = DT_DRAW_ID
     INNER   JOIN #ACCOUNTS_TEMP ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED   <  @pTicketTo
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pTicketFrom IS NULL AND @pTicketTo IS NULL
    RAISERROR ('ReportDrawTickets - DateRange not defined!', 20, 0) WITH LOG

    
  IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
  IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
  --
  -- For a period
  --
  SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
         , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
         , COUNT(1) NUM_TICKETS
         , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
         , MIN(DT_CREATED) FIRST_TICKET
         , MAX(DT_CREATED) LAST_TICKET
    FROM   DRAW_TICKETS WITH (INDEX(IX_DRAW_TICKET_CREATED))
   INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
   INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
   WHERE   DT_CREATED   >= @pTicketFrom
     AND   DT_CREATED   <  @pTicketTo
   GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
   ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

  DROP TABLE #ACCOUNTS_TEMP

END -- ReportDrawTickets

GO