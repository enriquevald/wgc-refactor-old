﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType') AND type in ('P', 'PC'))
  DROP PROCEDURE GetCageMovementType
GO

CREATE PROCEDURE GetCageMovementType
 @pStarDateTime DATETIME,
 @pEndDateTime  DATETIME
AS
BEGIN

	DECLARE @_isoCodeRegional AS VARCHAR(5)

	CREATE TABLE #isoCodeTypes
	( 
		movement_id BIGINT,
		iso_code VARCHAR(3),  
		currency_type INTEGER,
		amount MONEY,
		position INTEGER
	) 

	CREATE INDEX idx_isoCode
	ON #isoCodeTypes (movement_id);

	SET @_isoCodeRegional = (SELECT GP.GP_KEY_VALUE FROM GENERAL_PARAMS AS GP WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')     
	INSERT INTO #isoCodeTypes
	SELECT CMD_MOVEMENT_ID, ISNULL(CASE WHEN CMD_ISO_CODE = 'X01' THEN @_isoCodeRegional ELSE CMD_ISO_CODE END,@_isoCodeRegional) AS CMD_ISO_CODE
		  ,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
		   ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
		   ELSE 0 END END AS CMD_CAGE_CURRENCY_TYPE
		  ,SUM(CASE WHEN CMD_CHIP_ID IS NULL THEN 0 
					ELSE CASE WHEN CMD_QUANTITY IN(-1,-2,-7,-100,-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN CMD_DENOMINATION 
					ELSE CASE WHEN CMD_ISO_CODE IN ('X02') AND CGM_TYPE IN(0, 1, 4, 5) THEN CMD_QUANTITY * -1
					ELSE CASE WHEN CMD_ISO_CODE IN ('X02') AND CGM_TYPE NOT IN(0, 1, 4, 5) THEN CMD_QUANTITY 
					ELSE CASE WHEN CGM_TYPE IN(0, 1, 4, 5) THEN CMD_QUANTITY * CMD_DENOMINATION * -1
					ELSE CMD_QUANTITY * CMD_DENOMINATION END END END END END) AS CMD_AMOUNT
		  ,CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
				ELSE CASE WHEN CE_CURRENCY_ORDER IS NULL THEN 999 
				ELSE CE_CURRENCY_ORDER END END AS CMD_POSITION  
	FROM CAGE_MOVEMENTS AS A
	LEFT JOIN CAGE_MOVEMENT_DETAILS AS B ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID 
	LEFT JOIN CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE =  CMD_ISO_CODE AND CE_TYPE = 0  
	WHERE  CGM_MOVEMENT_DATETIME >= @pStarDateTime
			AND CGM_MOVEMENT_DATETIME < @pEndDateTime
			AND CGM_TYPE NOT IN(200) AND CGM_STATUS NOT IN(0, 1) AND CMD_MOVEMENT_ID IS NOT NULL
	GROUP BY CMD_MOVEMENT_ID
			,ISNULL(CASE WHEN CMD_ISO_CODE = 'X01' THEN @_isoCodeRegional ELSE CMD_ISO_CODE END,@_isoCodeRegional)		
			,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
			 ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE 0 END END
			,CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE CASE WHEN CE_CURRENCY_ORDER IS NULL THEN 999 
			 ELSE CE_CURRENCY_ORDER END END 
		
	ORDER BY CMD_MOVEMENT_ID, MAX(ISNULL(CE_CURRENCY_ORDER,999))
			,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
			 ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE 0 END END

	CREATE TABLE #cageData (        
			 movement_id BIGINT,
			 date_insert DATETIME, 
			 movement_type INTEGER,
			 movement_type_name NVARCHAR(MAX),       
			 status_name NVARCHAR(MAX),
			 t_user_name NVARCHAR(MAX),
			 c_user_name NVARCHAR(MAX),
			 machine_name NVARCHAR(MAX),  
			 collection_id BIGINT,
			 terminal_id INTEGER
	)   

	INSERT INTO #cageData
	SELECT
		 CM.CGM_MOVEMENT_ID   
		,CM.CGM_MOVEMENT_DATETIME
		,CM.CGM_TYPE
		,CASE 
			WHEN CM.CGM_TYPE = 0   THEN 'Envío a Cajero'                             WHEN CM.CGM_TYPE = 1   THEN 'Envío a destino personalizado'
			WHEN CM.CGM_TYPE = 2   THEN 'Cierre de bóveda'                           WHEN CM.CGM_TYPE = 3   THEN 'Arqueo de bóveda'
			WHEN CM.CGM_TYPE = 4   THEN 'Envío a mesa de juego'                      WHEN CM.CGM_TYPE = 5   THEN 'Envío a terminal'
			WHEN CM.CGM_TYPE = 99  THEN 'Pérdida de cuadratura'                      WHEN CM.CGM_TYPE = 100 THEN 'Recaudación de Cajero' 
			WHEN CM.CGM_TYPE = 101 THEN 'Recaudación de origen personalizado'        WHEN CM.CGM_TYPE = 102 THEN 'Apertura de bóveda'
			WHEN CM.CGM_TYPE = 103 THEN 'Recaudación de mesa de juego'               WHEN CM.CGM_TYPE = 104 THEN 'Recaudación de terminal'
			WHEN CM.CGM_TYPE = 105 THEN 'Reapertura de bóveda'                       WHEN CM.CGM_TYPE = 106 THEN 'Recaudación de drop box de mesa de juego'
			WHEN CM.CGM_TYPE = 107 THEN 'Recaudación de drop box de mesa de juego'   WHEN CM.CGM_TYPE = 108 THEN 'Recaudación de cierre de Cajero'
			WHEN CM.CGM_TYPE = 109 THEN 'Recaudación de cierre de Mesa de Juego'	 WHEN CM.CGM_TYPE = 199 THEN 'Ganancia de cuadratura'
			WHEN CM.CGM_TYPE = 200 THEN 'Solicitud desde Cajero'                     WHEN CM.CGM_TYPE = 300 THEN 'Provisión de progresivo'
			WHEN CM.CGM_TYPE = 301 THEN 'Progresivo otorgado'                        WHEN CM.CGM_TYPE = 302 THEN '(Anulación)Provisión de progresivo'
			WHEN CM.CGM_TYPE = 303 THEN '(Anulación)Progresivo otorgado'             ELSE CAST(CM.CGM_TYPE AS NVARCHAR(MAX))
			END AS CGM_TYPE_NAME
		,CASE
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (0, 4)                         THEN 'Enviado'
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud pendiente'
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (5, 100, 103, 104)             THEN 'Pendiente recaudar'            
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (0, 4, 5, 300, 301, 302, 303)  THEN 'Finalizado'
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (100, 103, 104)                THEN 'Recaudado'
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud tramitada'
			WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud cancelada'
			WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE NOT IN (200)                      THEN 'Cancelado'   
			WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE IN (0)                            THEN 'Finalizado con descuadre denominaciones'
			WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE NOT IN (0)                        THEN 'Recaudado con descuadre denominaciones'            
			WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE IN (0)                            THEN 'Finalizado con descuadre del monto'
			WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE NOT IN (0)                        THEN 'Recaudado con descuadre total'        
			WHEN CM.CGM_STATUS IN (9, 11, 12)                                        THEN 'Finalizado'
			WHEN CM.CGM_STATUS = 10                                                  THEN 'Recaudado'  
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (108)                          THEN 'Recaudado'            
			WHEN CM.CGM_STATUS = 13                                                  THEN '(Cancelado) Pendiente recaudar'
			ELSE '---'
			END AS CGM_STATUS_NAME  
		  ,ISNULL(GU.GU_FULL_NAME, '') AS GU_FULL_NAME
		  ,ISNULL(GC.GU_FULL_NAME,'') AS GU_CAGE_FULL_NAME
		  ,ISNULL(ISNULL(CA.CT_NAME, CT.CT_NAME),TE.TE_NAME) AS CGM_NAME   
		  ,CM.CGM_MC_COLLECTION_ID
		  ,CM.CGM_TERMINAL_CASHIER_ID	 
	 FROM CAGE_MOVEMENTS AS CM
	 LEFT JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CM.CGM_USER_CASHIER_ID  
	 LEFT JOIN GUI_USERS AS GC ON GC.GU_USER_ID = CM.CGM_USER_CAGE_ID 
	 LEFT JOIN CASHIER_TERMINALS AS CA ON CA.CT_CASHIER_ID = CM.CGM_TERMINAL_CASHIER_ID
	 LEFT JOIN CASHIER_SESSIONS AS CS ON CS.CS_SESSION_ID = CM.CGM_CASHIER_SESSION_ID  
	 LEFT JOIN CASHIER_TERMINALS AS CT ON CT.CT_CASHIER_ID = CS.CS_CASHIER_ID
	 LEFT JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = CM.CGM_TERMINAL_CASHIER_ID
	 WHERE CM.CGM_TYPE NOT IN(200) AND CM.CGM_STATUS NOT IN(0, 1) AND CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
			AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime  
			ORDER BY CM.CGM_MOVEMENT_ID

	DECLARE @_isoCode VARCHAR(3)                                                                                                                       
	DECLARE @_currencyType INTEGER  
	DECLARE @_query AS NVARCHAR(MAX)
	DECLARE @_update AS NVARCHAR(MAX)
	DECLARE @_select AS NVARCHAR(MAX)

	SET @_select = ''
    
	DECLARE currency_cursor CURSOR FOR 
	SELECT cc.iso_code, cc.currency_type FROM(SELECT iso_code, currency_type , MIN(position) position FROM #isoCodeTypes
	GROUP BY iso_code, currency_type) cc
	ORDER BY cc.position, cc.currency_type

	OPEN currency_cursor
	FETCH NEXT FROM currency_cursor INTO @_isoCode, @_currencyType                                                                 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  IF @_select = ''
		 SET @_select = 'ISNULL(' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ', 0) ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX))
	  ELSE
		 SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ', 0) ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX))
 
	  SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + '] MONEY '
	  EXEC SP_EXECUTESQL @_query

	  FETCH NEXT FROM currency_cursor INTO @_isoCode, @_currencyType 
	END
	CLOSE currency_cursor                                                                                                                          
	DEALLOCATE currency_cursor  

	DECLARE @_movementID AS BIGINT
	DECLARE @_amount AS MONEY

	DECLARE cage_cursor CURSOR FOR
	SELECT movement_id FROM #cageData 

	OPEN cage_cursor
	FETCH NEXT FROM cage_cursor INTO @_movementID                                                                 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_update = ''
	  DECLARE update_cursor CURSOR FOR
	  SELECT iso_code, currency_type, amount FROM #isoCodeTypes WHERE movement_id = @_movementID
	  OPEN update_cursor
	  FETCH NEXT FROM update_cursor INTO @_isoCode, @_currencyType, @_amount               
	  WHILE @@FETCH_STATUS = 0
	  BEGIN    
		IF @_update = ''
		  SET @_update = 'UPDATE #cageData SET ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ' = ' + CAST(@_amount AS VARCHAR(MAX))
		ELSE
		  SET @_update =  @_update + ', ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ' = ' + CAST(@_amount AS VARCHAR(MAX))

		FETCH NEXT FROM update_cursor INTO @_isoCode, @_currencyType, @_amount  
	  END
	  IF @_update <> ''
	  BEGIN
		SET @_update =  @_update + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))  
		EXEC SP_EXECUTESQL @_update
	  END

	  CLOSE update_cursor		                                                                                                     
	  DEALLOCATE update_cursor 
	  FETCH NEXT FROM cage_cursor INTO @_movementID 

	END
	CLOSE cage_cursor                                                                                                                          
	DEALLOCATE cage_cursor 

	CREATE TABLE #isoCodeMachineGame
	( 
		movement_id BIGINT,
		iso_code VARCHAR(3),    
		money_amount MONEY,
		tickets_count INTEGER,
		position INTEGER
	) 

	CREATE INDEX idx_isoCodeMachineGame
	ON #isoCodeMachineGame (movement_id);

	INSERT INTO #isoCodeMachineGame
	SELECT  A.movement_id, ISNULL(C.te_iso_code, @_isoCodeRegional)   
		   ,CASE WHEN A.movement_type = 104 THEN ISNULL(B.mc_collected_bill_amount,0) + ISNULL( B.mc_collected_coin_amount, 0) 
			ELSE ISNULL(B.mc_expected_bill_amount, 0) + ISNULL( B.mc_expected_coin_amount, 0)  END
		   ,CASE WHEN A.movement_type = 104 THEN ISNULL(B.mc_collected_ticket_count,0) 
			ELSE ISNULL(B.mc_expected_ticket_count,0) END
		   ,ISNULL(D.ce_currency_order,1000) 
	FROM #cageData A
	INNER JOIN MONEY_COLLECTIONS B ON A.collection_id = B.mc_collection_id AND A.movement_type IN(5,104)
	LEFT JOIN TERMINALS C ON A.terminal_id = C.TE_TERMINAL_ID 
	LEFT JOIN CURRENCY_EXCHANGE D ON D.ce_currency_iso_code =  ISNULL(C.te_iso_code, @_isoCodeRegional)  AND D.ce_type = 0  

	DECLARE @_countISO AS INTEGER
	DECLARE machineGame_cursor CURSOR FOR
	SELECT A.iso_code  FROM(SELECT iso_code, MIN(position) AS position FROM #isoCodeMachineGame WHERE money_amount > 0
	GROUP BY iso_code) A
	ORDER BY A.position

	OPEN machineGame_cursor
	FETCH NEXT FROM machineGame_cursor INTO @_isoCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_countISO = (SELECT COUNT(*) FROM #isoCodeTypes WHERE iso_code = @_isoCode AND currency_type = 0 )

	  IF @_countISO = 0
	  BEGIN
		  IF @_select = ''
		   SET @_select = 'ISNULL(' + @_isoCode + '_0, 0) ' + @_isoCode + '_0'
		 ELSE
		   SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_0, 0) ' + @_isoCode + '_0'

		 SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_0] MONEY '
		 EXEC SP_EXECUTESQL @_query
	  END

	  FETCH NEXT FROM machineGame_cursor INTO @_isoCode
	END
	CLOSE machineGame_cursor                                                                                                                          
	DEALLOCATE machineGame_cursor 


	DECLARE machineTicket_cursor CURSOR FOR
	SELECT A.iso_code  FROM(SELECT iso_code, MIN(position) AS position FROM #isoCodeMachineGame WHERE tickets_count > 0
	GROUP BY iso_code) A
	ORDER BY A.position

	OPEN machineTicket_cursor
	FETCH NEXT FROM machineTicket_cursor INTO @_isoCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_countISO = (SELECT COUNT(*) FROM #isoCodeTypes WHERE iso_code = @_isoCode AND currency_type = 2001 )

	  IF @_countISO = 0
	  BEGIN
		  IF @_select = ''
		   SET @_select = 'ISNULL(' + @_isoCode + '_2001, 0) ' + @_isoCode + '_2001'
		 ELSE
		   SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_2001, 0) ' + @_isoCode + '_2001'

		 SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_2001] MONEY '
		 EXEC SP_EXECUTESQL @_query
	  END

	  FETCH NEXT FROM machineTicket_cursor INTO @_isoCode
	END
	CLOSE machineTicket_cursor                                                                                                                          
	DEALLOCATE machineTicket_cursor 

	DECLARE @_countTicket AS MONEY

	DECLARE update_cursor CURSOR FOR
	SELECT movement_id, iso_code, money_amount, tickets_count  AS position FROM #isoCodeMachineGame WHERE tickets_count > 0 OR money_amount > 0

	OPEN update_cursor
	FETCH NEXT FROM update_cursor INTO @_movementID, @_isoCode, @_amount, @_countTicket
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_update = ''

	  IF @_amount > 0
	  BEGIN
		 SET @_update =' UPDATE #cageData SET ' + @_isoCode + '_0 = ' + CAST(@_amount AS VARCHAR(MAX)) + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))
	  END
  
	  IF @_countTicket > 0
	  BEGIN
		SET @_update = @_update + ' UPDATE #cageData SET ' + @_isoCode + '_2001 = ' + CAST(@_countTicket AS VARCHAR(MAX)) + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))
	  END

	  IF @_update <> ''
	  BEGIN
		EXEC SP_EXECUTESQL @_update
	  END

	  FETCH NEXT FROM update_cursor INTO @_movementID, @_isoCode, @_amount, @_countTicket
	END
	CLOSE update_cursor                                                                                                                          
	DEALLOCATE update_cursor 

	SET @_update = '
		  SELECT 
			 CONVERT(char(11), date_insert, 103) + CONVERT(char(8), date_insert, 108) AS ''Fecha''
			,c_user_name AS ''Usuario de Bóveda''  
		    ,CASE WHEN t_user_name LIKE ''SYS-%'' THEN '''' ELSE ISNULL(t_user_name,'''') END  AS ''Origen/Destino''
			,ISNULL(machine_name,'''') AS ''Terminal'' 
			,movement_type_name AS ''Tipo de movimiento'' 
			,status_name ''Estado'''
	
	IF @_select <> ''
	BEGIN
	  SET @_update = @_update + ',' + @_select
	END 
	
	SET @_update = @_update + ' FROM #cageData '		
								  
	EXEC SP_EXECUTESQL @_update

	DROP TABLE #cageData
	DROP TABLE #isoCodeMachineGame
	DROP TABLE #isoCodeTypes
END
GO

GRANT EXECUTE ON GetCageMovementType TO wggui WITH GRANT OPTION 
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType_GR') AND type in ('P', 'PC'))
  DROP PROCEDURE GetCageMovementType_GR
GO

CREATE PROCEDURE GetCageMovementType_GR
 @pFrom DATETIME,
 @pTo  DATETIME
AS
BEGIN
	EXEC GetCageMovementType @pFrom, @pTo
END
GO

GRANT EXECUTE ON GetCageMovementType_GR TO wggui WITH GRANT OPTION 
GO

DECLARE @_sheets AS XML
SET @_sheets = '
<ArrayOfReportToolDesignSheetsDTO>
  <ReportToolDesignSheetsDTO>
    <LanguageResources>
      <NLS09>
        <Label>Cage movements for currency</Label>
      </NLS09>
      <NLS10>
        <Label>Movimientos por divisa</Label>
      </NLS10>
    </LanguageResources>
    <Columns>
      <ReportToolDesignColumn>
        <Code>Fecha</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Date</Label>
          </NLS09>
          <NLS10>
            <Label>Fecha</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Usuario de Bóveda</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Cash cage user</Label>
          </NLS09>
          <NLS10>
            <Label>Usuario de Bóveda</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Origen/Destino</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Source/Destination</Label>
          </NLS09>
          <NLS10>
            <Label>Origen/Destino</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Tipo de movimiento</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Type</Label>
          </NLS09>
          <NLS10>
            <Label>Tipo de movimiento</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Estado</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Status</Label>
          </NLS09>
          <NLS10>
            <Label>Estado</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>  
	  <ReportToolDesignColumn>
        <Code>_0</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label></Label>
          </NLS09>
          <NLS10>
            <Label></Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_1001</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Chips RE</Label>
          </NLS09>
          <NLS10>
            <Label>-Fichas RE</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_1002</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Chips NR</Label>
          </NLS09>
          <NLS10>
            <Label>-Fichas NR</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>X02_1003</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Color chips</Label>
          </NLS09>
          <NLS10>
            <Label>Fichas de color</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_2001</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Tickets TITO</Label>
          </NLS09>
          <NLS10>
            <Label>-Tickets TITO</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>                    
    </Columns>
  </ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>
'

IF EXISTS (SELECT * FROM report_tool_config WHERE rtc_store_name = 'GetCageMovementType_GR')
	BEGIN
		UPDATE report_tool_config
		 SET rtc_design_sheets = @_sheets 
		WHERE rtc_store_name = 'GetCageMovementType_GR'
	END
ELSE
	BEGIN
	  DECLARE @_name AS XML
  
	  SET @_name = '
		<LanguageResources>
		  <NLS09>
			<Label>Cage movements for currency</Label>
		  </NLS09>
		  <NLS10>
			<Label>Movimientos de bóveda por divisa</Label>
		  </NLS10>
		</LanguageResources>
	  '
	   DECLARE @_filter AS XML

	   SET @_filter = '
		<ReportToolDesignFilterDTO>
			<FilterType>FromToDate</FilterType>
		</ReportToolDesignFilterDTO>
	   '

	  INSERT INTO report_tool_config
		(rtc_form_id
		,rtc_location_menu
		,rtc_report_name
		,rtc_store_name
		,rtc_design_filter
		,rtc_design_sheets
		,rtc_mailing
		,rtc_status
		,rtc_mode_type
		,rtc_html_header
		,rtc_html_footer)
	  VALUES (11000, 7, @_name, 'GetCageMovementType_GR', @_filter, @_sheets , 1, 0, 1, NULL, NULL)
	END
GO