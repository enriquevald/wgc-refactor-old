--------------------------------------------------------------------------------
-- Copyright © 2016 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AddTimestampToTable.sql
-- 
--   DESCRIPTION: Add Timestamp To Table 
-- 
--        AUTHOR: Lionel Asensio
-- 
-- CREATION DATE: 10-FEB-2016
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 10-FEB-2016 LA     First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddTimestampToTable]     
    @sTableName VARCHAR(50),
    @sColumnName VARCHAR(50),
    @sIndexName VARCHAR(50)
AS
BEGIN
    DECLARE @tSql VARCHAR (500)
    SET @tSql = 'ALTER TABLE [' + @sTableName + '] ADD [' + @sColumnName + '] TIMESTAMP; '
              + 'IF NOT EXISTS(SELECT * 
                               FROM sys.indexes 
                               WHERE name='+ CHAR(39)+ @sIndexName + CHAR(39)+' AND object_id = OBJECT_ID('+CHAR(39)+ @sTableName +CHAR(39)+'))'
              + 'BEGIN'
              + ' CREATE UNIQUE INDEX '+ @sIndexName + ' ON [' + @sTableName + '] (' + @sColumnName + ');'
              + 'END';
    EXEC(@tSql);
END