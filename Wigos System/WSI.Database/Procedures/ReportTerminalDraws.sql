--------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportTerminalDraws.sql
-- 
--   DESCRIPTION: Procedure for Terminal Draws detailed report
-- 
--        AUTHOR: Xavi Guirado
-- 
-- CREATION DATE: 27-GEN-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-GEN-2017 XGJ    First release.
-- 01-FEB-2017 FJC    PBI 22243:Sorteo de máquina - Reporte - US 1 - Pantalla GUI
-- 23-FEB-2017 XGJ    PBI 22243:Sorteo de máquina - Reporte - US 1 - Pantalla GUI 
-- 02-AGO-2017 FJC    WIGOS-4149 Terminal Draw: report screen needs review
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash desk draws filtering by its parameters
-- 
--  PARAMS:
--      - INPUT:
--   @pDrawFrom	       DATETIME     
--   @pDrawTo		       DATETIME      
--   @pDrawId		       BIGINT		  
--   @pTerminalId      INT			 
--   @pUserId          INT			 
--   @pSqlAccount      NVARCHAR(MAX) 
--   @pCashierMovement INT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportTerminalDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportTerminalDraws]
GO

SET QUOTED_IDENTIFIER ON
GO



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportTerminalDraws]  
  @pDrawFrom	      DATETIME      = NULL,
  @pDrawTo		      DATETIME      = NULL,
  @pDrawId		      BIGINT		    =	NULL,
  @pTerminalId      INT			      = NULL,
  @pUserId          INT			      = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pTerminalDraw	  INT				    = 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	SET @_Terminal_draw_type = 5
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --Filter by terminalDraw 
	  IF  @pTerminalDraw = 1
	  BEGIN
		      SELECT    DISTINCT
		                CD_DRAW_ID
		              , CD_DRAW_DATETIME
		              , TE_TERMINAL_TYPE
		              , CM_CASHIER_NAME		  
		              , CM_USER_NAME
		              , CD_OPERATION_ID		
		              , CD_ACCOUNT_ID
		              , AC_HOLDER_NAME
		              , CD_COMBINATION_BET
		              , CD_COMBINATION_WON
		              , CD_RE_BET
                  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                         ELSE CD_RE_WON END CD_RE_WON
		              , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		                     ELSE CD_NR_WON END CD_NR_WON
		              , CD_PRIZE_ID
		        FROM    CASHDESK_DRAWS WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		  INNER JOIN    CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
		          ON    CD_OPERATION_ID = CM_OPERATION_ID  
		  INNER JOIN    ACCOUNTS   
		          ON    CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		  INNER JOIN    TERMINALS 
		          ON    CD_TERMINAL =  TE_TERMINAL_ID
		   LEFT JOIN    ACCOUNT_OPERATIONS AC 
		          ON    AC.AO_OPERATION_ID = CD_OPERATION_ID
		   LEFT JOIN    ACCOUNT_PROMOTIONS AP 
		          ON    AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  		        
		       WHERE    CD_DRAW_ID = @pDrawId	
             AND    CD_DRAW_DATETIME >= @pDrawFrom
	           AND    CD_DRAW_DATETIME < @pDrawTo	
	           AND    ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
	           AND    ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	           AND    ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
	           AND    te_terminal_type = @_Terminal_draw_type
		    ORDER BY    CD_DRAW_ID
	
	  DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	--IF  @pTerminalDraw = 1

	  IF 	@pSqlAccount IS NOT NULL 
	  --
	  -- Filtering by account, but not by draw ID
	  --
	  BEGIN		
		  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		  IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	    --Filter by TERMINAL draw
	    IF  @pTerminalDraw = 1
	    BEGIN
            SELECT DISTINCT
		                CD_DRAW_ID
		              , CD_DRAW_DATETIME
		              , TE_TERMINAL_TYPE
		              , CM_CASHIER_NAME		  
		              , CM_USER_NAME
		              , CD_OPERATION_ID		
		              , CD_ACCOUNT_ID
		              , CD_COMBINATION_BET
		              , CD_COMBINATION_WON
		              , CD_RE_BET
                  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                         ELSE CD_RE_WON END CD_RE_WON
		              , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		                     ELSE CD_NR_WON END CD_NR_WON
		              , CD_PRIZE_ID
		          FROM  CASHDESK_DRAWS WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		    INNER JOIN  CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
		            ON  CD_OPERATION_ID = CM_OPERATION_ID 
		    INNER JOIN  #ACCOUNTS_TEMP    
		            ON  CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		    INNER JOIN  TERMINALS 
		            ON  CD_TERMINAL =  TE_TERMINAL_ID
		     LEFT JOIN  ACCOUNT_OPERATIONS AC 
		            ON  AC.AO_OPERATION_ID = CD_OPERATION_ID
		     LEFT JOIN  ACCOUNT_PROMOTIONS AP 
		            ON  AP.ACP_OPERATION_ID = CD_OPERATION_ID 
		           AND  AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  		        
		         WHERE  CD_DRAW_DATETIME >= @pDrawFrom
		           AND  CD_DRAW_DATETIME < @pDrawTo 		   
		           AND  ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
		           AND  ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		           AND  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		           AND  te_terminal_type = @_Terminal_draw_type
		      ORDER BY  CD_DRAW_ID
		 
		    DROP TABLE #ACCOUNTS_TEMP
  		    
		      RETURN
	    END
	  END --IF 	@pSqlAccount IS NOT NULL 
	
	END --IF	@pDrawId IS NOT NULL 
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	

	--Filter by Terminal draw
	IF  @pTerminalDraw = 1
	BEGIN
      SELECT  DISTINCT
	            CD_DRAW_ID
	          , CD_DRAW_DATETIME
	          , TE_TERMINAL_TYPE
	          , CM_CASHIER_NAME		  
	          , CM_USER_NAME
	          , CD_OPERATION_ID		
	          , CD_ACCOUNT_ID
	          , AC_HOLDER_NAME
	          , CD_COMBINATION_BET
	          , CD_COMBINATION_WON
	          , CD_RE_BET
            , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                   ELSE CD_RE_WON END CD_RE_WON
            , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
                   ELSE CD_NR_WON END CD_NR_WON
	          , CD_PRIZE_ID
	      FROM  CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
  INNER JOIN  CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
          ON  CD_OPERATION_ID = CM_OPERATION_ID
  INNER JOIN  ACCOUNTS          
          ON  CD_ACCOUNT_ID = AC_ACCOUNT_ID
  INNER JOIN  TERMINALS 
          ON  CD_TERMINAL =  TE_TERMINAL_ID
   LEFT JOIN  ACCOUNT_OPERATIONS AC 
          ON  AC.AO_OPERATION_ID = CD_OPERATION_ID
   LEFT JOIN  ACCOUNT_PROMOTIONS AP 
          ON  AP.ACP_OPERATION_ID = CD_OPERATION_ID 
         AND  AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
	     WHERE  CD_DRAW_DATETIME >= @pDrawFrom
	       AND 	CD_DRAW_DATETIME < @pDrawTo	     
	       AND  ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
	       AND 	((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	       AND  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
         AND  te_terminal_type = @_Terminal_draw_type
    ORDER BY  CD_DRAW_ID
  
  DROP TABLE #ACCOUNTS_TEMP
	
	  RETURN
	END

END --AS BEGIN

GO

GRANT EXECUTE ON [dbo].[ReportTerminalDraws] TO [wggui] WITH GRANT OPTION 
GO