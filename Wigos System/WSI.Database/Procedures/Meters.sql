IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGameIdForTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetGameIdForTerminal]
GO

CREATE PROCEDURE [dbo].[GetGameIdForTerminal]
      @pFromDate                  DATETIME 
,     @pToDate                    DATETIME
,     @pTerminalId                INT

AS
BEGIN 

  DECLARE @pGameId AS INT

  -- Get game id from sales per hour
  SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
    FROM   SALES_PER_HOUR
   WHERE   SPH_BASE_HOUR >= @pFromDate
     AND   SPH_BASE_HOUR < @pToDate
     AND   SPH_TERMINAL_ID = @pTerminalId
     
  IF @pGameId = 0
  BEGIN
    -- Get game id from terminals
    SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
      FROM   TERMINALS
     WHERE   TE_TERMINAL_ID = @pTerminalId
  END

  IF @pGameId = 0
  BEGIN
    -- Get game id from terminal game translation
    SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
      FROM   TERMINAL_GAME_TRANSLATION
     WHERE   TGT_TERMINAL_ID = @pTerminalId
  END
    
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 11, 10)
  END
  ELSE
  BEGIN
   SELECT @pGameId
  END
END
GO

GRANT EXECUTE ON [dbo].[GetGameIdForTerminal] TO [wggui] WITH GRANT OPTION
GO
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalSasMeterHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
GO

CREATE PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
    @pTerminaId       Int
  , @pMeterCode       Int
  , @pGameId          Int
  , @pDenomination    Money
  , @pType            Int
  , @pDateTime        DateTime
  , @pMeterIniValue   BigInt
  , @pMeterFinValue   BigInt
  , @pMeterIncrement  BigInt
AS
BEGIN

  Declare @pSasAccounDenom AS Money

  IF NOT EXISTS (SELECT 1 
                   FROM TERMINAL_SAS_METERS_HISTORY 
                  WHERE TSMH_TERMINAL_ID  = @pTerminaId
                    AND TSMH_METER_CODE   = @pMeterCode
                    AND TSMH_GAME_ID      = @pGameId
                    AND TSMH_DENOMINATION = @pDenomination
                    AND TSMH_TYPE         = @pType
                    AND TSMH_DATETIME     = @pDateTime )
  BEGIN

          SELECT @pSasAccounDenom = TE_SAS_ACCOUNTING_DENOM 
            FROM TERMINALS 
           WHERE TE_TERMINAL_ID = @pTerminaId

    INSERT INTO TERMINAL_SAS_METERS_HISTORY
              ( TSMH_TERMINAL_ID  
              , TSMH_METER_CODE  
              , TSMH_GAME_ID  
              , TSMH_DENOMINATION  
              , TSMH_TYPE  
              , TSMH_DATETIME  
              , TSMH_METER_INI_VALUE  
              , TSMH_METER_FIN_VALUE  
              , TSMH_METER_INCREMENT  
              , TSMH_RAW_METER_INCREMENT  
              , TSMH_LAST_REPORTED
              , TSMH_SAS_ACCOUNTING_DENOM )
       VALUES
             (  @pTerminaId     
              , @pMeterCode     
              , @pGameId        
              , @pDenomination  
              , @pType          
              , @pDateTime      
              , @pMeterIniValue 
              , @pMeterFinValue 
              , @pMeterIncrement
              , 0
              , NULL
              , @pSasAccounDenom )
  END  
  ELSE
  BEGIN

    UPDATE   TERMINAL_SAS_METERS_HISTORY
       SET   TSMH_METER_INI_VALUE = @pMeterIniValue
           , TSMH_METER_FIN_VALUE = @pMeterFinValue
           , TSMH_METER_INCREMENT = @pMeterIncrement
     WHERE   TSMH_TERMINAL_ID = @pTerminaId
       AND   TSMH_METER_CODE = @pMeterCode
       AND   TSMH_GAME_ID = @pGameId
       AND   TSMH_DENOMINATION = @pDenomination
       AND   TSMH_TYPE = @pType
       AND   TSMH_DATETIME = @pDateTime
  END
END
GO

GRANT EXECUTE ON [dbo].[Update_TerminalSasMeterHistory] TO [wggui] WITH GRANT OPTION;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_MachineStatsPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_MachineStatsPerHour]
GO

CREATE PROCEDURE  [dbo].[Update_Meters_MachineStatsPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                 INT
,     @pMSH_Played_Count          BIGINT = NULL
,     @pMSH_Played_Amount         MONEY = NULL
,     @pMSH_Won_Count             BIGINT = NULL
,     @pMSH_Won_Amount            MONEY = NULL
,     @pMSH_Jackpot_Amount        MONEY = NULL
,     @pMSH_To_GM_Amount          MONEY = NULL
,     @pMSH_From_GM_Amount        MONEY = NULL
,     @pMSH_HPC_Handpays_Amount   MONEY = NULL

AS
BEGIN 

DECLARE @pTerminalName AS NVARCHAR(50)
DECLARE @pGameName AS NVARCHAR(50)

IF NOT EXISTS ( SELECT   *
                  FROM   MACHINE_STATS_PER_HOUR
                 WHERE   MSH_BASE_HOUR = @pBaseHour
                   AND   MSH_TERMINAL_ID = @pTerminalId)
BEGIN

  INSERT INTO MACHINE_STATS_PER_HOUR
             ( MSH_BASE_HOUR
             , MSH_TERMINAL_ID
             , MSH_PLAYED_COUNT
             , MSH_PLAYED_AMOUNT
             , MSH_WON_COUNT
             , MSH_WON_AMOUNT
             , MSH_TO_GM_COUNT
             , MSH_TO_GM_AMOUNT
             , MSH_FROM_GM_COUNT
             , MSH_FROM_GM_AMOUNT
             , MSH_JACKPOT_AMOUNT
             , MSH_HPC_HANDPAYS_AMOUNT)
       VALUES
             ( @pBaseHour
             , @pTerminalId
             , ISNULL(@pMSH_Played_Count, 0)
             , ISNULL(@pMSH_Played_Amount, 0)
             , ISNULL(@pMSH_Won_Count, 0)
             , ISNULL(@pMSH_Won_Amount, 0)
             , 0
             , ISNULL(@pMSH_To_GM_Amount, 0)
             , 0
             , ISNULL(@pMSH_From_GM_Amount, 0)
             , ISNULL(@pMSH_Jackpot_Amount, 0)
             , ISNULL(@pMSH_HPC_Handpays_Amount, 0))

END
ELSE
BEGIN

  UPDATE   MACHINE_STATS_PER_HOUR
     SET   MSH_PLAYED_COUNT = ISNULL(@pMSH_Played_Count, MSH_PLAYED_COUNT)
         , MSH_PLAYED_AMOUNT = ISNULL(@pMSH_Played_Amount, MSH_PLAYED_AMOUNT)
         , MSH_WON_COUNT = ISNULL(@pMSH_Won_Count, MSH_WON_COUNT)
         , MSH_WON_AMOUNT = ISNULL(@pMSH_Won_Amount, MSH_WON_AMOUNT)
         , MSH_TO_GM_AMOUNT = ISNULL(@pMSH_To_GM_Amount, MSH_TO_GM_AMOUNT)
         , MSH_FROM_GM_AMOUNT = ISNULL(@pMSH_From_GM_Amount, MSH_FROM_GM_AMOUNT)
         , MSH_JACKPOT_AMOUNT  = ISNULL(@pMSH_Jackpot_Amount, MSH_JACKPOT_AMOUNT)
         , MSH_HPC_HANDPAYS_AMOUNT = ISNULL(@pMSH_HPC_Handpays_Amount, MSH_HPC_HANDPAYS_AMOUNT)
   WHERE   MSH_BASE_HOUR = @pBaseHour
     AND   MSH_TERMINAL_ID = @pTerminalId

END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_MachineStatsPerHour] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_SalesPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_SalesPerHour]
GO

CREATE PROCEDURE [dbo].[Update_Meters_SalesPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                INT
,     @pGameId                    INT
,     @pTerminalDefaultPayout     MONEY
,     @pSPH_Played_Count          BIGINT
,     @pSPH_Played_Amount         MONEY
,     @pSPH_Won_Count             BIGINT
,     @pSPH_Won_Amount            MONEY
,     @pSPH_Jackpot_Amount        MONEY

AS
BEGIN 

  DECLARE @pTerminalName AS NVARCHAR(50)
  DECLARE @pGameName AS NVARCHAR(50)
  DECLARE @payout AS MONEY 

  IF @pGameId = 0
  BEGIN
    -- Get game id from sales per hour
    SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR >= dbo.Opening(0, @pBaseHour)
       AND   SPH_BASE_HOUR < DATEADD(DAY, 1, dbo.Opening(0, @pBaseHour))
       AND   SPH_TERMINAL_ID = @pTerminalId
     
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminals
      SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId
    END
    
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminal game translation
      SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
        FROM   TERMINAL_GAME_TRANSLATION
       WHERE   TGT_TERMINAL_ID = @pTerminalId
    END
  END
  
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 16, 10)
    RETURN
  END

  -- Get terminal name 
  SELECT   @pTerminalName = TE_NAME
         , @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) 
    FROM   TERMINALS
   WHERE   TE_TERMINAL_ID = @pTerminalId

  IF NOT EXISTS ( SELECT   *
                    FROM   SALES_PER_HOUR
                   WHERE   SPH_BASE_HOUR = @pBaseHour
                     AND   SPH_TERMINAL_ID = @pTerminalId
                     AND   SPH_GAME_ID = @pGameId)
  BEGIN

    -- Get game name
    SELECT   @pGameName = GM_NAME
      FROM   GAMES
     WHERE   GM_GAME_ID = @pGameId   
     
    INSERT INTO SALES_PER_HOUR
               ( SPH_BASE_HOUR
               , SPH_TERMINAL_ID
               , SPH_TERMINAL_NAME
               , SPH_GAME_ID
               , SPH_GAME_NAME
               , SPH_PLAYED_COUNT
               , SPH_PLAYED_AMOUNT
               , SPH_WON_COUNT
               , SPH_WON_AMOUNT
               , SPH_NUM_ACTIVE_TERMINALS
               , SPH_LAST_PLAY_ID
               , SPH_THEORETICAL_WON_AMOUNT
               , SPH_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
               , SPH_PROGRESSIVE_PROVISION_AMOUNT)
         VALUES
               ( @pBaseHour
               , @pTerminalId
               , @pTerminalName
               , @pGameId
               , @pGameName
               , ISNULL(@pSPH_Played_Count, 0)
               , ISNULL(@pSPH_Played_Amount, 0)
               , ISNULL(@pSPH_Won_Count, 0)
               , ISNULL(@pSPH_Won_Amount, 0)
               , 0
               , 0
               , ISNULL(@pSPH_Played_Amount, 0) * @payout
               , ISNULL(@pSPH_Jackpot_Amount, 0)
               , 0
               , 0
               , 0)

  END
  ELSE
  BEGIN

    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT   = ISNULL(@pSPH_Played_Count, SPH_PLAYED_COUNT)
           , SPH_PLAYED_AMOUNT  = ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT)
           , SPH_WON_COUNT      = ISNULL(@pSPH_Won_Count, SPH_WON_COUNT)
           , SPH_WON_AMOUNT     = ISNULL(@pSPH_Won_Amount, SPH_WON_AMOUNT)
           , SPH_JACKPOT_AMOUNT = ISNULL(@pSPH_Jackpot_Amount, SPH_JACKPOT_AMOUNT)
           , SPH_THEORETICAL_WON_AMOUNT = CASE WHEN ISNULL(SPH_PLAYED_AMOUNT, 0) = 0
                                               THEN ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * @payout
                                               ELSE ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * (SPH_THEORETICAL_WON_AMOUNT / SPH_PLAYED_AMOUNT)
                                                END
     WHERE   SPH_BASE_HOUR = @pBaseHour
       AND   SPH_TERMINAL_ID = @pTerminalId
       AND   SPH_GAME_ID = @pGameId

  END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_SalesPerHour] TO [wggui] WITH GRANT OPTION
GO

