--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CommonProcedures.sql
-- 
--   DESCRIPTION: Procedures shared between Wigos and 3GS components
-- 
--        AUTHOR: Agust� Poch
-- 
-- CREATION DATE: 06-OCT-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-OCT-2010 APB    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Inserts a new movement in the database
-- 
--  PARAMS:
--      - INPUT:
--          @PlaySessionId  bigint ,
--          @AccountId      bigint ,
--          @TerminalId     int ,
--          @MovementType   int ,                                       
--          @InitialBalance money , 
--          @SubAmount      money , 
--          @AddAmount      money , 
--          @FinalBalance   money
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode, 
--      StatusText, 
--      SessionID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMovement]
GO
CREATE PROCEDURE dbo.InsertMovement
  @PlaySessionId  bigint ,
  @AccountId      bigint ,
  @TerminalId     int ,
  @MovementType   int ,                                       
  @InitialBalance money , 
  @SubAmount      money , 
  @AddAmount      money , 
  @FinalBalance   money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @terminal_name nvarchar (254)
  
  SET @terminal_name = ''

  SELECT  @terminal_name = TE_NAME 
    FROM  TERMINALS
   WHERE  TE_TERMINAL_ID = @TerminalId 

  INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID
                               , AM_ACCOUNT_ID 
                               , AM_TERMINAL_ID 
                               , AM_TYPE 
                               , AM_INITIAL_BALANCE 
                               , AM_SUB_AMOUNT 
                               , AM_ADD_AMOUNT 
                               , AM_FINAL_BALANCE 
                               , AM_DATETIME 
                               , AM_TERMINAL_NAME 
                               ) 
                         VALUES (@PlaySessionId
                               , @AccountId
                               , @TerminalId
                               , @MovementType
                               , @InitialBalance
                               , @SubAmount
                               , @AddAmount
                               , @FinalBalance
                               , GETDATE()
                               , @terminal_name 
                               )
                               
  -- SET @MovementId = SCOPE_IDENTITY() 

END -- InsertMovement

GO 

--------------------------------------------------------------------------------
-- PURPOSE: Concat the Opening Time to the parameter Date.
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--        @Date   datetime
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConcatOpeningTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ConcatOpeningTime]
GO
CREATE FUNCTION dbo.ConcatOpeningTime
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @closing_hour    int
  DECLARE @closing_minutes int
  DECLARE @today_opening  datetime

  SET @closing_hour = ISNULL((SELECT   gp_key_value
                                FROM   general_params
                               WHERE   gp_group_key   = 'WigosGUI'
                                 AND   gp_subject_key = 'ClosingTime'), 6)

  SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                   FROM   general_params
                                  WHERE   gp_group_key   = 'WigosGUI'
                                    AND   gp_subject_key = 'ClosingTimeMinutes'), 0)

  -- Trunc date to start of the day (00:00h).
  SET @today_opening = DATEADD(DAY, DATEDIFF(day, 0, @Date), 0)
  
  SET @today_opening = DATEADD(HOUR, @closing_hour, @today_opening)
  SET @today_opening = DATEADD(MINUTE, @closing_minutes, @today_opening)

  RETURN @today_opening
END -- ConcatOpeningTime

GO

--------------------------------------------------------------------------------
-- PURPOSE: Returns the Opening DateTime according to the DateTime parameter
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--        @Date   datetime
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Opening]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Opening]
GO
CREATE FUNCTION dbo.Opening
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @now           datetime
  DECLARE @today_opening datetime

  SET @now = @Date
  SET @today_opening = dbo.ConcatOpeningTime(@SiteId, @now)

  IF @today_opening > @now
  BEGIN
    SET @today_opening = DATEADD(DAY, -1, @today_opening)
  END

  RETURN @today_opening
END -- Opening

GO

--------------------------------------------------------------------------------
-- PURPOSE: Returns the Opening DateTime
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TodayOpening]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[TodayOpening]
GO
CREATE FUNCTION dbo.TodayOpening
  (@SiteId int)
RETURNS datetime
AS
BEGIN
  RETURN dbo.Opening(@SiteId, GETDATE())
END -- TodayOpening

GO
