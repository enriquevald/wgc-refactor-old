--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: SessionEnd.sql
--
--   DESCRIPTION: End Session procedure
--
--        AUTHOR: Miquel Beltran
--
-- CREATION DATE: 30-APR-2010
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-APR-2010 MBF    First release.
-- 17-JUN-2010 MBF    Audit
-- 10-MAY-2012 DDM    Add field in function SetPlaySessionBalanceMismatch.
-- 05-JUL-2012 MPO    Added try/catch. Rename labels.
-- 25-JUL-2012 RCI    Call the procedures that matches C# routines.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: End play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionEnd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionEnd]
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
WITH EXECUTE AS OWNER
AS
BEGIN

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 19-DES-2014, The number of retries has been incremented to 6 (retries seem to work so we try one more time)
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
      
      EXECUTE dbo.Trx_3GS_EndCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @SessionId,
                         @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                         @CreditBalance,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END -- WHILE
  
  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR) 
  
  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                          @SessionId, @status_code, @CreditBalance, 1,
                                          @input, @output

  COMMIT TRANSACTION

  -- AJQ 21-AUG-2014, Always return 0 "Success" to the caller.
  --                  This is to avoid retries from the client side.
  --                  When an exception has occurred, we will return status=4
  IF (@_exception = 0)
    SET  @status_code = 0

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO
