--------------------------------------------------------------------------------
-- Copyright � 2012 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME : WSP_PlayerInfo.sql
--
--   DESCRIPTION : Procedure for PlayerInfo.
--
--        AUTHOR: Andreu Juli�
--
-- CREATION DATE: 20-NOV-2012
--
-- REVISION HISTORY :
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 20-NOV-2012 AJQ    First release.
-- 14-DEC-2012 RCI    Added SP WSP_PlayerQuerySpentYesterday, WSP_PlayerResetSpentYesterday and WSP_PlayerAddNonRedeemableAmount.
--                    Also added VendorID control and Try/Catch block in SP WSP_PlayerInfo.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Check Conditions
-- 
--  PARAMS:
--      - INPUT:
--        @pVendorID    NVARCHAR(16)
--        @pTrackData   NVARCHAR(24)
--        @pCheckWCP    BIT
--
--      - OUTPUT:
--        @pStatusCode   INT
--        @pStatusText   NVARCHAR(256)
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_CheckConditions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_CheckConditions]
GO
CREATE PROCEDURE [dbo].[WSP_CheckConditions]
  @pVendorID       NVARCHAR(16),
  @pAccountID      BIGINT,
  @pCheckWCP       BIT,
  @pStatusCode     INT           OUTPUT,
  @pStatusText     NVARCHAR(256) OUTPUT
AS
BEGIN
  DECLARE @auth_vendor      AS BIT
  DECLARE @blocked          AS BIT
  DECLARE @wcp_num_running  AS INT

  SET @pStatusCode = 4
  SET @pStatusText = N'Error'

  -- Check if vendor is authorized
  --
  SELECT   @auth_vendor  = WAV_AUTHORIZED
    FROM   WSP_AUTHORIZED_VENDORS
   WHERE   WAV_VENDOR_ID = @pVendorID
 
  SET @auth_vendor = ISNULL(@auth_vendor, 0)
  IF @auth_vendor = 0
  BEGIN
    SET @pStatusCode = 1
    SET @pStatusText = N'Vendor Not Authorized'
  
    RETURN
  END
 
  -- Check if account exists
  -- 
  SET @pAccountID = ISNULL(@pAccountID, 0)
  IF @pAccountID <= 0
  BEGIN
    SET @pStatusCode = 2
    SET @pStatusText = N'Account Not Found'

    RETURN
  END

  -- Check if account is blocked
  --
  SELECT   @blocked      = AC_BLOCKED
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountID

  SET @blocked = ISNULL(@blocked, 1)
  IF @blocked = 1
  BEGIN
    SET @pStatusCode = 3
    SET @pStatusText = N'Account Blocked'

    RETURN
  END

  SET @pCheckWCP = ISNULL(@pCheckWCP, 1)
  IF @pCheckWCP = 0
  BEGIN
    SET @pStatusCode = 0
    SET @pStatusText = N'Success'

    RETURN
  END

  -- Check if the WCP thread is on.
  SELECT   @wcp_num_running = COUNT(SVC_STATUS)
    FROM   SERVICES
   WHERE   SVC_STATUS       = 'RUNNING'
     AND   SVC_PROTOCOL     = 'WCP'

  IF @wcp_num_running = 0
  BEGIN
    SET @pStatusCode = 4
    SET @pStatusText = N'Can not add NR amount. WCP Service not running.'

    RETURN
  END

  SET @pStatusCode = 0
  SET @pStatusText = N'Success'

END -- WSP_CheckConditions

GO

--------------------------------------------------------------------------------
-- PURPOSE : Returns personal data, information about the Wigos loyalty program and activity information
--           from the customer.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--          PlayerName
--          Gender
--          Birthdate
--          Level
--          LevelName
--          Points
--          LastActivity
--          PlayedToday
--          WonToday
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerInfo]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerInfo]
    @pVendorID   NVARCHAR(16),
    @pTrackData  NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @pStatusCode    AS INT
  DECLARE @pStatusText    AS NVARCHAR(256)

  DECLARE @pBlocked       AS BIT
  DECLARE @pPlayed        AS MONEY
  DECLARE @pWon           AS MONEY
  DECLARE @pToday         AS DATETIME
  DECLARE @pSiteId        AS INT
  DECLARE @pAccountId     AS BIGINT
  DECLARE @pHolderLevel   AS INT
  DECLARE @pLevelPrefix   AS NVARCHAR (50)
  DECLARE @pLevelName     AS NVARCHAR (50)
  DECLARE @auth_vendor    AS BIT
  DECLARE @pPointsToday   AS MONEY
  DECLARE @bucket_puntos_canje AS INT

  SET @pStatusCode = 4
  SET @pStatusText = N'Error'
  SET @bucket_puntos_canje = 1

  BEGIN TRY
  
    -- Check if vendor is authorized
    --
    SELECT   @auth_vendor  = WAV_AUTHORIZED
      FROM   WSP_AUTHORIZED_VENDORS
     WHERE   WAV_VENDOR_ID = @pVendorID

    SET @auth_vendor = ISNULL(@auth_vendor, 0)
    IF @auth_vendor = 0
    BEGIN
      SET @pStatusCode = 1
      SET @pStatusText = N'Vendor Not Authorized'

      GOTO END_PROCEDURE
    END

    SET @pAccountId = ( SELECT AC_ACCOUNT_ID From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@pTrackData))
    SET @pAccountId = ISNULL (@pAccountId, 0)

    SET @pStatusCode = 2
    SET @pStatusText = N'Account Not Found'

    IF ( @pAccountId > 0 )
    BEGIN

      SET @pStatusCode = 3
      SET @pStatusText = N'Account Blocked'

      SELECT @pBlocked = AC_BLOCKED, @pHolderLevel = AC_HOLDER_LEVEL FROM ACCOUNTS WHERE ac_account_id = @pAccountId

      IF ( @pHolderLevel = 1 )      SET @pLevelPrefix = 'Level01'
      ELSE IF ( @pHolderLevel = 2 ) SET @pLevelPrefix = 'Level02'
      ELSE IF ( @pHolderLevel = 3 ) SET @pLevelPrefix = 'Level03'
      ELSE IF ( @pHolderLevel = 4 ) SET @pLevelPrefix = 'Level04'
      ELSE SET @pLevelPrefix = NULL

      IF ( @pLevelPrefix IS NOT NULL )
      BEGIN
        SELECT @pLevelName = GP_KEY_VALUE
          FROM GENERAL_PARAMS
         WHERE GP_GROUP_KEY = 'PlayerTracking'
           AND GP_SUBJECT_KEY = @pLevelPrefix + '.Name'
      END

      SET @pSiteId = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                         FROM   GENERAL_PARAMS
                        WHERE   GP_GROUP_KEY   = 'Site'
                          AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )

      SET @pToday = ( SELECT dbo.TodayOpening (@pSiteId) )

      SELECT   @pPlayed       = SUM(PS_PLAYED_AMOUNT)
             , @pWon          = SUM(PS_WON_AMOUNT)
             , @pPointsToday  = SUM(ISNULL(PS_AWARDED_POINTS, 0))
        FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_started))
       WHERE   PS_ACCOUNT_ID  = @pAccountId
         AND   PS_STARTED    >= @pToday

      IF ( @pBlocked = 0 )
      BEGIN
        -- Insert statements for procedure here
        SELECT   0                                                                                                         StatusCode
               , 'Success'                                                                                                 StatusText
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_NAME END                                         PlayerName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_GENDER END                                       Gender
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_BIRTH_DATE END                                   Birthdate
               , AC_HOLDER_LEVEL                                                                                           Level
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE @pLevelName END                                            LevelName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(DBO.GETBUCKETVALUE(@bucket_puntos_canje, AC_ACCOUNT_ID) , 0, 1) AS INT) END Points
               , AC_LAST_ACTIVITY                                                                                          LastActivity
               , ISNULL (@pPlayed, 0)                                                                                      PlayedToday
               , ISNULL (@pWon,    0)                                                                                      WonToday
               , AC_ACCOUNT_ID                                                                                             AccountId
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(ISNULL (@pPointsToday, 0) , 0, 1) AS INT) END  PointsToday

        From ACCOUNTS
        where ac_account_id = @pAccountId

        SET @pStatusCode = 0
        SET @pStatusText = N'Success'

      END
    END

  END TRY
  BEGIN CATCH
    SET @pStatusCode = 4
    SET @pStatusText  = N'Error:'
                      + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                      + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                      + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                      + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                      + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                      + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  IF ( @pStatusCode <> 0 )
    SELECT  @pStatusCode   StatusCode
          , @pStatusText  StatusText
          , NULL PlayerName
          , NULL Gender
          , NULL Birthdate
          , NULL Level
          , NULL LevelName
          , NULL Points
          , NULL LastActivity
          , NULL PlayedToday
          , NULL WonToday
          , NULL AccountId
          , NULL PointsToday

END -- WSP_PlayerInfo

-- Only if exists user wg_wsp, then 'grant execute' to procedure
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_wsp')
  GRANT EXECUTE ON OBJECT::dbo.WSP_PlayerInfo TO wg_wsp;
GO


--------------------------------------------------------------------------------
-- PURPOSE : Obtain the redeemable amount that the customer spent during the previous day.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--          SpentYesterday
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error
--        5: Amount Already Reset

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerQuerySpentYesterday]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerQuerySpentYesterday]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerQuerySpentYesterday]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)
  
  DECLARE @spent            AS MONEY
  DECLARE @account_id       AS BIGINT
  DECLARE @spent_used       AS BIT
  DECLARE @spent_amount     AS MONEY
  DECLARE @site_id          AS INT
  DECLARE @today            AS DATETIME
  DECLARE @yesterday        AS DATETIME
  DECLARE @yesterday_day    AS DATETIME
  DECLARE @rc               AS INT

  SET @spent = 0

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY
  
    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)
        
    SET @account_id = ISNULL(@account_id, 0)
   
    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 0, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux
      
      GOTO END_PROCEDURE
    END

    -- Get yesterday 'day'
    SET @site_id = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                                FROM   GENERAL_PARAMS
                               WHERE   GP_GROUP_KEY   = 'Site'
                                 AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )
    SET @today = dbo.TodayOpening(@site_id)
    SET @yesterday = DATEADD(DAY, -1, @today)
    -- Trunc date to start of the day (00:00h).
    SET @yesterday_day = DATEADD(DAY, DATEDIFF(day, 0, @yesterday), 0)

    -- Check if spent amount is already used
    --
    SELECT   @spent_used    = WPS_USED
           , @spent_amount  = WPS_AMOUNT
      FROM   WSP_PLAYER_SPENT_BY_DAY
     WHERE   WPS_ACCOUNT_ID = @account_id
       AND   WPS_DAY        = @yesterday_day

    SET @spent_used = ISNULL(@spent_used, 0)
    IF @spent_used = 1
    BEGIN
      SET @status_code = 5
      SET @status_text = N'Amount Already Reset'

      GOTO END_PROCEDURE
    END

    -- Check if the spent amount is already calculated
    --
    IF @spent_amount IS NOT NULL
    BEGIN
      SET @spent = @spent_amount
      SET @status_code = 0
      SET @status_text = N'Success'

      GOTO END_PROCEDURE
    END

    -- Calculate the spent amount and save it
    --
    SELECT   @spent = SUM(CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON
                               THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON
                               ELSE 0 END)
      FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))
     WHERE   PS_ACCOUNT_ID  = @account_id
       AND   PS_FINISHED   >= @yesterday
       AND   PS_FINISHED    < @today
       AND   PS_STATUS     <> 0   -- PlaySessionStatus.Opened = 0

    SET @spent = ISNULL(@spent, 0)

    INSERT INTO   WSP_PLAYER_SPENT_BY_DAY
                ( WPS_ACCOUNT_ID
                , WPS_DAY
                , WPS_AMOUNT
                , WPS_USED
                )
         VALUES
                ( @account_id
                , @yesterday_day
                , @spent
                , 0
                )

    SET @rc = @@ROWCOUNT
    IF @rc <> 1
    BEGIN
      SET @status_code = 4
      SET @status_text = N'Error: Can not save the spent amount of ' + CAST(@spent AS NVARCHAR)
                       + N' for account_id ' + CAST(@account_id AS NVARCHAR)
                       + N', day ' + CAST(@yesterday_day AS NVARCHAR)
                       + N'.'

      GOTO END_PROCEDURE
    END

    SET @status_code = 0
    SET @status_text = N'Success'

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText, @spent AS SpentYesterday

END -- WSP_PlayerQuerySpentYesterday

GO

--------------------------------------------------------------------------------
-- PURPOSE : Places a mark in the account to indicate that the customer has already used the amount
--           spent in the previous day.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerResetSpentYesterday]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerResetSpentYesterday]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerResetSpentYesterday]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)  

  DECLARE @account_id       AS BIGINT
  DECLARE @site_id          AS INT
  DECLARE @today            AS DATETIME
  DECLARE @yesterday        AS DATETIME
  DECLARE @yesterday_day    AS DATETIME
  DECLARE @rc               AS INT

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY

    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)

    SET @account_id = ISNULL(@account_id, 0)

    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 0, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux

      GOTO END_PROCEDURE
    END

    -- Get yesterday 'day'
    SET @site_id = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                                FROM   GENERAL_PARAMS
                               WHERE   GP_GROUP_KEY   = 'Site'
                                 AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )
    SET @today = dbo.TodayOpening(@site_id)
    SET @yesterday = DATEADD(DAY, -1, @today)
    -- Trunc date to start of the day (00:00h).
    SET @yesterday_day = DATEADD(DAY, DATEDIFF(day, 0, @yesterday), 0)

    UPDATE   WSP_PLAYER_SPENT_BY_DAY
       SET   WPS_USED       = 1
     WHERE   WPS_ACCOUNT_ID = @account_id
       AND   WPS_DAY        = @yesterday_day

    -- If no row is updated, don't raise an error.
    -- At this point, always return ok.

    SET @status_code = 0
    SET @status_text = N'Success'

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- WSP_PlayerResetSpentYesterday

GO

--------------------------------------------------------------------------------
-- PURPOSE : Add the specified amount of non-redeemable credits to the specified user account.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--          @pNRAmount  MONEY
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerAddNonRedeemableAmount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerAddNonRedeemableAmount]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerAddNonRedeemableAmount]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24),
    @pNRAmount  MONEY
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)  

  DECLARE @account_id       AS BIGINT
  DECLARE @promo_exists     AS BIT
  DECLARE @wcp_num_running  AS INT
  DECLARE @nr_amount        AS MONEY

  DECLARE @time_delay       AS VARCHAR(12)
  DECLARE @timeout          AS VARCHAR(8)
  DECLARE @is_timeout       AS BIT
  DECLARE @recharge_id      AS BIGINT
  DECLARE @start_time       AS DATETIME
  DECLARE @wpr_status       AS INT

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY

    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)

    SET @account_id = ISNULL(@account_id, 0)

    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 1, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux

      GOTO END_PROCEDURE
    END     

    -- Check if received amount is rounded ok.
    --
    SET @nr_amount = ROUND(@pNRAmount, 2)   -- Currency: 2 decimals only!
    IF @nr_amount <> @pNRAmount
    BEGIN
      SET @status_code = 4
      SET @status_text = N'NR Amount must be 2 decimals rounded.'

      GOTO END_PROCEDURE
    END
    
    --
    -- Add the NR amount to the account
    --

    -- Check the EXTERNAL (type = 10) promotion exists
    --
    SELECT   @promo_exists = 1
      FROM   PROMOTIONS
     WHERE   PM_TYPE       = 10 -- EXTERNAL

    SET @promo_exists = ISNULL(@promo_exists, 0)
    IF @promo_exists = 0
    BEGIN
      SET @status_code = 4
      SET @status_text = N'Error: EXTERNAL Promotion does not exist.'

      GOTO END_PROCEDURE
    END


    SET @wpr_status = 1 --- Pending

    -- Insert the request to an intermediate table, so the WCP thread can add the NR amount.
    --
    INSERT INTO   WSP_PLAYER_RECHARGE
                ( WPR_ACCOUNT_ID
                , WPR_NR_AMOUNT
                , WPR_STATUS
                )
         VALUES
                ( @account_id
                , @nr_amount
                , @wpr_status
                )
    SET @recharge_id = SCOPE_IDENTITY()

    --  Wait for the answer from the WCP thread (using WPR_STATUS column)
    --
    SET @start_time = GETDATE()

    SET @time_delay = '00:00:00.500'
    SET @timeout = 30
    SET @is_timeout = 0

    WHILE @wpr_status = 1 --- While 'PENDING'
    BEGIN

      SELECT   @wpr_status   = WPR_STATUS
        FROM   WSP_PLAYER_RECHARGE
       WHERE   WPR_UNIQUE_ID = @recharge_id

      IF @wpr_status <> 1
        BREAK
         
      IF DATEDIFF(second, @start_time, GETDATE()) >= @timeout
      BEGIN
        SET @is_timeout = 1
        BREAK
      END

      WAITFOR DELAY @time_delay
    END

    DECLARE @tmp_table TABLE (RECHARGE_STATUS INT)

    DELETE   WSP_PLAYER_RECHARGE
    OUTPUT   DELETED.WPR_STATUS INTO @tmp_table
     WHERE   WPR_UNIQUE_ID = @recharge_id

    SELECT @wpr_status = RECHARGE_STATUS FROM @tmp_table

    SET @wpr_status = ISNULL(@wpr_status, 3)

    IF @wpr_status = 4  -- Ok
    BEGIN
      SET @status_code = 0
      SET @status_text = N'Success'
    END
    ELSE
    BEGIN
      --- ERRORS
      SET @status_code = 4
      SET @status_text = N'Unknown error. wpr_status: ' + CAST(@wpr_status AS NVARCHAR) + '.'
      IF @wpr_status = 3  -- Error
      BEGIN
        SET @status_text = N'Error processing the recharge.'
      END
      IF @wpr_status = 5 OR @is_timeout = 1 -- Timeout
      BEGIN
        SET @status_text = N'Timeout processing the recharge.'
      END
    END

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- WSP_PlayerAddNonRedeemableAmount

GO

-- This GRANTs go in another file.
--

--GRANT EXECUTE ON OBJECT::dbo.WSP_PlayerQuerySpentYesterday TO wg_wsp;
--GRANT EXECUTE ON OBJECT::dbo.WSP_PlayerResetSpentYesterday TO wg_wsp;
--GRANT EXECUTE ON OBJECT::dbo.WSP_PlayerAddNonRedeemableAmount TO wg_wsp;
