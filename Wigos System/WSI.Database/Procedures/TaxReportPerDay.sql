﻿--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: TaxReportPerDay.sql
-- 
--   DESCRIPTION: Get taxes grouped by day
-- 
--        AUTHOR: JPJ
-- 
-- CREATION DATE: 02-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date         Author  Description
-- -----------  ------  ----------------------------------------------------------
-- 02-DEC-2014  JPJ     First release.
-- 16-JUL-2015  DCS     Add new movements for card pay with all payment types
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_Report_Per_Day]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Tax_Report_Per_Day]
GO

CREATE PROCEDURE [dbo].[Tax_Report_Per_Day]        
                 @pDateFrom DATETIME
               , @pDateTo   DATETIME       
  AS
BEGIN 

  DECLARE @CARD_DEPOSIT_IN                              INT
  DECLARE @CARD_REPLACEMENT                             INT
  DECLARE @CARD_DEPOSIT_IN_CHECK                        INT
  DECLARE @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE            INT
  DECLARE @CARD_DEPOSIT_IN_CARD_CREDIT                  INT
  DECLARE @CARD_DEPOSIT_IN_CARD_DEBIT                   INT
  DECLARE @CARD_DEPOSIT_IN_CARD_GENERIC                 INT
  DECLARE @CARD_REPLACEMENT_CHECK                       INT
  DECLARE @CARD_REPLACEMENT_CARD_CREDIT                 INT
  DECLARE @CARD_REPLACEMENT_CARD_DEBIT                  INT
  DECLARE @CARD_REPLACEMENT_CARD_GENERIC                INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN                    INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT                   INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CHECK              INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE  INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT        INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT         INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CHECK             INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT        INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC      INT

  DECLARE @CASH_IN_SPLIT2                               INT
  DECLARE @MB_CASH_IN_SPLIT2                            INT
  DECLARE @NA_CASH_IN_SPLIT2                            INT
  DECLARE @_DAY_VAR                                     DATETIME 

  SET @CARD_DEPOSIT_IN                                  =   9
  SET @CARD_REPLACEMENT                                 =   28

  SET @CARD_DEPOSIT_IN_CHECK                            =   532
  SET @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE                =   533
  SET @CARD_DEPOSIT_IN_CARD_CREDIT                      =   534
  SET @CARD_DEPOSIT_IN_CARD_DEBIT                       =   535
  SET @CARD_DEPOSIT_IN_CARD_GENERIC                     =   536
  SET @CARD_REPLACEMENT_CHECK                           =   537
  SET @CARD_REPLACEMENT_CARD_CREDIT                     =   538
  SET @CARD_REPLACEMENT_CARD_DEBIT                      =   539
  SET @CARD_REPLACEMENT_CARD_GENERIC                    =   540
  SET @COMPANY_B_CARD_DEPOSIT_IN                        =   541
  SET @COMPANY_B_CARD_REPLACEMENT                       =   543
  SET @COMPANY_B_CARD_DEPOSIT_IN_CHECK                  =   544
  SET @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE      =   545
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT            =   546
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT             =   547
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC           =   548
  SET @COMPANY_B_CARD_REPLACEMENT_CHECK                 =   549
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT           =   550
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT            =   551
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC          =   552

  SET @CASH_IN_SPLIT2                                   =   34
  SET @MB_CASH_IN_SPLIT2                                =   35
  SET @NA_CASH_IN_SPLIT2                                =   55

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TEMP_TABLE') AND type in (N'U'))
  BEGIN                                
    DROP TABLE #TEMP_TABLE  
  END       

  CREATE  TABLE #TEMP_TABLE ( TODAY   DATETIME PRIMARY KEY ) 
  
  IF @pDateTo IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @pDateTo = CAST(GETDATE() AS DATETIME)
  END

  -- TEMP TABLE IS FILLED WITH THE RANGE OF DATES
  SET @_DAY_VAR = @pDateFrom

  WHILE @_DAY_VAR < @pDateTo 
  BEGIN 
     INSERT INTO   #TEMP_TABLE (Today) VALUES (@_DAY_VAR)
     SET @_DAY_VAR =  DATEADD(Day,1,@_DAY_VAR)
  END 
  
  ;
  -- CASHIER MOVEMENTS FILTERED BY DATE AND MOVEMENT TYPES
  WITH MovementsPerWorkingday  AS 
   (
     SELECT   dbo.Opening(0, CM_DATE) 'WorkingDate'
            , CM_TYPE
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
      WHERE   (CM_DATE >= @pDateFrom AND (CM_DATE < @pDateTo))
        AND   CM_SUB_TYPE = 0
              AND  CM_TYPE IN (  @MB_CASH_IN_SPLIT2
                               , @CASH_IN_SPLIT2
                               , @NA_CASH_IN_SPLIT2
                               , @CARD_DEPOSIT_IN
                               , @CARD_REPLACEMENT 
                               , @CARD_DEPOSIT_IN_CHECK                       
                               , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                               , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                               , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                               , @CARD_DEPOSIT_IN_CARD_GENERIC                
                               , @CARD_REPLACEMENT_CHECK                      
                               , @CARD_REPLACEMENT_CARD_CREDIT                
                               , @CARD_REPLACEMENT_CARD_DEBIT                 
                               , @CARD_REPLACEMENT_CARD_GENERIC               
                               , @COMPANY_B_CARD_DEPOSIT_IN                   
                               , @COMPANY_B_CARD_REPLACEMENT                  
                               , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                               , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                               , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                            )
  )

  SELECT   TimeRange.Today WorkingDate
         , ISNULL(((GrossCard - TaxCard) + (GrossCompanyB - TaxCompanyB)),0) BaseTotal
         , ISNULL((TaxCard + TaxCompanyB),0)       TaxTotal
         , ISNULL((GrossCard + GrossCompanyB),0)   GrossTotal
         , ISNULL((GrossCard - TaxCard),0)         BaseCard
         , ISNULL(TaxCard,0)                       TaxCard
         , ISNULL(GrossCard,0)                     GrossCard
         , ISNULL((GrossCompanyB - TaxCompanyB),0) BaseCompanyB         
         , ISNULL(TaxCompanyB,0)                   TaxCompanyB
         , ISNULL(GrossCompanyB,0)                 GrossCompanyB
  FROM (
           SELECT  WorkingDate
           ,
                   SUM(ISNULL(CASE WHEN CM_TYPE IN (  @CARD_DEPOSIT_IN
                                                    , @CARD_REPLACEMENT 
                                                    , @CARD_DEPOSIT_IN_CHECK                       
                                                    , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                    , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                    , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                    , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                    , @CARD_REPLACEMENT_CHECK                      
                                                    , @CARD_REPLACEMENT_CARD_CREDIT                
                                                    , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                    , @CARD_REPLACEMENT_CARD_GENERIC               
                                                    , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                    , @COMPANY_B_CARD_REPLACEMENT                  
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                                                   )  THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN (   @CARD_DEPOSIT_IN
                                                      , @CARD_REPLACEMENT 
                                                      , @CARD_DEPOSIT_IN_CHECK                       
                                                      , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                      , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                      , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                      , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                      , @CARD_REPLACEMENT_CHECK                      
                                                      , @CARD_REPLACEMENT_CARD_CREDIT                
                                                      , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                      , @CARD_REPLACEMENT_CARD_GENERIC               
                                                      , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                      , @COMPANY_B_CARD_REPLACEMENT                  
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC  
                                                    )  THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCompanyB'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCompanyB'
           FROM MovementsPerWorkingday 
           GROUP BY WorkingDate
         ) MOVEMENTS RIGHT JOIN #TEMP_TABLE TimeRange on TimeRange.Today = MOVEMENTS.WorkingDate
   ORDER BY TimeRange.Today ASC       
   
   DROP TABLE #TEMP_TABLE
END
GO

GRANT EXECUTE ON [dbo].[Tax_Report_Per_Day] TO [wggui] WITH GRANT OPTION
GO