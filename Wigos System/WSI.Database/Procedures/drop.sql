DROP PROCEDURE [dbo].[zsp_AccountStatus]
GO
DROP PROCEDURE [dbo].[InsertGameMeters]
GO
DROP PROCEDURE [dbo].[UpdateGameMeters]
GO
DROP PROCEDURE [dbo].[InsertMovement]
GO
DROP PROCEDURE [dbo].[InsertPlaySession]
GO
DROP FUNCTION [dbo].[GetTerminalID]
GO
DROP PROCEDURE [dbo].[InsertTerminal]
GO
DROP FUNCTION [dbo].[GetAccountID]
GO
DROP FUNCTION [dbo].[CheckAccountBlocked]
GO
DROP FUNCTION [dbo].[CheckTerminalBlocked]
GO
DROP PROCEDURE [dbo].[UnlockPlaySession]
GO
DROP FUNCTION [dbo].[IsPlaySessionActive]
GO
DROP FUNCTION [dbo].[CheckTerminalPlaySession]
GO
DROP FUNCTION [dbo].[CheckAccountBalance]
GO
DROP PROCEDURE [dbo].[CheckAndGetPlaySessionData]
GO
DROP PROCEDURE [dbo].[UpdatePlaySessionData]
GO
DROP PROCEDURE [dbo].[UpdateAccountData]
GO
DROP PROCEDURE [dbo].[SessionUpdate_Internal]
GO
DROP PROCEDURE [dbo].[CloseOpenedPlaySession]
GO
DROP PROCEDURE [dbo].[CloseAccountSession]
GO
DROP PROCEDURE [dbo].[zsp_SendEvent]
GO
DROP PROCEDURE [dbo].[zsp_SessionEnd]
GO
DROP PROCEDURE [dbo].[zsp_SessionStart]
GO
DROP PROCEDURE [dbo].[zsp_SessionUpdate]
GO