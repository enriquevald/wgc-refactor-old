--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: InsertPlaySession.sql
-- 
--   DESCRIPTION: Insert new play session procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 30-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-APR-2010 MBF    First release.
-- 21-NOV-2011 RCI    When updating the current play session in accounts, check balance has not been changed.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Inserts the play session
-- 
--  PARAMS:
--      - INPUT:
--        TerminalId 	   int
--        AccountId 	   bigint
--        InitialBalance money
--        Promo          int
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode, 
--      StatusText, 
--      SessionID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertPlaySession]
GO
CREATE PROCEDURE dbo.InsertPlaySession
  	@TerminalId 	  int,
  	@AccountId 	    bigint,
  	@InitialBalance money,
  	@Promo          int
AS
BEGIN
  
  DECLARE @activity_due_to_card_in int  
  DECLARE @ac_promo_balance        money
  DECLARE @ac_balance              money
  DECLARE @playsession_id          int
  DECLARE @status_code             int
	DECLARE @status_text             varchar (254)
  DECLARE @rc                      int
  
  SET @activity_due_to_card_in = 0
  SET @ac_promo_balance        = 0 
  SET @ac_balance              = 0 
  SET @status_code             = 0
	SET @status_text             = 'Successful Session Start'
	SET @playsession_id          = 0

  SELECT   @activity_due_to_card_in = CAST (GP_KEY_VALUE AS INT) 
    FROM   GENERAL_PARAMS 
   WHERE   GP_GROUP_KEY   = 'Play' 
     AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn' 
     
   -- Mark abandoned play session
  UPDATE   PLAY_SESSIONS SET PS_STATUS        = 2 -- Abandoned
         -- AJQ 18-SEP-2010 Don't touch the final balance! , PS_FINAL_BALANCE = PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT + PS_CASH_IN - PS_CASH_OUT  
         , PS_FINISHED      = GETDATE()  
   WHERE   PS_TERMINAL_ID   = @TerminalId  
     AND   PS_STATUS        = 0 -- Opened
     AND   PS_STAND_ALONE   = 0 -- Not stand alone  

  -- Get current balance on Card
  SELECT   @ac_promo_balance       = AC_PROMO_BALANCE
         , @ac_balance             = AC_BALANCE
    FROM   ACCOUNTS
   WHERE  AC_ACCOUNT_ID = @AccountId
       
  IF ( @Promo = 1 )
    BEGIN
      SET @ac_balance = @ac_promo_balance
    END
  
  IF ( @InitialBalance <> @ac_balance )
    BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
	    GOTO ERROR_PROCEDURE
    END
        
  INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID 
                           , PS_TERMINAL_ID
                           , PS_TYPE
                           , PS_TYPE_DATA 
                           , PS_INITIAL_BALANCE 
                           , PS_FINAL_BALANCE 
                           , PS_FINISHED 
                           , PS_STAND_ALONE 
                           , PS_PROMO) 
                     VALUES (@AccountId
                           , @TerminalId
                           , 2            -- 2 = TYPE WIN
                           , NULL
                           , @InitialBalance
                           , @InitialBalance
                           , CASE WHEN (@activity_due_to_card_in = 1) THEN GETDATE() ELSE NULL END
                           , 0
                           , @Promo ) 
                         
  SET @playsession_id = SCOPE_IDENTITY()
   
  IF ( @playsession_id = 0 )
  BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
  -- RCI 21-NOV-2011: Check balance has not been changed.
  UPDATE  ACCOUNTS 
     SET  AC_CURRENT_TERMINAL_ID      = @TerminalId
        , AC_CURRENT_TERMINAL_NAME    = (SELECT TE_NAME FROM TERMINALS WHERE TE_TYPE = 1 AND TE_TERMINAL_ID = @TerminalId)
        , AC_CURRENT_PLAY_SESSION_ID  = @playsession_id 
   WHERE  AC_ACCOUNT_ID               = @AccountId 
     AND  AC_CURRENT_TERMINAL_ID     IS NULL 
     AND  AC_CURRENT_TERMINAL_NAME   IS NULL  
     AND  AC_CURRENT_PLAY_SESSION_ID IS NULL
     AND  AC_BALANCE                  = @InitialBalance
 
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
 ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @playsession_id AS SessionID

END -- InsertPlaySession 

GO