﻿-------------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
-------------------------------------------------------------------------------------
-- 
--   MODULE NAME: SP_AFIP_GetTerminalLastPreviousMeters.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 15-MAR-2018
-- 
-- REVISION HISTORY:
-- 
-- Date        Author      Description
-------------- ----------- ----------------------------------------------------------
-- 15-MAR-2018 JML         Bug 31907:WIGOS-8806 [Ticket #13051] [Ticket#2018030710003365] AFIP - Trámites con Mesa de Ayuda - Sistema Jaza - Apoyo ate
-------------- ----------- ----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetTerminalLastPreviousMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GetTerminalLastPreviousMeters]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GetTerminalLastPreviousMeters]
(
    @pTerminalId                NVARCHAR(50)
  , @pTerminalAFIP              NVARCHAR(4000)
  , @pMaxDateTime               DATETIME
)
AS
BEGIN
  --Meter Types CONSTANTS
  DECLARE @TYPE_DAILY           INT;
  DECLARE @TYPE_FIRST_TIME      INT;

  --Meter Codes CONSTANTS
  DECLARE @METER_COIN_IN        INT;
  DECLARE @METER_COIN_OUT       INT;
  DECLARE @METER_JACKPOTS       INT;
  DECLARE @METER_GAMES_PLAYED   INT;

  --Meter Types
  SET @TYPE_DAILY = 1           --Daily
  SET @TYPE_FIRST_TIME = 12     --First Time

  --Meter Codes
  SET @METER_COIN_IN = 0        --Coin In
  SET @METER_COIN_OUT = 1       --Coin Out
  SET @METER_JACKPOTS = 2       --Jackpots
  SET @METER_GAMES_PLAYED = 5   --Games Played

  -- Get last previous meters
  SELECT @pTerminalId                         as TERMINALID
       , @pTerminalAFIP                       as REGISTRATION_CODE
       , MIN(th.TSMH_SAS_ACCOUNTING_DENOM)    as SAS_ACCOUNTING_DENOM
       , th.TSMH_DATETIME                     as METER_DATE
       , th.TSMH_METER_CODE                   as METER_CODE
       , @TYPE_DAILY                          as METER_TYPE   --@@todo: verify is correct
       , th.TSMH_METER_FIN_VALUE              as INI_VALUE
       , th.TSMH_METER_FIN_VALUE              as FIN_VALUE
       , NULL                                 as GROUP_ID
       , NULL                                 as METER_ORIGIN
       , NULL                                 as METER_MAX_VALUE
       , MIN(th.TSMH_DATETIME)                as INTERVAL_FROM
       , MAX(th.TSMH_DATETIME)                as INTERVAL_TO
  FROM TERMINAL_SAS_METERS_HISTORY AS th
  INNER JOIN
          (
          SELECT hi.TSMH_TERMINAL_ID    as TERMINAL_ID
               , hi.TSMH_METER_CODE     as METER_CODE
               , MAX(hi.TSMH_DATETIME)  as MAX_DATE
           FROM TERMINAL_SAS_METERS_HISTORY AS hi
          WHERE hi.TSMH_TYPE IN (
                                    @TYPE_DAILY, @TYPE_FIRST_TIME   --We don't need Rollover, Denom change or Service RAM Clear
                                )
            AND hi.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED)
            --Get date of last record previous to @pMaxDateTime
            AND hi.TSMH_DATETIME <= @pMaxDateTime
            --Terminal to filter
            AND hi.TSMH_TERMINAL_ID = @pTerminalId
          GROUP BY hi.TSMH_TERMINAL_ID, hi.TSMH_METER_CODE
          ) mm  on th.TSMH_TERMINAL_ID = mm.TERMINAL_ID
               AND th.TSMH_METER_CODE  = mm.METER_CODE
               AND th.TSMH_DATETIME    = mm.MAX_DATE
  GROUP BY th.TSMH_DATETIME
         , th.TSMH_METER_CODE
         , th.TSMH_METER_FIN_VALUE
  ;
  --@@todo: has to be deleted??
/*
      WHERE th.TSMH_TYPE IN (
                                @TYPE_DAILY, @TYPE_FIRST_TIME
                            )
        AND th.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED)
        --Get date of last record previous to @pMaxDateTime
        AND th.TSMH_DATETIME <= @pMaxDateTime
        --Terminal to filter
        AND th.TSMH_TERMINAL_ID = @pTerminalId
*/
END
GO


GRANT EXECUTE ON [dbo].[SP_AFIP_GetTerminalLastPreviousMeters] TO [WGGUI] WITH GRANT OPTION
GO

