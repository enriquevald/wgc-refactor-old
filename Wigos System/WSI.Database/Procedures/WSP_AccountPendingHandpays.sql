--------------------------------------------------------------------------------
-- Copyright � 2012 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME : WSP_AccountPendingHandpays.sql
-- 
--   DESCRIPTION : Procedures for Handpays
-- 
--        AUTHOR: Andreu Juli�
--
-- CREATION DATE: 27-FEB-2012
--
-- REVISION HISTORY :
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-FEB-2012 AJQ    First release.
-- 06-FEB-2015 MPO    Added status expired for handpay
-- 30-MAR-2016 RAB		Added HP_AMT0 to last select
-- 16-OCT-2017 DPC		Added HP_LEVEL to last select
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE : Obtain the pending handpays for an account
--
--  PARAMS :
--      - INPUT :
--          @AccountId  BigInt
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_AccountPendingHandpays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_AccountPendingHandpays]
GO
CREATE PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
	SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money
  DECLARE @expired as int

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  SET @expired              = 16384 -- 0x4000 Expired

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =   5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 2500 ) SET @TimePeriod = 2500  -- Maximum: 40 h x 60 min/h = 2500 min approximately

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   HP_STATUS_CALCULATED <> @expired
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , ISNULL(HP_AMOUNT, HP_AMT0)
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID  
         , ISNULL(HP_AMT0, HP_AMOUNT)     
         , ISNULL(HP_LEVEL,0) 
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC


  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays
GO

/****** PERMISSIONS ******/
GRANT EXECUTE ON OBJECT::dbo.WSP_AccountPendingHandpays TO wggui;
GO
