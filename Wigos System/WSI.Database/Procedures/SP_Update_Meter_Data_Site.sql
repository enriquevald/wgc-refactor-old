
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Meter_Data_Site]') AND type in (N'P', N'PC'))
--  DROP PROCEDURE [dbo].[SP_Update_Meter_Data_Site]
--GO


CREATE PROCEDURE [dbo].[SP_Update_Meter_Data_Site] (
	@pTableId CHAR(1) -- L (Life), Y (Year), M (Month), W (Week), T (Today)
	,@pEntityId CHAR(1) -- T (Terminales), S (Site), X (Terminal - Site)
	,@pDate DATETIME
	,@pId INT -- Terminal ID or Site Id
	,@pMeterId INT -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
	,@pMeterItem INT -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
	,@pValue DECIMAL(12,3) -- Value to aggregate
	)
AS
BEGIN
/*
	JRC - 20150925
	This SP receives the information from the thread and the calls the _CAL sp maybe many times based on:
	the value in the @pTableId parameter, creating a waterfall efect. If it is T(oday) it will also be called for 
	W(eek), M(onth) and Y. Each of these accumulate the information on several tables (by Day, by Week, by Month and by Year).
	But if the parameter is for example M(onth), then it should be accumulated only for M(onth) and Y(year).
	If the @pEntityId is X, the information goes for T(terminal) and also for S(ite), so it should be processed twice by the _CALC SP

*/




	DECLARE @_EntityId CHAR(1)
	DECLARE @_accion VARCHAR(10)

  SET @_EntityId = @pEntityId
	SET @_accion = CASE 
						WHEN @pTableId = 'L' THEN 'L'
						WHEN @pTableId = 'Y' THEN 'LY'
						WHEN @pTableId = 'M' THEN 'LYM'
						WHEN @pTableId = 'W' THEN 'LYMW'
						ELSE 'LYMWT' END

						
	IF @pEntityId = 'X' BEGIN
		-- first execution for Terminal
		SET @_EntityId = 'T'
	--	IF CHARINDEX('L', @_accion) > 0 Entiendo que el L no se utiliza
	--		EXECUTE SP_Update_Meter_Data_Site_Calc @pTableId,@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
		IF CHARINDEX('Y', @_accion) > 0 
			EXECUTE SP_Update_Meter_Data_Site_Calc 'Y',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
		IF CHARINDEX('M', @_accion) > 0 
			EXECUTE SP_Update_Meter_Data_Site_Calc 'M',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
		IF CHARINDEX('W', @_accion) > 0 
			EXECUTE SP_Update_Meter_Data_Site_Calc 'W',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
		IF CHARINDEX('T', @_accion) > 0 
			EXECUTE SP_Update_Meter_Data_Site_Calc 'T',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
		SET @_EntityId = 'S'
		--next execution for Site
	END				

--	IF CHARINDEX('L', @_accion) > 0 Entiendo que el L no se utiliza
--		EXECUTE SP_Update_Meter_Data_Site_Calc @pTableId,@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
	IF CHARINDEX('Y', @_accion) > 0 
		EXECUTE SP_Update_Meter_Data_Site_Calc 'Y',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
	IF CHARINDEX('M', @_accion) > 0 
		EXECUTE SP_Update_Meter_Data_Site_Calc 'M',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
	IF CHARINDEX('W', @_accion) > 0 
		EXECUTE SP_Update_Meter_Data_Site_Calc 'W',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
	IF CHARINDEX('T', @_accion) > 0 
		EXECUTE SP_Update_Meter_Data_Site_Calc 'T',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
			
			
END

	
	






