IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCreateAllCageMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCreateAllCageMeters]
GO

/****** Object:  StoredProcedure [dbo].[CageCreateAllCageMeters]    Script Date: 09/30/2014 17:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CageCreateAllCageMeters]
	    @pSourceTagetId BIGINT
	  , @pConceptId BIGINT
  AS
BEGIN   
--
-- Para cada registro de la tabla cage_source_target_concepts, crea un contador en la tabla cage_meters.
--
	DECLARE @Currencies VARCHAR(100)
	DECLARE @NationalCurrency VARCHAR(3)
	DECLARE @CurrenciesAccepted VARCHAR(100)
	DECLARE @Current_Currency VARCHAR(3)
	--DECLARE @Current_ConceptId BIGINT
	--DECLARE @Current_SourceTargetId BIGINT
	DECLARE @OnlyNationalCurrency BIT
	
	-- Get national currency
	SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'
           
    -- Get accepted currencies
	SELECT @CurrenciesAccepted = GP_KEY_VALUE FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY = 'RegionalOptions' 
	   AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
    -- Create meters for each currency
	
	SELECT	@OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY
	  FROM  CAGE_SOURCE_TARGET_CONCEPTS
			INNER JOIN  CAGE_CONCEPTS ON CC_CONCEPT_ID = CSTC_CONCEPT_ID
	 WHERE ISNULL(CSTC_ENABLED,0) = 1
	   AND ISNULL(CC_ENABLED,0) = 1
		
	    SET @OnlyNationalCurrency = ISNULL(@OnlyNationalCurrency, 0)
	    
		IF ISNULL(@OnlyNationalCurrency,0) = 1
			SET @Currencies	= @NationalCurrency
		ELSE
			SET @Currencies = @CurrenciesAccepted
		
		-- Split currencies ISO codes
		SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
		
		DECLARE CursIsoCode CURSOR FOR 
		SELECT CURRENCY_ISO_CODE
		  FROM #TMP_CURRENCIES_ISO_CODES 
	      
		SET NOCOUNT ON;

		OPEN CursIsoCode
		FETCH NEXT FROM CursIsoCode INTO @Current_Currency
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			-- CAGE METERS --
				IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
									 FROM CAGE_METERS 
									WHERE CM_SOURCE_TARGET_ID = @pSourceTagetId
									  AND CM_CONCEPT_ID = @pConceptId
									  AND CM_ISO_CODE = @Current_Currency)
				INSERT INTO CAGE_METERS
						  ( CM_SOURCE_TARGET_ID
						  , CM_CONCEPT_ID
						  , CM_ISO_CODE
						  , CM_VALUE
						  , CM_VALUE_IN
						  , CM_VALUE_OUT )
					 VALUES
						  ( @pSourceTagetId
						  , @pConceptId
						  , @Current_Currency
						  , 0
						  , 0
						  , 0 )
						  
			FETCH NEXT FROM CursIsoCode INTO @Current_Currency
		
		END
		DROP TABLE #TMP_CURRENCIES_ISO_CODES
		CLOSE CursIsoCode
		DEALLOCATE CursIsoCode
	
END -- CageCreateAllCageMeters
 