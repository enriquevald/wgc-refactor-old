﻿ --------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GetBucketValue.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
--                    First release.
-- 09-MAY-2016 ETP    Bug 14343: Buckets: Redondeo al transferir promociones redimibles y No redimibles
-- ----------- ------ ----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBucketValue]') AND type in (N'F', N'FN'))
DROP FUNCTION [dbo].[GetBucketValue]
go

CREATE FUNCTION [dbo].[GetBucketValue]  (@BucketId bigint, @AccountId bigint) 
RETURNS DECIMAL(12,2)
AS
BEGIN
  DECLARE @Result DECIMAL(12,2)
  
  
  SELECT @Result  = ROUND ( CBU_VALUE , 2 , 1 )  -- When the third parameter != 0 it truncates rather than rounds
  FROM CUSTOMER_BUCKET 
  WHERE CBU_BUCKET_ID = @BucketId
  AND CBU_CUSTOMER_ID = @AccountId
  
  RETURN ISNULL(@Result ,0)
END -- GetBucketValue

GO

GRANT EXECUTE ON [dbo].[GetBucketValue] TO [wggui] WITH GRANT OPTION