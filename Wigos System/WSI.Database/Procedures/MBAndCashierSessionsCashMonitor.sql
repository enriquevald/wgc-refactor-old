﻿
--------------------------------------------------------------------------------
-- Copyright © 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MBAndCashierSessionsCashMonitor.sql
-- 
--   DESCRIPTION: Report for cash monitor
-- 
--        AUTHOR: David Rigal
-- 
-- CREATION DATE: 30-JUL-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-JUL-2015 DRV    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MBAndCashierSessionsCashMonitor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<David Rigal>
-- Create date: <30-JUL-2015>
-- Description:	<MB And Cashier Sessions Cash Monitor>
-- =============================================
CREATE PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
AS
BEGIN
  SELECT     '0'                             AS ROW_TYPE                   
   	           , ISNULL(GU_USER_ID, '')        AS ROW_ID                   
               , ISNULL(GU_USERNAME, '')       AS USR_NAME                    
               , ISNULL(GU_FULL_NAME, '')      AS USR_FULL_NAME                   
               , ISNULL(GU_EMPLOYEE_CODE, '')  AS EMPLOYEE_CODE                   
               , ISNULL(CS_BALANCE, '')        AS CASHIER_BALANCE                   
               , 0                             AS MB_CASH      
               , 0                             AS MB_RECHARGE_LIMIT
               , 0                             AS MB_NUM_RECHARGES
               , ''                            AS MB_LAST_ACTIVITY
               , CT_NAME                       AS CASHIER_NAME
               , 0                             AS MB_RECHARGE
          FROM   GUI_USERS                            
     LEFT JOIN   CASHIER_SESSIONS                     
            ON   CS_USER_ID = GU_USER_ID     
     
     LEFT JOIN   CASHIER_TERMINALS                     
            ON   CS_CASHIER_ID = CT_CASHIER_ID
                      
         WHERE   CS_STATUS = 0
           AND	 GU_USER_TYPE = 0
         UNION                                        
        
        SELECT   '1'                               			                AS ROW_TYPE  
               , ISNULL(MB_ACCOUNT_ID, '')				                      AS ROW_ID    
               , ISNULL('MB-' + CAST(MB_ACCOUNT_ID AS NVARCHAR), '')  	AS USR_NAME         
               , ISNULL(MB_HOLDER_NAME, '')				                      AS USR_FULL_NAME    
               , ISNULL(MB_EMPLOYEE_CODE, '')				                    AS EMPLOYEE_CODE    
               , 0         						                                  AS CASHIER_BALANCE  
               , ISNULL(MB_PENDING_CASH, 0)				                      AS MB_CASH      
               , ISNULL(MB_BALANCE, 0)					                        AS MB_RECHARGE_LIMIT
               , ISNULL(MB_ACTUAL_NUMBER_OF_RECHARGES, 0)			          AS MB_NUM_RECHARGES
               , ISNULL(MB_LAST_ACTIVITY, '')				                    AS MB_LAST_ACTIVITY
               , CT_NAME                                              	AS CASHIER_NAME
               , MB_CASH_IN                                             AS MB_RECHARGE
          
          FROM  MOBILE_BANKS                           
     LEFT JOIN  CASHIER_SESSIONS                     
            ON	CS_SESSION_ID = MB_CASHIER_SESSION_ID 
     LEFT JOIN  CASHIER_TERMINALS                     
            ON  CS_CASHIER_ID = CT_CASHIER_ID
         WHERE  CS_STATUS = 0
         AND	MB_ACCOUNT_TYPE IN ( 1 , 0 ) 
END

GO

GRANT EXECUTE ON [dbo].[MBAndCashierSessionsCashMonitor] TO [wggui] WITH GRANT OPTION

GO