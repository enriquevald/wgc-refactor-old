--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsOldHistoryByHour.sql
-- 
--   DESCRIPTION: Historicized cashier movements by Hours
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 07-APR-2014 SMN		Create new SP CM_OldHistoryByHour
-- 09-MAR-2017 DHA&RAB Bug 25248: Cashier: Incorrect count to cancel a ticket TITO
-- 07-JUL-2017 EOR		Fixed Bug 28596: WIGOS-3000 Cash summary variance to activity summary report
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CM_OldHistoryByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[CM_OldHistoryByHour]
GO


CREATE PROCEDURE [dbo].[CM_OldHistoryByHour]
      @Date1 DATETIME,
      @CM_CURRENCY_ISO_CODE NVARCHAR(20) = NULL 
      
AS
BEGIN
            DECLARE @Date2   DATETIME

            IF @CM_CURRENCY_ISO_CODE IS NULL BEGIN
                  SELECT @CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
                  WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
            END

            SET @Date1 = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Date1), 0)
            SET @Date2 = DATEADD (HOUR, 1, @Date1)

            DELETE FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE   CM_DATE >= @Date1 AND CM_DATE <  @Date2 AND CM_SUB_TYPE <> 2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

            INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                                    ( CM_DATE
                                   , CM_TYPE
                                   , CM_SUB_TYPE
                                   , CM_TYPE_COUNT
                                   , CM_CURRENCY_ISO_CODE
                                   , CM_CURRENCY_DENOMINATION
                                   , CM_SUB_AMOUNT
                                   , CM_ADD_AMOUNT
                                   , CM_AUX_AMOUNT
                                   , CM_INITIAL_BALANCE
                                   , CM_FINAL_BALANCE
                                   )     
                    SELECT   @Date1 CM_DATE
                                   , CM_TYPE
                                   , 0                             AS CM_SUB_TYPE                  --CAIXER MOVEMENT
                                   , SUM(CASE WHEN CM_UNDO_STATUS IS NULL THEN 1 ELSE 0 END) AS CM_TYPE_COUNT
                                   , ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE) AS CM_CURRENCY_ISO_CODE
                                   , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
                                     , SUM(ISNULL(CM_SUB_AMOUNT,       0)) CM_SUB_AMOUNT
                                   , SUM(ISNULL(CM_ADD_AMOUNT,        0)) CM_ADD_AMOUNT
                                   , SUM(ISNULL(CM_AUX_AMOUNT,        0)) CM_AUX_AMOUNT
                                   , SUM(ISNULL(CM_INITIAL_BALANCE,   0)) CM_INITIAL_BALANCE
                                   , SUM(ISNULL(CM_FINAL_BALANCE,     0)) CM_FINAL_BALANCE
                      
                             FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_CM_DATE_TYPE)) 
                             WHERE   CM_DATE >= @Date1
                                   AND   CM_DATE <  @Date2
                             GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE), ISNULL(CM_CURRENCY_DENOMINATION, 0)
            UNION ALL 
                    SELECT  @Date1                      AS CM_DATE
                        , mbm_type                      AS CM_TYPE 
                         , 1                             AS CM_SUB_TYPE         --MBANK MOVEMENT
                        , COUNT(mbm_type)               AS CM_TYPE_COUNT
                        , @CM_CURRENCY_ISO_CODE         AS CM_CURRENCY_ISO_CODE
                        , 0                             AS CM_CURRENCY_DENOMINATION
                        , SUM(ISNULL(mbm_sub_amount,0)) AS CM_SUB_AMOUNT
                        , SUM(ISNULL(mbm_add_amount,0)) AS CM_ADD_AMOUNT        
                         , 0                             AS CM_AUX_AMOUNT 
                         , 0                             AS CM_INITIAL_BALANCE
                        , 0                             AS CM_FINAL_BALANCE
                      
             FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))
            WHERE   mbm_datetime >= @Date1
                             AND   mbm_datetime <  @Date2
            GROUP BY    mbm_type 
            
RETURN

END--[CM_OldHistoryByHour]
GO

GRANT EXECUTE ON [dbo].CM_OldHistoryByHour TO [wggui] WITH GRANT OPTION
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsOldHistoryByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsOldHistoryByHour]
GO

CREATE PROCEDURE [dbo].[CashierMovementsOldHistoryByHour]
      @HoursToProcess INT OUTPUT
AS
BEGIN
      --DECLARE @Date0   DATETIME
      DECLARE @DateFrom   DATETIME

      DECLARE @MaxHoursPerIteration INT
      
      DECLARE @HoursDiff INT
      DECLARE @Counter INT

      DECLARE @LastHourHistorified DATETIME
      DECLARE @CMLastHourNotHistorified DATETIME
      DECLARE @CMFirstHour DATETIME

      DECLARE @Cs_session_id AS BIGINT

      SET NOCOUNT ON;

      -- Read @MaxHoursPerIteration from General Params
      SELECT  @MaxHoursPerIteration = GP_KEY_VALUE 
                        FROM  GENERAL_PARAMS 
                   WHERE  GP_GROUP_KEY   = 'HistoricalData' 
                         AND  GP_SUBJECT_KEY = 'OldCashierSessions.MaxHoursPerIteration'
            
                  SET @MaxHoursPerIteration = ISNULL(@MaxHoursPerIteration, 24)
      SET @HoursToProcess = -1

      -- Default CURRENCY_ISO_CODE
      DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
      SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
      WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'

      SELECT @LastHourHistorified = MIN(CM_DATE) FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE CM_SUB_TYPE <> 2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

      IF @LastHourHistorified  IS NULL
            RETURN

      SELECT @CMFirstHour = MIN(CM_DATE) FROM CASHIER_MOVEMENTS
      SELECT @CMLastHourNotHistorified = MAX(CM_DATE) FROM CASHIER_MOVEMENTS WHERE CM_DATE < @LastHourHistorified

      SET @HoursDiff = ISNULL(DATEDIFF(HOUR, @CMFirstHour, @CMLastHourNotHistorified),0)
      SET @HoursToProcess = @HoursDiff

      IF @HoursDiff <= 0 
      BEGIN
            IF @CMLastHourNotHistorified IS NOT NULL
            BEGIN
                  SET @HoursDiff = 1
                  SET @HoursToProcess = @HoursDiff
            END
            ELSE
            BEGIN
                  RETURN
            END
      END
      
      IF @HoursDiff > @MaxHoursPerIteration 
            SET @HoursDiff = @MaxHoursPerIteration
      
      SET @Counter = @HoursDiff

      WHILE @Counter >= -1
      BEGIN

            SET @DateFrom = DATEADD (HOUR, -@Counter, @CMLastHourNotHistorified)
                
                EXEC CM_OldHistoryByHour @DateFrom, @_CM_CURRENCY_ISO_CODE

      SET @Counter = @Counter -1
      END

RETURN

END  --CreateCashierMovementsHistoryByHour

