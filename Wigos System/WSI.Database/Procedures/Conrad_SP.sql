--------------------------------------------------------------------------------
-- Copyright � 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--    QUERY NAME: Conrad_SP.sql
--   DESCRIPTION: Creates a set of stored procedures and functions for Conrad
--                  WSP_002_GetSumPlaySessions
--                  WSP_002_GetPlaySessions
--                  WSP_002_CreateAccount
--                  WSP_002_GetCustomerIdFromExternalId
--                  WSP_002_GetCustomerIdFromTrackdata
--                  WSP_002_GetTrackdata
--                  WSP_002_GetExternalId  
--        AUTHOR: Francisco Vicente
-- CREATION DATE: 17-DEC-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 17-DEC-2015 FAV    First release.
-- 23-DEC-2015 MPO    Fixed Bug TFS-7993: Accounts created are blocked
-- 24-DEC-2015 RCI    Fixed Bug TFS-8013: Accounts created doesn't have level
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_external_reference')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_ac_external_reference] ON [dbo].ACCOUNTS 
  (
      ac_external_reference ASC
  )WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

--------------------------------------------------------------------------------
-- PURPOSE: Get play sessions summary
-- 
--  PARAMS:
--      - INPUT:
--        @CustomerId      BIGINT,
--        @StartDateTime  DATETIME,
--        @EndDateTime    DATETIME
--
--      - OUTPUT:
--
-- RETURNS:
--        CustomerId
--        DurationSeconds
--        TotalPlayed
--        TheoreticalTotalWon
--        TotalWon
--        EarnedPoints
--        RePlayed
--        ReWon
--        NrPlayed
--        NrWon
--
--   NOTES: If @CustomerId is NULL is going to return all records
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetSumPlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_GetSumPlaySessions 
GO
CREATE PROCEDURE [dbo].[WSP_002_GetSumPlaySessions]
    @CustomerId     BIGINT,
    @StartDateTime  DATETIME,
    @EndDateTime    DATETIME
  AS
BEGIN

  DECLARE @_theoretical_payout MONEY

  SET @_theoretical_payout = ISNULL ( ( SELECT  CAST (GP_KEY_VALUE AS MONEY)
                                          FROM  GENERAL_PARAMS
                                         WHERE  GP_GROUP_KEY   = 'PlayerTracking'
                                           AND  GP_SUBJECT_KEY = 'TerminalDefaultPayout' ), 0) / 100

  IF  @CustomerId IS NULL
  BEGIN
      SELECT    PS_ACCOUNT_ID                              AS CustomerId
              , SUM(DATEDIFF(s,PS_STARTED,PS_FINISHED))    AS DurationSeconds
              , SUM(PS_TOTAL_PLAYED)                       AS TotalPlayed
              , SUM(PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
              , SUM(PS_TOTAL_WON)                          AS TotalWon
              , SUM(ISNULL(PS_AWARDED_POINTS,0))           AS EarnedPoints
              , SUM(PS_REDEEMABLE_PLAYED)                  AS RePlayed
              , SUM(PS_REDEEMABLE_WON)                     AS ReWon
              , SUM(ISNULL(PS_NON_REDEEMABLE_PLAYED,0))    AS NrPlayed
              , SUM(ISNULL(PS_NON_REDEEMABLE_WON,0))       AS NrWon
        FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status))
  INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
       WHERE    PS_STATUS <> 0
         AND    PS_FINISHED >= @StartDateTime
         AND    PS_FINISHED  < @EndDateTime
    GROUP BY    PS_ACCOUNT_ID 
    ORDER BY    PS_ACCOUNT_ID   
  END
  ELSE
  BEGIN
      SELECT    PS_ACCOUNT_ID                              AS CustomerId
              , SUM(DATEDIFF(s,PS_STARTED,PS_FINISHED))    AS DurationSeconds
              , SUM(PS_TOTAL_PLAYED)                       AS TotalPlayed
              , SUM(PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
              , SUM(PS_TOTAL_WON)                          AS TotalWon
              , SUM(ISNULL(PS_AWARDED_POINTS,0))           AS EarnedPoints
              , SUM(PS_REDEEMABLE_PLAYED)                  AS RePlayed
              , SUM(PS_REDEEMABLE_WON)                     AS ReWon
              , SUM(ISNULL(PS_NON_REDEEMABLE_PLAYED,0))    AS NrPlayed
              , SUM(ISNULL(PS_NON_REDEEMABLE_WON,0))       AS NrWon
        FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_account_id_finished))
  INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
       WHERE    PS_ACCOUNT_ID = @CustomerId
         AND    PS_STATUS <> 0
         AND    PS_FINISHED >= @StartDateTime
         AND    PS_FINISHED  < @EndDateTime
    GROUP BY    PS_ACCOUNT_ID 
    ORDER BY    PS_ACCOUNT_ID   
  END

END  -- WSP_002_GetSumPlaySessions
                                                                                       
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetSumPlaySessions] TO [wg_datareader] WITH GRANT OPTION
  END
GO


--------------------------------------------------------------------------------
-- PURPOSE: Get play sessions detail by Customer
-- 
--  PARAMS:
--      - INPUT:
--        @CustomerId      BIGINT,
--        @StartDateTime  DATETIME,
--        @EndDateTime    DATETIME
--
--      - OUTPUT:
--
-- RETURNS:
--        PlaySessionId
--        Started
--        Finished
--        DurationSeconds
--        TotalPlayed
--        TheoreticalTotalWon
--        TotalWon
--        EarnedPoints
--        RePlayed
--        ReWon
--        NrPlayed
--        NrWon
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetPlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_GetPlaySessions 
GO
CREATE PROCEDURE [dbo].[WSP_002_GetPlaySessions]
    @CustomerId      BIGINT,
    @StartDateTime  DATETIME,
    @EndDateTime    DATETIME
  AS
BEGIN
  DECLARE @_theoretical_payout MONEY

  SET @_theoretical_payout = ISNULL ( ( SELECT  CAST (GP_KEY_VALUE AS MONEY)
                                          FROM  GENERAL_PARAMS
                                         WHERE  GP_GROUP_KEY   = 'PlayerTracking'
                                           AND  GP_SUBJECT_KEY = 'TerminalDefaultPayout' ), 0) / 100

    SELECT    PS_PLAY_SESSION_ID                  AS PlaySessionId
            , PS_STARTED                          AS Started
            , PS_FINISHED                         AS Finished
            , DATEDIFF(s,PS_STARTED,PS_FINISHED)  AS DurationSeconds
            , PS_TOTAL_PLAYED                     AS TotalPlayed
            , (PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
            , PS_TOTAL_WON                        AS TotalWon
            , PS_AWARDED_POINTS                   AS EarnedPoints
            , PS_REDEEMABLE_PLAYED                AS RePlayed
            , PS_REDEEMABLE_WON                   AS ReWon
            , PS_NON_REDEEMABLE_PLAYED            AS NrPlayed
            , PS_NON_REDEEMABLE_WON               AS NrWon
      FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_account_id_finished))
INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID      
     WHERE    PS_ACCOUNT_ID = @CustomerId
       AND    PS_STATUS <> 0
       AND    PS_FINISHED >= @StartDateTime 
       AND    PS_FINISHED  < @EndDateTime

END  -- WSP_002_GetPlaySessions
                                                                                       
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetPlaySessions] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------
-- PURPOSE: Create an Account
-- 
--  PARAMS:
--      - INPUT:
--        @ExternalPlayerId    BIGINT,
--        @Name1              NVARCHAR (50),
--        @Name2              NVARCHAR (50),
--        @LastName1          NVARCHAR (50),
--        @LastName2          NVARCHAR (50),
--        @DocType            INT, 
--        @DocId              NVARCHAR (20),
--        @DateOfBirth        DATETIME,
--        @Gender             INT,
--        @TrackData          NVARCHAR (50),
--
--      - OUTPUT:
--        @AccountId          BIGINT
--        @StatusCode         INT
--        @StatusText         NVARCHAR (254)
--
-- RETURNS:
--
--   NOTES: 
--        Allowed values for @DocType: 'Otro' = 110, 'CI' = 111, 'CPF' = 112, 'DNI' = 113, 'Licencia C�vica' = 114, 'Licencia de Conductor' = 115, 'Pasaporte' = 116, 'RG' = 117
--        Allowed values for @Gender:   Male = 1, Female = 2
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_CreateAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_CreateAccount 
GO
CREATE PROCEDURE [dbo].[WSP_002_CreateAccount]
    @ExternalPlayerId   BIGINT,
    @Name1              NVARCHAR (50),
    @Name2              NVARCHAR (50),
    @LastName1          NVARCHAR (50),
    @LastName2          NVARCHAR (50),
    @DocType            INT,
    @DocId              NVARCHAR (20),
    @DateOfBirth        DATETIME,
    @Gender             INT,
    @TrackData          NVARCHAR (50),
    @AccountId          BIGINT OUTPUT,
    @StatusCode         INT OUTPUT,
    @StatusText         NVARCHAR (254) OUTPUT
  AS
BEGIN
  DECLARE @_holder_name         NVARCHAR (254)
  DECLARE @_internal_track_data NVARCHAR(50)
  
  SET @AccountId = 0
  SET @StatusCode = 2
  SET @StatusText = 'Unexpected error'

  ---------------------------------------------------------
  -- Parameters Validations
  ---------------------------------------------------------
  
  ---------------------------------------------------------
  SET @ExternalPlayerId = ISNULL(@ExternalPlayerId, 0)
  SET @Name1 = ISNULL(RTRIM(LTRIM(@Name1)), '')
  SET @LastName1 = ISNULL(RTRIM(LTRIM(@LastName1)), '')
  SET @DocId = ISNULL(RTRIM(LTRIM(@DocId)), '')
  SET @DocType = ISNULL(@DocType, 0)
  SET @Gender = ISNULL(@Gender, -1)

  IF (@ExternalPlayerId <= 0)
  BEGIN
    SET @StatusCode = 10
    SET @StatusText = 'The @ExternalPlayerId parameter is mandatory and should be different to 0'
    GOTO ERROR_PROCEDURE
  END 

  IF (@Name1 = '')
  BEGIN
    SET @StatusCode = 11
    SET @StatusText = 'The @Name1 parameter is mandatory and should be different to empty'
    GOTO ERROR_PROCEDURE
  END 

  IF (@LastName1 = '')
  BEGIN
    SET @StatusCode = 12
    SET @StatusText = 'The @LastName1 parameter is mandatory and should be different to empty'
    GOTO ERROR_PROCEDURE
  END 
     
  IF (@DocId = '')
  BEGIN
    SET @StatusCode = 13
    SET @StatusText = 'The @DocId parameter is mandatory and should be different to empty'
    GOTO ERROR_PROCEDURE
  END  

  IF (@DateOfBirth IS NULL)
  BEGIN
    SET @StatusCode = 14
    SET @StatusText = 'The @DateOfBirth parameter is mandatory'
    GOTO ERROR_PROCEDURE
  END  
 
  IF (@DocType < 110 OR @DocType > 117)
  BEGIN
    SET @StatusCode = 15
    SET @StatusText = 'The @DocType parameter is invalid'
    GOTO ERROR_PROCEDURE
  END 

  IF (@Gender < 1 OR @Gender > 2)
  BEGIN
    SET @StatusCode = 16
    SET @StatusText = 'The @Gender parameter is invalid'
    GOTO ERROR_PROCEDURE
  END 
  ---------------------------------------------------------

  ---------------------------------------------------------
  -- Data Validations
  ---------------------------------------------------------

  IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_EXTERNAL_REFERENCE = CAST(@ExternalPlayerId AS NVARCHAR(50)))
  BEGIN
    SET @StatusCode = 30
    SET @StatusText = 'The @ExternalPlayerId is assgined to another customer'
    GOTO ERROR_PROCEDURE
  END

  IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_holder_id)) WHERE AC_HOLDER_ID = @DocId AND AC_HOLDER_ID_TYPE = @DocType )
  BEGIN
    SET @StatusCode = 31
    SET @StatusText = 'The document @DocId is assgined to another customer'
    GOTO ERROR_PROCEDURE
  END

  IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_holder_birth_date)) 
                     WHERE AC_HOLDER_BIRTH_DATE = @DateOfBirth 
                       AND AC_HOLDER_NAME3 = @Name1 
                       AND AC_HOLDER_NAME4 = @Name2 
                       AND AC_HOLDER_NAME1 = @LastName1 
                       AND AC_HOLDER_NAME2 = @LastName2 
                       AND AC_HOLDER_GENDER = @Gender)
  BEGIN
    SET @StatusCode = 32
    SET @StatusText = 'A customer with this data already exists'
    GOTO ERROR_PROCEDURE
  END

  ---------------------------------------------------------
  -- Create account
  ---------------------------------------------------------

  BEGIN TRY
    -- Validate TrackData
    SET @_internal_track_data = ''

    IF @TrackData is not null and @TrackData <> ''
    BEGIN
      SET @_internal_track_data = dbo.TrackDataToInternal(@TrackData)
      IF @_internal_track_data = ''
      BEGIN
        SET @StatusCode = 33
        SET @StatusText = 'Trackdata is not a valid Wigos card'
        GOTO ERROR_PROCEDURE
      END
    END

    -- Build holder name
    IF @Name1 <> ''
      SET @_holder_name = @Name1
    IF @Name2 <> ''
      SET @_holder_name = @_holder_name + ' ' + @Name2
    IF @LastName1 <> ''
      SET @_holder_name =  @_holder_name + ' ' + @LastName1
    IF @LastName2 <> ''
      SET @_holder_name =  @_holder_name + ' ' + @LastName2

    BEGIN TRANSACTION
        
    -- Create account     
    EXECUTE CreateAccount @AccountId OUTPUT

    IF (@_internal_track_data = '')
    BEGIN
        SET @_internal_track_data =  '00000000000000000000-RECYCLED-VIRTUAL-' + CONVERT(VARCHAR(19), @AccountId)
    END

    -- Update account
    UPDATE    ACCOUNTS
       SET    AC_EXTERNAL_REFERENCE   = CAST(@ExternalPlayerId AS NVARCHAR(50))
            , AC_HOLDER_NAME3         = @Name1
            , AC_HOLDER_NAME4         = @Name2
            , AC_HOLDER_NAME1         = @LastName1
            , AC_HOLDER_NAME2         = @LastName2
            , AC_HOLDER_NAME          = @_holder_name
            , AC_HOLDER_ID_TYPE       = @DocType
            , AC_HOLDER_ID            = @DocId
            , AC_HOLDER_BIRTH_DATE    = @DateOfBirth
            , AC_HOLDER_GENDER        = @Gender
            , AC_TRACK_DATA           = @_internal_track_data
            , AC_BLOCKED              = 0
            , AC_USER_TYPE            = 1
            , AC_HOLDER_LEVEL         = 1
            , AC_HOLDER_LEVEL_ENTERED = GETDATE()
            , AC_POINTS_STATUS        = 0
     WHERE    AC_ACCOUNT_ID           = @AccountId

    COMMIT TRANSACTION

    SET @StatusCode = 0
    SET @StatusText = 'Success'

  END TRY
  BEGIN CATCH
    ROLLBACK
    SET @StatusCode = 1
    SET @StatusText = 'EXCEPTION in WSP_002_CreateAccount. Operation canceled! ERROR: ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX))
  END CATCH

  ERROR_PROCEDURE:
  
END  -- WSP_002_CreateAccount                                                                                  

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_CreateAccount] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------
-- PURPOSE: Return a Customer ID
-- 
--  PARAMS:
--      - INPUT:
--        @ExternalPlayerId       Bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetCustomerIdFromExternalId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetCustomerIdFromExternalId]
GO
CREATE FUNCTION dbo.WSP_002_GetCustomerIdFromExternalId
 (@ExternalPlayerId       Bigint) 
RETURNS Bigint
AS
BEGIN

  RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_EXTERNAL_REFERENCE = CAST(@ExternalPlayerId AS NVARCHAR(50))),0))
  
END -- WSP_002_GetCustomerIdFromExternalId

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetCustomerIdFromExternalId] TO [wg_datareader] WITH GRANT OPTION
  END
GO
--------------------------------------------------------------------------------
-- PURPOSE: Return a Customer ID
-- 
--  PARAMS:
--      - INPUT:
--        @Trackdata       NVarchar(40)
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetCustomerIdFromTrackdata]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetCustomerIdFromTrackdata]
GO
CREATE FUNCTION dbo.WSP_002_GetCustomerIdFromTrackdata
 (@Trackdata       NVarchar(40)) 
RETURNS Bigint
AS
BEGIN
    
  RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WITH (INDEX(IX_track_data)) WHERE AC_TRACK_DATA = dbo.TrackDataToInternal(@Trackdata)),0))
  
END -- WSP_002_GetCustomerIdFromTrackdata

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetCustomerIdFromTrackdata] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------
-- PURPOSE: Return Track Data
-- 
--  PARAMS:
--      - INPUT:
--        @CustomerId       Bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      Track Data
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetTrackdata]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetTrackdata]
GO
CREATE FUNCTION dbo.WSP_002_GetTrackdata
 (@CustomerId       BigInt) 
RETURNS NVarchar(40)
AS
BEGIN

  RETURN (ISNULL((SELECT  dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WITH (INDEX(PK_accounts)) WHERE AC_ACCOUNT_ID = @CustomerId),''))
  
END -- WSP_002_GetTrackdata

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetTrackdata] TO [wg_datareader] WITH GRANT OPTION
  END
GO
--------------------------------------------------------------------------------
-- PURPOSE: Return External ID 
-- 
--  PARAMS:
--      - INPUT:
--        @CustomerId       Bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      External ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetExternalId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetExternalId]
GO
CREATE FUNCTION dbo.WSP_002_GetExternalId
 (@CustomerId       BigInt) 
RETURNS Bigint
AS
BEGIN

  DECLARE @_external_reference NVARCHAR(50)
  SELECT @_external_reference = AC_EXTERNAL_REFERENCE FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_ACCOUNT_ID = @CustomerId
  
  IF (@_external_reference IS NULL OR ISNUMERIC (@_external_reference) <> 1) RETURN 0

  RETURN CAST(@_external_reference AS BIGINT)
  
END -- WSP_002_GetExternalId

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[WSP_002_GetExternalId] TO [wg_datareader] WITH GRANT OPTION
  END
GO
