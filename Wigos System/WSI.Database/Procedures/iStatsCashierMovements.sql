 
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: iStatsCashierMovements.sql
-- 
--   DESCRIPTION: Procedure for iStat Cashier Movements 
-- 
--        AUTHOR: Jordi Masachs 
-- 
-- CREATION DATE: 24-NOV-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 24-NOV-2014 JMV    First release
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Show cashier movements grouped by day, week or month
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom	   DATETIME     
--   @pDateTo		   DATETIME   
--   @pGrouped     NVARCHAR ('D'/'W'/'M')  
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[iStatsCashierMovements]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[iStatsCashierMovements]
GO  

CREATE PROCEDURE [dbo].[iStatsCashierMovements]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pGrouped NVARCHAR
  
AS
  
BEGIN  

	DECLARE @pConcatDateFrom DateTime
	DECLARE @pConcatDateTo DateTime
	DECLARE @pConcatDate2007 DateTime
	DECLARE @pOH INT
	DECLARE @NationalCurrency VARCHAR(3)
	
	SET @pConcatDateFrom = dbo.ConcatOpeningTime(0, @pDateFrom)
	SET @pConcatDateTo = dbo.ConcatOpeningTime(0, @pDateTo)
	SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')
	SET @pOH = DATEDIFF (HOUR, '2007/01/01', @pConcatDate2007)
	
	-- Get national currency
	SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'	
	
	
	SELECT   BASE_DATE
		   , TOTAL_CASH_IN
		   , TOTAL_OUTPUTS - PRIZE_TAX AS TOTAL_CASH_OUT
		   , PRIZE_TAX + TAXES AS PRIZE_TAXES
		   , TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES - CASH_IN_TAXES AS RESULT_CASHIER
	  FROM   (
			  SELECT  
				CASE @pGrouped
					WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
					WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
					WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
				END AS BASE_DATE			   
				 , ( SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86, 142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN CM_TYPE IN (78, 79, 92,  147, 148, 152, 153, 154, 155) THEN CM_SUB_AMOUNT ELSE 0 END)
                   + SUM(CASE WHEN (CM_TYPE = 1000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) ) AS TOTAL_CASH_IN
                 , ( SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70, 143, 302, 620) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN (CM_TYPE = 2000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END)) AS TOTAL_OUTPUTS
                 , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                 , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
                 , SUM(CASE WHEN CM_TYPE IN (142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS CASH_IN_TAXES
				FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR WITH (INDEX([PK_cashier_movements_grouped_by_hour]))
			   WHERE   CM_DATE <  @pConcatDateTo
				 AND   CM_DATE >= @pConcatDateFrom
				 AND   CM_TYPE in ( 6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102, 142, 143, 146, 147, 148, 152, 153, 154, 155, 302, 620, 1000000, 2000000)
				 AND   CM_SUB_TYPE = 0
			  GROUP BY 
			  	CASE @pGrouped
					WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
					WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
					WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
				END 
			 
			 ) X
	 WHERE   TOTAL_CASH_IN > 0
		OR   TOTAL_OUTPUTS > 0
		OR   PRIZE_TAX   > 0
		OR   TAXES > 0
	ORDER BY BASE_DATE

END
GO
