﻿--/**** GENERAL PARAM *****/
--IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxUnbalanceInCents')
--    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.DrawTicket', 'ToGiveDrawNumbers.MaxUnbalanceInCents', '0');
--GO

--IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxBetAmountAverageInCents')
--    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.DrawTicket', 'ToGiveDrawNumbers.MaxBetAmountAverageInCents', '0');
--GO


--/******  Alarms  *******/

---- Importante: Las alarmas se deben insertar en el site y en el Multisite
--IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262162 AND [alcg_language_id] = 10)
--BEGIN
--  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
--    VALUES ( 262162, 10, 0, N'Sesión de juego descuadrada', N'Sesión de juego descuadrada', 1)
--END
--GO

--IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262162 AND [alcg_language_id] = 9)
--BEGIN
--  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
--    VALUES ( 262162,  9, 0, N'Play Session unbalance', N'Play Session unbalance', 1)
--END 
--GO

--IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262162 AND [alcc_category] = 40) 
--BEGIN
--  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
--    VALUES ( 262162, 40, 0, GETDATE() )
--END
--GO


------/******* Selects para customer service  *******/

------/******* Max & Avg, unbalance from unbalance sessions  *******/
------SELECT   MAX (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON)))    AS MAX_UNBALANCE
------       , AVG (ABS((PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON)))   AS AVG_UNBALANCE
------  FROM   PLAY_SESSIONS
------ WHERE   PS_FINISHED > DATEADD(YEAR, -1, GETDATE())
------   AND   PS_PLAYED_COUNT <> 0
------   AND   (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON)) > 0)

------/******* Max & Avg, average bet from balance sessions  *******/
------SELECT   MAX (ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT))                                            AS MAX_AVG_SESSION_BET_AMOUNT
------       , AVG (ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT))                                            AS AVG_AVG_SESSION_BET_AMOUNT
------  FROM   PLAY_SESSIONS
------ WHERE   PS_FINISHED > DATEADD(YEAR, -1, GETDATE())
------   AND   PS_PLAYED_COUNT <> 0
------   AND   (ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT)) = 0)

 
--------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GetSpentGroupByTerminal.sql
-- 
--   DESCRIPTION: Retrive play sessions for draws
-- 
--        AUTHOR: José Martínez López
-- 
-- CREATION DATE: 20-DEC-2016
-- 
-- REVISION HISTORY:
-- 
-- Date        Author  Description
-- ----------- ------  ----------------------------------------------------------
-- 20-DEC-2016 JML     First release.
-- 24-JAN-2017 DHA&RAB Bug 23383: Cashier: Draws aren't generated when the game session has 0 plays.
--------------------------------------------------------------------------------

/******* PROCEDURES *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSpentGroupByTerminal]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetSpentGroupByTerminal]
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetSpentGroupByTerminal]  
       @pAccountId as BigInt
     , @pLowerLimitDatetime as DateTime
     , @pPlaySessionOpened as Int = 0
AS
BEGIN

  DECLARE @MaxUnbalanceInCents as decimal(19,3)
  DECLARE @MaxBetAmountAverageInCents as decimal(19,3)
  SET NOCOUNT ON;

  SELECT @MaxUnbalanceInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxUnbalanceInCents'

  SELECT @MaxBetAmountAverageInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxBetAmountAverageInCents'
                                                         
      SELECT   TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime) AS STARTED 
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime) AS FINISHED 
             , SUM (PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS TOTAL_PLAYED                                      
             , SUM (PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS REDEEMABLE_PLAYED                                 
             , SUM (CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS TOTAL_SPENT                                       
             , SUM (CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS REDEEMABLE_SPENT                                  
             , SUM (PS_INITIAL_BALANCE) AS BAL_INI
             , SUM (PS_FINAL_BALANCE) AS BAL_FIN
             , SUM (PS_WON_AMOUNT) AS WON_AMOUNT
             , SUM (PS_PLAYED_COUNT) AS NUM_PLAYS
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))          AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
             , @MaxUnbalanceInCents as a
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))       < @MaxUnbalanceInCents OR @MaxUnbalanceInCents = 0)
         AND   (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END < @MaxBetAmountAverageInCents OR @MaxBetAmountAverageInCents = 0)
    GROUP BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))      
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END


      SELECT   PS_PLAY_SESSION_ID
             , PS_ACCOUNT_ID
             , TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , PS_STARTED
             , PS_FINISHED
             , PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END      AS TOTAL_PLAYED                                      
             , PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END  AS REDEEMABLE_PLAYED                                 
             , CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS TOTAL_SPENT                                       
             , CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS REDEEMABLE_SPENT                                  
             , PS_INITIAL_BALANCE
             , PS_FINAL_BALANCE
             , PS_WON_AMOUNT
             , PS_PLAYED_COUNT
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))          AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (  (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))       >= @MaxUnbalanceInCents AND @MaxUnbalanceInCents > 0)
               OR (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END >= @MaxBetAmountAverageInCents AND @MaxBetAmountAverageInCents > 0))
    ORDER BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   

END
GO

GRANT EXECUTE ON [dbo].[GetSpentGroupByTerminal] TO [wggui] WITH GRANT OPTION
GO

