--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGrouped_Read.sql
-- 
--   DESCRIPTION: Read historied or not historified cashier sessions
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 11-FEB-2014 DLL    Fixed Bug WIG-613: Cashier session detail all with 0 value
-- 10-NOV-2014 DLL    Fixed Bug WIG-1651: Date filter without minutes. 
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Read]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGrouped_Read]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Read]        
         @pCashierSessionId NVARCHAR(MAX)
        ,@pDateFrom DATETIME
        ,@pDateTo DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

	IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
	BEGIN	
	SET @_sql =	'IF EXISTS (SELECT TOP 1 CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN ( ' + @pCashierSessionId + '))  
				BEGIN
	
						SELECT
							 CM_SESSION_ID
							,CM_TYPE
							,CM_SUB_TYPE
							,CM_TYPE_COUNT
							,CM_CURRENCY_ISO_CODE
							,CM_CURRENCY_DENOMINATION
							,CM_SUB_AMOUNT
							,CM_ADD_AMOUNT
							,CM_AUX_AMOUNT
							,CM_INITIAL_BALANCE
							,CM_FINAL_BALANCE
              ,CM_CAGE_CURRENCY_TYPE
						FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN (' + @pCashierSessionId + ')
				END
				ELSE
				BEGIN 
					
					EXEC CashierMovementsGrouped_Select ''' + REPLACE(@pCashierSessionId,'''','''''') + ''',NULL ,NULL
				END	'
				
	  EXEC sp_executesql @_sql
				
	END
	ELSE
	BEGIN	  
		IF DATEPART(MINUTE,@pDateFrom) = 0 AND DATEPART(MINUTE,@pDateTo) = 0 AND (DATEDIFF(HOUR,@pDateFrom,@pDateTo) = (SELECT COUNT(DISTINCT(CM_DATE)) FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo)) AND @pCashierSessionId IS NULL
		BEGIN
			SET @_sql ='
						SELECT
							 NULL
							,CM_TYPE
							,CM_SUB_TYPE
							,CM_TYPE_COUNT							
							,CM_CURRENCY_ISO_CODE
							,CM_CURRENCY_DENOMINATION
							,CM_SUB_AMOUNT
							,CM_ADD_AMOUNT
							,CM_AUX_AMOUNT
							,CM_INITIAL_BALANCE
							,CM_FINAL_BALANCE
              ,CM_CAGE_CURRENCY_TYPE
						FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= ''' +CONVERT(VARCHAR, @pDateFrom, 21) + '''  AND CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
						
	      EXEC sp_executesql @_sql
				
		END
		ELSE
		BEGIN
			  EXEC CashierMovementsGrouped_Select @pCashierSessionId, @pDateFrom, @pDateTo
		END
	END

	
END --CashierMovementsGrouped_Read
GO

GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Read] TO [wggui] WITH GRANT OPTION
GO