--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GameMeters.sql
-- 
--   DESCRIPTION: Game Meters related procedures
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 03-MAY-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-MAY-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

-- INIT GAME_3GS ON PROCEDURE CREATION TIME
IF NOT EXISTS ( SELECT * FROM GAMES WHERE GM_NAME = 'GAME_3GS')
INSERT INTO GAMES (GM_NAME) VALUES ('GAME_3GS')

GO
--------------------------------------------------------------------------------
-- PURPOSE: Insert game meter if not exists
-- 
--  PARAMS:
--      - INPUT:
--         @TerminalID      int
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertGameMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertGameMeters]
GO
CREATE PROCEDURE dbo.InsertGameMeters
	@TerminalID      int
AS
BEGIN

  IF ( (SELECT COUNT(*) FROM GAME_METERS WHERE GM_TERMINAL_ID = @TerminalID AND GM_GAME_BASE_NAME = 'GAME_3GS') = 0 )
  BEGIN
    INSERT INTO GAME_METERS ( GM_TERMINAL_ID 
                            , GM_GAME_BASE_NAME 
                            , GM_DELTA_GAME_NAME
                            , GM_WCP_SEQUENCE_ID 
                            , GM_DENOMINATION 
                            , GM_PLAYED_COUNT 
                            , GM_PLAYED_AMOUNT 
                            , GM_WON_COUNT 
                            , GM_WON_AMOUNT
                            , GM_JACKPOT_AMOUNT
                            , GM_LAST_REPORTED ) 
                   VALUES   ( @TerminalID 
                            , 'GAME_3GS'
                            , 'GAME_3GS'
                            , 0 
                            , 0.01
                            , 0 
                            , 0 
                            , 0 
                            , 0 
                            , 0 
                            , GETDATE() )
  END
 
END -- InsertGameMeters 

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update game meter
-- 
--  PARAMS:
--      - INPUT:
--         @TerminalID         int
--         @DeltaPlayedCount   money
--         @DeltaPlayedAmount  money
--         @DeltaWonCount      money
--         @DeltaWonAmount     money
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateGameMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateGameMeters]
GO
CREATE PROCEDURE dbo.UpdateGameMeters
	@TerminalID         int,
	@DeltaPlayedCount   money,
	@DeltaPlayedAmount  money,
	@DeltaWonCount      money,
	@DeltaWonAmount     money
AS
BEGIN

  DECLARE @status_code             int
	DECLARE @status_text             varchar (254)
  DECLARE @rc                      int

	
  SET @status_code = 0
  SET @status_text = ''
	

  IF EXISTS ( SELECT GM_PLAYED_COUNT FROM GAME_METERS WHERE GM_TERMINAL_ID = @TerminalID AND GM_GAME_BASE_NAME = 'GAME_3GS' )
  BEGIN
    
    UPDATE GAME_METERS SET GM_WCP_SEQUENCE_ID            = 0
                         , GM_DENOMINATION               = 0.01
                         , GM_PLAYED_COUNT               = GM_PLAYED_COUNT         + @DeltaPlayedCount
                         , GM_PLAYED_AMOUNT              = GM_PLAYED_AMOUNT        + @DeltaPlayedAmount
                         , GM_WON_COUNT                  = GM_WON_COUNT            + @DeltaWonCount
                         , GM_WON_AMOUNT                 = GM_WON_AMOUNT           + @DeltaWonAmount
                         , GM_DELTA_PLAYED_COUNT         = GM_DELTA_PLAYED_COUNT   + @DeltaPlayedCount
                         , GM_DELTA_PLAYED_AMOUNT        = GM_DELTA_PLAYED_AMOUNT  + @DeltaPlayedAmount
                         , GM_DELTA_WON_COUNT            = GM_DELTA_WON_COUNT      + @DeltaWonCount
                         , GM_DELTA_WON_AMOUNT           = GM_DELTA_WON_AMOUNT     + @DeltaWonAmount
                         , GM_LAST_REPORTED              = GETDATE()
                   WHERE   GM_TERMINAL_ID                = @TerminalID
                       AND GM_GAME_BASE_NAME             = 'GAME_3GS'
    
    SET @rc = @@ROWCOUNT
    IF ( @rc <> 1 )
    BEGIN
      SET @status_code = 2
      SET @status_text = 'Invalid Machine Information'
    END
  END
  ELSE
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
  END
  
  SELECT @status_code AS StatusCode, @status_text AS StatusText
  
END -- UpdateGameMeters

GO