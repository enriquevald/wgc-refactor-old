﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Coljuegos_ReporteDiario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
GO

CREATE PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
  (@Date DATETIME) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @FechaReporte nvarchar(1024)
  DECLARE @NIT          nvarchar(1024)
  DECLARE @Contrato     nvarchar(1024)
  DECLARE @Codigo       nvarchar(1024)
      
  -- FechaReporte
  SELECT   @FechaReporte = CONVERT(VARCHAR(25), @Date, 112)

  -- NIT
  SELECT   @NIT = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.NIT'
 
  -- Contrato
  SELECT   @Contrato = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.Contrato.Numero'
     
  -- Codigo
  SELECT   @Codigo = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.Municipio.CodigoDANE'     

  -------------------
  -- ReporteDiario --
  -------------------
  SELECT   4             AS NumColumns
         , 'RI'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , (SELECT   gp_key_value
              FROM   general_params
             WHERE   gp_group_key = 'Interface.Coljuegos'
               AND   gp_subject_key = 'Fabricante.Clave') AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
   UNION
  SELECT   3         AS NumColumns
         , 'RC'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NULL      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
   UNION
  SELECT   13                         AS NumColumns
         , 'RD'                       AS Indicador
         , WXM_NUC                    AS Col1
         , WXM_NUID                   AS Col2
         , WXM_SERIAL                 AS Col3
         , ROUND(WXM_COIN_IN, 0, 1)   AS Col4
         , ROUND(WXM_COIN_OUT, 0, 1)  AS Col5
         , ROUND(WXM_JACKPOT, 0, 1)   AS Col6
         , ROUND(WXM_HAND_PAID, 0, 1) AS Col7
         , ROUND(WXM_BILL_IN, 0, 1)   AS Col8
         , WXM_GAMES_PLAYED           AS Col9
         , WXM_PAYOUT                 AS Col10
         , RIGHT('00' + CAST(WXM_EVENT_ID AS VARCHAR), 2) AS Col11
         , CASE WHEN WXM_EVENT_ID > 0 THEN REPLACE(
            REPLACE(
             REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, wxm_datetime, 112), 126)
             , '-', '')
            , 'T', '')
           , ':', '')     ELSE '' END AS Col12
    FROM   WXP_002_MESSAGES
   WHERE   WXM_DATETIME >= @Date
     AND   WXM_DATETIME <  DATEADD(DAY,1,@Date)
   UNION
  SELECT   3         AS NumColumns
         , 'RE'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NUll      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
  UNION  
  SELECT   3             AS NumColumns
         , 'RF'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , NULL          AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
                    
END

GO