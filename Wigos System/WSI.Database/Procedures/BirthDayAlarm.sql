﻿------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2018 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: BirthDayAlarm.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author    Description
-------------- --------- -----------------------------------------------------------------------------------------------------------------------
-- 06-MAR-2018 JML       Fixed Bug 31801:WIGOS-8173 Procedure dbo.BirthDayAlarm is consuming too much disk I/O
------------------------------------------------------------------------------------------------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[BirthDayAlarm]    Script Date: 03/06/2018 10:49:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BirthDayAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[BirthDayAlarm]
GO

CREATE PROCEDURE [dbo].[BirthDayAlarm]
                       @pAccountId BIGINT
                     , @pAlarmType INT OUTPUT
AS
BEGIN
      DECLARE @_birthday                  DATETIME
      DECLARE @_date_from_range           DATETIME
      DECLARE @_today                     DATETIME
      DECLARE @_gp_days_to_remove         INT
      DECLARE @_birthday_alarm_code       INT
      DECLARE @_near_birthday_alarm_code  INT
      DECLARE @_is_leap_year              INT

      SET @pAlarmType = 0
      SET @_today = DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0)
      SET @_birthday_alarm_code = 2097160
      SET @_near_birthday_alarm_code = 2097161

      SELECT   @_birthday = DATEADD(dd, DATEDIFF(dd, 0, AC_HOLDER_BIRTH_DATE ), 0)
        FROM   ACCOUNTS 
       WHERE   AC_ACCOUNT_ID = @pAccountId
       
      SET @_birthday = DATEADD(yy, DATEDIFF(yy, @_birthday, @_today), @_birthday)
       
      IF MONTH(@_birthday) = 2 AND DAY(@_birthday) = 29
      BEGIN
        SELECT @_is_leap_year = DATEPART(MM, DATEADD(DD, 1, CAST((CAST(YEAR(@_today) AS VARCHAR(4)) + '0228') AS DATETIME))) 
        
        IF @_is_leap_year = 0
        BEGIN
          SET @_birthday = DATEADD(dd,-1,@_birthday)
        END
      END
      
      IF @_birthday = @_today
      BEGIN
        /*SEARCH IF ALARMS EXISTS*/
        IF NOT EXISTS ( SELECT   1 
                          FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
                         WHERE   AL_DATETIME   >= @_birthday
                           AND   AL_DATETIME   <  DATEADD(DD, 1, @_birthday)
                           AND   AL_SEVERITY   = 1
                           AND   AL_SOURCE_ID  = @pAccountId
                           AND   AL_ALARM_CODE = @_birthday_alarm_code )
        BEGIN
          SET @pAlarmType = 1
          
          RETURN
        END --IF NOT EXISTS
      END --IF @_birthday = @_today
      
      ELSE
      BEGIN
        SELECT   @_gp_days_to_remove = GP_KEY_VALUE
          FROM   GENERAL_PARAMS
         WHERE   GP_GROUP_KEY = 'Accounts' AND GP_SUBJECT_KEY = 'Player.BirthdayWarningDays'

        SET @_gp_days_to_remove = CAST(@_gp_days_to_remove AS INT)
        SET @_date_from_range = DATEADD(dd,-@_gp_days_to_remove,@_birthday)

        /*Today's date is within range*/

        IF  @_today >=  @_date_from_range
        AND @_today <  @_birthday
        
        BEGIN
          /*SEARCH IF ALARMS EXISTS*/
          IF NOT EXISTS (SELECT   1
                           FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
                          WHERE   AL_DATETIME   >= @_date_from_range 
                            AND   AL_DATETIME   < @_birthday
                            AND   AL_SEVERITY   = 1
                            AND   AL_SOURCE_ID  = @pAccountId 
                            AND   AL_ALARM_CODE = @_near_birthday_alarm_code )
          BEGIN
            SET @pAlarmType=2
          END --IF NOT EXISTS
        END --IF  @_today >=  @_date_from_range AND @_today <  @_birthday
      END --ELSE (IF @_birthday = @_today)
END
GO

GRANT EXECUTE ON [dbo].[BirthDayAlarm] TO [wggui] WITH GRANT OPTION
GO
