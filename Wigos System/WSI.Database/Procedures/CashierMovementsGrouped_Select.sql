--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGrouped_Select.sql
-- 
--   DESCRIPTION: Select cashier sessions
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 11-FEB-2014 DLL    Fixed Bug WIG-613: Cashier session detail all with 0 value
-- 09-MAR-2017 DHA&RAB Bug 25248: Cashier: Incorrect count to cancel a ticket TITO
-- 29-MAR-2017 JBP		Fixed Bug 25289: Reportes contables muestran discrepancias en tickets TITO
-- 07-JUL-2017 EOR		Fixed Bug 28596: WIGOS-3000 Cash summary variance to activity summary report
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Select]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CashierMovementsGrouped_Select]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Select]
        @pCashierSessionId NVARCHAR(MAX)
       ,@pDateFrom DATETIME
       ,@pDateTo DATETIME 
  AS
BEGIN

DECLARE @_sql NVARCHAR(MAX)

DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
	SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
	WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
	SET @_CM_CURRENCY_ISO_CODE = ISNULL(@_CM_CURRENCY_ISO_CODE, 'NULL')

	IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
	BEGIN     
      SET @_sql =  '
      SELECT   CM_SESSION_ID
					,CM_TYPE
          ,0                AS CM_SUB_TYPE
          ,SUM(CASE WHEN CM_UNDO_STATUS IS NULL THEN 1 ELSE 0 END) AS CM_TYPE_COUNT
					,ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''')  AS CM_CURRENCY_ISO_CODE
					,ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
					,SUM(ISNULL(CM_SUB_AMOUNT,0)) CM_SUB_AMOUNT
					,SUM(ISNULL(CM_ADD_AMOUNT,0)) CM_ADD_AMOUNT
					,SUM(ISNULL(CM_AUX_AMOUNT,0)) CM_AUX_AMOUNT
					,SUM(ISNULL(CM_INITIAL_BALANCE,0)) CM_INITIAL_BALANCE
					,SUM(ISNULL(CM_FINAL_BALANCE,0)) CM_FINAL_BALANCE
          ,CM_CAGE_CURRENCY_TYPE
					
			FROM   CASHIER_MOVEMENTS WITH (INDEX (IX_CM_SESSION_ID)) 
      WHERE   CM_SESSION_ID IN ('+@pCashierSessionId+') 
			GROUP BY CM_SESSION_ID, CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''), ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
		 	  
 			  UNION ALL 
         SELECT  mbm_cashier_session_id             AS CM_SESSION_ID
					   , mbm_type                              AS CM_TYPE  
             , 1                     AS CM_SUB_TYPE          
             , COUNT(mbm_type)                 AS CM_TYPE_COUNT     
					   ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
             , 0                     AS CM_CURRENCY_DENOMINATION
             , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
             , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
             , 0                     AS CM_AUX_AMOUNT 
             , 0                     AS CM_INITIAL_BALANCE
             , 0                     AS CM_FINAL_BALANCE
             , 0                     AS CM_CAGE_CURRENCY_TYPE      
					    
				 FROM    MB_MOVEMENTS WITH (INDEX (IX_MB_MOVEMENTS))          
				WHERE   mbm_cashier_session_id IN ('+@pCashierSessionId+')
				GROUP BY    mbm_cashier_session_id, mbm_type '
	END --ByID
	ELSE--ByDate
	BEGIN
  SET @_sql =  '
				SELECT   
					  NULL
		 			, CM_TYPE
           , 0                AS CM_SUB_TYPE      
           , SUM(CASE WHEN CM_UNDO_STATUS IS NULL THEN 1 ELSE 0 END) AS CM_TYPE_COUNT
		 			, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''') AS CM_CURRENCY_ISO_CODE
		 			, ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
		  			, SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
					, SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
					, SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
					, SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
					, SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
				    
			   FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type)) 
			  WHERE  ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' CM_DATE >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
			  ' CM_DATE <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
			    
			 CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
			 'AND CM_SESSION_ID IN ('+@pCashierSessionId +')'END +'

		   GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''),ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
 		  UNION ALL 
		   SELECT
					 NULL   
				   , mbm_type                              AS CM_TYPE 
           , 1                     AS CM_SUB_TYPE         
           , COUNT(mbm_type)             AS CM_TYPE_COUNT
				   ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
           , 0                     AS CM_CURRENCY_DENOMINATION
           , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
           , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
           , 0                     AS CM_AUX_AMOUNT 
           , 0                     AS CM_INITIAL_BALANCE
           , 0                     AS CM_FINAL_BALANCE    
           , 0                     AS CM_CAGE_CURRENCY_TYPE
			 FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))             
			WHERE   ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' MBM_DATETIME >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
			  ' MBM_DATETIME <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
			
					CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
					'AND mbm_cashier_session_id IN ('+@pCashierSessionId +')'END +'
			GROUP BY    mbm_type '
		
	END--ByDate
 	
	EXEC sp_executesql @_sql
	
 END --CashierMovementsGrouped_Select
 
 GO

 GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Select] TO [wggui] WITH GRANT OPTION

 GO
 
 GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Select] TO [wggui] WITH GRANT OPTION
 GO