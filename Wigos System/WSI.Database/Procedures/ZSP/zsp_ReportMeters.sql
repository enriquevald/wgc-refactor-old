﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_ReportMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[zsp_ReportMeters]
GO

CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16) = null,
    @pSerialNumber varchar(30) = null,
    @pMachineNumber int= null,
    @pXmlMeters Xml                              
 
AS
BEGIN
  declare @terminal_id    int
  declare @status_code    int
  declare @status_text    varchar(254)
  declare @exception_info varchar
  DECLARE @meters_count   int
  
  SET @terminal_id = 0
  SET @status_code = 0
  SET @status_text = ''
  SET @exception_info = ''
  
  select @terminal_id =t3gs_terminal_id from TERMINALS_3GS where (t3gs_machine_number = @pMachineNumber) or (t3gs_vendor_id = @pVendorId and t3gs_serial_number = @pSerialNumber)
  if(@terminal_id<1)
    set @status_code = 1
  else
    begin
      begin transaction
      begin try
        DECLARE @metercode int
        DECLARE @metervalue bigint
        DECLARE @denom int
        
        SELECT
              meters.meter.value('(@Id)[1]', 'int')       AS METER_CODE , 
                 meters.meter.value('(@Value)[1]', 'bigint') AS METER_VALUE,  
              case meters.meter.value('(@Id)[1]', 'int')
                      when 66 then 5
                      when 67 then 10
                      when 68 then 20
                      when 70 then 50
                      when 71 then 100
                      when 72 then 200
                      when 74 then 500
                      when 75 then 1000
                      else 0.000
              end                                         AS DENOM
        INTO #XML_METERS_TEMP           
        FROM @pXmlMeters.nodes('Meters/Meter') meters(meter)

	   DECLARE  XML_Meters_Cursor CURSOR FOR 
         SELECT  METER_CODE, METER_VALUE, DENOM
          FROM  #XML_METERS_TEMP          

        OPEN XML_Meters_Cursor
        FETCH NEXT FROM XML_Meters_Cursor INTO @metercode, @metervalue, @denom

        WHILE @@FETCH_STATUS = 0
	   BEGIN
		  IF EXISTS (SELECT 1 FROM terminal_sas_meters where terminal_sas_meters.tsm_terminal_id = @terminal_id 
					 AND terminal_sas_meters.tsm_meter_code = @metercode 
					 AND terminal_sas_meters.tsm_denomination = @denom)
			 BEGIN
			 
				UPDATE   terminal_sas_meters
				   SET   tsm_delta_value = CASE WHEN @stacker_enabled = '1' 
				                                THEN 
												      CASE WHEN (@metervalue - tsm_meter_value) < 0 THEN 0
													       ELSE @metervalue - tsm_meter_value + tsm_delta_value
														   END
				                                ELSE 0 
				                           END
				       , tsm_meter_value = @metervalue
					  , tsm_last_reported = GETDATE()               
				 WHERE   terminal_sas_meters.tsm_terminal_id = @terminal_id 
				   AND   terminal_sas_meters.tsm_meter_code = @metercode 
		  	        AND   terminal_sas_meters.tsm_denomination = @denom
		  	 END
		  ELSE
		  	BEGIN	
		  	    INSERT INTO terminal_sas_meters ([tsm_terminal_id], [tsm_meter_code],  [tsm_game_id], [tsm_denomination],[tsm_wcp_sequence_id],
		  	      [tsm_last_reported], [tsm_last_modified], [tsm_meter_value], [tsm_meter_max_value], [tsm_delta_value],
		  	      [tsm_raw_delta_value], [tsm_delta_updating], [tsm_sas_accounting_denom])
                VALUES (@terminal_id, @metercode, 0, @denom, 0, getdate(), null, @metervalue, 9999999999, 0, 0, 0, null)
		  END
          
          FETCH NEXT FROM XML_Meters_Cursor INTO @metercode, @metervalue, @denom
	   END          

	   CLOSE XML_Meters_Cursor
	   DEALLOCATE XML_Meters_Cursor
	   
	   DROP TABLE #XML_METERS_TEMP 
	   
        set @status_code=0
        commit transaction 
      end try
      begin catch
        rollback transaction
        set @exception_info= ' (ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                       + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                       + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                       + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                       + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                       + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
                       + ')'
        set @status_code=4
      end catch
     end

  set @status_text = case @status_code 
                       when 0 then 'OK'
                       when 1 then 'Numero de terminal invalido'
                       when 4 then 'Acceso denegado'
                       else 'Unknown' 
                     end
  
  DECLARE @input AS nvarchar(MAX)

  set @input = CAST( @pXmlMeters as nvarchar(max));
  DECLARE @output AS nvarchar(MAX)

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              + @exception_info;
         
  EXECUTE dbo.zsp_Audit 'zsp_ReportMeters', '', @pVendorId , @pSerialNumber, @terminal_id, NULL,  
                                         @status_code, NULL, 1, @input , @output


  select @status_code as StatusCode, @status_text  as StatusText
  
END -- [zsp_ReportMeters]
GO


