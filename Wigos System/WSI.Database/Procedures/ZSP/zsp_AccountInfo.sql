﻿IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_AccountInfo')
       DROP PROCEDURE [zsp_AccountInfo]
GO

CREATE PROCEDURE [dbo].[zsp_AccountInfo]
    @VendorId        varchar(16),
	@TrackData       varchar(50)
 
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  declare @AccountNumber              bigint ,
  @Name                       varchar(255) ,
  @FirstSurname               varchar(255) ,
  @SecondSurname              varchar(255) ,
  @Gender                     varchar(255) ,
  @BirthDate                  varchar(255) ,
  @BirthStateCode             varchar(255) ,
  @BirthState                 varchar(255) ,
  @Email                      varchar(255) ,
  @AuxEmail                   varchar(255) ,
  @MobileNumber               varchar(255) ,
  @FixedPhone                 varchar(255) ,
  @ClientTypeCode             varchar(255) ,
  @ClientType                 varchar(255) ,
  @FiscalCode                 varchar(255) ,
  @NationalityCode            varchar(255) ,
  @Nationality                varchar(255) ,
  @Street                     varchar(255) ,
  @HouseNumber                varchar(255) ,
  @DoorNumber                 varchar(255) ,
  @Colony                     varchar(255) ,
  @CountyCode                 varchar(255) ,
  @Country                    varchar(255) ,
  @StateCode                  varchar(255) ,
  @State                      varchar(255) ,
  @CityCode                   varchar(255) ,
  @City                       varchar(255) ,
  @IssuingAuthority           varchar(255) ,
  @DocumentType               varchar(255) ,
  @DocumentNumber             varchar(255) ,
  @LevelCode				  varchar(255) ,
  @Level	  				  varchar(255) ,
  @Points					  varchar(255) ,
  @Document2Type               varchar(255) ,
  @Document2Number             varchar(255) ,
  @Document3Type               varchar(255) ,
  @Document3Number             varchar(255) 

  



  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Ok'
    SET @error_text   = ''
    BEGIN TRY
      SET @_try = @_try + 1

	  DECLARE @_account_id BIGINT

      SET @_account_id = 0
      IF @TrackData IS NOT NULL  
      BEGIN 
        SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
                       )
      END
	  IF @_account_id > 0
	  begin
		SELECT @status_code  = 1,
		
		@AccountNumber = ac_account_id ,
			   @Name = ac_holder_name3 ,
			   @FirstSurname = ac_holder_name1 ,
			   @SecondSurname = ac_holder_name2,
			   @Gender = ac_holder_gender ,
			   @BirthDate = ac_holder_birth_date,
			   @Email = ac_holder_email_01 ,
			   @AuxEmail = ac_holder_email_02 ,
			   @MobileNumber = ac_holder_phone_number_01 ,
			   @FixedPhone = ac_holder_phone_number_02 ,
			   @ClientTypeCode = 'F' ,
			   @ClientType = 'FISICA',
			   @FiscalCode = oc_code ,
			   @Nationality = co_name,
			   @Street = ac_holder_address_01 ,
			   @HouseNumber = ac_holder_ext_num,
			   @Colony = ac_holder_address_02 ,
			   @State = fs_name ,
			   @City = ac_holder_city ,
			   @DocumentType = identification_types.idt_name ,
			   @DocumentNumber = ac_holder_id,
			   @LevelCode = ac_holder_level,
			   @Level = (select top 1 gp_key_value from general_params where gp_group_key='PlayerTracking' and gp_subject_key='Level0'+ cast(ac_holder_level as varchar(2)) +'.Name'),
			   @Points = ac_points,
			   @Document2Type = idt2.idt_name ,
			   @Document2Number = ac_holder_id2,
			   @Document3Type = idt3.idt_name ,
			   @Document3Number = ac_holder_id3

		  FROM   accounts
		  LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
		  LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
		  LEFT JOIN identification_types on ac_holder_id_type = identification_types.idt_id
		  LEFT JOIN identification_types idt2 on ac_holder_id2_type = idt2.idt_id
		  LEFT JOIN identification_types idt3 on ac_holder_id3_type = idt3.idt_id
		  LEFT JOIN occupations on oc_id = ac_holder_occupation_id
		 WHERE   ac_account_id = @_account_id
	  end

      SET @_completed = 1

    END TRY
    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData 
			  +';VenbdorId='      + @VendorId
  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text



  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_GetAccountInfo', @_account_id, 0, 0, 0,
                                            0, @status_code, 0, 1,
                                            @input, @output

  COMMIT TRANSACTION
	SELECT @status_code		AS StatusCode,
	    @status_text		AS StatusText,
		@AccountNumber      as  AccountNumber     ,
		@Name				as  Name             , 
		@FirstSurname		as  FirstSurname     , 
		@SecondSurname		as  SecondSurname    , 
		@Gender				as  Gender           , 
		@BirthDate			as  BirthDate        , 
		@BirthStateCode		as  BirthStateCode   , 
		@BirthState			as  BirthState       , 
		@Email				as  Email            , 
		@AuxEmail			as  AuxEmail         , 
		@MobileNumber		as  MobileNumber     , 
		@FixedPhone			as  FixedPhone       , 
		@ClientTypeCode		as  ClientTypeCode   , 
		@ClientType			as  ClientType       , 
		@FiscalCode			as  FiscalCode       , 
		@NationalityCode	as  NationalityCode  , 
		@Nationality		as  Nationality      , 
		@Street				as  Street           , 
		@HouseNumber		as  HouseNumber      , 
		@DoorNumber			as  DoorNumber       , 
		@Colony				as  Colony           , 
		@CountyCode			as  CountyCode       , 
		@Country			as  Country          , 
		@StateCode			as  StateCode        , 
		@State				as  State            , 
		@CityCode			as  CityCode         , 
		@City				as  City             , 
		@IssuingAuthority	as  IssuingAuthority , 
		@DocumentType		as  DocumentType     , 
		@DocumentNumber		as  DocumentNumber   ,
		@LevelCode			as  LevelCode   ,
		@Level				as  Level   ,
		@Points				as  Points,
		@Document2Type		as  Document2Type     , 
		@Document2Number		as  Document2Number   ,
		@Document3Type		as  Document3Type     , 
		@Document3Number		as  Document3Number   

	

END -- zsp_SessionStart
go

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_AccountInfo')
       DROP PROCEDURE [S2S_AccountInfo]
GO
CREATE PROCEDURE [dbo].[S2S_AccountInfo]
  @VendorId        varchar(16),
  @Trackdata       varchar(24)
AS
BEGIN
  
  EXECUTE zsp_AccountInfo  @VendorId,@Trackdata
    
END

GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [3GS] WITH GRANT OPTION 
GO