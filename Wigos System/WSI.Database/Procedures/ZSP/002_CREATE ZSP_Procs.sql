﻿
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionEnd')
       DROP PROCEDURE zsp_SessionEnd
GO

--------------------------------------------------------------------------------
-- PURPOSE: End play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0,
  @BillIn		  money		
WITH EXECUTE AS OWNER
AS
BEGIN

  DECLARE @status_code		  int
  DECLARE @status_text		  varchar (254)
  DECLARE @error_text		  nvarchar (MAX)
  DECLARE @input			  nvarchar(MAX)
  DECLARE @output			  nvarchar(MAX)
  DECLARE @_try			  int
  DECLARE @_max_tries		  int
  DECLARE @_exception		  bit
  DECLARE @_completed		  bit  
  DECLARE @_generation_enabled  bit
  DECLARE @_type_enabled  bit

  DECLARE @_auto_print_cash_out_voucher_type  int

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 19-DES-2014, The number of retries has been incremented to 6 (retries seem to work so we try one more time)
  SET @_completed = 0
  SET @_auto_print_cash_out_voucher_type = 1

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
      
      EXECUTE dbo.Trx_3GS_EndCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @SessionId,
                         @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                         @CreditBalance, @BillIn,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1


	 SELECT @_generation_enabled = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AutoPrintVoucher' AND GP_SUBJECT_KEY = 'CashOut.Generation.Enabled'
	 
	 SELECT   @_type_enabled = COUNT(1) 
	   FROM   dbo.SplitString((SELECT   GP_KEY_VALUE 
							   FROM   GENERAL_PARAMS 
							  WHERE   GP_GROUP_KEY   = 'AutoPrintVoucher' 
							    AND   GP_SUBJECT_KEY = 'Types.Enabled'), ',') 
	  WHERE   Item = @_auto_print_cash_out_voucher_type

	 IF @_type_enabled = 1 AND @_generation_enabled = 1
      BEGIN
	   IF @status_code = 0
	   BEGIN
    
		  INSERT INTO   AUTO_PRINT_PENDING_GENERATION_VOUCHER        
		              ( APPGV_SESSION_ID                             
		              , APPGV_TYPE )                                 
		       VALUES (                                              
		                @SessionId                                 
		              , @_auto_print_cash_out_voucher_type )   
	   END                          
	 END	

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END -- WHILE
  
  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR) 
  
  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                          @SessionId, @status_code, @CreditBalance, 1,
                                          @input, @output

  COMMIT TRANSACTION

  -- AJQ 21-AUG-2014, Always return 0 "Success" to the caller.
  --                  This is to avoid retries from the client side.
  --                  When an exception has occurred, we will return status=4
  IF (@_exception = 0)
    SET  @status_code = 0

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionUpdate')
       DROP PROCEDURE zsp_SessionUpdate
GO

--------------------------------------------------------------------------------
-- PURPOSE: Update play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
 CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0,
  @BillIn		      money = 0,
  @UpdateByFreq   bit = 1
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @update_freq  int
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @update_freq = 30

  -------- For Cadillac store all session updates received
  ------IF (@VendorId like '%CADILLAC%')
  ------    SET @update_freq = 1

  ---- AJQ & XI 12-12-2013, Limit the number of updates 
  IF ( (@GamesPlayed % @update_freq <> 0) AND (@UpdateByFreq = 1) ) 
  BEGIN
    -- When the provider calls every:
    -- 1 play --> 1 / 30 -->  3.33% trx --> 3.33% plays
    -- 2 play --> 1 / 15 -->  6.67% trx --> 3.33% plays
    -- 3 play --> 1 / 10 --> 10.00% trx --> 3.33% plays
    -- 4 play --> 1 / 60 -->  1.67% trx --> 0.42% plays
    -- 5 play --> 1 /  6 --> 16.67% trx --> 3.33% plays
    SELECT CAST (0 AS INT) AS StatusCode, CAST ('Success' AS varchar (254)) AS StatusText
    RETURN
  END

  SET @_try       = 0
  SET @_max_tries = 3
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
    
      EXECUTE dbo.Trx_3GS_UpdateCardSession @VendorId, @SerialNumber, @MachineNumber,
                                            @AccountID,
                                            @SessionId,
                                            @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                            @CreditBalance, @BillIn,
                                            @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                             @SessionId, @status_code, @CreditBalance, 1,
                                             @input, @output

  COMMIT TRANSACTION

  -- AJQ 19-DES-2014, When an exception occurred we will return 0-"Success" to the caller.
  IF (@_exception = 1)
    SET  @status_code = 0
    
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate


GO


IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_AccountInfo')
       DROP PROCEDURE [zsp_AccountInfo]
GO


CREATE PROCEDURE [dbo].[zsp_AccountInfo]
    @VendorId        varchar(16),
	@TrackData       varchar(50)
 
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  declare @AccountNumber              bigint ,
  @Name                       varchar(255) ,
  @FirstSurname               varchar(255) ,
  @SecondSurname              varchar(255) ,
  @Gender                     varchar(255) ,
  @BirthDate                  varchar(255) ,
  @BirthStateCode             varchar(255) ,
  @BirthState                 varchar(255) ,
  @Curp                       varchar(255) ,
  @Email                      varchar(255) ,
  @AuxEmail                   varchar(255) ,
  @MobileNumber               varchar(255) ,
  @FixedPhone                 varchar(255) ,
  @ClientTypeCode             varchar(255) ,
  @ClientType                 varchar(255) ,
  @FiscalCode                 varchar(255) ,
  @RFC                        varchar(255) ,
  @NationalityCode            varchar(255) ,
  @Nationality                varchar(255) ,
  @Street                     varchar(255) ,
  @HouseNumber                varchar(255) ,
  @DoorNumber                 varchar(255) ,
  @Colony                     varchar(255) ,
  @CountyCode                 varchar(255) ,
  @Country                    varchar(255) ,
  @StateCode                  varchar(255) ,
  @State                      varchar(255) ,
  @CityCode                   varchar(255) ,
  @City                       varchar(255) ,
  @DocuemntTypeCode           varchar(255) ,
  @DocuemntType               varchar(255) ,
  @IssuingAuthority           varchar(255) ,
  @DocumentNumber             varchar(255) ,
  @LevelCode				  varchar(255) ,
  @Level	  				  varchar(255) ,
  @Points					  varchar(255) 

  



  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Ok'
    SET @error_text   = ''
    BEGIN TRY

      SET @_try = @_try + 1

	  DECLARE @_account_id BIGINT

      SET @_account_id = 0

      IF @TrackData IS NOT NULL  
      BEGIN 
        SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
                       )
      END
	  IF @_account_id > 0
	  begin
		SELECT @status_code  = 1,
		
		@AccountNumber = ac_account_id ,
			   @Name = ac_holder_name3 ,
			   @FirstSurname = ac_holder_name1 ,
			   @SecondSurname = ac_holder_name2,
			   @Gender = ac_holder_gender ,
			   @BirthDate = ac_holder_birth_date,
			   @BirthStateCode = '' ,
			   @BirthState = '' ,
			   @Curp = ac_holder_id2 ,
			   @Email = ac_holder_email_01 ,
			   @AuxEmail = ac_holder_email_02 ,
			   @MobileNumber = ac_holder_phone_number_01 ,
			   @FixedPhone = ac_holder_phone_number_02 ,
			   @ClientTypeCode = 'F' ,
			   @ClientType = 'FISICA',
			   @FiscalCode = oc_code ,
			   @RFC = ac_holder_id1 ,
			   @NationalityCode = '',
			   @Nationality = co_name,
			   @Street = ac_holder_address_01 ,
			   @HouseNumber = ac_holder_ext_num,
			   @DoorNumber = '' ,
			   @Colony = ac_holder_address_02 ,
			   @CountyCode = '' ,
			   @Country = '' ,
			   @StateCode = '' ,
			   @State = fs_name ,
			   @CityCode = '' ,
			   @City = ac_holder_city ,
			   @DocuemntTypeCode = ac_holder_id_type ,
			   @DocuemntType = idt_name ,
			   @IssuingAuthority = '' ,
			   @DocumentNumber = ac_holder_id,
			   @LevelCode = ac_holder_level,
			   @Level = (select top 1 gp_key_value from general_params where gp_group_key='PlayerTracking' and gp_subject_key='Level0'+ cast(ac_holder_level as varchar(2)) +'.Name'),
			   @Points = ac_points
		  FROM   accounts
		  LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
		  LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
		  LEFT JOIN identification_types on ac_holder_id_type = idt_id
		  LEFT JOIN occupations on oc_id = ac_holder_occupation_id
		 WHERE   ac_account_id = @_account_id
	  end

      SET @_completed = 1

    END TRY
    BEGIN CATCH
    
      ROLLBACK TRANSACTION


      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

print @_account_id 

  SET @input = '@TrackData='       + @TrackData 
			  +';VenbdorId='      + @VendorId
  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text



  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_GetAccountInfo', @_account_id, 0, 0, 0,
                                            0, @status_code, 0, 1,
                                            @input, @output

  COMMIT TRANSACTION
	SELECT @status_code		AS StatusCode,
	    @status_text		AS StatusText,
		@AccountNumber      as  AccountNumber     ,
		@Name				as  Name             , 
		@FirstSurname		as  FirstSurname     , 
		@SecondSurname		as  SecondSurname    , 
		@Gender				as  Gender           , 
		@BirthDate			as  BirthDate        , 
		@BirthStateCode		as  BirthStateCode   , 
		@BirthState			as  BirthState       , 
		@Curp				as  Curp             , 
		@Email				as  Email            , 
		@AuxEmail			as  AuxEmail         , 
		@MobileNumber		as  MobileNumber     , 
		@FixedPhone			as  FixedPhone       , 
		@ClientTypeCode		as  ClientTypeCode   , 
		@ClientType			as  ClientType       , 
		@FiscalCode			as  FiscalCode       , 
		@RFC				as  RFC              , 
		@NationalityCode	as  NationalityCode  , 
		@Nationality		as  Nationality      , 
		@Street				as  Street           , 
		@HouseNumber		as  HouseNumber      , 
		@DoorNumber			as  DoorNumber       , 
		@Colony				as  Colony           , 
		@CountyCode			as  CountyCode       , 
		@Country			as  Country          , 
		@StateCode			as  StateCode        , 
		@State				as  State            , 
		@CityCode			as  CityCode         , 
		@City				as  City             , 
		@DocuemntTypeCode	as  DocuemntTypeCode , 
		@DocuemntType		as  DocuemntType     , 
		@IssuingAuthority	as  IssuingAuthority , 
		@DocumentNumber		as  DocumentNumber   ,
		@LevelCode			as  LevelCode   ,
		@Level				as  Level   ,
		@Points				as  Points

	

END -- zsp_SessionStart

go

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_CardInfo')
       DROP PROCEDURE [zsp_CardInfo]
GO

CREATE PROCEDURE [dbo].[zsp_CardInfo]
    @TrackData			varchar(24)

WITH EXECUTE AS OWNER
AS

BEGIN
                  
BEGIN TRANSACTION
	DECLARE @Owner				nvarchar(250)
	DECLARE @Account			bigint
	DECLARE @CardIndex			int 	
	DECLARE @CustumerId			bigint 	
	DECLARE @AccountType		int 	
	DECLARE @CardStatus			int 	
	DECLARE @Status_code  		int
	DECLARE @Status_text  		varchar (254)
	DECLARE @_try         		int
	DECLARE @_max_tries   		int
	DECLARE @_exception   		bit
	DECLARE @error_text   		nvarchar (MAX)
	DECLARE @input        		nvarchar(MAX)
	DECLARE @output       		nvarchar(MAX)

	SET @Status_code = 1
	SET @Status_text = 'TrackData wrong'
	SET @Account = 0
	SET @CardIndex = 0
	SET @CustumerId = 0
	SET @AccountType = 0
	SET @CardStatus = 0
	      
	BEGIN
		BEGIN TRY      
			IF @TrackData IS NOT NULL  
			BEGIN
				SELECT	@Account		= AC_ACCOUNT_ID, 
						@Owner			= LTRIM(RTRIM(ISNULL(ac_holder_name,'')))
				FROM	ACCOUNTS
				WHERE	AC_TRACK_DATA	= dbo.TrackDataToInternal(@TrackData)
			    
				IF @Account > 0
					BEGIN
						SET @CardIndex 		= 1
						SET @CustumerId 	= @Account
						SET @CardStatus		= 1
						SET @Status_code	= 0
						SET @Status_text	= 'OK'
						  
						 IF LEN(@Owner) = 0 	SET @AccountType = 0
						 ELSE 					SET @AccountType = 1
					  END
			  END   
		END TRY
		BEGIN CATCH
			  ROLLBACK TRANSACTION
				SET @_exception  = 1;
				SET @status_code = 4;
				SET @status_text = 'Access Denied';
				SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
								 + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
								 + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
								 + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
								 + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
								 + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))

			  BEGIN TRANSACTION
		END CATCH
	END	  
	  
	  
	SET @input = '@TrackData='       + @TrackData 
	SET @output = 'StatusCode='      + CAST (@Status_code     AS NVARCHAR)
				  +';StatusText='      + @Status_text
	              
	EXECUTE dbo.zsp_Audit 'zsp_CardInfo', @CustumerId, 0, 0, 0, 0, @status_code, 0, 1, @input, @output
	COMMIT TRANSACTION 
      
	SELECT	@Status_code	AS StatusCode, 
			@Status_text	AS StatusText, 
			@CardIndex		AS CardIndex, 
			@CustumerId		AS CustomerId, 
			@AccountType	AS AccountType, 
			@CardStatus		AS CardStatus
     
END --Zsp_GetCardInfo
				  
GO

-- [3GS]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [3GS] WITH GRANT OPTION 
GO


-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO
