--------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SP_FS2S_CloseSession.sql
-- 
--   DESCRIPTION: Procedure for FS2S Close session
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 07-SEP-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-SEP-2017 ETP    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FS2S_CloseSession]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[SP_FS2S_CloseSession]
END

GO

CREATE  PROCEDURE [dbo].[SP_FS2S_CloseSession]
AS
BEGIN
	DECLARE @_am_reserved_expired AS INTEGER
	DECLARE @_am_reserve_credit AS INTEGER	
	DECLARE @_prov_id as INTEGER
	
	DECLARE @_start_session AS DATETIME
	DECLARE @_FBM_vendor_id AS NVARCHAR(20)

-- Moments type:
	SET @_am_reserve_credit = 219
	SET @_am_reserved_expired = 266
	
-- Get started data	
	SELECT TOP 1 @_start_session = FL_CREATION 
	  FROM       FBM_LOG 
	 WHERE       FL_STATUS = 1 
  ORDER BY       FL_CREATION DESC	
	
-- Get Provider id:
    SELECT   @_FBM_vendor_id = gp_key_value 
      FROM   general_params 
     WHERE   gp_group_key = 'WS2S' 
       AND   GP_SUBJECT_KEY = 'VendorId'
    
    IF @_FBM_vendor_id is null
    BEGIN
     SET @_FBM_vendor_id = 'FBM'
    END

    SELECT   @_prov_id = PV_ID 
      FROM   PROVIDERS
     WHERE   PV_NAME = @_FBM_vendor_id
    
-- CLOSE SESSION RESPONSE:            
     SELECT   SUM(ISNULL(AC_RE_RESERVED,0))*100                            AS TotalValueAccounts
            , 0.0                                                          AS TotalValueAccountsPromo 
            , SUM(ISNULL(AM.TotalSessionExpiredAccounts,0))*100            AS TotalSessionExpiredAccounts 
            , 0.0                                                          AS TotalSessionExpiredAccountsPromo
            , SUM(ISNULL(AM.TotalSessionSalesPOS,0))*100                   AS TotalSessionSalesPOS 
            , 0.0                                                          AS TotalSessionPromoInPOS  
            , SUM(ISNULL(AM.TotalSessionPaymentsPOS,0))*100                AS TotalSessionPaymentsPOS 
            , 0.0                                                          AS TotalSessionPromoOutPOS    
            , SUM(ISNULL(PS.TotalSessionJackpotPaymentPOS,0))*100          AS TotalSessionPaymentsPOS  
            , SUM(ISNULL(PS.TotalSessionEletronicTransferIn,0))*100        AS TotalSessionEletronicTransferIn  
            , 0.0                                                          AS TotalSessionEletronicTransferInPromo 
            , SUM(ISNULL(PS.TotalSessionEletronicTransferOut,0))*100       AS TotalSessionEletronicTransferOut 
            , 0.0                                                          AS TotalSessionEletronicTransferOutPromo 
            , SUM(ISNULL(PS.TotalSessionHandPay,0))*100                    AS TotalSessionHandPay
            , SUM(ISNULL(PS.TotalSessionCoinIn,0))*100                     AS TotalSessionCoinIn
            , SUM(ISNULL(PS.TotalSessionCoinOut,0))*100                    AS TotalSessionCoinOut
            , SUM(ISNULL(PS.TotalSessionJackpot,0))*100                    AS TotalSessionJackpot 
            , SUM(ISNULL(PS.TotalSessionCoinIn,0)
                 -ISNULL(PS.TotalSessionCoinOut,0)
                 -ISNULL(PS.TotalSessionJackpot,0)
                 +ISNULL(AM.TotalSessionExpiredAccounts,0))*100            AS TotalSessionNet 
       FROM   ACCOUNTS a        
  LEFT JOIN (   SELECT   AM_ACCOUNT_ID
                       , SUM(CASE WHEN (AM_TYPE = @_am_reserved_expired) 
                                  THEN AM_SUB_AMOUNT 
                                  ELSE 0.0 END)                           AS TotalSessionExpiredAccounts 
                       , 0.0  AS TotalSessionExpiredAccountsPromo -- OK 
                       , SUM(CASE WHEN (AM_TYPE = @_am_reserve_credit) 
                                  THEN AM_ADD_AMOUNT 
                                  ELSE 0.0 END)                           AS TotalSessionSalesPOS 
                       , 0.0  AS TotalSessionPromoInPOS           
                       , SUM(CASE WHEN (AM_TYPE = @_am_reserve_credit) 
                                  THEN AM_SUB_AMOUNT 
                                  ELSE 0.0 END)                           AS TotalSessionPaymentsPOS 
                  FROM   ACCOUNT_MOVEMENTS
                 WHERE   AM_DATETIME >= @_start_session
              GROUP BY   AM_ACCOUNT_ID                          
            )                                                             AS am
         ON   a.AC_ACCOUNT_ID = am.AM_ACCOUNT_ID
  LEFT JOIN (   SELECT   PS_ACCOUNT_ID 
                       , SUM (0.0)                                        AS TotalSessionJackpotPaymentPOS 
                       , SUM (PS_CASH_IN)                                 AS TotalSessionEletronicTransferIn 
                       , SUM (PS_CASH_OUT)                                AS TotalSessionEletronicTransferOut 
                       , SUM (0.0)                                        AS TotalSessionHandPay 
                       , SUM (ps_re_cash_in)                              AS TotalSessionCoinIn   -- TODO HULK CHECK
                       , SUM (ps_re_cash_out)                             AS TotalSessionCoinOut  -- TODO HULK CHECK
                       , SUM (0.0)                                        AS TotalSessionJackpot  -- TODO HULK            
                  FROM   PLAY_SESSIONS
            RIGHT JOIN   TERMINALS
                    ON ( PS_TERMINAL_ID = TE_TERMINAL_ID
                   AND   TE_PROV_ID = @_prov_id ) 
                 WHERE   PS_STARTED >= @_start_session 
              GROUP BY   PS_ACCOUNT_ID                    
					  )                                                   AS ps
         ON   ps.PS_ACCOUNT_ID = a.AC_ACCOUNT_ID        
END

GO

GRANT EXECUTE ON [dbo].[SP_FS2S_CloseSession] TO [wggui] WITH GRANT OPTION 

GO