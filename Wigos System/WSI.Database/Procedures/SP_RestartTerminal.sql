﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_RestartTerminal]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_RestartTerminal]
GO

CREATE PROCEDURE [dbo].[SP_RestartTerminal] 
       @pTerminalId                   INT

AS
BEGIN
  INSERT INTO  WCP_COMMANDS 
              (CMD_TERMINAL_ID, CMD_CODE, CMD_STATUS)
       VALUES (@pTerminalId, 2, 0)
END

GO

GRANT EXECUTE ON [dbo].[SP_RestartTerminal] TO [wggui] WITH GRANT OPTION

GO