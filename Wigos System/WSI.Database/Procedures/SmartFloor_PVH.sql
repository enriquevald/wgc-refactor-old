--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmartFloor_PVH]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[SmartFloor_PVH]
--GO

CREATE PROCEDURE [dbo].[SmartFloor_PVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
  
  -- JRC 21-SEP-2015 Reads and Inserts or Updates as needed into H_PVH

AS
BEGIN
  
  DECLARE @ROW_COUNT INT
  SET @ROW_COUNT = 0
  
  -- ACCOUNTS
     SELECT   ACCOUNT_INFO.AC_ACCOUNT_ID                                            AS PVH_ACCOUNT_ID 
            , CAST(CONVERT(NVARCHAR(8),@DateEnd,112) AS INTEGER)                    AS PVH_DATE
            , DATEPART(DW,@DateEnd)                                                 AS PVH_WEEKDAY
            , 1                                                                     AS PVH_VISIT
            
            -- INFO CHECK-IN, GAME TIME, ROOM TIME, etc... BY PLAYER.
            , CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                ACCOUNT_MOV.[FIRST MOV]
              ELSE
                PLAY_SES.STARTSESSION
              END                                                                   AS PVH_CHECK_IN               -- Fecha en la que ha entrado el player al casino
            , CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                ACCOUNT_MOV.[LAST MOV]
              ELSE
                PLAY_SES.LASTSESSION
              END                                                                   AS PVH_CHECK_OUT              -- Fecha en la que ha ha salido el player del casino
            , DATEDIFF(MINUTE, 
                CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                  ACCOUNT_MOV.[FIRST MOV]
                ELSE
                  PLAY_SES.STARTSESSION
                END,
                CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                  ACCOUNT_MOV.[LAST MOV]
                ELSE
                  PLAY_SES.LASTSESSION
                END)                                                                AS PVH_ROOM_TIME              -- Tiempo (minutos) que está el "player" dentro del Casino
            
            -- INFO TOTAL PLAYED, JACKPOTS WON, etc...    
            , PLAY_SES.GAME_TIME                                                    AS PVH_GAME_TIME              -- Tiempo de Juego (minutos) que lleva el player jugando en la máquina
            , PLAY_SES.TOTAL_PLAYED_COUNT                                           AS PVH_TOTAL_PLAYED_COUNT     -- Num Jugadas (count)
            , PLAY_SES.TOTAL_WON_COUNT                                              AS PVH_PLAYED_WON_COUNT       -- Num Jugadas Ganadas (count)
            , PLAY_SES.TOTAL_PLAYED                                                 AS PVH_TOTAL_PLAYED           -- Total Monto Jugado
            , CASE WHEN PLAY_SES.TOTAL_PLAYED_COUNT > 0 THEN 
                ROUND(PLAY_SES.TOTAL_PLAYED / PLAY_SES.TOTAL_PLAYED_COUNT,2) 
              ELSE 
                0 
              END                                                                   AS PVH_TOTAL_BET_AVG          -- Apuesta Media (Total Jugado / Num Jugadas)
            , JACKPOTS.HP_AMOUNT                                                    AS PVH_JACKPOTS_WON           -- JACKPOTS Won (amount)
            
            , ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
              ISNULL(ACCOUNT_MOV.PRIZE,0) +                                                                     
              ISNULL(JACKPOTS.HP_AMOUNT,0)                                          AS PVH_TOTAL_WON              -- MONEY_OUT + JACKPOTS
              
            -- INFO TOTAL PLAYED (REEDEMABLE AND NOT REEDEMABLE)...   
            , PLAY_SES.TOTAL_PLAYED_REDEEM                                          AS PVH_RE_PLAYED              -- Total Jugado Redimible
            , PLAY_SES.TOTAL_PLAYED_NON_REDEEM                                      AS PVH_NR_PLAYED              -- Total Jugado NO Redimible
            
            -- INFO THEORICAL WON
            , POS.PO_THEO_WON                                                       AS PVH_THEORICAL_WON          -- Teórico Ganado (Jugado * PayOut Teórico de la máquina)
            
            -- INFO MONEY IN, MONEY OUT, JACKPOTS, TAX, etc...
            , CASE WHEN (@IsTitoMode = 1) THEN 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0) + 
                ISNULL(PLAY_SES.BILL_IN,0)
              ELSE 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0)
              END                                                                   AS PVH_MONEY_IN               -- CASH IN (Recargas / Bill In)
              
            , ISNULL(ACCOUNT_MOV.[MONEY IN TAX],0)                                  AS PVH_MONEY_IN_TAX           -- CASH IN TAX
            
            , ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
              ISNULL(ACCOUNT_MOV.PRIZE,0)                                           AS PVH_MONEY_OUT              -- TOTAL DINERO RETIRADO (BRUTO)
            
            , PLAY_SES.TOTAL_RE_WON_AMOUNT                                          AS PVH_RE_WON
            , PLAY_SES.TOTAL_NR_WON_AMOUNT                                          AS PVH_NR_WON
            , ACCOUNT_MOV.REFUNDS                                                   AS PVH_DEVOLUTION             -- DEVOLUCIÓN
            , ACCOUNT_MOV.PRIZE                                                     AS PVH_PRIZE                  -- PREMIO
            , ACCOUNT_MOV.[TAX ON PRIZE 1]                                          AS PVH_TAX1                   -- TAX 1 (PREMIO)
            , ACCOUNT_MOV.[TAX ON PRIZE 2]                                          AS PVH_TAX2                   -- TAX 2 (PREMIO)
            , ACCOUNT_MOV.NUM_RECHARGES                                             AS PVH_MONEY_IN_COUNT         -- CAHSLESS (NUM RECARGAS) | TITO (NUM BILL IN) 
            , ACCOUNT_MOV.NUM_REFUNDS                                               AS PVH_MONEY_OUT_COUNT        -- NUM RETIROS DE CAJA O COBRO DE TICKETS
            
            -- INFO PLAYER (AGE, LEVEL, NUM REGISTERED DAYS, etc...)
            , ISNULL(DATEDIFF(YEAR,AC_HOLDER_BIRTH_DATE, GETDATE()),0)              AS PVH_AGE                    
            , DATEDIFF(DAY,AC_CREATED, GETDATE())			                              AS PVH_REGISTERED_NUM_DAYS
            , ACCOUNT_INFO.AC_HOLDER_LEVEL                                          AS PVH_LEVEL
       INTO #TEMP_ACCOUNT_INFO
	   FROM   ACCOUNTS AS ACCOUNT_INFO
      
  -- PLAY SESSIONS
	  INNER JOIN 
				(                                                     
				SELECT    PS_ACCOUNT_ID                                
						  , MIN(PS_STARTED)                                               AS   STARTSESSION               -- Primera play sessión
						  , MAX(PS_FINISHED)                                              AS   LASTSESSION                -- Última play sessión
						  , DATEDIFF (MINUTE,   MIN(PS_STARTED), MAX(PS_FINISHED))        AS   GAME_TIME                  -- Tiempo que lleva el player jugando
						  , SUM(PS_PLAYED_AMOUNT)                                         AS   TOTAL_PLAYED               -- Total monto jugado (redimible y no redimible)
						  , SUM(PS_PLAYED_COUNT)                                          AS   TOTAL_PLAYED_COUNT         -- Total jugadas 
						  , SUM(PS_WON_COUNT)                                             AS   TOTAL_WON_COUNT            -- Total numero de jugadas ganadas  
						  , SUM(PS_REDEEMABLE_PLAYED)                                     AS   TOTAL_PLAYED_REDEEM        -- Total jugado redimible
						  , SUM(PS_NON_REDEEMABLE_PLAYED)                                 AS   TOTAL_PLAYED_NON_REDEEM    -- Total jugado no redimible
						  , SUM(PS_WON_AMOUNT)                                            AS   AMOUNT_WON                 -- Total ganado (redimible y no redimible)
						  , SUM(PS_CASH_IN)                                               AS   BILL_IN                    -- Bill In (ONLY FOR TITO)
						  , SUM(PS_REDEEMABLE_WON)                                        AS   TOTAL_RE_WON_AMOUNT        -- Total ganado (redimible)
						  , SUM(PS_NON_REDEEMABLE_WON)                                    AS   TOTAL_NR_WON_AMOUNT        -- Total ganado (no redimible)
				FROM    PLAY_SESSIONS                                   
				 WHERE    PS_FINISHED >= @DateStart                       
				   AND    PS_FINISHED <  @DateEnd                       
				   AND    PS_STATUS   <> 0                               
		 GROUP  BY    PS_ACCOUNT_ID) AS PLAY_SES 
		ON    PLAY_SES.PS_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
     
  -- ACCOUNT_MOVEMENTS
	  LEFT JOIN
				  ( 
				SELECT  AM_ACCOUNT_ID 
						, SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [DEPOSIT A]         -- Recarga / Cash In
						, SUM(CASE AM_TYPE WHEN  1  THEN 1 ELSE 0 END)                    AS  [NUM_RECHARGES]     -- Número de Recargas / Num de Billetes Introducidos
						
						, SUM(CASE AM_TYPE WHEN  3  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [REFUNDS]           -- Reembolso (Devolución)
						, SUM(CASE AM_TYPE WHEN  3  THEN 1 ELSE 0 END)                    AS  [NUM_REFUNDS]       -- Número de Retiros (o cobro de tickets)
						
						, SUM(CASE AM_TYPE WHEN  2  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [PRIZE]             -- Premio
						, SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [PRIZE COUPON]      -- Cupón Premio
						
						, SUM(CASE AM_TYPE WHEN 82  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [MONEY IN TAX]        
						, SUM(CASE AM_TYPE WHEN 4   THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 1]        
						, SUM(CASE AM_TYPE WHEN 13  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 2]        
						, MIN(AM_DATETIME)                                                AS  [FIRST MOV]         -- Primer movimiento de cuenta 
						, MAX(AM_DATETIME)                                                AS  [LAST MOV]          -- Último movimiento de cuenta 
				  FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_AM_DATETIME) )                       
				 WHERE  AM_DATETIME >= @DateStart                                                   
					   AND  AM_DATETIME <  @DateEnd 
			GROUP BY  AM_ACCOUNT_ID) ACCOUNT_MOV
		ON  ACCOUNT_MOV.AM_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
  			
  -- THEO WON
	  LEFT JOIN 
			(
			SELECT    PO_RESULT.PS_ID                                               AS PS_ID
					, SUM(PO_RESULT.PS_RESULT)                                      AS PO_THEO_WON 
			  FROM  
			  (    
				SELECT    PS_ACCOUNT_ID                                                         AS PS_ID
						, SUM(TE_THEORETICAL_PAYOUT)                                            AS PS_PO
						, SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)                                   AS PS_PA
						, SUM(TE_THEORETICAL_PAYOUT) * SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)      AS PS_RESULT
				  FROM    TERMINALS 
			INNER JOIN    PLAY_SESSIONS
					ON    PLAY_SESSIONS.PS_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
				 WHERE    PS_FINISHED >= @DateStart                       
					   AND    PS_FINISHED <  @DateEnd                       
					   AND    PS_STATUS   <> 0                      
			  GROUP BY    PS_ACCOUNT_ID
						, PS_PLAY_SESSION_ID
			  ) PO_RESULT
		  GROUP BY PO_RESULT.PS_ID
		) POS 			
		ON POS.PS_ID = ACCOUNT_INFO.AC_ACCOUNT_ID  
    
  -- HANDPAYS (JACKPOTS)
	  LEFT JOIN
			(
			SELECT    PS_ACCOUNT_ID             AS PS_ACCOUNT_ID
					, SUM(HP_AMOUNT)            AS HP_AMOUNT
			  FROM    HANDPAYS  
		INNER JOIN    PLAY_SESSIONS
				ON    PS_PLAY_SESSION_ID = HP_PLAY_SESSION_ID
			 WHERE    HP_DATETIME >= @DateStart   
			   AND    HP_DATETIME <  @DateEnd 
			   AND    HP_TYPE IN(1, 20, 1001)   --  Jackpots(Progressive, Non Progressive, Major Prize), Site Jackpots and Manual Jackpots
			   AND    HP_STATUS & 61440 = 32768 --  Paid
		  GROUP BY    PS_ACCOUNT_ID
		) JACKPOTS
	  ON ACCOUNT_INFO.AC_ACCOUNT_ID = JACKPOTS.PS_ACCOUNT_ID
 
 
	CREATE INDEX IDX_TEMP_ACCOUNT_INFO ON #TEMP_ACCOUNT_INFO (PVH_ACCOUNT_ID, PVH_DATE)
	
	INSERT INTO H_PVH
		(
			  PVH_ACCOUNT_ID
			, PVH_DATE
			, PVH_WEEKDAY             
			, PVH_VISIT               
			, PVH_CHECK_IN            
			, PVH_CHECK_OUT           
			, PVH_ROOM_TIME           
			, PVH_GAME_TIME           
			, PVH_TOTAL_PLAYED_COUNT  
			, PVH_PLAYED_WON_COUNT    
			, PVH_TOTAL_PLAYED        
			, PVH_TOTAL_BET_AVG       
			, PVH_JACKPOTS_WON        
			, PVH_TOTAL_WON           
			, PVH_RE_PLAYED           
			, PVH_NR_PLAYED           
			, PVH_THEORICAL_WON       
			, PVH_MONEY_IN            
			, PVH_MONEY_IN_TAX        
			, PVH_MONEY_OUT           
			, PVH_RE_WON              
			, PVH_NR_WON              
			, PVH_DEVOLUTION          
			, PVH_PRIZE               
			, PVH_TAX1                
			, PVH_TAX2                
			, PVH_MONEY_IN_COUNT      
			, PVH_MONEY_OUT_COUNT     
			, PVH_AGE                 
			, PVH_REGISTERED_NUM_DAYS 
			, PVH_LEVEL               	
		)
	SELECT
		  TEMP.PVH_ACCOUNT_ID
		, TEMP.PVH_DATE
		, TEMP.PVH_WEEKDAY             
		, TEMP.PVH_VISIT              
		, TEMP.PVH_CHECK_IN           
		, TEMP.PVH_CHECK_OUT          
		, TEMP.PVH_ROOM_TIME          
		, TEMP.PVH_GAME_TIME          
		, TEMP.PVH_TOTAL_PLAYED_COUNT 
		, TEMP.PVH_PLAYED_WON_COUNT   
		, TEMP.PVH_TOTAL_PLAYED       
		, TEMP.PVH_TOTAL_BET_AVG      
		, TEMP.PVH_JACKPOTS_WON       
		, TEMP.PVH_TOTAL_WON          
		, TEMP.PVH_RE_PLAYED          
		, TEMP.PVH_NR_PLAYED          
		, TEMP.PVH_THEORICAL_WON      
		, TEMP.PVH_MONEY_IN           
		, TEMP.PVH_MONEY_IN_TAX       
		, TEMP.PVH_MONEY_OUT          
		, TEMP.PVH_RE_WON             
		, TEMP.PVH_NR_WON             
		, TEMP.PVH_DEVOLUTION         
		, TEMP.PVH_PRIZE              
		, TEMP.PVH_TAX1               
		, TEMP.PVH_TAX2               
		, TEMP.PVH_MONEY_IN_COUNT     
		, TEMP.PVH_MONEY_OUT_COUNT    
		, TEMP.PVH_AGE                
		, TEMP.PVH_REGISTERED_NUM_DAYS
		, TEMP.PVH_LEVEL              	
	FROM #TEMP_ACCOUNT_INFO TEMP
	WHERE NOT EXISTS (SELECT 1 FROM H_PVH WHERE TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE)	
	SET @ROW_COUNT = @@ROWCOUNT
	
	UPDATE H_PVH
	SET
		  H_PVH.PVH_WEEKDAY             =  TEMP.PVH_WEEKDAY             
		, H_PVH.PVH_VISIT               =  TEMP.PVH_VISIT              
		, H_PVH.PVH_CHECK_IN            =  TEMP.PVH_CHECK_IN           
		, H_PVH.PVH_CHECK_OUT           =  TEMP.PVH_CHECK_OUT          
		, H_PVH.PVH_ROOM_TIME           =  TEMP.PVH_ROOM_TIME          
		, H_PVH.PVH_GAME_TIME           =  TEMP.PVH_GAME_TIME          
		, H_PVH.PVH_TOTAL_PLAYED_COUNT  =  TEMP.PVH_TOTAL_PLAYED_COUNT 
		, H_PVH.PVH_PLAYED_WON_COUNT    =  TEMP.PVH_PLAYED_WON_COUNT   
		, H_PVH.PVH_TOTAL_PLAYED        =  TEMP.PVH_TOTAL_PLAYED       
		, H_PVH.PVH_TOTAL_BET_AVG       =  TEMP.PVH_TOTAL_BET_AVG      
		, H_PVH.PVH_JACKPOTS_WON        =  TEMP.PVH_JACKPOTS_WON       
		, H_PVH.PVH_TOTAL_WON           =  TEMP.PVH_TOTAL_WON          
		, H_PVH.PVH_RE_PLAYED           =  TEMP.PVH_RE_PLAYED          
		, H_PVH.PVH_NR_PLAYED           =  TEMP.PVH_NR_PLAYED          
		, H_PVH.PVH_THEORICAL_WON       =  TEMP.PVH_THEORICAL_WON      
		, H_PVH.PVH_MONEY_IN            =  TEMP.PVH_MONEY_IN           
		, H_PVH.PVH_MONEY_IN_TAX        =  TEMP.PVH_MONEY_IN_TAX       
		, H_PVH.PVH_MONEY_OUT           =  TEMP.PVH_MONEY_OUT          
		, H_PVH.PVH_RE_WON              =  TEMP.PVH_RE_WON             
		, H_PVH.PVH_NR_WON              =  TEMP.PVH_NR_WON             
		, H_PVH.PVH_DEVOLUTION          =  TEMP.PVH_DEVOLUTION         
		, H_PVH.PVH_PRIZE               =  TEMP.PVH_PRIZE              
		, H_PVH.PVH_TAX1                =  TEMP.PVH_TAX1               
		, H_PVH.PVH_TAX2                =  TEMP.PVH_TAX2               
		, H_PVH.PVH_MONEY_IN_COUNT      =  TEMP.PVH_MONEY_IN_COUNT     
		, H_PVH.PVH_MONEY_OUT_COUNT     =  TEMP.PVH_MONEY_OUT_COUNT    
		, H_PVH.PVH_AGE                 =  TEMP.PVH_AGE                
		, H_PVH.PVH_REGISTERED_NUM_DAYS =  TEMP.PVH_REGISTERED_NUM_DAYS
		, H_PVH.PVH_LEVEL               =  TEMP.PVH_LEVEL              	
	FROM H_PVH
	INNER JOIN #TEMP_ACCOUNT_INFO TEMP ON TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE
	
	SET @ROW_COUNT = @ROW_COUNT + @@ROWCOUNT
	SELECT @ROW_COUNT
	
 
	

END


