﻿
--------------------------------------------------------------------------------
-- Copyright © 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CustomersPlaying.sql
-- 
--   DESCRIPTION: Report for players in session monitor
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 21-JUL-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-JUL-2015 AMF    First release.
-- 16-DEC-2015 RGR    Add Columns for insert in table online_player_tracking
-- 21-JUL-2015 ETP    Bug 12163: Dual Currency - Add Iso code filter & Output.
-- 08-NOV-2016 JCA    Change to no use function on where clause (TodayOpening)
-- 06-SEP-2017 ETP    Bug 29602: Delay caused by te_iso_code.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomersPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CustomersPlaying]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Alberto Marcos>
-- Create date: <21-JUL-2015>
-- Description:   <Customers Playing>
-- =============================================

CREATE PROCEDURE [dbo].[CustomersPlaying]
AS
BEGIN
         -- SET NOCOUNT ON added to prevent extra result sets from
         -- interfering with SELECT statements.
         SET NOCOUNT ON;

   -- TODAY OPENING
   DECLARE @opening AS DATETIME
       SET @opening = dbo.TodayOpening(0)
   
   -- TERMINALES
           SELECT   TE_TERMINAL_ID
                  , TE_PROV_ID
                  , ISNULL(PV_NAME,'') AS PV_NAME
                  , ISNULL(TE_NAME,'') AS TE_NAME
                  , ISNULL(TE_FLOOR_ID,'') AS TE_FLOOR_ID
                  , ISNULL(BK_AREA_ID, -1) AS BK_AREA_ID --'AREA'
                  , ISNULL(AR_NAME,'') AS AR_NAME
                  , ISNULL(TE_BANK_ID, -1) AS TE_BANK_ID --'ISLA'
                  , ISNULL(BK_NAME,'') AS BK_NAME
                  , ISNULL(TE_POSITION, -1) AS TE_POSITION --'POSICION'
                  -- JUGADORES
                  , AC_ACCOUNT_ID
                  , ISNULL(AC_HOLDER_NAME,'') AS AC_HOLDER_NAME
                  , ISNULL(AC_TRACK_DATA,'') AS AC_TRACK_DATA
                  , AC_HOLDER_LEVEL AS AC_HOLDER_LEVEL
                  , ( SELECT   GP_KEY_VALUE     
                        FROM   GENERAL_PARAMS   
                       WHERE   GP_GROUP_KEY   = 'PlayerTracking'  
                         AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' 
                     ) AS AC_LEVEL                      
                  , AC_HOLDER_IS_VIP        
                  -- SESION DE JUEGO
                  , PS1.PS_PLAY_SESSION_ID
                  , PS1.PS_STARTED
                  , DATEDIFF(SECOND, PS1.PS_STARTED, ISNULL(PS1.PS_FINISHED, GETDATE())) AS PS1PS_DURATION
                  , PS1.PS_PLAYED_COUNT
                  , PS1.PS_PLAYED_AMOUNT
                  , CASE WHEN  PS1.PS_PLAYED_COUNT = 0 THEN '' 
				         ELSE  ROUND((PS1.PS_PLAYED_AMOUNT / PS1.PS_PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
                  -- CLIENTE JORNADA
                  , WD.DURATION AS WD_DURATION
                  , WD.PLAYED_COUNT AS WD_PLAYED_COUNT
                  , WD.PLAYED_AMOUNT AS WD_PLAYED_AMOUNT
                  , WD.PS_AVERAGE_BET AS WD_AVERAGE_BET
                  , AC_TYPE
                  , AC_CREATED
                  , AC_HOLDER_BIRTH_DATE            
                  , AC_HOLDER_WEDDING_DATE
                  , AC_USER_TYPE -- GMV 27-10-2015 52755 - HighRollers
                  , PS1.PS_FINISHED -- GMV 27-10-2015 52755 - HighRollers
                  , ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS -- GMV 27-10-2015 52755 - HighRollers
                  , terminals.TE_ISO_CODE
             FROM ( SELECT   PS.PS_ACCOUNT_ID
                           , PS.DURATION
                           , PS.PLAYED_COUNT
                           , PS.PLAYED_AMOUNT
                           , CASE WHEN  PS.PLAYED_COUNT = 0 THEN NULL 
						          ELSE  ROUND((PS.PLAYED_AMOUNT / PS.PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
                      FROM ( SELECT   PS_ACCOUNT_ID
                                    , SUM(DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE()))) AS DURATION
                                    , SUM(PS_PLAYED_COUNT) AS PLAYED_COUNT
                                    , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT
                               FROM   PLAY_SESSIONS
                         INNER JOIN   TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
                              WHERE   PS_STARTED >= @opening --dbo.TodayOpening(0) 
                           GROUP BY   PS_ACCOUNT_ID) AS PS ) AS WD
                  , TERMINALS
       INNER JOIN   PROVIDERS                  ON  TE_PROV_ID                 = PV_ID
       INNER JOIN   BANKS                      ON  TE_BANK_ID                 = BK_BANK_ID
       INNER JOIN   AREAS                      ON  BK_AREA_ID                 = AR_AREA_ID
       INNER JOIN   PLAY_SESSIONS PS1          ON  TE_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
       INNER JOIN   ACCOUNTS                   ON  PS_ACCOUNT_ID              = AC_ACCOUNT_ID
        LEFT JOIN   TERMINAL_STATUS            ON  TE_TERMINAL_ID             = TS_TERMINAL_ID 
            WHERE   TE_CURRENT_PLAY_SESSION_ID IS  NOT NULL
              AND   WD.PS_ACCOUNT_ID            =  AC_ACCOUNT_ID 
END

GO

GRANT EXECUTE ON [dbo].[CustomersPlaying] TO [wggui] WITH GRANT OPTION

GO
