 --------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsHistory.sql
-- 
--   DESCRIPTION: Real time group cashier movements (by Hour or by Id)
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 10-FEB-2014  AJQ & RCI & RMS    The historicize by hour will be made in a separate thread
-- 10-MAR-2014 RMS    Return real number of movements pending 
-- 02-APR-2014 SMN		Defect WIG-786
-- 29-MAR-2017 JBP		Bug 25289: Reportes contables muestran discrepancias en tickets TITO
-- 09-MAR-2017 DHA&RAB Bug 25248: Cashier: Incorrect count to cancel a ticket TITO
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsHistory]
GO

CREATE PROCEDURE [dbo].[CashierMovementsHistory]        
                        @pSessionId             BIGINT
                       ,@MovementId             BIGINT
                       ,@pType                  INT    
                       ,@pSubType               INT
                       ,@pInitialBalance        MONEY 
                       ,@pAddAmount             MONEY
                       ,@pSubAmmount            MONEY
                       ,@pFinalBalance          MONEY
                       ,@pCurrencyCode          NVARCHAR(3)
                       ,@pAuxAmount             MONEY
                       ,@pCurrencyDenomination  MONEY
                       ,@pCurrencyCageType      INT
AS
BEGIN 
  DECLARE @_idx_try int
  DECLARE @_row_count int

  SET @pCurrencyDenomination = ISNULL(@pCurrencyDenomination, 0)
  SET @pCurrencyCageType     = ISNULL(@pCurrencyCageType, 0)
  
/******** Grouped by ID ***************/

  SET @_idx_try = 0

  WHILE @_idx_try < 2
  BEGIN
    UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       SET   CM_TYPE_COUNT            = CM_TYPE_COUNT      +  CASE WHEN @pSubAmmount > 0 THEN 1 ELSE -1 END 
           , CM_SUB_AMOUNT            = CM_SUB_AMOUNT      + ISNULL(@pSubAmmount, 0)
           , CM_ADD_AMOUNT            = CM_ADD_AMOUNT      + ISNULL(@pAddAmount, 0)
           , CM_AUX_AMOUNT            = CM_AUX_AMOUNT      + ISNULL(@pAuxAmount, 0)
           , CM_INITIAL_BALANCE       = CM_INITIAL_BALANCE + ISNULL(@pInitialBalance, 0)
           , CM_FINAL_BALANCE         = CM_FINAL_BALANCE   + ISNULL(@pFinalBalance, 0)
     WHERE   CM_SESSION_ID            = @pSessionId 
       AND   CM_TYPE                  = @pType 
       AND   CM_SUB_TYPE              = @pSubType 
       AND   CM_CURRENCY_ISO_CODE     = @pCurrencyCode
       AND   CM_CURRENCY_DENOMINATION = @pCurrencyDenomination
       AND   CM_CAGE_CURRENCY_TYPE    = @pCurrencyCageType

    SET @_row_count = @@ROWCOUNT
   
    IF @_row_count = 1 BREAK

    IF @_idx_try = 1
    BEGIN
      RAISERROR ('Update into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
    END

    BEGIN TRY
      -- Entry doesn't exists, lets insert that
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
                  ( CM_SESSION_ID
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_TYPE_COUNT
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_CAGE_CURRENCY_TYPE)
           VALUES
                  ( @pSessionId
                  , @pType        
                  , @pSubType
                  , 1  
                  , @pCurrencyCode
                  , @pCurrencyDenomination
                  , ISNULL(@pSubAmmount, 0)
                  , ISNULL(@pAddAmount, 0)
                  , ISNULL(@pAuxAmount, 0)             
                  , ISNULL(@pInitialBalance, 0)
                  , ISNULL(@pFinalBalance, 0)
                  , ISNULL(@pCurrencyCageType, 0))

			SET @_row_count = @@ROWCOUNT

      -- Update history flag in CASHIER_SESSIONS
      -- UPDATE CASHIER_SESSIONS SET CS_HISTORY = 1 WHERE CS_SESSION_ID = @pSessionId

      -- All ok, exit WHILE      
      BREAK

    END TRY
    BEGIN CATCH
    END CATCH

    SET @_idx_try = @_idx_try + 1
  END
  
  IF @_row_count = 0
  BEGIN
    RAISERROR ('Insert into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
  END

  -- AJQ & RCI & RMS 10-FEB-2014: Movements historicized by hour are not inserted here. They serialize all workers and reduce performance.
  --                              A pending cashier movement to historify will be inserted in table CASHIER_MOVEMENTS_PENDING_HISTORY.
  --                              Later a thread will historify these pending movements.
  INSERT INTO CASHIER_MOVEMENTS_PENDING_HISTORY (CMPH_MOVEMENT_ID, CMPH_SUB_TYPE) VALUES (@MovementId, @pSubType) 

END --[CashierMovementsHistory]
GO

GRANT EXECUTE ON [dbo].[CashierMovementsHistory] TO [wggui] WITH GRANT OPTION
GO

/*
----------------------------------------------------------------------------------------------------------------
PERFOM HISTORY OF CASHIER AND MOBILE BANK MOVEMENTS PER HOUR

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    12-FEB-2014    RMS      First Release
1.0.1    17-FEB-2014    RMS      Added parameter for Currency ISO Code
1.0.2		 07-JUL-2017		EOR			 Fixed Bug 28596: WIGOS-3000 Cash summary variance to activity summary report

Parameters:
   -- MAXITEMSPERLOOP:    Maximum number of items to process.
                      
 Results:
   Return the number of items processed and the total of pending items to process before start.
----------------------------------------------------------------------------------------------------------------   
*/
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateHistoricalMovements_ByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
GO
 
CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
@pMaxItemsPerLoop Integer, 
@pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

-- Declarations
DECLARE @_max_movements_per_loop Integer
DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                                          CMPH_SUB_TYPE Integer)   
DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                                          UM_TYPE Integer, 
                                          UM_SUBTYPE Integer, 
                                          UM_CURRENCY_ISO_CODE NVarChar(3), 
                                          UM_CURRENCY_DENOMINATION Money,
                                          UM_CAGE_CURRENCY_TYPE Integer )

-- Initialize max items per loop
SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- If there are no items in pending movements table
IF @_total_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), CAST(0 AS Integer)
   
   RETURN
END

-- Get the movements id's to be updated
INSERT   INTO @_tmp_updating_movements
SELECT   TOP(@_max_movements_per_loop) 
         CMPH_MOVEMENT_ID
       , CMPH_SUB_TYPE
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- Obtain the number of movements to update
SELECT   @_updated_movements_count = COUNT(*) 
  FROM   @_tmp_updating_movements 

-- If there are items to update process it
IF @_updated_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), @_total_movements_count 
   
   RETURN
END

-- DELETE THE TO BE UPDATED REGS FROM PENDING
DELETE   X
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY AS X
 INNER   JOIN @_tmp_updating_movements AS Z ON Z.CMPH_MOVEMENT_ID = X.CMPH_MOVEMENT_ID AND Z.CMPH_SUB_TYPE = X.CMPH_SUB_TYPE

-- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
       , X.CM_CAGE_CURRENCY_TYPE
  INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
  FROM
       (
         SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
                , CM_TYPE
                , 0 AS CM_SUB_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
                , SUM(CASE WHEN CM_UNDO_STATUS IS NULL THEN 1 ELSE 0 END) AS CM_TYPE_COUNT
                , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
                , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
                , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
                , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
                , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)               AS CM_CAGE_CURRENCY_TYPE
           FROM   @_tmp_updating_movements
          INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
          WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
                , CM_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)
       ) AS X 

---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
            , MBM_TYPE   AS CM_TYPE
            , 1          AS CM_SUB_TYPE
            , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
            , 0                AS CM_CURRENCY_DENOMINATION
            , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
            , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
            , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
            , 0 AS CM_AUX_AMOUNT
            , 0 AS CM_INITIAL_BALANCE
            , 0 AS CM_FINAL_BALANCE
            , 0 AS CM_CAGE_CURRENCY_TYPE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
   GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
            , MBM_TYPE 

-- Update the items already in database triggering the updates into a temporary table of updated elements
UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
   SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION
       , INSERTED.CM_CAGE_CURRENCY_TYPE
  INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION, UM_CAGE_CURRENCY_TYPE)
  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
 INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                                                AND T.CM_TYPE                  = S.CM_TYPE
                                                AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                                                AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                                                AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 
                                                AND T.CM_CAGE_CURRENCY_TYPE    = S.CM_CAGE_CURRENCY_TYPE

-- Insert the elements not previously updated (update items not present in temporary updated items)
INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
           (   CM_DATE
             , CM_TYPE
             , CM_SUB_TYPE
             , CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT     
             , CM_ADD_AMOUNT      
             , CM_AUX_AMOUNT     
             , CM_INITIAL_BALANCE 
             , CM_FINAL_BALANCE  
             , CM_CAGE_CURRENCY_TYPE )
     SELECT   CM_DATE
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_TYPE_COUNT
            , CM_SUB_AMOUNT     
            , CM_ADD_AMOUNT      
            , CM_AUX_AMOUNT     
            , CM_INITIAL_BALANCE 
            , CM_FINAL_BALANCE
            , CM_CAGE_CURRENCY_TYPE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                           FROM   @_tmp_updated_movements NTI
                          WHERE   NTI.UM_DATE                  = CM_DATE
                            AND   NTI.UM_TYPE                  = CM_TYPE
                            AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                            AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                            AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION
                            AND   NTI.UM_CAGE_CURRENCY_TYPE    = CM_CAGE_CURRENCY_TYPE) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
  
-- Return values
SELECT @_updated_movements_count, @_total_movements_count

-- Erase the temporary data
DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 

END  -- End Procedure
GO

-- Permissions
GRANT EXECUTE ON [dbo].[SP_GenerateHistoricalMovements_ByHour] TO [wggui] WITH GRANT OPTION ;
GO
