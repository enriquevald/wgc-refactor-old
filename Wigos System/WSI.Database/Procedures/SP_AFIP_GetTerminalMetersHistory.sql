﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetRequestedTerminalStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AFIP_GetRequestedTerminalStatus]
GO

/*
---------------------------------------------------------------------------------
Given a date and a terminal, it returns the terminal status

Version    Date             User       Description
---------------------------------------------------------------------------------
1.0.0      14-DEC-2017      FGB        Initial version

Parameters:
   -- pRequestDateTime:             Date to check
   -- pTerminalStatusRetired:       Terminal status retired
   -- pTerminalAFIP:                Terminal request (only a terminal)
   -- pTerminalType:                Terminal type
   -- pStatusRequestedTerminal:     Status of the requested terminal (OUTPUT)
*/
CREATE PROCEDURE [dbo].[SP_AFIP_GetRequestedTerminalStatus]
(
    @pRequestDateTime               DATETIME
  , @pTerminalStatusRetired         INT
  , @pTerminalAFIP                  NVARCHAR(100)
  , @pTerminalType                  NVARCHAR(4000)
  , @pStatusRequestedTerminal       INT       OUTPUT
)
AS
BEGIN
  --Internal variables
  DECLARE @ACTIVATION_DATE                DATETIME;
  DECLARE @RETIREMENT_DATE                DATETIME;
  DECLARE @STATUS                         INT;

  DECLARE @v_NUM_TERMINAL_STATUS_OK       INT;
  DECLARE @v_NUM_SESSIONS_CONNECTED       INT;

  DECLARE @v_TerminalID                   INT;

  --Terminal Status SAS error
  DECLARE @v_SAS_HOST_WITHOUT_ERROR       INT;

  --WCP Session
  DECLARE @v_WCP_SESSION_STATUS_OPEN      INT;
  DECLARE @v_WCP_SESSION_TIMEOUT          INT;

  --Status constants to return
  DECLARE @K_STATUS_ACTIVE                INT;
  DECLARE @K_STATUS_RETIRED               INT;
  DECLARE @K_STATUS_WITHOUT_METERS        INT;

  --Status constants to return
  SET @K_STATUS_ACTIVE                = 1;
  SET @K_STATUS_RETIRED               = 2;
  SET @K_STATUS_WITHOUT_METERS        = 3;

  --Terminal Status has no SAS error
  SET @v_SAS_HOST_WITHOUT_ERROR       = 0;

  --WCP Session is connected
  SET @v_WCP_SESSION_STATUS_OPEN      = 0;

  -- Request date is invalid
  IF (@pRequestDateTime IS NULL)
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_RETIRED
    RETURN
  END

  -- Terminal registration code is invalid
  IF (@pTerminalAFIP IS NULL)
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_RETIRED
    RETURN
  END

  --Get Terminal ID
  SET @v_TerminalID = ( SELECT   MAX(te.TE_TERMINAL_ID)
                          FROM   TERMINALS te
                         WHERE   (te.TE_REGISTRATION_CODE = @pTerminalAFIP)
                           -- Only show terminal with the terminal type
                           AND   ((ISNULL(@pTerminalType, '') = '')  OR  (te.TE_TERMINAL_TYPE IN (SELECT   tt.SST_VALUE
                                                                                                    FROM   SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))
                      );
  
  -- Terminal registration code is invalid
  IF (@v_TerminalID IS NULL)
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_RETIRED
    RETURN
  END

  --Get terminal data
  SELECT   @ACTIVATION_DATE  =  TE_ACTIVATION_DATE
        ,  @RETIREMENT_DATE  =  TE_RETIREMENT_DATE
        ,  @STATUS           =  TE_STATUS
    FROM   TERMINALS
   WHERE   TE_TERMINAL_ID = @v_TerminalID

  --Terminal not found or is not yet active
  IF ((@ACTIVATION_DATE IS NULL) OR (@pRequestDateTime < @ACTIVATION_DATE))
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_RETIRED
    RETURN
  END

  --Terminal has been retired
  --  If a terminal is RETIRED after it can NOT BE activated again
  --    , it has to be created as a new terminal.
  IF ( (@STATUS = @pTerminalStatusRetired)
      AND (@pRequestDateTime >= @RETIREMENT_DATE) )
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_RETIRED
    RETURN
  END

  --Check terminal status?
  SET @v_NUM_TERMINAL_STATUS_OK = (SELECT   COUNT(*)
                                     FROM   TERMINAL_STATUS
                                    WHERE   TS_TERMINAL_ID  =  @v_TerminalID
                                      AND   ISNULL(TS_SAS_HOST_ERROR, @v_SAS_Host_without_error) = @v_SAS_Host_without_error)

  IF (ISNULL(@v_NUM_TERMINAL_STATUS_OK, 0) = 0)
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_WITHOUT_METERS
    RETURN
  END

  --Get timeout (in seconds) for WCP session open status
  SET @v_WCP_SESSION_TIMEOUT = (SELECT   CAST(GP_KEY_VALUE AS INT)
                                  FROM   GENERAL_PARAMS
                                 WHERE   GP_GROUP_KEY    =  'AFIPClient'
                                   AND   GP_SUBJECT_KEY  =  'RequestStatusSessionTimeout')

  --Exists terminal active connection in wcp_session?
  SET @v_NUM_SESSIONS_CONNECTED = (SELECT   COUNT(*)
                                     FROM   WCP_SESSIONS
                                    WHERE   WS_TERMINAL_ID  =  @v_TerminalID
                                      AND   WS_STATUS       =  @v_WCP_SESSION_STATUS_OPEN
                                      AND   (DATEDIFF(SECOND, WS_LAST_RCVD_MSG, @pRequestDateTime)  <=  @v_WCP_SESSION_TIMEOUT))

  IF (ISNULL(@v_NUM_SESSIONS_CONNECTED, 0) = 0)
  BEGIN
    SET @pStatusRequestedTerminal = @K_STATUS_WITHOUT_METERS
    RETURN
  END

  --If it arrives here, the terminal is active
  SET @pStatusRequestedTerminal = @K_STATUS_ACTIVE

  RETURN
END
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetRequestedTerminalStatus] TO [WGGUI] WITH GRANT OPTION
GO

