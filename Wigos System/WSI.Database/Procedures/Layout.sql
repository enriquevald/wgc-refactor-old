 --------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CreateDefaultProfilesLayout.sql
-- 
--   DESCRIPTION: Create Layout Profiles with default permissions.
-- 
--        AUTHOR: Fernando Jimιnez
-- 
-- CREATION DATE: 13-FEB-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 13-FEB-2015 FJC    First release.
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Create Layout Profiles with default permissions.
-- 
--  PARAMS:
--      - INPUT:
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateDefaultProfilesLayout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateDefaultProfilesLayout]

GO

CREATE PROCEDURE [dbo].[CreateDefaultProfilesLayout]
AS 
BEGIN  
  
  DECLARE @ProfileId   BIGINT
  DECLARE @ProfileName NVARCHAR(40)
  DECLARE @pGuiId      INT
  
  SET @ProfileId = 0
  SET @ProfileName = ''
  SET @pGuiId = 202
  
  --If permissions are in BD
  IF EXISTS (SELECT GF_GUI_ID FROM GUI_FORMS WHERE GF_GUI_ID = @pGuiId)
  BEGIN 
  
    --********************************
    --**PCA - PLAYERS CLUB ATTENDANT**
    --********************************
    SET @ProfileName = 'LAYOUT - PLAYERS CLUB ATTENDANT'     
    
    --Check if profile exists
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Get Last Profile ID (like Gui doing it)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert Profile
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      
      --Assign permissions for profile
      --(PCA - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
           
      --(PCA - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010000, 1, 1, 1, 1) 
           
      --(PCA - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010100, 0, 0, 0, 0) 
           
      --(PCA - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010200, 1, 1, 1, 1) 
           
      --(PCA - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010201, 1, 1, 1, 1) 
      
      --(PCA - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010202, 1, 1, 1, 1) 
           
      --(PCA - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010203, 1, 1, 1, 1) 
           
      --(PCA - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010204, 1, 1, 1, 1) 

      --(PCA - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010205, 1, 1, 1, 1) 

      --(PCA - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010206, 1, 1, 1, 1) 

      --(PCA - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010207, 0, 0, 0, 0) 

      --Pantalla (PCA - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010208, 1, 1, 1, 1) 

      --(PCA - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010209, 1, 1, 1, 1) 

      --(PCA - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1010210, 1, 1, 1, 1) 

      --(PCA - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020000, 1, 1, 1, 1) 

      --(PCA  Floor  Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020100, 1, 1, 1, 1) 

 		 --(PCA  Floor  Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020200, 1, 1, 1, 1) 

      --(PCA  Floor  Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1020300, 1, 1, 1, 1) 

      --(PCA - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1030000, 1, 1, 1, 1) 

      --(PCA - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040000, 1, 1, 1, 1) 

      --(PCA - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040001, 1, 1, 1, 1) 

      --(PCA - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1040002, 1, 1, 1, 1) 
      
      --(PCA - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1050000, 1, 1, 1, 1) 
           
      --(PCA - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060000, 1, 1, 1, 1) 

      --(PCA - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060001, 1, 1, 1, 1) 

      --(PCA - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060100, 1, 1, 1, 1) 

      --(PCA - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1060200, 1, 1, 1, 1) 

      --(PCA - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070000, 0, 0, 0, 0) 

      --(PCA - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1070001, 0, 0, 0, 0) 
  
    END
    --********************************
    --**OPS - OPS MANAGER ************
    --********************************
    SET @ProfileName = 'LAYOUT - OPS MANAGER'     
    
    --Check if profile exists
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Get Last Profile ID (like Gui doing it)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert Profile
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assign permissions for profile
      --(OPS - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --(OPS - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010000, 1, 1, 1, 1)  
           
      --(OPS - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010100, 0, 0, 0, 0) 
           
      --(OPS - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010200, 1, 1, 1, 1)  
           
      --(OPS - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010201, 1, 1, 1, 1)  
      
      --(OPS - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010202, 1, 1, 1, 1)  
           
      --(OPS - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010203, 0, 0, 0, 0)  
           
      --(OPS - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010204, 0, 0, 0, 0)  

      --(OPS - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010205, 0, 0, 0, 0)  

      --(OPS - Dashboard - Evaluattion by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010206, 0, 0, 0, 0)  

      --(OPS - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010207, 1, 1, 1, 1) 

      --(OPS - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010208, 1, 1, 1, 1)  

      --(OPS - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010209, 1, 1, 1, 1)  

      --(OPS - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2010210, 1, 1, 1, 1)  

      --(OPS - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020000, 1, 1, 1, 1)  

      --(OPS  Floor  Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020100, 1, 1, 1, 1) 

 		  --(OPS  Floor  Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020200, 1, 1, 1, 1) 

      --(OPS  Floor  Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2020300, 1, 1, 1, 1) 

      --(OPS - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2030000, 1, 1, 1, 1)  

      --(OPS - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040000, 1, 1, 1, 1)  

      --(OPS - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040001, 1, 1, 1, 1)  

      --(OPS - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2040002, 1, 1, 1, 1)  
      
      --(OPS - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2050000, 1, 1, 1, 1) 
           
      --(OPS - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060000, 0, 0, 0, 0) 

      --(OPS - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060001, 0, 0, 0, 0) 

      --(OPS - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060100, 0, 0, 0, 0) 
     
      --(OPS - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2060200, 0, 0, 0, 0) 

      --(OPS - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070000, 1, 1, 1, 1) 

      --(OPS - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 2070001, 1, 1, 1, 1) 
      
      
    END

    --********************************
    --**SLO - SLOT ATTENDANT *********
    --********************************
    SET @ProfileName = 'LAYOUT - SLOT ATTENDANT'     
    
    --Check if profile exists
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Get Last Profile ID (like Gui doing it)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert Profile
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assign permissions for profile
      --(SLO - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --(SLO - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010000, 1, 1, 1, 1)  
           
      --(SLO - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010100, 0, 0, 0, 0) 
           
      --(SLO - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010200, 1, 1, 1, 1)  
           
      --(SLO - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010201, 1, 1, 1, 1)  
      
      --(SLO - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010202, 1, 1, 1, 1)  
           
      --(SLO - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010203, 1, 1, 1, 1)  
           
      --(SLO - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010204, 0, 0, 0, 0)  

      --(SLO - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010205, 0, 0, 0, 0)  

      --(SLO - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010206, 0, 0, 0, 0)  

      --(SLO - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010207, 0, 0, 0, 0) 

      --(SLO - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010208, 0, 0, 0, 0)  

      --(SLO - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010209, 0, 0, 0, 0)  

      --(SLO - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4010210, 0, 0, 0, 0)  

      --(SLO - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020000, 1, 1, 1, 1)  

      --(SLO  Floor  Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020100, 1, 1, 1, 1)  

      --Pantalla (SLO  Floor  Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020200, 1, 1, 1, 1)  

      --(SLO  Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4020300, 1, 1, 1, 1)  

      --(SLO - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4030000, 1, 1, 1, 1)  

      --(SLO - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040000, 1, 1, 1, 1)  

      --(SLO - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040001, 1, 1, 1, 1)  

      --(SLO - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4040002, 1, 1, 1, 1)  
      
      --(SLO - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4050000, 0, 0, 0, 0) 
           
      --(SLO - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060000, 1, 1, 1, 1) 

      --(SLO - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060001, 0, 0, 0, 0) 

      --(SLO - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060100, 1, 1, 1, 1) 

      --(SLO - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4060200, 1, 1, 1, 1) 

      --(SLO - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070000, 1, 1, 1, 1) 

      --(SLO - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 4070001, 0, 0, 0, 0) 
     
    END

    --********************************
    --**TCH - TECHNICAL SUPPORT ******
    --********************************
    SET @ProfileName = 'LAYOUT - TECHNICAL SUPPORT'     
    
    --Check if profile exists
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Get Last Profile ID (like Gui doing it)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert Profile
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assignar Permisos de cada pantalla al perfil creat
    
      --Assign permissions for profile
      --(TCH - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
           
      --(TCH - Dashboard)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010000, 1, 1, 1, 1)  
           
      --(TCH - Dashboard - Important)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010100, 0, 0, 0, 0) 
           
      --(TCH - Dashboard - Statistics)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010200, 1, 1, 1, 1)  
           
      --(TCH - Dashboard - Ocupattion)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010201, 1, 1, 1, 1)  
      
      --(TCH - Dashboard - Ocupattion by card)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010202, 1, 1, 1, 1)  
           
      --(TCH - Dashboard - VIP Customers)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010203, 1, 1, 1, 1)  
           
      --(TCH - Dashboard - Ocupattion Percentage by gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010204, 1, 1, 1, 1)  

      --(TCH - Dashboard - Ocupattion Percentage by age)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010205, 1, 1, 1, 1)  

      --(TCH - Dashboard - Evaluation by age and gender)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010206, 1, 1, 1, 1)  

      --(TCH - Dashboard - Technics issues percentage)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010207, 1, 1, 1, 1) 

      --(TCH - Dashboard - Coin in, ANW, spins by game and manufacturer representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010208, 1, 1, 1, 1)  

      --(TCH - Dashboard - Gain Floor by machine and coin representation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010209, 1, 1, 1, 1)  

      --(TCH - Dashboard - Coin in evaluation)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8010210, 1, 1, 1, 1)  

      --(TCH - Floor)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020000, 1, 1, 1, 1)  

      --(TCH  Floor  Compare Terminals)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020100, 1, 1, 1, 1)  

      --(TCH  Floor  Temperature Map)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020200, 1, 1, 1, 1)  

      --(TCH  Floor - Visualization)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8020300, 1, 1, 1, 1)  

      --(TCH - My Tasks)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8030000, 1, 1, 1, 1)  

      --(TCH - Alarms - All)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040000, 1, 1, 1, 1)  
    
      --(TCH - Alarms - Players Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040001, 1, 1, 1, 1)  

      --(TCH - Alarms - Machines Only)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8040002, 1, 1, 1, 1)  
      
      --(TCH - Charts)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8050000, 0, 0, 0, 0) 
           
      --(TCH - Players)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060000, 0, 0, 0, 0) 
      
      --(TCH - Players - List Mode)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060001, 0, 0, 0, 0) 

      --(TCH - Players - Find)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060100, 0, 0, 0, 0) 

      --(TCH - Players - Filter)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8060200, 0, 0, 0, 0) 

      --(TCH - Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070000, 1, 1, 1, 1) 

      --(TCH - Comparation Machines)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 8070001, 0, 0, 0, 0) 
      
    END
    
    
    --********************************
    --**ADM - ADMINISTRATOR **********
    --********************************
    SET @ProfileName = 'LAYOUT - ADMINISTRATOR'     
    
    --Check if profile exists
    IF NOT EXISTS (SELECT  GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE UPPER (GUP_NAME) LIKE @ProfileName ESCAPE '\' ) 
    BEGIN
    
      --Get Last Profile ID (like Gui doing it)
      SET @ProfileId = (SELECT MAX(ISNULL(GUP_PROFILE_ID, 0)) + 1  FROM GUI_USER_PROFILES)
      
      --Insert Profile
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME, GUP_MAX_USERS) VALUES (@ProfileId, @ProfileName, 0)
      
      --Assign permissions for profile
      --(ADM - LOGIN)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 1, 1, 1, 1, 1) 
      
      --(ADM - Configuration)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16010000, 0, 0, 0, 0) 

      --(ADM - Floor - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16020000, 0, 0, 0, 0) 

      --(ADM - Caption - Edit)
      INSERT INTO GUI_PROFILE_FORMS (GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM) 
           VALUES (@ProfileId, @pGuiId, 16030000, 0, 0, 0, 0) 
      
    END
  END
END

GO

GRANT EXECUTE ON [dbo].[CreateDefaultProfilesLayout] TO [wggui] WITH GRANT OPTION