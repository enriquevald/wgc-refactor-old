--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Audit.sql
-- 
--   DESCRIPTION: Audit procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 28-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 17-JUN-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Audit procedure execution
-- 
--  PARAMS:
--      - INPUT:
--         @ProcedureName nvarchar(50),
--         @AccountID     nvarchar(24), 
--         @VendorID      nvarchar(16), 
--         @SerialNumber  nvarchar(30), 
--         @MachineNumber int,
--         @StatusCode    int,
--         @Balance       Money,
--         @Input         nvarchar(MAX),
--         @Output        nvarchar(MAX) 
--      - OUTPUT:
--
-- RETURNS:
--      Nothing
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_Audit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_Audit]
GO
CREATE PROCEDURE [dbo].[zsp_Audit]
  @ProcedureName nvarchar(50),
  @AccountID     nvarchar(24), 
  @VendorID      nvarchar(16), 
  @SerialNumber  nvarchar(30), 
  @MachineNumber int,
  @SessionId     bigint,
  @StatusCode    int,
  @Balance       Money,
  @ForceAudit    bit,
  @Input         nvarchar(MAX),
  @Output        nvarchar(MAX) 
AS
BEGIN 

  DECLARE @active int
  
  -- Check general params
  SELECT @active = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='AuditEnabled'

  IF ( @active = 1 OR (@StatusCode <> 0 AND @StatusCode < 100) OR @ForceAudit = 1 )
  BEGIN

    INSERT INTO AUDIT_3GS ( A3GS_PROCEDURE
                          , A3GS_ACCOUNT_ID
                          , A3GS_VENDOR_ID
                          , A3GS_SERIAL_NUMBER
                          , A3GS_MACHINE_NUMBER
                          , A3GS_SESSION_ID
                          , A3GS_STATUS_CODE
                          , A3GS_BALANCE
                          , A3GS_INPUT
                          , A3GS_OUTPUT )
                   VALUES ( @ProcedureName
                          , @AccountID
                          , @VendorID
                          , @SerialNumber
                          , @MachineNumber
                          , @SessionId
                          , @StatusCode
                          , @Balance
                          , @Input
                          , @Output )
                          
  END      
                      
END -- [zsp_Audit]

GO