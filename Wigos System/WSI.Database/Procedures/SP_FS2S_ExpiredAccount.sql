﻿--------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SP_FS2S_ExpiredAccount.sql
-- 
--   DESCRIPTION: Procedure for FS2S expired accounts
-- 
--        AUTHOR: Enric Tomas
-- 
-- CREATION DATE: 02-OCT-2017
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-OCT-2017 ETP    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_FS2S_ExpiredAccount]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[SP_FS2S_ExpiredAccount]
END

GO

CREATE  PROCEDURE [dbo].[SP_FS2S_ExpiredAccount]
AS
BEGIN
    DECLARE @_am_reserved_expired AS INTEGER
    DECLARE @_start_session AS DATETIME

-- Moments type:
    SET @_am_reserved_expired = 266

-- Get started data	
     SELECT TOP 1 @_start_session = FL_CREATION 
       FROM       FBM_LOG 
      WHERE       FL_STATUS = 1 
   ORDER BY       FL_CREATION DESC	  
     
     SELECT   AM_TRACK_DATA   AS  TRACKDATA 
            , SUM(AM_SUB_AMOUNT)  AS  Value 
       FROM   ACCOUNT_MOVEMENTS
      WHERE   AM_TYPE = @_am_reserved_expired
        AND   AM_DATETIME > @_start_session
   GROUP BY   AM_TRACK_DATA

END

GO

GRANT EXECUTE ON [dbo].[SP_FS2S_ExpiredAccount] TO [wggui] WITH GRANT OPTION 

GO