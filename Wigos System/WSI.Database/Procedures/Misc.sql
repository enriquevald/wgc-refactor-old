--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Misc.sql
-- 
--   DESCRIPTION: Misc procedure functions
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 28-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 28-APR-2010 MBF    First release.
-- 10-MAY-2012 DDM    Modifications in SetPlaySessionBalanceMismatch, added 
--                    ps_reported_balance_mismatch field in the UPDATE
--                    
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Return the internal terminal ID
-- 
--  PARAMS:
--      - INPUT:
--        @VendorId 	   varchar(16)
--        @SerialNumber    varchar(30)
--        @MachineNumber   int
--      - OUTPUT:
--
-- RETURNS:
--      Internal Terminal ID, 0 if error
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTerminalID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetTerminalID]
GO
CREATE FUNCTION dbo.GetTerminalID
 (@VendorId 	   varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int) 
RETURNS INT
AS
BEGIN
  DECLARE @terminal_id int
  DECLARE @ignore_machine_number int
  
  SET @terminal_id = 0
  
	SELECT @ignore_machine_number = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber'
  
   SELECT @terminal_id = TE_TERMINAL_ID 
     FROM TERMINALS_3GS, TERMINALS
  WHERE T3GS_VENDOR_ID      = @VendorId
    AND T3GS_SERIAL_NUMBER  = @SerialNumber
    AND (     T3GS_MACHINE_NUMBER = @MachineNumber
          OR  @ignore_machine_number = 1 )
    AND T3GS_TERMINAL_ID    = TE_TERMINAL_ID
  
  RETURN @terminal_id
END -- GetTerminalID

GO

--------------------------------------------------------------------------------
-- PURPOSE: No terminal found, insert to Pending
-- 
--  PARAMS:
--      - INPUT:
--        @CardData 	    varchar(24)
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertTerminal]
GO
CREATE PROCEDURE dbo.InsertTerminal
 (@Source          int,
  @VendorId 	     varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int)
AS
BEGIN
  -- No terminal found, insert to Pending
  BEGIN TRAN 
  IF (( SELECT COUNT(*) 
          FROM TERMINALS_PENDING 
         WHERE TP_SOURCE         = @Source
           AND TP_VENDOR_ID      = @VendorId
           AND TP_SERIAL_NUMBER  = @SerialNumber) = 0)
  BEGIN
     INSERT INTO TERMINALS_PENDING (TP_SOURCE
                                 , TP_VENDOR_ID
                                 , TP_SERIAL_NUMBER
                                 , TP_MACHINE_NUMBER)
                           VALUES (@Source
                                 , @VendorId
                                 , @SerialNumber
                                 , @MachineNumber)
  END
  ELSE
  BEGIN
    -- MACHINE_NUMBER HAS CHANGED
    UPDATE TERMINALS_PENDING 
       SET TP_MACHINE_NUMBER = @MachineNumber
     WHERE TP_SOURCE         = @Source
       AND TP_VENDOR_ID      = @VendorId
       AND TP_SERIAL_NUMBER  = @SerialNumber   
  END
  COMMIT TRAN
END -- InsertTerminal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Return the account ID
-- 
--  PARAMS:
--      - INPUT:
--        @CardData 	    varchar(24)
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAccountID]
GO
CREATE FUNCTION dbo.GetAccountID
 (@CardData 	    varchar(24)) 
RETURNS bigint
AS
BEGIN
  DECLARE @internal nvarchar (255)

  -- -- Force 20 digits (like BETSTONE)
  SET @internal = dbo.TrackDataToInternal(RIGHT('00000000000000000000' + @CardData, 20))
  
  -- -- To use the received track data (like ZITRO, the account has to be updated with this track data)
  -- SET @internal = @CardData

  RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA LIKE @internal),0))

END -- GetAccountID

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if the account is blocked
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId      bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if it is NOT blocked
--      1 if it is blocked
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAccountBlocked]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckAccountBlocked]
GO
CREATE FUNCTION dbo.CheckAccountBlocked
 (@AccountId       bigint) 
RETURNS int
AS
BEGIN
  DECLARE @blocked int

  SELECT @blocked = AC_BLOCKED FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @AccountId
  IF @blocked = 1 RETURN 1

  RETURN 0
END -- CheckAccountBlocked

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if the terminal is blocked
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId	  int
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if it is NOT blocked
--      1 if it is blocked
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckTerminalBlocked]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckTerminalBlocked]
GO
CREATE FUNCTION dbo.CheckTerminalBlocked
 (@TerminalId	   int)
RETURNS int
AS
BEGIN
  DECLARE @blocked int
  DECLARE @status int
  DECLARE @disable_new_sessions int
  
  --- 
  --- Do not allow to start session if new sessions are disabled
  ---
  SELECT @disable_new_sessions = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Site'
     AND GP_SUBJECT_KEY = 'DisableNewSessions'
     
  IF @disable_new_sessions = 1 RETURN 1

  SELECT @blocked = TE_BLOCKED ,
         @status  = TE_STATUS 
    FROM TERMINALS 
   WHERE TE_TERMINAL_ID = @TerminalId
   
  -- Terminal Blocked or Retired
  IF @blocked = 1 OR @status <> 0 RETURN 1

  RETURN 0
END -- CheckTerminalBlocked

GO

--------------------------------------------------------------------------------
-- PURPOSE: Unlocks the play session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId	    bigint
--
--      - OUTPUT:
--
--   NOTES:
--      - @SessionId must be a valid Session Id. It is not checked in this procedure
--        that it is valid.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UnlockPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UnlockPlaySession]
GO
CREATE PROCEDURE dbo.UnlockPlaySession
 (@SessionId bigint)
AS
BEGIN
  UPDATE PLAY_SESSIONS 
  SET PS_LOCKED = NULL 
  WHERE PS_PLAY_SESSION_ID = @SessionId
END -- UnlockPlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: say if the play session is active (open)
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId	    bigint
--
--      - OUTPUT:
--
--   NOTES:
--      - @SessionId must be a valid Session Id. It is not checked in this procedure
--        that it is valid.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsPlaySessionActive]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsPlaySessionActive]
GO
CREATE FUNCTION dbo.IsPlaySessionActive
 (@SessionId bigint)
RETURNS int
AS
BEGIN
  IF NOT EXISTS  (SELECT PS_PLAY_SESSION_ID 
                    FROM PLAY_SESSIONS
                   WHERE PS_PLAY_SESSION_ID = @SessionId
                     AND PS_STATUS = 0)
    RETURN 0
  
  RETURN 1
END -- IsPlaySessionActive

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if terminal has an active session and it's open
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId       int
--        @SessionId        bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if false
--      1 if true
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckTerminalPlaySession]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckTerminalPlaySession]
GO
CREATE FUNCTION dbo.CheckTerminalPlaySession
 (@TerminalId    int,
  @PlaySessionId bigint)
RETURNS int
AS
BEGIN
  -- Check if terminal.te_session_id is equal to received pSessionId
  RETURN (SELECT COUNT(*)
            FROM PLAY_SESSIONS
           WHERE PS_TERMINAL_ID     = @TerminalId
             AND PS_PLAY_SESSION_ID = @PlaySessionId 
             AND PS_STATUS          = 0)
END -- CheckTerminalPlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check account.ac_balance vs pCreditBalance
-- 
--  PARAMS:
--      - INPUT:
--       @CreditBalance     money
--       @AccountId         bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if are not equal
--      1 if are equal
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAccountBalance]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckAccountBalance]
GO
CREATE FUNCTION dbo.CheckAccountBalance
  (@CreditBalance money,
   @AccountId bigint)
RETURNS int
AS
BEGIN
  IF NOT EXISTS (SELECT AC_ACCOUNT_ID FROM ACCOUNTS
                   WHERE AC_ACCOUNT_ID = @AccountId
                     AND AC_BALANCE = @CreditBalance)
  BEGIN
    RETURN 0
  END

  RETURN 1
END -- CheckAccountBalance

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check Gaming Data from the Play Session
--          Also calculates the delta values: DeltaValue = ActualValue - PreviousValue
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId      bigint
--        @CreditsPlayed  money,
--        @CreditsWon     money,
--        @GamesPlayed    int,
--        @GamesWon       int
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAndGetPlaySessionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckAndGetPlaySessionData]
GO
CREATE PROCEDURE dbo.CheckAndGetPlaySessionData
 (@SessionId      bigint,
  @PlayedAmount   money,
  @WonAmount      money,
  @PlayedCount    int,
  @WonCount       int,
  @CreditBalance  money,
  @IsEndSession   int)
AS
BEGIN
  DECLARE @rc                   int
  DECLARE @status_code          int
  DECLARE @status_text          varchar (254)
  DECLARE @played_amount        money
  DECLARE @won_amount           money
  DECLARE @played_count         int
  DECLARE @won_count            int
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  IF (  ( @PlayedAmount  < 0 )
     OR ( @WonAmount     < 0 )
     OR ( @PlayedCount   < 0 )
     OR ( @WonCount      < 0 )
     OR ( @CreditBalance < 0 ))
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  SELECT   @played_count  = PS_PLAYED_COUNT
         , @played_amount = PS_PLAYED_AMOUNT
         , @won_count     = PS_WON_COUNT
         , @won_amount    = PS_WON_AMOUNT
    FROM   PLAY_SESSIONS
   WHERE   PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  -- 04-AUG-2010 MBF: Patch for CADILLAC vendor
  IF ( ( @IsEndSession <> 0 ) AND ( @WonCount = 0 ) )
  BEGIN
    SET @WonCount = @won_count
  END
    
  -- AJQ 18-SEP-2010, Accept counters going back

  SET @delta_played_amount = @PlayedAmount - @played_amount
  SET @delta_won_amount    = @WonAmount    - @won_amount
  SET @delta_played_count  = @PlayedCount  - @played_count
  SET @delta_won_count     = @WonCount     - @won_count

ERROR_PROCEDURE:
	SELECT @status_code         AS StatusCode,        @status_text      AS StatusText
	     , @delta_played_amount AS DeltaPlayedAmount, @delta_won_amount AS DeltaWonAmount
	     , @delta_played_count  AS DeltaPlayedCount,  @delta_won_count  AS DeltaWonCount

END -- CheckAndGetPlaySessionData


GO

--------------------------------------------------------------------------------
-- PURPOSE: Update Gaming Data from the Play Session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId      bigint
--        @CreditsPlayed  money,
--        @CreditsWon     money,
--        @GamesPlayed    int,
--        @GamesWon       int,
--        @CreditBalance  money
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdatePlaySessionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdatePlaySessionData]
GO
CREATE PROCEDURE dbo.UpdatePlaySessionData
 (@SessionId      bigint,
  @CreditsPlayed  money,
  @CreditsWon 	  money,
  @GamesPlayed 	  int,
  @GamesWon 	  int,
  @CreditBalance  money)
AS
BEGIN
  DECLARE @rc             int
  DECLARE @status_code    int
	DECLARE @status_text    varchar (254)

  SET @status_code = 0
	SET @status_text = ''

  UPDATE PLAY_SESSIONS
  SET PS_PLAYED_COUNT  = @GamesPlayed
    , PS_PLAYED_AMOUNT = @CreditsPlayed
    , PS_WON_COUNT     = @GamesWon
    , PS_WON_AMOUNT    = @CreditsWon
    , PS_FINAL_BALANCE = @CreditBalance
    , PS_FINISHED      = GETDATE()  
  WHERE PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- UpdatePlaySessionData

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update Gaming Data from the Play Session using Delta values
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId          bigint
--        @SessionId          bigint
--        @TerminalId         int
--        @DeltaPlayedAmount  money
--        @DeltaWonAmount     money
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAccountData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateAccountData]
GO
CREATE PROCEDURE dbo.UpdateAccountData
 (@AccountId         bigint,
  @SessionId         bigint,
  @TerminalId        int,
  @ReportedBalance   money,
  @IsEndSession      int)
AS
BEGIN
  DECLARE @is_promotion   int
  DECLARE @promo_balance  money
  DECLARE @balance        money
  DECLARE @rc             int
  DECLARE @status_code    int
  DECLARE @status_text    varchar (254)
  DECLARE @aux_amount     money
  DECLARE @cashin_playing money
  DECLARE @am_id_started  bigint

  SET @status_code = 0
    SET @status_text = ''

  SELECT @is_promotion   = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance  = AC_PROMO_BALANCE 
       , @balance        = AC_BALANCE 
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @AccountId )

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @is_promotion = 0
    SET @balance      = 0
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

  IF ( @is_promotion = 1 )
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @promo_balance  = @ReportedBalance 
  END
  ELSE
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @balance        = @ReportedBalance 
    
    SELECT   @cashin_playing    = ISNULL (AC_CASHIN_WHILE_PLAYING, 0)
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID      = @AccountId;
       
    -- AJQ 17-SEP-2010, Add all the CashIn's while playing
    SET @balance = @ReportedBalance + @cashin_playing      

  END

  IF ( @is_promotion = 1 )
  BEGIN
    UPDATE ACCOUNTS
    SET AC_PROMO_BALANCE  = @promo_balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
  END
  ELSE
  BEGIN
  
    IF ( @IsEndSession = 1 )
      BEGIN
        UPDATE ACCOUNTS
           SET AC_CASHIN_WHILE_PLAYING    = NULL
         WHERE AC_ACCOUNT_ID              = @AccountId
           AND AC_CURRENT_TERMINAL_ID     = @TerminalId
           AND AC_CURRENT_PLAY_SESSION_ID = @SessionId     	
    END
    
    UPDATE ACCOUNTS
    SET AC_BALANCE        = @balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
    
  END
  
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
  IF ( @is_promotion = 0 ) SET @aux_amount = @balance
  ELSE SET @aux_amount = @promo_balance

    SELECT @status_code AS StatusCode, @status_text AS StatusText, @aux_amount AS Balance

END -- UpdateAccountData

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update session (INTERNAL)
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      bigint
--          @TerminalId     int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionUpdate_Internal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SessionUpdate_Internal]
GO
CREATE PROCEDURE dbo.SessionUpdate_Internal
  @AccountId         bigint,
  @TerminalId        int,
  @SerialNumber      varchar(30),
  @SessionId         bigint,
  @AmountPlayed      money,
  @AmountWon         money,
  @GamesPlayed       int,
  @GamesWon          int,
  @CreditBalance     money,
  @CurrentJackpot    money = 0,
  @IsEndSession      int = 0,
  @DeltaAmountPlayed money OUTPUT,
  @DeltaAmountWon    money OUTPUT,
  @status_code       int OUTPUT,
  @status_text       nvarchar (254) OUTPUT
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @balance              money
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  -- Check if terminal has session active
  -- Check if terminal session_id is equal to received terminal id
  -- Check if play session is Open
  IF ((SELECT dbo.CheckTerminalPlaySession(@TerminalId, @SessionId)) <> 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END	
  
    -- Check play session data and get delta values (actual - previous)
  DECLARE @update_cs_table TABLE
     (StatusCode int, StatusText nvarchar(254)
    , DeltaPlayedAmount money, DeltaWonAmount money
	  , DeltaPlayedCount int, DeltaWonCount int)

  INSERT INTO @update_cs_table
    EXECUTE dbo.CheckAndGetPlaySessionData @SessionId, 
                                           @AmountPlayed, @AmountWon,
                                           @GamesPlayed,  @GamesWon, 
                                           @CreditBalance, 
                                           @IsEndSession

  SELECT @status_code         = StatusCode
       , @status_text         = StatusText
       , @delta_played_amount = DeltaPlayedAmount
       , @delta_won_amount    = DeltaWonAmount
       , @delta_played_count  = DeltaPlayedCount
       , @delta_won_count     = DeltaWonCount
    FROM @update_cs_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update account data!
  DECLARE @update_ac_table TABLE (StatusCode int, StatusText nvarchar(254), Balance money)
  INSERT INTO @update_ac_table
    EXECUTE dbo.UpdateAccountData @AccountId, @SessionId, @TerminalId, @CreditBalance, @IsEndSession
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @balance     = Balance
    FROM @update_ac_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE 

  -- Update Game Meters
  DECLARE @update_gm_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_gm_table
    EXECUTE dbo.UpdateGameMeters @TerminalId, 
                                 @delta_played_count, @delta_played_amount,
                                 @delta_won_count,    @delta_won_amount
  
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_gm_table
    
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update data play session
  DECLARE @update_se_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_se_table
    EXECUTE dbo.UpdatePlaySessionData @SessionId, @AmountPlayed, @AmountWon, @GamesPlayed,
                                      @GamesWon, @CreditBalance
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_se_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @DeltaAmountPlayed = @delta_played_amount
  SET @DeltaAmountWon    = @delta_won_amount


ERROR_PROCEDURE:

END -- SessionUpdate_Internal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Close Play Session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId       bigint
--
--      - OUTPUT:
--        @StatusCode int OUTPUT
--        @StatusText nvarchar (254) OUTPUT
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CloseOpenedPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CloseOpenedPlaySession]
GO
CREATE PROCEDURE dbo.CloseOpenedPlaySession
 (@SessionId bigint,
  @StatusCode int OUTPUT,
  @StatusText nvarchar (254) OUTPUT)
AS
BEGIN
  DECLARE @rc             int

  SET @StatusCode = 0
	SET @StatusText = ''

  UPDATE PLAY_SESSIONS
  SET PS_STATUS            = 1
    , PS_LOCKED            = NULL
    , PS_FINISHED          = GETDATE()
  WHERE PS_PLAY_SESSION_ID = @SessionId
    AND PS_STATUS          = 0
  
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 3
    SET @StatusText = 'Invalid Session ID number'
  END

END -- ClosePlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Close Account Session
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId       bigint
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CloseAccountSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CloseAccountSession]
GO
CREATE PROCEDURE dbo.CloseAccountSession
 (@AccountId bigint)
AS
BEGIN

  UPDATE ACCOUNTS
  SET AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID
    , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME
    , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID
    , AC_CURRENT_TERMINAL_ID     = NULL
    , AC_CURRENT_TERMINAL_NAME   = NULL
    , AC_CURRENT_PLAY_SESSION_ID = NULL
  WHERE AC_ACCOUNT_ID            = @AccountId
    AND AC_CURRENT_TERMINAL_ID     IS NOT NULL
    AND AC_CURRENT_PLAY_SESSION_ID IS NOT NULL
    AND AC_CURRENT_TERMINAL_NAME   IS NOT NULL
  
END -- CloseAccountSession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check final balance using the initial balance of the play session
-- 
--  PARAMS:
--      - INPUT:
--          @SessionId               bigint
--          @ReportedAmountPlayed    money
--          @ReportedAmountWon       money
--          @ReportedFinalBalance    money
--
--      - OUTPUT:
--          @CalculatedFinalBalance   money
--          @BalanceMismatch          bit
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckFinalBalanceMismatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckFinalBalanceMismatch]
GO
CREATE PROCEDURE dbo.CheckFinalBalanceMismatch
 (@SessionId                bigint,
  @ReportedAmountPlayed     money,
  @ReportedAmountWon        money,
  @ReportedFinalBalance     money,
  @CalculatedFinalBalance   money OUTPUT,
  @BigMismatch              bit OUTPUT,
  @BalanceMismatch          bit OUTPUT)
AS
BEGIN
  DECLARE @ps_initial_balance       money
  DECLARE @maximum_balance_error    money
  DECLARE @diff_balance             money

  SELECT   @ps_initial_balance = PS_INITIAL_BALANCE
    FROM   PLAY_SESSIONS
   WHERE   PS_PLAY_SESSION_ID  = @SessionId

  SELECT @maximum_balance_error = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='MaximumBalanceErrorCents'
   
  -- Convert Cents to money
  SET @maximum_balance_error = @maximum_balance_error / 100 
  
  -- Calculate Final Balance
  SET @CalculatedFinalBalance = @ps_initial_balance - @ReportedAmountPlayed + @ReportedAmountWon

  -- Calculate difference  
  SET @diff_balance = ABS(@ReportedFinalBalance - @CalculatedFinalBalance)

  SET @BigMismatch = 0
  IF ( @diff_balance > @maximum_balance_error )
  BEGIN
    SET @BigMismatch = 1
  END
  
  SET @BalanceMismatch = 0
  IF ( @diff_balance > 0 )
  BEGIN
    SET @BalanceMismatch = 1
  END

END -- CheckFinalBalanceMismatch

GO

--------------------------------------------------------------------------------
-- PURPOSE: Set ps_balance_mismatch flag into play session
-- 
--  PARAMS:
--      - INPUT:
--          @SessionId               bigint
--          @BalanceMismatch         bit
--          @CreditBalance           Money
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetPlaySessionBalanceMismatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SetPlaySessionBalanceMismatch]
GO
CREATE PROCEDURE dbo.SetPlaySessionBalanceMismatch
 (@SessionId                bigint,
  @BalanceMismatch          bit,
  @CreditBalance            Money)
AS
BEGIN

   --DDM 09-MAY-2012: Modified, for save the PS_BALANCE_REPORTED_MISMATCH     
  UPDATE   PLAY_SESSIONS 
     SET   PS_BALANCE_MISMATCH          = @BalanceMismatch 
         , PS_REPORTED_BALANCE_MISMATCH = CASE WHEN (@BalanceMismatch = 1) THEN @CreditBalance ELSE NULL END
   WHERE   PS_PLAY_SESSION_ID           = @SessionId

END -- SetPlaySessionBalanceMismatch

GO

-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AM_3GS_Trigger]'))
DROP TRIGGER [dbo].[AM_3GS_Trigger]
GO
CREATE TRIGGER [dbo].[AM_3GS_Trigger]
   ON  [dbo].[account_movements] 
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
DECLARE @mov_type    int
DECLARE @new_cashin  money
DECLARE @account_id  bigint
DECLARE @terminal_id int

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

    -- Check Movement Type
      -- 1 - CashIn
      -- 9 - NonRedeemable CashIn
      -- 44 - Prize Coupon
      SET @mov_type = (SELECT AM_TYPE FROM INSERTED)
    
      IF ( @mov_type = 1 OR @mov_type = 9 OR @mov_type = 44 ) 
    BEGIN
       SET @account_id  = (SELECT AM_ACCOUNT_ID FROM INSERTED)
         
       -- Check if it is in session
       SET @terminal_id = (SELECT AC_CURRENT_TERMINAL_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @account_id)
         -- Is Account 'InUse'?
       IF @terminal_id IS NOT NULL
       BEGIN
            -- Yes, It is 'InUse'
        -- Is 3GS Terminal?
            IF EXISTS ( SELECT T3GS_TERMINAL_ID FROM TERMINALS_3GS WHERE T3GS_TERMINAL_ID = @terminal_id )
        BEGIN
              -- 3GS Terminal
          SET @new_cashin  = (SELECT AM_ADD_AMOUNT FROM INSERTED)
          -- Update 'cashin_while_playing'
              UPDATE ACCOUNTS 
             SET AC_CASHIN_WHILE_PLAYING = ISNULL (AC_CASHIN_WHILE_PLAYING, 0) + @new_cashin
           WHERE AC_ACCOUNT_ID           = @account_id
             AND AC_CURRENT_TERMINAL_ID  = @terminal_id         
        END
       END
      END

END
GO   
