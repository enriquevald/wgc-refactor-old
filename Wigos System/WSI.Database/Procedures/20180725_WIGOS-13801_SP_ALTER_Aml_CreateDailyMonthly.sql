IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_CreateDailyMonthly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_CreateDailyMonthly]
GO

CREATE PROCEDURE [dbo].[AML_CreateDailyMonthly] 
  @Day DATETIME
AS
BEGIN
  DECLARE @Date1 DATETIME
  DECLARE @Date2 DATETIME
  DECLARE @Count DATETIME

  SET NOCOUNT ON;

  -- Delete any previous row of the given day  
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  SELECT @Count = COUNT(*) FROM AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  IF @Count > 0 
  BEGIN
    DELETE AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  END

    -- Generate 'day'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  INSERT INTO AML_DAILY (AMD_DAY, AMD_ACCOUNT_ID, AMD_TRACK_DATA, AMD_SPLIT_A, AMD_PRIZE)
  (
    SELECT   DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)      
           , AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')                            
           , SUM(CASE WHEN (AM_TYPE =  1) THEN AM_ADD_AMOUNT ELSE 0 END )
           - SUM(CASE WHEN (AM_TYPE IN (77)) THEN AM_SUB_AMOUNT + AM_ADD_AMOUNT ELSE 0 END )           
           , SUM(CASE WHEN (AM_TYPE =  2) THEN AM_SUB_AMOUNT ELSE 0 END )      
      FROM   ACCOUNT_MOVEMENTS  WITH (INDEX (IX_am_datetime))
	  WHERE  am_datetime >= @Date1 
              AND am_datetime < @Date2 
              AND am_type IN ( 1, 2, 77 ) 
			  AND am_operation_id NOT IN (	SELECT am_operation_id 
											FROM   account_movements 
											WHERE  am_type = 1000) 

    GROUP BY AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')
           , DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)
  )

  -- First day of the month at '00:00:00'
  SET @Date1 = DATEADD (DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SELECT @Count = COUNT(*) FROM AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  IF @Count > 0
  BEGIN
    DELETE AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  END

  -- First day of the month at 'Opening'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD(DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  INSERT INTO AML_MONTHLY (AMM_MONTH, AMM_ACCOUNT_ID, AMM_TRACK_DATA, AMM_SPLIT_A, AMM_PRIZE)
  (
    SELECT   @Date1
           , AMD_ACCOUNT_ID
           , AMD_TRACK_DATA
           , SUM(AMD_SPLIT_A)
           , SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (PK_aml_daily))
     WHERE   AMD_DAY >= @Date1
       AND   AMD_DAY <  DATEADD(MONTH, 1, @Date1)
    GROUP BY AMD_ACCOUNT_ID, AMD_TRACK_DATA
  )
END


GO


