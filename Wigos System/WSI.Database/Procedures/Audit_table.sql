--------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Audit_table.sql
-- 
--   DESCRIPTION: Audit procedure table
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 17-JUN-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 17-JUN-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

/****** Object:  Table [dbo].[audit_3gs]    Script Date: 06/17/2010 16:56:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[audit_3gs]') AND type in (N'U'))
DROP TABLE [dbo].[audit_3gs]
GO

/****** Object:  Table [dbo].[audit_3gs]    Script Date: 06/17/2010 16:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[audit_3gs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[audit_3gs](
	[a3gs_execution] [datetime] NOT NULL CONSTRAINT [DF_audit_procedure_a3gs_exec_time]  DEFAULT (getdate()),
	[a3gs_id] [bigint] IDENTITY(1,1) NOT NULL,
	[a3gs_procedure] [nvarchar](50) NOT NULL,
	[a3gs_account_id] [nvarchar](24) NOT NULL,
	[a3gs_vendor_id] [nvarchar](16) NOT NULL,
	[a3gs_machine_number] [int] NOT NULL,
	[a3gs_serial_number] [nvarchar](30) NULL,
	[a3gs_session_id] [bigint] NULL,
	[a3gs_balance] [money] NULL,
	[a3gs_status_code] [int] NOT NULL,
	[a3gs_input] [nvarchar](max) NULL,
	[a3gs_output] [nvarchar](max) NULL
) ON [PRIMARY]
END
GO

/****** Object:  Index [IX_audit_3gs]    Script Date: 06/17/2010 16:56:51 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[audit_3gs]') AND name = N'IX_audit_3gs')
CREATE NONCLUSTERED INDEX [IX_audit_3gs] ON [dbo].[audit_3gs] 
(
	[a3gs_execution] ASC,
	[a3gs_vendor_id] ASC,
	[a3gs_machine_number] ASC,
	[a3gs_status_code] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO
