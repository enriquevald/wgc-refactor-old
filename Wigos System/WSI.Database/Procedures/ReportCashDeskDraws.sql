--------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportCashDeskDraws.sql
-- 
--   DESCRIPTION: Procedure for Cash Desk Draws detailed report
-- 
--        AUTHOR: Marcos Mohedano
-- 
-- CREATION DATE: 05-SEP-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 05-SEP-2013 MMG    First release.
-- 09-OCT-2013 MMG    -Added use of index IX_cm_date_type on all the joins with cashier_movements table.
--                    -Changed filters order.
-- 25-FEB-2014 RMS    Fixed Bug WIG-674: Modified to use correct index on cashier_movements
-- 14-FEB-2018 AGS    Bug 31527:WIGOS-8123 [Ticket #12344] Fallo en Descuadre de Promociones Redimibles ref: Ticket 1060 V03,06.0035
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash desk draws filtering by its parameters
-- 
--  PARAMS:
--      - INPUT:
--   @pDrawFrom	       DATETIME     
--   @pDrawTo		       DATETIME      
--   @pDrawId		       BIGINT		  
--   @pCashierId       INT			 
--   @pUserId          INT			 
--   @pSqlAccount      NVARCHAR(MAX) 
--   @pCashierMovement INT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pCashDeskDraw    INT           = 1,
  @pGamingTableDraw INT           = 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_CashDesk_draw_type AS INT;
	DECLARE @_Terminal_GamingTable_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	
	SET @_Terminal_CashDesk_draw_type = 106
	SET @_Terminal_GamingTable_draw_type = 111
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_CashDesk_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		        ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
		        INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
    --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID 
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_CashDesk_draw_type
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END
--Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_GamingTable_draw_type
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	  
		
	END 	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN				
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement ) 
		       AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
	  --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_CashDesk_draw_type
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
  --Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_GamingTable_draw_type
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END	  
	END 
		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	
	--No filter by draw type
	IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	BEGIN
	  SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		   CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement )
	     AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
	--Filter by CASH DESK draw
	ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_CashDesk_draw_type
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
--Filter by GAMING TABLE draw
	ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS with ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
	      LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_GamingTable_draw_type
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP	
	END	

END


GO

GRANT EXECUTE ON [dbo].[ReportCashDeskDraws] TO [wggui] WITH GRANT OPTION
GO
