--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ProgresiveReports.sql
-- 
--   DESCRIPTION: Progresive reports reports procedures and functions
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-- 23-OCT-2014 JML    Fixed bug: WIG-1570 - Report collections by machine and date: Hand pays for cancel credits are not.
-- 21-JAN-2015 DCS    Fixed bug: WIG-1944 - Netwin report by denomination and machine: Don't show site jackpots
-- 02-FEB-2015 FJC    Fixed bug: WIG-1994
-- 03-FEB-2015 FJC    Fixed bug: WIG-1998 & 2004
-- 09-MAR-2015 FJC    Fixed bug: WIG-2144
-- 09-NOV-2015 ECP    Backlog Item 5685 GUI - Manual pays balance report Revision (PR_collection_by_machine_and_date)
-- 06-APR-2016 ETP    Fixed bug 11220 GUI - Manual pays balance report by hour (not by day)
-------------------------------------------------------------------------------- 

--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and denomination
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-- 02-FEB-2015 FJC    Fixed bug: WIG-1994 - Annulled provisions are showed in the report.
-- 03-FEB-2015 FJC    Fixed bug: WIG-1998 & 2004 - Changes in handpays placement 
--                      Column Manual:                   Others                     (MANUAL & MANUAL_OTHERS)
--                      Column Cancelled Credits:        Cr�ditos cancelados        
--                      Column No-Progressive Jackpots:  Jackpot + Jackpot Top Win  
--                      Column Progressive Jackpots:     Progressive Jackpots
-- 09-MAR-2015 FJC    Fixed bug: WIG-2144
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @nRows AS INT                              
 DECLARE @index AS INT                              
 DECLARE @tDays AS TABLE(NumDay INT)                       
 DECLARE @p2007Opening AS VARCHAR(MAX)

 SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
 SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
 
 SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @nRows                           
 BEGIN	                                          
   INSERT INTO @tDays VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT NumDay INTO #TempTable_Days FROM @tDays 
 
 SET @Sql = '
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PS_FINISHED
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID  
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_FROM < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '

EXECUTE (@Sql)

DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-FEB-2016 JML    First release.
-- 09-FEB-2016 JML    GetDualCurrencyAmount
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDualCurrencyAmount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetDualCurrencyAmount]
GO

CREATE FUNCTION [dbo].[GetDualCurrencyAmount]
(@pAmount  MONEY,
 @pAmt0    MONEY,
 @pPreferAmt INT)
RETURNS MONEY
AS
BEGIN

  DECLARE @Amount MONEY

  SET @Amount = @pAmount;
      
  IF (@pAmt0 IS NOT NULL AND @pPreferAmt = 1)
  BEGIN
     SET @Amount = @pAmt0;
  END
     
  RETURN @Amount
  
END -- GetDualCurrencyAmount
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetDualCurrencyAmount] TO [wggui] WITH GRANT OPTION
GO




--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-- 26-JAN-2015 FJC    Fixed Bug WIG-1966: change filter dates for provisions.
-- 29-JAN-2015 FJC    Fixed Bug WIG-1981 
-- 02-FEB-2015 FJC    Fixed bug: WIG-1994 - Annulled provisions are showed in the report.
-- 03-FEB-2015 FJC    Fixed bug: WIG-1998 & 2004 - Changes in handpays placement 
--                      Column Manual:                   Others                     (MANUAL & MANUAL_OTHERS)
--                      Column Cancelled Credits:        Cr�ditos cancelados        
--                      Column No-Progressive Jackpots:  Jackpot + Jackpot Top Win  
--                      Column Progressive Jackpots:     Progressive Jackpots
-- 09-FEB-2015 JML & FJC Fixed bug: WIG-2012: Site Jackpot is not displayed in some cases
-- 10-FEB-2015 FJC    WIG-2003. 
-- 09-MAR-2015 FJC    Fixed bug: WIG-2144
-- 30-MAR-2015 FJC    BacklogItem 542: BV-75: Reporte JCJ � Cambio en Net TITO
-- 28-MAY-2015 FAV    BacklogItem 1722: @pShowMetters and @pOnlyGroupByTerminal parameters added. 
--                    For the 'Netwin control report' form.
-- 14-JUL-2015 FAV    Fixed Bug WIG-2516 and 2536, check and rebuild 'PR_collection_by_machine_and_date' stored procedure
-- 21-SEP-2015 JML    Add cash-in coins
-- 26-JUN-2017 RGR    Fixed Bug 28489: WIGOS-3112 Ticket in status Pending to Print are considered in Ticket Out Report
-- 20-jul-2017 JML    Fixed Bug 28489: WIGOS-3112 Ticket in status Pending to Print are considered in Ticket Out Report
-------------------------------------------------------------------------------- 


IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 OR TI_STATUS = 6 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1              
               AND   TI_TYPE_ID IN (0,1,2,5)    --TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
               AND   TI_STATUS <> 4 -- DISCARDED 
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS JACKPOT_DE_SALA  '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
  SET @Sql =  @Sql +
  '                 , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 1
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO



--------------------------------------------------------------------------------
--   DESCRIPTION: Balance Handpays Report
-- 
--        AUTHOR: Ervin Olvera
-- 
-- CREATION DATE: 21-JUN-2016
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-JUN-2016 EOR    First release.
-- 04-JUL-2016 EOR    Bug 14939: Handpays Balance: Only Once Search and Window exit
-- 25-JUL-2018 AGS		Bug 33714:WIGOS-13830 [Ticket #15912] Fallo - Cuadratura de Pago Manuales Version V03.08.0027
-------------------------------------------------------------------------------- 



IF OBJECT_ID (N'dbo.GetHandpaysBalanceReport', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GetHandpaysBalanceReport                 
GO    
CREATE PROCEDURE [dbo].[GetHandpaysBalanceReport]
( @pFromDt            DATETIME   
  ,@pToDt             DATETIME    
  ,@pClosingTime      INTEGER
  ,@pTerminalWhere    NVARCHAR(MAX)
  ,@pWithOutActivity  BIT
  ,@pOnlyUnbalance    BIT
)
AS
BEGIN
  DECLARE @index        AS INT
  DECLARE @nRows        AS INT
  DECLARE @p2007Opening AS DATETIME
  
  DECLARE @c2007Opening AS CHAR(20)
  DECLARE @cFromDt      AS CHAR(20)
  DECLARE @cToDt        AS CHAR(20)
  DECLARE @query        AS VARCHAR(MAX)
  DECLARE @apos         AS CHAR(1)
  DECLARE @query_day    AS VARCHAR(MAX)
  DECLARE @query_where  AS VARCHAR(MAX)
  
  SET @index = 0   
  SET @query_day = ''
  SET @query_where = ''
  
  SET @p2007Opening  = CAST('2007-01-01 00:00:00' AS DATETIME)
  SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
  
  SET @c2007Opening = CONVERT(CHAR(20),@p2007Opening,120)
  SET @cFromDt = CONVERT(CHAR(20),@pFromDt,120)
  SET @cToDt = CONVERT(CHAR(20),@pToDt,120)
  
  SET @apos = CHAR(39)
  SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt) + 1 
  
  
  WHILE @index < @nRows                           
  BEGIN
    SET @query_day = @query_day + ' UNION SELECT ' + LTRIM(RTRIM(CAST(@index AS CHAR(5)))) + ' AS ORDER_DATE '                                         
    SET @index = @index + 1                       
  END

  SET @query_day = ' SELECT DATEADD(DAY, ORDER_DATE, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)) AS ORDER_DATE 
                     FROM (' + SUBSTRING(@query_day,8, LEN(@query_day)) + ') TABLE_DAYS '
           
           
  IF @pWithOutActivity = 1
    BEGIN
      SET @query_where = ' AND (BALANCE_HANDPAYS.SYSTEM_HANDPAYS_TOTAL <> 0 OR BALANCE_HANDPAYS.COUNTERS_HANDPAYS_TOTAL <> 0) '
    END          
         
  IF @pOnlyUnbalance = 1
    BEGIN
      SET @query_where = @query_where + ' AND BALANCE_HANDPAYS.DIFFERENCE <> 0 '
    END
    
    
  IF @query_where <> ''
    BEGIN
      SET @query_where = ' WHERE ' + SUBSTRING(@query_where, 6, LEN(@query_where))
    END 


  SET @query = ' SELECT * FROM (
                                SELECT  TE_PROVIDER_ID, 
                                        TE_TERMINAL_ID, 
                                        TE_NAME, 
                                        ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') AS DENOMINATION, 
                                        
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) AS SYSTEM_CREDIT_CANCEL, 
                                        ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) AS SYSTEM_PROGRESSIVE_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_HANDPAYS_TOTAL,

                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) AS COUNTERS_CREDIT_CANCEL,
                                        ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_JACKPOTS,
                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_HANDPAYS_TOTAL,
                                        
                                        (ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0)) -
                                        (ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0)) AS DIFFERENCE
                                        
                                FROM   TERMINALS 
                                LEFT JOIN (' + LTRIM(RTRIM(@query_day)) + '
                                          ) DIA ON ORDER_DATE <= GETDATE() 
                                LEFT JOIN (SELECT TC_TERMINAL_ID, 
                                                  DATEADD(HOUR, 10, TC_DATE) AS TC_DATE 
                                           FROM   TERMINALS_CONNECTED 
                                           WHERE  TC_DATE >= DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_DATE < DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cToDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_STATUS = 0 
                                                  AND TC_CONNECTED = 1
                                          ) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT HP_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS HP_DATETIME, 
                                                  SUM(CASE WHEN HP_TYPE IN (0, 1000) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
														   ELSE 0 END)  AS CREDIT_CANCEL, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32) AND HP_PROGRESSIVE_ID IS NOT NULL) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS PROGRESIVES, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL < 1 OR HP_LEVEL > 32 OR HP_LEVEL IS NULL)) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                           ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32 AND HP_PROGRESSIVE_ID IS NULL)) 
                                                           THEN CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0)
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS NO_PROGRESIVES 
                                           FROM HANDPAYS WITH (INDEX (IX_HP_STATUS_CHANGED)) 
                                           WHERE  HP_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND HP_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                                  AND HP_TYPE <> 20 
                                          GROUP BY HP_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT TSMH_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS TSMH_DATE, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 2 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_JACKPOT_CREDITS, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 3 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS 
                                           FROM   TERMINAL_SAS_METERS_HISTORY 
                                           WHERE  TSMH_TYPE = 1 
                                                  AND TSMH_METER_CODE IN (0, 1, 2, 3 ) 
                                                  AND TSMH_METER_INCREMENT > 0 
                                                  AND TSMH_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND TSMH_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                           GROUP BY TSMH_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE 
                                WHERE (HAND_PAYS.CREDIT_CANCEL IS NOT NULL 
                                        OR HAND_PAYS.PROGRESIVES IS NOT NULL 
                                        OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL 
                                        OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL 
                                        OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL 
                                        OR TC_DATE IS NOT NULL ) ' + LTRIM(RTRIM(@pTerminalWhere)) + '
                                GROUP BY  TE_PROVIDER_ID, 
                                          TE_TERMINAL_ID, 
                                          TE_NAME, 
                                          ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') 
                               )  BALANCE_HANDPAYS ' + @query_where + '                                                                
                 ORDER BY BALANCE_HANDPAYS.TE_PROVIDER_ID, 
                          BALANCE_HANDPAYS.TE_TERMINAL_ID, 
                          BALANCE_HANDPAYS.TE_NAME, 
                          BALANCE_HANDPAYS.DENOMINATION '

  EXECUTE (@query)                          
END 
GO
-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetHandpaysBalanceReport] TO [wggui] WITH GRANT OPTION
GO
