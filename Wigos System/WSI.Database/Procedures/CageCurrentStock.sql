﻿------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: CageCurrentStock.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
------------------------------------------------------------------------------------------------------------------------------------------------
-- 14-JUL-2017 JML    Bug 28673:WIGOS-3575 Cage - There are different fields and data between GUI and The excel report.
------------------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCurrentStock]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageCurrentStock]
GO

CREATE  PROCEDURE [dbo].[CageCurrentStock]
AS
BEGIN
  --Get current stock     
  SELECT   CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
         , CGS_QUANTITY 
         , CGS_CAGE_CURRENCY_TYPE
         , SET_ID 
         , CGS_CHIP_ID
         , COLOR
         , NAME
         , DRAWING
         , CE_CURRENCY_ORDER 
    FROM ( SELECT   CGS_ISO_CODE                                          
                  , CGS_DENOMINATION                                      
                  , CGS_QUANTITY                     
                  , CASE WHEN CGS_DENOMINATION > 0 THEN ISNULL(CGS_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS CGS_CAGE_CURRENCY_TYPE   
                  , -1 AS CGS_CHIP_ID  
                  , -1 AS SET_ID                
                  , '' AS COLOR
                  , '' AS NAME
                  , '' AS DRAWING
             FROM   CAGE_STOCK
        UNION ALL
           SELECT   C.CH_ISO_CODE AS CGS_ISO_CODE
                  , C.CH_DENOMINATION AS CGS_DENOMINATION
                  , CST.CHSK_QUANTITY AS CGS_QUANTITY
                  , C.CH_CHIP_TYPE AS CGS_CAGE_CURRENCY_TYPE
                  , C.CH_CHIP_ID AS CGS_CHIP_ID
                  , CSC_SET_ID AS SET_ID
                  , C.CH_COLOR AS COLOR
                  , C.CH_NAME AS NAME
                  , C.CH_DRAWING AS DRAWING
             FROM   CHIPS_STOCK AS CST
       INNER JOIN   CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
       INNER JOIN   CHIPS_SETS_CHIPS ON C.CH_CHIP_ID = CSC_CHIP_ID
                  ) AS _TBL 
    LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
   WHERE   CGS_QUANTITY > 0  OR CGS_DENOMINATION = -100  
ORDER BY   CE_CURRENCY_ORDER
         , CGS_ISO_CODE
         , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                   WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                   ELSE 1 END
         , SET_ID ASC
         , CGS_DENOMINATION DESC
         , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                ELSE CGS_DENOMINATION * (-100000) END
END --CageCurrentStock



GRANT EXECUTE ON [dbo].[CageCurrentStock] TO [wggui] WITH GRANT OPTION
GO
