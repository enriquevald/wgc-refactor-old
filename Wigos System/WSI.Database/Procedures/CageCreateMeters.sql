--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CageCreateMeters.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 22-ABR-2016 JML    Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
-- ----------- ------ ----------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCreateMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCreateMeters]
GO

CREATE PROCEDURE [dbo].[CageCreateMeters]
        @pSourceTargetId BIGINT
      , @pConceptId BIGINT
      , @pCageSessionId BIGINT = NULL
      , @CreateMetersOption INT
      , @CurrencyForced VARCHAR(3) =  NULL
  AS
BEGIN   

  -- @@CreateMetersOption  =    0 - Create in cage_meters and cage_session_meters
  --                       =    1 - Create in cage_meters only
  --                       =    2 - Create in cage_session_meters only 
  --                       =    3 - Create in cage_meters and all opened cage sessions 


  DECLARE @Currencies VARCHAR(100)
  DECLARE @OnlyNationalCurrency BIT
  DECLARE @CageSessionId BIGINT
	DECLARE @_dual_currency_enabled BIT
	DECLARE @_cage_currency_type_others INT
                
  SET @OnlyNationalCurrency = 0
  SET @_cage_currency_type_others = 99  -- Others
                
  SELECT @OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY 
    FROM CAGE_SOURCE_TARGET_CONCEPTS 
   WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetId
     AND  CSTC_CONCEPT_ID = @pConceptId
    
  -- Terminals: ONLY NATIONAL CURRENCY FORCED 
	SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
  SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)

  IF @pSourceTargetId = 12
	begin
		if @_dual_currency_enabled = 1
			SET @OnlyNationalCurrency = 0
		else
			SET @OnlyNationalCurrency = 1
	end

  -- Get national currency
  IF @OnlyNationalCurrency = 1 AND @CurrencyForced IS NULL
  BEGIN 
    SELECT @Currencies = GP_KEY_VALUE 
      FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  END
  
  -- Get accepted currencies
  ELSE
  BEGIN
    IF @CurrencyForced IS NOT NULL
      SET @Currencies = @CurrencyForced
    ELSE
      SELECT @Currencies = GP_KEY_VALUE 
        FROM GENERAL_PARAMS 
       WHERE GP_GROUP_KEY = 'RegionalOptions' 
         AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
  END
    
  -- Split currencies ISO codes
  SELECT * INTO #TMP_CURRENCIES_ISO_CODES FROM 
  (  SELECT   DISTINCT CGC_ISO_CODE AS CURRENCY_ISO_CODE
            , CGC_CAGE_CURRENCY_TYPE AS CAGE_CURRENCY_TYPE 
       FROM   CAGE_CURRENCIES 
      WHERE   CGC_ISO_CODE IN (SELECT SST_VALUE FROM [SplitStringIntoTable] (@Currencies, ';', 1))
      UNION   ALL
     SELECT   DISTINCT CGC_ISO_CODE AS CURRENCY_ISO_CODE
            , @_cage_currency_type_others AS CAGE_CURRENCY_TYPE 
       FROM   CAGE_CURRENCIES 
      WHERE   CGC_ISO_CODE IN (SELECT SST_VALUE FROM [SplitStringIntoTable] (@Currencies, ';', 1))
      UNION   ALL
     SELECT   DISTINCT CHS_ISO_CODE AS CURRENCY_ISO_CODE
            , CHS_CHIP_TYPE AS CAGE_CURRENCY_TYPE  
       FROM   CHIPS_SETS  ) AS A

  -- Create a new meters for each currency
  DECLARE @CurrentCurrency VARCHAR(3)
  DECLARE @CageCurrencyType INT

              
  DECLARE Curs_CageCreateMeters CURSOR FOR 
   SELECT CURRENCY_ISO_CODE, CAGE_CURRENCY_TYPE
    FROM #TMP_CURRENCIES_ISO_CODES 
    
  SET NOCOUNT ON;

  OPEN Curs_CageCreateMeters

  FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency, @CageCurrencyType
  
  WHILE @@FETCH_STATUS = 0
  BEGIN
                
    -- CAGE METERS --
    IF @CreateMetersOption = 0 OR @CreateMetersOption = 1 OR @CreateMetersOption = 3
    BEGIN
      IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
                       FROM CAGE_METERS 
                      WHERE CM_SOURCE_TARGET_ID = @pSourceTargetId
                        AND CM_CONCEPT_ID = @pConceptId
                        AND CM_ISO_CODE = @CurrentCurrency
                        AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType)
        INSERT INTO CAGE_METERS
                  ( CM_SOURCE_TARGET_ID
                  , CM_CONCEPT_ID
                  , CM_ISO_CODE
                  , CM_CAGE_CURRENCY_TYPE
                  , CM_VALUE
                  , CM_VALUE_IN
                  , CM_VALUE_OUT )
             VALUES
                  ( @pSourceTargetId
                  , @pConceptId
                  , @CurrentCurrency
                  , @CageCurrencyType
                  , 0
                  , 0
                  , 0 )
                  
    END

    -- CAGE SESSION METERS --
    IF (@CreateMetersOption = 0 OR @CreateMetersOption = 2) AND @pCageSessionId IS NOT NULL
    BEGIN
      IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                       FROM CAGE_SESSION_METERS 
                      WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                        AND CSM_CONCEPT_ID = @pConceptId
                        AND CSM_ISO_CODE = @CurrentCurrency
                        AND CSM_CAGE_CURRENCY_TYPE = @CageCurrencyType
                        AND CSM_CAGE_SESSION_ID = @pCageSessionId)
        INSERT INTO CAGE_SESSION_METERS
                  ( CSM_CAGE_SESSION_ID
                  , CSM_SOURCE_TARGET_ID
                  , CSM_CONCEPT_ID
                  , CSM_ISO_CODE
                  , CSM_CAGE_CURRENCY_TYPE
                  , CSM_VALUE
                  , CSM_VALUE_IN
                  , CSM_VALUE_OUT )
            VALUES
                  ( @pCageSessionId
                  , @pSourceTargetId
                  , @pConceptId
                  , @CurrentCurrency
                  , @CageCurrencyType
                  , 0
                  , 0
                  , 0 )

    END
                               
    IF (@CreateMetersOption = 3)
    BEGIN
                               
      DECLARE Cur2 CURSOR FOR 
       SELECT CGS_CAGE_SESSION_ID  
         FROM CAGE_SESSIONS 
        WHERE (CGS_CLOSE_DATETIME IS NULL)
                                                           
      OPEN Cur2
                                                     
      FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     
      WHILE @@FETCH_STATUS = 0
      BEGIN
      
        IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                         FROM CAGE_SESSION_METERS 
                        WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                          AND CSM_CONCEPT_ID = @pConceptId
                          AND CSM_ISO_CODE = @CurrentCurrency
                          AND CSM_CAGE_CURRENCY_TYPE = @CageCurrencyType
                          AND CSM_CAGE_SESSION_ID = @CageSessionId)
          INSERT INTO CAGE_SESSION_METERS
                    ( CSM_CAGE_SESSION_ID
                    , CSM_SOURCE_TARGET_ID
                    , CSM_CONCEPT_ID
                    , CSM_ISO_CODE
                    , CSM_CAGE_CURRENCY_TYPE
                    , CSM_VALUE
                    , CSM_VALUE_IN
                    , CSM_VALUE_OUT )
              VALUES
                    ( @CageSessionId
                    , @pSourceTargetId
                    , @pConceptId
                    , @CurrentCurrency
                    , @CageCurrencyType
                    , 0
                    , 0
                    , 0 )
                                                                               
        FETCH NEXT FROM Cur2 INTO @CageSessionId
        
      END
                                                           
      CLOSE Cur2
      DEALLOCATE Cur2                       
    
    END
                               
    FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency, @CageCurrencyType
                
  END

  CLOSE Curs_CageCreateMeters
  DEALLOCATE Curs_CageCreateMeters
              
END -- CageCreateMeters


GO
GRANT EXECUTE ON [dbo].[CageCreateMeters] TO [wggui] WITH GRANT OPTION 
GO


