--------------------------------------------------------------------------------
-- Copyright � 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GetPaymentOrderByOperation.sql
-- 
--   DESCRIPTION: Get Payment Order by operation Id
-- 
--        AUTHOR: Ferran Ortner
-- 
-- CREATION DATE: 16-MAR-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAR-2015 FOS    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Get Payment Order register by operationId
-- 
--  PARAMS:
--      - INPUT:
--        OperationId	   bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      OperationId, 
--      AccountId, 
--      ReturnBalance,
--			Prize,
--			Tax1,
--			Tax2,
--			TotalPayment,
--			CashPayment,
--			CheckPayment
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPaymentOrderByOperation]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetPaymentOrderByOperation]
GO

CREATE PROCEDURE [dbo].[GetPaymentOrderByOperation] 
 (  
	@pOperationId bigint
  )
AS
BEGIN
	 SELECT   APO_OPERATION_ID    AS OPERATION_ID        
					, APO_ACCOUNT_ID      AS ACCOUNT_ID
					, APO_RETURN_BALANCE  AS RETURN_BALANCE       
					, APO_PRIZE           AS PRIZE
					, APO_TAX1            AS TAX_1
					, APO_TAX2            AS TAX_2
					, APO_TOTAL_PAYMENT   AS TOTAL_PAYMENT      
					, APO_CASH_PAYMENT    AS CASH_PAYMENT      
					, APO_CHECK_PAYMENT   AS CHECK_PAYMENT      
		 FROM   ACCOUNT_PAYMENT_ORDERS    WITH ( INDEX ( PK_account_payment_order))
	  WHERE   APO_OPERATION_ID = @pOperationId   

END
GO

