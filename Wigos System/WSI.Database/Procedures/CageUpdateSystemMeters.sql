IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateSystemMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageUpdateSystemMeters]
GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
    @pCashierSessionId BIGINT,
    @pIsClosingCashierSession BIT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  DECLARE @Currencies AS VARCHAR(200)
  DECLARE @CurrentCurrency AS VARCHAR(3)
  DECLARE @CageCurrencyType INT
      
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  DECLARE @TaxIEJC MONEY

  DECLARE @ClosingShort MONEY
  DECLARE @ClosingOver MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
              FROM CAGE_MOVEMENTS
              WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
              AND CGM_TYPE IN (0, 1, 4, 5)
              ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
    SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                FROM CAGE_MOVEMENTS
                WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                AND CGM_TYPE IN (100, 101, 103, 104)
                ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  WHERE CGS_CLOSE_DATETIME IS NULL
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
        -- FEDERAL TAX --
  
        SELECT @Tax1 = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CM_ADD_AMOUNT) ELSE -SUM(CM_ADD_AMOUNT) END
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (6) 
          AND CM_SESSION_ID = @pCashierSessionId

        SET @Tax1 = ISNULL(@Tax1, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax1,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 3,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = 0,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0          
     
        -- STATE TAX --
   
        SELECT @Tax2 = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CM_ADD_AMOUNT) ELSE -SUM(CM_ADD_AMOUNT) END
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (14) 
          AND CM_SESSION_ID = @pCashierSessionId 
        
        SET @Tax2 = ISNULL(@Tax2, 0)
         
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax2,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 2,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = 0,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        -- IEJC TAX --
  
        SELECT @TaxIEJC = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CM_ADD_AMOUNT) ELSE -SUM(CM_ADD_AMOUNT) END 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (142,146) 
          AND CM_SESSION_ID = @pCashierSessionId
 
        SET @TaxIEJC = ISNULL(@TaxIEJC, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @TaxIEJC,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 12,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = @CageCurrencyType,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    

        SELECT @CardRefundable = GP_KEY_VALUE 
        FROM GENERAL_PARAMS  
        WHERE GP_GROUP_KEY = 'Cashier'                             
          AND GP_SUBJECT_KEY = 'CardRefundable'                    

        IF ISNULL(@CardRefundable, 0) = 1                                                                        
        BEGIN                                                                                                   
          SELECT @CardDeposit = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT ELSE CM_ADD_AMOUNT END) 
          ELSE -SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT ELSE CM_ADD_AMOUNT END) END                                                   
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
                WHERE CM_TYPE IN (9, 10, 532, 533, 534, 535, 536, 544, 545, 546, 547, 548)                                             
                  AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                             FROM CAGE_MOVEMENTS                                                                     
                             WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId)
                                                    
          SET @CardDeposit = ISNULL(@CardDeposit, 0)
              
          EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @CardDeposit,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 5,
            @pIsoCode = @NationalCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0
                     
        END                                                                                                     

      SELECT @Currencies = GP_KEY_VALUE 
      FROM GENERAL_PARAMS 
      WHERE GP_GROUP_KEY = 'RegionalOptions' 
        AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
      -- Split currencies ISO codes      
       SELECT * INTO #TMP_CURRENCIES_ISO_CODES FROM 
       (SELECT SST_VALUE AS CURRENCY_ISO_CODE, 0 AS CAGE_CURRENCY_TYPE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
        UNION ALL
        SELECT SST_VALUE AS CURRENCY_ISO_CODE, 99 AS CAGE_CURRENCY_TYPE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
        UNION ALL
        SELECT [chs_iso_code] AS CURRENCY_ISO_CODE, [chs_chip_type] AS CAGE_CURRENCY_TYPE  FROM [chips_sets] 
       ) AS A
           
      DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
      SELECT CURRENCY_ISO_CODE, CAGE_CURRENCY_TYPE
        FROM #TMP_CURRENCIES_ISO_CODES 
      
      SET NOCOUNT ON;

      OPEN Curs_CageUpdateSystemMeters

      FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
    
      WHILE @@FETCH_STATUS = 0
      BEGIN
  
        -- CLOSING SHORT --
        
        SELECT @ClosingShort = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CM_ADD_AMOUNT) ELSE -SUM(CM_ADD_AMOUNT) END 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (160) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
            
        SET @ClosingShort = ISNULL(@ClosingShort, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingShort,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 7,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType, 
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
        
        -- CLOSING OVER --
        
        SELECT @ClosingOver = CASE WHEN @pIsClosingCashierSession=1 THEN SUM(CM_ADD_AMOUNT) ELSE -SUM(CM_ADD_AMOUNT) END 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (161) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
        
        SET @ClosingOver = ISNULL(@ClosingOver, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingOver,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 8,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
        FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
                
      END

      CLOSE Curs_CageUpdateSystemMeters
      DEALLOCATE Curs_CageUpdateSystemMeters

      END                                      

END -- CageUpdateSystemMeters
GO

GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO
