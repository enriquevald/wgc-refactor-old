--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Account.sql
-- 
--   DESCRIPTION: Procedures for Accounts 
-- 
--        AUTHOR: Alberto Cuesta
-- 
-- CREATION DATE: 06-SEP-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-SEP-2010 ACC    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Calculate Redeemable & NonRedeemable balance 
-- 
--  PARAMS:
--      - INPUT:
--          @Balance              money
--          @InitialCashIn        money
--          @InitialNoRedeemable  money
--          @NonRedeemableWonLock  money
--
--      - OUTPUT:
--
-- RETURNS:
--          RedeemableBalance     money          
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_BalancePart]') AND type in (N'FN'))
DROP FUNCTION [dbo].[Account_BalancePart]
GO
CREATE FUNCTION [dbo].[Account_BalancePart]
( @PartNonRedeemable      bit,
  @Balance                money, 
  @InitialCashIn          money, 
  @InitialNonRedeemable   money, 
  @NonRedeemableWonLock   money )
RETURNS Money
AS
BEGIN

  DECLARE @redeemable_balance         money
  DECLARE @non_redeemable_balance     money
  
  DECLARE @aux_balance                money
  DECLARE @balance_non_redimable      money
  DECLARE @cash_in                    money
  DECLARE @won                        money
  DECLARE @won_redimable              money
  DECLARE @won_non_redimable          money

  SET @redeemable_balance = 0
  SET @non_redeemable_balance = 0

  SET @aux_balance            = @Balance
  SET @balance_non_redimable  = dbo.Minimum_Money (@aux_balance, @InitialNonRedeemable)
  SET @aux_balance            = @aux_balance - @balance_non_redimable
  SET @cash_in                = dbo.Minimum_Money (@aux_balance, @InitialCashIn)
  SET @won                    = @aux_balance - @cash_in

  IF @NonRedeemableWonLock = 0
  BEGIN
    SET @won_redimable      = @won
    SET @won_non_redimable  = 0
  END
  ELSE
  BEGIN
    SET @won_redimable      = 0
    SET @won_non_redimable  = @won
  END

  SET @redeemable_balance     = ROUND(@cash_in + @won_redimable, 2)
  SET @non_redeemable_balance = ROUND(@balance_non_redimable + @won_non_redimable, 2)

  IF @PartNonRedeemable = 1
  BEGIN
    RETURN @non_redeemable_balance
  END

  RETURN @redeemable_balance

END -- Account_BalancePart
GO



