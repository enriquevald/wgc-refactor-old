--------------------------------------------------------------------------------
-- Copyright � 2015 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GetVouchersByOperation.sql
-- 
--   DESCRIPTION: Get Voucher by operationId
-- 
--        AUTHOR: Ferran Ortner
-- 
-- CREATION DATE: 16-MAR-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAR-2015 FOS    First release.
-- 24-MAR-2015 FOS		Protect Stored Procedure
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Get all vouchers payment order by OperationId
-- 
--  PARAMS:
--      - INPUT:
--        OperationId	   bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      TicketTipe 
--      Voucher 
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVouchersByOperation]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetVouchersByOperation]
GO

CREATE PROCEDURE [dbo].[GetVouchersByOperation] 
(  
	@pOperationId bigint
)
AS
	BEGIN

	 SELECT	  CV_TYPE			AS TICKET_TYPE
			    , CV_HTML			AS VOUCHER
	   FROM	  CASHIER_VOUCHERS WITH(INDEX(IX_operation_id))
	  WHERE   CV_OPERATION_ID = @pOperationId  
			AND		CV_OPERATION_ID > 0 

   END
GO
