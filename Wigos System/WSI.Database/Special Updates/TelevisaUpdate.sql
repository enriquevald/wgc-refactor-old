USE [wgdb_000]
GO

-- Update to activate some codes only
--	AGRICULTURA	 
--	AMA DE CASA	 
--	DESEMPLEADO	 
--	EMPLEADO DEL SECTOR PRIVADO 	 
--	EMPLEADO PUBLICO  	 
--	ESTUDIANTE Ó MENOR DE EDAD SIN OCUPACIÓN	Solo poner Estudiante en WIGOS
--	GANADERÍA	 
--	JUBILADO	 
--	NO APLICA	Poner la leyenda de OTROS en WIGOS
--	PENSIONADO	 
--	SERVICIOS PROFESIONALES Y TÉCNICOS	Con actividad independiente

UPDATE occupations
SET oc_enabled = 0
WHERE oc_code NOT IN ('0100008', '9900909','9900906','9501009','9411998','9900905','0200006','9900907','9999999','9900908','8400004')

UPDATE occupations
set oc_description = 'ESTUDIANTE'
where oc_code = 9900905

UPDATE occupations
set oc_description = 'SERVICIOS PROFESIONALES Y TÉCNICOS (CON ACTIVIDAD INDEPENDIENTE)'
where oc_code = 8400004

GO
