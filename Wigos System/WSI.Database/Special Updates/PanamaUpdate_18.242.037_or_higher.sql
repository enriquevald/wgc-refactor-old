/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int;

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 242;

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id >= @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5)) + ' or higher.'
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END 

DECLARE @ISOCode AS VARCHAR(3)
SET @ISOCode = 'USD'

DECLARE @OldISOCode AS VARCHAR(3)
SET @OldISOCode = 'MXN'

DECLARE @Executed AS BIT

SELECT @Executed = GP_KEY_VALUE 
  FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'Cage' 
   AND GP_SUBJECT_KEY = 'Meters.SpecialUpdateExecuted'

--/*************** UPDATE/DELETE   **********************************/

IF @Executed = 0
BEGIN

-- Update "Propinas"
IF NOT EXISTS (SELECT TOP 1 CM_CONCEPT_ID 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (10, 11) 
                  AND CM_CONCEPT_ID = 6
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_CONCEPT_ID = 6
     AND CM_SOURCE_TARGET_ID IN (10, 11)
     AND CM_ISO_CODE = @OldISOCode
END
   
-- Update "Global mesas"
IF NOT EXISTS (SELECT TOP 1 CM_CONCEPT_ID 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (11) 
                  AND CM_CONCEPT_ID = 0
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_CONCEPT_ID = 0
     AND CM_SOURCE_TARGET_ID IN (11)
     AND CM_ISO_CODE = @OldISOCode  
END

-- Global
-- Terminals
IF NOT EXISTS (SELECT TOP 1 CM_CONCEPT_ID 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (12) 
                  AND CM_CONCEPT_ID = 0
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS 
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_SOURCE_TARGET_ID = 12
     AND CM_CONCEPT_ID = 0 
     AND CM_ISO_CODE = @OldISOCode
END

UPDATE CAGE_METERS 
    SET CM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0) + ISNULL(MC_COLLECTED_TICKET_AMOUNT, 0)) AS CMD_DEPOSITS       
                                FROM MONEY_COLLECTIONS                                                                                    
                                     INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID                              
                               WHERE CGM_TYPE = 104), 0)
  WHERE CM_SOURCE_TARGET_ID = 12
    AND CM_CONCEPT_ID = 0 
    AND CM_ISO_CODE = @ISOCode
        
        
UPDATE CAGE_METERS 
   SET CM_VALUE = CM_VALUE_IN
WHERE CM_SOURCE_TARGET_ID = 12
   AND CM_CONCEPT_ID = 0 
   AND CM_ISO_CODE = @ISOCode
    
-- Global
-- Cashiers
IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (10) 
                  AND CM_CONCEPT_ID = 0 
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
      UPDATE CAGE_METERS 
         SET CM_ISO_CODE = @ISOCode
      WHERE CM_SOURCE_TARGET_ID = 10
         AND CM_CONCEPT_ID = 0 
         AND CM_ISO_CODE = @OldISOCode
END

UPDATE CAGE_METERS 
    SET CM_VALUE_IN = ISNULL(CM_VALUE_IN, 0) +  ISNULL((SELECT SUM (CMD_DENOMINATION)
                                                          FROM CAGE_MOVEMENTS AS CM
                                                               INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                                         WHERE CGM_TYPE IN (100) 
                                                           AND CMD_QUANTITY = -200), 0)
  WHERE CM_SOURCE_TARGET_ID = 10
    AND CM_CONCEPT_ID = 0 
    AND CM_ISO_CODE = @ISOCode
    
UPDATE CAGE_METERS 
    SET CM_VALUE_OUT = ISNULL(CM_VALUE_OUT, 0) +  ISNULL((SELECT SUM (CMD_DENOMINATION)
                                                            FROM CAGE_MOVEMENTS AS CM
                                                                 INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                                           WHERE CGM_TYPE IN (0) 
                                                             AND CMD_QUANTITY = -200), 0)
  WHERE CM_SOURCE_TARGET_ID = 10
    AND CM_CONCEPT_ID = 0 
    AND CM_ISO_CODE = @ISOCode
    
UPDATE CAGE_METERS 
   SET CM_VALUE = CM_VALUE_IN - CM_VALUE_OUT
WHERE CM_SOURCE_TARGET_ID = 10
   AND CM_CONCEPT_ID = 0 
   AND CM_ISO_CODE = @ISOCode

    
-- Tickets
-- Terminals
IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (12) 
                  AND CM_CONCEPT_ID = 9 
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS 
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_SOURCE_TARGET_ID = 12
     AND CM_CONCEPT_ID = 9 
     AND CM_ISO_CODE = @OldISOCode
END

UPDATE CAGE_METERS 
   SET CM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_TICKET_AMOUNT, 0)) AS CMD_DEPOSITS       
                               FROM MONEY_COLLECTIONS
                                    INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID                             
                              WHERE CGM_TYPE = 104), 0)
WHERE CM_SOURCE_TARGET_ID = 12
   AND CM_CONCEPT_ID = 9
   AND CM_ISO_CODE = @ISOCode
        
        
 UPDATE CAGE_METERS 
    SET CM_VALUE = CM_VALUE_IN
  WHERE CM_SOURCE_TARGET_ID = 12
    AND CM_CONCEPT_ID = 9 
    AND CM_ISO_CODE = @ISOCode
    
    
-- Tickets
-- Cashiers
IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (10) 
                  AND CM_CONCEPT_ID = 9 
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS 
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_SOURCE_TARGET_ID = 10
     AND CM_CONCEPT_ID = 9 
     AND CM_ISO_CODE = @OldISOCode
END

UPDATE CAGE_METERS 
    SET CM_VALUE_IN = ISNULL((SELECT SUM (CMD_DENOMINATION)
                                FROM CAGE_MOVEMENTS AS CM
                                     INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                               WHERE CGM_TYPE IN (100) 
                                 AND CMD_QUANTITY = -200), 0)
  WHERE CM_SOURCE_TARGET_ID = 10
    AND CM_CONCEPT_ID = 9 
    AND CM_ISO_CODE = @ISOCode
    
UPDATE CAGE_METERS 
    SET CM_VALUE_OUT = ISNULL((SELECT SUM (CMD_DENOMINATION)
                                 FROM CAGE_MOVEMENTS AS CM
                                      INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                WHERE CGM_TYPE IN (0) 
                                  AND CMD_QUANTITY = -200), 0)
WHERE CM_SOURCE_TARGET_ID = 10
    AND CM_CONCEPT_ID = 9 
    AND CM_ISO_CODE = @ISOCode
    
UPDATE CAGE_METERS 
   SET CM_VALUE = CM_VALUE_IN - CM_VALUE_OUT
WHERE CM_SOURCE_TARGET_ID = 10
   AND CM_CONCEPT_ID = 9 
   AND CM_ISO_CODE = @ISOCode
    

-- Efectivo
IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_METERS 
                WHERE CM_SOURCE_TARGET_ID IN (12) 
                  AND CM_CONCEPT_ID = 10 
                  AND CM_ISO_CODE = @ISOCode) 
BEGIN
  UPDATE CAGE_METERS 
     SET CM_ISO_CODE = @ISOCode
  WHERE CM_SOURCE_TARGET_ID = 12
     AND CM_CONCEPT_ID = 10 
     AND CM_ISO_CODE = @OldISOCode
END
     
UPDATE CAGE_METERS 
   SET CM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS CMD_DEPOSITS
                               FROM MONEY_COLLECTIONS
                                    INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID
                              WHERE CGM_TYPE = 104), 0)
WHERE CM_SOURCE_TARGET_ID = 12
   AND CM_CONCEPT_ID = 10
   AND CM_ISO_CODE = @ISOCode
        
        
UPDATE CAGE_METERS 
   SET CM_VALUE = CM_VALUE_IN
WHERE CM_SOURCE_TARGET_ID = 12
   AND cm_concept_id = 10
   AND CM_ISO_CODE = @ISOCode 
    
      
   DECLARE @CurrentSessionId AS BIGINT
      
   DECLARE Curs CURSOR FOR 
    SELECT cgs_cage_session_id
        FROM cage_sessions 
      
    SET NOCOUNT ON;

    OPEN Curs

    FETCH NEXT FROM Curs INTO @CurrentSessionId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
      -- Global
      -- Terminals
      IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_SESSION_METERS 
                WHERE CSM_SOURCE_TARGET_ID IN (12) 
                  AND CSM_CONCEPT_ID = 0
                  AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
                  AND CSM_ISO_CODE = @ISOCode) 
      BEGIN
        UPDATE CAGE_SESSION_METERS 
           SET CSM_ISO_CODE = @ISOCode
         WHERE CSM_SOURCE_TARGET_ID = 12
           AND CSM_CONCEPT_ID = 0 
           AND CSM_ISO_CODE = @OldISOCode
           AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
      END
      
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0) + ISNULL(MC_COLLECTED_TICKET_AMOUNT, 0)) AS CMD_DEPOSITS       
                                      FROM MONEY_COLLECTIONS                                                                                    
                                           INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID                              
                                     WHERE CGM_TYPE = 104
                                       AND CGM_CAGE_SESSION_ID = @CurrentSessionId), 0)
       WHERE CSM_SOURCE_TARGET_ID = 12
         AND CSM_CONCEPT_ID = 0
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
         AND CSM_ISO_CODE = @ISOCode
               
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE = CSM_VALUE_IN
       WHERE CSM_SOURCE_TARGET_ID = 12
         AND CSM_CONCEPT_ID = 0
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
         AND CSM_ISO_CODE = @ISOCode
         
      -- Global
      -- Cashiers
      IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_SESSION_METERS 
                WHERE CSM_SOURCE_TARGET_ID IN (10) 
                  AND CSM_CONCEPT_ID = 0
                  AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
                  AND CSM_ISO_CODE = @ISOCode) 
      BEGIN
        UPDATE CAGE_SESSION_METERS 
           SET CSM_ISO_CODE = @ISOCode
         WHERE CSM_SOURCE_TARGET_ID = 10
           AND CSM_CONCEPT_ID = 0 
           AND CSM_ISO_CODE = @OldISOCode
           AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
      END

      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_IN = ISNULL(CSM_VALUE_IN, 0) +  ISNULL((SELECT SUM (CMD_DENOMINATION)
                                                                 FROM CAGE_MOVEMENTS AS CM
                                                                      INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                                                WHERE CGM_TYPE IN (100)
                                                                  AND CGM_CAGE_SESSION_ID = @CurrentSessionId  
                                                                  AND CMD_QUANTITY = -200), 0)
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 0
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode
    
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_OUT = ISNULL(CSM_VALUE_OUT, 0) +  ISNULL((SELECT SUM (CMD_DENOMINATION)
                                                                   FROM CAGE_MOVEMENTS AS CM
                                                                        INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                                                  WHERE CGM_TYPE IN (0)
                                                                    AND CGM_CAGE_SESSION_ID = @CurrentSessionId  
                                                                    AND CMD_QUANTITY = -200), 0)
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 0 
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode
    
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE = CSM_VALUE_IN - CSM_VALUE_OUT
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 0 
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode

    
      -- Tickets
      -- Terminals
      IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_SESSION_METERS 
                WHERE CSM_SOURCE_TARGET_ID IN (12) 
                  AND CSM_CONCEPT_ID = 9
                  AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
                  AND CSM_ISO_CODE = @ISOCode) 
      BEGIN
        UPDATE CAGE_SESSION_METERS 
           SET CSM_ISO_CODE = @ISOCode
         WHERE CSM_SOURCE_TARGET_ID = 12
           AND CSM_CONCEPT_ID = 9 
           AND CSM_ISO_CODE = @OldISOCode
           AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
      END
      
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_TICKET_AMOUNT, 0)) AS CMD_DEPOSITS       
                                     FROM MONEY_COLLECTIONS 
                                          INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID                              
                                    WHERE CGM_TYPE = 104
                                      AND CGM_CAGE_SESSION_ID = @CurrentSessionId), 0)
       WHERE CSM_SOURCE_TARGET_ID = 12
         AND CSM_CONCEPT_ID = 9
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
         AND CSM_ISO_CODE = @ISOCode
           
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE = CSM_VALUE_IN
       WHERE CSM_SOURCE_TARGET_ID = 12
         AND CSM_CONCEPT_ID = 9
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
         AND CSM_ISO_CODE = @ISOCode
         
         
      -- Tickets
      -- Cashiers
      IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_SESSION_METERS 
                WHERE CSM_SOURCE_TARGET_ID IN (10) 
                  AND CSM_CONCEPT_ID = 9
                  AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
                  AND CSM_ISO_CODE = @ISOCode) 
      BEGIN
        UPDATE CAGE_SESSION_METERS 
           SET CSM_ISO_CODE = @ISOCode
         WHERE CSM_SOURCE_TARGET_ID = 10
           AND CSM_CONCEPT_ID = 9 
           AND CSM_ISO_CODE = @OldISOCode
           AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
      END
      
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_IN = ISNULL((SELECT SUM (CMD_DENOMINATION)
                                      FROM CAGE_MOVEMENTS AS CM
                                           INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                     WHERE CGM_TYPE IN (100)
                                       AND CGM_CAGE_SESSION_ID = @CurrentSessionId  
                                       AND CMD_QUANTITY = -200), 0)
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 9
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode
    
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_OUT = ISNULL((SELECT SUM (CMD_DENOMINATION)
                                       FROM CAGE_MOVEMENTS AS CM
                                           INNER JOIN CAGE_MOVEMENT_DETAILS AS CMD ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID
                                      WHERE CGM_TYPE IN (0)
                                        AND CGM_CAGE_SESSION_ID = @CurrentSessionId 
                                        AND CMD_QUANTITY = -200), 0)
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 9 
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode
    
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE = CSM_VALUE_IN - CSM_VALUE_OUT
       WHERE CSM_SOURCE_TARGET_ID = 10
         AND CSM_CONCEPT_ID = 9
         AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
         AND CSM_ISO_CODE = @ISOCode
       
       
       -- Efectivo
       IF NOT EXISTS (SELECT TOP 1 * 
                 FROM CAGE_SESSION_METERS 
                WHERE CSM_SOURCE_TARGET_ID IN (12) 
                  AND CSM_CONCEPT_ID = 10
                  AND CSM_CAGE_SESSION_ID =  @CurrentSessionId 
                  AND CSM_ISO_CODE = @ISOCode) 
      BEGIN
        UPDATE CAGE_SESSION_METERS 
           SET CSM_ISO_CODE = @ISOCode
         WHERE CSM_SOURCE_TARGET_ID = 12
           AND CSM_CONCEPT_ID = 10
           AND CSM_ISO_CODE = @OldISOCode
           AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
      END
      
      UPDATE CAGE_SESSION_METERS 
         SET CSM_VALUE_IN = ISNULL((SELECT SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS CMD_DEPOSITS
                                      FROM MONEY_COLLECTIONS
                                           INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CGM_MC_COLLECTION_ID
                                     WHERE CGM_TYPE = 104
                                       AND CGM_CAGE_SESSION_ID = @CurrentSessionId), 0)      
       WHERE CSM_SOURCE_TARGET_ID = 12
        AND CSM_CONCEPT_ID = 10
        AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
        AND CSM_ISO_CODE = @ISOCode
            
     UPDATE CAGE_SESSION_METERS 
        SET CSM_VALUE = CSM_VALUE_IN
      WHERE CSM_SOURCE_TARGET_ID = 12
        AND CSM_CONCEPT_ID = 10
        AND CSM_CAGE_SESSION_ID =  @CurrentSessionId
        AND CSM_ISO_CODE = @ISOCode



     FETCH NEXT FROM Curs INTO @CurrentSessionId
                
    END

    CLOSE Curs
    DEALLOCATE Curs


DELETE FROM CAGE_METERS WHERE CM_ISO_CODE = @OldISOCode
DELETE FROM CAGE_SESSION_METERS WHERE CSM_ISO_CODE = @OldISOCode

UPDATE GENERAL_PARAMS
   SET GP_KEY_VALUE = 1
WHERE GP_GROUP_KEY = 'Cage' 
   AND GP_SUBJECT_KEY = 'Meters.SpecialUpdateExecuted'
   
END
--/*************** UPDATE/DELETE   **********************************/

GO
