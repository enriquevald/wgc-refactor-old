﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GET PROFIT REPORT FROM TERMINALS

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    24-JAN-2017			EOR				New procedure
1.0.1	    20-FEB-2017			EOR				Modify Meters Code
2.0.0     28-FEB-2017     JML       Bug 24011*:Errors in New report "Profit". El "Profit" no está bien calculado ya que debería coincidir con el NetWin.
                                    Bug 24871*:Reporte Generic reports - Terminals contempla los incrementos de los contadores
3.0.0	    05-MAY-2017			LTC				Bug 25750: GUI - Generic Reports:"Profit Report" shows column name "Total Out Redimido" in english language
3.0.1	    19-JAN-2017			RGR				Bug 31287:WIGOS-5979 [Ticket #9759] Site Jackpot report 

Requeriments:
   -- Functions:
         
Parameters:
   -- pDateFrom:					
   -- pDateTo :	
*/

IF  EXISTS (SELECT 	1 FROM 	sys.objects WHERE 	object_id = OBJECT_ID(N'[dbo].[GetProfitReport]') AND type in (N'P', N'PC'))
BEGIN
  DROP PROCEDURE [dbo].[GetProfitReport]
END
GO

CREATE PROCEDURE [dbo].[GetProfitReport]
  @pFrom  DATETIME,
  @pTo    DATETIME
AS
BEGIN

-- Create temp table for whole report
CREATE TABLE #TT_PROFITREPORT (
ORDER_TYPE            INT,
TSMH_DATETIME         DATETIME, 
TE_PROVIDER_ID        NVARCHAR(200), 
TE_NAME               NVARCHAR(200), 
BILL_IN               MONEY, 
TICKET_OUT_REDIMIBLE  MONEY,
HANDPAYS              MONEY,
CANCELLED_CREDITS     MONEY,
PROFIT                MONEY,
JACKPOT               MONEY
)

--DECLARE @pDetail BIT
DECLARE @closing_hour    int
DECLARE @closing_minutes int

--SET @pDetail = 0

SET @closing_hour = ISNULL((SELECT   gp_key_value
                              FROM   general_params
                             WHERE   gp_group_key   = 'WigosGUI'
                               AND   gp_subject_key = 'ClosingTime'), 6)

SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                 FROM   general_params
                                WHERE   gp_group_key   = 'WigosGUI'
                                  AND   gp_subject_key = 'ClosingTimeMinutes'), 0)
--IF @pDetail = 0
--BEGIN
  INSERT INTO #TT_PROFITREPORT
  SELECT  0 ORDER_TYPE   
        , CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
               THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
               ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                END 
        , '--'  TE_PROVIDER_ID
        , '--'  TE_NAME
        /*BILL IN 11 128*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET OUT 134 */
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*HANDPAYS 130 132*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS HANDPAYS
        /*CANCELLED CREDITS 3*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS		
        /*PROFIT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS PROFIT
		/*JACKPOT*/
		, SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOT
  FROM TERMINALS
  INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
  WHERE TSMH_DATETIME >= @pFrom AND TSMH_DATETIME < @pTo
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
							  , 2   /*Jackpot*/
                              ) 
  GROUP BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 
  ORDER BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 

SELECT  CASE TSMH_DATETIME WHEN '9999-12-31' THEN '' ELSE CONVERT(NVARCHAR(10),TSMH_DATETIME,103) END TSMH_DATETIME
      , TE_PROVIDER_ID
      , TE_NAME
      , BILL_IN
      , TICKET_OUT_REDIMIBLE
      , HANDPAYS
      , CANCELLED_CREDITS
	  , PROFIT 	  
      , JACKPOT 
FROM (
  SELECT  ORDER_TYPE 
        , TSMH_DATETIME
        , TE_PROVIDER_ID
        , TE_NAME
        , BILL_IN
        , TICKET_OUT_REDIMIBLE
        , HANDPAYS
        , CANCELLED_CREDITS
		, PROFIT
        , JACKPOT
  FROM #TT_PROFITREPORT
  UNION ALL
  SELECT  3 ORDER_TYPE
        , CAST('9999-12-31' AS DATETIME) TSMH_DATETIME
        , 'TOTAL' TE_PROVIDER_ID
        , '' TE_NAME
        , SUM(BILL_IN)  AS BILL_IN
        , SUM(TICKET_OUT_REDIMIBLE) AS  TICKET_OUT_REDIMIBLE
        , SUM(HANDPAYS) AS  HANDPAYS
        , SUM(CANCELLED_CREDITS)  AS  CANCELLED_CREDITS		
        , SUM(PROFIT) AS  PROFIT 
        , SUM(JACKPOT) AS JACKPOT
  FROM #TT_PROFITREPORT
  WHERE ORDER_TYPE = 0) T
ORDER BY T.TSMH_DATETIME,T.ORDER_TYPE
        
DROP TABLE #TT_PROFITREPORT

END
GO

GRANT EXECUTE ON [dbo].[GetProfitReport] TO wggui WITH GRANT OPTION
GO


BEGIN
  DECLARE @rtc_form_id INT

  DECLARE @_sheet XML

  SET @_sheet =
  '<ArrayOfReportToolDesignSheetsDTO>
	<ReportToolDesignSheetsDTO>
		<LanguageResources>
			<NLS09><Label>Profit Report</Label></NLS09>
			<NLS10><Label>Reporte Fairbet</Label></NLS10>
		</LanguageResources>
		<Columns>
			<ReportToolDesignColumn>
				<Code>TSMH_DATETIME</Code>
				<Width>150</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Session Date</Label>
					</NLS09>
					<NLS10>
						<Label>Fecha Sesión</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_PROVIDER_ID</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Provider</Label>
					</NLS09>
					<NLS10>
						<Label>Proveedor</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_NAME</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Terminal name</Label>
					</NLS09>
					<NLS10>
						<Label>Nombre de la terminal</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>BILL_IN</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total Bill IN</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada de billetes</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TICKET_OUT_REDIMIBLE</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total OUT Redeemed</Label>
					</NLS09>
					<NLS10>
						<Label>Total OUT Redimido</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>HANDPAYS</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Manual Handpays</Label>
					</NLS09>
					<NLS10>
						<Label>Pagos Manuales</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>CANCELLED_CREDITS</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Cancelled credits</Label>
					</NLS09>
					<NLS10>
						<Label>Créditos cancelados</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>			
			<ReportToolDesignColumn>
				<Code>PROFIT</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Profit</Label>
					</NLS09>
					<NLS10>
						<Label>Profit</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>JACKPOT</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jackpot</Label>
					</NLS09>
					<NLS10>
						<Label>Jackpot</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
		</Columns>
	</ReportToolDesignSheetsDTO>
   </ArrayOfReportToolDesignSheetsDTO>
  '
  
  IF EXISTS(SELECT TOP 1 1
            FROM report_tool_config
            WHERE rtc_store_name = 'GetProfitReport')
  BEGIN
    UPDATE  report_tool_config
    SET     rtc_design_sheets = @_sheet
          , rtc_design_filter = '<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>'
    WHERE rtc_store_name = 'GetProfitReport'
  END
  ELSE
  BEGIN 
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id),10999) + 1
    FROM report_tool_config

    INSERT INTO report_tool_config
              ( rtc_form_id 
              , rtc_location_menu
              , rtc_report_name
              , rtc_store_name
              , rtc_design_filter
              , rtc_design_sheets
              , rtc_mailing
              , rtc_status
              , rtc_mode_type
              )
    VALUES    (  @rtc_form_id 
	            ,2
				,'<LanguageResources><NLS09><Label>Profit Report</Label></NLS09><NLS10><Label>Reporte Fairbet</Label></NLS10></LanguageResources>' 
                ,'GetProfitReport' 
				,'<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>' 
                ,@_sheet
				,0 
				,1 
				,0  
              )        
  END 
END
GO
