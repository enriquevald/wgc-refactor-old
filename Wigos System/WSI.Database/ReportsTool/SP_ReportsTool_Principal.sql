IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportsTool_Principal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_ReportsTool_Principal]
GO

CREATE PROCEDURE [dbo].[SP_ReportsTool_Principal]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- NOMBRES SHEETS
SELECT 'NombreSheet1', 1
UNION
SELECT 'NombreSheet2', 2
ORDER BY 2 ASC

-- SP_ReportsTool_1
EXEC SP_ReportsTool_1 @Month, @Year

-- SP_ReportsTool_2
EXEC SP_ReportsTool_2 @Month, @Year

END
