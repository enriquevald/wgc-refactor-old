IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetMetersCollectionToCollection_PR') AND type in ('P', 'PC'))
DROP PROCEDURE GetMetersCollectionToCollection_PR
GO
/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GET METERS COLLECTION TO COLLECTION FOR PUERTO RICO

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    11-MAY-2016			        RGR				New procedure

Requeriments:
   -- Functions:
         
Parameters:
   -- pStarDateTime:					Start date for data collection.
   -- pEndDateTime :					End date for data collection.
*/ 
CREATE PROCEDURE GetMetersCollectionToCollection_PR
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN

CREATE TABLE #WORK_TABLE_PR(
   ID INTEGER,
   MC_START_INSERT_DATETIME  DATETIME,
   MC_END_INSERT_DATETIME DATETIME,
   MC_TERMINAL_ID INTEGER,
   MC_DENOMINATION MONEY,
   ME_COIN_IN DECIMAL,
   ME_COIN_OUT DECIMAL,
   ME_JACKPOT DECIMAL,
   ME_DOOR_OPEN INTEGER,
   MCM_1_BILL_EXPECTED INTEGER,
   MCM_2_BILL_EXPECTED INTEGER,
   MCM_5_BILL_EXPECTED INTEGER,
   MCM_10_BILL_EXPECTED INTEGER,
   MCM_20_BILL_EXPECTED INTEGER,
   MCM_25_BILL_EXPECTED INTEGER,
   MCM_50_BILL_EXPECTED INTEGER,
   MCM_100_BILL_EXPECTED INTEGER,
   MCM_200_BILL_EXPECTED INTEGER,
   MCM_250_BILL_EXPECTED INTEGER,
   MCM_500_BILL_EXPECTED INTEGER,
   MCM_1000_BILL_EXPECTED INTEGER,
   MCM_2000_BILL_EXPECTED INTEGER,
   MCM_2500_BILL_EXPECTED INTEGER,
   MCM_5000_BILL_EXPECTED INTEGER,
   MCM_10000_BILL_EXPECTED INTEGER,
   MCM_20000_BILL_EXPECTED INTEGER,
   MCM_25000_BILL_EXPECTED INTEGER,
   MCM_50000_BILL_EXPECTED INTEGER,
   MCM_100000_BILL_EXPECTED INTEGER,
   MCM_200000_BILL_EXPECTED INTEGER,
   MCM_250000_BILL_EXPECTED INTEGER,
   MCM_500000_BILL_EXPECTED INTEGER,
   MCM_1000000_BILL_EXPECTED INTEGER,
   MC_EXPECTED_BILL_AMOUNT INTEGER,   
   MCM_1_BILL_COLLECTED INTEGER,
   MCM_2_BILL_COLLECTED INTEGER,
   MCM_5_BILL_COLLECTED INTEGER,
   MCM_10_BILL_COLLECTED INTEGER,
   MCM_20_BILL_COLLECTED INTEGER,
   MCM_25_BILL_COLLECTED INTEGER,
   MCM_50_BILL_COLLECTED INTEGER,
   MCM_100_BILL_COLLECTED INTEGER,
   MCM_200_BILL_COLLECTED INTEGER,
   MCM_250_BILL_COLLECTED INTEGER,
   MCM_500_BILL_COLLECTED INTEGER,
   MCM_1000_BILL_COLLECTED INTEGER,
   MCM_2000_BILL_COLLECTED INTEGER,
   MCM_2500_BILL_COLLECTED INTEGER,
   MCM_5000_BILL_COLLECTED INTEGER,
   MCM_10000_BILL_COLLECTED INTEGER,
   MCM_20000_BILL_COLLECTED INTEGER,
   MCM_25000_BILL_COLLECTED INTEGER,
   MCM_50000_BILL_COLLECTED INTEGER,
   MCM_100000_BILL_COLLECTED INTEGER,
   MCM_200000_BILL_COLLECTED INTEGER,
   MCM_250000_BILL_COLLECTED INTEGER,
   MCM_500000_BILL_COLLECTED INTEGER,
   MCM_1000000_BILL_COLLECTED INTEGER,
   MC_COLLECTED_BILL_AMOUNT INTEGER
) 
INSERT INTO #WORK_TABLE_PR 
EXEC GetMetersCollectionToCollection @pStarDateTime, @pEndDateTime 

SELECT  
    CONVERT(char(10), WT.MC_START_INSERT_DATETIME, 126)  AS 'Gaming Date'
   ,WT.MC_TERMINAL_ID                                    AS 'Machine Number'    
   ,WT.MC_DENOMINATION                                   AS 'Machine Denomination' 
   ,WT.ME_COIN_IN                                        AS 'Coin In' 
   ,WT.ME_COIN_OUT                                       AS 'Coin Out'
   ,0                                                    AS 'Coin Drop'
   ,WT.ME_JACKPOT                                        AS 'Jackpots'                  
   ,WT.ME_DOOR_OPEN                                      AS 'Main Door Open'
   ,WT.MCM_1_BILL_EXPECTED                               AS 'Soft Count $1 Meter'  
   ,WT.MCM_5_BILL_EXPECTED                               AS 'Soft Count $5 Meter'
   ,WT.MCM_10_BILL_EXPECTED                              AS 'Soft Count $10 Meter'
   ,WT.MCM_20_BILL_EXPECTED                              AS 'Soft Count $20 Meter'
   ,WT.MCM_50_BILL_EXPECTED                              AS 'Soft Count $50 Meter'
   ,WT.MCM_100_BILL_EXPECTED                             AS 'Soft Count $100 Meter'
   ,WT.MC_EXPECTED_BILL_AMOUNT                           AS 'Soft Count Total Meter'
   ,0                                                    AS 'Total Coin Drop fisico'
   ,WT.MCM_1_BILL_COLLECTED                              AS '$1 fisico'
   ,WT.MCM_5_BILL_COLLECTED                              AS '$5 fisico'
   ,WT.MCM_10_BILL_COLLECTED                             AS '$10 fisico'
   ,WT.MCM_20_BILL_COLLECTED                             AS '$20 fisico'
   ,WT.MCM_50_BILL_COLLECTED                             AS '$50 fisico'
   ,WT.MCM_100_BILL_COLLECTED                            AS '$100 fisico'
   ,WT.MC_COLLECTED_BILL_AMOUNT                          AS 'Total contado en el soft count'
FROM #WORK_TABLE_PR AS WT

DROP TABLE #WORK_TABLE_PR
END
GO
GRANT EXECUTE ON GetMetersCollectionToCollection_PR TO wggui WITH GRANT OPTION 