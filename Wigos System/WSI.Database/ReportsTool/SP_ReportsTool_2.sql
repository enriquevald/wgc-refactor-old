IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportsTool_2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_ReportsTool_2]
GO

CREATE PROCEDURE [dbo].[SP_ReportsTool_2]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- SP_ReportsTool_2
SELECT   PV_ID
       , PV_NAME
  FROM   PROVIDERS  
END

GO

