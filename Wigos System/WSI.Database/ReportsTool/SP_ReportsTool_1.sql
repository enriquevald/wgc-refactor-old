IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportsTool_1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_ReportsTool_1]
GO

CREATE PROCEDURE [dbo].[SP_ReportsTool_1]
  (@Month int,
   @Year  int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- SP_ReportsTool_1
SELECT   TE_TERMINAL_ID
       , TE_EXTERNAL_ID
       , TE_PROVIDER_ID
       , TE_BASE_NAME
       , TE_NAME
  FROM   TERMINALS
  
END

GO

