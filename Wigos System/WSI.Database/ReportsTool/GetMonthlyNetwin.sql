﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GET MONTHLY NETWIN FROM TERMINALS

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    09-NOV-2016			ESE				New procedure
1.0.1     17-NOV-2016     ESE       Change Total IN = Ticket In + Bill In and Total OUT = Ticket out + HP + JP
2.0.0     28-FEB-2017     JML       Bug 24011*:Errors in New report "Profit". El "Profit" no está bien calculado ya que debería coincidir con el NetWin.
                                    Bug 24871*:Reporte Generic reports - Terminals contempla los incrementos de los contadores
3.0.0     16-JUN-2017     LTC       WIGOS-2799 - [Ticket #6217] After updating the database to the v03.005.0060 version , Wigos doesn´t work.

Requeriments:
   -- Functions:
         
Parameters:
   -- pMonth:					
   -- pYear :					
*/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMonthlyNetwin]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetMonthlyNetwin]
GO

CREATE PROCEDURE [dbo].[GetMonthlyNetwin]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @_aux_date AS DATETIME
DECLARE @_aux_date_init AS DATETIME
DECLARE @_aux_date_final AS DATETIME
DECLARE @_aux_site_name AS NVARCHAR(200)

-- Create temp table for whole report
CREATE TABLE #TT_MONTHLYNETWIN (
TSMH_MONTH				        NVARCHAR(5), 
SITE_NAME				          NVARCHAR(200), 
TE_NAME					          NVARCHAR(200), 
TE_SERIAL_NUMBER		      NVARCHAR(200),
BILL_IN					          MONEY, 
TICKET_IN_REDIMIBLE		    MONEY,
TICKET_OUT_REDIMIBLE	    MONEY,
TOTAL_IN				          MONEY,
TOTAL_OUT				          MONEY,
REDEEM_TICKETS			      MONEY,
TICKET_IN_PROMOTIONAL	    MONEY,
CANCELLED_CREDITS		      MONEY,
JACKPOTS				          MONEY,
LGA_NETWIN				        MONEY,
TOTAL_COIN_IN			        MONEY,
TOTAL_WON				          MONEY,
TOTAL_COIN_IN_TOTAL_WON	  MONEY)

--Obtain the INITIAL and FINAL date
set @_aux_date = CONVERT(DATETIME,CONVERT(NVARCHAR(4),@pYear) + '-' + CONVERT(NVARCHAR(2),@pMonth) + '-' +  '1')
set @_aux_date_init = CONVERT(NVARCHAR(25),DATEADD(dd,-(DAY(@_aux_date)-1),@_aux_date),101)
set @_aux_date_final = DATEADD(dd,-(DAY(DATEADD(mm,1,@_aux_date))),DATEADD(hh,23,DATEADD(mi,59,DATEADD(ss,59,DATEADD(mm,1,@_aux_date)))))

--Obtain site ID and site name
set @_aux_site_name = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier') + ' - ' + (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Name')

INSERT INTO #TT_MONTHLYNETWIN
SELECT    (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME)END) AS TSMH_MONTH
        , @_aux_site_name AS SITE_NAME
        , TE_NAME
        , TE_SERIAL_NUMBER
        /*BILL IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_REDIMIBLE
        /*TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*TOTAL IN = BILL IN + TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_IN
        /*TOTAL OUT = JACKPOTS + CANCELLED CREDITS + TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_OUT
        /*REDEEM TICKETS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN 
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               ELSE 
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               END ) AS  REDEEM_TICKETS
        /*TICKET IN PROMOTIONAL (redeem, no redeem)*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_PROMOTIONAL
        /*CANCELLED CREDITS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOTS
        /*LGA NETWIN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS LGA_NETWIN
        /*TOTAL COIN IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN

        /*TOTAL WON = COIN OUT + JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_WON
        /*TOTAL COIN IN - TOTAL WON*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN_TOTAL_WON
FROM TERMINALS
INNER JOIN TERMINAL_SAS_METERS_HISTORY ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
WHERE TSMH_DATETIME >= @_aux_date_init AND TSMH_DATETIME < @_aux_date_final
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                              , 1   /*Total OUT*/
                              , 2   /*Jackpots*/
                              , 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
GROUP BY  TE_TERMINAL_ID, TE_NAME, TE_SERIAL_NUMBER, TE_MACHINE_SERIAL_NUMBER,
          (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME) END)
ORDER BY TE_NAME
          
/*Add the TOTAL ROW*/
SELECT TSMH_MONTH,SITE_NAME, TE_NAME, TE_SERIAL_NUMBER, BILL_IN, TICKET_IN_REDIMIBLE, TICKET_OUT_REDIMIBLE, TOTAL_IN, TOTAL_OUT, REDEEM_TICKETS, TICKET_IN_PROMOTIONAL, CANCELLED_CREDITS, JACKPOTS, LGA_NETWIN, TOTAL_COIN_IN, TOTAL_WON, TOTAL_COIN_IN_TOTAL_WON
FROM #TT_MONTHLYNETWIN
UNION ALL
SELECT 'TOTAL' AS TSMH_MONTH, '' AS SITE_NAME, '' AS TE_NAME, '' AS TE_SERIAL_NUMBER, SUM(BILL_IN), SUM(TICKET_IN_REDIMIBLE), SUM(TICKET_OUT_REDIMIBLE), SUM(TOTAL_IN), SUM(TOTAL_OUT), SUM(REDEEM_TICKETS), SUM(TICKET_IN_PROMOTIONAL), SUM(CANCELLED_CREDITS), SUM(JACKPOTS), SUM(LGA_NETWIN), SUM(TOTAL_COIN_IN), SUM(TOTAL_WON), SUM(TOTAL_COIN_IN_TOTAL_WON)
FROM #TT_MONTHLYNETWIN
          
DROP TABLE #TT_MONTHLYNETWIN

END
GO

GRANT EXECUTE ON [dbo].[GetMonthlyNetwin] TO wggui WITH GRANT OPTION
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  
  SET @rtc_design_sheet =                                   '<ArrayOfReportToolDesignSheetsDTO>
                                                                <ReportToolDesignSheetsDTO>
                                                                  <LanguageResources>
                                                                    <NLS09>
                                                                      <Label>Monthly Report NetWin</Label>
                                                                    </NLS09>
                                                                    <NLS10>
                                                                      <Label>Reporte mensual de NetWin</Label>
                                                                    </NLS10>
                                                                  </LanguageResources>
                                                                  <Columns>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_MONTH</Code>
                                                                      <Width>150</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Month</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_DAY</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Day</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Día</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>SITE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Site Name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la sala</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Terminal name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la terminal</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_SERIAL_NUMBER</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Serial number</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Número de serie</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Bill IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>BILL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Bill IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada de billetes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket IN redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_OUT_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket OUT redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Salida ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total entradas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_OUT</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total OUT</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total salidas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Redeem tickets-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>REDEEM_TICKETS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Redeem tickets</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Tickets redimibles</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Promotional ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_PROMOTIONAL</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Promotional ticket IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket promocional</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Cancelled credits-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>CANCELLED_CREDITS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Cancelled credits</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Créditos cancelados</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Jackpots-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>JACKPOTS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--LGA Netwin-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>LGA_NETWIN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL COIN IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_COIN_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Coin In</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Jugado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL WON-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_WON</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Won</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Ganado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL COIN IN - TOTAL WON-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_COIN_IN_TOTAL_WON</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Coin In - Total Won</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Jugado - Total Ganado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                  </Columns>
                                                                </ReportToolDesignSheetsDTO>
                                                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'GetMonthlyNetwin')
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id),10999) + 1
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID     4*/
                                      /*RTC_FORM_ID*/        (@rtc_form_id,
                                      /*RTC_LOCATION_MENU*/   2, 
                                      /*RTC_REPORT_NAME*/     '<LanguageResources><NLS09><Label>Monthly Report NetWin</Label></NLS09><NLS10><Label>Reporte mensual de NetWin</Label></NLS10></LanguageResources>',
                                      /*RTC_STORE_NAME*/      'GetMonthlyNetwin',
                                      /*RTC_DESIGN_FILTER*/   '<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
                                      /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
                                      /*RTC_MAILING*/         0,
                                      /*RTC_STATUS*/          1,
                                      /*RTC_MODE*/            1)
                                      
  END 
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS =           /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'GetMonthlyNetwin'
  END
END
GO
