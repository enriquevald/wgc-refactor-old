﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

	DECLARE @CurrentDay AS DATETIME
	SELECT @CurrentDay = [dbo].[TodayOpening] (1)

	SET NOCOUNT ON;


	-- Insert all gaming tables to terminals connected with default values to 0
	INSERT INTO   GAMING_TABLES_CONNECTED
		 SELECT   GT_GAMING_TABLE_ID, CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)), 0, 0
		   FROM   GAMING_TABLES
	  LEFT JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
		  WHERE   GMC_GAMING_DAY IS NULL

    -- Update Enabled
    	 UPDATE   GAMING_TABLES_CONNECTED
	        SET   GMC_ENABLED = (CASE WHEN S.S_ENABLED = 1 OR GMC_ENABLED = 1 THEN 1 ELSE 0 END)
	       FROM 
			(
				  SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
					       , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
					       , GT_ENABLED AS S_ENABLED
				    FROM   GAMING_TABLES
			  INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))			
			    GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
			)S
	      WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY
    
    -- Update Used    
	       UPDATE   GAMING_TABLES_CONNECTED
	          SET   GMC_USED = CASE WHEN S.S_USED = 1 OR GMC_USED = 1 THEN 1 ELSE 0 END
	         FROM 
			(
				  SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
					       , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
					       , CASE WHEN MAX(GTS.GTS_CASHIER_SESSION_ID) > 0 THEN 1 ELSE 0 END AS S_USED
				    FROM   GAMING_TABLES
			INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			 LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GMC_GAMINGTABLE_ID = GTS_GAMING_TABLE_ID  AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			INNER JOIN   CASHIER_SESSIONS CS ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
						 AND (
							   (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
							OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
							OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
							OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
							 )
			  GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
			)S
	      WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO