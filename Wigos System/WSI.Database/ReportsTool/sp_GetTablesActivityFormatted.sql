﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetTablesActivityFormatted]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetTablesActivityFormatted]
GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GET METERS COLLECTION TO COLLECTION

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.1     13-JUL-2017     DHA
1.0.2     31-JUL-2017     RAB       Bug 29019: WIGOS-4022 Inconsistent English translations for "Mesas de juego" in "Tables" GUI menu entries and titles in related windows

Requeriments:
   -- Functions:
         
Parameters:
   -- pMonth:					Month date for data collection.
   -- pYear :					Year date for data collection.
*/ 

CREATE PROCEDURE [dbo].[sp_GetTablesActivityFormatted]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @Date AS DATETIME
DECLARE @Count INT
DECLARE @ColsVal VARCHAR(MAX)
DECLARE @ColsValU VARCHAR(MAX)
DECLARE @ColsEnabled VARCHAR(MAX)
DECLARE @ColsUsed VARCHAR(MAX)
DECLARE @ColsEnabledSum VARCHAR(MAX)
DECLARE @ColsUsedSum VARCHAR(MAX)

SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(4),@pYear) + '-' + CONVERT(VARCHAR(2),@pMonth) + '-' +  '1')
SELECT @Count = 0
SELECT @ColsVal = ''
SELECT @ColsValU = ''
SELECT @ColsEnabled = ''
SELECT @ColsUsed = ''
SELECT @ColsEnabledSum = ''
SELECT @ColsUsedSum = ''

WHILE @Count < DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,@Date),0)))
BEGIN

   SELECT @Count = @Count + 1
   IF @Count <> 1
    BEGIN
		SELECT @ColsVal = @ColsVal + ','
		SELECT @ColsValU = @ColsValU + ','
		SELECT @ColsEnabled = @ColsEnabled + ','
		SELECT @ColsUsed = @ColsUsed + ','
		SELECT @ColsEnabledSum = @ColsEnabledSum + ','
		SELECT @ColsUsedSum = @ColsUsedSum + ','
	END
	
   SELECT @ColsVal = @ColsVal + ' T_D' + CONVERT(VARCHAR,@Count) + ' INT Default 0'
   SELECT @ColsValU = @ColsValU + ' T_DU' + CONVERT(VARCHAR,@Count) + ' INT Default 0'
   SELECT @ColsEnabled = @ColsEnabled + 'T_D' + CONVERT(VARCHAR,@Count) + ' ''' + CONVERT(VARCHAR,@Count) +  ''''
   SELECT @ColsUsed = @ColsUsed + 'T_DU' + CONVERT(VARCHAR,@Count) + ' ''' + CONVERT(VARCHAR,@Count) +  ' '''
   SELECT @ColsEnabledSum = @ColsEnabledSum + 'ISNULL(SUM(T_D' + CONVERT(VARCHAR,@Count) + '),0) ' + ''
   SELECT @ColsUsedSum = @ColsUsedSum + 'ISNULL(SUM(T_DU' + CONVERT(VARCHAR,@Count) + '),0) ' +  ''
END

DECLARE @Query NVARCHAR(MAX)
SET @Query = '  

		IF OBJECT_ID(''tempdb..#TableMetricsHeader'') IS NOT NULL
			DROP TABLE #TableMetricsHeader

			CREATE TABLE #TableMetricsHeader
			(
				t_Id		INT,
				t_Name		VARCHAR(50),
				t_Type		VARCHAR(50),
				t_Month		INT Default 0,
				t_Days		INT Default 0,
				' + @ColsVal + ',
				t_DaysUse	INT Default 0,
				' + @ColsValU + '
			)

			INSERT INTO #TableMetricsHeader
			EXEC [dbo].[sp_GetTablesActivity] ''' + CONVERT(VARCHAR(10),@Date,112) + '''

			SELECT		t_Type ''TIPO'',
						t_Name ''MESA'',
						t_Month	''MES'',
						t_Days ''DIAS HABILITADA'',
						' + @ColsEnabled + ',	   
						t_DaysUse ''DIAS USADO'',
						' + @ColsUsed + '	   
			FROM #TableMetricsHeader
			UNION ALL
			SELECT	''TOTAL'',						
						''' + cast(@pYear as nvarchar(4)) + '-' + right('00'+cast( @pMonth as nvarchar(2)), 2) +''',						
						ISNULL(SUM(t_Month),0),
						ISNULL(SUM(t_Days),0),
						' + @ColsEnabledSum + ',	   
						ISNULL(SUM(t_DaysUse),0),
						' + @ColsUsedSum + '	   
			FROM #TableMetricsHeader

	'

EXEC SP_EXECUTESQL @Query

END

GO

GRANT EXECUTE ON [dbo].[sp_GetTablesActivityFormatted] TO [WGGUI] WITH GRANT OPTION
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)
  
  SET @rtc_report_name =                                    '<LanguageResources><NLS09><Label>Gambling table activity</Label></NLS09><NLS10><Label>Actividad en mesa de juego</Label></NLS10></LanguageResources>'
  SET @rtc_design_sheet =                                   '<ArrayOfReportToolDesignSheetsDTO>
                                                                <ReportToolDesignSheetsDTO>
                                                                  <LanguageResources>
                                                                    <NLS09>
                                                                      <Label>Gambling table activity</Label>
                                                                    </NLS09>
                                                                    <NLS10>
                                                                      <Label>Actividad en mesa de juego</Label>
                                                                    </NLS10>
                                                                  </LanguageResources>
                                                                  <Columns>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TIPO</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Type</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Tipo</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>MESA</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Table</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mesa</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>MES</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Month</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>DIAS HABILITADA</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Enabled days</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Dias habilitada</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>DIAS USADO</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Used days</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Dias usado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                  </Columns>
                                                                </ReportToolDesignSheetsDTO>
                                                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'sp_GetTablesActivityFormatted')
  BEGIN
    SELECT @rtc_form_id = 11001
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID*/
                                      /*RTC_FORM_ID*/        (@rtc_form_id,
                                      /*RTC_LOCATION_MENU*/   12, 
                                      /*RTC_REPORT_NAME*/     @rtc_report_name,
                                      /*RTC_STORE_NAME*/      'sp_GetTablesActivityFormatted',
                                      /*RTC_DESIGN_FILTER*/   '<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
                                      /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
                                      /*RTC_MAILING*/         1,
                                      /*RTC_STATUS*/          1,
                                      /*RTC_MODE_TYPE*/       11)
                                      
  END 
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'sp_GetTablesActivityFormatted'
  END
END
GO