 
--
--
--  Script actualización AC_HOLDER_LEVEL
--
--  Set the holder level to 1 (Silver) if the account is not Anon
--
 
 
 UPDATE ACCOUNTS SET ac_holder_level = 1
				       WHERE ac_holder_name  <> ''
				         AND ac_holder_name  IS NOT NULL
				         AND ( ac_holder_level IS NULL
				         OR    ac_holder_level = 0 )
				          
 -- ROLLBACK
 -- COMMIT
 
 