--------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_SetTicketCanceled.sql
--
--   DESCRIPTION: --
--
--        AUTHOR: --
--
-- CREATION DATE: 24-JUL-2017
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- xx-xxx-xxxx XXX    First release .
-- 24-JUL-2017 FJC    Fixed Bug WIGOS-3915 Accounting - TITO tickets from the EGM terminal are associated with anonymous accounts when using players card.

 
 /****** Object:  StoredProcedure [dbo].[TITO_SetTicketCanceled]    Script Date: 01/21/2014 11:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketCanceled]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketCanceled]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketCanceled]
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @MsgSequenceId               BIGINT,
      @pTicketRejectReasonEgm      BIGINT,
      @pTicketRejectReasonWcp      BIGINT,
      --- OUTPUT
      @pTicketId                   BIGINT   OUTPUT,
      @pTicketAmount               MONEY    OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_canceled              INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_ticket_amount                       MONEY
      DECLARE  @_virtual_account_id                  BIGINT
      DECLARE  @_player_account_id                   BIGINT
      DECLARE  @_last_action_account_id              BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL
      SET @pTicketAmount = NULL

      SET @_ticket_status_canceled       = 1 -- TITO_TICKET_STATUS.CANCELED
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
      
      --SELECT   @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
      --  FROM   TERMINALS
      -- WHERE   TE_TERMINAL_ID = @pTerminalId 
      
      --Get AccountID (From indentified Player or not)
      SELECT TOP 1  @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)                                                                       
                  , @_player_account_id  = ISNULL(PS_ACCOUNT_ID, 0)                                                                       
            FROM    TERMINALS                                                                                               
       LEFT JOIN    PLAY_SESSIONS      
              ON    PS_TERMINAL_ID  = TE_TERMINAL_ID 
             AND    PS_STATUS       = 0               --PlaySession OPENED     
           WHERE    TE_TERMINAL_ID  = @pTerminalId                                                
        ORDER BY    PS_PLAY_SESSION_ID  DESC                     

      SET @_last_action_account_id = @_virtual_account_id --Without Card (VirtualAccountId) "anonymous"

      IF (@_player_account_id) > 0 
      BEGIN
        SET @_last_action_account_id = @_player_account_id --Player identified
      END


      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
             , @_ticket_amount = MAX (TI_AMOUNT)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_canceled 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
                   , TI_LAST_ACTION_ACCOUNT_ID  = @_last_action_account_id 
                   , TI_REJECT_REASON_EGM       = @pTicketRejectReasonEgm
                   , TI_REJECT_REASON_WCP       = @pTicketRejectReasonWcp
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
            BEGIN
                  SET   @pTicketId     = @_min_ticket_id
                  SET   @pTicketAmount = @_ticket_amount

                  INSERT INTO   TITO_TASKS 
                              ( TT_TASK_TYPE
                              , TT_TICKET_ID )
                       VALUES ( 1 -- TITO_TICKET_STATUS.CANCELED
                              , @pTicketId   ) 
            END

            RETURN
      END
END

GO

