﻿------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: GetTicketAmounts
--
--   DESCRIPTION: Calculate ticket Amount from when ticket change from PENDING_PRINT to VALID
--        ALARMS: 0 - No alarm
--                1 - New amount is null or zero
--                2 - New amount greater of old amount
--                3 - New amount smaller of old amount
--                4 - New currency is diferent of older
--                5 - New amount greater of old amount with diferent currency
--                6 - New amount smaller of old amount with diferent currency
--
--        AUTHOR: José Martínez López
--
-- CREATION DATE: 13-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 13-NOV-2017 JML    First release. 
-- 15-NOV-2017 JML    Bug 30798:WIGOS-5926 [Ticket #9396] error titios promo
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTicketAmounts]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetTicketAmounts]
GO


CREATE PROCEDURE [dbo].[GetTicketAmounts]
                  @TerminalId           AS BigInt
                  
                , @OriginalTicketAmount AS Money
                , @OriginalTicketAmt0   AS Money
                , @OriginalTicketCur0   AS NVARCHAR(3)
                
                , @NewTicketAmount      AS Money
                , @NewTicketAmt0        AS Money
                , @NewTicketCur0        AS NVARCHAR(3)
                
                , @UpdateTicketAmt0     AS Money       OUTPUT
                , @UpdateTicketCur0     AS NVARCHAR(3) OUTPUT
                , @UpdateTicketAmount   AS Money       OUTPUT
                
                , @AlarmId              AS Int         OUTPUT

AS
BEGIN
  
  -- Set default values
  SET @UpdateTicketAmount = @NewTicketAmount
  SET @UpdateTicketAmt0   = @NewTicketAmt0
  SET @UpdateTicketCur0   = @NewTicketCur0

  -- ALARMS --
  
  SET @AlarmId = 0

  -- Set original amout if new amount is null or zero an set alarm
  IF @NewTicketAmt0 = 0
  BEGIN
    SET @AlarmId = 1 
    SET @UpdateTicketAmt0 = @OriginalTicketAmt0
  END

  -- Compare amounts and set alarm if it is necesary
  IF @OriginalTicketCur0 = @NewTicketCur0
  BEGIN
    IF @OriginalTicketAmt0 < @NewTicketAmt0
    BEGIN
      SET @AlarmId = 2
    END
    IF @OriginalTicketAmt0 > @NewTicketAmt0
    BEGIN
      SET @AlarmId = 3
    END
  END

  IF @OriginalTicketCur0 <> @NewTicketCur0
  BEGIN
    SET @AlarmId = 4
    IF @OriginalTicketAmt0 < @NewTicketAmt0
    BEGIN
      SET @AlarmId = 5
    END
    IF @OriginalTicketAmt0 > @NewTicketAmt0
    BEGIN
      SET @AlarmId = 6
    END
  END
  
END
GO

GRANT EXECUTE ON [GetTicketAmounts] TO wggui WITH GRANT OPTION
GO

--****************************************************************************************************************************
--****************************************************************************************************************************
--****************************************************************************************************************************

------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_UpdateTicketStatusToValid
--
--   DESCRIPTION: Change tickets status from PENDING_PRINT to VALID
--
--        AUTHOR: José Martínez López
--
-- CREATION DATE: 7-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 07-NOV-2017 JML    First release.
-- 15-NOV-2017 JML    Bug 30798:WIGOS-5926 [Ticket #9396] error titios promo
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_UpdateTicketStatusToValid]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_UpdateTicketStatusToValid]
GO


CREATE PROCEDURE [dbo].[TITO_UpdateTicketStatusToValid]
                  @pStatus              AS Int
                , @pMachineNumber       AS BigInt
                , @pValidationType      AS Int                                                       
                        
                , @pCreatedTerminalId   AS Int
                , @pValidationNumber    AS BigInt
                , @pTransactionId       AS BigInt
                , @pStatusPendingPrint  AS Int
                        
                , @pAmountCents         AS BigInt                                                       
                , @pCreatedAccountID    AS BigInt
                , @pPlaySessionId       AS BigInt

                , @UpdateTicketAmt0    AS Money       OUTPUT
                , @UpdateTicketCur0    AS NVARCHAR(3) OUTPUT
                , @UpdateTicketAmount  AS Money       OUTPUT
                
                , @AlarmId             AS Int         OUTPUT

AS
BEGIN
  DECLARE @LastTicketId         AS BigInt
  
  DECLARE @OriginalTicketAmount AS Money
  DECLARE @OriginalTicketAmt0   AS Money
  DECLARE @OriginalTicketCur0   AS NVARCHAR(3)
 
  DECLARE @SysCur               AS NVARCHAR(3)
  DECLARE @NewTicketAmount      AS Money
  DECLARE @NewTicketAmt0        AS Money
  DECLARE @NewTicketCur0        AS NVARCHAR(3)
  
  SET @LastTicketId = 0
                
  -- SELECT LAST TICKET ID
  SELECT   @LastTicketId = MAX(TI_TICKET_ID)
    FROM   TICKETS 
   WHERE   TI_CREATED_TERMINAL_ID = @pCreatedTerminalId        
     AND   TI_VALIDATION_NUMBER   = @pValidationNumber         
     AND   TI_STATUS              = @pStatusPendingPrint 
     AND   DATEDIFF(DAY, TI_CREATED_DATETIME, GETDATE()) <= 30  --FJC 18-10-2016 (PBI 18258) ValidationNumber could be repeated in one or more years. (ACC)  

  IF IsNull(@LastTicketId, 0) > 0
  BEGIN
    -- REGISTER TICKET BLOKED
    UPDATE   TICKETS 
       SET   TI_VALIDATION_NUMBER = TI_VALIDATION_NUMBER
     WHERE   TI_TICKET_ID = @LastTicketId
    
    -- GET SYSTEM CURRENCY CODE
    SELECT @SysCur = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

    -- Change amount in cents to amount in money with decimals
    SET @NewTicketAmt0 = isNull(@pAmountCents, 0.00) / 100.00

    -- SELECT TICKET/TERMINAL DATA
         SELECT   @OriginalTicketAmount = TI_AMOUNT
                , @OriginalTicketAmt0   = TI_AMT0
                , @OriginalTicketCur0  = TI_CUR0
                , @NewTicketCur0       = ISNULL(TE_ISO_CODE, @SysCur)
           FROM   TICKETS
      LEFT JOIN   TERMINALS ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID
          WHERE   TI_TICKET_ID = @LastTicketId
    
    -- SET DEFAULT VALUE FOR NEW TICKET AMOUNT
    SET @NewTicketAmount = @OriginalTicketAmount
    IF @NewTicketCur0 = @SysCur and @NewTicketAmt0 > 0
    BEGIN
      SET @NewTicketAmount = @NewTicketAmt0
    END

      -- GET AMOUNTS FOR UPDATE TICKET
      EXEC dbo.GetTicketAmounts   @pCreatedTerminalId
      
                                 , @OriginalTicketAmount 
                                 , @OriginalTicketAmt0
                                 , @OriginalTicketCur0
                                 
                                 , @NewTicketAmount
                                 , @NewTicketAmt0
                                 , @NewTicketCur0
                                 
                                 , @UpdateTicketAmt0   OUTPUT
                                 , @UpdateTicketCur0   OUTPUT
                                 , @UpdateTicketAmount OUTPUT
                                 
                                 , @AlarmId            OUTPUT
                                 
    -- If change amount amount or currency, calculate new amount in local currency (floor Dual Currency)
    IF @OriginalTicketAmt0 <> @UpdateTicketAmt0 OR @OriginalTicketCur0 <> @UpdateTicketCur0 OR @OriginalTicketAmount <> @UpdateTicketAmount
    BEGIN
      SELECT @UpdateTicketAmount = dbo.ApplyExchange2(@UpdateTicketAmt0, @UpdateTicketCur0, @SysCur)
    END


    SET NOCOUNT ON
     
      -- UPDATE TICKET AND SET STATUS VALID
      UPDATE   TICKETS                                             
         SET   TI_STATUS              = @pStatus                   
             , TI_MACHINE_NUMBER      = @pMachineNumber            
             , TI_VALIDATION_TYPE     = @pValidationType           
             , TI_TRANSACTION_ID      = @pTransactionId            
             , TI_AMOUNT              = @UpdateTicketAmount
             , TI_AMT0                = @UpdateTicketAmt0
             , TI_CUR0                = @UpdateTicketCur0
       WHERE   TI_TICKET_ID = @LastTicketId
       
  END --   IF IsNull(@LastTicketId, 0) > 0
  
END
GO

GRANT EXECUTE ON [TITO_UpdateTicketStatusToValid] TO wggui WITH GRANT OPTION
GO

--****************************************************************************************************************************
--****************************************************************************************************************************
--****************************************************************************************************************************

------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_CreateTicket
--
--   DESCRIPTION: Insert TITO Ticket in Database
--
--        AUTHOR: José Martínez López
--
-- CREATION DATE: 10-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 10-NOV-2017 JML    First release.
-- 15-NOV-2017 JML    Bug 30798:WIGOS-5926 [Ticket #9396] error titios promo
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_CreateTicket]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_CreateTicket]
GO

CREATE PROCEDURE [dbo].[TITO_CreateTicket]
                 @pTicketValidationNumber        AS BigInt           
               , @pTicketAmount                  AS Money
               , @pTicketStatus                  AS Int              
               , @pTicketStatusCheck             AS Int              
               , @pTicketType                    AS Int              
               , @pTicketCreatedDateTime         AS DateTime         
               , @pCreatedTerminalId             AS Int              
               , @pTerminalType                  AS Int              
               , @pTicketExpirationDateTime      AS DateTime       
               , @pTicketCreatedAccountID        AS BigInt           
               , @pPromotionId                   AS BigInt           
               , @pValidationType                AS Int              
               , @pMachineTicketNumber           AS Int              
               , @pTransactionId                 AS BigInt           
               , @pCreatedPlaySessionId          AS BigInt           
               , @pAccountPromotion              AS BigInt           
               , @pAmt0                          AS Money            
               , @pCur0                          AS NVarChar(3)
               , @pPendingPrintEnabled           AS Bit
               , @pTimeToCheckDuplicityInCashout AS Int

               --- OUTPUT
               , @pTicketId                      AS BigInt OUTPUT
               , @pValidationNumberOutput        AS BigInt OUTPUT
               , @pTransactionIdOutput           AS BigInt OUTPUT

                                
AS
BEGIN

  IF @pPendingPrintEnabled = 1 
  BEGIN

    SET @pValidationNumberOutput = 0  
    SET @pTransactionIdOutput    = 0  

    DECLARE @TempTableTicket TABLE ( TI_TICKET_ID            BIGINT                                                          
                                   , TI_VALIDATION_NUMBER    BIGINT                                                          
                                   , TI_TRANSACTION_ID       BIGINT                                                          
                                   )  

    -- PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creación nuevo estado

    IF (@pTimeToCheckDuplicityInCashout > 0)
    BEGIN
      -- FJC PBI 18258:TITO: We try to avoid "duplicated tickets"
      -- If there's an equality between ticket in DB and requested creation ticket with: (the same Amount, terminalId and is been created in less than N seconds) and this .
      -- Probably is a duplicated ticket.
      INSERT INTO @TempTableTicket    
      SELECT   TI_TICKET_ID                                                                                  
             , TI_VALIDATION_NUMBER                                                                          
             , TI_TRANSACTION_ID                                                                             
        FROM   TICKETS                                                                                       
       WHERE   TI_STATUS = @pTicketStatusCheck                                                               
         AND   TI_CREATED_TERMINAL_ID = @pCreatedTerminalId                                                  
         AND   TI_AMOUNT = @pTicketAmount                                                                    
         AND   DATEDIFF(SECOND, TI_CREATED_DATETIME, GETDATE()) < @pTimeToCheckDuplicityInCashout
      -- FJC PBI 18258:TITO: We try to avoid "duplicated tickets"
    END

    IF NOT EXISTS ( SELECT   TI_TICKET_ID FROM @TempTableTicket)                                                           
    BEGIN

       INSERT   INTO TICKETS              
              ( TI_VALIDATION_NUMBER      
              , TI_AMOUNT                 
              , TI_STATUS                 
              , TI_TYPE_ID                
              , TI_CREATED_DATETIME       
              , TI_CREATED_TERMINAL_ID    
              , TI_CREATED_TERMINAL_TYPE  
              , TI_EXPIRATION_DATETIME    
              , TI_CREATED_ACCOUNT_ID     
              , TI_PROMOTION_ID           
              , TI_VALIDATION_TYPE        
              , TI_MACHINE_NUMBER         
              , TI_TRANSACTION_ID         
              , TI_CREATED_PLAY_SESSION_ID                                                                                      
              , TI_ACCOUNT_PROMOTION      
              , TI_AMT0                   
              , TI_CUR0                   
              )                           

       OUTPUT   INSERTED.TI_TICKET_ID     
              , INSERTED.TI_VALIDATION_NUMBER                                                                                   
              , INSERTED.TI_TRANSACTION_ID                                                                                      
         INTO   @TempTableTicket          
       VALUES ( @pTicketValidationNumber  
              , @pTicketAmount            
              , @pTicketStatus            
              , @pTicketType              
              , @pTicketCreatedDateTime   
              , @pCreatedTerminalId       
              , @pTerminalType            
              , @pTicketExpirationDateTime                                                                                      
              , @pTicketCreatedAccountID  
              , @pPromotionId             
              , @pValidationType          
              , @pMachineTicketNumber     
              , @pTransactionId           
              , @pCreatedPlaySessionId    
              , @pAccountPromotion        
              , @pAmt0                    
              , @pCur0)                   

    END                               

    SELECT   @pTicketId = TI_TICKET_ID                                                                                       
           , @pValidationNumberOutput = TI_VALIDATION_NUMBER                                                                 
           , @pTransactionIdOutput = ISNULL(TI_TRANSACTION_ID, 0)                                                            
      FROM   @TempTableTicket          
  END
  ELSE        --  IF @pPendingPrintEnabled = 1 
  BEGIN
       INSERT   INTO TICKETS                  
              ( TI_VALIDATION_NUMBER          
              , TI_AMOUNT                     
              , TI_STATUS                     
              , TI_TYPE_ID                    
              , TI_CREATED_DATETIME           
              , TI_CREATED_TERMINAL_ID        
              , TI_CREATED_TERMINAL_TYPE      
              , TI_EXPIRATION_DATETIME        
              , TI_CREATED_ACCOUNT_ID         
              , TI_PROMOTION_ID               
              , TI_VALIDATION_TYPE            
              , TI_MACHINE_NUMBER             
              , TI_TRANSACTION_ID             
              , TI_CREATED_PLAY_SESSION_ID    
              , TI_ACCOUNT_PROMOTION          
              , TI_AMT0                       
              , TI_CUR0                       
              )                               
       VALUES ( @pTicketValidationNumber      
              , @pTicketAmount                
              , @pTicketStatus                
              , @pTicketType                  
              , @pTicketCreatedDateTime       
              , @pCreatedTerminalId           
              , @pTerminalType                
              , @pTicketExpirationDateTime    
              , @pTicketCreatedAccountID      
              , @pPromotionId                 
              , @pValidationType              
              , @pMachineTicketNumber         
              , @pTransactionId               
              , @pCreatedPlaySessionId        
              , @pAccountPromotion            
              , @pAmt0                        
              , @pCur0)                       

    SET   @pTicketId = SCOPE_IDENTITY() 
  END

END --- TITO_CreateTicket
GO

GRANT EXECUTE ON [TITO_CreateTicket] TO wggui WITH GRANT OPTION
GO

