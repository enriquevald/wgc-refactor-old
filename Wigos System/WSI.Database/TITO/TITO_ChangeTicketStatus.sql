/****** Object:  StoredProcedure [dbo].[TITO_ChangeTicketStatus]    Script Date: 01/21/2014 11:07:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_ChangeTicketStatus]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_ChangeTicketStatus]
GO

CREATE PROCEDURE [dbo].[TITO_ChangeTicketStatus] 
       @pTerminalId                   INT,
       @pTicketValidationNumber       BIGINT,
       @pTicketNewStatus              INT,
       @MsgSequenceId                 BIGINT,
       @pDefaultAllowRedemption       BIT,
       @pDefaultMaxAllowedTicketIn    MONEY,
       @pDefaultMinAllowedTicketIn    MONEY,
       @pTicketRejectReasonEgm        BIGINT,
       @pTicketRejectReasonWcp        BIGINT,
       --- OUTPUT
       @pTicketId                     BIGINT    OUTPUT,
       @pTicketType                   INT       OUTPUT,
       @pTicketAmount                 MONEY     OUTPUT,
       @pTicketExpiration             DATETIME  OUTPUT

AS
BEGIN
       IF @pTicketNewStatus = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
       BEGIN
           exec TITO_SetTicketPendingCancel   @pTerminalId, @pTicketValidationNumber, @pDefaultAllowRedemption, @pDefaultMaxAllowedTicketIn, @pDefaultMinAllowedTicketIn, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp
                                            , @pTicketId output, @pTicketType output, @pTicketAmount output, @pTicketExpiration output 
           RETURN
       END

       IF @pTicketNewStatus = 0 -- TITO_TICKET_STATUS.VALID
       BEGIN
           exec TITO_SetTicketValid @pTerminalId, @pTicketValidationNumber, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp, @pTicketId output 
           
           RETURN
       END
       
       IF @pTicketNewStatus = 1 -- TITO_TICKET_STATUS.CANCELED
       BEGIN       
           exec TITO_SetTicketCanceled @pTerminalId, @pTicketValidationNumber, @MsgSequenceId, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp, @pTicketId output, @pTicketAmount output 
           
           RETURN
       END   
END
GO

 