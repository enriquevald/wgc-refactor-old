--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_SetTicketPendingCancel.sql
--
--   DESCRIPTION: Update the valid tickets to pending cancellation. ApplyExchange2 Function
--
--        AUTHOR: Andreu Juli�
--
-- CREATION DATE: 14-JAN-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 14-JAN-2014 JPJ      First release (Include the script in the project).
-- 05-MAR-2014 DDM/JPJ  Added restrictions by Terminal and by provider
-- 13-JAN-2016 MPO/JMV  Added floor dual currency support. Including ApplyExchange2 function.
-- 14-OCT-2016 JBP      PBI 18246:eBox - Ticket Amount Not Multiple of Machine Denomination: L�gica WCP
-- 20-OCT-2016 XGJ      PBI 18259:TITO: Nuevo estado "Pending Print" en tickets - Mostrar nuevo estado en pantallas (GUI y Cashier)
-- 27-OCT-2016 FAV      PBI 19597:eBox - Ticket Amount Not Multiple of Machine Denomination: REVIEW Sprint 31
-- 24-JUL-2017 FJC      Fixed Bug WIGOS-3915 Accounting - TITO tickets from the EGM terminal are associated with anonymous accounts when using players card.
--------------------------------------------------------------------------------

-- APPLYEXCHANGE2 
IF OBJECT_ID (N'dbo.ApplyExchange2', N'FN') IS NOT NULL
    DROP FUNCTION dbo.ApplyExchange2;                 
GO  

CREATE FUNCTION [dbo].[ApplyExchange2]
 ( @pAmount  MONEY,
   @pFromIsoCode VARCHAR(3),
   @pToIsoCode VARCHAR(3))
RETURNS MONEY
AS
BEGIN
  DECLARE @Exchanged   MONEY
  DECLARE @Change      MONEY
  DECLARE @NumDecimals INT
      
  IF(@pFromIsoCode = @pToIsoCode)
    SET @Exchanged = @pAmount
  ELSE
    BEGIN
    -- to national currency
    IF (@pToIsoCode = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'))
       BEGIN
    SELECT TOP 1 @NumDecimals = CE_NUM_DECIMALS FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pToIsoCode 
		SELECT TOP 1 @Change = CE_CHANGE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pFromIsoCode 
       END
    ELSE  -- to not national currency     
       BEGIN
		SELECT TOP 1 @Change = 1 / CE_CHANGE, @NumDecimals = CE_NUM_DECIMALS FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pToIsoCode 
       END    
    SET @NumDecimals = ISNULL(@NumDecimals, 0)     
    SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   
    END   
		 
    RETURN @Exchanged
END -- ApplyExchange2

GO

GRANT EXECUTE ON [dbo].[ApplyExchange2] TO [wggui] WITH GRANT OPTION

GO

-- TITO_SetTicketPendingCancel
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[TITO_SetTicketPendingCancel]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  @pDefaultMinAllowedTicketIn   MONEY,
  @pTicketRejectReasonEgm       BIGINT,
  @pTicketRejectReasonWcp       BIGINT,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE @_ticket_status_valid            INT
  DECLARE @_ticket_status_pending_printing INT
  DECLARE @_ticket_status_pending_cancel   INT
  DECLARE @_min_ticket_id                  BIGINT
  DECLARE @_max_ticket_id                  BIGINT
  DECLARE @_max_ticket_amount              MONEY
  DECLARE @_min_ticket_amount              MONEY
  DECLARE @_min_denom_amount               MONEY
  DECLARE @_redeem_allowed                 BIT
  DECLARE @_promotion_id                   BIGINT
  DECLARE @_restricted                     INT
  DECLARE @_prov_id                        INT
  DECLARE @_element_type                   INT
  DECLARE @_ticket_type                    INT
  DECLARE @_only_redeemable                INT
  DECLARE @_virtual_account_id             BIGINT
  DECLARE @_player_account_id              BIGINT
  DECLARE @_last_action_account_id         BIGINT

  
  
  DECLARE @_amt0                           MONEY
  DECLARE @_cur0                           NVARCHAR(3)
  DECLARE @_sys_amt                        MONEY
  DECLARE @_egm_amt                        MONEY
  DECLARE @_egm_iso                        NVARCHAR(3)   
  DECLARE @_dual_currency_enabled          BIT
  DECLARE @_sys_iso_national_cur           AS NVARCHAR(3)   
  DECLARE @_allow_to_play_on_egm           BIT
  DECLARE @_allow_to_pay_pending_printing  BIT  
  DECLARE @_terminal_allow_truncate        BIT
  DECLARE @_gp_allow_truncate              BIT
  DECLARE @_is_truncate                    BIT
  DECLARE @_gp_min_denom_amount            MONEY
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT @_allow_to_play_on_egm = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM'
  SET @_allow_to_play_on_egm = ISNULL(@_allow_to_play_on_egm,0)
  
  SELECT @_allow_to_pay_pending_printing = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM'
  SET @_allow_to_pay_pending_printing = ISNULL(@_allow_to_pay_pending_printing,0)
     
  --SELECT   @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
  --       , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
  --       , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
  --       , @_min_denom_amount         = TE_MIN_DENOMINATION
  --       , @_terminal_allow_truncate  = TE_ALLOW_TRUNCATE
  --       , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
  --       , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
  --  FROM  TERMINALS                     
  -- WHERE  TE_TERMINAL_ID = @pTerminalId 

  --Get AccountID (From indentified Player or not)
  SELECT TOP 1  @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
              , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
              , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
              , @_min_denom_amount         = ISNULL(TE_MIN_DENOMINATION, 0)
              , @_terminal_allow_truncate  = ISNULL(TE_ALLOW_TRUNCATE, 0)
              , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
              , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
              , @_player_account_id        = ISNULL(PS_ACCOUNT_ID, 0)                                                                       
        FROM    TERMINALS   
   LEFT JOIN    PLAY_SESSIONS      
          ON    PS_TERMINAL_ID  = TE_TERMINAL_ID 
         AND    PS_STATUS       = 0               --PlaySession OPENED     
       WHERE    TE_TERMINAL_ID  = @pTerminalId                                                
    ORDER BY    PS_PLAY_SESSION_ID  DESC 
      
   SET @_last_action_account_id = @_virtual_account_id --Without Card (VirtualAccountId) "anonymous"

   IF (@_player_account_id) > 0 
   BEGIN
     SET @_last_action_account_id = @_player_account_id --Player identified
   END


   SET @_egm_amt = NULL
   SET @_egm_iso = NULL

   SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
   SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)   
   
   IF (@_dual_currency_enabled = 1)
   BEGIN
     SET @_sys_iso_national_cur = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
     SET @_egm_iso  = (SELECT ISNULL(TE_ISO_CODE, @_sys_iso_national_cur) FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId)
     -- Convert max allowed ticket In when terminal iso code is different National Currency
     IF (@_max_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_max_ticket_amount = dbo.ApplyExchange2(@_max_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
     IF (@_min_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_min_ticket_amount = dbo.ApplyExchange2(@_min_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
   END

   SET @_max_ticket_amount = ISNULL(@_max_ticket_amount, @pDefaultMaxAllowedTicketIn)   
   SET @_min_ticket_amount = ISNULL(@_min_ticket_amount, @pDefaultMinAllowedTicketIn)   

  IF   @_max_ticket_amount IS NULL 
    OR @_min_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
  set @_ticket_status_pending_printing = 6

  if @_allow_to_pay_pending_printing = 0 begin
	-- Not allow to pay tickets pending priting
	set @_ticket_status_pending_printing = -9
  end

	  SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
			 , @_max_ticket_id = MAX (TI_TICKET_ID)
			 , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
			 , @_ticket_type   = MAX (TI_TYPE_ID)
			 , @_amt0          = ISNULL (MAX(TI_AMT0),MAX(TI_AMOUNT))
			 , @_cur0          = MAX(TI_CUR0)
			 , @_sys_amt       = MAX(TI_AMOUNT)
		FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
		 AND   1 = CASE 
					 WHEN @_allow_to_play_on_egm = 0 AND (TI_STATUS = @_ticket_status_valid or ti_status = @_ticket_status_pending_printing) THEN 1
					 WHEN @_allow_to_play_on_egm = 1 AND (TI_STATUS = @_ticket_status_valid OR TI_STATUS = @_ticket_status_pending_cancel or ti_status = @_ticket_status_pending_printing) THEN 1
				   ELSE 0 END
		 AND   TI_AMOUNT  > 0
		 AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		 AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
     AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )

 IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN

    
    IF (@_dual_currency_enabled = 1) 
    BEGIN  
        SET    @_cur0     =  ISNULL(@_cur0,@_sys_iso_national_cur)                      
        IF  @_egm_iso = @_cur0 
            SET @_egm_amt = @_amt0
        ELSE
            SET @_egm_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_egm_iso)
                      
        SET @_sys_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_sys_iso_national_cur)
    END
      
    -- One ticket found, change its status
		UPDATE    TICKETS 
		   SET    TI_STATUS                    = @_ticket_status_pending_cancel 
				, TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
				, TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
				, TI_LAST_ACTION_DATETIME      = GETDATE() 
				, TI_AMOUNT                    = @_sys_amt
				, TI_AMT1                      = @_egm_amt
				, TI_CUR1                      = @_egm_iso 
				, TI_LAST_ACTION_ACCOUNT_ID    = @_last_action_account_id 
        , TI_REJECT_REASON_EGM         = @pTicketRejectReasonEgm
        , TI_REJECT_REASON_WCP         = @pTicketRejectReasonWcp
		   WHERE  TI_TICKET_ID                 = @_min_ticket_id 
			AND   TI_AMOUNT  > 0
			AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
			AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
      AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )
			AND   (TI_STATUS = @_ticket_status_valid or TI_STATUS = @_ticket_status_pending_cancel or TI_STATUS = @_ticket_status_pending_printing)
			
			   IF @@ROWCOUNT = 1
				SELECT    @pTicketId         = TI_TICKET_ID
						, @pTicketType       = TI_TYPE_ID
						, @pTicketAmount     = CASE WHEN @_dual_currency_enabled = 1 THEN TI_AMT1
                ELSE TI_AMOUNT
                END
					, @pTicketExpiration = TI_EXPIRATION_DATETIME
					FROM    TICKETS 
				WHERE    TI_TICKET_ID  = @_min_ticket_id 


      -- Alow truncate
      SET @_gp_allow_truncate       = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
      SET @_terminal_allow_truncate = ISNULL(@_terminal_allow_truncate, @_gp_allow_truncate)
      SET @_is_truncate             = 0

      -- Min denomination
      SET @_gp_min_denom_amount     = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinDenominationMultiple')
      SET @_min_denom_amount        = ISNULL(@_min_denom_amount, @_gp_min_denom_amount)

      if (@_terminal_allow_truncate = 0 AND @_min_denom_amount > 0)
      BEGIN
		      SET @_is_truncate = @pTicketAmount % @_min_denom_amount
      END

      IF    @pTicketAmount < @_min_denom_amount                     -- TicketAmount is lower than Machine Min Denomination
        OR (@_terminal_allow_truncate = 0 AND @_is_truncate > 0)    -- Is Truncate and Not Alow Truncate
	    BEGIN

	      SET @pTicketId         = NULL
	      SET @pTicketType       = NULL
	      SET @pTicketAmount     = NULL
	      SET @pTicketExpiration = NULL

      END

  END

END

GO