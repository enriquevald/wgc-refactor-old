----------------------------------------------------------------------------------------------------------------
--GET BASIC TITO SUMMARY
--
--Version   Date            User      Description
----------------------------------------------------------------------------------------------------------------
--1.0.0     11-APR-2017     RAB       Initial procedure
----------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTitoSummary]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetTitoSummary]
GO

CREATE PROCEDURE [dbo].[GetTitoSummary]
         @pHour             INT
        ,@pCashable         INT
        ,@pPromoRe          INT   
		    ,@pPromoNR          INT   
		    ,@pRedeemed         INT 
		    ,@pPlayed           INT 
		    ,@pHandpay          INT 
		    ,@pJackpot          INT
        ,@pChipsDealerCopy  INT
		    ,@pDateFrom         DATETIME
		    ,@pDateTo           DATETIME
AS
BEGIN 
	      DECLARE @pStatusDiscared INT
	      DECLARE @pStatusCanceled INT
	      DECLARE @pStatusRedeemed INT
	      SET @pStatusDiscared = 4
	      SET @pStatusCanceled = 1
	      SET @pStatusRedeemed = 2
BEGIN
	  SELECT   ISNULL(T1.TI_CREATED_DATETIME, T2.TI_LAST_ACTION_DATETIME)     
             , T2.TI_CASHABLE_QUANTIY     
             , T2.TI_CASHABLE_AMOUNT     
             , T2.TI_PROMO_RE_QUANTITY      
             , T2.TI_PROMO_RE_AMOUNT      
             , T2.TI_PROMO_NR_QUANTITY      
             , T2.TI_PROMO_NR_AMOUNT      
             , T2.TOTAL_TICKET_IN      
             , T1.TO_CASHABLE_QUANTIY      
             , T1.TO_CASHABLE_AMOUNT      
             , T1.TO_PROMO_NR_QUANTITY      
             , T1.TO_PROMO_NR_AMOUNT      
             , T1.TOTAL_TICKET_OUT      
             , T2.PAID_QUANTITY      
             , T2.PAID_AMOUNT      
             , T1.GENERATED_QUANTITY      
             , T1.GENERATED_AMOUNT      
        FROM (      
              SELECT   TI_CREATED_DATETIME      
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN 1 END) AS  TO_CASHABLE_QUANTIY              
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN TI_AMOUNT END) AS  TO_CASHABLE_AMOUNT       
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID = @pPromoNR )  THEN 1 END) AS  TO_PROMO_NR_QUANTITY             
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID = @pPromoNR )  THEN TI_AMOUNT END) AS  TO_PROMO_NR_AMOUNT       
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pPromoNR, @pHandpay, @pJackpot))  THEN TI_AMOUNT END) AS  TOTAL_TICKET_OUT      
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE IN (0, 2)) THEN 1 END) AS  GENERATED_QUANTITY             
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE IN (0, 2)) THEN TI_AMOUNT END) AS  GENERATED_AMOUNT       
              FROM  (SELECT  CASE WHEN (DATEPART(HOUR, TI_CREATED_DATETIME) < @pHour)            
                                    THEN DATEADD (DD, -1, DATEDIFF(DD, 0, TI_CREATED_DATETIME))
                                    ELSE DATEADD (DD,  0, DATEDIFF(DD, 0, TI_CREATED_DATETIME)) END AS  TI_CREATED_DATETIME      
                               , TI_CREATED_TERMINAL_TYPE          
                               , TI_LAST_ACTION_TERMINAL_TYPE      
                               , TI_AMOUNT       
                               , TI_TYPE_ID      
                       FROM TICKETS         
              WHERE   TI_STATUS <> @pStatusDiscared AND (TI_CREATED_DATETIME >= CAST(@pDateFrom AS DATETIME)) AND (TI_CREATED_DATETIME < CAST(@pDateTo AS DATETIME)) AND TI_TYPE_ID <> @pChipsDealerCopy) AS TMP_TABLE
              GROUP BY   TI_CREATED_DATETIME) AS T1      
     FULL OUTER JOIN (      
              SELECT   TI_LAST_ACTION_DATETIME      
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN 1 END) AS  TI_CASHABLE_QUANTIY              
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN TI_AMOUNT END) AS  TI_CASHABLE_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoRe )  THEN 1 END) AS  TI_PROMO_RE_QUANTITY             
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoRe )  THEN TI_AMOUNT END) AS  TI_PROMO_RE_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoNR )  THEN 1 END) AS  TI_PROMO_NR_QUANTITY            
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoNR )  THEN TI_AMOUNT END) AS  TI_PROMO_NR_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pPromoRe, @pPromoNR))  THEN TI_AMOUNT END) AS  TOTAL_TICKET_IN      
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE IN (0, 2) AND TI_STATUS = @pRedeemed AND TI_TYPE_ID != @pPromoNR)  THEN 1 END) AS  PAID_QUANTITY            
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE IN (0, 2) AND TI_STATUS = @pRedeemed AND TI_TYPE_ID != @pPromoNR)  THEN TI_AMOUNT END) AS  PAID_AMOUNT      
              FROM   (SELECT   CASE WHEN  (DATEPART(HOUR, TI_LAST_ACTION_DATETIME) < @pHour)            
                                    THEN   DATEADD (DD, -1, DATEDIFF(DD, 0, TI_LAST_ACTION_DATETIME))      
                                    ELSE DATEADD(DD,  0, DATEDIFF(DD, 0, TI_LAST_ACTION_DATETIME)) END AS  TI_LAST_ACTION_DATETIME      
                               , TI_CREATED_TERMINAL_TYPE          
                               , TI_LAST_ACTION_TERMINAL_TYPE      
                               , TI_AMOUNT       
                               , TI_TYPE_ID      
                               , TI_STATUS       
                          FROM    TICKETS         
              WHERE   TI_STATUS IN (@pStatusCanceled, @pStatusRedeemed) AND (TI_LAST_ACTION_DATETIME >= CAST(@pDateFrom AS DATETIME)) AND (TI_LAST_ACTION_DATETIME < CAST(@pDateTo AS DATETIME)) AND TI_TYPE_ID <> @pChipsDealerCopy) AS TMP_TABLE      
              GROUP BY   TI_LAST_ACTION_DATETIME) AS T2 ON  T1.TI_CREATED_DATETIME = T2.TI_LAST_ACTION_DATETIME      
     ORDER BY   ISNULL(T1.TI_CREATED_DATETIME, T2.TI_LAST_ACTION_DATETIME)  			
	END
END --END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetTitoSummary] TO [wggui] WITH GRANT OPTION
GO