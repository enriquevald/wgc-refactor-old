--------------------------------------------------------------------------------
-- Copyright � 2011 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: PD_PopulateData.sql
-- 
--   DESCRIPTION: Populate the necessary data to run iPad demo.
-- 
--        AUTHOR: Ra�l Cervera
-- 
-- CREATION DATE: 06-APR-2011
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-APR-2011 RCI    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Populate the necessary data.
-- 
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_PopulateData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_PopulateData]
GO
CREATE PROCEDURE dbo.PD_PopulateData
AS
BEGIN
  DECLARE @upper             int
  DECLARE @lower             int
  DECLARE @idx_provider      int
  DECLARE @row_provider      nvarchar(50)
  DECLARE @row_num_terminals int
  DECLARE @period_in_months  int
  DECLARE @from_date         datetime
  DECLARE @to_date           datetime
  DECLARE @site_id           nvarchar(4)
  DECLARE @base_track_data   nvarchar(50)
  DECLARE @num_accounts      int
  DECLARE @tbl_providers     TABLE ( PROVIDER      nvarchar(50),
                                     NUM_TERMINALS int )
  DECLARE @num_active_terminals int

  -- Number of months to populate data: from now to @period_in_months months ago.
  SET @period_in_months = 18

  -- SiteId: Format to '0000'.
  SET @site_id = '1234'

  -- Base track data
  SET @base_track_data = @site_id + '878700000'
  -- Number of accounts to use to play.
  SET @num_accounts = 1000

  -- Create accounts to use to play.
  EXEC dbo.PD_CreateAccounts @base_track_data, @num_accounts

  -- Trunc date to start of the day (00:00h).
  SET @to_date   = DATEADD(DAY, DATEDIFF(day, 0, GETDATE()), 0)
  SET @from_date = DATEADD(MONTH, -@period_in_months, @to_date)

  -- Range for random the number of terminals per provider.
  SET @lower = 50
  SET @upper = 100

  -- The providers to insert.
  INSERT INTO @tbl_providers VALUES ( 'Atronic', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'BCM', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'Cadillac Jack', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'DigiDeal', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'EIBE', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'Metronia', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'Novomatic', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'Williams', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'WSI', ROUND((@upper - @lower) * RAND() + @lower, 0) )
  INSERT INTO @tbl_providers VALUES ( 'ZITRO', ROUND((@upper - @lower) * RAND() + @lower, 0) )

  SELECT   @num_active_terminals = SUM(NUM_TERMINALS)
    FROM   @tbl_providers

  DECLARE providers_cursor CURSOR FOR
    SELECT   PROVIDER
           , NUM_TERMINALS
      FROM   @tbl_providers
    ORDER BY PROVIDER
    
  OPEN providers_cursor
  FETCH NEXT FROM providers_cursor INTO @row_provider, @row_num_terminals

  SET @idx_provider = 1
  
  WHILE @@Fetch_Status = 0
  BEGIN
    -- Create the terminals and all data related.
    EXEC dbo.PD_CreateDataForProvider @row_provider, @idx_provider, @row_num_terminals, @num_active_terminals,
                                      @from_date, @to_date
    
    FETCH NEXT FROM providers_cursor INTO @row_provider, @row_num_terminals

    SET @idx_provider = @idx_provider + 1
  END
  
  CLOSE providers_cursor
  DEALLOCATE providers_cursor

END -- PD_PopulateData
