--------------------------------------------------------------------------------
-- Copyright � 2011 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: PD_TerminalsFunc.sql
-- 
--   DESCRIPTION: Define all functions related to Terminals.
-- 
--        AUTHOR: Ra�l Cervera
-- 
-- CREATION DATE: 06-APR-2011
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-APR-2011 RCI    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert the game for the Population Data and return its Id and Name.
-- 
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--        @GameId   int
--        @GameName nvarchar(50)
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_InsertGame]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_InsertGame]
GO
CREATE PROCEDURE dbo.PD_InsertGame
  (@GameId   int OUTPUT,
   @GameName nvarchar(50) OUTPUT)
AS
BEGIN
  SET @GameName = 'PD_GAME'

  SELECT   @GameId = GM_GAME_ID
    FROM   GAMES
   WHERE   GM_NAME = @GameName

  IF @GameId IS NULL
  BEGIN
    INSERT INTO GAMES ( GM_NAME
                      )
               VALUES ( @GameName
                      )
    SET @GameId = SCOPE_IDENTITY()
  END
END -- PD_InsertGame

GO

--------------------------------------------------------------------------------
-- PURPOSE: Create accounts to use to play.
-- 
--  PARAMS:
--      - INPUT:
--        @BaseTrackData nvarchar(50)
--        @NumAccounts   int
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_CreateAccounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_CreateAccounts]
GO
CREATE PROCEDURE dbo.PD_CreateAccounts
  (@BaseTrackData nvarchar(50),
   @NumAccounts   int)
AS
BEGIN
  DECLARE @idx_account int
  DECLARE @holder_name nvarchar(50)
  DECLARE @track_data  nvarchar(50)

  BEGIN TRAN

  DELETE   ACCOUNTS
   WHERE   AC_HOLDER_NAME LIKE 'PD_ACCOUNT_%'

  SET @idx_account = 0

  WHILE @idx_account < @NumAccounts
  BEGIN

    SET @holder_name = 'PD_ACCOUNT_' + RIGHT('0000' + CAST(@idx_account AS nvarchar), 4)
    SET @track_data  = RIGHT('0000000000000' + CAST(CAST(@BaseTrackData AS bigint) + @idx_account AS nvarchar), 13)

    INSERT INTO ACCOUNTS ( AC_TYPE
                         , AC_HOLDER_NAME
                         , AC_BLOCKED
                         , AC_TRACK_DATA
                         , AC_BALANCE
                         , AC_INITIAL_CASH_IN
                         )
                  VALUES ( 2
                         , @holder_name
                         , 0
                         , @track_data
                         , 1000000
                         , 1000000
                         )

    SET @idx_account = @idx_account + 1
  END

  COMMIT TRAN

END -- PD_CreateAccounts

GO

--------------------------------------------------------------------------------
-- PURPOSE: Get the MIN and MAX account_id from the PD accounts.
-- 
--  PARAMS:
--      - INPUT:
--
--      - OUTPUT:
--        @MinAccountId int
--        @MaxAccountId int
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_GetAccountIdRange]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_GetAccountIdRange]
GO
CREATE PROCEDURE dbo.PD_GetAccountIdRange
  (@MinAccountId bigint OUTPUT,
   @MaxAccountId bigint OUTPUT)
AS
BEGIN
  SELECT   @MinAccountId  = MIN(AC_ACCOUNT_ID)
         , @MaxAccountId  = MAX(AC_ACCOUNT_ID)
    FROM   ACCOUNTS
   WHERE   AC_HOLDER_NAME LIKE 'PD_ACCOUNT_%'

END -- PD_GetAccountIdRange

GO

--------------------------------------------------------------------------------
-- PURPOSE: Get a valid and random PD AccountId.
-- 
--  PARAMS:
--      - INPUT:
--        @MinAccountId bigint
--        @MaxAccountId bigint
--
--      - OUTPUT:
--        @AccountId    bigint
--        @TrackData    nvarchar(50)
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_GetValidAccountId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_GetValidAccountId]
GO
CREATE PROCEDURE dbo.PD_GetValidAccountId
 (@MinAccountId bigint,
  @MaxAccountId bigint,
  @AccountId    bigint OUTPUT,
  @TrackData    nvarchar(50) OUTPUT)
AS
BEGIN
  DECLARE @idx_try int
  
  SET @idx_try = 0
  
  -- Try 10 times. If not success, return last one.
  WHILE @idx_try < 10
  BEGIN
    SET @AccountId = ROUND((@MaxAccountId - @MinAccountId) * RAND() + @MinAccountId, 0)

    SELECT   @TrackData = AC_TRACK_DATA
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID = @AccountId
       AND   AC_HOLDER_NAME LIKE 'PD_ACCOUNT_%'

    IF @TrackData IS NOT NULL
    BEGIN
      BREAK
    END

    SET @idx_try = @idx_try + 1
  END
END -- PD_GetValidAccountId

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert connected terminals.
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId int
--        @FromDate   datetime
--        @ToDate     datetime
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_GenerateTerminalConnected]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_GenerateTerminalConnected]
GO
CREATE PROCEDURE dbo.PD_GenerateTerminalConnected
 (@TerminalId int,
  @FromDate   datetime,
  @ToDate     datetime)
AS
BEGIN
  DECLARE @date datetime
  
  SET @date = @FromDate
  
  WHILE @date <= @ToDate
  BEGIN

    IF NOT EXISTS (
      SELECT   1
        FROM   TERMINALS_CONNECTED
       WHERE   TC_DATE        = @date
         AND   TC_TERMINAL_ID = @TerminalId
      )
    BEGIN
      INSERT INTO TERMINALS_CONNECTED ( TC_DATE
                                      , TC_TERMINAL_ID
                                      , TC_STATUS
                                      , TC_CONNECTED
                                      )
                               VALUES ( @date
                                      , @TerminalId
                                      , 0
                                      , 1
                                      )
    END
    ELSE
    BEGIN
      UPDATE   TERMINALS_CONNECTED
         SET   TC_STATUS      = 0
             , TC_CONNECTED   = 1
       WHERE   TC_DATE        = @date
         AND   TC_TERMINAL_ID = @TerminalId
    END
    
    SET @date = DATEADD(DAY, 1, @date)
  END
  
END -- PD_GenerateTerminalConnected

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert a play session.
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId    bigint
--        @TrackData    nvarchar(50)
--        @TerminalId   int
--        @StartDate    datetime
--        @FinishDate   datetime
--        @PlayedAmount money
--        @WonAmount    money
--        @PlayedCount  int
--        @WonCount     int
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_InsertPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_InsertPlaySession]
GO
CREATE PROCEDURE dbo.PD_InsertPlaySession
 (@AccountId    bigint,
  @TrackData    nvarchar(50),
  @TerminalId   int,
  @StartDate    datetime,
  @FinishDate   datetime,
  @PlayedAmount money,
  @WonAmount    money,
  @PlayedCount  int,
  @WonCount     int)
AS
BEGIN
  DECLARE @initial_balance money
  DECLARE @final_balance   money

  SET @initial_balance = @PlayedAmount + ROUND((1000 - 0) * RAND() + 0, 0)
  SET @final_balance   = @initial_balance - @PlayedAmount + @WonAmount

  INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID
                            , PS_TERMINAL_ID
                            , PS_TYPE
                            , PS_TYPE_DATA
                            , PS_STATUS
                            , PS_STARTED
                            , PS_INITIAL_BALANCE
                            , PS_PLAYED_COUNT
                            , PS_PLAYED_AMOUNT
                            , PS_WON_COUNT
                            , PS_WON_AMOUNT
                            , PS_FINISHED
                            , PS_FINAL_BALANCE
                            , PS_LOCKED
                            , PS_STAND_ALONE
                            , PS_PROMO)
                     VALUES ( @AccountId
                            , @TerminalId
                            , 2            -- 2 = TYPE WIN
                            , @TrackData
                            , 1            -- 1 = Closed
                            , @StartDate
                            , @initial_balance
                            , @PlayedCount
                            , @PlayedAmount
                            , @WonCount
                            , @WonAmount
                            , @FinishDate
                            , @final_balance
                            , NULL
                            , 1
                            , 0
                            )

END -- PD_InsertPlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert a sale.
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId         int
--        @TerminalName       nvarchar(50)
--        @NumActiveTerminals int
--        @GameId             int
--        @GameName           nvarchar(50)
--        @Date               datetime
--        @PlayedAmount       money
--        @WonAmount          money
--        @PlayedCount        int
--        @WonCount           int
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_InsertSalesPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_InsertSalesPerHour]
GO
CREATE PROCEDURE dbo.PD_InsertSalesPerHour
 (@TerminalId         int,
  @TerminalName       nvarchar(50),
  @NumActiveTerminals int,
  @GameId             int,
  @GameName           nvarchar(50),
  @Date               datetime,
  @PlayedAmount       money,
  @WonAmount          money,
  @PlayedCount        int,
  @WonCount           int)

AS
BEGIN
  DECLARE @base_hour DATETIME

  SET @base_hour = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Date), 0)

  IF NOT EXISTS (
    SELECT   1
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR     = @base_hour
       AND   SPH_TERMINAL_ID   = @TerminalId
       AND   SPH_GAME_ID       = @GameId
    )
  BEGIN
    INSERT INTO SALES_PER_HOUR ( SPH_BASE_HOUR
                               , SPH_TERMINAL_ID
                               , SPH_TERMINAL_NAME
                               , SPH_GAME_ID
                               , SPH_GAME_NAME
                               , SPH_PLAYED_COUNT
                               , SPH_PLAYED_AMOUNT
                               , SPH_WON_COUNT
                               , SPH_WON_AMOUNT
                               , SPH_NUM_ACTIVE_TERMINALS
                               , SPH_LAST_PLAY_ID)
                       VALUES  ( @base_hour
                               , @TerminalId
                               , @TerminalName
                               , @GameId
                               , @GameName
                               , @PlayedCount
                               , @PlayedAmount
                               , @WonCount
                               , @WonAmount
                               , @NumActiveTerminals
                               , 0
                               )
  END
  ELSE
  BEGIN
    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT  = SPH_PLAYED_COUNT  + @PlayedCount
           , SPH_PLAYED_AMOUNT = SPH_PLAYED_AMOUNT + @PlayedAmount
           , SPH_WON_COUNT     = SPH_WON_COUNT     + @WonCount
           , SPH_WON_AMOUNT    = SPH_WON_AMOUNT    + @WonAmount
     WHERE   SPH_BASE_HOUR     = @base_hour
       AND   SPH_TERMINAL_ID   = @TerminalId
       AND   SPH_GAME_ID       = @GameId
  END

END -- PD_InsertSalesPerHour

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert play sessions and sales statistics for a terminal.
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId         int
--        @TerminalName       nvarchar(50)
--        @NumActiveTerminals int
--        @FromDate           datetime
--        @ToDate             datetime
--        @MinAccountId       bigint
--        @MaxAccountId       bigint
--        @GameId             int
--        @GameName           nvarchar(50)
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_InsertSessionsAndSalesForTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_InsertSessionsAndSalesForTerminal]
GO
CREATE PROCEDURE dbo.PD_InsertSessionsAndSalesForTerminal
 (@TerminalId         int,
  @TerminalName       nvarchar(50),
  @NumActiveTerminals int,
  @FromDate           datetime,
  @ToDate             datetime,
  @MinAccountId       bigint,
  @MaxAccountId       bigint,
  @GameId             int,
  @GameName           nvarchar(50))
AS
BEGIN
  DECLARE @date          datetime
  DECLARE @upper         int
  DECLARE @lower         int
  DECLARE @num_sessions  int
  DECLARE @idx_session   int
  DECLARE @account_id    bigint
  DECLARE @track_data    nvarchar(50)
  DECLARE @played_amount money
  DECLARE @won_amount    money
  DECLARE @netwin_pct    float
  DECLARE @played_count  int
  DECLARE @won_count     int
  DECLARE @start_date    datetime
  DECLARE @finish_date   datetime
  DECLARE @minutes       int

  SET @lower = 50
  SET @upper = 100

  SET @date = @FromDate

  WHILE @date <= @ToDate
  BEGIN

    SET @start_date   = @date
    SET @finish_date  = @date

    SET @num_sessions = ROUND((@upper - @lower) * RAND() + @lower, 0)

    BEGIN TRAN

    SET @idx_session  = 0
    WHILE @idx_session < @num_sessions
    BEGIN
      EXEC dbo.PD_GetValidAccountId @MinAccountId, @MaxAccountId, @account_id OUTPUT, @track_data OUTPUT

      -- Calculate values for the fake play session:
      --  * PLAYED_AMOUNT = RAND(100, 4000). For precission: RAND(1000, 40000)/10
      --  * NETWIN_PCT    = RAND(-0.10, 0.20). For precission: RAND(-100, 200)/1000
      --  * WON_AMOUNT    = PLAYED_AMOUNT * (1 - NETWIN_PCT)
      SET @played_amount = ROUND((40000 - 1000) * RAND() + 1000, 0) / 10
      SET @netwin_pct    = ROUND((200 - (-100)) * RAND() + (-100), 0) / 1000
      SET @won_amount    = @played_amount * (1 - @netwin_pct)
      SET @played_count  = ROUND((50 - 25) * RAND() + 25, 0)
      SET @won_count     = ROUND((15 - 5) * RAND() + 5, 0)

      -- New session starts between 1 and 5 minutes later than last @finish_date.
      SET @minutes       = ROUND((5 - 1) * RAND() + 1, 0)
      SET @start_date    = DATEADD(MINUTE, @minutes, @finish_date)
      -- Duration of sessions is between 5 and 20 minutes.
      SET @minutes       = ROUND((20 - 5) * RAND() + 5, 0)
      SET @finish_date   = DATEADD(MINUTE, @minutes, @start_date)

      -- If day is over, finish this day. Start new day!      
      IF @finish_date >= DATEADD(DAY, 1, @date)
      BEGIN
        BREAK
      END

      EXEC dbo.PD_InsertPlaySession @account_id, @track_data, @TerminalId, @start_date, @finish_date,
                                    @played_amount, @won_amount, @played_count, @won_count

      EXEC dbo.PD_InsertSalesPerHour @TerminalId, @TerminalName, @NumActiveTerminals,
                                     @GameId, @GameName, @finish_date,
                                     @played_amount, @won_amount, @played_count, @won_count

      SET @idx_session = @idx_session + 1
    END

    COMMIT TRAN

    SET @date = DATEADD(DAY, 1, @date)
  END

END -- PD_InsertSessionsAndSalesForTerminal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert all necessary data for a terminal.
-- 
--  PARAMS:
--      - INPUT:
--        @ProviderId   nvarchar(50)
--        @IdxProv      int
--        @IdxTerm      int
--        @GameId       int
--
--      - OUTPUT:
--        @TerminalId   int
--        @TerminalName nvarchar(50)
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_CreateTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_CreateTerminal]
GO
CREATE PROCEDURE dbo.PD_CreateTerminal
 (@ProviderId   nvarchar(50),
  @IdxProv      int,
  @IdxTerm      int,
  @GameId       int,
  @TerminalId   int OUTPUT,
  @TerminalName nvarchar(50) OUTPUT)
AS
BEGIN
  DECLARE @term_id          int
  DECLARE @term_external_id nvarchar(40)

  SET @TerminalName     = 'PD_T' + RIGHT('00' + CAST(@IdxProv AS NVARCHAR), 2) + '-' + RIGHT('000' + CAST(@IdxTerm AS NVARCHAR), 3)
  SET @term_external_id = @TerminalName

  SELECT   @term_id       = TE_TERMINAL_ID
    FROM   TERMINALS
   WHERE   TE_NAME        = @TerminalName
     AND   TE_PROVIDER_ID = @ProviderId

  IF @term_id IS NULL
  BEGIN
    INSERT INTO TERMINALS ( TE_TYPE
                          , TE_NAME
                          , TE_EXTERNAL_ID
                          , TE_BLOCKED
                          , TE_ACTIVE
                          , TE_PROVIDER_ID
                          , TE_TERMINAL_TYPE
                          , TE_VENDOR_ID
                          , TE_STATUS
                          )
                   VALUES ( 1
                          , @TerminalName
                          , @term_external_id
                          , 0
                          , 1
                          , @ProviderId
                          , 5                 -- SAS-HOST
                          , @ProviderId
                          , 2                 -- RETIRED
                          )
    SET @term_id = SCOPE_IDENTITY()
  END
  ELSE
  BEGIN
    UPDATE   TERMINALS
       SET   TE_TYPE          = 1
           , TE_EXTERNAL_ID   = @term_external_id
           , TE_BLOCKED       = 0
           , TE_ACTIVE        = 1
           , TE_TERMINAL_TYPE = 5
           , TE_VENDOR_ID     = @ProviderId
           , TE_STATUS        = 2
     WHERE   TE_TERMINAL_ID   = @term_id

    -- Delete all previous play_sessions.
    DELETE   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID   = @term_id

    -- Delete all previous sales_per_hour.
    DELETE   SALES_PER_HOUR
     WHERE   SPH_TERMINAL_ID  = @term_id
  END

  IF NOT EXISTS (
    SELECT   1
      FROM   TERMINAL_GAME_TRANSLATION
     WHERE   TGT_TERMINAL_ID    = @term_id
       AND   TGT_SOURCE_GAME_ID = @GameId
    )
  BEGIN
    INSERT INTO TERMINAL_GAME_TRANSLATION ( TGT_TERMINAL_ID
                                          , TGT_SOURCE_GAME_ID
                                          , TGT_TARGET_GAME_ID
                                          )
                                   VALUES ( @term_id
                                          , @GameId
                                          , @GameId
                                          )
  END
  ELSE
  BEGIN
    UPDATE   TERMINAL_GAME_TRANSLATION
       SET   TGT_TARGET_GAME_ID = @GameId
     WHERE   TGT_TERMINAL_ID    = @term_id
       AND   TGT_SOURCE_GAME_ID = @GameId
  END

  SET @TerminalId = @term_id

END -- PD_CreateTerminal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Insert all necessary data for a provider.
-- 
--  PARAMS:
--      - INPUT:
--        @ProviderId         nvarchar(50)
--        @IdxProv            int
--        @NumTerminals       int
--        @NumActiveTerminals int
--        @FromDate           datetime
--        @ToDate             datetime
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PD_CreateDataForProvider]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PD_CreateDataForProvider]
GO
CREATE PROCEDURE dbo.PD_CreateDataForProvider
 (@ProviderId         nvarchar(50),
  @IdxProv            int,
  @NumTerminals       int,
  @NumActiveTerminals int,
  @FromDate           datetime,
  @ToDate             datetime)
AS
BEGIN
  DECLARE @idx_term       int
  DECLARE @term_id        int
  DECLARE @term_name      nvarchar(50)
  DECLARE @game_id        int
  DECLARE @game_name      nvarchar(50)
  DECLARE @min_account_id bigint
  DECLARE @max_account_id bigint

  -- Insert (if not exists) and obtain the GameId and GameName for the 'Population Data' Game.
  EXEC dbo.PD_InsertGame @game_id OUTPUT, @game_name OUTPUT

  EXEC dbo.PD_GetAccountIdRange @min_account_id OUTPUT, @max_account_id OUTPUT

  SET @idx_term = 1
  WHILE @idx_term <= @NumTerminals
  BEGIN
  
    EXEC dbo.PD_CreateTerminal @ProviderId, @IdxProv, @idx_term, @game_id, @term_id OUTPUT, @term_name OUTPUT

    IF @term_id > 0
    BEGIN

      -- Create the connected terminal.
      BEGIN TRAN
      EXEC dbo.PD_GenerateTerminalConnected @term_id, @FromDate, @ToDate
      COMMIT TRAN

      -- Insert play sessions and sales for a terminal.
      EXEC dbo.PD_InsertSessionsAndSalesForTerminal @term_id, @term_name, @NumActiveTerminals, @FromDate, @ToDate,
                                                    @min_account_id, @max_account_id, @game_id, @game_name
    END

    SET @idx_term = @idx_term + 1
  END

END -- PD_CreateDataForProvider
