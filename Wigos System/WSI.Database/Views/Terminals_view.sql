IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
			WHERE TABLE_NAME = 'terminals_view')
   DROP VIEW terminals_view
   
GO

/****** Object:  View [dbo].[terminals_view]    Script Date: 01/21/2016 14:55:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[terminals_view]    Script Date: 05/12/2009 16:42:03 ******/
CREATE VIEW [dbo].[terminals_view]
AS
SELECT     dbo.terminals.te_terminal_id, dbo.terminals.te_type, dbo.terminals.te_server_id, dbo.terminals.te_name, dbo.terminals.te_external_id, dbo.terminals.te_blocked, 
                      dbo.terminals.te_timestamp, dbo.terminals.te_active, dbo.terminals.te_provider_id, dbo.terminals.te_client_id, dbo.terminals.te_build_id, 
                      dbo.play_sessions.ps_play_session_id, dbo.play_sessions.ps_account_id, dbo.play_sessions.ps_terminal_id, dbo.play_sessions.ps_type, 
                      dbo.play_sessions.ps_type_data, dbo.play_sessions.ps_status, dbo.play_sessions.ps_started, dbo.play_sessions.ps_initial_balance, 
                      dbo.play_sessions.ps_played_count, dbo.play_sessions.ps_played_amount, dbo.play_sessions.ps_won_count, dbo.play_sessions.ps_won_amount, 
                      dbo.play_sessions.ps_cash_in, dbo.play_sessions.ps_cash_out, dbo.play_sessions.ps_finished, dbo.play_sessions.ps_final_balance, 
                      dbo.play_sessions.ps_timestamp, dbo.play_sessions.ps_locked, dbo.accounts.ac_account_id, dbo.accounts.ac_type, dbo.accounts.ac_holder_name, 
                      dbo.accounts.ac_blocked, dbo.accounts.ac_not_valid_before, dbo.accounts.ac_not_valid_after, dbo.accounts.ac_balance, dbo.accounts.ac_cash_in, 
                      dbo.accounts.ac_cash_won, dbo.accounts.ac_not_redeemable, dbo.accounts.ac_timestamp, dbo.accounts.ac_track_data, dbo.accounts.ac_total_cash_in, 
                      dbo.accounts.ac_total_cash_out, dbo.accounts.ac_initial_cash_in, dbo.accounts.ac_activated, dbo.accounts.ac_deposit, dbo.accounts.ac_current_terminal_id, 
                      dbo.accounts.ac_current_terminal_name, dbo.accounts.ac_current_play_session_id, dbo.accounts.ac_last_terminal_id, dbo.accounts.ac_last_terminal_name, 
                      dbo.accounts.ac_last_play_session_id, dbo.accounts.ac_user_type, ISNULL(CBU_VALUE, 0) AS ac_points, 
                      dbo.accounts.ac_initial_not_redeemable, dbo.play_sessions.ps_stand_alone, dbo.terminals.te_iso_code
FROM         dbo.accounts RIGHT OUTER JOIN
                      dbo.terminals_last_play_session INNER JOIN
                      dbo.play_sessions ON dbo.terminals_last_play_session.lp_play_session_id = dbo.play_sessions.ps_play_session_id ON 
                      dbo.accounts.ac_account_id = dbo.play_sessions.ps_account_id AND 
                      dbo.accounts.ac_current_play_session_id = dbo.play_sessions.ps_play_session_id RIGHT OUTER JOIN
                      dbo.terminals ON dbo.terminals_last_play_session.lp_terminal_id = dbo.terminals.te_terminal_id
                      LEFT JOIN CUSTOMER_BUCKET ON CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND CBU_BUCKET_ID = 1 -- Puntos de canje
GO


