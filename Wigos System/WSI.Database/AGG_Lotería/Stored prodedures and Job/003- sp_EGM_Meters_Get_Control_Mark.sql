/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Get_Control_Mark'.
Gets datetime value stored in table egm_control_mark, protecting returned value of invalid values.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_datetime DATETIME OUTPUT
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Get_Control_Mark]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Get_Control_Mark]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Get_Control_Mark] 
(
	@param_datetime DATETIME OUTPUT
)
AS
BEGIN TRY
	SET NOCOUNT ON;
	SELECT TOP (1) @param_datetime = ISNULL(ecm_control_mark, GETDATE()) FROM egm_control_mark;
	IF @param_datetime IS NULL
		BEGIN
			SET @param_datetime = ISNULL(@param_datetime, GETDATE())
		END
	END TRY
BEGIN CATCH
	EXEC dbo.spErrorHandling
END CATCH
GO