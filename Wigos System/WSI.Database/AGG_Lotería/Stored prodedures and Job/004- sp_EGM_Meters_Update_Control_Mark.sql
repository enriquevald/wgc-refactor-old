/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Update_Control_Mark'.
Updates value of egm_control_mark table, 
this procedure doesn't use an UPDATE statement to avoid multiple rows in table egm_control_mark, instead of that
it performs this operation in two steps, delete all rows ( avoiding manual insertions not controlled 
by the job (jb_Egm_Meters_Reporting_wgdb_xxx) and then insert updated control_mark witch will be received by parameter.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_datetime DATETIME
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Update_Control_Mark]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Update_Control_Mark]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Update_Control_Mark] 
(
	@param_datetime DATETIME
)
AS
	BEGIN TRY
		SET NOCOUNT ON;
		IF @param_datetime IS NOT NULL AND @param_datetime <= GETDATE()
			BEGIN
				DELETE FROM egm_control_mark;
				INSERT INTO egm_control_mark VALUES (@param_datetime);
			END
	RETURN 0;
END TRY
BEGIN CATCH
EXEC dbo.spErrorHandling
END CATCH
GO