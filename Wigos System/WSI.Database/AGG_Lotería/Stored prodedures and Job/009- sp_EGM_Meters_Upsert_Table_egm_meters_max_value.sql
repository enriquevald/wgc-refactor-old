/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Upsert_Table_egm_meters_max_value'.

New auxiliar stored procedure to be able to inform 'egm_meters_max_value' table witch is needed to 
detect tsmh_sas_accounting_denom modifications and in runtime of job apply right value when is modified.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_datetime DATETIME
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_max_value]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_max_value]
GO

CREATE PROCEDURE sp_EGM_Meters_Upsert_Table_egm_meters_max_value
	@param_datetime DATETIME
AS
	BEGIN TRY
		SET NOCOUNT ON;
				
		-------------------------	
		-- 1-. Variables Declaration.
		-------------------------	
		DECLARE @stored_call			VARCHAR(1000);
		DECLARE @site_id				INT
		DECLARE @sas_meters_interval	INT
		DECLARE @tsmh_period_obtained 	TABLE(
			  tsmh_site_id 					BIGINT 		NULL
			, tsmh_terminal_id 				INT 		NULL
			, tsmh_meter_code 				INT 		NULL
			, tsmh_game_id 					INT 		NULL
			, tsmh_denomination 			MONEY 		NULL
			, tsmh_type 					INT 		NULL
			, tsmh_datetime 				DATETIME 	NULL
			, tsmh_meter_ini_value 			BIGINT 		NULL
			, tsmh_meter_fin_value 			BIGINT 		NULL
			, tsmh_meter_increment 			BIGINT 		NULL
			, tsmh_raw_meter_increment 		BIGINT 		NULL
			, tsmh_last_reported 			DATETIME  	NULL
			, tsmh_sas_accounting_denom 	MONEY 		NULL
			, tsmh_created_datetime 		DATETIME 	NULL
			, tsmh_group_id 				BIGINT 		NULL
			, tsmh_meter_origin 			INT 		NULL
			, tsmh_meter_max_value 			BIGINT 		NULL
		);

		-------------------------	
		-- 1-. Variables/Params Set
		-------------------------	
		SET @stored_call = 'dbo.sp_EGM_Meters_Get_TSMH_Period_Data @param_datetime = ''' + CONVERT(VARCHAR(100), @param_datetime, 120) + '''';

		-------------------------	
		-- 2-. Building CTE over TSMH with raw data for current execution.	
		-------------------------	
		INSERT INTO @tsmh_period_obtained EXEC (@stored_call);

		-------------------------
		-- 3-. UPDATE [egm_meters_max_values] Treatment
		-------------------------	
		UPDATE dest
		SET dest.emmv_max_value 		= src.tsmh_meter_max_value
		FROM @tsmh_period_obtained src
		INNER JOIN egm_meters_max_values dest
			ON src.tsmh_site_id 		= dest.emmv_site_id
			AND src.tsmh_terminal_id 	= dest.emmv_terminal_id
			AND src.tsmh_meter_code 	= dest.emmv_meter_code
			AND src.tsmh_game_id 		= dest.emmv_game_id
			AND src.tsmh_denomination 	= dest.emmv_denomination
			AND src.tsmh_type 			= dest.emmv_type
			AND src.tsmh_datetime 		= dest.emmv_datetime

		-------------------------
		-- 4-. INSERT [egm_meters_max_values] Treatment
		-------------------------
		INSERT INTO egm_meters_max_values
			SELECT
				src.tsmh_site_id 				AS emmv_site_id
			   ,src.tsmh_terminal_id 			AS emmv_terminal_id
			   ,src.tsmh_meter_code 			AS emmv_meter_code
			   ,src.tsmh_game_id 				AS emmv_game_id
			   ,src.tsmh_denomination 			AS emmv_denomination
			   ,src.tsmh_type 					AS emmv_type
			   ,src.tsmh_datetime 				AS emmv_datetime
			   ,src.tsmh_meter_max_value 		AS emmv_max_value
			FROM @tsmh_period_obtained src
			LEFT JOIN egm_meters_max_values dest
				ON  src.tsmh_site_id 			= dest.emmv_site_id
				AND src.tsmh_terminal_id 		= dest.emmv_terminal_id
				AND src.tsmh_meter_code 		= dest.emmv_meter_code
				AND src.tsmh_game_id 			= dest.emmv_game_id
				AND src.tsmh_denomination 		= dest.emmv_denomination
				AND src.tsmh_type 				= dest.emmv_type
				AND src.tsmh_datetime 			= dest.emmv_datetime
			WHERE dest.emmv_site_id 		IS NULL
			  AND dest.emmv_terminal_id 	IS NULL
			  AND dest.emmv_meter_code  	IS NULL
			  AND dest.emmv_game_id 		IS NULL
			  AND dest.emmv_denomination 	IS NULL
			  AND dest.emmv_type 			IS NULL
			  AND dest.emmv_datetime		IS NULL
			  AND src.tsmh_type 			IN (11)
			  AND src.tsmh_meter_code 		IN (0, 1, 2);
	RETURN 0;
	END TRY
	BEGIN CATCH
		EXEC dbo.spErrorHandling
	END CATCH
GO