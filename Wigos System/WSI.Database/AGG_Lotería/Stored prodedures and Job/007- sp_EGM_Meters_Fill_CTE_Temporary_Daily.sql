/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Fill_CTE_Temporary_Daily'.

This Stored is required to be compatible (with a single script version) with site / multisite.
It composes dynamically the Select statement that obtains data between egm_meters_by_day and terminals
because in site mode terminals table doesn't have te_site_id field, otherwise in multisite it is.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_int_datetime INT
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily] 
	@param_int_datetime INT = NULL
AS
	BEGIN
		SET NOCOUNT ON;
		-- Insert statements for procedure here
		DECLARE @query					NVARCHAR(MAX);
		DECLARE @site_id				INT;
		DECLARE @sas_meters_interval	INT;
		DECLARE @is_multisite			BIT;

		SET @is_multisite 			= 		 (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'MultiSite' 	AND gp_subject_key = 'IsCenter');
		SET @site_id 				= 		 (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Site'	 	AND gp_subject_key = 'Identifier');
		SET @sas_meters_interval 	= ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' 		AND gp_subject_key = 'SASMetersInterval') , 60);

		SET @query = '	SELECT
			';

		SET @query += '
				  emd_working_day
				, emd_site_id
				, NULL
				, NULL
				, SUM(CASE WHEN emd_connected = 1 THEN 1 ELSE 0 END)
				, COUNT (DISTINCT te_terminal_id)
				, GETDATE()
				, NULL
				, NULL
			FROM 
				egm_meters_by_day 
			INNER JOIN terminals
				ON  te_type				=	 1
								AND te_terminal_type	IN	(1, 3, 5)
								AND te_activation_date <= DATEADD(DAY, 1, dbo.Opening(0, CONVERT(DATETIME, ''' + CONVERT(VARCHAR(8), @param_int_datetime) + ''')))
								AND (
											te_retirement_date IS NULL 
										OR	te_retirement_date > CONVERT(DATETIME, ''' + CONVERT(VARCHAR(8), @param_int_datetime) + ''')
									)
									';
					IF (@is_multisite=1)
		SET @query += 'AND te_site_id=emd_site_id
				';

		SET @query += 'WHERE emd_working_day = ' + CONVERT(VARCHAR(8), @param_int_datetime) + '
					GROUP BY emd_working_day , emd_site_id';
		EXEC sp_executesql @query;
	END
GO