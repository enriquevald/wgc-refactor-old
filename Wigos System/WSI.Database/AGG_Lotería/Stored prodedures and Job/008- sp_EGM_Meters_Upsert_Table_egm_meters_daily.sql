/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Upsert_Table_egm_meters_daily'.

This Stored is responsible of mantain updated 'egm_daily' table inserting / updating data based 
on temporal table filled by egm_meters_by_day.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_int_datetime INT
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]
	@param_int_datetime INT
AS
	BEGIN TRY
		SET NOCOUNT ON;
		-- 0-. Variables Declaration & Setting.
		DECLARE @stored_call			VARCHAR(1000);
		DECLARE @temp_table TABLE
		(
			  ed_working_day 				INT 		NULL
			, ed_site_id 					INT 		NULL
			, ed_status 					TINYINT 	NULL
			, ed_has_new_meters 			BIT 		NULL
			, ed_terminals_connected 		INT 		NULL
			, ed_terminals_number 			INT 		NULL
			, ed_last_updated_meters 		DATETIME 	NULL
			, ed_last_updated_user 			INT 		NULL
			, ed_last_updated_user_datetime DATETIME 	NULL
		);

		IF OBJECT_ID('egm_daily_temporary_working_table') IS NOT NULL
		BEGIN
		DROP TABLE egm_daily_temporary_working_table
		END;

		SET @stored_call = 'dbo.sp_EGM_Meters_Fill_CTE_Temporary_Daily @param_int_datetime = ' + CONVERT(VARCHAR(8), @param_int_datetime);

		INSERT INTO @temp_table EXEC (@stored_call);

		-- 2-. Build a CTE as helper to insert current working day data into egm_meters_by_day_temporary_working_table
		WITH cte_temporary_daily (ed_working_day, ed_site_id, ed_status, ed_has_new_meters, ed_terminals_connected, ed_terminals_number, ed_last_updated_meters, ed_last_updated_user, ed_last_updated_user_datetime)
		AS
		(SELECT
				ed_working_day
			   ,ed_site_id
			   ,ed_status
			   ,ed_has_new_meters
			   ,ed_terminals_connected
			   ,ed_terminals_number
			   ,ed_last_updated_meters
			   ,ed_last_updated_user
			   ,ed_last_updated_user_datetime
			FROM @temp_table)
		SELECT
			ed_working_day
		   ,ed_site_id
		   ,ed_status
		   ,ed_has_new_meters
		   ,ed_terminals_connected
		   ,ed_terminals_number
		   ,ed_last_updated_meters
		   ,ed_last_updated_user
		   ,ed_last_updated_user_datetime 
		INTO egm_daily_temporary_working_table
		FROM cte_temporary_daily
		ORDER BY ed_working_day DESC
		-------------------------
		-- 3-. UPDATE [egm_meters_by_period] Treatment
		-------------------------
		UPDATE dest
		SET dest.ed_has_new_meters 		= 1
		   ,dest.ed_last_updated_meters = src.ed_last_updated_meters
		   ,dest.ed_terminals_number 	= src.ed_terminals_number
		FROM egm_daily_temporary_working_table src
		INNER JOIN egm_daily dest
			ON src.ed_working_day	= dest.ed_working_day
			AND src.ed_site_id 		= dest.ed_site_id
		-------------------------
		-- 4-. INSERT [egm_meters_by_period] Treatment
		-------------------------
		INSERT INTO egm_daily
			SELECT
				src.ed_working_day
			   ,src.ed_site_id
			   ,0
			   ,1
			   ,0
			   ,src.ed_terminals_number
			   ,src.ed_last_updated_meters
			   ,NULL
			   ,NULL
			FROM egm_daily_temporary_working_table 	src
			LEFT JOIN egm_daily 					dest
				ON  src.ed_site_id 		= dest.ed_site_id
				AND src.ed_working_day 	= dest.ed_working_day
			WHERE dest.ed_working_day 	IS NULL
			AND dest.ed_site_id 		IS NULL;
	END TRY
	BEGIN CATCH
		EXEC dbo.spErrorHandling
	END CATCH
GO