/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table'.
This stored is used to, given subset of records from tsmh, it prepares data to make it insertable into a temporal table 
that will be used to decide Insert / Update against egm_meters_by_period.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_datetime DATETIME
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]
(
	@param_datetime DATETIME
)
AS
	BEGIN TRY
		SET NOCOUNT ON;
		-- Variables/Params Declarations
		DECLARE @stored_call			VARCHAR(1000);
		DECLARE @site_id				INT
		DECLARE @sas_meters_interval	INT
		DECLARE @tsmh_period_obtained TABLE(
			  tsmh_site_id 					BIGINT 		NULL
			, tsmh_terminal_id 				INT 		NULL
			, tsmh_meter_code 				INT 		NULL
			, tsmh_game_id 					INT 		NULL
			, tsmh_denomination 			MONEY 		NULL
			, tsmh_type 					INT 		NULL
			, tsmh_datetime 				DATETIME 	NULL
			, tsmh_meter_ini_value 			BIGINT 		NULL
			, tsmh_meter_fin_value 			BIGINT 		NULL
			, tsmh_meter_increment 			BIGINT 		NULL
			, tsmh_raw_meter_increment 		BIGINT 		NULL
			, tsmh_last_reported 			DATETIME  	NULL
			, tsmh_sas_accounting_denom 	MONEY 		NULL
			, tsmh_created_datetime 		DATETIME 	NULL
			, tsmh_group_id 				BIGINT 		NULL
			, tsmh_meter_origin 			INT 		NULL
			, tsmh_meter_max_value 			BIGINT 		NULL
		);
		DECLARE @tsmh_period_processed TABLE(
			  tsmh_site_id 					BIGINT 		NULL
			, tsmh_terminal_id 				INT 		NULL
			, tsmh_meter_code 				INT 		NULL
			, tsmh_game_id 					INT 		NULL
			, tsmh_denomination 			MONEY 		NULL
			, tsmh_type 					INT 		NULL
			, tsmh_datetime 				DATETIME 	NULL
			, tsmh_meter_ini_value 			BIGINT 		NULL
			, tsmh_meter_fin_value 			BIGINT 		NULL
			, tsmh_meter_increment 			BIGINT 		NULL
			, tsmh_raw_meter_increment 		BIGINT 		NULL
			, tsmh_last_reported 			DATETIME  	NULL
			, tsmh_sas_accounting_denom 	MONEY 		NULL
			, tsmh_created_datetime 		DATETIME 	NULL
			, tsmh_group_id 				BIGINT 		NULL
			, tsmh_meter_origin 			INT 		NULL
			, tsmh_meter_max_value 			BIGINT 		NULL
		);

	-- Variables/Params Set
	SET @site_id 				= 		 (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Site' 	AND gp_subject_key = 'Identifier');
	SET @sas_meters_interval 	= ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' 	AND gp_subject_key = 'SASMetersInterval'), 60);
	SET @stored_call 			= 'dbo.sp_EGM_Meters_Get_TSMH_Period_Data @param_datetime = ''' + CONVERT(VARCHAR(100), @param_datetime, 120) + '''';

	-- 1-. Deleting Temporary Working Table.
	IF OBJECT_ID('egm_meters_tsmh_temporary_working_table') IS NOT NULL
		BEGIN
			DROP TABLE egm_meters_tsmh_temporary_working_table
		END;
		
	-- 2-. Building CTE over TSMH with raw data for current execution.
	INSERT INTO @tsmh_period_obtained EXEC (@stored_call);

	-- 3-. Process Data to validate tsmh_meter_ini_value is not null, in this case tsmh_meter_fin_value of previous record will be taken.
	INSERT INTO @tsmh_period_processed
		SELECT
			tsmh_site_id 										AS tsmh_site_id
		   ,tsmh_terminal_id 									AS tsmh_terminal_id
		   ,tsmh_meter_code 									AS tsmh_meter_code
		   ,tsmh_game_id 										AS tsmh_game_id
		   ,tsmh_denomination 									AS tsmh_denomination
		   ,tsmh_type 											AS tsmh_type
		   ,tsmh_datetime 										AS tsmh_datetime
			-----------------------------------------------------------------------------------
			/* This change is requested by Dani, in order to avoid NULL values, in 'tsmh_meter_ini_value' to be able to that 
			  we take previous row 'tsmh_meter_fin_value' if isn't NULL else if is NULL we take current tsmh_fin_value.
			  This change is validated with Andreu & Joaquin.
		   . */
		   ,CASE
				WHEN (
					MAX(tsmh_meter_ini_value) IS NULL 
					AND
					LAG(MAX(tsmh_meter_fin_value), 1) OVER (PARTITION BY tsmh_site_id , tsmh_terminal_id , tsmh_meter_code ORDER BY   tsmh_site_id 		ASC
																																	, tsmh_terminal_id 	ASC
																																	, tsmh_meter_code 	ASC
																																	, tsmh_game_id 		ASC
																																	, tsmh_denomination ASC
																																	, tsmh_type 		ASC
																																	, tsmh_datetime 	ASC) IS NOT NULL) THEN 
						LAG(MAX(tsmh_meter_fin_value), 1) OVER (PARTITION BY tsmh_site_id , tsmh_terminal_id , tsmh_meter_code ORDER BY   tsmh_site_id 		ASC
																																		, tsmh_terminal_id 	ASC
																																		, tsmh_meter_code 	ASC
																																		, tsmh_game_id 		ASC
																																		, tsmh_denomination ASC
																																		, tsmh_type 		ASC
																																		, tsmh_datetime 	ASC)
				WHEN (
					MAX(tsmh_meter_ini_value) IS NULL 
					AND
					LAG(MAX(tsmh_meter_fin_value), 1) OVER (PARTITION BY tsmh_site_id , tsmh_terminal_id , tsmh_meter_code ORDER BY   tsmh_site_id 		ASC
																																	, tsmh_terminal_id 	ASC
																																	, tsmh_meter_code 	ASC
																																	, tsmh_game_id 		ASC
																																	, tsmh_denomination ASC
																																	, tsmh_type 		ASC
																																	, tsmh_datetime 	ASC) IS NULL) THEN 
						MAX(tsmh_meter_fin_value)
				ELSE 
					MAX(tsmh_meter_ini_value)
			END 												AS tsmh_meter_ini_value
			-----------------------------------------------------------------------------------
		   ,MAX(tsmh_meter_fin_value) 							AS tsmh_meter_fin_value
		   ,MAX(tsmh_meter_increment) 							AS tsmh_meter_increment
		   ,MAX(tsmh_raw_meter_increment) 						AS tsmh_raw_meter_increment
		   ,MAX(tsmh_last_reported) 							AS tsmh_last_reported
		   ,MAX(ISNULL(tsmh_sas_accounting_denom, 0)) 			AS tsmh_sas_accounting_denom /* FIX: This change has been requested by Dani in order to avoid NULL values in this field to not affect Gain operation */
		   ,MAX(tsmh_created_datetime) 							AS tsmh_created_datetime
		   ,MAX(tsmh_group_id) 									AS tsmh_group_id
		   ,MAX(tsmh_meter_origin) 								AS tsmh_meter_origin
		   ,MAX(tsmh_meter_max_value) 							AS tsmh_meter_max_value
		FROM @tsmh_period_obtained
		GROUP BY tsmh_site_id
				,tsmh_terminal_id
				,tsmh_meter_code
				,tsmh_game_id
				,tsmh_denomination
				,tsmh_type
				,tsmh_datetime
		ORDER BY 	tsmh_site_id 		ASC,
					tsmh_terminal_id 	ASC,
					tsmh_meter_code 	ASC,
					tsmh_game_id 		ASC,
					tsmh_denomination 	ASC,
					tsmh_type 			ASC,
					tsmh_datetime 		ASC;
					
	WITH cte_tsmh (tsmh_site_id, tsmh_terminal_id, tsmh_meter_code, tsmh_game_id, tsmh_denomination, tsmh_type, tsmh_datetime, tsmh_meter_ini_value, tsmh_meter_fin_value, tsmh_meter_increment, tsmh_raw_meter_increment, tsmh_last_reported, tsmh_sas_accounting_denom, tsmh_created_datetime, tsmh_group_id, tsmh_meter_origin, tsmh_meter_max_value)
	AS
	(SELECT
			tsmh_site_id
		   ,tsmh_terminal_id
		   ,tsmh_meter_code
		   ,tsmh_game_id
		   ,tsmh_denomination
		   ,tsmh_type
		   ,tsmh_datetime
		   ,tsmh_meter_ini_value
		   ,tsmh_meter_fin_value
		   ,tsmh_meter_increment
		   ,tsmh_raw_meter_increment
		   ,tsmh_last_reported
		   ,tsmh_sas_accounting_denom
		   ,tsmh_created_datetime
		   ,tsmh_group_id
		   ,tsmh_meter_origin
		   ,tsmh_meter_max_value
		FROM @tsmh_period_processed)
	-----------------------------------------------------------------
	-- HOURLY DATA (TSMH_TYPE = 1) as is expected to retrieve.
	-----------------------------------------------------------------
	,
	cte_pivoted_hourly_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment)
	AS
	(SELECT
			tsmh_site_id 				AS tsmh_site_id
		   ,tsmh_datetime 				AS tsmh_datetime
		   ,tsmh_terminal_id 			AS tsmh_terminal_id
		   ,tsmh_game_id 				AS tsmh_game_id
		   ,tsmh_denomination 			AS tsmh_denomination
		   ,tsmh_type 					AS tsmh_type
		   ,tsmh_sas_accounting_denom 	AS tsmh_sas_accounting_denom
		   ,tsmh_created_datetime 		AS tsmh_created_datetime
		   ,tsmh_last_reported 			AS tsmh_last_reported
		   ,ISNULL([0], 0) 				AS emp_mc_0000
		   ,ISNULL([1], 0) 				AS emp_mc_0001
		   ,ISNULL([2], 0) 				AS emp_mc_0002
		   ,ISNULL([5], 0) 				AS emp_mc_0005
		   ,0 							AS emp_mc_0000_increment
		   ,0 							AS emp_mc_0001_increment
		   ,0 							AS emp_mc_0002_increment
		   ,0 							AS emp_mc_0005_increment
		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime
			   ,tsmh_meter_ini_value
			   ,tsmh_meter_fin_value
			   ,tsmh_meter_increment
			   ,tsmh_raw_meter_increment
			   ,tsmh_last_reported
			   ,tsmh_sas_accounting_denom
			   ,tsmh_created_datetime
			   ,tsmh_group_id
			   ,tsmh_meter_origin
			   ,tsmh_meter_max_value
			FROM cte_tsmh
			WHERE 	tsmh_type IN (1)
				AND tsmh_meter_code IN (0, 1, 2, 5)) AS tsmh
			PIVOT(MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])
		) AS TSMH_PIVOTED_FIN_VALUE
		UNION ALL
		SELECT
			tsmh_site_id 														AS tsmh_site_id
		   ,tsmh_datetime 														AS tsmh_datetime
		   ,tsmh_terminal_id 													AS tsmh_terminal_id
		   ,tsmh_game_id 														AS tsmh_game_id
		   ,tsmh_denomination 													AS tsmh_denomination
		   ,tsmh_type 															AS tsmh_type
		   ,tsmh_sas_accounting_denom 											AS tsmh_sas_accounting_denom
		   ,tsmh_created_datetime 												AS tsmh_created_datetime
		   ,tsmh_last_reported 													AS tsmh_last_reported
		   ,0 																	AS emp_mc_0000
		   ,0 																	AS emp_mc_0001
		   ,0 																	AS emp_mc_0002
		   ,0 																	AS emp_mc_0005
		   ,CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * tsmh_sas_accounting_denom) AS emp_mc_0000_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * tsmh_sas_accounting_denom) AS emp_mc_0001_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * tsmh_sas_accounting_denom) AS emp_mc_0002_increment
		   ,ISNULL([5], 0) 														AS emp_mc_0005_increment
		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime
			   ,tsmh_meter_ini_value
			   ,tsmh_meter_fin_value
			   ,tsmh_meter_increment
			   ,tsmh_raw_meter_increment
			   ,tsmh_last_reported
			   ,tsmh_sas_accounting_denom
			   ,tsmh_created_datetime
			   ,tsmh_group_id
			   ,tsmh_meter_origin
			   ,tsmh_meter_max_value
			FROM cte_tsmh
			WHERE tsmh_type IN (1)
			AND tsmh_meter_code IN (0, 1, 2, 5)) AS tsmh
			PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT),
	cte_leading_hourly
	AS
	(SELECT
			t1.emp_site_id 								AS emp_site_id
		   ,t1.emp_terminal_id 							AS emp_terminal_id
		   ,t1.emp_game_id 								AS emp_game_id
		   ,t1.emp_denomination 						AS emp_denomination
		   ,t1.emp_datetime 							AS emp_datetime
		   ,MAX(t1.emp_record_type) 					AS emp_record_type
		   ,MAX(t1.emp_sas_accounting_denom) 			AS emp_sas_accounting_denom
		   ,MAX(t1.emp_last_created_datetime) 			AS emp_last_created_datetime
		   ,MAX(t1.emp_last_reported_datetime) 			AS emp_last_reported_datetime
		   ,MAX(t1.emp_mc_0000) 						AS emp_mc_0000
		   ,MAX(t1.emp_mc_0001) 						AS emp_mc_0001
		   ,MAX(t1.emp_mc_0002) 						AS emp_mc_0002
		   ,MAX(t1.emp_mc_0005) 						AS emp_mc_0005
		   ,t1.emp_mc_0000_increment 					AS emp_mc_0000_increment
		   ,t1.emp_mc_0001_increment 					AS emp_mc_0001_increment
		   ,t1.emp_mc_0002_increment 					AS emp_mc_0002_increment
		   ,t1.emp_mc_0005_increment 					AS emp_mc_0005_increment
		FROM cte_pivoted_hourly_data_tsmh t1
		GROUP BY t1.emp_site_id
				,t1.emp_terminal_id
				,t1.emp_game_id
				,t1.emp_denomination
				,t1.emp_datetime
				,t1.emp_mc_0000_increment
				,t1.emp_mc_0001_increment
				,t1.emp_mc_0002_increment
				,t1.emp_mc_0005_increment)
	,
	-----------------------------------------------------------------
	-- ANOMALIES DATA (TSMH_TYPE IN (10, 11, 12, 13, 14, 15, 16)) as is expected to retrieve.
	-----------------------------------------------------------------
	cte_pivoted_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment)
	AS
	(SELECT
			tsmh_site_id
		   ,tsmh_datetime
		   ,tsmh_terminal_id
		   ,tsmh_game_id
		   ,tsmh_denomination
		   ,tsmh_type
		   ,tsmh_sas_accounting_denom
		   ,tsmh_created_datetime
		   ,tsmh_last_reported
		   ,ISNULL([0], 0) 					AS emp_mc_0000
		   ,ISNULL([1], 0) 					AS emp_mc_0001
		   ,ISNULL([2], 0) 					AS emp_mc_0002
		   ,ISNULL([5], 0) 					AS emp_mc_0005
		   ,0 								AS emp_mc_0000_increment
		   ,0 								AS emp_mc_0001_increment
		   ,0 								AS emp_mc_0002_increment
		   ,0 								AS emp_mc_0005_increment

		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime
			   ,tsmh_meter_ini_value
			   ,tsmh_meter_fin_value
			   ,tsmh_meter_increment
			   ,tsmh_raw_meter_increment
			   ,tsmh_last_reported
			   ,tsmh_sas_accounting_denom
			   ,tsmh_created_datetime
			   ,tsmh_group_id
			   ,tsmh_meter_origin
			   ,tsmh_meter_max_value
			FROM cte_tsmh
			WHERE tsmh_type IN (10, 11, 12, 13, 14, 15, 16)
			AND tsmh_meter_code IN (0, 1, 2, 5)) AS tsmh
		PIVOT (MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
		UNION ALL
		SELECT
			tsmh_site_id
		   ,tsmh_datetime
		   ,tsmh_terminal_id
		   ,tsmh_game_id
		   ,tsmh_denomination
		   ,tsmh_type
		   ,tsmh_sas_accounting_denom
		   ,tsmh_created_datetime
		   ,tsmh_last_reported
		   ,0 																	AS emp_mc_0000
		   ,0 																	AS emp_mc_0001
		   ,0 																	AS emp_mc_0002
		   ,0 																	AS emp_mc_0005
		   ,CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * tsmh_sas_accounting_denom) AS emp_mc_0000_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * tsmh_sas_accounting_denom) AS emp_mc_0001_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * tsmh_sas_accounting_denom) AS emp_mc_0002_increment
		   ,ISNULL([5], 0) AS emp_mc_0005_increment
		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime
			   ,tsmh_meter_ini_value
			   ,tsmh_meter_fin_value
			   ,tsmh_meter_increment
			   ,tsmh_raw_meter_increment
			   ,tsmh_last_reported
			   ,tsmh_sas_accounting_denom
			   ,tsmh_created_datetime
			   ,tsmh_group_id
			   ,tsmh_meter_origin
			   ,tsmh_meter_max_value
			FROM cte_tsmh
			WHERE tsmh_type IN (10, 11, 12, 13, 14, 15, 16)
			AND tsmh_meter_code IN (0, 1, 2, 5)) AS tsmh
		PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT),
	cte_leading_anomalies
	AS
	(SELECT
			t1.emp_site_id 							AS emp_site_id
		   ,t1.emp_terminal_id 						AS emp_terminal_id
		   ,t1.emp_game_id 							AS emp_game_id
		   ,t1.emp_denomination 					AS emp_denomination
		   ,t1.emp_datetime 						AS emp_datetime
		   ,MAX(t1.emp_record_type) 				AS emp_record_type
		   ,MAX(t1.emp_sas_accounting_denom) 		AS emp_sas_accounting_denom
		   ,MAX(t1.emp_last_created_datetime) 		AS emp_last_created_datetime
		   ,MAX(t1.emp_last_reported_datetime) 		AS emp_last_reported_datetime
		   ,MAX(t1.emp_mc_0000) 					AS emp_mc_0000
		   ,MAX(t1.emp_mc_0001) 					AS emp_mc_0001
		   ,MAX(t1.emp_mc_0002) 					AS emp_mc_0002
		   ,MAX(t1.emp_mc_0005) 					AS emp_mc_0005
		   ,t1.emp_mc_0000_increment 				AS emp_mc_0000_increment
		   ,t1.emp_mc_0001_increment 				AS emp_mc_0001_increment
		   ,t1.emp_mc_0002_increment 				AS emp_mc_0002_increment
		   ,t1.emp_mc_0005_increment 				AS emp_mc_0005_increment
		FROM cte_pivoted_anomalies_data_tsmh t1
		GROUP BY t1.emp_site_id
				,t1.emp_terminal_id
				,t1.emp_game_id
				,t1.emp_denomination
				,t1.emp_datetime
				,t1.emp_mc_0000_increment
				,t1.emp_mc_0001_increment
				,t1.emp_mc_0002_increment
				,t1.emp_mc_0005_increment)
	-----------------------------------------------------------------
	-- ANOMALIES DATA (TSMH_TYPE IN (110, 140, 150, 160)) as is expected to retrieve.
	-----------------------------------------------------------------
	,
	cte_pivoted_group_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment)
	AS
	(SELECT
			tsmh_site_id
		   ,tsmh_datetime
		   ,tsmh_terminal_id
		   ,tsmh_game_id
		   ,tsmh_denomination
		   ,tsmh_type
		   ,tsmh_sas_accounting_denom
		   ,tsmh_created_datetime
		   ,tsmh_last_reported
		   ,ISNULL([0], 0) 					AS emp_mc_0000
		   ,ISNULL([1], 0) 					AS emp_mc_0001
		   ,ISNULL([2], 0) 					AS emp_mc_0002
		   ,ISNULL([5], 0) 					AS emp_mc_0005
		   ,0 								AS emp_mc_0000_increment
		   ,0 								AS emp_mc_0001_increment
		   ,0 								AS emp_mc_0002_increment
		   ,0 								AS emp_mc_0005_increment
		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime 					AS tsmh_datetime
			   ,MAX(tsmh_meter_ini_value) 		AS tsmh_meter_ini_value
			   ,MAX(tsmh_meter_fin_value) 		AS tsmh_meter_fin_value
			   ,MAX(tsmh_meter_increment) 		AS tsmh_meter_increment
			   ,MAX(tsmh_raw_meter_increment) 	AS tsmh_raw_meter_increment
			   ,MAX(tsmh_last_reported) 		AS tsmh_last_reported
			   ,MAX(tsmh_sas_accounting_denom) 	AS tsmh_sas_accounting_denom
			   ,MAX(tsmh_created_datetime) 		AS tsmh_created_datetime
			   ,MAX(tsmh_group_id) 				AS tsmh_group_id
			   ,MAX(tsmh_meter_origin) 			AS tsmh_meter_origin
			   ,MAX(tsmh_meter_max_value) 		AS tsmh_meter_max_value
			FROM cte_tsmh
			WHERE tsmh_type IN (110, 140, 150, 160)
			AND tsmh_meter_code IN (0, 1, 2, 5)
			GROUP BY 
					 tsmh_datetime
					,tsmh_site_id
					,tsmh_terminal_id
					,tsmh_meter_code
					,tsmh_game_id
					,tsmh_denomination
					,tsmh_type) AS tsmh
			PIVOT (MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
		UNION ALL
		SELECT
			tsmh_site_id
		   ,tsmh_datetime
		   ,tsmh_terminal_id
		   ,tsmh_game_id
		   ,tsmh_denomination
		   ,tsmh_type
		   ,tsmh_sas_accounting_denom
		   ,tsmh_created_datetime
		   ,tsmh_last_reported
		   ,0 																	AS emp_mc_0000
		   ,0 																	AS emp_mc_0001
		   ,0 																	AS emp_mc_0002
		   ,0 																	AS emp_mc_0005
		   ,CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * tsmh_sas_accounting_denom) AS emp_mc_0000_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * tsmh_sas_accounting_denom) AS emp_mc_0001_increment
		   ,CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * tsmh_sas_accounting_denom) AS emp_mc_0002_increment
		   ,ISNULL([5], 0) 														AS emp_mc_0005_increment
		FROM (SELECT
				tsmh_site_id
			   ,tsmh_terminal_id
			   ,tsmh_meter_code
			   ,tsmh_game_id
			   ,tsmh_denomination
			   ,tsmh_type
			   ,tsmh_datetime 					AS tsmh_datetime
			   ,MAX(tsmh_meter_ini_value) 		AS tsmh_meter_ini_value
			   ,MAX(tsmh_meter_fin_value) 		AS tsmh_meter_fin_value
			   ,MAX(tsmh_meter_increment) 		AS tsmh_meter_increment
			   ,MAX(tsmh_raw_meter_increment) 	AS tsmh_raw_meter_increment
			   ,MAX(tsmh_last_reported) 		AS tsmh_last_reported
			   ,MAX(tsmh_sas_accounting_denom) 	AS tsmh_sas_accounting_denom
			   ,MAX(tsmh_created_datetime) 		AS tsmh_created_datetime
			   ,MAX(tsmh_group_id) 				AS tsmh_group_id
			   ,MAX(tsmh_meter_origin) 			AS tsmh_meter_origin
			   ,MAX(tsmh_meter_max_value) 		AS tsmh_meter_max_value
			FROM cte_tsmh
			WHERE tsmh_type IN (110, 140, 150, 160)
			AND tsmh_meter_code IN (0, 1, 2, 5)
			GROUP BY 
					 tsmh_datetime
					,tsmh_site_id
					,tsmh_terminal_id
					,tsmh_meter_code
					,tsmh_game_id
					,tsmh_denomination
					,tsmh_type) AS tsmh
			PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT),
	cte_leading_group_anomalies
	AS
	(SELECT
			t1.emp_site_id 							AS emp_site_id
		   ,t1.emp_terminal_id 						AS emp_terminal_id
		   ,t1.emp_game_id 							AS emp_game_id
		   ,t1.emp_denomination 					AS emp_denomination
		   ,t1.emp_datetime 						AS emp_datetime
		   ,MAX(t1.emp_record_type) 				AS emp_record_type
		   ,MAX(t1.emp_sas_accounting_denom) 		AS emp_sas_accounting_denom
		   ,MAX(t1.emp_last_created_datetime) 		AS emp_last_created_datetime
		   ,MAX(t1.emp_last_reported_datetime) 		AS emp_last_reported_datetime
		   ,MAX(t1.emp_mc_0000) 					AS emp_mc_0000
		   ,MAX(t1.emp_mc_0001) 					AS emp_mc_0001
		   ,MAX(t1.emp_mc_0002) 					AS emp_mc_0002
		   ,MAX(t1.emp_mc_0005) 					AS emp_mc_0005
		   ,t1.emp_mc_0000_increment 				AS emp_mc_0000_increment
		   ,t1.emp_mc_0001_increment 				AS emp_mc_0001_increment
		   ,t1.emp_mc_0002_increment 				AS emp_mc_0002_increment
		   ,t1.emp_mc_0005_increment 				AS emp_mc_0005_increment
		FROM cte_pivoted_group_anomalies_data_tsmh t1
		GROUP BY t1.emp_site_id
				,t1.emp_terminal_id
				,t1.emp_game_id
				,t1.emp_denomination
				,t1.emp_datetime
				,t1.emp_mc_0000_increment
				,t1.emp_mc_0001_increment
				,t1.emp_mc_0002_increment
				,t1.emp_mc_0005_increment)
	-----------------------------------------------------------------
	-- 	SELECT to insert data into termporary working table the result of previous execution
	-- 	It contains: 
			-- 1-. 			   Hourly records of tsmh queried period.
			-- 2-. Single	Anomalies records of tsmh queried period.
			-- 3-. Groupal 	Anomalies records of tsmh queried period.
	-- 	Ordered by datetime ascendant.
	-----------------------------------------------------------------
	SELECT
		emp_working_day
	   ,emp_site_id
	   ,emp_datetime
	   ,emp_terminal_id
	   ,emp_game_id
	   ,emp_denomination
	   ,emp_record_type
	   ,emp_sas_accounting_denom
	   ,emp_last_created_datetime
	   ,emp_last_reported_datetime
	   ,emp_user_ignored
	   ,emp_user_ignored_datetime
	   ,emp_last_updated_user_id
	   ,emp_last_updated_user_datetime
	   ,emp_status
	   ,emp_mc_0000
	   ,emp_mc_0001
	   ,emp_mc_0002
	   ,emp_mc_0005
	   ,emp_mc_0000_increment
	   ,emp_mc_0001_increment
	   ,emp_mc_0002_increment
	   ,emp_mc_0005_increment 
	   INTO egm_meters_tsmh_temporary_working_table
	FROM ((SELECT
			CASE
				WHEN (NOT EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)) OR 
						  EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0) ) THEN 
					dbo.GamingDayFromDateTime(emp_datetime)
				ELSE (SELECT TOP (1) ed_working_day FROM egm_daily WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0 ORDER BY ed_working_day ASC)
			END 											AS emp_working_day
		   ,emp_site_id 									AS emp_site_id
		   ,emp_terminal_id 								AS emp_terminal_id
		   ,emp_game_id 									AS emp_game_id
		   ,emp_denomination 								AS emp_denomination
		   ,emp_datetime 									AS emp_datetime
		   ,emp_record_type 								AS emp_record_type
		   ,emp_sas_accounting_denom 						AS emp_sas_accounting_denom
		   ,MAX(emp_last_created_datetime) 					AS emp_last_created_datetime
		   ,MAX(emp_last_reported_datetime) 				AS emp_last_reported_datetime
		   ,NULL 											AS emp_user_ignored
		   ,NULL 											AS emp_user_ignored_datetime
		   ,NULL 											AS emp_last_updated_user_id
		   ,NULL 											AS emp_last_updated_user_datetime
		   ,NULL 											AS emp_status
		   ,MAX(emp_mc_0000) 								AS emp_mc_0000
		   ,MAX(emp_mc_0001) 								AS emp_mc_0001
		   ,MAX(emp_mc_0002) 								AS emp_mc_0002
		   ,MAX(emp_mc_0005) 								AS emp_mc_0005
		   ,SUM(emp_mc_0000_increment) 						AS emp_mc_0000_increment
		   ,SUM(emp_mc_0001_increment) 						AS emp_mc_0001_increment
		   ,SUM(emp_mc_0002_increment) 						AS emp_mc_0002_increment
		   ,SUM(emp_mc_0005_increment) 						AS emp_mc_0005_increment
		FROM cte_leading_hourly
		GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
				,emp_site_id
				,emp_datetime
				,emp_terminal_id
				,emp_game_id
				,emp_denomination
				,emp_record_type
				,emp_sas_accounting_denom
		)
		UNION
		(
		SELECT
			CASE
				WHEN (NOT 	EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)) OR
							EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0) ) THEN 
					dbo.GamingDayFromDateTime(emp_datetime)
				ELSE (SELECT TOP (1) ed_working_day FROM egm_daily WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0 ORDER BY ed_working_day ASC)
			END 								AS emp_working_day
		   ,emp_site_id 						AS emp_site_id
		   ,emp_terminal_id 					AS emp_terminal_id
		   ,emp_game_id 						AS emp_game_id
		   ,emp_denomination 					AS emp_denomination
		   ,emp_datetime 						AS emp_datetime
		   ,emp_record_type 					AS emp_record_type
		   ,emp_sas_accounting_denom 			AS emp_sas_accounting_denom
		   ,MAX(emp_last_created_datetime) 		AS emp_last_created_datetime
		   ,MAX(emp_last_reported_datetime) 	AS emp_last_reported_datetime
		   ,NULL 								AS emp_user_ignored
		   ,NULL 								AS emp_user_ignored_datetime
		   ,NULL 								AS emp_last_updated_user_id
		   ,NULL 								AS emp_last_updated_user_datetime
		   ,NULL 								AS emp_status
		   ,MAX(emp_mc_0000) 					AS emp_mc_0000
		   ,MAX(emp_mc_0001) 					AS emp_mc_0001
		   ,MAX(emp_mc_0002) 					AS emp_mc_0002
		   ,MAX(emp_mc_0005) 					AS emp_mc_0005
		   ,SUM(emp_mc_0000_increment) 			AS emp_mc_0000_increment
		   ,SUM(emp_mc_0001_increment) 			AS emp_mc_0001_increment
		   ,SUM(emp_mc_0002_increment) 			AS emp_mc_0002_increment
		   ,SUM(emp_mc_0005_increment) 			AS emp_mc_0005_increment
		FROM cte_leading_anomalies
		GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
				,emp_site_id
				,emp_datetime
				,emp_terminal_id
				,emp_game_id
				,emp_denomination
				,emp_record_type
				,emp_sas_accounting_denom
		)
		UNION
		(SELECT
			CASE
				WHEN (NOT 	EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)) OR
							EXISTS (SELECT * FROM egm_daily WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0) ) THEN 
					dbo.GamingDayFromDateTime(emp_datetime)
				ELSE (SELECT TOP (1) ed_working_day FROM egm_daily WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime) AND ed_status = 0 ORDER BY ed_working_day ASC)
			END 									AS emp_working_day
		   ,emp_site_id 							AS emp_site_id
		   ,emp_terminal_id 						AS emp_terminal_id
		   ,emp_game_id 							AS emp_game_id
		   ,emp_denomination 						AS emp_denomination
		   ,emp_datetime 							AS emp_datetime
		   ,emp_record_type 						AS emp_record_type
		   ,emp_sas_accounting_denom 				AS emp_sas_accounting_denom
		   ,MAX(emp_last_created_datetime) 			AS emp_last_created_datetime
		   ,MAX(emp_last_reported_datetime) 		AS emp_last_reported_datetime
		   ,NULL 									AS emp_user_ignored
		   ,NULL 									AS emp_user_ignored_datetime
		   ,NULL 									AS emp_last_updated_user_id
		   ,NULL 									AS emp_last_updated_user_datetime
		   ,NULL 									AS emp_status
		   ,MAX(emp_mc_0000) 						AS emp_mc_0000
		   ,MAX(emp_mc_0001) 						AS emp_mc_0001
		   ,MAX(emp_mc_0002) 						AS emp_mc_0002
		   ,MAX(emp_mc_0005) 						AS emp_mc_0005
		   ,SUM(emp_mc_0000_increment) 				AS emp_mc_0000_increment
		   ,SUM(emp_mc_0001_increment) 				AS emp_mc_0001_increment
		   ,SUM(emp_mc_0002_increment) 				AS emp_mc_0002_increment
		   ,SUM(emp_mc_0005_increment) 				AS emp_mc_0005_increment
		FROM cte_leading_group_anomalies
		GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
				,emp_site_id
				,emp_datetime
				,emp_terminal_id
				,emp_game_id
				,emp_denomination
				,emp_record_type
				,emp_sas_accounting_denom
		)) AS x
		ORDER BY emp_datetime;
		RETURN 0;
	END TRY
	BEGIN CATCH
		EXEC dbo.spErrorHandling
	END CATCH
GO