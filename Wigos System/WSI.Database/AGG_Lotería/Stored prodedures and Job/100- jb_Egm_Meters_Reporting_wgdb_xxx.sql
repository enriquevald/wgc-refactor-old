/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'jb_Egm_Meters_Reporting_wgdb_xxx'.

Job that links all related stored procedures to have an scheduling and recursive calling to them to perform
infinite loop on information getting.

Default value for new installation and new related model tables creation for the first execution is beggining 
of the previous working day.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New job

Requeriments:

Parameters:
	
*/
DECLARE @start_job_return		INT;
DECLARE @schedule_id 			INT;
DECLARE @job_id                 BINARY(16);
DECLARE @job_id_To_Delete       BINARY(16);
DECLARE @job_name               VARCHAR(100);
DECLARE @dest_db_name           VARCHAR(100);
DECLARE @step_name_1            VARCHAR(100);
DECLARE @step_name_2			VARCHAR(100);

SET @dest_db_name = (SELECT
		DB_NAME());
SET @job_name = 'jb_Egm_Meters_Reporting_' + DB_NAME();
SET @step_name_1 = '1-. Drop, Create & Fill Temporary Working Table With Current Control Datetime Period';
SET @step_name_2 = '2-. UPSERT data in tables: [egm_meters_by_period, egm_meters_by_day, egm_meters_daily]';


IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @job_name)
	BEGIN
		SET @job_id_To_Delete = (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @job_name);
		EXEC msdb.dbo.sp_delete_job @job_id = @job_id_To_Delete ,@delete_unused_schedule = 1;
		PRINT 'Job ''' + @job_name + ''' deleted succesfully.';
	END

BEGIN TRANSACTION
	DECLARE @ReturnCode INT
SELECT
	@ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'Data Collector' AND category_class = 1)
	BEGIN
		EXEC @ReturnCode = msdb.dbo.sp_add_category  @class = N'JOB'
													,@type = N'LOCAL'
													,@name = N'Data Collector'
IF (@@ERROR <> 0 OR @ReturnCode <> 0)
	GOTO QuitWithRollback
END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode = msdb.dbo.sp_add_job @job_name 				= @job_name
										  ,@enabled 				= 1
										  ,@notify_level_eventlog 	= 0
										  ,@notify_level_email 		= 0
										  ,@notify_level_netsend 	= 0
										  ,@notify_level_page 		= 0
										  ,@delete_level 			= 0
										  ,@description 			= N'This job is designed for collect meters history data to be reported into [egm_meters_by_period], [egm_meters_by_day] and [egm_meters_daily] tables.'
										  ,@category_name 			= N'Data Collector'
										  ,@owner_login_name 		= N'sa'
										  ,@job_id 					= @jobId OUTPUT
	PRINT 'Job ''' + @job_name + ''' created succesfully.'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
/* Step [1-. Drop, Create & Fill Temporary Working Table With Current Control Datetime Period] */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_name = @job_name
										  ,@step_name = @step_name_1
										  ,@step_id = 1
										  ,@cmdexec_success_code = 0
										  ,@on_success_action = 3
										  ,@on_success_step_id = 0
										  ,@on_fail_action = 2
										  ,@on_fail_step_id = 0
										  ,@retry_attempts = 5
										  ,@retry_interval = 0
										  ,@os_run_priority = 0
										  ,@subsystem = N'TSQL'
										  ,@database_name = @dest_db_name
										  ,@flags = 12
										  ,@command = N'DECLARE @param_datetime DATETIME;
EXEC sp_EGM_Meters_Get_Control_Mark @param_datetime OUTPUT;
EXEC sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table @param_datetime=@param_datetime;'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
/* Step 2: [2-. UPSERT data in tables: [egm_meters_by_period, egm_meters_by_day, egm_meters_daily]]] */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_name 				= @job_name
										  ,@step_name 				= @step_name_2
										  ,@step_id 				= 2
										  ,@cmdexec_success_code 	= 0
										  ,@on_success_action 		= 1
										  ,@on_success_step_id 		= 0
										  ,@on_fail_action 			= 2
										  ,@on_fail_step_id 		= 0
										  ,@retry_attempts 			= 5
										  ,@retry_interval 			= 0
										  ,@os_run_priority 		= 0
										  ,@subsystem 				= N'TSQL'
										  ,@database_name 			= @dest_db_name
										  ,@flags 					= 12
										  ,@command 				= N'DECLARE @updated_control_mark 	DATETIME;
DECLARE @sas_meters_interval  	INT;
DECLARE @param_int_datetime  	INT;
DECLARE @param_datetime 		DATETIME;

EXEC sp_EGM_Meters_Get_Control_Mark @param_datetime OUTPUT;

SET @sas_meters_interval = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = ''WCP'' AND gp_subject_key = ''SASMetersInterval''),60)
BEGIN TRY
	BEGIN TRANSACTION
	--================================================
	-- 1-. UPSERT egm_meters_by_period
		EXEC sp_EGM_Meters_Upsert_Table_egm_meters_by_period							
		SET @param_int_datetime = dbo.GamingDayFromDateTime(@param_datetime);
	-- 2-. UPSERT egm_meters_by_day
		EXEC sp_EGM_Meters_Upsert_Table_egm_meters_by_day @param_working_day = @param_int_datetime;
	-- 3-. UPSERT egm_meters_daily
			EXEC sp_EGM_Meters_Upsert_Table_egm_meters_daily @param_int_datetime  = @param_int_datetime;
	-- 4-. UPSERT egm_meters_max_value
			EXEC sp_EGM_Meters_Upsert_Table_egm_meters_max_value @param_datetime = @param_datetime;
	-- 5-. UPDATE CONTROL MARK [Only If process ends succesfully control mark will be updated].
		SET @updated_control_mark = DATEADD(MINUTE, @sas_meters_interval,  (SELECT TOP(1) ecm_control_mark FROM egm_control_mark));
		EXEC sp_EGM_Meters_Update_Control_Mark @param_datetime=@updated_control_mark;
	--================================================    
	COMMIT
END TRY
BEGIN CATCH
	EXEC dbo.spErrorHandling
	ROLLBACK
END CATCH'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_name 		= @job_name
										 ,@start_step_id 	= 1
PRINT ' - Job step ''' + @job_name + '.' + @step_name_1 + ''' appended succesfully.'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_name = @job_name
												,@server_name = N'(local)'
PRINT 'Job ''' + @job_name + ''' added succesfully.'
	
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
		COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) 
			ROLLBACK TRANSACTION
	EndSave:
/* Creation And Appending Schedule to the Job */
EXEC msdb.dbo.sp_add_jobschedule @job_name 					= @job_name
								,@name 						= N'EGM_Meters_Schedule'
								,@enabled 					= 1
								,@freq_type 				= 4
								,@freq_interval 			= 1
								,@freq_subday_type 			= 4
								,@freq_subday_interval 		= 8
								,@freq_relative_interval 	= 0
								,@freq_recurrence_factor 	= 1
								,@active_start_date 		= 20171227
								,@active_end_date 			= 99991231
								,@active_start_time 		= 0
								,@active_end_time 			= 235959
								,@schedule_id 				= @schedule_id OUTPUT

/* Zero Execution Of Job */
EXEC @start_job_return = msdb.dbo.sp_start_job @job_name = @job_name;