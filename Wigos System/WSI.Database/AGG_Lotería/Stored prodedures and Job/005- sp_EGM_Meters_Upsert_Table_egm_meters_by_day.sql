/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Upsert_Table_egm_meters_by_day'.

This Stored is responsible of mantain updated 'egm_meters_by_day' table inserting / updating data based 
on egm_meters_by_period.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_working_day INT
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]
	@param_working_day INT
AS
	BEGIN TRY
		SET NOCOUNT ON;	
		-- 0-. Declare Variables Area.
		DECLARE @table_temporary_by_day TABLE(
			  emd_working_day INT NULL
			, emd_site_id BIGINT NULL
			, emd_terminal_id INT NULL
			, emd_game_id INT NULL
			, emd_warning INT NULL
			, emd_user_period_modified BIT NULL
			, emd_user_validated BIT NULL
			, emd_user_validated_datetime DATETIME NULL
			, emd_last_updated DATETIME NULL
			, emd_last_updated_user_id INT NULL
			, emd_connected BIT NULL
			, emd_status INT NULL
			, emd_mc_0000_increment DECIMAL(20,2) NULL
			, emd_mc_0001_increment DECIMAL(20,2) NULL
			, emd_mc_0002_increment DECIMAL(20,2) NULL
			, emd_mc_0005_increment BIGINT NULL
			, emd_mc_0000 BIGINT NULL
			, emd_mc_0001 BIGINT NULL
			, emd_mc_0002 BIGINT NULL
			, emd_mc_0005 BIGINT NULL
		);

	-- 1-. Deleting Temporary Working Table.
	IF OBJECT_ID('egm_meters_by_day_temporary_working_table') IS NOT NULL
		BEGIN
	DROP TABLE egm_meters_by_day_temporary_working_table
	END;
	INSERT INTO @table_temporary_by_day
		SELECT
			p.emp_working_day
		   ,p.emp_site_id
		   ,p.emp_terminal_id
		   ,MAX(p3.emp_game_id)
		   ,SUM(
			DISTINCT CASE p.emp_record_type
				WHEN 1 THEN 0 	/* Hourly Report - [Isn't an anomaly] */
				WHEN 10 THEN 2 	/* Reset */
				WHEN 11 THEN 4 	/* Rollover */
				WHEN 12 THEN 8 	/* First Time */
				WHEN 13 THEN 16 	/* Discarded */
				WHEN 14 THEN 32 	/* SAS Accounting Denom Change */
				WHEN 15 THEN 64 	/* Service RAM Clear */
				WHEN 16 THEN 128 	/* Machine RAM Clear */
				ELSE 0 	/* Rest Of Cases */
			END
			)
		   ,1
		   ,1
		   ,NULL
		   ,NULL
		   ,NULL
		   ,0
		   ,0
		   ,SUM(ISNULL(p.emp_mc_0000_increment, 0))
		   ,SUM(ISNULL(p.emp_mc_0001_increment, 0))
		   ,SUM(ISNULL(p.emp_mc_0002_increment, 0))
		   ,SUM(ISNULL(p.emp_mc_0005_increment, 0))
		   ,MAX(ISNULL(p2.emp_mc_0000, 0))
		   ,MAX(ISNULL(p2.emp_mc_0001, 0))
		   ,MAX(ISNULL(p2.emp_mc_0002, 0))
		   ,MAX(ISNULL(p2.emp_mc_0005, 0))
		FROM dbo.egm_meters_by_period p
		LEFT JOIN dbo.egm_meters_by_period p2
			ON p.emp_working_day = p2.emp_working_day
				AND p.emp_site_id = p2.emp_site_id
				AND p.emp_terminal_id = p2.emp_terminal_id
				AND p.emp_game_id = p2.emp_game_id
				AND p.emp_denomination = p2.emp_denomination
				AND p.emp_datetime = p2.emp_datetime
				AND p2.emp_datetime = (SELECT
						MAX(p21.emp_datetime)
					FROM dbo.egm_meters_by_period p21
					WHERE p.emp_working_day = p21.emp_working_day
					AND p.emp_site_id = p21.emp_site_id
					AND p.emp_terminal_id = p21.emp_terminal_id
					AND p.emp_game_id = p21.emp_game_id
					AND p.emp_denomination = p21.emp_denomination)
		LEFT JOIN dbo.egm_meters_by_period p3
			ON p.emp_working_day = p3.emp_working_day
				AND p.emp_site_id = p3.emp_site_id
				AND p.emp_terminal_id = p3.emp_terminal_id
				AND p.emp_game_id = p3.emp_game_id
				AND p.emp_denomination = p3.emp_denomination
				AND p.emp_datetime = p3.emp_datetime
				AND p3.emp_datetime = (SELECT
						MIN(p31.emp_datetime)
					FROM dbo.egm_meters_by_period p31
					WHERE p.emp_working_day = p31.emp_working_day
					AND p.emp_site_id = p31.emp_site_id
					AND p.emp_terminal_id = p31.emp_terminal_id
					AND p.emp_game_id = p31.emp_game_id
					AND p.emp_denomination = p31.emp_denomination)
		LEFT JOIN dbo.terminals_connected tc
			ON tc.tc_date = CONVERT(DATETIME, CONVERT(CHAR(8), @param_working_day))
				AND tc.tc_connected = 1
				AND tc.tc_terminal_id = p.emp_terminal_id
		WHERE p.emp_working_day = @param_working_day
		GROUP BY p.emp_working_day
				,p.emp_site_id
				,p.emp_terminal_id
				,p.emp_game_id
				,p.emp_denomination;


		WITH cte_temporary_by_day (emd_working_day, emd_site_id, emd_terminal_id, emd_game_id, emd_warning, emd_user_period_modified, emd_user_validated, emd_user_validated_datetime, emd_last_updated, emd_last_updated_user_id, emd_connected, emd_status, emd_mc_0000_increment, emd_mc_0001_increment, emd_mc_0002_increment, emd_mc_0005_increment, emd_mc_0000, emd_mc_0001, emd_mc_0002, emd_mc_0005)
		AS
		(SELECT
				emd_working_day
			   ,emd_site_id
			   ,emd_terminal_id
			   ,emd_game_id
			   ,emd_warning
			   ,emd_user_period_modified
			   ,emd_user_validated
			   ,emd_user_validated_datetime
			   ,emd_last_updated
			   ,emd_last_updated_user_id
			   ,emd_connected
			   ,emd_status
			   ,emd_mc_0000_increment
			   ,emd_mc_0001_increment
			   ,emd_mc_0002_increment
			   ,emd_mc_0005_increment
			   ,emd_mc_0000
			   ,emd_mc_0001
			   ,emd_mc_0002
			   ,emd_mc_0005
			FROM @table_temporary_by_day)
		SELECT
			emd_working_day
		   ,emd_site_id
		   ,emd_terminal_id
		   ,emd_game_id
		   ,emd_warning
		   ,emd_user_period_modified
		   ,emd_user_validated
		   ,emd_user_validated_datetime
		   ,emd_last_updated
		   ,emd_last_updated_user_id
		   ,emd_connected
		   ,emd_status
		   ,emd_mc_0000_increment
		   ,emd_mc_0001_increment
		   ,emd_mc_0002_increment
		   ,emd_mc_0005_increment
		   ,emd_mc_0000
		   ,emd_mc_0001
		   ,emd_mc_0002
		   ,emd_mc_0005 
		INTO egm_meters_by_day_temporary_working_table
		FROM cte_temporary_by_day
		ORDER BY emd_working_day DESC
		, emd_terminal_id ASC
		-------------------------
		-- 3-. UPDATE [egm_meters_by_period] Treatment
		-------------------------
		UPDATE dest
		SET dest.emd_warning = dbo.CompareTwoAnomaliesMasks(src.emd_warning, dest.emd_warning)
		   ,dest.emd_status = ISNULL(src.emd_status, 0)
		   ,dest.emd_mc_0000_increment = ISNULL(src.emd_mc_0000_increment, 0)
		   ,dest.emd_mc_0001_increment = ISNULL(src.emd_mc_0001_increment, 0)
		   ,dest.emd_mc_0002_increment = ISNULL(src.emd_mc_0002_increment, 0)
		   ,dest.emd_mc_0005_increment = ISNULL(src.emd_mc_0005_increment, 0)
		   ,dest.emd_mc_0000 = ISNULL(src.emd_mc_0000, 0)
		   ,dest.emd_mc_0001 = ISNULL(src.emd_mc_0001, 0)
		   ,dest.emd_mc_0002 = ISNULL(src.emd_mc_0002, 0)
		   ,dest.emd_mc_0005 = ISNULL(src.emd_mc_0005, 0)
		FROM egm_meters_by_day_temporary_working_table src
		INNER JOIN egm_meters_by_day dest
			ON src.emd_working_day = dest.emd_working_day
			AND src.emd_site_id = dest.emd_site_id
			AND src.emd_terminal_id = dest.emd_terminal_id
		-------------------------
		-- 4-. INSERT [egm_meters_by_period] Treatment
		-------------------------
		INSERT INTO egm_meters_by_day
			SELECT
				src.emd_working_day
			   ,src.emd_site_id
			   ,src.emd_terminal_id
			   ,src.emd_game_id
			   ,src.emd_warning
			   ,0
			   ,0
			   ,NULL
			   ,NULL
			   ,NULL
			   ,src.emd_connected
			   ,ISNULL(src.emd_status, 0)
			   ,ISNULL(src.emd_mc_0000_increment, 0)
			   ,ISNULL(src.emd_mc_0001_increment, 0)
			   ,ISNULL(src.emd_mc_0002_increment, 0)
			   ,ISNULL(src.emd_mc_0005_increment, 0)
			   ,ISNULL(src.emd_mc_0000, 0)
			   ,ISNULL(src.emd_mc_0001, 0)
			   ,ISNULL(src.emd_mc_0002, 0)
			   ,ISNULL(src.emd_mc_0005, 0)
			FROM egm_meters_by_day_temporary_working_table src
			LEFT JOIN egm_meters_by_day dest
				ON src.emd_site_id = dest.emd_site_id
					AND src.emd_terminal_id = dest.emd_terminal_id
					AND src.emd_working_day = dest.emd_working_day
			WHERE dest.emd_working_day IS NULL
			AND dest.emd_terminal_id IS NULL
			AND dest.emd_site_id IS NULL;
	END TRY
	BEGIN CATCH
		EXEC dbo.spErrorHandling
	END CATCH
GO