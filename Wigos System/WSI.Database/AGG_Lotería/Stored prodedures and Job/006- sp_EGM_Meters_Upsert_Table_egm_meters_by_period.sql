/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Upsert_Table_egm_meters_by_period'.

This Stored is responsible of mantain updated 'egm_meters_by_period' table inserting / updating data based 
on temporal table filled by terminal_sas_meters_history.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:

*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]
AS
	BEGIN TRY
		SET NOCOUNT ON;
		-------------------------
		--UPDATE [egm_meters_by_period] Treatment
		-------------------------
		UPDATE dest
		SET dest.emp_status  			= ISNULL(src.emp_status, 0)
		   ,dest.emp_mc_0000 			= ISNULL(src.emp_mc_0000, 0)
		   ,dest.emp_mc_0001 			= ISNULL(src.emp_mc_0001, 0)
		   ,dest.emp_mc_0002 			= ISNULL(src.emp_mc_0002, 0)
		   ,dest.emp_mc_0005 			= ISNULL(src.emp_mc_0005, 0)
		   ,dest.emp_mc_0000_increment 	= ISNULL(src.emp_mc_0000_increment, 0)
		   ,dest.emp_mc_0001_increment 	= ISNULL(src.emp_mc_0001_increment, 0)
		   ,dest.emp_mc_0002_increment 	= ISNULL(src.emp_mc_0002_increment, 0)
		   ,dest.emp_mc_0005_increment 	= ISNULL(src.emp_mc_0005_increment, 0)
		FROM egm_meters_tsmh_temporary_working_table src
		INNER JOIN egm_meters_by_period dest
			ON 	src.emp_working_day 			= dest.emp_working_day
			AND src.emp_site_id 				= dest.emp_site_id
			AND src.emp_datetime 				= dest.emp_datetime
			AND src.emp_terminal_id 			= dest.emp_terminal_id
			AND src.emp_game_id 				= dest.emp_game_id
			AND src.emp_denomination 			= dest.emp_denomination
			AND src.emp_record_type 			= dest.emp_record_type
			AND src.emp_sas_accounting_denom 	= dest.emp_sas_accounting_denom
		-------------------------
		-- INSERT [egm_meters_by_period] Treatment
		-------------------------
		INSERT INTO egm_meters_by_period
			SELECT
				src.emp_working_day
			   ,src.emp_site_id
			   ,src.emp_terminal_id
			   ,src.emp_game_id
			   ,src.emp_denomination
			   ,src.emp_datetime
			   ,src.emp_record_type
			   ,src.emp_sas_accounting_denom
			   ,ISNULL(src.emp_last_created_datetime, GETDATE())
			   ,ISNULL(src.emp_last_reported_datetime, GETDATE())
			   ,ISNULL(src.emp_user_ignored, 0)
			   ,src.emp_user_ignored_datetime
			   ,src.emp_last_updated_user_id
			   ,src.emp_last_updated_user_datetime
			   ,ISNULL(src.emp_status, 0)
			   ,ISNULL(src.emp_mc_0000_increment, 0)
			   ,ISNULL(src.emp_mc_0001_increment, 0)
			   ,ISNULL(src.emp_mc_0002_increment, 0)
			   ,ISNULL(src.emp_mc_0005_increment, 0)
			   ,ISNULL(src.emp_mc_0000, 0)
			   ,ISNULL(src.emp_mc_0001, 0)
			   ,ISNULL(src.emp_mc_0002, 0)
			   ,ISNULL(src.emp_mc_0005, 0)
			FROM egm_meters_tsmh_temporary_working_table src
			LEFT JOIN egm_meters_by_period dest
				ON src.emp_working_day 					= dest.emp_working_day
					AND src.emp_site_id 				= dest.emp_site_id
					AND src.emp_datetime 				= dest.emp_datetime
					AND src.emp_terminal_id 			= dest.emp_terminal_id
					AND src.emp_game_id 				= dest.emp_game_id
					AND src.emp_denomination 			= dest.emp_denomination
					AND src.emp_record_type 			= dest.emp_record_type
					AND src.emp_sas_accounting_denom 	= dest.emp_sas_accounting_denom
			WHERE 	dest.emp_working_day 	IS NULL
				AND dest.emp_terminal_id 	IS NULL
				AND dest.emp_site_id 		IS NULL
				AND dest.emp_datetime 		IS NULL;
		RETURN 0;
	END TRY
	BEGIN CATCH
		EXEC dbo.spErrorHandling
	END CATCH
GO