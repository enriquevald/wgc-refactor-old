/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Get_TSMH_Period_Data'.
Returns all data from terminal_sas_meters_history table based on filter parameter datetime.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
	-- @param_datetime DATETIME
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]
	@param_datetime DATETIME = NULL
AS
	BEGIN
		SET NOCOUNT ON;
			-- Insert statements for procedure here
			DECLARE @query					NVARCHAR(MAX);
			DECLARE @site_id				INT;
			DECLARE @sas_meters_interval	INT;
			DECLARE @is_multisite			BIT;

		SET @is_multisite 			= 		 (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'MultiSite' 	AND gp_subject_key = 'IsCenter');
		SET @site_id 				= 		 (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Site' 		AND gp_subject_key = 'Identifier');
		SET @sas_meters_interval 	= ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' 		AND gp_subject_key = 'SASMetersInterval')
		, 60);
		
		SET @query = '	SELECT
			';
			
		IF (@is_multisite=1)
			SET @query += '	  tsmh_site_id';
		ELSE
		SET @query += '	  ' + CONVERT(VARCHAR(10), @site_id) + 'AS tsmh_site_id';
		SET @query += '
				, tsmh_terminal_id
				, tsmh_meter_code
				, tsmh_game_id
				, tsmh_denomination
				, tsmh_type
				, tsmh_datetime
				, tsmh_meter_ini_value
				, tsmh_meter_fin_value
				, tsmh_meter_increment
				, tsmh_raw_meter_increment
				, tsmh_last_reported
				, tsmh_sas_accounting_denom
				, tsmh_created_datetime
				, tsmh_group_id
				, tsmh_meter_origin
				, tsmh_meter_max_value 
			FROM 
				terminal_sas_meters_history 
					WITH (INDEX(IX_tsmh_datetime_type))'
		SET @query += 'WHERE tsmh_datetime >= DATEADD(MINUTE, -' + CONVERT(VARCHAR(10), @sas_meters_interval) + ', CONVERT(DATETIME, ''' + CONVERT(VARCHAR(1000), @param_datetime, 120) + '''))
				AND tsmh_datetime < CONVERT(DATETIME, ''' + CONVERT(VARCHAR(1000), @param_datetime, 120) + ''')
				AND tsmh_type IN (1, 10, 11, 12, 13, 14, 15, 16 , 110, 140, 150, 160) /*) :: Pending to Review with Dani.*/
				AND (tsmh_meter_origin IN (0, 1, 2) OR tsmh_meter_origin IS NULL)
			';
		EXEC sp_executesql @query
	END
GO