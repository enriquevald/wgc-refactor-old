/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'spErrorHandling'.
Is implemented to detect last error that has occurred and insert related information into ErrorHandling table.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:

*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spErrorHandling]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[spErrorHandling]
GO

CREATE PROCEDURE [dbo].[spErrorHandling] 
AS 
	-- Declaration statements
	DECLARE @Error_Number 		INT
	DECLARE @Error_Message 		VARCHAR(4000)
	DECLARE @Error_Severity 	INT
	DECLARE @Error_State 		INT
	DECLARE @Error_Procedure 	VARCHAR(200)
	DECLARE @Error_Line 		INT
	DECLARE @UserName 			VARCHAR(200)
	DECLARE @HostName 			VARCHAR(200)
	DECLARE @Time_Stamp 		DATETIME

-- Initialize variables
SELECT
	@Error_Number 		= ISNULL(ERROR_NUMBER(), 0)
   ,@Error_Message 		= ISNULL(ERROR_MESSAGE(), 'NULL Message')
   ,@Error_Severity 	= ISNULL(ERROR_SEVERITY(), 0)
   ,@Error_State 		= ISNULL(ERROR_STATE(), 1)
   ,@Error_Line 		= ISNULL(ERROR_LINE(), 0)
   ,@Error_Procedure 	= ISNULL(ERROR_PROCEDURE(), '')
   ,@UserName 			= SUSER_SNAME()
   ,@HostName 			= HOST_NAME()
   ,@Time_Stamp 		= GETDATE();

-- Insert into the dbo.ErrorHandling table (protecting if table is in the database to avoid errors).
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = 'ErrorHandling')
	INSERT INTO dbo.ErrorHandling (Error_Number, Error_Message, Error_Severity, Error_State, Error_Line, Error_Procedure, UserName, HostName, Time_Stamp)
		SELECT
			@Error_Number
		   ,@Error_Message
		   ,@Error_Severity
		   ,@Error_State
		   ,@Error_Line
		   ,@Error_Procedure
		   ,@UserName
		   ,@HostName
		   ,@Time_Stamp
GO