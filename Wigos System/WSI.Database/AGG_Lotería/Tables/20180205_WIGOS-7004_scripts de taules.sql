-------------------------------------------
-- Drop and create table EGM_CONTROL_MARK
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_control_mark]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[egm_control_mark](
      [ecm_control_mark] [DATETIME] NOT NULL 
	) ON [PRIMARY]
	GO 
/* Add default value as previous working day. */
IF ((SELECT COUNT(ecm_control_mark) FROM egm_control_mark)=0)
	INSERT INTO egm_control_mark VALUES(DATEADD(DAY, -1, dbo.Opening(0,GETDATE())))
-------------------------------------------
-- Drop and create table EGM_DAILY
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_daily]') AND TYPE IN (N'U'))	
	CREATE TABLE [dbo].[egm_daily](
		[ed_working_day] 					[INT] 		NOT NULL,
		[ed_site_id] 						[INT] 		NOT NULL,
		[ed_status] 						[TINYINT] 	NOT NULL,
		[ed_has_new_meters] 				[BIT] 		NOT NULL,
		[ed_terminals_connected]  			[INT]  		NOT NULL,
		[ed_terminals_number]  				[INT]  		NOT NULL,	
		[ed_last_updated_meters] 			[DATETIME] 		NULL,
		[ed_last_updated_user] 				[INT] 			NULL,
		[ed_last_updated_user_datetime] 	[DATETIME] 		NULL,
	  CONSTRAINT [PK_egm_daily] PRIMARY KEY CLUSTERED 
	  (
		[ed_working_day] 	ASC,
		[ed_site_id] 		ASC

	  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF OBJECT_ID('[dbo].[DF_Table_1_wb_status]') IS NOT NULL 
		ALTER TABLE [dbo].[egm_daily] DROP  CONSTRAINT [DF_Table_1_wb_status];
GO

IF OBJECT_ID('[dbo].[DF_Table_1_wb_status]') IS NULL 
		ALTER TABLE [dbo].[egm_daily] ADD   CONSTRAINT [DF_Table_1_wb_status]  			DEFAULT ((0)) FOR [ed_status];
GO		

IF OBJECT_ID('[dbo].[DF_Table_1_wb_has_new_counters]') IS NOT NULL 
		ALTER TABLE [dbo].[egm_daily] DROP  CONSTRAINT [DF_Table_1_wb_has_new_counters];
GO

IF OBJECT_ID('[dbo].[DF_Table_1_wb_has_new_counters]') IS NULL 
		ALTER TABLE [dbo].[egm_daily] ADD   CONSTRAINT [DF_Table_1_wb_has_new_counters]  			DEFAULT ((0)) FOR [ed_has_new_meters];
GO
	
-------------------------------------------
-- Drop and create table EGM_METERS_BY_DAY
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_day]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[egm_meters_by_day](
		[emd_working_day] 				[INT] 				NOT NULL,
		[emd_site_id] 					[INT] 				NOT NULL,
		[emd_terminal_id] 				[INT] 				NOT NULL,
		[emd_game_id]					[INT]				NOT NULL,	
		[emd_warning] 					[INT] 					NULL,
		[emd_user_period_modified] 		[BIT] 					NULL,
		[emd_user_validated] 			[BIT] 					NULL,
		[emd_user_validated_datetime]	[DATETIME] 				NULL,
		[emd_last_updated] 				[DATETIME] 				NULL,
		[emd_last_updated_user_id] 		[INT] 					NULL,	
		[emd_connected]  				[BIT]  				NOT NULL,
		[emd_status]					[INT]				NOT NULL,
		[emd_mc_0000_increment]			[DECIMAL](20,2) 	NOT NULL,
		[emd_mc_0001_increment] 		[DECIMAL](20,2) 	NOT NULL,
		[emd_mc_0002_increment] 		[DECIMAL](20,2) 	NOT NULL,
		[emd_mc_0005_increment] 		[BIGINT] 			NOT NULL,
		[emd_mc_0000] 					[BIGINT] 			NOT NULL,
		[emd_mc_0001] 					[BIGINT] 			NOT NULL,
		[emd_mc_0002] 					[BIGINT] 			NOT NULL,
		[emd_mc_0005] 					[BIGINT] 			NOT NULL,
	  CONSTRAINT [PK_egm_meters_by_day] PRIMARY KEY CLUSTERED 
	  (
		[emd_working_day] 	ASC,
		[emd_site_id] 		ASC,
		[emd_terminal_id] 	ASC

	  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO
	

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_warning]') IS NOT NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] DROP  CONSTRAINT [DF_egm_meters_by_day2_emd_warning];
GO

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_warning]') IS NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] ADD   CONSTRAINT [DF_egm_meters_by_day2_emd_warning]  			DEFAULT ((0)) FOR [emd_warning];
GO

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_user_period_modified]') IS NOT NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] DROP  CONSTRAINT [DF_egm_meters_by_day2_emd_user_period_modified];
GO

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_user_period_modified]') IS NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] ADD   CONSTRAINT [DF_egm_meters_by_day2_emd_user_period_modified]  			DEFAULT ((0)) FOR [emd_user_period_modified];
GO

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_user_validated]') IS NOT NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] DROP  CONSTRAINT [DF_egm_meters_by_day2_emd_user_validated];
GO

IF OBJECT_ID('[dbo].[DF_egm_meters_by_day2_emd_user_validated]') IS NULL 
		ALTER TABLE [dbo].[egm_meters_by_day] ADD   CONSTRAINT [DF_egm_meters_by_day2_emd_user_validated]  			DEFAULT ((0)) FOR [emd_user_validated];
GO

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_day]') and name = 'emd_mc_0000_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_day ALTER COLUMN emd_mc_0000_increment DECIMAL(20,2)
GO

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_day]') and name = 'emd_mc_0001_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_day ALTER COLUMN emd_mc_0001_increment DECIMAL(20,2)
GO

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_day]') and name = 'emd_mc_0002_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_day ALTER COLUMN emd_mc_0002_increment DECIMAL(20,2)
GO

-------------------------------------------
-- Drop and create table EGM_METERS_BY_PERIOD
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_period]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[egm_meters_by_period](
		[emp_working_day] 					[INT] 				NOT NULL,
		[emp_site_id] 						[INT] 				NOT NULL,
		[emp_terminal_id] 					[INT] 				NOT NULL,
		[emp_game_id] 						[INT] 				NOT NULL,
		[emp_denomination] 					[MONEY] 			NOT NULL,
		[emp_datetime] 						[DATETIME] 			NOT NULL,
		[emp_record_type] 					[INT] 				NOT NULL,
		[emp_sas_accounting_denom] 			[MONEY] 				NULL,
		[emp_last_created_datetime] 		[DATETIME] 			NOT NULL,
		[emp_last_reported_datetime] 		[DATETIME] 			NOT NULL,
		[emp_user_ignored] 					[BIT] 				NOT NULL,
		[emp_user_ignored_datetime] 		[DATETIME] 				NULL,
		[emp_last_updated_user_id] 			[INT] 					NULL,
		[emp_last_updated_user_datetime]	[DATETIME]				NULL,
		[emp_status]						[INT]				NOT NULL,	
		[emp_mc_0000_increment]				[DECIMAL](20,2) 	NOT NULL,
		[emp_mc_0001_increment] 			[DECIMAL](20,2) 	NOT NULL,
		[emp_mc_0002_increment] 			[DECIMAL](20,2) 	NOT NULL,
		[emp_mc_0005_increment] 			[BIGINT] 			NOT NULL,
		[emp_mc_0000] 						[BIGINT] 			NOT NULL,
		[emp_mc_0001] 						[BIGINT] 			NOT NULL,
		[emp_mc_0002]		 				[BIGINT] 			NOT NULL,
		[emp_mc_0005]		 				[BIGINT] 			NOT NULL,
		CONSTRAINT [PK_egm_meters_by_period] PRIMARY KEY CLUSTERED 
	  (
		[emp_working_day] 	ASC,
		[emp_site_id] 		ASC,
		[emp_terminal_id] 	ASC,
		[emp_game_id] 		ASC,
		[emp_denomination] 	ASC,
		[emp_datetime] 		ASC,
		[emp_record_type] 	ASC
	  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO
	

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_period]') and name = 'emp_mc_0000_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_period ALTER COLUMN emp_mc_0000_increment DECIMAL(20,2)
GO

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_period]') and name = 'emp_mc_0001_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_period ALTER COLUMN emp_mc_0001_increment DECIMAL(20,2)
GO

IF NOT EXISTS(SELECT system_type_id FROM  sys.columns WHERE  object_id = object_id(N'[dbo].[egm_meters_by_period]') and name = 'emp_mc_0002_increment' AND system_type_id = 106)
		ALTER TABLE egm_meters_by_period ALTER COLUMN emp_mc_0002_increment DECIMAL(20,2)
GO
	
-------------------------------------------
-- Drop and create table EGM_METERS_MAX_VALUES
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_max_values]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[egm_meters_max_values](
			[emmv_site_id]			[BIGINT] 		NOT NULL
		,	[emmv_terminal_id]		[INT]			NOT NULL
		,	[emmv_meter_code]		[INT]			NOT NULL
		,	[emmv_game_id]			[INT]			NOT NULL
		,	[emmv_denomination]		[MONEY]			NOT NULL
		,	[emmv_type]				[INT]			NOT NULL
		,	[emmv_datetime]			[DATETIME] 		NOT NULL
		,	[emmv_max_value]		[BIGINT] 		    NULL
		 CONSTRAINT [PK_egm_meters_max_values] PRIMARY KEY CLUSTERED 
		(
				[emmv_site_id]	
			,	[emmv_terminal_id]
			,	[emmv_meter_code]
			,	[emmv_game_id]	
			,	[emmv_denomination]
			,	[emmv_type]		
			,	[emmv_datetime] 
		)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	GO
-------------------------------------------
-- Drop and create table ErrorHandling
-------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ErrorHandling]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[ErrorHandling](
		[pkErrorHandlingID] [INT] 			IDENTITY(1,1) 		NOT NULL,
		[Error_Number] 		[INT] 								NOT NULL,
		[Error_Message] 	[VARCHAR](4000) 						NULL,
		[Error_Severity] 	[SMALLINT] 							NOT NULL,
		[Error_State] 		[SMALLINT] 							NOT NULL	DEFAULT 1,
		[Error_Procedure] 	[VARCHAR](200) 						NOT NULL,
		[Error_Line] 		[INT] 								NOT NULL	DEFAULT 0,
		[UserName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[HostName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[Time_Stamp] 		[DATETIME] 							NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[pkErrorHandlingID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO

/* 
**********************************************************
**********************************************************
					TABLE INDEXES
**********************************************************
**********************************************************
 */ 
/* Index: 'IX_ed_workingday_site' for table 'egm_daily'. */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_daily]') AND name = N'IX_ed_workingday_site')
	CREATE UNIQUE NONCLUSTERED INDEX [IX_ed_workingday_site] ON [dbo].[egm_daily] 
	(
		[ed_working_day] ASC,
		[ed_site_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO

/* Index: 'IX_emd_workingday_site_terminal' for table 'egm_meters_by_day'. */
IF NOT  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_day]') AND name = N'IX_emd_workingday_site_terminal')
	CREATE UNIQUE NONCLUSTERED INDEX [IX_emd_workingday_site_terminal] ON [dbo].[egm_meters_by_day] 
	(
		[emd_working_day] ASC,
		[emd_site_id] ASC,
		[emd_terminal_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO

/* Index: 'IX_emp_workingday_site_terminal_datetime' for table 'egm_meters_by_period'. */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_period]') AND name = N'IX_emp_workingday_site_terminal_datetime')
	CREATE NONCLUSTERED INDEX [IX_emp_workingday_site_terminal_datetime] ON [dbo].[egm_meters_by_period] 
	(
		[emp_working_day] ASC,
		[emp_site_id] ASC,
		[emp_terminal_id] ASC,
		[emp_datetime] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	GO

