USE [wgdb_000]
GO

DECLARE AccountCursor CURSOR
FOR
SELECT  ac_account_id
      , ac_balance
      , ac_promo_balance
      , ac_initial_not_redeemable
      , ac_cash_in
      , ac_cash_won
      , ac_promo_limit
      , ac_promo_expiration
FROM      accounts
WHERE ( ac_promo_balance > 0
  AND   ac_promo_limit > 0
  AND   ac_promo_expiration > GETDATE() )
   OR ( ac_initial_not_redeemable > 0
  AND   ac_balance > 0 )

DECLARE @tmp_ac_account_id       AS bigint
DECLARE @tmp_ac_balance          AS money
DECLARE @tmp_ac_promo_balance    AS money
DECLARE @tmp_ac_initial_not_redeemable AS money
DECLARE @tmp_ac_cash_in          AS money
DECLARE @tmp_ac_cash_won         AS money
DECLARE @tmp_ac_promo_limit      AS money
DECLARE @tmp_ac_promo_expiration AS Datetime

DECLARE @current_promo_id        AS bigint


IF NOT EXISTS (SELECT * FROM promotions WHERE pm_name = 'Antiguas Promociones')
INSERT INTO [dbo].[promotions]
           ([pm_name]
           ,[pm_enabled]
           ,[pm_type]
           ,[pm_date_start]
           ,[pm_date_finish]
           ,[pm_schedule_weekday]
           ,[pm_schedule1_time_from]
           ,[pm_schedule1_time_to]
           ,[pm_schedule2_enabled]
           ,[pm_schedule2_time_from]
           ,[pm_schedule2_time_to]
           ,[pm_gender_filter]
           ,[pm_birthday_filter]
           ,[pm_expiration_type]
           ,[pm_expiration_value]
           ,[pm_min_cash_in]
           ,[pm_min_cash_in_reward]
           ,[pm_cash_in]
           ,[pm_cash_in_reward]
           ,[pm_won_lock]
           ,[pm_num_tokens]
           ,[pm_token_name]
           ,[pm_token_reward])
     VALUES
           ('Antiguas Promociones'
           ,0
           ,0
           ,GETDATE()
           ,GETDATE()
           ,127
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,1
           ,1
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,0)
           
 SELECT @current_promo_id = pm_promotion_id
   FROM promotions
  WHERE pm_name           = 'Antiguas Promociones'
 
OPEN AccountCursor

-- FETCH FIRST ACCOUNT
FETCH AccountCursor INTO @tmp_ac_account_id     
				       , @tmp_ac_balance        
				       , @tmp_ac_promo_balance  
			           , @tmp_ac_initial_not_redeemable
					   , @tmp_ac_cash_in        
					   , @tmp_ac_cash_won 
					   , @tmp_ac_promo_limit     
					   , @tmp_ac_promo_expiration

WHILE @@Fetch_Status = 0
  BEGIN

    -- UPDATE PROMO'S
    IF ( @tmp_ac_promo_balance > 0 )
    BEGIN 

      UPDATE ACCOUNTS SET ac_balance                = ac_balance + @tmp_ac_promo_balance
                        , ac_nr_expiration          = @tmp_ac_promo_expiration
                        , ac_nr_won_lock            = @tmp_ac_promo_limit
                        , ac_current_promotion_id   = @current_promo_id
				    WHERE ac_account_id           = @tmp_ac_account_id

	 -- SELECT @tmp_ac_account_id, @tmp_ac_promo_balance, @tmp_ac_promo_expiration

    END

    -- UPDATE NOT REDEEMABLE'S
    IF ( @tmp_ac_initial_not_redeemable > 0 )
    BEGIN
      
      UPDATE ACCOUNTS SET ac_current_promotion_id = @current_promo_id
				    WHERE ac_account_id           = @tmp_ac_account_id

	  -- SELECT @tmp_ac_account_id, @tmp_ac_initial_not_redeemable

    END

    -- FETCH NEXT RESULT
    FETCH AccountCursor INTO @tmp_ac_account_id     
				       , @tmp_ac_balance        
				       , @tmp_ac_promo_balance  
			           , @tmp_ac_initial_not_redeemable
					   , @tmp_ac_cash_in        
					   , @tmp_ac_cash_won 
					   , @tmp_ac_promo_limit     
					   , @tmp_ac_promo_expiration

  END

CLOSE AccountCursor

DEALLOCATE AccountCursor
