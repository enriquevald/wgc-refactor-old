USE [wgdb_000]
GO


--
--
--  Script actualizaci�n SPLITS para TAPACHULA
--
--  Informaci�n:
--  - El script creara 2 movimientos de caja en concepto de "Uso de Sala" y "Dep�sitos de juego" 
--    por cada movimiento de "Recarga" "Recarga desde Banco M�vil" y "devoluciones"
--  - La cantidad de dinero a�adido o restado est� definido por un por un porcentaje (20.00% por defecto)
--
--  Instrucciones:
--  - Definir el porcentaje:
--      - Buscar la l�nea con el c�digo: "SET @percentage_cashin = 20.00" (l�nea 100)
--      - Usar el c�digo seg�n TAPACHULA o MIRAVALLE (comentar/descomentar)
--  - Ejecutar el script:
--      - Aparecer�n dos filas de resultados:
--        La primera son los valores ANTES de ejecutar el script
--        La segunda son los valores DESPU�S de ejecutar el script
--      
--      - Significado de las columnas:
--        CASHIER CASH_IN: Suma total del Cash In seg�n el cajero
--        ACCOUNTS CASH_IN: Suma total del Cash In seg�n los movimientos de cuenta
--        DIFF CASH IN: Diferencia de los dos valores, deber�a ser 0.
--        CASH_IN_SPLIT_1: Suma total de los splits_1 de Cash In
--        CASH_IN_SPLIT_2: Suma total de los splits_2 de Cash In
--        MB_CASH_IN_SPLIT_1: Suma total de los splits_1 de Cash In desde Banco M�vil
--        MB_CASH_IN_SPLIT_2: Suma total de los splits_2 de Cash In desde Banco M�vil
--        DIFF SPLITS: Diferencia entre la suma de todos los splits y el valor inicial de Cash In. 
--                     Deber�a ser 0 en la segunda fila de resultados.
--        ACCOUNTS DEV: Suma total de las Devoluciones seg�n los movimientos de cuenta.
--        DEV_SPLIT_1: Suma total de los splits_1 de Devoluciones
--        DEV_SPLIT_2: Suma total de los splits_2 de Devoluciones
--        DIFF SPLITS: Diferencia entre la suma de todos los splits y el valor inicial de Devoluciones. 
--                     Deber�a ser 0 en la segunda fila de resultados.
--                     
--  - COMMIT O ROLLBACK:
--    - Si los valores de la columna DIFF SPLITS de la segunda fila son 0, significa que el script ha ejecutado correctamente.
--      Por lo tanto, se puede hacer COMMIT.
--    - Si los valores de la columna DIFF SPLITS de la segunda fila no son 0, significa que el script o la base de datos han tenido problemas.
--      Se recomienda hacer ROLLBACK en caso de duda.
--       


DROP PROCEDURE ShowCashInOutSplits
GO

CREATE PROCEDURE [dbo].[ShowCashInOutSplits]
  @DateStart AS datetime,
  @DateEnd   AS datetime  
AS
BEGIN
	DECLARE @cash_in            money
	DECLARE @cash_in_split_1    money
	DECLARE @cash_in_split_2    money
	DECLARE @mb_cash_in         money
	DECLARE @mb_cash_in_split_1 money
	DECLARE @mb_cash_in_split_2 money
	DECLARE @cash_out           money
	DECLARE @dev_split_1        money
	DECLARE @dev_split_2        money
DECLARE @cash_in_account money
	
	SELECT   @cash_in            = SUM(CASE WHEN CM_TYPE IN (4, 13) THEN CM_ADD_AMOUNT ELSE 0 END)
		   , @cash_in_split_1    = SUM(CASE WHEN CM_TYPE IN (37) THEN CM_ADD_AMOUNT ELSE 0 END)
		   , @cash_in_split_2    = SUM(CASE WHEN CM_TYPE IN (34) THEN CM_ADD_AMOUNT ELSE 0 END)
--			, @mb_cash_in         = SUM(CASE WHEN CM_TYPE IN (13) THEN CM_ADD_AMOUNT ELSE 0 END)
			, @mb_cash_in_split_1 = SUM(CASE WHEN CM_TYPE IN (39) THEN CM_ADD_AMOUNT ELSE 0 END)
			, @mb_cash_in_split_2 = SUM(CASE WHEN CM_TYPE IN (35) THEN CM_ADD_AMOUNT ELSE 0 END)
		   , @cash_out           = SUM(CASE WHEN CM_TYPE IN (5) THEN CM_SUB_AMOUNT ELSE 0 END)
		   , @dev_split_1        = SUM(CASE WHEN CM_TYPE IN (38) THEN CM_SUB_AMOUNT ELSE 0 END)
		   , @dev_split_2        = SUM(CASE WHEN CM_TYPE IN (36) THEN CM_SUB_AMOUNT ELSE 0 END)
	 FROM    cashier_movements 
	 WHERE  cm_date >= @DateStart
	   AND  cm_date <  @DateEnd
       AND  cm_date < '20-10-2010 8:00'

	SELECT @cash_in_account     = SUM(CASE WHEN AM_TYPE IN (1) THEN AM_ADD_AMOUNT ELSE 0 END)
		 , @cash_out 	        = SUM(CASE WHEN AM_TYPE IN (3) THEN AM_SUB_AMOUNT ELSE 0 END)
     FROM account_movements
	 WHERE  am_datetime >= @DateStart
	   AND  am_datetime <  @DateEnd
       AND  am_datetime < '20-10-2010 8:00'

	SELECT @cash_in  AS 'CASHIER CASH_IN'
		 , @cash_in_account AS 'ACCOUNTS CASH_IN'
		 , - @cash_in + (@cash_in_account) AS 'DIFF CASH IN'
		 , @cash_in_split_1 AS CASH_IN_SPLIT_1
		 , @cash_in_split_2  AS CASH_IN_SPLIT_2
		 , @mb_cash_in_split_1 AS MB_CASH_IN_SPLIT_1
		 , @mb_cash_in_split_2 AS MB_CASH_IN_SPLIT_2
		 , - @cash_in_account + (@cash_in_split_1 + @cash_in_split_2 + @mb_cash_in_split_1 + @mb_cash_in_split_2) AS 'DIFF SPLITS'
		 , @cash_out AS 'ACCOUNTS DEV'
		 , @dev_split_1 AS DEV_SPLIT_1
		 , @dev_split_2 AS DEV_SPLIT_2
		 , - @cash_out + (@dev_split_1 + @dev_split_2) AS 'DIFF SPLITS'

END

GO
                            

DECLARE @percentage_cashin float
DECLARE @percentage_dev float
DECLARE @date_start datetime
DECLARE @date_end datetime

-- TAPACHULA
SET @percentage_cashin = 20.00
SET @percentage_dev    = 20.00

-- MIRAVALLE
-- SET @percentage_cashin = 10.00
-- SET @percentage_dev    = 0.00

-- FROM DATE TO DATE
SET @date_start = '1-10-2010 08:00'
SET @date_end   = '20-10-2010 08:00'

-- INITIAL STATUS
EXEC ShowCashInOutSplits @date_start, @date_end

-- OPERATIONS - Itera por cada movimiento y ejecuta UPDATE e INSERT

-- BEGIN TRANSACTION

-- declare the cursor

DECLARE AccountMovement CURSOR
FOR
SELECT  am_movement_id
      , am_datetime
      , am_type
      , am_initial_balance
      , am_sub_amount
      , am_add_amount
      , am_final_balance
      , am_cashier_id
      , am_cashier_name
      , am_account_id
FROM      account_movements
WHERE  AM_TYPE IN (1, 3) -- 1: Cash In; 3: Devolution
and AM_DATETIME < '20-10-2010 8:00'
and AM_DATETIME >= @date_start
and AM_DATETIME <  @date_end


DECLARE @tmp_am_movement_id         bigint
DECLARE @tmp_am_datetime            datetime
DECLARE @tmp_am_type                int
DECLARE @tmp_am_initial_balance     money
DECLARE @tmp_am_sub_amount          money
DECLARE @tmp_am_add_amount          money
DECLARE @tmp_am_final_balance       money
DECLARE @tmp_am_cashier_id          int
DECLARE @tmp_am_cashier_name        nvarchar(50)
DECLARE @tmp_am_account_id          bigint

-- WE NEED TO GUESS
DECLARE @tmp_cm_session_id          bigint
DECLARE @tmp_cm_user_id             int
DECLARE @tmp_cm_user_name           nvarchar(50)
DECLARE @tmp_cm_type                int
DECLARE @tmp_cm_money_type          int
DECLARE @tmp_cm_card_track_data     nvarchar(50)

-- SPLITS
DECLARE @tmp_cm_sub_amount1         money
DECLARE @tmp_cm_add_amount1         money

DECLARE @tmp_cm_sub_amount2         money
DECLARE @tmp_cm_add_amount2         money

OPEN AccountMovement

FETCH AccountMovement INTO @tmp_am_movement_id     
						 , @tmp_am_datetime        
						 , @tmp_am_type            
						 , @tmp_am_initial_balance 
						 , @tmp_am_sub_amount      
						 , @tmp_am_add_amount      
						 , @tmp_am_final_balance   
						 , @tmp_am_cashier_id      
						 , @tmp_am_cashier_name    
						 , @tmp_am_account_id      

-- start the main processing loop.

WHILE @@Fetch_Status = 0
  BEGIN

	-- CALCULATE SPLIT VALUES
	SET @tmp_cm_add_amount2 = ROUND(@tmp_am_add_amount * @percentage_cashin / 100, 2)
	SET @tmp_cm_add_amount1 = @tmp_am_add_amount - @tmp_cm_add_amount2

	SET @tmp_cm_sub_amount2 = ROUND(@tmp_am_sub_amount * @percentage_dev / 100, 2)
	SET @tmp_cm_sub_amount1 = @tmp_am_sub_amount - @tmp_cm_sub_amount2
    
--      GET REST OF PARAMETERS
	
SELECT TOP 1 
           @tmp_cm_session_id      = cm_session_id
         , @tmp_cm_user_id         = cm_user_id
         , @tmp_cm_user_name       = cm_user_name
         , @tmp_cm_money_type	   = cm_money_type
         , @tmp_cm_card_track_data = cm_card_track_data
		     , @tmp_am_cashier_id      = cm_cashier_id  
		     , @tmp_am_cashier_name    = cm_cashier_name 
	    FROM cashier_movements
	   WHERE ABS(DATEDIFF(second, cm_date, @tmp_am_datetime)) <= 1
       AND cm_account_id = @tmp_am_account_id
  ORDER BY ABS(cm_cashier_id - ISNULL(@tmp_am_cashier_id,0))
	
	-- IF @tmp_am_cashier_id IS NULL means that is a Mobile Bank movement...
	
	IF (@tmp_am_cashier_id IS NULL) 
	BEGIN

		-- Search for MB movement
		SELECT TOP 1 
                 @tmp_cm_session_id      = cs_session_id
			   , @tmp_cm_user_id         = cs_user_id
			   , @tmp_cm_user_name       = ''
			   , @tmp_cm_money_type	     = 0
			   , @tmp_cm_card_track_data = mbm_player_trackdata
			   , @tmp_am_cashier_id      = cs_cashier_id
			   , @tmp_am_cashier_name    = ''
		 FROM mb_movements, cashier_sessions
		WHERE mbm_cashier_session_id = cs_session_id
		  AND mbm_acct_movement_id = @tmp_am_movement_id
		
		SET @tmp_am_type = 0 -- CASH IN MB

--		SELECT    @tmp_cm_session_id      
--				, @tmp_cm_user_id         
--				, @tmp_cm_user_name       
--				, @tmp_cm_money_type	   
--				, @tmp_cm_card_track_data 

	END

   -- INSERT SPLIT 1

	SET @tmp_cm_type = CASE WHEN @tmp_am_type IN (1)    THEN 37 -- CASH IN SPLIT1
					             		WHEN @tmp_am_type IN (3)    THEN 38 -- CASH OUT SPLIT 1           
												                              ELSE 39 END -- CASH IN MB SPLIT 1

	INSERT INTO cashier_movements
              ( cm_session_id
			        , cm_cashier_id
			        , cm_user_id
			        , cm_date
			        , cm_type
			        , cm_money_type
			        , cm_account_movement_id
			        , cm_initial_balance
			        , cm_sub_amount
			        , cm_add_amount
			        , cm_final_balance
			        , cm_user_name
			        , cm_cashier_name
			        , cm_card_track_data
			        , cm_account_id )
             VALUES (	@tmp_cm_session_id          
			        , @tmp_am_cashier_id          
			        , @tmp_cm_user_id             
			        , @tmp_am_datetime             
			        , @tmp_cm_type              
			        , @tmp_cm_money_type             
			        , @tmp_am_movement_id             
			        , @tmp_am_initial_balance     
			        , @tmp_cm_sub_amount1 -- SPLIT 1            
			        , @tmp_cm_add_amount1 -- SPLIT 1            
			        , @tmp_am_final_balance            
			        , @tmp_cm_user_name                 
			        , @tmp_am_cashier_name               
			        , @tmp_cm_card_track_data             
			        , @tmp_am_account_id )
			  
	-- INSERT SPLIT 2
  IF (( @percentage_dev > 0 AND @tmp_am_type = 3) OR (@tmp_am_type <> 3))
  BEGIN

    SET @tmp_cm_type = CASE WHEN @tmp_am_type IN (1)    THEN 34 -- CASH IN SPLIT 2
                            WHEN @tmp_am_type IN (3)    THEN 36 -- CASH OUT SPLIT 2      
                                                        ELSE 35 END -- CASH IN MB SPLIT 2


    
	  INSERT INTO cashier_movements
                ( cm_session_id
			          , cm_cashier_id
			          , cm_user_id
			          , cm_date
			          , cm_type
			          , cm_money_type
			          , cm_account_movement_id
			          , cm_initial_balance
			          , cm_sub_amount
			          , cm_add_amount
			          , cm_final_balance
			          , cm_user_name
			          , cm_cashier_name
			          , cm_card_track_data
			          , cm_account_id )
               VALUES (	@tmp_cm_session_id          
			          , @tmp_am_cashier_id          
			          , @tmp_cm_user_id             
			          , @tmp_am_datetime             
			          , @tmp_cm_type              
			          , @tmp_cm_money_type             
			          , @tmp_am_movement_id             
			          , @tmp_am_initial_balance     
			          , @tmp_cm_sub_amount2 -- SPLIT 2            
			          , @tmp_cm_add_amount2 -- SPLIT 2            
			          , @tmp_am_final_balance            
			          , @tmp_cm_user_name                 
			          , @tmp_am_cashier_name               
			          , @tmp_cm_card_track_data             
			          , @tmp_am_account_id )
  END
   -- Get the next row.

FETCH AccountMovement INTO @tmp_am_movement_id     
						 , @tmp_am_datetime        
						 , @tmp_am_type            
						 , @tmp_am_initial_balance 
						 , @tmp_am_sub_amount      
						 , @tmp_am_add_amount      
						 , @tmp_am_final_balance   
						 , @tmp_am_cashier_id      
						 , @tmp_am_cashier_name    
						 , @tmp_am_account_id           
        

   END

CLOSE AccountMovement

DEALLOCATE AccountMovement

-- COMPROBACION - Comprueba que los resultados concuerdan con los anteriores

EXEC ShowCashInOutSplits @date_start, @date_end 

DROP PROCEDURE ShowCashInOutSplits

-- Commit or Rollback as desired

-- ROLLBACK
 