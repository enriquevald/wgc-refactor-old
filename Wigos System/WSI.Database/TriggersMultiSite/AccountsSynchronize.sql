--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountsSynchronize.sql
-- 
--   DESCRIPTION: Procedures for trigger Trigger_SiteToMultiSite_Accounts and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 25-FEB-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 25-FEB-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when update an account.
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_SiteToMultiSite_Accounts]') AND type in (N'TR'))
DROP TRIGGER [dbo].[Trigger_SiteToMultiSite_Accounts]
GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Accounts]
ON [dbo].[ACCOUNTS]
AFTER UPDATE
NOT FOR REPLICATION
AS 
  BEGIN
    -- TODO: Check Multisite
    
    -- Updated locally?
    IF ( UPDATE (AC_MS_CHANGE_GUID) ) RETURN
    
    -- Personal Info has been updated?
    IF ( NOT ( UPDATE (AC_HOLDER_NAME) OR UPDATE (AC_TRACK_DATA) ) ) RETURN
    
    DECLARE @site_identifier    INT
    DECLARE @account_id         BIGINT
    DECLARE @new_guid           UNIQUEIDENTIFIER
    DECLARE modified_accounts   CURSOR FOR SELECT AC_ACCOUNT_ID FROM INSERTED

    SET NOCOUNT ON
    
    SET @site_identifier = (SELECT isNull(gp_key_value, 0) FROM general_params WHERE gp_group_key='Site' AND gp_subject_key='Identifier')
  
    OPEN modified_accounts;
    FETCH NEXT FROM modified_accounts INTO @account_id;

    WHILE @@FETCH_STATUS = 0
       BEGIN
          -- Change GUID 
          SET @new_guid = NEWID()
        
          -- Insert account to synchronize

          IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @account_id)
            BEGIN
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS 
                          ( SPA_ACCOUNT_ID
                          , SPA_GUID)
                   VALUES ( @account_id
                          , @new_guid )
            END
          ELSE
            BEGIN
              UPDATE   MS_SITE_PENDING_ACCOUNTS
                 SET   SPA_GUID       = @new_guid
               WHERE   SPA_ACCOUNT_ID = @account_id
            END
          
          --
          -- Update table accounts for synchronization control.
          -- 
          UPDATE   ACCOUNTS
             SET   AC_MS_HAS_LOCAL_CHANGES   = 1
                 , AC_MS_CHANGE_GUID         = @new_guid
                 , AC_MS_MODIFIED_ON_SITE_ID = @site_identifier
           WHERE   AC_ACCOUNT_ID             = @account_id
      
          FETCH NEXT FROM modified_accounts INTO @account_id;
       END;
       
    CLOSE modified_accounts;
    DEALLOCATE modified_accounts;

    SET NOCOUNT OFF

END -- Trigger_SiteToMultiSite_Accounts
GO

