--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountMovementsSynchronize.sql
-- 
--   DESCRIPTION: Procedures for trigger ACCOUNT_MOVEMENTS_SYNCHRONIZE and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 25-FEB-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 25-FEB-2013 JML    First release.
-- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNT_MOVEMENTS when insert new movements.
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--
-- Movements related with points:
--     
--   36, 'PointsAwarded'
--   37, 'PointsToGiftRequest'
--   38, 'PointsToNotRedeemable'
--   39, 'PointsGiftDelivery'
--   40, 'PointsExpired'
--   41, 'PointsToDrawTicketPrint'
--   42, 'PointsGiftServices'
--   43, 'HolderLevelChanged'
--   46, 'PointsToRedeemable'
--   50, 'CardAdjustment'
--   60, 'PromotionPoint'
--   61, 'CancelPromotionPoint'
--   62, 'ManualHolderLevelChanged'
--   66, 'CancelGiftInstance' 
--   67, 'PointsStatusChanged' 
--   68, 'ManuallyAddedPointsForLevel'
--   71, 'ImportedPointsOnlyForRedeem'
--   72, 'ImportedPointsForLevel'
--   73, ImportedPointsHistory'
--   84, 'CardReplacementInPoints'
--   86, 'ExternalSystemPointsSubstract'
--  101, 'MultiSiteCurrentLocalPoints'
-- 1101, 'MULTIPLE_BUCKETS_EndSession RedemptionPoints --PuntosCanje
-- 1201, 'MULTIPLE_BUCKETS_Expired    RedemptionPoints --PuntosCanje
-- 1301, 'MULTIPLE_BUCKETS_Manual_Add RedemptionPoints --PuntosCanje
-- 1401, 'MULTIPLE_BUCKETS_Manual_Sub RedemptionPoints --PuntosCanje
-- 1501, 'MULTIPLE_BUCKETS_Manual_Set RedemptionPoints --PuntosCanje
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_SiteToMultiSite_Points]') AND type in (N'TR'))
DROP TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
          WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 46, 50, 60, 61, 62, 66, 67, 68, 71, 72, 73, 84, 86, 101)
			 OR   AM_TYPE IN (1101, 1201, 1301, 1401, 1501)
   
    SET NOCOUNT OFF

  END -- [Trigger_SiteToMultiSite_Points]
GO
