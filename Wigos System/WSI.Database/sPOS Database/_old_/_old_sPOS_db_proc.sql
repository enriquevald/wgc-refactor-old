USE [master]
GO

/**** CHECK DATABASE EXISTENCE SECTION *****/
IF (EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
/**** FAILURE SECTION *****/
SELECT 'Database sPOS already exists.'

raiserror('Not updated', 20, -1) with log
END

/****** Object:  Database [sPOS]    Script Date: 05/18/2010 15:00:53 ******/
CREATE DATABASE [sPOS] ON  PRIMARY 
( NAME = N'sPOS', FILENAME = N'C:\Wigos\Database\sPOS.mdf' , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'sPOS_log', FILENAME = N'C:\Wigos\Database\sPOS_1.ldf' , MAXSIZE = UNLIMITED , FILEGROWTH = 10240KB )
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'sPOS', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sPOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sPOS] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [sPOS] SET ANSI_NULLS OFF
GO
ALTER DATABASE [sPOS] SET ANSI_PADDING OFF
GO
ALTER DATABASE [sPOS] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [sPOS] SET ARITHABORT OFF
GO
ALTER DATABASE [sPOS] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [sPOS] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [sPOS] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [sPOS] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [sPOS] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [sPOS] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [sPOS] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [sPOS] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [sPOS] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [sPOS] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [sPOS] SET  ENABLE_BROKER
GO
ALTER DATABASE [sPOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [sPOS] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [sPOS] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [sPOS] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [sPOS] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [sPOS] SET  READ_WRITE
GO
ALTER DATABASE [sPOS] SET RECOVERY FULL
GO
ALTER DATABASE [sPOS] SET  MULTI_USER
GO
ALTER DATABASE [sPOS] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [sPOS] SET DB_CHAINING OFF
GO
USE [sPOS]
GO
CREATE PROCEDURE [dbo].[zsp_AccountStatus]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pMachineNumber   int
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_AccountStatus  @pAccountID
                                                                   , @pVendorId
                                                                   , @pMachineNumber
END
GO
CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16),
  @pSerialNumber varchar(30),
  @pMachineNumber int,
  @pXmlMeters xml
  
AS
    execute vscage.wgdb_000.dbo.zsp_ReportMeters 
      @pVendorId ,
      @pSerialNumber ,
      @pMachineNumber ,
      @pXmlMeters
GO
CREATE PROCEDURE [dbo].[zsp_SendEvent]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pEventID         bigint, 
	@pPayoutAmt       money
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SendEvent  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber  
                                        , @pEventID        
                                        , @pPayoutAmt  
END
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId 	    bigint,
  @pCreditsPlayed   money,
  @pCreditsWon      money,
  @pGamesPlayed    int,
  @pGamesWon       int,
  @pCreditBalance  money,
  @pCurrentJackpot money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionEnd  @pAccountID
                                     , @pVendorId
                                     , @pSerialNumber
                                     , @pMachineNumber
                                     , @pSessionId 
                                     , @pCreditsPlayed
                                     , @pCreditsWon 
                                     , @pGamesPlayed 
                                     , @pGamesWon  
                                     , @pCreditBalance
                                     , @pCurrentJackpot
									 , 0
END
GO
CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@pAccountID 	    varchar(24),
	@pVendorId 	      varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pCurrentJackpot  money = 0,
	@pVendorSessionId bigint = 0,
	@pGameTitle       varchar(50) = '',
	@pDummy           int = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionStart  @pAccountID
                                       , @pVendorId
                                       , @pSerialNumber
                                       , @pMachineNumber
                                       , @pCurrentJackpot
                                       , @pVendorSessionId
END
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId      bigint,
  @pCreditsPlayed  money,
  @pCreditsWon     money,
  @pGamesPlayed    int,
  @pGamesWon        int,
  @pCreditBalance   money,
  @pCurrentJackpot  money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionUpdate  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber
                                        , @pSessionId 
                                        , @pCreditsPlayed
                                        , @pCreditsWon 
                                        , @pGamesPlayed 
                                        , @pGamesWon  
                                        , @pCreditBalance
                                        , @pCurrentJackpot
										, 0
END
GO