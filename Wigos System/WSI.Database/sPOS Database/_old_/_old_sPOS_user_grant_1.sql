/****** Object:  Login [wg_interface]    Script Date: 05/10/2010 09:33:20 ******/

USE [sPOS]
GO

/****** Object:  Login [wg_interface]    Script Date: 05/12/2010 14:29:39 ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface')
DROP LOGIN [wg_interface]
GO

CREATE LOGIN [wg_interface] WITH PASSWORD=N'wg_interface_pwd', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0x70A641208FA0834A9DBA6F1F09E84B2D
GO
ALTER LOGIN [wg_interface] ENABLE
GO

/****** Object:  Login [EIBE]    Script Date: 05/12/2010 14:29:39 ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE')
DROP LOGIN [EIBE]
GO

CREATE LOGIN [EIBE] WITH PASSWORD=N'EIBE', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0x6FD4AFBDF7F55342B507927F575A4B57
GO
ALTER LOGIN [EIBE] ENABLE
GO

/****** Object:  Login [3GS]  ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'3GS')
DROP LOGIN [3GS]
GO

CREATE LOGIN [3GS] WITH PASSWORD=N'3GS_PWD', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0xAE48EE0F60BD3C49B6AFA6D0E231A791
GO
ALTER LOGIN [3GS] ENABLE
GO
