
USE [sPOS]
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionEnd')
       DROP PROCEDURE zsp_SessionEnd
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId 	    bigint,
  @pCreditsPlayed   money,
  @pCreditsWon      money,
  @pGamesPlayed    int,
  @pGamesWon       int,
  @pCreditBalance  money,
  @pCurrentJackpot money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionEnd  @pAccountID
                                     , @pVendorId
                                     , @pSerialNumber
                                     , @pMachineNumber
                                     , @pSessionId 
                                     , @pCreditsPlayed
                                     , @pCreditsWon 
                                     , @pGamesPlayed 
                                     , @pGamesWon  
                                     , @pCreditBalance
                                     , @pCurrentJackpot
									 , 0
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionUpdate')
       DROP PROCEDURE zsp_SessionUpdate
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId      bigint,
  @pCreditsPlayed  money,
  @pCreditsWon     money,
  @pGamesPlayed    int,
  @pGamesWon        int,
  @pCreditBalance   money,
  @pCurrentJackpot  money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionUpdate  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber
                                        , @pSessionId 
                                        , @pCreditsPlayed
                                        , @pCreditsWon 
                                        , @pGamesPlayed 
                                        , @pGamesWon  
                                        , @pCreditBalance
                                        , @pCurrentJackpot
										, 0
END
GO

-- [3GS]
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION 
GO
