/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

--
-- Script only for winpot!
--

UPDATE   PROMOTIONS
   SET   PM_NAME = 'PREMIOS EN PUNTOS DISPONIBLES'
 WHERE   PM_TYPE = 19 -- 
GO

UPDATE   general_params SET gp_key_value = 'Dep�sito disponibles para apuestas' 
 WHERE   gp_group_key = 'Cashier' and gp_subject_key = 'Tax.OnPrizeInKind.1.Name';
GO
 
UPDATE   general_params SET gp_key_value = '1' 
 WHERE   gp_group_key = 'Cashier' and gp_subject_key = 'Tax.OnPrizeInKind.SetNegative';
GO

--series
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sequence_saved]') AND type in (N'U'))
BEGIN
   
   SELECT *
   INTO SEQUENCE_SAVED
   FROM SEQUENCES
   
   UPDATE SEQUENCES
      SET SEQ_NEXT_VALUE = 0
    WHERE SEQ_ID IN (1, 2)

END
GO