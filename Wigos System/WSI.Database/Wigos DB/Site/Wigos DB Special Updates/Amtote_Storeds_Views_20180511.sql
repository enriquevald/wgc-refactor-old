/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[VIEW_AMTOTE_PlayerInfo]    Script Date: 05/11/2018 4:36:45 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_AMTOTE_PlayerInfo]') AND TYPE IN (N'V'))
	DROP VIEW [dbo].[VIEW_AMTOTE_PlayerInfo]
GO

CREATE VIEW [dbo].[VIEW_AMTOTE_PlayerInfo]
AS
SELECT	CAST (AC_EXTERNAL_REFERENCE as INT) PlayerId,
		AC_HOLDER_NAME1  as LastName, 
		AC_HOLDER_NAME3  as FirstName, 
		AC_HOLDER_NAME2  as MiddleName, 
		CASE AC_HOLDER_GENDER WHEN 1 THEN 'M'  
								WHEN 2 THEN 'F'  
								ELSE NULL END as Gender,
		CASE WHEN (AC_BLOCKED = 0) THEN 1 ELSE 0 END AS Enabled,
		CAST (AC_HOLDER_BIRTH_DATE as DATE) as Birthdate,
		AC_HOLDER_PHONE_NUMBER_02           as Phone,
		AC_HOLDER_PHONE_NUMBER_01           as Mobile,
		CAST (AC_CREATED as DATE)           as EnrollDate,
		AC_TRACK_DATA                       as Track1,
		AC_ACCOUNT_ID                       as WigosID
FROM accounts
WHERE ISNUMERIC(AC_EXTERNAL_REFERENCE)=1
GO

/****** Object:  View [dbo].[VIEW_AMTOTE_PlayerPoints]    Script Date: 05/11/2018 4:36:55 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_AMTOTE_PlayerPoints]') AND TYPE IN (N'V'))
	DROP VIEW [dbo].[VIEW_AMTOTE_PlayerPoints]
GO

CREATE VIEW [dbo].[VIEW_AMTOTE_PlayerPoints]
AS
SELECT	CASE WHEN ISNUMERIC(AC_EXTERNAL_REFERENCE) = 1 THEN CAST(AC_EXTERNAL_REFERENCE AS INT) ELSE NULL END AS PlayerID
		, CAST(ISNULL(FLOOR(cbu_value),0) AS INT) Points
FROM ACCOUNTS
	LEFT JOIN customer_bucket ON ac_account_id = cbu_customer_id and cbu_bucket_id = 1
WHERE ISNUMERIC(ac_external_reference) = 1
GO

/****** Object:  View [dbo].[VIEW_AMTOTE_Plays]    Script Date: 05/11/2018 4:37:09 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_AMTOTE_Plays]') AND TYPE IN (N'V'))
	DROP VIEW [dbo].[VIEW_AMTOTE_Plays]
GO

CREATE VIEW [dbo].[VIEW_AMTOTE_Plays]
AS
SELECT   CASE WHEN ISNUMERIC(AC_EXTERNAL_REFERENCE) = 1 THEN CAST(AC_EXTERNAL_REFERENCE AS INT) ELSE NULL END AS PlayerID
         , PL_DATETIME              AS PlayTime
         , PL_GAME_ID               AS GameTheme
	     , PL_PLAYED_AMOUNT         AS BetAmount
	     , 0                        AS PromotionalAmount
	     , PL_WON_AMOUNT            AS Win
	     , 0                        AS Jackpot
	     , PL_PLAY_ID               AS PlayID
	     , PL_PLAY_SESSION_ID       AS SessionID
FROM PLAYS WITH (INDEX (IX_Plays))
	INNER JOIN PLAY_SESSIONS ON PS_PLAY_SESSION_ID = PL_PLAY_SESSION_ID AND PS_STARTED >= DATEADD(DAY, -1, DBO.OPENING(0,GETDATE()))
	INNER JOIN ACCOUNTS      ON PS_ACCOUNT_ID      = AC_ACCOUNT_ID
GO

/****** Object:  View [dbo].[VIEW_AMTOTE_PlaySessions]    Script Date: 05/11/2018 4:37:20 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_AMTOTE_PlaySessions]') AND TYPE IN (N'V'))
	DROP VIEW [dbo].[VIEW_AMTOTE_PlaySessions]
GO

CREATE VIEW [dbo].[VIEW_AMTOTE_PlaySessions]
AS
SELECT	PS_PLAY_SESSION_ID AS SessionID
		, CASE WHEN ISNUMERIC(AC_EXTERNAL_REFERENCE) = 1 THEN CAST(AC_EXTERNAL_REFERENCE AS INT) ELSE NULL END AS PlayerID
		, CAST (dbo.Opening(0,PS_STARTED) AS DATE) AS AccountingDate
  	    , PS_STARTED               AS StartTime
	    , PS_FINISHED              AS EndTime
        , TE_NAME                           AssetNumber
		, BK_NAME                           Bank
		, ISNULL(TE_FLOOR_ID,'---')         FloorId
		, ISNULL(TE_GAME_THEME, N'Not Set') GameTheme
		, PS_PLAYED_COUNT          AS Games
		, PS_REDEEMABLE_PLAYED     AS CoinIn
		, PS_REDEEMABLE_WON - PS_REDEEMABLE_PLAYED       AS Win
		, ROUND((1-ISNULL(te_theoretical_payout, 0.95)) * PS_REDEEMABLE_PLAYED, 2) TheoWin
		, CAST(0 AS MONEY)         AS Jackpot
		, PS_NON_REDEEMABLE_PLAYED AS XC_Used
		, 0                        AS PTP_SPUsedCents
		, NULL                     AS PointsEarned
		, NULL                     AS BasePoints
		, NULL                     AS Multiplier
FROM PLAY_SESSIONS WITH (INDEX (IX_ps_finished_status))
	INNER JOIN ACCOUNTS  ON PS_ACCOUNT_ID = AC_ACCOUNT_ID AND AC_TYPE = 2 -- With Card
	INNER JOIN TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID
	LEFT  JOIN BANKS     ON BANKS.BK_BANK_ID = TE_BANK_ID 
WHERE PS_FINISHED >=  DATEADD(DAY, -30, dbo.Opening(0,GETDATE()))
	AND PS_STATUS <> 0 -- Not opened
	AND PS_PLAYED_AMOUNT > 0
GO

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_QueryPoints]    Script Date: 05/11/2018 4:38:20 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_QueryPoints]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_QueryPoints]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_QueryPoints]
	@PlayerId  INT,
	@Points INT OUTPUT
AS
BEGIN
	SET @Points = NULL

	declare @AccountId		BIGINT
	select @AccountId = ac_account_id from accounts where ac_external_reference = CAST (@PlayerId AS NVARCHAR(50))
	IF @AccountId IS NULL RETURN

	SELECT @Points = FLOOR(cbu_value) from customer_bucket WHERE cbu_customer_id = @AccountId and cbu_bucket_id = 1
END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_QueryPoints] TO wggui;

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_AddPoints]    Script Date: 05/11/2018 4:37:39 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_AddPoints]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_AddPoints]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_AddPoints]
	@PlayerId  INT,
	@AddPoints INT,
	@Points    INT OUTPUT
AS
BEGIN
	SET @Points = NULL

	declare @AccountId		BIGINT
	select @AccountId = ac_account_id from accounts where ac_external_reference = CAST (@PlayerId AS NVARCHAR(50))
	IF @AccountId IS NULL RETURN

	EXEC SP_AMTOTE_QueryPoints @PlayerId, @Points out

	IF @Points IS NULL
		BEGIN
			INSERT INTO [dbo].[customer_bucket] ([cbu_customer_id], [cbu_bucket_id], [cbu_value], [cbu_updated])
				 VALUES (@AccountId , 1, ISNULL(@AddPoints,0), GETDATE())
		END
	ELSE
		BEGIN
			UPDATE	[dbo].[customer_bucket]
			SET		[cbu_value]       = ISNULL([cbu_value],0) + ISNULL(@AddPoints,0)
					,[cbu_updated]     = GETDATE()
			WHERE [cbu_customer_id] = @AccountId
				AND [cbu_bucket_id] = 1
		END

	EXEC SP_AMTOTE_QueryPoints @PlayerId, @Points out

END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_AddPoints] TO wggui;

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_GetPlays]    Script Date: 05/11/2018 4:37:52 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_GetPlays]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_GetPlays]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_GetPlays]
	@PlaySessionId AS BIGINT
AS
BEGIN
	SELECT * 
	FROM VIEW_AMTOTE_Plays 
	WHERE SessionID = @PlaySessionId
END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_GetPlays] TO wggui;

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_GetPlaySessions]    Script Date: 05/11/2018 4:38:06 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_GetPlaySessions]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_GetPlaySessions]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_GetPlaySessions]
	@FromDate AS DATE,
	@ToDate   AS DATE,
	@PlayerId AS INT = NULL
AS
BEGIN
	IF @PlayerId IS NULL
		BEGIN
			SELECT * FROM VIEW_AMTOTE_PlaySessions 
			WHERE EndTime >= @FromDate 
				AND EndTime < @ToDate 
		END
	ELSE
		BEGIN
			SELECT * FROM VIEW_AMTOTE_PlaySessions 
			WHERE EndTime >= @FromDate
				AND EndTime < @ToDate
				AND PlayerID = @PlayerId
		END
END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_GetPlaySessions] TO wggui;

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_SetPoints]    Script Date: 05/11/2018 4:38:34 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_SetPoints]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_SetPoints]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_SetPoints]
	@PlayerId  INT,
	@SetPoints INT
AS
BEGIN
	declare @Points                 INT
	declare @AccountId		BIGINT
	select @AccountId = ac_account_id from accounts where ac_external_reference = CAST (@PlayerId AS NVARCHAR(50))
	IF @AccountId IS NULL RETURN

	EXEC SP_AMTOTE_QueryPoints @PlayerId, @Points out

	IF @Points IS NULL
		BEGIN
			INSERT INTO [dbo].[customer_bucket] ([cbu_customer_id], [cbu_bucket_id], [cbu_value], [cbu_updated])
				 VALUES (@AccountId , 1, ISNULL(@SetPoints,0), GETDATE())
		END
	ELSE
		BEGIN
			UPDATE [dbo].[customer_bucket]
			   SET [cbu_value]       = ISNULL(@SetPoints,0)
				  ,[cbu_updated]     = GETDATE()
			 WHERE [cbu_customer_id] = @AccountId
			   AND [cbu_bucket_id]   = 1
		END
END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_SetPoints] TO wggui;

/****** Object:  StoredProcedure [dbo].[SP_AMTOTE_UpdatePlayerInfo]    Script Date: 05/11/2018 4:38:45 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AMTOTE_UpdatePlayerInfo]') AND TYPE IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_AMTOTE_UpdatePlayerInfo]
GO

CREATE PROCEDURE [dbo].[SP_AMTOTE_UpdatePlayerInfo]
	@Playerid                        int,    
	@LastName                        nvarchar(25),
	@FirstName                       nvarchar(25),
	@MiddleName                      nvarchar(25),
	@NickName                        nvarchar(25),
	@Suffix                          nvarchar(10),
	@Gender                          nchar(1),
	@Enabled                         bit,
	@DLN                             nvarchar(25),
	@DLState                         nchar(2),
	@Address                         nvarchar(100),
	@City                            nvarchar(50),
	@State                           nvarchar(16),
	@Country                         nvarchar(50),
	@Zip                             nvarchar(10),
	@Birthdate                       datetime,
	@Email                           nvarchar(50),
	@Phone                           nvarchar(20),
	@Mobile                          nvarchar(20),
	@EnrollDate                      datetime,
	@EnrolledBy                      nvarchar(50),
	@EnrolledAt                      nvarchar(50),
	@DateModified					 datetime,
	@ModifiedBy                      nvarchar(50),
	@ModifiedAt                      nvarchar(50),
	@Status                          nvarchar(50),                                                    
	@MailOptOut						 bit,
	@EmailOptOut                     bit,
	@SMSOptIn                        bit,
	@SelfExcluded                    bit,
	@Notes                           nvarchar(max)
AS
BEGIN
	DECLARE @AccountId		BIGINT
	DECLARE @AccountIdOld	BIGINT

	SELECT @AccountId = AC_ACCOUNT_ID 
	FROM ACCOUNTS 
	WHERE AC_EXTERNAL_REFERENCE = CAST (@PlayerId AS NVARCHAR(50))

	IF @AccountId IS NULL 
	BEGIN
		EXEC CreateAccount @AccountId out
	END

	SET @FirstName  = ISNULL(@FirstName,  '')
	SET @MiddleName = ISNULL(@MiddleName, '')
	SET @LastName   = ISNULL(@LastName,   '')

	UPDATE	ACCOUNTS 
	SET		AC_HOLDER_NAME   = @FirstName + ' ' + LTRIM(@MiddleName + ' ') + @LastName , 
	  		AC_HOLDER_NAME1  = @LastName, 
	  		AC_HOLDER_NAME2  = @MiddleName, 
	  		AC_HOLDER_NAME3  = @FirstName, 
	  		AC_BLOCKED       = CASE WHEN @Enabled = 1 THEN 0 ELSE 1 END,
			AC_HOLDER_GENDER = CASE @Gender WHEN 'M' THEN 1 
											WHEN 'F' THEN 2 
											ELSE          NULL END,
			AC_HOLDER_BIRTH_DATE = CASE WHEN @Birthdate  IS NULL THEN GETDATE() ELSE @Birthdate  END,
			AC_CREATED           = CASE WHEN @EnrollDate IS NULL THEN GETDATE() ELSE @EnrollDate END,
			AC_HOLDER_PHONE_NUMBER_01 = @Mobile,
			AC_HOLDER_PHONE_NUMBER_02 = @Phone,
			AC_EXTERNAL_REFERENCE     = CAST (@PlayerId AS NVARCHAR(50))
	WHERE	AC_ACCOUNT_ID = @AccountId
END
GO

GRANT EXECUTE ON [dbo].[SP_AMTOTE_UpdatePlayerInfo] TO wggui;