IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TMP_TERMINALS_SWAP_FIELDS]') AND type in (N'U'))
BEGIN
  select 'This script has already been run before' AS ERROR
  return ;
END	

DECLARE @Unknown as int
DECLARE @Site as int
DECLARE @CashdeskDraw as int
DECLARE @GamingTableSeat as int

SELECT   TE_TERMINAL_ID
       , TE_BASE_NAME
       , TE_FLOOR_ID 
  INTO   TMP_TERMINALS_SWAP_FIELDS 
  FROM   TERMINALS

BEGIN TRY
    
    BEGIN TRANSACTION
	--Terminal Types
	SET @Unknown  = -1					
	SET @Site     = 100
	SET @CashdeskDraw = 106
	SET @GamingTableSeat = 107
		
	UPDATE   TERM
	   SET   TERM.TE_BASE_NAME = TMP.TE_FLOOR_ID
           , TERM.TE_FLOOR_ID  = LEFT(TMP.TE_BASE_NAME,20)
      FROM   TERMINALS AS TERM
INNER JOIN   TMP_TERMINALS_SWAP_FIELDS	AS TMP
        ON   TMP.TE_TERMINAL_ID = TERM.TE_TERMINAL_ID
	 WHERE   TERM.TE_TERMINAL_TYPE NOT IN ( @Unknown
					   					  , @Site
										  , @CashdeskDraw
										  , @GamingTableSeat
										  ) -- avoid system types
  
	COMMIT
		
END TRY
BEGIN CATCH
	SELECT  ERROR_MESSAGE() AS ERROR
	ROLLBACK	
END CATCH


