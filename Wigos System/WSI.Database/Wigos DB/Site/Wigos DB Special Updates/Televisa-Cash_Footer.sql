USE [wgdb_000]
GO

UPDATE GENERAL_PARAMS
   SET GP_KEY_VALUE = '<br>Recibí la cantidad de $_____________<br><br>'+
                        '_________________________________<br>'+ 
                        '<center>Nombre y firma del Anfitrión</center><br><br>'+
                        '_________________________________<br>'+ 
                        '<center>Nombre y firma del Cajero General</center>'
 WHERE GP_SUBJECT_KEY = 'CashDeskWithdraw.Footer'
   AND GP_GROUP_KEY = 'Cashier.Voucher'
GO

UPDATE GENERAL_PARAMS
   SET GP_KEY_VALUE = '<br>Entregué la cantidad de $_____________<br><br>'+
                        '_________________________________<br>'+
                        '<center>Nombre y firma del Anfitrión</center><br><br>'+ 
                        '_________________________________<br>'+ 
                        '<center>Nombre y firma del Cajero General</center>'
 WHERE GP_SUBJECT_KEY = 'CashDeskOpen.Footer'
   AND GP_GROUP_KEY = 'Cashier.Voucher'
GO

