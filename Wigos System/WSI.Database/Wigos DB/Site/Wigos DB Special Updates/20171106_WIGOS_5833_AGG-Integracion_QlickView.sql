/****** Object:  Table [dbo].[daily_meters]    Script Date: 10/19/2017 15:32:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[daily_meters]') AND type in (N'U'))
DROP TABLE [dbo].[daily_meters]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[daily_meters](
	[Site] [int] NOT NULL,
	[Term] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Games] [int] NULL,
	[Won] [int] NULL,
	[Bills] [int] NULL,
	[Tickets] [int] NULL,
	[Cards] [int] NULL,
	[Bets] [money] NULL,
	[Payments] [money] NULL,
	[Jackpots] [money] NULL,
	[Benefit] [money] NULL,
 CONSTRAINT [PK_daily_meters] PRIMARY KEY CLUSTERED 
(
	[Site] ASC,
	[Term] ASC,
	[Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/******   Fin Object:  Table [dbo].[daily_meters]     ******/


/****** Object:  StoredProcedure [dbo].[sp_GenerateDailyMeters]    Script Date: 10/20/2017 10:34:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateDailyMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_GenerateDailyMeters
GO

CREATE PROCEDURE sp_GenerateDailyMeters

@CurrentDay AS DATETIME

AS
BEGIN

DECLARE @ProcessDay AS DATETIME
DECLARE @Site AS INT

SET @ProcessDay = DATEADD(DAY, -1, @CurrentDay)
SET @Site = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'SITE' AND gp_subject_key = 'Identifier'), 0)

-- TEMP TABLES
DECLARE @daily_meters_tmp TABLE ( 
  AssetNumber varchar(100),	  
  CoinIn bigint,	
  TotalWon bigint,	
  BillIn bigint,	 
  CashableTicketIn bigint,	
  RestrictedTicketIn bigint,	
  NonRestrictedTicketIn bigint,	  	
  Jackpots bigint,	  
  Games bigint,
  Won bigint,
  CashableCardsIn bigint, 
  RestrictedCardsIn bigint,
  NonRestrictedCardsIn bigint,
  CashableCardsOut bigint, 
  RestrictedCardsOut bigint,
  NonRestrictedCardsOut bigint
)

DECLARE @daily_meters_2_tmp TABLE ( 
  Site int,
  Date datetime,
  Term int,    
  CoinIn bigint,	
  TotalWon bigint,	  
  BillIn bigint,	  
  CashableTicketIn bigint,	
  RestrictedTicketIn bigint,	
  NonRestrictedTicketIn bigint,	
  Jackpots bigint,	  
  Games bigint,
  Won bigint,
  CashableCardsIn bigint, 
  RestrictedCardsIn bigint,
  NonRestrictedCardsIn bigint,
  CashableCardsOut bigint, 
  RestrictedCardsOut bigint,
  NonRestrictedCardsOut bigint
)

-- TEMP 1
INSERT @daily_meters_tmp EXEC dbo.TITO_HoldvsWin @CurrentDay, 3, 0
        
-- TEMP 2
INSERT INTO @daily_meters_2_tmp 
SELECT  
  @Site,
  dateadd(dd, datediff(dd, 0, @ProcessDay), 0), 
  ISNULL((SELECT TOP 1 te_terminal_id FROM terminals WHERE te_name = AssetNumber), 0),
  CoinIn,	
  TotalWon,	
  BillIn,	
  CashableTicketIn,	
  RestrictedTicketIn,	
  NonRestrictedTicketIn,	  
  Jackpots,	
  Games,	
  Won,	
  CashableCardsIn,
  RestrictedCardsIn,
  NonRestrictedCardsIn,
  CashableCardsOut, 
  RestrictedCardsOut,
  NonRestrictedCardsOut
FROM @daily_meters_tmp 


INSERT INTO @daily_meters_2_tmp
SELECT DISTINCT
@Site,
dateadd(dd, datediff(dd, 0, @ProcessDay), 0), 
Term,
0, 
0, 
0, 
0, 
0, 
0, 
0, 
0, 
0,
0,
0,
0,
0,
0,
0
FROM daily_meters WHERE Term NOT IN (SELECT DISTINCT Term FROM @daily_meters_2_tmp WHERE Date > DATEADD(YEAR, -1 ,GETDATE()))


-- DELETE 
DELETE FROM daily_meters WHERE Site = @Site AND Date = dateadd(dd, datediff(dd, 0, @ProcessDay), 0) 


-- INSERT
INSERT INTO daily_meters
SELECT 
  Site, 
  Term,
  Date,
  SUM(Games) Games, 
  SUM(Won) Won, 
  SUM(BillIn) Bills, 
  SUM(CashableTicketIn+RestrictedTicketIn+NonRestrictedTicketIn) Tickets,
  SUM(CashableCardsIn+RestrictedCardsIn+NonRestrictedCardsIn) Cards,
  SUM(CoinIn) Bets,
  SUM(TotalWon-Jackpots) Payments,
  SUM(Jackpots) Jackpots,
  SUM(CoinIn-TotalWon) Benefit
FROM @daily_meters_2_tmp
GROUP BY 
  Site, 
  Term, 
  Date

END
GO

GRANT EXECUTE ON [sp_GenerateDailyMeters] TO [wggui] WITH GRANT OPTION
GO

/******   Fin Object:  StoredProcedure [dbo].[sp_GenerateDailyMeters]     ******/


/****** Object:  StoredProcedure [dbo].[sp_GetDailyMeters]    Script Date: 10/20/2017 11:51:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetDailyMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetDailyMeters]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetDailyMeters] 
	-- Add the parameters for the stored procedure here
	@Datatype AS INT = 0, 
	@Date AS DATETIME
AS
BEGIN	



IF @Datatype = 1
BEGIN

SELECT 
  Site Sala,
  Term,
  CONVERT(VARCHAR(10), DATE, 103) + ' ' + CONVERT(VARCHAR(10), DATE, 108) Fecha,
  Games,
  Won  
FROM daily_meters
WHERE Date = dateadd(dd, datediff(dd, 0, @Date), 0)

END

ELSE IF @Datatype = 2
BEGIN

SELECT 
  Site Sala,
  Term,
  CONVERT(VARCHAR(10), DATE, 103) + ' ' + CONVERT(VARCHAR(10), DATE, 108) Fecha_Drop,
  Bills,
  Cards,
  Tickets
FROM daily_meters
WHERE Date = dateadd(dd, datediff(dd, 0, @Date), 0)

END

ELSE IF @Datatype = 3
BEGIN

SELECT 
  Site Sala,
  Term,
  CONVERT(VARCHAR(10), DATE, 103) + ' ' + CONVERT(VARCHAR(10), DATE, 108) FechaDesde,
  Jackpots,
  Payments Pagos,
  Bets Apuestas,
  Benefit Beneficio
FROM daily_meters
WHERE Date = dateadd(dd, datediff(dd, 0, @Date), 0)

END

ELSE
BEGIN

SELECT 
  Site Sala,
  Term,
  CONVERT(VARCHAR(10), DATE, 103) + ' ' + CONVERT(VARCHAR(10), DATE, 108) Fecha,
  Games,
  Won,
  Bills,
  Tickets,
  Cards,
  Bets Apuestas,
  Payments Pagos,
  Jackpots,
  Benefit Beneficio
FROM daily_meters
WHERE Date = dateadd(dd, datediff(dd, 0, @Date), 0)

END 

END

GO

/******   Fin Object:  StoredProcedure [dbo].[sp_GetDailyMeters]     ******/


/****** CREATE LOGIN     ******/

IF NOT EXISTS(SELECT principal_id FROM sys.server_principals WHERE name = N'read_daily_meters') 
BEGIN
  
  CREATE LOGIN read_daily_meters with password = N'Let5play17'
  
END
GO

IF NOT EXISTS (SELECT * FROM DBO.SYSUSERS WHERE NAME = N'read_daily_meters' )
BEGIN
  
  CREATE USER read_daily_meters FOR LOGIN read_daily_meters   
  GRANT EXECUTE ON sp_GetDailyMeters to read_daily_meters 
  
END
GO

/******   Fin CREATE LOGIN     ******/

USE [msdb]
GO


/****** Object:  Job [jb_DailyMeters]    Script Date: 05/06/2016 10:43:45 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'jb_DailyMeters')
EXEC msdb.dbo.sp_delete_job @job_name = N'jb_DailyMeters' , @delete_unused_schedule=1
GO

USE [msdb]
GO


BEGIN TRANSACTION

DECLARE @LOGIN VARCHAR(50)
DECLARE @DB VARCHAR(50)
DECLARE @EXECHOUR CHAR(6)
DECLARE @EXECMIN VARCHAR(6)
DECLARE @DEFAULTEXEC INT

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

SET @LOGIN = N'sa'
SET @DB = N'wgdb_301'
SET @DEFAULTEXEC = 73000

/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 05/06/2016 10:43:45 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jb_DailyMeters', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Task created to execute sp_generate_daily_meters in order to populate daily_meters table', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=@LOGIN, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [jb_DailyMeters]    Script Date: 05/06/2016 10:43:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'jb_step_daily_meters_execution', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=3, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
            		DECLARE @DATE VARCHAR(25)
                SET @DATE = convert(VARCHAR(25), GETDATE(), 126)
                EXEC dbo.sp_GenerateDailyMeters @DATE
    ', 
		@database_name=@DB, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'jb_daily_meters_schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20171023, 
		@active_end_date=99991231, 
		@active_start_time=@DEFAULTEXEC, 
		@active_end_time=235959, 
		@schedule_uid=N'CE560300-EA48-4CFD-91AA-ED32E513C3C3'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

/******   Fin Object:  Job [jb_DailyMeters]    ******/
