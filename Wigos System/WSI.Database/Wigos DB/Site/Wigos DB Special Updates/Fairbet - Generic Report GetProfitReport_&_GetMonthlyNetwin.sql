use [wgdb_000]
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GET PROFIT REPORT FROM TERMINALS

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    24-JAN-2017			EOR				New procedure

Requeriments:
   -- Functions:
         
Parameters:
   -- pDateFrom:					
   -- pDateTo :	
   -- pDetail :				
*/
IF  EXISTS (SELECT 	1 FROM 	sys.objects WHERE 	object_id = OBJECT_ID(N'[dbo].[GetProfitReport]') AND type in (N'P', N'PC'))
BEGIN
  DROP PROCEDURE [dbo].[GetProfitReport]
END
GO

CREATE PROCEDURE [dbo].[GetProfitReport]
  @pFrom  DATETIME,
  @pTo    DATETIME
AS
BEGIN

-- Create temp table for whole report
CREATE TABLE #TT_PROFITREPORT (
ORDER_TYPE            INT,
TSMH_DATETIME         DATETIME, 
TE_PROVIDER_ID        NVARCHAR(200), 
TE_NAME               NVARCHAR(200), 
BILL_IN               MONEY, 
TICKET_OUT_REDIMIBLE  MONEY,
HANDPAYS              MONEY,
CANCELLED_CREDITS     MONEY,
PROFIT                MONEY)

DECLARE @closing_hour    int
DECLARE @closing_minutes int

SET @closing_hour = ISNULL((SELECT   gp_key_value
                              FROM   general_params
                             WHERE   gp_group_key   = 'WigosGUI'
                               AND   gp_subject_key = 'ClosingTime'), 6)

SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                 FROM   general_params
                                WHERE   gp_group_key   = 'WigosGUI'
                                  AND   gp_subject_key = 'ClosingTimeMinutes'), 0)
  INSERT INTO #TT_PROFITREPORT
  SELECT  0 ORDER_TYPE   
        , CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
               THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
               ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                END 
        , '--'  TE_PROVIDER_ID
        , '--'  TE_NAME
        /*BILL IN 11 128*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET OUT 134 */
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*HANDPAYS 130 132*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS HANDPAYS
        /*CANCELLED CREDITS 3*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*PROFIT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS PROFIT
  FROM TERMINALS
  INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
  WHERE TSMH_DATETIME >= @pFrom AND TSMH_DATETIME < @pTo
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
  GROUP BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 
  ORDER BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 


SELECT  CASE TSMH_DATETIME WHEN '9999-12-31' THEN '' ELSE CONVERT(NVARCHAR(10),TSMH_DATETIME,103) END TSMH_DATETIME
      , TE_PROVIDER_ID
      , TE_NAME
      , BILL_IN
      , TICKET_OUT_REDIMIBLE
      , HANDPAYS
      , CANCELLED_CREDITS
      , PROFIT 
FROM (
  SELECT  ORDER_TYPE 
        , TSMH_DATETIME
        , TE_PROVIDER_ID
        , TE_NAME
        , BILL_IN
        , TICKET_OUT_REDIMIBLE
        , HANDPAYS
        , CANCELLED_CREDITS
        , PROFIT
  FROM #TT_PROFITREPORT
  UNION ALL
  SELECT  3 ORDER_TYPE
        , CAST('9999-12-31' AS DATETIME) TSMH_DATETIME
        , 'TOTAL' TE_PROVIDER_ID
        , '' TE_NAME
        , SUM(BILL_IN)  AS BILL_IN
        , SUM(TICKET_OUT_REDIMIBLE) AS  TICKET_OUT_REDIMIBLE
        , SUM(HANDPAYS) AS  HANDPAYS
        , SUM(CANCELLED_CREDITS)  AS  CANCELLED_CREDITS
        , SUM(PROFIT) AS  PROFIT 
  FROM #TT_PROFITREPORT
  WHERE ORDER_TYPE = 0) T
ORDER BY T.TSMH_DATETIME,T.ORDER_TYPE
        
DROP TABLE #TT_PROFITREPORT

END
GO

GRANT EXECUTE ON [dbo].[GetProfitReport] TO wggui WITH GRANT OPTION
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GET MONTHLY NETWIN FROM TERMINALS

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    09-NOV-2016			ESE				New procedure
1.0.1     17-NOV-2016     ESE       Change Total IN = Ticket In + Bill In and Total OUT = Ticket out + HP + JP
2.0.0     28-FEB-2017     JML       Bug 24011*:Errors in New report "Profit". El "Profit" no está bien calculado ya que debería coincidir con el NetWin.
                                    Bug 24871*:Reporte Generic reports - Terminals contempla los incrementos de los contadores

Requeriments:
   -- Functions:
         
Parameters:
   -- pMonth:					
   -- pYear :					
*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMonthlyNetwin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMonthlyNetwin]
GO

CREATE PROCEDURE [dbo].[GetMonthlyNetwin]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @_aux_date AS DATETIME
DECLARE @_aux_date_init AS DATETIME
DECLARE @_aux_date_final AS DATETIME
DECLARE @_aux_site_name AS NVARCHAR(200)

-- Create temp table for whole report
CREATE TABLE #TT_MONTHLYNETWIN (
TSMH_MONTH            NVARCHAR(5), 
SITE_NAME             NVARCHAR(200), 
TE_NAME               NVARCHAR(200), 
TE_SERIAL_NUMBER      NVARCHAR(200),
BILL_IN               MONEY, 
TICKET_IN_REDIMIBLE   MONEY,
TICKET_OUT_REDIMIBLE  MONEY,
TOTAL_IN              MONEY,
TOTAL_OUT             MONEY,
REDEEM_TICKETS        MONEY,
TICKET_IN_PROMOTIONAL MONEY,
CANCELLED_CREDITS     MONEY,
JACKPOTS              MONEY,
LGA_NETWIN            MONEY)

--Obtain the INITIAL and FINAL date
set @_aux_date = CONVERT(DATETIME,CONVERT(NVARCHAR(4),@pYear) + '-' + CONVERT(NVARCHAR(2),@pMonth) + '-' +  '1')
set @_aux_date_init = CONVERT(NVARCHAR(25),DATEADD(dd,-(DAY(@_aux_date)-1),@_aux_date),101)
set @_aux_date_final = DATEADD(dd,-(DAY(DATEADD(mm,1,@_aux_date))),DATEADD(hh,23,DATEADD(mi,59,DATEADD(ss,59,DATEADD(mm,1,@_aux_date)))))

--Obtain site ID and site name
set @_aux_site_name = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier') + ' - ' + (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Name')

INSERT INTO #TT_MONTHLYNETWIN
SELECT    (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME)END) AS TSMH_MONTH
        , @_aux_site_name AS SITE_NAME
        , TE_NAME
        , TE_SERIAL_NUMBER
        /*BILL IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_REDIMIBLE
        /*TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*TOTAL IN = BILL IN + TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_IN
        /*TOTAL OUT = JACKPOTS + CANCELLED CREDITS + TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_OUT
        /*REDEEM TICKETS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN 
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               ELSE 
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               END ) AS  REDEEM_TICKETS
        /*TICKET IN PROMOTIONAL (redeem, no redeem)*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_PROMOTIONAL
        /*CANCELLED CREDITS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOTS
        /*LGA NETWIN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS LGA_NETWIN
FROM TERMINALS
INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
WHERE TSMH_DATETIME >= @_aux_date_init AND TSMH_DATETIME < @_aux_date_final
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                              , 1   /*Total OUT*/
                              , 2   /*Jackpots*/
                              , 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
GROUP BY  TE_TERMINAL_ID, TE_NAME, TE_SERIAL_NUMBER, TE_MACHINE_SERIAL_NUMBER,
          (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME) END)
ORDER BY TE_NAME
          
/*Add the TOTAL ROW*/
SELECT TSMH_MONTH,SITE_NAME, TE_NAME, TE_SERIAL_NUMBER, BILL_IN, TICKET_IN_REDIMIBLE, TICKET_OUT_REDIMIBLE, TOTAL_IN, TOTAL_OUT, REDEEM_TICKETS, TICKET_IN_PROMOTIONAL, CANCELLED_CREDITS, JACKPOTS, LGA_NETWIN
FROM #TT_MONTHLYNETWIN
UNION ALL
SELECT 'TOTAL' AS TSMH_MONTH, '' AS SITE_NAME, '' AS TE_NAME, '' AS TE_SERIAL_NUMBER, SUM(BILL_IN), SUM(TICKET_IN_REDIMIBLE), SUM(TICKET_OUT_REDIMIBLE), SUM(TOTAL_IN), SUM(TOTAL_OUT), SUM(REDEEM_TICKETS), SUM(TICKET_IN_PROMOTIONAL), SUM(CANCELLED_CREDITS), SUM(JACKPOTS), SUM(LGA_NETWIN)
FROM #TT_MONTHLYNETWIN
          
DROP TABLE #TT_MONTHLYNETWIN

END
GO

GRANT EXECUTE ON [dbo].[GetMonthlyNetwin] TO wggui WITH GRANT OPTION
GO
