USE [wgdb_000]
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Redeem.SignaturesRoom')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
      VALUES ('Cashier.Voucher', 'Redeem.SignaturesRoom', '0');
GO

UPDATE general_params SET gp_key_value = '0' where gp_group_key = 'TITO'            AND gp_subject_key = 'TITOMode'
UPDATE general_params SET gp_key_value = '7' where gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'Mode'
UPDATE general_params SET gp_key_value = '1' where gp_group_key = 'NoteAcceptor'    AND gp_subject_key = 'Enabled'
UPDATE general_params SET gp_key_value = '1' where gp_group_key = 'GamingTables'    AND gp_subject_key = 'Cashier.IntegratedChipAndCreditOperations'
UPDATE general_params SET gp_key_value = '2' where gp_group_key = 'Cashier'         AND gp_subject_key = 'PrizeComputationMode'
UPDATE general_params SET gp_key_value = '1' where gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'Redeem.SignaturesRoom'
UPDATE general_params SET gp_key_value = '0' where gp_group_key = 'SiteJackpot'     AND gp_subject_key = 'Bonusing.Enabled'
GO