USE [wgdb_000]
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintAlesisDeleteMsg')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'PrintAlesisDeleteMsg', '0');
GO

IF EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintMsgAlesis')
    UPDATE GENERAL_PARAMS 
    SET GP_SUBJECT_KEY = 'PrintAlesisInsertMsg'
    WHERE GP_GROUP_KEY = 'PSAClient' 
      AND GP_SUBJECT_KEY = 'PrintMsgAlesis';
GO
 
--If not exists PrintAlesisInsertMsg then create
IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintAlesisInsertMsg')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'PrintAlesisInsertMsg', '0');
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'ReportMajorPrizeAdditionalFields')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'ReportMajorPrizeAdditionalFields', '0');
GO
