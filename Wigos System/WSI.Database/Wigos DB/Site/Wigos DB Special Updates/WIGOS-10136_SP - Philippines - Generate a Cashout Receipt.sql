------------------------------------------------------------------
------------------------------- GP -------------------------------
------------------------------------------------------------------
UPDATE   general_params SET gp_key_value = '1' WHERE gp_group_key = 'AutoPrintVoucher' and gp_subject_key = 'Types.Enabled';
GO

UPDATE   general_params SET gp_key_value = '1' WHERE gp_group_key = 'AutoPrintVoucher' and gp_subject_key = 'CashOut.Generation.Enabled';
GO

UPDATE   general_params SET gp_key_value = '1' WHERE gp_group_key = 'AutoPrintVoucher' and gp_subject_key = 'CashOut.Print.Enabled';
GO

UPDATE   general_params SET gp_key_value = 'PAGCOR' WHERE gp_group_key = 'AutoPrintVoucher' and gp_subject_key = 'CashOut.Voucher.Title';
GO

UPDATE   general_params SET gp_key_value = 'CASHOUT RECEIPT' WHERE gp_group_key = 'AutoPrintVoucher' and gp_subject_key = 'CashOut.Voucher.CashOutReceipt';
GO