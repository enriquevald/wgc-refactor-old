
  --------------------------------------------------------------------------------
-- Copyright © 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountPointsCache.sql
-- 
--   DESCRIPTION: Procedures for Accounts 
-- 
--        AUTHOR: José Martínez López
-- 
-- NOTES:
--       Product Backlog Item 151:AccountPointsCache --> Task 153: Procedures creation
-- 
-- CREATION DATE: 13-JAN-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 13-JAN-2015 JML    First release.
-- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
-- 12-APR-2016 ETP    Fixed Bug 10726: Transfer NR/RE credit to promobox.
-------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Calculate AccountPoints for level
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId                        BIGINT,         
--           @DateFrom                         DATETIME,
--           @DateTo                           DATETIME,
--           @MovementId                       BIGINT, 
--
--      - OUTPUT:
--           @LastMovementId                   BIGINT OUTPUT, 
--           @PointsGeneratedForLevel          MONEY OUTPUT,
--           @PointsDiscretionalForLevel       MONEY OUTPUT,
--           @PointsDiscretionalOnlyForRedeem  MONEY OUTPUT,
--           @PointsPromotionOnlyForRedeem     MONEY OUTPUT
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
  -- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
  DECLARE @EndSession_RankingLevelPoints		AS INT;	
  DECLARE @Expired_RankingLevelPoints		    AS INT
  DECLARE @Manual_Add_RankingLevelPoints		AS INT
  DECLARE @Manual_Sub_RankingLevelPoints		AS INT
  DECLARE @Manual_Set_RankingLevelPoints		AS INT
  DECLARE @EndSession_RedemptionPoints			AS INT
  DECLARE @Expired_RedemptionPoints				AS INT
  DECLARE @Manual_Add_RedemptionPoints			AS INT
  DECLARE @Manual_Sub_RedemptionPoints			AS INT
  DECLARE @Manual_Set_RedemptionPoints			AS INT
  


  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
  
     -- Discretional
  
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel

  SET @imported_points_history               = 73
  --ImportedPointsHistory

  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  --ManuallyAddedPointsOnlyForRedeem

  SET @imported_points_only_for_redeem       = 71
  --ImportedPointsOnlyForRedeem

     -- Promotion 
  SET @promotion_point                       = 60
  --PromotionPoint
  
  SET @cancel_promotion_point                = 61
  --CancelPromotionPoint 

  SET @EndSession_RankingLevelPoints		     = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints		         = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints		     = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints		     = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints		     = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @EndSession_RedemptionPoints		     = 1101
  --'MULTIPLE_BUCKETS_END_SESSION + RedemptionPoints PuntosCanje'
  SET @Expired_RedemptionPoints		         = 1201
  --'MULTIPLE_BUCKETS_Expired + RedemptionPoints PuntosCanje'
  SET @Manual_Add_RedemptionPoints		     = 1301
  --'MULTIPLE_BUCKETS_Manual_Add + RedemptionPoints PuntosCanje'
  SET @Manual_Sub_RedemptionPoints		     = 1401
  --'MULTIPLE_BUCKETS_Manual_Sub + RedemptionPoints PuntosCanje'
  SET @Manual_Set_RedemptionPoints		     = 1501
  --'MULTIPLE_BUCKETS_Manual_Set + RedemptionPoints PuntosCanje'




  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
				 '                   @EndSession_RankingLevelPoints, @Expired_RankingLevelPoints, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints,' +
				 '                   @EndSession_RedemptionPoints, @Expired_RedemptionPoints, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints,' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point,@EndSession_RedemptionPoints, @Expired_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
						@EndSession_RankingLevelPoints			INT,
						@Expired_RankingLevelPoints				INT,	
						@Manual_Add_RankingLevelPoints			INT,
						@Manual_Sub_RankingLevelPoints			INT,
						@Manual_Set_RankingLevelPoints			INT,
						@EndSession_RedemptionPoints			INT,	
						@Expired_RedemptionPoints				INT,	
						@Manual_Add_RedemptionPoints			INT,	
						@Manual_Sub_RedemptionPoints			INT,	
						@Manual_Set_RedemptionPoints			INT,	
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
					@ParamDefinition,
					@AccountId                             = @AccountId, 
					@points_awarded                        = @points_awarded,                        
					@manually_added_points_for_level       = @manually_added_points_for_level,
					@imported_points_for_level             = @imported_points_for_level,
					@imported_points_history               = @imported_points_history, 
					@manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
					@imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
					@promotion_point                       = @promotion_point,
					@cancel_promotion_point                = @cancel_promotion_point,
					@EndSession_RankingLevelPoints    =   @EndSession_RankingLevelPoints, 
					@Expired_RankingLevelPoints		  =   @Expired_RankingLevelPoints		,
					@Manual_Add_RankingLevelPoints    =   @Manual_Add_RankingLevelPoints ,
					@Manual_Sub_RankingLevelPoints    =   @Manual_Sub_RankingLevelPoints ,
					@Manual_Set_RankingLevelPoints    =   @Manual_Set_RankingLevelPoints ,
					@EndSession_RedemptionPoints	  =   @EndSession_RedemptionPoints	,
					@Expired_RedemptionPoints		  =   @Expired_RedemptionPoints		,
					@Manual_Add_RedemptionPoints	  =   @Manual_Add_RedemptionPoints	,
					@Manual_Sub_RedemptionPoints	  =   @Manual_Sub_RedemptionPoints	,
					@Manual_Set_RedemptionPoints	  =   @Manual_Set_RedemptionPoints	,
					@LastMovementId_out                    = @LastMovementId                   OUTPUT, 
					@PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
					@PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
					@PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
					@PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable´s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Save AccountPoints for level in DB (for simulate a cache)
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT       
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculateToday]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  

--------------------------------------------------------------------------------
-- PURPOSE: Return Accounts & AccountPointsCache data for account
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT 
--
--      - OUTPUT:
--           AC_HOLDER_NAME 
--           AC_HOLDER_GENDER 
--           AC_HOLDER_BIRTH_DATE 
--           AC_BALANCE 
--           AC_POINTS 
--           AC_CURRENT_HOLDER_LEVEL 
--           AC_HOLDER_LEVEL_EXPIRATION 
--           AC_HOLDER_LEVEL_ENTERED 
--           AC_RE_BALANCE 
--           AC_PROMO_RE_BALANCE 
--           AC_PROMO_NR_BALANCE 
--           AC_CURRENT_PLAY_SESSION_ID 
--           AC_CURRENT_TERMINAL_NAME 
--           AM_POINTS_GENERATED 
--           AM_POINTS_DISCRETIONARIES 
--           AM_POINTS_PROMO_DISCRETIONARIES 
--           AM_POINTS_PROMO_ONLY_FOR_REDEEM 
--           APC_TODAY_LAST_MOVEMENT_ID 
--           BU_NR_BALANCE
--           BU_RE_BALANCE
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache] @pAccountId bigint
AS
BEGIN

  --EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  DECLARE @BucketNR Int 
  DECLARE @BucketRE Int
  DECLARE @BucketPuntosCanjeGenerados Int
  DECLARE @BucketPuntosCanjeDiscrecionales Int

    
  SET @BucketPuntosCanje = 1
  SET @BucketPuntosCanjeGenerados = 8
  SET @BucketPuntosCanjeDiscrecionales = 9
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs función Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
				 , AC_IN_SESSION_RE_BALANCE   
				 , AC_PIN
				 , AC_IN_SESSION_PROMO_RE_BALANCE
				 , AC_IN_SESSION_PROMO_NR_BALANCE
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
		     , ISNULL(AC_POINTS.CBU_VALUE                      ,0) AS  AC_POINTS                 
		     , ISNULL(AM_POINTS_GENERATED.CBU_VALUE      	  ,0) AS  AM_POINTS_GENERATED       
		     , ISNULL(AM_POINTS_DISCRETIONARIES.CBU_VALUE	  ,0) AS  AM_POINTS_DISCRETIONARIES 
         , AC_PIN_FAILURES
         , AC_TRACK_DATA 
	 , AC_HOLDER_NAME3
    FROM   ACCOUNTS 
      LEFT JOIN CUSTOMER_BUCKET AC_POINTS ON AC_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_POINTS.CBU_BUCKET_ID = 1 /* REDEMPTIONPOINTS */
			LEFT JOIN CUSTOMER_BUCKET AC_LEVEL_POINTS ON AC_LEVEL_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_LEVEL_POINTS.CBU_BUCKET_ID = 2 /* LEVELPOINTS */  
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_GENERATED ON AM_POINTS_GENERATED.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_GENERATED.CBU_BUCKET_ID = 8 /* RANKINGLEVELPOINTS_GENERATED */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_DISCRETIONARIES ON AM_POINTS_DISCRETIONARIES.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_DISCRETIONARIES.CBU_BUCKET_ID = 9 /* RANKINGLEVELPOINTS_DISCRETIONAL */
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
END

GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO                          

-- Alter PROMOTIONS Table
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'pm_journey_limit')
BEGIN
  ALTER TABLE [dbo].[promotions] ADD [pm_journey_limit] bit NOT NULL DEFAULT(0)
END
GO  

-- Alter PROMOTIONS Table
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'pm_pyramidal_dist')
BEGIN
  ALTER TABLE dbo.promotions ADD pm_pyramidal_dist XML NULL CONSTRAINT DF_promotions_pm_pyramidal_dist DEFAULT NULL
END
GO   

-- Add PyramidalDistributionReward General Param
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Features' AND GP_SUBJECT_KEY = 'PyramidalDistributionReward')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Features', 'PyramidalDistributionReward', '0')
GO

-- Create table PROMOGAME_TYPE_PLAY_SESSION
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promogame_type_play_session')
BEGIN
CREATE TABLE [dbo].[promogame_type_play_session](
	[ptp_playsession_id] [bigint] NOT NULL,
	[ptp_gametype] [bigint] NOT NULL,
 CONSTRAINT [PK_promogametype_playsession] PRIMARY KEY CLUSTERED 
(
	[ptp_playsession_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS( SELECT * FROM sys.indexes WHERE name='IDX_Play_Session_Id' AND object_id = OBJECT_ID('promogame_type_play_session'))
BEGIN
CREATE NONCLUSTERED INDEX [IDX_Play_Session_Id] ON [dbo].[promogame_type_play_session] 
(
  [ptp_playsession_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS( SELECT * FROM sys.indexes WHERE name='IDX_Game_Type' AND object_id = OBJECT_ID('promogame_type_play_session'))
BEGIN
CREATE NONCLUSTERED INDEX [IDX_Game_Type] ON [dbo].[promogame_type_play_session] 
(
  [ptp_gametype] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

-- Rename INTOUCH_PALETTE Table
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'intouch_palette') 
BEGIN
	EXEC sp_rename	'intouch_palette.itp_id',
					'pli_id',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_index',
					'pli_index',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_name',
					'pli_name',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_level',
					'pli_level',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_NLS_id',
					'pli_NLS_id',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_icon',
					'pli_icon',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_pending_download',
					'pli_pending_download',
					'COLUMN'
	EXEC sp_rename	'intouch_palette',
					'player_level_icons'
END
GO
  
-- Rename INTOUCH_PROMOGAME Table
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'intouch_promogame') 
BEGIN
	EXEC sp_rename	'intouch_promogame.ip_id',
					'pg_id',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_name',
					'pg_name',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_type',
					'pg_type',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_price',
					'pg_price',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_price_units',
					'pg_price_units',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_return_price',
					'pg_return_price',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_return_units',
					'pg_return_units',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_percentatge_cost_return',
					'pg_percentatge_cost_return',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_game_url',
					'pg_game_url',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_mandatory_ic',
					'pg_mandatory_ic',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_show_buy_dialog',
					'pg_show_buy_dialog',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_player_can_cancel',
					'pg_player_can_cancel',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_autocancel',
					'pg_autocancel',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_transfer_screen',
					'pg_transfer_screen',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_transfer_timeout',
					'pg_transfer_timeout',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_pay_table',
					'pg_pay_table',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_last_update',
					'pg_last_update',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame',
					'promogames'
END
GO

 -- Rename PROMOTIONS.PM_INTOUCH_PROMOGAME
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'PM_INTOUCH_PROMOGAME') 
BEGIN 
	EXEC sp_rename	'promotions.pm_intouch_promogame',
					'pm_promogame_id',
					'COLUMN'
END
GO

 -- Rename PROMOTIONS.PM_PYRAMIDAL_DIST
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'PM_INTOUCH_PYRAMIDAL_DIST') 
BEGIN 					
	EXEC sp_rename	'promotions.PM_INTOUCH_PYRAMIDAL_DIST',
					'pm_pyramidal_dist',
					'COLUMN'					
END
GO