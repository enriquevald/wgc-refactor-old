USE [wgdb_000]

GO

--TABLE PENDING RECHARGES
--PENDING CREATE INDEX IF NEEDED
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.terminal_draws_recharges') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.terminal_draws_recharges(
	tdr_id              BIGINT IDENTITY(1,1)  NOT NULL,
	tdr_account_id      BIGINT                NOT NULL,
	tdr_status          INT                   NOT NULL,
	tdr_total_cash_in   MONEY                 NULL,
	tdr_re_bet          MONEY                 NULL,
	tdr_nr_bet          MONEY                 NULL,
	tdr_points_bet      MONEY                 NULL,
	tdr_operation_id    BIGINT                NOT NULL,
	tdr_game_id         BIGINT                NOT NULL
	
CONSTRAINT [PK_tdr_id] PRIMARY KEY CLUSTERED 
(
  tdr_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

IF NOT EXISTS(SELECT * FROM TERMINALS WHERE te_base_name = 'SORTEADOR TERMINAL')
BEGIN
	INSERT INTO TERMINALS
	(
	TE_TYPE,
	TE_SERVER_ID,
	TE_EXTERNAL_ID,
	TE_BLOCKED,
	TE_ACTIVE,
	TE_PROVIDER_ID,
	TE_CLIENT_ID,
	TE_BUILD_ID,
	TE_TERMINAL_TYPE,
	TE_VENDOR_ID,
	TE_STATUS,
	TE_RETIREMENT_DATE,
	TE_RETIREMENT_REQUESTED,
	TE_DENOMINATION,
	TE_MULTI_DENOMINATION,
	TE_PROGRAM,
	TE_THEORETICAL_PAYOUT,
	TE_PROV_ID,
	TE_BANK_ID,
	TE_FLOOR_ID,
	TE_GAME_TYPE,
	TE_ACTIVATION_DATE,
	TE_CURRENT_ACCOUNT_ID,
	TE_CURRENT_PLAY_SESSION_ID,
	TE_REGISTRATION_CODE,
	TE_SAS_FLAGS,
	TE_SERIAL_NUMBER,
	TE_CABINET_TYPE,
	TE_JACKPOT_CONTRIBUTION_PCT,
	TE_CONTRACT_TYPE,
	TE_CONTRACT_ID,
	TE_ORDER_NUMBER,
	TE_WXP_REPORTED,
	TE_WXP_REPORTED_STATUS_DATETIME,
	TE_WXP_REPORTED_STATUS,
	TE_SEQUENCE_ID,
	TE_VALIDATION_TYPE,
	TE_ALLOWED_CASHABLE_EMISSION,
	TE_ALLOWED_PROMO_EMISSION,
	TE_ALLOWED_REDEMPTION,
	TE_MAX_ALLOWED_TI,
	TE_MAX_ALLOWED_TO,
	TE_SAS_VERSION,
	TE_SAS_MACHINE_NAME,
	TE_BONUS_FLAGS,
	TE_FEATURES_BYTES,
	TE_VIRTUAL_ACCOUNT_ID,
	TE_SAS_FLAGS_USE_SITE_DEFAULT,
	TE_AUTHENTICATION_METHOD,
	TE_AUTHENTICATION_SEED,
	TE_AUTHENTICATION_SIGNATURE,
	TE_AUTHENTICATION_STATUS,
	TE_AUTHENTICATION_LAST_CHECKED,
	TE_MACHINE_ID,
	TE_POSITION,
	TE_TOP_AWARD,
	TE_MAX_BET,
	TE_NUMBER_LINES,
	TE_GAME_THEME,
	TE_ACCOUNT_PROMOTION_ID,
	TE_MASTER_ID,
	TE_CHANGE_ID,
	TE_BASE_NAME,
	TE_MACHINE_ASSET_NUMBER,
	TE_ASSET_NUMBER,
	TE_MACHINE_SERIAL_NUMBER,
	TE_METER_DELTA_ID,
	TE_LAST_GAME_PLAYED_ID,
	TE_TRANSFER_STATUS,
	TE_SMIB2EGM_COMM_TYPE,
	TE_SMIB2EGM_CONF_ID,
	TE_BRAND_CODE,
	TE_MODEL,
	TE_MANUFACTURE_YEAR,
	TE_MET_HOMOLOGATED,
	TE_BET_CODE,
	TE_COIN_COLLECTION,
	TE_EQUITY_PERCENTAGE,
	TE_TERMINAL_CURRENCY_ID,
	TE_ISO_CODE,
	TE_SAS_ACCOUNTING_DENOM,
	TE_TITO_HOST_ID)
		SELECT TOP  1 
								TE_TYPE,
								TE_SERVER_ID,
								TE_EXTERNAL_ID,
								TE_BLOCKED,
								TE_ACTIVE,
								TE_PROVIDER_ID,
								TE_CLIENT_ID,
								TE_BUILD_ID,
								113,
								TE_VENDOR_ID,
								TE_STATUS,
								TE_RETIREMENT_DATE,
								TE_RETIREMENT_REQUESTED,
								TE_DENOMINATION,
								TE_MULTI_DENOMINATION,
								TE_PROGRAM,
								TE_THEORETICAL_PAYOUT,
								TE_PROV_ID,
								TE_BANK_ID,
								TE_FLOOR_ID,
								TE_GAME_TYPE,
								TE_ACTIVATION_DATE,
								TE_CURRENT_ACCOUNT_ID,
								TE_CURRENT_PLAY_SESSION_ID,
								TE_REGISTRATION_CODE,
								TE_SAS_FLAGS,
								TE_SERIAL_NUMBER,
								TE_CABINET_TYPE,
								TE_JACKPOT_CONTRIBUTION_PCT,
								TE_CONTRACT_TYPE,
								TE_CONTRACT_ID,
								TE_ORDER_NUMBER,
								TE_WXP_REPORTED,
								TE_WXP_REPORTED_STATUS_DATETIME,
								TE_WXP_REPORTED_STATUS,
								TE_SEQUENCE_ID,
								TE_VALIDATION_TYPE,
								TE_ALLOWED_CASHABLE_EMISSION,
								TE_ALLOWED_PROMO_EMISSION,
								TE_ALLOWED_REDEMPTION,
								TE_MAX_ALLOWED_TI,
								TE_MAX_ALLOWED_TO,
								TE_SAS_VERSION,
								TE_SAS_MACHINE_NAME,
								TE_BONUS_FLAGS,
								TE_FEATURES_BYTES,
								TE_VIRTUAL_ACCOUNT_ID,
								TE_SAS_FLAGS_USE_SITE_DEFAULT,
								TE_AUTHENTICATION_METHOD,
								TE_AUTHENTICATION_SEED,
								TE_AUTHENTICATION_SIGNATURE,
								TE_AUTHENTICATION_STATUS,
								TE_AUTHENTICATION_LAST_CHECKED,
								TE_MACHINE_ID,
								TE_POSITION,
								TE_TOP_AWARD,
								TE_MAX_BET,
								TE_NUMBER_LINES,
								TE_GAME_THEME,
								TE_ACCOUNT_PROMOTION_ID,
								TE_MASTER_ID,
								TE_CHANGE_ID,
								'SORTEADOR TERMINAL',
								TE_MACHINE_ASSET_NUMBER,
								TE_ASSET_NUMBER,
								TE_MACHINE_SERIAL_NUMBER,
								TE_METER_DELTA_ID,
								TE_LAST_GAME_PLAYED_ID,
								TE_TRANSFER_STATUS,
								TE_SMIB2EGM_COMM_TYPE,
								TE_SMIB2EGM_CONF_ID,
								TE_BRAND_CODE,
								TE_MODEL,
								TE_MANUFACTURE_YEAR,
								TE_MET_HOMOLOGATED,
								TE_BET_CODE,
								TE_COIN_COLLECTION,
								TE_EQUITY_PERCENTAGE,
								TE_TERMINAL_CURRENCY_ID,
								TE_ISO_CODE,
								TE_SAS_ACCOUNTING_DENOM,
								TE_TITO_HOST_ID
		FROM TERMINALS 
		WHERE TE_TERMINAL_TYPE = 106

END
GO

UPDATE terminals 
SET te_master_id = 
	(SELECT TOP 1
		te_terminal_id
	 FROM terminals
	 WHERE te_name = 'SORTEADOR TERMINAL')
WHERE te_terminal_id = (SELECT TOP 1
		TE_TERMINAL_ID
	 FROM terminals
	 WHERE te_name = 'SORTEADOR TERMINAL')

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportTerminalDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportTerminalDraws]
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportTerminalDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pTerminalDraw	INT				= 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	SET @_Terminal_draw_type = 5
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --Filter by terminalDraw 
	IF  @pTerminalDraw = 1
	
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS ON CD_TERMINAL =  TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CD_TERMINAL = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND te_terminal_type = @_Terminal_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN		
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --Filter by TERMINAL draw
	  IF  @pTerminalDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CD_ACCOUNT_ID
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS ON CD_TERMINAL =  TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CD_TERMINAL = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND    te_terminal_type = @_Terminal_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
	END 
	
	end		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	

	--Filter by Terminal draw
	IF  @pTerminalDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS ON CD_TERMINAL =  TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CD_TERMINAL = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    te_terminal_type = @_Terminal_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  RETURN
	END
end

GO

GRANT EXECUTE ON [dbo].[ReportTerminalDraws] TO [wggui] WITH GRANT OPTION 

GO

if not exists(select gp_key_value from general_params where gp_group_key = 'StaticWebHost' and gp_subject_key = 'Folders') 
begin
	insert into general_params (gp_group_key,gp_subject_key,gp_key_value)values('StaticWebHost','Folders','')
end

if not exists(select gp_key_value from general_params where gp_group_key = 'StaticWebHost' and gp_subject_key = 'Ports') 
begin
	insert into general_params (gp_group_key,gp_subject_key,gp_key_value)values('StaticWebHost','Ports','')
end




IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig

GO

create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortesía sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortesía sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCIÓN'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoción')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   



--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenForceParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenForceParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine0', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine1', 'Bienvenido')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine2', 'Pulse[1][2][3][4][5]')
END

--
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine0', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine1', 'Su saldo es de ')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine2', '')
END
      
END

GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui]

GO

IF EXISTS(SELECT 1 
            FROM GENERAL_PARAMS 
           WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
             AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw')
BEGIN
  UPDATE GENERAL_PARAMS 
     SET GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
   WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
     AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw'
END

--CFDI--

-- CFDI - LastOperationId - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'LastOperationId')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'LastOperationId', '0')
GO

-- CFDI - Enabled - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Enabled' )
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Enabled', '0')
GO

-- CFDI - Uri - http://201.99.72.63/appConstancias/Ws_CertConstRetenciones.asmx
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Uri')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Uri', 'http://201.99.72.63/appConstancias/Ws_CertConstRetenciones.asmx')
GO
	
-- CFDI - Retries - 5
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Retries')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Retries', '5')
GO
	
-- CFDI - User - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'User')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'User', '')
GO

-- CFDI - Password - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Password')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Password', '')
GO
	
-- CFDI - Site - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Site')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Site', '')
GO
-- CFDI - Scheduling.Load - 60
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Load')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Load', '60')
GO
	
-- CFDI - Scheduling.Process - 60
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Process')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Process', '60')
GO
	
-- CFDI - Scheduling.Delay - 30
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Delay')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Delay', '30')
GO
	
-- CFDI - CFDI.FirstDate - NULL
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'FirstDate' )
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'FirstDate', NULL )
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Active')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Active', 0)
GO 

-- CFDI - CFDI.Anonymous.RFC - XAXX010101000
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Anonymous.RFC')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Anonymous.RFC', 'XAXX010101000')
GO 

-- CFDI - CFDI.Anonymous.EntidadFederativa - 02
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Anonymous.EntidadFederativa')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Anonymous.EntidadFederativa', '01')
GO
	
-- CFDI - CFDI.Foreing.RFC - XEXX010101000
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreing.RFC')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreing.RFC', 'XEXX010101000')
GO 
	
-- CFDI - CFDI.Foreign.EntidadFederativa - 01
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreign.EntidadFederativa')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreign.EntidadFederativa', '01')
GO

-- NEW TABLE CFDI_REGISTERS
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CFDI_REGISTERS')
BEGIN

CREATE TABLE [dbo].[cfdi_registers](
	[cr_operation_id] [bigint] NOT NULL,
	[cr_status] [int] NOT NULL,
	[cr_inserted] [datetime] NOT NULL,
	[cr_updated] [datetime] NULL,
	[cr_xml_request] [xml] NOT NULL,
	[cr_retries] [int] NULL,
	[cr_error_code] [int] NULL,
	[cr_error_message] [varchar](255) NULL,
 CONSTRAINT [PK_cfdi_registers] PRIMARY KEY CLUSTERED 
(
	[cr_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[federal_states]') and name = 'fs_cfdi_id')
BEGIN
  ALTER TABLE dbo.federal_states ADD fs_cfdi_id int NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cfdi_registers]') and name = 'cr_reported_data')
BEGIN
  ALTER TABLE dbo.cfdi_registers ADD cr_reported_data text NULL
END
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_operation_id_status' AND object_id = OBJECT_ID('cfdi_registers'))
BEGIN
	/****** Object:  Index [IX_operation_id_status]   ******/
	CREATE NONCLUSTERED INDEX [IX_operation_id_status] ON [dbo].[cfdi_registers] 
	(
		[cr_operation_id] ASC,
		[cr_status] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/******* RECORDS *******/

-- FILL VALUES
UPDATE   federal_states
   SET   fs_cfdi_id = (CASE WHEN fs_state_id  = 1 THEN 9
                            WHEN fs_state_id  = 2 THEN 1
                            WHEN fs_state_id  = 3 THEN 2
                            WHEN fs_state_id  = 4 THEN 3
                            WHEN fs_state_id  = 5 THEN 4
                            WHEN fs_state_id  = 6 THEN 7
                            WHEN fs_state_id  = 7 THEN 8
                            WHEN fs_state_id  = 8 THEN 5
                            WHEN fs_state_id  = 9 THEN 6
                            WHEN fs_state_id  = 10 THEN 10
                            WHEN fs_state_id  = 11 THEN 11
                            WHEN fs_state_id  = 12 THEN 12
                            WHEN fs_state_id  = 13 THEN 13
                            WHEN fs_state_id  = 14 THEN 14
                            WHEN fs_state_id  = 15 THEN 15
                            WHEN fs_state_id  = 16 THEN 16
                            WHEN fs_state_id  = 17 THEN 17
                            WHEN fs_state_id  = 18 THEN 18
                            WHEN fs_state_id  = 19 THEN 19
                            WHEN fs_state_id  = 20 THEN 20
                            WHEN fs_state_id  = 21 THEN 21
                            WHEN fs_state_id  = 22 THEN 22
                            WHEN fs_state_id  = 23 THEN 23
                            WHEN fs_state_id  = 24 THEN 24
                            WHEN fs_state_id  = 25 THEN 25
                            WHEN fs_state_id  = 26 THEN 26
                            WHEN fs_state_id  = 27 THEN 27
                            WHEN fs_state_id  = 28 THEN 28
                            WHEN fs_state_id  = 29 THEN 29
                            WHEN fs_state_id  = 30 THEN 30
                            WHEN fs_state_id  = 31 THEN 31
                            WHEN fs_state_id  = 32 THEN 32
                            END)
                            
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportMachineInputOutputBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportMachineInputOutputBalance]
GO


CREATE PROCEDURE [dbo].[ReportMachineInputOutputBalance]
  @pDateFrom        DATETIME,
  @pDateTo          DATETIME,
  @pShowTerminal    BIT,
  @pShowImbalance   BIT,
  @pTerminalWhere   NVARCHAR(MAX) 
  
AS
BEGIN  
  
  DECLARE @_QUERY   NVARCHAR(MAX)
  
  SET @_QUERY = 
  '
    SELECT 
          P.PV_NAME
        , TOTAL.* 
      FROM
      (
        SELECT'
  IF (@pShowTerminal = 1)  --GROUP BY TERMINAL
  BEGIN
    SET @_QUERY = @_QUERY + '         
              [ProviderID]                                                                               [ProviderID]                    
            , [TerminalName]                                                                             [TerminalName]                  
            , T.TerminalID                                                                               [TerminalID]                    
            , ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)						                             [SYS.Total.IN]                  
            , ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)                                  [SYS.Total.OUT]                 
	          , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 																	  
	             (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                               [SYS.Total.DIFF]                
            , ISNULL([FTToEGM],0)												                                                 [METER.Total.IN]                
            , ISNULL([FTFromEGM],0)											                                                 [METER.Total.OUT]               
	          , ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0)					                                     [METER.Total.DIFF]              
            , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)			           [DIFF.Total.IN]                 
            , (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0))	       [DIFF.Total.OUT]                
            , ((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 					  
               ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))	   [DIFF.Total]                    
            , ISNULL([SYSToEGM],0)                                                                       [SYSToEGM]                      
            , ISNULL([SYSToEGM.Cancel],0)                                                                [SYSToEGM.Cancel]
            , ISNULL([SYSFromEGM],0)                                                                     [SYSFromEGM]
            , ISNULL([SYSFromEGM.Abandoned],0)                                                           [SYSFromEGM.Abandoned]
            , ISNULL([SYSToEGM.Count],0)                                                                 [SYSToEGM.Count]
            , ISNULL([SYSFromEGM.Count],0)                                                               [SYSFromEGM.Count]
            , ISNULL([SYSFromEGM.Abandoned.Count],0)                                                     [SYSFromEGM.Abandoned.Count]
            , ISNULL([SYSToEGM.Cancel.Count],0)                                                          [SYSToEGM.Cancel.Count]
            , ISNULL([FTToEGM],0)                                                                        [FTToEGM]
            , ISNULL([FTFromEGM],0)                                                                      [FTFromEGM]
            , ISNULL([FTToEGM.Count],0)                                                                  [FTToEGM.Count]
            , ISNULL([FTFromEGM.Count],0)                                                                [FTFromEGM.Count]'

  END
  ELSE                  -- GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY +' 
              [ProviderID]
            , NULL                                                                                       [TerminalName]
            , NULL                                                                                       [TerminalID]
            , SUM(ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0))						                         [SYS.Total.IN]
            , SUM(ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                             [SYS.Total.OUT]
	          , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 															   
	                (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)))                            [SYS.Total.DIFF]
            , SUM(ISNULL([FTToEGM],0))												                                           [METER.Total.IN]
            , SUM(ISNULL([FTFromEGM],0))											                                           [METER.Total.OUT]
	          , SUM(ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0))					                                 [METER.Total.DIFF]
            , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0))			       [DIFF.Total.IN]
            , SUM((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))   [DIFF.Total.OUT]
            , SUM(((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 
                  ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))) [DIFF.Total] 
            , SUM(ISNULL([SYSToEGM],0))                                                                  [SYSToEGM]
            , SUM(ISNULL([SYSToEGM.Cancel],0))                                                           [SYSToEGM.Cancel]
            , SUM(ISNULL([SYSFromEGM],0))                                                                [SYSFromEGM]
            , SUM(ISNULL([SYSFromEGM.Abandoned],0))                                                      [SYSFromEGM.Abandoned]
            , SUM(ISNULL([SYSToEGM.Count],0))                                                            [SYSToEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Count],0))                                                          [SYSFromEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Abandoned.Count],0))                                                [SYSFromEGM.Abandoned.Count]
            , SUM(ISNULL([SYSToEGM.Cancel.Count],0))                                                     [SYSToEGM.Cancel.Count]
            , SUM(ISNULL([FTToEGM],0))                                                                   [FTToEGM]
            , SUM(ISNULL([FTFromEGM],0))                                                                 [FTFromEGM]
            , SUM(ISNULL([FTToEGM.Count],0))                                                             [FTToEGM.Count]
            , SUM(ISNULL([FTFromEGM.Count],0))                                                           [FTFromEGM.Count]'
  END  

SET @_QUERY = @_QUERY + '    
      FROM
      (
          SELECT    TE_TERMINAL_ID [TerminalID]
                  , TE_PROV_ID     [ProviderID]
                  , TE_PROVIDER_ID [ProviderNAME]
                  , TE_NAME        [TerminalName]
           FROM     TERMINALS
           WHERE    ' + @pTerminalWhere  + '
       ) T
       
       LEFT JOIN
       (   
        SELECT   AM_TERMINAL_ID   TERMINALID                                                       
               , SUM (CASE WHEN (AM_TYPE = 5 and (am_operation_id=0)) THEN ISNULL(am_sub_amount, 0) ELSE 0 END) [SYSToEGM]
               , SUM (CASE WHEN (AM_TYPE = 6 and (am_operation_id=0)) THEN ISNULL(am_add_amount, 0) ELSE 0 END) [SYSFromEGM]
               , SUM (CASE WHEN (AM_TYPE = 5 AND am_sub_amount > 0) THEN 1 ELSE 0 END)  [SYSToEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 6 AND am_add_amount > 0) THEN 1 ELSE 0 END)  [SYSFromEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 56) THEN ISNULL(am_add_amount, 0) ELSE 0 END)[SYSToEGM.Cancel]
               , SUM (CASE WHEN (AM_TYPE = 56 AND am_add_amount > 0) THEN 1 ELSE 0 END) [SYSToEGM.Cancel.Count]
          FROM   ACCOUNT_MOVEMENTS 
         WHERE   AM_TYPE IN (5,6, 56) -- StartCardSession = 5,EndCardSession = 6,CancelStartCardSession
           AND   AM_DATETIME  >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME)  
           AND   AM_DATETIME  <  CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME)  
      GROUP BY   AM_TERMINAL_ID
      ) S   ON   T.TERMINALID = S.TERMINALID 
      LEFT JOIN
      (
      SELECT   MSH_TERMINAL_ID                   [TerminalID] 
             , SUM(ISNULL(MSH_TO_GM_AMOUNT,0))   [FTToEGM]
             , SUM(ISNULL(MSH_FROM_GM_AMOUNT,0)) [FTFromEGM]
             , SUM(ISNULL(MSH_TO_GM_COUNT,0))    [FTToEGM.Count]
             , SUM(ISNULL(MSH_FROM_GM_COUNT,0))  [FTFromEGM.Count]
        FROM   MACHINE_STATS_PER_HOUR 
       WHERE   MSH_BASE_HOUR >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
         AND   MSH_BASE_HOUR < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
    GROUP BY   MSH_TERMINAL_ID
      ) M ON   T.TERMINALID = M.TERMINALID 
      LEFT JOIN 
      (
      SELECT    HP_TERMINAL_ID  
             ,  SUM (HP_AMOUNT) [SYSFROMEGM.ABANDONED]
             ,  SUM (1) [SYSFROMEGM.ABANDONED.COUNT]
       FROM     HANDPAYS 
      WHERE     HP_DATETIME >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_DATETIME < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_TYPE = 2
   GROUP BY     HP_TERMINAL_ID
     ) H ON     T.TERMINALID = HP_TERMINAL_ID'
      
  IF (@pShowTerminal = 0)  --GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY + '
      GROUP BY [ProviderID]'
  END

  SET @_QUERY = @_QUERY + '      
      ) TOTAL 
   
   INNER JOIN  PROVIDERS P 
           ON  P.PV_ID = TOTAL.PROVIDERID'
  
  
  IF (@pShowImbalance = 1) -- SHOW IMBALANCE
  BEGIN
    SET @_QUERY = @_QUERY + '
      WHERE [DIFF.Total] <> 0'
  END
  
  SET @_QUERY = @_QUERY + '
    ORDER BY 1'
    
  EXEC (@_QUERY)
 
END
GO

GRANT EXECUTE ON [dbo].[ReportMachineInputOutputBalance] TO [wggui] WITH GRANT OPTION 
GO



