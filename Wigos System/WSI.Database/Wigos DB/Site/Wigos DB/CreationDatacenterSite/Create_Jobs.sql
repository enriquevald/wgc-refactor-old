USE [msdb]
GO

/****** Object:  Job [Backup log]    Script Date: 10/19/2016 13:23:28 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Backup log')
EXEC msdb.dbo.sp_delete_job @job_id=N'bf8059f0-d51d-4678-aac9-73ff18608a22', @delete_unused_schedule=1
GO

/****** Object:  Job [jb_GamingTablesActivity]    Script Date: 10/19/2016 13:23:33 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'jb_GamingTablesActivity')
EXEC msdb.dbo.sp_delete_job @job_id=N'c9a77f24-a2b7-4d88-bdad-13521c84dc8f', @delete_unused_schedule=1
GO

/****** Object:  Job [Message purgue]    Script Date: 10/19/2016 13:23:46 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Message purgue')
EXEC msdb.dbo.sp_delete_job @job_id=N'4da95489-09d4-4f2c-90b4-34ceda4804bd', @delete_unused_schedule=1
GO

/****** Object:  Job [WigosDataPurge]    Script Date: 10/19/2016 13:24:01 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'WigosDataPurge')
EXEC msdb.dbo.sp_delete_job @job_id=N'88cbae7d-4a1f-4296-9e9b-bbe600415ea9', @delete_unused_schedule=1
GO


/********************************************************************************************************
****************************   NEW DAILY JOB   **********************************************************
*********************************************************************************************************/

/****** Object:  Job [Daily maintenance]    Script Date: 10/19/2016 13:42:02 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 10/19/2016 13:42:02 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Daily maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Daily maintenance', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Data purge]    Script Date: 10/19/2016 13:42:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Data purge', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Name AS NVARCHAR(20)  

DECLARE BBDDCursor CURSOR FOR 
                       SELECT   NAME
                         FROM   sys.databases 
                        WHERE   NAME LIKE ''wgdb____''
                          AND   NAME <> ''wgdb_100''
                        ORDER   BY NAME;  

OPEN BBDDCursor

FETCH NEXT FROM BBDDCursor INTO @Name

WHILE @@FETCH_STATUS = 0
BEGIN
    print @Name
    
    EXEC (N''use ''+ @Name + '';'')
 
    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.wcp_transactions'') AND type in (N''U''))
    BEGIN
      print @Name + N''.dbo.wcp_transactions''
      EXEC (N''truncate table ''+ @Name +''.dbo.wcp_transactions;'')
      EXEC (N''DBCC DBREINDEX (''''''+ @Name +''.dbo.wcp_transactions'''', '''''''', 0);'')
    END
    
    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.wcp_messages'') AND type in (N''U''))
    BEGIN
      print @Name + N''.dbo.wcp_messages''
      EXEC (N''truncate table ''+ @Name +''.dbo.wcp_messages;'')
      EXEC (N''DBCC DBREINDEX (''''''+ @Name +''.dbo.wcp_messages'''', '''''''', 0);'')
    END
    
    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.cj_transactions'') AND type in (N''U''))
    BEGIN
      print @Name + N''.dbo.cj_transactions''
      EXEC (N''truncate table ''+ @Name +''.dbo.cj_transactions;'')
      EXEC (N''DBCC DBREINDEX (''''''+ @Name +''.dbo.cj_transactions'''', '''''''', 0);'')
    END
    
    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.wc2_transactions'') AND type in (N''U''))
    BEGIN
      print @Name + N''.dbo.wc2_transactions''
      EXEC (N''truncate table ''+ @Name +''.dbo.wc2_transactions;'')
      EXEC (N''DBCC DBREINDEX (''''''+ @Name +''.dbo.wc2_transactions'''', '''''''', 0);'')
    END
    
    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.wc2_messages'') AND type in (N''U''))
    BEGIN
      print @Name + N''.dbo.wc2_messages''
      EXEC (N''truncate table ''+ @Name +''.dbo.wc2_messages;'')
      EXEC (N''DBCC DBREINDEX (''''''+ @Name +''.dbo.wc2_messages'''', '''''''', 0);'')
    END
    
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.MassiveRemoval_AccountMovementsPlays'') AND type in (N''P'', N''PC''))
    BEGIN
      print @Name + N''.dbo.MassiveRemoval_AccountMovementsPlays''
      EXEC (@Name + N''.dbo.MassiveRemoval_AccountMovementsPlays'')
    END
    
    FETCH NEXT FROM BBDDCursor INTO @Name
END

CLOSE BBDDCursor
DEALLOCATE BBDDCursor', 
		@database_name=N'wgdb_000', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20070717, 
		@active_end_date=99991231, 
		@active_start_time=60000, 
		@active_end_time=235959, 
		@schedule_uid=N'e4f44266-ac58-424f-a078-32780125bfcc'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

/********************************************************************************************************
****************************   NEW HOURLY JOB   *********************************************************
*********************************************************************************************************/

/****** Object:  Job [Hourly Maintenance]    Script Date: 10/19/2016 13:42:40 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 10/19/2016 13:42:40 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Hourly Maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Hourly Maintenance', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [BACKUP log]    Script Date: 10/19/2016 13:42:40 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'BACKUP log', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Name AS NVARCHAR(20)  

DECLARE BBDDCursor CURSOR FOR 
                       SELECT   NAME
                         FROM   sys.databases 
                        WHERE   NAME LIKE ''wgdb____''
                          AND   NAME <> ''wgdb_100''
                        ORDER   BY NAME;  

OPEN BBDDCursor

FETCH NEXT FROM BBDDCursor INTO @Name

WHILE @@FETCH_STATUS = 0
BEGIN
    print @Name
    
    EXEC (N''use ''+ @Name + '';'')
    EXEC (N''backup log ''+ @Name +'' to disk=''''NUL:'''';'')
   
    FETCH NEXT FROM BBDDCursor INTO @Name
END

CLOSE BBDDCursor
DEALLOCATE BBDDCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [INSERT gaming_tables_connected]    Script Date: 10/19/2016 13:42:40 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'INSERT gaming_tables_connected', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Name AS NVARCHAR(20)  

DECLARE BBDDCursor CURSOR FOR 
                       SELECT   NAME
                         FROM   sys.databases 
                        WHERE   NAME LIKE ''wgdb____''
                          AND   NAME <> ''wgdb_100''
                        ORDER   BY NAME;  

OPEN BBDDCursor

FETCH NEXT FROM BBDDCursor INTO @Name

WHILE @@FETCH_STATUS = 0
BEGIN
    print @Name
    
    EXEC (N''use ''+ @Name + '';'')
    
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@Name + N''.dbo.sp_GenerateTablesActivity'') AND type in (N''P'', N''PC''))
    BEGIN
      print @Name + N''.dbo.sp_GenerateTablesActivity''
      EXEC (@Name + N''.dbo.sp_GenerateTablesActivity'')
    END
   
    FETCH NEXT FROM BBDDCursor INTO @Name
END

CLOSE BBDDCursor
DEALLOCATE BBDDCursor', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Hourly', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20161001, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'69befe84-61c7-43e5-90e1-95d5c3222080'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

