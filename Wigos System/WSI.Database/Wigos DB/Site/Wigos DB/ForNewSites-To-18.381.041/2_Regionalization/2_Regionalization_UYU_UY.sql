/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int;

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 100;

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
GO

/**** FUNCTION SECTION *****/
CREATE FUNCTION [dbo].[TMP_SplitStringIntoTable] 
(
  @pString VARCHAR(MAX),
  @pDelimiter VARCHAR(10),
  @pTrimItems BIT = 1
)
RETURNS 
@ReturnTable TABLE 
(
  [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
  [SST_VALUE] [VARCHAR](100) NULL
)
AS
BEGIN		   
  DECLARE @_ISPACES INT
  DECLARE @_PART VARCHAR(50)
  
  -- INITIALIZE SPACES
  SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  WHILE @_ISPACES > 0
  
  BEGIN
    SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))
  
    IF @pTrimItems = 1
    SET @_PART = LTRIM(RTRIM(@_PART))
  
    INSERT INTO @ReturnTable(SST_VALUE) SELECT @_PART
  
    SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))
  
    SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  END
  
  IF LEN(@pString) > 0
    INSERT INTO @ReturnTable
  	  SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END
  
    RETURN 
  END  
GO  

CREATE PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
  @pBill AS DECIMAL
AS
BEGIN   
  DECLARE @_meter_code AS INTEGER
	
  SET @_meter_code = -1
  
  SELECT @_meter_code = 
    CASE WHEN @pBill = 1       THEN 64	
         WHEN @pBill = 2       THEN 65	
         WHEN @pBill = 5       THEN 66	
         WHEN @pBill = 10      THEN 67	
         WHEN @pBill = 20      THEN 68	
         WHEN @pBill = 25      THEN 69	
         WHEN @pBill = 50      THEN 70	
         WHEN @pBill = 100     THEN 71	
         WHEN @pBill = 200     THEN 72	
         WHEN @pBill = 250     THEN 73	
         WHEN @pBill = 500     THEN 74	
         WHEN @pBill = 1000    THEN 75	
         WHEN @pBill = 2000    THEN 76	
         WHEN @pBill = 2500    THEN 77	
         WHEN @pBill = 5000    THEN 78	
         WHEN @pBill = 10000   THEN 79	
         WHEN @pBill = 20000   THEN 80	
         WHEN @pBill = 25000   THEN 81	
         WHEN @pBill = 50000   THEN 82	
         WHEN @pBill = 100000  THEN 83	
         WHEN @pBill = 200000  THEN 84	
         WHEN @pBill = 250000  THEN 85	
         WHEN @pBill = 500000  THEN 86	
         WHEN @pBill = 1000000 THEN 87	
    END
  
  IF (ISNULL(@_meter_code,-1) = -1)
    SELECT 'Meter code not inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE IF (EXISTS(SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002 AND SMCG_METER_CODE = @_meter_code))
    SELECT 'Bill already inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE
    INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID,SMCG_METER_CODE) VALUES (10002,@_meter_code)
  
END  
GO 

/**** DECLARATION SECTION *****/
DECLARE @CurrencyISOCode         AS VARCHAR(3)
DECLARE @CurrencyDescription     AS VARCHAR(MAX)
DECLARE @CurrenciesDenominations AS VARCHAR(MAX)
DECLARE @CurrencyDecimals        AS INTEGER
DECLARE @FormattedDecimals       AS INTEGER
DECLARE @CountryCode             AS VARCHAR(2)
DECLARE @CurrencySymbol          AS VARCHAR(5)
DECLARE @CommonLanguage          AS VARCHAR(5)

SET @CurrencyISOCode         = 'UYU'
SET @CurrencyDescription     = 'Peso Uruguayo'
SET @CurrenciesDenominations = '-200,-100,-50,-2,-1,20,50,100,200,500,1000,2000' -- Negative values are reserved denominations. Specify bills denominations.
SET @CurrencyDecimals        = 0
SET @FormattedDecimals       = -2
SET @CountryCode             = 'UY'
SET @CurrencySymbol          = '$U'    
SET @CommonLanguage          = 'es' -- 'es' OR 'en-US'

/**** UPDATE GENERAL PARAMS *****/
-- CurrenciesAccepted
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrenciesAccepted', @CurrencyISOCode)

-- CurrencyISOCode   
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrencyISOCode', @CurrencyISOCode)
         
-- CountryISOCode2
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CountryISOCode2', @CountryCode)
         
-- CurrencySymbol
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI') AND GP_SUBJECT_KEY = 'CurrencySymbol'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'CurrencySymbol', @CurrencySymbol)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'CurrencySymbol', @CurrencySymbol)
 
-- DefaultCountry
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.DefaultValues','Country',@CountryCode)

-- CommonLenguage
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI' OR GP_GROUP_KEY = 'Cashier') AND GP_SUBJECT_KEY = 'Language'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier',  'Language', @CommonLanguage)

-- Decimal Rounding Enabled
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'DecimalRoundingEnabled'
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Cashier', 'DecimalRoundingEnabled', '1' );
 
-- MinCashableCents
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'MinCashableCents'
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Cashier', 'MinCashableCents', '100' );
 
-- Remove all catalog of bills
DELETE FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002

/**** UPDATE DATA *****/  

DELETE FROM CURRENCY_DENOMINATIONS
DELETE FROM CURRENCIES
  
DECLARE @CurrentCurrencyDenom   AS DECIMAL
  
-- INSERT CURRENCY IN CURRENCIES TABLE
IF NOT EXISTS (SELECT 1 CUR_ISO_CODE FROM CURRENCIES WHERE CUR_ISO_CODE = @CurrencyISOCode)
  INSERT INTO CURRENCIES (CUR_ISO_CODE, CUR_ALLOWED) VALUES (@CurrencyISOCode, 0)
         
-- INSERT CURRENCY PAYMENT TYPE IN CURRENCY_EXCHANGE
IF NOT EXISTS (SELECT CE_CURRENCY_ISO_CODE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
  INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
                         VALUES (0, @CurrencyISOCode, @CurrencyDescription, 1, @CurrencyDecimals, 0, 0, 0, 0, 1, @FormattedDecimals)

-- INSERT CARD PAYMENT TYPE IN CURRENCY_EXCHANGE  
IF NOT EXISTS (SELECT CE_CURRENCY_ISO_CODE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 1 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
  INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
                         VALUES (1, @CurrencyISOCode, 'Tarjeta Bancaria', 1, @CurrencyDecimals, 0, 0, 0, 0, 0, @FormattedDecimals)

-- INSERT CHECK PAYMENT TYPE IN CURRENCY_EXCHANGE  		   
IF NOT EXISTS (SELECT CE_CURRENCY_ISO_CODE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 2 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
  INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
                         VALUES (2, @CurrencyISOCode, 'Cheque', 1, @CurrencyDecimals, 0, 0, 0, 0, 0, @FormattedDecimals)
    
-- INSERT CURRENCY AND CAGE DENOMINATIONS
SELECT SST_VALUE INTO #TMP_CURRENCY_DEN FROM [TMP_SplitStringIntoTable] (@CurrenciesDenominations, ',', 1)
    
DECLARE Curs_Denoms CURSOR FOR SELECT CAST(SST_VALUE AS DECIMAL) FROM #TMP_CURRENCY_DEN 

SET NOCOUNT ON;
OPEN Curs_Denoms

FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom
  
WHILE @@FETCH_STATUS = 0
  BEGIN      
    IF NOT EXISTS (SELECT * FROM CAGE_AMOUNTS WHERE CAA_ISO_CODE = @CurrencyISOCode AND CAA_DENOMINATION = @CurrentCurrencyDenom)
      INSERT INTO CAGE_AMOUNTS (CAA_ISO_CODE , CAA_DENOMINATION , CAA_ALLOWED) VALUES (@CurrencyISOCode , @CurrentCurrencyDenom , 1)
    
	  IF @CurrentCurrencyDenom > 0
	    EXEC dbo.TMP_InsertInSasMeterCatalog @CurrentCurrencyDenom
	
    FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom    
  END
    
CLOSE Curs_Denoms
DEALLOCATE Curs_Denoms   
DROP TABLE #TMP_CURRENCY_DEN
DROP FUNCTION [dbo].[TMP_SplitStringIntoTable] 
DROP PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[currency_exchange]') and name = 'ce_language_resources')
  ALTER TABLE dbo.currency_exchange ADD ce_language_resources XML NULL
GO

UPDATE CURRENCY_EXCHANGE SET CE_LANGUAGE_RESOURCES = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>pesos</PluralCurrency>
   <SingularCurrency>peso</SingularCurrency>
   <PluralCents>cents</PluralCents>
   <SingularCents>cent</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>pesos</PluralCurrency>
   <SingularCurrency>peso</SingularCurrency>
   <PluralCents>centÚsimos</PluralCents>
   <SingularCents>centÚsimo</SingularCents>
 </NLS10>
</LanguageResources>'
WHERE CE_CURRENCY_ISO_CODE = 'UYU' AND ce_type = 0
GO
