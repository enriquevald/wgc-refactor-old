/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

-- Take gp for check the regionalization

IF NOT EXISTS (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
  BEGIN   
    SELECT 'Needed regionalization for update to 258'
    RAISERROR('Needed regionalization for update to 258', 20, -1) WITH LOG  
  END

DECLARE @_gp_value AS VARCHAR(10);
SELECT @_gp_value = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

IF (@_gp_value = 'BZD')
  BEGIN   
    SELECT 'This database was regionalized for BZD and it don''t allow to add the same currency like foreign currency'
    RAISERROR('This database was regionalized for BZD and it don''t allow to add the same currency like foreign currency', 20, -1) WITH LOG  
  END
GO

/**** FUNCTION SECTION *****/

CREATE FUNCTION [dbo].[TMP_SplitStringIntoTable] 
(
  @pString VARCHAR(MAX),
  @pDelimiter VARCHAR(10),
  @pTrimItems BIT = 1
)
RETURNS 
@ReturnTable TABLE 
(
  [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
  [SST_VALUE] [VARCHAR](100) NULL
)
AS
BEGIN		   
  DECLARE @_ISPACES INT
  DECLARE @_PART VARCHAR(50)
  
  -- INITIALIZE SPACES
  SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  WHILE @_ISPACES > 0
  
  BEGIN
    SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))
  
    IF @pTrimItems = 1
    SET @_PART = LTRIM(RTRIM(@_PART))
  
    INSERT INTO @ReturnTable(SST_VALUE) SELECT @_PART
  
    SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))
  
    SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  END
  
  IF LEN(@pString) > 0
    INSERT INTO @ReturnTable
  	  SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END
  
    RETURN 
  END  

GO 
 
CREATE PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
  @pBill AS DECIMAL
AS
BEGIN   
  DECLARE @_meter_code AS INTEGER
	
  SET @_meter_code = -1
  
  SELECT @_meter_code = 
    CASE WHEN @pBill = 1       THEN 64	
         WHEN @pBill = 2       THEN 65	
         WHEN @pBill = 5       THEN 66	
         WHEN @pBill = 10      THEN 67	
         WHEN @pBill = 20      THEN 68	
         WHEN @pBill = 25      THEN 69	
         WHEN @pBill = 50      THEN 70	
         WHEN @pBill = 100     THEN 71	
         WHEN @pBill = 200     THEN 72	
         WHEN @pBill = 250     THEN 73	
         WHEN @pBill = 500     THEN 74	
         WHEN @pBill = 1000    THEN 75	
         WHEN @pBill = 2000    THEN 76	
         WHEN @pBill = 2500    THEN 77	
         WHEN @pBill = 5000    THEN 78	
         WHEN @pBill = 10000   THEN 79	
         WHEN @pBill = 20000   THEN 80	
         WHEN @pBill = 25000   THEN 81	
         WHEN @pBill = 50000   THEN 82	
         WHEN @pBill = 100000  THEN 83	
         WHEN @pBill = 200000  THEN 84	
         WHEN @pBill = 250000  THEN 85	
         WHEN @pBill = 500000  THEN 86	
         WHEN @pBill = 1000000 THEN 87	
    END
  
  IF (ISNULL(@_meter_code,-1) = -1)
    SELECT 'Meter code not inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE IF (NOT EXISTS(SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002 AND SMCG_METER_CODE = @_meter_code))
    INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID,SMCG_METER_CODE) VALUES (10002,@_meter_code)
    
END    

GO

/**** DECLARATION SECTION *****/

DECLARE @CurrencyISOCode        AS VARCHAR(3)
DECLARE @CurrenciesAccepted     AS VARCHAR(MAX)
DECLARE @CurrencyDenominations  AS VARCHAR(MAX)
DECLARE @CurrencyName           AS VARCHAR(MAX)  
DECLARE @CurrencyDecimals       AS INTEGER
DECLARE @FormattedDecimals      AS INTEGER

SET @CurrencyISOCode         = 'BZD'
SET @CurrencyName            = 'Belize Dollar'
SET @CurrencyDenominations   = '-200,-100,-50,-2,-1,2,5,10,20,50,100' 
SET @CurrencyDecimals        = 2 
SET @FormattedDecimals       = NULL

/**** UPDATE GENERAL PARAMS *****/

-- CurrenciesAccepted
IF EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted')
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = GP_KEY_VALUE + ';' + @CurrencyISOCode WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
ELSE
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrenciesAccepted', @CurrencyISOCode)

/**** UPDATE DATA *****/  
-- ADD ALL CURRENCIES AND DENOMINATIONS
  
DECLARE @CurrentCurrencyDenom   AS DECIMAL
              
-- INSERT CURRENCY IN CURRENCIES TABLE
IF NOT EXISTS (SELECT TOP 1 CUR_ISO_CODE FROM CURRENCIES WHERE CUR_ISO_CODE = @CurrencyISOCode)
  INSERT INTO CURRENCIES (CUR_ISO_CODE, CUR_ALLOWED) VALUES (@CurrencyISOCode, 0)
         
-- INSERT CURRENCY PAYMENT TYPE IN CURRENCY_EXCHANGE
IF NOT EXISTS (SELECT TOP 1 CE_CURRENCY_ISO_CODE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
    INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
	       VALUES (0, @CurrencyISOCode, @CurrencyName, 0, @CurrencyDecimals, 0, 0, 0, 0, 0, @FormattedDecimals)
  
-- INSERT CARD PAYMENT TYPE IN CURRENCY_EXCHANGE  
IF NOT EXISTS (SELECT TOP 1 CE_CURRENCY_ISO_CODE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 1 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
    INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
         VALUES (1, @CurrencyISOCode, 'Tarjeta Bancaria', 0, @CurrencyDecimals, 0, 0, 0, 0, 0, @FormattedDecimals)

-- INSERT CHECK PAYMENT TYPE IN CURRENCY_EXCHANGE  		   
IF NOT EXISTS (SELECT 1 FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 2 AND CE_CURRENCY_ISO_CODE = @CurrencyISOCode)
    INSERT INTO CURRENCY_EXCHANGE (CE_TYPE, CE_CURRENCY_ISO_CODE, CE_DESCRIPTION, CE_CHANGE, CE_NUM_DECIMALS, CE_VARIABLE_COMMISSION, CE_FIXED_COMMISSION, CE_VARIABLE_NR2, CE_FIXED_NR2, CE_STATUS, CE_FORMATTED_DECIMALS)
         VALUES (2, @CurrencyISOCode, 'Cheque', 0, @CurrencyDecimals, 0, 0, 0, 0, 0, @FormattedDecimals)
    
-- INSERT CURRENCY AND CAGE DENOMINATIONS
SELECT SST_VALUE INTO #TMP_CURRENCY_DEN FROM [TMP_SplitStringIntoTable] (@CurrencyDenominations, ',', 1)
    
DECLARE Curs_Denoms CURSOR FOR SELECT CAST(SST_VALUE AS DECIMAL) FROM #TMP_CURRENCY_DEN 

SET NOCOUNT ON;
OPEN Curs_Denoms

FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom
  
WHILE @@FETCH_STATUS = 0
  BEGIN      
    IF NOT EXISTS (SELECT * FROM CAGE_CURRENCIES WHERE CGC_ISO_CODE = @CurrencyISOCode AND CGC_DENOMINATION = @CurrentCurrencyDenom)
      INSERT INTO CAGE_CURRENCIES (CGC_ISO_CODE , CGC_DENOMINATION , CGC_ALLOWED, CGC_CAGE_CURRENCY_TYPE) VALUES (@CurrencyISOCode , @CurrentCurrencyDenom , 1, 0)
	
    IF @CurrentCurrencyDenom > 0
	    EXEC dbo.TMP_InsertInSasMeterCatalog @CurrentCurrencyDenom
	
    FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom    
  END 
 
CLOSE Curs_Denoms
DEALLOCATE Curs_Denoms   
DROP TABLE #TMP_CURRENCY_DEN
DROP PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog]
DROP FUNCTION [dbo].[TMP_SplitStringIntoTable] 

UPDATE	CURRENCY_EXCHANGE SET CE_LANGUAGE_RESOURCES = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>Belize Dollars</PluralCurrency>
   <SingularCurrency>Belize Dollar</SingularCurrency>
   <PluralCents>cents</PluralCents>
   <SingularCents>cent</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>Dólares Beliceños</PluralCurrency>
   <SingularCurrency>Dólar Beliceño</SingularCurrency>
   <PluralCents>centavos</PluralCents>
   <SingularCents>centavo</SingularCents>
 </NLS10>
</LanguageResources>'
WHERE CE_CURRENCY_ISO_CODE = @CurrencyISOCode AND CE_TYPE = 0
GO

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'ce_configuration' AND Object_ID = Object_ID(N'CURRENCY_EXCHANGE'))
	BEGIN
	 UPDATE CURRENCY_EXCHANGE
			 SET CE_CONFIGURATION =
	'<configuration>
		  <data>
				<type>0</type>
				<scope>0</scope>
				<fixed_commission>'+ CAST(CE_FIXED_COMMISSION as nvarchar)+'</fixed_commission>
				<variable_commission>' + CAST(CE_VARIABLE_COMMISSION as nvarchar)+ '</variable_commission>
				<fixed_nr2>'+CAST(CE_FIXED_NR2 as nvarchar)+'</fixed_nr2>
				<variable_nr2>'+CAST(CE_VARIABLE_NR2 as nvarchar) +'</variable_nr2>
		  </data>     
	</configuration>
	'
	WHERE CE_CONFIGURATION IS NULL
END
GO