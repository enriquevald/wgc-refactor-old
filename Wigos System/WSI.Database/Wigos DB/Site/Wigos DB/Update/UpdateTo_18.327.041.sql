/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 326;

SET @New_ReleaseId = 327;
SET @New_ScriptName = N'UpdateTo_18.327.041.sql';
SET @New_Description = N'GP GamingTables-Mode';

/**** CHECK VERSION SECTION *****/
--IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
--BEGIN
--/**** VERSION FAILURE SECTION *****/
--SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
--FROM DB_VERSION

--raiserror('Not updated', 20, -1) with log
--END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

DECLARE @GAMING_TABLES_ENABLED    AS INT
DECLARE @PLAYERTRACKING_ENABLED   AS INT
DECLARE @GAMING_TABLES_MODE       AS INT

SET @GAMING_TABLES_MODE = (SELECT  GP_KEY_VALUE
                             FROM  GENERAL_PARAMS
                            WHERE  GP_GROUP_KEY = 'GamingTables'
                              AND  GP_SUBJECT_KEY = 'Mode')
--Already exists the 'Mode' parameter
IF (@GAMING_TABLES_MODE IS NULL)
BEGIN
  SET @GAMING_TABLES_ENABLED = ISNULL((SELECT  GP_KEY_VALUE
                                         FROM  GENERAL_PARAMS
                                        WHERE  GP_GROUP_KEY = 'GamingTables'
                                          AND  GP_SUBJECT_KEY = 'Enabled'), 0)

  SET @PLAYERTRACKING_ENABLED = ISNULL((SELECT  GP_KEY_VALUE
                                          FROM  GENERAL_PARAMS
                                         WHERE  GP_GROUP_KEY = 'GamingTable.PlayerTracking'
                                           AND  GP_SUBJECT_KEY = 'Enabled'), 0)

  --Mode = 0: gaming tables are disabled
  --Mode = 1: gaming tables are enabled and PT are enabled
  --Mode = 2: gaming tables are enabled and PT are disabled
  SET @GAMING_TABLES_MODE = 0

  IF (@GAMING_TABLES_ENABLED = 1)
  BEGIN
      SET @GAMING_TABLES_MODE = 2

      IF (@PLAYERTRACKING_ENABLED = 1)
      BEGIN
          SET @GAMING_TABLES_MODE = 1
      END
  END

  --Update the 'GamingTables.Enabled' to 'GamingTables.Mode' parameter
  UPDATE GENERAL_PARAMS
  SET    GP_KEY_VALUE = @GAMING_TABLES_MODE
       , GP_SUBJECT_KEY = 'Mode'
  WHERE  GP_GROUP_KEY = 'GamingTables'
    AND  GP_SUBJECT_KEY = 'Enabled';
END

GO

/******* PROCEDURES *******/
