/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 185;

SET @New_ReleaseId = 193;
SET @New_ScriptName = N'UpdateTo_18.193.037.sql';
SET @New_Description = N'Changes for ELP, Chisps SP and Historical Movements';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collection_details]') and name = 'mcd_num_expected')
  ALTER TABLE [dbo].[money_collection_details] ALTER COLUMN [mcd_num_expected] [money] NOT NULL
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collection_details]') and name = 'mcd_num_collected')
  ALTER TABLE [dbo].[money_collection_details] ALTER COLUMN [mcd_num_collected] [money] NOT NULL
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_money_collection_details_mcd_num_expected]') AND type in (N'D'))
  ALTER TABLE [money_collection_details] DROP CONSTRAINT [DF_money_collection_details_mcd_num_expected];
GO
  
ALTER TABLE dbo.money_collection_details ADD CONSTRAINT DF_money_collection_details_mcd_num_expected DEFAULT 0 FOR mcd_num_expected
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_money_collection_details_mcd_num_collected]') AND type in (N'D'))
  ALTER TABLE [money_collection_details] DROP CONSTRAINT [DF_money_collection_details_mcd_num_collected];
GO

ALTER TABLE dbo.money_collection_details ADD CONSTRAINT DF_money_collection_details_mcd_num_collected DEFAULT 0 FOR mcd_num_collected
GO

/******* INDEXES  *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[money_collections]') AND name = N'IX_mc_terminal_id')
  CREATE NONCLUSTERED INDEX [IX_mc_terminal_id] ON [dbo].[money_collections] 
  (
        [mc_terminal_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[money_collections]') AND name = N'IX_mc_cashier_session_terminal_id')
  CREATE NONCLUSTERED INDEX [IX_mc_cashier_session_terminal_id] ON [dbo].[money_collections] 
  (
        [mc_cashier_session_id] ASC,
        [mc_terminal_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters]') AND name = N'IX_tsm_delta_updating_meter_code')
  CREATE NONCLUSTERED INDEX [IX_tsm_delta_updating_meter_code] ON [dbo].[terminal_sas_meters] 
  (
        [tsm_delta_updating] ASC,
        [tsm_meter_code] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO

CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pCashiers INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_CASHIERS      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_CASHIERS    =   ISNULL(@pCashiers, 1)
SET @_CASHIERS_NAME = ISNULL(@pCashierGroupName, '---')

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           , CS_SESSION_ID AS SESSION_ID
           , GT_NAME AS GT_NAME
           , CS_OPENING_DATE AS SESSION_DATE
           , GTT_NAME AS GTT_NAME
           , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   CASHIER_MOVEMENTS
         INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
         INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID            = CT_CASHIER_ID
              LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID
         INNER JOIN   GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
         INNER JOIN   GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
     WHERE   GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
       AND   GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
       AND   GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
       AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <  @_DATE_TO
       AND  GT_HAS_INTEGRATED_CASHIER = 1
       AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
  GROUP BY   GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
  ORDER BY   GTT_NAME
  
-- Check if cashiers must be visible  
IF @_CASHIERS = 1 
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              , CM_SESSION_ID AS SESSION_ID
              , CT_NAME as GT_NAME
              , CS_OPENING_DATE AS SESSION_DATE
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
   INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
    LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
          AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
          AND   CM_DATE >= @_DATE_FROM
          AND   CM_DATE <  @_DATE_TO
     GROUP BY   CT_NAME, CM_SESSION_ID, CS_OPENING_DATE

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO


-- Drops
IF OBJECT_ID (N'dbo.SP_GenerateHistoricalMovements_ByHour', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_GenerateHistoricalMovements_ByHour;                 
GO  

/*
----------------------------------------------------------------------------------------------------------------
PERFOM HISTORY OF CASHIER AND MOBILE BANK MOVEMENTS PER HOUR

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    12-FEB-2014    RMS      First Release
1.0.1    17-FEB-2014    RMS      Added parameter for Currency ISO Code

Parameters:
   -- MAXITEMSPERLOOP:    Maximum number of items to process.
                      
 Results:
   Return the number of items processed and the total of pending items to process before start.
----------------------------------------------------------------------------------------------------------------   
*/

CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
@pMaxItemsPerLoop Integer, 
@pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

-- Declarations
DECLARE @_max_movements_per_loop Integer
DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                                          CMPH_SUB_TYPE Integer)   
DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                                          UM_TYPE Integer, 
                                          UM_SUBTYPE Integer, 
                                          UM_CURRENCY_ISO_CODE NVarChar(3), 
                                          UM_CURRENCY_DENOMINATION Money)

-- Initialize max items per loop
SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- If there are no items in pending movements table
IF @_total_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), CAST(0 AS Integer)
   
   RETURN
END

-- Get the movements id's to be updated
DELETE   TOP(@_max_movements_per_loop) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
OUTPUT   DELETED.CMPH_MOVEMENT_ID
       , DELETED.CMPH_SUB_TYPE 
  INTO   @_tmp_updating_movements(CMPH_MOVEMENT_ID, CMPH_SUB_TYPE)   

-- Obtain the number of movements to update
SELECT   @_updated_movements_count = COUNT(*) 
  FROM   @_tmp_updating_movements 

-- If there are items to update process it
IF @_updated_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), @_total_movements_count 
   
   RETURN
END

-- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
  INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
  FROM
       (
         SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
                , CM_TYPE
                , 0 AS CM_SUB_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
                , COUNT(CM_TYPE)          AS CM_TYPE_COUNT
                , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
                , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
                , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
                , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
                , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
           FROM   @_tmp_updating_movements
          INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
          WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
                , CM_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)
       ) AS X 

---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
            , MBM_TYPE   AS CM_TYPE
            , 1          AS CM_SUB_TYPE
            , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
            , 0                AS CM_CURRENCY_DENOMINATION
            , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
            , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
            , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
            , 0 AS CM_AUX_AMOUNT
            , 0 AS CM_INITIAL_BALANCE
            , 0 AS CM_FINAL_BALANCE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
   GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
            , MBM_TYPE 

-- Update the items already in database triggering the updates into a temporary table of updated elements
UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
   SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION 
  INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION)
  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
 INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                                                AND T.CM_TYPE                  = S.CM_TYPE
                                                AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                                                AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                                                AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 

-- Insert the elements not previously updated (update items not present in temporary updated items)
INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
           (   CM_DATE
             , CM_TYPE
             , CM_SUB_TYPE
             , CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT     
             , CM_ADD_AMOUNT      
             , CM_AUX_AMOUNT     
             , CM_INITIAL_BALANCE 
             , CM_FINAL_BALANCE  )
     SELECT   CM_DATE
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_TYPE_COUNT
            , CM_SUB_AMOUNT     
            , CM_ADD_AMOUNT      
            , CM_AUX_AMOUNT     
            , CM_INITIAL_BALANCE 
            , CM_FINAL_BALANCE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                           FROM   @_tmp_updated_movements NTI
                          WHERE   NTI.UM_DATE                  = CM_DATE
                            AND   NTI.UM_TYPE                  = CM_TYPE
                            AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                            AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                            AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION) 

-- Return values
SELECT @_updated_movements_count, @_total_movements_count - @_updated_movements_count

-- Erase the temporary data
DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 
--

END  -- End Procedure
GO

/******* RECORDS *******/
--INSERT DE LAS 9 PROMOCIONES RESTRINGIDAS A GRUPOS
/* Promotion - Externa Restringida 01 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 101)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 01'
             , 0
             , 101
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 01 */

/* Promotion - Externa Restringida 02 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 102)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 02'
             , 0
             , 102
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 02 */

/* Promotion - Externa Restringida 03 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 103)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 03'
             , 0
             , 103
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 03 */

/* Promotion - Externa Restringida 04 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 104)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 04'
             , 0
             , 104
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 04 */

/* Promotion - Externa Restringida 05 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 105)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 05'
             , 0
             , 105
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 05 */

/* Promotion - Externa Restringida 06 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 106)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 06'
             , 0
             , 106
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 06 */

/* Promotion - Externa Restringida 07 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 107)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 07'
             , 0
             , 107
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 07 */

/* Promotion - Externa Restringida 08 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 108)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 08'
             , 0
             , 108
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 07 */

/* Promotion - Externa Restringida 09 */
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 109)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Externa Restringida 09'
             , 0
             , 109
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 30
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO
/* Promotion - Externa Restringida 09 */
