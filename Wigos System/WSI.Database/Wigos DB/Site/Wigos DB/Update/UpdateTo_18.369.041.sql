/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 368;

SET @New_ReleaseId = 369;
SET @New_ScriptName = N'UpdateTo_18.369.041.sql';
SET @New_Description = N'21737 - No se puede cancelar recaudaciones de Stackers'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******  ALARMS  *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT  * FROM  sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[Trigger_audit_TicketStatusChange]') AND  type in (N'TR'))
  DROP TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
GO
					
CREATE TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
ON [dbo].[TICKETS]
  AFTER INSERT, UPDATE
  NOT FOR REPLICATION
AS
BEGIN

  SET NOCOUNT ON;
  
  DECLARE @Action as char(1);
  SET @Action = (CASE WHEN EXISTS (SELECT * FROM INSERTED) 
                       AND EXISTS (SELECT * FROM DELETED)  THEN 'U'  -- Set Action to Updated.
                      WHEN EXISTS (SELECT * FROM INSERTED) THEN 'I'  -- Set Action to Insert.
                      WHEN EXISTS (SELECT * FROM DELETED)  THEN 'D'  -- Set Action to Deleted.
                 ELSE NULL                                           -- Skip. It may have been a "failed delete".   
                 END)
  
  IF @Action = 'I' OR UPDATE (TI_STATUS) OR UPDATE(TI_CAGE_MOVEMENT_ID)	OR UPDATE(TI_REJECT_REASON_EGM)	OR UPDATE(TI_REJECT_REASON_WCP)	
    INSERT INTO   TICKETS_AUDIT_STATUS_CHANGE
              (   TIA_INSERT_DATE
                , TIA_TICKET_ID
                , TIA_VALIDATION_NUMBER
                , TIA_AMOUNT
                , TIA_STATUS
	              , TIA_OLD_STATUS
                , TIA_CREATED_DATETIME
                , TIA_CREATED_TERMINAL_ID
                , TIA_MONEY_COLLECTION_ID
                , TIA_CREATED_ACCOUNT_ID
                , TIA_COLLECTED
                , TIA_LAST_ACTION_TERMINAL_ID
                , TIA_LAST_ACTION_DATETIME
                , TIA_LAST_ACTION_ACCOUNT_ID
                , TIA_MACHINE_NUMBER
                , TIA_TRANSACTION_ID
                , TIA_CREATED_PLAY_SESSION_ID
                , TIA_CANCELED_PLAY_SESSION_ID
                , TIA_DB_INSERTED
                , TIA_COLLECTED_MONEY_COLLECTION
                , TIA_CAGE_MOVEMENT_ID
                , TIA_TYPE_ID	 
                , TIA_CREATED_TERMINAL_TYPE
                , TIA_EXPIRATION_DATETIME					
                , TIA_REJECT_REASON_EGM
                , TIA_REJECT_REASON_WCP
              )
         SELECT   GETDATE()
                , I.TI_TICKET_ID
                , I.TI_VALIDATION_NUMBER
                , I.TI_AMOUNT
                , I.TI_STATUS
                , D.TI_STATUS     
                , I.TI_CREATED_DATETIME
                , I.TI_CREATED_TERMINAL_ID
                , I.TI_MONEY_COLLECTION_ID
                , I.TI_CREATED_ACCOUNT_ID
                , I.TI_COLLECTED
                , I.TI_LAST_ACTION_TERMINAL_ID
                , I.TI_LAST_ACTION_DATETIME
                , I.TI_LAST_ACTION_ACCOUNT_ID
                , I.TI_MACHINE_NUMBER
                , I.TI_TRANSACTION_ID
                , I.TI_CREATED_PLAY_SESSION_ID
                , I.TI_CANCELED_PLAY_SESSION_ID
                , I.TI_DB_INSERTED
                , I.TI_COLLECTED_MONEY_COLLECTION
                , I.TI_CAGE_MOVEMENT_ID
                , I.TI_TYPE_ID	 
                , I.TI_CREATED_TERMINAL_TYPE
                , I.TI_EXPIRATION_DATETIME					
                , I.TI_REJECT_REASON_EGM
                , I.TI_REJECT_REASON_WCP
           FROM   INSERTED I
      LEFT JOIN   DELETED D
             ON   I.TI_TICKET_ID = D.TI_TICKET_ID

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF
	
END -- [Trigger_Audit_TicketStatusChange]

