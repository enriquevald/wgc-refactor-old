/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 66;

SET @New_ReleaseId = 67;
SET @New_ScriptName = N'UpdateTo_18.067.015.sql';
SET @New_Description = N'New columns in table play_sessions.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/* play_sessions */

/* play_sessions.ps_re_cash_in */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_cash_in')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_re_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_re_cash_in] DEFAULT ((0))
ELSE
  SELECT '***** Field play_sessions.ps_re_cash_in already exists *****';
GO

/* play_sessions.ps_promo_re_cash_in */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_re_cash_in')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_promo_re_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_promo_re_cash_in] DEFAULT ((0))
ELSE
  SELECT '***** Field play_sessions.ps_promo_re_cash_in already exists *****';
GO

/* play_sessions.ps_re_cash_out */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_cash_out')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_re_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_re_cash_out] DEFAULT ((0))
ELSE
  SELECT '***** Field play_sessions.ps_re_cash_out already exists *****';
GO

/* play_sessions.ps_promo_re_cash_out */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_re_cash_out')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_promo_re_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_promo_re_cash_out] DEFAULT ((0))
ELSE
  SELECT '***** Field play_sessions.ps_promo_re_cash_out already exists *****';
GO

/* terminals.te_sas_flags */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_sas_flags')
  ALTER TABLE [dbo].[terminals]
          ADD [te_sas_flags] [int] NOT NULL CONSTRAINT [DF_terminals_te_sas_flags] DEFAULT ((0))
ELSE
  SELECT '***** Field terminals.te_sas_flags already exists *****';
GO
