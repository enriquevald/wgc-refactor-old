/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 427;

SET @New_ReleaseId = 428;

SET @New_ScriptName = N'UpdateTo_18.428.041.sql';
SET @New_Description = N'New release v03.006.0029'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/**** TABLES *****/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_found_in_egm')
   ALTER TABLE play_sessions ADD [ps_re_found_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_nr_found_in_egm')
   ALTER TABLE play_sessions ADD [ps_nr_found_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_remaining_in_egm')
   ALTER TABLE play_sessions ADD [ps_re_remaining_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_nr_remaining_in_egm')
   ALTER TABLE play_sessions ADD [ps_nr_remaining_in_egm] [money] NULL
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
   ALTER TABLE [dbo].[play_sessions]     DROP COLUMN [ps_total_cash_in]   
GO    

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
   ALTER TABLE [dbo].[play_sessions]     DROP COLUMN [ps_total_cash_out]     
GO    

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
   ALTER TABLE play_sessions ADD [ps_total_cash_in]  AS (((((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+isnull([ps_aux_ft_re_cash_in],(0)))+isnull([ps_promo_nr_ticket_in],(0)))+isnull([ps_aux_ft_nr_cash_in],(0)))+isnull([ps_re_found_in_egm],(0)))+isnull([ps_nr_found_in_egm],(0)))
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
   ALTER TABLE play_sessions ADD [ps_total_cash_out]  AS (((((isnull([ps_final_balance],(0))+[ps_cash_out])+isnull([ps_re_ticket_out],(0)))+isnull([ps_promo_nr_ticket_out],(0)))+isnull([ps_re_remaining_in_egm],(0)))+isnull([ps_nr_remaining_in_egm],(0)))
GO

/**** RECORDS *****/


/**** STORED PROCEDURES *****/

/****** Object:  StoredProcedure [dbo].[SP_LastPlaySession]    Script Date: 20-09-2017 7:03:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_LastPlaySession]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[SP_LastPlaySession]
GO


/****** Object:  StoredProcedure [dbo].[SP_LastPlaySession]    Script Date: 20-09-2017 7:03:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Andreu Julia
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_LastPlaySession] 
	@pTerminalId int, 
	@pOpened bit
AS
BEGIN
declare @_play_session_id as bigint


IF @pOpened = 1
    SELECT @_play_session_id = MAX(PS_PLAY_SESSION_ID) 
	  FROM PLAY_SESSIONS WITH (INDEX (IX_ps_status))      
	 WHERE PS_STATUS = 0 
	   AND ps_terminal_id = @pTerminalId
ELSE
    SELECT @_play_session_id = MAX(PS_PLAY_SESSION_ID) 
	  FROM PLAY_SESSIONS WITH (INDEX (IX_ps_terminal_id)) 
	 WHERE ps_terminal_id = @pTerminalId 
	   AND PS_STATUS != 0
	
SET @_play_session_id = ISNULL(@_play_session_id, 0)

SELECT   PS_PLAY_SESSION_ID
       , PS_WCP_TRANSACTION_ID  
	   , PS_TYPE  
	   , PS_STAND_ALONE  
	   , PS_INITIAL_BALANCE  
	   , PS_FINAL_BALANCE  
	   , TE_NAME  
	   , PS_ACCOUNT_ID  
	   , AC_TRACK_DATA  
	   , CASE WHEN ( PS_CASH_IN > 0 ) THEN 1 ELSE 0 END AS  HAS_CASHIN_AMOUNT 
	   , ISNULL (PS_CANCELLABLE_AMOUNT, 0)					PS_CANCELLABLE_AMOUNT  
	   , PS_INITIAL_BALANCE + PS_CASH_IN - PS_PLAYED_AMOUNT + PS_WON_AMOUNT    
  FROM   PLAY_SESSIONS  
 INNER JOIN TERMINALS  ON  PS_TERMINAL_ID = TE_TERMINAL_ID
 INNER JOIN ACCOUNTS   ON  PS_ACCOUNT_ID  = AC_ACCOUNT_ID
 WHERE  PS_PLAY_SESSION_ID = @_play_session_id 

END

GO


/****** Object:  Index [IX_ps_terminal_id]    Script Date: 20-09-2017 7:09:30 ******/
DROP INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions]
GO

/****** Object:  Index [IX_ps_terminal_id]    Script Date: 20-09-2017 7:09:30 ******/
CREATE NONCLUSTERED INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions]
(
	[ps_terminal_id] ASC,
	[ps_status] ASC,
	[ps_stand_alone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_RetryGetLogger]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[SP_RetryGetLogger]
GO

-- =============================================
-- Author:		Andreu Julia
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_RetryGetLogger] 
AS
BEGIN
	UPDATE WCP_COMMANDS
	SET CMD_STATUS = 0, CMD_STATUS_CHANGED = GETDATE()
	WHERE CMD_ID IN 
	(
		SELECT CMD_ID FROM WCP_COMMANDS WITH (INDEX (IX_WCP_CMD_CREATED))
		WHERE 
		CMD_CREATED  >= DATEADD(MINUTE, -15, GETDATE())
		AND CMD_CODE = 1 AND CMD_STATUS IN (4, 5) 
	)
END
GO



