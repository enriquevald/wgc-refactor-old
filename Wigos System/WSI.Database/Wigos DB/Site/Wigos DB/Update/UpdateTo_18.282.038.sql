/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 281;

SET @New_ReleaseId = 282;
SET @New_ScriptName = N'UpdateTo_18.282.038.sql';
SET @New_Description = N'New table: gt_sessions_conciliate; Update SP GT_Session_Information';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_sessions_conciliate]') AND type in (N'U'))
   CREATE TABLE [dbo].[gt_sessions_conciliate](
      [gtsc_session_id] [bigint] NOT NULL,
      [gtsc_type] [int] NOT NULL DEFAULT (0),
      [gtsc_data] [xml] NOT NULL,
      [gtsc_user_id] [int] NOT NULL,
      [gtsc_created] [datetime] NOT NULL DEFAULT getdate()
      CONSTRAINT [PK_gaming_tables_sessions_conciliate] PRIMARY KEY CLUSTERED 
   (
   	[gtsc_session_id] ASC
   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
   ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'IntegratedChipsSales.Split.A.CashInVoucherTitle')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'IntegratedChipsSales.Split.A.CashInVoucherTitle', '')
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'IntegratedChipsSales.Split.A.Name')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'IntegratedChipsSales.Split.A.Name', '')
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'IntegratedChipsSales.Voucher.Promotion.Text')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'IntegratedChipsSales.Voucher.Promotion.Text', '')
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'IDCardScanner' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('IDCardScanner', 'Enabled', '0')
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'Deposito.ImpuestoEnEspecie.RestarImpuestoFederal')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('PSAClient', 'Deposito.ImpuestoEnEspecie.RestarImpuestoFederal', '0')
GO

IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'IDCardScanner.01' AND gp_subject_key = 'LicenseSDK')
BEGIN
  INSERT INTO   [dbo].[general_params] 
                ([gp_group_key],[gp_subject_key],[gp_key_value])
       VALUES   ('IDCardScanner.01', 'LicenseSDK', '');
END
GO

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

-------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION	      AS   INTEGER

-------------------------------------------------------------------------------------------------------
-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201
SET @_MOVEMENT_CLOSE_SESSION        =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

-------------------------------------------------------------------------------------------------------
IF @_BASE_TYPE = 0 
BEGIN

   -- REPORT BY GAMING TABLE SESSION
 SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME          		                                                            
         , BK_NAME 
         , GTS_GAMING_TABLE_SESSION_ID  
         , dbo.GT_Calculate_DROP(GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER )
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_OPENING_DATE < @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN

   -- REPORT BY CASHIER MOVEMENTS
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT	            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT	          
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
					--WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 --AND CM_TYPE				  = @_MOVEMENT_CLOSE_SESSION
                    --THEN CM_INITIAL_BALANCE 
					WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 AND CM_TYPE				  = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT	                          
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE                    
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_TIPS                       
         , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                  CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                      CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                           WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                           WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                           ELSE 0 END 
                  ELSE 0 END
                WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                           CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                                WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                           ELSE 0 END
                ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME          		                                                              
         , BK_NAME 
         , MAX(CM_GAMING_TABLE_SESSION_ID)
         , dbo.GT_Calculate_DROP(GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_TOTAL_SALES_AMOUNT, GT_HAS_INTEGRATED_CASHIER)
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN	CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN	GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN	GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN	GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN	GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN	AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN	BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN	CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
   AND    CM_SESSION_ID = CS_SESSION_ID 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME
      , GTS_OWN_SALES_AMOUNT
      , GTS_EXTERNAL_SALES_AMOUNT
      , GTS_TOTAL_SALES_AMOUNT        		                                                              
END

END -- END PROCEDURE
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsValidRegistrationCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsValidRegistrationCode]
GO

CREATE FUNCTION IsValidRegistrationCode 
(
	-- Add the parameters for the function here
	@pTerminalId       INT
  , @pStatus           INT
  , @pRegistrationCode NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	
	DECLARE @_value AS INT;
	DECLARE @_count AS INT;
	
	IF ( @pStatus <> 0 )					  RETURN 1; -- Check only 'Active Terminals'
	IF ( @pRegistrationCode IS NULL )		  RETURN 1; -- Allow NULL
	IF ( ISNUMERIC (@pRegistrationCode) = 0 ) RETURN 0; -- Don't allow NonNumericValues
	
	-- Convert to Integer
	SET @_value = CAST (@pRegistrationCode AS INT);
	
	-- Check range value
	IF (@_value <= 0 OR @_value > 99999999)   RETURN 0; -- Out of range
	
	
	SELECT   @_count = COUNT(1) 
	  FROM   TERMINALS 
	 WHERE   TE_TERMINAL_ID <> @pTerminalId
       AND   TE_STATUS                          = 0
	   AND   ISNUMERIC (TE_REGISTRATION_CODE)   = 1
       AND   CAST (TE_REGISTRATION_CODE AS INT) = @_value;
	   
	IF ( @_count > 0 ) RETURN 0; -- Duplicated ID   
	
	RETURN 1;

END
GO


IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TriggerValidateRegistrationCode]'))
DROP TRIGGER [dbo].[TriggerValidateRegistrationCode]
GO

CREATE TRIGGER dbo.TriggerValidateRegistrationCode 
   ON  dbo.TERMINALS 
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @_wxp_enabled       as INT
	DECLARE @_terminal_id       as INT
	DECLARE @_status            as INT
	DECLARE @_registration_code as NVARCHAR(50)
	DECLARE @_raise             as BIT
	DECLARE @_err_msg           as NVARCHAR(MAX)
	
	SET NOCOUNT ON;
	
	IF ( NOT UPDATE (TE_STATUS) AND NOT UPDATE (TE_REGISTRATION_CODE) ) RETURN;

	SELECT @_wxp_enabled = CAST (GP_KEY_VALUE AS INT)
	  FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY    = 'ExternalProtocol'
	   AND GP_SUBJECT_KEY  = 'Enabled'
	   AND ISNUMERIC (GP_KEY_VALUE) = 1
	   
	IF ( @_wxp_enabled <> 1 ) RETURN;   

    SET @_raise = 0;
    
    DECLARE _terminals CURSOR FOR SELECT TE_TERMINAL_ID, TE_STATUS, TE_REGISTRATION_CODE  FROM INSERTED
    OPEN _terminals

	WHILE (1=1)
	BEGIN
      FETCH NEXT FROM _terminals INTO @_terminal_id, @_status, @_registration_code
	  IF ( @@Fetch_Status <> 0 ) BREAK;


	  IF dbo.IsValidRegistrationCode(@_terminal_id, @_status, @_registration_code) = 0
      BEGIN
		SET @_raise  = 1;
		SET @_err_msg = 'Registration Code Not Valid, T:' + CAST(@_terminal_id as NVARCHAR) + ', NewCode:' + @_registration_code;
		BREAK;
      END
    END
    
	CLOSE _terminals
	DEALLOCATE _terminals

	IF @_raise = 1 
	BEGIN
		RAISERROR(@_err_msg, 25, 0) WITH LOG;
	END
	
    RETURN

END
GO

