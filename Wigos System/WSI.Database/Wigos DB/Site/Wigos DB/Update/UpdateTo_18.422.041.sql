/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 421;

SET @New_ReleaseId = 422;

SET @New_ScriptName = N'UpdateTo_18.422.041.sql';
SET @New_Description = N'New release v03.006.0023'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Ticket.ManualConciliation')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Ticket.ManualConciliation', '0')

/******* TABLES  *******/


/******* RECORDS  *******/


/******* STORED PROCEDURES  *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
 )
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols             AS NVARCHAR(MAX)
  DECLARE @query            AS NVARCHAR(MAX)
  DECLARE @NationalCurrency AS NVARCHAR(3)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyIsoCode')
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE AND GTSC_ISO_CODE = @NATIONALCURRENCY
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo         
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)                                            

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(CASE WHEN GTPM_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(GTPM_VALUE, GTPM_ISO_CODE, @NationalCurrency) ELSE  GTPM_VALUE END)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)                                                                                           
                                              
      --  SET WIN/LOSS WITH CURSORS      
      DECLARE @GT_SESSION_ID BIGINT
      DECLARE GT_SESSION_WIN_LOSS_CURSOR CURSOR GLOBAL
      FOR SELECT GAMING_TABLE_SESSION_ID
            FROM #VALUE_TABLE 
        GROUP BY GAMING_TABLE_SESSION_ID
                                                            
      OPEN GT_SESSION_WIN_LOSS_CURSOR
      FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      
      -- FIRST CURSOR: ITERANCE THROUGH ALL SESSIONS OF #VALUE_TABLE
      WHILE(@@FETCH_STATUS = 0)
        BEGIN        
        
          DECLARE @WIN_LOSS_HOUR INT
          DECLARE @WIN_LOSS_LAST_AMOUNT MONEY
          DECLARE @WIN_LOSS_CURRENT_AMOUNT MONEY
          DECLARE @WIN_LOSS_FINAL_AMOUNT MONEY
          DECLARE @WIN_LOSS_CLOSING_HOUR AS INT
          DECLARE @WIN_LOSS_CLOSING_TABLE_AMOUNT MONEY
          DECLARE @CREDITS_SUB_FILLS MONEY          
          DECLARE HOUR_WIN_LOSS_CURSOR CURSOR GLOBAL
          FOR SELECT HORA
                FROM #VALUE_TABLE
               WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID              
                                                            
          OPEN HOUR_WIN_LOSS_CURSOR
          FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR     
                                      
          -- WE HAVE THE HOUR THE TABLE WAS CLOSED                                                            
          SET @WIN_LOSS_CLOSING_HOUR = (SELECT DATEPART(HOUR, CLOSING_DATE)
                                          FROM #VALUE_TABLE
                                         WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                      GROUP BY CLOSING_DATE)  
                                      
          SET @WIN_LOSS_FINAL_AMOUNT = 0
          SET @WIN_LOSS_CLOSING_TABLE_AMOUNT = (SELECT FINAL_BALANCE
                                                  FROM #VALUE_TABLE
                                                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                              GROUP BY FINAL_BALANCE)                 
                                                          
          -- SECOND CURSOR: ITERANCE THE 24 HOURS OF THE PREVIOUS CURSOR SESSION
          WHILE(@@FETCH_STATUS = 0)
            BEGIN
              
              IF @WIN_LOSS_HOUR <> 9999
              BEGIN
                -- GET CURRENT WIN LOSS AMOUNT BY THE CURRENT TIME ITERANCE              
                SET @WIN_LOSS_CURRENT_AMOUNT = (SELECT ISNULL(GTWL_WIN_LOSS_AMOUNT, 0)
                                                  FROM GAMING_TABLES_WIN_LOSS
                                                 WHERE GTWL_GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND GTWL_DATETIME_HOUR = DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom))
                
                SET @CREDITS_SUB_FILLS = (SELECT SUM(CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_SUB_AMOUNT END -
                                                     CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_ADD_AMOUNT END)
                                            FROM CASHIER_MOVEMENTS
                                           WHERE CM_DATE BETWEEN DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom) AND DATEADD(HOUR, @WIN_LOSS_HOUR + 1, @pDateFrom)
                                             AND CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                                             AND CM_CAGE_CURRENCY_TYPE = @pChipRE
                                             AND CM_SESSION_ID = (SELECT CASHIER_SESSION_ID
                                                                    FROM #VALUE_TABLE
                                                                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                                                GROUP BY CASHIER_SESSION_ID))                                           
                
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0)
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = @WIN_LOSS_HOUR                              
                
                -- IF THE ITERATED TIME IS EQUAL TO THE CLOSURE OF THE GAMBLING TABLE WE SET THE VALUE OF WIN LOSS
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) = @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  SET @WIN_LOSS_FINAL_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + ISNULL(@WIN_LOSS_CLOSING_TABLE_AMOUNT, 0)
                END
                
                -- UPDATE WIN LOSS LAST AMOUNT WITH CURRENT WIN LOSS AMOUNT
                SET @WIN_LOSS_LAST_AMOUNT = @WIN_LOSS_CURRENT_AMOUNT
              END
              ELSE
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = @WIN_LOSS_FINAL_AMOUNT
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = 9999
                           
              FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR                         
            
            END       
            CLOSE HOUR_WIN_LOSS_CURSOR
            DEALLOCATE HOUR_WIN_LOSS_CURSOR
  
        FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      END
      CLOSE GT_SESSION_WIN_LOSS_CURSOR
      DEALLOCATE GT_SESSION_WIN_LOSS_CURSOR         

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_WIN_LOSS / A.GT_DROP * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_DROP <> 0
 
      --  SET OCCUPATION BY HOUR 
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999

      --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO




