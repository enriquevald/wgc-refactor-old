/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 315;

SET @New_ReleaseId = 316;
SET @New_ScriptName = N'UpdateTo_18.316.039.sql';
SET @New_Description = N'SP GetAccountPointsCache;';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache]
       @pAccountId    bigint
AS
BEGIN
  
  EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  DECLARE @BucketNR Int 
  DECLARE @BucketRE Int
    
  SET @BucketPuntosCanje = 1
  SET @BucketNR = 3
  SET @BucketRE = 4
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , ISNULL (DBO.GETBUCKETVALUE(@BucketPuntosCanje, @pAccountId), 0)  AS AC_POINTS
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
         , ISNULL(APC_HISTORY_POINTS_GENERATED_FOR_LEVEL         , 0) + ISNULL(APC_TODAY_POINTS_GENERATED_FOR_LEVEL         , 0)  AS AM_POINTS_GENERATED 
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0)  AS AM_POINTS_DISCRETIONARIES
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0)  AS AM_POINTS_PROMO_DISCRETIONARIES 
         , ISNULL(APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0) + ISNULL(APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0)  AS AM_POINTS_PROMO_ONLY_FOR_REDEEM
         , APC_TODAY_LAST_MOVEMENT_ID
         , ISNULL (DBO.GETBUCKETVALUE(@BucketNR, @pAccountId), 0)  AS BU_NR_BALANCE
         , ISNULL (DBO.GETBUCKETVALUE(@BucketRE, @pAccountId), 0)  AS BU_RE_BALANCE		 
    FROM   ACCOUNTS 
   INNER   JOIN ACCOUNT_POINTS_CACHE ON APC_ACCOUNT_ID = AC_ACCOUNT_ID
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
  
END  -- PROCEDURE [dbo].[GetAccountPointsCache]
GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO
--EXEC	[dbo].[GetCashierTransactionsTaxes_Prueba3]
--		@pStartDatetime = N'2015-09-01 00:00:00.000',
--		@pEndDatetime = N'2016-01-01 00:00:00.000',
--		@pTerminalId = 13,
--		@pCmMovements = N'103, 304',
--		@pHpMovements = N'0,1,2,30, 1000, 1001, 11001, 11002, 1010',
--		@pPaymentUserId = 16,
--		@pAuthorizeUserId = null,  --18,

/*

----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CASHIER TRANSACTIONS TAXES

Version           Date                    User                    Description
----------------------------------------------------------------------------------------------------------------
1.0.0             22-OCT-2015             CPC                     New procedure
1.0.1             24-DEC-2015             JML                     New version
1.0.2             12-APR-2016             EOR					            Add Handpays Pending                     

Requirements:
   -- Functions:
         
Parameters:
   -- START DATE:            Start date for data collection. (If NULL then use first available date).
   -- END DATE:              End date for data collection. (If NULL then use current date).
   -- TERMINAL ID:           The terminal id for data collection (If NULL then all terminals)
   -- CASHIER MOVEMENTS:     Cashier movements in format '1,2,3,4'.  CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) 
   --                         TAX_ON_PRIZE1 = 6,
   --                         TAX_ON_PRIZE2 = 14,
   --                                                                                      
   --                         TITO_TICKET_CASHIER_PAID_CASHABLE = 103,
   --                         TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE = 124,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE = 125,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE = 126,
   --                                                                                      
   --                         CHIPS_PURCHASE_TOTAL = 304,
   --
   --                         HANDPAY = 30,
   --                         MANUAL_HANDPAY = 32,
   --
   -- HANDPAYS MOVEMENTS:    Handpays movements in format '1,2,3,4'. 
   -- PAYMENT USER ID:       The payment user id for data collection (If NULL then all payment users)
   -- AUTHORIZE USER ID:     The authorize user id for data collection (If NULL then all authorize users)
   
*/  
CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
  DECLARE @SelectPartToExecute INT -- 1:HP,  2:CM,  3:Both
      
  SET @_PAID = 32768
  
  SET @SelectPartToExecute = 3 

  IF @pHpMovements IS NOT NULL AND @pCmMovements IS NULL
  BEGIN
     SET @SelectPartToExecute = 2
  END
  ELSE IF @pHpMovements IS NULL AND @pCmMovements IS NOT NULL
  BEGIN
     SET @SelectPartToExecute = 1
  END
     
  
  SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 WHEN  38 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32, 38) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
' + CHAR(10) 

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 2
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , AM_TYPE                   AS TYPE2
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   ACCOUNT_MOVEMENTS WITH(INDEX( IX_am_operation_id)) ON AM_OPERATION_ID = AO_OPERATION_ID
INNER JOIN   HANDPAYS          WITH(INDEX( IX_HP_MOVEMENT_ID))       ON HP_MOVEMENT_ID  = AM_MOVEMENT_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
--------      + CASE WHEN @pWithTaxes = 1 THEN ' AND HP_TAX_AMOUNT > 0' ELSE ' AND HP_TAX_AMOUNT = 0' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + CHAR(10) 
END


IF @SelectPartToExecute = 3
BEGIN
  SET @_SQL = @_SQL + ' UNION ' + CHAR(10)
END

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 1
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS BEFORE_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE2
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
----     + CASE WHEN @pWithTaxes = 1 THEN ' AND CM_ADD_AMOUNT > 0' ELSE ' AND CM_ADD_AMOUNT = 0' END + CHAR(10) 
END
 
SET @_SQL = @_SQL + ' ORDER   BY DATETIME DESC ' + CHAR(10) 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL  
   
END  

GO
GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO


--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-- 26-JAN-2015 FJC    Fixed Bug WIG-1966: change filter dates for provisions.
-- 29-JAN-2015 FJC    Fixed Bug WIG-1981 
-- 02-FEB-2015 FJC    Fixed bug: WIG-1994 - Annulled provisions are showed in the report.
-- 03-FEB-2015 FJC    Fixed bug: WIG-1998 & 2004 - Changes in handpays placement 
--                      Column Manual:                   Others                     (MANUAL & MANUAL_OTHERS)
--                      Column Cancelled Credits:        Cr�ditos cancelados        
--                      Column No-Progressive Jackpots:  Jackpot + Jackpot Top Win  
--                      Column Progressive Jackpots:     Progressive Jackpots
-- 09-FEB-2015 JML & FJC Fixed bug: WIG-2012: Site Jackpot is not displayed in some cases
-- 10-FEB-2015 FJC    WIG-2003. 
-- 09-MAR-2015 FJC    Fixed bug: WIG-2144
-- 30-MAR-2015 FJC    BacklogItem 542: BV-75: Reporte JCJ � Cambio en Net TITO
-- 28-MAY-2015 FAV    BacklogItem 1722: @pShowMetters and @pOnlyGroupByTerminal parameters added. 
--                    For the 'Netwin control report' form.
-- 14-JUL-2015 FAV    Fixed Bug WIG-2516 and 2536, check and rebuild 'PR_collection_by_machine_and_date' stored procedure
-- 21-SEP-2015 JML    Add cash-in coins
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
  ,@pTypeReport			BIT	= 0
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN (0, 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS JACKPOT_DE_SALA  '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
  SET @Sql =  @Sql +
  '                 , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN (0, 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN (0, 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) '
IF @pTypeReport = 1 
BEGIN
	SET @Sql =  @Sql + ' AND HP_TYPE <> 20 '
END                  
               
             SET @Sql =  @Sql + ' GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN (0, 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 1
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO
 
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportShortfallExcess.sql
-- 
--   DESCRIPTION: Procedure for Cash ShortFall and Excess Report 
-- 
--        AUTHOR: Fernando Jim�nez
-- 
-- CREATION DATE: 04-OCT-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-OCT-2014 FJC    First release.
-- 21-NOV-2014 FJC    Changes in Mobile bank query.
-- 24-NOV-2014 FJC    Changes in Mobile bank query.
-- 16-JUN-2015 AMF    Search by gaming day
-- 06-AUG-2015 JML    Change short/over movements 
-- 14-APR-2016 ETP    Fixed Bug 11864: Mobile banks: Filter by working day. 
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash shortfall and excess report.
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom			 DATETIME     
--   @pDateTo				 DATETIME      
--   @pGamingDayMode BIT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
	@pGamingDayMode BIT
    
AS 
BEGIN
    --GUI USERS
		SELECT   GUI_USERS.GU_USER_ID        as  USERID 
					 , 0                           as  TYPE_USER
					 , GUI_USERS.GU_USERNAME       as  USERNAME 
					 , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
					 , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
					 , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
					 , X.FALTANTE                  as  FALTANTE 
					 , X.SOBRANTE                  as  SOBRANTE 
					 , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (        
      SELECT CS_USER_ID
             , CM_CURRENCY_ISO_CODE
             , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
             , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE             
        FROM CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))
				INNER JOIN CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID				
       WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END >= @pDateFrom  
         AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END < @pDateTo
         AND CM_TYPE IN (160,  -- CASH_CLOSING_SHORT
                         161)  -- CASH_CLOSING_OVER
			GROUP BY CS_USER_ID
							 , CM_CURRENCY_ISO_CODE
    ) X
         
  INNER JOIN GUI_USERS
          ON GU_USER_ID = X.CS_USER_ID
         AND (X.FALTANTE<>0 OR X.SOBRANTE<>0)
         AND GU_USER_TYPE=0

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE)) 
      INNER JOIN   cashier_sessions 
              ON   MB1.mbm_cashier_session_id = cs_session_id          
          
           WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END >= @pDateFrom   
             AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END < @pDateTo   
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB2.MBM_DATETIME END < @pDateTo 
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
            AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END

GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
