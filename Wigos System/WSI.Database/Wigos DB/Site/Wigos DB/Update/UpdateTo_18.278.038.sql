/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 277;

SET @New_ReleaseId = 278;
SET @New_ScriptName = N'UpdateTo_18.278.038.sql';
SET @New_Description = N'Update SP CageUpdateMeter';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/
DECLARE @_source_target_id BIGINT
DECLARE @_source_target_name NVARCHAR(50)

DECLARE   Curs_SourceTargetID CURSOR FOR 
 SELECT   CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME
   FROM   CAGE_SOURCE_TARGET 
  WHERE   CST_SOURCE_TARGET_ID NOT IN (SELECT CSTC_SOURCE_TARGET_ID FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 0)
    AND   CST_SOURCE_TARGET_ID <> 0

OPEN Curs_SourceTargetID
FETCH NEXT FROM Curs_SourceTargetID INTO @_source_target_id, @_source_target_name
  
WHILE @@FETCH_STATUS = 0
BEGIN

  IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 0 AND CSTC_SOURCE_TARGET_ID = @_source_target_id)
  BEGIN
    SELECT 'Creando concepto GLOBAL para Origen/Destino = ( ' + CAST(@_source_target_id AS NVARCHAR) + ', ' + @_source_target_name + ').'
    INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID, CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY, CSTC_ENABLED )
         VALUES                             ( 0,               @_source_target_id   , 2        , 0                          , 1 )

    EXEC CageCreateMeters @_source_target_id, 0, NULL, 3 -- Create in cage_meters and all opened cage sessions
  END
  
  FETCH NEXT FROM Curs_SourceTargetID INTO @_source_target_id, @_source_target_name
END

CLOSE Curs_SourceTargetID
DEALLOCATE Curs_SourceTargetID

GO

ALTER PROCEDURE [dbo].[CageUpdateMeter]
		@pValueIn MONEY
      , @pValueOut MONEY
	  ,	@pSourceTagetId BIGINT
	  , @pConceptId BIGINT
	  , @pIsoCode VARCHAR(3)
	  , @pOperation INT
	  , @pSessionId BIGINT = NULL
	  , @pSessionGetMode INT
      
  AS
BEGIN 

	-- @pOperationId  =	0 - Set
	--				  =	1 - Update
	
	-- , @pCageSessionGetMode = 0 - Get cage session from parameters
	--						  = 1 - Get cage_sessions


	DECLARE @CageSessionId BIGINT
	DECLARE @GetFromMode INT
	DECLARE @out_CageMetersValue TABLE(CM_VALUE DECIMAL)
	DECLARE @out_CageSessionMetersValue TABLE (CSM_VALUE DECIMAL)

	-- Get Cage Session
	
	IF @pSessionGetMode = 0
		SET @CageSessionId = @pSessionId

	ELSE IF @pSessionGetMode = 1
	BEGIN
	
		-------------- TODO : GET FROM GP!! -------------- 
		--SELECT @GetFromMode = GP_KEY_VALUE FROM GENERAL_PARAMS 
		-- WHERE GP_GROUP_KEY = 'RegionalOptions' 
		--   AND GP_SUBJECT_KEY = 'CurrencyISOCode'
		SET @GetFromMode = 1
     
		IF @GetFromMode = 1
			SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
									FROM CAGE_SESSIONS
								ORDER BY cgs_cage_session_id DESC)
		ELSE
			SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
									FROM CAGE_SESSIONS
								ORDER BY cgs_cage_session_id ASC)
	END				   

  -- Create Global and CageSession Meters. It checks if they don't exist and create them if necessary
  -- RCI 17-JUN-2015: Fixed Bug WIG-2443: Create them before update
	EXEC CageCreateMeters @pSourceTagetId
	                    , @pConceptId
	                    , @CageSessionId  -- @pCageSessionId
	                    , 0     -- Create in cage_meters and cage_session_meters
	                    , NULL  --@CurrencyForced

	-- CAGE METERS --

	UPDATE CAGE_METERS SET                                                                  
 	       CM_VALUE_IN = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_IN, 0) + @pValueIn    
                              ELSE CM_VALUE_IN END
         , CM_VALUE_OUT = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_OUT, 0) + @pValueOut  
                               ELSE CM_VALUE_OUT END                                                                       
         , CM_VALUE = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE, 0) + @pValueIn - @pValueOut            
                           WHEN @pOperation = 0 THEN @pValueIn - @pValueOut                                
                           ELSE CM_VALUE END 
    OUTPUT  INSERTED.CM_VALUE INTO @out_CageMetersValue
 	 WHERE CM_SOURCE_TARGET_ID = @pSourceTagetId
 	   AND CM_CONCEPT_ID = @pConceptId                                                     
 	   AND CM_ISO_CODE = @pIsoCode                                                         

 	   
	-- CAGE SESSION METERS --

	IF @CageSessionId IS NOT NULL
	BEGIN
	 	UPDATE CAGE_SESSION_METERS SET                                                          
	 	       CSM_VALUE_IN = ISNULL(CSM_VALUE_IN, 0) + @pValueIn 
	 	     , CSM_VALUE_OUT = ISNULL(CSM_VALUE_OUT, 0) + @pValueOut                                  
	         , CSM_VALUE = ISNULL(CSM_VALUE, 0) + @pValueIn - @pValueOut                                      
	 	OUTPUT INSERTED.CSM_VALUE INTO @out_CageSessionMetersValue                                                            
	 	 WHERE CSM_SOURCE_TARGET_ID = @pSourceTagetId
	 	   AND CSM_CONCEPT_ID = @pConceptId                                                                                              
	 	   AND CSM_ISO_CODE = @pIsoCode                                                        
	 	   AND CSM_CAGE_SESSION_ID = @CageSessionId 
	END                                                        
 
	SELECT  ISNULL((SELECT TOP 1 CM_VALUE FROM @out_CageMetersValue),0) as CM_VALUE
		  , ISNULL((SELECT TOP 1 CSM_VALUE FROM @out_CageSessionMetersValue),0) as CSM_VALUE
		  
 END  -- CageUpdateMeter
                                                                                       
 GO

 ALTER PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
 
  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
     -- Discretional
  SET @manually_added_points_for_level       = 68
  SET @imported_points_for_level             = 72
  SET @imported_points_history               = 73
  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  SET @imported_points_only_for_redeem       = 71
     -- Promotion 
  SET @promotion_point                       = 60
  SET @cancel_promotion_point                = 61
  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
                   @ParamDefinition,
                   @AccountId                             = @AccountId, 
                   @points_awarded                        = @points_awarded,                        
                   @manually_added_points_for_level       = @manually_added_points_for_level,
                   @imported_points_for_level             = @imported_points_for_level,
                   @imported_points_history               = @imported_points_history, 
                   @manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
                   @imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
                   @promotion_point                       = @promotion_point,
                   @cancel_promotion_point                = @cancel_promotion_point,
                   @LastMovementId_out                    = @LastMovementId                   OUTPUT, 
                   @PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
                   @PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
                   @PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
                   @PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

ALTER PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  
