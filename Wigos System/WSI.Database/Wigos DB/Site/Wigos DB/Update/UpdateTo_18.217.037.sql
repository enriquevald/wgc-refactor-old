/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 216;

SET @New_ReleaseId = 217;
SET @New_ScriptName = N'UpdateTo_18.217.037.sql';
SET @New_Description = N'Update alarms catalog;';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.cage_movement_template') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cage_movement_template](
      [cmt_id] [bigint] IDENTITY(1,1) NOT NULL,
      [cmt_name] [nvarchar](50) NOT NULL,
      [cmt_denominations] [xml] NOT NULL,
      [cmt_creation] [datetime] NOT NULL,
      [cmt_last_used] [datetime] NULL,
CONSTRAINT [PK_cage_movement_template] PRIMARY KEY CLUSTERED 
(
      [cmt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

CREATE TABLE dbo.Tmp_cage_amounts
	(
	caa_iso_code nvarchar(3) NOT NULL,
	caa_denomination money NOT NULL,
	caa_allowed bit NOT NULL,
	caa_color int NULL
	)  ON [PRIMARY]
GO

IF EXISTS(SELECT * FROM dbo.cage_amounts)
	 EXEC('INSERT INTO dbo.Tmp_cage_amounts (caa_iso_code, caa_denomination, caa_allowed, caa_color)
		SELECT caa_iso_code, CONVERT(money, caa_denomination), caa_allowed, caa_color FROM dbo.cage_amounts WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.cage_amounts
GO
EXECUTE sp_rename N'dbo.Tmp_cage_amounts', N'cage_amounts', 'OBJECT' 
GO
ALTER TABLE dbo.cage_amounts ADD CONSTRAINT
	PK_CAGE_AMOUNTS PRIMARY KEY CLUSTERED 
	(
	caa_iso_code,
	caa_denomination
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE dbo.cage_stock
	DROP CONSTRAINT DF_cage_stock_cgs_quantity
GO
CREATE TABLE dbo.Tmp_cage_stock
	(
	cgs_iso_code nvarchar(3) NOT NULL,
	cgs_denomination money NOT NULL,
	cgs_quantity money NOT NULL
	)  ON [PRIMARY]
GO

ALTER TABLE dbo.Tmp_cage_stock ADD CONSTRAINT
	DF_cage_stock_cgs_quantity DEFAULT ((0)) FOR cgs_quantity
GO
IF EXISTS(SELECT * FROM dbo.cage_stock)
	 EXEC('INSERT INTO dbo.Tmp_cage_stock (cgs_iso_code, cgs_denomination, cgs_quantity)
		SELECT cgs_iso_code, CONVERT(money, cgs_denomination), cgs_quantity FROM dbo.cage_stock WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.cage_stock
GO
EXECUTE sp_rename N'dbo.Tmp_cage_stock', N'cage_stock', 'OBJECT' 
GO
ALTER TABLE dbo.cage_stock ADD CONSTRAINT
	PK_CAGE_STOCK PRIMARY KEY CLUSTERED 
	(
	cgs_iso_code,
	cgs_denomination
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

/******* INDEXES *******/

/******* RECORDS *******/

DELETE FROM ALARM_CATEGORIES WHERE ALC_CATEGORY_ID=37  
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=99997
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=99998
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=1048584
GO

INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE,ALCC_CATEGORY,ALCC_TYPE) VALUES (589827,38,0)
INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE,ALCC_CATEGORY,ALCC_TYPE) VALUES (589829,38,0)
GO

UPDATE ALARM_GROUPS SET ALG_TYPE=0
UPDATE ALARM_GROUPS SET ALG_NAME='M�quina' WHERE ALG_ALARM_GROUP_ID =1
GO

INSERT INTO   [dbo].[ALARM_CATEGORIES] 
            ( [ALC_CATEGORY_ID], [ALC_ALARM_GROUP_ID], [ALC_TYPE], [ALC_NAME], [ALC_VISIBLE] )
     VALUES ( 45, 1, 0, N'T�cnico', 1 )
     
INSERT INTO   [dbo].[ALARM_CATALOG] 
            ( [ALCG_ALARM_CODE], [ALCG_TYPE], [ALCG_NAME], [ALCG_DESCRIPTION], [ALCG_VISIBLE] )
     VALUES ( 393254, 0, N'Insertada tarjeta de t�cnico', N'Insertada tarjeta de t�cnico', 1 )

INSERT INTO   [dbo].[ALARM_CATALOG] 
            ( [ALCG_ALARM_CODE], [ALCG_TYPE], [ALCG_NAME], [ALCG_DESCRIPTION], [ALCG_VISIBLE] )
     VALUES ( 393255, 0, N'Retirada tarjeta de t�cnico', N'Retirada tarjeta de t�cnico', 1 )
     
INSERT INTO   [dbo].[ALARM_CATALOG_PER_CATEGORY] 
            ( [ALCC_ALARM_CODE], [ALCC_CATEGORY], [ALCC_TYPE], [ALCC_DATETIME] ) 
     VALUES ( 393254, 45, 0, GETDATE() )
     
INSERT INTO   [dbo].[ALARM_CATALOG_PER_CATEGORY] 
            ( [ALCC_ALARM_CODE], [ALCC_CATEGORY], [ALCC_TYPE], [ALCC_DATETIME] ) 
     VALUES ( 393255, 45, 0, GETDATE() )
GO	 

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'MaxNumberTemplates')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'MaxNumberTemplates', '20')

/******* STORED PROCEDURES *******/

IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;                 
GO 

CREATE PROCEDURE [dbo].[GT_Base_Report_Data] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP) TOTAL_DROP         
         , SUM(X.S_WIN)  TOTAL_WIN         
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP         
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP         
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP) / SUM(X.S_DROP) * 100 END AS TIP_DROP         
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
           -- CORE QUERY
                             SELECT    CASE 
                      WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                      END AS CM_DATE_ONLY 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)                        AS TABLE_IDENTIFIER  -- GET THE BASE TYPE IDENTIFIER
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                                                                             AS TABLE_NAME                  -- GET THE BASE TYPE IDENTIFIER NAME
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                                                        AS TABLE_TYPE                -- TYPE 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                                                                            AS TABLE_TYPE_NAME      -- TYPE NAME
                                                     , DBO.GT_CALCULATE_DROP(  ISNULL(GTS_OWN_SALES_AMOUNT, 0)       , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))     AS S_DROP
                                                     , DBO.GT_CALCULATE_WIN (  ISNULL(GTS.GTS_FINAL_CHIPS_AMOUNT  , 0)
                                                                                                   , ISNULL(GTS.GTS_INITIAL_CHIPS_AMOUNT, 0)
                                                                                                   , ISNULL(GTS.GTS_COLLECTED_AMOUNT    , 0)
                                                                                                   , ISNULL(GTS_TIPS, 0))                                                                                                                                                                 AS S_WIN
                                                     , ISNULL(GTS_TIPS, 0)                                                                                                                                                                                                  AS S_TIP
                                                     , CS.CS_OPENING_DATE                                                                                                                                                                 AS OPEN_HOUR
                                                     , CS.CS_CLOSING_DATE                                                                                                                                                                 AS CLOSE_HOUR
                                                     , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                                                             AS SESSION_HOURS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID 
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID 
                                         AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
                                   WHERE   CS_STATUS = 1   -- Only closed sessions      
          -- END CORE QUERY
          
        ) AS X          
 
 GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP, 
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT
        
        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER 
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP, 
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME 

      FROM @_DAYS_AND_TABLES DT
        
       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       
       -- SET ORDER             
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO