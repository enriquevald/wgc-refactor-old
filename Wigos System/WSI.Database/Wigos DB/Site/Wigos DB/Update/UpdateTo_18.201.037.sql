/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 200;

SET @New_ReleaseId = 201;
SET @New_ScriptName = N'UpdateTo_18.201.037.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[currency_exchange_audit]') and name = 'cea_old_num_decimals')
  ALTER TABLE dbo.currency_exchange_audit ADD
      cea_old_num_decimals int NOT NULL CONSTRAINT DF_currency_exchange_audit_cea_old_num_decimals DEFAULT 2
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[currency_exchange_audit]') and name = 'cea_new_num_decimals')
  ALTER TABLE dbo.currency_exchange_audit ADD
      cea_new_num_decimals int NOT NULL CONSTRAINT DF_currency_exchange_audit_cea_new_num_decimals DEFAULT 2
GO

/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[currency_exchange_audit]') AND name = N'IX_cea_iso_code_type_datetime')
  CREATE NONCLUSTERED INDEX [IX_cea_iso_code_type_datetime] ON [dbo].[currency_exchange_audit] 
  (
        [cea_currency_iso_code] ASC,
        [cea_type] ASC,
        [cea_datetime] ASC
  )
GO

/******* STORED PROCEDURES *******/

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;                 
GO 

CREATE FUNCTION ApplyExchange
 (@pAmount  MONEY,
  @pIsoCode VARCHAR(3),
  @pMovementDate DATETIME)
RETURNS MONEY
AS
BEGIN

      DECLARE @Exchanged      MONEY
      DECLARE @Change         MONEY
      DECLARE @NumDecimals INT
      
      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
          SELECT TOP 1
                  @Change = CEA_NEW_CHANGE,
                  @NumDecimals = CEA_NEW_NUM_DECIMALS
            FROM  CURRENCY_EXCHANGE_AUDIT WITH (INDEX(IX_CEA_ISO_CODE_TYPE_DATETIME))
           WHERE  CEA_TYPE = 0
             AND  CEA_CURRENCY_ISO_CODE = @pIsoCode
             AND  CEA_DATETIME <= @pMovementDate
           ORDER  BY CEA_DATETIME DESC

            SET @NumDecimals = ISNULL(@NumDecimals, 0)     
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   
      END
      
      RETURN @Exchanged
END -- ApplyExchange
GO
GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;                 
GO  

CREATE PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION	      AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201
SET @_MOVEMENT_CLOSE_SESSION        =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0 
BEGIN
   -- REPORT BY GAMING TABLE SESSION
 SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME          		                                                            
         , BK_NAME          		                                                            
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_OPENING_DATE < @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN
   -- REPORT BY CASHIER MOVEMENTS

                      
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT	            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT	          
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
					--WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 --AND CM_TYPE				  = @_MOVEMENT_CLOSE_SESSION
                    --THEN CM_INITIAL_BALANCE 
					WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 AND CM_TYPE				  = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT	                          
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE                    
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_TIPS                       
         , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                  CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                      CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * DBO.ApplyExchange(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, CM_DATE) 
                           WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      DBO.ApplyExchange(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, CM_DATE) 
                           WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      DBO.ApplyExchange(CM_INITIAL_BALANCE, CM_CURRENCY_ISO_CODE, CM_DATE) 
                           ELSE 0 END 
                  ELSE 0 END
                WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                           CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                                WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                           ELSE 0 END
                ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME          		                                                              
         , BK_NAME          		                                                              
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN	CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN	GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN	GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN	GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN	GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN	AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN	BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN	CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
   AND    CM_SESSION_ID = CS_SESSION_ID 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME
  
END

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO

/******* RECORDS *******/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'Cashier.AllowAnonymousCardChange')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('ExternalLoyaltyProgram.Mode01', 'Cashier.AllowAnonymousCardChange', '0')
GO
