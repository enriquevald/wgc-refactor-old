/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 245;

SET @New_ReleaseId = 246;
SET @New_ScriptName = N'UpdateTo_18.246.037.sql';
SET @New_Description = N'SP modified ReportShortfallExcess.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF  NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collection_meters]') and name = 'mcm_total_coin_amount')
BEGIN
      ALTER TABLE dbo.money_collection_meters ADD
            mcm_total_coin_amount MONEY NULL  DEFAULT (0)
END
GO

/******* INDEXES *******/

/******* RECORDS *******/

/******* STORED PROCEDURES *******/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME
    
AS 
BEGIN
    --GUI USERS
    SELECT   GUI_USERS.GU_USER_ID        as  USERID 
           , 0                           as  TYPE_USER
           , GUI_USERS.GU_USERNAME       as  USERNAME 
           , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
           , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
           , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
           , X.FALTANTE                  as  FALTANTE 
           , X.SOBRANTE                  as  SOBRANTE 
           , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (
        SELECT CM_USER_ID
               , CM_CURRENCY_ISO_CODE
               , SUM (CASE WHEN CM_TYPE = 158 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
               , SUM (CASE WHEN CM_TYPE = 159 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE
          FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))
         WHERE   CM_DATE    >= @pDateFrom  
           AND   CM_DATE    <= @pDateTo
           AND   CM_TYPE    IN (158, 159) 
      GROUP BY   CM_USER_ID
               , CM_CURRENCY_ISO_CODE
      ) X
           
    INNER JOIN   GUI_USERS
            ON   GU_USER_ID = X.CM_USER_ID
           AND  (X.FALTANTE<>0 OR X.SOBRANTE<>0)

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE))    
           WHERE   
                  (MBM_DATETIME  >= @pDateFrom  
             AND   MBM_DATETIME  <= @pDateTo)
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   MB2.MBM_DATETIME <= @pDateTo
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
             AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[Opening] TO [wggui] WITH GRANT OPTION 
GRANT EXECUTE ON [dbo].[TodayOpening] TO [wggui] WITH GRANT OPTION 
GO
