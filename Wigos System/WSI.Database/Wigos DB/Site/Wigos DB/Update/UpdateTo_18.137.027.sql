/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 136;

SET @New_ReleaseId = 137;
SET @New_ScriptName = N'UpdateTo_18.137.027.sql';
SET @New_Description = N'Stored: update_MasterUsers';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** RECORDS ******/

UPDATE   GENERAL_PARAMS
   SET   GP_GROUP_KEY = 'Cashier.Scan', GP_SUBJECT_KEY = 'AutoStartAfterSeconds'
 WHERE   GP_GROUP_KEY = 'Cashier'   AND GP_SUBJECT_KEY = 'ScanCountDown'

UPDATE   GENERAL_PARAMS
   SET   GP_GROUP_KEY = 'Cashier.Scan', GP_SUBJECT_KEY = 'MaxDocuments'
 WHERE   GP_GROUP_KEY = 'Cashier'   AND GP_SUBJECT_KEY = 'MaxAllowedScanDocs'

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Quality')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Quality', '90');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'GrayScale')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'GrayScale', '0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.000.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.000.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.001.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.001.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.002.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.002.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.003.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.003.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.004.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.004.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.005.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.005.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.006.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.006.XYWH', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.007.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.007.XYWH', '');

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente debe identificarse para realizar esta recarga.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Identification.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar esta recarga, el cliente va a ser inclu�do en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Report.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente debe identificarse para realizar este reintegro.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Identification.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar este reintegro, el cliente va a ser inclu�do en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximos reintegros.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Identification.Warning.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximos reintegros.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Warning.Message'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '001,002'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '001,002'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '1'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '1'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '003'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '003'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList'

GO

/****** STORED PROCEDURES ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: update_MasterUsers.sql
--
--   DESCRIPTION: Update Corporate Users in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 15-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[update_MasterUsers]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[update_MasterUsers]
GO 

CREATE PROCEDURE   [dbo].[update_MasterUsers]
                   @ProfileMasterId       BIGINT
                 , @UserName              NVARCHAR(50)
                 , @UserEnabled           BIT
                 , @UserPassword          BINARY(40)
                 , @UserNotValidBefore    DATETIME
                 , @UserNotValidAfter     DATETIME
                 , @UserLastChanged       DATETIME
                 , @UserPasswordExp       DATETIME
                 , @UserPwdChgReq         BIT
                 , @UserLoginFailures     INT
                 , @UserFullName          NVARCHAR(50)
                 , @UserTimestamp         BIGINT
                 , @UserType              SMALLINT
                 , @UserSalesLimit        MONEY
                 , @UserMbSalesLimit      MONEY
                 , @UserBlockReason       INT
                 , @UserMasterId          INT
                 
AS
BEGIN

  DECLARE @UserId             AS BIGINT
  DECLARE @ProfileId          AS BIGINT
  

  SET @ProfileId = NULL
  SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @ProfileMasterId )
 
  SET @UserId = NULL
  SET @UserId = ( SELECT GU_USER_ID FROM GUI_USERS WHERE GU_USERNAME = @UserName )

  IF @UserId IS NULL AND @ProfileId IS NOT NULL
  BEGIN
    -- is new User
    SET @UserId = ISNULL((SELECT MAX(GU_USER_ID) FROM GUI_USERS), 0) + 1
    
    INSERT INTO GUI_USERS (GU_USER_ID, GU_PROFILE_ID, GU_USERNAME, GU_ENABLED,   GU_PASSWORD,   GU_NOT_VALID_BEFORE, GU_PWD_CHG_REQ, GU_USER_TYPE,  GU_BLOCK_REASON) 
                   VALUES (@UserId,    @ProfileId,    @UserName,   @UserEnabled, @UserPassword, @UserNotValidBefore, @UserPwdChgReq, @UserType, @UserBlockReason)
  END

  IF @UserId IS NOT NULL AND @ProfileId IS NOT NULL
  BEGIN
    UPDATE   GUI_USERS                    
       SET   GU_PROFILE_ID          = @ProfileId  
           , GU_USERNAME            = @UserName            
           , GU_ENABLED             = @UserEnabled         
           , GU_PASSWORD            = @UserPassword        
           , GU_NOT_VALID_BEFORE    = @UserNotValidBefore  
           , GU_NOT_VALID_AFTER     = @UserNotValidAfter   
           , GU_LAST_CHANGED        = @UserLastChanged     
           , GU_PASSWORD_EXP        = @UserPasswordExp     
           , GU_PWD_CHG_REQ         = @UserPwdChgReq
           , GU_LOGIN_FAILURES      = @UserLoginFailures
           , GU_FULL_NAME           = @UserFullName 
           , GU_USER_TYPE           = @UserType        
           , GU_SALES_LIMIT         = @UserSalesLimit      
           , GU_MB_SALES_LIMIT      = @UserMbSalesLimit    
           , GU_BLOCK_REASON        = @UserBlockReason     
           , GU_MASTER_ID           = @UserMasterId        
           , GU_MASTER_SEQUENCE_ID  = @UserTimestamp       
     WHERE   GU_USER_ID   = @UserId
  END
END
GO 

