/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 115;

SET @New_ReleaseId = 116;
SET @New_ScriptName = N'UpdateTo_18.116.025.sql';
SET @New_Description = N'PrizeCoupon deleted as promotion. PromoBOX ZIP Code as numeric. SP for report draw tickets. Index for ACP_OPERATION_ID on table ACCOUNT_PROMOTIONS.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

-- Multisite
--
DECLARE @site_id AS INT
SELECT @site_id = CAST(GP_KEY_VALUE AS INT)
  FROM GENERAL_PARAMS
 WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier'
   AND ISNUMERIC(GP_KEY_VALUE) = 1

SET @site_id = ISNULL(@site_id, 0)

IF @site_id > 0
BEGIN
  UPDATE   GENERAL_PARAMS
     SET   GP_KEY_VALUE   = '1'
   WHERE   GP_GROUP_KEY   = 'Site' AND GP_SUBJECT_KEY = 'Configured' 
END

GO

-- DELETE PRIZE COUPON FROM ACCOUNT_PROMOTIONS --
DELETE
  FROM   ACCOUNT_PROMOTIONS
 WHERE   ACP_PROMO_TYPE = 7

GO

/****** UPDATE SALES_PER_HOUR.SPH_THEORETICAL_WON_AMOUNT ******/

DECLARE @default_payout AS MONEY

SELECT @default_payout = CAST(GP_KEY_VALUE AS MONEY) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'TerminalDefaultPayout' AND ISNUMERIC(GP_KEY_VALUE) = 1

SET @default_payout = ISNULL(@default_payout, 95) / 100

UPDATE   SALES_PER_HOUR
   SET   SPH_THEORETICAL_WON_AMOUNT = SPH_PLAYED_AMOUNT * ISNULL(TE_THEORETICAL_PAYOUT, @default_payout)
  FROM   SALES_PER_HOUR
INNER   JOIN TERMINALS ON TE_TERMINAL_ID = SPH_TERMINAL_ID

GO

/****** INDEXES ******/

CREATE NONCLUSTERED INDEX [IX_acp_operation_id] ON [dbo].[account_promotions] 
(
	[acp_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

-- PromoBOX: ZIP CODE as NUMERIC ZIP CODE
UPDATE   WKT_PLAYER_INFO_FIELDS
   SET   PIF_TYPE     = 4
 WHERE   PIF_FIELD_ID = 3

GO

/****** STORE PROCEDURES ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportDrawTickets]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ReportDrawTickets]
GO

CREATE PROCEDURE [dbo].[ReportDrawTickets]
  @pDrawId      BIGINT        = NULL,
  @pDrawName    NVARCHAR(50)  = NULL,
  @pSqlAccount  NVARCHAR(MAX) = NULL,
  @pTicketFrom  DATETIME      = NULL,
  @pTicketTo    DATETIME      = NULL
AS
BEGIN
  SET NOCOUNT ON;

  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_HOLDER_LEVEL INT)
  IF @pSqlAccount IS NOT NULL
  BEGIN
    INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_HOLDER_LEVEL FROM ACCOUNTS ' + @pSqlAccount)
    IF @@ROWCOUNT > 500
      ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
  END

  IF @pDrawId IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_DRAW_ID  = @pDrawId
       AND   DT_CREATED >= @pTicketFrom
       AND   DT_CREATED  < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pDrawName IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DR_NAME    LIKE '%' + @pDrawName + '%'
       AND   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED    < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pSqlAccount IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by account
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_TICKET_ACCOUNT_CREATED))
     INNER   JOIN DRAWS          ON DR_ID         = DT_DRAW_ID
     INNER   JOIN #ACCOUNTS_TEMP ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED   <  @pTicketTo
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pTicketFrom IS NULL AND @pTicketTo IS NULL
    RAISERROR ('ReportDrawTickets - DateRange not defined!', 20, 0) WITH LOG

    
  IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
  IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
  --
  -- For a period
  --
  SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
         , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
         , COUNT(1) NUM_TICKETS
         , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
         , MIN(DT_CREATED) FIRST_TICKET
         , MAX(DT_CREATED) LAST_TICKET
    FROM   DRAW_TICKETS WITH (INDEX(IX_DRAW_TICKET_CREATED))
   INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
   INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
   WHERE   DT_CREATED   >= @pTicketFrom
     AND   DT_CREATED   <  @pTicketTo
   GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
   ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

  DROP TABLE #ACCOUNTS_TEMP

END -- ReportDrawTickets

GO

/****** PERMISSIONS ******/

GRANT EXECUTE ON [dbo].[ReportDrawTickets] TO [wggui] WITH GRANT OPTION
GO
