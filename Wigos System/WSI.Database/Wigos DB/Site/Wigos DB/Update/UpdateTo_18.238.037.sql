/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 237;

SET @New_ReleaseId = 238;
SET @New_ScriptName = N'UpdateTo_18.238.037.sql';
SET @New_Description = N'Changes for PSA Clients, New occupations, Concepts and GT PlayerTracking';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

--
-- PSA
--
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[psa_client_daily_report]') AND type in (N'U'))
  CREATE TABLE [dbo].[psa_client_daily_report](
	[pcdr_date_report] [datetime] NOT NULL,
	[pcdr_status] [int] NULL,
	[pcdr_num_send] [int] NULL,
	[pcdr_num_registers] [int] NULL,
	[pcdr_num_constancies] [int] NULL,	
	[pcdr_initial_balance] [money] NULL,
	[pcdr_final_balance] [money] NULL,
	[pcdr_date_send] [datetime] NULL,
	[pcdr_date_first_register] [datetime] NULL,
	[pcdr_date_last_register] [datetime] NULL,
 CONSTRAINT [PK_psa_client_daily_report] PRIMARY KEY CLUSTERED 
(
	[pcdr_date_report] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--
-- Gaming Tables
--
IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[gaming_tables]') AND name = 'gt_last_tick_date') 
BEGIN 
  ALTER TABLE dbo.gaming_tables ADD gt_last_tick_date Datetime NULL 
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[gaming_tables]') AND name = 'gt_theoric_hold') 
BEGIN 
  ALTER TABLE dbo.gaming_tables ADD gt_theoric_hold decimal(5,2) null
END
GO

--
-- Concepts
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cage_concept_movement_detail]') AND type in (N'U'))
DROP TABLE [dbo].[cage_concept_movement_detail]
GO
CREATE TABLE [dbo].[cage_concept_movement_detail](
      [ccmd_cage_concept_movement_detail_id] [bigint] IDENTITY(1,1) NOT NULL,
      [ccmd_movement_id] [bigint] NOT NULL,
      [ccmd_value] [money] NOT NULL,
      [ccmd_iso_code] [nvarchar](3) NOT NULL,
      [ccmd_concept_id] [bigint] NOT NULL,
CONSTRAINT [PK_cage_concept_movement_detail] PRIMARY KEY CLUSTERED 
(
      [ccmd_cage_concept_movement_detail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/******* INDEXES *******/

/******* RECORDS *******/

--
-- PSA
--
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'OperadorId')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'OperadorId', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'EstablecimientoId')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'EstablecimientoId', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PSAType')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'PSAType', '1')
GO

--
-- Concepts
--
IF NOT EXISTS (SELECT TOP 1 cc_concept_id FROM cage_concepts WHERE cc_concept_id = 7)
      INSERT INTO cage_concepts
              ( cc_concept_id, cc_description, cc_is_provision, cc_show_in_report, cc_name, cc_type, cc_enabled)
        VALUES
              ( 7, 'Faltante', 0, 1, 'Faltante', 0, 1 )
GO     
IF NOT EXISTS (SELECT TOP 1 cc_concept_id FROM cage_concepts WHERE cc_concept_id = 8)      
      INSERT INTO cage_concepts
              ( cc_concept_id, cc_description, cc_is_provision, cc_show_in_report, cc_name, cc_type, cc_enabled)
        VALUES
              ( 8, 'Sobrante', 0, 1, 'Sobrante', 0, 1 )
GO
IF NOT EXISTS (SELECT TOP 1 cstc_concept_id FROM cage_source_target_concepts WHERE cstc_concept_id = 7 AND cstc_source_target_id = 0)   
      INSERT INTO cage_source_target_concepts
                 ( cstc_concept_id, cstc_source_target_id, cstc_type, cstc_only_national_currency, cstc_enabled, cstc_price_factor )
           VALUES
                 ( 7, 0, 0, 0, 1, 1 )
GO
IF NOT EXISTS (SELECT TOP 1 cstc_concept_id FROM cage_source_target_concepts WHERE cstc_concept_id = 8 AND cstc_source_target_id = 0)   
      INSERT INTO cage_source_target_concepts
                 ( cstc_concept_id, cstc_source_target_id, cstc_type, cstc_only_national_currency, cstc_enabled, cstc_price_factor )
           VALUES
                 ( 8, 0, 0, 0, 1, 1 )
GO                 
EXEC CageCreateMeters 0, 7, NULL, 3, NULL -- Create in cage_meters and oppened cage sessions
EXEC CageCreateMeters 0, 8, NULL, 3, NULL -- Create in cage_meters and oppened cage sessions
GO

IF NOT EXISTS (SELECT TOP 1 cc_concept_id FROM cage_concepts WHERE cc_concept_id = 9)
  INSERT INTO cage_concepts
                ( cc_concept_id, cc_description, cc_is_provision, cc_show_in_report, cc_name, cc_type, cc_enabled)
          VALUES
                ( 9, 'Tickets', 0, 1, 'Tickets', 0, 1 )
GO
IF NOT EXISTS (SELECT TOP 1 cstc_concept_id FROM cage_source_target_concepts WHERE cstc_concept_id = 9 AND cstc_source_target_id = 0)   
      INSERT INTO cage_source_target_concepts
                 ( cstc_concept_id, cstc_source_target_id, cstc_type, cstc_only_national_currency, cstc_enabled, cstc_price_factor )
           VALUES
                 ( 9, 10, 0, 1, 1, 1 )
GO
IF NOT EXISTS (SELECT TOP 1 cstc_concept_id FROM cage_source_target_concepts WHERE cstc_concept_id = 9 AND cstc_source_target_id = 0)   
  INSERT INTO cage_source_target_concepts
			 ( cstc_concept_id, cstc_source_target_id, cstc_type, cstc_only_national_currency, cstc_enabled, cstc_price_factor )
	   VALUES
			 ( 9, 12, 0, 1, 1, 1 )

GO             
EXEC CageCreateMeters 10, 9, NULL , 3, NULL -- Create in cage_meters and oppened cage sessions                         
EXEC CageCreateMeters 12, 9, NULL , 3, NULL -- Create in cage_meters and oppened cage sessions
GO

---
--- Occupations
---
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'NO APLICA','1000000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - DISE�ADORES GR�FICOS','1014010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - EDITORES, PERIODISTAS, REPORTEROS Y REDACTORES','1014030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - ASISTENTES Y/U OPERADORES DE PRODUCCI�N DE CINE, RADIO Y TELEVISI�N','1023010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - LOCUTORES, COMENTARISTAS Y CRONISTAS DE RADIO Y TELEVISI�N','1023070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - PRODUCTORES Y DIRECTORES DE CINE, TELEVISI�N Y TEATRO','1024020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ACTORES, BAILARINES, M�SICOS, ESCRITORES','1033010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ARTESANOS','1033020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - FOT�GRAFOS','1033040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ARTISTAS PL�STICOS','1034010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRADUCCI�N E INTERPRETACI�N LING��STICA - INT�RPRETES Y/O TRADUCTORES','1044010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PUBLICIDAD, PROPAGANDA Y RELACIONES P�BLICAS - MODELOS Y EDECANES','1052010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PUBLICIDAD, PROPAGANDA Y RELACIONES P�BLICAS - DIRECTORES, GERENTES Y EMPLEADOS DE PUBLICIDAD','1054010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'AGRICULTURA Y SILVICULTURA - ADMINISTRADORES O TRABAJADORES AGRICOLAS','1110100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'AGRICULTURA Y SILVICULTURA - ADMINISTRADORES O TRABAJADORES SILV�COLAS Y FORESTALES','1110400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ENCUESTADORES Y CODIFICADORES','1112010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ASISTENTES DE INVESTIGADORES','1113010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - QUIMICOS Y/O T�CNICOS EN QU�MICA','1113020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - BI�LOGOS Y CIENT�FICOS RELACIONADOS','1114010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ECONOMISTAS Y POLIT�LOGOS (NO FUNCIONARIOS PUBLICOS)','1114020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - F�SICOS ASTR�NOMOS','1114030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - GE�LOGOS, GEOQU�MICOS, GEOF�SICOS Y GE�GRAFOS','1114040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - INVESTIGADORES Y CONSULTORES EN MERCADOTECNIA','1114050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - MATEM�TICOS, ESTAD�STICOS Y ACTUARIOS','1114060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - METEOR�LOGOS','1114070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - SOCI�LOGOS, ANTROP�LOGOS E HISTORIADORES','1114090',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - CAPACITADORES E INSTRUCTORES','1123020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - PROFESORES O DOCENTES','1124070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - DIRECTORES GENERALES DE EDUCACI�N','1125010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DIFUSI�N CULTURAL - PROMOTORES DE DIFUSI�N CULTURAL','1133010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DIFUSI�N CULTURAL - TRABAJADORES DE BIBLIOTECA, ARCHIVO, MUSEO Y GALER�A DE ARTE','1134010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - ESTUDIANTE � MENOR DE EDAD SIN OCUPACI�N','1135010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - DESEMPLEADO','1135020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - JUBILADO O PENSIONADO','1135030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - AMA DE CASA O QUEHACERES DEL HOGAR','1135050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - MINISTROS DE CULTO RELIGIOSO (SACERDOTE, PASTOR, MONJA, ETC)','1135060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - AGENTE ADUANAL','1135070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - PROPIETARIO, ACCIONISTA O SOCIO','1135080',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER EJECUTIVO FEDERAL','1136010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO PODER EJECUTIVO ESTATAL O DEL DISTRITO FEDERAL','1136020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER EJECUTIVO MUNICIPAL O DELEGACIONAL','1136030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER JUDICIAL FEDERAL','1136040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER LEGISLATIVO FEDERAL','1136050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER LEGISLATIVO ESTATAL O DEL DISTRITO FEDERAL','1136060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER JUDICIAL ESTATAL O DEL DISTRITO FEDERAL','1136070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ORGANISMOS INTERNACIONALES Y EXTRATERRITORIALES - EMPLEADOS DE ORGANISMOS INTERNACIONALES Y EXTRATERRITORIALES','1136080',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EJERCITO, ARMADA Y FUERZA AEREA','1136090',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'GANADER�A - APICULTORES','1220100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'GANADER�A - ADMINISTRADORES, CRIADORES O SUPERVISORES AVICOLAS Y GANADEROS','1220200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PESCA Y ACUACULTURA - PESCADORES Y TRABAJADORES EN LA CR�A Y CULTIVO DE ESPECIES MARINAS','1330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - T�CNICOS GEOL�GICOS Y DE MINERALES','2130100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - INGENIEROS, OPERADORES O AYUDANTES EN LA EXTRACCI�N Y REFINACI�N MINERA','2140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - AYUDANTES EN LA PERFORACI�N DE POZOS DE PETR�LEO Y GAS NATURAL','2210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - INGENIEROS PETROLEROS','2240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - OPERADORES DE CENTRALES O SISTEMAS  DE ENERG�A EL�CTRICAS','2420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - OPERADORES DE M�QUINAS DE VAPOR','2420200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - GERENTE, SUPERVISOR U OPERADORES DE  TRATAMIENTO Y POTABILIZACI�N, ABASTECIMIENTO Y RECOLECCI�N DE AGUA','2530100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - DECORADORES DE INTERIORES','3130100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N -  INGENIEROS, T�CNICOS Y OPERADORES DE LA CONSTRUCCI�N','3130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - COLOCADORES DE PRODUCTOS PREFABRICADOS EN INMUEBLES','3310300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - PINTORES','3310400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - VIDRIEROS','3310600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - REPARADORES DE V�AS DE COMUNICACI�N','3410200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - PLOMEROS E INSTALADORES DE TUBER�A','3420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - ARQUITECTOS','3420200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - MEC�NICOS DE EQUIPO PESADO','4130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - INGENIERO O MEC�NICOS INSTALADORES DE MAQUINARIA INDUSTRIAL','4130500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - INGENIERO O T�CNICO EN MEC�NICA DE VEH�CULOS TERRESTRES, A�REOS Y ACU�TICOS','4131000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTRICIDAD - INGENIEROS O T�CNICOS ELECTRICISTAS','4230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTRICIDAD - T�CNICOS EN REFRIGERACI�N, AIRE ACONDICIONADO Y CALEFACCI�N','4230500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTR�NICA - MEC�NICOS DE INSTRUMENTOS INDUSTRIALES','4330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTR�NICA - INGENIERO O T�CNICOS EN  ELECTR�NICA','4330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INFORM�TICA - INGENIERO O T�CNICOS PROGRAMADORES EN INFORM�TICA','4430200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INFORM�TICA - PROFESIONISTAS O T�CNICOS DE SISTEMAS DE INFORMACI�N Y PROCESAMIENTO DE DATOS','4440100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TELECOMUNICACIONES - INGENIERO, INSTALADORES Y REPARADORES DE EQUIPOS Y ACCESORIOS DE TELECOMUNICACIONES','4530200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TELECOMUNICACIONES - TELEGRAFISTAS Y RADIO-OPERADORES','4530500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROCESOS INDUSTRIALES - INGENIERO O T�CNICOS INDUSTRIAL Y DE PRODUCCI�N','4630100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROCESOS INDUSTRIALES - INGENIEROS METAL�RGICOS Y DE MATERIALES','4640300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINERALES NO MET�LICOS - OPERADORES O TRABAJADORES DE VIDRIO Y CONCRETO','5110100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINERALES NO MET�LICOS - OPERADORES DE M�QUINAS PROCESADORAS DE MINERALES NO MET�LICOS','5121300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'METALES - SUPERVISORES U OPERADORES DE PROCESAMIENTO Y FUNDICI�N DE METALES','5210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTOS Y BEBIDAS - TRABAJADORES EN LA ELABORACI�N  Y PROCESAMIENTO DE ALIMENTOS, BEBIDAS Y TABACO','5310700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TEXTILES Y PRENDAS DE VESTIR - TRABAJADORES EN LA PRODUCCI�N DE TEXTILES, PRENDAS DE VESTIR Y CALZADO','5410100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TEXTILES Y PRENDAS DE VESTIR - TRABAJADORES DE REPARACI�N DE PRENDAS DE VESTIR Y CALZADO','5420700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MADERA, PAPEL, Y PIEL - TRABAJADORES EN LA FABRICACI�N DE MUEBLES O PRODUCTOS DE MADERA Y/O PIEL','5510100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS QU�MICOS - TRABAJADORES EN EL PROCESAMIENTO Y FABRICACI�N DE PRODUCTOS QU�MICOS Y FARMACOQU�MICAS','5610200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - TRABAJADORES  EN LA FABRICACI�N DE PRODUCTOS MET�LICOS, DE HULE Y PL�STICOS','5710200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - ENSAMBLADORES Y ACABADORES DE PRODUCTOS DE PL�STICO','5720800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - FABRICANTES DE HERRAMIENTAS Y TROQUELES','5720900',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - HERREROS Y FORJADORES','5721000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - JOYEROS Y ORFEBRES','5721100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - SOLDADORES Y OXICORTADORES','5722900',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - TRABAJADORES EN EL ENSAMBLADO DE VEH�CULOS, MOLDEADO, LAMINADO Y MONTAJE DE PIEZAS MET�LICAS, HULE O PLASTICO','5730600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - SUPERVISORES EN LA FABRICACI�N Y MONTAJE DE ART�CULOS DEPORTIVOS, DE JUGUETES Y SIMILARES','5731100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS EL�CTRICOS Y ELECTR�NICOS - TRABAJADORES EN LA FABRICACI�N DE PRODUCTOS EL�CTRICOS Y ELECTR�NICOS','5810200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS IMPRESOS - TRABAJADORES EN LA ELABORACI�N DE PRODUCTOS IMPRESOS','5910400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE FERROVIARIO - CONDUCTORES Y OPERADORES DE TREN SUBTERR�NEO Y DE TREN LIGERO','6120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE FERROVIARIO - TRABAJADORES DE FERROCARRILES','6120200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE TERRESTRE - CONDUCTORES DE VEH�CULOS DE TRANSPORTE, SERVICIOS DE CARGA Y/O REPARTO','6220100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - COORDINADORES Y SUPERVISORES EN SERVICIOS DE TRANSPORTE A�REO','6330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - DESPACHADORES DE VUELO Y ESPECIALISTAS EN SERVICIOS A�REOS','6330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - SUPERVISORES DE SISTEMAS DE COMUNICACI�N PARA LA AERONAVEGACI�N','6330400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - PILOTOS DE AVIACI�N E INSTRUCTORES DE VUELO','6330500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - SOBRECARGOS','6330600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - CONDUCTORES DE EMBARCACIONES','6420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - JEFES Y CONTROLADORES DE TR�FICO MAR�TIMO','6430100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - PILOTOS, CAPITANES DE PUERTOS Y OFICIALES DE CUBIERTA','6430300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - DESPACHADORES DE GASOLINERA','7110200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - EMPACADORES DE MERCANC�AS','7110300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - TAQUILLEROS','7110500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - VENDEDORES AMBULANTES','7110600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - CAJEROS REGISTRADORES','7120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - REPRESENTANTES DE VENTAS POR TEL�FONO O POR TELEVISI�N','7130200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - VENDEDORES ESPECIALIZADOS','7130400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - GERENTES O SUPERVISOR DE ESTABLECIMIENTO COMERCIAL','7140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - GERENTES O EMPLEADOS DE VENTAS','7140200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - TRABAJADORES DE SERVICIO DE ALIMENTOS Y BEBIDAS','7210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - JEFES DE COCINA, RESTAURANTE Y/O BAR','7230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - TRABAJADORES DE SERVICIOS DE ALOJAMIENTO','7240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TURISMO - COORDINADORES DE OPERACIONES EN AGENCIAS DE VIAJES','7330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TURISMO - GU�AS DE EXCURSIONES O ECOTUR�STICO','7330300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - ANIMADORES RECREATIVOS','7430100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - ATLETAS, ENTRENADORES O INSTRUCTORES EN DEPORTE Y RECREACI�N','7430200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - OFICIALES, JUECES Y �RBITROS DEPORTIVOS','7430400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS PERSONALES - ESTILISTAS, ESTETICISTAS Y MASAJISTAS','7530100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS PERSONALES - TRBAJADORES DE SERVICIOS FUNERARIOS O CEMENTERIOS','7540100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - CERRAJEROS','7620100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - REPARADORES DE ART�CULOS DE HULE','7620200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - RELOJEROS Y REPARADORES DE RELOJES','7630100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - REPARADORES DE APARATOS EL�CTRICOS','7630200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - SERVICIOS DE CAMARISTAS Y ASEADORES','7720100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - TRABAJADORES DE TINTORER�A Y LAVANDER�A','7720200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - FUMIGADORES DE PLAGAS','7720300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIO POSTAL Y MENSAJER�A - EMPLEADOS DE SERVICIOS DE MENSAJER�A','7810200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - GERENTES O TRABAJADORES DE SERVICIOS Y PRODUCTOS FINANCIEROS','8120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - VALUADORES','8130500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - AGENTES DE VALORES, PROMOTORES Y CORREDORES DE INVERSI�N','8140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - TRABAJADORES DE ARCHIVO, ALMACEN DE INVENTARIOS','8210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CAPTURISTA Y OPERADORES DE TEL�FONO','8220400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - PAGADORES Y COBRADORES','8220600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES  Y EMPLEADOS DE COMPRAS, FINANZAS, RECURSOS HUMANOS Y SERVICIOS ADMINISTRATIVOS','8230300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - ASISTENTES ADMINISTRATIVOS','8240100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CONTADORES Y AUDITORES','8240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE PRODUCCI�N','8240600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE SERVICIOS DE TRANSPORTE','8240700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CONSULTORES','8250100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE COMERCIALIZACI�N','8250200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPELADOS ADMINISTRATIVOS','8250800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS LEGALES - ABOGADOS Y ASESORES LEGALES','8340100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS LEGALES - NOTARIOS Y CORREDORES P�BLICOS','8340300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - ENFERMERAS Y/O PARAMEDICOS','9120700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - DIETISTAS Y NUTRI�LOGOS','9130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - T�CNICOS DE LABORATORIO M�DICO','9131200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - DIRECTORES DE INSTITUCIONES EN EL CUIDADO DE LA SALUD','9140200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - FARMAC�UTICOS','9140400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - FISIOTERAP�UTAS Y QUIROPR�CTICOS','9140500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - M�DICOS ESPECIALISTAS','9140700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - M�DICOS GENERALES Y FAMILIARES','9140800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES DE SALUD AMBIENTAL, SANIDAD Y DEL TRABAJO','9230100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES DE TRANSPORTE DE CARGA Y DE PASAJEROS','9230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES FISCALES Y DE PRECIOS','9230300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES SANITARIOS Y DE CONTROL DE CALIDAD DE PRODUCTOS C�RNICOS, PESQUEROS Y AGR�COLAS','9230400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SEGURIDAD SOCIAL - CONSEJEROS DE EMPLEO','9330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SEGURIDAD SOCIAL - TRABAJADORES DE SERVICIO SOCIAL Y DE LA COMUNIDAD','9330300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - BOMBEROS','9420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - GUARDIAS DE SEGURIDAD','9420300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - DETECTIVES PRIVADOS','9430100',1,1000,'MX')
GO

--
-- Handpays
--
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'Handpay.TotalInLetters') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','Handpay.TotalInLetters','0')
GO

--
-- PSA
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='FieldToReportPrize')
  INSERT INTO [dbo].[general_params] (gp_group_key, gp_subject_key, gp_key_value)
                              VALUES ('PSAClient',  'FieldToReportPrize', '0');
GO
       
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='ReportRefunds')
  INSERT INTO [dbo].[general_params] (gp_group_key, gp_subject_key, gp_key_value)
                              VALUES ('PSAClient',  'ReportRefunds', '0');
GO

--
-- Progressive
--
DECLARE @HpId         AS BIGINT
DECLARE @LongPoll_1b  AS XML
DECLARE @Level        AS INT

DECLARE cursor_handpays CURSOR FOR SELECT  HP_ID, HP_LONG_POLL_1B_DATA, HP_LEVEL 
                                     FROM  HANDPAYS
                                    WHERE  HP_LONG_POLL_1B_DATA IS NOT NULL AND HP_LEVEL IS NULL
OPEN cursor_handpays
FETCH NEXT FROM cursor_handpays INTO @HpId, @LongPoll_1b, @Level

WHILE @@FETCH_STATUS = 0
BEGIN 

  UPDATE HANDPAYS
     SET HP_LEVEL = hp_long_poll_1b_data.value('(/WCP_MsgEGMHandpays/Handpays/Handpay/@Level)[1]', 'int')
   WHERE HP_ID = @HpId;

FETCH NEXT FROM cursor_handpays INTO @HpId, @LongPoll_1b, @Level
END

CLOSE cursor_handpays
DEALLOCATE cursor_handpays
GO

/******* STORED PROCEDURES *******/

IF OBJECT_ID (N'dbo.GT_Calculate_DROP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP;                 
GO 

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type
   
   PARAMS:  @pOwnSalesAmount: Chips sales
            @pExternalSalesAmount: Chips sales from other cashiers
   
   RETURN:  Money: Drop
*/
CREATE FUNCTION GT_Calculate_DROP ( @pOwnSalesAmount AS MONEY, @pExternalSalesAmount AS MONEY, @pCollectedAmount AS MONEY, @pIsIntegratedCashier AS BIT ) RETURNS MONEY
  BEGIN 
  DECLARE @drop as Money
    IF @pIsIntegratedCashier = 1 
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount
    ELSE
     SET @drop = @pOwnSalesAmount + @pExternalSalesAmount + @pCollectedAmount
     
   RETURN @drop
  END
GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.1    24-DEC-2013    RMS      Using GAMING_TABLE_SESSION
1.0.2    20-FEB-2014    RMS      Only get data of closed gaming table sessions

Requeriments:
   -- Functions:
         dbo.GT_Calculate_DROP( Amount )
         dbo.GT_Calculate_WIN ( AmountAdded, AmountSubtracted, CollectedAmount, TipsAmount )

Parameters:
   -- BASE TYPE:      Indicates if data is based on table types (0) or the tables (1).
   -- TIME INTERVAL:  Range of time to group data between days (0), months (1) and years (2).
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- ONLY ACTIVITY:  If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
   -- ORDER BY:       (0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.   
   -- VALID TYPES:    A string that contains the valid table types to list.
                      
Process: (From inside to outside)
   
   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.
   
   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.
         
   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.
   
 Results:
   
   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------   
*/

IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;                 
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP) TOTAL_DROP         
         , SUM(X.S_WIN)  TOTAL_WIN         
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP         
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP       
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD  
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP) / SUM(X.S_DROP) * 100 END AS TIP_DROP         
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
           -- CORE QUERY
                             SELECT    CASE 
                      WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                      END AS CM_DATE_ONLY 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)                        AS TABLE_IDENTIFIER  -- GET THE BASE TYPE IDENTIFIER
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                                                                             AS TABLE_NAME                  -- GET THE BASE TYPE IDENTIFIER NAME
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                                                        AS TABLE_TYPE                -- TYPE 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                                                                            AS TABLE_TYPE_NAME      -- TYPE NAME
                                                     , DBO.GT_CALCULATE_DROP(  ISNULL(GTS_OWN_SALES_AMOUNT, 0)       , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)   , ISNULL(GTS.GTS_COLLECTED_AMOUNT, 0), GT.GT_HAS_INTEGRATED_CASHIER)     AS S_DROP
                                                     , GT.GT_THEORIC_HOLD  AS THEORIC_HOLD
                                                     , DBO.GT_CALCULATE_WIN (  ISNULL(GTS.GTS_FINAL_CHIPS_AMOUNT  , 0)
                                                                                                   , ISNULL(GTS.GTS_INITIAL_CHIPS_AMOUNT, 0)
                                                                                                   , ISNULL(GTS.GTS_COLLECTED_AMOUNT    , 0)
                                                                                                   , ISNULL(GTS_TIPS, 0))                                                                                                                                                                 AS S_WIN
                                                     , ISNULL(GTS_TIPS, 0)                                                                                                                                                                                                  AS S_TIP
                                                     , CS.CS_OPENING_DATE                                                                                                                                                                 AS OPEN_HOUR
                                                     , CS.CS_CLOSING_DATE                                                                                                                                                                 AS CLOSE_HOUR
                                                     , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                                                             AS SESSION_HOURS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID 
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID 
                                         AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
                                   WHERE   CS_STATUS = 1   -- Only closed sessions      
          -- END CORE QUERY
          
        ) AS X          
 
 GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT
        
        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER 
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD, 
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME 

      FROM @_DAYS_AND_TABLES DT
        
       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       
       -- SET ORDER             
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GamingTables.sql
-- 
--   DESCRIPTION: Gaming Tables reports procedures and functions
-- 
--        AUTHOR: Dani Dom�nguez
-- 
-- CREATION DATE: 05-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 05-SEP-2014 DDM    First release.
-- 19-SEP-2014 DCS    Edit Procedures Player Tracking
-- 08-OCT-2014 JML    Fixed Bug WIG-1439: PlayerTracking: Report columns with wrong format 
-- 15-OCT-2014 DCS		Fixed Bug WIG-1505: Error in Group BY
-- 16-OCT-2014 DCS    Add theoric hold

----------------------------------------------------------------------------------------------------------------
-------------------------- STORED PROCEDURE
----------------------------------------------------------------------------------------------------------------

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------

Parameters:
   -- BASE TYPE:      Indicates if data is based on table types (0) or the tables (1).
   -- TIME INTERVAL:  Range of time to group data between days (0), months (1) and years (2).
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- ONLY ACTIVITY:  If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
   -- ORDER BY:       (0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.   
   -- VALID TYPES:    A string that contains the valid table types to list.
                      
Process: (From inside to outside)
   
   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.
   
   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.
         
   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.
   
 Results:
   
   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------   
*/


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data_Player_Tracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         , SUM(X.BUY_IN) AS BUY_IN
         , SUM(X.CHIPS_IN) AS CHIPS_IN
         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
		   		   THEN 0
		   		   ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100) 
		       END AS HOLD
		     , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
		   		   THEN 0
		   		   ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
		       END AS PAYOUT
		     , SUM(X.NETWIN) AS NETWIN
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
            -- CORE QUERY
					  SELECT   CASE 
												WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
														DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
												WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
														CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
												WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
														CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
										 END AS CM_DATE_ONLY 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER			-- GET THE BASE TYPE IDENTIFIER
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME																					-- GET THE BASE TYPE IDENTIFIER NAME
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE														-- TYPE 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME																				-- TYPE NAME
									 , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN  
									 , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
									 , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
									 , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
									 , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
									 , SUM(GTPS_NETWIN) AS NETWIN  
									 , GT_THEORIC_HOLD  AS THEORIC_HOLD                                             
									 , CS_OPENING_DATE AS OPEN_HOUR
									 , CS_CLOSING_DATE AS CLOSE_HOUR
									 , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
						  FROM   GT_PLAY_SESSIONS   
						 INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
						 INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
						 INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
						 INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
						   AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
						 WHERE   CS_STATUS = 1   
						  AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
						 GROUP   BY   GTPS_GAMING_TABLE_ID
									 , GT_NAME
									 , CS_OPENING_DATE 
									 , CS_CLOSING_DATE 
									 , GTT_GAMING_TABLE_TYPE_ID
									 , GTT_NAME
									 , GT_THEORIC_HOLD  
             -- END CORE QUERY      
        ) AS X          
 GROUP BY  X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY    

  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
			SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
							 DT.TABLE_IDENT_NAME AS TABLE_NAME,
							 DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
							 DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
							 ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			         ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
               ISNULL(ZZ.HOLD, 0) AS HOLD, 
               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
               ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
               ZZ.OPEN_HOUR,
               ZZ.CLOSE_HOUR,
               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
               DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ 
               ON (
									(@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                )
         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER 
		ORDER BY   DT.TABLE_TYPE_IDENT ASC,
							 CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
							 DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT	DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			        ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.HOLD, 0) AS HOLD, 
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
              ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME 
        FROM	@_DAYS_AND_TABLES DT        
  INNER JOIN	#GT_TEMPORARY_REPORT_DATA ZZ 
              ON(
                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
              )
              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER             
   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
              DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
										 ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD,
										 ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER              
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD,
										 ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN,  
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT               
        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER                
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION
GO


--
-- Concepts
--
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateSystemMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageUpdateSystemMeters]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
        @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  DECLARE @Currencies AS VARCHAR(200)
  DECLARE @CurrentCurrency AS VARCHAR(3)
    
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  DECLARE @ClosingShort MONEY
  DECLARE @ClosingOver MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                     FROM CAGE_MOVEMENTS
                                   WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                      AND CGM_TYPE IN (0, 1, 4, 5)
                               ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                FROM CAGE_MOVEMENTS
                               WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                 AND CGM_TYPE IN (100, 101, 103, 104)
                          ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                               WHERE CGS_CLOSE_DATETIME IS NULL
                          ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                             ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
            -- FEDERAL TAX --
  
            SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (142, 6) 
             AND CM_SESSION_ID = @pCashierSessionId

            SET @Tax1 = ISNULL(@Tax1, 0)
            
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax1,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 3,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
    
     
            -- STATE TAX --
   
            SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
             WHERE CM_TYPE IN (146, 14) 
               AND CM_SESSION_ID = @pCashierSessionId 
          
          SET @Tax2 = ISNULL(@Tax2, 0)
         
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax2,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 2,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
          
            ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    


            SELECT @CardRefundable = GP_KEY_VALUE 
              FROM GENERAL_PARAMS  
             WHERE GP_GROUP_KEY = 'Cashier'                             
               AND GP_SUBJECT_KEY = 'CardRefundable'                    

            IF ISNULL(@CardRefundable, 0) = 1                                                                        
            BEGIN                                                                                                   
                  SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 9 THEN CM_ADD_AMOUNT   
                                                              ELSE -1 * CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
               WHERE CM_TYPE IN (9, 10)                                             
                 AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                                                       FROM CAGE_MOVEMENTS                                                                     
                                                    WHERE CGM_CAGE_SESSION_ID = @pCashierSessionId)
                                                    
                  SET @CardDeposit = ISNULL(@CardDeposit, 0)
            
                  EXEC [dbo].[CageUpdateMeter]
                        @pValueIn = @CardDeposit,
                        @pValueOut = 0,
                        @pSourceTagetId = 0,
                        @pConceptId = 5,
                        @pIsoCode = @NationalCurrency,
                        @pOperation = 1,
                        @pSessionId = @CageSessionId,
                        @pSessiongetMode = 0
                     
            END                                                                                                     

                  SELECT @Currencies = GP_KEY_VALUE 
                    FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
                  -- Split currencies ISO codes
                  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)

                  
                  DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
                  SELECT CURRENCY_ISO_CODE
                    FROM #TMP_CURRENCIES_ISO_CODES 
      
            SET NOCOUNT ON;

                  OPEN Curs_CageUpdateSystemMeters

                  FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
    
                  WHILE @@FETCH_STATUS = 0
                  BEGIN
      
                        -- CLOSING SHORT --
                        
                        SELECT @ClosingShort = SUM(CM_ADD_AMOUNT) 
                             FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
                        WHERE CM_TYPE IN (158) 
                             AND CM_SESSION_ID = @pCashierSessionId 
                             AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
                        
                        SET @ClosingShort = ISNULL(@ClosingShort, 0)
                        
                        EXEC [dbo].[CageUpdateMeter]
                                   @pValueIn = @ClosingShort,
                                   @pValueOut = 0,
                                   @pSourceTagetId = 0,
                                   @pConceptId = 7,
                                   @pIsoCode = @CurrentCurrency,
                                   @pOperation = 1,
                                   @pSessionId = @CageSessionId,
                                   @pSessiongetMode = 0 
                        
                        
                        -- CLOSING OVER --
                        
                        SELECT @ClosingOver = SUM(CM_ADD_AMOUNT) 
                             FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
                        WHERE CM_TYPE IN (159) 
                             AND CM_SESSION_ID = @pCashierSessionId 
                             AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
                        
                        SET @ClosingOver = ISNULL(@ClosingOver, 0)
                        
                        EXEC [dbo].[CageUpdateMeter]
                                   @pValueIn = @ClosingOver,
                                   @pValueOut = 0,
                                   @pSourceTagetId = 0,
                                   @pConceptId = 8,
                                   @pIsoCode = @CurrentCurrency,
                                   @pOperation = 1,
                                   @pSessionId = @CageSessionId,
                                   @pSessiongetMode = 0 
                             
                             
                        FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
                
            END

                  CLOSE Curs_CageUpdateSystemMeters
                  DEALLOCATE Curs_CageUpdateSystemMeters

      END                                      

END -- CageUpdateSystemMeters

GO
GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO
                   

--/ ***********************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO


--/******************************  CageGetGlobalReportData  ************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN

      DECLARE @NationalIsoCode NVARCHAR(3)
      DECLARE @CageOperationType_FromTerminal INT

      SET @CageOperationType_FromTerminal = 104
      
      -- TABLE[0]: Get cage stock
    SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
             , CGS_QUANTITY 
      FROM (SELECT CGS_ISO_CODE                                          
                   , CGS_DENOMINATION                                      
                         , CGS_QUANTITY                                          
                    FROM CAGE_STOCK
                  
                  UNION ALL
                  
                  SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
                        , C.CH_DENOMINATION AS CGS_DENOMINATION
                        , CST.CHSK_QUANTITY AS CGS_QUANTITY
                    FROM CHIPS_STOCK AS CST
                           INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
                  ) AS _tbl         
                                                   
  ORDER BY CGS_ISO_CODE                                          
              ,CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                      ELSE CGS_DENOMINATION * (-100000) END -- show negative denominations at the end (coins, cheks, etc.)



      IF @pCageSessionIds IS NOT NULL
      BEGIN
           -- Session IDs Split
            SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
      
          -- TABLE[1]: Get cage sessions information
            SELECT      CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
                    , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
                    , CSM.CSM_ISO_CODE AS CM_ISO_CODE
                    , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
                    , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
                    , CST.CST_SOURCE_TARGET_NAME
                    , CC.CC_DESCRIPTION
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                        INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND      CC.CC_SHOW_IN_REPORT = 1
               AND      CC.CC_ENABLED = 1
               AND      (CC.CC_TYPE = 1 OR CC_CONCEPT_ID = 9)
             AND  CSTC.CSTC_ENABLED = 1
        GROUP BY  CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
        ORDER BY  CSM.CSM_SOURCE_TARGET_ID                   

            -- TABLE[2]: Get dynamic liabilities
            SELECT  CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                    , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
                   , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
                    AND CC.CC_SHOW_IN_REPORT = 1
              AND CC.CC_IS_PROVISION = 1
              AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) 
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC
      
            -- TABLE[3]: Get session sumary
            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN CAST(MCD_FACE_VALUE AS INT) 
                                                     ELSE 0 END AS ORIGIN
                              , CASE WHEN ISNULL(MCD_FACE_VALUE,0) = -200 THEN MC_COLLECTED_TICKET_AMOUNT
                                                     ELSE (CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                                                       ELSE 
                                                                             (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END) END AS CMD_DEPOSITS       
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                           AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE
         
         
         -- TABLE[4]: Other System Concepts           
         SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS CM_DESCRIPTION
                    , CSM_ISO_CODE AS CM_ISO_CODE
                    , SUM(CSM_VALUE) AS CM_VALUE
               FROM CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
              WHERE CSM_SOURCE_TARGET_ID = 0
                AND CSM_CONCEPT_ID IN (7, 8)
                AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
         GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
         ORDER BY CM_CONCEPT_ID, CSM_ISO_CODE

         DROP TABLE #TMP_CAGE_SESSIONS 

      END -- IF @pCageSessionIds IS NOT NULL THEN
      
      ELSE
      BEGIN
      
            -- TABLE[1]: Get cage sessions information
            SELECT CM.CM_SOURCE_TARGET_ID
                 , CM.CM_CONCEPT_ID
                  , CM.CM_ISO_CODE
                  , CM.CM_VALUE_IN
                  , CM.CM_VALUE_OUT
                  , CST.CST_SOURCE_TARGET_NAME
                  , CC.CC_DESCRIPTION
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                     INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID = 9)
               AND CC.CC_ENABLED = 1
               AND CSTC.CSTC_ENABLED = 1
               AND  CC.CC_SHOW_IN_REPORT = 1
        ORDER BY CM.CM_SOURCE_TARGET_ID 


            -- TABLE[2]: Get dynamic liabilities
            SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                  , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                   , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
                  , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
                    AND CC.CC_SHOW_IN_REPORT = 1
              AND CC.CC_IS_PROVISION = 1
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC 
      
                  -- TABLE[3]: Get session sumary

            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN CAST(MCD_FACE_VALUE AS INT) 
                                                     ELSE 0 END AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) = -200 THEN MC_COLLECTED_TICKET_AMOUNT
                                                     ELSE (CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                                                       ELSE 
                                                                             (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END) END AS CMD_DEPOSITS       
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB
                        
         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE
         
         -- TABLE[4]: Other System Concepts           
         SELECT CM_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS CM_DESCRIPTION
                    , CM_ISO_CODE
                    , SUM(CM_VALUE) AS CM_VALUE
               FROM CAGE_METERS AS CM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
              WHERE CM_SOURCE_TARGET_ID = 0
                AND CM_CONCEPT_ID IN (7, 8)
         GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
         ORDER BY CM_CONCEPT_ID, CM_ISO_CODE

      END --ELSE
                   
END -- CageGetGlobalReportData
GO
GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO
-- /*******************************************************************************/

 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCreateMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCreateMeters]
GO

CREATE PROCEDURE [dbo].[CageCreateMeters]
        @pSourceTargetId BIGINT
      , @pConceptId BIGINT
      , @pCageSessionId BIGINT = NULL
      , @CreateMetersOption INT
      , @CurrencyForced VARCHAR(3) =  NULL
  AS
BEGIN   

                -- @@CreateMetersOption  =    0 - Create in cage_meters and cage_session_meters
                --                       =    1 - Create in cage_meters only
                --                       =    2 - Create in cage_session_meters only 
                --                       =    3 - Create in cage_meters and all opened cage sessions 


                DECLARE @Currencies VARCHAR(100)
                DECLARE @OnlyNationalCurrency BIT
                DECLARE @CageSessionId BIGINT
                
                SET @OnlyNationalCurrency = 0
                
                SELECT @OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY 
                  FROM CAGE_SOURCE_TARGET_CONCEPTS 
                 WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetId
                   AND  CSTC_CONCEPT_ID = @pConceptId  

                -- Get national currency
                IF @OnlyNationalCurrency = 1 AND @CurrencyForced IS NULL
                               SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
         WHERE GP_GROUP_KEY = 'RegionalOptions' 
           AND GP_SUBJECT_KEY = 'CurrencyISOCode'
    
    -- Get accepted currencies
    ELSE
    BEGIN
                               IF @CurrencyForced IS NOT NULL
                                               SET @Currencies = @CurrencyForced
                               ELSE
                                               SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
                                                WHERE GP_GROUP_KEY = 'RegionalOptions' 
                                                  AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    END
    
    -- Split currencies ISO codes
   SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
                
                
                -- Create a new meters for each currency
                DECLARE @CurrentCurrency VARCHAR(3)
                
                DECLARE Curs_CageCreateMeters CURSOR FOR 
    SELECT CURRENCY_ISO_CODE
      FROM #TMP_CURRENCIES_ISO_CODES 
      
                SET NOCOUNT ON;

    OPEN Curs_CageCreateMeters

    FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
                
                               -- CAGE METERS --
                               IF @CreateMetersOption = 0 OR @CreateMetersOption = 1 OR @CreateMetersOption = 3
                               BEGIN
                                               IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
                                                                    FROM CAGE_METERS 
                                                                   WHERE CM_SOURCE_TARGET_ID = @pSourceTargetId
                                                                     AND CM_CONCEPT_ID = @pConceptId
                                                                     AND CM_ISO_CODE = @CurrentCurrency)
                                               INSERT INTO CAGE_METERS
                                                                                ( CM_SOURCE_TARGET_ID
                                                                                , CM_CONCEPT_ID
                                                                                , CM_ISO_CODE
                                                                               , CM_VALUE
                                                                                , CM_VALUE_IN
                                                                                , CM_VALUE_OUT )
                                                               VALUES
                                                                                ( @pSourceTargetId
                                                                                , @pConceptId
                                                                                , @CurrentCurrency
                                                                                , 0
                                                                                , 0
                                                                                , 0 )
                               END

                               -- CAGE SESSION METERS --
                               
                               IF (@CreateMetersOption = 0 OR @CreateMetersOption = 2) AND @pCageSessionId IS NOT NULL
                               BEGIN
                                                     IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                                                                             FROM CAGE_SESSION_METERS 
                                                                            WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                                                                              AND CSM_CONCEPT_ID = @pConceptId
                                                                              AND CSM_ISO_CODE = @CurrentCurrency
                                                                              AND CSM_CAGE_SESSION_ID = @pCageSessionId)
                                                           INSERT INTO CAGE_SESSION_METERS( CSM_CAGE_SESSION_ID
                                                                       , CSM_SOURCE_TARGET_ID
                                                                       , CSM_CONCEPT_ID
                                                                       , CSM_ISO_CODE
                                                                      , CSM_VALUE
                                                                       , CSM_VALUE_IN
                                                                       , CSM_VALUE_OUT )
                                                                                             VALUES( @pCageSessionId
                                                                                                       , @pSourceTargetId
                                                                       , @pConceptId
                                                                       , @CurrentCurrency
                                                                       , 0
                                                                       , 0
                                                                       , 0 )
                               END
                               
                               IF (@CreateMetersOption = 3)
                               BEGIN
                               
                                                     DECLARE Cur2 CURSOR FOR SELECT cgs_cage_session_id  
                                                                                   FROM cage_sessions 
                                                                                  WHERE (cgs_close_datetime IS NULL)
                                                           
                                                     OPEN Cur2
                                                     FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     WHILE @@FETCH_STATUS = 0
                                                     BEGIN
                               
                                                           
                                                           IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                                                         FROM CAGE_SESSION_METERS 
                                                        WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                                                          AND CSM_CONCEPT_ID = @pConceptId
                                                          AND CSM_ISO_CODE = @CurrentCurrency
                                                          AND CSM_CAGE_SESSION_ID = @CageSessionId)
                                               INSERT INTO CAGE_SESSION_METERS( CSM_CAGE_SESSION_ID
                                                                               , CSM_SOURCE_TARGET_ID
                                                                               , CSM_CONCEPT_ID
                                                                               , CSM_ISO_CODE
                                                                               , CSM_VALUE
                                                                               , CSM_VALUE_IN
                                                                               , CSM_VALUE_OUT )
                                                                                                          VALUES( @CageSessionId
                                                                               , @pSourceTargetId
                                                                               , @pConceptId
                                                                               , @CurrentCurrency
                                                                               , 0
                                                                               , 0
                                                                               , 0 )
                                                                               
                                                           FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     END
                                                           
                                                     CLOSE Cur2
                                                     DEALLOCATE Cur2                       
                               END
                               
                               FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency
                
                END

    CLOSE Curs_CageCreateMeters
    DEALLOCATE Curs_CageCreateMeters
              
END -- CageCreateMeters

GO
GRANT EXECUTE ON [dbo].[CageCreateMeters] TO [wggui] WITH GRANT OPTION 
GO

