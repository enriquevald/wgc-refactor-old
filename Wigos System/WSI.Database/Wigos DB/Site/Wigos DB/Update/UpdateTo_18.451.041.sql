﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 450;

SET @New_ReleaseId = 451;

SET @New_ScriptName = N'2018-06-25 - UpdateTo_18.451.041.sql';
SET @New_Description = N'New release v03.008.0020'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/

/************************************* begin  BUG33206-WIGOS13055-ADD_SEATS.SQL ********************************/
--- Actualizar registro de Gaming Tables ---
  DECLARE @AccountId 		   AS BIGINT
	DECLARE @SiteId            AS INT
	DECLARE @NewTrackData      AS NVARCHAR(50)
	DECLARE @addXML 		   AS NVARCHAR(Max)
	DECLARE @newXML 		   AS NVARCHAR(Max)
	DECLARE @externalId        AS NVARCHAR(50)
	DECLARE @terminalId        AS INT
	DECLARE @isoCode		   AS NVARCHAR(50)
	
	SET @addXML = N'     <GamingSeatProperties>
							<RelativeLocationX>-321</RelativeLocationX>
							<RelativeLocationY>-78</RelativeLocationY>
							<Enabled>true</Enabled>
							<SeatPosition>1</SeatPosition>
						</GamingSeatProperties>
					</TableDesign>'
					
	SELECT   @SiteId = CAST(GP_KEY_VALUE AS INT) 
	  FROM   GENERAL_PARAMS 
	 WHERE   GP_GROUP_KEY   = N'Site' 
	   AND   GP_SUBJECT_KEY = N'Identifier'
	   AND   ISNUMERIC(GP_KEY_VALUE) = 1

  IF ( ISNULL(@SiteId, -1) >= 0 ) 
  BEGIN

	  IF @SiteId <= 999
    BEGIN 
	    SELECT @isoCode = GP_KEY_VALUE 
	      FROM GENERAL_PARAMS 
	     WHERE GP_GROUP_KEY = N'RegionalOptions' 
	       AND GP_SUBJECT_KEY = N'CurrencyISOCode'

	    DECLARE @gt_gaming_table_id AS INT
	    DECLARE @gt_design_xml AS XML
	    DECLARE @gt_provider_id AS INT
	    DECLARE cursor_gaming_tables CURSOR FOR 
		    SELECT gt_gaming_table_id, gt_design_xml, gt_provider_id
		    FROM   GAMING_TABLES 
		    WHERE  gt_num_seats = 0	
	
	    OPEN cursor_gaming_tables
	    FETCH NEXT FROM cursor_gaming_tables INTO @gt_gaming_table_id, @gt_design_xml, @gt_provider_id
	    WHILE @@FETCH_STATUS = 0
	    BEGIN	
		    BEGIN TRY
			    BEGIN TRANSACTION			
			    --- CREAR ACCOUNT -------------	    		
			    UPDATE   SEQUENCES 
			       SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
			     WHERE   SEQ_ID         = 4

			    IF @@ROWCOUNT = 0 
			    BEGIN
				    RAISERROR ('Sequence #4 does not exist!', 20, 0) WITH LOG
			    END

			    SELECT @AccountId = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4

			    SET @AccountId = 1000000 + @AccountId * 1000 + @SiteId % 1000
			    SET @NewTrackData = N'00000000000000000000-RECYCLED-VIRTUAL-' + CAST(@AccountId AS VARCHAR(MAX))
			    INSERT INTO ACCOUNTS (AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) VALUES (@AccountId, 5, 0, @NewTrackData)
			    -------------------------------
			    --- CREAR TERMINAL ------------ 
			    SET @externalId = N'GT' + RIGHT('000' + CAST(@gt_gaming_table_id as varchar(max)),3) + '-Seat01'
			    INSERT INTO TERMINALS (TE_TYPE,TE_EXTERNAL_ID, TE_BLOCKED, TE_ACTIVE, TE_PROVIDER_ID, TE_TERMINAL_TYPE,
								       TE_STATUS, TE_PROV_ID, TE_BANK_ID, TE_GAME_TYPE, TE_SAS_FLAGS,
								       TE_CONTRACT_TYPE, TE_WXP_REPORTED, TE_WXP_REPORTED_STATUS, TE_SEQUENCE_ID, TE_VIRTUAL_ACCOUNT_ID,
								       TE_SAS_FLAGS_USE_SITE_DEFAULT, TE_METER_DELTA_ID, TE_MASTER_ID, TE_CHANGE_ID,
								       TE_BASE_NAME, TE_TRANSFER_STATUS, TE_LAST_GAME_PLAYED_ID, TE_COIN_COLLECTION, TE_ISO_CODE,
								       TE_TITO_HOST_ID, TE_CREATION_DATE) 
						       VALUES (1,@externalId,0,1,(SELECT PV_NAME FROM PROVIDERS WHERE PV_ID = @gt_provider_id),107,0,@gt_provider_id,1,0,0,0,0,0,0,
								       @AccountId,-1,0,(IDENT_CURRENT('Terminals')),0,@externalId,0,0,0,@isoCode,0,SYSDATETIME())
			    SET @terminalId = @@IDENTITY
			    -------------------------------
			    --- CREAR SEATS ---------------
			    INSERT INTO GT_SEATS (GTS_GAMING_TABLE_ID,GTS_ENABLED,GTS_TERMINAL_ID,GTS_ACCOUNT_ID,GTS_ENABLED_DATE,GTS_POSITION) 
						      VALUES (@gt_gaming_table_id,1,@terminalId,@AccountId,SYSDATETIME(),1)
			    -------------------------------
			    --- ACTUALIZAR GAMING_TABLE ---
			    SET @newXML = CAST(@gt_design_xml AS NVARCHAR(MAX))
			    SET @newXML = REPLACE(@newXML, N'</TableDesign>',@addXML)
			
			    UPDATE GAMING_TABLES 
			    SET    gt_num_seats = 1, gt_design_xml = CAST(@newXML AS XML)
			    WHERE  gt_gaming_table_id = @gt_gaming_table_id
			    -------------------------------
			    COMMIT	
			
		    END TRY
		    BEGIN CATCH
			    ROLLBACK
		    END CATCH
		    FETCH NEXT FROM cursor_gaming_tables INTO @gt_gaming_table_id, @gt_design_xml, @gt_provider_id
	    END
	    CLOSE cursor_gaming_tables
	    DEALLOCATE cursor_gaming_tables
    END
END
GO

/************************************* end    BUG33206-WIGOS13055-ADD_SEATS.SQL ********************************/


/******* PROCEDURES *******/


/******* TRIGGERS *******/



