/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 339;

SET @New_ReleaseId = 340;
SET @New_ScriptName = N'UpdateTo_18.340.041.sql';
SET @New_Description = N'Recepción Mejoras II'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

-- Recepcion - 001 - AddColumn_CUSTOMER_ENTRANCES -----------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[customer_entrances]') and name = 'cue_price_description')
ALTER TABLE [dbo].[customer_entrances] 
	ADD [cue_price_description] nvarchar(MAX) NULL;

GO

-- Recepcion - 002 - Update_CustomerEntrancePriceDescription ------------------------------------------------------------------------

UPDATE customer_entrances 
SET customer_entrances.cue_price_description = customer_entrances_prices.cuep_description
FROM customer_entrances
   INNER JOIN customer_entrances_prices ON customer_entrances.cue_ticket_entry_price_paid = customer_entrances_prices.cuep_price 
GO


-- Recepcion - 003 - VIEW_TotalEntrancesByVisits ------------------------------------------------------------------------------------
IF EXISTS( SELECT 1 FROM sys.objects WHERE NAME = 'v_TotalEntrancesByVisits')
  DROP VIEW v_TotalEntrancesByVisits
GO

CREATE VIEW [dbo].[v_TotalEntrancesByVisits]
AS
SELECT     TOP (100) PERCENT dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY, dbo.v_TotalVisitsByDay.TotalByDay AS SQL_COLUMN_VISIT_NUMBER, 
                      --dbo.accounts.ac_holder_name AS SQL_COLUMN_HOLDER_NAME, 
                      dbo.accounts.AC_HOLDER_NAME3 SQL_COLUMN_NAME,

                       CASE WHEN dbo.accounts.ac_holder_name1 IS NULL THEN '' ELSE ac_holder_name1 END
                       + 
                       CASE WHEN dbo.accounts.ac_holder_name2 IS NOT NULL AND ac_holder_name1 IS NOT NULL THEN ' ' ELSE '' END
                       +
                       CASE WHEN dbo.accounts.ac_holder_name2 IS NULL THEN '' ELSE ac_holder_name2 END AS SQL_COLUMN_AC_HOLDER_LAST_NAME,

                      dbo.TrackDataToExternal(dbo.accounts.ac_track_data) AS SQL_COLUMN_CARD_TRACK, 
                      COUNT(dbo.customer_entrances.cue_visit_id) AS SQL_COLUMN_ENTRIES, 
                      dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_THEORICAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay AS SQL_COLUMN_REAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_DIFFERENCE_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS SQL_COLUMN_THEORICAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_DIFFERENCE,
                      dbo.accounts.ac_holder_name AS SQL_COLUMN_HOLDER_NAME, 
                      STUFF(
						(SELECT customer_entrances.cue_price_description + '; '
						FROM customer_entrances
						WHERE  customer_entrances.cue_visit_id = customer_visits.cut_visit_id
						FOR XML PATH ('')), 1, 0, '')  AS SQL_COLUMN_PRICE_DESCRIPTION
FROM         dbo.accounts INNER JOIN
                      dbo.customer_visits ON dbo.accounts.ac_account_id = dbo.customer_visits.cut_customer_id INNER JOIN
                      dbo.v_TotalVisitsByDay ON dbo.customer_visits.cut_gaming_day = dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.v_TotalVisitsByDay.TotalByDay, 
                      dbo.accounts.AC_HOLDER_NAME3,
                      CASE WHEN dbo.accounts.ac_holder_name1 IS NULL THEN '' ELSE ac_holder_name1 END
                      + 
                      CASE WHEN dbo.accounts.ac_holder_name2 IS NOT NULL AND ac_holder_name1 IS NOT NULL THEN ' ' ELSE '' END
                      +
                      CASE WHEN dbo.accounts.ac_holder_name2 IS NULL THEN '' ELSE ac_holder_name2 END,  
                      dbo.TrackDataToExternal(dbo.accounts.ac_track_data), 
                      dbo.v_TotalVisitsByDay.TotalTeoricByDay, dbo.v_TotalVisitsByDay.TotalRealByDay, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay, 
                      dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY,
                      dbo.accounts.ac_holder_name,
					  customer_visits.cut_visit_id
ORDER BY dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY DESC

GO
 
 

-- Recepcion - 004 - VIEW_TotalEntriesByMonth ----------------------------------------------------------------------------------------
 IF EXISTS( SELECT 1 FROM sys.objects WHERE NAME = 'v_TotalEntriesByMonth')
   DROP VIEW v_TotalEntriesByMonth
GO

CREATE VIEW [dbo].[v_TotalEntriesByMonth]
AS
SELECT     TOP (100) PERCENT dbo.customer_visits.cut_gaming_day AS SQL_COLUMN_GAMING_DAY, COUNT(DISTINCT dbo.customer_visits.cut_visit_id) 
                      AS SQL_COLUMN_VISIT_NUMBER, '""' AS Col1, '""' AS Col2, '""' AS Col21, 
                      0 AS Col3, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS SQL_COLUMN_THEORICAL_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS SQL_COLUMN_DIFFERENCE_BY_DAY, 
                      COUNT(dbo.customer_entrances.cue_cashier_user_id) AS SQL_COLUMN_TOTAL_ENTRIES,
                      '""' AS Col22


FROM         dbo.customer_visits INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.customer_visits.cut_gaming_day
ORDER BY SQL_COLUMN_GAMING_DAY DESC

GO


-- Recepcion - 005- SP_ReceptionCustomerVisits ----------------------------------------------------------------------------------------
IF EXISTS( SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'ReceptionCustomerVisits')
    DROP PROCEDURE ReceptionCustomerVisits
GO

CREATE PROCEDURE [dbo].[ReceptionCustomerVisits] @pStartDate BIGINT
AS BEGIN

       SELECT                                                                                                                          
                ACCOUNT_PHOTO.APH_PHOTO                                                                                                
               --,ACCOUNTS.AC_HOLDER_NAME   
               ,ACCOUNTS.AC_HOLDER_NAME3

               ,CASE WHEN ac_holder_name1 IS NULL THEN '' ELSE ac_holder_name1 END
                + 
                CASE WHEN ac_holder_name2 IS NOT NULL AND ac_holder_name1 IS NOT NULL THEN ' ' ELSE '' END
                +
                CASE WHEN ac_holder_name2 IS NULL THEN '' ELSE ac_holder_name2 END AS AC_HOLDER_NAME12
               
               ,CAST(CUSTOMER_VISITS.CUT_GENDER AS VARCHAR) CUT_GENDER                                                                 
               ,ACCOUNTS.AC_HOLDER_BIRTH_DATE                                                                                          
               ,ADIC_DATA.CUE_DOCUMENT_TYPE                                                                                            
               ,ADIC_DATA.CUE_DOCUMENT_NUMBER                                                                                          
               ,CAST(ACCOUNTS.AC_HOLDER_LEVEL AS VARCHAR)   AC_HOLDER_LEVEL                                                            
               ,ADIC_DATA.ENTRANCES_COUNT                                                                                              
               ,ADIC_DATA.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                              
               ,ADIC_DATA.CUE_ENTRANCE_DATETIME
               ,ACCOUNTS.AC_ACCOUNT_ID                                                                                        
                                                                                                                                       
         FROM   CUSTOMER_VISITS                                                                                                        
   INNER JOIN  ACCOUNTS ON     CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNTS.AC_ACCOUNT_ID                                                
    LEFT JOIN  ACCOUNT_PHOTO ON    CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNT_PHOTO.APH_ACCOUNT_ID                                      
                                                                                                                                       
                                                                                                                                       
   INNER JOIN                                                                                                                          
               (                                                                                                                       
                  /* datos de la ultima entrada de cada una de esas visitas de esa jornada*/                                           
                  SELECT                                                                                                               
                           CUSTOMER_ENTRANCES.CUE_VISIT_ID                                                                             
                          ,CAST(CUSTOMER_ENTRANCES.CUE_DOCUMENT_TYPE AS VARCHAR) CUE_DOCUMENT_TYPE                                     
                          ,CUSTOMER_ENTRANCES.CUE_DOCUMENT_NUMBER                                                                      
                          ,CUSTOMER_ENTRANCES.CUE_TICKET_ENTRY_PRICE_PAID                                                              
                          ,Z.ENTRANCES_COUNT                                                                                           
                          ,Z.CUE_ENTRANCE_DATETIME                                                                                     
                          ,Z.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                           
                    FROM                                                                                                               
                          CUSTOMER_ENTRANCES                                                                                           
              INNER JOIN (                                                                                                             
                           SELECT                                                                                                      
                                    MAX(CUE_ENTRANCE_DATETIME) AS CUE_ENTRANCE_DATETIME                                                
                                   ,CUE_VISIT_ID                                                                                       
                                   ,COUNT(*) AS ENTRANCES_COUNT                                                                        
                                   ,SUM(CUE_TICKET_ENTRY_PRICE_PAID) AS SUM_CUE_TICKET_ENTRY_PRICE_PAID                                
                             FROM                                                                                                      
                                   CUSTOMER_ENTRANCES                                                                                  
                            WHERE                                                                                                      
                                   CUSTOMER_ENTRANCES.CUE_VISIT_ID IN (                                                                
                                                                       SELECT                                                          
                                                                               CUT_VISIT_ID                                            
                                                                         FROM                                                          
                                                                               CUSTOMER_VISITS                                         
                                                                        WHERE                                                          
                                                                               CUT_GAMING_DAY = @pStartDate /* visitas de la jornada*/ 
                                                                      )                                                                
                           GROUP BY CUSTOMER_ENTRANCES.CUE_VISIT_ID  /* ultima entrada de cada una de esas visitas*/                   
                           ) Z                                                                                                         
                      ON CUSTOMER_ENTRANCES.CUE_ENTRANCE_DATETIME = Z.CUE_ENTRANCE_DATETIME                                            
                         AND CUSTOMER_ENTRANCES.CUE_VISIT_ID = Z.CUE_VISIT_ID                                                          
               ) ADIC_DATA                                                                                                             
                                                                                                                                       
           ON CUSTOMER_VISITS.CUT_VISIT_ID = ADIC_DATA.CUE_VISIT_ID                                                                    
                                                                                                                                       
        WHERE  CUT_GAMING_DAY = @pStartDate                                                                                            
        ORDER BY ADIC_DATA.CUE_ENTRANCE_DATETIME                                                                                       

END

GO

GRANT EXECUTE ON RECEPTIONCUSTOMERVISITS TO WGGUI WITH GRANT OPTION

GO



-- Recepcion - 006 - Add Phonetic Search To Blacklist -------------------------------------------------------------------------------------------

if EXISTS(SELECT * FROM sys.indexes
            WHERE name = 'IX_blacklist_file_imported_mklastname1' and Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  drop index [IX_blacklist_file_imported_mklastname1] on blacklist_file_imported
end
go
if EXISTS(SELECT * FROM sys.indexes
            WHERE name = 'IX_blacklist_file_imported_mklastname2' and Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  drop index [IX_blacklist_file_imported_mklastname2] on blacklist_file_imported
end
go
if EXISTS(SELECT * FROM sys.indexes
            WHERE name = 'IX_blacklist_file_imported_mkname' and Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  drop index [IX_blacklist_file_imported_mkname] on blacklist_file_imported
end
go
if EXISTS(SELECT * FROM sys.indexes
            WHERE name = 'IX_blacklist_file_imported_mkmiddelname' and Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  drop index IX_blacklist_file_imported_mkmiddelname on blacklist_file_imported
end
go

IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'blkf_metaphone_key_name' AND Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  alter table blacklist_file_imported 
  drop column blkf_metaphone_key_name
end
go
IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'blkf_metaphone_key_middle_name' AND Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  alter table blacklist_file_imported 
  drop column [blkf_metaphone_key_middle_name]
end
go
IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'blkf_metaphone_key_lastname_1' AND Object_ID = Object_ID(N'blacklist_file_imported'))

begin
  alter table blacklist_file_imported 
  drop column [blkf_metaphone_key_lastname_1]
end
go
IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'blkf_metaphone_key_lastname_2' AND Object_ID = Object_ID(N'blacklist_file_imported'))
begin
  alter table blacklist_file_imported 
  drop column [blkf_metaphone_key_lastname_2]
end
go

IF EXISTS(SELECT * FROM sys.objects where  object_id = object_id('Metaphone'))
begin 
drop function Metaphone
end
go

CREATE FUNCTION DBO.METAPHONE(@STRIN NVARCHAR(50)) RETURNS NVARCHAR(50)
WITH SCHEMABINDING AS
BEGIN
DECLARE @STR NVARCHAR(50),
	@RESULT NVARCHAR(50),
	@STR3	NCHAR(3),
	@STR2 	NCHAR(2),
	@STR1 	NCHAR(1),
	@STRP 	NCHAR(1),
	@STRLEN TINYINT,
	@CNT 	TINYINT

 SET @STR = @STRIN COLLATE SQL_LATIN1_GENERAL_CP1251_CS_AS 
SET 	@STRLEN = 	LEN(@STRIN)
SET	@CNT	=	1
SET	@RESULT	=	''

--PROCESS BEGINNING EXCEPTIONS
SET 	@STR2 	= 	LEFT(@STR,2)
IF 	@STR2 	IN 	('AE', 'GN', 'KN', 'PN', 'WR')
	BEGIN
		SET 	@STR 	= 	RIGHT(@STR , @STRLEN - 1)
		SET 	@STRLEN = 	@STRLEN - 1
	END
IF	@STR2 	= 	'WH'	
	BEGIN
		SET 	@STR 	= 	'W' + RIGHT(@STR , @STRLEN - 2)
		SET 	@STRLEN = 	@STRLEN - 1
	END
SET 	@STR1 	= 	LEFT(@STR,1)
IF 	@STR1	= 	'X' 	
	BEGIN
		SET 	@STR 	= 	'S' + RIGHT(@STR , @STRLEN - 1)
	END
IF 	@STR1	IN 	('A','E','I','O','U')
	BEGIN
		SET 	@STR 	= 	RIGHT(@STR , @STRLEN - 1)
		SET 	@STRLEN = 	@STRLEN - 1
		SET	@RESULT	=	@STR1
	END

WHILE @CNT <= @STRLEN
	BEGIN
		SET 	@STR1 	= 	SUBSTRING(@STR,@CNT,1)
		IF 	@CNT 	<> 	1	
			SET	@STRP	=	SUBSTRING(@STR,(@CNT-1),1)
		ELSE	SET	@STRP	=	' '
	
	IF @STRP	<> 	@STR1
		BEGIN
			SET 	@STR2 	= 	SUBSTRING(@STR,@CNT,2)
				
			IF  	@STR1	IN	('F','J','L','M','N','R')	
				SET	@RESULT	=	@RESULT + @STR1
	
			IF  	@STR1	=	'Q'	SET @RESULT	=	@RESULT + 'K'
			IF  	@STR1	=	'V'	SET @RESULT	=	@RESULT + 'F'
			IF  	@STR1	=	'X'	SET @RESULT	=	@RESULT + 'KS'
			IF  	@STR1	=	'Z'	SET @RESULT	=	@RESULT + 'S'
	
			IF 	@STR1	=	'B'
				IF @CNT = @STRLEN
					IF 	SUBSTRING(@STR,(@CNT - 1),1) <> 'M'
						SET	@RESULT	=	@RESULT + 'B'
				ELSE
					SET	@RESULT	=	@RESULT + 'B'
	
			IF 	@STR1	=	'C'

				IF 	@STR2 	= 	'CH' 	OR 	SUBSTRING(@STR,@CNT,3) 	= 	'CIA'
					SET	@RESULT	=	@RESULT + 'X'
				ELSE
					IF 	@STR2	IN	('CI','CE','CY')	AND	@STRP	<>	'S'
						SET	@RESULT	=	@RESULT + 'S'
					ELSE	SET	@RESULT	=	@RESULT + 'K'
	
			IF 	@STR1	=	'D'
				IF 	SUBSTRING(@STR,@CNT,3) 	IN  	('DGE','DGY','DGI')
					SET	@RESULT	=	@RESULT + 'J'
				ELSE	SET	@RESULT	=	@RESULT + 'T'
	
			IF 	@STR1	=	'G'
				IF 	SUBSTRING(@STR,(@CNT - 1),3) NOT IN ('DGE','DGY','DGI','DHA','DHE','DHI','DHO','DHU')
					IF  @STR2 IN ('GI', 'GE','GY')
						SET	@RESULT	=	@RESULT + 'J'
					ELSE
						IF	(@STR2	<>	'GN') OR ((@STR2	<> 'GH') AND ((@CNT + 1) <> @STRLEN))
							SET	@RESULT	=	@RESULT + 'K'
	
			IF  	@STR1	=	'H'
				IF 	(@STRP NOT IN ('A','E','I','O','U')) AND (@STR2 NOT IN ('HA','HE','HI','HO','HU'))
					IF	@STRP NOT IN ('C','S','P','T','G')
						SET	@RESULT	=	@RESULT + 'H'
	
			IF  	@STR1	=	'K'	
				IF @STRP <> 'C'
					SET	@RESULT	=	@RESULT + 'K'
	
			IF  	@STR1	=	'P'	
				IF @STR2 = 'PH'
					SET	@RESULT	=	@RESULT + 'F'
				ELSE
					SET	@RESULT	=	@RESULT + 'P'
	
			IF 	@STR1	=	'S'
				IF 	SUBSTRING(@STR,@CNT,3) 	IN  	('SIA','SIO') OR @STR2 = 'SH'
					SET	@RESULT	=	@RESULT + 'X'
				ELSE	SET	@RESULT	=	@RESULT + 'S'
	
			IF 	@STR1	=	'T'
				IF 	SUBSTRING(@STR,@CNT,3) 	IN  	('TIA','TIO')
					SET	@RESULT	=	@RESULT + 'X'
				ELSE	
					IF	@STR2	=	'TH'
						SET	@RESULT	=	@RESULT + '0'
					ELSE
						IF SUBSTRING(@STR,@CNT,3) <> 'TCH'
							SET	@RESULT	=	@RESULT + 'T'
	
			IF 	@STR1	=	'W'
				IF @STR2 NOT IN('WA','WE','WI','WO','WU')
					SET	@RESULT	=	@RESULT + 'W'
	
			IF 	@STR1	=	'Y'
				IF @STR2 NOT IN('YA','YE','YI','YO','YU')
					SET	@RESULT	=	@RESULT + 'Y'
		END
		SET 	@CNT	=	@CNT + 1
	END

  IF(LEN(LTRIM(RTRIM(@RESULT)))=0)
  BEGIN
    RETURN @STRIN
  END

RETURN @RESULT
END
GO

ALTER TABLE BLACKLIST_FILE_IMPORTED
    add [blkf_metaphone_key_name]      AS             ([dbo].[Metaphone]([blkf_name])) PERSISTED
GO

	ALTER TABLE BLACKLIST_FILE_IMPORTED
    add [blkf_metaphone_key_middle_name]      AS             ([dbo].[Metaphone]([blkf_middle_name])) PERSISTED
GO

	ALTER TABLE BLACKLIST_FILE_IMPORTED
    add [blkf_metaphone_key_lastname_1]      AS             ([dbo].[Metaphone]([blkf_lastname_1])) PERSISTED
GO

	ALTER TABLE BLACKLIST_FILE_IMPORTED
    add [blkf_metaphone_key_lastname_2]      AS             ([dbo].[Metaphone]([blkf_lastname_2])) PERSISTED
GO 

CREATE NONCLUSTERED INDEX [IX_blacklist_file_imported_mklastname1]
    ON [dbo].[blacklist_file_imported]([blkf_metaphone_key_lastname_1] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_blacklist_file_imported_mklastname2]
    ON [dbo].[blacklist_file_imported]([blkf_metaphone_key_lastname_2] ASC);
GO



CREATE NONCLUSTERED INDEX IX_blacklist_file_imported_mkmiddelname
    ON [dbo].[blacklist_file_imported]([blkf_metaphone_key_middle_name] ASC);

GO


CREATE NONCLUSTERED INDEX [IX_blacklist_file_imported_mkname]
    ON [dbo].[blacklist_file_imported]([blkf_metaphone_key_name] ASC);
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY='BlackListService' AND GP_SUBJECT_KEY='PhoneticSearchMode')
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES('BlackListService','PhoneticSearchMode',0)
END
GO

