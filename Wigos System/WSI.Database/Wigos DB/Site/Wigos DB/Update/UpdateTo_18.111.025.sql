/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 110;

SET @New_ReleaseId = 111;
SET @New_ScriptName = N'UpdateTo_18.111.025.sql';
SET @New_Description = N'Operations Schedule and MultiSite DB elements.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


--
-- OPERATIONS_SCHEDULE
--

DECLARE @operations_schedule_enabled AS INT
DECLARE @closing_time AS INT

SELECT @operations_schedule_enabled = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'OperationsSchedule' AND GP_SUBJECT_KEY = 'Enabled'
SET @operations_schedule_enabled = ISNULL(@operations_schedule_enabled, 0)

IF @operations_schedule_enabled = 0 
BEGIN
  SELECT @closing_time = CAST(CASE WHEN GP_KEY_VALUE = '' THEN 0 ELSE GP_KEY_VALUE END AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY = 'ClosingTime'
  SET @closing_time = ISNULL(@closing_time * 60, 0)
  UPDATE   OPERATIONS_SCHEDULE
     SET   OS_DATE_FROM  = NULL
         , OS_DATE_TO    = NULL
         , OS_OPERATIONS_ALLOWED = 'True'
         , OS_TIME1_FROM = @closing_time
         , OS_TIME1_TO   = @closing_time
         , OS_TIME2_FROM = NULL
         , OS_TIME2_TO   = NULL
   WHERE   OS_TYPE       = 0
END
GO

--
-- GENERAL_PARAMS
--

UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '' 
 WHERE   GP_GROUP_KEY   = 'OperationsSchedule' 
   AND   GP_SUBJECT_KEY = 'MailingList' 
   AND   GP_KEY_VALUE   = '0'
GO

--
-- WWP_STATUS
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wwp_status]') AND type in (N'U'))
DROP TABLE [dbo].[wwp_status]

CREATE TABLE [dbo].[wwp_status](
      [wwp_type] [int] NOT NULL,
      [wwp_status] [int] NOT NULL CONSTRAINT [DF_wwp_status_wwp_status]  DEFAULT ((0)),
      [wwp_status_changed] [datetime] NULL,
      [wwp_last_address] [nvarchar](500) NULL,
      [wwp_sent_bytes] [bigint] NOT NULL CONSTRAINT [DF_wwp_status_wwp_sent_bytes]  DEFAULT ((0)),
      [wwp_sent_messages] [bigint] NOT NULL CONSTRAINT [DF_wwp_status_wwp_sent_messages]  DEFAULT ((0)),
      [wwp_received_bytes] [bigint] NOT NULL CONSTRAINT [DF_wwp_status_wwp_received_bytes]  DEFAULT ((0)),
      [wwp_received_messages] [bigint] NOT NULL CONSTRAINT [DF_wwp_status_wwp_received_messages]  DEFAULT ((0)),
      [wwp_last_sent_msg] [datetime] NULL,
      [wwp_last_received_msg] [datetime] NULL,
CONSTRAINT [PK_wwp_status] PRIMARY KEY CLUSTERED 
(
      [wwp_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO WWP_STATUS (WWP_TYPE, WWP_STATUS) VALUES (1, 0)  -- MULTISITE,
INSERT INTO WWP_STATUS (WWP_TYPE, WWP_STATUS) VALUES (2, 0)  -- SUPERCENTER

GO

--
-- GUI_PROFILE_FORMS
--

DELETE  GUI_PROFILE_FORMS
WHERE   GPF_FORM_ID = 136
   AND  GPF_GUI_ID  = 200
   
DELETE  GUI_FORMS
WHERE   GF_FORM_ID = 136
   AND  GF_GUI_ID  = 200

GO

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'VoucherOnPromotionAwarded.HidePrizeCoupon', '0')

GO

--
-- ACCOUNTS IDENTITY OFF
--

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plays_accounts]') AND parent_object_id = OBJECT_ID(N'[dbo].[plays]'))
ALTER TABLE [dbo].[plays] DROP CONSTRAINT [FK_plays_accounts]
GO

BEGIN TRANSACTION
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_cash_in
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_cash_won
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_not_redeemable
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_total_cash_in
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_total_cash_out
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_initial_cash_in
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_activated
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_deposit
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_user_type
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_points
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_initial_not_redeemable
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_created
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_limit
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_creation
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_expiration
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_last_activity
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_draw_last_session_id
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_draw_last_remainder
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_nr_won_lock
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_holder_level
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_card_paid
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_ac_block_reason
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_holder_level_entered
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_holder_level_notify
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_re_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_re_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_promo_nr_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_played
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_won
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_re_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_nr_balance
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_to_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_re_to_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_nr_to_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_from_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_re_from_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_nr_from_gm
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_played
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_nr_played
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_won
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_nr_won
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_re_cancellable
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_re_cancellable
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_promo_nr_cancellable
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_in_session_cancellable_transaction_id
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_holder_id_type
GO
ALTER TABLE dbo.accounts
	DROP CONSTRAINT DF_accounts_ac_points_status
GO
CREATE TABLE dbo.Tmp_accounts
	(
	ac_account_id bigint NOT NULL,
	ac_type int NOT NULL,
	ac_holder_name nvarchar(200) NULL,
	ac_blocked bit NOT NULL,
	ac_not_valid_before datetime NULL,
	ac_not_valid_after datetime NULL,
	ac_balance money NOT NULL,
	ac_cash_in money NOT NULL,
	ac_cash_won money NOT NULL,
	ac_not_redeemable money NOT NULL,
	ac_timestamp timestamp NULL,
	ac_track_data nvarchar(50) NULL,
	ac_total_cash_in money NOT NULL,
	ac_total_cash_out money NOT NULL,
	ac_initial_cash_in money NOT NULL,
	ac_activated bit NOT NULL,
	ac_deposit money NOT NULL,
	ac_current_terminal_id int NULL,
	ac_current_terminal_name nvarchar(50) NULL,
	ac_current_play_session_id bigint NULL,
	ac_last_terminal_id int NULL,
	ac_last_terminal_name nvarchar(50) NULL,
	ac_last_play_session_id bigint NULL,
	ac_user_type int NULL,
	ac_points money NULL,
	ac_initial_not_redeemable money NOT NULL,
	ac_created datetime NOT NULL,
	ac_promo_balance money NOT NULL,
	ac_promo_limit money NOT NULL,
	ac_promo_creation datetime NOT NULL,
	ac_promo_expiration datetime NOT NULL,
	ac_last_activity datetime NOT NULL,
	ac_holder_id nvarchar(20) NULL,
	ac_holder_address_01 nvarchar(50) NULL,
	ac_holder_address_02 nvarchar(50) NULL,
	ac_holder_address_03 nvarchar(50) NULL,
	ac_holder_city nvarchar(50) NULL,
	ac_holder_zip nvarchar(10) NULL,
	ac_holder_email_01 nvarchar(50) NULL,
	ac_holder_email_02 nvarchar(50) NULL,
	ac_holder_phone_number_01 nvarchar(20) NULL,
	ac_holder_phone_number_02 nvarchar(20) NULL,
	ac_holder_comments nvarchar(100) NULL,
	ac_holder_gender int NULL,
	ac_holder_marital_status int NULL,
	ac_holder_birth_date datetime NULL,
	ac_draw_last_play_session_id bigint NOT NULL,
	ac_draw_last_play_session_remainder money NOT NULL,
	ac_nr_won_lock money NULL,
	ac_nr_expiration datetime NULL,
	ac_cashin_while_playing money NULL,
	ac_holder_level int NOT NULL,
	ac_card_paid bit NOT NULL,
	ac_cancellable_operation_id bigint NULL,
	ac_current_promotion_id bigint NULL,
	ac_block_reason int NOT NULL,
	ac_holder_level_expiration datetime NULL,
	ac_holder_level_entered datetime NULL,
	ac_holder_level_notify int NULL,
	ac_pin nvarchar(12) NULL,
	ac_pin_failures int NULL,
	ac_holder_name1 nvarchar(50) NULL,
	ac_holder_name2 nvarchar(50) NULL,
	ac_holder_name3 nvarchar(50) NULL,
	ac_holder_id1 nvarchar(20) NULL,
	ac_holder_id2 nvarchar(20) NULL,
	ac_holder_document_id1 bigint NULL,
	ac_holder_document_id2 bigint NULL,
	ac_nr2_expiration datetime NULL,
	ac_recommended_by bigint NULL,
	ac_re_balance money NOT NULL,
	ac_promo_re_balance money NOT NULL,
	ac_promo_nr_balance money NOT NULL,
	ac_in_session_played money NOT NULL,
	ac_in_session_won money NOT NULL,
	ac_in_session_re_balance money NOT NULL,
	ac_in_session_promo_re_balance money NOT NULL,
	ac_in_session_promo_nr_balance money NOT NULL,
	ac_in_session_re_to_gm money NOT NULL,
	ac_in_session_promo_re_to_gm money NOT NULL,
	ac_in_session_promo_nr_to_gm money NOT NULL,
	ac_in_session_re_from_gm money NOT NULL,
	ac_in_session_promo_re_from_gm money NOT NULL,
	ac_in_session_promo_nr_from_gm money NOT NULL,
	ac_in_session_re_played money NOT NULL,
	ac_in_session_nr_played money NOT NULL,
	ac_in_session_re_won money NOT NULL,
	ac_in_session_nr_won money NOT NULL,
	ac_in_session_re_cancellable money NOT NULL,
	ac_in_session_promo_re_cancellable money NOT NULL,
	ac_in_session_promo_nr_cancellable money NOT NULL,
	ac_in_session_cancellable_transaction_id bigint NOT NULL,
	ac_holder_id_type int NOT NULL,
	ac_holder_id_indexed  AS (isnull((CONVERT([nvarchar],[ac_holder_id_type],0)+' - ')+[ac_holder_id],'<NULL>'+CONVERT([nvarchar],[ac_account_id],(0)))),
	ac_promo_ini_re_balance money NULL,
	ac_holder_is_vip bit NULL,
	ac_holder_title nvarchar(15) NULL,
	ac_holder_name4 nvarchar(50) NULL,
	ac_holder_wedding_date datetime NULL,
	ac_holder_phone_type_01 int NULL,
	ac_holder_phone_type_02 int NULL,
	ac_holder_state nvarchar(50) NULL,
	ac_holder_country nvarchar(50) NULL,
	ac_holder_address_01_alt nvarchar(50) NULL,
	ac_holder_address_02_alt nvarchar(50) NULL,
	ac_holder_address_03_alt nvarchar(50) NULL,
	ac_holder_city_alt nvarchar(50) NULL,
	ac_holder_zip_alt nvarchar(10) NULL,
	ac_holder_state_alt nvarchar(50) NULL,
	ac_holder_country_alt nvarchar(50) NULL,
	ac_holder_is_smoker bit NULL,
	ac_holder_nickname nvarchar(25) NULL,
	ac_holder_credit_limit money NULL,
	ac_holder_request_credit_limit money NULL,
	ac_pin_last_modified datetime NULL,
	ac_last_activity_site_id nvarchar(25) NULL,
	ac_creation_site_id nvarchar(25) NULL,
	ac_last_update_site_id nvarchar(25) NULL,
	ac_external_reference nvarchar(50) NULL,
	ac_points_status int NULL,
	ac_ms_has_local_changes bit NULL,
	ac_ms_change_guid uniqueidentifier NULL,
	ac_ms_created_on_site_id int NULL,
	ac_ms_modified_on_site_id int NULL,
	ac_ms_last_site_id int NULL,
	ac_ms_points_seq_id bigint NULL,
	ac_ms_points_synchronized datetime NULL,
	ac_ms_personal_info_seq_id bigint NULL,
	ac_ms_hash varbinary(20) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_balance DEFAULT ((0)) FOR ac_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_cash_in DEFAULT ((0)) FOR ac_cash_in
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_cash_won DEFAULT ((0)) FOR ac_cash_won
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_not_redeemable DEFAULT ((0)) FOR ac_not_redeemable
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_total_cash_in DEFAULT ((0)) FOR ac_total_cash_in
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_total_cash_out DEFAULT ((0)) FOR ac_total_cash_out
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_initial_cash_in DEFAULT ((0)) FOR ac_initial_cash_in
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_activated DEFAULT ((0)) FOR ac_activated
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_deposit DEFAULT ((0)) FOR ac_deposit
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_user_type DEFAULT ((0)) FOR ac_user_type
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_points DEFAULT ((0)) FOR ac_points
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_initial_not_redeemable DEFAULT ((0)) FOR ac_initial_not_redeemable
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_created DEFAULT (getdate()) FOR ac_created
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_balance DEFAULT ((0)) FOR ac_promo_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_limit DEFAULT ((0)) FOR ac_promo_limit
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_creation DEFAULT (getdate()) FOR ac_promo_creation
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_expiration DEFAULT (getdate()) FOR ac_promo_expiration
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_last_activity DEFAULT (getdate()) FOR ac_last_activity
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_draw_last_session_id DEFAULT ((0)) FOR ac_draw_last_play_session_id
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_draw_last_remainder DEFAULT ((0)) FOR ac_draw_last_play_session_remainder
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_nr_won_lock DEFAULT ((0)) FOR ac_nr_won_lock
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_holder_level DEFAULT ((0)) FOR ac_holder_level
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_card_paid DEFAULT ((0)) FOR ac_card_paid
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_ac_block_reason DEFAULT ((0)) FOR ac_block_reason
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_holder_level_entered DEFAULT (getdate()) FOR ac_holder_level_entered
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_holder_level_notify DEFAULT ((0)) FOR ac_holder_level_notify
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_re_balance DEFAULT ((0)) FOR ac_re_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_re_balance DEFAULT ((0)) FOR ac_promo_re_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_promo_nr_balance DEFAULT ((0)) FOR ac_promo_nr_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_played DEFAULT ((0)) FOR ac_in_session_played
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_won DEFAULT ((0)) FOR ac_in_session_won
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_balance DEFAULT ((0)) FOR ac_in_session_re_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_re_balance DEFAULT ((0)) FOR ac_in_session_promo_re_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_nr_balance DEFAULT ((0)) FOR ac_in_session_promo_nr_balance
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_to_gm DEFAULT ((0)) FOR ac_in_session_re_to_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_re_to_gm DEFAULT ((0)) FOR ac_in_session_promo_re_to_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_nr_to_gm DEFAULT ((0)) FOR ac_in_session_promo_nr_to_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_from_gm DEFAULT ((0)) FOR ac_in_session_re_from_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_re_from_gm DEFAULT ((0)) FOR ac_in_session_promo_re_from_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_nr_from_gm DEFAULT ((0)) FOR ac_in_session_promo_nr_from_gm
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_played DEFAULT ((0)) FOR ac_in_session_re_played
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_nr_played DEFAULT ((0)) FOR ac_in_session_nr_played
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_won DEFAULT ((0)) FOR ac_in_session_re_won
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_nr_won DEFAULT ((0)) FOR ac_in_session_nr_won
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_re_cancellable DEFAULT ((0)) FOR ac_in_session_re_cancellable
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_re_cancellable DEFAULT ((0)) FOR ac_in_session_promo_re_cancellable
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_promo_nr_cancellable DEFAULT ((0)) FOR ac_in_session_promo_nr_cancellable
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_in_session_cancellable_transaction_id DEFAULT ((0)) FOR ac_in_session_cancellable_transaction_id
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_holder_id_type DEFAULT ((0)) FOR ac_holder_id_type
GO
ALTER TABLE dbo.Tmp_accounts ADD CONSTRAINT
	DF_accounts_ac_points_status DEFAULT ((0)) FOR ac_points_status
GO
IF EXISTS(SELECT * FROM dbo.accounts)
	 EXEC('INSERT INTO dbo.Tmp_accounts (ac_account_id, ac_type, ac_holder_name, ac_blocked, ac_not_valid_before, ac_not_valid_after, ac_balance, ac_cash_in, ac_cash_won, ac_not_redeemable, ac_track_data, ac_total_cash_in, ac_total_cash_out, ac_initial_cash_in, ac_activated, ac_deposit, ac_current_terminal_id, ac_current_terminal_name, ac_current_play_session_id, ac_last_terminal_id, ac_last_terminal_name, ac_last_play_session_id, ac_user_type, ac_points, ac_initial_not_redeemable, ac_created, ac_promo_balance, ac_promo_limit, ac_promo_creation, ac_promo_expiration, ac_last_activity, ac_holder_id, ac_holder_address_01, ac_holder_address_02, ac_holder_address_03, ac_holder_city, ac_holder_zip, ac_holder_email_01, ac_holder_email_02, ac_holder_phone_number_01, ac_holder_phone_number_02, ac_holder_comments, ac_holder_gender, ac_holder_marital_status, ac_holder_birth_date, ac_draw_last_play_session_id, ac_draw_last_play_session_remainder, ac_nr_won_lock, ac_nr_expiration, ac_cashin_while_playing, ac_holder_level, ac_card_paid, ac_cancellable_operation_id, ac_current_promotion_id, ac_block_reason, ac_holder_level_expiration, ac_holder_level_entered, ac_holder_level_notify, ac_pin, ac_pin_failures, ac_holder_name1, ac_holder_name2, ac_holder_name3, ac_holder_id1, ac_holder_id2, ac_holder_document_id1, ac_holder_document_id2, ac_nr2_expiration, ac_recommended_by, ac_re_balance, ac_promo_re_balance, ac_promo_nr_balance, ac_in_session_played, ac_in_session_won, ac_in_session_re_balance, ac_in_session_promo_re_balance, ac_in_session_promo_nr_balance, ac_in_session_re_to_gm, ac_in_session_promo_re_to_gm, ac_in_session_promo_nr_to_gm, ac_in_session_re_from_gm, ac_in_session_promo_re_from_gm, ac_in_session_promo_nr_from_gm, ac_in_session_re_played, ac_in_session_nr_played, ac_in_session_re_won, ac_in_session_nr_won, ac_in_session_re_cancellable, ac_in_session_promo_re_cancellable, ac_in_session_promo_nr_cancellable, ac_in_session_cancellable_transaction_id, ac_holder_id_type, ac_promo_ini_re_balance, ac_holder_is_vip, ac_holder_title, ac_holder_name4, ac_holder_wedding_date, ac_holder_phone_type_01, ac_holder_phone_type_02, ac_holder_state, ac_holder_country, ac_holder_address_01_alt, ac_holder_address_02_alt, ac_holder_address_03_alt, ac_holder_city_alt, ac_holder_zip_alt, ac_holder_state_alt, ac_holder_country_alt, ac_holder_is_smoker, ac_holder_nickname, ac_holder_credit_limit, ac_holder_request_credit_limit, ac_pin_last_modified, ac_last_activity_site_id, ac_creation_site_id, ac_last_update_site_id, ac_external_reference, ac_points_status, ac_ms_has_local_changes, ac_ms_change_guid, ac_ms_created_on_site_id, ac_ms_modified_on_site_id, ac_ms_last_site_id, ac_ms_points_seq_id, ac_ms_points_synchronized, ac_ms_personal_info_seq_id, ac_ms_hash)
		SELECT ac_account_id, ac_type, ac_holder_name, ac_blocked, ac_not_valid_before, ac_not_valid_after, ac_balance, ac_cash_in, ac_cash_won, ac_not_redeemable, ac_track_data, ac_total_cash_in, ac_total_cash_out, ac_initial_cash_in, ac_activated, ac_deposit, ac_current_terminal_id, ac_current_terminal_name, ac_current_play_session_id, ac_last_terminal_id, ac_last_terminal_name, ac_last_play_session_id, ac_user_type, ac_points, ac_initial_not_redeemable, ac_created, ac_promo_balance, ac_promo_limit, ac_promo_creation, ac_promo_expiration, ac_last_activity, ac_holder_id, ac_holder_address_01, ac_holder_address_02, ac_holder_address_03, ac_holder_city, ac_holder_zip, ac_holder_email_01, ac_holder_email_02, ac_holder_phone_number_01, ac_holder_phone_number_02, ac_holder_comments, ac_holder_gender, ac_holder_marital_status, ac_holder_birth_date, ac_draw_last_play_session_id, ac_draw_last_play_session_remainder, ac_nr_won_lock, ac_nr_expiration, ac_cashin_while_playing, ac_holder_level, ac_card_paid, ac_cancellable_operation_id, ac_current_promotion_id, ac_block_reason, ac_holder_level_expiration, ac_holder_level_entered, ac_holder_level_notify, ac_pin, ac_pin_failures, ac_holder_name1, ac_holder_name2, ac_holder_name3, ac_holder_id1, ac_holder_id2, ac_holder_document_id1, ac_holder_document_id2, ac_nr2_expiration, ac_recommended_by, ac_re_balance, ac_promo_re_balance, ac_promo_nr_balance, ac_in_session_played, ac_in_session_won, ac_in_session_re_balance, ac_in_session_promo_re_balance, ac_in_session_promo_nr_balance, ac_in_session_re_to_gm, ac_in_session_promo_re_to_gm, ac_in_session_promo_nr_to_gm, ac_in_session_re_from_gm, ac_in_session_promo_re_from_gm, ac_in_session_promo_nr_from_gm, ac_in_session_re_played, ac_in_session_nr_played, ac_in_session_re_won, ac_in_session_nr_won, ac_in_session_re_cancellable, ac_in_session_promo_re_cancellable, ac_in_session_promo_nr_cancellable, ac_in_session_cancellable_transaction_id, ac_holder_id_type, ac_promo_ini_re_balance, ac_holder_is_vip, ac_holder_title, ac_holder_name4, ac_holder_wedding_date, ac_holder_phone_type_01, ac_holder_phone_type_02, ac_holder_state, ac_holder_country, ac_holder_address_01_alt, ac_holder_address_02_alt, ac_holder_address_03_alt, ac_holder_city_alt, ac_holder_zip_alt, ac_holder_state_alt, ac_holder_country_alt, ac_holder_is_smoker, ac_holder_nickname, ac_holder_credit_limit, ac_holder_request_credit_limit, ac_pin_last_modified, ac_last_activity_site_id, ac_creation_site_id, ac_last_update_site_id, ac_external_reference, ac_points_status, ac_ms_has_local_changes, ac_ms_change_guid, ac_ms_created_on_site_id, ac_ms_modified_on_site_id, ac_ms_last_site_id, ac_ms_points_seq_id, ac_ms_points_synchronized, ac_ms_personal_info_seq_id, ac_ms_hash FROM dbo.accounts WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.account_major_prizes
	DROP CONSTRAINT FK_account_major_prizes_accounts
GO
ALTER TABLE dbo.account_flags
	DROP CONSTRAINT FK_account_flags_accounts
GO
ALTER TABLE dbo.account_movements
	DROP CONSTRAINT FK_account_movements_accounts
GO
ALTER TABLE dbo.play_sessions
	DROP CONSTRAINT FK_play_sessions_accounts
GO
ALTER TABLE dbo.account_payment_orders
	DROP CONSTRAINT FK_account_payment_order_accounts
GO
DROP TABLE dbo.accounts
GO
EXECUTE sp_rename N'dbo.Tmp_accounts', N'accounts', 'OBJECT' 
GO
ALTER TABLE dbo.accounts ADD CONSTRAINT
	PK_accounts PRIMARY KEY CLUSTERED 
	(
	ac_account_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_track_data ON dbo.accounts
	(
	ac_track_data
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_ac_holder_id_indexed ON dbo.accounts
	(
	ac_holder_id_indexed
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ac_holder_level_expiration ON dbo.accounts
	(
	ac_holder_level_expiration
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ac_current_play_session_id ON dbo.accounts
	(
	ac_current_play_session_id,
	ac_last_activity
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ac_last_activity ON dbo.accounts
	(
	ac_last_activity
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** TRIGGERS ******/

--
-- MultiSite
--
CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON dbo.accounts
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            AS BIT
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN

    SET @updated = 0;  

IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') )
       FROM   INSERTED
      WHERE   AC_TRACK_DATA not like '%-NEW-%'

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
           DECLARE @new_id as uniqueidentifier
           
           SET @new_id = NEWID()
           
            -- Personal Info
            UPDATE   ACCOUNTS
               SET   AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = @new_id
             WHERE   AC_ACCOUNT_ID              = @AccountId
             
            IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
            ELSE 
              UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId
            
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO
CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountInsert] ON dbo.accounts
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence10Value AS BIGINT
    DECLARE @Sequence11Value AS BIGINT
    DECLARE @AccountId       AS BIGINT
    DECLARE @NewId as uniqueidentifier
    
    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AC_ACCOUNT_ID 
            , INSERTED.AC_MS_CHANGE_GUID            
       FROM   INSERTED
      WHERE   INSERTED.AC_TRACK_DATA NOT LIKE '%-NEW-%' 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN      
      
      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
      BEGIN
        INSERT INTO   MS_SITE_PENDING_ACCOUNTS 
                    ( SPA_ACCOUNT_ID
                    , SPA_GUID)
             VALUES ( @AccountId
                    , @NewId )
      END

      FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.account_payment_orders ADD CONSTRAINT
	FK_account_payment_order_accounts FOREIGN KEY
	(
	apo_account_id
	) REFERENCES dbo.accounts
	(
	ac_account_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.play_sessions ADD CONSTRAINT
	FK_play_sessions_accounts FOREIGN KEY
	(
	ps_account_id
	) REFERENCES dbo.accounts
	(
	ac_account_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.account_movements ADD CONSTRAINT
	FK_account_movements_accounts FOREIGN KEY
	(
	am_account_id
	) REFERENCES dbo.accounts
	(
	ac_account_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.account_flags ADD CONSTRAINT
	FK_account_flags_accounts FOREIGN KEY
	(
	af_account_id
	) REFERENCES dbo.accounts
	(
	ac_account_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.account_major_prizes ADD CONSTRAINT
	FK_account_major_prizes_accounts FOREIGN KEY
	(
	amp_account_id
	) REFERENCES dbo.accounts
	(
	ac_account_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

COMMIT

GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsInAccount.sql
--
--   DESCRIPTION: Update points of the account in site 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
--------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Update_PointsInAccount]
	@pMovementId                BIGINT
,	@pAccountId                 BIGINT
, @pErrorCode                 INT         
, @pPointsSequenceId          BIGINT      
, @pReceivedPoints            MONEY          

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN
  
  DECLARE @PrevSequence as BIGINT
  
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_POINTS = AC_POINTS + 0 WHERE AC_ACCOUNT_ID = @pAccountId

  SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  
  IF ( @PrevSequence >= @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM (am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,39,40,41,42,46,50,60,61,66) --Not included level movements
  

   	UPDATE   ACCOUNTS
	     SET   AC_POINTS                  = @pReceivedPoints + ISNULL (@LocalDelta, 0)
	         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
	         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
	   WHERE   AC_ACCOUNT_ID              = @pAccountId
	     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) < @pPointsSequenceId
END
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
GO

--SITE_TASK
DELETE MS_SITE_TASKS 
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES ( 1, 1,   60); --DownloadParameters
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (11, 1,   60); --UploadPointsMovements
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (12, 1,   60); --UploadPersonalInfo
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (21, 0,  180); --DownloadPoints_Site
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (22, 1,  180); --DownloadPoints_All 
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (31, 0,  180); --DownloadPersonalInfo_Card
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (32, 0,  180); --DownloadPersonalInfo_Site
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (33, 1,  180); --DownloadPersonalInfo_All 

GO
