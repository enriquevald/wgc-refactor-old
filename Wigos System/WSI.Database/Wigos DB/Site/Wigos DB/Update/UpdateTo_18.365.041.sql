/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 364;

SET @New_ReleaseId = 365;
SET @New_ScriptName = N'UpdateTo_18.365.041.sql';
SET @New_Description = N'JOB RESTART TERMINALS'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

-- TITO - Tickets.MinAllowedTicketIn - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinAllowedTicketIn')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.MinAllowedTicketIn', '0')
GO

-- TITO - Tickets.AllowTruncate - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.AllowTruncate', '0')
GO

-- CFDI - LastOperationId - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'LastOperationId')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'LastOperationId', '0')
GO

-- CFDI - Enabled - 0
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Enabled' )
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Enabled', '0')
GO

-- CFDI - Uri - http://201.99.72.63/appConstancias/Ws_CertConstRetenciones.asmx
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Uri')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Uri', 'http://201.99.72.63/appConstancias/Ws_CertConstRetenciones.asmx')
GO
	
-- CFDI - Retrys - 5
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Retrys')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Retrys', '5')
GO
	
-- CFDI - User - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'User')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'User', '')
GO

-- CFDI - Password - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Password')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Password', '')
GO
	
-- CFDI - Site - ''
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Site')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Site', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToExpire')
BEGIN
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO','PendingPrint.AllowToExpire','0')
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM')
BEGIN
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO','PendingPrint.AllowToPlayOnEGM','1')
END

GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToRedeem')
BEGIN
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO','PendingPrint.AllowToRedeem','1')
END
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.TimeToCheckDuplicityInCashOut')
BEGIN
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.TimeToCheckDuplicityInCashOut', '5')
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinDenominationMultiple')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.MinDenominationMultiple', '0')
GO

-- CFDI - Scheduling.Load - 60
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Load')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Load', '60')
GO
	
-- CFDI - Scheduling.Process - 60
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Process')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Process', '60')
GO
	
-- CFDI - Scheduling.Delay - 30
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Scheduling.Delay')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Scheduling.Delay', '30')
GO
	
-- CFDI - CFDI.FirstDate - NULL
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'FirstDate' )
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'FirstDate', NULL )
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Terminal' AND GP_SUBJECT_KEY = 'Island.Mask')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Terminal', 'Island.Mask', '\d{3}$')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintAlesisDeleteMsg')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'PrintAlesisDeleteMsg', '0');
GO

IF EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintMsgAlesis')
    UPDATE GENERAL_PARAMS 
    SET GP_SUBJECT_KEY = 'PrintAlesisInsertMsg'
    WHERE GP_GROUP_KEY = 'PSAClient' 
      AND GP_SUBJECT_KEY = 'PrintMsgAlesis';
GO
 
IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'PrintAlesisInsertMsg')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'PrintAlesisInsertMsg', '0');
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'ReportMajorPrizeAdditionalFields')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'ReportMajorPrizeAdditionalFields', '0');
GO

IF NOT EXISTS(SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Terminal' AND GP_SUBJECT_KEY = 'EquityPercentage')
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Terminal','EquityPercentage','50')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'InventoryCount.Total')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'InventoryCount.Total', '0')
GO	

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Active')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Active', 0)
GO 

-- CFDI - CFDI.Anonymous.RFC - XAXX010101000
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Anonymous.RFC')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Anonymous.RFC', 'XAXX010101000')
GO 

-- CFDI - CFDI.Anonymous.EntidadFederativa - 02
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Anonymous.EntidadFederativa')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Anonymous.EntidadFederativa', '01')
GO
	
-- CFDI - CFDI.Foreing.RFC - XEXX010101000
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreing.RFC')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreing.RFC', 'XEXX010101000')
GO 
	
-- CFDI - CFDI.Foreing.EntidadFederativa - 01
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreing.EntidadFederativa')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreing.EntidadFederativa', '01')
GO

IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'Cashier.Handpays' AND gp_subject_key = 'AutoPayMode')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Handpays', 'AutoPayMode', '0')
GO

IF EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY LIKE 'Foreing%')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY LIKE 'Foreing%'
GO

-- CFDI - CFDI.Foreign.RFC - XEXX010101000
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreign.RFC')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreign.RFC', 'XEXX010101000' )
GO 
	
-- CFDI - CFDI.Foreign.EntidadFederativa - 01
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CFDI' AND GP_SUBJECT_KEY = 'Foreign.EntidadFederativa')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CFDI', 'Foreign.EntidadFederativa', '01')
GO

UPDATE GENERAL_PARAMS SET GP_GROUP_KEY='WinUP' WHERE GP_GROUP_KEY='Appmazing'
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WinUP' AND GP_SUBJECT_KEY = 'PlayersClub.About')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WinUP', 'PlayersClub.About', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WinUP' AND GP_SUBJECT_KEY = 'PlayersClub.Join')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WinUP', 'PlayersClub.Join', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WinUP' AND GP_SUBJECT_KEY = 'PlayersClub.Link')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WinUP', 'PlayersClub.Link', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WinUP' AND GP_SUBJECT_KEY = 'PlayersClub.Terms')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WinUP', 'PlayersClub.Terms', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WinUP' AND GP_SUBJECT_KEY = 'AuthorizationCode.Timeout')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WinUP', 'AuthorizationCode.Timeout', '0')
GO

/******* TABLES  *******/

-- NEW TABLE CFDI_REGISTERS
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CFDI_REGISTERS')
BEGIN

CREATE TABLE [dbo].[cfdi_registers](
	[cr_operation_id] [bigint] NOT NULL,
	[cr_status] [int] NOT NULL,
	[cr_inserted] [datetime] NOT NULL,
	[cr_updated] [datetime] NULL,
	[cr_xml_request] [xml] NOT NULL,
	[cr_retrys] [int] NULL,
	[cr_error_code] [int] NULL,
	[cr_error_message] [varchar](255) NULL,
 CONSTRAINT [PK_cfdi_registers] PRIMARY KEY CLUSTERED 
(
	[cr_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name      = N'te_min_allowed_ti' AND Object_ID = Object_ID(N'terminals'))
	ALTER TABLE dbo.terminals ADD te_min_allowed_ti money NULL
GO	

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name      = N'te_allow_truncate' AND Object_ID = Object_ID(N'terminals'))	
	ALTER TABLE dbo.terminals ADD te_allow_truncate bit NULL
GO
	
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name      = N'te_min_denomination' AND Object_ID = Object_ID(N'terminals'))
	ALTER TABLE dbo.terminals ADD te_min_denomination money NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[federal_states]') and name = 'fs_cfdi_id')
BEGIN
  ALTER TABLE dbo.federal_states ADD fs_cfdi_id int NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tickets_audit_status_change]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[tickets_audit_status_change](
	  [tia_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [tia_insert_date] [datetime] NOT NULL,
	  [tia_ticket_id] [bigint] NOT NULL,
	  [tia_validation_number] [bigint] NULL,
	  [tia_amount] [money] NULL,
	  [tia_status] [int] NULL,
	  [tia_created_datetime] [datetime] NULL,
	  [tia_created_terminal_id] [int] NULL,
	  [tia_money_collection_id] [bigint] NULL,
	  [tia_created_account_id] [bigint] NULL,
	  [tia_collected] [bit] NULL,
	  [tia_last_action_terminal_id] [int] NULL,
	  [tia_last_action_datetime] [datetime] NULL,
	  [tia_last_action_account_id] [bigint] NULL,
	  [tia_machine_number] [int] NULL,
	  [tia_transaction_id] [bigint] NULL,
	  [tia_created_play_session_id] [bigint] NULL,
	  [tia_canceled_play_session_id] [bigint] NULL,
	  [tia_db_inserted] [datetime] NULL,
	  [tia_collected_money_collection] [money] NULL,
	  [tia_cage_movement_id] [bigint] NULL,
	  [tia_type_id]             [int] NULL,
	  [tia_created_terminal_type] [int] NULL,
	  [tia_expiration_datetime]  [datetime] NULL
	  
   CONSTRAINT [PK_tickets_audit] PRIMARY KEY CLUSTERED 
  (
	  [tia_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

  CREATE NONCLUSTERED INDEX IDX_ticket_id ON dbo.tickets_audit_status_change
	  (
	  tia_ticket_id
	  ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	-- Si entra por este ELSE: es el caso en que la tabla ha sido creada a mano en un sólo cliente (chile). A continuación se añaden las columnas que hemos creado, para que dicho cliente tenga la tabla actualizada.

	IF NOT EXISTS(SELECT 1 FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'tickets_audit_status_change' AND COLUMN_NAME = 'tia_type_id')
		ALTER TABLE dbo.tickets_audit_status_change ADD tia_type_id int NULL

	IF NOT EXISTS(SELECT 1 FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'tickets_audit_status_change' AND COLUMN_NAME = 'tia_created_terminal_type')
		ALTER TABLE dbo.tickets_audit_status_change ADD tia_created_terminal_type int NULL

	IF NOT EXISTS(SELECT 1 FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'tickets_audit_status_change' AND COLUMN_NAME = 'tia_expiration_datetime')
		ALTER TABLE dbo.tickets_audit_status_change ADD tia_expiration_datetime datetime NULL


END
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets]') and name = 'ti_reject_reason_egm')
  ALTER TABLE tickets ADD ti_reject_reason_egm BIGINT NULL
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets]') and name = 'ti_reject_reason_wcp')
  ALTER TABLE tickets ADD ti_reject_reason_wcp BIGINT NULL
GO
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets_audit_status_change]') and name = 'tia_reject_reason_egm')
  ALTER TABLE tickets_audit_status_change ADD tia_reject_reason_egm BIGINT NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets_audit_status_change]') and name = 'tia_reject_reason_wcp')
  ALTER TABLE tickets_audit_status_change ADD tia_reject_reason_wcp BIGINT NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets_audit_status_change]') and name = 'tia_old_status')
  ALTER TABLE tickets_audit_status_change ADD tia_old_status INT NULL
GO

ALTER TABLE dbo.tickets_audit_status_change 
ALTER COLUMN tia_validation_number BIGINT not null;
GO
IF EXISTS (SELECT 1 FROM SYS.key_constraints WHERE NAME = 'PK_tickets_audit')
    ALTER TABLE dbo.tickets_audit_status_change DROP CONSTRAINT PK_tickets_audit
GO

-- Create PK
ALTER TABLE dbo.tickets_audit_status_change ADD CONSTRAINT
	PK_tickets_audit PRIMARY KEY CLUSTERED 
	(
	tia_id,
	tia_validation_number
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cfdi_registers]') and name = 'cr_reported_data')
BEGIN
  ALTER TABLE dbo.cfdi_registers ADD cr_reported_data text NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_chk_equity_percentage')
BEGIN 
   ALTER TABLE [dbo].[terminals] ADD te_chk_equity_percentage bit NULL	
END
GO

	UPDATE TERMINALS SET TE_CHK_EQUITY_PERCENTAGE = 1
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapp_images]') AND type in (N'U'))
  DROP TABLE [dbo].[mapp_images]
GO

CREATE TABLE [dbo].[mapp_images](
	[im_image_id] [bigint] IDENTITY(1,1) NOT NULL,
	[im_content_type] [nvarchar](50) NULL,
	[im_created_date] [datetime] NOT NULL,
	[im_file_name] [nvarchar](max) NULL,
	[im_name] [nvarchar](50) NULL,
	[im_updated_date] [datetime] NOT NULL,
	[im_data] [varbinary](max) NULL,
 CONSTRAINT [PK_Image] PRIMARY KEY CLUSTERED 
(
	[im_image_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mapp_section_schema]') )
BEGIN		
  CREATE TABLE [dbo].[mapp_section_schema](
	  [ss_section_schema_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [ss_active] [bit] NOT NULL,
	  [ss_deleted] [bit] NOT NULL,
	  [ss_icon_image_id] [bigint] NULL,
	  [ss_background_image_id] [bigint] NULL,
	  [ss_name] [nvarchar](50) NULL,
	  [ss_parent_id] [bigint] NULL,
	  [ss_tipo] [int] NULL,
	  [ss_last_update_date] [datetime] NULL,
	  [ss_order] [smallint] NULL
   CONSTRAINT [PK_SectionSchema] PRIMARY KEY CLUSTERED 
  (
	  [ss_section_schema_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]



  ALTER TABLE [dbo].[mapp_section_schema]  WITH CHECK ADD  CONSTRAINT [FK_SectionSchema_Image_IconId] FOREIGN KEY([ss_icon_image_id])
  REFERENCES [dbo].[mapp_images] ([im_image_id])


  ALTER TABLE [dbo].[mapp_section_schema] CHECK CONSTRAINT [FK_SectionSchema_Image_IconId]


  ALTER TABLE [dbo].[mapp_section_schema]  WITH CHECK ADD  CONSTRAINT [FK_SectionSchema_Image_ImageId] FOREIGN KEY([ss_background_image_id])
  REFERENCES [dbo].[mapp_images] ([im_image_id])


  ALTER TABLE [dbo].[mapp_section_schema] CHECK CONSTRAINT [FK_SectionSchema_Image_ImageId]


  ALTER TABLE [dbo].[mapp_section_schema]  WITH CHECK ADD  CONSTRAINT [FK_SectionSchema_SectionSchema_ParentId] FOREIGN KEY([ss_parent_id])
  REFERENCES [dbo].[mapp_section_schema] ([ss_section_schema_id])


  ALTER TABLE [dbo].[mapp_section_schema] CHECK CONSTRAINT [FK_SectionSchema_SectionSchema_ParentId]
 
END

GO

IF NOT EXISTS(SELECT *  FROM sys.columns  WHERE Name = N'ss_isfooter' AND Object_ID = Object_ID(N'mapp_section_schema'))
BEGIN
  ALTER TABLE mapp_section_schema ADD ss_isfooter bit NULL,	ss_footer_active bit NULL, ss_footer_image_id bigint NULL, ss_footer_order int NULL
END

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_handpays_amount')
ALTER TABLE [dbo].[play_sessions] ADD
      ps_handpays_amount money NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_handpays_paid_amount')
ALTER TABLE [dbo].[play_sessions] ADD
      ps_handpays_paid_amount money NULL;
GO

/******* INDEXES *******/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_operation_id_status' AND object_id = OBJECT_ID('cfdi_registers'))
BEGIN
	/****** Object:  Index [IX_operation_id_status]   ******/
	CREATE NONCLUSTERED INDEX [IX_operation_id_status] ON [dbo].[cfdi_registers] 
	(
		[cr_operation_id] ASC,
		[cr_status] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_tia_insert_date')
  DROP INDEX IDX_tia_insert_date ON tickets_audit_status_change
GO

/****** Object:  Index [IDX_tia_insert_date]    Script Date: 7/11/2016 10:10:13 a. m. ******/
CREATE NONCLUSTERED INDEX [IDX_tia_insert_date] ON [dbo].[tickets_audit_status_change]
(
	[tia_insert_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE name='PK_cards' AND object_id = OBJECT_ID('CARDS'))
BEGIN
  ALTER TABLE CARDS DROP CONSTRAINT PK_cards
  ALTER TABLE CARDS ADD  CONSTRAINT PK_cards PRIMARY KEY (CA_TRACKDATA, CA_LINKED_TYPE)  
END

GO

/******* RECORDS *******/

-- FILL VALUES
UPDATE   federal_states
   SET   fs_cfdi_id = (CASE WHEN fs_state_id  = 1 THEN 9
                            WHEN fs_state_id  = 2 THEN 1
                            WHEN fs_state_id  = 3 THEN 2
                            WHEN fs_state_id  = 4 THEN 3
                            WHEN fs_state_id  = 5 THEN 4
                            WHEN fs_state_id  = 6 THEN 7
                            WHEN fs_state_id  = 7 THEN 8
                            WHEN fs_state_id  = 8 THEN 5
                            WHEN fs_state_id  = 9 THEN 6
                            WHEN fs_state_id  = 10 THEN 10
                            WHEN fs_state_id  = 11 THEN 11
                            WHEN fs_state_id  = 12 THEN 12
                            WHEN fs_state_id  = 13 THEN 13
                            WHEN fs_state_id  = 14 THEN 14
                            WHEN fs_state_id  = 15 THEN 15
                            WHEN fs_state_id  = 16 THEN 16
                            WHEN fs_state_id  = 17 THEN 17
                            WHEN fs_state_id  = 18 THEN 18
                            WHEN fs_state_id  = 19 THEN 19
                            WHEN fs_state_id  = 20 THEN 20
                            WHEN fs_state_id  = 21 THEN 21
                            WHEN fs_state_id  = 22 THEN 22
                            WHEN fs_state_id  = 23 THEN 23
                            WHEN fs_state_id  = 24 THEN 24
                            WHEN fs_state_id  = 25 THEN 25
                            WHEN fs_state_id  = 26 THEN 26
                            WHEN fs_state_id  = 27 THEN 27
                            WHEN fs_state_id  = 28 THEN 28
                            WHEN fs_state_id  = 29 THEN 29
                            WHEN fs_state_id  = 30 THEN 30
                            WHEN fs_state_id  = 31 THEN 31
                            WHEN fs_state_id  = 32 THEN 32
                            END)
                            
GO
IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 58 AND [alc_language_id] = 9) 
BEGIN
  INSERT [dbo].alarm_categories ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (58, 9, 1, 0, N'Tickets TITO', N'', 1)
END
GO

IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 58 AND [alc_language_id] = 10) 
BEGIN
  INSERT [dbo].alarm_categories ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (58, 10, 1, 0, N'Tickets TITO', N'', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393283 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393283, 10, 0, N'Ticket value not even multiple machine denom', N'Ticket value not even multiple machine denom', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393283 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393283,  9, 0, N'El ticket no es múltiplo de la denominación', N'El ticket no es múltiplo de la denominación', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393283 AND [alcc_category] = 58) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393283, 58, 0, GETDATE() )
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262160 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262160, 10, 0, N'Reabrir Sesión', N'Reabrir Sesión', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262160 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262160,  9, 0, N'Reopen Session', N'Reopen Session', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262160 AND [alcc_category] = 40) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 262160, 40, 0, GETDATE() )
END
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_ChangeTicketStatus]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_ChangeTicketStatus]
GO

CREATE PROCEDURE [dbo].[TITO_ChangeTicketStatus] 
       @pTerminalId                   INT,
       @pTicketValidationNumber       BIGINT,
       @pTicketNewStatus              INT,
       @MsgSequenceId                 BIGINT,
       @pDefaultAllowRedemption       BIT,
       @pDefaultMaxAllowedTicketIn    MONEY,
       @pDefaultMinAllowedTicketIn    MONEY,
       --- OUTPUT
       @pTicketId                     BIGINT    OUTPUT,
       @pTicketType                   INT       OUTPUT,
       @pTicketAmount                 MONEY     OUTPUT,
       @pTicketExpiration             DATETIME  OUTPUT

AS
BEGIN
       IF @pTicketNewStatus = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
       BEGIN
           exec TITO_SetTicketPendingCancel   @pTerminalId, @pTicketValidationNumber, @pDefaultAllowRedemption, @pDefaultMaxAllowedTicketIn, @pDefaultMinAllowedTicketIn
                                            , @pTicketId output, @pTicketType output, @pTicketAmount output, @pTicketExpiration output 
           RETURN
       END

       IF @pTicketNewStatus = 0 -- TITO_TICKET_STATUS.VALID
       BEGIN
           exec TITO_SetTicketValid @pTerminalId, @pTicketValidationNumber, @pTicketId output 
           
           RETURN
       END
       
       IF @pTicketNewStatus = 1 -- TITO_TICKET_STATUS.CANCELED
       BEGIN       
           exec TITO_SetTicketCanceled @pTerminalId, @pTicketValidationNumber, @MsgSequenceId, @pTicketId output, @pTicketAmount output 
           
           RETURN
       END   
END
GO

IF OBJECT_ID (N'dbo.ApplyExchange2', N'FN') IS NOT NULL
    DROP FUNCTION dbo.ApplyExchange2;                 
GO  

CREATE FUNCTION [dbo].[ApplyExchange2]
 ( @pAmount  MONEY,
   @pFromIsoCode VARCHAR(3),
   @pToIsoCode VARCHAR(3))
RETURNS MONEY
AS
BEGIN
  DECLARE @Exchanged   MONEY
  DECLARE @Change      MONEY
  DECLARE @NumDecimals INT
      
  IF(@pFromIsoCode = @pToIsoCode)
    SET @Exchanged = @pAmount
  ELSE
    BEGIN
    -- to national currency
    IF (@pToIsoCode = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'))
       BEGIN
    SELECT TOP 1 @NumDecimals = CE_NUM_DECIMALS FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pToIsoCode 
		SELECT TOP 1 @Change = CE_CHANGE FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pFromIsoCode 
       END
    ELSE  -- to not national currency     
       BEGIN
		SELECT TOP 1 @Change = 1 / CE_CHANGE, @NumDecimals = CE_NUM_DECIMALS FROM CURRENCY_EXCHANGE WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = @pToIsoCode 
       END    
    SET @NumDecimals = ISNULL(@NumDecimals, 0)     
    SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   
    END   
		 
    RETURN @Exchanged
END -- ApplyExchange2

GO

GRANT EXECUTE ON [dbo].[ApplyExchange2] TO [wggui] WITH GRANT OPTION

GO

-- TITO_SetTicketPendingCancel
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
DROP PROCEDURE TITO_SetTicketPendingCancel
GO


/****** Object:  StoredProcedure [dbo].[TITO_SetTicketPendingCancel]    Script Date: 10/14/2016 13:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  @pDefaultMinAllowedTicketIn   MONEY,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE @_ticket_status_valid            INT
  DECLARE @_ticket_status_pending_printing INT
  DECLARE @_ticket_status_pending_cancel   INT
  DECLARE @_min_ticket_id                  BIGINT
  DECLARE @_max_ticket_id                  BIGINT
  DECLARE @_max_ticket_amount              MONEY
  DECLARE @_min_ticket_amount              MONEY
  DECLARE @_min_denom_amount               MONEY
  DECLARE @_redeem_allowed                 BIT
  DECLARE @_promotion_id                   BIGINT
  DECLARE @_restricted                     INT
  DECLARE @_prov_id                        INT
  DECLARE @_element_type                   INT
  DECLARE @_ticket_type                    INT
  DECLARE @_only_redeemable                INT
  DECLARE @_virtual_account_id             BIGINT
  
  
  DECLARE @_amt0                           MONEY
  DECLARE @_cur0                           NVARCHAR(3)
  DECLARE @_sys_amt                        MONEY
  DECLARE @_egm_amt                        MONEY
  DECLARE @_egm_iso                        NVARCHAR(3)   
  DECLARE @_dual_currency_enabled          BIT
  DECLARE @_sys_iso_national_cur           AS NVARCHAR(3)   
  DECLARE @_allow_to_play_on_egm           BIT
  DECLARE @_allow_to_pay_pending_printing  BIT  
  DECLARE @_tickets_allow_truncate         BIT
  DECLARE @_gp_allow_truncate              BIT
  DECLARE @_is_truncate                    BIT
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT @_allow_to_play_on_egm = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM'
  SET @_allow_to_play_on_egm = ISNULL(@_allow_to_play_on_egm,0)
  
  SELECT @_allow_to_pay_pending_printing = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM'
  SET @_allow_to_pay_pending_printing = ISNULL(@_allow_to_pay_pending_printing,0)
  
  SELECT   @_redeem_allowed         = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
         , @_max_ticket_amount      = TE_MAX_ALLOWED_TI
         , @_min_ticket_amount      = TE_MIN_ALLOWED_TI
         , @_min_denom_amount       = ISNULL(TE_MIN_DENOMINATION, 0)
         , @_tickets_allow_truncate = ISNULL(TE_ALLOW_TRUNCATE, 0)
         , @_prov_id                = ISNULL(TE_PROV_ID, -1)
         , @_virtual_account_id     = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
    FROM  TERMINALS                     
   WHERE  TE_TERMINAL_ID = @pTerminalId 

   SET @_egm_amt = NULL
   SET @_egm_iso = NULL

   SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
   SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)   
   
   IF (@_dual_currency_enabled = 1)
   BEGIN
     SET @_sys_iso_national_cur = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
     SET @_egm_iso  = (SELECT ISNULL(TE_ISO_CODE, @_sys_iso_national_cur) FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId)
     -- Convert max allowed ticket In when terminal iso code is different National Currency
     IF (@_max_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_max_ticket_amount = dbo.ApplyExchange2(@_max_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
     IF (@_min_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_min_ticket_amount = dbo.ApplyExchange2(@_min_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
   END

   SET @_max_ticket_amount = ISNULL(@_max_ticket_amount, @pDefaultMaxAllowedTicketIn)   
   SET @_min_ticket_amount = ISNULL(@_min_ticket_amount, @pDefaultMinAllowedTicketIn)   

  IF   @_max_ticket_amount IS NULL 
    OR @_min_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
  set @_ticket_status_pending_printing = 6

  if @_allow_to_pay_pending_printing = 0 begin
	-- Not allow to pay tickets pending priting
	set @_ticket_status_pending_printing = -9
  end
  
	  SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
			 , @_max_ticket_id = MAX (TI_TICKET_ID)
			 , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
			 , @_ticket_type   = MAX (TI_TYPE_ID)
			 , @_amt0          = ISNULL (MAX(TI_AMT0),MAX(TI_AMOUNT))
			 , @_cur0          = MAX(TI_CUR0)
			 , @_sys_amt       = MAX(TI_AMOUNT)
		FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
		 AND   1 = CASE 
					 WHEN @_allow_to_play_on_egm = 0 AND (TI_STATUS = @_ticket_status_valid or ti_status = @_ticket_status_pending_printing) THEN 1
					 WHEN @_allow_to_play_on_egm = 1 AND (TI_STATUS = @_ticket_status_valid OR TI_STATUS = @_ticket_status_pending_cancel or ti_status = @_ticket_status_pending_printing) THEN 1
				   ELSE 0 END
		 AND   TI_AMOUNT  > 0
		 AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		 AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
     AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )

 IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN

    
    IF (@_dual_currency_enabled = 1) 
    BEGIN  
        SET    @_cur0     =  ISNULL(@_cur0,@_sys_iso_national_cur)                      
        IF  @_egm_iso = @_cur0 
            SET @_egm_amt = @_amt0
        ELSE
            SET @_egm_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_egm_iso)
                      
        SET @_sys_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_sys_iso_national_cur)
    END
      
    -- One ticket found, change its status
		UPDATE    TICKETS 
		   SET    TI_STATUS                    = @_ticket_status_pending_cancel 
				, TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
				, TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
				, TI_LAST_ACTION_DATETIME      = GETDATE() 
				, TI_AMOUNT                    = @_sys_amt
				, TI_AMT1                      = @_egm_amt
				, TI_CUR1                      = @_egm_iso 
				, TI_LAST_ACTION_ACCOUNT_ID    = @_virtual_account_id 
		   WHERE  TI_TICKET_ID                 = @_min_ticket_id 
			AND   TI_AMOUNT  > 0
			AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
			AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
      AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )
			AND   (TI_STATUS = @_ticket_status_valid or TI_STATUS = @_ticket_status_pending_cancel or TI_STATUS = @_ticket_status_pending_printing)
			
			   IF @@ROWCOUNT = 1
				SELECT    @pTicketId         = TI_TICKET_ID
						, @pTicketType       = TI_TYPE_ID
						, @pTicketAmount     = CASE WHEN @_dual_currency_enabled = 1 THEN TI_AMT1
                ELSE TI_AMOUNT
                END
					, @pTicketExpiration = TI_EXPIRATION_DATETIME
					FROM    TICKETS 
				WHERE    TI_TICKET_ID  = @_min_ticket_id 


      -- Alow truncate
      SET @_gp_allow_truncate       = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
      SET @_tickets_allow_truncate  = ISNULL(@_tickets_allow_truncate, @_gp_allow_truncate)
      SET @_is_truncate             = 0
      
      IF  @_min_denom_amount > 0
	    BEGIN
		    SET @_is_truncate = @pTicketAmount % @_min_denom_amount
      END

      IF    @pTicketAmount < @_min_denom_amount                     -- TicketAmount is lower than Machine Min Denomination
        OR (@_tickets_allow_truncate = 0 AND @_is_truncate > 0)     -- Is Truncate and Not Alow Truncate
	    BEGIN

	      SET @pTicketId         = NULL
	      SET @pTicketType       = NULL
	      SET @pTicketAmount     = NULL
	      SET @pTicketExpiration = NULL

      END

  END

END

GO

IF  EXISTS (SELECT  * FROM  sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[Trigger_audit_TicketStatusChange]') AND  type in (N'TR'))
  DROP TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
GO
					
CREATE TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
ON [dbo].[TICKETS]
  AFTER INSERT, UPDATE
  NOT FOR REPLICATION
AS
BEGIN

  SET NOCOUNT ON;
  
  DECLARE @Action as char(1);
  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                        AND EXISTS(SELECT * FROM DELETED)
                        THEN 'U'  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN 'I'  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN 'D'  -- Set Action to Deleted.
                        ELSE NULL -- Skip. It may have been a "failed delete".   
                    END)
  
  IF @Action = 'I' OR UPDATE (TI_STATUS) OR UPDATE(TI_CAGE_MOVEMENT_ID)		
    INSERT INTO tickets_audit_status_change
             (tia_insert_date
             ,tia_ticket_id
             ,tia_validation_number
             ,tia_amount
             ,tia_status
             ,tia_created_datetime
             ,tia_created_terminal_id
             ,tia_money_collection_id
             ,tia_created_account_id
             ,tia_collected
             ,tia_last_action_terminal_id
             ,tia_last_action_datetime
             ,tia_last_action_account_id
             ,tia_machine_number
             ,tia_transaction_id
             ,tia_created_play_session_id
             ,tia_canceled_play_session_id
             ,tia_db_inserted
             ,tia_collected_money_collection
             ,tia_cage_movement_id
		     ,tia_type_id	 
			 ,tia_created_terminal_type
             ,tia_expiration_datetime					
)
       SELECT   GETDATE()
             ,ti_ticket_id
             ,ti_validation_number
             ,ti_amount
             ,ti_status
             ,ti_created_datetime
             ,ti_created_terminal_id
             ,ti_money_collection_id
             ,ti_created_account_id
             ,ti_collected
             ,ti_last_action_terminal_id
             ,ti_last_action_datetime
             ,ti_last_action_account_id
             ,ti_machine_number
             ,ti_transaction_id
             ,ti_created_play_session_id
             ,ti_canceled_play_session_id
             ,ti_db_inserted
             ,ti_collected_money_collection
             ,ti_cage_movement_id
		     ,ti_type_id	 
			 ,ti_created_terminal_type
             ,ti_expiration_datetime					
         FROM  INSERTED

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF
	
END -- [Trigger_Audit_TicketStatusChange]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[TITO_SetTicketPendingCancel]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  @pDefaultMinAllowedTicketIn   MONEY,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE @_ticket_status_valid            INT
  DECLARE @_ticket_status_pending_printing INT
  DECLARE @_ticket_status_pending_cancel   INT
  DECLARE @_min_ticket_id                  BIGINT
  DECLARE @_max_ticket_id                  BIGINT
  DECLARE @_max_ticket_amount              MONEY
  DECLARE @_min_ticket_amount              MONEY
  DECLARE @_min_denom_amount               MONEY
  DECLARE @_redeem_allowed                 BIT
  DECLARE @_promotion_id                   BIGINT
  DECLARE @_restricted                     INT
  DECLARE @_prov_id                        INT
  DECLARE @_element_type                   INT
  DECLARE @_ticket_type                    INT
  DECLARE @_only_redeemable                INT
  DECLARE @_virtual_account_id             BIGINT
  
  
  DECLARE @_amt0                           MONEY
  DECLARE @_cur0                           NVARCHAR(3)
  DECLARE @_sys_amt                        MONEY
  DECLARE @_egm_amt                        MONEY
  DECLARE @_egm_iso                        NVARCHAR(3)   
  DECLARE @_dual_currency_enabled          BIT
  DECLARE @_sys_iso_national_cur           AS NVARCHAR(3)   
  DECLARE @_allow_to_play_on_egm           BIT
  DECLARE @_allow_to_pay_pending_printing  BIT  
  DECLARE @_terminal_allow_truncate        BIT
  DECLARE @_gp_allow_truncate              BIT
  DECLARE @_is_truncate                    BIT
  DECLARE @_gp_min_denom_amount            MONEY
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT @_allow_to_play_on_egm = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM'
  SET @_allow_to_play_on_egm = ISNULL(@_allow_to_play_on_egm,0)
  
  SELECT @_allow_to_pay_pending_printing = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM'
  SET @_allow_to_pay_pending_printing = ISNULL(@_allow_to_pay_pending_printing,0)
     
  SELECT   @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
         , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
         , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
         , @_min_denom_amount         = TE_MIN_DENOMINATION
         , @_terminal_allow_truncate  = TE_ALLOW_TRUNCATE
         , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
         , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
    FROM  TERMINALS                     
   WHERE  TE_TERMINAL_ID = @pTerminalId 

   SET @_egm_amt = NULL
   SET @_egm_iso = NULL

   SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
   SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)   
   
   IF (@_dual_currency_enabled = 1)
   BEGIN
     SET @_sys_iso_national_cur = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
     SET @_egm_iso  = (SELECT ISNULL(TE_ISO_CODE, @_sys_iso_national_cur) FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId)
     -- Convert max allowed ticket In when terminal iso code is different National Currency
     IF (@_max_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_max_ticket_amount = dbo.ApplyExchange2(@_max_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
     IF (@_min_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_min_ticket_amount = dbo.ApplyExchange2(@_min_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
   END

   SET @_max_ticket_amount = ISNULL(@_max_ticket_amount, @pDefaultMaxAllowedTicketIn)   
   SET @_min_ticket_amount = ISNULL(@_min_ticket_amount, @pDefaultMinAllowedTicketIn)   

  IF   @_max_ticket_amount IS NULL 
    OR @_min_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
  set @_ticket_status_pending_printing = 6

  if @_allow_to_pay_pending_printing = 0 begin
	-- Not allow to pay tickets pending priting
	set @_ticket_status_pending_printing = -9
  end

	  SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
			 , @_max_ticket_id = MAX (TI_TICKET_ID)
			 , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
			 , @_ticket_type   = MAX (TI_TYPE_ID)
			 , @_amt0          = ISNULL (MAX(TI_AMT0),MAX(TI_AMOUNT))
			 , @_cur0          = MAX(TI_CUR0)
			 , @_sys_amt       = MAX(TI_AMOUNT)
		FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
		 AND   1 = CASE 
					 WHEN @_allow_to_play_on_egm = 0 AND (TI_STATUS = @_ticket_status_valid or ti_status = @_ticket_status_pending_printing) THEN 1
					 WHEN @_allow_to_play_on_egm = 1 AND (TI_STATUS = @_ticket_status_valid OR TI_STATUS = @_ticket_status_pending_cancel or ti_status = @_ticket_status_pending_printing) THEN 1
				   ELSE 0 END
		 AND   TI_AMOUNT  > 0
		 AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		 AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
     AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )

 IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN

    
    IF (@_dual_currency_enabled = 1) 
    BEGIN  
        SET    @_cur0     =  ISNULL(@_cur0,@_sys_iso_national_cur)                      
        IF  @_egm_iso = @_cur0 
            SET @_egm_amt = @_amt0
        ELSE
            SET @_egm_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_egm_iso)
                      
        SET @_sys_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_sys_iso_national_cur)
    END
      
    -- One ticket found, change its status
		UPDATE    TICKETS 
		   SET    TI_STATUS                    = @_ticket_status_pending_cancel 
				, TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
				, TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
				, TI_LAST_ACTION_DATETIME      = GETDATE() 
				, TI_AMOUNT                    = @_sys_amt
				, TI_AMT1                      = @_egm_amt
				, TI_CUR1                      = @_egm_iso 
				, TI_LAST_ACTION_ACCOUNT_ID    = @_virtual_account_id 
		   WHERE  TI_TICKET_ID                 = @_min_ticket_id 
			AND   TI_AMOUNT  > 0
			AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
			AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
      AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )
			AND   (TI_STATUS = @_ticket_status_valid or TI_STATUS = @_ticket_status_pending_cancel or TI_STATUS = @_ticket_status_pending_printing)
			
			   IF @@ROWCOUNT = 1
				SELECT    @pTicketId         = TI_TICKET_ID
						, @pTicketType       = TI_TYPE_ID
						, @pTicketAmount     = CASE WHEN @_dual_currency_enabled = 1 THEN TI_AMT1
                ELSE TI_AMOUNT
                END
					, @pTicketExpiration = TI_EXPIRATION_DATETIME
					FROM    TICKETS 
				WHERE    TI_TICKET_ID  = @_min_ticket_id 


      -- Alow truncate
      SET @_gp_allow_truncate       = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
      SET @_terminal_allow_truncate = ISNULL(@_terminal_allow_truncate, @_gp_allow_truncate)
      SET @_is_truncate             = 0

      -- Min denomination
      SET @_gp_min_denom_amount     = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinDenominationMultiple')
      SET @_min_denom_amount        = ISNULL(@_min_denom_amount, @_gp_min_denom_amount)

      if (@_terminal_allow_truncate = 0 AND @_min_denom_amount > 0)
      BEGIN
		      SET @_is_truncate = @pTicketAmount % @_min_denom_amount
      END

      IF    @pTicketAmount < @_min_denom_amount                     -- TicketAmount is lower than Machine Min Denomination
        OR (@_terminal_allow_truncate = 0 AND @_is_truncate > 0)    -- Is Truncate and Not Alow Truncate
	    BEGIN

	      SET @pTicketId         = NULL
	      SET @pTicketType       = NULL
	      SET @pTicketAmount     = NULL
	      SET @pTicketExpiration = NULL

      END

  END

END
GO

IF  EXISTS (SELECT  * FROM  sys.objects WHERE  object_id = OBJECT_ID(N'[dbo].[Trigger_audit_TicketStatusChange]') AND  type in (N'TR'))
  DROP TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
GO
					
CREATE TRIGGER [dbo].[Trigger_Audit_TicketStatusChange]
ON [dbo].[TICKETS]
  AFTER INSERT, UPDATE
  NOT FOR REPLICATION
AS
BEGIN

  SET NOCOUNT ON;
  
  DECLARE @Action as char(1);
  SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                        AND EXISTS(SELECT * FROM DELETED)
                        THEN 'U'  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN 'I'  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN 'D'  -- Set Action to Deleted.
                        ELSE NULL -- Skip. It may have been a "failed delete".   
                    END)
  
  IF @Action = 'I' OR UPDATE (TI_STATUS) OR UPDATE(TI_CAGE_MOVEMENT_ID)	OR UPDATE(TI_REJECT_REASON_EGM)	OR UPDATE(TI_REJECT_REASON_WCP)	
    INSERT INTO tickets_audit_status_change
             (tia_insert_date
             ,tia_ticket_id
             ,tia_validation_number
             ,tia_amount
             ,tia_status
	     ,tia_old_status
             ,tia_created_datetime
             ,tia_created_terminal_id
             ,tia_money_collection_id
             ,tia_created_account_id
             ,tia_collected
             ,tia_last_action_terminal_id
             ,tia_last_action_datetime
             ,tia_last_action_account_id
             ,tia_machine_number
             ,tia_transaction_id
             ,tia_created_play_session_id
             ,tia_canceled_play_session_id
             ,tia_db_inserted
             ,tia_collected_money_collection
             ,tia_cage_movement_id
             ,tia_type_id	 
             ,tia_created_terminal_type
             ,tia_expiration_datetime					
             ,tia_reject_reason_egm
             ,tia_reject_reason_wcp
)
       SELECT   GETDATE()
             ,ti_ticket_id
             ,ti_validation_number
             ,ti_amount
             ,ti_status
             ,tia_old_status = (SELECT ti_status FROM DELETED)
             ,ti_created_datetime
             ,ti_created_terminal_id
             ,ti_money_collection_id
             ,ti_created_account_id
             ,ti_collected
             ,ti_last_action_terminal_id
             ,ti_last_action_datetime
             ,ti_last_action_account_id
             ,ti_machine_number
             ,ti_transaction_id
             ,ti_created_play_session_id
             ,ti_canceled_play_session_id
             ,ti_db_inserted
             ,ti_collected_money_collection
             ,ti_cage_movement_id
             ,ti_type_id	 
             ,ti_created_terminal_type
             ,ti_expiration_datetime					
             ,ti_reject_reason_egm
             ,ti_reject_reason_wcp
         FROM  INSERTED

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF
	
END -- [Trigger_Audit_TicketStatusChange]

GO					


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_ChangeTicketStatus]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_ChangeTicketStatus]
GO


/****** Object:  StoredProcedure [dbo].[TITO_ChangeTicketStatus]    Script Date: 10/14/2016 13:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TITO_ChangeTicketStatus] 
       @pTerminalId                   INT,
       @pTicketValidationNumber       BIGINT,
       @pTicketNewStatus              INT,
       @MsgSequenceId                 BIGINT,
       @pDefaultAllowRedemption       BIT,
       @pDefaultMaxAllowedTicketIn    MONEY,
       @pDefaultMinAllowedTicketIn    MONEY,
       @pTicketRejectReasonEgm        BIGINT,
       @pTicketRejectReasonWcp        BIGINT,
       --- OUTPUT
       @pTicketId                     BIGINT    OUTPUT,
       @pTicketType                   INT       OUTPUT,
       @pTicketAmount                 MONEY     OUTPUT,
       @pTicketExpiration             DATETIME  OUTPUT

AS
BEGIN
       IF @pTicketNewStatus = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
       BEGIN
           exec TITO_SetTicketPendingCancel   @pTerminalId, @pTicketValidationNumber, @pDefaultAllowRedemption, @pDefaultMaxAllowedTicketIn, @pDefaultMinAllowedTicketIn, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp
                                            , @pTicketId output, @pTicketType output, @pTicketAmount output, @pTicketExpiration output 
           RETURN
       END

       IF @pTicketNewStatus = 0 -- TITO_TICKET_STATUS.VALID
       BEGIN
           exec TITO_SetTicketValid @pTerminalId, @pTicketValidationNumber, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp, @pTicketId output 
           
           RETURN
       END
       
       IF @pTicketNewStatus = 1 -- TITO_TICKET_STATUS.CANCELED
       BEGIN       
           exec TITO_SetTicketCanceled @pTerminalId, @pTicketValidationNumber, @MsgSequenceId, @pTicketRejectReasonEgm, @pTicketRejectReasonWcp, @pTicketId output, @pTicketAmount output 
           
           RETURN
       END   
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketCanceled]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketCanceled]
GO


/****** Object:  StoredProcedure [dbo].[TITO_SetTicketCanceled]    Script Date: 10/14/2016 13:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketCanceled]
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @MsgSequenceId               BIGINT,
      @pTicketRejectReasonEgm      BIGINT,
      @pTicketRejectReasonWcp      BIGINT,
      --- OUTPUT
      @pTicketId                   BIGINT   OUTPUT,
      @pTicketAmount               MONEY    OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_canceled              INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_ticket_amount                       MONEY
      DECLARE  @_virtual_account_id                  BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL
      SET @pTicketAmount = NULL

      SET @_ticket_status_canceled       = 1 -- TITO_TICKET_STATUS.CANCELED
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
      
      SELECT   @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId 
      

      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
             , @_ticket_amount = MAX (TI_AMOUNT)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_canceled 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
                   , TI_LAST_ACTION_ACCOUNT_ID  = @_virtual_account_id 
                   , TI_REJECT_REASON_EGM       = @pTicketRejectReasonEgm
                   , TI_REJECT_REASON_WCP       = @pTicketRejectReasonWcp
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
            BEGIN
                  SET   @pTicketId     = @_min_ticket_id
                  SET   @pTicketAmount = @_ticket_amount

                  INSERT INTO   TITO_TASKS 
                              ( TT_TASK_TYPE
                              , TT_TICKET_ID )
                       VALUES ( 1 -- TITO_TICKET_STATUS.CANCELED
                              , @pTicketId   ) 
            END

            RETURN
      END
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
DROP PROCEDURE TITO_SetTicketPendingCancel
GO


/****** Object:  StoredProcedure [dbo].[TITO_SetTicketPendingCancel]    Script Date: 10/14/2016 13:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  @pDefaultMinAllowedTicketIn   MONEY,
  @pTicketRejectReasonEgm       BIGINT,
  @pTicketRejectReasonWcp       BIGINT,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE @_ticket_status_valid            INT
  DECLARE @_ticket_status_pending_printing INT
  DECLARE @_ticket_status_pending_cancel   INT
  DECLARE @_min_ticket_id                  BIGINT
  DECLARE @_max_ticket_id                  BIGINT
  DECLARE @_max_ticket_amount              MONEY
  DECLARE @_min_ticket_amount              MONEY
  DECLARE @_min_denom_amount               MONEY
  DECLARE @_redeem_allowed                 BIT
  DECLARE @_promotion_id                   BIGINT
  DECLARE @_restricted                     INT
  DECLARE @_prov_id                        INT
  DECLARE @_element_type                   INT
  DECLARE @_ticket_type                    INT
  DECLARE @_only_redeemable                INT
  DECLARE @_virtual_account_id             BIGINT
  
  
  DECLARE @_amt0                           MONEY
  DECLARE @_cur0                           NVARCHAR(3)
  DECLARE @_sys_amt                        MONEY
  DECLARE @_egm_amt                        MONEY
  DECLARE @_egm_iso                        NVARCHAR(3)   
  DECLARE @_dual_currency_enabled          BIT
  DECLARE @_sys_iso_national_cur           AS NVARCHAR(3)   
  DECLARE @_allow_to_play_on_egm           BIT
  DECLARE @_allow_to_pay_pending_printing  BIT  
  DECLARE @_terminal_allow_truncate        BIT
  DECLARE @_gp_allow_truncate              BIT
  DECLARE @_is_truncate                    BIT
  DECLARE @_gp_min_denom_amount            MONEY
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT @_allow_to_play_on_egm = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM'
  SET @_allow_to_play_on_egm = ISNULL(@_allow_to_play_on_egm,0)
  
  SELECT @_allow_to_pay_pending_printing = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM'
  SET @_allow_to_pay_pending_printing = ISNULL(@_allow_to_pay_pending_printing,0)
     
  SELECT   @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
         , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
         , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
         , @_min_denom_amount         = TE_MIN_DENOMINATION
         , @_terminal_allow_truncate  = TE_ALLOW_TRUNCATE
         , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
         , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
    FROM  TERMINALS                     
   WHERE  TE_TERMINAL_ID = @pTerminalId 

   SET @_egm_amt = NULL
   SET @_egm_iso = NULL

   SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
   SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)   
   
   IF (@_dual_currency_enabled = 1)
   BEGIN
     SET @_sys_iso_national_cur = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
     SET @_egm_iso  = (SELECT ISNULL(TE_ISO_CODE, @_sys_iso_national_cur) FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId)
     -- Convert max allowed ticket In when terminal iso code is different National Currency
     IF (@_max_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_max_ticket_amount = dbo.ApplyExchange2(@_max_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
     IF (@_min_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_min_ticket_amount = dbo.ApplyExchange2(@_min_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
   END

   SET @_max_ticket_amount = ISNULL(@_max_ticket_amount, @pDefaultMaxAllowedTicketIn)   
   SET @_min_ticket_amount = ISNULL(@_min_ticket_amount, @pDefaultMinAllowedTicketIn)   

  IF   @_max_ticket_amount IS NULL 
    OR @_min_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
  set @_ticket_status_pending_printing = 6

  if @_allow_to_pay_pending_printing = 0 begin
	-- Not allow to pay tickets pending priting
	set @_ticket_status_pending_printing = -9
  end

	  SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
			 , @_max_ticket_id = MAX (TI_TICKET_ID)
			 , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
			 , @_ticket_type   = MAX (TI_TYPE_ID)
			 , @_amt0          = ISNULL (MAX(TI_AMT0),MAX(TI_AMOUNT))
			 , @_cur0          = MAX(TI_CUR0)
			 , @_sys_amt       = MAX(TI_AMOUNT)
		FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
		 AND   1 = CASE 
					 WHEN @_allow_to_play_on_egm = 0 AND (TI_STATUS = @_ticket_status_valid or ti_status = @_ticket_status_pending_printing) THEN 1
					 WHEN @_allow_to_play_on_egm = 1 AND (TI_STATUS = @_ticket_status_valid OR TI_STATUS = @_ticket_status_pending_cancel or ti_status = @_ticket_status_pending_printing) THEN 1
				   ELSE 0 END
		 AND   TI_AMOUNT  > 0
		 AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		 AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
     AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )

 IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN

    
    IF (@_dual_currency_enabled = 1) 
    BEGIN  
        SET    @_cur0     =  ISNULL(@_cur0,@_sys_iso_national_cur)                      
        IF  @_egm_iso = @_cur0 
            SET @_egm_amt = @_amt0
        ELSE
            SET @_egm_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_egm_iso)
                      
        SET @_sys_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_sys_iso_national_cur)
    END
      
    -- One ticket found, change its status
		UPDATE    TICKETS 
		   SET    TI_STATUS                    = @_ticket_status_pending_cancel 
				, TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
				, TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
				, TI_LAST_ACTION_DATETIME      = GETDATE() 
				, TI_AMOUNT                    = @_sys_amt
				, TI_AMT1                      = @_egm_amt
				, TI_CUR1                      = @_egm_iso 
				, TI_LAST_ACTION_ACCOUNT_ID    = @_virtual_account_id 
        , TI_REJECT_REASON_EGM         = @pTicketRejectReasonEgm
        , TI_REJECT_REASON_WCP         = @pTicketRejectReasonWcp
		   WHERE  TI_TICKET_ID                 = @_min_ticket_id 
			AND   TI_AMOUNT  > 0
			AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
			AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
      AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )
			AND   (TI_STATUS = @_ticket_status_valid or TI_STATUS = @_ticket_status_pending_cancel or TI_STATUS = @_ticket_status_pending_printing)
			
			   IF @@ROWCOUNT = 1
				SELECT    @pTicketId         = TI_TICKET_ID
						, @pTicketType       = TI_TYPE_ID
						, @pTicketAmount     = CASE WHEN @_dual_currency_enabled = 1 THEN TI_AMT1
                ELSE TI_AMOUNT
                END
					, @pTicketExpiration = TI_EXPIRATION_DATETIME
					FROM    TICKETS 
				WHERE    TI_TICKET_ID  = @_min_ticket_id 


      -- Alow truncate
      SET @_gp_allow_truncate       = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
      SET @_terminal_allow_truncate = ISNULL(@_terminal_allow_truncate, @_gp_allow_truncate)
      SET @_is_truncate             = 0

      -- Min denomination
      SET @_gp_min_denom_amount     = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinDenominationMultiple')
      SET @_min_denom_amount        = ISNULL(@_min_denom_amount, @_gp_min_denom_amount)

      if (@_terminal_allow_truncate = 0 AND @_min_denom_amount > 0)
      BEGIN
		      SET @_is_truncate = @pTicketAmount % @_min_denom_amount
      END

      IF    @pTicketAmount < @_min_denom_amount                     -- TicketAmount is lower than Machine Min Denomination
        OR (@_terminal_allow_truncate = 0 AND @_is_truncate > 0)    -- Is Truncate and Not Alow Truncate
	    BEGIN

	      SET @pTicketId         = NULL
	      SET @pTicketType       = NULL
	      SET @pTicketAmount     = NULL
	      SET @pTicketExpiration = NULL

      END

  END

END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketValid]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketValid]
GO


/****** Object:  StoredProcedure [dbo].[TITO_SetTicketValid]    Script Date: 10/14/2016 13:07:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketValid] 
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @pTicketRejectReasonEgm      BIGINT,
      @pTicketRejectReasonWcp      BIGINT,
      --- OUTPUT
      @pTicketId                               BIGINT   OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_valid                 INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_virtual_account_id                  BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL

      SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL      
            
      SELECT   @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId 

      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_valid 
                   , TI_LAST_ACTION_TERMINAL_ID = @pTerminalId 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
                   , TI_LAST_ACTION_ACCOUNT_ID  = @_virtual_account_id 
                   , TI_REJECT_REASON_EGM       = @pTicketRejectReasonEgm
                   , TI_REJECT_REASON_WCP       = @pTicketRejectReasonWcp
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
                  SET   @pTicketId = @_min_ticket_id
      END
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEquityPercentageMachine]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetEquityPercentageMachine]
GO

CREATE PROCEDURE [dbo].[GetEquityPercentageMachine]
 (@pStarDateTime DATETIME,
  @pEndDateTime DATETIME,
  @pCreditType INT ,
  @pTerminalsID VARCHAR(4096))
AS
BEGIN

DECLARE @_Coin_In_ALL INT;
DECLARE @_Coin_In_NR INT;

DECLARE @_Bill_In INT;
DECLARE @_Ticket_In_RE INT;
DECLARE @_Ticket_In_RE_PROMO INT;
DECLARE @_Ticket_In_NR_PROMO INT;
DECLARE @_Coin INT;
DECLARE @_Transfer_In INT;
DECLARE @_Transfer_Promotional_In_RE INT;
DECLARE @_Transfer_Promotional_In_NR INT;

DECLARE @_Ticket_Out_RE INT;
DECLARE @_Ticket_Out_NR INT;
DECLARE @_Jackpots INT;
DECLARE @_Cancelled_Credits INT;
DECLARE @_Transfer_Out INT;
DECLARE @_Transfer_Promotional_Out_RE INT;
DECLARE @_Transfer_Promotional_Out_NR INT;

SET @_Coin_In_ALL = 0
SET @_Coin_In_NR = 25

SET @_Bill_In = 11
SET @_Ticket_In_RE = 128
SET @_Ticket_In_RE_PROMO = 132
SET @_Ticket_In_NR_PROMO = 130
SET @_Coin = 8
SET @_Transfer_In = 160 
SET @_Transfer_Promotional_In_RE = 164
SET @_Transfer_Promotional_In_NR = 162

SET @_Ticket_Out_RE = 134
SET @_Ticket_Out_NR = 136
SET @_Jackpots = 2
SET @_Cancelled_Credits = 3
SET @_Transfer_Out = 184 
SET @_Transfer_Promotional_Out_RE = 188
SET @_Transfer_Promotional_Out_NR = 186

SELECT  EQ_MACHINE_ID, 
		EQ_DENOMINATION,
		EQ_EQUITY,
		EQ_COIN_IN,
	   (EQ_BILL_IN + EQ_TICKET_IN + EQ_COINS + EQ_TRANSFER_IN + EQ_TRANSFER_PROMOTIONAL_IN) AS EQ_TOTAL_IN,
	   (EQ_TICKET_OUT + EQ_JACKPOTS + EQ_CANCELLED_CREDITS + EQ_TRANSFER_OUT + EQ_TRANSFER_PROMOTIONAL_OUT) AS EQ_TOTAL_OUT,
	   EQ_CHK_EQUITY
FROM(
		SELECT TC.TE_TERMINAL_ID AS EQ_MACHINE_ID, 
			   TC.TE_PROVIDER_ID AS EQ_PROVIDER, 
			   TC.TE_NAME AS EQ_MACHINE, 			
			   TC.TE_DENOMINATION AS EQ_DENOMINATION, 
			   TC.TE_EQUITY_PERCENTAGE AS EQ_EQUITY,
			   TC.TE_CHK_EQUITY AS EQ_CHK_EQUITY,

			   -- COIN IN
			   CASE @pCreditType
				 WHEN 1 THEN TH.TE_COIN_IN_ALL - TH.TE_COIN_IN_NR
				 WHEN 2 THEN  TH.TE_COIN_IN_NR
				 ELSE TH.TE_COIN_IN_ALL
			   END AS EQ_COIN_IN,
		     
-----------------TOTAL IN
		      
			   --BILL IN
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_BILL_IN
			   END AS EQ_BILL_IN,
		      
		       --TICKET IN
			   CASE @pCreditType
				 WHEN 1 THEN  TE_TICKET_IN_RE + TE_TICKET_IN_RE_PROMO
				 WHEN 2 THEN  TE_TICKET_IN_NR_PROMO
				 ELSE TE_TICKET_IN_RE + TE_TICKET_IN_RE_PROMO + TE_TICKET_IN_NR_PROMO
			   END AS EQ_TICKET_IN,
		      
		       --COINS
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_COIN
			   END AS EQ_COINS,
			   
			   --TRANSFER IN
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_TRANSFER_IN
			   END AS EQ_TRANSFER_IN,
			   
			   --TRANSFER PROMOTIONAL IN
			   CASE @pCreditType
				 WHEN 1 THEN TE_TRANSFER_PROMOTIONAL_IN_RE
				 WHEN 2 THEN TE_TRANSFER_PROMOTIONAL_IN_NR
				 ELSE TE_TRANSFER_PROMOTIONAL_IN_RE + TE_TRANSFER_PROMOTIONAL_IN_NR
			   END AS EQ_TRANSFER_PROMOTIONAL_IN,
		      
-----------------TOTAL OUT
		      
		       --TICKET OUT
			   CASE @pCreditType
				 WHEN 1 THEN TE_TICKET_OUT_RE
				 WHEN 2 THEN  TE_TICKET_OUT_NR
				 ELSE TE_TICKET_OUT_RE + TE_TICKET_OUT_NR
			   END AS EQ_TICKET_OUT, 
		      
			   --JACKPOT
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_JACKPOTS
			   END AS EQ_JACKPOTS,
			   
			   --CANCELLED CREDITS
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_CANCELLED_CREDITS 
			   END AS EQ_CANCELLED_CREDITS,
			   
			    --TRANSFER OUT
			   CASE @pCreditType
				 WHEN 2 THEN 0
				 ELSE TH.TE_TRANSFER_OUT
			   END AS EQ_TRANSFER_OUT,
			   
			   --TRANSFER PROMOTIONAL OUT
			   CASE @pCreditType
				 WHEN 1 THEN TE_TRANSFER_PROMOTIONAL_OUT_RE
				 WHEN 2 THEN TE_TRANSFER_PROMOTIONAL_OUT_NR
				 ELSE TE_TRANSFER_PROMOTIONAL_OUT_RE + TE_TRANSFER_PROMOTIONAL_OUT_NR
			   END AS EQ_TRANSFER_PROMOTIONAL_OUT					         			   
		       			     		       
		FROM(SELECT TE.TE_TERMINAL_ID, ISNULL(TE.TE_PROVIDER_ID,'') AS TE_PROVIDER_ID, TE.TE_NAME,
				MIN(TE.TE_DENOMINATION) AS TE_DENOMINATION, MIN(TE.TE_EQUITY_PERCENTAGE) AS TE_EQUITY_PERCENTAGE,
				TE.TE_CHK_EQUITY_PERCENTAGE AS TE_CHK_EQUITY

			FROM TERMINALS AS TE 
			INNER JOIN TERMINALS_CONNECTED AS TC
			ON TE.TE_TERMINAL_ID = TC.TC_TERMINAL_ID AND TC.TC_CONNECTED = 1 AND TE_TERMINAL_TYPE IN (1, 3, 5, 106, 108)
			AND (TC.TC_DATE >= @pStarDateTime  AND TC.TC_DATE < @pEndDateTime )
			WHERE  TE.TE_TERMINAL_ID IN(SELECT tp.SST_VALUE FROM SplitStringIntoTable(@pTerminalsID, ',', 1) AS tp)
			GROUP BY TE.TE_TERMINAL_ID, TE.TE_PROVIDER_ID, TE.TE_NAME, TE.TE_CHK_EQUITY_PERCENTAGE) AS TC
			LEFT JOIN (SELECT TH.TSMH_TERMINAL_ID, 
					
					--GET COIN_IN
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Coin_In_ALL THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL)  / 100 AS TE_COIN_IN_ALL,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Coin_In_NR THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_COIN_IN_NR,
					
					--GET TOTAL IN
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Bill_In THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_BILL_IN,		   
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Ticket_In_RE THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TICKET_IN_RE,
          CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Ticket_In_RE_PROMO THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TICKET_IN_RE_PROMO,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Ticket_In_NR_PROMO THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TICKET_IN_NR_PROMO,			
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Coin THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_COIN,					
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_In THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_IN,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_Promotional_In_RE THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_PROMOTIONAL_IN_RE,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_Promotional_In_NR THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_PROMOTIONAL_IN_NR,
							
					--GET TOTAL OUT
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Ticket_Out_RE THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TICKET_OUT_RE,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Ticket_Out_NR THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TICKET_OUT_NR,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Jackpots THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_JACKPOTS,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Cancelled_Credits THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_CANCELLED_CREDITS,					
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_Out THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_OUT,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_Promotional_Out_RE THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_PROMOTIONAL_OUT_RE,
					CAST(SUM(CASE TH.TSMH_METER_CODE WHEN @_Transfer_Promotional_Out_NR THEN TH.TSMH_METER_INCREMENT  ELSE 0 END) AS DECIMAL) / 100 AS TE_TRANSFER_PROMOTIONAL_OUT_NR
								
					FROM TERMINAL_SAS_METERS_HISTORY AS TH WITH (INDEX(PK_terminal_sas_meters_history))
					WHERE TH.TSMH_DATETIME >= @pStarDateTime AND TH.TSMH_DATETIME < @pEndDateTime 
						  AND TH.TSMH_TYPE = 20
						  AND TH.TSMH_METER_CODE IN (@_Coin_In_ALL, @_Coin_In_NR, 
						                             @_Bill_In, @_Ticket_In_RE, @_Ticket_In_RE_PROMO, @_Ticket_In_NR_PROMO, @_Coin,
						                             @_Transfer_In, @_Transfer_Promotional_In_RE, @_Transfer_Promotional_In_NR, 													 
													 @_Ticket_Out_RE, @_Ticket_Out_NR,@_Jackpots,@_Cancelled_Credits, 
													 @_Transfer_Out, @_Transfer_Promotional_Out_RE, @_Transfer_Promotional_Out_NR) 
						  AND TH.TSMH_TERMINAL_ID IN(SELECT tp.SST_VALUE FROM SplitStringIntoTable(@pTerminalsID, ',', 1) AS tp)
					GROUP BY TH.TSMH_TERMINAL_ID) AS TH
						
		ON TC.TE_TERMINAL_ID = TH.TSMH_TERMINAL_ID) AS EQ
ORDER BY EQ.EQ_PROVIDER, EQ.EQ_MACHINE 
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_ReportMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[zsp_ReportMeters]
GO

CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16) = null,
    @pSerialNumber varchar(30) = null,
    @pMachineNumber int= null,
    @pXmlMeters Xml                              
 
AS
BEGIN
  declare @terminal_id    int
  declare @status_code    int
  declare @status_text    varchar(254)
  declare @exception_info varchar
  DECLARE @meters_count   int
  
  SET @terminal_id = 0
  SET @status_code = 0
  SET @status_text = ''
  SET @exception_info = ''
  
  select @terminal_id =t3gs_terminal_id from TERMINALS_3GS where (t3gs_machine_number = @pMachineNumber) or (t3gs_vendor_id = @pVendorId and t3gs_serial_number = @pSerialNumber)
  if(@terminal_id<1)
    set @status_code = 1
  else
    begin
      begin transaction
      begin try
        DECLARE @metercode int
        DECLARE @metervalue bigint
        DECLARE @denom int
     
        SELECT
              meters.meter.value('(@Id)[1]', 'int')       AS METER_CODE , 
              meters.meter.value('(@Value)[1]', 'bigint') AS METER_VALUE, 
              case meters.meter.value('(@Id)[1]', 'int')
                      when 66 then 5
                      when 67 then 10
                      when 68 then 20
                      when 70 then 50
                      when 71 then 100
                      when 72 then 200
                      when 74 then 500
                      when 75 then 1000
                      else 0.000
              end                                         AS DENOM
        INTO #XML_METERS_TEMP           
        FROM @pXmlMeters.nodes('Meters/Meter') meters(meter)

        DECLARE  XML_Meters_Cursor CURSOR FOR 
         SELECT  METER_CODE, METER_VALUE, DENOM
           FROM  #XML_METERS_TEMP          

        OPEN XML_Meters_Cursor
        FETCH NEXT FROM XML_Meters_Cursor INTO @metercode, @metervalue, @denom

        WHILE @@FETCH_STATUS = 0
        BEGIN
          IF EXISTS (SELECT 1 FROM terminal_sas_meters where terminal_sas_meters.tsm_terminal_id = @terminal_id 
                        AND terminal_sas_meters.tsm_meter_code = @metercode 
                        AND terminal_sas_meters.tsm_denomination = @denom)
          BEGIN
            UPDATE terminal_sas_meters SET tsm_meter_value = @metervalue, tsm_last_reported = GETDATE(), tsm_last_modified = GETDATE()
              WHERE terminal_sas_meters.tsm_terminal_id = @terminal_id 
                                  AND terminal_sas_meters.tsm_meter_code = @metercode 
                                  AND terminal_sas_meters.tsm_denomination = @denom
          END
          ELSE
          BEGIN
                INSERT INTO terminal_sas_meters ([tsm_terminal_id], [tsm_meter_code],  [tsm_game_id], [tsm_denomination],[tsm_wcp_sequence_id],
                  [tsm_last_reported], [tsm_last_modified], [tsm_meter_value], [tsm_meter_max_value], [tsm_delta_value],
                  [tsm_raw_delta_value], [tsm_delta_updating], [tsm_sas_accounting_denom])
                VALUES (@terminal_id, @metercode, 0, @denom, 0, getdate(), null, @metervalue, 9999999999, 0, 0, 0, null)
          END
          
          FETCH NEXT FROM XML_Meters_Cursor INTO @metercode, @metervalue, @denom
        END          

        CLOSE XML_Meters_Cursor
        DEALLOCATE XML_Meters_Cursor
        
        DROP TABLE #XML_METERS_TEMP 
        
        set @status_code=0
        commit transaction 
      end try
      begin catch
        rollback transaction
        set @exception_info= ' (ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                       + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                       + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                       + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                       + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                       + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
                       + ')'
        set @status_code=4
      end catch
     end

  set @status_text = case @status_code 
                       when 0 then 'OK'
                       when 1 then 'Numero de terminal invalido'
                       when 4 then 'Acceso denegado'
                       else 'Unknown' 
                     end
  
  DECLARE @input AS nvarchar(MAX)

  set @input = CAST( @pXmlMeters as nvarchar(max));
  DECLARE @output AS nvarchar(MAX)

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              + @exception_info;
         
  EXECUTE dbo.zsp_Audit 'zsp_ReportMeters', '', @pVendorId , @pSerialNumber, @terminal_id, NULL,  
                                         @status_code, NULL, 1, @input , @output


  select @status_code as StatusCode, @status_text  as StatusText
  
END -- [zsp_ReportMeters]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LinkCard]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[LinkCard]
GO

CREATE PROCEDURE [dbo].[LinkCard]
    @pExternalTrackData as CHAR(20)
  , @pLinkedType        as INT    = NULL
  , @pLinkedId          as BIGINT = NULL
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  --
  -- Unlink the 'old card' from the linked 'user'
  --    
  IF @pLinkedType IS NOT NULL
    DELETE cards WHERE ca_linked_type = @pLinkedType AND ca_linked_id = @pLinkedId

  IF @pExternalTrackData IS NULL OR LEN(@pExternalTrackData) <= 0 RETURN 

  --
  -- Unlink the 'card' from any other 'user'
  --    
  DELETE cards WHERE ca_trackdata = @pExternalTrackData AND ca_linked_id <> @pLinkedId

  IF @pLinkedType   IS NULL OR @pLinkedId  IS NULL RETURN
                
  INSERT INTO   CARDS 
              ( CA_TRACKDATA, CA_LINKED_TYPE, CA_LINKED_ID )
       VALUES ( @pExternalTrackData, @pLinkedType, @pLinkedId )
END
GO

/****** Object:  StoredProcedure [dbo].[MachineAndGameReportAcum]    Script Date: 11/09/2016 13:01:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MachineAndGameReportAcum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MachineAndGameReportAcum]
GO


CREATE PROCEDURE [dbo].[MachineAndGameReportAcum]
  @pFrom  DATETIME,  
  @pTo    DATETIME,
  @pGroup INT = 0 
AS

BEGIN

-- Date fields
DECLARE @pDateIniAcum DATETIME
DECLARE @pDays INT

-- Cash in fields
DECLARE @_ticket_in_RE INT
DECLARE @_ticket_in_pro_RE INT
DECLARE @_ticket_in_pro_NR INT
DECLARE @_credit_from_bills INT

-- Cash out fields
DECLARE @_ticket_out_RE INT
DECLARE @_ticket_out_pro_NR INT

-- Coin fields
DECLARE @_coin_in INT
DECLARE @_coin_out INT

-- Language
DECLARE @_language VARCHAR(2)

-- Set field codes
SET @_ticket_in_RE = 0x80
SET @_ticket_in_pro_RE = 0x84
SET @_ticket_in_pro_NR = 0x82
SET @_credit_from_bills = 0x0B
SET @_ticket_out_RE = 0x86
SET @_ticket_out_pro_NR = 0x88
SET @_coin_in = 0x00
SET @_coin_out = 0x01

SELECT @pDateIniAcum = DATEADD(day, 1, dbo.Opening(0, DATEADD(month, DATEDIFF(month, 0, @pTo), 0)))
SET @pDays = DATEDIFF(D,@pFrom, @pTo)

SELECT @_language = gp_key_value FROM general_params WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'Language'

-- Check dates
IF @pTo <= @pFrom
BEGIN

IF UPPER(@_language) = 'ES'
  RAISERROR ('La fecha final debe ser mayor que la fecha inicial', 1, 1)
ELSE
  RAISERROR ('Final date must be greater than end date', 1, 1)
  
  RETURN
END

IF CONVERT(VARCHAR(4),year(@pFrom)) + CONVERT(VARCHAR(2),month(@pFrom)) <> CONVERT(VARCHAR(4),year(@pTo)) + CONVERT(VARCHAR(2),month(@pTo))
BEGIN

IF UPPER(@_language) = 'ES'
  RAISERROR ('La fecha inicial y final deben pertenecer al mismo mes y año', 1, 1)
ELSE
  RAISERROR ('Initial date and final must belong to same month and year', 1, 1)
  
  RETURN
END

IF @pDays = 0
BEGIN
  SET @pDays = 1
END

-- Create temp table for whole report
CREATE TABLE #TT_MACHINEGAME (
  TERMINAL_ID INT, 
  TERMINAL_NAME NVARCHAR(50), 
  GAMENAME NVARCHAR(50), 
  CASH_IN MONEY, 
  CASH_OUT MONEY, 
  HANDPAYS MONEY, 
  COIN_IN MONEY,
  COIN_OUT MONEY,
  CASH_IN_ACUM MONEY,
  CASH_OUT_ACUM MONEY,
  HANDPAYS_ACUM MONEY,
  COIN_IN_ACUM MONEY,
  COIN_OUT_ACUM MONEY
  )

CREATE TABLE #NODATA (
  FIELD INT
  )

-- Create temp table for terminal - game relation
CREATE TABLE #TT_GAMES (TTG_TERMINAL_ID INT, TTG_GAMENAME NVARCHAR(50))
INSERT INTO #TT_GAMES


    SELECT TGT_TERMINAL_ID, 
          (SELECT TOP 1 PG_GAME_NAME FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID AND TGT_TERMINAL_ID = XX.TGT_TERMINAL_ID) AS TTG_GAMENAME
    FROM
    (
        SELECT TGT_TERMINAL_ID, 
               count(*) AS TOTAL 
        FROM
        (
          SELECT DISTINCT TGT_TERMINAL_ID, TGT_TRANSLATED_GAME_ID, PG_GAME_NAME
            FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES
           WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID
        ) YY
        GROUP BY TGT_TERMINAL_ID
    ) AS XX

-- Insert into the result values
INSERT INTO #TT_MACHINEGAME
	SELECT 
	      TSMH_SUM.TSMH_TERMINAL_ID AS TERMINAL_ID, 
	      TE_NAME AS TERMINAL_NAME, 
	      (CASE WHEN TTG_GAMENAME IS NOT NULL THEN TTG_GAMENAME ELSE 'UNKNOWN' END) AS GAME_NAME, 
        ISNULL(CASH_IN,0), 
	      ISNULL(CASH_OUT,0),
	      ISNULL(HANDPAYS, 0) AS HANDPAYS, 
	      ISNULL(COIN_IN,0),
	      ISNULL(COIN_OUT,0),
	      ISNULL(CASH_IN_ACUM,0),
	      ISNULL(CASH_OUT_ACUM,0),
	      ISNULL(HANDPAYS_ACUM, 0)AS HANDPAYS_ACUM,
	      ISNULL(COIN_IN_ACUM,0),
	      ISNULL(COIN_OUT_ACUM,0)    
	      
	FROM  
	(
	  SELECT TSMH_TERMINAL_ID, 	
			  SUM(CASE WHEN 
				TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_IN_ACUM,                  -- CASH IN ACUM				
			  SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_ticket_out_RE, @_ticket_out_pro_NR)		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_OUT_ACUM,                 -- CASH OUT ACUM       				
				SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_coin_in) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS COIN_IN_ACUM,                  -- COIN IN ACUM				
				SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_coin_out) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS COIN_OUT_ACUM                  -- COIN OUT ACUM				
		FROM	TERMINAL_SAS_METERS_HISTORY WITH (INDEX(IX_tsmh_type_code_datetime))
		WHERE	TSMH_TYPE = 1  
				AND TSMH_DATETIME >= @pDateIniAcum
				AND TSMH_DATETIME < @pTo
				AND TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills, @_ticket_out_RE, @_ticket_out_pro_NR, @_coin_in, @_coin_out)	
		GROUP BY TSMH_TERMINAL_ID	
	) AS TSMH_SUM_ACUM 
	LEFT JOIN
	(	
	  SELECT TSMH_TERMINAL_ID, 	
			  SUM(CASE WHEN 
				TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_IN,                        -- CASH IN				
			  SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_ticket_out_RE, @_ticket_out_pro_NR) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_OUT,                       -- CASH OUT				
				SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_coin_in) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS COIN_IN,                        -- COIN IN				
				SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_coin_out) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS COIN_OUT                        -- COIN OUT				
		FROM	TERMINAL_SAS_METERS_HISTORY WITH (INDEX(IX_tsmh_type_code_datetime))
		WHERE	TSMH_TYPE = 1  
				AND TSMH_DATETIME >= @pFrom
				AND TSMH_DATETIME < @pTo
				AND TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills, @_ticket_out_RE, @_ticket_out_pro_NR, @_coin_in, @_coin_out)
		GROUP BY TSMH_TERMINAL_ID	
			
	) AS TSMH_SUM       ON TSMH_SUM_ACUM.tsmh_terminal_id = TSMH_SUM.tsmh_terminal_id
	LEFT JOIN TERMINALS ON TSMH_SUM_ACUM.TSMH_TERMINAL_ID = TE_TERMINAL_ID                    -- MAQUINAS
  LEFT JOIN #TT_GAMES ON TSMH_SUM_ACUM.TSMH_TERMINAL_ID = TTG_TERMINAL_ID                   -- JUEGOS
	LEFT JOIN 
	(
		SELECT 	
			HP_TERMINAL_ID,
			SUM(HP_AMOUNT) as HANDPAYS
			FROM HANDPAYS
			WHERE HP_DATETIME >= @pFrom
			  AND HP_DATETIME < @pTo
		GROUP BY HP_TERMINAL_ID
  ) AS HP_SUM 
		ON TSMH_SUM_ACUM.TSMH_TERMINAL_ID = HP_SUM.HP_TERMINAL_ID                               -- HANDPAY
	LEFT JOIN 
	(
		SELECT 	
			HP_TERMINAL_ID,
			SUM(HP_AMOUNT) as HANDPAYS_ACUM
			FROM HANDPAYS
			WHERE HP_DATETIME >= @pDateIniAcum
			  AND HP_DATETIME < @pTo
		GROUP BY HP_TERMINAL_ID
	) AS HP_SUM_ACUM 
		ON TSMH_SUM_ACUM.TSMH_TERMINAL_ID = HP_SUM_ACUM.HP_TERMINAL_ID                          -- HANDPAY ACUM

-- Check Grouping
IF @pGroup = 0 OR @pGroup = 1
BEGIN

	SELECT 
	  TERMINAL_NAME AS CONCEPT, 
	  SUM(CASH_IN) AS CASH_IN, 
	  SUM(CASH_OUT) AS CASH_OUT, 
	  SUM(HANDPAYS) AS HANDPAYS, 
	  SUM(CASH_IN) - (SUM(CASH_OUT) + ISNULL(SUM(HANDPAYS), 0)) AS NETWIN, 
	  
	  SUM(COIN_IN) AS COIN_IN_1, 
	  SUM(COIN_OUT) AS COIN_OUT_1, 
	  SUM(COIN_IN) - SUM(COIN_OUT) AS NETWIN_COIN_1,
	  
	  SUM(CASH_IN_ACUM) AS CASH_IN_ACUM,
	  SUM(CASH_OUT_ACUM) AS CASH_OUT_ACUM,
	  SUM(HANDPAYS_ACUM) AS HANDPAYS_ACUM,
	  SUM(CASH_IN_ACUM) - (SUM(CASH_OUT_ACUM) + ISNULL(SUM(HANDPAYS_ACUM), 0)) AS NETWIN_ACUM,
	  (SUM(CASH_IN_ACUM) - (SUM(CASH_OUT_ACUM) + ISNULL(SUM(HANDPAYS_ACUM), 0))) / (@pDays) AS NETWIN_ACUM_AVG, 
	  
	  SUM(COIN_IN_ACUM) AS COIN_IN_ACUM_1,
	  SUM(COIN_OUT_ACUM) AS COIN_OUT_ACUM_1,
	  SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM) AS NETWIN_COIN_ACUM_1,
	  (SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM)) / (@pDays) AS NETWIN_COIN_ACUM_AVG_1	  
	FROM #TT_MACHINEGAME
	GROUP BY TERMINAL_NAME
	
  UNION ALL
  
	SELECT 
	  CASE WHEN UPPER(@_language) = 'ES' THEN 'TOTAL MAQUINAS' ELSE 'TOTAL MACHINES' END AS CONCEPT,
	  SUM(CASH_IN) AS CASH_IN, 
	  SUM(CASH_OUT) AS CASH_OUT, 
	  SUM(HANDPAYS) AS HANDPAYS, 
	  SUM(CASH_IN) - (SUM(CASH_OUT) + ISNULL(SUM(HANDPAYS), 0)) AS NETWIN,
	    
	  SUM(COIN_IN) AS COIN_IN_1, 
	  SUM(COIN_OUT) AS COIN_OUT_1, 
	  SUM(COIN_IN) - SUM(COIN_OUT) AS NETWIN_COIN_1,
	  
	  SUM(CASH_IN_ACUM) AS CASH_IN_ACUM,
	  SUM(CASH_OUT_ACUM) AS CASH_OUT_ACUM,
	  SUM(HANDPAYS_ACUM) AS HANDPAYS_ACUM,
	  SUM(CASH_IN_ACUM) - (SUM(CASH_OUT_ACUM) + ISNULL(SUM(HANDPAYS_ACUM), 0)) AS NETWIN_ACUM,
	  (SUM(CASH_IN_ACUM) - (SUM(CASH_OUT_ACUM) + ISNULL(SUM(HANDPAYS_ACUM), 0))) / (@pDays) AS NETWIN_ACUM_AVG,
	  
	  SUM(COIN_IN_ACUM) AS COIN_IN_ACUM_1,
	  SUM(COIN_OUT_ACUM) AS COIN_OUT_ACUM_1,
	  SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM) AS NETWIN_COIN_ACUM_1,
	  (SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM)) / (@pDays) AS NETWIN_COIN_ACUM_AVG_1	  
	FROM #TT_MACHINEGAME
	
	IF @pGroup = 1
  BEGIN
  
  SELECT FIELD FROM #NODATA
  
  END

END
IF @pGroup = 0 OR @pGroup = 2
BEGIN

	IF @pGroup = 2
  BEGIN
  
  SELECT FIELD FROM #NODATA
  
  END

	SELECT 
	  GAMENAME AS CONCEPT, 	  
	  SUM(COIN_IN)  AS COIN_IN_2, 
	  SUM(COIN_OUT) AS COIN_OUT_2, 
	  SUM(COIN_IN) - SUM(COIN_OUT) AS NETWIN_COIN_2,	  	  
	  SUM(COIN_IN_ACUM) AS COIN_IN_ACUM_2,
	  SUM(COIN_OUT_ACUM) AS COIN_OUT_ACUM_2,
	  SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM) AS NETWIN_COIN_ACUM_2,
	  (SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM)) / (@pDays) AS NETWIN_COIN_ACUM_AVG_2
	FROM #TT_MACHINEGAME
 	GROUP BY GAMENAME
 	
  UNION ALL
  
	SELECT 
	  CASE WHEN UPPER(@_language) = 'ES' THEN 'TOTAL JUEGOS' ELSE 'TOTAL GAMES' END AS CONCEPT, 	  
	  SUM(COIN_IN) AS COIN_IN_2, 
	  SUM(COIN_OUT) AS COIN_OUT_2, 
	  SUM(COIN_IN) - SUM(COIN_OUT) AS NETWIN_COIN_2,  	  
	  SUM(COIN_IN_ACUM) AS COIN_IN_ACUM_2,
	  SUM(COIN_OUT_ACUM) AS COIN_OUT_ACUM_2,
	  SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM) AS NETWIN_COIN_ACUM_2,
	  (SUM(COIN_IN_ACUM) - SUM(COIN_OUT_ACUM)) / (@pDays) AS NETWIN_COIN_ACUM_AVG_2	  
	FROM #TT_MACHINEGAME
	
END

-- Drop temp tables
DROP TABLE #TT_MACHINEGAME
DROP TABLE #TT_GAMES
DROP TABLE #NODATA

END

GO

GRANT EXECUTE ON MachineAndGameReportAcum TO wggui WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REPORT_TOOL_CONFIG]') AND type IN ('U'))
BEGIN
          DELETE FROM REPORT_TOOL_CONFIG WHERE rtc_store_name = 'MachineAndGameReportAcum'
          
          DECLARE @MAX_ID AS INT
          SELECT @MAX_ID= CASE WHEN MAX(rtc_form_id) = 0 THEN 11000 ELSE (MAX(rtc_form_id) + 1) END FROM REPORT_TOOL_CONFIG

          INSERT INTO REPORT_TOOL_CONFIG VALUES /*RTC_REPORT_TOOL_ID     4*/
                                            /*RTC_FORM_ID*/        (@MAX_ID,
                                            /*RTC_LOCATION_MENU*/   5, 
                                            /*RTC_REPORT_NAME*/     '<LanguageResources><NLS09><Label>Machine and game report</Label></NLS09><NLS10><Label>Reporte de máquinas y juegos</Label></NLS10></LanguageResources>',
                                            /*RTC_STORE_NAME*/      'MachineAndGameReportAcum',
                                            /*RTC_DESIGN_FILTER*/   
                                            '<ReportToolDesignFilterDTO>
                                              <FilterType>CustomFilters</FilterType>
                                              <Filters>
                                                <ReportToolDesignFilter>
                                                  <TypeControl>uc_date_picker</TypeControl>
                                                  <TextControl>
                                                    <LanguageResources>
                                                      <NLS09>
                                                        <Label>From Date</Label>
                                                      </NLS09>
                                                      <NLS10>
                                                        <Label>Fecha Desde</Label>
                                                      </NLS10>
                                                    </LanguageResources>
                                                  </TextControl>
                                                  <ParameterStoredProcedure>
                                                    <Name>@pFrom</Name>
                                                    <Type>DateTime</Type>
                                                  </ParameterStoredProcedure>
                                                  <Methods>
                                                    <Method>
                                                      <Name>SetFormat</Name>
                                                      <Parameters>1,4</Parameters>
                                                    </Method>
                                                  </Methods>
                                                  <Value>TodayOpening() - 1</Value>
                                                </ReportToolDesignFilter>
                                                <ReportToolDesignFilter>
                                                  <TypeControl>uc_date_picker</TypeControl>
                                                  <TextControl>
                                                    <LanguageResources>
                                                      <NLS09>
                                                        <Label>To Date</Label>
                                                      </NLS09>
                                                      <NLS10>
                                                        <Label>Fecha Hasta</Label>
                                                      </NLS10>
                                                    </LanguageResources>
                                                  </TextControl>
                                                  <ParameterStoredProcedure>
                                                    <Name>@pTo</Name>
                                                    <Type>DateTime</Type>
                                                  </ParameterStoredProcedure>
                                                  <Propertys>
                                                    <Property>
                                                      <Name>Format</Name>
                                                      <Value>8</Value>
                                                    </Property>
                                                    <Property>
                                                      <Name>CustomFormat</Name>
                                                      <Value>dd/MM/yyyy HH:mm:ss</Value>
                                                    </Property>
                                                  </Propertys>
                                                  <Methods>
                                                    <Method>
                                                      <Name>SetFormat</Name>
                                                      <Parameters>1,4</Parameters>
                                                    </Method>
                                                  </Methods>
                                                  <Value>TodayOpening()</Value>
                                                </ReportToolDesignFilter>
                                                <ReportToolDesignFilter>
                                                  <TypeControl>uc_combo</TypeControl>
                                                  <TextControl>
                                                    <LanguageResources>
                                                      <NLS09>
                                                        <Label>Group by</Label>
                                                      </NLS09>
                                                      <NLS10>
                                                        <Label>Agrupar por</Label>
                                                      </NLS10>
                                                    </LanguageResources>
                                                  </TextControl>
                                                  <ParameterStoredProcedure>
                                                    <Name>@pGroup</Name>
                                                    <Type>Int</Type>
                                                  </ParameterStoredProcedure>
                                                  <Items>
                                                    <Item>
                                                      <Id>1</Id>
                                                      <Text>
                                                        <LanguageResources>
                                                          <NLS09>
                                                            <Label>Terminal</Label>
                                                          </NLS09>
                                                          <NLS10>
                                                            <Label>Terminal</Label>
                                                          </NLS10>
                                                        </LanguageResources>
                                                      </Text>
                                                    </Item>                                                                                                      
                                                    <Item>
                                                      <Id>2</Id>
                                                      <Text>
                                                        <LanguageResources>
                                                          <NLS09>
                                                            <Label>Game</Label>
                                                          </NLS09>
                                                          <NLS10>
                                                            <Label>Juego</Label>
                                                          </NLS10>
                                                        </LanguageResources>
                                                      </Text>
                                                    </Item>
                                                  </Items>
                                                  <Value>0</Value>
                                                </ReportToolDesignFilter>
                                              </Filters>
                                             </ReportToolDesignFilterDTO>',
                                            /*RTC_DESIGN_SHEETS*/   
                                            '<ArrayOfReportToolDesignSheetsDTO>
                                            <ReportToolDesignSheetsDTO>
                                              <LanguageResources>
                                                <NLS09>
                                                  <Label>Machine report</Label>
                                                </NLS09>
                                                <NLS10>
                                                  <Label>Reporte de máquinas</Label>
                                                </NLS10>
                                              </LanguageResources>
                                              <Columns>
                                                <ReportToolDesignColumn>
                                                  <Code>CONCEPT</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Desciption</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Descripción</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>CASH_IN</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Cash In</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Cash In</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>CASH_OUT</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Cash Out</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Cash Out</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>HANDPAYS</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Handpay</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Pago manual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_IN_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Contains</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin In</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin In</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_OUT_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin Out</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin Out</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>CASH_IN_ACUM</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Cash In monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Cash In acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>CASH_OUT_ACUM</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Cash Out monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Cash Out acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>HANDPAYS_ACUM</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Handpays monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Pago manual acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_ACUM</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_ACUM_AVG</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin average by day</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin promedio por día</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_IN_ACUM_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin In monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin In acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_OUT_ACUM_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin Out monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin Out acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_ACUM_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin coin monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin coin acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_ACUM_AVG_1</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin coin average by day</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin coin promedio por día</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                              </Columns>
                                            </ReportToolDesignSheetsDTO>
                                            <ReportToolDesignSheetsDTO>
                                              <LanguageResources>
                                                <NLS09>
                                                  <Label>Game report</Label>
                                                </NLS09>
                                                <NLS10>
                                                  <Label>Reporte de juegos</Label>
                                                </NLS10>
                                              </LanguageResources>
                                              <Columns>
                                                <ReportToolDesignColumn>
                                                  <Code>CONCEPT</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Description</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Descripción</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_IN_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Contains</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin In</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin In</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_OUT_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin Out</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin Out</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_IN_ACUM_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin In monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin In acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>COIN_OUT_ACUM_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Coin Out monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Coin Out acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_ACUM_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin coin monthly acumulated</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin coin acumulado mensual</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                                <ReportToolDesignColumn>
                                                  <Code>NETWIN_COIN_ACUM_AVG_2</Code>
                                                  <Width>300</Width>
                                                  <EquityMatchType>Equality</EquityMatchType>
                                                  <LanguageResources>
                                                    <NLS09>
                                                      <Label>Netwin coin average by day</Label>
                                                    </NLS09>
                                                    <NLS10>
                                                      <Label>Netwin coin promedio por día</Label>
                                                    </NLS10>
                                                  </LanguageResources>
                                                </ReportToolDesignColumn>
                                              </Columns>
                                            </ReportToolDesignSheetsDTO>
                                          </ArrayOfReportToolDesignSheetsDTO>',
                                                                                /*RTC_MAILING*/         1,
                                                                                /*RTC_STATUS*/          0,
                                                                                /*RTC_MODE*/            3)

   DECLARE @Visible AS BIT
   SET @Visible = 0
  
   SELECT @Visible = CASE WHEN gp_key_value = 1 THEN 0 ELSE 1 END FROM general_params WHERE gp_group_key = 'WigosGUI' AND gp_subject_key = 'HideMachineAndGameReport'
   
   UPDATE REPORT_TOOL_CONFIG SET rtc_status = @Visible WHERE rtc_store_name = 'MachineAndGameReportAcum'
   
   END
GO

GRANT EXECUTE ON [dbo].[LinkCard] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[GetEquityPercentageMachine] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S2S_UpdateCardSession]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[S2S_UpdateCardSession]
GO

CREATE PROCEDURE [dbo].[S2S_UpdateCardSession]
	@pVendorId			varchar(16),
	@pSessionId			bigint,
	@pAmountPlayed		money,
	@pAmountWon			money,
	@pGamesPlayed		int,
	@pGamesWon			int,
	@pBillIn			money,
	@pCurrentBalance	money
AS
BEGIN

	DECLARE @TrackData	varchar(24)
	DECLARE @SerialNumber varchar(30)
	DECLARE @MachineNumber int  
	SET @MachineNumber = 0

	select @TrackData = dbo.TrackDataToExternal(a.ac_track_data), 
	@SerialNumber = t.t3gs_serial_number, 
	@MachineNumber = t.t3gs_machine_number 
	from play_sessions p
	inner join terminals_3gs t on p.ps_terminal_id= t.t3gs_terminal_id
	inner join accounts a on a.ac_account_id=p.ps_account_id
	where p.ps_play_session_id = @pSessionId

	set @TrackData= ISNULL(@TrackData,'')

	-- @AccountId
	--,@VendorId	OK
	--,@SerialNumber
	--,@MachineNumber	OK
	--,@SessionId	OK
	--,@AmountPlayed	OK
	--,@AmountWon	OK
	--,@GamesPlayed	OK
	--,@GamesWon	OK
	--,@CreditBalance	@pCurrentBalance??
	--,@CurrentJackpot

  EXECUTE zsp_SessionUpdate  @TrackData, @pVendorId, @SerialNumber, @MachineNumber,@pSessionId,@pAmountPlayed,@pAmountWon,@pGamesPlayed, @pGamesWon,@pCurrentBalance,0,@pBillIn,0
   
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionUpdate]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[zsp_SessionUpdate]
GO

CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0,
  @BillIn		      money = 0,
  @UpdateByFreq   bit = 1
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @update_freq  int
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @update_freq = 30

  -------- For Cadillac store all session updates received
  ------IF (@VendorId like '%CADILLAC%')
  ------    SET @update_freq = 1

  ---- AJQ & XI 12-12-2013, Limit the number of updates 
  IF ( (@GamesPlayed % @update_freq <> 0) AND (@UpdateByFreq = 1) ) 
  BEGIN
    -- When the provider calls every:
    -- 1 play --> 1 / 30 -->  3.33% trx --> 3.33% plays
    -- 2 play --> 1 / 15 -->  6.67% trx --> 3.33% plays
    -- 3 play --> 1 / 10 --> 10.00% trx --> 3.33% plays
    -- 4 play --> 1 / 60 -->  1.67% trx --> 0.42% plays
    -- 5 play --> 1 /  6 --> 16.67% trx --> 3.33% plays
    SELECT CAST (0 AS INT) AS StatusCode, CAST ('Success' AS varchar (254)) AS StatusText
    RETURN
  END

  SET @_try       = 0
  SET @_max_tries = 3
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
    
      EXECUTE dbo.Trx_3GS_UpdateCardSession @VendorId, @SerialNumber, @MachineNumber,
                                            @AccountID,
                                            @SessionId,
                                            @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                            @CreditBalance, @BillIn,
                                            @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                             @SessionId, @status_code, @CreditBalance, 1,
                                             @input, @output

  COMMIT TRANSACTION

  -- AJQ 19-DES-2014, When an exception occurred we will return 0-"Success" to the caller.
  IF (@_exception = 1)
    SET  @status_code = 0
    
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S2S_EndCardSession]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[S2S_EndCardSession]
GO

CREATE PROCEDURE [dbo].[S2S_EndCardSession]
	@pVendorId			varchar(16),
	@pSessionId			bigint,
	@pAmountPlayed		money,
	@pAmountWon			money,
	@pGamesPlayed		int,
	@pGamesWon			int,
	@pBillIn			money,
	@pCurrentBalance	money
AS
BEGIN

	DECLARE @TrackData	varchar(24)
	DECLARE @SerialNumber varchar(30)
	DECLARE @MachineNumber int  
	SET @MachineNumber = 0

	select @TrackData = dbo.TrackDataToExternal(a.ac_track_data),
    @SerialNumber = t.t3gs_serial_number, 
    @MachineNumber = t.t3gs_machine_number from play_sessions p
	inner join terminals_3gs t on p.ps_terminal_id= t.t3gs_terminal_id
	inner join accounts a on a.ac_account_id=p.ps_account_id
	where p.ps_play_session_id = @pSessionId

  set @TrackData= ISNULL(@TrackData,'')

	-- @AccountId
	--,@VendorId	OK
	--,@SerialNumber
	--,@MachineNumber	OK
	--,@SessionId	OK
	--,@AmountPlayed	OK
	--,@AmountWon	OK
	--,@GamesPlayed	OK
	--,@GamesWon	OK
	--,@CreditBalance	@pCurrentBalance??
	--,@CurrentJackpot

	EXECUTE zsp_SessionEnd  @TrackData, @pVendorId, @SerialNumber, @MachineNumber,@pSessionId,@pAmountPlayed,@pAmountWon,@pGamesPlayed, @pGamesWon,@pCurrentBalance,0,@pBillIn

    
END
GO


IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_ReportEvent')
       DROP PROCEDURE S2S_ReportEvent
GO

CREATE PROCEDURE [dbo].[S2S_ReportEvent]
  @pVendorId varchar(16),
  @pSerialNumber varchar(30),
  @pMachineNumber int,
  @pEventId int,
  @pAmount money,
  @pSessionId bigint
AS
  declare @AccountId varchar(24)

  select @AccountId = dbo.TrackDataToExternal(a.ac_track_data) 
    from play_sessions p
      inner join accounts a on a.ac_account_id=p.ps_account_id
    where p.ps_play_session_id = @pSessionId
  
  set @AccountId= ISNULL(@AccountId,'')

  execute zsp_SendEvent @AccountId,
      @pVendorId,
      @pSerialNumber,
      @pMachineNumber,
      @pEventId,
      @pAmount
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_UpdateCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_StartCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_StartCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_SendEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_SendEvent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_EndCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_EndCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_AccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_AccountStatus]
GO

IF  EXISTS (SELECT * FROM sys.assemblies asms WHERE asms.name = N'SQLBusinessLogic')
DROP ASSEMBLY [SQLBusinessLogic]
GO

CREATE ASSEMBLY [SQLBusinessLogic]
AUTHORIZATION [dbo]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A24000000000000005045000064860200201A3C580000000000000000F00022200B020B00006C040000040000000000000000000000200000000000800100000000200000000200000400000000000000040000000000000000C00400000200000000000003004085000040000000000000400000000000000000100000000000002000000000000000000000100000000000000000000000000000000000000000A00400A0030000000000000000000000000000000000000000000000000000208A04001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000004800000000000000000000002E74657874000000586B040000200000006C040000020000000000000000000000000000200000602E72737263000000A003000000A0040000040000006E0400000000000000000000000000400000402E72656C6F6300000000000000C004000000000000720400000000000000000000000000400000424800000002000500D892010048F702000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000013300700CF000000010000110073F50000060A166A05281101000616731100000A1612020E0428CD000006130411042D1700061F0A087BC10400046FF100000600060D389100000002030412010E042836010006130411042D1300061F1472010000706FF100000600060D2B6C0E0472030000706F1200000A00166A0708160E04289E0000060A067B5004000416FE01130411042D0F000E0472030000706F1300000A0000087BB0040004087BB5040004077BFB0400048C270000017219000070077BEE040004281400000A190E0428D700000626060D2B00092A001B300700DC000000020000110073050100060A166A0C160D0E0772210000706F1200000A0000020E072809000006130511052D1600061872010000706F0101000600061304DD9A0000000E04120212030E07280A000006130511052D1300061872010000706F0101000600061304DE740805281101000616731100000A1612010E0728CD000006130511052D1500061F0A077BC10400046F0101000600061304DE4209070E040E050E060E0728060000060A061304DE2D00067B810400042C0B067B810400041AFE012B011700130511052D0F000E0772210000706F1300000A000000DC0011042A011000000200190092AB002D000000001B300700B5010000030000110073050100060A166A13041613050E0772390000706F1200000A00000E04120412050E07280A000006130711072D1600061872010000706F0101000600061306DD6C010000110405281101000616731100000A1612020E0728CD000006130711072D1800061F0A087BC10400046F0101000600061306DD36010000087BB90400040E072835010006130711072D1700061F1472010000706F0101000600061306DD0C010000087BB904000412010E072838010006130711072D1700061F1472010000706F0101000600061306DDE0000000087BB9040004080E040E050E060E0728060000060A067B810400042C0B067B810400041AFE012B011700130711072D0900061306DDA700000073F60000060D09087BB00400047D54040004090E067D5804000409177D59040004090E057D5A040004091C7D52040004090E047D5504000409166A7D56040004077BEB040004077BED040004077BEE0400040912000E0728AA000006130711072D13000618724B0000706F0101000600061306DE32061306DE2D00067B810400042C0B067B810400041AFE012B011700130711072D0F000E0772390000706F1300000A000000DC0011062A000000411C0000020000001B00000069010000840100002D000000000000001B30070019010000040000110073F50000060A0572850000706F1200000A0000030528090000060D092D1500061772010000706FF100000600060CDDE2000000166A02281101000616731100000A1612010528CD0000060D092D1700061F0A077BC10400046FF100000600060CDDB0000000072C0F077BB0040004166AFE0116FE012B0116000D092D1600061F0A72010000706FF100000600060CDD82000000077BB104000416FE010D092D1300061F0B72010000706FF100000600060CDE62077BB8040004166AFE010D092D1F0006077BB80400047D48040004061F0C72010000706FF100000600060CDE3506077BBF0400047D4C040004066FF300000600060CDE1E00067B5004000416FE010D092D0E000572850000706F1300000A000000DC00082A0000000110000002001300E5F8001E000000001B300700FF020000050000110073F50000060A0E0672AB0000706F1200000A000003040512010E062836010006130811082D1700061F1472010000706FF100000600061307DDBD020000072C0E077BEB04000416FE0116FE012B011600130811082D1700061F1472010000706FF100000600061307DD8D0200000E0413091109196A301E1109166A326911096945040000003A0000002B000000300000003500000011091F126A301B11091F116A324311091F116A59694502000000270000002B00000011091F1D6A2E112B261F100D2B361F110D2B311F120D2B2C066FF300000600061307DD1C020000170D2B19180D2B15061772C90000706FF100000600061307DDFF010000166A02281101000616731100000A1612020E0628CD00000626166A1305082C0C087BB8040004166AFE012B011700130811082D0A00087BB8040004130500731500000A1304110472EB0000706F1600000A26110472450100706F1600000A261104729D0100706F1600000A26110472F10100706F1600000A26110472490200706F1600000A26110472A90200706F1600000A261104720B0300706F1600000A26110472610300706F1600000A26110472BD0300706F1600000A261104720D0400706F1600000A26110472610400706F1600000A26110472BD0400706F1600000A2611046F1700000A0E066F1800000A0E06731900000A13060011066F1A00000A720D0500701E6F1B00000A077BEB0400048C2D0000016F1C00000A0011066F1A00000A72270500701E6F1B00000A11058C270000016F1C00000A0011066F1A00000A72470500701E6F1B00000A188C2D0000016F1C00000A0011066F1A00000A725F0500701E6F1B00000A098C2D0000016F1C00000A0011066F1A00000A727F0500701F096F1B00000A0E058C080000016F1C00000A0011066F1D00000A17FE01130811082D1300061772910500706FF100000600061307DE4500DE14110614FE01130811082D0811066F1E00000A00DC00066FF300000600061307DE2100067B5004000416FE01130811082D0F000E0672AB0000706F1300000A000000DC0011072A004134000002000000F4010000C5000000B902000014000000000000000200000014000000C6020000DA02000021000000000000001B30060038040000060000110073050100060A032C0F037BB0040004166AFE0116FE012B011600130A110A2D1700061F0A72010000706F010100060006130938FC030000037BB804000404330B037BBA04000416FE012B011600130A110A2D3E00061F0D72CF0500700F02281F00000A037CB8040004281F00000A037BBA0400048C110000026F1700000A282000000A6F010100060006130938A20300000E0416731100000A282100000A2D50057B6504000416731100000A282100000A2D3D057B64040004166A3233057B6704000416731100000A282100000A2D20057B66040004166A3216057B6904000416731100000A282100000A16FE012B011600130A110A2D6D00061F15724F0600701B8D01000001130B110B160F04282200000AA2110B17057C65040004282200000AA2110B18057C64040004281F00000AA2110B19057C67040004282200000AA2110B1A057C66040004281F00000AA2110B282300000A6F010100060006130938CE02000016731100000A0E04282400000A0D16731100000A037BBC040004057B65040004282500000A057B67040004282600000A057B69040004282600000A282400000A0C0E0408282500000A1305110516731100000A282700000A1306161307110616FE01130A110A2D5B0072CD06007072E70600700E0528EF0000061204282800000A2611041F64731100000A282900000A13041105282A00000A1104282B00000A16FE01130A110A2D190017130716731100000A080E04282C00000A282400000A0D000019040512010E0528A9000006130A110A2D1600061872190700706F010100060006130938E301000004090E0411061C0E0528E7000006130A110A2D1600061872670700706F010100060006130938B90100000E05283E01000613080011086F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0011086F1A00000A72B50700701F0C1F326F2D00000A72D30700706F1C00000A0011086F1A00000A72E50700701F096F1B00000A237B14AE47E17A843F8C310000016F1C00000A0011086F1A00000A7203080070166F1B00000A168C2D0000016F1C00000A0011086F1A00000A7223080070166F1B00000A077B640400048C270000016F1C00000A0011086F1A00000A72490800701F096F1B00000A077B650400048C080000016F1C00000A0011086F1A00000A7271080070166F1B00000A077B660400048C270000016F1C00000A0011086F1A00000A72910800701F096F1B00000A077B670400048C080000016F1C00000A0011086F1A00000A72B30800701F096F1B00000A168C2D0000016F1C00000A0011086F1D00000A17FE01130A110A2D1300061872DD0800706F0101000600061309DE5C00DE14110814FE01130A110A2D0811086F1E00000A00DC00110716FE01130A110A2D2D00061A720F0900700F04282200000A1202282200000A1203282200000A282000000A6F01010006000613092B0C066F03010006000613092B000011092A411C0000020000008402000057010000DB0300001400000000000000133002008A000000070000110005026FF400000651027B500400040A064502000000280000005F000000061F0A594503000000260000004A00000032000000061F145945020000002D0000002D0000002B3703165404728309007051057201000070512B30031754047293090070512B240318540472C1090070512B180319540472E7090070512B0C031A5404721F0A0070512B002A000013300200B1000000080000110005026F0401000651027B810400040A06450E000000130000004A0000004A0000004A000000560000004A0000004A0000004A0000004A0000004A000000260000004A0000004A0000003E000000061F1459450200000021000000210000002B3703165404728309007051057201000070512B3C031754047293090070512B300318540472E7090070512B2403195404723B0A0070512B18031A5404721F0A0070512B0C031B5404726F0A0070512B002A0000001B300300A20000000900001100140B731500000A0A000672C10A00706F1600000A2606720F0B00706F1600000A2606725D0B00706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A72AB0B00701F0C6F1B00000A026F1C00000A00086F2E00000A0B072C0A077E2F00000AFE012B011700130411042D0500170DDE2300DE120814FE01130411042D07086F1E00000A00DC0000DE05260000DE0000160D2B0000092A0000011C0000020041003E7F001200000000000009008C950005010000011B300300CE0000000A00001100731500000A0A03166A55041654000672C10B00706F1600000A2606721F0C00706F1600000A2606727D0C00706F1600000A26066F1700000A056F1800000A05731900000A0B00076F1A00000A72DB0C0070166F1B00000A028C270000016F1C00000A00076F3000000A0C00086F3100000A16FE01130411042D17000308166F3200000A550408176F3300000A54170DDE3900DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE05260000DE0000160D2B0000092A00000128000002006B002A950012000000000200460065AB00120000000000000E00B3C1000501000001133008006E0000000B000011000373110000060A1F310B06176A20930300006A071F65731100000A16731100000A16731100000A1F65731100000A6F150000062606186A20930300006A0720CA000000731100000A20CA000000731100000A16731100000A16731100000A6F150000062606046F1F000006262A000013300800760000000B0000110003040573130000060A1B0B06196A20930300006A07202F010000731100000A16731100000A16731100000A202F010000731100000A6F1500000626061A6A20930300006A072094010000731100000A2094010000731100000A16731100000A16731100000A6F1500000626060E046F1F000006262A1E02283400000A2A0000133002001A0000000C00001100027B05000004166AFE010A062D03002B0702037D050000042A0000133002002E0000000C0000110004283500000A16FE010A062D0F000272010000707D06000004002B100002047D0600000402037D07000004002A0000133005005B0200000D0000110002733600000A7D01000004027B010000046F3700000A72FB0C007072190D0070283800000A6F3900000A0A06176F3A00000A0006156A6F3B00000A0006156A6F3C00000A00027B010000046F3700000A72330D007072190D0070283800000A6F3900000A26027B010000046F3700000A72530D007072190D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A726F0D0070727F0D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A72990D007072BF0D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A72DD0D007072BF0D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A72F90D007072BF0D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A72150E007072BF0D0070283800000A6F3900000A166F3D00000A00027B010000046F3700000A72370E0070724D0E0070283800000A6F3900000A176F3D00000A00027B010000046F3700000A72690E0070724D0E0070283800000A6F3900000A176F3D00000A00027B010000046F3700000A727F0E0070724D0E0070283800000A6F3900000A176F3D00000A00027B010000046F3700000A729B0E0070727F0D0070283800000A6F3900000A176F3D00000A00027B010000046F3700000A72B90E0070724D0E0070283800000A6F3900000A176F3D00000A00027B010000046F3700000A72DB0E0070724D0E0070283800000A6F3900000A176F3D00000A00027B01000004178D370000010B0716027B010000046F3700000A72FB0C00706F3E00000AA2076F3F00000A002A0003300200840000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283400000A00000228100000060002036F220000067D0200000402167D030000040272010000707D0400000402166A7D050000040272010000707D08000004002A133004008D0000000C00001102032811000006000005283500000A0A062D0E00027B02000004057D0C00000400027B020000047B1200000418FE010A062D580002047D0300000402027B020000047B0C0000047D04000004027B020000047B0F000004283500000A0A062D230002027B020000047B0F00000472010F0070027B04000004284000000A7D040000040002147D0200000400002A4A02030405720100007028140000060000002A03300200770000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283400000A00000228100000060002147D0200000402037D0300000402047D0400000402057D05000004020E047D08000004002A0013300B00220000000C00001100020304050E040E050E060E07720100007015720100007028180000060A2B00062A000013300D00290000000C00001100020304050E040E050E060E07720100007015720100007072010000700E08281A0000060A2B00062A00000013300B001F0000000C00001100020304050E040E050E060E070E0815720100007028180000060A2B00062A0013300D002A0000000C00001100020304050E040E050E060E070E081572010000707201000070027B05000004281A0000060A2B00062A000013300D00250000000C00001100020304050E040E050E060E070E080E090E0A0E0B027B05000004281A0000060A2B00062A0000001B300300B90100000E0000110000027B010000046F4100000A0A0672330D0070038C270000016F4200000A000672530D0070048C270000016F4200000A0006726F0D0070058C0D0000026F4200000A000672990D00700E048C080000016F4200000A000672DD0D00700E058C080000016F4200000A000672F90D00700E068C080000016F4200000A000672150E00700E078C080000016F4200000A000E08283500000A16FE010C082D15000672370E00707E2F00000A6F4200000A00002B10000672370E00700E086F4200000A00000E0B283500000A16FE010C082D15000672690E00707E2F00000A6F4200000A00002B10000672690E00700E0B6F4200000A00000E0915FE010C082D170006729B0E00700E098C2D0000016F4200000A00002B130006729B0E00707E2F00000A6F4200000A00000E0A283500000A16FE010C082D15000672B90E00707E2F00000A6F4200000A00002B10000672B90E00700E0A6F4200000A00000E0C166AFE010C082D17000672DB0E00700E0C8C270000016F4200000A00002B19000672DB0E0070027B050000048C270000016F4200000A0000027B010000046F4300000A066F4400000A00170BDE0A260000DE0000160B2B0000072A000000411C00000000000001000000AB010000AC01000005000000010000011B300300860000000F0000110072010000700C0072050F00700A06036F1800000A03731900000A0D00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00096F2E00000A0B00DE120914FE01130511052D07096F1E00000A00DC00072C0A077E2F00000AFE012B011700130511052D09000774280000010C0000DE05260000DE00000813042B0011042A0000011C000002001C00284400120000000000000700717800053900000113300300560000001000001100027B010000046F4300000A6F4500000A16FE0216FE010B072D34027B010000046F4300000A027B010000046F4300000A6F4500000A17596F4600000A72FB0C00706F4700000AA5270000010A2B05156A0A2B00062A3A00027B010000046F4800000A002A00000013300200610000001000001100027B010000046F4300000A6F4500000A16311C0316321803027B010000046F4300000A6F4500000AFE0416FE012B0117000B072D2400027B010000046F4300000A036F4600000A72FB0C00706F4700000AA5270000010A2B05156A0A2B00062A0000001B300400F5080000110000110072DE0F007072F20F00700328EF0000061203284900000A260917FE010C00027B010000046F4300000A6F4500000A16FE0116FE01130B110B2D090017130ADDAD08000002281C000006166AFE0216FE01130B110B2D090017130ADD91080000027B0600000414FE0116FE01130B110B2D3000027B010000046F4300000A166F4600000A72530D00706F4700000AA5270000011305110503281B0000061306002B1200027B070000041305027B0600000413060000027B010000046F4300000A6F4A00000A130C2B3F110C6F4B00000A740B0000011307001105110772530D00706F4700000AA527000001FE0116FE01130B110B2D11001107727F0E007011066F4200000A000000110C6F4C00000A130B110B2DB4DE1D110C7505000001130D110D14FE01130B110B2D08110D6F1E00000A00DC00731500000A0A0672041000706F1600000A260672541000706F1600000A2606728E1000706F1600000A260672BE1000706F1600000A260672A11100706F1600000A260672CD1100706F1600000A260816FE01130B110B2D62000672171200706F1600000A260672651200706F1600000A260672691200706F1600000A260672911200706F1600000A260672ED1200706F1600000A260672651200706F1600000A260672391300706F1600000A260672651200706F1600000A26000672EE1300706F1600000A260672501400706F1600000A260672B21400706F1600000A260672141500706F1600000A260672761500706F1600000A260672D81500706F1600000A2606723A1600706F1600000A2606729C1600706F1600000A260672FE1600706F1600000A260672601700706F1600000A260672C21700706F1600000A260672241800706F1600000A260672861800706F1600000A260672E81800706F1600000A2606724A1900706F1600000A260672AC1900706F1600000A2606720E1A00706F1600000A260816FE01130B110B2D0E000672701A00706F1600000A26000672CE1A00706F1600000A260672D41A00706F1600000A260672361B00706F1600000A260672981B00706F1600000A260672FA1B00706F1600000A2606725C1C00706F1600000A260672BE1C00706F1600000A260672201D00706F1600000A260672821D00706F1600000A260672E41D00706F1600000A260672461E00706F1600000A260672A81E00706F1600000A2606725F1F00706F1600000A260672C11F00706F1600000A260672232000706F1600000A260672852000706F1600000A260672E72000706F1600000A260816FE01130B110B2D0E000672492100706F1600000A26000672CE1A00706F1600000A260816FE01130B110B2D10000672A72100706F1600000A26002B0E000672F32100706F1600000A2600066F1700000A036F1800000A03731900000A13080011086F1A00000A723D220070166F1B00000A72330D00706F4D00000A0011086F1A00000A72C60F0070166F1B00000A72530D00706F4D00000A0011086F1A00000A72592200701E6F1B00000A726F0D00706F4D00000A0011086F1A00000A72672200701F096F1B00000A72990D00706F4D00000A0011086F1A00000A72892200701F096F1B00000A72DD0D00706F4D00000A0011086F1A00000A72A12200701F096F1B00000A72F90D00706F4D00000A0011086F1A00000A72B92200701F096F1B00000A72150E00706F4D00000A0011086F1A00000A72D72200701F0C20000100006F2D00000A72370E00706F4D00000A0011086F1A00000A72EB2200701F0C1F406F2D00000A72690E00706F4D00000A0011086F1A00000A72FF2200701F0C1F326F2D00000A727F0E00706F4D00000A00027B0200000414FE01130B110B3A8201000000027B020000047B0A00000416FE0216FE01130B110B2D2C0011086F1A00000A72152300701E6F1B00000A027B020000047B0A0000048C2D0000016F1C00000A00002B1F0011086F1A00000A72152300701E6F1B00000A7E2F00000A6F1C00000A0000027B020000047B0C000004283500000A130B110B3A8400000000027B020000047B0F000004283500000A16FE01130B110B2D2A0011086F1A00000A722D2300701F0C1F326F2D00000A027B020000047B0C0000046F1C00000A00002B3D0011086F1A00000A722D2300701F0C1F326F2D00000A027B020000047B0F00000472010F0070027B020000047B0C000004284000000A6F1C00000A0000002B1F0011086F1A00000A722D2300701E6F1B00000A7E2F00000A6F1C00000A000011086F1A00000A720D0500701E6F1B00000A729B0E00706F4D00000A0011086F1A00000A72492300701F0C1F326F2D00000A72B90E00706F4D00000A0011086F1A00000A72270500701F0C1F326F2D00000A72DB0E00706F4D00000A0000384D0100000011086F1A00000A72152300701E6F1B00000A7E2F00000A6F1C00000A00027B08000004283500000A16FE01130B110B2D240011086F1A00000A722D2300701F0C1F326F2D00000A7E2F00000A6F1C00000A00002B230011086F1A00000A722D2300701F0C1F326F2D00000A027B080000046F1C00000A0000027B0300000416FE0116FE01130B110B2D210011086F1A00000A720D0500701E6F1B00000A7E2F00000A6F1C00000A00002B250011086F1A00000A720D0500701E6F1B00000A027B030000048C2D0000016F1C00000A0000027B04000004283500000A16FE01130B110B2D240011086F1A00000A72492300701F0C1F326F2D00000A7E2F00000A6F1C00000A00002B230011086F1A00000A72492300701F0C1F326F2D00000A027B040000046F1C00000A000011086F1A00000A72270500701F0C1F326F2D00000A7E2F00000A6F1C00000A00000816FE01130B110B2D210011086F1A00000A72672300701E6F1B00000A1F0C8C2D0000016F1C00000A000011086F1A00000A727B230070166F1B00000A1304110472FB0C00706F4D00000A001104186F4E00000A001108176F4F00000A00735000000A130900110911086F5100000A001109027B010000046F5200000A0B07027B010000046F4300000A6F4500000AFE0116FE01130B110B2D060017130ADE3E00DE14110914FE01130B110B2D0811096F1E00000A00DC0000DE14110814FE01130B110B2D0811086F1E00000A00DC0000DE05260000DE000016130A2B0000110A2A0000004164000002000000C700000050000000170100001D0000000000000002000000780800003E000000B6080000140000000000000002000000FF030000CF040000CE0800001400000000000000000000001E000000C8080000E608000005000000010000011330060038000000120000110020842900006A0A1F32731100000A0B20030900006A0C0373270000060D09061F2B070872010000706F290000062609046F41000006262A1E02283400000A2A1330010011000000130000110002285300000A74060000020A2B00062A1E02283400000A2A000000133001000C0000001300001100027B150000040A2B00062A260002037D130000042A0000033003000F030000000000000002733600000A7D13000004027B130000046F3700000A729523007072190D0070283800000A6F3900000A26027B130000046F3700000A72B523007072190D0070283800000A6F3900000A26027B130000046F3700000A72D1230070724D0E0070283800000A6F3900000A26027B130000046F3700000A72F7230070727F0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A720724007072BF0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A722D24007072BF0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A724924007072BF0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A726524007072BF0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A728724007072BF0D0070283800000A6F3900000A166F3D00000A00027B130000046F3700000A72B1240070724D0E0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72C7240070724D0E0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72F124007072BF0D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72F12400706F3E00000A168C2D0000016F5400000A00027B130000046F3700000A720D25007072BF0D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A720D2500706F3E00000A168C2D0000016F5400000A00027B130000046F3700000A723F25007072190D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A727525007072190D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A728B250070727F0D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72B7250070727F0D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72D325007072190D0070283800000A6F3900000A176F3D00000A00027B130000046F3700000A72EF25007072FF250070283800000A6F3900000A176F3D00000A002ACA02147D1300000402147D1400000402147D1500000402283400000A00000228260000060002036F220000067D15000004002A000013300200230000000C00001102032827000006000004283500000A0A062D0E00027B15000004047D0C00000400002A0013300700180000000C00001100020304050E040E057201000070282A0000060A2B00062A133008001A0000000C00001100020304050E040E050E067201000070282C0000060A2B00062A0000133009001C0000000C00001100020304050E040E050E0672010000700E07282D0000060A2B00062A133009001D0000000C00001100020304050E040E050E060E0716731100000A282F0000060A2B00062A00000013300A001F0000000C00001100020304050E040E050E060E0716731100000A0E08282E0000060A2B00062A0013300C001F0000000C00001100020304050E040E050E060E070E08166A166A0E0928310000060A2B00062A0013300B001D0000000C00001100020304050E040E050E060E070E08166A166A28300000060A2B00062A00000013300C00230000000C00001100020304050E040E050E060E070E080E090E0A15731100000A28320000060A2B00062A0013300D00250000000C00001100020304050E040E050E060E070E080E090E0A15731100000A0E0B28330000060A2B00062A00000013301000350000001400001100020304050E040E050E060E070E080E090E0A0E0B16735500000A16735600000A1201FE150300001B071628340000060A2B00062A00000013301000360000001400001100020304050E040E050E060E070E080E090E0A0E0B16735500000A16735600000A1201FE150300001B070E0C28340000060A2B00062A00001B30060066030000150000110000020E097D160000040405120112020F050E0F2842000006000407080228240000067B120000041CFE0128430000060A02027B130000046F4100000A7D14000004027B140000047295230070038C270000016F4200000A00027B1400000472F7230070048C0C0000026F4200000A00027B140000047207240070168C2D0000016F4200000A00027B14000004722D240070078C080000016F4200000A00027B140000047249240070088C080000016F4200000A00027B140000047265240070168C2D0000016F4200000A00027B140000047287240070068C080000016F4200000A00027B14000004720D2500700E088C080000016F4200000A00027B14000004723F2500700E0A8C270000016F4200000A000E06283500000A16FE01130411042D1A00027B1400000472B12400707E2F00000A6F4200000A00002B1500027B1400000472B12400700E066F4200000A00000E05283500000A130411042D1700027B1400000472D12300700E056F4200000A00002B1800027B1400000472D12300707E2F00000A6F4200000A00000E04166AFE01130411042D1C00027B1400000472B52300700E048C270000016F4200000A00002B1800027B1400000472B52300707E2F00000A6F4200000A00000E077201000070285700000A16FE01130411042D1700027B1400000472C72400700E076F4200000A00002B1800027B1400000472C72400707E2F00000A6F4200000A00000E0B16731100000A285800000A16FE01130411042D1C00027B1400000472F12400700E0B8C080000016F4200000A00002B1800027B1400000472F12400707E2F00000A6F4200000A00000F0D285900000A16FE01130411042D1C00027B14000004728B2500700E0D8C0200001B6F4200000A00002B1800027B14000004728B2500707E2F00000A6F4200000A00000F0E285A00000A16FE01130411042D1C00027B1400000472752500700E0E8C0300001B6F4200000A00002B1800027B1400000472752500707E2F00000A6F4200000A00000F0C285B00000A16FE01130411042D1C00027B1400000472B72500700E0C8C0100001B6F4200000A00002B1800027B1400000472B72500707E2F00000A6F4200000A0000027B1400000472EF2500707E2F00000A6F4200000A00027B130000046F4300000A027B140000046F4400000A00170DDE0A260000DE0000160D2B0000092A0000411C000000000000010000005803000059030000050000000100000113300600170000000C00001100020304050E0416731100000A28360000060A2B00062A00133007002F0100001600001100160C0E046F630000061304110445030000009200000005000000B300000038CF0000000E046F65000006130511051959450200000002000000290000002B4E08130611062D100020930000000A20950000000B002B0E0020050200000A20060200000B002B4808130611062D100020940000000A20960000000B002B0E0020070200000A20080200000B002B2108130611062D0A001F4F0A1F580B002B0E0020030200000A20040200000B002B002B4608130611062D0A001F4E0A1F570B002B0E00200A0200000A200B0200000B002B2508130611062D0A001F5C0A1F5D0B002B0E00200C0200000A200D0200000B002B04160D2B36020304050E04060E052839000006130611062D0500160D2B1D020304050E04070E052839000006130611062D0500160D2B04170D2B00092A00133007009D00000017000011000E046F6300000616FE010C082D07160B38850000000E046F480000060E05285C00000A2C130E046F4A0000060E05285700000A16FE012B0117000C082D08204D0200000A2B350E046F480000060E05285700000A2C130E046F4A0000060E05285C00000A16FE012B0117000C082D08204B0200000A2B04160B2B1F020304050E040616731100000A28390000060C082D0500160B2B04170B2B00072A00000013300700FF0000001800001100160B0E046F630000060D0945030000000500000023000000A000000038B600000007130411042D0A0020970000000A002B0800200E0200000A00389C0000000E046F65000006130511054505000000020000005300000002000000380000001D0000002B5107130411042D0A0020980000000A002B0800200F0200000A002B3A07130411042D0A00209A0000000A002B080020110200000A002B1F07130411042D0A0020990000000A002B080020100200000A002B04160C2B422B1F07130411042D0A00209B0000000A002B080020120200000A002B04160C2B21020304050E040616731100000A2839000006130411042D0500160C2B04170C2B00082A0013300300CD040000190000110002027B130000046F4100000A7D14000004027B140000047295230070038C270000016F4200000A00027B1400000472F72300700E058C0C0000026F4200000A0004166AFE010C082D1B00027B1400000472B5230070048C270000016F4200000A00002B1800027B1400000472B52300707E2F00000A6F4200000A000005283500000A0C082D1600027B1400000472D1230070056F4200000A00002B1800027B1400000472D12300707E2F00000A6F4200000A0000027B1400000472B12400707E2F00000A6F4200000A00027B140000047287240070168C2D0000016F4200000A00027B1400000472072400700E046F440000068C080000016F4200000A00027B14000004722D2400700E046F520000068C080000016F4200000A00027B1400000472C72400700E046F480000066F4200000A000E051F4F3BB90000000E0520930000003BAD0000000E0520940000003BA10000000E051F4E3B980000000E051F5C3B8F0000000E0520970000003B830000000E0520980000002E7A0E0520990000002E710E05209A0000002E680E05209B0000002E5F0E0520030200002E560E0520050200002E4D0E0520070200002E440E05200A0200002E3B0E05200C0200002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C083AE200000000027B1400000472492400700E046F540000068C080000016F4200000A00027B1400000472652400700E046F570000068C080000016F4200000A00027B1400000472F12400700E046F5B0000068C080000016F4200000A000E0520970000002E560E0520980000002E4D0E0520990000002E440E05209A0000002E3B0E05209B0000002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C082D1F00027B1400000472652400700E046F560000068C080000016F4200000A00000038830100000E05204B020000FE0116FE010C082D5E00027B1400000472492400700E046F540000068C080000016F4200000A00027B1400000472652400700E046F560000068C080000016F4200000A00027B1400000472F12400700E046F5B0000068C080000016F4200000A000038150100000E05204D020000FE0116FE010C083AB5000000000E046F440000060E046F69000006282500000A0A027B1400000472072400700E046F560000068C080000016F4200000A00027B1400000472492400700E046F540000068C080000016F4200000A00027B14000004722D240070068C080000016F4200000A00027B140000047265240070068C080000016F4200000A00027B1400000472F12400700E046F5B0000068C080000016F4200000A00027B1400000472C72400700E046F4A0000066F4200000A00002B4D00027B140000047249240070168C2D0000016F4200000A00027B1400000472652400700E046F590000068C080000016F4200000A00027B1400000472F1240070168C2D0000016F4200000A0000027B14000004720D2500700E068C080000016F4200000A00027B14000004723F250070168C2D0000016F4200000A00027B1400000472B72500700E046F630000068C240000026F4200000A00027B130000046F4300000A027B140000046F4400000A00170B2B00072A00000013300300280000000C00001100027B1400000414FE010A062D1900027B1400000472D3250070038C270000016F4200000A00002A13300300280000000C00001100027B1400000414FE010A062D1900027B140000047249240070038C080000016F4200000A00002A13300300280000000C00001100027B1400000414FE010A062D1900027B14000004722D240070038C080000016F4200000A00002A13300300280000000C00001100027B1400000414FE010A062D1900027B140000047207240070038C080000016F4200000A00002A13300300280000000C00001100027B1400000414FE010A062D1900027B1400000472EF250070038C0A0000016F4200000A00002A133003003F0000000C00001100027B1400000414FE010A062D3000027B140000047207240070038C080000016F4200000A00027B140000047265240070048C080000016F4200000A00002A0013300200260000001A00001100150A027B1400000414FE010C082D1000027B140000046F5D00000A8E690A00060B2B00072A00001B300400172900001B0000110000027B130000046F4300000A6F4500000A16FE0116FE01131511152D0900171314DDEC280000166A130A7201000070130B00027B130000046F4300000A6F4A00000A131638B000000011166F4B00000A740B000001130E00110E72B52300706F5E00000A16FE01131511152D06003886000000110E72D12300706F5E00000A131511152D03002B71110A166AFE0116FE01131511152D3000110E72B52300706F4700000AA527000001130A110A03281B000006130B110E72D1230070110B6F4200000A00002B3100110A110E72B52300706F4700000AA527000001FE0116FE01131511152D1100110E72D1230070110B6F4200000A0000000011166F4C00000A131511153A40FFFFFFDE1D111675050000011317111714FE01131511152D0811176F1E00000A00DC00731500000A0A06721F2600706F1600000A260672592600706F1600000A260672692600706F1600000A260672A72600706F1600000A260672272700706F1600000A260672672700706F1600000A260672AB2700706F1600000A260672032800706F1600000A2606725D2800706F1600000A260672C52800706F1600000A260672292900706F1600000A2606723D2900706F1600000A260672892900706F1600000A2606729B2900706F1600000A260672D32900706F1600000A2606723F2A00706F1600000A260672AB2A00706F1600000A260672132B00706F1600000A260672772B00706F1600000A260672D32B00706F1600000A260672702C00706F1600000A260672BA2C00706F1600000A260672062D00706F1600000A260672722D00706F1600000A260672DA2D00706F1600000A2606723C2E00706F1600000A260672702E00706F1600000A260672CC2E00706F1600000A2606722E2F00706F1600000A260672AC2F00706F1600000A2606722C3000706F1600000A260672BB3000706F1600000A260672463100706F1600000A2606727E3100706F1600000A2606728E3100706F1600000A260672F03100706F1600000A260672363200706F1600000A260672783200706F1600000A260672B23200706F1600000A260672F23200706F1600000A260672383300706F1600000A260672603300706F1600000A260672903300706F1600000A260672E23300706F1600000A2606720C3400706F1600000A2606724C3400706F1600000A260672883400706F1600000A260672BE3400706F1600000A2606721C3500706F1600000A260672383300706F1600000A2606724C3500706F1600000A26066F1700000A036F1800000A03731900000A130F00110F6F1A00000A725C350070166F1B00000A027B150000047B090000048C270000016F1C00000A00110F6F1A00000A72743500701E6F1B00000A168C220000026F1C00000A00110F6F1A00000A728E3500701E6F1B00000A188C220000026F1C00000A00110F6F1A00000A72B63500701E6F1B00000A198C220000026F1C00000A00110F6F1A00000A72D63500701F166F1B00000A0C110F6F1A00000A72592200701E6F1B00000A0D110F6F1A00000A72EA3500701F096F1B00000A1304110F6F1A00000A72103600701F096F1B00000A1305110F6F1A00000A7246360070186F1B00000A1306110F6F1A00000A726C360070186F1B00000A130772A636007072C63600700328EF000006130872E636007072003700700328EF000006130C110C721A370070285C00000A130D00027B130000046F4300000A6F4A00000A131638C01B000011166F4B00000A740B000001130E0008110E72C72400706F4700000A6F1C00000A0009110E72B72500706F4700000A6F1C00000A001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001105168C2D0000016F1C00000A00110E72F72300706F4700000AA50C000002130911091318111820BF0000003DFE00000011181F58305B111845040000000306000092060000920600009206000011181F0D3B7F0D000011181F4E59450B00000043030000430300001D1700001D1700001D1700001D1700001D1700001B0300001B0300004303000043030000381817000011181F5C5945080000001403000014030000EE160000EE160000EE1600001008000053080000430500001118208F00000059451300000080020000951600009516000095160000BB020000BB020000BB020000BB020000860A0000860A0000860A0000860A0000860A0000D30A00009516000095160000951600009006000090060000111820BE000000594502000000B6050000FC090000387B160000111820360100003057111820C90000003B17070000111820CC000000594503000000D80E00004D160000D80E00001118202F010000594508000000C8070000BC080000681400003F15000020160000201600002408000009090000381B160000111820F401000059457700000068030000AE0700003214000032140000321400003214000032140000321400003214000032140000321400003214000032140000321400003214000058000000580000005800000058000000580000005800000032140000580000005800000058000000580000003214000023080000230800002308000023080000321400009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00003214000032140000321400009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00009F0A00006803000068030000150E0000321400004B0D000032140000321400004B0D0000321400003214000032140000BD0C0000BD0C00003214000032140000321400003214000032140000321400000E0C0000580000005800000058000000321400003214000032140000321400003214000032140000321400003214000032140000321400003214000082100000E40E000008110000B80F000032140000931100009F0A00009F0A00009F0A00009F0A00006712000067120000321400003214000032140000321400003214000032140000321400003214000032140000321400003214000032140000090200000902000048020000480200004802000048020000990B0000990B000011182040420F003B9A08000011182080841E003B6509000038151400001104168C2D0000016F1C00000A0038D61400001104110E722D2400706F4700000AA508000001285F00000A8C080000016F1C00000A0038AE1400001104110E72652400706F4700000AA508000001285F00000A8C080000016F1C00000A0011091F4E2E6B11091F4F2E65110920930000002E5C110920940000002E5311091F5C2E4D110920030200002E44110920050200002E3B110920070200002E321109200A0200002E291109200C0200002E201109203F0200002E171109204B0200002E0E1109204D020000FE0116FE012B011600131511152D16001105110E72072400706F4700000A6F1C00000A00001107178C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285C00000A16FE012B011700131511153AB6000000001109131811181F62303711181F4F3B8300000011181F582E7D11181F5C594507000000660000006600000075000000750000007500000057000000660000002B7311182093000000594504000000380000003800000038000000380000001118200302000059450500000017000000350000001700000035000000170000001118200C02000059450200000011000000110000002B1E09178C2D0000016F1C00000A002B0F09188C2D0000016F1C00000A002B000038FD1200001107168C2D0000016F1C00000A001106178C2D0000016F1C00000A001104110E72492400706F4700000AA5080000018C080000016F1C00000A0038BE1200001107168C2D0000016F1C00000A001106178C2D0000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A00387F1200001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A00382D1200001104110E72872400706F4700000A6F1C00000A00110E72C72400706F5E00000A2D20110E72C72400706F4700000A74280000017201000070285700000A16FE012B011700131511152D40001104168C2D0000016F1C00000A001105110E72872400706F4700000A6F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A0000389E1100001105110E72872400706F4700000A6F1C00000A001104110E72872400706F4700000A6F1C00000A00110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D3C110E72C72400706F5E00000A2D2A110E72C72400706F4700000A74280000011108285C00000A2C10096F6000000AA52400000216FE012B0117002B011600131511152D2C001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038D91000001105110E72872400706F4700000A6F1C00000A001104110E72872400706F4700000A6F1C00000A00110E72C72400706F5E00000A2D47110E72C72400706F4700000A74280000017201000070285700000A2C2A110E72C72400706F4700000A74280000011108285C00000A2C10096F6000000AA52400000216FE012B011700131511152D2C001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A00003823100000110E7207240070110E722D2400706F4700000AA5080000018C080000016F4200000A00110E7265240070110E722D2400706F4700000AA5080000018C080000016F4200000A00110E7287240070168C2D0000016F4200000A00110E722D240070168C2D0000016F4200000A0038B20F00001105110E72872400706F4700000A6F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A00386F0F00001105110E72872400706F4700000A6F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A00382C0F00001107178C2D0000016F1C00000A001106178C2D0000016F1C00000A001104168C2D0000016F1C00000A00110D131511152D25001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A000038D00E0000110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D51001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72072400706F4700000AA508000001285F00000A8C080000016F1C00000A00002B02000038380E00001107178C2D0000016F1C00000A001106178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E722D2400706F4700000AA5080000018C080000016F1C00000A0038EB0D0000110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D4C001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72072400706F4700000AA5080000018C080000016F1C00000A00002B02000038580D00001105168C2D0000016F1C00000A001104168C2D0000016F1C00000A00110E72C72400706F5E00000A2D20110E72C72400706F4700000A74280000017201000070285700000A16FE012B011700131511152D1E001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038E30C00001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001105110E72072400706F4700000AA5080000018C080000016F1C00000A0038960C00001106178C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A0038600C00001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E722D2400706F4700000AA5080000018C080000016F1C00000A000038890B00001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A000038A80A00001105110E722D2400706F4700000AA5080000018C080000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A0038670A00001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A0009178C2D0000016F1C00000A00110E72C72400706F5E00000A16FE01131511152D2400110E72C724007011086F4200000A0008110E72C72400706F4700000A6F1C00000A0000110920150200002E0E11092021020000FE0116FE012B011600131511152D1D0009168C2D0000016F1C00000A001105168C2D0000016F1C00000A0000110920140200002E29110920200200002E20110920190200002E17110920250200002E0E11092054020000FE0116FE012B011600131511152D0F0009188C2D0000016F1C00000A0000386D0900001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A0009178C2D0000016F1C00000A00110E72C72400706F5E00000A16FE01131511152D2400110E72C724007011086F4200000A0008110E72C72400706F4700000A6F1C00000A000038F808000009168C2D0000016F1C00000A001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107168C2D0000016F1C00000A001105168C2D0000016F1C00000A00086F6000000A6F1700000A1108285C00000A16FE01131511152D26001106178C2D0000016F1C00000A001104110E72872400706F4700000A6F1C00000A00002B24001107178C2D0000016F1C00000A001105110E72872400706F4700000A6F1C00000A000038490800001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104168C2D0000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D1E001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A000038BB0700001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E722D2400706F4700000AA5080000018C080000016F1C00000A000038F10600001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA5080000018C080000016F1C00000A000038220600001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A0000384E0500001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E722D2400706F4700000AA5080000018C080000016F1C00000A00003884040000110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4C001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72072400706F4700000AA5080000018C080000016F1C00000A00002B02000038FE030000110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D51001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72072400706F4700000AA508000001285F00000A8C080000016F1C00000A00002B02000038730300001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1D110E72C72400706F4700000A74280000011108285700000A16FE012B011700131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A0000389F0200001104168C2D0000016F1C00000A00388C0200001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E722D2400706F4700000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E722D2400706F4700000AA5080000018C080000016F1C00000A000038B50100001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72C72400706F5E00000A2D1A110E72C72400706F4700000A74280000011108285700000A2D10096F6000000AA52400000216FE012B011600131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72492400706F4700000AA508000001285F00000A8C080000016F1C00000A000038D40000001105110E72872400706F4700000A6F1C00000A001104110E72872400706F4700000A6F1C00000A0011092040420F00310911092080841E00327211092080841E003109110920BFC62D0031601109204C0400003109110920AF040000314E110920B0040000310911092013050000313C11092014050000310911092077050000312A110920780500003109110920DB0500003118110920DC050000310B1109203F060000FE022B0117002B011600131511152D1E001105168C2D0000016F1C00000A001104168C2D0000016F1C00000A00002B00110920CA0000002E2F110920CB0000002E26110920C90000002E1D110920CC0000002E14110920CE0000002E0B110920D0000000FE012B011700131511153AAD02000000110F6F3000000A13100011106F3100000A131511152D0900161314DDF20A000011091318111820360100003D9700000011181F5D303D11181F4E594502000000480100004801000011181F5759450700000022010000220100006001000060010000600100002201000022010000385B01000011181F633B1401000011182093000000594509000000E3000000E3000000E3000000E3000000E3000000E3000000E3000000E3000000E300000011182035010000594502000000CE000000CE000000380701000011182035020000306711182003020000594510000000730000007300000073000000730000007300000073000000B100000073000000730000007300000073000000730000007300000073000000730000007300000011182034020000594502000000630000006300000038970000001118203D0200005945030000004000000040000000400000001118204B020000594503000000270000006500000027000000111820630200005945060000000200000002000000020000000200000002000000020000002B3E38FE000000110E72072400701110166F6100000A8C080000016F4200000A00110E72652400701110166F6100000A8C080000016F4200000A0038C500000011092040420F00310911092080841E00327211092080841E003109110920BFC62D0031601109204C0400003109110920AF040000314E110920B0040000310911092013050000313C11092014050000310911092077050000312A110920780500003109110920DB0500003118110920DC050000310B1109203F060000FE022B0117002B011600131511152D03002B36110E72072400701110166F6100000A8C080000016F4200000A00110E72652400701110176F6100000A8C080000016F4200000A002B0000DE14111014FE01131511152D0811106F1E00000A00DC00000011166F4C00000A131511153A30E4FFFFDE1D111675050000011317111714FE01131511152D0811176F1E00000A00DC0000DE14110F14FE01131511152D08110F6F1E00000A00DC0006166F6200000A0006721E3700706F1600000A260672041000706F1600000A260672541000706F1600000A2606726A3700706F1600000A260672F13700706F1600000A260672BE1000706F1600000A260672A11100706F1600000A260672CD1100706F1600000A260672593800706F1600000A260672B13800706F1600000A260672093900706F1600000A260672613900706F1600000A260672B93900706F1600000A260672113A00706F1600000A260672693A00706F1600000A260672C13A00706F1600000A260672193B00706F1600000A260672713B00706F1600000A260672C93B00706F1600000A260672213C00706F1600000A260672793C00706F1600000A260672D13C00706F1600000A260672293D00706F1600000A260672813D00706F1600000A260672D93D00706F1600000A260672313E00706F1600000A260672893E00706F1600000A260672E13E00706F1600000A260672393F00706F1600000A260672913F00706F1600000A260672E93F00706F1600000A260672414000706F1600000A260672994000706F1600000A260672F14000706F1600000A260672494100706F1600000A260672A14100706F1600000A260672F94100706F1600000A260672514200706F1600000A260672A94200706F1600000A260672014300706F1600000A260672594300706F1600000A260672B14300706F1600000A260672094400706F1600000A260672614400706F1600000A260672B94400706F1600000A260672114500706F1600000A260672694500706F1600000A260672C14500706F1600000A260672194600706F1600000A260672714600706F1600000A260672C94600706F1600000A260672214700706F1600000A260672794700706F1600000A260672D14700706F1600000A260672414000706F1600000A260672294800706F1600000A260672814800706F1600000A260672304900706F1600000A2606723C4900706F1600000A2606729C4900706F1600000A260672BE4900706F1600000A260672044A00706F1600000A260672464A00706F1600000A260672684A00706F1600000A260672704A00706F1600000A260672864A00706F1600000A260672E64A00706F1600000A260672464B00706F1600000A260672FB4B00706F1600000A260672C44C00706F1600000A260672A74D00706F1600000A260672BB4D00706F1600000A260672D74D00706F1600000A260672314E00706F1600000A260672774E00706F1600000A260672B74E00706F1600000A260672D14E00706F1600000A260672254F00706F1600000A2606727D4F00706F1600000A260672C94F00706F1600000A260672075000706F1600000A260672475000706F1600000A260672815000706F1600000A26066F1700000A036F1800000A03731900000A130F00110F6F1A00000A723D220070166F1B00000A72952300706F4D00000A00110F6F1A00000A72C60F0070166F1B00000A72B52300706F4D00000A00110F6F1A00000A72FF2200701F0C1F326F2D00000A72D12300706F4D00000A00110F6F1A00000A72592200701E6F1B00000A72F72300706F4D00000A00110F6F1A00000A72672200701F096F1B00000A72072400706F4D00000A00110F6F1A00000A72A12200701F096F1B00000A722D2400706F4D00000A00110F6F1A00000A72892200701F096F1B00000A72492400706F4D00000A00110F6F1A00000A72B92200701F096F1B00000A72652400706F4D00000A00110F6F1A00000A72EA3500701F096F1B00000A72872400706F4D00000A00110F6F1A00000A72D72200701F0C20000100006F2D00000A72B12400706F4D00000A00110F6F1A00000A72975000701F0C6F1B00000A72C72400706F4D00000A00110F6F1A00000A72B55000701F096F1B00000A72F12400706F4D00000A00110F6F1A00000A72CD5000701F0C6F1B00000A11086F1C00000A00110F6F1A00000A725C350070166F1B00000A027B150000047B090000048C270000016F1C00000A00110F6F1A00000A7215230070166F1B00000A027B150000047B0A0000048C2D0000016F1C00000A00110F6F1A00000A722D2300701F0C1F326F2D00000A027B150000047B0C0000046F1C00000A00110F6F1A00000A72FB500070166F1B00000A027B150000047B0E0000048C2D0000016F1C00000A00110F6F1A00000A720D5100701F0C1F326F2D00000A027B150000047B0F0000046F1C00000A00110F6F1A00000A72235100701E6F1B00000A168C2D0000016F1C00000A00110F6F1A00000A72375100701F096F1B00000A720D2500706F4D00000A00110F6F1A00000A7265510070166F1B00000A723F2500706F4D00000A00110F6F1A00000A72935100701E6F1B00000A20C80000008C0C0000026F1C00000A00110F6F1A00000A72B55100701E6F1B00000A202B0100008C0C0000026F1C00000A00110F6F1A00000A72D5510070166F1B00000A027B160000048C270000016F1C00000A00110F6F1A00000A72FB510070166F1B00000A72752500706F4D00000A00110F6F1A00000A720D5200701E6F1B00000A728B2500706F4D00000A00110F6F1A00000A7237520070166F1B00000A72D32500706F4D00000A00110F6F1A00000A724F5200701A6F1B00000A72EF2500706F4D00000A00110F166F4F00000A00735000000A1311001111110F6F5100000A00111120F40100006F6300000A001111027B130000046F5200000A0B07027B130000046F4300000A6F4500000AFE0116FE01131511152D0600171314DE4500DE14111114FE01131511152D0811116F1E00000A00DC0000DE14110F14FE01131511152D08110F6F1E00000A00DC0000DE0C13120011127A13130011137A001613142B000011142A0041C400000200000045000000C70000000C0100001D00000000000000020000000A1E00008D02000097200000140000000000000002000000E9040000D71B0000C02000001D0000000000000002000000A8030000391D0000E1200000140000000000000002000000862800004B000000D1280000140000000000000002000000F6240000F3030000E9280000140000000000000000000000010000000029000001290000060000004000000100000000010000000029000007290000060000003900000113300200EA0F00001C000011000E04500A0416731100000A81080000010516731100000A81080000010E04720100007051020B0720370100003D8B0300000745D10000004C020000640200002B030000730300008B030000DF030000A9040000710400008D040000E1040000FD04000035050000510500006D050000A9040000640B0000640B0000640B0000640B0000640B0000B6020000EE0200000A030000D20200009902000099020000640B0000640B000019050000DD050000FE0500001A0600003606000052060000A6060000A5050000DE0600008A060000C206000089050000FA06000016070000DD050000320700004E0700006F070000B1070000D207000071040000640B0000640B0000640B0000640B0000640B000089050000A5050000640B0000640B0000640B000090070000C1050000C1050000D2020000D2020000D20200007104000035050000F30700000F080000C5040000C5040000A9040000A70300002B080000430800005F080000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000FB0300008A060000A6060000640B0000640B0000640B00002B0800007C080000640B0000640B000098080000B4080000B4080000640B0000640B00007303000098080000D0080000D008000055040000390400005504000039040000390400003904000039040000100A0000100A0000100A000039040000390400003904000039040000550400005504000039040000390400003904000055040000550400005504000055040000550400005504000055040000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000F8090000E0090000780900006E060000640B0000640B000078090000640B0000640B0000640B0000640B0000280A0000280A0000280A0000280A0000280A0000400A0000880A0000640B0000640B00008102000081020000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B00002B03000073030000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B00004C0200004C0200002B0300007303000043030000640B00005B030000640B00007303000007202C01000059450C0000002009000004090000200900002009000004090000740900008C090000280B0000280B0000F80A0000100B00002009000038230B00000720F4010000594579000000FA000000FA000000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000920100003309000033090000330900003309000033090000330900007308000033090000330900003309000033090000F7070000F7070000F7070000F7070000F707000033090000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E8020000B0020000CC020000E8020000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E802000042010000FA0000002A0100003309000012010000330900003309000012010000330900000802000008020000120100002A010000BB060000BB060000D7060000D7060000F3060000F3060000120100003309000033090000330900008B0800008B0800008B0800008B0800008B0800006307000040020000630700009402000033090000E802000033090000BB08000033090000A308000008020000D308000012010000120100001201000012010000EB080000EB0800003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000780200005C020000072040420F003B1C080000072080841E003B290800003818090000040381080000010516731100000A810800000138090A00000416731100000A81080000010516731100000A810800000138EC090000040381080000010516731100000A810800000138D40900000416731100000A81080000010516731100000A810800000138B7090000040381080000010516731100000A81080000010E040651389B0900000416731100000A8108000001050381080000010E040651387F090000040381080000010516731100000A81080000010E04065138630900000416731100000A81080000010516731100000A81080000010E0406513842090000040381080000010516731100000A8108000001382A090000040381080000010516731100000A810800000138120900000416731100000A81080000010503810800000138FA0800000416731100000A81080000010503810800000138E2080000040381080000010516731100000A81080000010E04065138C60800000416731100000A8108000001050381080000010E04065138AA080000040381080000010516731100000A81080000010E040651388E0800000416731100000A8108000001050381080000010E04065138720800000E0516FE010C082D17000416731100000A810800000105038108000001002B1500040381080000010516731100000A8108000001000E0406513834080000040381080000010516731100000A81080000010E04065138180800000416731100000A8108000001050381080000010E04065138FC070000040381080000010516731100000A81080000010E04065138E00700000416731100000A8108000001050381080000010E04065138C4070000040381080000010516731100000A81080000010E04065138A80700000416731100000A8108000001050381080000010E040651388C070000040381080000010516731100000A81080000010E04065138700700000416731100000A8108000001050381080000010E0406513854070000040381080000010516731100000A81080000010E04065138380700000416731100000A8108000001050381080000010E040651381C070000040381080000010516731100000A81080000010E0406513800070000040381080000010516731100000A81080000010E04065138E4060000040381080000010516731100000A81080000010E04065138C8060000040381080000010516731100000A81080000010E04065138AC060000040381080000010516731100000A81080000010E04065138900600000416731100000A81080000010516731100000A81080000010E040651386F0600000416731100000A8108000001050381080000010E0406513853060000040381080000010516731100000A81080000010E04065138370600000416731100000A8108000001050381080000010E040651381B060000040381080000010516731100000A81080000010E04065138FF0500000416731100000A8108000001050381080000010E04065138E3050000040381080000010516731100000A81080000010E04065138C7050000040381080000010516731100000A81080000010E04065138AB0500000416731100000A8108000001050381080000010E040651388F0500000416731100000A8108000001050381080000010E04065138730500000416731100000A8108000001050381080000010E04065138570500000416731100000A8108000001050381080000010E040651383B050000040381080000010516731100000A81080000010E040651381F0500000416731100000A81080000010516731100000A81080000010E04065138FE0400000416731100000A81080000010516731100000A81080000010E04065138DD0400000416731100000A81080000010516731100000A81080000010E04065138BC0400000416731100000A81080000010516731100000A81080000010E040651389B0400000416731100000A81080000010516731100000A81080000010E040651387A040000040381080000010516731100000A81080000010E040651385E0400000416731100000A8108000001050381080000010E0406513842040000040381080000010516731100000A8108000001382A040000040381080000010516731100000A81080000010E040651380E0400000416731100000A81080000010516731100000A810800000138F1030000040381080000010516731100000A81080000010E04065138D50300000416731100000A8108000001050381080000010E04065138B9030000040381080000010516731100000A81080000010E040651389D0300000416731100000A8108000001050381080000010E04065138810300000416731100000A8108000001050381080000010E0406513865030000040381080000010516731100000A81080000010E04065138490300000416731100000A8108000001050381080000010E040651382D030000040381080000010516731100000A81080000010E04065138110300000416731100000A8108000001050381080000010E04065138F5020000040381080000010516731100000A81080000010E04065138D9020000040381080000010516731100000A81080000010E04065138BD020000040381080000010516731100000A810800000138A50200000416731100000A810800000105038108000001388D020000040381080000010516731100000A810800000138750200000416731100000A810800000105038108000001385D0200000416731100000A8108000001050381080000013845020000040381080000010516731100000A8108000001382D0200000416731100000A8108000001050381080000013815020000040381080000010516731100000A810800000138FD0100000416731100000A81080000010503810800000138E50100000416731100000A8108000001050381080000010E04065138C9010000040381080000010516731100000A810800000138B1010000040381080000010516731100000A81080000013899010000040381080000010516731100000A810800000138810100000416731100000A81080000010503810800000138690100000416731100000A8108000001050381080000013851010000040381080000010516731100000A810800000138390100000416731100000A8108000001050381080000013821010000040381080000010516731100000A81080000013809010000022040420F00310D022080841E00FE0416FE012B0117000C082D1900040381080000010516731100000A810800000138D5000000022080841E00310A0220BFC62D00FE022B0117000C082D19000416731100000A81080000010503810800000138A4000000022014050000310A022077050000FE022B0117000C082D1600040381080000010516731100000A81080000012B76022078050000310A0220DB050000FE022B0117000C082D16000416731100000A8108000001050381080000012B4802204C040000310A0220AF040000FE022B0117000C082D1600040381080000010516731100000A81080000012B1A0416731100000A81080000010516731100000A81080000012B002A000013300200480400001D00001100020C0820370100003D040300000820BF0000003D88020000081C59459C0000005701000057010000570100008201000082010000570100005701000082010000570100008201000082010000820100008201000082010000570100005701000082010000570100008201000082010000820100008201000082010000820100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000570100008201000057010000820100008201000082010000820100005701000082010000820100008201000082010000820100005701000057010000820100008201000082010000820100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000820100008201000082010000820100008201000082010000820100008201000082010000820100005701000057010000570100008201000082010000820100005701000057010000820100008201000057010000570100005701000082010000820100005701000057010000570100005701000064010000570100005701000057010000570100005701000057010000570100005701000057010000820100008201000057010000570100006401000064010000570100005701000057010000570100005701000064010000640100006401000064010000640100008201000082010000820100008201000082010000820100008201000082010000820100008201000082010000570100005701000057010000570100008201000082010000570100008201000082010000820100008201000082010000820100008201000082010000820100008201000057010000820100008201000057010000570100000820BF0000003B4C01000038720100000820C800000059450900000017010000170100001701000017010000170100004201000017010000170100001701000008202C01000059450C000000DB000000DB00000006010000DB000000DB00000006010000060100000601000006010000DB000000DB000000DB000000380101000008200902000030300820FE010000594505000000AE000000AE000000AE000000AE000000AE0000000820090200003BA300000038C900000008203202000059451F00000016000000160000001600000016000000160000001600000016000000160000001600000016000000410000004100000041000000410000001600000016000000160000001600000016000000160000004100000041000000160000004100000041000000410000001600000041000000160000001F0000001600000008206B02000059450200000002000000020000002B2B16731100000A0A2B51030A2B4D0516FE010D092D0B0004285F00000A0A002B090016731100000A0A002B2F02204C040000310A02203F060000FE022B0117000D092D0B0016731100000A0A002B0A000304282500000A0A002B00060B2B00072A133001000C0000001E00001100027B850300040A2B00062A260002037D850300042A0000133001000C0000001F00001100027B940300040A2B00062A260002037D940300042A0000133001000C0000002000001100027B860300040A2B00062A260002037D860300042A0000133001000C0000002000001100027B870300040A2B00062A260002037D870300042A0000133001000C0000001E00001100027B8F0300040A2B00062A260002037D8F0300042A0000133001000C0000002100001100027B900300040A2B00062A260002037D900300042A0000133001000C0000000C00001100027B910300040A2B00062A260002037D910300042A0000133001000C0000001E00001100027B880300040A2B00062A260002037D880300042A0000133001000C0000001E00001100027B890300040A2B00062A260002037D890300042A000013300200170000001E00001100027B8A030004027B8B030004282600000A0A2B00062A00133001000C0000001E00001100027B8A0300040A2B00062A260002037D8A0300042A0000133001000C0000001E00001100027B8B0300040A2B00062A260002037D8B0300042A0000133001000C0000001E00001100027B8C0300040A2B00062A260002037D8C0300042A0000133001000C0000001E00001100027B8D0300040A2B00062A260002037D8D0300042A0000133001000C0000001E00001100027B8E0300040A2B00062A260002037D8E0300042A0000133001000C0000000C00001100027B920300040A2B00062A260002037D920300042A0000133001000C0000002200001100027B930300040A2B00062A260002037D930300042A0000133001000C0000002300001100027B970300040A2B00062A260002037D970300042A0000133001000C0000002400001100027B950300040A2B00062A260002037D950300042A0000133001000C0000001E00001100027B960300040A2B00062A260002037D960300042A8602167D9203000402738E0000067D9503000402167D9703000402283400000A002A133001000C0000001F00001100027B980300040A2B00062A260002037D980300042A0000133001000C0000002300001100027B990300040A2B00062A260002037D990300042A0000133001000C0000002000001100027B9A0300040A2B00062A260002037D9A0300042A0000133001000C0000002000001100027B9B0300040A2B00062A260002037D9B0300042A0000133001000C0000002000001100027B9C0300040A2B00062A260002037D9C0300042A0000133001000C0000002100001100027B9D0300040A2B00062A260002037D9D0300042A0000133001000C0000002000001100027B9E0300040A2B00062A260002037D9E0300042A0000133001000C0000001E00001100027B9F0300040A2B00062A260002037D9F0300042A0000133001000C0000002000001100027BA00300040A2B00062A260002037DA00300042A0000133001000C0000002000001100027BA10300040A2B00062A260002037DA10300042A0000133001000C0000002000001100027BA30300040A2B00062A260002037DA30300042A0000133001000C0000002000001100027BA40300040A2B00062A260002037DA40300042A0000133001000C0000000C00001100027BA20300040A2B00062A260002037DA20300042A0000133001000C0000002000001100027BA50300040A2B00062A260002037DA50300042A0000133001000C0000002500001100027BA60300040A2B00062A260002037DA60300042A0000133001000C0000002600001100027BA70300040A2B00062A260002037DA70300042A0000133001000C0000002000001100027BA80300040A2B00062A260002037DA80300042A2A02283400000A0000002A000000133001000C0000002100001100027BF60300040A2B00062A260002037DF60300042A0000133001000C0000001E00001100027BF70300040A2B00062A260002037DF70300042A0000133001000C0000002000001100027BF80300040A2B00062A260002037DF80300042A0000133001000C0000002200001100027BF90300040A2B00062A260002037DF90300042A0000133001000C0000001F00001100027BF50300040A2B00062A260002037DF50300042A0000133001000C0000002000001100027BFA0300040A2B00062A260002037DFA0300042A1E02283400000A2A00001B3005006B05000027000011000428110100065105165400032811010006120212050E0428C8000006130C110C2D090016130BDD3B0500001105166AFE01130C110C2D090016130BDD26050000731500000A0A02130D110D1759450200000005000000D700000038600100001F0A130406166F6200000A0006725D5200706F1600000A260672B95200706F1600000A2606721B5300706F1600000A260672AC5300706F1600000A260672185400706F1600000A26066F1700000A0D06166F6200000A00067262540070097251550070284000000A6F1600000A2606725F55007009286600000A6F1600000A2606724E5600706F1600000A2606725A560070097251550070284000000A6F1600000A2606724557007009286600000A6F1600000A2606727E3100706F1600000A260672305800706F1600000A2638960000001B130406166F6200000A0006725D5200706F1600000A260672B95200706F1600000A260672745800706F1600000A260672C25800706F1600000A260672265900706F1600000A260672825900706F1600000A26066F1700000A0D06166F6200000A000672D85900706F1600000A2606720A5A00706F1600000A2606728A5A007009286600000A6F1600000A262B0816130BDD9F030000066F1700000A0E046F1800000A0E04731900000A13060011066F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A0011066F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011066F1A00000A72BE5A00701E6F1B00000A178C090000026F1C00000A0011066F1A00000A72DE5A00701E6F1B00000A188C090000026F1C00000A0011066F1A00000A72FE5A00701E6F1B00000A188C2D0000016F1C00000A001106736700000A130700733600000A0B1107076F6800000A2600DE14110714FE01130C110C2D0811076F1E00000A00DC0000DE14110614FE01130C110C2D0811066F1E00000A00DC00076F4300000A6F4500000A16FE02130C110C2D090017130BDD8C02000006166F6200000A000672245B00706F1600000A260672605B00706F1600000A260672BC5B00706F1600000A260672FA5B00706F1600000A2606727D5C00706F1600000A260672D75C007009286600000A6F1600000A26066F1700000A0E046F1800000A0E04731900000A13060011066F1A00000A72ED5C0070166F1B00000A130811066F1A00000A72035D00701E6F1B00000A11048C0A0000026F1C00000A0011066F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A0011066F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011066F1A00000A72BE5A00701E6F1B00000A178C090000026F1C00000A0011066F1A00000A72DE5A00701E6F1B00000A188C090000026F1C00000A0011066F1A00000A72FE5A00701E6F1B00000A188C2D0000016F1C00000A0000076F4300000A6F4A00000A130E389F000000110E6F4B00000A740B00000113090011081109166F6900000A6F1C00000A001109166F6900000AA527000001156AFE0116FE01130C110C2D13000516540450087BA70400047DA70400042B6411066F3000000A130A00110A6F3100000A130C110C2D0300DE3905254A1758540450257BA7040004110A166F6100000A282600000A7DA704000400DE14110A14FE01130C110C2D08110A6F1E00000A00DC000000110E6F4C00000A130C110C3A50FFFFFFDE1D110E7505000001130F110F14FE01130C110C2D08110F6F1E00000A00DC0000DE14110614FE01130C110C2D0811066F1E00000A00DC00045004507BA5040004087BA5040004282C00000A7DA5040004045004507BA6040004087BA6040004282C00000A7DA6040004045004507BA7040004087BA7040004282C00000A7DA704000417130BDE0B260000DE000016130B2B0000110B2A0041940000020000007E0200001300000091020000140000000000000002000000DE010000CB000000A90200001400000000000000020000007904000034000000AD0400001400000000000000020000001F040000B7000000D60400001D000000000000000200000047030000B0010000F70400001400000000000000000000000B000000510500005C050000050000000100000113300800B4000000010000110073F50000060A027B4404000416FE01130411042D15000617721B5D00706FF100000600060D388700000000166A027B43040004281101000616731100000A1612020328CD000006130411042D1300061F0A72010000706FF100000600060D2B51027B420400041201032838010006130411042D1300061F1472010000706FF100000600060D2B2A00027B410400040708027B44040004027B45040004027B46040004027B4704000403289F0000060D2B00092A1330080018000000280000110002030405162811010006160E04289F0000060A2B00062A13300B0088010000290000110073F50000060A72755D0070727F5D00700E0728EF0000061201284900000A260716FE010D092D1600061F1572A55D00706FF100000600060C3848010000032C0E037BEB04000416FE0116FE012B0116000D092D1600061F1472010000706FF100000600060C381B010000037BF00400042D0B037BF104000416FE012B0116000D092D1600061F1572010000706FF100000600060C38EC000000042C0F047BB0040004166AFE0116FE012B0116000D092D1600061F0A72010000706FF100000600060C38BE000000047BB104000416FE010D092D1600061F0B72010000706FF100000600060C389B000000050D092D6100047BB8040004166AFE010D092D5100037BED0400041B3321047BB9040004037BEB0400043313047BBA0400042D0B047BBB04000416FE012B0116000D092D1F0006047BB80400047D48040004061F0C72010000706FF100000600060C2B3700000402037BEB040004037BED040004037BEE040004050E040E050E0612000E0728A00000062606047BB50400047D4F040004060C2B00082A1B300900D00800002A000011001613170E0973F5000006510E09501772010000706FF100000600027BB0040004130F001613111613121613190E05131C72755D007072F15D00700E0A28EF00000672075E00706F6A00000A131D111C16FE01132911292D37000E06182E1D0E061733140E076F1001000616731100000A282700000A2B0116002B01170013110E061A3304111D2B0116001319002B06000E0813120011192D0F111D2C07111116FE012B0117002B011600132911292D050016131C0011112D0411122B0117001313111116FE01132911292D05001A131700111216FE01132911292D0E0011171A60131711171E60131700166A131B72A636007072C63600700E0A28EF000006131A720B5E00700E0A6F1800000A0E0A731900000A131E00111E6F1A00000A720D0500701F0C1F326F2D00000A048C2D0000016F1C00000A00111E6F3000000A131F00111F6F3100000A132911292D18000E09501772C65E00706FF100000600161328DD64070000111F166F6B00000A131A111F176F6C00000A2D0A111F176F3200000A2B02166A00131B00DE14111F14FE01132911292D08111F6F1E00000A00DC0000DE14111E14FE01132911292D08111E6F1E00000A00DC00111116FE01132911293A3C010000001B1310166A130F731500000A130E110E722A5F00706F1600000A26110E72865F00706F1600000A26110E72E25F00706F1600000A26110E723E6000706F1600000A26110E729A6000706F1600000A26110E72F66000706F1600000A26110E72526100706F1600000A26110E6F1700000A0E0A6F1800000A0E0A731900000A13200011206F1A00000A720D0500701E6F1B00000A048C2D0000016F1C00000A0011206F1A00000A72AE6100701E6F1B00000A168C110000026F1C00000A0011206F3000000A13210011216F3100000A16FE01132911292D33001121166F6C00000A2D0A1121166F3200000A2B02166A00130F1121176F6C00000A2D0A1121176F3300000A2B01160013100000DE14112114FE01132911292D0811216F1E00000A00DC0000DE14112014FE01132911292D0811206F1E00000A00DC0000110F7E6D00000A281101000616731100000A16111712140E0A28CE000006132911292D18000E09501772C06100706FF100000600161328DD8B05000011147BBF0400040B11147BC0040004130811147BB8040004130B11147BBE0400041305111216FE01132911292D2A0016731100000A077BAC040004282400000A131516731100000A077BAD040004282400000A1316002B340016731100000A131616731100000A0E077BA7040004077BA7040004077BAC040004282500000A282500000A282400000A1315000E0617FE0116FE01132911292D4A0007077BA70400040E077BA7040004282C00000A7DA704000407077BA60400040E077BA6040004282C00000A7DA604000407077BA50400040E077BA5040004282C00000A7DA504000400027BB00400041322111916FE01132911292D0600111B13220011220405111C0E060E07120012070E0A28A5000006132911292D18000E095017721C6200706FF100000600161328DD5B040000111316FE01132911292D18000E09501772566200706FF100000600161328DD38040000111116FE01132911293AFB0000000016130A110F046A060307111D16FE01120312230E0A28BC000006132911292D18000E095017729E6200706FF100000600161328DDF1030000096F1001000616731100000A286E00000A16FE01132911292D6F000E0950171C8D28000001132A112A1672DC620070A2112A17096F10010006132B122B282200000AA2112A18724C630070A2112A190E076F10010006132B122B282200000AA2112A1A727A630070A2112A1B0F02286F00000AA2112A287000000A6FF100000600161328DD68030000111D132911292D2E00110F09046A11230E0A28D9000006132911292D18000E095017729A6300706FF100000600161328DD3303000000002B7C00110B166AFE01130A110A2D07111C16FE012B011600132911292D3100110F166A12241225120C0E0A28B4000006132911292D18000E09501772F66300706FF100000600161328DDE302000000110F046A06030712030E0A28BA000006132911292D18000E095017729E6200706FF100000600161328DDB40200000009097BA60400041F64731100000A287100000A287200000A1F64731100000A282900000A7DA604000409097BA70400041F64731100000A287100000A287200000A1F64731100000A282900000A7DA7040004110F09281901000616731100000A12021209120B12050E0A28CB000006132911292D23000E095017723A640070096F1700000A286600000A6FF100000600161328DD1B020000040E0406731300000613061106166A110F1B076F10010006096F1001000616731100000A086F100100066F15000006132911292D18000E09501772A06400706FF100000600161328DDCE01000011060E0A6F1F000006132911292D18000E09501772086500706FF100000600161328DDA7010000110F0E0A28CF000006132911292D18000E095017724E6500706FF100000600161328DD8001000072A636007072C63600700E0A28EF0000061326091126111A12180E0A28A2000006132911292D2F000E095017727E6500701200281F00000A729E650070096F1700000A287300000A6FF100000600161328DD2A010000110A061118111C11190E0A28D4000006132911292D5D000E0950171C8D28000001132A112A1672C6650070A2112A17120A287400000AA2112A187222660070A2112A191200281F00000AA2112A1A729E650070A2112A1B096F1700000AA2112A287000000A6FF100000600161328DDB700000073090100061304111C132911292D3B001107110F03091204120C120D0E0A28D2000006132911292D20000E0950177246660070096F1700000A286600000A6FF100000600161328DE6E000E0950067D480400040E0950110A7D490400040E0950077D4A0400040E0950097D4B0400040E095011047D4D0400040E0950087D4C0400040E095011097D4E0400040E09506FF300000600171328DE1D1327000E09501711276F7500000A6FF10000060000DE00001613282B000011282A417C000002000000420100004C0000008E010000140000000000000002000000170100008F000000A60100001400000000000000020000009002000047000000D70200001400000000000000020000004A020000A5000000EF020000140000000000000000000000230000008C080000AF08000017000000390000011B3004002D0100000A000011000502810800000103046F6A00000A16FE01130411042D0800170D380A01000000731500000A0A0672B06600706F1600000A260672106700706F1600000A260672706700706F1600000A260672D06700706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A727F0500701F096F1B00000A028C080000016F1C00000A00076F1A00000A72306800701F0C6F1B00000A036F1C00000A00076F1A00000A72446800701F0C6F1B00000A046F1C00000A00076F3000000A0C00086F3100000A16FE01130411042D1C0005080872546800706F7600000A6F6100000A8108000001170DDE3A00DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE062600160DDE0500160D2B0000092A000000012800000200C4002FF300120000000002006C009D0901120000000000002000FF1F01063900000113300500AF0000002B0000110005025103046F6A00000A16FE010C082D0800170B3893000000027BA5040004030405507CA50400040E0428A10000060A062C18027BA6040004030405507CA60400040E0428A10000062B0116000A062C18027BA7040004030405507CA70400040E0428A10000062B0116000A062C18027BAC040004030405507CAC0400040E0428A10000062B0116000A062C18027BAD040004030405507CAD0400040E0428A10000062B0116000A060B2B00072A001B3003002C0100002C0000110000731500000A0A0672686800706F1600000A260672EB6800706F1600000A260672DC6900706F1600000A260672656A00706F1600000A260516FE010D092D26000672E56A00706F1600000A260672596B00706F1600000A260672CD6B00706F1600000A26000672416C00706F1600000A260672496C00706F1600000A260672C96C00706F1600000A260672526D00706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1A00000A720D0500701E6F1B00000A038C2D0000016F1C00000A00076F1A00000A7227050070166F1B00000A048C270000016F1C00000A00076F1D00000A18FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A4134000002000000AB000000640000000F010000100000000000000000000000010000001E0100001F01000005000000010000011B3003004F0100002C0000110000731500000A0A0672D56D00706F1600000A2606726E6E00706F1600000A2606720B6F00706F1600000A260672AC6F00706F1600000A260672207000706F1600000A260672947000706F1600000A260672087100706F1600000A2606728B7100706F1600000A260672107200706F1600000A260672416C00706F1600000A2606729B7200706F1600000A2606720F7300706F1600000A260672837300706F1600000A260672087400706F1600000A2606728B7400706F1600000A26066F1700000A056F1800000A05731900000A0B00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1A00000A720D0500701E6F1B00000A038C2D0000016F1C00000A00076F1A00000A7227050070166F1B00000A048C270000016F1C00000A00076F1D00000A26170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A004134000002000000CF000000630000003201000010000000000000000000000001000000410100004201000005000000010000011B3007001D0400002D000011000E06166A550E071654160B166A0C160D1613047201000070130516130C16130D16731100000A130E16731100000A130F16731100000A131016731100000A131116731100000A1312731500000A0A000672167500706F1600000A2606725A7500706F1600000A260672A67500706F1600000A260672CC7500706F1600000A260672F67500706F1600000A260672287600706F1600000A260672667600706F1600000A260672AA7600706F1600000A260672F47600706F1600000A2606723E7700706F1600000A260672947700706F1600000A260672EA7700706F1600000A260672367800706F1600000A2606728E7800706F1600000A2606724D7900706F1600000A26066F1700000A0E086F1800000A0E08731900000A13130011136F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011136F3000000A13140011146F3100000A131611162D0900161315DDC00200001114166F6C00000A2D0A1114166F3300000A2B0116000B1114176F6C00000A2D0A1114176F3200000A2B02166A000C1114186F6C00000A2D0A1114186F3300000A2B0116000D1114196F6C00000A2D0A1114196F3300000A2B011700130405131611162D350011141A6F6C00000A131611162D2500120611141A6F6B00000A1628470100061307110716FE01131611162D06001106130500000011141B6F6C00000A2D0A11141B6F7700000A2B011600130811141C6F3300000A130C11141D6F3300000A130D11141E6F6100000A130E11141F096F6100000A130F11141F0A6F6100000A131011141F0B6F6100000A131111141F0C6F6100000A131200DE14111414FE01131611162D0811146F1E00000A00DC0000DE14111314FE01131611162D0811136F1E00000A00DC00052C070E0416FE022B011700131611163AF800000000110C163053110D16304E110E16731100000A282B00000A2D3F110F16731100000A282B00000A2D30111016731100000A282B00000A2D21111116731100000A282B00000A2D12111216731100000A282B00000A16FE012B011600131611163A930000000018081209120A120B0E0828E6000006131611162D0900161315DDFC00000011092D07110A16FE012B011700131611162D1900080E0828EC000006131611162D0900161315DDD10000000002091105030412020E0828A6000006131611162D0900161315DDB2000000020308160E0828A3000006131611162D0900161315DD980000000E0717540E060855171315DD880000000008166AFE0116FE01131611162D400002091105030412020E0828A6000006131611162D0600161315DE5D020308170E0828A3000006131611162D0600161315DE460E0717540E060855171315DE390307330F11042D0B041B3307110816FE012B011600131611162D0600161315DE180E0718540E060855171315DE0B260000DE00001613152B000011152A000000414C0000020000004201000016010000580200001400000000000000020000001A01000056010000700200001400000000000000000000004F000000BF0300000E04000005000000010000011B300300090300002E000011000E05166A55731500000A0B160A03130511051759450500000002000000060000000A0000000E000000060000002B0C170A2B0C180A2B08190A2B04160A2B000007166F6200000A0007729B7900706F1600000A260772651200706F1600000A260772EF7900706F1600000A260772697A00706F1600000A260772E37A00706F1600000A2607725D7B00706F1600000A260772651200706F1600000A260772D77B00706F1600000A260772237C00706F1600000A2607726F7C00706F1600000A260772BB7C00706F1600000A260772651200706F1600000A260772077D00706F1600000A260772697D00706F1600000A260772CB7D00706F1600000A2607722D7E00706F1600000A2607728F7E00706F1600000A260772F17E00706F1600000A260772537F00706F1600000A260772B57F00706F1600000A260772178000706F1600000A260772798000706F1600000A260772DB8000706F1600000A2607723D8100706F1600000A2607729F8100706F1600000A2607729F8100706F1600000A260772018200706F1600000A260772C08200706F1600000A260772651200706F1600000A260772248300706F1600000A26076F1700000A0E066F1800000A0E06731900000A0D00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00096F1A00000A72768300701E6F1B00000A058C2D0000016F1C00000A00096F1A00000A72928300701E6F1B00000A068C0E0000026F1C00000A00096F1A00000A72A68300701F096F1B00000A168C2D0000016F1C00000A00096F1A00000A72C88300701F196F1B00000A04283500000A2D03042B057E2F00000A006F1C00000A00096F1A00000A72DE830070186F1B00000A0E041BFE018C430000016F1C00000A00096F1A00000A7227050070166F1B00000A0C08186F4E00000A00096F1D00000A17FE01130611062D0600161304DE50086F6000000A7E2F00000AFE0116FE01130611062D0600161304DE340E05086F6000000AA5270000015500DE120914FE01130611062D07096F1E00000A00DC00171304DE0B260000DE00001613042B000011042A0000004134000002000000C60100001C010000E202000012000000000000000000000040000000BA020000FA02000005000000010000011B300300860000002F000011000072F8830070046F1800000A04731900000A0A00066F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A00066F1A00000A72B98400701E6F1B00000A028C270000016F1C00000A00066F1D00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1E00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300300F203000030000011000E06156A55731500000A0A0E056F1001000616731100000A282B00000A16FE01130711072D06001B0D002B04001C0D0016731100000A130402130811081F14302111084503000000380000003D0000004B00000011081F0A2E4A11081F142E492B5511081F1E2E3C110820E7030000594503000000360000002300000023000000110820F20300002E1A2B2B1F640C2B2E1F650C0E056F1001000613042B201F660C2B1B1F6E0C2B161F780C0E056F1001000613042B08161306382E030000000672DB8400706F1600000A260672318500706F1600000A260672898500706F1600000A260672D38500706F1600000A260672338600706F1600000A2606728F8600706F1600000A260672E98600706F1600000A260672458700706F1600000A260672998700706F1600000A260672EF8700706F1600000A2606723F8800706F1600000A260672918800706F1600000A260672DF8800706F1600000A2606722F8900706F1600000A260672818900706F1600000A260672D98900706F1600000A260672258A00706F1600000A2606725F8A00706F1600000A260672B18A00706F1600000A260672058B00706F1600000A2606724D8B00706F1600000A260672A98B00706F1600000A260672018C00706F1600000A260672018C00706F1600000A260672018C00706F1600000A2606723F8C00706F1600000A260672018C00706F1600000A260672018C00706F1600000A260672918C00706F1600000A260672DD8C00706F1600000A260672DD8C00706F1600000A2606722B8D00706F1600000A260672018C00706F1600000A260672258A00706F1600000A2606727F8D00706F1600000A26066F1700000A0E076F1800000A0E07731900000A13050011056F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A0011056F1A00000A720D0500701E6F1B00000A048C2D0000016F1C00000A0011056F1A00000A72592200701E6F1B00000A088C0E0000026F1C00000A0011056F1A00000A72AE6100701E6F1B00000A098C110000026F1C00000A0011056F1A00000A72672200701F096F1B00000A0E046F100100068C080000016F1C00000A0011056F1A00000A72068E00701F096F1B00000A11048C080000016F1C00000A0011056F1A00000A72B92200701F096F1B00000A0E046F100100060E056F10010006282600000A8C080000016F1C00000A0011056F1A00000A72DE830070186F1B00000A051BFE018C430000016F1C00000A0011056F1A00000A7227050070166F1B00000A0B07186F4E00000A0011056F1D00000A17FE01130711072D0600161306DE320E06076F6000000AA5270000015500DE14110514FE01130711072D0811056F1E00000A00DC00171306DE072600161306DE000011062A000041340000020000007B02000052010000CD030000140000000000000000000000C000000027030000E703000007000000010000011B3003005C0200000A000011000573F70000065100731500000A0A06721E8E00706F1600000A260219FE0116FE01130411042D340006724E8E00706F1600000A260672458F00706F1600000A260672A38F00706F1600000A2606729A9000706F1600000A26002B320006724E8E00706F1600000A260672F89000706F1600000A260672A38F00706F1600000A260672EF9100706F1600000A26000672E69200706F1600000A2606722E9300706F1600000A260672809300706F1600000A260672FE9300706F1600000A2606727C9400706F1600000A260672FA9400706F1600000A260672789500706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F1A00000A72D6950070166F1B00000A166A047B64040004287800000A8C270000016F1C00000A00076F1A00000A72F29500701F096F1B00000A16731100000A047B65040004282400000A8C080000016F1C00000A00076F1A00000A7210960070166F1B00000A166A047B66040004287800000A8C270000016F1C00000A00076F1A00000A72068E00701F096F1B00000A16731100000A047B67040004282400000A8C080000016F1C00000A00076F3000000A0C00086F3100000A130411042D0500160DDE7D055008166F3300000A6A7D64040004055008176F6100000A7D65040004055008186F3300000A6A7D66040004055008196F6100000A7D67040004055016731100000A7D6804000400DE120814FE01130411042D07086F1E00000A00DC00170DDE1C0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A414C000002000000CA0100005C00000026020000120000000000000002000000F7000000460100003D02000012000000000000000000000008000000470200004F02000005000000010000011B300800D808000031000011000E047305010006510E04501772010000706F0101000600057B5B0400041308057B5F040004130911092C0C057B54040004166AFE012B011700131111112D05001613080000057B55040004166A310F057B56040004166AFE0416FE012B011600131111112D38000E0450187226960070057C55040004281F00000A7278960070057C56040004281F00000A287300000A6F0101000600161310DD35080000110816FE01131111112D0C00057B580400041305002B150016731100000A057B58040004282400000A1305001105057B58040004282700000A16FE01131111112D2C000E0450727E960070057C58040004282200000A72B69600701205282200000A287300000A6F02010006000003131211121759450500000002000000670000006500000067000000070000002B653887000000057B56040004166A2E08057B590400042B011600131111112D4200057B520400041F20FE01131111112D30000E045072C2960070057C56040004281F00000A721C970070057C59040004287400000A287300000A6F020100060000002B292B270E045018723C970070038C190000026F1700000A286600000A6F0101000600161310DD19070000057B520400041CFE0116FE01131111112D360003057B55040004057B5A040004120A0E0528A9000006131111112D18000E04501772709700706F0101000600161310DDD2060000000E0572BE9700706F1200000A00110816FE01131111112D0F000528C200000613060038B600000000057B5504000412060E0528C1000006131111112D18000E04501772EA9700706F0101000600161310DD7C060000110916FE01131111112D4D001106177D8F0400041106057B600400047D900400041106057B5A0400047B650400047D860400041106057B5A0400047B670400047D870400041106057B61040004730A0100067D910400040011060E04507B820400040E0528BF000006131111112D18000E04501772269800706F0101000600161310DDF5050000001106130711067B8C040004730A0100060A06730A0100060B031312111217594505000000020000003B0000000E0000003B0000000C0000002B39066F1001000613052B562B54057B520400041F20FE0116FE01131111112D1800057B57040004131111112D0A00066F10010006130500002B270E045018723C970070038C190000026F1700000A286600000A6F0101000600161310DD5A050000066F100100061105282700000A0D090C0916FE01131111113A4B0200000006730A0100060B1105066F10010006282500000A130B160C031B2E090317FE0116FE012B011600131111112D4100726498007072E70600700E0528EF000006120C282800000A26110C1F64731100000A282900000A130C110B110C282B00000A16FE01131111112D0400170C00000319FE0116FE01131111112D460072CD06007072E70600700E0528EF000006120C282800000A26110C1F64731100000A282900000A130C110B282A00000A110C282B00000A16FE01131111112D0400170C00007306010006130D110D057B550400047D83040004110D11067B840400047D84040004110D06730A0100067D8C040004110B16731100000A282B00000A16FE01131111112D1A00110D16731100000A7D86040004110D110B7D87040004002B1D00110D110B282A00000A7D86040004110D16731100000A7D8704000400110816FE01131111112D110073090100060B7309010006130E002B4200110D0E04507B820400040E0528BF000006131111112D18000E04501772269800706F0101000600161310DDB3030000110D7B8C040004130E110E730A0100060B00031B2E090317FE0116FE012B011600131111112D430002057B550400040E0528ED000006131111112D2D000E04501772749800700F00286F00000A72C2980070057C55040004281F00000A287300000A6F01010006000000057B520400041F20FE0116FE01131111112D0600160D160C000816FE01131111112D3300110E6F10010006066F10010006282B00000A16FE01131111112D090006730A0100060B000E0572BE9700706F1300000A0000001108131111112D3C00057B55040004076F10010006110509057B520400040E0528E7000006131111112D18000E04501772E49800706F0101000600161310DDAF0200000011092C37057B630400046F1001000616731100000A282B00000A2D1B057B620400046F1001000616731100000A282B00000A16FE012B0116002B011700131111112D3900057B55040004057B63040004057B620400040E0528AB000006131111112D18000E04501772329900706F0101000600161310DD33020000000816FE01131111112D4C000E04501A720F090070057B580400048C08000001066F1700000A076F1700000A282000000A6F01010006000E045007730A0100067D800400041108131111112D0900171310DDDD01000000730701000613041104057B520400047D920400041104057B530400047D930400041104057B550400047D940400041104057B540400047D950400041104027D960400041104047D970400041104097D98040004110407730A0100067D9904000411040E04507B820400047D9A0400041104057B5A0400047B640400047D9B040004110411077B8D0400047D9C0400041104057B5A0400047B660400047D9D040004110411077B8E0400047D9E0400041104057B5E0400047DA20400041104057B5F0400047DA30400041104057B5A0400047B6A0400047DA4040004110816FE01131111113A93000000001104177D9F0400041104057B5C0400047DA00400041104057B5D0400047DA1040004057B55040004057B5D0400047B6C040004057B5D0400047B6D040004057B5D0400047B6B040004057B5D0400047B6E040004057B5D0400047B6F040004057B5D0400047B700400047B730400040E0528B2000006131111112D15000E04501772849900706F0101000600161310DE610011040E04500E0528AD000006131111112D15000E04501772CE9900706F0101000600161310DE390E045007730A0100067D800400040E04506F0301000600171310DE1D130F000E045017110F6F7500000A6F010100060000DE00001613102B000011102A411C0000000000004500000072080000B708000017000000390000011B300300620100002C00001100731500000A0A0672089A00706F1600000A260672369A00706F1600000A260672989A00706F1600000A260672FC9A00706F1600000A260672689B00706F1600000A260672D49B00706F1600000A2600066F1700000A056F1800000A05731900000A0B00076F1A00000A723A9C0070166F1B00000A028C270000016F1C00000A00076F1A00000A725A9C00701F096F1B00000A037BA504000416731100000A282400000A8C080000016F1C00000A00076F1A00000A72789C00701F096F1B00000A036F0F01000616731100000A282400000A8C080000016F1C00000A00076F1A00000A72969C00701F096F1B00000A047BA504000416731100000A282400000A8C080000016F1C00000A00076F1A00000A72BC9C00701F096F1B00000A046F0F01000616731100000A282400000A8C080000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000413400000200000063000000E2000000450100001000000000000000000000004F000000060100005501000005000000010000011B300300F90000003200001100031652160B00731500000A0A0672E29C00706F1600000A260672529D00706F1600000A260672C29D00706F1600000A260672329E00706F1600000A260672A29E00706F1600000A260672129F00706F1600000A26066F1700000A046F1800000A04731900000A0D00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00096F1A00000A72829F00701E6F1B00000A1B8C0F0000026F1C00000A00096F2E00000A0C00DE120914FE01130611062D07096F1E00000A00DC00082C0A087E2F00000AFE012B011700130611062D090008A52D0000010B00030716FE015200DE08130400161305DE06001713052B000011052A000000011C00000200680045AD00120000000000000600E1E70008390000011B3009007D0600003300001100027B9F040004130C027BA30400042C0C027B95040004166AFE012B011700131611162D050016130C00110C131611162D1400027B95040004130D027B95040004130E002B2500027B95040004166A3308027BA00400042B06027B9504000400130D027BA0040004130E00027BA30400042C0C027B95040004166AFE012B011700131611163ACC00000000027B95040004130D027BA0040004130E110E166AFE0116FE01131611163AA8000000000072B49F0070046F1800000A04731900000A13120011126F1A00000A720D0500701F0C1F326F2D00000A027B960400048C2D0000016F1C00000A0011126F2E00000A131111112C0B11117E2F00000AFE012B011700131611162D0B001111A527000001130E0000DE14111214FE01131611162D0811126F1E00000A00DC0000DE2413130003177255A0007011136F1700000A286600000A6F0101000600161315DD2605000000000073090100060A73090100060B027B96040004120A042838010006131611162D1600031772D5A000706F010100060016131538ED040000110C131611162D3F00027B95040004027B9904000412010428C7000006131611162D16000317720DA100706F010100060016131538B404000007027B9904000428140100060A00110D0428CF000006131611162D16000317724E6500706F01010006001613153882040000110C131611162D6B0019027B95040004166A027B990400041202120312040428D2000006131611162D16000317724FA100706F01010006001613153842040000027B95040004027B94040004166A0428B5000006131611162D160003177295A100706F01010006001613153812040000002B4B00027BA10400046FF8000006027BA10400046FF9000006027BA10400046FFA000006730B0100060C027BA10400046FFC00000616731100000A027BA10400046FFD000006730B0100060D00027B92040004131711171C2E0811171F202E0F2B15171306027B9604000413092B321913061613092B2A031872E1A10070027B920400048C0D0000026F1700000A286600000A6F01010006001613153870030000027B9404000408027B9C040004027B9E04000409027BA40400040428B1000006131611162D160003177215A200706F010100060016131538340300001106027B9404000412071208120B0428E6000006131611162D160003177257A200706F0101000600161315380403000011072D07110816FE012B011700131611162D2A00027B940400040428EC000006131611162D16000317729FA200706F010100060016131538C802000000110E027B96040004027B940400040428A4000006131611162D1600031772EFA200706F01010006001613153897020000110C16FE01131611162D2C00027B94040004110D0428A7000006131611162D160003177233A300706F0101000600161315386102000000027B9C0400046F1001000616731100000A282B00000A16FE01131611163AE200000000110D0428B3000006131611162D16000317726FA300706F01010006001613153819020000110A027B9C0400040428B0000006131611162D1600031772BBA300706F010100060016131538EF010000027B920400041C3308027B980400042B011700131611162D7900110C16FE01131611162D4200027B94040004027BA10400047B6E040004027BA10400046FF80000060428B8000006131611162D160003177203A400706F0101000600161315388B010000002B2A00027B950400040428B7000006131611162D16000317724BA400706F0101000600161315385F0100000000001109027B97040004027B94040004027B9304000473140000061305027B950400041314110C16FE01131611162D0A00027BA00400041314001105166A1114027B92040004066F1001000616731100000A027B990400046F10010006076F10010006027BA20400046F17000006131611162D30000317728DA40070027B920400048C0D0000026F1700000A72C3A40070284000000A6F010100060016131538BA0000001105046F1F000006131611162D1600031772086500706F01010006001613153896000000120F0428D6000006131611162D050016130F00110F16FE0116FE01131611162D7000027B9C0400046F0E01000616731100000A282B00000A16FE01131611162D4F00027B9504000412100428AC000006131611162D0500161310001110131611162D2C00110B0203110A7BED0400040428AF000006131611162D1300031772D7A400706F01010006001613152B080000001713152B000011152A000000011C00000200C20055170114000000000000AE00812F01243900000113300800BB0100003400001100731500000A0D0312020E0428C1000006130C110C2D090016130B389801000008090E0428BF000006130C110C2D090016130B3880010000087B8C040004730A01000613041104730A010006130511046F10010006056F10010006282700000A1306110616FE01130C110C2D0200000311056F10010006056F1001000611061C0E0428E7000006130C110C2D090016130B382201000002110512000E0428C7000006130C110C2D090016130B380701000006110528140100060B1902166A110512081209120A0E0428D2000006130C110C2D090016130B38DC0000000203166A0E0428B5000006130C110C2D090016130B38C2000000031108087B8D040004087B8E040004110916731100000A0E0428B1000006130C110C2D090016130B38950000000204030E0428A4000006130C110C2D060016130B2B7F087B8D0400046F1001000616731100000A282B00000A16FE01130C110C2D5B00020E0428B3000006130C110C2D060016130B2B4B0412070E042838010006130C110C2D060016130B2B351107087B8D0400040E0428B0000006130C110C2D060016130B2B1A020E0428B7000006130C110C2D060016130B2B060017130B2B00110B2A001B300300640200002C0000110000731500000A0A067219A500706F1600000A26067289A500706F1600000A260672F9A500706F1600000A26067269A600706F1600000A260672D9A600706F1600000A26067249A700706F1600000A260672B9A700706F1600000A26067229A800706F1600000A26067299A800706F1600000A26067209A900706F1600000A26067279A900706F1600000A260672E9A900706F1600000A26067259AA00706F1600000A260672C9AA00706F1600000A26067239AB00706F1600000A260672A9AB00706F1600000A26067219AC00706F1600000A26067289AC00706F1600000A260672F9AC00706F1600000A26067269AD00706F1600000A260672D9AD00706F1600000A26067249AE00706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A72B9AE0070166F1B00000A037B940400048C270000016F1C00000A00076F1A00000A72D9AE0070166F1B00000A037B950400048C270000016F1C00000A00076F1A00000A72F9AE00701F096F1B00000A037B9C0400046F0E0100068C080000016F1C00000A00076F1A00000A7213AF00701E6F1B00000A037B960400048C2D0000016F1C00000A00076F1A00000A7235AF00701E6F1B00000A0F00287900000A8C310000016F1C00000A00076F1A00000A726DAF0070166F1B00000A037B9B0400048C270000016F1C00000A00076F1A00000A7299AF0070186F1B00000A037B980400048C430000016F1C00000A00076F1A00000A72D3AF00701E6F1B00000A058C190000026F1C00000A00076F1D00000A2600DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A4134000002000000250100001D0100004202000010000000000000000000000001000000560200005702000005000000010000011B300300C40000002C0000110000027BF70400040D092D0800170CDDAD000000731500000A0A0672F9AF00706F1600000A2606723BB000706F1600000A26067234B100706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A7266B100701F096F1B00000A036F0E0100068C080000016F1C00000A00076F1A00000A7296B100701F096F1B00000A036F100100068C080000016F1C00000A00076F1D00000A2600DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A011C00000200510051A200100000000000000100B6B70005010000011B30030047030000350000110000731500000A0A06166F6200000A000672B2B100706F1600000A260672E4B100706F1600000A26067267B200706F1600000A260672DBB200706F1600000A26067245B300706F1600000A260672B9B300706F1600000A2606723CB400706F1600000A260672B2B400706F1600000A2606721EB500706F1600000A26067294B500706F1600000A26067214B600706F1600000A2606728EB600706F1600000A260672FEB600706F1600000A26067278B700706F1600000A260672ECB700706F1600000A26067256B800706F1600000A260672CAB800706F1600000A26066F1700000A0E066F1800000A0E06731900000A0B00076F1A00000A7240B900701F096F1B00000A036F0F0100068C080000016F1C00000A00076F1A00000A726CB900701F096F1B00000A036F0E0100068C080000016F1C00000A00076F1A00000A728CB900701F096F1B00000A037BA50400048C080000016F1C00000A00076F1A00000A72A2B900701F096F1B00000A037BA60400048C080000016F1C00000A00076F1A00000A72C2B900701F096F1B00000A0E046F0F0100068C080000016F1C00000A00076F1A00000A72F0B900701F096F1B00000A0E046F0E0100068C080000016F1C00000A00076F1A00000A7212BA00701F096F1B00000A0E047BA50400048C080000016F1C00000A00076F1A00000A722ABA00701F096F1B00000A0E047BA60400048C080000016F1C00000A00076F1A00000A724CBA00701F096F1B00000A046F0F0100068C080000016F1C00000A00076F1A00000A7278BA00701F096F1B00000A046F0E0100068C080000016F1C00000A00076F1A00000A7296B100701F096F1B00000A046F100100068C080000016F1C00000A00076F1A00000A729EBA00701F096F1B00000A056F0F0100068C080000016F1C00000A00076F1A00000A72C4BA00701F096F1B00000A056F0E0100068C080000016F1C00000A00076F1A00000A72E4BA00701F096F1B00000A056F100100068C080000016F1C00000A00076F1A00000A72FABA00701F096F1B00000A0E058C080000016F1C00000A00076F1A00000A72B9840070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010DDE1C0714FE01130411042D07076F1E00000A00DC0C0000DE0000160D2B0000092A004134000002000000F1000000370200002803000012000000000000000000000001000000390300003A03000005000000390000011B300300830100002C0000110000731500000A0A06166F6200000A000672B2B100706F1600000A2606721CBB00706F1600000A26067288BB00706F1600000A260672F4BB00706F1600000A26067256BC00706F1600000A260672C4BC00706F1600000A26067228BD00706F1600000A2606728EBD00706F1600000A26066F1700000A0E076F1800000A0E07731900000A0B00076F1A00000A72F8BD00701F096F1B00000A038C080000016F1C00000A00076F1A00000A721CBE00701F096F1B00000A048C080000016F1C00000A00076F1A00000A7240BE00701F096F1B00000A058C080000016F1C00000A00076F1A00000A725ABE00701F096F1B00000A0E058C080000016F1C00000A00076F1A00000A7280BE00701F096F1B00000A0E048C080000016F1C00000A00076F1A00000A729CBE00701F096F1B00000A0E068C080000016F1C00000A00076F1A00000A72B9840070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00413400000200000085000000E10000006601000010000000000000000000000001000000750100007601000005000000010000011B300300860000002C0000110000731500000A0A0672BABE00706F1600000A260672E2BE00706F1600000A2606723EBF00706F1600000A26066F1700000A036F1800000A03731900000A0B00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A690010000000000000010078790005010000011B300500D60200000A000011000416731100000A81080000010516731100000A81080000010E0428110100065100731500000A0A0672ACBF00706F1600000A26067231C000706F1600000A260672B6C000706F1600000A2606723BC100706F1600000A260672C0C100706F1600000A26066F7A00000A260672BABE00706F1600000A26067249C200706F1600000A26067299C200706F1600000A260672E9C200706F1600000A26067239C300706F1600000A26067289C300706F1600000A260672D9C300706F1600000A26067229C400706F1600000A26067285C400706F1600000A260672E1C400706F1600000A2606723DC500706F1600000A26067299C500706F1600000A260672F5C500706F1600000A26067251C600706F1600000A260672ADC600706F1600000A26067209C700706F1600000A26067265C700706F1600000A260672D5C700706F1600000A26067245C800706F1600000A260672B5C800706F1600000A26067225C900706F1600000A26067275C900706F1600000A260672BFC900706F1600000A26067217CA00706F1600000A2606727BCA00706F1600000A260672DFCA00706F1600000A2606724D7900706F1600000A2603166AFE01130411042D0E0006720FCB00706F1600000A260006727FCB00706F1600000A260672ABCB00706F1600000A26066F1700000A0E056F1800000A0E05731900000A0B00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F3000000A0C00086F3100000A130411042D0800160DDD870000000408166F6100000A81080000010508176F6100000A81080000010E0408186F6100000A08196F6100000A081A6F6100000A730B0100065100DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC000203166A0E0528B5000006130411042D0500160DDE0E170DDE0A260000DE0000160D2B0000092A0000414C000002000000370200004F00000086020000120000000000000002000000F5010000A70000009C02000012000000000000000000000021000000A8020000C902000005000000010000011B3003001B030000360000110000731500000A0A0672D85900706F1600000A260672E7CB00706F1600000A2606726ACC00706F1600000A260672B95200706F1600000A2603166AFE01130711072D29000672C6CC00706F1600000A2604166AFE01130711072D0E00067228CD00706F1600000A2600002B0E0006728ACD00706F1600000A2600066F1700000A056F1800000A05731900000A0C00086F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00086F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A00086F1A00000A7227050070166F1B00000A038C270000016F1C00000A00086F1A00000A72E0CD0070166F1B00000A048C270000016F1C00000A0008736700000A0D00733600000A0B09076F6800000A2600DE120914FE01130711072D07096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00076F4300000A6F4500000A16FE0116FE01130711072D0900171306DDB301000006166F6200000A000672245B00706F1600000A26067200CE00706F1600000A2606724CCE00706F1600000A260672FA5B00706F1600000A26067298CE00706F1600000A260672F0CE00706F1600000A2606724ACF00706F1600000A2603166AFE01130711072D27000672C6CC00706F1600000A2604166AFE01130711072D0E00067228CD00706F1600000A260000066F1700000A056F1800000A05731900000A0C00086F1A00000A72ED5C0070166F1B00000A1304086F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00086F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A00086F1A00000A7227050070166F1B00000A038C270000016F1C00000A00086F1A00000A72E0CD0070166F1B00000A048C270000016F1C00000A0000076F4300000A6F4A00000A13082B2711086F4B00000A740B00000113050011041105166F6900000A6F1C00000A00086F1D00000A260011086F4C00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00171306DE0B260000DE00001613062B000011062A00417C00000200000009010000120000001B0100001200000000000000020000008D000000A4000000310100001200000000000000020000009B02000038000000D30200001D000000000000000200000005020000EF000000F4020000120000000000000000000000010000000B0300000C03000005000000010000011B3003003F020000360000110000731500000A0A0672D85900706F1600000A260672E7CB00706F1600000A260672AACF00706F1600000A260672FCCF00706F1600000A26067254D000706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00086F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0008736700000A0D00733600000A0B09076F6800000A2600DE120914FE01130711072D07096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00076F4300000A6F4500000A16FE0116FE01130711072D0900171306DD4701000006166F6200000A000672245B00706F1600000A26067292D000706F1600000A260672FA5B00706F1600000A26067265D100706F1600000A260672B5D100706F1600000A260672FCCF00706F1600000A26067254D000706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A7207D20070166F1B00000A1304086F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00086F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0000076F4300000A6F4A00000A13082B2711086F4B00000A740B00000113050011041105166F6900000A6F1C00000A00086F1D00000A260011086F4C00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00171306DE0B260000DE00001613062B000011062A00417C0000020000009900000012000000AB000000120000000000000002000000570000006A000000C1000000120000000000000002000000BF01000038000000F70100001D000000000000000200000063010000B500000018020000120000000000000000000000010000002F0200003002000005000000010000011B300400670300003700001100160C733600000A0B150D00731500000A0A0672D85900706F1600000A260672E7CB00706F1600000A260672AACF00706F1600000A260672FCCF00706F1600000A260672745800706F1600000A2606721BD200706F1600000A26066F1700000A036F1800000A03731900000A13040011046F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011046F1A00000A72BE5A00701E6F1B00000A178C090000026F1C00000A0011046F1A00000A72DE5A00701E6F1B00000A188C090000026F1C00000A0011046F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011046F1A00000A729ED200701E6F1B00000A198C0A0000026F1C00000A001104736700000A1305001105076F6800000A2600DE14110514FE01130911092D0811056F1E00000A00DC0000DE14110414FE01130911092D0811046F1E00000A00DC00160D076F4300000A6F4500000A16FE0116FE01130911092D0900171308DDF901000006166F6200000A000672245B00706F1600000A260672C2D200706F1600000A260672FA5B00706F1600000A26067265D100706F1600000A260672B5D100706F1600000A260672FCCF00706F1600000A2606721ED300706F1600000A260672745800706F1600000A2606721BD200706F1600000A26066F1700000A036F1800000A03731900000A13040011046F1A00000A72ED5C0070166F1B00000A130611046F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011046F1A00000A72BE5A00701E6F1B00000A178C090000026F1C00000A0011046F1A00000A72DE5A00701E6F1B00000A188C090000026F1C00000A0011046F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011046F1A00000A729ED200701E6F1B00000A198C0A0000026F1C00000A0000076F4300000A6F4A00000A130A2B2A110A6F4B00000A740B00000113070011061107166F6900000A6F1C00000A000911046F1D00000A580D00110A6F4C00000A130911092DC9DE1D110A7505000001130B110B14FE01130911092D08110B6F1E00000A00DC0000DE14110414FE01130911092D0811046F1E00000A00DC00171308DE422600170C00DE0000DE3200082D13076F4300000A6F4500000A09FE0116FE012B011700130911092D1100021616731100000A0328EB000006260000DC001613082B000011082A0041940000020000000E0100000D0000001B0100001400000000000000020000006E000000C500000033010000140000000000000002000000AB0200003B000000E60200001D0000000000000002000000F201000015010000070300001400000000000000000000000B00000016030000210300000700000001000001020000000B000000200300002B03000032000000000000001B300300050100003800001100166A0C00731500000A0A06725CD300706F1600000A260672ACD300706F1600000A260672D6D300706F1600000A2606724CD400706F1600000A260672BCD400706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A7227050070166F1B00000A028C270000016F1C00000A00096F1A00000A7210D500701E6F1B00000A188C2D0000016F1C00000A00096F2E00000A0B072C0A077E2F00000AFE012B011700130511052D090007A5270000010C0000DE120914FE01130511052D07096F1E00000A00DC0008166AFE0116FE01130511052D0600171304DE1D0304282500000A080528B90000061304DE0B260000DE00001613042B000011042A000000011C000002005A0063BD00120000000000000400F2F60005010000011B300300CC0000002C00001100731500000A0A06722AD500706F1600000A26067268D500706F1600000A260672FDD500706F1600000A2606726BD600706F1600000A2606720ED700706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7270D70070166F1B00000A038C270000016F1C00000A00076F1A00000A727F0500701F096F1B00000A028C080000016F1C00000A00076F1D00000A17FE0116FE010D092D0500170CDE2100DE100714FE010D092D07076F1E00000A00DC0000DE05260000DE0000160C2B0000082A011C00000200570054AB001000000000000043007CBF00050100000113300900180000003900001100020304050E04160E0512000E0628BC0000060B2B00072A1B300300760100002C0000110002165400731500000A0A06729AD700706F1600000A26067241D800706F1600000A260672E8D800706F1600000A2606728FD900706F1600000A26067236DA00706F1600000A260672DDDA00706F1600000A26067284DB00706F1600000A26067241D800706F1600000A2606722BDC00706F1600000A260672E8D800706F1600000A2606728FD900706F1600000A26067236DA00706F1600000A260672D2DC00706F1600000A26067279DD00706F1600000A26067220DE00706F1600000A26067241D800706F1600000A2606722BDC00706F1600000A260672C7DE00706F1600000A2606726EDF00706F1600000A26067215E000706F1600000A26067241D800706F1600000A260672BCE000706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A0002076F2E00000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A00004134000002000000260100002E0000005401000010000000000000000000000004000000650100006901000005000000010000011B3004001F0700003A000011000E07733600000A510E0616731100000A0E047BA5040004282400000A16731100000A0E047BA6040004282400000A16731100000A730B010006511206020E0828BB00000616FE01130C110C2D38001106172E0A110618FE0116FE012B011600130C110C2D1F000E060E046F12010006730A010006510E065016731100000A7DA7040004000004210000000000000080FE01130C110C2D26000E047BA704000416731100000A286E00000A16FE01130C110C2D090017130B385E0600000072010000701304160D731500000A0A0006166F6200000A00067263E100706F1600000A26067287E100706F1600000A260672C1E100706F1600000A2606723FE200706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A720D050070166F1B00000A038C270000016F1C00000A0011076F3000000A13080011086F3100000A130C110C2D090016130BDDC00500001108166F6B00000A13041108176F7700000A0D00DE14110814FE01130C110C2D0811086F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC000916FE01130C110C2D090017130BDD6A0500000E07506F3700000A728FE2007072190D0070283800000A6F3900000A260E07506F3700000A72ABE20070727F0D0070283800000A6F3900000A260E07506F3700000A72CBE2007072BF0D0070283800000A6F3900000A2606166F6200000A000672D85900706F1600000A260672E3E200706F1600000A2606726EE300706F1600000A260672CDE400706F1600000A26067203E500706F1600000A260672E7CB00706F1600000A26067231E500706F1600000A2606726ACC00706F1600000A260672B95200706F1600000A260672B4E500706F1600000A26067222E600706F1600000A260672745800706F1600000A260672BBE600706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011076F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011076F1A00000A72F7E600701E6F1B00000A178C090000026F1C00000A0011076F1A00000A720BE700701E6F1B00000A188C090000026F1C00000A0011076F1A00000A720D0500701E6F1B00000A038C270000016F1C00000A0011076F1A00000A721FE700701E6F1B00000A198C2E0000026F1C00000A0011076F3000000A1308002B5F001108176F3300000A16FE0116FE01130C110C2D03002B470E07506F4100000A13051105161108166F3200000A8C270000016F7B00000A001105181108186F6100000A8C080000016F7B00000A000E07506F4300000A11056F4400000A000011086F3100000A130C110C2D9400DE14110814FE01130C110C2D0811086F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC0006166F6200000A0006724DE700706F1600000A26067287E700706F1600000A260672E7E700706F1600000A26067247E800706F1600000A2606728BE800706F1600000A260672CFE800706F1600000A2606724FE900706F1600000A260672A5E900706F1600000A260672FDE900706F1600000A2606725BEA00706F1600000A260672C7EA00706F1600000A2606725EEB00706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A72ED5C0070166F1B00000A130911076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011076F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011076F1A00000A72F7E600701E6F1B00000A178C090000026F1C00000A0011076F1A00000A720BE700701E6F1B00000A188C090000026F1C00000A000E0516FE01130C110C2D3E0011076F1A00000A7227050070166F1B00000A7E2F00000A6F1C00000A0011076F1A00000A72E0CD0070166F1B00000A7E2F00000A6F1C00000A00002B3E0011076F1A00000A7227050070166F1B00000A048C270000016F1C00000A0011076F1A00000A72E0CD0070166F1B00000A058C270000016F1C00000A0000000E07506F4300000A6F4A00000A130D38A5000000110D6F4B00000A740B000001130A001109110A166F6900000A6F1C00000A0011076F3000000A13080011086F3100000A130C110C2D0300DE6C1108166F3300000A0B1108176F6100000A0C16731100000A08282400000A0C07130E110E4504000000180000000200000002000000180000002B160E0650257BA704000408282600000A7DA70400042B022B0000DE14110814FE01130C110C2D0811086F1E00000A00DC000000110D6F4C00000A130C110C3A4AFFFFFFDE1D110D7505000001130F110F14FE01130C110C2D08110F6F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC000E065016731100000A0E06507BA70400040E047BA7040004282C00000A282400000A7DA704000417130BDE0B260000DE000016130B2B0000110B2A0041C4000002000000440100002D000000710100001400000000000000020000001C0100006D000000890100001400000000000000020000008003000072000000F2030000140000000000000002000000C2020000480100000A0400001400000000000000020000001E0600006700000085060000140000000000000002000000F1050000BD000000AE0600001D0000000000000002000000CD04000002020000CF060000140000000000000000000000CD0000004306000010070000050000000100000113300300500000003B00001100731500000A0B0328110100065102166AFE0116FE010D092D0500160C2B2F0212000428C10000060D092D0500160C2B1D06070428BF0000060D092D0500160C2B0C03067B8C04000451170C2B00082A1B300300810000003C0000110002165400731500000A0A06166F6200000A000672AAEB00706F1600000A26066F1700000A036F1800000A03731900000A0B0002076F2E00000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00024A13041104450200000002000000020000002B022B050216542B00170CDE0A260000DE0000160C2B0000082A000000011C0000020032001143001000000000000004007074000501000001133004000F0000000C000011000203160428C00000060A2B00062A001B300600840B00003D00001100027B8604000416731100000A282700000A2D13027B8704000416731100000A282700000A2B0117000D027B8F04000416FE01131011102D3E00092D18027B910400047BA704000416731100000A282B00000A2B0117000D092D18027B910400047BA604000416731100000A282B00000A2B0117000D0000731500000A0A0916FE01131011103A290700000012020528BE000006131011102D1500037209ED00706F1600000A2616130FDDD10A0000027B8F04000416FE01131011102D0400170C0006166F6200000A00067247ED00706F1600000A26067279ED00706F1600000A260672ADED00706F1600000A260672DFED00706F1600000A2606720FEE00706F1600000A26067239EE00706F1600000A26067273EE00706F1600000A260672A9EE00706F1600000A26067232EF00706F1600000A26067290EF00706F1600000A260672F4EF00706F1600000A2606725AF000706F1600000A26066F1700000A056F1800000A05731900000A13040011046F1A00000A7227050070166F1B00000A027B830400048C270000016F1C00000A0011046F1A00000A72C60F0070166F1B00000A027B840400048C270000016F1C00000A0011046F1A00000A72A05A00701E6F1B00000A188C2D0000016F1C00000A0002733600000A7D8504000411046F3000000A130500027B8504000411056F7C00000A0000DE14110514FE01131011102D0811056F1E00000A00DC0000DE14110414FE01131011102D0811046F1E00000A00DC0008131111114502000000050000005A010000389E01000016731100000A1308027B8C0400046F1001000616731100000A282B00000A2C1B027B8C0400046F0F01000616731100000A282B00000A16FE012B011700131011102D1F00027B8C0400046F0F010006027B8C0400046F10010006282900000A130800170228E400000600180228E1000006000228E50000060016731100000A027B86040004027B87040004282C00000A282400000A130911091108287100000A1817287D00000A1309110916731100000A282B00000A16FE01131011102D6600027B860400041109282500000A1306027B870400041109282500000A13070211097D860400040211097D87040004021728DF00000600170228E10000060002027B860400041106282600000A7D8604000402027B870400041107282600000A7D8704000400021628DF000006000228E000000600170228E400000600180228E1000006000228E500000600170228E1000006002B680228E000000600180228E100000600170228E1000006000228E500000600027B8F04000416FE01131011102D1A00180228E200000600170228E200000600190228E300000600002B1F037298F00070088C5E000002287E00000A6F1600000A2616130FDD860700000216731100000A027B86040004282400000A7D860400040216731100000A027B87040004282400000A7D87040004027B85040004720100007072010000701F106F7F00000A0B078E6916FE0216FE01131011103A610300000006166F6200000A0006724DE700706F1600000A260672D0F000706F1600000A26067224F100706F1600000A26067270F100706F1600000A260672CFE800706F1600000A2606724FE900706F1600000A260672A5E900706F1600000A260672FDE900706F1600000A260672C2F100706F1600000A26735000000A130A00110A066F1700000A056F1800000A05731900000A6F8000000A00110A6F8100000A6F1A00000A7222F200701F096F1B00000A72CBE200706F4D00000A00110A6F8100000A6F1A00000A7236F200701F096F1B00000A7242F200706F4D00000A00110A6F8100000A6F1A00000A7252F200701F096F1B00000A7264F200706F4D00000A00110A6F8100000A6F1A00000A72ED5C0070166F1B00000A728FE200706F4D00000A00110A6F8100000A6F1A00000A72C60F0070166F1B00000A027B840400048C270000016F1C00000A00110A6F8100000A6F1A00000A72A05A00701E6F1B00000A188C2D0000016F1C00000A00110A6F8100000A6F1A00000A7227050070166F1B00000A027B830400048C270000016F1C00000A00110A6F8100000A166F4F00000A00110A176F6300000A00110A176F8200000A00110A076F8300000A130B110B078E69FE01131011103A790100000003727AF20070110B8C2D000001078E698C2D000001288400000A6F1600000A260007131216131338A4000000111211139A130C000372E2F200701B8D010000011314111416110C728FE200706F4700000AA2111417110C72ABE200706F4700000AA2111418110C72CBE200706F4700000AA2111419110C7264F200706F4700000AA211141A110C7242F200706F4700000AA21114282300000A6F1600000A26110C6F8500000A16FE01131011102D1A0003727FF30070110C6F8600000A286600000A6F1600000A260000111317581313111311128E69FE04131011103A4BFFFFFF03728FF30070110A6F8100000A6F8700000A286600000A6F1600000A2600110A6F8100000A6F1A00000A6F8800000A13152B2F11156F4B00000A742B000001130D000372B7F30070110D6F8900000A110D6F6000000A288400000A6F1600000A260011156F4C00000A131011102DC4DE1D111575050000011316111614FE01131011102D0811166F1E00000A00DC0016130FDDE603000000DE14110A14FE01131011102D08110A6F1E00000A00DC000000027B8F04000416FE01131011102D0E0002027B900400047D8C0400040006166F6200000A000672DFF300706F1600000A26067255F400706F1600000A260672CBF400706F1600000A26067241F500706F1600000A26066F7A00000A260672BABE00706F1600000A260416FE01131011102D28000672BDF500706F1600000A2606728CF600706F1600000A2606725BF700706F1600000A26002B260006722AF800706F1600000A260672B7F800706F1600000A26067244F900706F1600000A26000672D1F900706F1600000A26067286FA00706F1600000A2606723BFB00706F1600000A260672EAFB00706F1600000A26067299FC00706F1600000A2606724AFD00706F1600000A260672F5FD00706F1600000A2606724FFE00706F1600000A260672A9FE00706F1600000A26067203FF00706F1600000A260672DFCA00706F1600000A2606725DFF00706F1600000A2606724D7900706F1600000A2606727FCB00706F1600000A260672ABCB00706F1600000A26066F1700000A056F1800000A05731900000A13040011046F1A00000A72C60F0070166F1B00000A027B840400048C270000016F1C00000A0011046F1A00000A7285FF00701F096F1B00000A027B8C0400047BA50400048C080000016F1C00000A0011046F1A00000A72AFFF00701F096F1B00000A027B8C0400047BA60400048C080000016F1C00000A0011046F1A00000A72E3FF00701F096F1B00000A027B8C0400047BA70400048C080000016F1C00000A0011046F1A00000A72170001701F096F1B00000A027B880400048C080000016F1C00000A0011046F1A00000A723F0001701F096F1B00000A027B890400048C080000016F1C00000A0011046F1A00000A72670001701F096F1B00000A027B8A0400048C080000016F1C00000A0011046F1A00000A72890001701F096F1B00000A027B8B0400048C080000016F1C00000A0011046F1A00000A72AB0001701F096F1B00000A027B860400048C080000016F1C00000A0011046F1A00000A72CF0001701F096F1B00000A027B870400048C080000016F1C00000A0011046F3000000A13050011056F3100000A131011102D25000372ED000170027B840400048C27000001287E00000A6F1600000A2616130FDD91000000021105166F6100000A16731100000A1105176F6100000A730B0100067D8D040004021105186F6100000A16731100000A1105196F6100000A730B0100067D8E04000400DE14110514FE01131011102D0811056F1E00000A00DC0000DE14110414FE01131011102D0811046F1E00000A00DC0017130FDE1A130E0003110E6F7500000A6F1600000A2600DE000016130F2B0000110F2A41AC000002000000E701000012000000F90100001400000000000000020000006E010000A3000000110200001400000000000000020000003407000040000000740700001D0000000000000002000000CE040000CF0200009D070000140000000000000002000000BC0A000078000000340B00001400000000000000020000003C090000100200004C0B000014000000000000000000000077000000EF0A0000660B000014000000390000011B300500E90100000A0000110003730601000651731500000A0A0672430101706F1600000A260672C80101706F1600000A260672B6C000706F1600000A2606723BC100706F1600000A2606724D0201706F1600000A260672D20201706F1600000A26066F7A00000A260672BABE00706F1600000A2606725D0301706F1600000A260672720401706F1600000A260672870501706F1600000A260672380601706F1600000A260672BFC900706F1600000A26067217CA00706F1600000A2606727BCA00706F1600000A260672E90601706F1600000A260672DFCA00706F1600000A2606722B0701706F1600000A260672F00701706F1600000A26066F7A00000A2606727FCB00706F1600000A260672ABCB00706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F3000000A0C00086F3100000A130411042D0800160DDD8C00000003730601000651035008166F6100000A7D86040004035008176F6100000A7D87040004035008186F6100000A08196F6100000A081A6F6100000A730B0100067D8C0400040350081B6F3200000A7D840400040350027D83040004170DDE2E0814FE01130411042D07086F1E00000A00DC0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A000000012800000200450173B801120000000002002001AACA01120000000000000C01D0DC01050100000113300400170100003E00001100027B5D0400046FF80000060B027B5D0400046FF90000060C027B5D0400046FFA0000060D027B5D0400046FFD000006130473060100060A06027B5A0400047B650400047D8604000406027B5A0400047B670400047D8704000406027B540400047D8404000406067B86040004091104282500000A16731100000A282400000A282C00000A7D8904000406067B86040004067B89040004282500000A7D8804000406027B5A0400047B670400047D8A0400040616731100000A7D8B04000406067B8804000416731100000A067B89040004730B0100067D8D04000406067B8A04000416731100000A067B8B040004730B0100067D8E04000406067B8D040004067B8E04000428140100067D8C0400040613052B0011052A00133008001A0000003F0000110002281101000603120004120212010528CB0000060D2B00092A0000133009001C0000003F000011000228110100060304120005120212010E0428CC0000060D2B00092A13300800170000004000001100020304050E04120112000E0528CB0000060C2B00082A00133008004D00000041000011000E0414510E0516731100000A8108000001027E6D00000A0304160512000E0628CE00000616FE010C082D1B000E04067BBF040004510E05067BC00400048108000001170B2B04160B2B00072A000000133008001B0000004200001100020316731100000A041201120212000528CB0000060D2B00092A00133008001B0000004300001100020316731100000A0412000512010E0428CB0000060C2B00082A00133008001B0000004400001100020316731100000A04050E0412000E0528CB0000060B2B00072A00133008001B0000004500001100020316731100000A041200050E040E0528CB0000060B2B00072A00133007006A0000004100001100052811010006510E0416731100000A81080000010E05166A550E06281101000651027E6D00000A03041612000E0728CD0000060C082D0500160B2B2B05067BBF040004510E04067BC004000481080000010E05067BB8040004550E06067BBE04000451170B2B00072A0000133007006C00000041000011000E042811010006510E0516731100000A81080000010E06166A550E07281101000651027E6D00000A03040512000E0828CD0000060C082D0500160B2B2C0E04067BBF040004510E05067BC004000481080000010E06067BB8040004550E07067BBE04000451170B2B00072A13300800160000000C00001100020304050E04160E050E0628CE0000060A2B00062A00001B300900560B00004600001100170C190D1A130472010000700B0E067327010006510E051A5F16FE0116FE0113070E051E5F16FE0116FE01130872DE0F007072F20F00700E0728EF0000061205284900000A26110517FE011306731500000A0A0672560801706F1600000A2606729E0801706F1600000A260672020901706F1600000A2606724A0901706F1600000A260672920901706F1600000A260672DA0901706F1600000A260672220A01706F1600000A2606727E0A01706F1600000A260672CA0A01706F1600000A260672160B01706F1600000A260672620B01706F1600000A260672AE0B01706F1600000A2606720C0C01706F1600000A260672640C01706F1600000A260672DE0C01706F1600000A260672560D01706F1600000A260672CE0D01706F1600000A260672460E01706F1600000A260672AA0E01706F1600000A2606720A0F01706F1600000A2606726A0F01706F1600000A260672CA0F01706F1600000A2606722A1001706F1600000A26066F7A00000A260672861001706F1600000A260672C41001706F1600000A2606721A1101706F1600000A260672361101706F1600000A2602166AFE01130D110D2D10000672FF1101706F1600000A26002B1A000672511201706F1600000A260672E21201706F1600000A26000672A51301706F1600000A260672191401706F1600000A2606728D1401706F1600000A260516731100000A282100000A2C040E042B011700130D110D2D0E000672011501706F1600000A26000672751501706F1600000A260672ED1501706F1600000A260672651601706F1600000A260672E11601706F1600000A260672391701706F1600000A260672AF1701706F1600000A260672111801706F1600000A260672731801706F1600000A260672D51801706F1600000A260672371901706F1600000A260672991901706F1600000A260672FB1901706F1600000A2606725D1A01706F1600000A260672BF1A01706F1600000A260672211B01706F1600000A260672991901706F1600000A260672831B01706F1600000A260672BABE00706F1600000A260672E51B01706F1600000A260672041D01706F1600000A2606728B1D01706F1600000A261106130D110D2D5F0006721C1E01706F1600000A260672371F01706F1600000A260672522001706F1600000A2606726D2101706F1600000A26047BAB04000416FE01130D110D2D10000672882201706F1600000A26002B0E000672EA2201706F1600000A2600000672502301706F1600000A260672942301706F1600000A260672DE2301706F1600000A260672282401706F1600000A260672722401706F1600000A260672D22401706F1600000A260672322501706F1600000A260672822501706F1600000A260672D22501706F1600000A260672222601706F1600000A2606726C2601706F1600000A260672CA2601706F1600000A260672282701706F1600000A260672A22701706F1600000A260672042801706F1600000A260672722801706F1600000A260672E02801706F1600000A261106130D110D2D1C000672562901706F1600000A260672B62901706F1600000A26002B1A000672162A01706F1600000A260672162A01706F1600000A2600110716FE01130D110D2D10000672302A01706F1600000A26002B0E000672162A01706F1600000A2600110816FE01130D110D2D100006727A2A01706F1600000A26002B0E000672C02A01706F1600000A26000672D62A01706F1600000A260672DFCA00706F1600000A2606720E2B01706F1600000A2606723A2B01706F1600000A26110716FE01130D110D2D0E000672292C01706F1600000A2600110816FE01130D110D2D0E000672182D01706F1600000A260006720B2E01706F1600000A26066F7A00000A2606727FCB00706F1600000A260672ABCB00706F1600000A2600066F1700000A0E076F1800000A0E07731900000A13090002166AFE0116FE01130D110D2D4C00036F8A00000A1F141F306F8B00000A0B11096F1A00000A72832E01701F0C1F326F2D00000A036F1C00000A0011096F1A00000A72A72E01701F0C1F326F2D00000A076F1C00000A00002B200011096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A000011096F1A00000A72C52E01701F096F1B00000A047BA50400048C080000016F1C00000A0011096F1A00000A72DD2E01701F096F1B00000A047BA60400048C080000016F1C00000A0011096F1A00000A72FF2E01701F096F1B00000A047BA70400048C080000016F1C00000A0011096F1A00000A72212F01701F096F1B00000A058C080000016F1C00000A001106130D110D2D490011096F1A00000A72332F01701F096F1B00000A047BA80400048C080000016F1C00000A0011096F1A00000A72492F0170186F1B00000A047BAA0400048C430000016F1C00000A000011096F1A00000A72672F01701E6F1B00000A088C2D0000016F1C00000A00110716FE01130D110D2D200011096F1A00000A72832F01701E6F1B00000A098C2D0000016F1C00000A0000110816FE01130D110D2D210011096F1A00000A729F2F01701E6F1B00000A11048C2D0000016F1C00000A000011096F3000000A130A00110A6F3100000A130D110D2D580002166AFE0116FE01130D110D2D21000E065072BB2F0170036F8A00000A72E52F0170284000000A7DC1040004002B20000E065072173001700F00281F00000A722F300170284000000A7DC10400040016130CDD940300000E0650110A166F3200000A7DB00400040E0650110A176F7700000A7DB10400040E0650110A186F3300000A7DB20400040E0650110A196F3300000A7DB30400040E06507BB304000416FE0216FE01130D110D2D22000E0650110A1A6F3300000A7DB40400040E0650110A1B6F6B00000A7DB5040004000E0650110A1C6F6100000A110A1D6F6100000A110A1E6F6100000A110A1F116F6100000A110A1F126F7700000A110A1F136F6100000A110A1F146F6100000A730D0100067DBF0400040E0650110A1F096F6100000A7DC00400040E0650110A1F0A6F6C00000A2D0B110A1F0A6F3200000A2B02166A007DB80400040E0650110A1F0B6F6C00000A2D0B110A1F0B6F3300000A2B0116007DB90400040E06507BB8040004166AFE01130D110D2D71000E0650110A1F0C6F6C00000A2D0B110A1F0C6F3200000A2B02166A007DBD0400040E06507BBD040004166AFE01130D110D2D3C000E0650110A1F0D6F6100000A110A1F0E6F6100000A110A1F0F6F6100000A110A1F116F6100000A110A1F126F7700000A730C0100067DBE04000400000E0650110A1F106F6C00000A2D0B110A1F106F3200000A2B02166A007DB70400040E0650110A1F156F3300000A7DB604000400DE14110A14FE01130D110D2D08110A6F1E00000A00DC0000DE14110914FE01130D110D2D0811096F1E00000A00DC000E06507BB8040004166AFE01130D110D3A1B0100000006166F6200000A0006725F3001706F1600000A260672893001706F1600000A260672DD3001706F1600000A260672193101706F1600000A260672453101706F1600000A260672773101706F1600000A26066F1700000A0E076F1800000A0E07731900000A13090011096F1A00000A7227050070166F1B00000A0E06507BB80400048C270000016F1C00000A0011096F3000000A130A00110A6F3100000A130D110D2D090016130CDDDC0000000E0650110A166F3300000A7DBA0400040E0650110A176F7700000A7DBB0400040E0650110A186F6100000A110A196F6100000A282600000A7DBC04000400DE14110A14FE01130D110D2D08110A6F1E00000A00DC0000DE14110914FE01130D110D2D0811096F1E00000A00DC000017130CDE69130B0002166AFE0116FE01130D110D2D28000E065072BB2F0170036F8A00000A72D7310170110B6F7500000A287300000A7DC1040004002B27000E065072173001700F00281F00000A72FB310170110B6F7500000A287300000A7DC10400040000DE000016130C2B0000110C2A0000417C000002000000580700002F02000087090000140000000000000002000000830500001C0400009F0900001400000000000000020000005F0A000057000000B60A0000140000000000000002000000300A00009E000000CE0A00001400000000000000000000006C0500007D050000E90A000063000000390000011B300300860000002C00001100731500000A0A06721D3201706F1600000A260672433201706F1600000A260672933201706F1600000A2600066F1700000A036F1800000A03731900000A0B00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A6900100000000000002B004E790005010000011B300300F90000002C00001100731500000A0A06721D3201706F1600000A260672E13201706F1600000A2606721D3301706F1600000A260672D63301706F1600000A260672203401706F1600000A260672703401706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A720B3501701E6F1B00000A038C170000026F1C00000A00076F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00076F1A00000A72273501701E6F1B00000A1A8C0F0000026F1C00000A00076F1A00000A72829F00701E6F1B00000A1B8C0F0000026F1C00000A00076F1D00000A17FE0216FE010CDE100714FE010D092D07076F1E00000A00DC00082A000000011000000200620084E60010000000001B30080006050000470000110004165205166A5517130E16731100000A131017130B000E0472573501706F1200000A00731500000A0A0672853501706F1600000A260672C13501706F1600000A260672293601706F1600000A260672913601706F1600000A260672F73601706F1600000A2606725D3701706F1600000A260672C33701706F1600000A260672293801706F1600000A2606728F3801706F1600000A260672F53801706F1600000A2606725B3901706F1600000A260672C13901706F1600000A26066F7A00000A260672BABE00706F1600000A260672393A01706F1600000A260672D5C700706F1600000A26067245C800706F1600000A260672B5C800706F1600000A260672A93A01706F1600000A260672153B01706F1600000A260672813B01706F1600000A260672ED3B01706F1600000A260672593C01706F1600000A260672C53C01706F1600000A260672193D01706F1600000A260672793D01706F1600000A260672D93D01706F1600000A2606721B3E01706F1600000A2606724D3E01706F1600000A260672DFCA00706F1600000A2606725DFF00706F1600000A260672793E01706F1600000A2606723E3F01706F1600000A260672034001706F1600000A260672714001706F1600000A260672044101706F1600000A260672974101706F1600000A2606722A4201706F1600000A260672BD4201706F1600000A260672504301706F1600000A260672E34301706F1600000A260672704401706F1600000A260672D44401706F1600000A2606727FCB00706F1600000A260672ABCB00706F1600000A26066F1700000A0E046F1800000A0E04731900000A13110011116F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0011116F1A00000A72E0CD0070166F1B00000A038C270000016F1C00000A0011116F1A00000A72264501701E6F1B00000A168C110000026F1C00000A0011116F3000000A13120011126F3100000A131611162D0900171315DD300200001112166F3200000A0B1112176F3200000A0C1112186F6100000A1112196F6100000A11121A6F6100000A730B010006130411121B6F6100000A11121C6F6100000A11121D6F6100000A730B010006130511121E6F6100000A131011121F0A6F6B00000A0D00DE14111214FE01131611162D0811126F1E00000A00DC0000DE14111114FE01131611162D0811116F1E00000A00DC001A070311041206120712080E0428D2000006131611162D0900161315DD7B010000071104120A0E0428C7000006131611162D0900161315DD60010000110A110428140100061309110411052815010006130E1104730A0100061313111011046F10010006282100000A16FE01131611162D1700111016731100000A16731100000A730B010006131300110E0811132819010006160E0428D3000006131611162D0900161315DDF2000000110E16FE01131611162D3C001A08120C120D120F0E0428E6000006131611162D0900161315DDC80000000508550702080E0428A4000006131611162D0900161315DDAC000000000708030E0428B5000006131611162D0900161315DD92000000020908731300000613141114166A071F3811096F1001000616731100000A11046F10010006110A6F100100066F15000006131611162D0600161315DE5511140E046F1F000006131611162D0600161315DE4004175216130B171315DE35260000DE0000DE2700110B16FE01131611162D1900000E0472573501706F1300000A0000DE05260000DE00000000DC001613152B000011152A0000417C000002000000BB0200007E0000003903000014000000000000000200000057020000FA0000005103000014000000000000000000000016000000B7040000CD040000050000000100000100000000E204000011000000F304000005000000010000010200000016000000BF040000D504000027000000000000001B3005006F05000048000011000E042811010006510E052811010006510E06281101000651057BA504000416731100000A282100000A2D29057BA604000416731100000A282100000A2D16057BA704000416731100000A282100000A16FE012B011600130911092D090016130838050500000002130A110A17594504000000020000000200000014000000300000002B4F051304050C28110100060D050B0413052B45052819010006130428110100060C050D28110100060B166A13052B2905281901000613040528190100060C28110100060D28110100060B166A13052B08161308DD8F040000731500000A0A0672853501706F1600000A260672444501706F1600000A260672A24501706F1600000A260672004601706F1600000A2606725E4601706F1600000A260672BC4601706F1600000A2606721A4701706F1600000A260672784701706F1600000A260672D64701706F1600000A260672344801706F1600000A26066F7A00000A260672BABE00706F1600000A260672964801706F1600000A2606724B4901706F1600000A260672FA4901706F1600000A260672A94A01706F1600000A260672664B01706F1600000A2606721D4C01706F1600000A260672D44C01706F1600000A260672954D01706F1600000A260672504E01706F1600000A2606720B4F01706F1600000A260672A44F01706F1600000A260672375001706F1600000A260672C45001706F1600000A260672515101706F1600000A260672A75101706F1600000A260672095201706F1600000A2606726B5201706F1600000A260672C55201706F1600000A2606722B5301706F1600000A260672915301706F1600000A260672EB5301706F1600000A260672515401706F1600000A260672DFCA00706F1600000A2606725DFF00706F1600000A2606724D7900706F1600000A26066F7A00000A2606727FCB00706F1600000A260672ABCB00706F1600000A26066F1700000A0E076F1800000A0E07731900000A13060011066F1A00000A72C60F0070166F1B00000A038C270000016F1C00000A0011066F1A00000A72B75401701B6F1B00000A11047BA50400048C080000016F1C00000A0011066F1A00000A72D15401701B6F1B00000A11047BA60400048C080000016F1C00000A0011066F1A00000A72E55401701B6F1B00000A11047BA70400048C080000016F1C00000A0011066F1A00000A72F95401701B6F1B00000A087BA50400048C080000016F1C00000A0011066F1A00000A721B5501701B6F1B00000A087BA60400048C080000016F1C00000A0011066F1A00000A72375501701B6F1B00000A087BA70400048C080000016F1C00000A0011066F1A00000A72535501701B6F1B00000A097BA50400048C080000016F1C00000A0011066F1A00000A72795501701B6F1B00000A097BA60400048C080000016F1C00000A0011066F1A00000A72995501701B6F1B00000A097BA70400048C080000016F1C00000A0011066F1A00000A72B9550170166F1B00000A11058C270000016F1C00000A0011066F1A00000A72E55501701B6F1B00000A077BA50400048C080000016F1C00000A0011066F1A00000A720B5601701B6F1B00000A077BA60400048C080000016F1C00000A0011066F1A00000A722B5601701B6F1B00000A077BA70400048C080000016F1C00000A0011066F3000000A13070011076F3100000A130911092D0900161308DD980000000E041107166F6100000A1107176F6100000A1107186F6100000A730B010006510E051107196F6100000A11071A6F6100000A11071B6F6100000A730B010006510E0611071C6F6100000A11071D6F6100000A11071E6F6100000A730B01000651171308DE33110714FE01130911092D0811076F1E00000A00DC110614FE01130911092D0811066F1E00000A00DC260000DE00001613082B000011082A00414C000002000000BC0400007C00000038050000140000000000000002000000CE0200007E0200004C05000014000000000000000000000066000000FA04000060050000050000000100000113300600120000000C0000110002030405160E0428D40000060A2B00062A00001B3003005B0100002C0000110000731500000A0A06724B5601706F1600000A2606727F5601706F1600000A260516FE010D092D1C000672965701706F1600000A260672395801706F1600000A26002B0E000672DC5801706F1600000A260006721B5A01706F1600000A26066F1700000A0E056F1800000A0E05731900000A0B00076F1A00000A727F5A0170186F1B00000A028C430000016F1C00000A00076F1A00000A72A35A0170186F1B00000A0E048C430000016F1C00000A00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F1A00000A72D55A01701B6F1B00000A046F100100068C080000016F1C00000A00076F1A00000A72F95A01701B6F1B00000A046F0F0100068C080000016F1C00000A00076F1A00000A72115B01701B6F1B00000A046F0E0100068C080000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00413400000200000073000000CB0000003E010000100000000000000000000000010000004D0100004E01000005000000010000011B3004000C04000049000011000E0616520E0516731100000A810800000112050E0728D600000616FE01130911092D1600110516FE01130911092D090017130838CF03000000731500000A0D0972295B01706F1600000A2609725DFF00706F1600000A2609724D7900706F1600000A2600096F1700000A0E076F1800000A0E07731900000A13060011066F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011066F1A00000A72829F00701E6F1B00000A1B8C0F0000026F1C00000A0011066F2E00000AA52D000001130400DE14110614FE01130911092D0811066F1E00000A00DC00110416FE0116FE01130911092D0900171308DD1303000009166F6200000A000972F25B01706F1600000A260972225C01706F1600000A260972565C01706F1600000A260972AE5C01706F1600000A2609722A5D01706F1600000A260972F25B01706F1600000A260972225C01706F1600000A260972565C01706F1600000A260972305D01706F1600000A2609722A5D01706F1600000A260972F25B01706F1600000A260972225C01706F1600000A260972565C01706F1600000A260972AA5D01706F1600000A2609722A5D01706F1600000A2609721E5E0170120472245E0170288C00000A6F8D00000A26096F1700000A0E076F1800000A0E07731900000A13060011066F3000000A13070011076F3100000A130911092D0900161308DD080200001107166F6B00000A288E00000A0B11076F8F00000A130911092D0900161308DDE401000011076F3100000A130911092D0900161308DDCE0100001107166F6B00000A288E00000A0A11076F8F00000A130911092D0900161308DDAA01000011076F3100000A130911092D0900161308DD940100001107166F6B00000A288E00000A0C00DE14110714FE01130911092D0811076F1E00000A00DC0000DE14110614FE01130911092D0811066F1E00000A00DC00037BF604000416731100000A289000000A16FE01130911092D1A000E0617520E0516731100000A8108000001171308DD220100000716731100000A282400000A0B0616731100000A282400000A0A0816731100000A282400000A0C0716731100000A282B00000A16FE01130911092D2A000E0617520E0525710800000116731100000A05282400000A07282900000A282600000A8108000001000616731100000A282B00000A16FE01130911092D2A000E0617520E0525710800000116731100000A04282400000A06282900000A282600000A8108000001000816731100000A282B00000A16FE01130911092D2B000E0617520E0525710800000116731100000A0E04282400000A08282900000A282600000A8108000001000E050E06462D0816731100000A2B180E057108000001037BF6040004287100000A1A289100000A008108000001171308DE0B260000DE00001613082B000011082A41640000020000007B0000004E000000C9000000140000000000000002000000E90100009C00000085020000140000000000000002000000DF010000BE0000009D0200001400000000000000000000006400000099030000FD03000005000000010000011B300300950000002C0000110002165400731500000A0A06166F6200000A0006722A5E01706F1600000A260672985E01706F1600000A260672EE5E01706F1600000A260672995F01706F1600000A260672016001706F1600000A26066F1700000A036F1800000A03731900000A0B0002076F2E00000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A000000011C00000200620011730010000000000000040084880005010000011B300700280100004A00001100160A166A0B7E6D00000A0C00727F6001700E046F1800000A0E04731900000A0D00091A6F9200000A0072C60F007016739300000A13041104028C270000016F1C00000A001104176F4E00000A00096F1A00000A11046F9400000A26729B6001701E739300000A13051105186F4E00000A00096F1A00000A11056F9400000A26096F1D00000A26096F1A00000A729B6001706F9500000A6F6000000AA5550000020A00DE120914FE01130711072D07096F1E00000A00DC0072B36001700C0616FE01130711072D4C0006130811081759450200000002000000130000002B2220080020006A0B0804286600000A0C2B1320090020006A0B0804286600000A0C2B022B00196A0203076D08170E0428D80000062600171306DE0B260000DE00001613062B000011062A41340000020000002100000084000000A50000001200000000000000000000000C0000000D0100001901000005000000010000011B300300FB0100002C0000110000731500000A0A0672C96001706F1600000A260672216101706F1600000A260672796101706F1600000A260672D16101706F1600000A260672296201706F1600000A260672816201706F1600000A260672D96201706F1600000A260672316301706F1600000A260672896301706F1600000A260672E16301706F1600000A260672396401706F1600000A260672916401706F1600000A260672E96401706F1600000A260672416501706F1600000A260672996501706F1600000A260672F16501706F1600000A260672496601706F1600000A260672A16601706F1600000A260672A16601706F1600000A260672E16301706F1600000A26066F1700000A0E066F1800000A0E06731900000A0B00076F1A00000A72F96601701E6F1B00000A028C270000016F1C00000A00076F1A00000A7213670170166F1B00000A038C270000016F1C00000A00076F1A00000A72296701701F0C6F1B00000A046F1C00000A00076F1A00000A72436701701E6F1B00000A058C4B0000016F1C00000A00076F1A00000A725B6701701F0C6F1B00000A72736701706F1C00000A00076F1A00000A727D6701701F0C6F1B00000A0E046F1C00000A00076F1A00000A72A36701701E6F1B00000A0E058C2D0000016F1C00000A00076F1D00000A26170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0041340000020000000D010000D1000000DE01000010000000000000000000000001000000ED010000EE01000005000000010000011B3003009B0200004B0000110000166A0B056F4300000A6F4500000A2C16036F0F01000616731100000A289000000A16FE012B011600130A110A2D0900171309DD5E020000731500000A0A0672245B00706F1600000A260672B96701706F1600000A260672076801706F1600000A260672FA5B00706F1600000A26067298CE00706F1600000A260672F0CE00706F1600000A2606724ACF00706F1600000A26066F1700000A0E046F1800000A0E04731900000A13040011046F1A00000A72ED5C0070166F1B00000A130511046F1A00000A72516801701F096F1B00000A130611046F1A00000A72035D00701E6F1B00000A130711046F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A0011046F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A00036F0F0100060C00056F4300000A6F4A00000A130B38E8000000110B6F4B00000A740B0000011308001108166F6900000AA5270000010B1105078C270000016F1C00000A001108186F6900000AA5080000010D0908285800000A16FE01130A110A2D620011060908282500000A8C080000016F1C00000A000908289000000A16FE01130A110A2D12001107198C0A0000026F1C00000A00002B10001107188C0A0000026F1C00000A000011046F1D00000A17FE01130A110A2D0900161309DDB10000002B4D000809282500000A0C1106168C2D0000016F1C00000A001107198C0A0000026F1C00000A0011046F1D00000A17FE01130A110A2D0600161309DE740000110B6F4C00000A130A110A3A08FFFFFFDE1D110B7505000001130C110C14FE01130A110A2D08110C6F1E00000A00DC0000DE14110414FE01130A110A2D0811046F1E00000A00DC0007166AFE0216FE01130A110A2D0C0004070E0428DA0000062600171309DE0B260000DE00001613092B000011092A00414C00000200000038010000FF000000370200001D0000000000000002000000A9000000AF01000058020000140000000000000000000000010000008B0200008C02000005000000010000011B300300860000002F0000110000726D680170046F1800000A04731900000A0A00066F1A00000A7228690170166F1B00000A038C270000016F1C00000A00066F1A00000A720D0500701E6F1B00000A028C270000016F1C00000A00066F1D00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1E00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300800C80300004C000011001713060516520E04166A550073090100060B731500000A0A06723C6901706F1600000A260672DB6901706F1600000A2606727A6A01706F1600000A260672196B01706F1600000A260672B86B01706F1600000A260672576C01706F1600000A260672F66C01706F1600000A260672956D01706F1600000A260672346E01706F1600000A260672D36E01706F1600000A260672726F01706F1600000A26066F1700000A0E056F1800000A0E05731900000A13090011096F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0011096F1A00000A72117001701E6F1B00000A188C0F0000026F1C00000A0011096F1A00000A722D7001701E6F1B00000A1B8C0F0000026F1C00000A0011096F1A00000A72AE6100701E6F1B00000A168C110000026F1C00000A0011096F3000000A130A00110A6F3100000A16FE01130C110C2D2A00110A166F3200000A0C110A176F3200000A0D110A186F3200000A1304110A196F6B00000A1305002B090017130BDD4B02000000DE14110A14FE01130C110C2D08110A6F1E00000A00DC0000DE14110914FE01130C110C2D0811096F1E00000A00DC000E0572573501706F1200000A00080412010E0528C7000006130C110C2D090016130BDDF4010000161104042819010006170E0528D3000006130C110C2D090016130BDDD4010000047BA6040004047BA7040004282600000A16731100000A282B00000A16FE01130C110C3A0C0100000006166F6200000A000672245B00706F1600000A260672577001706F1600000A260672C37001706F1600000A260672197101706F1600000A2606727B7101706F1600000A26066F1700000A0E056F1800000A0E05731900000A13090011096F1A00000A72F95A01701F096F1B00000A046F100100068C080000016F1C00000A0011096F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A0011096F1A00000A7270D70070166F1B00000A098C270000016F1C00000A0011096F1A00000A72C60F0070166F1B00000A088C270000016F1C00000A0011096F1D00000A17FE01130C110C2D090016130BDDB900000000DE14110914FE01130C110C2D0811096F1E00000A00DC0000076F10010006046F10010006282500000A13070211051104731300000613081108166A081F38110716731100000A046F10010006076F100100066F15000006130C110C2D060016130BDE5511080E056F1F000006130C110C2D060016130BDE4016130605175217130BDE35260000DE0000DE2700110616FE01130C110C2D1900000E0572573501706F1300000A0000DE05260000DE00000000DC0016130B2B0000110B2A419400000200000035010000470000007C010000140000000000000002000000B3000000E100000094010000140000000000000002000000730200009B0000000E0300001400000000000000000000000C000000830300008F030000050000000100000100000000A403000011000000B50300000500000001000001020000000C0000008B03000097030000270000000000000013300500100000000C00001100020304170528DD0000060A2B00062A133002000B0000000C000011000E041754160A2B00062A001B300500630200004D000011000E0516731100000A81080000010E041754000316731100000A282700000A2D090418FE0116FE012B011600130511053A1C02000000731500000A0C022811010006120012010E0628C800000616FE01130511053AF7010000000E05067BA80400048108000001041306110645030000000500000059000000A200000038C10000000316731100000A282100000A16FE01130511052D2200066F0E010006067BA8040004282500000A066F0E010006282C00000A1001002B1B00066F0E010006067BA8040004282500000A03282C00000A1001002B750316731100000A282100000A16FE01130511052D1700067BA804000415731100000A287100000A1001002B1B00067BA804000403282C00000A15731100000A287100000A1001002B2C03066F0E010006282B00000A16FE01130511052D0D000E041854161304DD1E0100002B08161304DD14010000731500000A0C0872CB7101706F1600000A260418FE0116FE01130511052D1C000872377201706F1600000A260872A37201706F1600000A26002B0E0008720F7301706F1600000A260008727B7301706F1600000A26086F1700000A0E066F1800000A0E06731900000A0D00096F1A00000A727F0500701F096F1B00000A038C080000016F1C00000A00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A000418FE0116FE01130511052D1F00096F1A00000A72492F0170186F1B00000A058C430000016F1C00000A0000096F1D00000A17FE0116FE01130511052D0A000E041654171304DE2600DE120914FE01130511052D07096F1E00000A00DC00000000DE05260000DE00001613042B000011042A004134000002000000B5010000870000003C02000012000000000000000000000012000000420200005402000005000000010000011B3004009C0100004E0000110000027B850400046F4300000A6F4A00000A0C3859010000086F4B00000A740B0000010B00027B8704000416731100000A286E00000A16FE010D092D0600383B0100000772ABE200706F4700000AA50900000217FE010D092D060038110100000772E77301706F5E00000A2C060316FE012B0116000D092D0B00027B870400040A002B4A000772FF7301706F4700000AA5080000010772CBE200706F4700000AA5080000010772FF7301706F4700000AA508000001282C00000A282500000A0A06027B87040004282C00000A0A000616731100000A286E00000A16FE010D092D0600388A0000000772CBE200700772CBE200706F4700000AA50800000106282600000A8C080000016F4200000A00077242F20070077242F200706F4700000AA50800000106282600000A8C080000016F4200000A0002257B8704000406282500000A7D8704000402257B8B04000406282600000A7D8B040004027B8C040004257BA704000406282600000A7DA704000400086F4C00000A0D093A9AFEFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A411C000002000000130000006D010000800100001A00000000000000133003005F0000004500001100027B8704000416731100000A286E00000A16FE010B072D03002B42027B870400040A027B8C040004257BA504000406282600000A7DA504000402257B8A04000406282600000A7D8A04000402257B8704000406282500000A7D870400042A001B3004005C0100004E0000110000037B850400046F4300000A6F4A00000A0C3819010000086F4B00000A740B0000010B00037B8604000416731100000A286E00000A16FE010D092D060038FB0000000772ABE200706F4700000AA50900000202FE010D092D060038D10000000772CBE200706F4700000AA508000001037B86040004282C00000A0A06037B8C0400047BA7040004282C00000A0A0616731100000A286E00000A16FE010D092D0600388A0000000772CBE200700772CBE200706F4700000AA50800000106282500000A8C080000016F4200000A00077264F20070077264F200706F4700000AA50800000106282600000A8C080000016F4200000A0003257B8904000406282600000A7D8904000403257B8604000406282500000A7D86040004037B8C040004257BA704000406282500000A7DA704000400086F4C00000A0D093ADAFEFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A411C000002000000130000002D010000400100001A000000000000001B3004002D0100004E0000110000037B850400046F4300000A6F4A00000A0C38EA000000086F4B00000A740B0000010B00037B910400047BA704000416731100000A286E00000A16FE010D092D060038C70000000772ABE200706F4700000AA50900000202FE010D092D0600389D0000000772CBE200706F4700000AA508000001037B910400047BA7040004282C00000A0A0616731100000A286E00000A16FE010D092D03002B660772CBE200700772CBE200706F4700000AA50800000106282500000A8C080000016F4200000A00077264F20070077264F200706F4700000AA50800000106282600000A8C080000016F4200000A00037B91040004257BA704000406282500000A7DA704000400086F4C00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A0000000110000002001300FE11011A000000001B3004002D0100004E0000110000037B850400046F4300000A6F4A00000A0C38EA000000086F4B00000A740B0000010B00037B910400047BA604000416731100000A286E00000A16FE010D092D060038C70000000772ABE200706F4700000AA50900000202FE010D092D0600389D0000000772CBE200706F4700000AA508000001037B910400047BA6040004282C00000A0A0616731100000A286E00000A16FE010D092D03002B660772CBE200700772CBE200706F4700000AA50800000106282500000A8C080000016F4200000A00077264F20070077264F200706F4700000AA50800000106282600000A8C080000016F4200000A00037B91040004257BA604000406282500000A7DA604000400086F4C00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A0000000110000002001300FE11011A000000001B300400A20100004F0000110000037B850400046F4300000A6F4A00000A0D385B010000096F4B00000A740B0000010C00037B8604000416731100000A286E00000A16FE01130411042D0600383D0100000872ABE200706F4700000AA50900000202FE01130411042D0600380F0100000872E77301706F5E00000A16FE01130411042D060038F500000016731100000A0872CBE200706F4700000AA5080000010872FF7301706F4700000AA508000001282500000A282400000A0B037B8604000407282C00000A0A037B8C0400047BA704000406282C00000A0A0616731100000A286E00000A16FE01130411042D0600388A0000000872CBE200700872CBE200706F4700000AA50800000106282500000A8C080000016F4200000A00087264F20070087264F200706F4700000AA50800000106282600000A8C080000016F4200000A0003257B8904000406282600000A7D8904000403257B8604000406282500000A7D86040004037B8C040004257BA704000406282500000A7DA704000400096F4C00000A130411043A96FEFFFFDE1C0975050000011305110514FE01130411042D0811056F1E00000A00DC002A0000411C0000020000001300000071010000840100001C0000000000000013300300D00000004500001100027B8C0400047BA6040004027B86040004282C00000A0A0616731100000A282B00000A16FE010B072D3D00027B8C040004257BA604000406282500000A7DA604000402257B8804000406282600000A7D8804000402257B8604000406282500000A7D8604000400027B8C0400047BA5040004027B86040004282C00000A0A0616731100000A282B00000A16FE010B072D3D00027B8C040004257BA504000406282500000A7DA504000402257B8804000406282600000A7D8804000402257B8604000406282500000A7D86040004002A1B3003008A01000050000011000416540516541201FE150A0000011202FE150A0000010E04FE150D00000100731500000A0A06721E8E00706F1600000A260672197401706F1600000A260672F27401706F1600000A260672CB7501706F1600000A260672A47601706F1600000A260672DC7601706F1600000A260672167701706F1600000A260672507701706F1600000A260672B27701706F1600000A26066F1700000A0E056F1800000A0E05731900000A0D00096F1A00000A7227050070166F1B00000A038C270000016F1C00000A00096F1A00000A72264501701E6F1B00000A168C110000026F1C00000A00096F1A00000A72035D00701E6F1B00000A028C110000026F1C00000A00096F3000000A13040011046F3100000A130611062D0600161305DE6B041104166F3300000A54051104176F3300000A541104186F9600000A0B1104196F9600000A0C0E04120207289700000A810D000001171305DE31110414FE01130611062D0811046F1E00000A00DC0914FE01130611062D07096F1E00000A00DC260000DE00001613052B000011052A0000414C000002000000070100004E00000055010000140000000000000002000000A7000000C2000000690100001200000000000000000000001F0000005C0100007B01000005000000010000011B30030069010000510000110016731100000A03282400000A100116731100000A04282400000A1002720E7801700A06723E780170286600000A0A06724F790170286600000A0A0672BF790170286600000A0A0672357A0170286600000A0A0E041F20FE0116FE010D092D10000672A77A0170286600000A0A002B0E000672487B0170286600000A0A0000060E056F1800000A0E05731900000A0B00076F1A00000A723A9C0070166F1B00000A028C270000016F1C00000A00076F1A00000A72B92200701F096F1B00000A038C080000016F1C00000A00076F1A00000A72B87B01701F096F1B00000A048C080000016F1C00000A00076F1A00000A72DC7B0170186F1B00000A058C430000016F1C00000A00076F1A00000A72264501701E6F1B00000A168C2D0000016F1C00000A00076F1A00000A72007C01701E6F1B00000A188C2D0000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A000000011C000002008F00BD4C01100000000000007E00DE5C0105010000011B300300BE000000350000110000731500000A0A0672247C01706F1600000A2606728C7C01706F1600000A260672F47C01706F1600000A2606725C7D01706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72007C01701E6F1B00000A188C110000026F1C00000A00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010DDE290714FE01130411042D07076F1E00000A00DC0C0003086F7500000A6F1600000A2600DE0000160D2B0000092A0000011C000002004B00479200120000000000000100A3A40012390000011B3009002E01000052000011000E07731500000A510073F60000060A061F207D5204000406047D54040004060E067D5304000406037D5804000406177D5904000406027D5504000406166A7D56040004050E040E050612010E0828AA000006130511052D26000E0750076F040100066F1600000A260E075072C47D01706F1600000A26161304DDAB000000020E07500E0828E8000006130511052D17000E075072FE7D01706F1600000A26161304DD830000000420000010000E0828D0000006130511052D14000E0750724C7E01706F1600000A26161304DE5C050E050273130000060C08166A041F4B0316731100000A16731100000A037E6D00000A6F1700000626080E086F1F000006130511052D0600161304DE1F171304DE1A0D000E0750096F7500000A6F1600000A2600DE00001613042B000011042A0000411C00000000000009000000070100001001000014000000390000011B300300200100000A00001100731500000A0A0672887E01706F1600000A260672BE7E01706F1600000A260672F67E01706F1600000A260672267F01706F1600000A260672453101706F1600000A260672773101706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F3000000A0C00086F3100000A130411042D0800160DDD80000000037B5A04000408166F3300000A6A7D64040004037B5A04000408176F6100000A7D65040004037B5A04000408186F3300000A6A7D66040004037B5A04000408196F6100000A7D6704000400DE120814FE01130411042D07086F1E00000A00DC00170DDE1C0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A012800000200880062EA001200000000020063009E0101120000000000004F00C4130105010000011B30030085030000530000110000731500000A0A160B03130511054502000000050000000500000038F000000006166F6200000A000672587F01706F1600000A260672807F01706F1600000A260672BC7F01706F1600000A2606720C8001706F1600000A260672628001706F1600000A26066F1700000A056F1800000A05731900000A0C00086F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00086F1A00000A72B88001701E6F1B00000A178C090000026F1C00000A00086F1A00000A72C48001701E6F1B00000A188C090000026F1C00000A00086F1A00000A72A05A00701E6F1B00000A188C0A0000026F1C00000A00086F2E00000AA52D0000010B00DE120814FE01130611062D07086F1E00000A00DC002B0003130511054505000000F1000000F10000000500000005000000F1000000383D0200000416731100000A289000000A16FE01130611062D0900171304DD2F02000006166F6200000A0006721D3201706F1600000A260672D08001706F1600000A2606727D8101706F1600000A260672308201706F1600000A260672BF8201706F1600000A2606722F8301706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00096F1A00000A727F0500701F096F1B00000A03182E0804285F00000A2B0104008C080000016F1C00000A00096F1D00000A17FE011304DD730100000914FE01130611062D07096F1E00000A00DC06166F6200000A0006721D3201706F1600000A2606727B8301706F1600000A2606722F8301706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A72C60F0070166F1B00000A028C270000016F1C00000A00096F1A00000A727F0500701F096F1B00000A260316FE0116FE01130611062D4E000716FE0116FE01130611062D1F00096F1A00000A727F0500706F9500000A7E2F00000A6F1C00000A00002B1E00096F1A00000A727F0500706F9500000A048C080000016F1C00000A0000002B67071733090317FE0116FE012B011700130611062D2000096F1A00000A727F0500706F9500000A048C080000016F1C00000A00002B32031AFE0116FE01130611062D1F00096F1A00000A727F0500706F9500000A7E2F00000A6F1C00000A00002B0600171304DE2F096F1D00000A17FE011304DE220914FE01130611062D07096F1E00000A00DC161304DE0B260000DE00001613042B000011042A00000041640000020000007800000084000000FC000000120000000000000002000000B5010000590000000E0200001200000000000000020000005F020000000100005F03000012000000000000000000000001000000750300007603000005000000010000011B300300C1000000540000110072755D007072D98301700328EF0000061201284900000A260716FE0116FE01130411042D0800170D389000000000731500000A0A0672F98301706F1600000A260672518401706F1600000A260672998401706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A7227050070166F1B00000A028C270000016F1C00000A00086F1D00000A17FE01130411042D0500160DDE20170DDE1C0814FE01130411042D07086F1E00000A00DC260000DE0000160D2B0000092A000000011C000002006C0036A200120000000000002E0086B40005010000011B300300E90000002C0000110000731500000A0A0672DB8401706F1600000A2606720F8501706F1600000A260672498501706F1600000A260672758501706F1600000A260672A58501706F1600000A260672D78501706F1600000A260672178601706F1600000A260672478601706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72E08601701E6F1B00000A028C2D0000016F1C00000A00076F1A00000A72F8860170166F1B00000A038C270000016F1C00000A00076F1D00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A000000011C000002007B0051CC00100000000000000100DBDC0005010000011B300400E205000055000011000E051752031305731500000A1306731500000A1307160A160B160C160D731500000A13081108166F6200000A00110872F25B01706F1600000A26110872225C01706F1600000A26110872168701706F1600000A26110872928701706F1600000A261108720E8801706F1600000A26110872F25B01706F1600000A26110872225C01706F1600000A26110872168701706F1600000A26110872288801706F1600000A261108720E8801706F1600000A26110872F25B01706F1600000A26110872225C01706F1600000A26110872168701706F1600000A26110872B78801706F1600000A261108720E8801706F1600000A26110872F25B01706F1600000A26110872225C01706F1600000A26110872168701706F1600000A26110872218901706F1600000A261108722A5D01706F1600000A260011086F1700000A0E066F1800000A0E06731900000A13090011096F3000000A130A00110A6F3100000A16FE01130C110C2D1B00110A166F6B00000A1200284900000A130C110C2D0400160A0000110A6F3100000A16FE01130C110C2D1B00110A166F6B00000A1201284900000A130C110C2D0400160B0000110A6F3100000A16FE01130C110C2D1B00110A166F6B00000A1202289800000A130C110C2D0400160C0000110A6F3100000A16FE01130C110C2D1B00110A166F6B00000A1203289800000A130C110C2D0400160D000000DE14110A14FE01130C110C2D08110A6F1E00000A00DC0000DE14110914FE01130C110C2D0811096F1E00000A00DC000E054616FE01130C110C2D4D000616311106289900000A03282100000A16FE012B011700130C110C2D2E000E0516521106729B8901700F01282200000A6F9A00000A26110672C58901701200286F00000A6F9A00000A2600000E054616FE01130C110C3ACF000000000F02287900000A230000000000000000FE0216FE01130C110C3AAF000000000716312307289900000A030F02287900000A6C289B00000A282900000A282100000A16FE012B011700130C110C2D7E000E0516521106729B8901700F01282200000A6F9A00000A26110672038A01700F02287900000A130D120D722F8A0170289C00000A6F9A00000A26110672398A0170030F02287900000A6C289B00000A282900000A130E120E722F8A0170289D00000A6F9A00000A261106726B8A01701201286F00000A6F9A00000A260000000E054616FE01130C110C2D66000817330F057B9B040004166AFE0116FE012B011700130C110C2D4900027BED04000417FE0116FE01130C110C2D0400002B31000E051652110672BF8A0170057C9B040004281F00000A6F9A00000A26110672E18A0170168D010000016F9E00000A260000000E054616FE01130C110C2D4A000917330B057B9804000416FE012B011700130C110C2D31000E0516521106720B8B0170057C98040004287400000A6F9A00000A26110672378B0170168D010000016F9E00000A2600001713040E0546130C110C2D760016731100000A1305181304110772718B01706F9F00000A26110772978B0170057B950400048C270000016F9A00000A26110772B58B0170057B960400048C2D0000016F9A00000A26110772D98B01706F9F00000A2611061611076F1700000A6FA000000A260E0411066F1700000A6F020100060000731500000A1308110872F58B01706F1600000A261108722B8C01706F1600000A26110872368D01706F1600000A26110872198E01706F1600000A26110872FC8E01706F1600000A2611086F1700000A0E066F1800000A0E06731900000A13090011096F1A00000A726C8F01701E6F1B00000A168C2D0000016F1C00000A0011096F1A00000A72888F01701E6F1B00000A11048C2D0000016F1C00000A0011096F1A00000A72A68F01701F096F1B00000A038C080000016F1C00000A0011096F1A00000A72C88F01701F096F1B00000A11058C080000016F1C00000A0011096F1A00000A7227050070166F1B00000A057B940400048C270000016F1C00000A0011096F1D00000A17FE01130BDE1F110914FE01130C110C2D0811096F1E00000A00DC260000DE000016130B2B0000110B2A0000416400000200000054010000B0000000040200001400000000000000020000004A010000D20000001C02000014000000000000000200000011050000AE000000BF05000014000000000000000000000032010000A1040000D305000005000000010000011B300400D4000000560000110072010000700C00731500000A0A0672F25B01706F1600000A260672225C01706F1600000A260672E88F01706F1600000A260672329001706F1600000A26066F1700000A046F1800000A04731900000A0D00096F1A00000A72809001701F0C1F326F2D00000A026F1C00000A00096F1A00000A72909001701F0C1F326F2D00000A036F1C00000A00096F2E00000A0B072C0A077E2F00000AFE012B011700130511052D09000774280000010C0000DE120914FE01130511052D07096F1E00000A00DC0000DE05260000DE00000813042B0011042A011C0000020051005FB000120000000000000700BFC60005010000018602167D450400040228110100067D4604000402167D4704000402283400000A002A000013300200220000000C0000110002037D5004000404283500000A0A062D0F00027B51040004046F1600000A26002A000013300300250000000C0000110003283500000A0A062D1900027B5104000472A490017003286600000A6F1600000A26002A260002167D500400042A0013300100110000002000001100027B510400046F1700000A0A2B00062A00000003300200610000000000000002166A7D4804000402167D490400040228110100067D4A0400040228110100067D4B0400040228110100067D4C0400040228110100067D4D0400040216731100000A7D4E04000402177D5004000402731500000A7D5104000402283400000A002A00000003300200AF00000000000000021C7D520400040272010000707D5304000402156A7D5404000402156A7D5504000402166A7D5604000402167D570400040216731100000A7D5804000402167D590400040273F70000067D5A04000402167D5B04000402166A7D5C0400040273FF0000067D5D0400040272010000707D5E04000402167D5F0400040228110100067D600400040228110100067D610400040228110100067D620400040228110100067D6304000402283400000A002A0003300200540000000000000002166A7D640400040216731100000A7D6504000402166A7D660400040216731100000A7D670400040216731100000A7D680400040216731100000A7D690400040216731100000A7D6A04000402283400000A002A133002001C0000001E00001100027B6B040004027B700400047B73040004282600000A0A2B00062A133001000C0000001E00001100027B6D0400040A2B00062A133001000C0000001E00001100027B6C0400040A2B00062A13300200220000001E000011000228F80000060228F9000006282600000A0228FA000006282600000A0A2B00062A0000133001000C0000001E00001100027B6E0400040A2B00062A133001000C0000001E00001100027B6F0400040A2B00062A13300200170000001E000011000228FC0000060228FD000006282600000A0A2B00062A4E0273000100067D7004000402283400000A002A1E02283400000A2A0013300200220000000C0000110002037D8104000404283500000A0A062D0F00027B82040004046F1600000A26002A000013300300250000000C0000110003283500000A0A062D1900027B8204000472A490017003286600000A6F1600000A26002A260002167D810400042A0013300100110000002000001100027B820400046F1700000A0A2B00062A960228110100067D8004000402177D8104000402731500000A7D8204000402283400000A002A0003300200A50000000000000002166A7D8304000402166A7D8404000402147D850400040216731100000A7D860400040216731100000A7D870400040216731100000A7D880400040216731100000A7D890400040216731100000A7D8A0400040216731100000A7D8B0400040228110100067D8C0400040228110100067D8D0400040228110100067D8E04000402167D8F0400040228110100067D900400040228110100067D9104000402283400000A002A00000003300200B700000000000000021C7D920400040272010000707D9304000402166A7D9404000402166A7D9504000402167D960400040272010000707D9704000402177D980400040228110100067D9904000402731500000A7D9A04000402166A7D9B0400040228110100067D9C04000402166A7D9D0400040228110100067D9E04000402167D9F04000402166A7DA00400040273FF0000067DA10400040272010000707DA204000402167DA30400040216731100000A7DA404000402283400000A002A001330040053000000570000110072B89001701A8D010000010B07160228100100060C1202282200000AA20717027CA5040004282200000AA20718027CA6040004282200000AA20719027CA7040004282200000AA207282300000A0A2B00062A4602283400000A000002167DA9040004002A00000003300200820000000000000002283400000A000002037BA50400047DA504000402037BA60400047DA604000402037BA70400047DA704000402037BA50400047DA504000402037BA80400047DA804000402037BA90400047DA904000402037BAA0400047DAA04000402037BAB0400047DAB04000402037BAC0400047DAC04000402037BAD0400047DAD040004002A000003300200580000000000000002283400000A000002037DA504000402047DA604000402057DA70400040216731100000A7DA804000402167DA904000402167DAA04000402167DAB0400040216731100000A7DAC0400040216731100000A7DAD040004002A9202030405280B0100060000020E047DA8040004020E057DAA04000402177DAB040004002A000000033006004700000000000000020304050E040E05280C010006000002257BA70400040E06282600000A7DA7040004020E067DAC04000402257BA60400040E07282600000A7DA6040004020E077DAD040004002A0013300200170000001E00001100027BA5040004027BA6040004282600000A0A2B00062A00133001000C0000001E00001100027BA70400040A2B00062A13300200220000001E00001100027BA5040004027BA6040004282600000A027BA7040004282600000A0A2B00062A0000133001000B000000580000110073090100060A2B00062A0013300300100100004300001100027BA90400042D16027BA804000416731100000A289000000A16FE012B0116000C082D0800020B38E100000002280E010006027BA8040004282100000A16FE010C082D20000216731100000A7DA50400040216731100000A7DA6040004020B38A9000000027BA80400040A027BA504000406285800000A16FE010C082D240002257BA504000406282500000A7DA504000416731100000A0A02177DA9040004002B1B0006027BA5040004282500000A0A0216731100000A7DA5040004000616731100000A282B00000A2C11027BA604000406285800000A16FE012B0117000C082D1B0002257BA604000406282500000A7DA604000402177DA9040004000216731100000A7DAC040004020B2B00072A133004003E0000005800001100027BA5040004037BA5040004282600000A027BA6040004037BA6040004282600000A027BA7040004037BA7040004282600000A730B0100060A2B00062A0000133004003E0000005800001100027BA5040004037BA5040004282500000A027BA6040004037BA6040004282500000A027BA7040004037BA7040004282500000A730B0100060A2B00062A000013300200410000000C00001100027BA5040004037BA5040004289000000A2C26027BA6040004037BA6040004289000000A2C13027BA7040004037BA7040004289000000A2B0116000A2B00062A00000013300200100000000C000011000203281501000616FE010A2B00062A13300200120000000C000011000203745100000228150100060A2B00062A0000133001000C00000021000011000228A100000A0A2B00062A133002000D00000058000011000216281A0100060A2B00062A000000133005007000000044000011000316FE010B072D3B00027BA5040004285F00000A027BA6040004285F00000A027BA7040004285F00000A027BA8040004285F00000A027BAA040004730C0100060A2B2A00027BA5040004285F00000A027BA6040004285F00000A027BA7040004285F00000A730B0100060A2B00062A1330020061000000590000110073090100060A06027BA50400047DA504000406027BA60400047DA604000406027BA70400047DA704000406027BA80400047DA804000406027BA90400047DA904000406027BAA0400047DAA04000406027BAB0400047DAB040004060B2B00072A0000001330030026000000200000110072F8900170027BAE0400046F1700000A027CAF040004282200000A288400000A0A2B00062A8602283400000A00000228110100067DAE0400040216731100000A7DAF040004002A9E02283400000A000002037BAE040004730A0100067DAE04000402037BAF0400047DAF040004002A7602283400000A00000203730A0100067DAE04000402047DAF040004002A0000133001000B0000005A00001100731D0100060A2B00062A00133003002D0000005A00001100027BAE040004037BAE0400042813010006027BAF040004037BAF040004282600000A731F0100060A2B00062A000000133003002D0000005A00001100027BAE040004037BAE0400042814010006027BAF040004037BAF040004282500000A731F0100060A2B00062A000000133002002E0000000C00001100027BAE040004037BAE04000428150100062C13027BAF040004037BAF040004289000000A2B0116000A2B00062A000013300200100000000C000011000203282301000616FE010A2B00062A13300200120000000C000011000203745200000228230100060A2B00062A0000133001000C00000021000011000228A100000A0A2B00062A033002009D0000000000000002166A7DB004000402177DB104000402167DB204000402167DB304000402167DB40400040272010000707DB504000402166A7DB704000402166A7DB804000402167DB904000402177DBA04000402177DBB0400040216731100000A7DBC04000402166A7DBD0400040228110100067DBE0400040228110100067DBF0400040216731100000A7DC00400040272010000707DC104000402283400000A002A9A02147DE004000402147DE104000402147DE204000402283400000A000002283101000600002A13300200220000005B00001100027BE10400046F4300000A166F4600000A166F6900000AA55C0000020A2B00062AB200027BE10400046F4300000A166F4600000A16038C5C0000026F7B00000A00027BE00400046F4800000A002A001B300400300100005C00001100027BE10400046F4300000A166F4600000A166F6900000AA55C00000213041104450300000002000000090000000D0000002B0F030D38F3000000040A2B35050A2B317212910170027BE10400046F4300000A166F4600000A166F6900000AA52D0000018C2D00000128A200000A73A300000A7A731500000A0B00027BE00400046F4300000A6F4A00000A13052B4711056F4B00000A740B0000010C00076FA400000A16FE0216FE01130611062D0E000772789600706F9F00000A26000708166F6900000A74280000016FA500000A6F9F00000A260011056F4C00000A130611062DACDE1D110575050000011307110714FE01130611062D0811076F1E00000A00DC00067248910170076F1700000A7E6D00000A285700000A2D040E042B06076F1700000A00284000000A0D2B00092A0110000002008D0058E5001D0000000013300300630000005D00001100027BE10400046F4300000A166F4600000A166F6900000AA55C00000216FE0116FE010B072D03002B380203283201000614FE010B072D03002B27027BE00400046F4100000A0A0616036F7B00000A00027BE00400046F4300000A066F4400000A002A0013300200270000005D00001100020328320100060A0614FE010B072D1500066FA600000A00027BE00400046FA700000A00002A0013300200590000005E00001100027BE10400046F4300000A166F4600000A166F6900000AA55C0000020B0745030000000200000006000000160000002B21170A2B210203283201000614FE0116FE010A2B110203283201000614FE010A2B04160A2B00062A000000133003003E0000005F00001100027BE10400046F4300000A166F4600000A166F6900000AA55C00000216FE0116FE010B072D0500140A2B1002027BE20400041728330100060A2B00062A0000133004003F0000000C000011000314FE0116FE010A062D0B000216282A010006002B27027BE10400046F4800000A00027BE00400046F4800000A0002027BE204000403182834010006002A0013300500F70000006000001100160B02724E91017073A800000A7DE204000402726891017073A900000A7DE1040004027BE10400046F3700000A727A910170078C5C00000228AA00000A6F3900000A26027BE10400046F4100000A0A0616168C5C0000026F7B00000A00027BE10400046F4300000A066F4400000A0002728491017073A900000A7DE0040004027BE00400046F3700000A7296910170724D0E0070283800000A6F3900000A26027BE0040004178D370000010C0816027BE00400046F3700000A166FAB00000AA2086F3F00000A00027BE20400046FAC00000A027BE10400046FAD00000A00027BE20400046FAC00000A027BE00400046FAD00000A002A001B30030070000000610000110000027BE00400046F4300000A6F4A00000A0C2B2D086F4B00000A740B0000010A000306166F6900000A7428000001196FAE00000A16FE010D092D0500060BDE2C00086F4C00000A0D092DC9DE1A0875050000011304110414FE010D092D0811046F1E00000A00DC00140B2B0000072A01100000020013003B4E001A000000001B30030038000000620000110072010000700A73AF00000A0B000307046FB000000A00076F1700000A0A00DE100714FE010D092D07076F1E00000A00DC00060C2B00082A0110000002000D0014210010000000001B3003002700000063000011000473B100000A0A000306056FB200000A2600DE100614FE010B072D07066F1E00000A00DC002A0001100000020008000D150010000000001B300300D1000000640000110000731500000A0B07166F6200000A000772A09101706F1600000A260772C89101706F1600000A260772FA9101706F1600000A260772789201706F1600000A2607728A9201706F1600000A26076F1700000A036F1800000A03731900000A0C00086F1A00000A72E08601701E6F1B00000A028C2D0000016F1C00000A00086F2E00000A0A062C0A067E2F00000AFE012B011700130511052D100006A52D0000010D0916FE021304DE2400DE120814FE01130511052D07086F1E00000A00DC0000DE05260000DE00001613042B000011042A000000011C000002005F004DAC00120000000000000100C1C20005010000011B3005009F01000065000011000573420100065100731500000A0A06166F6200000A000672D09201706F1600000A260672225C01706F1600000A260672489301706F1600000A260672EF9301706F1600000A260672239401706F1600000A2606726B9401706F1600000A260672CF9401706F1600000A2606727A9501706F1600000A260672259601706F1600000A26066F1700000A0E046F1800000A0E04731900000A0C00086F1A00000A72AB0B00701F0C6F1B00000A026F1C00000A00086F1A00000A72919601701F0C6F1B00000A032C03032B057201000070006F1C00000A00086F1A00000A72AF9601701E6F1B00000A0314FE018C430000016F1C00000A00086F1A00000A72D79601701E6F1B00000A048C2D0000016F1C00000A00086F1A00000A72F79601701E6F1B00000A168C2D0000016F1C00000A00086F2E00000A0B072C0A077E2F00000AFE012B011700130511052D150007A52D0000010D09050E0428380100061304DE3A0314FE01130511052D0E00160203040E0428370100062600171304DE1D0814FE01130511052D07086F1E00000A00DC260000DE00001613042B000011042A00413400000200000098000000E60000007E01000012000000000000000000000008000000880100009001000005000000010000011B300300CD0100002C0000110000731500000A0A06166F6200000A000672219701706F1600000A260672419701706F1600000A260672499701706F1600000A260672639701706F1600000A2606729D9701706F1600000A260672ED9701706F1600000A260672419801706F1600000A2606729D9801706F1600000A260672A59801706F1600000A260672019901706F1600000A260672639901706F1600000A260672CD9901706F1600000A2606723D9A01706F1600000A260672979A01706F1600000A260672F59A01706F1600000A2606725B9B01706F1600000A260672C79B01706F1600000A260672D59B01706F1600000A260672139C01706F1600000A260672759C01706F1600000A260672C99C01706F1600000A260672219D01706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A72819D01701E6F1B00000A028C2D0000016F1C00000A00076F1A00000A72AB0B00701F0C6F1B00000A036F1C00000A00076F1A00000A72919601701F0C6F1B00000A046F1C00000A00076F1A00000A72D79601701E6F1B00000A058C2D0000016F1C00000A00076F1D00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00000041340000020000002D01000083000000B001000010000000000000000000000001000000BF010000C0010000050000000100000113300500180000000C0000110002720100007072010000700304283B0100060A2B00062A13300500140000000C00001100160272010000700304283B0100060A2B00062A13300500140000000C00001100167201000070020304283B0100060A2B00062A1B300400570500006600001100726498007072939D01700E0428EF0000061203284900000A130711072D0400160D0005734201000651731500000A0A000672BF9D01706F1600000A260672489E01706F1600000A260672D19E01706F1600000A2606725A9F01706F1600000A260672E39F01706F1600000A2606726CA001706F1600000A260672F5A001706F1600000A2606727EA101706F1600000A26067207A201706F1600000A26067290A201706F1600000A26067219A301706F1600000A260672A2A301706F1600000A2606722BA401706F1600000A260672B4A401706F1600000A2606723DA501706F1600000A260672C6A501706F1600000A2606724DA601706F1600000A260672D6A601706F1600000A2606725FA701706F1600000A260672E8A701706F1600000A26067271A801706F1600000A260672FAA801706F1600000A26067283A901706F1600000A2606720CAA01706F1600000A26067293AA01706F1600000A2606721AAB01706F1600000A260672A1AB01706F1600000A26067228AC01706F1600000A260672B1AC01706F1600000A2606723AAD01706F1600000A260672C3AD01706F1600000A2606724CAE01706F1600000A260672D5AE01706F1600000A2606725EAF01706F1600000A260672E7AF01706F1600000A2603283500000A130711072D1000067270B001706F1600000A26002B2C0004283500000A130711072D10000672B8B001706F1600000A26002B0E0006721AB101706F1600000A260000066F1700000A0E046F1800000A0E04731900000A13040011046F1A00000A726CB101701E6F1B00000A1A8C2E0000026F1C00000A0003283500000A130711072D200011046F1A00000A72492300701F0C1F326F2D00000A036F1C00000A00002B4E0004283500000A130711072D200011046F1A00000A7288B101701F0C1F326F2D00000A046F1C00000A00002B200011046F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A00000011046F3000000A13050011056F3100000A130711072D0900161306DD6702000005501105166F3300000A7DEB0400041105176F6C00000A130711072D110005501105176F3300000A7DEC0400040005501105186FB300000A7DED04000405501105196F6B00000A7DEE040004055011051A6F6B00000A7DEF040004055011051B6F7700000A7DF0040004055011051C6F3300000A7DF1040004055011051D6F6C00000A2D0A11051D6F3200000A2B02166A007DF3040004055011051E6F6C00000A2D0A11051E6F3200000A2B02166A007DF4040004055016731100000A11051F096F6100000A282400000A7DF6040004055011051F0A6F7700000A7DF7040004055011051F0B6F7700000A7DF804000411051F0C6F3300000A0B11051F0D6F3300000A0C055009085F0708665F607DF5040004055011051F0E6F3300000A7DF2040004055011051F0F6F6C00000A2D0B11051F0F6F6B00000A2B057E6D00000A007D07050004055011051F106F6C00000A2D0B11051F106F6B00000A2B057E6D00000A007D08050004055011051F116F6B00000A7D09050004055011051F126F6B00000A7D0A050004055011051F136F3300000A7D0B050004055011051F146F6C00000A2D0B11051F146F6B00000A2B057E6D00000A007D0C05000405507BF204000413081108450200000002000000020000002B022B0A0550167DF20400042B0005507BF204000417FE0116FE01130711072D320005507BED0400041BFE0116FE01130711072D1D000550186F4101000616FE01130711072D0A000550187DF204000400000000DE14110514FE01130711072D0811056F1E00000A00DC0000DE14110414FE01130711072D0811046F1E00000A00DC00171306DE0B260000DE00001613062B000011062A00414C000002000000D5020000410200001605000014000000000000000200000033020000FB0200002E05000014000000000000000000000030000000180500004805000005000000010000011B3003007401000067000011000313047E6D00000A0B7E6D00000A0C7E6D00000A0D047E6D00000A51731500000A0A0672B2B101706F1600000A260672EEB101706F1600000A2606722AB201706F1600000A26067266B201706F1600000A260672A2B201706F1600000A2600066F1700000A056F1800000A05731900000A13050011056F1A00000A72F8B20170166F1B00000A028C2D0000016F1C00000A0011056F3000000A1306002B53001106166F6C00000A2D0A1106166F6B00000A2B057201000070000B1106176F6C00000A2D0A1106176F6B00000A2B057201000070000C1106186F6C00000A2D0A1106186F6B00000A2B057201000070000D0011066F3100000A130811082DA000DE14110614FE01130811082D0811066F1E00000A00DC0000DE14110514FE01130811082D0811056F1E00000A00DC0011047210B30170076FB400000A13041104721CB30170086FB400000A130411047230B30170096FB400000A130404110451171307DE0B260000DE00001613072B000011072A414C0000020000009C000000660000000201000014000000000000000200000074000000A60000001A0100001400000000000000000000005F00000006010000650100000500000001000001133002000C0000000C0000110002035F03FE010A2B00062A13300300D80200006800001100731500000A0A067242B301706F1600000A26067264B301706F1600000A26067288B301706F1600000A260672C0B301706F1600000A26067222B401706F1600000A26067288B401706F1600000A2606729AB401706F1600000A260672D6B401706F1600000A26067218B501706F1600000A26067260B501706F1600000A260672AAB501706F1600000A260672EEB501706F1600000A26067232B601706F1600000A26067278B601706F1600000A260672B6B601706F1600000A260672F6B601706F1600000A2606723EB701706F1600000A26067288B701706F1600000A260672D8B701706F1600000A2606722AB801706F1600000A26067274B801706F1600000A260672C0B801706F1600000A26067206B901706F1600000A2606722AB901706F1600000A2606724AB901706F1600000A26067288B901706F1600000A260672CAB901706F1600000A2606720EBA01706F1600000A26067250BA01706F1600000A2606729ABA01706F1600000A260672E6BA01706F1600000A2606722ABB01706F1600000A26067270BB01706F1600000A26067288B901706F1600000A26067250BA01706F1600000A2606729ABA01706F1600000A260672E6BA01706F1600000A2606722ABB01706F1600000A260672BEBB01706F1600000A26067206B901706F1600000A2606724E5600706F1600000A260672F6BB01706F1600000A26067216BC01706F1600000A26067248BC01706F1600000A260672B4BC01706F1600000A2606721EBD01706F1600000A260672C5BD01706F1600000A2606726EBE01706F1600000A2606720FBF01706F1600000A260672B2BF01706F1600000A2606721CC001706F1600000A260672C3C001706F1600000A2606726CC101706F1600000A2606720DC201706F1600000A260672B0C201706F1600000A2606725BC301706F1600000A260672BBC301706F1600000A26067221C401706F1600000A26066F1700000A026F1800000A02731900000A0B070C2B00082A1B300300890100006900001100160A731500000A0B07728BC401706F1600000A2607724EC501706F1600000A26077211C601706F1600000A260772D4C601706F1600000A26077297C701706F1600000A2607725AC801706F1600000A2607721DC901706F1600000A260772E0C901706F1600000A260772A3CA01706F1600000A26077266CB01706F1600000A26077229CC01706F1600000A260772ECCC01706F1600000A260772AFCD01706F1600000A26077272CE01706F1600000A26077235CF01706F1600000A260772F8CF01706F1600000A260772BBD001706F1600000A2607727ED101706F1600000A26077241D201706F1600000A2600076F1700000A036F1800000A03731900000A0C00086F1A00000A7204D301701E6F1B00000A198C220000026F1C00000A00086F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A00086F1A00000A7232D301701E6F1B00000A168C220000026F1C00000A00086F1D00000A26170A00DE120814FE01130411042D07086F1E00000A00DC0000DE072600160A00DE0000060D2B00092A000000011C00000200010164650112000000000000ED008E7B0107390000011B300300630000002F0000110004155400724CD30170036F1800000A03731900000A0A00066F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0004066F2E00000AA54C00000154170BDE1A0614FE010C082D07066F1E00000A00DC260000DE0000160B2B0000072A00011C0000020017002F4600100000000000000400525600050100000113300200120000000C00001100027BF504000403283D0100060A2B00062A000003300200790000000000000002167DEB04000402167DEC04000402157DED0400040272010000707DEE0400040272010000707DEF04000402177DF004000402187DF104000402167DF204000402156A7DF304000402166A7DF404000402167DF50400040216731100000A7DF604000402177DF704000402167DF804000402283400000A002A000000133003002D0000006A00001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B0E1200021628470100060B060C2B00082A00000013300300390000006B00001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B1A021200120128460100060D092D0872010000700C2B04060C2B00082A1E02283400000A2A0000001B300400A00100006C000011001E8D54000001130A0372010000705104165400026FB600000A1F0DFE0416FE01131311132D0900161312DD6C010000736C0100060A061F406FB700000A00067E0E0500047E0F0500046FB800000A130D156A1314121428B900000A130C02110C28BA00000A16FE0216FE01131311132D0900161312DD210100000228BB00000A1307110728BC00000A130E110E73BD00000A130F110F110D1673BE00000A13101110110A161E6FBF00000A26110A1628C000000A1308120828B900000A1000021F141F306F8B00000A10000216176FC100000A0B02171A6FC100000A0C021B1F0D6FC100000A0D021F12186FC100000A13040772E3D30170285700000A16FE01131311132D0900161312DD8C000000161305091104286600000A28BB00000A1309110928BC00000A130B1613112B12001105110B111191581305001111175813111111110B8E69FE04131311132DE0110520F52600005E130572E7D3017011058C4B000001287E00000A1306081106285700000A16FE01131311132D0600161312DE1803095104110428C200000A54171312DE072600161312DE000011122A411C00000000000013000000820100009501000007000000010000011B300400460100006D000011000272010000705100036FB600000A1F0DFE01130D110D2D090016130CDD200100000416FE01130D110D2D090016130CDD0D010000736C0100060A061F406FB700000A00067E0E0500047E0F0500046FC300000A130673C400000A1307110711061773BE00000A13080372F5D30170048C2D000001287E00000A286600000A28BB00000A1304110428BC00000A1305160D1613092B1000091105110991580D00110917581309110911058E69FE04130D110D2DE20920F52600005E0D72E7D30170098C4B000001287E00000A0C080372F5D30170048C2D000001287E00000A284000000A0B0728BB00000A1304110428BC00000A130A1108110A16110A8E696FC500000A0011086FC600000A0011076FC700000A130B02110B1628C000000A130E120E28B900000A510202501F141F306F8B00000A5117130CDE07260016130CDE0000110C2A0000411C00000000000008000000330100003B0100000700000001000001133003002B000000200000110072E7D30170028C2D000001287E00000A7203D40170038C27000001287E00000A286600000A0A2B00062A7E000302161A6FC100000A28C800000A5404021A6FC900000A28CA00000A552A00133003003D0000006E0000110003120212012846010006130511052D06001613042B230716FE01130511052D06001613042B1308120012032849010006000206FE0113042B0011042A00000013300300220000006F000011000403510217FE0116FE010C082D0D000304120028460100060B2B04170B2B00072A00000000000023215E48475E2423464453484A76622D1330030029000000700000111F108D5400000125D01C05000428CC00000A800E050004178D540000010A0616179C06800F0500042A1E02283400000A2AEE02283400000A000002036FCD00000A740400001B7D1305000402038E697D140500040220000100008D540000017D1505000402285501000600002A00000013300100070000000C00001100170A2B00062A0013300100070000000C00001100170A2B00062A0013300100070000002100001100170A2B00062A0013300100070000002100001100170A2B00062A00133005005D0100007100001100027B1805000416FE01130411042D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE01130411042D11007211D401707229D4017073D000000A7A0E0414FE0116FE01130411042D11007273D40170728DD4017073D000000A7A0416321C0E05163217040558038E69300F0E0505580E048E69FE0216FE012B011600130411042D0C0072D9D4017073D100000A7A0405580C38AE0000000002027B16050004175820000100005DD27D1605000402027B17050004027B15050004027B16050004915820000100005DD27D17050004027B15050004027B16050004910B027B15050004027B16050004027B15050004027B17050004919C027B15050004027B17050004079C027B15050004027B1605000491027B15050004027B17050004915820000100005DD20A0E040E05030491027B15050004069161D29C0004175810020E05175810050408FE04130411043A45FFFFFF050D2B00092A00000013300600400000007200001100027B1805000416FE010C082D12000228AA00000A6FCE00000A73CF00000A7A058D540000010A02030405061628530100062602285501000600060B2B00072A13300400910000007300001100160B2B1000027B150500040707D29C000717580B07027B150500048E69FE040D092DE102167D1605000402167D17050004160C160B2B490008027B15050004079158027B1305000407027B140500045D915820000100005D0C027B1505000407910A027B1505000407027B1505000408919C027B1505000408069C000717580B07027B150500048E69FE040D092DA82A00000013300300540000000C00001100027B180500040A062D4800027B1305000416027B130500048E6928D200000A00027B1505000416027B150500048E6928D200000A0002167D1605000402167D1705000402177D180500040228D300000A00002A4A0228D400000A0000021F407DD500000A002A00133001000700000021000011001E0A2B00062A0013300200160000000C00001100031EFE010A062D0C007229D5017073D600000A7A2A1E0073D700000A7A1E0073D700000A7A0000133001000C0000007000001100178D540000010A2B00062A13300200220000000C00001100032C0B038E6917FE0216FE012B0117000A062D0C007271D5017073D600000A7A2A000013300500190000007400001100178D130000010B07161E1E1673D800000AA2070A2B00062A000000133005001D0000007400001100178D130000010B07161E20000800001E73D800000AA2070A2B00062A00000013300100070000007500001100190A2B00062A0013300200160000000C000011000319FE010A062D0C0072A5D5017073D600000A7A2A000013300100070000007600001100170A2B00062A0013300200160000000C000011000317FE010A062D0C0072E5D5017073D600000A7A2A0A002A0000001330020026000000770000110073D900000A0A026FDA00000A1E5B8D540000010B06076FDB00000A0002076FDC00000A002A000013300100100000007800001100722DD6017028670100060A2B00062A133003005B00000079000011000214FE0116FE010B072D1100723DD60170724DD6017073D000000A7A02727DD601701928DD00000A16FE010B072D0900736C0100060A2B2002722DD601701928DD00000A16FE010B072D090073680100060A2B04140A2B00062A2A0228570100060000002A0000133002008D0000007A00001100027B1905000416FE010B072D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE010B072D11007285D601707293D6017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C0072D3D6017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C007217D7017073D600000A7A03734E0100060A2B00062A000000133003000E0000007B000011000203046FB800000A0A2B00062A4600020328DE00000A0002177D190500042A5602285701000600000273680100067D1A050004002A000013300100110000002100001100027B1A0500046FDF00000A0A2B00062A00000013300200230000000C00001100031EFE010A062D0C007259D7017073D600000A7A027B1A050004036FE000000A002A0013300100110000002100001100027B1A0500046FE100000A0A2B00062A3E00027B1A050004036FE200000A002A00000013300100110000007000001100027B1A0500046FE300000A0A2B00062A3E00027B1A050004036FE400000A002A00000013300100110000007000001100027B1A0500046FE500000A0A2B00062A3E00027B1A050004036FDC00000A002A00000013300100110000002100001100027B1A0500046FDA00000A0A2B00062A3E00027B1A050004036FB700000A002A00000013300100110000007C00001100027B1A0500046FE600000A0A2B00062A00000013300100110000007C00001100027B1A0500046FE700000A0A2B00062A00000013300100110000007500001100027B1A0500046FE800000A0A2B00062A3E00027B1A050004036FE900000A002A00000013300100110000007600001100027B1A0500046FEA00000A0A2B00062A3E00027B1A050004036FEB00000A002A3A00027B1A0500046FEC00000A002A3A00027B1A0500046FED00000A002A0013300300990000007A00001100027B1B05000416FE010B072D17000228AA00000A6FCE00000A7295D7017073EE00000A7A0314FE0116FE010B072D110072C7D7017072D5D7017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C007215D8017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C007259D8017073D600000A7A027B1A05000403046FB800000A0A2B00062A000000133003000E0000007B000011000203046FB800000A0A2B00062A0000133002003E0000000C00001100027B1B0500040A062D320002177D1B050004027B1A05000414FE010A062D1500027B1A0500046FEF00000A0002147D1A050004000228D300000A00002A00001B3005004C0100007D000011000E061A540E07721F0A0070510E087201000070510E04166A550E0516731100000A810800000100729BD8017073F100000A0B00076FF200000A00076FF300000A0C000872CBD801706F1200000A0000020304050828010000060A060E060E070E08280700000600067B500400041F0CFE0116FE01130411042D0B000E04067B480400045500067B5004000416FE0116FE01130411042D34000E04067B48040004550E05067B4B0400046F1001000681080000010E0616540E0772FDD80170067B4F040004286600000A510000DE1A0D00000872CBD801706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E061A540E07721F0A0070510E087215D90170096F7500000A286600000A5100DE00002A014C00000000D10010E100050100000100004F0080CF001A3900000102004F009DEC000A0000000002004200B8FA00120000000002003300DD1001120000000000002700FF260124390000011B3008001B0100007E000011000E0B1A540E0C721F0A0070510E0D72010000705100729BD8017073F100000A0C00086FF200000A00086FF300000A0D00097271D901706F1200000A000073F70000060B070E076A7D64040004070E057D65040004070E086A7D66040004070E067D670400040716731100000A7D68040004070E0A7D69040004020304050E04070E090928020000060A060E0B0E0C0E0D28080000060000DE1C13040000097271D901706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1E00000A00DC0000DE120814FE01130511052D07086F1E00000A00DC0000DE261304000E0B1A540E0C721F0A0070510E0D72A5D9017011046F7500000A286600000A5100DE00002A00014C000000009D0010AD00050100000100003D005D9A001C3900000102003D007CB9000A000000000200300097C700120000000002002100BCDD00120000000000001500DEF30026390000011B3008001B0100007E000011000E0B1A540E0C721F0A0070510E0D72010000705100729BD8017073F100000A0C00086FF200000A00086FF300000A0D00097203DA01706F1200000A000073F70000060B070E076A7D64040004070E057D65040004070E086A7D66040004070E067D670400040716731100000A7D68040004070E0A7D69040004020304050E04070E090928030000060A060E0B0E0C0E0D28080000060000DE1C13040000097203DA01706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1E00000A00DC0000DE120814FE01130511052D07086F1E00000A00DC0000DE261304000E0B1A540E0C721F0A0070510E0D7231DA017011046F7500000A286600000A5100DE00002A00014C000000009D0010AD00050100000100003D005D9A001C3900000102003D007CB9000A000000000200300097C700120000000002002100BCDD00120000000000001500DEF30026390000011B300400F20000007D000011000516731100000A81080000010E041A540E05721F0A0070510E0672010000705100729BD8017073F100000A0B00076FF200000A00076FF300000A0C00087289DA01706F1200000A00000203040828040000060A05067B4C0400046F100100068108000001060E040E050E0628070000060000DE1A0D0000087289DA01706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E041A540E05721F0A0070510E0672B5DA0170096F7500000A286600000A5100DE00002A0000014C0000000077001087000501000001000049002C75001A39000001020049004992000A0000000002003C0064A000120000000002002D0089B600120000000000002100ABCC0024390000011B300700000100007D000011000E061A540E07721F0A0070510E0872010000705100729BD8017073F100000A0B00076FF200000A00076FF300000A0C0008720BDB01706F1200000A0000020304050E040E050828050000060A060E060E070E08280700000600067B5004000416FE0116FE01130411042D14000E060E041F646A5869540E077201000070510000DE1A0D000008720BDB01706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E061A540E07721F0A0070510E08722FDB0170096F7500000A286600000A5100DE00002A014C000000008500109500050100000100003D004683001A3900000102003D0063A0000A00000000020030007EAE00120000000002002100A3C400120000000000001500C5DA00243900000142534A4201000100000000000C00000076322E302E35303732370000000005006C00000098770000237E0000047800009086000023537472696E67730000000094FE000080DB01002355530014DA020010000000234755494400000024DA0200241D000023426C6F620000000000000002000010579FA2290902000000FA253300160000010000006A000000690000001C05000086010000A303000003000000F4000000F303000018000000010000007E0000000E0000005200000089000000040000000100000001000000020000001B00000000000A000100000000000600790772070600800772070600850772070600AD0790070600BE0772070600CA0790070A00FF07E9070600140872070A00AE08DD0706002C0A72070A004D0ADD070600940A72070600615D72070600165F0A5F0A00746CDD070A00EB6CDD070A00056DDD070A00C06DE9070600497290070600787290070600957290070600A27483740600457B337B06005C7B337B0600797B337B0600987B337B0600B17B337B0600CA7B337B0600E57B337B0600007C337B0600197C837406002D7C837406003B7C337B0600547C337B0600847C717C8F00987C00000600C77CA77C0600E77CA77C06001F7D72070600257D72070A003E7DE9070A005B7DE9070A00817DE9070A008E7DDD070600987D72070A00B17D9E7D0A00C77D9E7D0600F47D72070600297E720706003E7E72070A00457EE9070A00617E9E7D0A00937EDD0706001E5672070A00BC7EDD070A003D7FDD070600587F72070A00627FDD0706009A7F877F0A00DA7FDD070A00FB7FDD070A002080E9070A0041809E7D0A00ED80E9070600FA8072070600098172070600658172070A009481DD070600A58172070A00BC81DD070A00F8819E7D0A0049829E7D0600868272070A00A382DD070600BF8272070600DB8272070A003C83DD0706005B837207060076836C83060083836C83060097836C830600A4836C830A00DB83C0830600F08372070600F583720706000A847207060020846C8306002D84900706003A846C830600418490070600DF84A77C0600FA84720706003585A77C06004485720706004A85720706007A8572070600928572070600A88572070600C48572070600E58590070600FC85720706001286900706002B8690070A004186C0830A0057869E7D0A007A869E7D0000000001000000000001000100810110001F002400050001000100010010002F002400050001000B000100100044002400050001000E00010010005A002400050009002000010010006F0024000500090022000100100082002400050013002400010100009800240009001700440001010000A200240009002B00440001010000BC00240009003500440001010000D100240009004200440001010000DE00240009004D00440001010000EF00240009007101440001010000FC002400090013024400010100000C0124000900220244000101000018012400090029024400010100002501240009002D024400010100003701240009003F024400010100004B0124000900430244000101000061012400090048024400010100007A01240009004D024400010100009701240009005002440001010000B901240009005302440001010000CC01240009006502440001010000E601240009006A02440001010000F401240009007C0244000101000001022400090087024400010100001002240009008B02440001010000240224000900910244000101000038022400090095024400010100004E02240009009902440001010000680224000900C002440001010000730224000900DC02440001010000890224000900E102440001010000A00224000900E602440001210000B80224000900ED02440001010000CD0224000900F802440001010000DD0224000900FD02440001010000EB02240009005003440001010000FE02240009007B034400010100000903240009007F034400010100001803240009008203440001001000290324000500850344000100100040032400050098036C0001010000540324000900A9038F0001010000670324000900B2038F00010100007C0324000900BD038F0001010000960324000900C1038F0001010000AD0324000900C6038F0001010000BF0324000900CC038F0001010000D20324000900D5038F0001010000DE0324000900DB038F0001010000EB0324000900E1038F0001010000F80324000900E6038F0001010000090424000900ED038F0001010000160424000900F0038F00010010002F0424000500F5038F00010100004D0424000900FB039C0001010000560424000900FE039C000101000073042400090003049C000101000083042400090007049C00010100009904240009000B049C0001010000A6042400090014049C0001010000C6042400090018049C0001010000D104240009001C049C0001010000E1042400090022049C0001010000EA04240009002C049C0081011000F8042400050032049C00020100000405000009003304F000020100001705000009003B04F000020010003005000005004104F000020010004205000005004804F100020010005505000005005204F600020010006505000005006404F700020010007505000005006B04F8000200100087050000050071040001020100009F05000009007404010102001000B005000005008004010102001000C105000005008304060102001000D505000005009204070102001000F00500000500A504080102001000FF0500000500AE041C01020010000C0600000500B004270102010000180600000900C204280102010000300600000900C5042801020100003F0600000900C9042801020100004C0600000900CE04280102010000620600000900D2042801020100007D0600000900D604280102010000A40600000900DA04280101001000B40624000500E004280102010000C10600000900E304350181011000D20624000500E704350102010000DB0600000900E704410102001000EB0600000500EB04410101001000F806000005000D054301010010000207000005000D054601020100000D070000090010054E010001100017070000050013054E01810010002F0700001900190557010101100033070000900119056801010110004207000090011A056C01810110005B07240005001C058201000000009A84000005001C058701130100000485000071011D0587010100B80895000100C00899000100D7089D000100E508A0000100F508A30001000709A00001001409A30001002109A0000600BE09A3000600CF099D000600DA099D000600E109A0000600EE09A0000600F7099D0006000A0AA00006001F0A56010600350A590106003F0A5D010100B80895000100550A66010100C008990001005B0AA3000606DE0B9D005680E60BB4025680EB0BB4025680FF0BB40256801D0CB4025680350CB40256804F0CB4025680700CB4025680910CB4025680B20CB4025680D30CB4025680F40CB4025680150DB4025680360DB4025680570DB40256806E0DB4025680800DB40256809E0DB4025680C10DB4025680E20DB4020606DE0B9D005680090E17035680110E17035680150E17035680190E17035680240E170356802A0E170356802F0E17035680340E17035680380E17030606DE0B9D005680090E2F0356804B0E2F035680530E2F0356805A0E2F035680640E2F0356806C0E2F035680750E2F035680810E2F035680870E2F0356809B0E2F035680A50E2F035680B90E2F030606DE0B9D005680C50E5D015680D20E5D015680D70E5D015680E40E5D015680F10E5D015680FC0E5D015680050F5D015680160F5D015680250F5D0156802E0F5D010606DE0B9D005680C50E47035680380F47035680450F47035680530F470356805D0F47035680680F47035680700F47035680790F47035680870F47035680A00F47035680A70F47035680B70F47035680C80F47035680DE0F47035680F50F470356800010470356800E10470356801C10470356803010470356803E10470356804B10470356805F10470356807110470356808210470356809410470356809C1047035680B11047035680C01047035680DC1047035680EB1047035680FD10470356800811470356801711470356802211470356803411470356804211470356805011470356806C11470356807911470356808711470356809C1147035680AF1147035680C21147035680D71147035680EF11470356800912470356802112470356803C12470356804C12470356805E12470356807012470356808012470356809A1247035680B21247035680C01247035680D91247035680EE12470356800513470356801E13470356803513470356804A13470356806613470356807613470356808D1347035680AB1347035680BC1347035680CB1347035680D81347035680F313470356800114470356801614470356802614470356803814470356805114470356806F14470356808C1447035680B11447035680D71447035680FD14470356801615470356803415470356805115470356806B15470356808A1547035680A01547035680B61547035680CB1547035680DF1547035680F315470356800B16470356801F16470356803016470356804516470356805916470356806D16470356808F1647035680BC1647035680E016470356801117470356802517470356804917470356806D1747035680921747035680BB1747035680DA17470356800018470356802618470356804A18470356806E18470356808F1847035680B01847035680C51847035680EA18470356801B1947035680471947035680771947035680A11947035680CB1947035680FD1947035680261A470356804F1A47035680621A47035680761A47035680891A470356809C1A47035680C11A47035680DB1A47035680F61A47035680091B470356802E1B47035680521B47035680771B470356809B1B47035680BA1B47035680D91B47035680F71B47035680141C47035680271C47035680391C47035680561C47035680741C47035680911C47035680A41C47035680B61C47035680CE1C47035680E71C47035680F91C470356800C1D470356801B1D470356802B1D47035680511D47035680751D470356809E1D47035680CE1D47035680E11D47035680EC1D47035680FB1D470356801A1E470356802B1E47035680401E47035680501E47035680611E47035680791E47035680961E47035680B01E47035680CE1E47035680E81E47035680FC1E47035680151F47035680341F47035680471F470356805C1F470356806B1F47035680811F470356808B1F47035680B31F47035680DB1F470356800A2047035680392047035680672047035680952047035680A92047035680CC2047035680EF20470356800F21470356802F2147035680582147035680812147035680A92147035680D02147035680ED21470356800822470356801E22470356804022470356805C2247035680772247035680942247035680AB2247035680C82247035680E422470356800223470356801C23470356803723470356805223470356807223470356809E2347035680C42347035680E923470356801024470356803124470356805824470356807E2447035680A62447035680BD2447035680D72447035680F624470356800D25470356802D25470356804E2547035680802547035680AE2547035680D42547035680F825470356801026470356802826470356803F26470356805626470356806D2647035680842647035680962647035680C12647035680EC26470356800B27470356802D2747035680572747035680852747035680A72747035680D52747035680E427470356800728470356801328470356802928470356803828470356805F28470356807C2847035680A22847035680BE2847035680E228470356800529470356801C29470356803929470356805729470356806F2947035680902947035680AF2947035680CA2947035680E52947035680FB2947035680112A47035680282A470356803F2A470356805E2A470356807F2A47035680A22A47035680C52A47035680DB2A47035680F42A47035680172B470356803A2B470356805E2B47035680822B47035680A52B47035680C72B47035680E12B47035680052C47035680212C47035680382C470356805B2C470356806E2C470356808B2C47035680AD2C47035680C62C47035680E42C47035680002D47035680212D470356803D2D470356805E2D470356807A2D470356809B2D47035680B12D47035680C82D47030606DE0B9D005680E02DA5085680E52DA5085680EC2DA5085680F42DA5085680FF2DA50856800B2EA50856801C2EA50856802B2EA5085680342EA50856803F2EA5085680562EA5085680602EA50856806B2EA50856807F2EA50856808B2EA5085680982EA5085680AA2EA5085680B72EA5085680C32EA5085680D52EA5085680E52EA5085680F42EA5085680102FA5085680202FA5085680302FA5085680382FA50856804C2FA50856805D2FA5085680732FA5085680812FA50856809B2FA5085680A92FA5085680BD2FA5085680D32FA5085680E62FA5085680F42FA50856800C30A50856801F30A50856803230A50856803E30A50856806930A50856807C30A50856808930A50856809530A5085680AC30A5085680CD30A5085680DE30A5085680EF30A50856800C31A50856801E31A50856803331A50856804631A50856805331A50856806731A50856808131A50856809031A5085680A531A5085680BE31A5085680D831A5085680E831A5085680F631A50856800932A50856801D32A50856803932A50856804932A50856806532A50856807C32A50856809232A5085680A132A5085680B232A5085680BF32A5085680D132A5085680E232A5085680EC32A50856800333A50856801B33A50856802733A50856804533A50856806133A50856808333A5085680A233A5085680CB33A5085680EC33A5085680FF33A50856801C34A50856803934A50856804C34A50856807834A50856809934A5085680BA34A5085680D834A5085680F634A50856801835A50856804435A50856806A35A50856809235A5085680BD35A5085680E335A50856801036A50856803536A50856805A36A50856807436A50856808C36A5085680A736A5085680B636A5085680C936A5085680E136A5085680F436A50856801237A50856802A37A50856804137A50856805A37A50856806F37A50856808937A5085680A237A5085680BD37A5085680CC37A5085680DD37A50856800A38A50856803438A50856804D38A50856806438A50856807238A50856807D38A50856808E38A5085680A938A5085680CA38A5085680D838A5085680F238A50856801039A50856802C39A50856803F39A50856805239A50856806639A50856807A39A50856809639A5085680B439A5085680CC39A5085680E439A5085680043AA5085680243AA5085680373AA50856804D3AA5085680593AA5085680673AA50856807B3AA5085680853AA5085680913AA50856809D3AA5085680A83AA5085680C83AA5085680E73AA5085680033BA5085680233BA50856803C3BA5085680593BA5085680753BA5085680963BA5085680B23BA5085680D33BA5085680EF3BA5080606DE0B9D005680E60B94095680103C940956801E3C94095680223C94095680293C94095680433C94095680533C940956806A3C94095680793C940956808E3C94095680933C94095680A93C94095680BD3C94095680C93C94090606DE0B9D005680D23C9D095680E23C9D095680F83C9D095680043D9D095680133D9D0956802B3D9D090606DE0B9D005680E60BA10956801E3CA1095680223CA1090606DE0B9D005680443DA50956804B3DA5095680523DA50956805C3DA5095680693DA5095680733DA5095680823DA5095680973DA5095680A23DA5095680AC3DA5095680BF3DA5095680BD37A5095680CC37A50956804D38A50956804D3AA5095680593AA5095680673AA5090606DE0B9D005680443DA90956804B3DA9095680D43DA9090606DE0B9D005680D93DAD095680E13DAD095680E63DAD095680ED3DAD090606DE0B9D005680D93DB1095680F23DB1095680F73DB1095680FF3DB1090606DE0B9D005680043EB50956800C3EB5090606DE0B9D0056802B3EB9095680303EB9090606DE0B9D005680E60BBD095680383EBD095680453EBD095680513EBD095680603EBD095680693EBD0956807A3EBD095680813EBD095680913EBD095680A83EBD095680BD3EBD095680D63EBD095680E33EBD095680ED3EBD095680033FBD095680113FBD0956801F3FBD090606DE0B9D005680293FDF0956802D3FDF095680353FDF0956803A3FDF090606DE0B9D005680090EE30956801E3CE30956801F00E30956804E3FE3095680573FE30956805C3FE3095680693FE3095680753FE3095680853FE30956808C3FE3095680953FE3095680A33FE3095680B53FE3095680BD3CE3095680C93CE3095680BD3FE3095680D13FE3090606DE0B9D005680D83FE7095680EA3FE7095680F23FE70956800140E70956805C3FE70956800840E7095680090EE70956801C40E70956803540E70956804440E7090606DE0B9D005680530EFA0956805240FA0956806140FA090606DE0B9D0056806940FE0956807940FE0956809340FE095680A640FE095680B740FE090606DE0B9D005680090E020A5680C940020A5680D340020A0606DE0B9D005680090E060A5680DF40060A5680E940060A0606DE0B9D005680D93D0A0A5680F6400A0A56800C410A0A568022410A0A568035410A0A56804C410A0A568060410A0A568073410A0A56808D410A0A5680A7410A0A5680C0410A0A5680D7410A0A5680EC410A0A5680FE410A0A568017420A0A56802A420A0A56803D420A0A56804D420A0A568059420A0A568065420A0A568075420A0A56808E420A0A5680A5420A0A5680BC420A0A5680D3420A0A5680E6420A0A568009430A0A568016430A0A56802C430A0A568040430A0A56804F430A0A568062430A0A568071430A0A568087430A0A5680A4430A0A5680C2430A0A5680E1430A0A568005440A0A0606DE0B9D00568028440E0A56802F440E0A56803E440E0A56804D440E0A568062440E0A56806C440E0A568080440E0A568092440E0A5680A3440E0A5680B2440E0A5680BC440E0A5680CD440E0A5680DA440E0A5680E5440E0A5680F6440E0A56800B450E0A56801D450E0A568037450E0A56804E450E0A568067450E0A56807D450E0A568093450E0A5680AF450E0A5680C1450E0A5680C9450E0A5680D2450E0A5680DF450E0A0606DE0B9D005680F145180A5680FD45180A56800D46180A56802E46180A0606DE0B9D0056804A461D0A56804F461D0A568056461D0A568063461D0A0606DE0B9D005680E60B220A56807346220A56807946220A56808346220A56808F46220A56809A46220A0606DE0B9D005680A246270A5680AB46270A56807346270A5680B046270A5680BB46270A5680CA46270A5680DA46270A5680EC46270A5680F146270A56809A46270A0606DE0B9D005680F846360A56800547360A56800E47360A56807110360A0606DE0B9D00568025473B0A5680680F3B0A5680700F3B0A56802D473B0A5680F50F3B0A568037473B0A568044473B0A568051473B0A56805F473B0A56806B473B0A56807C473B0A5680761A3B0A5680621A3B0A5680E11D3B0A568096473B0A5680EC1D3B0A5680791E3B0A5680F8463B0A5680AF473B0A5680BA473B0A5680C7473B0A5680D7473B0A5680F2473B0A568011483B0A568020483B0A568035483B0A568046483B0A56805E483B0A5680AF113B0A568072483B0A568092483B0A56809D483B0A5680AA483B0A5680BF483B0A568094103B0A56809C103B0A5680DE483B0A5680F1483B0A5680FF483B0A56800C493B0A568019493B0A568030493B0A568052493B0A568067493B0A5680ED213B0A568071103B0A56807D493B0A5680A2493B0A5680F6243B0A5680C4493B0A5680DD493B0A5680F8493B0A5680AD2C3B0A5680184A3B0A5680344A3B0A568084263B0A56804C4A3B0A5680624A3B0A56807D4A3B0A5680944A3B0A5680B04A3B0A5680C94A3B0A568029283B0A5680D44A3B0A5680E14A3B0A5680FB4A3B0A5680144B3B0A5680314B3B0A5680E5293B0A5680FB293B0A5680112A3B0A5680282A3B0A56803F2A3B0A56805E2A3B0A5680AF293B0A5680CA293B0A56807F2A3B0A5680A22A3B0A5680382C3B0A5680464B3B0A5680564B3B0A5680644B3B0A0606DE0B9D0056802844810A5680774B810A5680804B810A5680894B810A5680934B810A56809C4B810A5680AC4B810A5680C04B810A5680CA4B810A5680D44B810A5680DD4B810A5680E94B810A5680FD4B810A5680094C810A5680154C810A5680284C810A5680324C810A56803C4C810A5680494C810A56805A4C810A56806D4C810A5680814C810A56808D4C810A56809C4C810A5680AC4C810A5680BE4C810A5680102F810A5680CF4C810A5680E74C810A5680024D810A5680204D810A5680334D810A5680454D810A56806438810A5680564D810A56807238810A5680604D810A5680734D810A5680834D810A5680954D810A5680D245810A5680DF45810A0606DE0B9D005680A44D860A5680A94D860A5680B04D860A0606DE0B9D005680BE4D8B0A5680C64D8B0A0606DE0B9D005680D24D900A5680E54D900A0100F44D950A0100004EA00001000E4EA00001001D4E950A01002C4E950A0100384E950A01004C4E950A0100604E950A01006D4E950A01007A4E950A01008C4E950A01009A4E9D000100A94E56010100C04E56010100D94E270A0100E34EA3000100FA4E990A0100124F950A01002A4F220A0100E552A3000100F452220A0100FB52A00001000D53A00001001953A000010027539D0001003653A0000100F44D950A01004853A00001005853A00001006F53560101008253A00001009953A0000100B053A0000100BA5359010100CB53360A0100DE53A0000606DE0B9D005680C356210B5680C756210B5680CD56210B5680D256210B5680D756210B5680DC56210B5680E156210B5680E656210B0606DE0B9D005680C756260B56802D47260B5680EC56260B5680EA3F260B56800157260B56800A57260B56801657260B56802257260B56803357260B56804057260B0606DE0B9D00568053572B0B56805B572B0B56805F572B0B0606DE0B9D005680A44D300B56806257300B56806F57300B56807C57300B0606DE0B9D0056805357350B56808157350B56809557350B56809D57350B5680A457350B0606DE0B9D005680B5573A0B5680BD573A0B5680C5573A0B5680CB573A0B5680D3573A0B5680DB573A0B5680E3573A0B5680EA573A0B0606DE0B9D005680F2573F0B5680FB573F0B568000583F0B568005583F0B568010583F0B0606DE0B9D005680090E440B56801658440B56802258440B56803C58440B56804558440B0606DE0B9D005680D93D490B56805E58490B56807058490B56807B58490B0606DE0B9D00568090584E0B568095584E0B56809A584E0B5680A1584E0B5680B0584E0B5680C1584E0B0606DE0B9D005680CC58530B5680D458530B0606DE0B9D005680DD58580B5680E558580B5680F158580B56800959580B01002059A30001002D599D0001003859950A01004159A00001004C59270A0100B053A0000606DE0B9D005680245A5D0B5680223C5D0B0606DE0B9D005680A44D620B56802B5A620B5680305A620B5680693D620B0606DE0B9D005680A44D670B5680395A670B56803D5A670B0606DE0B9D0056807C576C0B5680445A6C0B5680475A6C0B0606DE0B9D005680E60B710B56804B5A710B5680575A710B5680635A710B5680665A710B5680695A710B5680745A710B56807C5A710B0606DE0B9D005680E60B760B5680845A760B5680995A760B0606DE0B9D005680E60B7B0B5680B25A7B0B5680B85A7B0B0606DE0B9D005680A44D800B5680C05A800B5680C25A800B5680C45A800B5680C65A800B0606DE0B9D005680C85A850B5680D95A850B5680EC5A850B5680F65A850B5680005B850B5680175B850B5680275B850B5680375B850B5680545B850B0606DE0B9D005680745B8A0B56807C5B8A0B5680845B8A0B56808C5B8A0B5680945B8A0B56809C5BA3000606DE0B9D005680A462D40F5680A762D40F5680AD62D40F56809232D40F5680BC62D40F5680CD62D40F5680DD62D40F0606DE0B9D0056805357D90F5680ED62D90F5680F562D90F5680F962D90F56800963D90F06000F63A3000600CF099D000600F806A00006001D63560106002863D90F06003563DE0F06004E63560106006263A30006007063560106008163DE0F06008C63DE0F06009C63DE0F0600A763DE0F0600B863950A06003B56A0000600BF63D40F0600CA63E30F0600EF00A50806000364A00006006244A30006006263A30006000F63A30006000F64560106003764950A06004564560106004F64EF0F06005664560106005D64A30006006E64F40F06008064A00006009464560106009C64DE0F0600AD64DE0F0600C864DE0F0600D764DE0F0600E264A3000600EE64950A0600FB64A30006000465950A06000E65950A06001C65950A06002365950A06003265950A06004365950A06005365950A06006365950A06007565950A0600E52DF90F06007066950A06007C66950A06008866950A0606DE0B9D005680A462FE0F56809466FE0F56809F66FE0F5680AA66FE0F5680B466FE0F5680AD62FE0F56809232FE0F5680BC62FE0F5680C466FE0F5680CD62FE0F5680DD62FE0F0600D766DE0F0600BF63FE0F0600CA63E30F06006263A30006006244A3000600EF66950006000067950A06000C67950A06001567950A06002367950A06003167950A06003C67950A06004767DE0F06004F67DE0F06005667DE0F06009464560106009C64DE0F0600AD64DE0F0600EF00A50806000364A00006006263A30006006244A3000600CF099D000600E109A0000600B466560106005A67DE0F0600CA63E30F06006D67A30006004F67DE0F06007767A30006005667DE0F06005664560106005D64A30006006E64F40F06008064A00006009464560106007E67950A06008C67950A06009767950A0600A767950A0600BA67950A0600C36756010600D66756010600E36756010600F767950A06000668950A06004767DE0F0600B863950A06006244A30006000769560106000F699D0006001B699D00060027699D0006003B56A000060034699D0906003B69A30006005269A300060067699D0006007969A5090600926956010600B469950A0600D569A3000600E669DE0F0600F969DE0F0600066A950A0600126AA0000606DE0B9D0056801F6AB61056802E6AB6100606DE0B9D005680486ABB105680536ABB105680686ABB100606DE0B9D0056807F6AC0105680966AC0105680AD6AC0105680C46AC0100606DE0B9D005680D66AC5105680EF6AC51056800B6BC5100606DE0B9D005680336BCA1056805E6BCA105680866BCA100606DE0B9D005680A76BCF105680CE6BCF105680FE6BCF100606DE0B9D005680D93DD41056802C6CD4105680396CD4105680486CD4105680536CD4100100696C95000100F452950001007C6CD9100606DE0B9D005680F56214115680296D14115680306D14110606DE0B9D0056801E6E7C115680306E7C115680426E7C110600CF099D0006004D6E9D000600546EE3090600616EA0000600666EA0000600076956010600736EFA0906007A6E7C1106005269A3000600836EA3000600946E9D0006009D6E950A0600B06E56010600BE6E56010600E06E9D0006006802A3000600E96EA3000600F36E560106000A6F56010600246F56010600326F9D0006004C6F9D000600696FA0000600806FA0000600946FA0000600A86FA0000600C26FA0000600D76FA3000600F06FA0000600FB6FA00006003256A00006005B56A000060003709D0006000A70A000518029709D0031003970911131004E7091110606DE0B9D005680E60BC5115680E670C5110100EA7091110100F8709D0001000371911101001471CA1101001B71CA1101002271560101001C73560101004873331201002271560113012185B81B50200000000096000E080A0001002C210000000096001C08160006002422000000009600230816000E0004240000000096002708280016003C25000000009600350833001A007C280000000091003F0842002100DC2C0000000096004F0854002700742D0000000096004F0861002B00342E00000000960059086E002F00002F00000000960072087500310004300000000086009C08800035008030000000008600A208880037000231000000008618A80891003B000C310000000086003009A6003B0034310000000086004109AB003C007031000000008600550991003E00D833000000008618A808B1003E006834000000008618A808B7003F000135000000008618A808BF0042001435000000008618A808C600450098350000000086006009CE004900C8350000000086006009DE00500000360000000086006009EF0058002C360000000086006009000160006436000000008600600913016A0098360000000086006009270175007C3800000000960064093C0181002C390000000086087609430183008E390000000086008E0991008300A039000000008600940947018300103A000000008600A5094C01840078430000000086009C0880008500BC43000000008618A80891008700C443000000008600480A61018700E143000000008618A80891008700EC43000000008608690A610187000444000000008608800A6A01870010440000000086005509910088002B47000000008618A808B10088006047000000008618A808700189009047000000008600600977018B00B447000000008600600982019000DC4700000000860060098E019600044800000000860060099B019D0030480000000086006009A801A4005C480000000086006009B601AC0088480000000086006009C601B500B4480000000086006009D501BD00E4480000000086006009E601C70018490000000086006009F801D2005C4900000000860060090B02DD00A04900000000860060091F02E900304D0000000086009F0A4602F800544D0000000086009F0A5002FC00904E000000008600AB0A5C0201013C4F000000008600BF0A460206014850000000008100D60A67020A012455000000008600EB0AA60010015855000000008600010B750211018C55000000008600170B75021201C0550000000086002D0B75021301F455000000008600480B7B0214012856000000008600590B810215017456000000008600820B89021701A856000000008600A5094C01170190800000000096008C0B8D0218018890000000009600AB0B9E021E01DC94000000008608344F9E0A2201F494000000008608414F7502220100950000000086084E4F430123011895000000008608654FA600230124950000000086087C4FA30A24013C950000000086088F4FA70A24014895000000008608A24FA30A25016095000000008608B64FA70A25016C95000000008608CA4F9E0A26018495000000008608D94F750226019095000000008608E84F89022701A895000000008608F54FAC0A2701B4950000000086080250B10A2801CC950000000086081950B50A2801D89500000000860830509E0A2901F095000000008608405075022901FC9500000000860850509E0A2A0114960000000086085E5075022A0120960000000086086C509E0A2B0144960000000086087A509E0A2B015C960000000086088E5075022B016896000000008608A2509E0A2C018096000000008608B65075022C018C96000000008608CA509E0A2D01A496000000008608D85075022D01B096000000008608E6509E0A2E01C896000000008608F45075022E01D49600000000860802519E0A2F01EC96000000008608155175022F01F8960000000086082851B10A300110970000000086084151B50A30011C970000000086085A51BA0A310134970000000086086551C00A310140970000000086087051C70A320158970000000086087C51CD0A320164970000000086088851D40A33017C97000000008608A051DA0A33018897000000008608B8519E0A3401A097000000008608D05175023401AA97000000008618A80891003501CC97000000008608F45343013501E4970000000086080454A6003501F0970000000086081454C70A360108980000000086081D54CD0A360114980000000086082654A30A37012C980000000086083954A70A370138980000000086084C54A30A380150980000000086085954A70A38015C980000000086086654A30A390174980000000086087554A70A39018098000000008608845489023A0198980000000086089454AC0A3A01A498000000008608A454A30A3B01BC98000000008608B254A70A3B01C898000000008608C0549E0A3C01E098000000008608CD5475023C01EC98000000008608DA54A30A3D010499000000008608EB54A70A3D011099000000008608FC54A30A3E0128990000000086080F55A70A3E0134990000000086082255A30A3F014C990000000086083455A70A3F0158990000000086084655A30A400170990000000086085855A70A40017C990000000086086A55B10A410194990000000086087955B50A4101A0990000000086088855A30A4201B8990000000086089555A70A4201C499000000008608A255040B4301DC99000000008608B0557B024301E899000000008608BE55090B4401009A000000008608D2550F0B44010C9A000000008608E655A30A4501249A000000008608FC55A70A45012E9A000000008618A808910046013C9A0000000086085C5989024601549A0000000086086959AC0A4601609A00000000860876599E0A4701789A000000008608815975024701849A0000000086088C59A30A48019C9A0000000086089859A70A4801A89A000000008608A459BA0A4901C09A000000008608B559C00A4901CC9A000000008608C65943014A01E49A000000008608D459A6004A01F09A000000008608E259A30A4B01089B000000008608EE59A70A4B01129B000000008618A80891004C011C9B000000009600BC5B980B4C0128A1000000009600D85BA80B5101E8A1000000009600F05BB30B53010CA2000000009600F05BC30B5801A0A30000000091000B5CDA0B6001F8AC000000009600225CF40B6B015CAE000000009600225C010C700118AF000000009600325C100C750184B00000000096004A5C1A0C7A0114B2000000009600645C230C7E018CB6000000009600795C3A0C8701D8B90000000096008F5C490C8E0188BA000000009600A55C510C9101BCBE000000009600C25C650C990170C1000000009600E15C750C9E0170CA000000009100F65C860CA40114CC000000009100175D930CA80138CD000000009100335D9C0CAB01E0D3000000009600485DA80CAE01A8D50000000096006A5DB40CB3014CD8000000009100835DC40CB80138D90000000096009F5DD00CBB01C0DC000000009600B85DE50CC20184DE000000009600D55DF80CCA0134DF000000009600F35DFF0CCC0164E20000000096000D5E110DD20108E60000000096002B5EF80CD601D0E8000000009600505EF80CD801D8EC000000009600695E1A0DDA0108EE000000009600845E250DDE01FCEE000000009600A35E2E0DE10120EF000000009100BA5E3F0DE801D8F0000000009600A35E480DEB01C8F8000000009600DC5E5D0DF40124F9000000009100F35E680DF701D0F9000000009600DC5E720DF901ECF9000000009600DC5E7D0DFC012806010000009600245F890D000248080100000096003A5F940D03026C09010000009600515F9D0D04029409010000009600515FA90D0802BC09010000009600695FB60D0D02E009010000009600695FC90D13023C0A010000009600695FDF0D1A02640A010000009600695FED0D1E028C0A010000009600695FFD0D2302B40A010000009600695F100E2902DC0A010000009600695F240E2F02540B010000009600695F3D0E3702CC0B010000009600695F570E4002F00B010000009600695F690E4702D017010000009600825FF80C4F028018010000009600925F7E0E51029819010000009600A85F870E5402281F010000009600C35F930E5902F024010000009600DE5FAD0E61021025010000009600DE5FB90E6602AC26010000009600F75FC60E6C02282B0100000091001760DB0E7402E82B0100000096003960E30E7602502D0100000096004C60EE0E7B028C2F0100000091005560FA0E820280320100000096007C60490C870230330100000096009E60070F8A029837010000009600D260160F9002B437010000009600D260240F9402CC37010000009600D260330F9902703A010000009100F160470FA002343C01000000910007614F0FA202A03C0100000091001561560FA302243E0100000091003361560FA502703F0100000091005261560FA702BC400100000091007161560FA902884201000000910084614F0FAB02644301000000910093615F0FAC024845010000009600AF61710FB202DC46010000009100CE617F0FB802C447010000009600ED61880FBB021C490100000096000C629A0FC402704A0100000096003062A40FC702684E0100000091004962F80CCB02544F0100000091006962AF0FCD0268500100000091007A62B70FD002BC560100000096009262CC0FD702B857010000008618A8089100DA02DC57010000008600D363E70FDA020C58010000008600DC63A70ADC023D58010000008600E4639100DD024858010000008608EF63A30ADD026858010000008618A8089100DD02D858010000008618A8089100DD029459010000008618A8089100DD02F45901000000860886659E0ADD021C5A0100000086089B659E0ADD02345A010000008608AD659E0ADD024C5A010000008608BF659E0ADD027C5A010000008608CF659E0ADD02945A010000008608E5659E0ADD02AC5A010000008608F8659E0ADD02CF5A010000008618A8089100DD02E35A010000008618A8089100DD02EC5A010000008600D3630310DD021C5B010000008600DC63A70ADF024D5B010000008600E4639100E002585B010000008608EF63A30AE002755B010000008618A8089100E0029C5B010000008618A8089100E002505C010000008618A8089100E002145D01000000C6001568A30AE002735D010000008618A8089100E002885D010000008618A8080B10E002185E010000008618A8081210E1027C5E010000008618A8081C10E402A45E010000008618A8082910E902F85E0100000086081E689E0AF0021C5F01000000860832689E0AF002345F01000000860849689E0AF002645F0100000096085A683A10F0027C5F01000000860863684010F002986001000000960872684610F002E4600100000096087E684610F20230610100000096088D685210F402806101000000960899685210F6029C6101000000C600A7685C10F802BC6101000000C600AE688902F902D461010000009600BA686110F902F061010000009600BA686A10FA026C6201000000E601C1687410FC02DC6201000000C6001568A30AFC020E63010000008618A8089100FC023063010000008618A8088410FC025863010000008618A8088B10FD0278630100000096085A689410FF02906301000000960872689A10FF02CC630100000096087E689A10010308640100000096088D68A610030344640100000096089968A6100503606401000000C600A7685C100703806401000000C600AE68890208039864010000008618A808910008034165010000008618A808910008036865010000008608816CDD10080396650100000086088E6CE3100803C4650100000086009B6CEA1009031067010000008600A76CA70A0D038067010000008600B36CA70A0E03B467010000008600C26CF2100F031C68010000008600CB6CA30A10036868010000008600D16CA70A1003B468010000008100D96C91001103B869010000008100DE6CF7101103446A010000008100F86CFD101203986A010000008100116D05111403DC6A0100000096003A6D19111703D86B0100000096004C6D20111903B86D010000009600606D2D111E03C86F0100000096004C6D37112303EC6F0100000096004C6D421126030C700100000096007A6D421129032C700100000091004C6D4D112C03DC750100000096009A6D5A113103A877010000009600B16D64113503C077010000009600CB6D6B113703A47A010000009600E66D19113803587C0100000096000A6E72113A03E47C010000008600CF6E81113D03047D010000008618A80891003E038C7D010000009600137087113E03C87D0100000096001E7087113F030D7E010000008618A80891004003187E010000009600627095114003E07F01000000960076709E11430350810100000096008A70A61146038781010000009600A070AC114803A881010000009600B770B5114B03F481010000009600D570BB114D036D82010000008618A8089100500338820100000091189384B41B50037582010000008618A808CD115003B48201000000E6092A71B10A5103C88201000000E6094071B10A5103DC8201000000E6095F7189025103F08201000000E609727189025103048301000000E6018671D3115103708401000000E6019571DE115603BC84010000008100D96C910059035C8501000000E601A97191005903BC85010000008418A80891005903D08501000000C608FD7189025903E48501000000C6080B72AC0A5903068601000000C608197289025A030E8601000000C6082A72AC0A5A03188601000000C6083B72E7115B03308601000000C6084272CD115B03608601000000C6085272EC115C03888601000000C6086672EC115C03B48601000000C6088372F2115C03C88601000000C6088C72F7115C03EC8601000000C608A172FD115D03008701000000C608AD7202125D03228701000000C600B97291005E03288701000000C600C47291005E035C87010000009600D07208125E037887010000009600D0720E125E03DF87010000008618A80891005F03EC8701000000C60028732A125F03888801000000C60038732A126103A28801000000C400A971B50A6303B488010000008618A80891006403CC8801000000C608FD7189026403EC8801000000C6080B72AC0A64031C8901000000C608197289026503398901000000C6082A72AC0A65034C8901000000C6083B72E7116603698901000000C6084272CD1166037C8901000000C6085773E7116703998901000000C6085F73CD116703AC8901000000C608677389026803C98901000000C6087373AC0A6803DC8901000000C6085272EC116903FC8901000000C6086672EC1169031C8A01000000C6088372F2116903398A01000000C6088C72F71169034C8A01000000C608A172FD116A03698A01000000C608AD7202126A03798A01000000C600B97291006B03888A01000000C600C47291006B03988A01000000C60028732A126B03408B01000000C60038732A126D035C8B010000008100A97191006F03A88B0100000096008B7338126F034C8D010000009600A4734B127803C08E010000009600BE734B1286033490010000009600D573641294038091010000009600EB7374129B0300000100FD7300000200067400000300137400000400640900000500217400000100FD73000002002574000003003774000004004A7400000500626300000600617400000700F96900000800217400000100FD73000002002574000003003774000004004A7400000500626300000600617400000700F96900000800217400000100640900000200FD7300000300377400000400217400000100640900000200FD7300000300067400000400137400000500737400000600035A00000700217400000100CF09000002007B7400000300626300000400617400000500F969000006002174000001002C6C02000200BF6302000300AF7402000400BA7400000100486C02000200BF6302000300AF7402000400BA7400000100FD7300000200217400000100626302000200624402000300CF09000004002174000001006F0000000200C47400000100CF0900000200E10900000300626300000400C47400000100626300000100624400000200F806000001006F00000001006F0000000200CF0900000300E10900000100CF0900000200E10900000300626300000100CF0900000200E109000003006263000004000364000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F969000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F969000008006263000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F96900000800EE74000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F96900000800EE7400000900CF0900000A00E109000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F96900000800EE7400000900CF0900000A00E10900000B00F674000001001256000002006244000003001E5600000400CB7400000500DA7400000600E47400000700F96900000800EE7400000900CF0900000A00E10900000B00F67400000C00626300000100624400000200217400000100FE74000001002174000001006F0000000200C474000001000475000001006F00000001006F0000000200E10900000100125600000200EF0000000300035A000004006244000005000A7500000100125600000200EF0000000300035A000004006244000005000A7500000600187500000100125600000200EF0000000300035A000004006244000005000A75000006002375000007002D7500000100125600000200EF0000000300035A000004006244000005000A75000006001875000007000A5A00000100125600000200EF0000000300035A000004006244000005000A75000006002375000007000A5A000008002D7500000100125600000200EF0000000300035A000004006244000005000A75000006002375000007000A5A000008003475000009002D7500000100125600000200EF0000000300035A000004006244000005000A75000006001875000007000A5A00000800347500000100125600000200EF0000000300035A000004006244000005000A75000006001875000007000A5A00000800347500000900497500000A00547500000100125600000200EF0000000300035A000004006244000005000A75000006002375000007000A5A00000800347500000900497500000A00547500000B002D7500000100125600000200EF0000000300035A000004006244000005000A75000006001875000007000A5A00000800347500000900497500000A00547500000B00697500000100125600000200EF0000000300035A000004006244000005000A75000006002375000007000A5A00000800347500000900497500000A00547500000B00697500000C002D7500000100125600000200EF0000000300035A000004006244000005000A75000006001875000007000A5A00000800347500000900497500000A00547500000B00697500000C00737500000D00090400000E00847510100F002D75000001001256000002006244000003000A75000004008B75000001001256000002006244000003000A75000004008B75000005003475000001001256000002006244000003000A75000004008B75000005009A75000001001256000002006244000003000A75000004008B75000001001256000002006244000003000A75000004008B7500000500AB7500000600347500000100B47500000100DA7400000100E47400000100E474000001002C0A00000100BE7500000200CC7500000100217400000100EF0000000200035A02000300E47402000400DA74000005000A75101006002D7500000100EF0000000200E47400000300DA7410100400D87500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100E17500000200624402000300E87502000400F875000005002174000001000276000002002174000001000F6300000200D206000003007B74000004000876000005002174000001000F6300000200D206000003007B74000004000876000005002863000006001176000007004E63000008002174000001007B74000002000F6300000300CF0900000400546E00000500E109000006000876000007002863000008001176000009004E6302000A00237600000B00217400000100035A000002002A7600000300327602000400387600000500C47400000100035A000002002A7600000300327602000400387600000500C47400000100624400000200CF0900000300626300000400427600000500217400000100624400000200CF0900000300626300000400217400000100624400000200CF0900000300546E00000400087600000500286300000600117602000700626302000800E175000009002174000001006244000002000C0100000300557600000400CF0900000500546E020006006263000007002174000001006263000002006244000003002174000001001E5600000200624400000300CF0900000400546E000005005E7600000600787602000700626300000800217400000100546E00000200626300000300617402000400867600000500217400000100CF0900000200546E00000300E109000004000276020005002376000006002174000001006263000002008C76000003009276000004002174000001006244020002009C7600000300217400000100B17600000200237600000300217400000100624400000200626300000300CF0900000400B87600000500217400000100C57600000200B17600000300237600000400546E00000500217400000100EB06000002004F6700000300217400000100626300000200E52D000003004F6700000400566700000500EC2D00000600236500000700217400000100626300000200DB7600000300EB7600000400FB76000005000677000006001277000007002377000008002174000001006244000002002174000001006244000002006263020003003077020004004077020005004D7700000600C474000001006244000002006263000003000F6300000400C47400000100624400000200C47400000100624400000200C474000001006263000002005E77000003006A7700000400C47400000100035A00000200F15100000300C47400000100624400000200CF09000003006263000004000F63000005007377020006007B7700000700C474020001000F7300000200624400000300217400000100624400000200CF09000003006263000004000F63000005007377000006008477020007007B7702000800937700000900C474000001006263020002004D77000003002174020001007A6E00000200217400000100C10500000200CA6300000300217400000100C10500000200CA6300000300A07700000400217400000100626302000200C10500000300217400000100027600000100624400000200AD7702000300B97700000400217400000100624400000200AD7700000300D17702000400B97700000500217400000100624400000200E57700000300AD7702000400EB7702000500066A00000600217400000100624400000200E57700000300AD7700000400F17702000500EB7702000600066A00000700217400000100624400000200E57702000300EB7700000400217400000100624400000200E57702000300EB7702000400626300000500217400000100624400000200E57702000300EB7702000400066A02000500626300000600217400000100624400000200E57702000300EB7702000400626302000500FF7700000600217400000100624400000200E57700000300AD7702000400EB7702000500066A02000600626302000700FF7700000800217400000100624400000200E57700000300AD7700000400D17702000500EB7702000600066A02000700626302000800FF7700000900217400000100624400000200640900000300E57700000400AD7700000500D177020006000C0600000700217400000100624400000200640900000300E57700000400AD7700000500D17700000600F177020007000C06000008002174000001006244000002002174000001006244000002000F6900000300C47400000100CF09000002000F6302000300693D020004000B7800000500217400000100E175000002006244000003000F63000004008C6302000500A763020006001F78020007003278000008002174000001004178000002006263000003008C63000004000876000005002174000001004178000002006263000003008C6300000400087600000500517800000600217400000100624400000200EB06000003006878000004007978000005003077020006008B78020007009578000008002174020001000F73000002002174000001006244000002003B5600000300E10900000400546E00000500217400000100AA7800000200B57800000300BE7800000400C97800000500D37800000600DF7800000700C47400000100624400000200117600000300CF0900000400E87800000500C47400000100CF0900000200F87800000300C47400000100CF09000002000F6300000300007902000400693D020005000B78000006002174000001006244000002001179000003001F7902000400736E000001006244000002001179000003001F7900000400D66702000500736E000001006244000002001179000003001F7900000400D66702000500736E020006002B7900000700217400000100B17600000200427900000100B17600000100527900000200B17600000100527900000200B17600000100527900000200B17600000100527900000200B17600000100B176000001005D79000002006263020003006B79020004007579020005007F79000006002174000001006263000002008C7900000300A17900000400B46600000500EF0000000600217400000100626300000200CA6300000300217400000100626300000200F96900000300624400000400CF0900000500546E00000600E10900000700B67902000800CA63000009002174000001006263000002004F6400000300217400000100624400000200E17500000300035A00000400C47400000100626300000200217400000100CF0900000200626300000300217400000100D20600000200C879000003007F7900000400B176000005002376020006009B2F00000700217400000100D77900000200DD7900000300217400000100A76200000200FB6300000100FB6300000100A76200000200FB6300000100FB6300000100E579000001008C6700000200976700000300A767000001008C6700000200976700000300A76700000400BA6700000500D667000001008C6700000200976700000300A76700000400BA6700000500D66700000600F76700000700066800000100EB7900000200F07900000100EB7900000200F07900000100EB7900000200F07900000100EB7900000200F07900000100F57900000100EB7900000100EB7900000200F97900000100FF0500000100476700000200B86300000100EB7900000200F07900000100EB7900000200F07900000100EB7900000200F07900000100EB7900000200F07900000100F57900000100047500000100F56200000200296D00000300087A00000400A44D00000100117A00000100117A00000100117A000001001A7A00000100117A00000100746C000002001E7A00000100746C000002001A7A00000300287A00000100CF0900000200217400000100FD7300000200067400000300137402000400D20600000500217400000100317A00000200FD7300000300067400000400137400000500217400000100CF0902000200387A00000300217400000100E10902000200387A00000300217400000100487A02000200387A00000300217400000100CF0900000200E10900000300487A02000400387A00000500217400000100CF09000002005B7A02000300647A00000400217400000100707A000002007D7A00000100217400000100CF0900000200217400000100CF0900000200217402000300546E000001007D7A000001000A75000001000A7500000100640902000200827A02000300947A02000100640900000200827A00000300947A000001009D7A00000200A47A00000100827A020002009D7A02000300A47A000001009D7A000002006409000001000F7300000200AD7A02000300BF7A000001007F7300000100CB7A00000200D77A00000300E37A00000400EE7A00000500FB7A00000100CB7A00000200D77A00000300E37A00000100047500000100047500000100047500000100047500000100047500000100087B00000100107B00000200177B00000100107B00000200177B000001001D7B00000100047500000100047500000100047500000100047500000100047500000100047500000100047500000100107B00000200177B00000100107B00000200177B00000100FD73000002000674000003001374000004006409020005006263020006008C6302000700BF6302000800AF7402000900277B00000100FD73000002002574000003003774000004004A7400000500626300000600EE6400000700046500000800E26400000900FB6400000A00F96900000B001C6502000C00BF6302000D00AF7402000E00277B00000100FD73000002002574000003003774000004004A7400000500626300000600EE6400000700046500000800E26400000900FB6400000A00F96900000B001C6502000C00BF6302000D00AF7402000E00277B00000100640900000200FD7300000300377402000400476702000500BF6302000600AF7402000700277B00000100640900000200FD7300000300067400000400137400000500737400000600035A02000700BF6302000800AF7402000900277B51000D006300110063001500B100A8089100B900A808A70AC100A808A70AC900A808A70AD100A808A70AD900A808A70AE100A808A70AE900A808A70AF100A808A70AF900A808B50A0101A808A70A0901A808A70A1101A808A70A1901A80887122901A808AC0A3101A80891004100A808AC0A3900A509A70A3900167DA70A41012C7D8E127100A80891007100337DD61209001568A30A39004C7DDC129100A808E2129100727DEC1251016009F2127101BD7DFC127901D17D89022900A971910039011568A30A4101E17D18134100E87D201341001568A30A4101E17D28138101F97D2F1341007E682F13410072682F134100996820134100FD7D38134100067E2F138101127E40134100167E20138101257E2F135101600947137901307E74109101E57979139100537E8813A1016E7EB10AA101737E4701A1017C7E8E130900A80891004101857EAA134900A80891004900A87EAF13B101B47EB513A9016009BC13B901C77EB50AB901D97EA600B901EF7EA600B901057FB50AA901157FC61349001E7FCD1341012C7DDF1349002D7FE6135900347FEB1349004F7FF113C1016009F713D1017D7F8902C101157F0E145900157F141449008E0991006901FD7D1E14D101A67F2514D901B47F7410D901C07FB10A7101C97FA70A7101ED7F2B1479010B803214F101A8089100F1012F803914F9011C083F1409004F807410B9015F80FC120C00A80876141400A808761441019968931441007080201314008680B10A1C008680B10A0C008680B10A41018D68931459009380D0145900A180F2104100A88040137101B9807410A101C380DB147100CE80AC0AF901D980AC0A0902A80891001102A808910041012C7D5815F101A8083914F9011F813F145900157F5E154101A768F210A10124819915A1012E819E1541013781A00041003D81201369011568A30A41012C7DA315410050812F1381015C81401341012C7DA91519021568A30AC901EF63A30AA1016D810A16A10178819E158101F97D71166900838104177100337D14175900347F5C174900A08199178101B681A0174101E17DAB174900CD81B117F101D4813914F101E681BC1739020482B50AF9011C08C1174101E17DC81759001E82B10A59002C82A30A79013982A30A4102A67F251471015F82A30A41017182A30A41017682501869011568BC1871007E82C11849028E82C818A1019882B10A41008D6820138101B681CE187901AF82EA185901A808F11851016009F9185101157F0219A101C68280195100D28286196102FD7DCD194100E182D4197100ED82DA194100FA82E11989011568BC1841001568BC187100ED82E71971000683D61271000D83EF190900AE68890241012C7D3E1AC901A808A70A71001483890241011F83A30A59002783910049002E8391007900A808A70A4900A808A70A0900B47E681AA901157F6E1A79005083751A690260096A014101A768871A7902A808910079008E839C1A8902A808A70A7900AF83AE1AA101B783D51A41017E82EC1A9902A808910041011483890231007373AC0A310028732A12A9021568A30A4101FC83231BA9020484291BB10217842E1BB902A808CD11C102A808341BC9026E7E401BB1025284481B41015B844F1B69010484551B310038732A12B902A8089100C90265847C1BC1026B849100B9027B84E71149028384551B41015B84991549028B84A01BD902A8089100E9025D85BD1BF102C1687410B1016D85A30A0103A808A70A0903A808CF1B1103A808A70AF1028E09EC1B1903C785F51B3100A80891003100D8859D002103A808A70A2903A80891009900A808FA1B3103A808910031006773890239031784CD1131005F73CD114101A768221C3100A971B50A3100FD71890231000B72AC0A31001972890231002A72AC0A31003B72E71131004272CD1131005773E71131005272EC1131006672EC1131008372F21131008C72F7113100A172FD113100AD7202123100B97291003100C47291000103A808CF1B31008E0991004103A80891004901A808A70A49036486910049016986431C51038886910008006000B80208006400BD0208006800C20208006C00C70208007000CC0208007400D10208007800D60208007C00DB0208008000E00208008400E50208008800EA0208008C00EF0208009000F40208009400F90208009800FE0208009C0003030800A00008030800A4000D030800A80012030800B000B8020800B400BD020800B800C2020800BC001B030800C000C7020800C40020030800C80025030800CC002A030800D000CC020800D800B8020800DC00BD020800E000C2020800E4001B030800E800C7020800EC0020030800F00025030800F4002A030800F800CC020800FC003303080000013803080004013D0308000C01420308001001B80208001401BD0208001801C20208001C011B0308002001C70208002401200308002801250308002C012A0308003001380308003801420308003C01B80208004001BD0208004401C202080048011B0308004C01C702080050012003080054012503080058012A0308005C01CC02080060013303080064013803080068013D0308006C014B03080070015003080074015503080078015A0308007C015F03080080016403080084016903080088016E0308008C017303080090017803080094017D0308009801820308009C0187030800A001D6020800A4018C030800A80191030800AC0196030800B0019B030800B401A0030800B801A5030800BC01AA030800C001AF030800C401B4030800C801B9030800CC01BE030800D001C3030800D401C8030800D801CD030800DC01D2030800E001D7030800E401DC030800E801E1030800EC01E6030800F001EB030800F401F0030800F801F5030800FC01FA0308000002FF0308000402040408000802090408000C020E04080010021304080014021804080018021D0408001C02220408002002DB02080024022704080028022C0408002C023104080030023604080034023B0408003802400408003C024504080040024A04080044024F0408004802540408004C025904080050025E0408005402630408005802680408005C026D04080060027204080064027704080068027C0408006C028104080070028604080074028B0408007802900408007C029504080080029A04080084029F0408008802A40408008C02A90408009002AE0408009402B30408009802B80408009C02BD040800A002C2040800A402C7040800A802CC040800AC02D1040800B002D6040800B402DB040800B802E0040800BC02E5040800C002EA040800C402EF040800C802F4040800CC02F9040800D002FE040800D40203050800D80208050800DC020D050800E00212050800E40217050800E8021C050800EC0221050800F00226050800F4022B050800F80230050800FC023505080000033A05080004033F0508000803440508000C034905080010034E0508001403E00208001803530508001C035805080020035D0508002403620508002803670508002C036C05080030037105080034037605080038037B0508003C038005080040038505080044038A05080048038F0508004C039405080050039905080054039E0508005803A30508005C03A80508006003AD0508006403B20508006803B70508006C03BC0508007003C10508007403C60508007803CB0508007C03D00508008003D50508008403DA0508008803DF0508008C03E40508009003E90508009403EE0508009803F30508009C03F8050800A003FD050800A40302060800A80307060800AC030C060800B00311060800B40316060800B8031B060800BC0320060800C00325060800C4032A060800C8032F060800CC0334060800D00339060800D4033E060800D80343060800DC0348060200DD0385120800E0034D060800E403EA020800E80352060800EC0357060800F0035C060800F40361060800F80366060800FC036B06080000047006080004047506080008047A0608000C047F06080010048406080014048906080018048E0608001C049306080020049806080024049D0608002804A20608002C04A70608003004AC0608003404B10608003804B60608003C04BB0608004004C00608004404C50608004804CA0608004C04CF0608005004D40608005404D90608005804DE0608005C04E30608006004E80608006404ED0608006804F20608006C04F70608007004FC0608007404010702007504851208007804060708007C040B07080080041007080084041507020085048512080088041A0708008C041F07080090042407080094042907080098042E0708009C0433070800A00438070800A4043D070800A80442070800AC0447070800B0044C070800B40451070800B80456070800BC045B070800C00460070800C40465070800C8046A070800CC046F070800D00474070800D40479070800D8047E070800DC0483070800E00488070800E4048D070800E80492070800EC0497070800F0049C070800F404A1070800F804A6070800FC04AB0708000005B00708000405B50708000805BA0708000C05BF0708001005C40708001405C90708001805CE0708001C05D30708002005D80708002405DD0708002805E20708002C05E70708003005EC0708003405F10708003805F60708003C05FB07080040050008080044050508080048050A0808004C050F08080050051408080054051908080058051E0808005C052308080060052808080064052D0808006805320808006C053708080070053C0808007405410808007805460808007C054B08080080055008080084055508080088055A0808008C055F08080090056408080094056908080098056E0808009C0573080800A00578080800A4057D080800A80582080800AC0587080800B0058C080800B40591080800B80596080800BC059B080800C005A0080800C805B8020800CC05BD020800D005C2020800D4051B030800D805C7020800DC0520030800E00525030800E4052A030800E805CC020800EC0533030800F00538030800F4053D030800F8054B030800FC055003080000065A03080004065F0308000806640308000C066903080010066E0308001406730308001806A90808001C06AE08080020067803080024067D0308002806820308002C06870308003006D602080034068C0308003806910308003C069603080040069B0308004406A00308004806A50308004C06AA0308005006AF0308005406B40308005806B90308005C06BE0308006006C30308006406C80308006806CD0308006C06D20308007006D70308007406DC0308007806E10308007C06E60308008006EB0308008406F00308008806F50308008C06FA0308009006FF0308009406040408009806090408009C060E040800A00613040800A40618040800A8061D040800AC0622040800B006DB020800B40627040800B8062C040800BC0631040800C00636040800C40640040800C80645040800CC064A040800D0064F040800D40659040800D8065E040800DC0663040800E00668040800E4066D040800E806B3080800EC06B8080800F00677040800F4067C040800F80681040800FC06CC0408000007E00408000407E50408000807EA0408000C07EF0408001007F40408001407F90408001807FE0408001C070305080020070805080024070D0508002807120508002C071705080030071C0508003407210508003807260508003C072B05080040073005080044073505080048073A0508004C073F0508005007440508005407490508005807D00508005C07D50508006007DA0508006407DF0508006807E40508006C07BD0808007007E90508007407EE0508007807F30508007C07C20808008007C70808008407CC0808008807D10808008C07D60808009007DB0808009407E00808009807E50808009C07EA080800A007EF080800A407F4080800A807F9080800AC07FE080800B00703090800B40708090800B8070D090800BC0712090800C00717090800C4071C090800C80721090800CC0726090800D0072B090800D40730090800D80735090800DC073A090800E0073F090800E40744090800E80749090800EC074E090800F00753090800F40758090800F8075D090800FC076209080000086709080004086C0908000808710908000C087609080010087B0908001408800908001808850908001C088A09080020088F0908002408640808002808690808002C086E08080030087308080034087808080038087D0808003C088208080040088708080044088C0808004808910808005008B80208005408BD0208005808C20208005C081B0308006008C70408006408CC0408006808D10408006C08F904080070082B0508007408D00508007808FD0508007C080206080080089809080084083E0608008C08B80208009008BD0208009408C202080098081B0308009C08C7020800A00820030800A808B8020800AC08BD020800B008C2020800B808B8020800BC08BD020800C008C2020800C4081B030800C808C7020800CC0820030800D00825030800D40838030800D8083D030800DC085A030800E0085F030800E40882030800E80887030800EC08D6020800F0088C030800F40891030800F808960308000009B80208000409BD0208000809C20208001009B80208001409BD0208001809C20208001C091B0308002409B80208002809BD0208002C09C202080030091B0308003809B80208003C09BD0208004409B80208004809BD0208005009B80208005409E50208005809EA0208005C09EF0208006009F40208006409F90208006809FE0208006C090303080070090803080074090D0308007809120308007C09C10908008009C60908008409CB0908008809D00908008C09D50908009009DA0908009809B80208009C09BD020800A009C2020800A4091B030800AC0942030800B009BD020800B4091B030800B80920030800BC09C7040800C009CC040800C409D1040800C809D6040800CC09DB040800D009E0040800D409E5040800D809EA040800DC09EF040800E009F4040800E409F9040800E809FE040800EC0903050800F409B8020800F809BD020800FC09C2020800000A38030800040A5A030800080A820308000C0AEB090800100A8F090800140AF0090800180AF5090800200AB8020800240ABD020800280AC2020800300AB8020800340ABD020800380AC20208003C0A1B030800400AC7020800480AB80208004C0ABD020800500AC2020800580AB80208005C0ABD020800600AC2020800680AB80208006C0ABD020800700A3D030800740A4B030800780A500308007C0A5F030800800A64030800840A87030800880AD60208008C0A8C030800900AAF030800940AB4030800980AB90308009C0ABE030800A00AE1030800A40AE6030800A80AEB030800AC0AF0030800B00AF5030800B40AFA030800B80A09040800BC0A0E040800C00A13040800C40A18040800C80A1D040800CC0A22040800D00ADB020800D40A27040800D80A2C040800DC0A31040800E00A36040800E40A3B040800E80A40040800EC0A45040800F00A4A040800F40A4F040800F80A54040800FC0A59040800040BB8020800080BBD0208000C0BC2020800100B1B030800140BC7020800180B380308001C0B3D030800200B4B030800240B50030800280B550308002C0B130A0800300BB4030800340BB9030800380BBE0308003C0BC3030800400BC8030800440BCD030800480BD20308004C0BD7030800500BDC030800540BC7040800580BCC0408005C0BD6040800600BD1040800640BDB040800680B960808006C0B9B080800740BB8020800780BBD0208007C0BC2020800800B1B030800880BB80208008C0BBD020800900BC2020800940B1B0308009C0BB8020800A00BBD020800A40BC2020800A80B1B030800AC0BC7020800B00B20030800B80BB8020800BC0BBD020800C00BC2020800C40B1B030800C80BF0090800CC0B2C0A0800D00B310A0800D40BC7020800D80B20030800DC0B25030800E40BB8020800E80BBD020800EC0BC2020800F00B1B030800F80BB8020800FC0BBD020800000CC2020800040C1B030800080CC70208000C0C20030800100C25030800140C2A030800180CCC0208001C0C33030800200C38030800240C3D030800280C4B0308002C0C50030800300C55030800340C130A0800380CD10208003C0C400A0800400C450A0800440C4A0A0800480C5A0308004C0CC7040800500CCC040800540CD1040800580CD60408005C0CDB040800600CE0040800640CE5040800680CEA0408006C0CEF040800700CF4040800740CF9040800780CFE0408007C0C03050800800C08050800840C0D050800880C120508008C0C1C050800900C21050800940C26050800980C2B0508009C0C30050800A00C35050800A40C3A050800A80C3F050800AC0C44050800B00C49050800B40C4E050800B80CE0020800BC0C4F0A0800C00C540A0800C40C590A0800C80C5E0A0800CC0C630A0800D00C680A0800D40C6D0A0800D80C720A0800DC0C770A0800E00C7C0A0800E40C53050800E80C58050800EC0C5D050800F00C62050800F40C67050800F80C6C050800FC0C71050800000D76050800040D7B050800080D800508000C0D85050800100D8A050800140D8F050800180D940508001C0D99050800200D9E050800240DA3050800280DA80508002C0DAD050800300DB2050800340DB7050800380D960808003C0D9B080800440DB8020800480DBD0208004C0DC2020800500D1B030800540DC7020800580D200308005C0D25030800600D2A030800640DCC020800680D330308006C0D38030800700D3D030800740D4B030800780D500308007C0D55030800800D130A0800840DD1020800880D400A08008C0D450A0800900D4A0A0800940D5A030800980D5F0308009C0D64030800A00D69030800A40D6E030800A80D73030800AC0DA9080800B00DAE080800B40D78030800B80D7D030800BC0D82030800C00D87030800C40DD6020800C80D8C030800CC0D91030800D00D96030800D40D9B030800D80DA0030800DC0DA5030800E00DAA030800E40D96080800E80D9B080800F00DB8020800F40DBD020800F80DC2020800000EB8020800040EBD0208000C0EB8020800100EBD020800A80EB8020800AC0EBD020800B00EC2020800B40E1B030800B80EC7020800BC0E20030800C00E25030800C40E2A030800CC0EBD020800D00EC2020800D40E1B030800D80EC7020800DC0E20030800E00E25030800E40E2A030800E80ECC020800EC0E33030800F00E38030800F80EB8020800FC0EBD020800000FC2020800080FB80208000C0FBD020800100FC2020800140F1B0308001C0FB8020800200FBD020800240FC2020800280F250308002C0F2A030800340FB8020800380FC70208003C0F20030800400F33030800440F38030800480FD10208004C0F450A0800500F73030800580FB80208005C0FBD020800600FC2020800640F1B030800680FC7020800700FB8020800740FBD020800780FC20208007C0F1B030800800FC7020800880F420308008C0FB8020800900FBD020800940FC20208009C0FB8020800A00FBD020800A40FC2040800A80FF0090800AC0F2C0A0800B00F310A0800B80FB8020800BC0FBD020800C40FB8020800C80FBD020800CC0FC2020800D00FC7020800F00FBD020800F40FC2020800FC0FB80208000010BD0208000410C202080008101B0308001010B80208001410BD0208001810C20208002010B80208002410BD0208002810C20208003010B80208003410BD0208003810C20208003C10C70208004010CC0208004410D10208004810D60208004C10DB0208005410B80208005810BD0208005C10C20208006410420308006810B80208006C10BD0208007410B80208007810BD0208007C10C202080080101B0308008410C70208008C10BD0208009010C202080094101B0308009810C70208009C1020030800A01025030800A4102A030800A810CC020800AC1033030800B410BD020800B810C2020800BC101B030800C010C7020800C41020030A00C8108F0B0800D010B8020800D410BD020800D81038030800DC103D030800E0104B030800E4105A030800E8105F030800F010B8020800F410BD020800F810C2020800FC101B0308000011C7020800D411B8020800D811BD020800DC11C2020800E0111B030800E411C7020800E81138030800EC113D030800F0114B030800F41150030800F8115A030800FC115F0308000C13BD0208001013C20208001813B80208001C13BD0208002013C20208002813B80208002C13BD0208003013C202080034131B0308003C13B80208004013BD0208004413C20208004C13B80208005013BD0208005413C20208005C13B80208006013BD0208006413C20208006C13B80208007013BD0208007413C202080078131B0308007C13C70208009013B80208009413BD0208009813C2020800A013B8020800A413BD020800A813C202080034148C1108004414B80208004814BD022E001300691C2E001B00831C2E008300021D2E004300831C2E007B00F91C2E002B00891C2E003300691C2E003B00981C2E002300831C2E005300831C2E005B00B91C2E006B00E31C2E007300F01C03012303BD02E3022303BD02C3072303BD02030D5B06BD026028AB05BD028028AB05BD0240308307BD0260308307BD0280308307BD02A0308307BD02C0308307BD0201001000000069009512A512B412C912011352136D1373137E1393139F13A613D513FD13041419144514601469148A149914A414B414BB14C914D514E114191520152A152F15331537153B15411547154D155215631588158E15B1150F1615161E16461656165D167716A316B116E1160817191731174B175617621783178D17CF17041815182018281830183A1843184A1856186D18A018D61809191E193C195419641971198D199D19A519B519C319F619121A1D1A251A2B1A321A381A441A561A5C1A631A7B1A8F1AA51AB81ABF1ACA1ADA1AF21A021B0B1B151B1C1B5A1B841BA51BAE1BC71BD51BDD1BE51B011C0A1C0F1C141C1C1C2B1C321C381C3D1C481C571C04000100070002002B0004002C0018003900290048002F004B0030004E0037005100380052003D005B003E0063003F006400430066004A000000AA09520100006F00AA020000CE0BAF020000E851E10A0000F151520100000452E60A00001352E60A00002352E10A00002E52EA0A00003752EE0A00004A52E10A00005652E10A00006052E10A00006A52E10A00007A52E10A00008A52E10A00009452E10A00009E52E10A0000AD52EE0A0000C252F20A0000C952F80A00004003FE0A0000D152E10A00001256520100001E56F80A00002356E60A00003256E60A00003B56E60A00004656EA0A0000F806E60A00005256E10A00005B56E60A00006856E60A00007756E60A00008556E60A00009356EE0A00009E56E60A0000A756160B0000CD021B0B0000B156E60A0000FA59EA0A0000035AE10A00000A5AE60A00000904F20A0000125A520100001C5AE60A0000FB63E60A00000966E10A00001A66E10A00002866E10A00003666E10A00004266E10A00005466E10A00006366E10A0000FB63E60A0000C768E10A0000D768E10A0000EA68E10A0000F76878100000FC687E100000F768B0100000206D0E110000B171EE0A0000C371EE0A0000DE71EA0A0000ED71EA0A0000D772EA0A0000E172EA0A0000EE7215120000F1721A12000001731A1200000F7320120000147325120000D772EA0A0000E172EA0A0000EE72151200007F73151200008373EA0A0000F1721A12000001731A1200000F73201200001473251202001C000300020024000500010025000700020044000900010045000900020046000B00010047000B00020048000D00010049000D0002004A000F0001004B000F0002004C00110001004D00110002004E00130001004F001300020050001500010051001500010053001700020052001700020054001900010055001900020056001B00010058001D00020057001D00020059001F0001005A001F0001005C00210002005B00210001005E00230002005D00230002005F002500010060002500010062002700020061002700020063002900010064002900020065002B00010066002B00020067002D00010068002D0001006A002F00020069002F0002006C00310001006D00310001006F00330002006E003300010071003500020070003500020072003700010073003700020074003900010075003900020076003B00010077003B00020078003D00010079003D0002007A003F0001007B003F0001007D00410002007C00410002007E00430001007F004300010081004500020080004500010083004700020082004700020084004900010085004900020086004B00010087004B00020088004D00010089004D0002008A004F0001008B004F0002008C00510001008D00510002008F005300010090005300020091005500010092005500020093005700010094005700020095005900010096005900010098005B00020097005B00020099005D0001009A005D000200F4005F000200F80061000200F90063000200FA0065000200FB0067000200FC0069000200FD006B000200FE006D00020004016F0002000E01710002000F017300020010017500020011017700020012017900020020017B00020029017D0001002A017D0002004F017F0002005001810002005101830002005201850001005901870002005801870002005A01890001005B01890001005D018B0002005C018B0002005E018D0002005F018F0002006001910001006101910002006201930001006301930002006D01950001006E01950002006F019700010070019700020071019900010072019900010074019B00020073019B00020075019D00010076019D00020077019F0002007801A10002007901A30001007A01A30002007B01A50001007C01A5006E147C148414CC1B288201001C05048000000100000000000000000000000000057D000002000000000000000000000001006907000000000200000000000000000000000100DD070000000045004400460044004700440048004400490044004A0044004B0044004C0044004D0044004E0044004F004400500044005100440052004400530044005400440055004400560044005700440058004400590044005A0044005C005B005E005D005F005D0062006100690068000000003C4D6F64756C653E0053514C427573696E6573734C6F6769632E646C6C0054334753005753492E436F6D6D6F6E004163636F756E744D6F76656D656E747354657374004163636F756E744D6F76656D656E74735461626C6500436173686965724D6F76656D656E747354657374004361736869657253657373696F6E496E666F00436173686965724D6F76656D656E74735461626C65005341535F464C414753004143434F554E545F50524F4D4F5F4352454449545F54595045004143434F554E545F50524F4D4F5F5354415455530047555F555345525F5459504500434153484945525F4D4F56454D454E54004D6F76656D656E745479706500506C617953657373696F6E54797065004163636F756E745479706500436173686C6573734D6F646500506C617953657373696F6E537461747573004754506C617953657373696F6E537461747573004754506C61796572547261636B696E675370656564004754506C617953657373696F6E506C61796572536B696C6C0050656E64696E6747616D65506C617953657373696F6E5374617475730047616D696E675461626C6544726F70426F78436F6C6C656374696F6E436F756E74004163636F756E74426C6F636B526561736F6E004163636F756E74426C6F636B556E626C6F636B536F75726365005465726D696E616C54797065730048414E445041595F54595045005465726D696E616C537461747573004143434F554E54535F50524F4D4F5F434F5354005757505F434F4E4E454354494F4E5F54595045005757505F434F4E4E454354494F4E5F53544154555300545950455F4D554C5449534954455F534954455F5441534B530053657175656E6365496400506172616D617465727354797065466F725369746500434153484945525F53455353494F4E5F5354415455530043757272656E637945786368616E6765537562547970650043757272656E637945786368616E676554797065005472616E73616374696F6E54797065004F7065726174696F6E436F64650043617368696572566F75636865725479706500556E646F5374617475730046554C4C5F4E414D455F54595045005743505F5465726D696E616C547970650043757272656E637945786368616E6765526573756C740042616E6B5472616E73616374696F6E446174610047524F55505F454C454D454E545F54595045004558504C4F49545F454C454D454E545F54595045005061727469636970617465496E436173684465736B447261770055706772616465446F776E6772616465416374696F6E004177617264506F696E7473537461747573004C616E67756167654964656E7469666965720053595354454D5F4D4F4445005041545445524E5F54595045004754506C6179657254797065004361676543757272656E6379547970650043757272656E6379547970650053746174757355706461746543757272656E6379547970650043617368696572436F6E636570744F7065726174696F6E526573756C74005053415F54595045004361736869657253657373696F6E5472616E73666572537461747573005465726D696E616C426F7854797065005465726D696E616C43617368446972656374696F6E00466C61674275636B6574496400486F6C646572486170707942697274684461794E6F74696669636174696F6E0050696E5061645479706500486F6C6465724C6576656C54797065004275636B65744964005072697A6543617465676F7279004D756C746950726F6D6F7300537461727453657373696F6E53746174757300537461727453657373696F6E5472616E736665724D6F646500537461727453657373696F6E496E70757400537461727453657373696F6E4F757470757400456E6453657373696F6E496E70757400506C61796564576F6E4D6574657273005449544F53657373696F6E4D6574657273005449544F53657373696F6E4D657465727343617368496E00456E6453657373696F6E53746174757300456E6453657373696F6E4F757470757400496E53657373696F6E506172616D657465727300506C617953657373696F6E436C6F7365506172616D6574657273004163636F756E7442616C616E63650050726F6D6F42616C616E6365004163636F756E74496E666F004163636F756E7450726F6D6F74696F6E73416374696F6E00414C41524D5F424952544844415900524551554553545F5459504500524551554553545F545950455F524553504F4E53450047414D455F474154455741595F52455345525645445F4D4F444500454E554D5F47414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E00496E53657373696F6E416374696F6E0050726F76696465724C6973740050726F76696465724C69737454797065005465726D696E616C0043726564697473506C61794D6F6465005465726D696E616C496E666F00547261636B4461746100436172644E756D6265720043726970744D6F646500415243466F75724D616E616765645472616E73666F726D0052433400415243466F75724D616E616765640052433443727970746F5365727669636550726F76696465720053716C50726F63656475726573006D73636F726C69620053797374656D004F626A65637400456E756D0049436C6F6E6561626C650053797374656D2E53656375726974792E43727970746F677261706879004943727970746F5472616E73666F726D0049446973706F7361626C650053796D6D6574726963416C676F726974686D0053797374656D2E446174610053797374656D2E446174612E53716C436C69656E740053716C5472616E73616374696F6E00537461727400446563696D616C0055706461746500456E64004163636F756E745374617475730053656E644576656E74005570646174655F496E7465726E616C005472616E736C6174650044425F436865636B496645786973747356656E646F7249640044425F4765744163636F756E74416E645465726D696E616C49644279506C617953657373696F6E4964005465737431005465737432002E63746F7200446174615461626C65006D5F7461626C65006D5F636173686965725F73657373696F6E5F696E666F006D5F7465726D696E616C5F6964006D5F7465726D696E616C5F6E616D65006D5F706C61795F73657373696F6E5F6964006D5F747261636B5F64617461006D5F6163636F756E745F6964006D5F636173686965725F6E616D6500536574506C617953657373696F6E4964005365744163636F756E74547261636B4461746100496E6974536368656D61004164640045787465726E616C547261636B44617461006765745F4C61737453617665644D6F76656D656E74496400436C6561720053617665644D6F76656D656E747349640053617665004C61737453617665644D6F76656D656E744964004361736869657253657373696F6E4964005465726D696E616C496400557365724964005465726D696E616C4E616D6500557365724E616D6500417574686F72697A6564427955736572496400417574686F72697A65644279557365724E616D650049734D6F62696C6542616E6B004461746554696D650047616D696E6744617900557365725479706500436F70790044617461526F77006D5F726F77006D5F6D6F76656D656E745F6964006765745F4361736869657253657373696F6E496E666F007365745F446174615461626C65546F53617665004E756C6C61626C6560310041646445786368616E67650041646443757272656E637945786368616E67650041646443617368416476616E636545786368616E67650041646445786368616E67655F496E7465726E616C0041646452656C617465644964496E4C6173744D6F7600416464537562416D6F756E74496E4C6173744D6F7600536574416464416D6F756E74496E4C6173744D6F7600536574496E697469616C42616C616E6365496E4C6173744D6F760053657444617465496E4C6173744D6F7600536574496E697469616C416E6446696E616C42616C616E6365416D6F756E74496E4C6173744D6F7600526F7773436F756E7400476574436173686965724D6F76656D656E74416464537562416D6F756E7400476574436173686965724D6F76656D656E7442616C616E6365496E6372656D656E7400446174615461626C65546F536176650076616C75655F5F004E4F4E45005553455F455854454E4445445F4D455445525300435245444954535F504C41595F4D4F44455F5341535F4D414348494E45005553455F48414E445041595F494E464F524D4154494F4E005350454349414C5F50524F47524553534956455F4D45544552005553455F4D455445525F46495845445F344244435F4C454E4754485F42495430005553455F4D455445525F46495845445F344244435F4C454E4754485F42495431005553455F4D455445525F46495845445F344244435F4C454E4754485F42495432005553455F4D455445525F46495845445F344244435F4C454E4754485F42495433005553455F4D455445525F46495845445F354244435F4C454E4754485F42495430005553455F4D455445525F46495845445F354244435F4C454E4754485F42495431005553455F4D455445525F46495845445F354244435F4C454E4754485F42495432005553455F4D455445525F46495845445F354244435F4C454E4754485F42495433005553455F464C4147535F464F5243455F4E4F5F52544500414C4C4F575F53494E474C455F42595445005341535F464C4147535F49474E4F52455F4E4F5F4245545F504C415953005341535F464C41475F44495341424C45445F4146545F4F4E5F494E54525553494F4E005341535F464C4147535F4146545F414C54484F5547485F4C4F434B5F5A45524F005341535F464C4147535F454E41424C455F44495341424C455F4E4F54455F4143434550544F5200554E4B4E4F574E004E5231004E52320052454445454D41424C4500504F494E5400554E523100554E5232004E52330052454445454D41424C455F494E5F4B494E440041574152444544004143544956450045584841555354454400455850495245440052454445454D4544004E4F545F41574152444544004552524F520043414E43454C4C45445F4E4F545F41444445440043414E43454C4C45440043414E43454C4C45445F42595F504C415945520050524541535349474E4544004E4F545F41535349474E45440055534552005359535F4143434550544F52005359535F50524F4D4F424F58005359535F53595354454D005359535F5449544F005359535F47414D494E475F5441424C45005359535F524544454D5054494F4E005359535F4332474F00535550455255534552004F50454E5F53455353494F4E00434C4F53455F53455353494F4E0046494C4C45525F494E0046494C4C45525F4F555400434153485F494E00434153485F4F5554005441585F4F4E5F5052495A45310050524F4D4F54494F4E5F4E4F545F52454445454D41424C45005052495A455300434152445F4445504F5349545F494E00434152445F4445504F5349545F4F55540043414E43454C5F4E4F545F52454445454D41424C45004D425F4D414E55414C5F4348414E47455F4C494D4954004D425F434153485F494E005441585F4F4E5F5052495A45320050524F4D4F5F435245444954530050524F4D4F5F544F5F52454445454D41424C450050524F4D4F5F455850495245440050524F4D4F5F43414E43454C0050524F4D4F5F53544152545F53455353494F4E0050524F4D4F5F454E445F53455353494F4E00434152445F5245504C4143454D454E5400445241575F5449434B45545F5052494E540048414E445041590048414E445041595F43414E43454C4C4154494F4E004D414E55414C5F48414E44504159004D414E55414C5F48414E445041595F43414E43454C4C4154494F4E00434153485F494E5F53504C495432004D425F434153485F494E5F53504C495432004445565F53504C49543200434153485F494E5F53504C495431004445565F53504C495431004D425F434153485F494E5F53504C4954310043414E43454C5F53504C4954310043414E43454C5F53504C49543200504F494E54535F544F5F445241575F5449434B45545F5052494E54005052495A455F434F55504F4E00434152445F4352454154494F4E00434152445F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E4745004143434F554E545F50494E5F52414E444F4D00434153485F494E5F434F5645525F434F55504F4E00434153485F494E5F4E4F545F52454445454D41424C4532004D425F434153485F494E5F4E4F545F52454445454D41424C45004D425F434153485F494E5F434F5645525F434F55504F4E004D425F434153485F494E5F4E4F545F52454445454D41424C4532004D425F5052495A455F434F55504F4E004E415F434153485F494E5F53504C495431004E415F434153485F494E5F53504C495432004E415F5052495A455F434F55504F4E004E415F434153485F494E5F4E4F545F52454445454D41424C45004E415F434153485F494E5F434F5645525F434F55504F4E00434152445F52454359434C454400504F494E54535F544F5F4E4F545F52454445454D41424C4500504F494E54535F544F5F52454445454D41424C450052454445454D41424C455F4445565F455850495245440052454445454D41424C455F5052495A455F45585049524544004E4F545F52454445454D41424C455F455850495245440050524F4D4F54494F4E5F52454445454D41424C450043414E43454C5F50524F4D4F54494F4E5F52454445454D41424C450050524F4D4F54494F4E5F504F494E540043414E43454C5F50524F4D4F54494F4E5F504F494E54005441585F52455455524E494E475F4F4E5F5052495A455F434F55504F4E00444543494D414C5F524F554E44494E4700534552564943455F434841524745004D425F434153485F4C4F535400524551554553545F544F5F564F55434845525F52455052494E5400434845434B5F5041594D454E5400434153485F50454E44494E475F434C4F53494E47004143434F554E545F424C4F434B4544004143434F554E545F554E424C4F434B45440043555252454E43595F45584348414E47455F53504C4954310043555252454E43595F434152445F45584348414E47455F53504C49543100434153494E4F5F43484950535F45584348414E47455F53504C495431005449544F5F5449434B45545F434153484945525F5052494E5445445F4341534841424C4500504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C4954320043555252454E43595F45584348414E47455F53504C4954320043555252454E43595F434152445F45584348414E47455F53504C49543200434153494E4F5F43484950535F45584348414E47455F53504C49543200524551554553545F544F5F5449434B45545F52455052494E540050415254494349504154494F4E5F494E5F434153484445534B5F445241570043555252454E43595F434845434B5F53504C4954310043555252454E43595F434845434B5F53504C495432005052495A455F494E5F4B494E44315F47524F5353005052495A455F494E5F4B494E44315F54415831005052495A455F494E5F4B494E44315F5441583200434C4F53455F53455353494F4E5F42414E4B5F4341524400434C4F53455F53455353494F4E5F434845434B0046494C4C45525F4F55545F434845434B005052495A455F494E5F4B494E44325F47524F5353005052495A455F494E5F4B494E44325F54415831005052495A455F494E5F4B494E44325F54415832005449544F5F5449434B45545F434153484945525F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F52454953535545005449544F5F5449434B45545F4352454154455F4341534841424C455F4F46464C494E45005449544F5F5449434B45545F4352454154455F504C415941424C455F4F46464C494E45005449544F5F5449434B45545F455850495245445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F455850495245445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F455850495245445F52454445454D41424C45005449544F5F5449434B45545F4341534841424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F504C415941424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F48414E44504159005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4A41434B504F54005449544F5F5449434B45545F434153484945525F504149445F48414E44504159005449544F5F5449434B45545F434153484945525F504149445F4A41434B504F54005449544F5F56414C49444154455F5449434B4554005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F434153484945525F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4A41434B504F54005449544F5F5449434B45545F434153484945525F455850495245445F504149445F48414E44504159005449544F5F4C4153545F4D4F56454D454E54005452414E534645525F4352454449545F4F5554005452414E534645525F4352454449545F494E00434153485F494E5F5441585F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F5052495A4500434152445F5245504C4143454D454E545F464F525F4652454500434152445F5245504C4143454D454E545F494E5F504F494E545300434153485F494E5F5441585F53504C4954320043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954310043555252454E43595F44454249545F434152445F45584348414E47455F53504C4954310043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954320043555252454E43595F44454249545F434152445F45584348414E47455F53504C49543200434153485F414456414E43455F43555252454E43595F45584348414E474500434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434153485F414456414E43455F4352454449545F42414E4B5F4341524400434153485F414456414E43455F44454249545F42414E4B5F4341524400434153485F414456414E43455F434845434B00434153485F414456414E43455F43415348004E4F545F504C415945445F52454348415247455F524546554E44454400434153485F434C4F53494E475F53484F52545F4445505245434154454400434153485F434C4F53494E475F4F5645525F4445505245434154454400434153485F434C4F53494E475F53484F525400434153485F434C4F53494E475F4F5645520046494C4C45525F494E5F434C4F53494E475F53544F434B0046494C4C45525F4F55545F434C4F53494E475F53544F434B00434147455F4F50454E5F53455353494F4E00434147455F434C4F53455F53455353494F4E00434147455F46494C4C45525F494E00434147455F46494C4C45525F4F555400434147455F434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C00434147455F434153484945525F524546494C4C5F484F505045525F5445524D494E414C00434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C4500434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C455F44455441494C00434147455F4C4153545F4D4F56454D454E540043484950535F53414C450043484950535F50555243484153450043484950535F53414C455F4445564F4C5554494F4E5F464F525F5449544F0043484950535F53414C455F544F54414C0043484950535F50555243484153455F544F54414C0043484950535F4348414E47455F494E0043484950535F4348414E47455F4F55540043484950535F53414C455F574954485F434153485F494E0043484950535F50555243484153455F574954485F434153485F4F55540043484950535F53414C455F544F54414C5F45584348414E47450043484950535F50555243484153455F544F54414C5F45584348414E47450043484950535F53414C455F52454749535445525F544F54414C0043484950535F4C4153545F4D4F56454D454E540047414D494E475F5441424C455F47414D455F4E455457494E0047414D494E475F5441424C455F47414D455F4E455457494E5F434C4F53450048414E445041595F415554484F52495A45440048414E445041595F554E415554484F52495A45440048414E445041595F564F494445440048414E445041595F564F494445445F554E444F4E45004D425F45584345535300434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C4954320048414E445041595F57495448484F4C44494E4700434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543100434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543200434F4D50414E595F425F434153485F414456414E43455F43555252454E43595F45584348414E474500434F4D50414E595F425F434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F4352454449545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F44454249545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F434845434B0057494E5F4C4F53535F53544154454D454E545F5245515545535400434152445F4445504F5349545F494E5F434845434B00434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434152445F4445504F5349545F494E5F434152445F43524544495400434152445F4445504F5349545F494E5F434152445F444542495400434152445F4445504F5349545F494E5F434152445F47454E4552494300434152445F5245504C4143454D454E545F434845434B00434152445F5245504C4143454D454E545F434152445F43524544495400434152445F5245504C4143454D454E545F434152445F444542495400434152445F5245504C4143454D454E545F434152445F47454E4552494300434F4D50414E595F425F434152445F4445504F5349545F494E00434F4D50414E595F425F434152445F4445504F5349545F4F555400434F4D50414E595F425F434152445F5245504C4143454D454E5400434F4D50414E595F425F434152445F4445504F5349545F494E5F434845434B00434F4D50414E595F425F434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F43524544495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F444542495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F47454E4552494300434F4D50414E595F425F434152445F5245504C4143454D454E545F434845434B00434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F43524544495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F444542495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F47454E45524943005452414E534645525F43415348494552535F53454E44005452414E534645525F43415348494552535F5245434549564500434153484945525F524546494C4C5F484F505045525F5445524D494E414C005445524D494E414C5F524546494C4C5F484F5050455200434153484945525F4348414E47455F535441434B45525F5445524D494E414C00434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F52454445454D41424C4500434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C5F4341474500434153484945525F524546494C4C5F484F505045525F5445524D494E414C5F43414745005052495A455F52455F494E5F4B494E44315F47524F5353005052495A455F52455F494E5F4B494E44325F47524F5353005052495A455F52455F494E5F4B494E44315F54415831005052495A455F52455F494E5F4B494E44315F54415832005052495A455F52455F494E5F4B494E44325F54415831005052495A455F52455F494E5F4B494E44325F5441583200435553544F4D45525F454E5452414E4345005449544F5F5449434B4554535F4352454154455F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B4554535F504C415945445F45584348414E47455F4455414C5F43555252454E43590042494C4C5F494E5F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B45545F434153484945525F564F49445F4341534841424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F564F49445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005441585F50524F564953494F4E53005449544F5F5449434B45545F434153484945525F535741505F464F525F4348495053005441585F435553544F4459005441585F52455455524E494E475F435553544F445900434152445F4153534F43494154450043555252454E43595F45584348414E47455F544F5F4E4154494F4E414C5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F4F55540043555252454E43595F45584348414E47455F544F5F464F524549474E5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F494E005449544F5F5449434B45545F434F554E54525F5052494E5445445F4341534841424C450043555252454E43595F45584348414E47455F414D4F554E545F494E5F4348414E474500435553544F4D45525F454E5452414E43455F4341524400435553544F4D45525F454E5452414E43455F44454249545F4341524400435553544F4D45525F454E5452414E43455F4352454449545F4341524400435553544F4D45525F454E5452414E43455F434845434B0050524F4D4F54494F4E5F52454445454D41424C455F4645444552414C5F5441580050524F4D4F54494F4E5F52454445454D41424C455F53544154455F544158004332474F5F4C4F434B5F4143434F554E545F494E5445524E414C004332474F5F4C4F434B5F4143434F554E545F45585445524E414C004332474F5F434153485F494E5F494E5445524E414C004332474F5F434153485F494E5F45585445524E414C004332474F5F434153485F4F55545F494E5445524E414C004332474F5F434153485F4F55545F45585445524E414C004332474F5F41435449564154455F4143434F554E545F494E5445524E414C004332474F5F444541435449564154455F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F45585445524E414C004143434F554E545F4144445F424C41434B4C495354004143434F554E545F52454D4F56455F424C41434B4C4953540043555252454E43595F434152445F45584348414E47455F53504C4954315F554E444F0043555252454E43595F434152445F45584348414E47455F53504C4954325F554E444F00434153485F414456414E43455F43555252454E43595F45584348414E47455F554E444F00434153485F414456414E43455F47454E455249435F42414E4B5F434152445F554E444F00434153485F414456414E43455F4352454449545F42414E4B5F434152445F554E444F00434153485F414456414E43455F44454249545F42414E4B5F434152445F554E444F00434152445F4445504F5349545F494E5F434152445F554E444F00434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F554E444F00534552564943455F4348415247455F4F4E5F434F4D50414E595F41005052495A455F445241575F47524F53535F4F54484552004332474F5F4449534153534F43494154455F4143434F554E545F494E5445524E414C00434C4F53455F53455353494F4E5F4332474F004D554C5449504C455F4255434B4554535F454E445F53455353494F4E004D554C5449504C455F4255434B4554535F454E445F53455353494F4E5F4C415354004D554C5449504C455F4255434B4554535F45585049524544004D554C5449504C455F4255434B4554535F455850495245445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F414444004D554C5449504C455F4255434B4554535F4D414E55414C5F4144445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F535542004D554C5449504C455F4255434B4554535F4D414E55414C5F5355425F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F534554004D554C5449504C455F4255434B4554535F4D414E55414C5F5345545F4C41535400434153484945525F42595F434F4E434550545F494E00434153484945525F42595F434F4E434550545F4F555400434153484945525F42595F434F4E434550545F4C41535400506C61790043617368496E00436173684F7574004465766F6C7574696F6E005461784F6E5072697A65310053746172744361726453657373696F6E00456E644361726453657373696F6E00416374697661746500446561637469766174650050726F6D6F74696F6E4E6F7452656465656D61626C65004465706F736974496E004465706F7369744F75740043616E63656C4E6F7452656465656D61626C65005461784F6E5072697A65320050726F6D6F437265646974730050726F6D6F546F52656465656D61626C650050726F6D6F457870697265640050726F6D6F43616E63656C0050726F6D6F537461727453657373696F6E0050726F6D6F456E6453657373696F6E00437265646974734578706972656400437265646974734E6F7452656465656D61626C654578706972656400436172645265706C6163656D656E7400447261775469636B65745072696E740048616E647061790048616E6470617943616E63656C6C6174696F6E004D616E75616C456E6453657373696F6E0050726F6D6F4D616E75616C456E6453657373696F6E004D616E75616C48616E64706179004D616E75616C48616E6470617943616E63656C6C6174696F6E00506F696E74734177617264656400506F696E7473546F476966745265717565737400506F696E7473546F4E6F7452656465656D61626C6500506F696E74734769667444656C697665727900506F696E74734578706972656400506F696E7473546F447261775469636B65745072696E7400506F696E747347696674536572766963657300486F6C6465724C6576656C4368616E676564005072697A65436F75706F6E00444550524543415445445F52656465656D61626C655370656E7455736564496E50726F6D6F74696F6E7300506F696E7473546F52656465656D61626C65005072697A6545787069726564004361726443726561746564004163636F756E74506572736F6E616C697A6174696F6E004D616E75616C6C794164646564506F696E74734F6E6C79466F7252656465656D004163636F756E7450494E4368616E6765004163636F756E7450494E52616E646F6D00437265646974734E6F7452656465656D61626C6532457870697265640043617368496E436F766572436F75706F6E0043617368496E4E6F7452656465656D61626C65320043616E63656C537461727453657373696F6E004361726452656379636C65640050726F6D6F74696F6E52656465656D61626C650043616E63656C50726F6D6F74696F6E52656465656D61626C650050726F6D6F74696F6E506F696E740043616E63656C50726F6D6F74696F6E506F696E74004D616E75616C486F6C6465724C6576656C4368616E6765640054617852657475726E696E674F6E5072697A65436F75706F6E00446563696D616C526F756E64696E6700536572766963654368617267650043616E63656C47696674496E7374616E636500506F696E74735374617475734368616E676564004D616E75616C6C794164646564506F696E7473466F724C6576656C00496D706F727465644163636F756E7400496D706F72746564506F696E74734F6E6C79466F7252656465656D00496D706F72746564506F696E7473466F724C6576656C00496D706F72746564506F696E7473486973746F7279004163636F756E74426C6F636B6564004163636F756E74556E626C6F636B65640043616E63656C6C6174696F6E005472616E736665724372656469744F7574005472616E73666572437265646974496E0043617368496E54617800436172645265706C6163656D656E74466F724672656500436172645265706C6163656D656E74496E506F696E74730043617368416476616E63650045787465726E616C53797374656D506F696E7473537562737472616374004D756C74695369746543757272656E744C6F63616C506F696E7473005449544F5F5469636B6574436173686965725072696E7465644361736861626C65005449544F5F5469636B657443617368696572506169644361736861626C65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C617965644361736861626C65005449544F5F5469636B657452656973737565005449544F5F4163636F756E74546F5465726D696E616C437265646974005449544F5F5465726D696E616C546F4163636F756E74437265646974005449544F5F5469636B65744F66666C696E65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744D616368696E655072696E74656448616E64706179005449544F5F5469636B65744D616368696E655072696E7465644A61636B706F74005449544F5F5469636B6574436173686965725061696448616E64706179005449544F5F5469636B657443617368696572506169644A61636B706F74005449544F5F5469636B65744D616368696E655072696E7465644361736861626C65005449544F5F5469636B65744D616368696E655072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B6574436173686965725061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C6179656450726F6D6F52656465656D61626C65005449544F5F7469636B65744D616368696E65506C6179656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644361736861626C65005449544F5F5469636B657443617368696572457870697265645061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644A61636B706F74005449544F5F5469636B657443617368696572457870697265645061696448616E6470617900436173686465736B4472617750617274696369706174696F6E00436173686465736B44726177506C617953657373696F6E00436869707353616C654465766F6C7574696F6E466F725469746F00436869707353616C65546F74616C0043686970735075726368617365546F74616C0057696E4C6F737353746174656D656E745265717565737400436172644465706F736974496E436865636B00436172644465706F736974496E43757272656E637945786368616E676500436172644465706F736974496E4361726443726564697400436172644465706F736974496E43617264446562697400436172644465706F736974496E4361726447656E6572696300436172645265706C6163656D656E74436865636B00436172645265706C6163656D656E744361726443726564697400436172645265706C6163656D656E7443617264446562697400436172645265706C6163656D656E744361726447656E657269630047616D65476174657761794265740047616D65476174657761795072697A65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F52656465656D61626C650047616D6547617465776179526573657276654372656469740047616D6547617465776179426574526F6C6C6261636B0054617850726F766973696F6E7300546178437573746F64790054617852657475726E437573746F64790043757272656E637945786368616E6765436173686965724F7574005449544F5F5469636B6574436F756E74525072696E7465644361736861626C6500436172644173736F63696174650043757272656E637945786368616E676543617368696572496E0050726F6D6F74696F6E52656465656D61626C654665646572616C5461780050726F6D6F74696F6E52656465656D61626C655374617465546178004332474F43617368496E496E7465726E616C004332474F43617368496E45787465726E616C004332474F436173684F7574496E7465726E616C004332474F436173684F757445787465726E616C004332474F41637469766174654163636F756E74496E7465726E616C004332474F446561637469766174654163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E7445787465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E74496E7465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E7445787465726E616C004163636F756E74496E426C61636B6C697374004163636F756E744E6F74496E426C61636B6C6973740050617269506C61794265740050617269506C61795072697A650050617269506C6179426574526F6C6C6261636B0041646D697373696F6E004642537562416D6F756E74004642416464416D6F756E74004642526F6C6C6261636B004332474F4469736173736F63696174654163636F756E74496E7465726E616C0048494444454E5F52656368617267654E6F74446F6E655769746843617368004D554C5449504C455F4255434B4554535F456E6453657373696F6E004D554C5449504C455F4255434B4554535F456E6453657373696F6E4C617374004D554C5449504C455F4255434B4554535F45787069726564004D554C5449504C455F4255434B4554535F457870697265644C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F416464004D554C5449504C455F4255434B4554535F4D616E75616C5F4164645F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F537562004D554C5449504C455F4255434B4554535F4D616E75616C5F5375625F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F536574004D554C5449504C455F4255434B4554535F4D616E75616C5F5365745F4C61737400434144494C4C41435F4A41434B0057494E00414C455349530048414E445041595F43414E43454C4C45445F435245444954530048414E445041595F4A41434B504F540048414E445041595F4F525048414E5F435245444954530048414E445041595F4D414E55414C0048414E445041595F534954455F4A41434B504F5400445241570050524F47524553534956455F50524F564953494F4E0050524F47524553534956455F524553455256450047414D45474154455741590050415249504C4159004143434F554E545F554E4B4E4F574E004143434F554E545F434144494C4C41435F4A41434B004143434F554E545F57494E004143434F554E545F414C45534953004143434F554E545F5649525455414C5F43415348494552004143434F554E545F5649525455414C5F5445524D494E414C004F70656E656400436C6F736564004162616E646F6E6564004D616E75616C436C6F7365640043616E63656C6C65640048616E647061795061796D656E740048616E6470617943616E63656C5061796D656E74004472617757696E6E657200447261774C6F7365720050726F6772657373697665526573657276650050726F677265737369766550726F766973696F6E004177617900556E6B6E6F776E00536C6F77004D656469756D004661737400536F66740041766572616765004861726400496E697469616C00454C5030315F53706C69745F50535F52656464656D5F4E6F52656465656D004361676500436173686965720046524F4D5F43415348494552004D41585F42414C414E4345004142414E444F4E45445F434152440046524F4D5F475549004D41585F50494E5F4641494C5552455300494D504F52540045585445524E414C5F53595354454D00484F4C4445525F4C4556454C5F444F574E475241444500484F4C4445525F4C4556454C5F555047524144450045585445524E414C5F53595354454D5F43414E43454C45440045585445524E414C5F414D4C00494E54525553494F4E00494E4143544956455F504C41595F53455353494F4E004332474F5F494E5445524E414C004332474F5F45585445524E414C00424C41434B4C495354004755490043415348494552004332474F00524543455054494F4E5F424C41434B4C495354005341535F484F5354005349544500534954455F4A41434B504F54004D4F42494C455F42414E4B004D4F42494C455F42414E4B5F494D42004953544154530050524F4D4F424F5800434153484445534B5F445241570047414D494E475F5441424C455F53454154004F46464C494E4500434153484445534B5F445241575F5441424C450057494E5F55500043414E43454C4C45445F43524544495453004A41434B504F54004F525048414E5F43524544495453004D414E55414C005350454349414C5F50524F4752455353495645004D414E55414C5F43414E43454C4C45445F43524544495453004D414E55414C5F4A41434B504F54004D414E55414C5F4F5448455253004F55545F4F465F5345525649434500524554495245440052455345545F4F4E5F52454445454D005345545F4F4E5F46495253545F4E525F50524F4D4F54494F4E005550444154455F4F4E5F5245434841524745005550444154455F4F4E5F52454445454D0052455345545F4F4E5F5245434841524745004D554C54495349544500535550455243454E54455200434F4E4E454354454400444953434F4E4E454354454400446F776E6C6F6164436F6E66696775726174696F6E0055706C6F6164506F696E74734D6F76656D656E74730055706C6F6164506572736F6E616C496E666F0055706C6F61644163636F756E74446F63756D656E747300446F776E6C6F6164506F696E74735F5369746500446F776E6C6F6164506F696E74735F416C6C00446F776E6C6F6164506572736F6E616C496E666F5F4361726400446F776E6C6F6164506572736F6E616C496E666F5F5369746500446F776E6C6F6164506572736F6E616C496E666F5F416C6C00446F776E6C6F616450726F76696465727347616D65730055706C6F616450726F76696465727347616D657300446F776E6C6F616450726F76696465727300446F776E6C6F61644163636F756E74446F63756D656E74730055706C6F616453616C6573506572486F75720055706C6F6164506C617953657373696F6E730055706C6F616450726F7669646572730055706C6F616442616E6B730055706C6F616441726561730055706C6F61645465726D696E616C730055706C6F61645465726D696E616C73436F6E6E65637465640055706C6F616447616D65506C617953657373696F6E7300446F776E6C6F61644D617374657250726F66696C657300446F776E6C6F6164436F72706F7261746555736572730055706C6F61644C61737441637469766974790055706C6F6164436173686965724D6F76656D656E74734772757065644279486F75720055706C6F6164416C61726D7300446F776E6C6F6164416C61726D5061747465726E7300446F776E6C6F61644C63644D657373616765730055706C6F616448616E64706179730055706C6F616453656C6C73416E644275797300526563657074696F6E456E74727900446F776E6C6F61644275636B657473436F6E6669670055706C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F616445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F416C6C004E6F7453657400566F75636865727353706C69744100566F75636865727353706C69744200566F75636865727353706C69744243616E63656C004163636F756E74496400547261636B4163636F756E744368616E67657300547261636B506F696E744368616E676573004163636F756E744D6F76656D656E74730050726F76696465727347616D65730050726F766964657273004163636F756E74446F63756D656E747300436173684465736B44726177004C43444D65737361676500566F7563686572456E7472616E63657300566F756368657254617850726F766973696F6E7300566F7563686572546178437573746F6479004F70656E436C6F73654C6F6F70536974655265717565737473004C696E6B656445787465726E616C4163636F756E747300566F756368657250617274696369706174696F6E4472617700566F756368657253756D6D61727943726564697473005449544F5F56616C69646174696F6E4E756D626572005465726D696E616C4D6574657273486973746F72794576656E74730050696E5061645472616E73616374696F6E00417564697449640050617279506C61790043616765436F6E63657074730043616765436F6E63657074735F4C617374004E6F74446F776E6C6F616400446F776E6C6F61645F55706461746500446F776E6C6F61645F5570646174655072696F726974794D756C74697369746500446F776E6C6F61645F5570646174655072696F7269747953697465004F50454E00434C4F534544004F50454E5F50454E44494E470050454E44494E475F434C4F53494E4700434845434B0042414E4B5F43415244004352454449545F434152440044454249545F43415244004341524432474F0043555252454E4359004341524400434153494E4F4348495000434153494E4F5F434849505F524500434153494E4F5F434849505F4E524500434153494E4F5F434849505F434F4C4F520046524545005449434B455400434153485F414456414E43450052454348415247450052454348415247455F434152445F4352454154494F4E004E4F545F5345540050524F4D4F54494F4E004D425F50524F4D4F54494F4E00474946545F5245515545535400474946545F44454C495645525900445241575F5449434B455400474946545F445241575F5449434B455400474946545F52454445454D41424C455F41535F43415348494E0043484950535F53414C455F574954485F5245434841524745004D425F4445504F53495400434153485F4445504F53495400434153485F5749544844524157414C0052454445454D41424C455F435245444954535F45585049524544004E4F545F52454445454D41424C455F435245444954535F4558504952454400504F494E54535F4558504952454400484F4C4445525F4C4556454C5F4348414E474544004143434F554E545F4352454154494F4E004143434F554E545F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E474544004E4F545F52454445454D41424C45325F435245444954535F45585049524544004E415F434153485F494E004E415F50524F4D4F54494F4E0043414E43454C5F474946545F494E5354414E43450050524F4D4F424F585F544F54414C5F5245434841524745535F5052494E540048414E445041595F56414C49444154494F4E00494D504F52545F504F494E5453005449544F5F4F46464C494E45005449544F5F52454953535545005449544F5F5449434B45545F56414C49444154494F4E00494D504F52545F47414D494E475F5441424C455F504C41595F53455353494F4E5300534146455F4B454550494E475F4445504F53495400534146455F4B454550494E475F5749544844524157005452414E534645525F434153484945525F53455353494F4E535F5749544844524157414C005452414E534645525F434153484945525F53455353494F4E535F4445504F534954005445524D494E414C5F434F4C4C4543545F44524F50424F580047414D45474154455741595F524553455256455F435245444954005445524D494E414C5F4348414E47455F535441434B45525F52455155455354004D554C5449504C455F4255434B4554535F454E4453455353494F4E004D554C5449504C455F4255434B4554535F4D414E55414C00434153484945525F524546494C4C5F484F5050455200434153484945525F524546494C4C5F484F505045525F4341474500434153484945525F434F4C4C4543545F524546494C4C00434153484945525F434F4C4C4543545F524546494C4C5F43414745005449544F5F564F49445F4D414348494E455F5449434B45540043484950535F53574150005052495A455F5041594F5554005052495A455F5041594F55545F414E445F52454348415247450043555252454E43595F45584348414E47455F4348414E47450043555252454E43595F45584348414E47455F4445564F4C5554494F4E0050524F4D4F54494F4E5F574954485F5441584553004332474F5F4348414E47455F50494E00434147455F434F4E434550545300434147455F434F4E43455054535F4C4153540043617368496E5F410043617368496E5F4200436173684F75745F410043616E63656C5F4200536572766963654368617267655F4200436173684465736B447261775F57696E6E6572005469636B65744F757400436869707353616C650043686970734275790043686970734368616E676500436869707353616C654465616C6572436F707900436173684F70656E696E670043617368436C6F73696E67004D4253616C65734C696D69744368616E6765004D424465706F736974004D42436C6F73696E6700436F6D6D697373696F6E5F4200436865636B436F6D6D697373696F6E4200436173684465736B447261775F4C6F7365720045786368616E6765436F6D6D697373696F6E4200436173684465706F73697400436173685769746864726177616C00436172644465706F736974496E5F4200436172645265706C6163656D656E745F4200436172644465706F7369744F75745F4200436173684465736B436C6F7365556E62616C616E63656400436173686965725472616E73666572436173684465706F73697400436173686965725472616E73666572436173685769746864726177616C005465726D696E616C73546F436F6C6C656374005465726D696E616C73546F526566696C6C00437573746F6D6572456E7472616E63650043686970735377617000436173685769746864726177616C4361726400546F74616C697A696E6753747269700050617274696369706174696F6E447261770053756D6D61727943726564697473004E6F6E6500556E646F6E6500556E646F4F7065726174696F6E004143434F554E540057495448484F4C44494E4700496E7465726D6564696174655365727665720047616D696E675465726D696E616C006D5F696E5F616D6F756E74006D5F696E5F69736F5F636F6465006D5F6F75745F69736F5F636F6465006D5F67726F73735F616D6F756E74006D5F636F6D697373696F6E006D5F6E65745F616D6F756E745F73706C697431006D5F6E65745F616D6F756E745F73706C697432006D5F4E52325F616D6F756E74006D5F636172645F7072696365006D5F636172645F636F6D697373696F6E73006D5F6368616E67655F72617465006D5F6E756D5F646563696D616C73006D5F7761735F666F7265696E675F63757272656E6379006D5F6172655F70726F6D6F74696F6E735F656E61626C6564006D5F696E5F74797065006D5F6163636F756E745F70726F6D6F74696F6E5F6964006D5F62616E6B5F7472616E73616374696F6E5F64617461006D5F6E65745F616D6F756E745F6465766F6C7574696F6E006D5F73756274797065006765745F496E416D6F756E74007365745F496E416D6F756E74006765745F4163636F756E7450726F6D6F74696F6E4964007365745F4163636F756E7450726F6D6F74696F6E4964006765745F496E43757272656E6379436F6465007365745F496E43757272656E6379436F6465006765745F4F757443757272656E6379436F6465007365745F4F757443757272656E6379436F6465006765745F4368616E676552617465007365745F4368616E676552617465006765745F446563696D616C73007365745F446563696D616C73006765745F576173466F7265696E6743757272656E6379007365745F576173466F7265696E6743757272656E6379006765745F47726F7373416D6F756E74007365745F47726F7373416D6F756E74006765745F436F6D697373696F6E007365745F436F6D697373696F6E006765745F4E6574416D6F756E74006765745F4E6574416D6F756E7453706C697431007365745F4E6574416D6F756E7453706C697431006765745F4E6574416D6F756E7453706C697432007365745F4E6574416D6F756E7453706C697432006765745F4E5232416D6F756E74007365745F4E5232416D6F756E74006765745F436172645072696365007365745F436172645072696365006765745F43617264436F6D697373696F6E73007365745F43617264436F6D697373696F6E73006765745F41726550726F6D6F74696F6E73456E61626C6564007365745F41726550726F6D6F74696F6E73456E61626C6564006765745F496E54797065007365745F496E54797065006765745F53756254797065007365745F53756254797065006765745F42616E6B5472616E73616374696F6E44617461007365745F42616E6B5472616E73616374696F6E44617461006765745F4E6574416D6F756E744465766F6C7574696F6E007365745F4E6574416D6F756E744465766F6C7574696F6E00496E416D6F756E74004163636F756E7450726F6D6F74696F6E496400496E43757272656E6379436F6465004F757443757272656E6379436F6465004368616E67655261746500446563696D616C7300576173466F7265696E6743757272656E63790047726F7373416D6F756E7400436F6D697373696F6E004E6574416D6F756E74004E6574416D6F756E7453706C697431004E6574416D6F756E7453706C697432004E5232416D6F756E74004361726450726963650043617264436F6D697373696F6E730041726550726F6D6F74696F6E73456E61626C656400496E547970650053756254797065004E6574416D6F756E744465766F6C7574696F6E006D5F6F7065726174696F6E5F6964006D5F74797065006D5F646F63756D656E745F6E756D626572006D5F62616E6B5F6E616D65006D5F686F6C6465725F6E616D65006D5F62616E6B5F636F756E747279006D5F636172645F747261636B5F64617461006D5F63757272656E63795F636F6465006D5F636172645F65787069726174696F6E5F64617465006D5F636172645F646174615F656469746564006D5F636865636B5F726F7574696E675F6E756D626572006D5F636865636B5F6163636F756E745F6E756D626572006D5F636F6D6D656E74006D5F636865636B5F6461746574696D65006D5F7472616E73616374696F6E5F74797065006D5F6163636F756E745F686F6C6465725F6E616D65006765745F4F7065726174696F6E4964007365745F4F7065726174696F6E4964006765745F54797065007365745F54797065006765745F446F63756D656E744E756D626572007365745F446F63756D656E744E756D626572006765745F42616E6B4E616D65007365745F42616E6B4E616D65006765745F486F6C6465724E616D65007365745F486F6C6465724E616D65006765745F42616E6B436F756E747279007365745F42616E6B436F756E747279006765745F547261636B44617461007365745F547261636B44617461006765745F696E416D6F756E74007365745F696E416D6F756E74006765745F43757272656E6379436F6465007365745F43757272656E6379436F6465006765745F45787069726174696F6E44617465007365745F45787069726174696F6E44617465006765745F526F7574696E674E756D626572007365745F526F7574696E674E756D626572006765745F4163636F756E744E756D626572007365745F4163636F756E744E756D626572006765745F43617264456469746564007365745F43617264456469746564006765745F436F6D6D656E7473007365745F436F6D6D656E7473006765745F436865636B44617465007365745F436865636B44617465006765745F5472616E73616374696F6E54797065007365745F5472616E73616374696F6E54797065006765745F4163636F756E74486F6C6465724E616D65007365745F4163636F756E74486F6C6465724E616D65004F7065726174696F6E4964005479706500446F63756D656E744E756D6265720042616E6B4E616D6500486F6C6465724E616D650042616E6B436F756E74727900696E416D6F756E740043757272656E6379436F64650045787069726174696F6E4461746500526F7574696E674E756D626572004163636F756E744E756D626572004361726445646974656400436F6D6D656E747300436865636B44617465004163636F756E74486F6C6465724E616D6500414C4C0047524F55500050524F56005A4F4E4500415245410042414E4B005445524D0051554552590050524F4D4F54494F4E5F52455354524943544544005041545445524E530050524F4752455353495645004C43445F4D4553534147450050524F4D4F54494F4E5F504C415945440047414D455F47415445574159004255434B4554535F4D554C5449504C4945520044656661756C7400596573004E6F00426C6F636B4F6E456E74657200426C6F636B4F6E4C6561766500426F74680043616C63756C61746564416E645265776172640050656E64696E67004D616E75616C004D616E75616C43616C63756C61746564004E65757472616C004368696E65736500437A65636800456E676C697368005370616E697368004974616C69616E004B6F7265616E005275737369616E00434153484C455353005449544F00574153530047414D494E4748414C4C004D49434F3200434F4E5345435554495645004E4F5F434F4E53454355544956455F574954485F4F52444552004E4F5F4F52444552004E4F5F4F524445525F574954485F52455045544954494F4E00416E6F6E796D6F7573576974684361726400437573746F6D697A656400416E6F6E796D6F7573576974686F7574436172640042696C6C00436F696E004F7468657273004368697073526564696D69626C650043686970734E6F526564696D69626C65004368697073436F6C6F7200466F726569676E004E6174696F6E616C004E6F7468696E67004E657743757272656E6379004E6577536974654E6174696F6E616C43757272656E6379004E657753697465466F726569676E43757272656E6379006D5F636F6E636570745F6964006D5F7175616E74697479006D5F616D6F756E74006D5F69736F5F636F6465006D5F63757272656E63795F74797065006765745F5175616E74697479007365745F5175616E74697479006765745F416D6F756E74007365745F416D6F756E74006765745F49736F436F6465007365745F49736F436F6465006765745F43757272656E637954797065007365745F43757272656E637954797065006765745F436F6E636570744964007365745F436F6E636570744964006765745F436F6D6D656E74007365745F436F6D6D656E74005175616E7469747900416D6F756E740049736F436F646500436F6E63657074496400436F6D6D656E740057494E5053410053656E7400526563656976656400426F7800486F7070657200496E004F75740050756E746F7343616E6A650050756E746F734E6976656C004E5200524500436F6D703143616E6A6500436F6D70324E5200436F6D703352450048415050595F42495254484441595F544F4441590048415050595F42495254484441595F494E434F4D4D494E470044554D4D590042414E4F525445004100420043004400526564656D7074696F6E506F696E74730052616E6B696E674C6576656C506F696E7473004372656469745F4E52004372656469745F524500436F6D70315F526564656D7074696F6E506F696E747300436F6D70325F4372656469745F4E5200436F6D70335F4372656469745F52450052616E6B696E674C6576656C506F696E74735F47656E6572617465640052616E6B696E674C6576656C506F696E74735F44697363726574696F6E616C005072697A655F31005072697A655F32005072697A655F33005072697A655F34005072697A655F350048414E445041595F5649525455414C5F504C41595F53455353494F4E5F4944005472785F4163636F756E7450726F6D6F74696F6E73416374696F6E005472785F57637053746172744361726453657373696F6E005472785F436F6D6D6F6E53746172744361726453657373696F6E005472785F4F6E53746172744361726453657373696F6E005472785F47657445786368616E6765005472785F4C696E6B4163636F756E745465726D696E616C005472785F556E6C696E6B4163636F756E745465726D696E616C005472785F476574506C617953657373696F6E4964005472785F496E73657274506C617953657373696F6E005472785F55706461746550734163636F756E744964005472785F48616E64706179496E73657274506C617953657373696F6E005472785F557064617465506C617953657373696F6E506C61796564576F6E005472785F4F6E456E644361726453657373696F6E005472785F557064617465466F756E64416E6452656D61696E696E67496E45676D0049734163636F756E74416E6F6E796D6F75734F725669727475616C005472785F506C617953657373696F6E436C6F7365005472785F41646442616C616E636573546F4163636F756E740054696D655370616E005472785F506C617953657373696F6E546F50656E64696E67005472785F536974654A61636B706F74436F6E747269627574696F6E005472785F506C617953657373696F6E5365744D6574657273005472785F506C617953657373696F6E5449544F5365744D6574657273005472785F526573657443616E63656C6C61626C654F7065726174696F6E005472785F52657365744163636F756E74496E53657373696F6E005472785F556E6C696E6B50726F6D6F74696F6E73496E53657373696F6E005472785F50726F6D6F5265636F6D7075746557697468686F6C644F6E5265636861726765005472785F50726F6D6F4D61726B4173457868617573746564005472785F5449544F5F55706461746550726F6D6F4E52436F7374005472785F5570646174654163636F756E7450726F6D6F74696F6E436F7374005472785F476574506C617961626C6542616C616E6365005472785F47657447616D65476174657761795265736572766564456E61626C6564005472785F557064617465506C61796564416E64576F6E005472785F47657443726564697473506C61794D6F64650053797374656D2E5465787400537472696E674275696C646572005472785F47657444656C7461506C61796564576F6E005449544F5F47657444656C7461506C61796564576F6E005472785F5570646174654163636F756E74506F696E7473005472785F5570646174654163636F756E7442616C616E6365005472785F5365744163746976697479005472785F5365744163636F756E74426C6F636B6564005472785F43616E63656C53746172744361726453657373696F6E005472785F5570646174654163636F756E74496E53657373696F6E005472785F42616C616E6365546F506C617953657373696F6E00444550524543415445445F5472785F436F6D70757465576F6E506F696E7473005472785F47657445787465726E616C4C6F79616C747950726F6772616D4D6F646500436865636B4269727468646179416C61726D005265676973746572005472785F5449544F5F5570646174654163636F756E7450726F6D6F74696F6E42616C616E63650044425F5570646174655465726D696E616C4163636F756E7450726F6D6F74696F6E005472785F5449544F5F5472616E7366657246726F6D4163636F756E745F43616E63656C53746172744361726453657373696F6E005472785F47616D65476174657761795F526573657276655F43726564697400576F6E50726F6D6F4E6F7452656465656D61626C6500576F6E52656465656D61626C6500506C617950726F6D6F4E6F7452656465656D61626C6542616C616E6365005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F4E52005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F524500506C617950726F6D6F5072697A654F6E6C7900506C617952656465656D61626C65005472785F506C617953657373696F6E4368616E6765537461747573005472785F506C617953657373696F6E53657446696E616C42616C616E6365005472785F506C617953657373696F6E4D61726B41734162616E646F6E6564005472785F52656C65617365496E616374697665506C617953657373696F6E005472785F476574506C617953657373696F6E5449544F53657373696F6E4D6574657273005472785F4163636F756E7450726F6D6F74696F6E436F7374005472785F496E736572744167726F757047616D65506C617953657373696F6E00496E73657274576370436F6D6D616E64005472785F506C617953657373696F6E546F526576697365005265616447656E6572616C506172616D73004F6B004572726F72004163636F756E74556E6B6E6F776E004163636F756E74496E53657373696F6E005465726D696E616C556E6B6E6F776E005465726D696E616C426C6F636B6564005061727469616C00416C6C00496E636C7564655265736572766564004D69636F32005472616E73616374696F6E49640049735469746F4D6F6465005472616E736665724D6F6465004163636F756E7442616C616E6365546F5472616E73666572005472616E73666572576974684275636B65747300506C617953657373696F6E49640049734E6577506C617953657373696F6E00496E6942616C616E636500506C617961626C6542616C616E63650046696E42616C616E636500546F74616C546F474D42616C616E636500506F696E747300537461747573436F6465004572726F724D7367005365744572726F72005761726E696E670053657453756363657373006765745F4D657373616765004D65737361676500436173686965724E616D650046696E616C42616C616E636553656C656374656442795573657241667465724D69736D617463680042616C616E636546726F6D474D004861734D6574657273004D65746572730049735449544F005669727475616C4163636F756E744964005469746F53657373696F6E4D657465727300436C6F736553657373696F6E436F6D6D656E740049734D69636F320042616C616E6365546F4163636F756E740044656C746142616C616E6365466F756E64496E53657373696F6E0052656D61696E696E67496E45676D00466F756E64496E45676D00506C61796564436F756E7400506C61796564416D6F756E7400576F6E436F756E7400576F6E416D6F756E74004A61636B706F74416D6F756E740042696C6C496E0048616E6470617973416D6F756E74005469636B6574496E4361736861626C65005469636B6574496E50726F6D6F4E72005469636B6574496E50726F6D6F5265005469636B65744F75744361736861626C65005469636B65744F757450726F6D6F4E72006765745F52656465656D61626C6543617368496E006765745F50726F6D6F526543617368496E006765745F50726F6D6F4E7243617368496E006765745F546F74616C43617368496E006765745F52656465656D61626C65436173684F7574006765745F50726F6D6F4E72436173684F7574006765745F546F74616C436173684F75740052656465656D61626C6543617368496E0050726F6D6F526543617368496E0050726F6D6F4E7243617368496E00546F74616C43617368496E0052656465656D61626C65436173684F75740050726F6D6F4E72436173684F757400546F74616C436173684F75740043617368496E436F696E730043617368496E42696C6C730043617368496E546F74616C004572726F72526574727900466174616C4572726F72004E6F744F70656E65640042616C616E63654D69736D6174636800496E76616C6964506C617953657373696F6E00506C617953657373696F6E46696E616C42616C616E6365004163636F756E7450726F6D6F74696F6E0044656C7461506C617965640044656C7461576F6E0044656C7461506C6179656452650044656C7461506C617965644E720044656C7461576F6E52650044656C7461576F6E4E720042616C616E636500506C6179656400576F6E0042616C616E636546726F6D474D546F416464004E756D506C61796564004E756D576F6E0048616E647061797343656E74730052656465656D61626C650050726F6D6F52656465656D61626C650050726F6D6F4E6F7452656465656D61626C65005265736572766564005265736572766564446973636F756E746564004D6F64655265736572766564004D6F646552657365727665644368616E676564004275636B65744E52437265646974004275636B6574524543726564697400546F537472696E67006765745F546F74616C52656465656D61626C65006765745F546F74616C4E6F7452656465656D61626C65006765745F546F74616C42616C616E6365006765745F5A65726F006765745F42616C616E636545474D006F705F4164646974696F6E006F705F5375627472616374696F6E006F705F457175616C697479006F705F496E657175616C69747900457175616C730047657448617368436F6465004E656761746500436C6F6E6500546F74616C52656465656D61626C6500546F74616C4E6F7452656465656D61626C6500546F74616C42616C616E6365005A65726F0042616C616E636545474D00426C6F636B656400426C6F636B526561736F6E00486F6C6465724C6576656C00486F6C64657247656E646572004163547970650043616E63656C6C61626C654F7065726174696F6E49640043757272656E74506C617953657373696F6E49640043757272656E745465726D696E616C49640043757272656E74506C617953657373696F6E5374617475730043757272656E74506C617953657373696F6E42616C616E63654D69736D617463680043757272656E74506C617953657373696F6E496E697469616C42616C616E63650043616E63656C6C61626C6554727849640043616E63656C6C61626C6542616C616E63650046696E616C42616C616E63650046696E616C506F696E7473004572726F724D6573736167650043616E63656C4279506C61796572004E6F7452656465656D61626C65546F52656465656D61626C6500444F5F4E4F5448494E470053454E445F4249525448444154455F414C41524D0053454E445F4249525448444154455F49535F4E454152005245504F52545F52455345525645445F4352454449540043414E43454C5F52455345525645445F435245444954004D4F444946595F52455345525645445F435245444954005245504F52545F4745545F43524544495400524551554553545F545950455F524553504F4E53455F4F4B00524551554553545F545950455F524553504F4E53455F4552524F5200524551554553545F545950455F524553504F4E53455F4E4F545F454E4F5547485F4352454449540047414D455F474154455741595F52455345525645445F4D4F44455F574954484F55545F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F574954485F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F4D495845440047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F4E4F4E450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F434F4C4C4543545F5052495A450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F574954484F55545F50494E00537461727453657373696F6E005265537461727453657373696F6E00456E6453657373696F6E00537461727453657373696F6E43616E63656C6C6564006D5F70726F76696465720044617461536574006D5F6473006765745F4C69737454797065007365745F4C69737454797065004175646974537472696E670041646450726F76696465720052656D6F766550726F766964657200436F6E7461696E7300546F586D6C0046726F6D586D6C00496E6974005365656B50726F766964657200586D6C57726974654D6F64650044617461536574546F586D6C00586D6C526561644D6F6465004461746153657446726F6D586D6C004C69737454797065004C6973746564004E6F744C6973746564005472785F49735433535465726D696E616C005472785F4765745465726D696E616C496E666F005472785F496E7365727450656E64696E675465726D696E616C005472785F4765745465726D696E616C496E666F427945787465726E616C4964004765745465726D696E616C446973706C61794E616D6500534153466C6167416374697665640053716C436F6D6D616E640047657447616D654D6574657273557064617465436F6D6D616E6400436C6F7365416C6C4275744E657765724F70656E4361736869657253657373696F6E73005472785F4765745465726D696E616C54797065004E6F7452656465656D61626C654C6173740050726F6D6F74696F6E616C734669727374005361734D616368696E650050726F764964005465726D696E616C54797065004E616D650050726F76696465724E616D650053746174757300506C61794D6F64650043757272656E744163636F756E74496400534153466C616773005076506F696E74734D756C7469706C696572005076536974654A61636B706F740050764F6E6C7952656465656D61626C6500534153466C61674163746976617465640053797374656D4964004D616368696E65496400416C6C6F774361736861626C655469636B65744F757400416C6C6F7750726F6D6F74696F6E616C5469636B65744F757400416C6C6F775469636B6574496E0045787069726174696F6E5469636B6574734361736861626C650045787069726174696F6E5469636B65747350726F6D6F74696F6E616C005469746F5469636B65744C6F63616C697A6174696F6E005469746F5469636B6574416464726573735F31005469746F5469636B6574416464726573735F32005469746F5469636B65745469746C6552657374726963746564005469746F5469636B65745469746C654465626974005469636B6574734D61784372656174696F6E416D6F756E740045787465726E616C496400466C6F6F72496400486F7374496400417265614E616D6500546F45787465726E616C00546F496E7465726E616C00434845434B53554D5F4D4F44554C4500545241434B444154415F43525950544F5F4B455900545241434B444154415F43525950544F5F495600547261636B44617461546F496E7465726E616C00547261636B44617461546F45787465726E616C004D616B65496E7465726E616C547261636B446174610053706C6974496E7465726E616C547261636B446174610045787465726E616C547261636B4461746142656C6F6E67546F5369746500436F6E76657274547261636B4461746100574350007472616E73666F726D5F6B6579006B65795F6C656E67746800646174615F7065726D75746174696F6E00696E6465783100696E6465783200646973706F7365006765745F43616E52657573655472616E73666F726D006765745F43616E5472616E73666F726D4D756C7469706C65426C6F636B73006765745F496E707574426C6F636B53697A65006765745F4F7574707574426C6F636B53697A65005472616E73666F726D426C6F636B005472616E73666F726D46696E616C426C6F636B00446973706F73650043616E52657573655472616E73666F726D0043616E5472616E73666F726D4D756C7469706C65426C6F636B7300496E707574426C6F636B53697A65004F7574707574426C6F636B53697A65006765745F426C6F636B53697A65007365745F426C6F636B53697A65006765745F466565646261636B53697A65007365745F466565646261636B53697A65006765745F4956007365745F4956004B657953697A6573006765745F4C6567616C426C6F636B53697A6573006765745F4C6567616C4B657953697A6573004369706865724D6F6465006765745F4D6F6465007365745F4D6F64650050616464696E674D6F6465006765745F50616464696E67007365745F50616464696E670047656E657261746549560047656E65726174654B65790043726561746500426C6F636B53697A6500466565646261636B53697A65004956004C6567616C426C6F636B53697A6573004C6567616C4B657953697A6573004D6F64650050616464696E670069735F646973706F73656400437265617465446563727970746F7200437265617465456E63727970746F7200617263666F75726D616E61676564006765745F4B6579007365745F4B6579006765745F4B657953697A65007365745F4B657953697A65004B6579004B657953697A65005472785F3347535F53746172744361726453657373696F6E005472785F3347535F5570646174654361726453657373696F6E005472785F3347535F456E644361726453657373696F6E005472785F3347535F4163636F756E74537461747573005472785F3347535F53656E644576656E740056656E646F7249640053657269616C4E756D626572004D616368696E654E756D626572005472780053657269616C4E756D62657244756D6D79004D616368696E654E756D62657244756D6D790045787465726E616C547261636B4461746144756D6D7900506C617953657373696F6E4D6574657273004576656E744964004163636F756E740053797374656D2E52756E74696D652E496E7465726F705365727669636573004F75744174747269627574650053746174757354657874004572726F72546578740053716C54727800496E697469616C42616C616E636500537562416D6F756E7400416464416D6F756E740044657461696C7300526561736F6E7300496E6465780076616C75650043617264547261636B44617461004D6F7644657461696C730050726F6D6F4E616D65004973537761700043757272656E637944656E6F6D696E6174696F6E004D6F76656D656E7449640047616D696E675461626C6553657373696F6E496400417578416D6F756E74004375727245786368616E676554797065004368697049640045786368616E6765526573756C74004E6174696F6E616C43757272656E6379004D6F76656D656E740052656C61746564496400496E697469616C416D6F756E740046696E616C416D6F756E74004973436F756E745200416374696F6E00416666656374656442616C616E6365004E756D50726F6D6F7300496E707574005449544F4D6F64650042616C616E6365546F5472616E73666572004F75747075740046726F6D49534F00546F49534F0045786368616E67656400456E737572654E6F74496E53657373696F6E00547970654461746100506C617953657373696F6E496E697469616C42616C616E63650048616E64706179416D6F756E740044656C746100466F756E640052656D61696E696E67004973416E6F6E796D6F75734F725669727475616C00506172616D730042616C616E6365546F41646400506C61795F53657373696F6E5F6475726174696F6E0050726F6D6F4E725469636B6574496E0050726F6D6F52655469636B6574496E0052655469636B6574496E0052655469636B65744F75740050726F6D6F4E725469636B65744F75740043617368496E416D6F756E7400496E53657373696F6E506C6179656400496E53657373696F6E576F6E00496E53657373696F6E42616C616E6365005469636B65744F757452650043617368496E52650043757272656E7400506C617961626C65005449544F49735472616E736665720041637469766550726F6D6F7300416464496E53657373696F6E00506F696E7473546F41646400506F696E7473546F41646446696E616C42616C616E636500416C6C6F774E65676174697665506F696E747300546F4164640046696E616C00496E636C7564654275636B65740043616E63656C6C61626C6500436C6F736564506C617953657373696F6E496400546F74616C46726F6D474D42616C616E63650053657373696F6E42616C616E636500497346697273745472616E736665720049676E6F7265506C617961626C65496E43617368496E00496E53657373696F6E52655370656E7400496E53657373696F6E5265506C6179656400576F6E506F696E747300506F696E7448617665546F42654177617264656400536F75726365436F646500536F75726365496400536F757263654E616D6500416C61726D436F6465004465736372697074696F6E0053657665726974790050726F6D6F73546F436F6E73756D650050726F6D6F49640042616C616E636543616E63656C6C65640052657175657374416D6F756E74005265717565737454797065005265736572766564496E697469616C42616C616E63650053696D756C617465576F6E4C6F636B00437265646974547970650044657369726564537461747573004F6C64537461747573004E657753746174757300506C617954696D655370616E00456665637469766546696E616C42616C616E6365005265706F7274656446696E616C42616C616E63650053797374656D436173686965724E616D6500436F6D7075746564506F696E74730047726F7570005375626A6563740056616C75650042616C310042616C32006F626A004E65676174655265736572766564004578636C756465640050726F766964657200586D6C0057726974654D6F646500526561644D6F646500536F75726365004F75745465726D696E616C496E666F005465726D696E616C45787465726E616C4964004D61736B4E616D6500446973706C61794E616D650043757272656E74466C61677300466C616700496E7465726E616C547261636B44617461004361726454797065005369746549640053657175656E6365005265636569766564547261636B44617461004442547261636B4461746100496E70757442756666657200496E7075744F666673657400496E707574436F756E74004F7574707574427566666572004F75747075744F666673657400416C674E616D65005267624B657900526762495600446973706F73696E67005374617475734572726F720053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C65417474726962757465004775696441747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C6974794174747269627574650053514C427573696E6573734C6F67696300526F6C6C6261636B00496E74363400537472696E6700436F6E63617400417070656E644C696E650053716C436F6E6E656374696F6E006765745F436F6E6E656374696F6E0053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C506172616D657465720053716C44625479706500496E7433320053797374656D2E446174612E436F6D6D6F6E004462506172616D65746572007365745F56616C7565004462436F6D6D616E6400457865637574654E6F6E517565727900466F726D6174006F705F4C6573735468616E004D617468004D6178005472795061727365006F705F4469766973696F6E00416273006F705F477265617465725468616E004D696E00446F75626C6500457865637574655363616C61720044424E756C6C0053716C44617461526561646572004578656375746552656164657200446244617461526561646572005265616400476574496E74363400476574496E7433320049734E756C6C4F72456D7074790044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300476574547970650044617461436F6C756D6E007365745F4175746F496E6372656D656E74007365745F4175746F496E6372656D656E7453656564007365745F4175746F496E6372656D656E7453746570007365745F416C6C6F7744424E756C6C006765745F4974656D007365745F5072696D6172794B6579004E6577526F77007365745F4974656D0044617461526F77436F6C6C656374696F6E006765745F526F777300457863657074696F6E00496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E740053797374656D2E436F6C6C656374696F6E730049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E74004D6F76654E657874007365745F536F75726365436F6C756D6E00506172616D65746572446972656374696F6E007365745F446972656374696F6E00557064617465526F77536F75726365007365745F55706461746564526F77536F757263650053716C4461746141646170746572007365745F496E73657274436F6D6D616E640044624461746141646170746572004D656D62657277697365436C6F6E65007365745F44656661756C7456616C7565006F705F477265617465725468616E4F72457175616C006765745F48617356616C7565006765745F4974656D41727261790049734E756C6C006F705F556E6172794E65676174696F6E006765745F56616C756500476574446563696D616C007365745F4C656E677468007365745F557064617465426174636853697A650053716C457863657074696F6E00466C6167734174747269627574650053657269616C697A61626C654174747269627574650046696C6C00476574537472696E6700497344424E756C6C00456D707479006F705F4C6573735468616E4F72457175616C006F705F4D756C7469706C79005472756E6361746500426F6F6C65616E004765744F7264696E616C00476574426F6F6C65616E006765745F546F74616C4D696E75746573004944617461526561646572004C6F6164004D6964706F696E74526F756E64696E6700526F756E64004461746156696577526F7753746174650053656C656374007365745F557064617465436F6D6D616E64006765745F557064617465436F6D6D616E64004461746141646170746572007365745F436F6E74696E75655570646174654F6E4572726F72006765745F4861734572726F7273006765745F526F774572726F72006765745F436F6D6D616E6454657874004462506172616D65746572436F6C6C656374696F6E006765745F506172616D657465724E616D65005472696D005061644C656674005265706C61636500436F6E7665727400546F446563696D616C004E657874526573756C7400436F6D6D616E6454797065007365745F436F6D6D616E64547970650055496E743332004765744461746554696D6500537562747261637400496E743136006F705F496D706C6963697400417070656E64466F726D6174006F705F4578706C6963697400417070656E6400496E73657274006765745F4C656E67746800546F55707065720044656C657465004163636570744368616E67657300446174615461626C65436F6C6C656374696F6E006765745F5461626C657300537472696E67436F6D70617269736F6E0053797374656D2E494F00537472696E675772697465720054657874577269746572005772697465586D6C00537472696E6752656164657200546578745265616465720052656164586D6C00476574496E743136004D6963726F736F66742E53716C5365727665722E5365727665720053716C46756E6374696F6E41747472696275746500427974650055496E74363400436F6D7061726500506172736500426974436F6E766572746572004765744279746573004D656D6F727953747265616D0043727970746F53747265616D0053747265616D0043727970746F53747265616D4D6F646500546F55496E74363400537562737472696E6700577269746500466C75736846696E616C426C6F636B00546F417272617900546F496E74333200546F496E743634002E6363746F72003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B31324430414339422D374432382D343144442D414136362D3738383742454439413844377D00436F6D70696C657247656E6572617465644174747269627574650056616C756554797065005F5F5374617469634172726179496E69745479706553697A653D31360024246D6574686F643078363030303138362D310052756E74696D6548656C706572730041727261790052756E74696D654669656C6448616E646C6500496E697469616C697A654172726179006765745F46756C6C4E616D65004F626A656374446973706F736564457863657074696F6E00417267756D656E744E756C6C457863657074696F6E00417267756D656E744F75744F6652616E6765457863657074696F6E00474300537570707265737346696E616C697A65004B657953697A6556616C75650043727970746F67726170686963457863657074696F6E004E6F74537570706F72746564457863657074696F6E00524E4743727970746F5365727669636550726F76696465720052616E646F6D4E756D62657247656E657261746F720053716C50726F636564757265417474726962757465004462436F6E6E656374696F6E004F70656E00426567696E5472616E73616374696F6E0044625472616E73616374696F6E00436F6D6D697400000001001554003300470053002E0053007400610072007400000720002D002000011754003300470053002E00550070006400610074006500001154003300470053002E0045006E00640000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002100002554003300470053002E004100630063006F0075006E007400530074006100740075007300001D54003300470053002E00530065006E0064004500760065006E007400002155006E006B006E006F0077006E0020004500760065006E007400490064002E000059200049004E005300450052005400200049004E0054004F0020004500560045004E0054005F0048004900530054004F0052005900200028002000450048005F005400450052004D0049004E0041004C005F0049004400200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F00530045005300530049004F004E005F0049004400200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004400410054004500540049004D004500200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004500560045004E0054005F0054005900500045002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F0043004F0044004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F00440041005400410029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070005400650072006D0069006E0061006C00490064002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006C0061007900530065007300730069006F006E00490064002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004500760065006E00740054007900700065002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0043006F00640065002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006D006F0075006E007400290020000019400070005400650072006D0069006E0061006C0049006400001F4000700050006C0061007900530065007300730069006F006E00490064000017400070004500760065006E0074005400790070006500001F400070004F007000650072006100740069006F006E0043006F006400650000114000700041006D006F0075006E007400003D4500720072006F00720020006F006E00200069006E00730065007200740020006500760065006E0074005F0068006900730074006F00720079002100007F49006E00760061006C0069006400200050006C0061007900530065007300730069006F006E00490064002E0020005200650070006F0072007400650064003A0020007B0030007D002C002000430075007200720065006E0074003A0020007B0031007D002C0020005300740061007400750073003A0020007B0032007D00007D4E00650067006100740069007600650020004D00650074006500720073002E002000460069006E0061006C003A0020007B0030007D003B00200050006C0061007900650064003A0020007B0031007D002C00200023007B0032007D003B00200057006F006E003A0020007B0033007D002C00200023007B0034007D00001949006E00740065007200660061006300650033004700530000314D006100780069006D0075006D00420061006C0061006E00630065004500720072006F007200430065006E0074007300004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002100004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002100001D40007000470061006D00650042006100730065004E0061006D0065000011470041004D0045005F00330047005300001D40007000440065006E006F006D0069006E006100740069006F006E00001F40007000570063007000530065007100750065006E006300650049006400002540007000440065006C007400610050006C00610079006500640043006F0075006E007400002740007000440065006C007400610050006C00610079006500640041006D006F0075006E007400001F40007000440065006C007400610057006F006E0043006F0075006E007400002140007000440065006C007400610057006F006E0041006D006F0075006E007400002940007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074000031470061006D0065004D006500740065007200730055007000640061007400650020006600610069006C006500640021000073420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005200650070006F0072007400650064003D007B0030007D002C002000430061006C00630075006C0061007400650064003D007B0031007D002C002000460069006E0061006C003D007B0032007D00000F5300750063006300650073007300002D49006E00760061006C006900640020004100630063006F0075006E00740020004E0075006D0062006500720000254100630063006F0075006E007400200049006E002000530065007300730069006F006E00003749006E00760061006C006900640020004D0061006300680069006E006500200049006E0066006F0072006D006100740069006F006E00001B4100630063006500730073002000440065006E00690065006400003349006E00760061006C00690064002000530065007300730069006F006E0020004900440020004E0075006D006200650072000051460069006E0061006C002000620061006C0061006E0063006500200064006F006500730020006E006F00740020006D0061007400630068002000760061006C00690064006100740069006F006E002E00004D2000530045004C004500430054002000200054004F00500020003100200054003300470053005F00560045004E0044004F0052005F0049004400200020002000200020002000200020002000004D200020002000460052004F004D00200020005400450052004D0049004E0041004C0053005F003300470053002000200020002000200020002000200020002000200020002000200020002000004D20002000570048004500520045002000200054003300470053005F00560045004E0044004F0052005F004900440020003D00200040007000560065006E0064006F0072004900640020002000001540007000560065006E0064006F00720049006400005D2000530045004C0045004300540020002000500053005F004100430043004F0055004E0054005F00490044002C002000500053005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000005D200020002000460052004F004D002000200050004C00410059005F00530045005300530049004F004E005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D200020005700480045005200450020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900730065007300730069006F006E00490064002000001F4000700050006C0061007900730065007300730069006F006E0049006400001D41004D005F004D004F00560045004D0045004E0054005F00490044000019530079007300740065006D002E0049006E00740036003400001F41004D005F004F005000450052004100540049004F004E005F0049004400001B41004D005F004100430043004F0055004E0054005F0049004400000F41004D005F0054005900500045000019530079007300740065006D002E0049006E00740033003200002541004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001D530079007300740065006D002E0044006500630069006D0061006C00001B41004D005F005300550042005F0041004D004F0055004E005400001B41004D005F004100440044005F0041004D004F0055004E005400002141004D005F00460049004E0041004C005F00420041004C0041004E0043004500001541004D005F00440045005400410049004C005300001B530079007300740065006D002E0053007400720069006E006700001541004D005F0052004500410053004F004E005300001B41004D005F0054005200410043004B005F004400410054004100001D41004D005F005400450052004D0049004E0041004C005F0049004400002141004D005F005400450052004D0049004E0041004C005F004E0041004D004500002541004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400000340000080BF2000530045004C004500430054002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640020000017400070004100630063006F0075006E0074004900640000134D0075006C00740069005300690074006500001149007300430065006E00740065007200004F20004400450043004C0041005200450020002000200040005F0074007200610063006B00640061007400610020004100530020004E0056004100520043004800410052002800350030002900200000392000530045005400200020002000200020002000200040005F0074007200610063006B00640061007400610020003D002000270027002000012F2000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C0020000080E1200020002000530045004C004500430054002000200040005F0074007200610063006B00640061007400610020003D002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000002B200049004600200040005F0074007200610063006B00640061007400610020003D00200027002700200001492000200020005300450054002000200020002000200040005F0074007200610063006B00640061007400610020003D00200040007000430061007200640044006100740061002000004D4400450043004C0041005200450020002000200040007000530065007100750065006E006300650031003200560061006C0075006500200041005300200042004900470049004E0054002000000320000027550050004400410054004500200020002000530045005100550045004E004300450053002000005B2000200020005300450054002000200020005300450051005F004E004500580054005F00560041004C005500450020003D0020005300450051005F004E004500580054005F00560041004C005500450020002B00200031002000004B2000570048004500520045002000200020005300450051005F00490044002000200020002000200020002000200020003D0020004000700053006500710049006400310032003B0020000080B3530045004C0045004300540020002000200040007000530065007100750065006E006300650031003200560061006C007500650020003D0020005300450051005F004E004500580054005F00560041004C005500450020002D00200031002000460052004F004D002000530045005100550045004E0043004500530020005700480045005200450020005300450051005F004900440020003D0020004000700053006500710049006400310032003B002000016149004E005300450052005400200049004E0054004F002000200020004100430043004F0055004E0054005F004D004F00560045004D0045004E00540053002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002800200041004D005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00540059005000450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005300550042005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004100440044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004400410054004500540049004D0045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004F005000450052004100540049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00440045005400410049004C00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0052004500410053004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0054005200410043004B005F004400410054004100200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200041004D005F004D004F00560045004D0045004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000000529002000006120002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004300610073006800690065007200490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D00650020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080B52000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000490053004E0055004C004C0028004000700050006C0061007900530065007300730069006F006E00490064002C003000290020003D002000300020005400480045004E0020004E0055004C004C00200045004C005300450020004000700050006C0061007900530065007300730069006F006E0049006400200045004E0044002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004E0061006D0065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700052006500610073006F006E007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200040007000530065007100750065006E006300650031003200560061006C0075006500200020002000200020002000200020002000200020002000200020002000004B20005300450054002000400070004D006F00760065006D0065006E0074004900640020003D00200040007000530065007100750065006E006300650031003200560061006C0075006500004920005300450054002000400070004D006F00760065006D0065006E0074004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900001B400070004F007000650072006100740069006F006E0049006400000D40007000540079007000650000214000700049006E0069007400690061006C00420061006C0061006E006300650000174000700053007500620041006D006F0075006E00740000174000700041006400640041006D006F0075006E007400001D40007000460069006E0061006C00420061006C0061006E0063006500001340007000440065007400610069006C00730000134000700052006500610073006F006E00730000154000700043006100720064004400610074006100001740007000430061007300680069006500720049006400001B4000700043006100730068006900650072004E0061006D006500001D400070005400650072006D0069006E0061006C004E0061006D00650000134000700053006500710049006400310032000019400070004D006F00760065006D0065006E00740049006400001F43004D005F004F005000450052004100540049004F004E005F0049004400001B43004D005F004100430043004F0055004E0054005F0049004400002543004D005F0043004100520044005F0054005200410043004B005F004400410054004100000F43004D005F005400590050004500002543004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001B43004D005F004100440044005F0041004D004F0055004E005400001B43004D005F005300550042005F0041004D004F0055004E005400002143004D005F00460049004E0041004C005F00420041004C0041004E0043004500002943004D005F00420041004C0041004E00430045005F0049004E004300520045004D0045004E005400001543004D005F00440045005400410049004C005300002943004D005F00430055005200520045004E00430059005F00490053004F005F0043004F0044004500001B43004D005F004100550058005F0041004D004F0055004E005400003143004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E00003543004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F0049004400001543004D005F0043004800490050005F0049004400002B43004D005F0043004100470045005F00430055005200520045004E00430059005F005400590050004500001B430055005200520045004E00430059005F005400590050004500001B43004D005F00520045004C0041005400450044005F0049004400000F43004D005F004400410054004500001F530079007300740065006D002E004400610074006500540069006D00650000392000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730020003D002000310020002000000F200042004500470049004E002000003D20002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000007F200020002000200020002000530045005400200020002000430053005F00420041004C0041004E0043004500200020002000200020003D002000430053005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E006300650049006E006300720065006D0065006E00740029002000003F2000200020004F0055005400500055005400200020002000440045004C0045005400450044002E00430053005F00420041004C0041004E00430045002000004320002000200020002000200020002000200020002C00200049004E005300450052005400450044002E00430053005F00420041004C0041004E0043004500200020000057200020002000200057004800450052004500200020002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000005920002000200020002000200041004E004400200020002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E0020000067200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E00670020002900200000132000200020002000200045004E0044002000004B2000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E006300790020003D002000310020000011200042004500470049004E00200020000037200020002000490046002000450058004900530054005300200028002000530045004C00450043005400200020002000310020002000006B20002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000006B2000200020002000200020002000200020002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F004900440020003D00200040007000530065007300730069006F006E00490064002000006720002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F00440045002000200020003D00200040007000490073006F0043006F00640065002000006320002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00540059005000450020002000200020002000200020003D00200040007000540079007000650029002000005B200020002000200020002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E0043005900200000809B20002000200020002000200020002000200020005300450054002000200020004300530043005F00420041004C0041004E0043004500200020002000200020003D0020004300530043005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E00740029002000004920002000200020002000200020004F0055005400500055005400200020002000440045004C0045005400450044002E004300530043005F00420041004C0041004E00430045002000004B200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E00430045002000006B20002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D00200040007000530065007300730069006F006E004900640020000067200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F0044004500200020002000200020002000200020002000200020003D00200040007000490073006F0043006F006400650020000061200020002000200020002000200020002000200041004E0044002000200020004300530043005F0054005900500045002000200020002000200020002000200020002000200020002000200020003D00200040007000540079007000650020000033200020002000200020002000200020002000200041004E004400200020002000450058004900530054005300200028002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054002000430053005F00530045005300530049004F004E005F00490044002000006120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200043004100530048004900450052005F00530045005300530049004F004E0053002000007D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000570048004500520045002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000007F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E0067002000290020000037200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000000F2000200045004C0053004500200000612000200020002000200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000004520002000200020002000200020002000200020002000200020002000200020002000280020004300530043005F00530045005300530049004F004E005F004900440020000041200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00490053004F005F0043004F004400450020000039200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0054005900500045002000003F200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00420041004C0041004E004300450020000045200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0043004F004C004C00450043005400450044002000200000272000200020002000200020002000200020002000200020002000200020002000200029002000002F20002000200020002000200020002000200020004F005500540050005500540020002000200030002E00300020000051200020002000200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E0043004500200000292000200020002000200020002000200020002000560041004C005500450053002000200020002000003F200020002000200020002000200020002000200020002000200020002000200020002800200040007000530065007300730069006F006E00490064002000003B200020002000200020002000200020002000200020002000200020002000200020002C00200040007000490073006F0043006F006400650020000035200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005D200020002000200020002000200020002000200020002000200020002000200020002C00200040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074002000002F200020002000200020002000200020002000200020002000200020002000200020002C00200030002E0030002000000F20002000200045004E0044002000001740007000530065007300730069006F006E00490064000019400070005300740061007400750073004F00700065006E000027400070005300740061007400750073004F00700065006E00500065006E00640069006E006700001F40007000530074006100740075007300500065006E00640069006E006700001340007000490073006F0043006F0064006500002540007000420061006C0061006E006300650049006E006300720065006D0065006E007400003540007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074000025400049006E004300610073006800690065007200530065007300730069006F006E0073000039400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E0063007900001F52006500670069006F006E0061006C004F007000740069006F006E007300001F430075007200720065006E0063007900490053004F0043006F00640065000019470061006D0069006E0067005400610062006C0065007300001943006100730068006900650072002E004D006F006400650000033100004B20004400450043004C0041005200450020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F0069006400200041005300200042004900470049004E00540000808520005300450054002000200020002000200020002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020003D002000490053004E0055004C004C002800400070004300610067006500430075007200720065006E00630069006500730054007900700065002C00200030002900200000672000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C00200041004E0044002000400070004100630063006F0075006E0074004900640020004900530020004E004F00540020004E0055004C004C0020000057200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F004D004F00560045004D0045004E005400530020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002800200043004D005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005400590050004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100440044005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005300550042005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00460049004E0041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100520044005F0054005200410043004B005F0044004100540041002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004F005000450052004100540049004F004E005F00490044002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00440045005400410049004C005300200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00490053004F005F0043004F004400450020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100550058005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F004900440020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004800490050005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100470045005F00430055005200520045004E00430059005F0054005900500045002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00520045004C0041005400450044005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004400410054004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000057200020002000560041004C0055004500530020002000200020002800200040007000530065007300730069006F006E004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E0063006500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0049006400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C0073002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E006300790043006F006400650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041007500780041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006800690070004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000520065006C0061007400650064004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000690073004E0075006C006C0028004000700044006100740065002C00200047004500540044004100540045002800290029002000200020000057200053004500540020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640020003D002000530043004F00500045005F004900440045004E0054004900540059002800290020000080AD49004600200040007000540079007000650020003E003D00200040007000430061006700650046006900720073007400560061006C0075006500200041004E004400200040007000540079007000650020003C003D0020004000700043006100670065004C00610073007400560061006C0075006500200041004E004400200040007000450078007400650072006E004D006F00760065006D0065006E0074004900640020003E0020003000000B42004500470049004E00005F2000200049004E005300450052005400200049004E0054004F0020002000200043004100470045005F0043004100530048004900450052005F004D004F00560045004D0045004E0054005F00520045004C004100540049004F004E00200000212000200020002000200020002000560041004C005500450053002000280020000045200020002000200020002000200020002000200020002000200020002000200040007000450078007400650072006E004D006F00760065006D0065006E007400490064000041200020002000200020002000200020002000200020002000200020002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640000212000200020002000200020002000200020002000200020002000200029002000000745004E0044000015200042004500470049004E002000540052005900005F200049004600200040007000430075007200720065006E006300790043006F006400650020004900530020004E0055004C004C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000005F200020002000530045005400200040007000430075007200720065006E006300790043006F006400650020003D002000400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F006400650020000080B320002000200020002000200045005800450043002000640062006F002E0043006100730068006900650072004D006F00760065006D0065006E007400730048006900730074006F0072007900200040007000530065007300730069006F006E00490064002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F00690064002C0020004000700054007900700065002C0020004000700053007500620054007900700065002C0020000080C72000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700049006E0069007400690061006C00420061006C0061006E00630065002C0020004000700041006400640041006D006F0075006E0074002C0020004000700053007500620041006D006F0075006E0074002C00200040007000460069006E0061006C00420061006C0061006E00630065002C0020000080E120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000430075007200720065006E006300790043006F00640065002C0020004000700041007500780041006D006F0075006E0074002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E002C002000400070004300610067006500430075007200720065006E00630069006500730054007900700065000013200045004E00440020005400520059002000001B200042004500470049004E002000430041005400430048002000005920002000200020004400450043004C0041005200450020002000200040004500720072006F0072004D0065007300730061006700650020004E0056004100520043004800410052002800340030003000300029003B002000004520002000200020004400450043004C0041005200450020002000200040004500720072006F00720053006500760065007200690074007900200049004E0054003B002000003F20002000200020004400450043004C0041005200450020002000200040004500720072006F00720053007400610074006500200049004E0054003B00200000192000200020002000530045004C004500430054002000200000532000200020002000200020002000200040004500720072006F0072004D0065007300730061006700650020003D0020004500520052004F0052005F004D00450053005300410047004500280029002C00200000572000200020002000200020002000200040004500720072006F0072005300650076006500720069007400790020003D0020004500520052004F0052005F0053004500560045005200490054005900280029002C002000004B2000200020002000200020002000200040004500720072006F0072005300740061007400650020003D0020004500520052004F0052005F0053005400410054004500280029003B002000003D200020002000200052004100490053004500520052004F0052002000280040004500720072006F0072004D006500730073006100670065002C002000003F20002000200020002000200020002000200020002000200020002000200040004500720072006F007200530065007600650072006900740079002C002000003920002000200020002000200020002000200020002000200020002000200040004500720072006F0072005300740061007400650029003B000015200045004E004400200043004100540043004800001D40007000430075007200720065006E006300790043006F006400650000174000700041007500780041006D006F0075006E007400002D400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F006400650000114000700055007300650072004900640000154000700055007300650072004E0061006D0065000013400070005300750062005400790070006500002D40007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00002D40007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400002140007000430061006700650046006900720073007400560061006C0075006500001F4000700043006100670065004C00610073007400560061006C0075006500002540007000450078007400650072006E004D006F00760065006D0065006E007400490064000011400070004300680069007000490064000029400070004300610067006500430075007200720065006E0063006900650073005400790070006500001740007000520065006C00610074006500640049006400000D400070004400610074006500005B20002000200020002000200020002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D00200040007000530074006100740075007300410063007400690076006500200000808F200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F00540059005000450020002000200020002000200049004E002000280020004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E0052003200200029002000006B200020002000200041004E0044002000200020004100430050005F00500052004F004D004F005F00540059005000450020002000200020002000200020003C003E00200040007000500072006F006D006F0043006F0076006500720043006F00750070006F006E0020000049200020002000200041004E0044002000200020004100430050005F00420041004C0041004E004300450020002000200020002000200020002000200020003E003D002000300020000080ED4900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000D290020003E002000300020000080ED2000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000B45004C005300450020000080E9200020004900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000080E9200020002000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000043200020002000200020002000530045004C0045004300540020004300410053005400200028002D003100200041005300200042004900470049004E00540029002000014D200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C0020000063200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F0054005900500045002000200020002000200020003D0020004000700043007200650064006900740054007900700065004E00520031002000005B200020002000200041004E0044002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003E003D0020004100430050005F0057004F004E004C004F0043004B0020000055200020002000200041004E0044002000200020004100430050005F0057004F004E004C004F0043004B002000200020002000200020002000200020004900530020004E004F00540020004E0055004C004C00200000312000530045004C004500430054002000200020004100430050005F0055004E0049005100550045005F00490044002000007F200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F007300740061007400750073002900290000152000200057004800450052004500200020002000001D40007000530074006100740075007300410063007400690076006500001F4000700043007200650064006900740054007900700065004E0052003100001F4000700043007200650064006900740054007900700065004E0052003200002540007000500072006F006D006F0043006F0076006500720043006F00750070006F006E00003B20005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005B20002000200020005300450054002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D002000400070004E00650077005300740061007400750073002000003D20004F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F00420041004C0041004E00430045002000008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E007300290029002000005920002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000015200020002000200041004E00440020002000200000154000700055006E006900710075006500490064000017400070004E00650077005300740061007400750073000059530051004C005F0042005500530049004E004500530053005F004C004F00470049004300200064006F00650073006E0027007400200073007500700070006F007200740020005400490054004F0020004D006F0064006500010953006900740065000025440069007300610062006C0065004E0065007700530065007300730069006F006E007300004B530079007300740065006D0020006800610073002000640069007300610062006C006500640020006E0065007700200050006C0061007900530065007300730069006F006E0073002E000015530079007300740065006D004D006F0064006500000334000080B9530045004C004500430054002000540045005F00490053004F005F0043004F00440045002C002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000635400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E0063006500200047006500740020007400650072006D0069006E0061006C00200069006E0066006F0020006600610069006C0065006400005B2000200020002000530045004C00450043005400200020002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000005B20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020004F004E00200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000500053005F004100430043004F0055004E0054005F004900440020002000005B2000200020002000200057004800450052004500200020002000500053005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200020002000005B200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000001140007000530074006100740075007300005B5400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020004C004F0043004B0020006100630063006F0075006E00740020006600610069006C00650064002E0000395400720078005F0047006500740050006C0061007900530065007300730069006F006E004900640020006600610069006C00650064002E0000474C004300440020005400720061006E007300660065007200200069006E00200033004700530020006E006F007400200073007500700070006F00720074006500640021002100003D5400720078005F0047006500740050006C0061007900610062006C006500420061006C0061006E006300650020006600610069006C00650064002E00006F57006900740068006F00750074002000700072006F006D006F00740069006F006E007300200074006F0020007400720061006E0073006600650072003A002000420061006C0061006E00630065002000410063007400690076006500500072006F006D006F00730020003D002000002D2C002000420061006C0061006E006300650054006F005400720061006E00730066006500720020003D002000001F2C0020005400650072006D0069006E0061006C004900640020003D002000005B5400720078005F005400690074006F005500700064006100740065004100630063006F0075006E007400500072006F006D006F00740069006F006E00420061006C0061006E006300650020006600610069006C00650064002E0000435400720078005F00520065007300650074004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E0000655400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A00200000674100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E004100640064002800530074006100720074004300610072006400530065007300730069006F006E00290020006600610069006C00650064002E0000454100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E00530061007600650020006600610069006C00650064002E00002F5400720078005F005300650074004100630074006900760069007400790020006600610069006C00650064002E00001F50006C0061007900530065007300730069006F006E00490064003A00200000272C00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005B5400720078005F00420061006C0061006E006300650054006F0050006C0061007900530065007300730069006F006E0020006600610069006C00650064003A0020004E0065007700530065007300730069006F006E003A00200000232C00200050006C0061007900530065007300730069006F006E00490064003A00200000695400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005F200020002000530045004C0045004300540020002000200020002000640062006F002E004100700070006C007900450078006300680061006E006700650032002800200020004000700041006D006F0075006E0074002C00200020002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000460072006F006D00490073006F002C0020002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700054006F00490073006F0020002000200020002000005F20002000200020002000200020002000200020002000200020002000290020002000410053002000450078006300680061006E00670065006400200020002000200020002000200020002000200020002000200020002000200020002000001340007000460072006F006D00490073006F00000F4000700054006F00490073006F000013450078006300680061006E0067006500640000808155005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080EF200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020002800530045004C004500430054002000540045005F004E0041004D0045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640029002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000007F2000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C002000000720003B002000007F55005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000080812000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000008097200055005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200000809B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D004500200000809F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808120002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080832000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000073200055005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808320002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080812000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400200000432000530045004C00450043005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000004B200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020000025200020002000200020002000200020002C002000410043005F00540059005000450020000029200020002000200020002000200020002C002000500053005F0053005400410054005500530020000031200020002000200020002000200020002C002000410043005F0054005200410043004B005F0044004100540041002000003D200020002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020000043200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0057004F004E005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0050004C0041005900450044005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E002C00300029002000004B200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F004F00550054002C003000290020000057200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F00550054002C003000290020000080BD200020002000460052004F004D002000200020004100430043004F0055004E005400530020004C0045004600540020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000004D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005320004400450043004C00410052004500200040004100630074006900760069007400790044007500650054006F00430061007200640049006E00200041005300200049004E0054004500470045005200200000792000530045004C0045004300540020002000200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020000079200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000792000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C006100790027002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004100630074006900760069007400790044007500650054006F00430061007200640049006E002700200020002000200020002000200020002000200020002000014B20004900460020002800200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020004900530020004E0055004C004C00200029002000004B200042004500470049004E002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000004B200020002000530045005400200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003000200020002000200020002000004B200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200049004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E00530020002800500053005F004100430043004F0055004E0054005F004900440020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00540059005000450020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045005F004400410054004100200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E00490053004800450044002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E004500200020002000200020002900006120002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C0055004500530020002800400070004100630063006F0075006E007400490064002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500570069006E00200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500440061007400610020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000440065006600610075006C007400420061006C0061006E006300650020002000200020000080BD200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000280040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003100290020005400480045004E002000470045005400440041005400450028002900200045004C005300450020004E0055004C004C00200045004E00440020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E0065002000200020002000200020002000200029000051200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E005400490054005900280029002000001B400070005400650072006D0069006E0061006C004900640020000013400070005400790070006500570069006E00002140007000440065006600610075006C007400420061006C0061006E0063006500001540007000540079007000650044006100740061000019400070005300740061006E00640041006C006F006E0065000080BF550050004400410054004500200050004C00410059005F00530045005300530049004F004E00530020005300450054002000500053005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000570048004500520045002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640000214000700050006C0061007900530065007300730069006F006E00490064002000005549004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E005300200028002000500053005F004100430043004F0055004E0054005F004900440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F004900440020000049200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020000059200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000055200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F004F00550054002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041005400550053002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00530054004100520054004500440020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E004900530048004500440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E0045002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00500052004F004D004F0020000039200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200000512000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E0074004900640020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004900640020000047200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000003D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000300020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700057006F006E0041006D006F0075006E0074002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E006500200000808520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900200000174000700057006F006E0041006D006F0075006E007400002F55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080F5200020002000530045005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640043006F0075006E007400200020003E002000500053005F0050004C0041005900450044005F0043004F0055004E0054002900200020005400480045004E0020004000700050006C00610079006500640043006F0075006E0074002000200045004C00530045002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D0020004000700050006C00610079006500640041006D006F0075006E007400200020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0043006F0075006E007400200020002000200020003E002000500053005F0057004F004E005F0043004F0055004E0054002900200020002000200020005400480045004E0020004000700057006F006E0043006F0075006E0074002000200020002000200045004C00530045002000500053005F0057004F004E005F0043004F0055004E0054002000200020002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D0020004000700057006F006E0041006D006F0075006E007400200020002000200020000080F520002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640041006D006F0075006E00740020003E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400290020005400480045004E0020004000700050006C00610079006500640041006D006F0075006E007400200045004C00530045002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004E00440020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0041006D006F0075006E00740020002000200020003E002000500053005F0057004F004E005F0041004D004F0055004E005400290020002000200020005400480045004E0020004000700057006F006E0041006D006F0075006E007400200020002000200045004C00530045002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004E0044002000004720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D0020004E0055004C004C002000005120002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000007D4F005500540050005500540020002000200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E005400200020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E00540020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0043004F0055004E005400200020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0043004F0055004E00540020002000200020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000015D200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B4000700050006C00610079006500640043006F0075006E007400001D4000700050006C00610079006500640041006D006F0075006E00740000154000700057006F006E0043006F0075006E007400005155006E0065007800700065006300740065006400200050006C0061007900530065007300730069006F006E00490064002F005400720061006E00730061006300740069006F006E00490064003A00200000052C0020000037420061006C0061006E0063006500460072006F006D0047004D0020006300680061006E006700650064002000660072006F006D002000000B2C00200074006F0020000059530065007300730069006F006E0020004D006500740065007200730020006E006F0074002000720065006300650069007600650064002E0020005400720061006E00730061006300740069006F006E00490064003A002000001F2C00200048006100730043006F0075006E0074006500720073003A002000003355006E006500780070006500630074006500640020005400650072006D0069006E0061006C0054007900700065003A002000004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002E00002B430061006C00630075006C0061007400650050006C00610079006500640041006E00640057006F006E00003B5400720078005F00470065007400440065006C007400610050006C00610079006500640057006F006E0020006600610069006C00650064002E00003D5400720078005F0055007000640061007400650050006C00610079006500640041006E00640057006F006E0020006600610069006C00650064002E00000F53006100730048006F0073007400004D430061006C006C0069006E006700200049006E00730065007200740057006300700043006F006D006D0061006E0064003A0020005400650072006D0069006E0061006C00490064003A0020000021200050006C0061007900530065007300730069006F006E00490064003A002000004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002E0000515400720078005F0055007000640061007400650046006F0075006E00640041006E006400520065006D00610069006E0069006E00670049006E00450067006D0020006600610069006C00650064002E0000495400720078005F0050006C0061007900530065007300730069006F006E005400490054004F005300650074004D006500740065007200730020006600610069006C00650064002E0000395400720078005F0050006C0061007900530065007300730069006F006E0043006C006F007300650020006600610069006C00650064002E00002D55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053000061200020002000530045005400200020002000500053005F00520045005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070005200450046006F0075006E00640049006E00450067006D00006320002000200020002000200020002C002000500053005F004E0052005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070004E00520046006F0075006E00640049006E00450067006D002000006B20002000200020002000200020002C002000500053005F00520045005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D0020004000700052004500520065006D00610069006E0069006E00670049006E00450067006D002000006B20002000200020002000200020002C002000500053005F004E0052005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D002000400070004E005200520065006D00610069006E0069006E00670049006E00450067006D0020000065200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490044002000001F4000700050006C0061007900530065007300730069006F006E0049004400001D400070005200450046006F0075006E00640049006E00450067006D00001D400070004E00520046006F0075006E00640049006E00450067006D0000254000700052004500520065006D00610069006E0069006E00670049006E00450067006D000025400070004E005200520065006D00610069006E0069006E00670049006E00450067006D00006F2000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C002000006F200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C002000300029002000200020002000200020002000006F200020002000200020002000200020002000200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000460052004F004D002000200020004100430043004F0055004E00540053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020000031400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0000809F530045004C004500430054002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400007F5400720078005F0050006C0061007900530065007300730069006F006E0043006C006F00730065003A0020004500720072006F00720020007700680065006E00200067006500740020005600690072007400750061006C004100630063006F0075006E00740049006400200020006600610069006C00650064002E00200000375400720078005F004700650074005400650072006D0069006E0061006C0049006E0066006F0020006600610069006C00650064002E0000415400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E0000455400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00004B5400720078005F0055006E006C0069006E006B00500072006F006D006F00740069006F006E00730049006E00530065007300730069006F006E0020006600610069006C00650064002E00003355006E006500780070006500630074006500640020004D006F00760065006D0065006E00740054007900700065003A00200000415400720078005F0050006C0061007900530065007300730069006F006E005300650074004D006500740065007200730020006600610069006C00650064002E0000475400720078005F0050006C0061007900530065007300730069006F006E004300680061006E006700650053007400610074007500730020006600610069006C00650064002E00004F5400720078005F0049006E0073006500720074004100670072006F0075007000470061006D00650050006C0061007900530065007300730069006F006E0020006600610069006C00650064002E0000435400720078005F0055006E006C0069006E006B004100630063006F0075006E0074005400650072006D0069006E0061006C0020006600610069006C00650064002E00003B5400720078005F00550070006400610074006500500073004100630063006F0075006E0074004900640020006600610069006C00650064002E00004B5400720078005F0052006500730065007400430061006E00630065006C006C00610062006C0065004F007000650072006100740069006F006E0020006600610069006C00650064002E0000475400720078005F0053006900740065004A00610063006B0070006F00740043006F006E0074007200690062007500740069006F006E0020006600610069006C00650064002E0000475400720078005F005400490054004F005F00430061006C00630075006C00610074006500500072006F006D006F0043006F007300740020006600610069006C00650064002E0000415400720078005F00500072006F006D006F004D00610072006B004100730045007800680061007500730074006500640020006600610069006C00650064002E0000354100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E0041006400640028000013290020006600610069006C00650064002E0000415400720078005F0050006C0061007900530065007300730069006F006E0054006F00500065006E00640069006E00670020006600610069006C00650064002E00006F2000200049004E005300450052005400200049004E0054004F002000500045004E00440049004E0047005F0050004C00410059005F00530045005300530049004F004E0053005F0054004F005F0050004C0041005900450052005F0054005200410043004B0049004E0047002000006F2000200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F00530045005300530049004F004E005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004100430043004F0055004E0054005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0043004F0049004E005F0049004E002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F00490044002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400550052004100540049004F004E005F0054004F00540041004C005F004D0049004E0055005400450053002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F004E0055004D0050004C0041005900450044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F0054005900500045002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400410054004500540049004D004500430052004500410054004500440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000560041004C00550045005300200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F00730065007300730069006F006E005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006100630063006F0075006E0074005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0063006F0069006E005F0069006E002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F00690064002C002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E0075007400650073002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F006E0075006D0070006C0061007900650064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D0061007400630068002C002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F0074007900700065002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000670065007400640061007400650028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001F40007000700073005F00730065007300730069006F006E005F0069006400001F40007000700073005F006100630063006F0075006E0074005F0069006400001940007000700073005F0063006F0069006E005F0069006E00002140007000700073005F007400650072006D0069006E0061006C005F0069006400003740007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E007500740065007300002B40007000700073005F0070006100720061006D0073005F006E0075006D0070006C006100790065006400003940007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D006100740063006800002540007000700073005F007400650072006D0069006E0061006C005F007400790070006500004120002000550050004400410054004500200053004900540045005F004A00410043004B0050004F0054005F0050004100520041004D00450054004500520053000080F720002000200020002000530045005400200053004A0050005F0050004C004100590045004400200020003D00200053004A0050005F0050004C00410059004500440020002B002000430041005300450020005700480045004E002000280053004A0050005F004F004E004C0059005F00520045004400450045004D00410042004C00450020003D0020003100290020005400480045004E0020004000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400200045004C005300450020004000700054006F00740061006C0050006C006100790065006400200045004E004400003120002000200057004800450052004500200053004A0050005F0045004E00410042004C004500440020003D0020003100002F4000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400001B4000700054006F00740061006C0050006C0061007900650064000031200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080812000200020002000530045005400200020002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E0020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00200020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E00200020002000200020002000200020003D0020004000700054006F00740061006C0052006500430061007300680049006E0020000069200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020003D0020004000700052006500430061007300680049006E0020000073200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020003D00200040007000500072006F006D006F0052006500430061007300680049006E002000008081200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F00550054002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F005500540020002000200020002000200020003D0020004000700054006F00740061006C005200650043006100730068004F00750074002000006B200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020002000200020002000200020003D002000400070005200650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020003D00200040007000500072006F006D006F005200650043006100730068004F00750074002000007F200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0050004C004100590045004400200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0050004C0041005900450044002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650050006C0061007900650064002000006F200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0057004F004E00200020002000200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650057006F006E0020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0057004F004E002000200020002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650057006F006E0020000069200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0057006F006E0020000073200020002000200020002000200020002C002000500053005F00480041004E00440050004100590053005F0041004D004F0055004E005400200020002000200020002000200020002000200020003D00200040007000480061006E006400700061007900730041006D006F0075006E00740000752000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002B400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00001F4000700054006F00740061006C0052006500430061007300680049006E0000154000700052006500430061007300680049006E00001F40007000500072006F006D006F0052006500430061007300680049006E00002D400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740000214000700054006F00740061006C005200650043006100730068004F00750074000017400070005200650043006100730068004F0075007400002140007000500072006F006D006F005200650043006100730068004F0075007400002B400070004E006F006E00520065006400650065006D00610062006C00650050006C006100790065006400002540007000520065006400650065006D00610062006C00650050006C0061007900650064000025400070004E006F006E00520065006400650065006D00610062006C00650057006F006E00001F40007000520065006400650065006D00610062006C00650057006F006E0000154000700054006F00740061006C0057006F006E00002140007000480061006E006400700061007900730041006D006F0075006E007400006B2000200020002000530045005400200020002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F004E0072005400690063006B006500740049006E002000006B200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F00520065005400690063006B006500740049006E0020000061200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F0049004E00200020002000200020002000200020003D00200040007000520065005400690063006B006500740049006E002000006D200020002000200020002000200020002C002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F005500540020003D00200040007000500072006F006D006F004E0072005400690063006B00650074004F007500740020000063200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F004F005500540020002000200020002000200020003D00200040007000520065005400690063006B00650074004F007500740020000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020003D00200040007000430061007300680049006E0041006D006F0075006E007400200000692000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002340007000500072006F006D006F004E0072005400690063006B006500740049006E00002340007000500072006F006D006F00520065005400690063006B006500740049006E00001940007000520065005400690063006B006500740049006E00002540007000500072006F006D006F004E0072005400690063006B00650074004F0075007400001B40007000520065005400690063006B00650074004F0075007400001D40007000430061007300680049006E0041006D006F0075006E007400002720005500500044004100540045002000200020004100430043004F0055004E00540053002000005B2000200020002000530045005400200020002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020003D0020004E0055004C004C002000006D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000200020000080834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B00004F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000004F20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020000049200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020000057200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000002F20002000200049004E0054004F0020002000200040004F00750074007000750074005400610062006C0065002000006F200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000002B490046002000280020004000400052004F00570043004F0055004E00540020003D002000310020002900003B200020002000530045004C0045004300540020002A002000460052004F004D00200040004F00750074007000750074005400610062006C006500008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005B20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000061200020002000200041004E0044002000200020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E004900640020000055200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C002000001F400070005400720061006E00730061006300740069006F006E0049006400004B20002000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C002000004B200020002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D0020004E0055004C004C002000005720002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000059200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005F200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000005120002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000057200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000003D200020002000200041004E0044002000200020004100430050005F00570049005400480048004F004C00440020002000200020003E002000300020000080D120002000200020005300450054002000200020004100430050005F00570049005400480048004F004C00440020002000200020003D002000430041005300450020005700480045004E0020004100430050005F00420041004C0041004E004300450020003C0020004100430050005F00570049005400480048004F004C00440020005400480045004E0020004100430050005F00420041004C0041004E0043004500200045004C005300450020004100430050005F00570049005400480048004F004C004400200045004E0044002000004F20002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F00490044002000200020003D0020004000700055006E0069007100750065004900640020000051200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000013700055006E00690071007500650049006400008081200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E00200028004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E005200320029002000002340007000530074006100740075007300450078006800610075007300740065006400005B20002000200020005300450054002000200020004100430050005F005300540041005400550053002000200020002000200020003D00200040007000530074006100740075007300450078006800610075007300740065006400003D200020002000200041004E0044002000200020004100430050005F00420041004C0041004E0043004500200020002000200020003D00200030002000004F200020002000530045004C0045004300540020002000200054004F005000200031002000540049005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E002000002920002000200020002000460052004F004D002000200020005400490043004B0045005400530020000075200020002000200057004800450052004500200020002000540049005F00430041004E00430045004C00450044005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000006F20002000200020002000200041004E004400200020002000540049005F0054005900500045005F00490044002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005400690063006B006500740054007900700065002000005320004F005200440045005200200042005900200020002000540049005F004C004100530054005F0041004300540049004F004E005F004400410054004500540049004D0045002000440045005300430020000019400070005400690063006B00650074005400790070006500003D200020005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000008093200020002000200020005300450054002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020003D002000430041005300450020005700480045004E002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020004900530020004E0055004C004C002000006D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000200020004000700041006D006F0075006E00740020000080A12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020002B0020004000700041006D006F0075006E007400200045004E00440020000061200020002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E004900640020000029400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064000080A520004400450043004C00410052004500200040004D006F006400650052006500730065007200760065006400200041005300200049004E005400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000490053004E0055004C004C0028002800530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D0020002700470061006D006500470061007400650077006100790027002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270045006E00610062006C0065006400270020002000200020002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C002000300029002900200020000080A52000490046002000280040004D006F00640065005200650073006500720076006500640020003D00200031002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D0020002700520065007300650072007600650064002E0045006E00610062006C0065006400270020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020002800490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F0052002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D00200032002900200029002C0020003000290029000080A5200020002000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000200043004100530054002800410043005F004D004F00440045005F0052004500530045005200560045004400200041005300200049004E00540029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045004C00450043005400200040004D006F00640065005200650073006500720076006500640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000023530045004C00450043005400200020002000500056005F004E0041004D0045002000003920002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C0045002000007D20002000460052004F004D00200020002000500052004F00560049004400450052005300200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000540045005F00500052004F0056005F004900440020003D002000500056005F00490044002000004F200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4100430050005F0055004E0049005100550045005F0049004400001F4100430050005F004300520045004400490054005F00540059005000450000174100430050005F00420041004C0041004E0043004500008089200020002000200020002000200020002C002000430041005300450020005700480045004E00200050004D005F0052004500530054005200490043005400450044005F0054004F005F005400450052004D0049004E0041004C005F004C0049005300540020004900530020004E004F00540020004E0055004C004C0020005400480045004E00200000815D20002000200020002000200020002000200020002000200020002000200020002000490053004E0055004C004C0028002800530045004C00450043005400200031002000460052004F004D0020005400450052004D0049004E0041004C005F00470052004F005500500053002000570048004500520045002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200041004E0044002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020004100430050005F00500052004F004D004F005F0049004400200041004E0044002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0029002C002000300029002000003520002000200020002000200020002000200020002000200020002000200045004C005300450020003100200045004E0044002000002D200020002000200020002000200020002C0020004100430050005F00420041004C0041004E0043004500200000808120002000200049004E004E0045005200200020004A004F0049004E002000500052004F004D004F00540049004F004E00530020004F004E00200050004D005F00500052004F004D004F00540049004F004E005F004900440020003D0020004100430050005F00500052004F004D004F005F004900440020002000200020002000006D200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E005200320029002000008097200020002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000003B200020004F00520044004500520020004200590020004100430050005F0055004E0049005100550045005F00490044002000410053004300200000134000700054007900700065004E005200310000134000700054007900700065004E0052003200002D4000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0000395500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005F2000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000005F20002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E0049006400200000434F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F004300520045004400490054005F0054005900500045002000004320002000200020002000200020002C002000440045004C0045005400450044002E004100430050005F00420041004C0041004E004300450020002000200020002000007F20002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E00730029002900200000552000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E006900710075006500490064002000005720002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005D20002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006B20002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E00520032002900200000809520002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000004B20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C00200000815D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C004500430054002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000460052004F004D002000470045004E004500520041004C005F0050004100520041004D0053002000570048004500520045002000470050005F00470052004F00550050005F004B004500590020003D002000270043007200650064006900740073002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F00640065002700200041004E0044002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000013D5400720078005F00470065007400430072006500640069007400730050006C00610079004D006F006400650020006600610069006C00650064002E000031200020002000530045004C004500430054002000200020004100430050005F00420041004C0041004E00430045002000003320002000200020002000200020002000200020002C0020004100430050005F00570049005400480048004F004C0044002000003120002000200020002000200020002000200020002C0020004100430050005F0057004F004E004C004F0043004B002000002F20002000200020002000200020002000200020002C0020004100430050005F0050004C0041005900450044002000002920002000200020002000200020002000200020002C0020004100430050005F0057004F004E002000003920002000200020002000200020002000200020002C0020004100430050005F004300520045004400490054005F0054005900500045002000003520002000200020002000200020002000200020002C0020004100430050005F0055004E0049005100550045005F0049004400200000808720002000200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005300200057004900540048002000280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005D2000200020002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000006320002000200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006520002000200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000003D20004F0052004400450052002000420059002000200020004100430050005F0055004E0049005100550045005F004900440020004100530043002000003755006E006B006E006F0077006E00200043007200650064006900740050006C00610079004D006F00640065003A0020007B0030007D0000532000200020005300450054002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065002000004B20002000200020002000200020002C0020004100430050005F0057004F004E0020002000200020002000200020002000200020002000200020003D0020004000700057006F006E002000005120002000200020002000200020002C0020004100430050005F0050004C00410059004500440020002000200020002000200020002000200020003D0020004000700050006C0061007900650064002000005F20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001340007000420061006C0061006E0063006500000B4000700057006F006E00000F4100430050005F0057004F004E0000114000700050006C00610079006500640000154100430050005F0050004C0041005900450044000067430061006E0027007400200075007000640061007400650020006100630063006F0075006E0074002000700072006F006D006F00740069006F006E0073002E002000550070006400610074006500640020007B0030007D0020006F00660020007B0031007D0001809B500072006F006D006F00740069006F006E00200055006E006900710075006500490064003A0020007B0030007D002C00200043007200650064006900740054007900700065003A0020007B0031007D002C002000420061006C0061006E00630065003A0020007B0032007D002C00200050006C0061007900650064003A0020007B0033007D002C00200057006F006E003A0020007B0034007D00000F4500720072006F0072003A0020000027510075006500720079002E0043006F006D006D0061006E00640054006500780074003A002000002750006100720061006D00650074006500720020007B0030007D0020003D0020007B0031007D0000754400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020004D004F004E0045005900007B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020004D004F004E0045005900200029003B000080CD2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B0020004000700049006E00530065007300730069006F006E0052006500420061006C0061006E00630065002000200020002000200020000080CD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E006300650020000080CD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B0020004000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E0063006500200000808B2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D0020004000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650020002000200020002000200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E0063006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E005200650050006C00610079006500640020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E004E00720050006C00610079006500640020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E005200650057006F006E0020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E004E00720057006F006E0020000080AF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002000200020002D0020004000700049006E00530065007300730069006F006E0050006C00610079006500640020000180A9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002000200020002D0020004000700049006E00530065007300730069006F006E0057006F006E002000015920004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020000027200020002000460052004F004D002000200020004100430043004F0055004E0054005300200000294000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650000274000700049006E00530065007300730069006F006E005200650050006C00610079006500640000274000700049006E00530065007300730069006F006E004E00720050006C00610079006500640000214000700049006E00530065007300730069006F006E005200650057006F006E0000214000700049006E00530065007300730069006F006E004E00720057006F006E0000234000700049006E00530065007300730069006F006E0050006C006100790065006400001D4000700049006E00530065007300730069006F006E0057006F006E000055430061006E002700740020007500700064006100740065002000270069006E002000730065007300730069006F006E00270020006100630063006F0075006E0074002C002000490064003A0020007B0030007D000180834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000440045004C00540041005F0050004C00410059004500440020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000440045004C00540041005F0057004F004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E00450059000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E00540029003B000081132000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020003D002000430041005300450020005700480045004E0020002800500053005F0050004C0041005900450044005F0041004D004F0055004E00540020003E002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400290020005400480045004E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200045004E0044002000008113200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0057004F004E005F0041004D004F0055004E00540020002000200020003E002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00290020002000200020005400480045004E002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200045004E00440020000080AF20004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C0041005900450044002000410053002000440045004C00540041005F0050004C00410059004500440020000180AF200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E002000200020002000410053002000440045004C00540041005F0057004F004E0020002000200020000141200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000080C3200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E005300200041005300200050005300200049004E004E004500520020004A004F0049004E0020004100430043004F0055004E0054005300200041005300200041004300430020004F004E002000500053002E00500053005F004100430043004F0055004E0054005F004900440020003D0020004100430043002E00410043005F004100430043004F0055004E0054005F0049004400200000652000200057004800450052004500200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400200000474400450043004C00410052004500200040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200042004900470049004E005400200000634400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F004100430043004F0055004E0054005F0049004400200042004900470049004E0054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B00450044002000200020002000200020002000200020002000200020004200490054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004C004500560045004C00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F00470045004E0044004500520020002000200020002000200049004E005400005B200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004E0041004D004500200020002000200020002000200020004E00560041005200430048004100520028003200300030002900004B200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C0020004300420055005F00560041004C005500450020002000200020002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200042004900470049004E0054000057200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200049004E0054000079200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F0049004400200020002000200042004900470049004E0054000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000063200020002000200020002000200020002C002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020002000200042004900470049004E005400200020002000005F200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020004200490054002000200020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F004E0052005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F00520045005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005B200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200049004E00540029003B00200020002000200020002000200020002000003D530045005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D0020004E0055004C004C000055530045004C00450043005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D002000410043005F004100430043004F0055004E0054005F0049004400001B460052004F004D0020004100430043004F0055004E00540053000080C74C0045004600540020004A004F0049004E00200043005500530054004F004D00450052005F004200550043004B004500540020004F004E0020002800410043005F004100430043004F0055004E0054005F004900440020003D0020004300420055005F0043005500530054004F004D00450052005F00490044002900200041004E004400200028004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002900200000512000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000808F200020005700480045005200450020002000280020002000200028002000410043005F005400590050004500200020003D00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D0020004000700041006C00650073006900730054007200610063006B0044006100740061002000290020000080C120002000200020002000200020002000200020004F005200200028002000410043005F00540059005000450020003C003E00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D002000640062006F002E0054007200610063006B00440061007400610054006F0049006E007400650072006E0061006C002000280040007000570069006E0054007200610063006B0044006100740061002900200029002000290020000073200020002000200041004E0044002000200020002800410043005F00520045005F00420041004C0041004E0043004500200020002000200020002000200020002B0020004000700052006500420061006C0061006E006300650020002000200020002000290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F00520045005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F0052006500420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F004E007200420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E007400730020002000200020002000200020002000290020003E003D002000300020000077200020002000200055005000440041005400450020002000200043005500530054004F004D00450052005F004200550043004B00450054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007720002000200020002000200020005300450054002000200020004300420055005F00560041004C005500450020003D002000490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E0074007300200020002000007B20002000200020002000570048004500520045002000200020004300420055005F0043005500530054004F004D00450052005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C002000300029000057200020002000200020002000200041004E0044002000200020004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002000007520004900460020004000400052004F00570043004F0055004E00540020003D0020003000200041004E0044002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290020003C003E00200030000061200042004500470049004E00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200049004E005300450052005400200049004E0054004F00200043005500530054004F004D00450052005F004200550043004B0045005400200028004300420055005F0043005500530054004F004D00450052005F0049004400200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F004200550043004B00450054005F004900440020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F00560041004C0055004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F005500500044004100540045004400200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000290020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200020002000200020002000200020002000200020002000560041004C005500450053002000280040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004200750063006B0065007400490064005F005000540020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006F0069006E00740073002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020002000200020002000200020002000200020002000200020002000200020002000200020000061200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811D2000200020002000530045005400200020002000410043005F00420041004C0041004E004300450020002000200020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F004E007200420061006C0061006E00630065002000008085200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B0020004000700052006500420061006C0061006E0063006500200000808F200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F0052006500420061006C0061006E00630065002000008119200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020003D002000430041005300450020005700480045004E002000410043005F00520045005F0052004500530045005200560045004400200020002B002000400070005200650073006500720076006500640020003E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000410043005F00520045005F00420041004C0041004E00430045002000200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00520045005F00420041004C0041004E0043004500200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700052006500730065007200760065006400200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000400070004D006F00640065005200650073006500720076006500640020000065200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000410043005F004D004F00440045005F00520045005300450052005600450044002000004320004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B004500440020002000200020002000200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B005F0052004500410053004F004E00200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004C004500560045004C0020002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F00470045004E004400450052002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004E0041004D0045002C00200020002700270029002000014F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F00500054002E004300420055005F00560041004C00550045002C00200030002900005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020000079200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000075200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F00490044002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00520045005F00520045005300450052005600450044002C00200030002900200020002000005F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F004D004F00440045005F00520045005300450052005600450044002000200020002000200020002000200020002000200020000019200020002000200020002000200020002C002000300020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F004E0052002E004300420055005F00560041004C00550045002C0020003000290000452000200020002000200020002C002000490053004E0055004C004C002800430042005F00520045002E004300420055005F00560041004C00550045002C0020003000290000152000200020002000200020002C002000300020000037200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0054005900500045002000002B200020002000200020002000460052004F004D002000200020004100430043004F0055004E00540053000080ED49004E004E004500520020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005000540020004F004E0020002000430042005F00500054002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F00500054002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054000080ED4C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F004E00520020004F004E0020002000430042005F004E0052002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F004E0052002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F004E0052000080F14C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005200450020004F004E00200020002000430042005F00520045002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E004400200020002000430042005F00520045002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F005200450000772000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290000234000700041006C00650073006900730054007200610063006B004400610074006100001D40007000570069006E0054007200610063006B00440061007400610000174000700052006500420061006C0061006E0063006500002140007000500072006F006D006F0052006500420061006C0061006E0063006500002140007000500072006F006D006F004E007200420061006C0061006E006300650000114000700050006F0069006E007400730000154000700052006500730065007200760065006400001D400070004D006F006400650052006500730065007200760065006400001B400070004200750063006B0065007400490064005F0050005400001B400070004200750063006B0065007400490064005F004E005200001B400070004200750063006B0065007400490064005F00520045000029450078007400650072006E0061006C0054007200610063006B0044006100740061003A00200027000131270020002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001174100630063006F0075006E007400490064003A002000002F20002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001292000530045004C00450043005400200020002000500053005F0053005400410054005500530020000053200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C002000300029002000003B200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000002B200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000031200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000005F2000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000023270020002D002D003E00200045007800630065007000740069006F006E003A002000012120002D002D003E00200045007800630065007000740069006F006E003A00200001255500500044004100540045002000200020004100430043004F0055004E00540053002000004F200020002000530045005400200020002000410043005F004C004100530054005F0041004300540049005600490054005900200020003D0020004700450054004400410054004500280029002000004D200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064002000003B200020002000530045005400200020002000410043005F0042004C004F0043004B00450044002000200020002000200020003D002000310020000080B720002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E0020003D002000640062006F002E0042006C006F0063006B0052006500610073006F006E005F0054006F004E006500770045006E0075006D00650072006100740065002800410043005F0042004C004F0043004B005F0052004500410053004F004E00290020007C0020004000700042006C006F0063006B0052006500610073006F006E002000004920002000200020002000200020002C002000410043005F0042004C004F0043004B005F004400450053004300520049005000540049004F004E0020003D0020004E0055004C004C00004F200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000809920002000200041004E004400200020002000410043005F00540059005000450020004E004F005400200049004E0020002800400070004100630063006F0075006E0074005600690072007400750061006C0043006100730068006900650072002C002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0029002000001B4000700042006C006F0063006B0052006500610073006F006E00002F400070004100630063006F0075006E0074005600690072007400750061006C004300610073006800690065007200002D430061006E00630065006C00530074006100720074004300610072006400530065007300730069006F006E00003B4400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C00450020002800200000672000200020002000200020002000200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E0054000067200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200042004900470049004E0054000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000540045005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004E005600410052004300480041005200280035003000290029003B00006F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006B20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000053200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000041200020002000200020002000200020002C002000500053002E00500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020000031200020002000200020002000200020002C002000500053002E00500053005F0043004100530048005F0049004E002000002B200020002000200020002000200020002C002000540045002E00540045005F004E0041004D00450020000080C32000200049004E004E00450052002000200020004A004F0049004E0020005400450052004D0049004E0041004C005300200020002000200020004100530020005400450020004F004E002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020000080C32000200049004E004E00450052002000200020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004100530020005000530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006D2000200057004800450052004500200020002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400008091200020002000200041004E004400200020002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000008091200020002000200041004E004400200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000008091200020002000200041004E004400200020002000500053002E00500053005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F0049004400200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000808B200020002000200041004E004400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D002000400070005400720061006E00730061006300740069006F006E004900640020000063200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C0020000051200020002000200041004E004400200020002000500053002E00500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E00650064002000001D400070005300740061007400750073004F00700065006E0065006400005D2000200020002000200020002000200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000061200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B000080B32000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B00200040007000520065006400650065006D00610062006C00650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F005200650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F004E00720020000080BB200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020002B0020004000700054006F0047006D00520065006400650065006D00610062006C00650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F005200650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F004E00720020000080BF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020002B00200040007000460072006F006D0047006D00520065006400650065006D00610062006C00650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F005200650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F004E0072002000008097200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200040007000430061006E00630065006C005400720061006E00730061006300740069006F006E00490064002000008091200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200040007000430061006E00630065006C00520065006400650065006D00610062006C006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F0052006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F004E0072002000005520004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000001940007000520065006400650065006D00610062006C006500001340007000500072006F006D006F0052006500001340007000500072006F006D006F004E00720000214000700054006F0047006D00520065006400650065006D00610062006C006500001B4000700054006F0047006D00500072006F006D006F0052006500001B4000700054006F0047006D00500072006F006D006F004E007200002540007000460072006F006D0047006D00520065006400650065006D00610062006C006500001F40007000460072006F006D0047006D00500072006F006D006F0052006500001F40007000460072006F006D0047006D00500072006F006D006F004E007200002B40007000430061006E00630065006C005400720061006E00730061006300740069006F006E0049006400002540007000430061006E00630065006C00520065006400650065006D00610062006C006500001F40007000430061006E00630065006C00500072006F006D006F0052006500001F40007000430061006E00630065006C00500072006F006D006F004E00720000332000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200000811520002000200020002000530045005400200020002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004C00530045002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200045004E00440020000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E002C0020003000290020002B002000400070004E005200420061006C0061006E00630065000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E002C0020003000290020002B0020004000700052004500420061006C0061006E006300650000813D20002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020003D002000430041005300450020005700480045004E002000280040007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0020003D0020003100290020005400480045004E002000500053005F0043004100530048005F0049004E0020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E0020003000200045004C00530045002000500053005F0043004100530048005F0049004E0020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004E0044002000006320002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000200000234000700049007300460069007200730074005400720061006E007300660065007200003140007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0000234000700050006C0061007900610062006C006500420061006C0061006E00630065000017400070004E005200420061006C0061006E006300650000174000700052004500420061006C0061006E00630065000080C72000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0020005400480045004E0020003000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C00200030002900200045004E00440020002000002F2000530045004C00450043005400200020002000470050005F004B00450059005F00560041004C005500450020000033200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D005300200000572000200057004800450052004500200020002000470050005F00470052004F00550050005F004B0045005900200020003D002000270050006C00610079006500720054007200610063006B0069006E00670027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C00650050006C00610079006500640054006F00310050006F0069006E0074002700010520003B000079200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C0065005300700065006E00740054006F00310050006F0069006E00740027000173200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E0054006F00740061006C0050006C00610079006500640054006F00310050006F0069006E00740027002000010558005800000530003000006D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020000080A9200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E00450078007400650072006E0061006C004C006F00790061006C0074007900500072006F006700720061006D0027002000016720002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004D006F006400650027002000017D20002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000001B4200690072007400680044006100790041006C00610072006D0000174000700041006C00610072006D00540079007000650000155400650072006D0069006E0061006C003A0020000057200049004E005300450052005400200049004E0054004F002000200041004C00410052004D0053002000280041004C005F0053004F0055005200430045005F0043004F004400450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F00490044002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F004E0041004D00450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F0043004F0044004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004E0041004D004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004400450053004300520049005000540049004F004E0020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005300450056004500520049005400590020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005200450050004F00520054004500440020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F004400410054004500540049004D004500200020002000200020002000200020002000200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002000560041004C00550045005300200028004000700053006F00750072006300650043006F00640065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F00750072006300650049006400200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F0075007200630065004E0061006D0065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D0043006F006400650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004E0061006D00650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004400650073006300720069007000740069006F006E002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006500760065007200690074007900200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002000280029002000200020002000200020002000200020002000200000194000700053006F00750072006300650043006F006400650000154000700053006F0075007200630065004900640000194000700053006F0075007200630065004E0061006D00650000174000700041006C00610072006D0043006F006400650000174000700041006C00610072006D004E0061006D0065000009550073006500720000254000700041006C00610072006D004400650073006300720069007000740069006F006E0000154000700053006500760065007200690074007900004D20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020003D00200040007000420061006C0061006E0063006500520065007300740020000049200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020003D002000400070004E00650077005300740061007400750073002000001B40007000420061006C0061006E006300650052006500730074000080B955005000440041005400450020005400450052004D0049004E0041004C00530020005300450054002000540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F004900440020003D00200040007000500072006F006D006F00490064002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400001340007000500072006F006D006F004900640000809D2000200020002000530045004C00450043005400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000490053004E0055004C004C002800540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F00490044002C00200030002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000540045005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D20004C0045004600540020004A004F0049004E0020002000200050004C00410059005F00530045005300530049004F004E00530020004F004E002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000809D20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E00540053002000200020002000200020004F004E002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200000809D2000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E0044002000200020002800200020002000410043005F005400590050004500200020002000200020002000200020003D002000400070004100630063006F0075006E0074005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002000200020004F0052002000410043005F005400590050004500200020002000200020002000200020003D002000400070005600690072007400750061006C004100630063006F0075006E007400540079007000650020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001B400070004100630063006F0075006E00740054007900700065000029400070005600690072007400750061006C004100630063006F0075006E0074005400790070006500006B20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020002000200020003D0020004100430050005F00420041004C0041004E004300450020002B002000400070004E005200420061006C0061006E006300650020000055200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006120002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064002000004F200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000006B20005500500044004100540045002000200020004100430043004F0055004E005400530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D0020004000700041006D006F0075006E007400200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000410043005F004D004F00440045005F005200450053004500520056004500440020003D002000400070004D006F0064006500520065007300650072007600650064002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700041006D006F0075006E0074002000006B2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200000174100430050005F0057004F004E004C004F0043004B0000194100430050005F00570049005400480048004F004C0044000080D7200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E002000400070004E0065007700530074006100740075007300200045004C00530045002000500053005F0053005400410054005500530020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E0020004E0055004C004C002000200020002000200020002000200045004C00530045002000500053005F004C004F0043004B004500440020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E00200047004500540044004100540045002800290020002000200045004C00530045002000500053005F00460049004E0049005300480045004400200045004E004400200000374F0055005400500055005400200020002000440045004C0045005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C002000440045004C0045005400450044002E00500053005F0053005400410052005400450044002000006120002000200020002000200020002C002000490053004E0055004C004C002800440045004C0045005400450044002E00500053005F00460049004E00490053004800450044002C00200047004500540044004100540045002800290029002000005B200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400002F200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530000810F200020002000530045005400200020002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020003D002000430041005300450020005700480045004E002000280040007000420061006C0061006E00630065004D00690073006D00610074006300680020003D0020003100290020005400480045004E002000400070005200650070006F007200740065006400420061006C0061006E0063006500200045004C00530045002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D004100540043004800200045004E0044002000006F20002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020003D00200040007000460069006E0061006C00420061006C0061006E00630065002000007520002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065004D00690073006D00610074006300680020000071200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E0049004400200000809F20002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00650064002C002000400070005300740061007400750073004100620061006E0064006F006E0065006400200029002000006F20002000200041004E004400200020002000500053005F00530054004100540055005300200020002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005300740061007400750073004F00700065006E006500640020000023400070005200650070006F007200740065006400420061006C0061006E0063006500002340007000420061006C0061006E00630065004D00690073006D0061007400630068000023400070005300740061007400750073004100620061006E0064006F006E0065006400006720002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000067200020002000200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100620061006E0064006F006E00650064002000006720002000200020002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000200020002000200020002000200020000067200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000200000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002E00004D5400720078005F0050006C0061007900530065007300730069006F006E004D00610072006B00410073004100620061006E0064006F006E006500640020006600610069006C00650064002E00003B5400720078005F005300650074004100630063006F0075006E00740042006C006F0063006B006500640020006600610069006C00650064002E0000352000530045004C00450043005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020000037200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000002F200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000031200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E005400200000272000530045004C0045004300540020002000200043004F0055004E0054002800310029002000003B200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000004F20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064000055200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065000055200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002800400070004E00520031002C00400070004E00520032002900000B400070004E0052003100000B400070004E00520032000080AB200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020004900530020004E004F00540020004E0055004C004C002000290020000080B12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E00740020003E003D00200030002900200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E0074002000006F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C005300450020003000200045004E004400200045004C005300450020004E0055004C004C00200045004E0044002000004B200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005D200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002000200020003D0020004000700041006D006F0075006E007400001F4D0075006C007400690053006900740065004D0065006D00620065007200005749004E005300450052005400200049004E0054004F002000200020004D0053005F00500045004E00440049004E0047005F00470041004D0045005F0050004C00410059005F00530045005300530049004F004E00530000472000200020002000200020002000200020002000200020002000200028004D00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002900004120002000200020002000560041004C0055004500530020002000200028004000700050006C0061007900530065007300730069006F006E004900640029002000003349004E005300450052005400200049004E0054004F0020005700430050005F0043004F004D004D0041004E00440053002000003920002000200020002000200020002000200020002000280043004D0044005F005400450052004D0049004E0041004C005F00490044002000002B20002000200020002000200020002000200020002C00200043004D0044005F0043004F00440045002000002F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053002000003120002000200020002000200020002000200020002C00200043004D0044005F0043005200450041005400450044002000003F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053005F004300480041004E004700450044002000002F20002000200020002000200020002000200020002C00200043004D0044005F00500053005F00490044002900200000809720002000200020002000560041004C0055004500530020002800200040005400650072006D0069006E0061006C004900640020002C002000310020002C002000300020002C00200047004500540044004100540045002800290020002C00200047004500540044004100540045002800290020002C00400050006C0061007900530065007300730069006F006E004900640020002900001740005400650072006D0069006E0061006C0049006400001D400050006C0061007900530065007300730069006F006E0049006400007B2000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E0044006F004E006F0074004100770061007200640050006F0069006E007400730027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E007400730041007200650047007200650061007400650072005400680061006E00270001192000200055004E0049004F004E00200041004C004C00200000808D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E00740073005000650072004D0069006E0075007400650041007200650047007200650061007400650072005400680061006E0027000169200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E004800610073004E006F0050006C00610079007300270020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E00480061007300420061006C0061006E00630065004D00690073006D00610074006300680027002000012943006F006D0070007500740065006400200050006F0069006E00740073003A0020007B0030007D00003D2C00200050006F0069006E007400730020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D00002B2C0020004D0069006E007500740065007300200050006C0061007900650064003A0020007B0030007D00000930002E002300230000312C00200050006F0069006E0074007300200050006500720020004D0069006E007500740065003A0020007B0030007D0000532C00200050006F0069006E0074007300200050006500720020004D0069006E0075007400650020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D0000214E0075006D002E00200050006C0061007900650064003A0020007B0030007D0000292C00200048006100730020004E006F00200050006C006100790073003A0020005400720075006500002B420061006C0061006E006300650020004D00690073006D0061007400630068003A0020007B0030007D0000392C0020004800610073002000420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005400720075006500002550006C0061007900200074006F00200072006500760069007300650020002D003E002000011D4100630063006F0075006E007400490064003A0020007B0030007D0000232C0020005400650072006D0069006E0061006C00490064003A0020007B0030007D00001B20004300720069007400650072006900610020002D003E002000013520002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053002000008109200020002000200020002000530045005400200020002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F0053005400410054005500530020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700050006F0069006E007400730053007400610074007500730020002000200045004C00530045002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F00530054004100540055005300200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0043004F004D00500055005400450044005F0050004F0049004E005400530020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700043006F006D007000750074006500640050006F0069006E0074007300200045004C005300450020004E0055004C004C00200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0041005700410052004400450044005F0050004F0049004E0054005300200020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E00200040007000410077006100720064006500640050006F0069006E00740073002000200045004C005300450020004E0055004C004C00200045004E0044002000006F200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B40007000530074006100740065004F00700065006E0065006400001D4000700050006F0069006E007400730053007400610074007500730000214000700043006F006D007000750074006500640050006F0069006E0074007300001F40007000410077006100720064006500640050006F0069006E007400730000492000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020002000200020003D00200040007000470072006F00750070002000004D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B0045005900200020003D002000400070005300750062006A006500630074002000000F40007000470072006F00750070000013400070005300750062006A0065006300740000135700610072006E0069006E0067003A002000003F7B0030007D0020003C00520045003A0020007B0031007D002C002000500052003A0020007B0032007D002C0020004E0052003A0020007B0033007D003E0000197B0030007D002C002000500054003A0020007B0031007D00003555006E006B006E006F0077006E002000500072006F00760069006400650072004C0069007300740054007900700065003A00200000053A0020000019500072006F00760069006400650072004C0069007300740000114C006900730074005400790070006500000954007900700065000011500072006F007600690064006500720000094E0061006D00650000272000530045004C0045004300540020002000200043004F0055004E00540028002A00290020000031200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002000007D20002000200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D002000540045005F005400450052004D0049004E0041004C005F004900440000112000200057004800450052004500200000452000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D00200040005400650072006D0069006E0061006C0049006400200000772000530045004C004500430054002000200020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D00200043004100530054002800470050005F004B00450059005F00560041004C0055004500200041005300200069006E007400290020000080A52000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020003D002000270049006E0074006500720066006100630065003300470053002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002700490067006E006F00720065004D0061006300680069006E0065004E0075006D006200650072002700200001332000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F004900440020000047200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002C0020005400450052004D0049004E0041004C00530020000063200020005700480045005200450020002000200054003300470053005F00560045004E0044004F0052005F00490044002000200020002000200020002000200020002000200020003D00200040007000560065006E0064006F0072004900640020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200020004F00520020004000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200200020003D00200031002000290020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020004F00520020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D0020003100200029002000006B200020002000200020002000200020002000200041004E004400200054003300470053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020003D002000540045005F005400450052004D0049004E0041004C005F00490044002000001D40007000530065007200690061006C004E0075006D0062006500720000274000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200001F400070004D0061006300680069006E0065004E0075006D0062006500720000294000490067006E006F00720065004D0061006300680069006E0065004E0075006D00620065007200001F20004900460020004E004F00540020004500580049005300540053002000000720002800200000192000530045004C00450043005400200020002000310020000039200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E0047002000004F2000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F00750072006300650020000053200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005B200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D0062006500720020000007200029002000005B20002000200049004E005300450052005400200049004E0054004F0020005400450052004D0049004E0041004C0053005F00500045004E00440049004E004700200028002000540050005F0053004F005500520043004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00560045004E0044004F0052005F0049004400200000692000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00530045005200490041004C005F004E0055004D004200450052002000006F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F004D0041004300480049004E0045005F004E0055004D00420045005200200029002000005920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C005500450053002000280020004000700053006F0075007200630065002000005D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000560065006E0064006F00720049006400200000652000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530065007200690061006C004E0075006D006200650072002000006B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004D0061006300680069006E0065004E0075006D00620065007200200029002000000D200045004C00530045002000003D2000200020005500500044004100540045002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E00470020000061200020002000200020002000530045005400200020002000540050005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020000053200020002000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F0075007200630065002000005720002000200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005F20002000200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200000114000700053006F007500720063006500002B530079007300740065006D00440065006600610075006C00740053006100730046006C00610067007300008087200020002000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00500052004F0056005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005400450052004D0049004E0041004C005F00540059005000450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00500052004F00560049004400450052005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F0042004C004F0043004B004500440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00530054004100540055005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000500056005F0050004F0049004E00540053005F004D0055004C005400490050004C004900450052002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C00200043004100530054002800490053004E0055004C004C0020002800200028002000530045004C004500430054002000200020003100200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005F00470052004F005500500053002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E007400540079007000650020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020003100200029002C002000300020002900200041005300200042004900540029002000200020000080852000200009002000200020002000200020002000200041005300200053004900540045005F004A00410043004B0050004F005400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005300410053005F0046004C00410047005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005300410053005F0046004C004100470053005F005500530045005F0053004900540045005F00440045004600410055004C0054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000200020002000200020002000200020000080872000200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200009002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D0020002700430072006500640069007400730027002000200020002000200020002000200020000180872000200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F0064006500270020002000200020002000200020002000018085200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C002000300020002900200000808520002000090020002000200020002000200020002000410053002000530059005300540045004D005F0050004C00410059005F004D004F00440045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080852000200009002000200020002000200020002C002000540045005F00450058005400450052004E0041004C005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080852000200009002000200020002000200020002C002000540045005F0046004C004F004F0052005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C00200042004B005F004E0041004D0045002000410053002000420041004E004B00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00490053004F005F0043004F00440045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005400490054004F005F0048004F00530054005F0049004400200041005300200048004F00530054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000410052005F004E0041004D00450020004100530020004100520045004100200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080874C00450046005400200020004A004F0049004E0020002000500052004F0056004900440045005200530020004F004E002000500056005F004900440020003D002000540045005F00500052004F0056005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808749004E004E004500520020004A004F0049004E0020002000420041004E004B00530020004F004E00200042004B005F00420041004E004B005F004900440020003D002000540045005F00420041004E004B005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808749004E004E004500520020004A004F0049004E00200020004100520045004100530020004F004E00200042004B005F0041005200450041005F004900440020003D002000410052005F0041005200450041005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000472000200057004800450052004500200020002000540045005F004E0041004D00450020003D002000400070005400650072006D0069006E0061006C004E0061006D006500200000612000200057004800450052004500200020002000540045005F00450058005400450052004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400200000512000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4000700045006C0065006D0065006E00740054007900700065000029400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400003B20002000530045004C00450043005400200020002000540045005F004E0041004D00450020002000200020002000200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0042004100530045005F004E0041004D004500200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0046004C004F004F0052005F00490044002000200020002000200020002000003B2000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005300200020002000200020002000200020002000005520002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F00490044002000200020003D002000400070005400650072006D0069006E0061004900640020000017400070005400650072006D0069006E00610049006400000B25004E0061006D0065000013250042006100730065004E0061006D0065000011250046006C006F006F0072004900640000214900460020004E004F00540020004500580049005300540053002000280020000023200020002000200020002000530045004C0045004300540020002000200031002000003720002000200020002000200020002000460052004F004D00200020002000470041004D0045005F004D00450054004500520053002000006120002000200020002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F004900440020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006520002000200020002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D00450020003D00200040007000470061006D00650042006100730065004E0061006D0065002000001120002000200020002000200029002000003B20002000200049004E005300450052005400200049004E0054004F00200020002000470041004D0045005F004D0045005400450052005300200000412000200020002000200020002000200020002000200020002000200020002800200047004D005F005400450052004D0049004E0041004C005F0049004400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F00470041004D0045005F0042004100530045005F004E0041004D004500200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E0054002000003D2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E0054002000003F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E005400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D0045002000004F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200000512000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000004B2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F0052005400450044002000002320002000200020002000200020002000200020002000200020002000200029002000001F20002000200020002000200020002000560041004C005500450053002000003D20002000200020002000200020002000200020002000200020002000200028002000400070005400650072006D0069006E0061006C0049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000470061006D00650042006100730065004E0061006D006500200000432000200020002000200020002000200020002000200020002000200020002C00200040007000570063007000530065007100750065006E006300650049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000440065006E006F006D0069006E006100740069006F006E00200000492000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640043006F0075006E0074002000004B2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000432000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0043006F0075006E007400200000452000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000004D2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E007400200000372000200020002000200020002000200020002000200020002000200020002C0020004700450054004400410054004500280029002000001F2000200020002000200020002000200020002000200020002000200020000031200020002000550050004400410054004500200020002000470041004D0045005F004D00450054004500520053002000006B20002000200020002000200053004500540020002000200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200020002000200020003D00200040007000570063007000530065007100750065006E0063006500490064002000006920002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200020002000200020002000200020003D00200040007000440065006E006F006D0069006E006100740069006F006E0020000080A520002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0043004F0055004E0054002000200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0041004D004F0055004E005400200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E005400200020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0043004F0055004E0054002000200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0041004D004F0055004E005400200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000006920002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D004500200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080A520002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E0054002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E00540020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E005400200020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E00540020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E00740020000080A920002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E0054002000200020002000200020003D00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E00540020002000200020002000200020002B00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074002000005F20002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F00520054004500440020002000200020002000200020003D0020004700450054004400410054004500280029002000006520002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006920002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D0045002000200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080C12000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000530045005400200020002000430053005F0053005400410054005500530020003D00200040007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D0020002800200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000400070004F00700065006E0053007400610074007500730020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F00530045005300530049004F004E005F004900440020003C003E002000280020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C0045004300540020002000200054004F005000200031002000430053005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D00200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C00530020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F005200440045005200200042005900200020002000430053005F004F00500045004E0049004E0047005F004400410054004500200044004500530043002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000002D40007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073000019400070004F00700065006E00530074006100740075007300008095530045004C004500430054002000540045005F005400450052004D0049004E0041004C005F0054005900500045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000033000000D7B0030003A00440034007D00000D7B0030003A00440032007D00000D7B0030003A00440039007D00001769006E0070007500740042007500660066006500720000495400720061006E00730066006F0072006D0042006C006F0063006B003A00200049006E00700075007400200062007500660066006500720020006900730020006E0075006C006C0000196F0075007400700075007400420075006600660065007200004B5400720061006E00730066006F0072006D0042006C006F0063006B003A0020004F0075007400700075007400200062007500660066006500720020006900730020006E0075006C006C00004F5400720061006E00730066006F0072006D0042006C006F0063006B003A00200050006100720061006D006500740065007200730020006F007500740020006F0066002000720061006E0067006500004742006C006F0063006B00530069007A0065003A0020004500720072006F007200200049006E00760061006C0069006400200042006C006F0063006B002000530069007A0065000033490056003A0020004500720072006F007200200049006E00760061006C00690064002000490056002000530069007A006500003F4D006F00640065003A0020004500720072006F007200200049006E00760061006C0069006400200043006900700068006500720020004D006F00640065000047500061006400640069006E0067003A0020004500720072006F007200200049006E00760061006C00690064002000500061006400640069006E00670020004D006F0064006500000F41005200430046004F0055005200000F61006C0067004E0061006D006500002F4300720065006100740065003A0020004500720072006F0072005F0050006100720061006D004E0075006C006C000007520043003400000D5200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020005200670062006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020006B00650079002000730069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000730069007A006500003B42006C006F0063006B00530069007A0065003A00200049006E00760061006C0069006400200062006C006F0063006B002000730069007A006500003143007200650061007400650044006500630072007900700074006F0072003A00200044006900730070006F0073006500000D7200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020007200620067006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020004B00650079002000530069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000530069007A006500002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D00740072007500650000315400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0000174200690065006E00760065006E00690064006F002000005B5400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000335400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E00005D5400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002D5400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0000575400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002B5400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730000555400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000235400720078005F003300470053005F00530065006E0064004500760065006E007400004D5400720078005F003300470053005F00530065006E0064004500760065006E00740020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000000000009BACD012287DDD41AA667887BED9A8D70008B77A5C561934E0890B00051281200E0E080E121D1100081281380E0E080E0A1281281121121D0A00041281200E0E08121D0E00071281200E0E0E080A1121121D1100061281380812814C0A1281281121121D0C0004011281201008100E100E0C0004011281381008100E100E060002020E121D0A0004020A100A1008121D072002011218121D08200401080E0A121D03200001030612250306121802060802060E02060A042001010A052002010A0E052001011218072003011218080E06200301080E0A07200401080E0A0E0F2007020A0A11341121112111211121102008020A0A113411211121112111210A102008020A0A113411211121112111210E12200A020A0A113411211121112111210E080E13200B020A0A113411211121112111210E080E0E14200C020A0A113411211121112111210E080E0E0A0600020E0A121D0320000A0420010A0805200102121D0328000A020602030611290306112C04200012180306122D0520010112250620020112180E0A2005020A113011210A0E0B2006020A113011210A0E0E0C2007020A113011210A0E0E020C2007020A113011210A0E0E0E0D2008020A113011210A0E0E0E020F2009020A113011210A0E0E0E1121020E2008020A113011210A0E0E0E112110200A020A113011210A0E0E0E11210A0A11200B020A113011210A0E0E0E11210A0A0212200B020A113011210A0E0E0E11210A0A112113200C020A113011210A0E0E0E11210A0A11210226200F020A113011210A0E0E0E11210A0A112115113101118090151131011180D8151131010A02092004020A0A0E1280AC0B2005020A0A0E1280AC11210A2005020A0A0E1280AC0E0D2006020A0A0E1280AC113011210520010111210520010111290720020111211121032000081000060111301121101121101121100E020B000411211130112111210204280012180428001225030611200400000000040100000004020000000404000000040800000004100000000420000000044000000004800000000400010000040002000004000400000400080000040010000004002000000400400000040080000004000001000400000200030611240403000000040500000004060000000407000000030611280409000000040A000000040B00000004FFFFFFFF03061130040C000000040D000000040E000000041400000004150000000416000000041700000004180000000419000000041C000000041D000000041E000000041F000000042100000004220000000423000000042400000004250000000426000000042700000004280000000429000000042A000000042B000000042C000000042D000000042E000000042F0000000430000000043100000004320000000433000000043400000004350000000436000000043700000004380000000439000000043A000000043B000000043C000000043D000000043E000000043F000000044100000004420000000443000000044400000004450000000446000000044700000004480000000449000000044A000000044B000000044C000000044D000000044E000000044F0000000450000000045400000004550000000456000000045700000004580000000459000000045A000000045B000000045C000000045D000000045E000000045F0000000460000000046100000004620000000463000000046400000004650000000466000000046700000004680000000469000000046A000000046B000000046C000000046D000000046E000000046F0000000470000000047100000004720000000473000000047400000004750000000476000000047700000004780000000479000000047A000000047B000000047C000000047D000000047E000000047F000000048B000000048C000000048D000000048E000000048F0000000490000000049100000004920000000493000000049400000004950000000496000000049700000004980000000499000000049A000000049B000000049C000000049D000000049E000000049F00000004A000000004A100000004BE00000004BF00000004C800000004C900000004CA00000004CB00000004CC00000004CE00000004CF00000004D0000000042B010000042C010000042D010000042E010000042F01000004300100000431010000043201000004330100000434010000043501000004360100000437010000048F01000004F401000004F501000004FE01000004FF010000040102000004020200000403020000040402000004050200000406020000040702000004080200000409020000040A020000040B020000040C020000040D020000040E020000040F0200000410020000041102000004120200000413020000041402000004150200000416020000041702000004180200000419020000041A020000041B020000041C020000041D020000041E020000041F0200000420020000042102000004220200000423020000042402000004250200000426020000042702000004280200000429020000042A020000042B020000042D020000042F020000043002000004320200000433020000043402000004350200000436020000043702000004380200000439020000043A020000043B020000043C020000043D020000043E020000043F020000044002000004410200000442020000044302000004440200000445020000044602000004470200000448020000044A020000044B020000044C020000044D020000044E020000044F0200000450020000045102000004520200000453020000045402000004550200000456020000045702000004580200000459020000045A020000045B020000045C020000045D020000045E020000045F0200000460020000046102000004620200000463020000046402000004650200000466020000046702000004680200000469020000046A020000046B020000046C020000046D020000046E020000044C04000004AF04000004B0040000041305000004140500000477050000047805000004DB05000004DC050000043F0600000440420F000480841E0004BFC62D0003061134041A000000041B0000000452000000045300000004CD00000004D100000004D200000004D300000004D400000004D500000004D600000004D700000004D800000004D900000004DA00000004DB00000004DC00000004DD00000004DE00000004DF00000004E000000004E100000004E200000004E300000004E400000004E500000004E600000004E700000004E800000004E900000004EA00000004EB00000004EC00000004ED00000004EE00000004EF00000004F000000004F100000004F200000004F300000004F400000004F500000004F600000004F700000004F800000004F900000004E80300000306113804900100000306113C0306114003061144030611480306114C0306115003061154030611580306115C04000004000400000800040000100004000020000400004000040000800003061160030611640306116804E703000004E903000004F20300000306116C0306117003061174030611780306117C0406118080040F00000004061180840406118088040611808C040611809004EA03000004EB03000004061180940406118098041100000004120000000413000000048100000004820000000483000000048400000004850000000486000000048700000004880000000489000000048A000000040611809C04061180A004061180A404061180A80306112104061280B004200011210320000E042001010E04200101080320000204200101020520001180900620010111809005200011808C0620010111808C0520001280B0062001011280B004280011210328000E032800080328000205280011809005280011808C0528001280B0042000112905200011809406200101118094042800112905280011809404061180B404061180B804061180BC04061180C004061180C404061180C804061180CC04061180D004061180D404061180D804061180DC04061180E004061180E804061180EC04061180F004061180F404061180F804061180FC040611810004061181040406118108040611810C0800000000000000800F0005021181500A101281441008121D0A000212812012811C121D0F00051281200A12817C12814C02121D1600081281200A12817C12814C0211811812814402121D19000B0212814C0A0811640E021181181281440210128120121D0C00050211210E0E101121121D0E0005021281440E0E10128144121D090005020A080A02121D080004020A080A121D160009020A08116402118118128144100A10118168121D0E0007020A113C0E081164100A121D070003020A0A121D1300080211680A081164128144128144100A121D0F00050211640A12812810128128121D100006020811640E12812410128138121D0C0004020A128144128144121D080003020A1002121D0B000302128140128138121D0B0005020A0A08128144121D0F00050211351281401281381164121D0B00030212817C128144121D140007020A1281441281441281441281441121121D120008020A112111211121112111211121121D060002020A121D110006020A0A10112110112110128144121D080004020A0A0A121D0A0004020A11211121121D0800030211210A121D100007020A0A0A0A12814410128144121D0800030210080A121D140009020A0A0A0A1281440210128144101225121D0A0003020A10128144121D0900020210118178121D0A00030212813C1239121D0B00040212813C123902121D0A0003020A1012813C121D08000112813C1281240B0004020A1121101121121D0C0005020A112102101121121D120006020A128144112110128144101121121D150007020A12814411211180F810128144101121121D0D0004020A12814410128144121D0F0005020A12814410128144100A121D120006020A12814410128144101121100A121D130006020A12814410128144100A10128144121D180008020A128144112110128144101121100A10128144121D190009020A12814411210210128144101121100A10128144121D110007020A0E1281441121021012814C121D140008020A0E1281441121021180F81012814C121D080003020A115C121D0B000502080A1002100A121D190008021181680A0A128144101281441012814410128144121D0B000502020A12814402121D0C000602020A1281440202121D140008020A12817C1121112111211011211002121D070002021008121D0A0005020A0E0E1164121D0B0007020A0A0E090E08121D0C0005020A1281440A1225121D0E000602080A1281441002100A121D0D0004020A11211181581011815C0E0005020A1121118158021011815C130007020A1121118158021011815C101121121D0700020112813C020600010112813C08000201112412813C1100060211440A101144101144101135121D0D0006020A11211121021134121D080003020A1239121D110009020A11210A0811640E0E101239121D090003020A128124121D0A0004020A11701121121D07000302080A121D1400070212817C112111351281401281381002121D0700030E0E0E121D04061181140406118118040612814403061239072002011181140E0406128128040612812C04061281300406118134072002011181340E06200101128144092003011121112111210C20050111211121112111210210200701112111211121112102112111210500001281440520001281440B000212814412814412814409000202128144128144042001021C080001128144128144090002128144128144020320001C050800128144052800128144062001011281480820020112814411210500001281480B000212814812814812814809000202128148128148050800128148040611815004061181540406118158040611815C0406118160040611816404061181680306123D052000118170062001011181700720040E0E0E0E0E042001020E052001122D0E0720020E123D114108200301123D0E114505280011817004061181700600020208121D0C0005020E0E081012817C121D09000502080E0E08121D0A000302081012817C121D0A0003020E1012817C121D0C000502080E0E1012817C121D09000402080E100E121D060002020811200600011249121D0900030208121D10116404061181780520010211200400010E0E04F526000003061D05080003020E100E100807000302100E0E080500020E080A080003010E1008100A05000202080E090003021181880E100E0406118188020605052001011D050A2005081D0508081D05080820031D051D0508080420001D050520001D124D042000115105200101115104200011550520010111550500001281900600011281900E0428001D050528001D124D0428001151042800115508200212111D051D050406128194120009010E0E080E100A1011211008100E100E18000E010E0E080E0A112111210808112111211008100E100E0F0007010E0E081011211008100E100E100009010E0E0E080A11211008100E100E0100062001011180910600030E1C1C1C0F070512812012817C12814C128120020E070612813812814C0A081281380214070812813812817C12814C1281240A08128138020C070412812012814C1281200205200112390E0520001280A5092003010E1280A5121D0520001280A90920021280AD0E1180B1042001011C16070A12812012817C12814C0812390A1249128120020A0700040E0E1C1C1C07000202112111210600020E0E1D1C080002112111211121070002020E101121060001112111210A20031280AD0E1180B1081A070C128138128128112111211121112102021249128138021D1C05070111811405070111813404061280C909070512391C124902020520001280CD04200108080B0705123912491280CD02020607021210113403070102040001020E0520001280D50600011280D90E0920021280DD0E1280D90620011280DD0E072001011D1280DD0907021280DD1D1280DD0600030E0E0E0E042000122D052002010E1C0520001280E105200101122D060703122D02020907060E1C0E12490E02052001122D080420011C0E0407020A02060002020E10080520001280ED062001011180F1062001011180F50520010112490520010812251A070E12390802081280AD0A0E122D12491280F902021280ED12150807040A11210A121C0407011218071511310111809005200101130007151131011180D805151131010A08070202151131010A050002020E0E0A070511211121112102020F070711301130020211809011808C02060703113002020D0706113002021180900211808C060703112102020420001D1C0507030808020520011121083707191239081280AD1280AD1280AD1280AD1280AD1280AD0E11300A0E0E02122D12491280CD1280F91281011280E502021280ED121511300607030E1130020907041121112111300204070111210307010A0307010E0307010805070111809005070111808C0507011280B004070111290507011180940500020E0E0E0420011C08240710123912251281440E11280A12491280F91280AD122D1280CD02021181501280ED12150507011281200A070412812008128120020420010E0804200102080500010E1D0E0700040E0E0E0E0E58072C0A128144128144128144128144128144121011816811211121020A12814412814412390A113C02020212814C112111211180F8128144020E0A020212491280CD12491280CD0A1225112111210E1280E502021D0E1121042001080E0507030202020807041239124902022707171239080A113C11440E0E020211441144113508081121112111211121112112491280CD02020F0707113812391280AD124902113C020607031249020213070912391280AD1138114411211249020211680500020A0A0A2B07131281441281440202128140112112813C12813C02021281281121112112813C1281441280E5020211640D07071239081C12491280E502022F071812814412814412814412814412814412101144114411440812817C1135020A0A08021C12491280E50A0202113422070D12814412814412813C12391281441281440212817C12814412814412814402020320000D0B0705123912491280E50202042000123917070A1239122512491280F91280AD122D02021280ED121519070C12391225020812491280F91280AD122D02021280ED12150A070612391C0A1249020205070212250205200201081C200710123911241121020E122D0812491280CD1280AD122D02021280ED1124121509070412813C123902020B0705123912490202118178062001011281110A000311211121081181150500020E0E1C0A20031D122D0E0E1181190420001249062001081D122D0600030E0E1C1C34071712391D122D1181780212491280CD11211121112111211280F908122D1280AD1280E502021181781D122D081D1C1280ED121510070612813C112111211121112112813C0A07041281441281440A020707031281440A0207070312814C020209070412814411210A02080703112112814402060702128144020507021121020520020E080316070E12390E0808080802020212491280CD1280E5020232071712390A0A0E1281441281441281441281441281441281441281440211441144021135112112491280CD128144121002021B070B12391281441281441281441281440A12491280CD02021181680420010E0E06200212390E0E05000111210E070002112111210813070A1121112111211239080812491280CD020206200101118129072002010E1180B10820011280AD1280AD0620011280AD0E1407091181540A0E12491280AD1280AD02021181541D070D12390A1121112112491280AD1280AD1280AD122D02021280ED121517070D12391281440A0A0A0E021121121012491280CD02020F07071281440A1239124902021181580C07051121122D1280ED0212150E070611211121122D1280ED021215052001112908062001113511290F070712391129112912491280CD02020707040E124902020F070612812412813812101280E502020D0707123908124912490211700209070512390812490202060002020E100605000111210806200212390E1C05000111210D07200212390E1D1C0620021239080E1B070F080806061180C4112112391239123912491280CD02020D11210A070612391C0E12490E020707030E1D1C11210507011281440607021281441C0507011281480507011181700500020E1C1C1107080E1239122D0E1181701280ED021215050702122D02060702021181700407020E020520001280D90620011280DD080520001281350B0703122D1181701D1280DD072002020E1181390C0705122D122D1280ED0212150820020112814111410807040E12813D0E0209200211451281491145060702128145020A07061C123912490802020A070612391C12490802020420010608110709123908080812491280CD02021181780520020E0E0E0F070912390E0E0E0E12491280CD0202080703123912491249090705021239124902020607040E020E020607040E080E02050002080E0E0400010B0E0500011D050B0B2003011281651211118169072003081D0508080600020B1D05080520020E0808040001080E2107151281980E0E0E0E090E0B0B0B1D051D050E12111D0512815D1281610802020B072003011D0508081B070F1281980E0E090B1D05121112815D128161081D051D0502020B0400010A0E08070608080E0A02020507030802020300000104061181A40900020112817911817D0407011D05021D05052002010E0E07070505050808020707031D051D050206070405080802080003011281790808040001011C062003010808080807021D124D1D124D040701115104070111550707021281991D05050701128190080003020E0E1181390607021281900205070212110204070112110507011D124D042000121D0E07051281201280A5121D1280E5021107061281381281281280A5121D1280E502190100145753492E53514C427573696E6573734C6F67696300000501000000000E0100094D6963726F736F667400002001001B436F7079726967687420C2A9204D6963726F736F6674203230313200002901002431306432653537662D303966322D343639642D396665312D35366639356262383932303600000C010007312E302E302E3000000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F77730100000000000000201A3C5800000000020000001C0100003C8A04003C6C040052534453BF1E340709F0534195396111979A449804000000633A5C5446535C5769676F735C4465765C5769676F732053797374656D5C5753492E53514C427573696E6573734C6F6769635C6F626A5C44656275675C53514C427573696E6573734C6F6769632E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058A00400480300000000000000000000480334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004A8020000010053007400720069006E006700460069006C00650049006E0066006F00000084020000010030003000300030003000340062003000000034000A00010043006F006D00700061006E0079004E0061006D006500000000004D006900630072006F0073006F00660074000000540015000100460069006C0065004400650073006300720069007000740069006F006E00000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E00300000004C001500010049006E007400650072006E0061006C004E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000005C001B0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004D006900630072006F0073006F006600740020003200300031003200000000005400150001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000004C0015000100500072006F0064007500630074004E0061006D006500000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE

GO

CREATE PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_UpdateCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_StartCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint] OUTPUT,
	@PlayableBalance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_StartCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_SendEvent]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@EventId [bigint],
	@Amount [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_SendEvent]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_EndCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_EndCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_AccountStatus]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@MachineNumber [int],
	@Balance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_AccountStatus]
GO


/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Cashier_Sessions_Report]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
GO

CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
(  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pReportMode as Int
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN

  -- Members --
  DECLARE @_delimiter AS CHAR(1)
  SET @_delimiter = ','
  DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
  DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
  DECLARE @_table_sessions_ AS TABLE (  sess_id                      BIGINT 
                    , sess_name                    NVARCHAR(50) 
                    , sess_user_id                 INT      
                    , sess_full_name               NVARCHAR(50) 
                    , sess_opening_date            DATETIME 
                    , sess_closing_date            DATETIME  
                    , sess_cashier_id              INT 
                    , sess_ct_name                 NVARCHAR(50) 
                    , sess_status                  INT 
                    , sess_cs_limit_sale           MONEY
                    , sess_mb_limit_sale           MONEY
                    , sess_collected_amount        MONEY
                    , sess_gaming_day              DATETIME
                    , sess_te_iso_code             NVARCHAR(3)
                     ) 
                                 
  -- SYSTEM USER TYPES --
  IF @pShowSystemSessions = 0
  BEGIN
  INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
  END
  
  -- SESSIONS STATUS
  INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)
    IF @pReportMode = 0 OR @pReportMode = 2
     BEGIN -- Filter Dates on cashier session opening date  

     IF @pReportMode = 0
      BEGIN
      -- SESSIONS BY OPENING DATE --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
           , CS_NAME AS SESS_NAME
           , CS_USER_ID AS SESS_USER_ID
           , GU_FULL_NAME AS SESS_FULL_NAME
           , CS_OPENING_DATE AS SESS_OPENING_DATE
           , CS_CLOSING_DATE AS SESS_CLOSING_DATE
           , CS_CASHIER_ID AS SESS_CASHIER_ID
           , CT_NAME AS SESS_CT_NAME
           , CS_STATUS AS SESS_STATUS
           , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
           , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
           , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
           , CS_GAMING_DAY AS SESS_GAMING_DAY
           , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo)
        --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
            AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
        --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
        --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_OPENING_DATE DESC  
      END
    ELSE
      BEGIN
      -- @pReportMode = 2
      -- SESSIONS BY GAMING DAY --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
           , CS_NAME AS SESS_NAME
           , CS_USER_ID AS SESS_USER_ID
           , GU_FULL_NAME AS SESS_FULL_NAME
           , CS_OPENING_DATE AS SESS_OPENING_DATE
           , CS_CLOSING_DATE AS SESS_CLOSING_DATE
           , CS_CASHIER_ID AS SESS_CASHIER_ID
           , CT_NAME AS SESS_CT_NAME
           , CS_STATUS AS SESS_STATUS
           , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
           , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
           , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
           , CS_GAMING_DAY AS SESS_GAMING_DAY
           , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   (CS_GAMING_DAY >= @pDateFrom AND CS_GAMING_DAY < @pDateTo)
        --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
            AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
        --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
        --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_GAMING_DAY DESC  
      END

    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    -- MOVEMENTS
    -- By Movements in Historical Data --    
    SELECT   CM_SESSION_ID
         , CM_TYPE
         , CM_SUB_TYPE
         , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
         , CM_CURRENCY_DENOMINATION
         , CM_TYPE_COUNT
         , CM_SUB_AMOUNT
         , CM_ADD_AMOUNT
         , CM_AUX_AMOUNT
         , CM_INITIAL_BALANCE
         , CM_FINAL_BALANCE
         , CM_CAGE_CURRENCY_TYPE
      FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
     WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

     END
  ELSE
    BEGIN -- Filter Dates on cashier movements date (@pReportMode = 1)
       
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
        SELECT   CM_DATE
             , CM_SESSION_ID
             , CM_TYPE
             , 0 as CM_SUB_TYPE
             , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , 1 as CM_TYPE_COUNT
             , CM_SUB_AMOUNT
             , CM_ADD_AMOUNT
             , CM_AUX_AMOUNT
             , CM_INITIAL_BALANCE
             , CM_FINAL_BALANCE
             , ISNULL(CM_CAGE_CURRENCY_TYPE, 0) AS CM_CAGE_CURRENCY_TYPE
          FROM   CASHIER_MOVEMENTS 
         WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
         UNION   ALL
        SELECT   MBM_DATETIME AS CM_DATE
             , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
             , MBM_TYPE AS CM_TYPE
             , 1 AS CM_SUB_TYPE
             , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
             , 0 AS CM_CURRENCY_DENOMINATION
             , 1 AS CM_TYPE_COUNT
             , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
             , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
             , 0 AS CM_AUX_AMOUNT
             , 0 AS CM_INITIAL_BALANCE
             , 0 AS CM_FINAL_BALANCE
             , 0 AS CM_CAGE_CURRENCY_TYPE
          FROM   MB_MOVEMENTS 
         WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --  
      INSERT   INTO @_table_sessions_  
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID  
         , CS_NAME AS SESS_NAME  
         , CS_USER_ID AS SESS_USER_ID  
         , GU_FULL_NAME AS SESS_FULL_NAME  
         , CS_OPENING_DATE AS SESS_OPENING_DATE  
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE  
         , CS_CASHIER_ID AS SESS_CASHIER_ID  
         , CT_NAME AS SESS_CT_NAME  
         , CS_STATUS AS SESS_STATUS  
         , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE  
         , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE  
         , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT  
         , CS_GAMING_DAY AS SESS_GAMING_DAY  
             , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE  
      FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */  
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID  
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID  
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)  
        --   usuarios sistema s/n & user   
       AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )  
          AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )  
        --   cashier id  
       AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END)   
        --   session status  
       AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )  
       ORDER   BY CS_OPENING_DATE DESC    
       
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID  
       
       DROP TABLE #CASHIER_MOVEMENTS        
    
     END   
    
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- SESSIONS with CAGE INFO  
  SELECT  SESS_ID  
        , SESS_NAME  
        , SESS_USER_ID  
        , SESS_FULL_NAME  
        , SESS_OPENING_DATE  
        , SESS_CLOSING_DATE  
        , SESS_CASHIER_ID  
        , SESS_CT_NAME  
        , SESS_STATUS  
        , SESS_CS_LIMIT_SALE  
        , SESS_MB_LIMIT_SALE  
        , SESS_COLLECTED_AMOUNT   
        , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID  
        , X.CGS_SESSION_NAME AS CGS_SESSION_NAME   
        , SESS_GAMING_DAY  
        , SESS_TE_ISO_CODE
        , (SELECT SUM(AO_AMOUNT) FROM  account_operations A1 WHERE A1.ao_code IN (1, 17) AND NOT EXISTS(SELECT A.ao_operation_id FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 17) AND A.ao_operation_id = A1.ao_operation_id AND A.ao_cashier_session_id = SESS_ID) AND A1.ao_cashier_session_id = SESS_ID) AS SESS_MONEY_RECHARGE_AMOUNT
        , (SELECT SUM(T.pt_total_amount) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 17) AND A.ao_cashier_session_id = SESS_ID) AS SESS_BANK_CARD_RECHARGE_AMOUNT
        , (SELECT Count(*) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 17) AND A.ao_cashier_session_id = SESS_ID) AS SESS_TOTAL_VOUCHERS
        , (SELECT Count(*) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 17) AND A.ao_cashier_session_id = SESS_ID AND EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)) AS SESS_DELIVERED_VOUCHERS
        , (SELECT ISNULL(SUM(T.pt_total_amount),0) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 17) AND A.ao_cashier_session_id = SESS_ID AND EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)) AS SESS_TOTAL_VOUCHERS_AMOUNT
        --, ()
    FROM   @_table_sessions_  
    LEFT   JOIN ( SELECT    CGS_SESSION_NAME  
                          , CGM_CASHIER_SESSION_ID  
                          , CGS_CAGE_SESSION_ID   
                  FROM   CAGE_MOVEMENTS AS CM  
                  INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  
                  WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID  FROM    CAGE_MOVEMENTS AS CGM WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)
                 )
    AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID 
                   
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- SESSIONS CURRENCIES INFORMATION
  IF @pReportMode = 0 OR @pReportMode = 2
    SELECT   CSC_SESSION_ID
         , CSC_ISO_CODE
         , CSC_TYPE
         , CSC_BALANCE
         , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_WIN]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION dbo.GT_Calculate_WIN;
GO

CREATE FUNCTION [dbo].[GT_Calculate_WIN] ( @pChipsFinalAmount AS MONEY, @pChipsInitialAmount AS MONEY, @pChipsFillAmount AS MONEY, @pChipsCreditAmount AS MONEY, @pCollectedAmount AS MONEY, @pTipsAmount AS MONEY ) RETURNS MONEY
  BEGIN
    RETURN (@pChipsFinalAmount - @pChipsInitialAmount) + (@pChipsCreditAmount - @pChipsFillAmount)+ @pCollectedAmount - @pTipsAmount

END

GO

GRANT EXECUTE ON [dbo].[GT_Calculate_WIN] TO [wggui] WITH GRANT OPTION

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN)  TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , SUM(X.S_DROP) AS CALCULATED_DROP
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP)/SUM(X.S_DROP) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP(ISNULL(GTS_OWN_SALES_AMOUNT      , 0)
                                                         , ISNULL(GTS_EXTERNAL_SALES_AMOUNT , 0)
                                                         , ISNULL(GTS_COLLECTED_AMOUNT      , 0)
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT       , 0))
                                                         , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT  , 0))
                                                         , SUM(ISNULL(GTSC_COLLECTED_AMOUNT       , 0))
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END                                                                                  AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_TIPS                    , 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTS_CREDITS_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT    , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                , 0)))
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     ISNULL(GTSC_TIPS, 0)
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ZZ.CALCULATED_DROP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME              

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ZZ.CALCULATED_DROP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ZZ.CALCULATED_DROP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ZZ.CALCULATED_DROP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                     ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
                     
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE

GO

GRANT EXECUTE ON [dbo].[GT_Calculate_WIN] TO [wggui] WITH GRANT OPTION

GO

GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION

GO



/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

--SP_RestartTerminal
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_RestartTerminal]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_RestartTerminal]
GO

CREATE PROCEDURE [dbo].[SP_RestartTerminal] 
       @pTerminalId                   INT

AS
BEGIN
  INSERT INTO  WCP_COMMANDS 
              (CMD_TERMINAL_ID, CMD_CODE, CMD_STATUS)
       VALUES (@pTerminalId, 2, 0)
END

GO 

--SP_RestartAllTerminals
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_RestartAllTerminals]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_RestartAllTerminals]
GO

CREATE PROCEDURE [dbo].[SP_RestartAllTerminals] 
AS
BEGIN

  DECLARE @pTerminalId BIGINT  
  DECLARE CursorActiveTerminals CURSOR FOR SELECT TE_TERMINAL_ID 
                                             FROM TERMINALS 
                                            WHERE TE_TERMINAL_TYPE IN ( 1, 5, 101, 102, 105, 106, 108, 109, 110)  
                                              AND TE_STATUS = 0 

  OPEN CursorActiveTerminals
	  FETCH NEXT FROM CursorActiveTerminals INTO @pTerminalId

	  WHILE @@FETCH_STATUS = 0
	  BEGIN
		  EXEC SP_RestartTerminal @pTerminalId
  		  
		  FETCH NEXT FROM CursorActiveTerminals INTO @pTerminalId
	  END -- WHILE
  	
  CLOSE CursorActiveTerminals
  DEALLOCATE CursorActiveTerminals

END

GO

GRANT EXECUTE ON [dbo].[SP_RestartTerminal] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[SP_RestartAllTerminals] TO [wggui] WITH GRANT OPTION
GO

-- [3GS]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [wg_interface] WITH GRANT OPTION 
GO
