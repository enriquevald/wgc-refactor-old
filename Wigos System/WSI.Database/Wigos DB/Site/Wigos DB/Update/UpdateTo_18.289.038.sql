/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 288;

SET @New_ReleaseId = 289;
SET @New_ScriptName = N'UpdateTo_18.289.038.sql';
SET @New_Description = N'Changes for coins collection; Added Regionalization for URUGUAYAN';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_expected_coin_amount')
BEGIN
  ALTER TABLE   [dbo].[money_collections] ADD mc_expected_coin_amount money NULL DEFAULT (0);
END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_collected_coin_amount')
BEGIN
  ALTER TABLE   [dbo].[money_collections] ADD mc_collected_coin_amount money NOT NULL DEFAULT (0);
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_coin_collection')
BEGIN
  ALTER TABLE   [dbo].[terminals] ADD te_coin_collection bit NOT NULL DEFAULT (0);
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_meter_delta]') and name = 'tmd_sas_meter_big_inc_cents_coin_acceptor')
BEGIN
  ALTER TABLE dbo.terminal_meter_delta ADD tmd_sas_meter_big_inc_cents_coin_acceptor BIGINT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_status]') and name = 'ts_coin_flags')
ALTER TABLE [dbo].terminal_status ADD
      ts_coin_flags Integer NOT NULL DEFAULT ((0));
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Redeem.SignaturesRoom')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Redeem.SignaturesRoom', '0');
GO

-- Concepto de B�veda ([11])
IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 11)
  INSERT INTO CAGE_CONCEPTS
              ( CC_CONCEPT_ID, CC_DESCRIPTION
              , CC_IS_PROVISION, CC_SHOW_IN_REPORT
              , CC_TYPE, CC_ENABLED, CC_NAME )
        VALUES
              ( 11, 'Monedas'
              , 0, 1
              , 0, 1, 'Monedas')
              
-- Relacion: MONEDAS-STACKERS ([11]-[12])
IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 11 AND CSTC_SOURCE_TARGET_ID = 12)
   INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
        ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
        , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY, CSTC_ENABLED )
   VALUES
        ( 11, 12
        , 0, 1, 1 )

-- Creacion del Meter
EXEC CageCreateMeters @pSourceTargetId = 12, @pConceptId = 11, @pCageSessionId = NULL, @CreateMetersOption = 1, @CurrencyForced = NULL      
GO

/************************  sas_meters_groups ****************/
UPDATE sas_meters_groups SET smg_name = 'Bills And Coins' WHERE smg_group_id = 10002
GO

/************************  sas_meters_catalog_per_group ****************/
--Meter Code = 0x08 (8) 
IF NOT EXISTS(SELECT smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 8 AND smcg_group_id = 10002)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10002, 8)
GO

DECLARE @cents_coinin       AS BIGINT
DECLARE @cents_coinin_order AS INT

SELECT @cents_coinin       = CAST(ISNULL(gp_key_value,'500000000') AS BIGINT) FROM general_params WHERE gp_group_key='SasMeter.BigIncrement' AND gp_subject_key = 'Cents.BillIn'
SELECT @cents_coinin_order = MAX(tmdd_order)+1 FROM terminal_meter_delta_description  WHERE tmdd_group = 0

UPDATE terminal_meter_delta SET tmd_sas_meter_big_inc_cents_coin_acceptor = @cents_coinin

IF NOT EXISTS (SELECT 1 FROM terminal_meter_delta_description WHERE tmdd_field_description = 'CoinAccepted')
BEGIN
  INSERT INTO terminal_meter_delta_description (tmdd_field_name, tmdd_field_description, tmdd_group, tmdd_order)
    VALUES('tmd_sas_meter_big_inc_cents_coin_acceptor', 'CoinAccepted', 0, @cents_coinin_order)
END  
GO

UPDATE terminal_meter_delta_description SET tmdd_field_description = REPLACE(tmdd_field_description, 'BillIn', 'BillAccepted')
WHERE tmdd_field_description like 'Bill%'
GO

-- ALARM_CATEGORY - Coins validator
IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 50 AND [alc_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
  VALUES (50, 10, 1, 0, N'Validador de monedas', N'', 1)
END 
GO

IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 50 AND [alc_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (50,  9, 1, 0, N'Coin validator', N'', 1)
END 
GO

-- Coin in tilt
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65569 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65569, 10, 0, N'Atasco de moneda', N'Atasco de moneda', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65569 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65569,  9, 0, N'Coin in tilt', N'Coin in tilt', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65569 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65569, 50, 0, GETDATE() )
END
GO

-- Reverse coin in detected
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65581 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65581, 10, 0, N'Regreso de moneda', N'Regreso de moneda', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65581 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65581,  9, 0, N'Reverse coin in detected', N'Reverse coin in detected', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65581 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65581, 50, 0, GETDATE() )
END
GO
-- Coin in lockout malfunction
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65657 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65657, 10, 0, N'Fallo en bloqueo de monedas', N'Fallo en bloqueo de monedas', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65657 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65657,  9, 0, N'Coin in lockout malfunction', N'Coin in lockout malfunction', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65657 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65657, 50, 0, GETDATE() )
END
GO

DECLARE @CurrentCurrency AS VARCHAR(3)
DECLARE @Allowed AS BIT
DECLARE Curs_Currencies CURSOR FOR SELECT CGC_ISO_CODE,CGC_ALLOWED FROM CAGE_CURRENCIES GROUP BY CGC_ISO_CODE,CGC_ALLOWED

OPEN Curs_Currencies
FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency, @Allowed
WHILE @@FETCH_STATUS = 0
   BEGIN
      IF NOT EXISTS (SELECT * FROM CAGE_CURRENCIES WHERE CGC_DENOMINATION = -50 AND CGC_ISO_CODE = @CurrentCurrency) 
        INSERT INTO CAGE_CURRENCIES (CGC_ISO_CODE , CGC_DENOMINATION , CGC_ALLOWED, CGC_CAGE_CURRENCY_TYPE) VALUES (@CurrentCurrency , -50 , @Allowed, 0) -- -50 reserved for Coins in money collections
      FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency, @Allowed
   END
CLOSE Curs_Currencies
DEALLOCATE Curs_Currencies
GO

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @nRows AS INT                              
 DECLARE @index AS INT                              
 DECLARE @tDays AS TABLE(NumDay INT)                       
 DECLARE @p2007Opening AS VARCHAR(MAX)

 SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
 SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
 
 SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @nRows                           
 BEGIN	                                          
   INSERT INTO @tDays VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT NumDay INTO #TempTable_Days FROM @tDays 
 
 SET @Sql = '
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PS_FINISHED
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID  
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_FROM < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '

EXECUTE (@Sql)

DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

ALTER PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + 
            '  AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 20
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

ALTER PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    SELECT REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 4), '|', '.') AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 3), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 2), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE  
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS                                                                                                          

    SELECT * FROM #TMP_STOCK
    ORDER BY CGS_ISO_CODE, CGS_CAGE_CURRENCY_TYPE ASC, CGS_DENOMINATION DESC
    , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
         ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock  
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CSM.CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
  ORDER BY CSM.CSM_SOURCE_TARGET_ID
         , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                   ) AS T1        
          GROUP BY ORIGIN, CMD_ISO_CODE

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC
         
    -- TABLE[4]: Other System Concepts           
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
        WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE

    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CM.CM_ISO_CODE
         , CM.CM_VALUE_IN
         , CM.CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  ORDER BY CM.CM_SOURCE_TARGET_ID
         , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC 
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                    ) AS T1
           GROUP BY ORIGIN,CMD_ISO_CODE

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB 
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC 
         
    -- TABLE[4]: Other System Concepts           
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CM_ISO_CODE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE


  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
END -- CageGetGlobalReportData
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_actualizacion_expediente
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[wsp_001_pld_actualizacion_expediente]
 (  
    @v_NumeroTarjeta NVARCHAR (50)		= NULL
  , @v_Cliente_Id    BIGINT						= NULL
  , @v_Folio         UNIQUEIDENTIFIER = NULL
  , @v_Date          DATETIME					= NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @v_Cliente_Id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    BEGIN TRY 
      UPDATE   ACCOUNTS
         SET   AC_EXTERNAL_AML_FILE_SEQUENCE = @v_Folio,
               AC_EXTERNAL_AML_FILE_UPDATED  = @v_Date
       WHERE   AC_ACCOUNT_ID                 = @_account_id
       
      SELECT   0 AS  Estado -- Operaci�n realizada con �xito
    END TRY
    BEGIN CATCH
      SELECT   12 AS Estado -- Error: no se ha podido realizar la operaci�n (error de SQL)
    END CATCH    
  END
END
GO

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MachineAndGameReport.sql
-- 
--   DESCRIPTION: Procedure for Daily Machine Report 
-- 
--        AUTHOR: Jordi Masachs 
-- 
-- CREATION DATE: 30-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-DEC-2014 JMV    First release
-- 01-DEC-2015 DLL    Fixed Bug TFS-7220: Remove meter promotion redimible
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE: Show meter history grouped by machine and by name
-- 
--  PARAMS:
--      - INPUT:
--  @pDateFrom DATETIME,  
--  @pDateTo   DATETIME,
--  @pTotalOnTop BIT = 0 
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
-- 
ALTER PROCEDURE [dbo].[MachineAndGameReport]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pTotalOnTop BIT = 0 
AS

BEGIN

-- Cash in fields
DECLARE @_ticket_in_RE INT
DECLARE @_ticket_in_pro_RE INT
DECLARE @_ticket_in_pro_NR INT
DECLARE @_credit_from_bills INT

-- Cash out fields
DECLARE @_ticket_out_RE INT
DECLARE @_ticket_out_pro_NR INT

-- Set field codes
SET @_ticket_in_RE = 0x80
SET @_ticket_in_pro_RE = 0x84
SET @_ticket_in_pro_NR = 0x82
SET @_credit_from_bills = 0x0B
SET @_ticket_out_RE = 0x86
SET @_ticket_out_pro_NR = 0x88

-- Create temp table for whole report
CREATE TABLE #TT_MACHINEGAME (TERMINAL_ID INT, TERMINAL_NAME NVARCHAR(50), GAMENAME NVARCHAR(50), CASH_IN MONEY, CASH_OUT MONEY, HANDPAYS MONEY, NETWIN MONEY)

-- Create temp table for terminal - game relation
CREATE TABLE #TT_GAMES (TTG_TERMINAL_ID INT, TTG_GAMENAME NVARCHAR(50))
INSERT INTO #TT_GAMES

    SELECT TGT_TERMINAL_ID AS TTG_TERMINAL_ID, CASE WHEN TOTAL > 1 THEN 'Multigame' ELSE TTG_GAMENAME END AS TTG_GAMENAME
    FROM
    (
        SELECT TGT_TERMINAL_ID, TOTAL, (SELECT TOP 1 PG_GAME_NAME FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID AND TGT_TERMINAL_ID = XX.TGT_TERMINAL_ID) AS TTG_GAMENAME
        FROM
        (
            SELECT TGT_TERMINAL_ID, count(*) AS TOTAL FROM
            (
              SELECT DISTINCT TGT_TERMINAL_ID, TGT_TRANSLATED_GAME_ID, PG_GAME_NAME
                FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES
               WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID
            ) YY
            GROUP BY TGT_TERMINAL_ID
        ) AS XX
    ) AS HH

-- Insert into the result values
INSERT INTO #TT_MACHINEGAME
	SELECT TSMH_TERMINAL_ID AS TERMINAL_ID, TE_NAME AS TERMINAL_NAME, (CASE WHEN TTG_GAMENAME IS NOT NULL THEN TTG_GAMENAME ELSE 'UNKNOWN' END) AS GAME_NAME, 
	CASH_IN, CASH_OUT, ISNULL(HANDPAYS, 0) AS HANDPAYS, CASH_IN - CASH_OUT - ISNULL(HANDPAYS, 0) AS NETWIN 
	FROM  
	(	SELECT TSMH_TERMINAL_ID, 
			 SUM(CASE WHEN 
				TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_IN,
			 SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_ticket_out_RE, @_ticket_out_pro_NR) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_OUT
		FROM	TERMINAL_SAS_METERS_HISTORY WITH (INDEX(IX_tsmh_type_code_datetime))
		WHERE	TSMH_TYPE = 1  
				AND TSMH_DATETIME >= @pDateFrom AND TSMH_DATETIME < @pDateTo 
				AND TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills, @_ticket_out_RE, @_ticket_out_pro_NR) 	
		GROUP BY TSMH_TERMINAL_ID ) AS TSMH_SUM 
	LEFT JOIN TERMINALS ON TSMH_TERMINAL_ID = TE_TERMINAL_ID
  LEFT JOIN #TT_GAMES ON TSMH_TERMINAL_ID = TTG_TERMINAL_ID
	LEFT JOIN 
	(
		SELECT 	
			HP_TERMINAL_ID,
			SUM(HP_AMOUNT) as HANDPAYS
			FROM HANDPAYS
			WHERE HP_DATETIME >= @pDateFrom AND HP_DATETIME < @pDateTo
		GROUP BY HP_TERMINAL_ID
		) AS HP_SUM ON TSMH_TERMINAL_ID = HP_TERMINAL_ID

-- Prepare report values and view
SELECT BLANK, CTYPE, CONCEPT, 
(CASE WHEN CASH_IN IS NOT NULL THEN CASH_IN ELSE 0 END) AS CASH_IN, 
(CASE WHEN CASH_OUT IS NOT NULL THEN CASH_OUT ELSE 0 END) AS CASH_OUT, 
(CASE WHEN HANDPAYS IS NOT NULL THEN HANDPAYS ELSE 0 END) AS HANDPAYS, 
(CASE WHEN NETWIN IS NOT NULL THEN NETWIN ELSE 0 END) AS NETWIN FROM
(
	SELECT '' as BLANK, 1 AS CTYPE, 2 AS CTYPE2, TERMINAL_NAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY TERMINAL_NAME

	UNION

	SELECT '' as BLANK, 2 AS CTYPE, 3 AS CTYPE2, GAMENAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY GAMENAME

	UNION

	SELECT '' as BLANK, 3 AS CTYPE, 1 AS CTYPE2, '' AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN
	FROM #TT_MACHINEGAME
	) X

ORDER BY CASE @pTotalOnTop WHEN 0 THEN CTYPE ELSE CTYPE2 END

DROP TABLE #TT_MACHINEGAME

DROP TABLE #TT_GAMES

END
GO
