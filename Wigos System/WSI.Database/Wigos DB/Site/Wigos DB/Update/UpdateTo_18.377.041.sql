/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 376;

SET @New_ReleaseId = 377;
SET @New_ScriptName = N'UpdateTo_18.377.041.sql';
SET @New_Description = N'GT_Session_Information;SP_AFIP_GAMING_TABLES'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

GO


IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;
GO


CREATE PROCEDURE [dbo].[GT_Session_Information]
(
    @pBaseType            INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pStatus              INTEGER
  , @pEnabled             INTEGER
  , @pAreaId              INTEGER
  , @pBankId              INTEGER
  , @pChipsISOCode        VARCHAR(50)
  , @pChipsCoinsCode      INTEGER
  , @pValidTypes          VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
DECLARE @_NATIONAL_CURRENCY AS  VARCHAR(3)

-- CHIP TYPES
DECLARE @_CHIPS_RE    AS INTEGER
DECLARE @_CHIPS_NR    AS INTEGER
DECLARE @_CHIPS_COLOR AS INTEGER

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL          AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL             AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN                     AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT                    AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT               AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION            AS   INTEGER
DECLARE @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    AS   INTEGER

DECLARE @_MOVEMENT_OPEN_SESSION                  AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION                 AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN_CLOSING_STOCK       AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT_CLOSING_STOCK      AS   INTEGER

----------------------------------------------------------------------------------------------------------------
-- Initialization --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)
SET @_CHIPS_RE         =   1001
SET @_CHIPS_NR         =   1002
SET @_CHIPS_COLOR      =   1003

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL          =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL             =   303
SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE =   310
SET @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    =   309
SET @_MOVEMENT_FILLER_IN                     =   2
SET @_MOVEMENT_FILLER_OUT                    =   3
SET @_MOVEMENT_CAGE_FILLER_OUT               =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION            =   201
SET @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    =   1000006 --- CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN + CageMeters.CageConceptId.Tips;

SET @_MOVEMENT_OPEN_SESSION                  =   0
SET @_MOVEMENT_CLOSE_SESSION                 =   1
SET @_MOVEMENT_FILLER_IN_CLOSING_STOCK       =   190
SET @_MOVEMENT_FILLER_OUT_CLOSING_STOCK      =   191




----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

-- GET NATIONAL CURRENCY
SELECT @_NATIONAL_CURRENCY = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode' 

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0
BEGIN
  -- REPORT BY GAMING TABLE SESSION
  SELECT    GTS_CASHIER_SESSION_ID
          , CS_NAME
          , CS_STATUS
          , GT_NAME
          , GTT_NAME
          , GT_HAS_INTEGRATED_CASHIER
          , CT_NAME
          , GU_USERNAME
          , CS_OPENING_DATE
          , CS_CLOSING_DATE
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_PURCHASE_AMOUNT     ELSE GTSC_TOTAL_PURCHASE_AMOUNT   END, 0)  )  AS GTS_TOTAL_PURCHASE_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_SALES_AMOUNT        ELSE GTSC_TOTAL_SALES_AMOUNT      END, 0)  )  AS GTS_TOTAL_SALES_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_INITIAL_CHIPS_AMOUNT      ELSE GTSC_INITIAL_CHIPS_AMOUNT    END, 0)  )  AS GTS_INITIAL_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FILLS_CHIPS_AMOUNT        ELSE GTSC_FILLS_CHIPS_AMOUNT      END, 0)  )  AS GTS_FILLS_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_CREDITS_CHIPS_AMOUNT      ELSE GTSC_CREDITS_CHIPS_AMOUNT    END, 0)  )  AS GTS_CREDITS_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FINAL_CHIPS_AMOUNT        ELSE GTSC_FINAL_CHIPS_AMOUNT      END, 0)  )  AS GTS_FINAL_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TIPS                      ELSE GTSC_TIPS                    END, 0)  )  AS GTS_TIPS               
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_COLLECTED_AMOUNT          ELSE GTSC_COLLECTED_AMOUNT        END, 0)  )  AS GTS_COLLECTED_AMOUNT   
          , GTS_CLIENT_VISITS
          , AR_NAME
          , BK_NAME
          , GTS_GAMING_TABLE_SESSION_ID
          , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
          , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END AS GTSC_ISO_CODE
          , dbo.GT_Calculate_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0), ISNULL(GTS_COLLECTED_AMOUNT, 0), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
   FROM         GAMING_TABLES_SESSIONS
   LEFT    JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT    JOIN GUI_USERS                           ON GU_USER_ID                  = CS_USER_ID
   LEFT    JOIN GAMING_TABLES                       ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT    JOIN GAMING_TABLES_TYPES                 ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT    JOIN AREAS                               ON GT_AREA_ID                  = AR_AREA_ID
   LEFT    JOIN BANKS                               ON GT_BANK_ID                  = BK_BANK_ID
   LEFT    JOIN CASHIER_TERMINALS                   ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CS_OPENING_DATE >= @_DATE_FROM
   AND   CS_OPENING_DATE < @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   GROUP BY GTS_CASHIER_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , GTS_GAMING_TABLE_SESSION_ID
         , GTS_OWN_SALES_AMOUNT
         , GTS_EXTERNAL_SALES_AMOUNT
         , GTS_COLLECTED_AMOUNT
         , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END 
     ORDER BY CS_OPENING_DATE
;

END
ELSE
BEGIN
  -- REPORT BY CASHIER MOVEMENTS
   
    /* Select de open or close operations */
    SELECT   CM_OPERATION_ID 	      AS OPERATION_ID
           , MIN(CASE CM_TYPE	
                 WHEN @_MOVEMENT_FILLER_IN_CLOSING_STOCK THEN	@_MOVEMENT_OPEN_SESSION
                 WHEN @_MOVEMENT_FILLER_OUT_CLOSING_STOCK THEN @_MOVEMENT_CLOSE_SESSION
                 ELSE CM_TYPE END)  AS MOV_TYPE                                                    
      INTO   #TABLE_MOVEMENTS	                                                                              
      FROM   CASHIER_MOVEMENTS   WITH(INDEX(IX_cm_date_type)) 
     WHERE   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
       AND   CM_TYPE IN ( @_MOVEMENT_OPEN_SESSION                                                                             
 						            , @_MOVEMENT_CLOSE_SESSION                                                                            
 						            , @_MOVEMENT_FILLER_IN_CLOSING_STOCK                                                                  
 						            , @_MOVEMENT_FILLER_OUT_CLOSING_STOCK                                                                 
                        )
       AND   CM_OPERATION_ID > 0      	                                                                            
  GROUP BY   CM_OPERATION_ID                         	                                                              
  ORDER BY   CM_OPERATION_ID     	                                                             

    /* Report data */
    SELECT   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL          THEN CM_ADD_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_PURCHASE_AMOUNT
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL             THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_SALES_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND MOV_TYPE                 = @_MOVEMENT_OPEN_SESSION
                      THEN CM_ADD_AMOUNT ELSE 0 END
                )  AS CM_INITIAL_CHIPS_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                       AND MOV_TYPE               IS  NULL
                      THEN CM_ADD_AMOUNT ELSE 0 END
                )  AS CM_FILLS_CHIPS_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                 = @_MOVEMENT_FILLER_OUT
                       AND MOV_TYPE               IS  NULL
                      THEN CM_SUB_AMOUNT
                      WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       AND MOV_TYPE               IS  NULL
                       THEN CM_INITIAL_BALANCE
                       ELSE 0
                       END
                )  AS CM_CREDITS_CHIPS_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                       AND MOV_TYPE                 = @_MOVEMENT_CLOSE_SESSION
                      THEN CM_SUB_AMOUNT
                      WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       AND MOV_TYPE                 = @_MOVEMENT_CLOSE_SESSION
                      THEN CM_INITIAL_BALANCE
                      ELSE 0 END
                )  AS CM_FINAL_CHIPS_AMOUNT
           , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_INITIAL_BALANCE
                      
                      WHEN CM_TYPE                  = @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS  
                      THEN CM_ADD_AMOUNT    
                      ELSE 0    
                 END
                )  AS CM_TIPS
           , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                          CASE WHEN CM_CURRENCY_ISO_CODE IS NULL
                                 OR (CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE
                                AND  CM_CAGE_CURRENCY_TYPE NOT IN (@_CHIPS_RE, @_CHIPS_NR, @_CHIPS_COLOR) ) THEN
                               CASE WHEN CM_TYPE  = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                                    ELSE 0
                               END
                               ELSE 0
                          END
                      WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                          CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                               WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                               ELSE 0
                          END
                      ELSE 0
                 END
                )  AS CM_COLLECTED_AMOUNT
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END AS CM_CURRENCY_ISO_CODE
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTS_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
      FROM         CASHIER_MOVEMENTS
      LEFT   JOIN  #TABLE_MOVEMENTS                   ON CM_OPERATION_ID             = OPERATION_ID
     INNER   JOIN  CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
     INNER   JOIN  GAMING_TABLES_SESSIONS             ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
      LEFT   JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID 
                                                     AND GTSC_TYPE <> @_CHIPS_COLOR
                                                     AND gtsc_iso_code = cm_currency_iso_code 
                                                     AND gtsc_type = cm_cage_currency_type
      LEFT   JOIN  GUI_USERS                          ON GU_USER_ID                  = CS_USER_ID
      LEFT   JOIN  GAMING_TABLES                      ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
      LEFT   JOIN  GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
      LEFT   JOIN  AREAS                              ON GT_AREA_ID                  = AR_AREA_ID
      LEFT   JOIN  BANKS                              ON GT_BANK_ID                  = BK_BANK_ID
      LEFT   JOIN  CASHIER_TERMINALS                  ON GT_CASHIER_ID               = CT_CASHIER_ID
     WHERE   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
       AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
       AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
       AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
       AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
       AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
  GROUP BY   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END
  ORDER BY   CS_OPENING_DATE
  
  DROP TABLE #TABLE_MOVEMENTS	   
  
END

END -- END PROCEDURE GT_Session_Information

GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO

GO

IF OBJECT_ID (N'dbo.[SP_AFIP_GAMING_TABLES]', N'P') IS NOT NULL
    DROP PROCEDURE dbo.[SP_AFIP_GAMING_TABLES];
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
(
  @pStarDateTime    DATETIME
)
AS
BEGIN
  --Get the current iso code in general params
  DECLARE @_Currency_ISO_Code           AS  VARCHAR(5);
  DECLARE @_Default_Cashier_Afip_Type   AS  INT;
  DECLARE @_GUI_User_Type_SYS_GamingTables   AS  INT;  
  DECLARE @_GUI_User_Type_SYS_TITO_User      AS  INT;  
  DECLARE @_GUI_User_Type_SYS_SYSTEM         AS  INT;  
  
  SET @_Default_Cashier_Afip_Type = 99;  
  
  
  SET @_GUI_User_Type_SYS_SYSTEM = 3;  
  SET @_GUI_User_Type_SYS_TITO_User = 4;  
  SET @_GUI_User_Type_SYS_GamingTables = 5; 

  SET @_Currency_ISO_Code = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' and GP_SUBJECT_KEY = 'CurrencyISOCode')

  --Get the total sum of the GAMING TABLES
  SELECT TABLE_ID
       , GTABLE_NAME
       , GTABLE_TYPE_ID
       , GTABLE_TYPE_DESC
       , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
       , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
       , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
       , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
       , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
       , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
       , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
       , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
       , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
       , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
       , CS_OPENING_DATE
       , CS_CLOSING_DATE
  FROM
  (
      --Get partial sum
      SELECT CM_TYPE
           , CM_INITIAL_BALANCE
           , CM_GAMING_TABLE_SESSION_ID
           , CM_CURRENCY_ISO_CODE
           , GT_GAMING_TABLE_ID AS TABLE_ID
           , GT_NAME AS GTABLE_NAME
           , GTT_REPORT_TYPE_ID AS GTABLE_TYPE_ID
           , GTT_NAME AS GTABLE_TYPE_DESC
           , CASE WHEN CM_TYPE = 0 THEN CM_INITIAL_BALANCE ELSE 0 END AS OPENING_CASH
           , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CASH
           , GTS_INITIAL_CHIPS_AMOUNT AS OPENING_CHIPS
           , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
           , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
           , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
           , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
           , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
           , CASE WHEN CM_TYPE in (300, 302, 307, 311) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS SALE_CHIPS_TOTAL
           , CASE WHEN CM_TYPE in (301) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS BUY_CHIPS_TOTAL
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
      FROM  GAMING_TABLES_TYPES
      INNER JOIN GAMING_TABLES            ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
      INNER JOIN GAMING_TABLES_SESSIONS   ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
      INNER JOIN CASHIER_MOVEMENTS        ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
      INNER JOIN CASHIER_SESSIONS         ON CM_SESSION_ID = CS_SESSION_ID
      WHERE CM_DATE >= @pStarDateTime
        AND CM_DATE < DATEADD(DAY, 1, @pStarDateTime)
        AND CM_TYPE IN (0, 201, 202, 203, 300, 301, 302, 307, 311)
      GROUP BY CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE,
            GT_GAMING_TABLE_ID, GT_NAME, GTT_REPORT_TYPE_ID, GTT_NAME, GTS_GAMING_TABLE_SESSION_ID, CS_OPENING_DATE,
            CS_CLOSING_DATE, GTS_CASHIER_SESSION_ID, GTS_INITIAL_CHIPS_AMOUNT
   ) AS GT
  GROUP BY  TABLE_ID, GTABLE_NAME, GTABLE_TYPE_ID, GTABLE_TYPE_DESC, CS_OPENING_DATE, CS_CLOSING_DATE

  UNION ALL

  --Get the total sum of the CASHIERS
  SELECT TABLE_ID
       , GTABLE_NAME
       , @_Default_Cashier_Afip_Type
       , TIPO_MESA
       , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
       , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
       , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
       , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
       , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
       , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
       , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
       , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
       , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
       , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
       , CS_OPENING_DATE
       , CS_CLOSING_DATE
  FROM
  (
      --Select partial sum
      SELECT CM_TYPE
           , CM_INITIAL_BALANCE
           , CM_GAMING_TABLE_SESSION_ID
           , CM_CURRENCY_ISO_CODE
           , CS_USER_ID AS TABLE_ID
           , GU_FULL_NAME AS GTABLE_NAME
           , 'OTROS' AS TIPO_MESA
           , CASE WHEN CM_TYPE = 0 THEN CM_INITIAL_BALANCE ELSE 0 END AS OPENING_CASH
           , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CASH
           , 0.00 AS OPENING_CHIPS
           , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
           , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
           , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
           , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
           , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
           , 0.00 AS SALE_CHIPS_TOTAL
           , 0.00 AS BUY_CHIPS_TOTAL
           , CS_OPENING_DATE, CS_CLOSING_DATE
      FROM CASHIER_MOVEMENTS
      INNER JOIN  CASHIER_SESSIONS  ON CM_SESSION_ID = CS_SESSION_ID
      INNER JOIN GUI_USERS          ON CS_USER_ID    = GU_USER_ID
      WHERE CM_DATE >= @pStarDateTime
        AND CM_DATE < DATEADD(DAY, 1, @pStarDateTime)
        AND CM_TYPE IN (0, 201, 202, 203, 300, 301, 302, 307, 311)
        AND GU_USER_TYPE NOT IN (@_GUI_User_Type_SYS_SYSTEM,@_GUI_User_Type_SYS_TITO_User,@_GUI_User_Type_SYS_GamingTables) 
      GROUP BY cs_session_id, CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE, CS_USER_ID, GU_FULL_NAME,
            CS_OPENING_DATE, CS_CLOSING_DATE
  ) AS GT
  GROUP BY TABLE_ID, GTABLE_NAME, TIPO_MESA, CS_OPENING_DATE, CS_CLOSING_DATE
END  -- End procedure SP_AFIP_GAMING_TABLES
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES] TO [WGGUI] WITH GRANT OPTION
GO


/******* TRIGGERS *******/

