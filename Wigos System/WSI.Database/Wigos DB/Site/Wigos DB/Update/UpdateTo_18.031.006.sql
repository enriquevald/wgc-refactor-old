/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 30;

SET @New_ReleaseId = 31;
SET @New_ScriptName = N'UpdateTo_18.031.006.sql';
SET @New_Description = N'Provider-based Promotions and Draws.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Promotions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_provider_list')
ALTER TABLE [dbo].[promotions]
	ADD [pm_provider_list] [xml] NULL;
ELSE
SELECT '***** Field promotions.pm_provider_list already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_offer_list')
ALTER TABLE [dbo].[promotions]
	ADD [pm_offer_list] [xml] NULL;
ELSE
SELECT '***** Field promotions.pm_offer_list already exists *****';


/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_provider_list')
ALTER TABLE [dbo].[draws]
	ADD [dr_provider_list] [xml] NULL;	
ELSE
SELECT '***** Field draws.dr_provider_list already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_offer_list')
ALTER TABLE [dbo].[draws]
	ADD [dr_offer_list] [xml] NULL;	
ELSE
SELECT '***** Field draws.dr_offer_list already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_number_points')
ALTER TABLE [dbo].[draws]
	ADD [dr_number_points] [money] NULL;	
ELSE
SELECT '***** Field draws.dr_number_points already exists *****';

/* PK Draws */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = N'PK_draws' AND type = 'PK')
ALTER TABLE [dbo].[draws]
	ADD CONSTRAINT 
		PK_draws PRIMARY KEY CLUSTERED 
		( dr_id 
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ELSE
SELECT '***** Constraint draws.PK_draws already exists *****';


/****** INDEXES ******/

/****** Object:  Index [IX_dr_status_date]    Script Date: 04/12/2011 11:02:47 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[draws]') AND name = N'IX_dr_status_date')
CREATE NONCLUSTERED INDEX [IX_dr_status_date] ON [dbo].[draws] 
(
	[dr_status] ASC,
	[dr_starting_date] ASC,
	[dr_ending_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index draws.IX_dr_status_date already exists *****';
GO

/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/

/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='MinCashableCents')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'MinCashableCents'
           ,'0');
ELSE
SELECT '***** Record Cashier_MinCashableCents already exists *****';

UPDATE   [dbo].[draws]
   SET   DR_NUMBER_POINTS = (SELECT GI_POINTS FROM GIFTS WHERE GI_TYPE = 2)
 WHERE   DR_CREDIT_TYPE   = 3
   AND   DR_NUMBER_PRICE  = 0
   AND   DR_NUMBER_POINTS IS NULL;
