/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 27;

SET @New_ReleaseId = 28;
SET @New_ScriptName = N'UpdateTo_18.028.006.sql';
SET @New_Description = N'New index on account operations';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


ALTER TABLE dbo.terminals_pending ADD
	tp_terminal_type smallint NOT NULL CONSTRAINT DF_terminals_pending_tp_terminal_type DEFAULT 3
GO

/****** Mark all 3GS Terminals as type 3 ******/
UPDATE TERMINALS 
   SET TE_TERMINAL_TYPE = 3
 WHERE TE_TERMINAL_ID IN (SELECT T3GS_TERMINAL_ID FROM TERMINALS_3GS)
   AND TE_TERMINAL_TYPE = 1
GO

/****** Set the retirement date of all previously retired terminals ******/
UPDATE   TERMINALS
   SET   TE_RETIREMENT_DATE      = (SELECT   ISNULL(MAX(TC_DATE)+1, CONVERT (DATETIME, ROUND((CONVERT (REAL, GETDATE(), 0)), 0, 1))) 
                                      FROM   TERMINALS_CONNECTED 
                                     WHERE   TC_TERMINAL_ID = TE_TERMINAL_ID) 
       , TE_RETIREMENT_REQUESTED = GETDATE () 
  FROM TERMINALS 
 WHERE TE_STATUS               = 2
   AND TE_RETIREMENT_DATE      IS NULL
   AND TE_RETIREMENT_REQUESTED IS NULL
GO

/****** Insert terminal types ******/
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE =  -1) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES ( -1, 'Unknown')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE =   1) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (  1, 'LKT WIN')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE =   3) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (  3, '3GS')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE =   5) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (  5, 'LKT SAS Host')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE = 100) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (100, 'SITE')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE = 101) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (101, 'SITE JACKPOT')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE = 102) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (102, 'MOBILE BANK')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE = 103) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (103, 'iMB')
IF NOT EXISTS (SELECT TT_TYPE FROM TERMINAL_TYPES WHERE TT_TYPE = 104) INSERT INTO TERMINAL_TYPES (TT_TYPE, TT_NAME) VALUES (104, 'iSTATS')



/****** INDEXES ******/

/****** TRIGGERS ******/


/****** RECORDS ******/
   
