/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 272;

SET @New_ReleaseId = 273;
SET @New_ScriptName = N'UpdateTo_18.273.038.sql';
SET @New_Description = N'Update GP';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* PROCEDURES *******/
-- Cash Withdrawal
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashDeskWithdraw.Footer')
  UPDATE GENERAL_PARAMS SET GP_SUBJECT_KEY = 'CashWithdrawal.Footer' WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashDeskWithdraw.Footer'
ELSE
  IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashWithdrawal.Footer')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','CashWithdrawal.Footer','');
GO

-- Cash Deposit
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashDeskOpen.Footer')
  UPDATE GENERAL_PARAMS SET GP_SUBJECT_KEY = 'CashDeposit.Footer' WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashDeskOpen.Footer'
ELSE
  IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CashDeposit.Footer')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','CashDeposit.Footer','');
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_ficha_cliente
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_ficha_cliente]') AND type in (N'P', N'PC'))

DROP PROCEDURE [dbo].[wsp_001_pld_ficha_cliente]
GO

CREATE PROCEDURE [dbo].wsp_001_pld_ficha_cliente
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT

  SET @_account_id = @v_Cliente_Id

  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
  END

  IF @v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id
  BEGIN
    SET @_account_id = NULL
  END
  
SELECT   ac_account_id AS numCuenta,
         ac_holder_name3 AS Nombre,
         ac_holder_name1 AS ApellidoPaterno,
         ac_holder_name2 AS ApellidoMaterno,
         ac_holder_gender AS Genero,
         ac_holder_birth_date AS FechaNacimiento,
         '' AS cveEstadoNac,
         '' AS descEstadoNac,
         ac_holder_id2 as Curp,
         ac_holder_email_01 AS CorreoPrincipal,
         ac_holder_email_02 AS CorreoAlterno,
         ac_holder_phone_number_01 as TelefonoMovil,
         ac_holder_phone_number_02 as TelefonoFijo,
         'F' as cveTipoPersona,
         'FISICA' as descTipoPersona,
         oc_code as cveActEconomica,
         ac_holder_id1 as RFC,
         '' as cveNacionalidad,
         co_name as descNacionalidad,
         ac_holder_address_01 as Calle,
         ac_holder_ext_num as NumExterior,
         '' as NumInterior,
         ac_holder_address_02 as Colonia,
         '' as cvePais,
         '' as descPais,
         '' as cveEstado,
         fs_name as descEstado,
         '' as cveMunicipio,
         ac_holder_city as descMunicipio,
         ac_holder_id_type as cveTipoIdentificacion,
         idt_name as descTipoIdentificacion,
         '' as AutoridadEmisora,
         ac_holder_id as NumIdentificacion
  FROM   accounts
  LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
  LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
  LEFT JOIN identification_types on ac_holder_id_type = idt_id
  LEFT JOIN occupations on oc_id = ac_holder_occupation_id
 WHERE   ac_account_id = @_account_id

END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_ficha_cliente] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_bloqueo_desbloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta]
 (  
    @v_Account_Id    BIGINT        = NULL
  , @v_Block         BIT           = NULL
  , @v_Block_Reason  INT           = NULL
  , @v_Desc          NVARCHAR(256) = NULL
  , @v_Movement_Type INT           = NULL
 )
AS
BEGIN
  
DECLARE @v_Balance MONEY
  
BEGIN TRY 
  -- Block / Unblock
  UPDATE   ACCOUNTS
     SET   ac_blocked           = @v_Block,
           ac_block_reason      = CASE WHEN @v_Block = 0 THEN 0 ELSE AC_BLOCK_REASON | @v_Block_Reason END,
           ac_block_description = @v_Desc
   WHERE   AC_ACCOUNT_ID        = @v_Account_Id
  
  -- Points
  SET @v_Balance = ( SELECT   AC_BALANCE
                       FROM   ACCOUNTS
                      WHERE   AC_ACCOUNT_ID = @v_Account_Id
                  )
  -- Movement
  INSERT INTO   ACCOUNT_MOVEMENTS
              ( AM_PLAY_SESSION_ID, AM_ACCOUNT_ID, AM_TERMINAL_ID, AM_WCP_SEQUENCE_ID, AM_WCP_TRANSACTION_ID, AM_DATETIME
              , AM_TYPE, AM_INITIAL_BALANCE, AM_SUB_AMOUNT, AM_ADD_AMOUNT, AM_FINAL_BALANCE, AM_CASHIER_ID, AM_CASHIER_NAME
              , AM_TERMINAL_NAME, AM_OPERATION_ID, AM_DETAILS, AM_REASONS, AM_UNDO_STATUS, AM_TRACK_DATA)
       VALUES 
              ( NULL, @v_Account_Id, NULL, NULL, NULL, GETDATE()
              , @v_Movement_Type, @v_Balance, 0, 0, @v_Balance, NULL, NULL
              , NULL, 0, @v_Desc, NULL, NULL, NULL)  
  
  -- Return
  SELECT   0 AS  Estado -- Operaci�n realizada con �xito
  
END TRY
BEGIN CATCH
  SELECT   12 AS Estado -- Error: no se ha podido realizar la operaci�n (error de SQL)
END CATCH

END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_bloqueo_desbloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_bloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_bloqueo_cuenta]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[wsp_001_pld_bloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_bloqueo_cuenta]
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
  , @v_Reason        NVARCHAR(256) = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @_account_id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    EXEC wsp_001_pld_bloqueo_desbloqueo_cuenta @_account_id, 1, 0x40000, @v_Reason, 75 -- MovementType.AccountBlocked
  END
END
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_bloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO

--------------------------------------------------------------------------------  
-- Copyright � 2015 Win Systems International  
--------------------------------------------------------------------------------  
--   
--    QUERY NAME: wsp_001_pld_desbloqueo_cuenta
--        AUTHOR: Alberto Marcos
-- CREATION DATE: 15-MAY-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-MAY-2015 AMF    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wsp_001_pld_desbloqueo_cuenta]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[wsp_001_pld_desbloqueo_cuenta]
GO

CREATE PROCEDURE [dbo].[wsp_001_pld_desbloqueo_cuenta]
 (  
    @v_NumeroTarjeta NVARCHAR (50) = NULL
  , @v_Cliente_Id    BIGINT        = NULL
  , @v_Reason        NVARCHAR(256) = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @_account_id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    EXEC wsp_001_pld_bloqueo_desbloqueo_cuenta @_account_id, 0, 0, @v_Reason, 76 -- MovementType.AccountUnblocked
  END
END
GO
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
  BEGIN
    GRANT EXECUTE ON [dbo].[wsp_001_pld_desbloqueo_cuenta] TO [wg_datareader] WITH GRANT OPTION
  END
GO
