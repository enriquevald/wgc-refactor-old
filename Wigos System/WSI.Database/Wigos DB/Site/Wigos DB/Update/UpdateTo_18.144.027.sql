/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 143;

SET @New_ReleaseId = 144;
SET @New_ScriptName = N'UpdateTo_18.144.027.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

CREATE TABLE [dbo].[cashier_movements_grouped_by_date](
	[CM_DATE] [datetime] NULL,
	[CM_TYPE] [int] NOT NULL,
	[CM_MONEY_TYPE] [int] NOT NULL,
	[CM_CURRENCY_ISO_CODE] [nvarchar](3) NULL,
	[CM_SUB_AMOUNT] [money] NULL,
	[CM_ADD_AMOUNT] [money] NULL,
	[CM_AUX_AMOUNT] [money] NULL,
	[CM_INITIAL_BALANCE] [money] NULL,
	[CM_FINAL_BALANCE] [money] NULL
) ON [PRIMARY]
GO

--
-- GROUPS
--

CREATE TABLE [dbo].[groups](
	[gr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gr_content_type] [int] NOT NULL,
	[gr_type] [int] NOT NULL CONSTRAINT [DF_groups_gr_type]  DEFAULT ((0)),
	[gr_name] [nvarchar](50) NOT NULL,
	[gr_updated] [datetime] NOT NULL CONSTRAINT [DF_groups_gr_updated]  DEFAULT (getdate()),
	[gr_expanded] [datetime] NULL,
	[gr_description] [nvarchar](255) NULL,
	[gr_enabled] [bit] NOT NULL,
 CONSTRAINT [PK_groups] PRIMARY KEY CLUSTERED 
(
	[gr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[group_elements](
	[ge_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ge_element_type] [int] NOT NULL,
	[ge_element_id] [bigint] NOT NULL,
	[ge_group_id] [bigint] NOT NULL,
	[ge_query] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_group_elements] PRIMARY KEY CLUSTERED 
(
	[ge_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[group_elements]  WITH CHECK ADD  CONSTRAINT [FK_group_elements_groups] FOREIGN KEY([ge_group_id])
	REFERENCES [dbo].[groups] ([gr_id])
GO

ALTER TABLE [dbo].[group_elements] CHECK CONSTRAINT [FK_group_elements_groups]
GO

CREATE TABLE [dbo].[terminal_groups](
	[tg_terminal_id] [int] NOT NULL,
	[tg_element_id] [bigint] NOT NULL,
	[tg_element_type] [int] NOT NULL,
 CONSTRAINT [PK_terminal_groups] PRIMARY KEY CLUSTERED 
(
	[tg_terminal_id] ASC,
	[tg_element_id] ASC,
	[tg_element_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--
-- Promociones UNR
--

ALTER TABLE dbo.account_promotions ADD
      acp_prize_type int NULL,
      acp_prize_gross money NULL,
      acp_prize_tax1 money NULL,
      acp_prize_tax2 money NULL
GO

--
-- Regalos a diferente precio por Nivel
--

ALTER TABLE GIFTS ADD
      gi_points_level1 MONEY NULL,
      gi_points_level2 MONEY NULL,
      gi_points_level3 MONEY NULL,
      gi_points_level4 MONEY NULL
GO

UPDATE   GIFTS
   SET   GI_POINTS_LEVEL1 = GI_POINTS
       , GI_POINTS_LEVEL2 = GI_POINTS
       , GI_POINTS_LEVEL3 = GI_POINTS
       , GI_POINTS_LEVEL4 = GI_POINTS
GO

ALTER TABLE GIFTS ALTER COLUMN gi_points MONEY NULL
GO

/****** CONSTRAINTS ******/

/****** INDEXES ******/

--
--
--

IF    NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wxp_001_messages]') AND name = N'IX_wxm_datetime')
  AND NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wxp_001_messages]') AND name = N'IX_wxm_terminal_type_status')
  AND NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wxp_001_messages]') AND name = N'IX_wxm_type_status')

BEGIN
  CREATE NONCLUSTERED INDEX [IX_wxm_datetime] ON [dbo].[wxp_001_messages] 
  (
        [wxm_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

  CREATE NONCLUSTERED INDEX [IX_wxm_terminal_type_status] ON [dbo].[wxp_001_messages] 
  (
        [wxm_terminal_id] ASC,
        [wxm_type] ASC,
        [wxm_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

  CREATE NONCLUSTERED INDEX [IX_wxm_type_status] ON [dbo].[wxp_001_messages] 
  (
        [wxm_type] ASC,
        [wxm_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

  ALTER TABLE dbo.terminals ADD te_wxp_reported bit NOT NULL CONSTRAINT DF_terminals_te_wxp_reported DEFAULT 0

END
GO

/****** STORED PROCEDURES ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateCashierMovementsGroupedPerDate]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[CreateCashierMovementsGroupedPerDate]
GO

CREATE PROCEDURE [dbo].[CreateCashierMovementsGroupedPerDate]
AS
BEGIN
    DECLARE @Date0   DATETIME
    DECLARE @Date1   DATETIME
	DECLARE @Date2   DATETIME
	DECLARE @LastDay DATETIME

	SET NOCOUNT ON;

	SET @Date2 = dbo.Opening (0, GETDATE ())
	IF DATEDIFF (MINUTE, @Date2, GETDATE()) < 15 
	RETURN

	SET @Date0 = DATEADD (DAY, -1, @Date2)
	
	SELECT @LastDay = MAX(CM_DATE) FROM CASHIER_MOVEMENTS_GROUPED_BY_DATE WHERE CM_DATE >= DATEADD (DAY, -2, @Date2)
	IF @LastDay IS NOT NULL
		SET @Date1 = DATEADD (DAY, +1, @LastDay)
    ELSE
		SET @Date1 = @Date0

	IF @Date1 >= @Date2
	RETURN

	INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_DATE 
                ( CM_DATE
				, CM_TYPE, CM_MONEY_TYPE, CM_CURRENCY_ISO_CODE
				, CM_SUB_AMOUNT
		        , CM_ADD_AMOUNT
			    , CM_AUX_AMOUNT
				, CM_INITIAL_BALANCE
				, CM_FINAL_BALANCE
				)
		 SELECT   @Date0 CM_DATE
		 	    , CM_TYPE, CM_MONEY_TYPE, CM_CURRENCY_ISO_CODE
		  	    , SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
			    , SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
			    , SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
			    , SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
			    , SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
		   FROM   CASHIER_MOVEMENTS
		  WHERE   CM_DATE >= @Date1
		    AND   CM_DATE <  @Date2
	   GROUP BY   CM_TYPE, CM_MONEY_TYPE, CM_CURRENCY_ISO_CODE

END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsInAccount.sql
--
--   DESCRIPTION: Update points of the account in site 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 03-SEP-2013 JML    Avoid excessive points generated by connection problems.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsInAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsInAccount]
GO
CREATE PROCEDURE [dbo].[Update_PointsInAccount]
  @pMovementId                 BIGINT
, @pAccountId                  BIGINT
, @pErrorCode                  INT         
, @pPointsSequenceId           BIGINT      
, @pReceivedPoints             MONEY          

AS
BEGIN   
  DECLARE @LocalDelta       AS MONEY
  DECLARE @ActualPoints     AS MONEY
  DECLARE @ReceivedAddLocal AS MONEY
  DECLARE @NewPoints        AS MONEY


  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN
  
  DECLARE @PrevSequence as BIGINT
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_POINTS = AC_POINTS + 0 WHERE AC_ACCOUNT_ID = @pAccountId

  -- SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  SELECT   @PrevSequence = ISNULL(AC_MS_POINTS_SEQ_ID, 0) 
         , @ActualPoints = AC_POINTS 
    FROM   ACCOUNTS 
   WHERE   AC_ACCOUNT_ID = @pAccountId
  
  IF ( @PrevSequence > @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM(am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,40,41,46,50,60,61,66) -- Not included level movements,(39)PointsGiftDelivery, 
                                                      -- (42)PointsGiftServices and (67)PointsStatusChanged
  
  SET @ReceivedAddLocal = @pReceivedPoints + ISNULL(@LocalDelta , 0) 
  
  SET @NewPoints = CASE WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @ActualPoints    <= @pReceivedPoints AND @ActualPoints    <= @ReceivedAddLocal)
                             THEN @ActualPoints
                        WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @pReceivedPoints <= @ActualPoints    AND @pReceivedPoints <= @ReceivedAddLocal)
                             THEN @pReceivedPoints
                        ELSE @ReceivedAddLocal 
                        END 
  
  UPDATE   ACCOUNTS
     SET   AC_POINTS                  = @NewPoints
         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId

END
GO

/****** TRIGGERS ******/

/****** RECORDS ******/ 

--
--
--

IF NOT EXISTS (SELECT 1 FROM TERMINALS WHERE TE_WXP_REPORTED = 1)
  UPDATE   TERMINALS
     SET   TE_WXP_REPORTED = 1 
   WHERE   TE_TERMINAL_ID IN (SELECT DISTINCT WXM_TERMINAL_ID FROM WXP_001_MESSAGES WHERE WXM_TYPE = 1) --- E-Frame

--
-- Action for Upgrade/Downgrade level
--   


IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level01.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level01.UpgradeDowngradeAction',0)

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level02.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level02.UpgradeDowngradeAction',0)
	
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level03.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level03.UpgradeDowngradeAction',0)

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level04.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level04.UpgradeDowngradeAction',0)

--
-- Disable PIN request for anonymous player
--

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='Account' AND gp_subject_key = 'RequestPin.DisabledForAnonymous')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account', 'RequestPin.DisabledForAnonymous', '0')

--
--
--

IF NOT EXISTS (SELECT 1 FROM currency_exchange WHERE ce_type = 2 AND ce_currency_iso_code = 'MXN')
BEGIN
  INSERT INTO [currency_exchange]
           ([ce_type]
           ,[ce_currency_iso_code]
           ,[ce_description]
           ,[ce_change]
           ,[ce_num_decimals]
           ,[ce_variable_commission]
           ,[ce_fixed_commission]
           ,[ce_variable_nr2]
           ,[ce_fixed_nr2]
           ,[ce_status])
     VALUES
           (2
           , 'MXN'
           , 'Cheque'
           , 1
           , 2
           , 0
           , 0
           , 0
           , 0
           , 0)
END
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.Check.Title')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'PaymentMethod.Check.Title', 'Recarga con Cheque')

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.Check.HideVoucher')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'PaymentMethod.Check.HideVoucher', '0')

--
-- Working shift
--

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'WorkShiftDurationHours')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'WorkShiftDurationHours', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'MaxDailyCashOpenings')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'MaxDailyCashOpenings', '0');
  
  
  
  
/*** Table - cashdesk_draws ***/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND type in (N'U'))
DROP TABLE [dbo].[cashdesk_draws]
GO

CREATE TABLE [dbo].[cashdesk_draws](
       [cd_site_id] [int] NOT NULL,
       [cd_draw_id] [bigint] NOT NULL,
       [cd_operation_id] [bigint] NOT NULL,
       [cd_account_id] [bigint] NOT NULL,
       [cd_draw_datetime] [datetime] NOT NULL,
       [cd_combination_bet] [nvarchar](500) NOT NULL,
       [cd_combination_won] [nvarchar](500) NOT NULL,
       [cd_re_bet] [money] NULL,
       [cd_nr_bet] [money] NULL,
       [cd_re_won] [money] NULL,
       [cd_nr_won] [money] NULL,
       [cd_prize_id] [bigint] NULL,
       [cd_upload_to_ext_db] [int] NULL
CONSTRAINT [PK_cashdesk_draws] PRIMARY KEY CLUSTERED 
(
       [cd_site_id] ASC, 
       [cd_draw_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*** Table Index - CASHDESK_DRAWS ***/
CREATE NONCLUSTERED INDEX [IX_ACCOUNT_DATETIME] ON [DBO].[CASHDESK_DRAWS] 
(
	[CD_ACCOUNT_ID] ASC,
	[CD_DRAW_DATETIME] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_DRAW_DATETIME] ON [DBO].[CASHDESK_DRAWS] 
(
	[CD_DRAW_DATETIME] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_ID_DATETIME] ON [DBO].[CASHDESK_DRAWS] 
(
	[CD_DRAW_ID] ASC,
	[CD_DRAW_DATETIME] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_UPLOAD_TO_EXT_DB] ON [DBO].[CASHDESK_DRAWS] 
(
      [CD_UPLOAD_TO_EXT_DB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/*** Alter Table - account_promotions ***/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND type in (N'U'))
BEGIN
  IF NOT EXISTS (SELECT * FROM sys.columns WHERE (Name = N'acp_prize_type' OR Name = N'acp_prize_gross' OR Name = N'acp_prize_tax1' OR Name = N'acp_prize_tax2') AND object_id = OBJECT_ID(N'[dbo].[account_promotions]'))
  BEGIN
    ALTER TABLE dbo.account_promotions ADD
          acp_prize_type int NULL,
          acp_prize_gross money NULL,
          acp_prize_tax1 money NULL,
          acp_prize_tax2 money NULL
  END
  END
GO

/*** SP - ReportCashDeskDraws ***/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO

CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL

AS
BEGIN

	SET NOCOUNT ON;

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))

	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account
	--
	BEGIN		

		INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)

		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

		SELECT 
		    CD_DRAW_ID
		  , CD_DRAW_DATETIME
		  , CM_CASHIER_NAME		  
		  , CM_USER_NAME
		  , CD_OPERATION_ID		
		  , CM_DATE
		  , CD_ACCOUNT_ID
		  , AC_HOLDER_NAME
		  , CD_COMBINATION_BET
		  , CD_COMBINATION_WON
		  , CD_RE_BET
		  , CD_RE_WON
		  , CD_NR_WON
		FROM
		   CASHDESK_DRAWS
		   WITH(INDEX(IX_ACCOUNT_DATETIME))
		    INNER JOIN CASHIER_MOVEMENTS ON CD_OPERATION_ID = CM_OPERATION_ID  
		    INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		WHERE    
			CD_DRAW_DATETIME >= @pDrawFrom
		   AND
			CD_DRAW_DATETIME < @pDrawTo 
		   AND
			((@pDrawId IS NULL) OR (CD_DRAW_ID=@pDrawId))
		   AND
			((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		   AND
			((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		   AND ( CM_TYPE = @pCashierMovement )
		ORDER BY CD_DRAW_ID

		DROP TABLE #ACCOUNTS_TEMP

		RETURN
		
	END 
	
	IF 	((@pDrawFrom IS NOT NULL) OR (@pDrawTo IS NOT NULL))
	--
	-- Filtering by draw date, but not by account
	--
	BEGIN
		
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	    
		SELECT 
		    CD_DRAW_ID
		  , CD_DRAW_DATETIME
		  , CM_CASHIER_NAME		  
		  , CM_USER_NAME
		  , CD_OPERATION_ID		
		  , CM_DATE
		  , CD_ACCOUNT_ID
		  , AC_HOLDER_NAME
		  , CD_COMBINATION_BET
		  , CD_COMBINATION_WON
		  , CD_RE_BET
		  , CD_RE_WON
		  , CD_NR_WON
		FROM
		   CASHDESK_DRAWS
		   WITH(INDEX(IX_ACCOUNT_DATETIME))
		    INNER JOIN CASHIER_MOVEMENTS ON CD_OPERATION_ID = CM_OPERATION_ID  
			INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID			    
		WHERE     
			CD_DRAW_DATETIME >= @pDrawFrom
		   AND
			CD_DRAW_DATETIME < @pDrawTo	
		   AND
			((@pDrawId IS NULL) OR (CD_DRAW_ID=@pDrawId))
		   AND
			((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		   AND
			((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		   AND ( CM_TYPE = @pCashierMovement )
		ORDER BY CD_DRAW_ID		

		DROP TABLE #ACCOUNTS_TEMP

		RETURN
		
	END 

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID, but not by draw date neither account
	--
	BEGIN		

		SET @pDrawFrom = '2007-01-01T00:00:00'
		SET @pDrawTo   = GETDATE ()

		SELECT 
		    CD_DRAW_ID
		  , CD_DRAW_DATETIME
		  , CM_CASHIER_NAME		  
		  , CM_USER_NAME
		  , CD_OPERATION_ID		
		  , CM_DATE
		  , CD_ACCOUNT_ID
		  , AC_HOLDER_NAME
		  , CD_COMBINATION_BET
		  , CD_COMBINATION_WON
		  , CD_RE_BET
		  , CD_RE_WON
		  , CD_NR_WON
		FROM
		   CASHDESK_DRAWS
		   WITH(INDEX(IX_ACCOUNT_DATETIME))
		    INNER JOIN CASHIER_MOVEMENTS ON CD_OPERATION_ID = CM_OPERATION_ID  
		    INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID	
		WHERE    
		    CD_DRAW_ID = @pDrawId	
		   AND 
			CD_DRAW_DATETIME >= @pDrawFrom
		   AND
			CD_DRAW_DATETIME < @pDrawTo	
		   AND
			((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		   AND
			((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		   AND ( CM_TYPE = @pCashierMovement )		   
		ORDER BY CD_DRAW_ID

		DROP TABLE #ACCOUNTS_TEMP

		RETURN
		
	END 		

	--
	-- Filter without Date or Account or Draw ID
	--
	SET @pDrawFrom = '2007-01-01T00:00:00'
	SET @pDrawTo   = GETDATE ()
	
	SELECT 
	    CD_DRAW_ID
	  , CD_DRAW_DATETIME
	  , CM_CASHIER_NAME		  
	  , CM_USER_NAME
	  , CD_OPERATION_ID		
	  , CM_DATE
	  , CD_ACCOUNT_ID
	  , AC_HOLDER_NAME
	  , CD_COMBINATION_BET
	  , CD_COMBINATION_WON
	  , CD_RE_BET
	  , CD_RE_WON
	  , CD_NR_WON
	FROM
	   CASHDESK_DRAWS
	   WITH(INDEX(IX_ACCOUNT_DATETIME))
	    INNER JOIN CASHIER_MOVEMENTS ON CD_OPERATION_ID = CM_OPERATION_ID  
	    INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID	
	  
	WHERE     
		CD_DRAW_DATETIME >= @pDrawFrom
	   AND
		CD_DRAW_DATETIME < @pDrawTo	
	   AND
		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	   AND
		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	   AND ( CM_TYPE = @pCashierMovement )		   	
	ORDER BY CD_DRAW_ID

	DROP TABLE #ACCOUNTS_TEMP

END
GO

GRANT EXECUTE ON [dbo].[ReportCashDeskDraws] TO [wggui] WITH GRANT OPTION
GO

/*** General Params ***/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='ServerAddress1')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'ServerAddress1' ,'');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='ServerAddress2')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'ServerAddress2' ,'');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'Enabled' ,'0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='ShowCashDeskDraws')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'ShowCashDeskDraws' ,'1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='VoucherOnCashDeskDraws.Winner')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'VoucherOnCashDeskDraws.Winner' ,'1');
GO 

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='VoucherOnCashDeskDraws.Loser')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'VoucherOnCashDeskDraws.Loser' ,'1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AskForParticipation')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'AskForParticipation' ,'1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='ActionOnServerDown')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' , 'ActionOnServerDown','0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='ServerBDConnectionString')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' , 'ServerBDConnectionString','');
GO
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='LocalServer')
INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.Draw' ,'LocalServer' ,'1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='WinnerPrizeVoucherTitle')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'WinnerPrizeVoucherTitle' ,'Sorteo en caja');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='LoserPrizeVoucherTitle')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'LoserPrizeVoucherTitle' ,'Cortes�a sorteo en caja');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AccountingMode')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'AccountingMode' ,'1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AccountingMode.KindOfNameMovement')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'AccountingMode.KindOfNameMovement' ,'Premio en especie');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AccountingMode.PromoNameMovement')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'AccountingMode.PromoNameMovement' ,'Promoci�n');
GO

IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.HideCashDeskDrawWinner')
INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.Draw' ,'Voucher.HideCashDeskDrawWinner' ,'0');
GO

IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.HideCashDeskDrawLoser')
INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.Draw' ,'Voucher.HideCashDeskDrawLoser' ,'0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.UNRLabel')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.Draw', 'Voucher.UNRLabel', 'UNR');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='NumberOfParticipants')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'NumberOfParticipants', '10000');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='NumberOfWinners')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'NumberOfWinners', '9999');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='TotalsBallsNumber')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'TotalsBallsNumber', '90');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='BallsExtracted')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'BallsExtracted', '20');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='BallsOfParticipant')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'BallsOfParticipant', '5');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize1.Fixed')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '2');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize1.Percentage')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize2.Fixed')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '4');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize2.Percentage')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize3.Fixed')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '6');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize3.Percentage')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize4.Fixed')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration' ,'WinnerPrize4.Fixed', '8');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize4.Percentage')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize1.Enabled')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize2.Enabled')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize3.Enabled')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize4.Enabled')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize1.PrizeType')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '3');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize2.PrizeType')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '3');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize3.PrizeType')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '3');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='WinnerPrize4.PrizeType')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '3');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='LoserPrize1.Enabled')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='LoserPrize1.Fixed')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration ', 'LoserPrize1.Fixed', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='LoserPrize1.Percentage')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' AND GP_SUBJECT_KEY ='LoserPrize1.PrizeType')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '1');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promotion.NR1AsUNR')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('Cashier', 'Promotion.NR1AsUNR', '0');
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promotion.NR2AsUNR')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('Cashier', 'Promotion.NR2AsUNR', '0');
GO



/*** Sequences ***/
IF NOT EXISTS(SELECT SEQ_ID FROM SEQUENCES WHERE SEQ_ID = 41)
BEGIN
                INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (41, 1);
END
GO

/*** Promotions ***/
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 13)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Cortes�a NR'
             , 1
             , 13
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 14)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		 ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Cortes�a RE'
             , 1
             , 14
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 3
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 15)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Premio NR'
             , 1
             , 15
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 16)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Premio RE'
             , 1
             , 16
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 3
             , 0
             , ''
             , 0
             , NULL)
END
GO


IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 17)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		 ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo No Participa'
             , 1
             , 17
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 3
             , 0
             , ''
             , 0
             , NULL)
END
GO


IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 18)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
 ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo No Disponible'
             , 1
             , 18
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 3
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 19)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Premio UNR'
             , 1
             , 19
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 6
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 20)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo Cortes�a NR2'
             , 1
             , 20
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 2
             , 0
             , ''
             , 0
             , NULL)
END
GO

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 21)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_expiration_limit])
       VALUES
             ( 'Sorteo No Visible PS'
             , 1
             , 21
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , 21
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , NULL)
END
GO

/*** Terminals ***/
IF NOT EXISTS (SELECT * FROM terminals WHERE te_terminal_type = 106)
INSERT INTO [dbo].[terminals]
           ([te_type]
           ,[te_name]
           ,[te_external_id]
           ,[te_blocked]
           ,[te_active]
           ,[te_provider_id]
           ,[te_terminal_type]
           ,[te_status])
     VALUES (1
            ,'SORTEADOR'
            ,'SORTEADOR'
            ,0
            ,1
            ,'CASINO'
            ,106
            ,0);
ELSE            
SELECT '***** Record terminals SORTEADOR already exists *****';

IF NOT EXISTS (SELECT * FROM TERMINAL_TYPES WHERE TT_TYPE = 106)
BEGIN
  INSERT INTO   TERMINAL_TYPES 
              ( TT_TYPE, TT_NAME)
       VALUES ( 106    , 'CASHDESK DRAW')
END
GO

IF NOT EXISTS (SELECT * FROM GAMES WHERE GM_NAME = 'SORTEO DE CAJA')
BEGIN
  INSERT INTO   GAMES (GM_NAME) VALUES ('SORTEO DE CAJA')
END
GO

IF NOT EXISTS (SELECT   * 
                 FROM   TERMINAL_GAME_TRANSLATION 
                WHERE   TGT_TERMINAL_ID IN (SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_TERMINAL_TYPE = 106) 
                  AND   TGT_SOURCE_GAME_ID IN (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = 'SORTEO DE CAJA') )
BEGIN
  INSERT INTO   TERMINAL_GAME_TRANSLATION
              ( TGT_TERMINAL_ID, TGT_SOURCE_GAME_ID, TGT_TARGET_GAME_ID, TGT_CREATED, TGT_UPDATED)
       SELECT   TE_TERMINAL_ID , GM_GAME_ID        ,                  0, GETDATE()  , GETDATE()
         FROM   TERMINALS, GAMES  
        WHERE   TE_TERMINAL_TYPE = 106
          AND   GM_NAME = 'SORTEO DE CAJA'
END
GO   