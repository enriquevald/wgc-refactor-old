/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 57;

SET @New_ReleaseId = 58;
SET @New_ScriptName = N'UpdateTo_18.058.011.sql';
SET @New_Description = N'Multi-promos & WXP Protocol.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TRIGGERS ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AM_3GS_Trigger]'))
DROP TRIGGER [dbo].[AM_3GS_Trigger]

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger]'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger]

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger_Insert]'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger_Insert]

/****** TABLES ******/

/* account_promotions */
CREATE TABLE [dbo].[account_promotions](
	[acp_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
	[acp_created] [datetime] NOT NULL CONSTRAINT [DF_account_promotions_acp_datetime]  DEFAULT (getdate()),
	[acp_account_id] [bigint] NOT NULL,
	[acp_account_level] [int] NOT NULL,
	[acp_promo_type] [int] NOT NULL,
	[acp_promo_id] [bigint] NOT NULL,
	[acp_promo_name] [nvarchar](50) NOT NULL,
	[acp_promo_date] [datetime] NOT NULL,
	[acp_operation_id] [bigint] NOT NULL,
	[acp_credit_type] [int] NOT NULL,
	[acp_activation] [datetime] NULL,
	[acp_expiration] [datetime] NULL,
	[acp_cash_in] [money] NULL,
	[acp_points] [money] NULL,
	[acp_ini_balance] [money] NOT NULL,
	[acp_ini_withhold] [money] NOT NULL,
	[acp_ini_wonlock] [money] NULL,
	[acp_balance] [money] NOT NULL,
	[acp_withhold] [money] NOT NULL,
	[acp_wonlock] [money] NULL,
	[acp_played] [money] NOT NULL CONSTRAINT [DF_account_promotions_acp_played]  DEFAULT ((0)),
	[acp_won] [money] NOT NULL,
	[acp_status] [int] NOT NULL CONSTRAINT [DF_account_promotions_acp_status]  DEFAULT ((0)),
	[acp_updated] [datetime] NOT NULL CONSTRAINT [DF_account_promotions_acp_last_update]  DEFAULT (getdate()),
	[acp_play_session_id] [bigint] NULL,
	[acp_transaction_id] [bigint] NULL,
	[acp_recommended_account_id] [bigint] NULL,
	[acp_details] [nvarchar](200) NULL,
 CONSTRAINT [PK_account_promotions] PRIMARY KEY CLUSTERED 
(
	[acp_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/* gifts */

/* gifts.gi_small_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_small_resource_id')
  ALTER TABLE [dbo].[gifts]
          ADD [gi_small_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field gifts.gi_small_resource_id already exists *****';
GO

/* gifts.gi_large_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_large_resource_id')
  ALTER TABLE [dbo].[gifts]
          ADD [gi_large_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field gifts.gi_large_resource_id already exists *****';
GO

/* gifts.gi_description */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_description')
  ALTER TABLE [dbo].[gifts]
          ADD [gi_description] [nvarchar](200) NOT NULL CONSTRAINT [DF_gifts_gi_description] DEFAULT ((''))
ELSE
  SELECT '***** Field gifts.gi_description already exists *****';
GO


/* draws */

/* draws.dr_small_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_small_resource_id')
  ALTER TABLE [dbo].[draws]
          ADD [dr_small_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field draws.dr_small_resource_id already exists *****';
GO

/* draws.dr_large_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_large_resource_id')
  ALTER TABLE [dbo].[draws]
          ADD [dr_large_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field draws.dr_large_resource_id already exists *****';
GO


/* promotions */

/* promotions.pm_small_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_small_resource_id')
  ALTER TABLE [dbo].[promotions]
          ADD [pm_small_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field promotions.pm_small_resource_id already exists *****';
GO

/* promotions.pm_large_resource_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_large_resource_id')
  ALTER TABLE [dbo].[promotions]
          ADD [pm_large_resource_id] [bigint] NULL
ELSE
  SELECT '***** Field promotions.pm_large_resource_id already exists *****';
GO

/* promotions.pm_min_played */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_min_played')
  ALTER TABLE [dbo].[promotions]
          ADD [pm_min_played] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_played] DEFAULT ((0))
ELSE
  SELECT '***** Field promotions.pm_min_played already exists *****';
GO

/* promotions.pm_min_played_reward */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_min_played_reward')
  ALTER TABLE [dbo].[promotions]
	      ADD [pm_min_played_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_played_reward] DEFAULT ((0))
ELSE
  SELECT '***** Field promotions.pm_min_played_reward exists *****';
GO

/* promotions.pm_played */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_played')
  ALTER TABLE [dbo].[promotions]
	      ADD [pm_played] [money] NOT NULL CONSTRAINT [DF_promotions_pm_played] DEFAULT ((0))
ELSE
  SELECT '***** Field promotions.pm_played exists *****';
GO

/* promotions.pm_played_reward */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_played_reward')
  ALTER TABLE [dbo].[promotions]
	      ADD [pm_played_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_played_reward] DEFAULT ((0))
ELSE
  SELECT '***** Field promotions.pm_played_reward exists *****';
GO

/* promotions.pm_play_restricted_to_provider_list */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_play_restricted_to_provider_list')
  ALTER TABLE [dbo].[promotions]
          ADD [pm_play_restricted_to_provider_list] [bit] NOT NULL CONSTRAINT [DF_promotions_pm_play_restricted_to_provider_list] DEFAULT ((0))
ELSE
  SELECT '***** Field promotions.pm_play_restricted_to_provider_list exists *****';
GO

/* promotions.pm_last_executed */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_last_executed')
  ALTER TABLE [dbo].[promotions]
          ADD [pm_last_executed] [datetime] NULL
ELSE
  SELECT '***** Field promotions.pm_last_executed exists *****';
GO

/* promotions.pm_next_execution */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_next_execution')
  ALTER TABLE [dbo].[promotions]
	      ADD [pm_next_execution] [datetime] NULL
ELSE
  SELECT '***** Field promotions.pm_next_execution exists *****';
GO

/* account_movements */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_movements]') and name = 'am_details')
  ALTER TABLE [dbo].[account_movements]
          ADD	[am_details] [nvarchar](256) NULL
ELSE
  SELECT '***** Field account_movements.am_details exists *****';
GO

/* cashier movements */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_movements]') and name = 'cm_details')
  ALTER TABLE [dbo].[cashier_movements]
          ADD	[cm_details] [nvarchar](256) NULL
ELSE
  SELECT '***** Field cashier_movements.cm_details exists *****';
GO

/* accounts */

/* accounts.ac_recommended_by */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_recommended_by')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_recommended_by] [bigint] NULL
ELSE
  SELECT '***** Field accounts.ac_recommended_by exists *****';
GO

/* accounts.ac_re_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_re_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_re_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_re_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_re_balance exists *****';
GO

/* accounts.ac_promo_re_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_promo_re_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_promo_re_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_promo_re_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_promo_re_balance exists *****';
GO

/* accounts.ac_promo_nr_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_promo_nr_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_promo_nr_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_promo_nr_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_promo_nr_balance exists *****';
GO

/* accounts.ac_in_session_played */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_played')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_played] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_played] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_played exists *****';
GO

/* accounts.ac_in_session_won */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_won')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_won] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_won] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_won exists *****';
GO

/* accounts.ac_in_session_re_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_balance exists *****';
GO

/* accounts.ac_in_session_promo_re_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_re_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_re_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_re_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_re_balance exists *****';
GO

/* accounts.ac_in_session_promo_nr_balance */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_nr_balance')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_nr_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_nr_balance] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_nr_balance exists *****';
GO

/* accounts.ac_in_session_re_to_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_to_gm')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_to_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_to_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_to_gm exists *****';
GO

/* accounts.ac_in_session_promo_re_to_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_re_to_gm')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_re_to_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_re_to_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_re_to_gm exists *****';
GO

/* accounts.ac_in_session_promo_nr_to_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_nr_to_gm')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_nr_to_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_nr_to_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_nr_to_gm exists *****';
GO

/* accounts.ac_in_session_re_from_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_from_gm')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_from_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_from_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_from_gm exists *****';
GO

/* accounts.ac_in_session_promo_re_from_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_re_from_gm')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_re_from_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_re_from_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_re_from_gm exists *****';
GO

/* accounts.ac_in_session_promo_nr_from_gm */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_nr_from_gm')
  ALTER TABLE [dbo].[accounts]
        ADD [ac_in_session_promo_nr_from_gm] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_nr_from_gm] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_nr_from_gm exists *****';
GO

/* accounts.ac_in_session_re_played */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_played')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_played] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_played] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_played exists *****';
GO

/* accounts.ac_in_session_nr_played */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_nr_played')
  ALTER TABLE [dbo].[accounts]
        ADD [ac_in_session_nr_played] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_nr_played] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_nr_played exists *****';
GO

/* accounts.ac_in_session_re_won */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_won')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_won] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_won] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_won exists *****';
GO

/* accounts.ac_in_session_nr_won */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_nr_won')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_nr_won] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_nr_won] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_nr_won exists *****';
GO

/* accounts.ac_in_session_re_cancellable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_re_cancellable')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_re_cancellable] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_re_cancellable] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_re_cancellable exists *****';
GO

/* accounts.ac_in_session_promo_re_cancellable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_re_cancellable')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_re_cancellable] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_re_cancellable] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_re_cancellable exists *****';
GO

/* accounts.ac_in_session_promo_nr_cancellable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_promo_nr_cancellable')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_promo_nr_cancellable] [money] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_promo_nr_cancellable] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_promo_nr_cancellable exists *****';
GO

/* accounts.ac_in_session_cancellable_transaction_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_in_session_cancellable_transaction_id')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_in_session_cancellable_transaction_id] [bigint] NOT NULL CONSTRAINT [DF_accounts_ac_in_session_cancellable_transaction_id] DEFAULT ((0))
ELSE
  SELECT '***** Field accounts.ac_in_session_cancellable_transaction_id exists *****';
GO


/* account_operations */

/* account_operations.ao_redeemable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_operations]') and name = 'ao_redeemable')
  ALTER TABLE [dbo].[account_operations]
          ADD [ao_redeemable] [money] NOT NULL CONSTRAINT [DF_account_operations_ao_redeemable] DEFAULT ((0))
ELSE
  SELECT '***** Field account_operations.ao_redeemable exists *****';
GO

/* account_operations.ao_promo_redeemable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_operations]') and name = 'ao_promo_redeemable')
  ALTER TABLE [dbo].[account_operations]
          ADD [ao_promo_redeemable] [money] NOT NULL CONSTRAINT [DF_account_operations_ao_promo_redeemable] DEFAULT ((0))
ELSE
  SELECT '***** Field account_operations.ao_promo_redeemable exists *****';
GO

/* account_operations.ao_promo_not_redeemable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_operations]') and name = 'ao_promo_not_redeemable')
  ALTER TABLE [dbo].[account_operations]
          ADD [ao_promo_not_redeemable] [money] NOT NULL CONSTRAINT [DF_account_operations_ao_promo_not_redeemable] DEFAULT ((0))
ELSE
  SELECT '***** Field account_operations.ao_promo_not_redeemable exists *****';
GO

/* cashier_vouchers */

/* cashier_vouchers.cv_user_id */
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_user_id')
  ALTER TABLE  [dbo].[cashier_vouchers]
  ALTER COLUMN [cv_user_id] [int] NULL
ELSE
  SELECT '***** Field cashier_vouchers.cv_user_id does not exist *****';
GO

/* cashier_vouchers.cv_cashier_id */
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_cashier_id')
  ALTER TABLE  [dbo].[cashier_vouchers]
  ALTER COLUMN [cv_cashier_id] [int] NULL
ELSE
  SELECT '***** Field cashier_vouchers.cv_cashier_id does not exist *****';
GO

/* cashier_vouchers.cv_session_id */
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_session_id')
  ALTER TABLE  [dbo].[cashier_vouchers]
  ALTER COLUMN [cv_session_id] [bigint] NULL
ELSE
  SELECT '***** Field cashier_vouchers.cv_session_id does not exist *****';
GO

/* currencies */

CREATE TABLE [dbo].[currencies](
	[cur_iso_code] [nvarchar](3) NOT NULL,
	[cur_allowed] [bit] NOT NULL,
	[cur_name] [nvarchar](50) NULL,
	[cur_alias1] [nvarchar](3) NULL,
	[cur_alias2] [nvarchar](3) NULL,
 CONSTRAINT [PK_currencies] PRIMARY KEY CLUSTERED 
(
	[cur_iso_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* currency_denominations */

CREATE TABLE [dbo].[currency_denominations](
	[cud_iso_code] [nvarchar](3) NOT NULL,
	[cud_type] [int] NOT NULL CONSTRAINT [DF_currency_denominations_cud_type]  DEFAULT ((0)),
	[cud_denomination] [money] NOT NULL,
	[cud_rejected] [bit] NOT NULL,
 CONSTRAINT [PK_currency_denominations] PRIMARY KEY CLUSTERED 
(
	[cud_iso_code] ASC,
	[cud_type] ASC,
	[cud_denomination] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[currency_denominations]  WITH CHECK ADD  CONSTRAINT [FK_currency_denominations_currencies] FOREIGN KEY([cud_iso_code])
REFERENCES [dbo].[currencies] ([cur_iso_code])
GO
ALTER TABLE [dbo].[currency_denominations] CHECK CONSTRAINT [FK_currency_denominations_currencies]
GO

/* For Peru */

/* wxp_001_messages */
CREATE TABLE [dbo].[wxp_001_messages](
	[wxm_id] [bigint] IDENTITY(1,1) NOT NULL,
	[wxm_status] [int] NOT NULL CONSTRAINT [DF_wxp_001_messages_wxm_status]  DEFAULT ((0)),
	[wxm_datetime] [datetime] NOT NULL CONSTRAINT [DF_wxp_001_messages_wxm_created]  DEFAULT (getdate()),
	[wxm_type] [int] NOT NULL,
	[wxm_terminal_id] [int] NOT NULL,
	[wxm_data] [varbinary](128) NOT NULL,
	[wxm_status_changed] [datetime] NOT NULL CONSTRAINT [DF_wxp_001_messages_wxm_status_changed]  DEFAULT (getdate()),
 CONSTRAINT [PK_wxp_001_messages] PRIMARY KEY CLUSTERED 
(
	[wxm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/* wxp_parameters */
CREATE TABLE [dbo].[wxp_parameters](
	[wxp_last_execution] [datetime] NULL
) ON [PRIMARY]

GO



/* machine_meters */

/* machine_meters.mm_jackpot_amount */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[machine_meters]') and name = 'mm_jackpot_amount')
  ALTER TABLE [dbo].[machine_meters]
	      ADD [mm_jackpot_amount] [money] NULL 
ELSE
  SELECT '***** Field machine_meters.mm_jackpot_amount exists *****';
GO

/* machine_meters.mm_delta_jackpot_amount */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[machine_meters]') and name = 'mm_delta_jackpot_amount')
  ALTER TABLE [dbo].[machine_meters]
          ADD [mm_delta_jackpot_amount] [money] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_jackpot_amount] DEFAULT ((0))
ELSE
  SELECT '***** Field machine_meters.mm_delta_jackpot_amount exists *****';
GO

/* terminals */

/* terminals.te_current_account_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_current_account_id')
  ALTER TABLE [dbo].[terminals]
    	  ADD [te_current_account_id] [bigint] NULL
ELSE
  SELECT '***** Field terminals.te_current_account_id exists *****';
GO

/* terminals.te_current_play_session_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_current_play_session_id')
  ALTER TABLE [dbo].[terminals]
	      ADD [te_current_play_session_id] [bigint] NULL
ELSE
  SELECT '***** Field terminals.te_current_play_session_id exists *****';
GO

/* terminals.te_registration_code */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_registration_code')
  ALTER TABLE [dbo].[terminals]
	      ADD [te_registration_code] [nvarchar](50) NULL
ELSE
  SELECT '***** Field terminals.te_registration_code exists *****';
GO

/* providers */

/* providers.pv_only_redeemable */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[providers]') and name = 'pv_only_redeemable')
  ALTER TABLE [dbo].[providers]
	      ADD [pv_only_redeemable] [bit] NOT NULL CONSTRAINT [DF_providers_pv_only_redeemable] DEFAULT ((0))
ELSE
  SELECT '***** Field providers.pv_only_redeemable exists *****';
GO


/****** INDEXES ******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_account_status')
  CREATE NONCLUSTERED INDEX [IX_acp_account_status] ON [dbo].[account_promotions] 
  (
	  [acp_account_id] ASC,
	  [acp_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index account_promotions.IX_acp_account_status already exists *****';

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_created_status')
  CREATE NONCLUSTERED INDEX [IX_acp_created_status] ON [dbo].[account_promotions] 
  (
	  [acp_created] ASC,
	  [acp_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index account_promotions.IX_acp_created_status already exists *****';

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_status_promo_id')
  CREATE NONCLUSTERED INDEX [IX_acp_status_promo_id] ON [dbo].[account_promotions] 
  (
	  [acp_status] ASC,
	  [acp_promo_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index account_promotions.IX_acp_status_promo_id already exists *****';

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_promo_id_promo_date')
  CREATE NONCLUSTERED INDEX [IX_acp_promo_id_promo_date] ON [dbo].[account_promotions] 
  (
	  [acp_promo_id] ASC,
	  [acp_promo_date] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index account_promotions.IX_acp_promo_id_promo_date already exists *****';

GO

/****** RECORDS ******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='VoucherOnPromotionCancelled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher'
             ,'VoucherOnPromotionCancelled'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ManualPromotions' AND GP_SUBJECT_KEY ='OnlyWhenTotalNotRedeemableBalanceIsZero')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ManualPromotions'
             ,'OnlyWhenTotalNotRedeemableBalanceIsZero'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ManualPromotions' AND GP_SUBJECT_KEY ='OnlyWhenManuallyAddedNotRedeemableBalanceIsZero')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ManualPromotions'
             ,'OnlyWhenManuallyAddedNotRedeemableBalanceIsZero'
             ,'1');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Gifts.NotRedeemableCredits' AND GP_SUBJECT_KEY ='BalanceLimit')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Gifts.NotRedeemableCredits'
             ,'BalanceLimit'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Gifts.NotRedeemableCredits' AND GP_SUBJECT_KEY ='DailyLimit')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Gifts.NotRedeemableCredits'
             ,'DailyLimit'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Gifts.RedeemableCredits' AND GP_SUBJECT_KEY ='DailyLimit')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Gifts.RedeemableCredits'
             ,'DailyLimit'
             ,'0');

--
-- GeneralParams for Peru (ExternalProtocol)
--

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Enabled'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='ReportTime')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'ReportTime'
             ,'08:00');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='RemoteURL')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'RemoteURL'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='SiteRegistrationCode')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'SiteRegistrationCode'
             ,'');

GO

DECLARE @user_id AS INT

/****** CREATE USER 'PromoBOX' for the auto-service kiosk. ******/
IF NOT EXISTS (
               SELECT   1
                 FROM   GUI_USERS
                WHERE   GU_USER_TYPE = 2
              )
BEGIN
  SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
    FROM   GUI_USERS                      

  INSERT INTO GUI_USERS ( GU_USER_ID
                        , GU_PROFILE_ID
                        , GU_USERNAME
                        , GU_ENABLED
                        , GU_PASSWORD
                        , GU_NOT_VALID_BEFORE
                        , GU_NOT_VALID_AFTER
                        , GU_LAST_CHANGED
                        , GU_PASSWORD_EXP
                        , GU_PWD_CHG_REQ
                        , GU_FULL_NAME
                        , GU_LOGIN_FAILURES
                        , GU_USER_TYPE
                        )
                 VALUES ( @user_id
                        , 0
                        , 'SYS-PromoBOX'
                        , 1
                        , CAST('0000' AS BINARY(40))
                        , GETDATE() - 3
                        , NULL
                        , GETDATE()
                        , NULL
                        , 0
                        , 'SYS-PromoBOX'
                        , 0
                        , 2
                        )
END

/****** CREATE USER 'SYS-System' used for promotions and promo expiration. ******/
IF NOT EXISTS (
               SELECT   1
                 FROM   GUI_USERS
                WHERE   GU_USER_TYPE = 3
              )
BEGIN
  SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
    FROM   GUI_USERS                      

  INSERT INTO GUI_USERS ( GU_USER_ID
                        , GU_PROFILE_ID
                        , GU_USERNAME
                        , GU_ENABLED
                        , GU_PASSWORD
                        , GU_NOT_VALID_BEFORE
                        , GU_NOT_VALID_AFTER
                        , GU_LAST_CHANGED
                        , GU_PASSWORD_EXP
                        , GU_PWD_CHG_REQ
                        , GU_FULL_NAME
                        , GU_LOGIN_FAILURES
                        , GU_USER_TYPE
                        )
                 VALUES ( @user_id
                        , 0
                        , 'SYS-System'
                        , 1
                        , CAST('0000' AS BINARY(40))
                        , GETDATE() - 3
                        , NULL
                        , GETDATE()
                        , NULL
                        , 0
                        , 'SYS-System'
                        , 0
                        , 3
                        )
END

--
-- User SYS-PromoBOX (for the auto-service kiosk) for Mobile Banks
--
DECLARE @pin AS INT
DECLARE @mb_id AS BIGINT
DECLARE @track_data AS NVARCHAR(20)

IF NOT EXISTS
            (
              SELECT   1
                FROM   MOBILE_BANKS
               WHERE   MB_ACCOUNT_TYPE = 3
            )
BEGIN
  SET @pin = ROUND((9999 - 1000) * RAND() + 1000, 0)

  INSERT INTO   MOBILE_BANKS
              ( MB_BLOCKED
              , MB_BALANCE
              , MB_ACCOUNT_TYPE
              , MB_PIN
              , MB_HOLDER_NAME
              )
       VALUES ( 0
              , 0
              , 3
              , CAST(@pin AS NVARCHAR)
              , 'SYS-PromoBOX'
              )
  SET @mb_id = SCOPE_IDENTITY()

  SET @track_data = RIGHT('0000000000000' + CAST(@mb_id AS NVARCHAR), 13)

  UPDATE   MOBILE_BANKS
     SET   MB_TRACK_DATA = @track_data
   WHERE   MB_ACCOUNT_ID = @mb_id

END

--
-- Update User Names for NoteAcceptor: 
--
--   - GUI: ACEPT.BILLETES -> SYS-Acceptor
--   - MB:  SISTEMA        -> SYS-Acceptor

UPDATE   MOBILE_BANKS
   SET   MB_HOLDER_NAME = 'SYS-Acceptor'
 WHERE   MB_ACCOUNT_TYPE = 2

UPDATE   GUI_USERS
   SET   GU_USERNAME = 'SYS-Acceptor'
 WHERE   GU_USER_TYPE = 1

GO

/****** Automatic promotion cover coupon - recharge ******/
DECLARE @expiration_value AS INT
DECLARE @expiration_type  AS INT
DECLARE @str_value AS NVARCHAR(50)

SELECT @str_value = gp_key_value FROM general_params WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CoverCouponExpireAfterDays'
IF ISNUMERIC(@str_value) = 1
  SET @expiration_value = CAST(@str_value AS INT)
ELSE
  SET @expiration_value = 21

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 2)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_num_tokens]
             ,[pm_token_reward]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list])
       VALUES
             ( 'Cup�n Cover'
             , 1
             , 2
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , @expiration_value
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , 0.00
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0)
END

/****** Automatic promotion prize coupon - recharge ******/
SELECT @str_value = gp_key_value FROM general_params WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'PrizesExpireAfterDays'
IF ISNUMERIC(@str_value) = 1
  SET @expiration_value = CAST(@str_value AS INT)
ELSE
  SET @expiration_value = 21

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 7)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_reward]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list])
       VALUES
             ( 'Cup�n Premio'
             , 1
             , 7
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , @expiration_value
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , -0.0001
             , 0
             , 0.00
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0)
END

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 8)
BEGIN
  SELECT   TOP 1 @expiration_value = PM_EXPIRATION_VALUE,
                 @expiration_type  = PM_EXPIRATION_TYPE
    FROM   PROMOTIONS
   WHERE   PM_TYPE                 = 1
  ORDER BY PM_PROMOTION_ID DESC

  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_reward]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list])
       VALUES
             ( 'Por Puntos - Redimible'
             , 1
             , 8
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , @expiration_type
             , @expiration_value
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , -0.0001
             , 0
             , 0.00
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0)
END

UPDATE   PROMOTIONS
   SET   PM_NAME = 'Por Puntos - No Redimible'
 WHERE   PM_TYPE = 1

GO

/****** currencies ******/
INSERT INTO [dbo].[currencies] (cur_iso_code, cur_allowed) VALUES ('EUR', 0);   
INSERT INTO [dbo].[currencies] (cur_iso_code, cur_allowed) VALUES ('MXN', 0);   
INSERT INTO [dbo].[currencies] (cur_iso_code, cur_allowed) VALUES ('PEN', 0);   
INSERT INTO [dbo].[currencies] (cur_iso_code, cur_allowed) VALUES ('USD', 0);   

/****** currency_denominations ******/
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN',   20, 0);
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN',   50, 0);
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN',  100, 0);
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN',  200, 0);
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN',  500, 0);
INSERT INTO [dbo].[currency_denominations] (cud_iso_code, cud_denomination, cud_rejected) VALUES ('MXN', 1000, 0);

--// TODO: Procedures zsp_* de Marcos
--// TODO: PT_PlaySessionFinished i PT_ReadData
--
--// TODO: zsp_* de 3GS. Ahora llaman a las clases C-Sharp.

SELECT 'Update OK.';
