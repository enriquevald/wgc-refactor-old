/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 295;

SET @New_ReleaseId = 296;
SET @New_ScriptName = N'UpdateTo_18.296.038.sql';
SET @New_Description = N'SP Hold vs Win';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='SendAlesisConstanciasOnError')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('PSAClient', 'SendAlesisConstanciasOnError', '0')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY ='HideTITOHoldvsWinReport')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('WigosGUI', 'HideTITOHoldvsWinReport', '1')
  
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='NoLevel.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('PlayerTracking', 'NoLevel.Name', 'Anonymous')
  
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='NoGender.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('PlayerTracking', 'NoGender.Name', 'Anonymous')
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_HoldvsWin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_HoldvsWin]
GO

CREATE PROCEDURE [dbo].[TITO_HoldvsWin]
      (@pIniDate DATETIME,
       @pGroupByOption INT = 1,
       @pOrderGroup0 INT = 0)
AS
BEGIN

DECLARE @_d0 AS DATETIME
DECLARE @_d1 AS DATETIME


SET @_d0 = @pIniDate
SET @_d1 = DATEADD (DAY, 1, @_d0)


SET @_d1 = dbo.Opening(0, @_d0)
SET @_d0 = @_d1 -1 

SELECT   AssetNumber       
       , Manufacturer
       , M0                                                         CoinIn
       , (M1+M2)                                                    TotalWon
       , M0-(M1+M2)                                                 Hold
       , M11_BILLS                                                  BillIn
       , NBILL                                                      BillInPerDenom
       , M128                                                       CashableTicketIn
       , M130                                                       RestrictedTicketIn
       , M132                                                       NonRestrictedTicketIn
       , M134                                                       CashableTicketOut
       , M136                                                       RestrictedTicketOut
       , M2                                                         Jackpots
       , M3                                                         HandPaidCanceledCredits
       , (M11_BILLS + M128 + M130 + M132)                           TotalIn
       , M36                                                        TotalDrop
       , (M134 + M136 + M2 + M3)                                    TotalOut
       , (M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3) TotalWin
       , (M11_BILLS + M128 + 0 + M132)                              TotalReIn
       , (M134 + 0 + M2 + M3)                                       TotalReOut
       , (M11_BILLS + M128 + 0 + M132) - (M134 + 0 + M2 + M3)       TotalReWin
INTO #TerminalsMeter
FROM
(  
      SELECT   MIN(TE_TERMINAL_ID) TID
             , MIN(TE_NAME) AssetNumber
             , MIN(TE_PROVIDER_ID) Manufacturer
             , SUM (CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M0
             , SUM (CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M1
             , SUM (CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M2
             , SUM (CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M3
             , SUM (CASE WHEN TSMH_METER_CODE = 4 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M4
             , SUM (CASE WHEN TSMH_METER_CODE = 11 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )  * 0.01 M11_BILLS
             , SUM (ISNULL(TSMH_METER_INCREMENT, 0) * CASE TSMH_METER_CODE WHEN 64 THEN   1.00
                                                                           WHEN 65 THEN   2.00
                                                                           WHEN 66 THEN   5.00
                                                                           WHEN 67 THEN  10.00
                                                                           WHEN 68 THEN  20.00
                                                                           WHEN 69 THEN  25.00
                                                                           WHEN 70 THEN  50.00
                                                                           WHEN 71 THEN 100.00
                                                                           WHEN 72 THEN 200.00
                                                                           WHEN 73 THEN 250.00
                                                                           WHEN 74 THEN 500.00
                                                                           WHEN 75 THEN 1000.00
                                                                           WHEN 76 THEN 2000.00
                                                                           WHEN 77 THEN 2500.00
                                                                           WHEN 78 THEN 5000.00
                                                                           WHEN 79 THEN 10000.00
                                                                           WHEN 80 THEN 20000.00
                                                                           WHEN 81 THEN 25000.00
                                                                           WHEN 82 THEN 50000.00
                                                                           WHEN 83 THEN 100000.00
                                                                           WHEN 84 THEN 200000.00
                                                                           WHEN 85 THEN 250000.00
                                                                           WHEN 86 THEN 500000.00
                                                                           WHEN 87 THEN 1000000.00
                                                                           ELSE 0  END ) NBILL
      
             , SUM (CASE WHEN TSMH_METER_CODE = 128 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M128
             , SUM (CASE WHEN TSMH_METER_CODE = 130 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M130
             , SUM (CASE WHEN TSMH_METER_CODE = 132 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M132
      
             , SUM (CASE WHEN TSMH_METER_CODE = 134 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M134
             , SUM (CASE WHEN TSMH_METER_CODE = 136 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M136
      
             , SUM (CASE WHEN TSMH_METER_CODE = 36 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M36
      
             , SUM(CASE WHEN (TSMH_METER_CODE IN (11,128,130,132)) THEN ISNULL(TSMH_METER_INCREMENT, 0)  ELSE 0 END ) CommputedTotalIn
             , SUM(CASE WHEN (TSMH_METER_CODE IN (2,3,134,136)) THEN ISNULL(TSMH_METER_INCREMENT, 0)  ELSE 0 END ) CommputedTotalOut
      
             ,
               ( SUM (CASE WHEN tsmh_meter_code = 0 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 1 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 2 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               ) * 0.01 MNET0
             ,
               ( SUM (CASE WHEN tsmh_meter_code = 11 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 128 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 130 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 132 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 134 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 136 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 3 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               ) * 0.01 MNET1
             
      FROM   TERMINAL_SAS_METERS_HISTORY H 
INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID 
     WHERE   TSMH_DATETIME >= @_d0 AND TSMH_DATETIME < @_d1
       AND   TSMH_TYPE = 20 -- DAILY
       AND   TSMH_DENOMINATION = 0
       AND   TSMH_GAME_ID      = 0
  GROUP BY   TE_TERMINAL_ID, TE_PROVIDER_ID, TE_NAME
) TerminalsMeters 

-- @pGroupByOption == 1 => Provider
-- @pGroupByOption == 2 => Total
-- @pGroupByOption == 0 => Terminal

if @pGroupByOption = 1
   Begin
        select   Manufacturer,
                 sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin,
                 sum(1) NumEgms

        from
               ( select * from #TerminalsMeter ) ManufacturerHold
        GROUP BY  MANUFACTURER      
        ORDER BY  Manufacturer                                
   End
else if @pGroupByOption = 2
   begin    
        select   sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin

        from    #TerminalsMeter
   
    end
else
   begin  
      select * from #TerminalsMeter  ORDER BY 
      case @pOrderGroup0
        when 0
          then AssetNumber
        when 1
          then Manufacturer
      end
      , 
       case @pOrderGroup0
        when 0
          then Manufacturer
        when 1
          then AssetNumber
      end
                                
   end

DROP table #TerminalsMeter

END

GO

GRANT EXECUTE ON [dbo].[TITO_HoldvsWin] TO [wggui] WITH GRANT OPTION

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_HoldvsWin_Reception]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_HoldvsWin_Reception]
GO

CREATE PROCEDURE [dbo].[TITO_HoldvsWin_Reception]
AS
BEGIN
  
  DECLARE @_GD INT
  DECLARE @_RECEPTION INT

  select @_RECEPTION = CAST(gp_key_value as INT) from general_params
  where gp_group_key = 'Reception' and gp_subject_key = 'Enabled' and isnumeric(gp_key_value)=1
  SET @_RECEPTION = ISNULL(@_RECEPTION, 0)


  SET @_GD = DBO.GamingDayFromDateTime (DBO.TodayOpening(0) - 1)

  SELECT   @_RECEPTION ENABLED
         , 'TODAY' TYPE
         , ISNULL(SUM(VISIT), 0) VISIT, ISNULL(SUM(ENTRANCES), 0) ENTRANCES, ISNULL(SUM(TICKETS), 0) TICKETS, ISNULL(SUM(PRICE), 0) PRICE, ISNULL(SUM(PAID), 0) PAID
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_LEVEL  = 0) THEN 1 ELSE 0 END), 0) LEVEL0
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_LEVEL  = 1) THEN 1 ELSE 0 END), 0) LEVEL1
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_LEVEL  = 2) THEN 1 ELSE 0 END), 0) LEVEL2
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_LEVEL  = 3) THEN 1 ELSE 0 END), 0) LEVEL3
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_LEVEL  = 4) THEN 1 ELSE 0 END), 0) LEVEL4
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_IS_VIP = 1) THEN 1 ELSE 0 END), 0) VIP
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_GENDER = 0) THEN 1 ELSE 0 END), 0) GENDER0
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_GENDER = 1) THEN 1 ELSE 0 END), 0) GENDER1
           , ISNULL(SUM(CASE WHEN (AC_HOLDER_GENDER = 2) THEN 1 ELSE 0 END), 0) GENDER2

  FROM
  (
  SELECT CUT_VISIT_ID VISIT_ID, 1 VISIT, SUM(1) ENTRANCES, SUM(CASE WHEN CUE_TICKET_ENTRY_ID IS NULL THEN 0 ELSE 1 END) TICKETS,  SUM(ISNULL(CUE_TICKET_ENTRY_PRICE,0)) PRICE, 0 AS PAID
  FROM CUSTOMER_VISITS INNER JOIN CUSTOMER_ENTRANCES ON CUT_VISIT_ID = CUE_VISIT_ID
  INNER JOIN ACCOUNTS ON CUT_CUSTOMER_ID = AC_ACCOUNT_ID
  WHERE customer_visits.cut_gaming_day = @_GD 
  GROUP BY CUT_VISIT_ID
  ) VIEW_VISITS
  INNER JOIN CUSTOMER_VISITS ON VISIT_ID      = CUT_VISIT_ID
  INNER JOIN ACCOUNTS        ON AC_ACCOUNT_ID = CUT_CUSTOMER_ID 
  
END
GO

GRANT EXECUTE ON [dbo].[TITO_HoldvsWin_Reception] TO [wggui] WITH GRANT OPTION
GO
