/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 382;

SET @New_ReleaseId = 383;
SET @New_ScriptName = N'UpdateTo_18.383.041.sql';
SET @New_Description = N'CASH IN FBM'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'Zitro.Track'
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('Zitro.Track', 'Enabled', '0')
END

GO

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'Features'
                        AND GP_SUBJECT_KEY = 'CashDesk.Draw.02'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('Features','CashDesk.Draw.02','0')
END
/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_AccountInfo')
       DROP PROCEDURE [zsp_AccountInfo]
GO

CREATE PROCEDURE [dbo].[zsp_AccountInfo]
    @VendorId        varchar(16),
      @TrackData       varchar(50)

WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  declare @AccountNumber              bigint ,
  @Name                       varchar(255) ,
  @FirstSurname               varchar(255) ,
  @SecondSurname              varchar(255) ,
  @Gender                     varchar(255) ,
  @BirthDate                  varchar(255) ,
  @Email                      varchar(255) ,
  @AuxEmail                   varchar(255) ,
  @MobileNumber               varchar(255) ,
  @FixedPhone                 varchar(255) ,
  @ClientTypeCode             varchar(255) ,
  @ClientType                 varchar(255) ,
  @FiscalCode                 varchar(255) ,
  @Nationality                varchar(255) ,
  @Street                     varchar(255) ,
  @HouseNumber                varchar(255) ,
  @Colony                     varchar(255) ,
  @State                      varchar(255) ,
  @City                       varchar(255) ,
  @DocumentType               varchar(255) ,
  @DocumentNumber             varchar(255) ,
  @LevelCode                  varchar(255) ,
  @Level                      varchar(255) ,
  @Points                     varchar(255) ,
  @Document2Type              varchar(255) ,
  @Document2Number            varchar(255) ,
  @Document3Type              varchar(255) ,
  @Document3Number            varchar(255) 

  



  SET @_try       = 0
  SET @_max_tries = 6        -- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Ok'
    SET @error_text   = ''
    BEGIN TRY
      SET @_try = @_try + 1

        DECLARE @_account_id BIGINT

      SET @_account_id = 0
      IF @TrackData IS NOT NULL  
      BEGIN 
        SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
                       )
      END
        IF @_account_id > 0
        begin
            SELECT @status_code  = 1,
            
            @AccountNumber = ac_account_id ,
                     @Name = ac_holder_name3 ,
                     @FirstSurname = ac_holder_name1 ,
                     @SecondSurname = ac_holder_name2,
                     @Gender = ac_holder_gender ,
                     @BirthDate = ac_holder_birth_date,
                     @Email = ac_holder_email_01 ,
                     @AuxEmail = ac_holder_email_02 ,
                     @MobileNumber = ac_holder_phone_number_01 ,
                     @FixedPhone = ac_holder_phone_number_02 ,
                     @ClientTypeCode = 'F' ,
                     @ClientType = 'FISICA',
                     @FiscalCode = oc_code ,
                     @Nationality = co_name,
                     @Street = ac_holder_address_01 ,
                     @HouseNumber = ac_holder_ext_num,
                     @Colony = ac_holder_address_02 ,
                     @State = fs_name ,
                     @City = ac_holder_city ,
                     @DocumentType = identification_types.idt_name ,
                     @DocumentNumber = ac_holder_id,
                     @LevelCode = ac_holder_level,
                     @Level = (select top 1 gp_key_value from general_params where gp_group_key='PlayerTracking' and gp_subject_key='Level0'+ cast(ac_holder_level as varchar(2)) +'.Name'),
                     @Points = ac_points,
                     @Document2Type = idt2.idt_name ,
                     @Document2Number = ac_holder_id2,
                     @Document3Type = idt3.idt_name ,
                     @Document3Number = ac_holder_id3

              FROM   accounts
              LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
              LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
              LEFT JOIN identification_types on ac_holder_id_type = identification_types.idt_id
              LEFT JOIN identification_types idt2 on ac_holder_id2_type = idt2.idt_id
              LEFT JOIN identification_types idt3 on ac_holder_id3_type = idt3.idt_id
              LEFT JOIN occupations on oc_id = ac_holder_occupation_id
            WHERE   ac_account_id = @_account_id
        end

      SET @_completed = 1

    END TRY
    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData 
                    +';VenbdorId='      + @VendorId
  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text



  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_GetAccountInfo', @_account_id, 0, 0, 0,
                                            0, @status_code, 0, 1,
                                            @input, @output

  COMMIT TRANSACTION
      SELECT @status_code          AS StatusCode,
          @status_text        AS StatusText,
            @AccountNumber      as  AccountNumber     ,
            @Name                   as  Name             , 
            @FirstSurname           as  FirstSurname     , 
            @SecondSurname          as  SecondSurname    , 
            @Gender                      as  Gender           , 
            @BirthDate              as  BirthDate        , 
            @Email                       as  Email            , 
            @AuxEmail               as  AuxEmail         , 
            @MobileNumber           as  MobileNumber     , 
            @FixedPhone             as  FixedPhone       , 
            @ClientTypeCode         as  ClientTypeCode   , 
            @ClientType             as  ClientType       , 
            @FiscalCode             as  FiscalCode       , 
            @Nationality            as  Nationality      , 
            @Street                      as  Street           , 
            @HouseNumber            as  HouseNumber      , 
            @Colony                      as  Colony           , 
            @State                       as  State            , 
            @City                   as  City             , 
            @DocumentType           as  DocumentType     , 
            @DocumentNumber         as  DocumentNumber   ,
            @LevelCode              as  LevelCode   ,
            @Level                       as  Level   ,
            @Points                      as  Points,
            @Document2Type          as  Document2Type     , 
            @Document2Number        as  Document2Number   ,
            @Document3Type          as  Document3Type     , 
            @Document3Number        as  Document3Number   

      

END -- zsp_SessionStart
go

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_AccountInfo')
       DROP PROCEDURE [S2S_AccountInfo]
GO
CREATE PROCEDURE [dbo].[S2S_AccountInfo]
  @VendorId        varchar(16),
  @Trackdata       varchar(24)
AS
BEGIN
  
  EXECUTE zsp_AccountInfo  @VendorId,@Trackdata
    
END

GO

GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [3GS] WITH GRANT OPTION 
GO

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTitoSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTitoSummary]
GO

CREATE PROCEDURE [dbo].[GetTitoSummary]        
         @pHour INT
        ,@pCashable INT
        ,@pPromoRe INT   
		,@pPromoNR INT   
		,@pRedeemed INT 
		,@pPlayed INT 
		,@pHandpay INT 
		,@pJackpot INT 
		,@pDateFrom Datetime
		,@pDateTo Datetime
AS
BEGIN 
	DECLARE @pStatusDiscared INT
	DECLARE @pStatusCanceled INT
	DECLARE @pStatusRedeemed INT
	SET @pStatusDiscared = 4
	SET @pStatusCanceled = 1
	SET @pStatusRedeemed = 2
	
	
	
	BEGIN
	  SELECT   ISNULL(T1.TI_CREATED_DATETIME, T2.TI_LAST_ACTION_DATETIME)     
             , T2.TI_CASHABLE_QUANTIY     
             , T2.TI_CASHABLE_AMOUNT     
             , T2.TI_PROMO_RE_QUANTITY      
             , T2.TI_PROMO_RE_AMOUNT      
             , T2.TI_PROMO_NR_QUANTITY      
             , T2.TI_PROMO_NR_AMOUNT      
             , T2.TOTAL_TICKET_IN      
             , T1.TO_CASHABLE_QUANTIY      
             , T1.TO_CASHABLE_AMOUNT      
             , T1.TO_PROMO_NR_QUANTITY      
             , T1.TO_PROMO_NR_AMOUNT      
             , T1.TOTAL_TICKET_OUT      
             , T2.PAID_QUANTITY      
             , T2.PAID_AMOUNT      
             , T1.GENERATED_QUANTITY      
             , T1.GENERATED_AMOUNT      
        FROM (      
              SELECT   TI_CREATED_DATETIME      
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN 1 END) AS  TO_CASHABLE_QUANTIY              
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN TI_AMOUNT END) AS  TO_CASHABLE_AMOUNT       
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID = @pPromoNR )  THEN 1 END) AS  TO_PROMO_NR_QUANTITY             
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID = @pPromoNR )  THEN TI_AMOUNT END) AS  TO_PROMO_NR_AMOUNT       
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE = 1  AND TI_TYPE_ID IN (@pCashable, @pPromoNR, @pHandpay, @pJackpot))  THEN TI_AMOUNT END) AS  TOTAL_TICKET_OUT      
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE IN (0, 2)) THEN 1 END) AS  GENERATED_QUANTITY             
                     , SUM(CASE WHEN (TI_CREATED_TERMINAL_TYPE IN (0, 2)) THEN TI_AMOUNT END) AS  GENERATED_AMOUNT       
                FROM   (SELECT   CASE WHEN  ( DATEPART(HOUR, TI_CREATED_DATETIME) < @pHour)            
                 THEN DATEADD (DD, -1, DATEDIFF(DD, 0, TI_CREATED_DATETIME) )      
                                 ELSE DATEADD(DD,  0, DATEDIFF(DD, 0, TI_CREATED_DATETIME)) END AS  TI_CREATED_DATETIME      
                               , TI_CREATED_TERMINAL_TYPE          
                               , TI_LAST_ACTION_TERMINAL_TYPE      
                            , TI_AMOUNT       
                               , TI_TYPE_ID      
                          FROM    TICKETS         
       WHERE   TI_STATUS <> @pStatusDiscared AND (TI_CREATED_DATETIME >= CAST(@pDateFrom AS DATETIME)) AND (TI_CREATED_DATETIME < CAST(@pDateTo AS DATETIME))    
                        )AS TMP_TABLE      
             GROUP BY   TI_CREATED_DATETIME     
             ) AS T1      
     FULL OUTER JOIN (      
              SELECT   TI_LAST_ACTION_DATETIME      
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN 1 END) AS  TI_CASHABLE_QUANTIY              
                    , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pHandpay, @pJackpot) ) THEN TI_AMOUNT END) AS  TI_CASHABLE_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoRe )  THEN 1 END) AS  TI_PROMO_RE_QUANTITY             
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoRe )  THEN TI_AMOUNT END) AS  TI_PROMO_RE_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoNR )  THEN 1 END) AS  TI_PROMO_NR_QUANTITY            
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID = @pPromoNR )  THEN TI_AMOUNT END) AS  TI_PROMO_NR_AMOUNT       
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE = 1 AND TI_STATUS = @pPlayed AND TI_TYPE_ID IN (@pCashable, @pPromoRe, @pPromoNR))  THEN TI_AMOUNT END) AS  TOTAL_TICKET_IN      
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE IN (0, 2) AND TI_STATUS = @pRedeemed AND TI_TYPE_ID != @pPromoNR)  THEN 1 END) AS  PAID_QUANTITY            
                     , SUM(CASE WHEN (TI_LAST_ACTION_TERMINAL_TYPE IN (0, 2) AND TI_STATUS = @pRedeemed AND TI_TYPE_ID != @pPromoNR)  THEN TI_AMOUNT END) AS  PAID_AMOUNT      
                FROM   (SELECT   CASE WHEN  (DATEPART(HOUR, TI_LAST_ACTION_DATETIME) < @pHour)            
                        THEN   DATEADD (DD, -1, DATEDIFF(DD, 0, TI_LAST_ACTION_DATETIME))      
                                 ELSE DATEADD(DD,  0, DATEDIFF(DD, 0, TI_LAST_ACTION_DATETIME)) END AS  TI_LAST_ACTION_DATETIME      
                   , TI_CREATED_TERMINAL_TYPE          
                   , TI_LAST_ACTION_TERMINAL_TYPE      
                   , TI_AMOUNT       
                   , TI_TYPE_ID      
                   , TI_STATUS       
                          FROM    TICKETS         
       WHERE   TI_STATUS IN (@pStatusCanceled, @pStatusRedeemed) AND (TI_LAST_ACTION_DATETIME >= CAST(@pDateFrom AS DATETIME)) AND (TI_LAST_ACTION_DATETIME < CAST(@pDateTo AS DATETIME))    
                        )AS TMP_TABLE      
            GROUP BY   TI_LAST_ACTION_DATETIME     
           ) AS T2      
     ON  T1.TI_CREATED_DATETIME = T2.TI_LAST_ACTION_DATETIME      
     ORDER BY   ISNULL(T1.TI_CREATED_DATETIME, T2.TI_LAST_ACTION_DATETIME)  
     
				
	END
	
END --GetTitoSummary
GO

GRANT EXECUTE ON [dbo].[GetTitoSummary] TO [wggui] WITH GRANT OPTION
GO

GO
-- Recreate CashierMovementsGrouped_Select
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Select]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGrouped_Select]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Select]
        @pCashierSessionId NVARCHAR(MAX)
       ,@pDateFrom DATETIME
       ,@pDateTo DATETIME 
  AS
BEGIN

DECLARE @_sql NVARCHAR(MAX)

DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
  SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
  WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
  SET @_CM_CURRENCY_ISO_CODE = ISNULL(@_CM_CURRENCY_ISO_CODE, 'NULL')

  IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
  BEGIN     
      SET @_sql =  '
      SELECT   CM_SESSION_ID
          ,CM_TYPE
          ,0                AS CM_SUB_TYPE
          ,COUNT(CM_TYPE)               AS CM_TYPE_COUNT
          ,ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''')  AS CM_CURRENCY_ISO_CODE
          ,ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
          ,SUM(ISNULL(CM_SUB_AMOUNT,0)) CM_SUB_AMOUNT
          ,SUM(ISNULL(CM_ADD_AMOUNT,0)) CM_ADD_AMOUNT
          ,SUM(ISNULL(CM_AUX_AMOUNT,0)) CM_AUX_AMOUNT
          ,SUM(ISNULL(CM_INITIAL_BALANCE,0)) CM_INITIAL_BALANCE
          ,SUM(ISNULL(CM_FINAL_BALANCE,0)) CM_FINAL_BALANCE
          ,CM_CAGE_CURRENCY_TYPE
          
      FROM   CASHIER_MOVEMENTS WITH (INDEX (IX_CM_SESSION_ID)) 
      WHERE   CM_SESSION_ID IN ('+@pCashierSessionId+') AND CM_UNDO_STATUS IS NULL
      GROUP BY CM_SESSION_ID, CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''), ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
         
         UNION ALL 
         SELECT  mbm_cashier_session_id             AS CM_SESSION_ID
             , mbm_type                              AS CM_TYPE  
             , 1                     AS CM_SUB_TYPE          
             , COUNT(mbm_type)                 AS CM_TYPE_COUNT     
             ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
             , 0                     AS CM_CURRENCY_DENOMINATION
             , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
             , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
             , 0                     AS CM_AUX_AMOUNT 
             , 0                     AS CM_INITIAL_BALANCE
             , 0                     AS CM_FINAL_BALANCE
             , 0                     AS CM_CAGE_CURRENCY_TYPE      
              
         FROM    MB_MOVEMENTS WITH (INDEX (IX_MB_MOVEMENTS))          
        WHERE   mbm_cashier_session_id IN ('+@pCashierSessionId+')
        GROUP BY    mbm_cashier_session_id, mbm_type '
  END --ByID
  ELSE--ByDate
  BEGIN
  SET @_sql =  '
        SELECT   
            NULL
           , CM_TYPE
           , 0                AS CM_SUB_TYPE      
           , COUNT(CM_TYPE)                AS CM_TYPE_COUNT
           , ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''') AS CM_CURRENCY_ISO_CODE
           , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
            , SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
          , SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
          , SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
          , SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
          , SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
            
         FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type)) 
        WHERE  ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' CM_DATE >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
        ' CM_DATE <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
          
       CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
		'AND CM_SESSION_ID IN ('+@pCashierSessionId +')'END +'
			AND CM_UNDO_STATUS IS NULL 
       GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''),ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
       UNION ALL 
       SELECT
           NULL   
           , mbm_type                              AS CM_TYPE 
           , 1                     AS CM_SUB_TYPE         
           , COUNT(mbm_type)             AS CM_TYPE_COUNT
           ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
           , 0                     AS CM_CURRENCY_DENOMINATION
           , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
           , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
           , 0                     AS CM_AUX_AMOUNT 
           , 0                     AS CM_INITIAL_BALANCE
           , 0                     AS CM_FINAL_BALANCE    
           , 0                     AS CM_CAGE_CURRENCY_TYPE
       FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))             
      WHERE   ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' MBM_DATETIME >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
        ' MBM_DATETIME <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
      
          CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
          'AND mbm_cashier_session_id IN ('+@pCashierSessionId +')'END +'
      GROUP BY    mbm_type '
    
  END--ByDate
  
  EXEC sp_executesql @_sql
  
 END --CashierMovementsGrouped_Select
 
GO
GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Select] TO [WGGUI] WITH GRANT OPTION
GO



-- Recreate CM_OldHistoryByHour
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CM_OldHistoryByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CM_OldHistoryByHour]
GO

CREATE PROCEDURE [dbo].[CM_OldHistoryByHour]
      @Date1 DATETIME,
      @CM_CURRENCY_ISO_CODE NVARCHAR(20) = NULL 
      
AS
BEGIN
            DECLARE @Date2   DATETIME

            IF @CM_CURRENCY_ISO_CODE IS NULL BEGIN
                  SELECT @CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
                  WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
            END

            SET @Date1 = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Date1), 0)
            SET @Date2 = DATEADD (HOUR, 1, @Date1)

            DELETE FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE   CM_DATE >= @Date1 AND CM_DATE <  @Date2 AND CM_SUB_TYPE <> 2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

            INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                                    ( CM_DATE
                                   , CM_TYPE
                                   , CM_SUB_TYPE
                                   , CM_TYPE_COUNT
                                   , CM_CURRENCY_ISO_CODE
                                   , CM_CURRENCY_DENOMINATION
                                   , CM_SUB_AMOUNT
                                   , CM_ADD_AMOUNT
                                   , CM_AUX_AMOUNT
                                   , CM_INITIAL_BALANCE
                                   , CM_FINAL_BALANCE
                                   )     
                    SELECT   @Date1 CM_DATE
                                   , CM_TYPE
                                   , 0                             AS CM_SUB_TYPE                  --CAIXER MOVEMENT
                                   , COUNT(CM_TYPE)                AS CM_TYPE_COUNT
                                   , ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE) AS CM_CURRENCY_ISO_CODE
                                   , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
                                     , SUM(ISNULL(CM_SUB_AMOUNT,       0)) CM_SUB_AMOUNT
                                   , SUM(ISNULL(CM_ADD_AMOUNT,        0)) CM_ADD_AMOUNT
                                   , SUM(ISNULL(CM_AUX_AMOUNT,        0)) CM_AUX_AMOUNT
                                   , SUM(ISNULL(CM_INITIAL_BALANCE,   0)) CM_INITIAL_BALANCE
                                   , SUM(ISNULL(CM_FINAL_BALANCE,     0)) CM_FINAL_BALANCE
                      
                             FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_CM_DATE_TYPE)) 
                             WHERE   CM_DATE >= @Date1
                                   AND   CM_DATE <  @Date2
								   AND CM_UNDO_STATUS IS NULL 
                             GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE), ISNULL(CM_CURRENCY_DENOMINATION, 0)
            UNION ALL 
                    SELECT  @Date1                      AS CM_DATE
                        , mbm_type                      AS CM_TYPE 
                         , 1                             AS CM_SUB_TYPE         --MBANK MOVEMENT
                        , COUNT(mbm_type)               AS CM_TYPE_COUNT
                        , @CM_CURRENCY_ISO_CODE         AS CM_CURRENCY_ISO_CODE
                        , 0                             AS CM_CURRENCY_DENOMINATION
                        , SUM(ISNULL(mbm_sub_amount,0)) AS CM_SUB_AMOUNT
                        , SUM(ISNULL(mbm_add_amount,0)) AS CM_ADD_AMOUNT        
                         , 0                             AS CM_AUX_AMOUNT 
                         , 0                             AS CM_INITIAL_BALANCE
                        , 0                             AS CM_FINAL_BALANCE
                      
             FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))
            WHERE   mbm_datetime >= @Date1
                             AND   mbm_datetime <  @Date2
            GROUP BY    mbm_type 
            
RETURN

END--[CM_OldHistoryByHour]

GO

-- CashierMovementsHistory
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsHistory]
GO

CREATE PROCEDURE [dbo].[CashierMovementsHistory]        
                        @pSessionId             BIGINT
                       ,@MovementId             BIGINT
                       ,@pType                  INT    
                       ,@pSubType               INT
                       ,@pInitialBalance        MONEY 
                       ,@pAddAmount             MONEY
                       ,@pSubAmmount            MONEY
                       ,@pFinalBalance          MONEY
                       ,@pCurrencyCode          NVARCHAR(3)
                       ,@pAuxAmount             MONEY
                       ,@pCurrencyDenomination  MONEY
                       ,@pCurrencyCageType      INT
AS
BEGIN 
  DECLARE @_idx_try int
  DECLARE @_row_count int

  SET @pCurrencyDenomination = ISNULL(@pCurrencyDenomination, 0)
  SET @pCurrencyCageType     = ISNULL(@pCurrencyCageType, 0)
  
/******** Grouped by ID ***************/

  SET @_idx_try = 0

  WHILE @_idx_try < 2
  BEGIN
    UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       SET   CM_TYPE_COUNT            = CM_TYPE_COUNT      + CASE WHEN @pAddAmount > 0 THEN 1 ELSE -1 END 
           , CM_SUB_AMOUNT            = CM_SUB_AMOUNT      + ISNULL(@pSubAmmount, 0)
           , CM_ADD_AMOUNT            = CM_ADD_AMOUNT      + ISNULL(@pAddAmount, 0)
           , CM_AUX_AMOUNT            = CM_AUX_AMOUNT      + ISNULL(@pAuxAmount, 0)
           , CM_INITIAL_BALANCE       = CM_INITIAL_BALANCE + ISNULL(@pInitialBalance, 0)
           , CM_FINAL_BALANCE         = CM_FINAL_BALANCE   + ISNULL(@pFinalBalance, 0)
     WHERE   CM_SESSION_ID            = @pSessionId 
       AND   CM_TYPE                  = @pType 
       AND   CM_SUB_TYPE              = @pSubType 
       AND   CM_CURRENCY_ISO_CODE     = @pCurrencyCode
       AND   CM_CURRENCY_DENOMINATION = @pCurrencyDenomination
       AND   CM_CAGE_CURRENCY_TYPE    = @pCurrencyCageType

    SET @_row_count = @@ROWCOUNT
   
    IF @_row_count = 1 BREAK

    IF @_idx_try = 1
    BEGIN
      RAISERROR ('Update into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
    END

    BEGIN TRY
      -- Entry doesn't exists, lets insert that
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
                  ( CM_SESSION_ID
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_TYPE_COUNT
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_CAGE_CURRENCY_TYPE)
           VALUES
                  ( @pSessionId
                  , @pType        
                  , @pSubType
                  , 1  
                  , @pCurrencyCode
                  , @pCurrencyDenomination
                  , ISNULL(@pSubAmmount, 0)
                  , ISNULL(@pAddAmount, 0)
                  , ISNULL(@pAuxAmount, 0)             
                  , ISNULL(@pInitialBalance, 0)
                  , ISNULL(@pFinalBalance, 0)
                  , ISNULL(@pCurrencyCageType, 0))

			SET @_row_count = @@ROWCOUNT

      -- Update history flag in CASHIER_SESSIONS
      -- UPDATE CASHIER_SESSIONS SET CS_HISTORY = 1 WHERE CS_SESSION_ID = @pSessionId

      -- All ok, exit WHILE      
      BREAK

    END TRY
    BEGIN CATCH
    END CATCH

    SET @_idx_try = @_idx_try + 1
  END
  
  IF @_row_count = 0
  BEGIN
    RAISERROR ('Insert into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
  END

  -- AJQ & RCI & RMS 10-FEB-2014: Movements historicized by hour are not inserted here. They serialize all workers and reduce performance.
  --                              A pending cashier movement to historify will be inserted in table CASHIER_MOVEMENTS_PENDING_HISTORY.
  --                              Later a thread will historify these pending movements.
  INSERT INTO CASHIER_MOVEMENTS_PENDING_HISTORY (CMPH_MOVEMENT_ID, CMPH_SUB_TYPE) VALUES (@MovementId, @pSubType) 

END --[CashierMovementsHistory]
GO

GRANT EXECUTE ON [dbo].[CashierMovementsHistory] TO [wggui] WITH GRANT OPTION
GO


-- SP_GenerateHistoricalMovements_ByHour
-- Drops
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateHistoricalMovements_ByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
GO

CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
@pMaxItemsPerLoop Integer, 
@pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

-- Declarations
DECLARE @_max_movements_per_loop Integer
DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                                          CMPH_SUB_TYPE Integer)   
DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                                          UM_TYPE Integer, 
                                          UM_SUBTYPE Integer, 
                                          UM_CURRENCY_ISO_CODE NVarChar(3), 
                                          UM_CURRENCY_DENOMINATION Money,
                                          UM_CAGE_CURRENCY_TYPE Integer )

-- Initialize max items per loop
SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- If there are no items in pending movements table
IF @_total_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), CAST(0 AS Integer)
   
   RETURN
END

-- Get the movements id's to be updated
INSERT   INTO @_tmp_updating_movements
SELECT   TOP(@_max_movements_per_loop) 
         CMPH_MOVEMENT_ID
       , CMPH_SUB_TYPE
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- Obtain the number of movements to update
SELECT   @_updated_movements_count = COUNT(*) 
  FROM   @_tmp_updating_movements 

-- If there are items to update process it
IF @_updated_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), @_total_movements_count 
   
   RETURN
END

-- DELETE THE TO BE UPDATED REGS FROM PENDING
DELETE   X
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY AS X
 INNER   JOIN @_tmp_updating_movements AS Z ON Z.CMPH_MOVEMENT_ID = X.CMPH_MOVEMENT_ID AND Z.CMPH_SUB_TYPE = X.CMPH_SUB_TYPE

-- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
       , X.CM_CAGE_CURRENCY_TYPE
  INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
  FROM
       (
         SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
                , CM_TYPE
                , 0 AS CM_SUB_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
                , COUNT(CM_TYPE)          AS CM_TYPE_COUNT
                , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
                , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
                , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
                , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
                , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)               AS CM_CAGE_CURRENCY_TYPE
           FROM   @_tmp_updating_movements
          INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
          WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
                  AND CM_UNDO_STATUS IS NULL
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
                , CM_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)
       ) AS X 

---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
            , MBM_TYPE   AS CM_TYPE
            , 1          AS CM_SUB_TYPE
            , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
            , 0                AS CM_CURRENCY_DENOMINATION
            , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
            , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
            , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
            , 0 AS CM_AUX_AMOUNT
            , 0 AS CM_INITIAL_BALANCE
            , 0 AS CM_FINAL_BALANCE
            , 0 AS CM_CAGE_CURRENCY_TYPE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
   GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
            , MBM_TYPE 

-- Update the items already in database triggering the updates into a temporary table of updated elements
UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
   SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION
       , INSERTED.CM_CAGE_CURRENCY_TYPE
  INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION, UM_CAGE_CURRENCY_TYPE)
  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
 INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                                                AND T.CM_TYPE                  = S.CM_TYPE
                                                AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                                                AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                                                AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 
                                                AND T.CM_CAGE_CURRENCY_TYPE    = S.CM_CAGE_CURRENCY_TYPE

-- Insert the elements not previously updated (update items not present in temporary updated items)
INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
           (   CM_DATE
             , CM_TYPE
             , CM_SUB_TYPE
             , CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT     
             , CM_ADD_AMOUNT      
             , CM_AUX_AMOUNT     
             , CM_INITIAL_BALANCE 
             , CM_FINAL_BALANCE  
             , CM_CAGE_CURRENCY_TYPE )
     SELECT   CM_DATE
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_TYPE_COUNT
            , CM_SUB_AMOUNT     
            , CM_ADD_AMOUNT      
            , CM_AUX_AMOUNT     
            , CM_INITIAL_BALANCE 
            , CM_FINAL_BALANCE
            , CM_CAGE_CURRENCY_TYPE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                           FROM   @_tmp_updated_movements NTI
                          WHERE   NTI.UM_DATE                  = CM_DATE
                            AND   NTI.UM_TYPE                  = CM_TYPE
                            AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                            AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                            AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION
                            AND   NTI.UM_CAGE_CURRENCY_TYPE    = CM_CAGE_CURRENCY_TYPE) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
  
-- Return values
SELECT @_updated_movements_count, @_total_movements_count

-- Erase the temporary data
DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 
--

END  -- End Procedure
GO

-- Permissions
GRANT EXECUTE ON [dbo].[SP_GenerateHistoricalMovements_ByHour] TO [wggui] WITH GRANT OPTION ;
GO


/******* TRIGGERS *******/
