/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 40;

SET @New_ReleaseId = 41;
SET @New_ScriptName = N'UpdateTo_18.041.008.sql';
SET @New_Description = N'Added new field to Account_Operations; added index to Gui_Audit and Account_Operations.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Account_Operations */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Account_Operations]') and name = 'ao_automatically_printed')
  ALTER TABLE [dbo].[Account_Operations]
          ADD [ao_automatically_printed] [bit] NOT NULL CONSTRAINT DF_account_operations_ao_automatically_printed DEFAULT (1);
ELSE
  SELECT '***** Field Account_Operations.ao_automatically_printed already exists *****';
GO

ALTER TABLE [dbo].[Account_Operations] 
	DROP CONSTRAINT DF_account_operations_ao_automatically_printed;
GO

ALTER TABLE [dbo].[Account_Operations]
	ADD CONSTRAINT DF_account_operations_ao_automatically_printed DEFAULT 0 FOR ao_automatically_printed;
GO	

/****** INDEXES ******/
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gui_audit]') AND name = N'IX_gui_audit_ga_datetime')
CREATE NONCLUSTERED INDEX [IX_gui_audit_ga_datetime] ON [dbo].[gui_audit]
(
      [ga_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index gui_audit.IX_gui_audit_ga_datetime already exists *****';

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_operations]') AND name = N'IX_account_operations_code_printed_date')
CREATE NONCLUSTERED INDEX [IX_account_operations_code_printed_date] ON [dbo].[account_operations]
(
      [ao_code] ASC,
      [ao_automatically_printed] ASC,
      [ao_datetime] ASC
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index account_operations.IX_account_operations_code_printed_date already exists *****';
    

/****** TRIGGERS ******/


/****** RECORDS ******/
   
   
