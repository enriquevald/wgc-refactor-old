/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 203;

SET @New_ReleaseId = 204;
SET @New_ScriptName = N'UpdateTo_18.204.037.sql';
SET @New_Description = N'VIP columns for gifts, promotions and draws; alarms configuration; new task for site alarms.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/* Gifts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_vip')
  ALTER TABLE [dbo].[gifts] ADD [gi_vip] [bit] NULL;

/* Promotions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_vip')
  ALTER TABLE [dbo].[promotions] ADD [pm_vip] [bit] NULL;

/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_vip')
  ALTER TABLE [dbo].[draws] ADD [dr_vip] [bit] NULL;
GO

/*
DROP COLUMNS
*/
IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_PLAYS_PER_SECOND')
		  ALTER TABLE DBO.TERMINAL_STATUS DROP COLUMN TS_PLAYS_PER_SECOND
GO

IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_CENTS_PER_SECOND')
		  ALTER TABLE DBO.TERMINAL_STATUS DROP COLUMN TS_CENTS_PER_SECOND
GO

IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_JACKPOT_AMOUNT')
		  ALTER TABLE DBO.TERMINAL_STATUS DROP COLUMN TS_JACKPOT_AMOUNT
GO

IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_WON_AMOUNT')
		  ALTER TABLE DBO.TERMINAL_STATUS DROP COLUMN TS_WON_AMOUNT
GO

/*
CREATE COLUMNS
*/
IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_PLAYED_ALARM_ID')
			  ALTER TABLE DBO.TERMINAL_STATUS ADD ts_played_alarm_id BIGINT NULL
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_WON_ALARM_ID')
			  ALTER TABLE DBO.TERMINAL_STATUS ADD ts_won_alarm_id BIGINT NULL
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_JACKPOT_ALARM_ID')
			  ALTER TABLE DBO.TERMINAL_STATUS ADD ts_jackpot_alarm_id BIGINT NULL
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_CANCELED_CREDIT_ALARM_ID')
			  ALTER TABLE DBO.TERMINAL_STATUS ADD ts_canceled_credit_alarm_id BIGINT NULL
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_COUNTERFEIT_ALARM_ID')
			  ALTER TABLE DBO.TERMINAL_STATUS ADD ts_counterfeit_alarm_id BIGINT NULL
GO


/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alarms]') AND name = N'IX_alarms_timestamp')
CREATE NONCLUSTERED INDEX [IX_alarms_timestamp] ON [dbo].[alarms] 
(     [al_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* IX_ac_holder_is_vip */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_holder_is_vip')
  CREATE NONCLUSTERED INDEX [IX_ac_holder_is_vip] ON [dbo].[accounts] 
  (
        [ac_holder_is_vip] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.  
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
-- 03-JUL-2013 DDM    Added new fields about Money Laundering
-- 11-NOV-2013 JPJ    Added new fields AC_HOLDER_OCCUPATION_ID and AC_BENEFICIARY_OCCUPATION_ID
-- 23-APR-2014 SMN		
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId                  bigint
, @pTrackData                  nvarchar(50)
,	@pHolderName                 nvarchar(200)
,	@pHolderId                   nvarchar(20)
, @pHolderIdType               int
, @pHolderAddress01            nvarchar(50)
, @pHolderAddress02            nvarchar(50)
, @pHolderAddress03            nvarchar(50)
, @pHolderCity                 nvarchar(50)
, @pHolderZip                  nvarchar(10) 
, @pHolderEmail01              nvarchar(50)
,	@pHolderEmail02              nvarchar(50)
,	@pHolderTwitter              nvarchar(50)
,	@pHolderPhoneNumber01        nvarchar(20)
, @pHolderPhoneNumber02        nvarchar(20)
, @pHolderComments             nvarchar(100)
, @pHolderId1                  nvarchar(20)
, @pHolderId2                  nvarchar(20)
, @pHolderDocumentId1          bigint
, @pHolderDocumentId2          bigint
, @pHolderName1                nvarchar(50)
,	@pHolderName2                nvarchar(50)
,	@pHolderName3                nvarchar(50)
, @pHolderGender               int
, @pHolderMaritalStatus        int
, @pHolderBirthDate            datetime
, @pHolderWeddingDate          datetime
, @pHolderLevel                int
, @pHolderLevelNotify          int
, @pHolderLevelEntered         datetime
,	@pHolderLevelExpiration      datetime
,	@pPin                        nvarchar(12)
, @pPinFailures                int
, @pPinLastModified            datetime
, @pBlocked                    bit
, @pActivated                  bit
, @pBlockReason                int
, @pHolderIsVip                int
, @pHolderTitle                nvarchar(15)                       
, @pHolderName4                nvarchar(50)                       
, @pHolderPhoneType01          int
, @pHolderPhoneType02          int
, @pHolderState                nvarchar(50)                    
, @pHolderCountry              nvarchar(50)                
, @pPersonalInfoSequenceId     bigint                     
, @pUserType                   int
, @pPointsStatus               int
, @pDeposit                    money
, @pCardPay                    bit
, @pBlockDescription           nvarchar(256) 
, @pMSHash                     varbinary(20) 
, @pMSCreatedOnSiteId          int
, @pCreated                    datetime
, @pExternalReference          nvarchar(50) 
, @pHolderOccupation           nvarchar(50) 	
, @pHolderExtNum               nvarchar(10) 
, @pHolderNationality          Int 
, @pHolderBirthCountry         Int 
, @pHolderFedEntity            Int 
, @pHolderId1Type              Int -- RFC
, @pHolderId2Type              Int -- CURP
, @pHolderId3Type              nvarchar(50) 
, @pHolderId3                  nvarchar(20) 
, @pHolderHasBeneficiary       bit  
, @pBeneficiaryName            nvarchar(200) 
, @pBeneficiaryName1           nvarchar(50) 
, @pBeneficiaryName2           nvarchar(50) 
, @pBeneficiaryName3           nvarchar(50) 
, @pBeneficiaryBirthDate       Datetime 
, @pBeneficiaryGender          int 
, @pBeneficiaryOccupation      nvarchar(50) 
, @pBeneficiaryId1Type         int   -- RFC
, @pBeneficiaryId1             nvarchar(20) 
, @pBeneficiaryId2Type         int   -- CURP
, @pBeneficiaryId2             nvarchar(20) 
, @pBeneficiaryId3Type         nvarchar(50) 
, @pBeneficiaryId3             nvarchar(20)
, @pHolderOccupationId         int
, @pBeneficiaryOccupationId    int
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT
DECLARE @UserTypeSiteAccount as BIT

SELECT
       @pOtherAccountId = AC_ACCOUNT_ID 
      ,@UserTypeSiteAccount = AC_USER_TYPE
FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData

SET @pOtherAccountId = ISNULL(@pOtherAccountId, 0)
SET @UserTypeSiteAccount = ISNULL(@UserTypeSiteAccount, 1)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
	   IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
			AND @UserTypeSiteAccount = 0 -- IS ANONYMOUS
       BEGIN
			SET @pTrackData = '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
       END
       ELSE 
       BEGIN -- NOT IS ANONYMOUS OR ELP IS NOT ENABLED
			  UPDATE   ACCOUNTS   
				 SET   AC_TRACK_DATA =  '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
			   WHERE   AC_ACCOUNT_ID = @pOtherAccountId
       END


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY       = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED      = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION   = @pHolderLevelExpiration 
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @pBlocked 
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = @pBlockReason 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID   = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				          = @pUserType
         , AC_POINTS_STATUS             = @pPointsStatus
         , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription                
         , AC_CREATED                   = @pCreated
         , AC_MS_CREATED_ON_SITE_ID     = @pMSCreatedOnSiteId
         , AC_MS_HASH                   = @pMSHash
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 

END
GO



/******* RECORDS *******/
/* TASK */
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD) 
     values                   (    64,          1,                 900,                  1000);
GO

DELETE   SAS_METERS_CATALOG_PER_GROUP
 WHERE   SMCG_GROUP_ID      = 10004
   AND   SMCG_METER_CODE IN ( 0x0007, 0x0017, 0x0018 )
GO

IF EXISTS (SELECT TOP 1 * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts')
		   DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY='Alerts'



/*PAYOUT*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Payout.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Payout.Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Payout.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Payout.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Payout.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Payout.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Payout.Limit.Lower')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Payout.Limit.Lower','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Payout.Limit.Upper')
		       INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Payout.Limit.Upper','0')
GO

/*PLAYED*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Played.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Played.Enabled','0')
			   
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Played.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Played.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Played.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Played.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Played.Limit.Amount')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Played.Limit.Amount','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Played.Limit.Quantity')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Played.Limit.Quantity','0')
GO

/*WON*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Won.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Won.Enabled','0')
			   
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Won.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Won.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Won.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Won.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Won.Limit.Amount')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Won.Limit.Amount','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Won.Limit.Quantity')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Won.Limit.Quantity','0')
GO

/*COUNTERFEIT*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Counterfeit.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Counterfeit.Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Counterfeit.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Counterfeit.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Counterfeit.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Counterfeit.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Counterfeit.Limit.Quantity')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Counterfeit.Limit.Quantity','0')
GO

/*JACKPOT*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Jackpot.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Jackpot.Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Jackpot.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Jackpot.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Jackpot.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Jackpot.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Jackpot.Limit.Amount')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Jackpot.Limit.Amount','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Jackpot.Limit.Quantity')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','Jackpot.Limit.Quantity','0')
GO
                      
/*CANCELEDCREDIT*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'CanceledCredit.Enabled')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','CanceledCredit.Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'CanceledCredit.PeriodUnderStudy.Value')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','CanceledCredit.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'CanceledCredit.PeriodUnderStudy.Unit')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','CanceledCredit.PeriodUnderStudy.Unit','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'CanceledCredit.Limit.Amount')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','CanceledCredit.Limit.Amount','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'CanceledCredit.Limit.Quantity')
			   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','CanceledCredit.Limit.Quantity','0')
GO