/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 45;

SET @New_ReleaseId = 46;
SET @New_ScriptName = N'UpdateTo_18.046.010.sql';
SET @New_Description = N'New records in general params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/
/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Draws]') and name = 'dr_bingo_format')
  ALTER TABLE [dbo].[Draws]
          ADD [dr_bingo_format] [bit] NOT NULL CONSTRAINT DF_draws_dr_bingo_format DEFAULT (0);
ELSE
  SELECT '***** Field Draws.dr_bingo_format already exists *****';

ALTER TABLE  [dbo].[Draws]
ALTER COLUMN [dr_header]  [nvarchar](4000) NULL;
 
ALTER TABLE  [dbo].[Draws]
ALTER COLUMN [dr_footer]  [nvarchar](4000) NULL;
 
ALTER TABLE  [dbo].[Draws]
ALTER COLUMN [dr_detail1] [nvarchar](4000) NULL;
 
ALTER TABLE  [dbo].[Draws]
ALTER COLUMN [dr_detail2] [nvarchar](4000) NULL;
 
ALTER TABLE  [dbo].[Draws]
ALTER COLUMN [dr_detail3] [nvarchar](4000) NULL;


/****** INDEXES ******/


/****** RECORDS ******/
/* Cashier.SplitAsWon.Text */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='SplitAsWon.Text')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'SplitAsWon.Text'
             ,'Cup�n Premio');
ELSE
  SELECT '***** Record Cashier.SplitAsWon.Text already exists *****';

/* Cashier.Promotion.Text */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promotion.Text')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Promotion.Text'
             ,'Promoci�n');
ELSE
  SELECT '***** Record Cashier.Promotion.Text already exists *****';

/* Cashier.Split.A.HideTotal */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.A.HideTotal')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Split.A.HideTotal'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Split.A.HideTotal already exists *****';

/* Cashier.Split.B.HideTotal */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.B.HideTotal')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Split.B.HideTotal'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Split.B.HideTotal already exists *****';

/**************************************** 3GS Section ****************************************/
