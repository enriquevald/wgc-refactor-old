/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 165;

SET @New_ReleaseId = 166;
SET @New_ScriptName = N'UpdateTo_18.166.032.sql';
SET @New_Description = N'Update to TITO mode - Added sas_meter_catalog - Added cage columns and tables';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
		ALTER TABLE [dbo].[play_sessions]	   DROP COLUMN [ps_total_cash_in] 	
GO	  
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_total_cash_in] AS 	((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+isnull([ps_promo_nr_ticket_in],(0))))
GO

EXECUTE sp_rename N'dbo.cage_movements.cgm_operation_id', N'Tmp_cgm_type_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.cage_movements.Tmp_cgm_type_1', N'cgm_type', 'COLUMN' 
GO
ALTER TABLE dbo.cage_movements ADD cgm_source_target_id bigint NULL
GO

CREATE TABLE [dbo].[cage_source_target](
	[cst_source_target_id] [bigint] IDENTITY(1000,1) NOT NULL,
	[cst_source_target_name] [nvarchar](50) NOT NULL,
	[cst_source] [bit] NOT NULL,
	[cst_target] [bit] NOT NULL,
 CONSTRAINT [PK_CAGE_SOURCE_TARGET] PRIMARY KEY CLUSTERED 
(
	[cst_source_target_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cage_source_target] ADD  CONSTRAINT [DF_cage_source_cst_source]  DEFAULT ((0)) FOR [cst_source]
GO
ALTER TABLE [dbo].[cage_source_target] ADD  CONSTRAINT [DF_cage_source_cst_target]  DEFAULT ((0)) FOR [cst_target]
GO

CREATE TABLE [dbo].[cage_sessions](
	[cgs_cage_session_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cgs_open_datetime] [datetime] NOT NULL,
	[cgs_open_user_id] [int] NOT NULL,
	[cgs_close_datetime] [datetime] NULL,
	[cgs_close_user_id] [int] NULL,
 CONSTRAINT [PK_CAGE_SESSIONS] PRIMARY KEY CLUSTERED 
(
	[cgs_cage_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.cage_movements ADD cgm_cage_session_id bigint NULL
GO

ALTER TABLE dbo.cage_pending_movements ADD cpm_type int NULL
GO

ALTER TABLE dbo.cage_amounts ADD caa_color int NULL
GO

ALTER TABLE dbo.cage_movements ADD cgm_related_movement_id int NULL
GO

ALTER TABLE dbo.cage_movements DROP CONSTRAINT FK_cage_movements_terminal_cashier
GO	  
	  
ALTER TABLE dbo.cage_movements DROP CONSTRAINT FK_cage_movements_terminal_cage
GO	  

ALTER TABLE dbo.cage_movements DROP CONSTRAINT FK_cage_movements_user_cage
GO

ALTER TABLE dbo.cage_movements DROP CONSTRAINT FK_cage_movements_user_cashier
GO

ALTER TABLE dbo.cage_movements ALTER COLUMN cgm_user_cashier_id int NULL
GO

/****** RECORDS *******/
UPDATE cage_movements
   SET cgm_type = 100
 WHERE cgm_type = 1

UPDATE cage_movements
   SET cgm_type = 1
 WHERE cgm_type = 2

UPDATE cage_movements
   SET cgm_type = 101
 WHERE cgm_type = 3

 
IF NOT EXISTS ( SELECT * FROM CAGE_AMOUNTS WHERE CAA_ISO_CODE = 'X01' AND CAA_DENOMINATION = -100)
  INSERT INTO CAGE_AMOUNTS (CAA_ISO_CODE, CAA_DENOMINATION, CAA_ALLOWED) VALUES ('X01', -100, 'FALSE')


IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'AllowClosingWithOpenCashSessions')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'AllowClosingWithOpenCashSessions', '0')
GO

--
-- Insert sas_meter_catalog
--
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65559, 'AC power was applied to gaming machine', 0)               -- 0x00010017
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65553, 'Slot door was opened', 0)                                 -- 0x00010011
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65555, 'Drop door was opened', 0)                                 -- 0x00010013
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65557, 'Card cage was opened', 0)                                 -- 0x00010015
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65561, 'Cashbox door was opened', 0)                              -- 0x00010019
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65565, 'Belly door was opened', 0)                                -- 0x0001001D
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65554, 'Slot door was closed', 0)                                 -- 0x00010012
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65556, 'Drop door was closed', 0)                                 -- 0x00010014
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65558, 'Card cage was closed', 0)                                 -- 0x00010016
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65562, 'Cashbox door was closed', 0)                              -- 0x0001001A
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65566, 'Belly door was closed', 0)                                -- 0x0001001E
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65585, 'CMOS RAM error (data recovered from EEPROM)', 0)          -- 0x00010031
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65586, 'CMOS RAM error (no data recovered from EEPROM)', 0)       -- 0x00010032
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65587, 'CMOS RAM error (bad device)', 0)                          -- 0x00010033
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65588, 'EEPROM error (data error)', 0)                            -- 0x00010034
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65589, 'EEPROM error (bad device)', 0)                            -- 0x00010035
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65590, 'EPROM error (different checksum - version changed)', 0)   -- 0x00010036
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65591, 'EPROM error (bad checksum compare)', 0)                   -- 0x00010037
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65592, 'Partitioned EPROM error (checksum - versoin changed)', 0) -- 0x00010038
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65593, 'Partitioned EPROM error (bad checksum compare)', 0)       -- 0x00010039
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65595, 'Low backup battery detected', 0)                          -- 0x0001003B
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65793, 'SAS disconnected', 0)                                     -- 0x00010101
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65794, 'SAS connected', 0)                                        -- 0x00010102
                                                                                                                                                          
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65576, 'Bill jam', 0)                                             -- 0x00010028
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65577, 'Bill acceptor hardware failure', 0)                       -- 0x00010029
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65578, 'Reverse bill detected', 0)                                -- 0x0001002A
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65579, 'bill rejected', 0)                                        -- 0x0001002B
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65580, 'Counterfeit bill detected', 0)                            -- 0x0001002C
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65632, 'Printer communication error', 0)                          -- 0x00010060
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65633, 'Printer paper out error', 0)                              -- 0x00010061
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65652, 'Printer paper low', 0)                                    -- 0x00010074
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65653, 'Printer power off', 0)                                    -- 0x00010075
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65654, 'Printer power on', 0)                                     -- 0x00010076
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65655, 'Replace printer ribbon', 0)                               -- 0x00010077
INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(65656, 'Printer carriage jammed', 0)                              -- 0x00010078

--
-- Insert new group 'Events' 
--
INSERT INTO sas_meters_groups (smg_group_id, smg_name, smg_description, smg_required)
                       VALUES (       10003, 'Events', 'SAS Events'   ,            0)      
--
-- Insert relations group 'Events' 
--
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65559)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65553)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65555)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65557)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65561)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65565)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65554)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65556)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65558)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65562)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65566)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65585)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65586)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65587)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65588)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65589)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65590)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65591)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65592)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65593)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65595)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65793)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65794)

INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65576)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65577)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65578)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65579)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65580)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65632)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65633)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65652)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65653)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65654)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65655)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10003, 65656)
GO

/*
* 25-FEB-2013   JCA   Petici�n de no crear una sesi�n de b�veda predifinida. 
IF NOT EXISTS ( SELECT TOP 1 * FROM cage_sessions  WHERE CGS_CLOSE_DATETIME IS NULL AND CGS_OPEN_DATETIME IS NOT NULL)
  INSERT INTO   cage_sessions ( CGS_OPEN_USER_ID, CGS_OPEN_DATETIME ) VALUES ( 0, GETDATE() )

GO
 
DECLARE @pCageSessionID AS BIGINT
SET @pCageSessionID = (SELECT TOP 1 cgs_cage_session_id FROM cage_sessions  WHERE cgs_close_datetime IS NULL AND cgs_open_datetime IS NOT NULL)
UPDATE cage_movements SET cgm_cage_session_id = @pCageSessionID 
GO
*/