/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 433;

SET @New_ReleaseId = 434;

SET @New_ScriptName = N'UpdateTo_18.434.041.sql';
SET @New_Description = N'New release v03.007.0001'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/
IF NOT EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'Terminal' AND gp_subject_key = 'AllowStartCardSessionWithAnotherReserve')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)VALUES ('Terminal', 'AllowStartCardSessionWithAnotherReserve', 0)
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Countr' AND GP_SUBJECT_KEY = 'WebApi.Uri')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Countr', 'WebApi.Uri', 'http://0.0.0.0:8082')
GO  

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'PointsAssignment.Mode')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables', 'PointsAssignment.Mode', '0')
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Countr' COLLATE SQL_Latin1_General_CP1_CS_AS AND GP_SUBJECT_KEY = 'WebApi.Uri')
BEGIN
	UPDATE   GENERAL_PARAMS 
	   SET   gp_group_key   = 'CountR'
	 WHERE   gp_group_key   = 'Countr'
	   AND   gp_subject_key = 'WebApi.Uri'
END
GO  

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'BetsOver.HandByCustomer')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'BetsOver.HandByCustomer', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'SessionLossFor')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'SessionLossFor', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'Buffering.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AGG', 'Buffering.Enabled', '0')
GO  

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'InvestmentsWithCreditDebitCard.EqualOrOver')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'InvestmentsWithCreditDebitCard.EqualOrOver', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'CustomerGetsPaidWithoutAPurchase')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'CustomerGetsPaidWithoutAPurchase', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'BetsVariation.CustomerPerSession')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'BetsVariation.CustomerPerSession', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'PaymentDoneByPlayer.UnderMinutes')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'PaymentDoneByPlayer.UnderMinutes', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'PlayerSittingTimeMinutes')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'PlayerSittingTimeMinutes', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'AmountPurchaseInOneOperation')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'AmountPurchaseInOneOperation', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'AmountPaidInOneOperation')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'AmountPaidInOneOperation', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms.GamingTables' AND GP_SUBJECT_KEY = 'GamingTable3DayLost')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Alarms.GamingTables', 'GamingTable3DayLost', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Client.Registration' AND GP_SUBJECT_KEY = 'Output.Threshold')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Client.Registration', 'Output.Threshold', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Client.Registration' AND GP_SUBJECT_KEY = 'Input.Threshold')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Client.Registration', 'Input.Threshold', '0')
GO

IF NOT EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'Threshold.EGM.BillIn')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)VALUES ('TITO', 'Threshold.EGM.BillIn', 0)
GO

IF NOT EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'Threshold.EGM.BillIn.BlockMode')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)VALUES ('TITO', 'Threshold.EGM.BillIn.BlockMode', 0)
GO



/**** TABLES *****/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_reserve_account_id')
      ALTER TABLE [dbo].terminals ADD te_reserve_account_id bigint NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_reservation_expires')
      ALTER TABLE [dbo].terminals ADD te_reservation_expires datetime NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_egm_reserved')
      ALTER TABLE [dbo].accounts ADD ac_egm_reserved int NULL;
GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_configuration') AND type IN ('U'))
  CREATE TABLE reserved_terminal_configuration
  (
		rtc_holder_level  INT NOT NULL,
		rtc_minutes       INT NOT NULL,
	  rtc_terminal_list XML NULL,
	  rtc_create_date   DATETIME NOT NULL,
	  rtc_update_date   DATETIME NOT NULL,
	  rtc_status        INT NOT NULL
		CONSTRAINT pk_reserved_terminal_configuration PRIMARY KEY (rtc_holder_level)
  )     
GO

  -- CREATE NEW TABLE RESERVE TERMNINAL TRANSACTION

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_transaction') AND type IN ('U'))
  CREATE TABLE reserved_terminal_transaction
  (
		rtt_transaction_id  BIGINT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		rtt_account_id      BIGINT   NOT NULL FOREIGN KEY REFERENCES accounts (ac_account_id),
	  rtt_terminal_id     INT      NOT NULL FOREIGN KEY REFERENCES terminals(te_terminal_id),
	  rtt_start_reserved   DATETIME NOT NULL,
	  rtt_end_reserved     DATETIME         ,
	  rtt_start_utc       DATETIME NOT NULL,
	  rtt_end_utc         DATETIME         ,
	  rtt_max_minutes     INT      NOT NULL,
	  rtt_total_minutes   INT              ,
	  rtt_status          INT      NOT NULL   		
  )     
GO

-- ADD COLUMN ts_is_reserved IN terminal_status
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TERMINAL_STATUS' AND COLUMN_NAME = 'TS_IS_RESERVED')
      ALTER TABLE terminal_status ADD ts_is_reserved BIT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_reserve_account_id')
      ALTER TABLE [dbo].terminals ADD te_reserve_account_id bigint NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_reservation_expires')
      ALTER TABLE [dbo].terminals ADD te_reservation_expires datetime NULL;
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_egm_reserved')
      ALTER TABLE [dbo].accounts ADD ac_egm_reserved int NULL;
GO

IF NOT EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'Terminal' AND gp_subject_key = 'AllowStartCardSessionWithAnotherReserve')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)VALUES ('Terminal', 'AllowStartCardSessionWithAnotherReserve', 0)
GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_configuration') AND type IN ('U'))
  CREATE TABLE reserved_terminal_configuration
  (
		rtc_holder_level  INT NOT NULL,
		rtc_minutes       INT NOT NULL,
	  rtc_terminal_list XML NULL,
	  rtc_create_date   DATETIME NOT NULL,
	  rtc_update_date   DATETIME NOT NULL,
	  rtc_status        INT NOT NULL
		CONSTRAINT pk_reserved_terminal_configuration PRIMARY KEY (rtc_holder_level)
  )     
GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_transaction') AND type IN ('U'))
  CREATE TABLE reserved_terminal_transaction
  (
		rtt_transaction_id  BIGINT IDENTITY(1,1) NOT NULL PRIMARY KEY,
		rtt_account_id      BIGINT   NOT NULL FOREIGN KEY REFERENCES accounts (ac_account_id),
	  rtt_terminal_id     INT      NOT NULL FOREIGN KEY REFERENCES terminals(te_terminal_id),
	  rtt_start_reserved   DATETIME NOT NULL,
	  rtt_end_reserved     DATETIME         ,
	  rtt_start_utc       DATETIME NOT NULL,
	  rtt_end_utc         DATETIME         ,
	  rtt_max_minutes     INT      NOT NULL,
	  rtt_total_minutes   INT              ,
	  rtt_status          INT      NOT NULL   		
  )     
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TERMINAL_STATUS' AND COLUMN_NAME = 'TS_IS_RESERVED')
   ALTER TABLE terminal_status ADD ts_is_reserved BIT
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[countr_log]') and name = 'crl_request')
	ALTER TABLE countr_log ALTER COLUMN crl_request NVARCHAR (MAX)
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[countr_log]') and name = 'crl_response')
	ALTER TABLE countr_log ALTER COLUMN crl_response NVARCHAR (MAX)
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'gaming_tables' AND COLUMN_NAME = 'gt_linked_cashier_id') 
	ALTER TABLE [dbo].[gaming_tables] ADD [gt_linked_cashier_id] [Int] NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buffer_terminal_sas_meters]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[buffer_terminal_sas_meters](
		[btsm_terminal_id] [int] NOT NULL,
		[btsm_meter_code] [int] NOT NULL,
		[btsm_game_id] [int] NOT NULL,
		[btsm_denomination] [money] NOT NULL,
		[btsm_timestamp] [datetime] NOT NULL
		
		 CONSTRAINT [PK_buffer_terminal_sas_meters] PRIMARY KEY CLUSTERED 
	(
		[btsm_terminal_id] ASC,
		[btsm_meter_code] ASC,
		[btsm_game_id] ASC,
		[btsm_denomination] ASC,
		[btsm_timestamp] ASC
	)
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buffer_terminal_sas_meters_history]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[buffer_terminal_sas_meters_history](
		[btsmh_terminal_id] [int] NOT NULL,
		[btsmh_meter_code] [int] NOT NULL,
		[btsmh_game_id] [int] NOT NULL,
		[btsmh_denomination] [money] NOT NULL,
		[btsmh_type] [int] NOT NULL,
		[btsmh_datetime] [datetime] NOT NULL,
		[btsmh_timestamp] [datetime] NOT NULL
		 CONSTRAINT [PK_buffer_terminal_sas_meters_history] PRIMARY KEY CLUSTERED 
	(
		[btsmh_terminal_id] ASC,
		[btsmh_meter_code] ASC,
		[btsmh_game_id] ASC,
		[btsmh_denomination] ASC,
		[btsmh_type] ASC,
		[btsmh_datetime] ASC,
		[btsmh_timestamp] ASC	
	)
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buffer_alarms]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[buffer_alarms](
		[ba_id] [bigint] NOT NULL,
		[ba_timestamp] [datetime] NOT NULL	
		 CONSTRAINT [PK_buffer_alarms] PRIMARY KEY CLUSTERED 
	(
		[ba_id] ASC,
		[ba_timestamp] ASC	
	)
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'threshold_operations')
BEGIN
  CREATE TABLE [dbo].[threshold_operations](
      to_threshold_id              BigInt IDENTITY(1,1) NOT NULL,
      to_transaction_type          Int NOT NULL,
      to_transaction_subtype       Int NOT NULL,
      to_total_amount              Money NOT NULL,
      to_cash_amount               Money NOT NULL,
      to_transaction_date          DateTime NOT NULL,
      to_operation_id              BigInt, 
      to_operation_code            Int, 
      to_undo_operation_id         BigInt, 
      to_holder_id_type            Int,
      to_holder_id                 NVarChar(20) NOT NULL,
      to_user_id                   Int NOT NULL,
      to_user_name                 NVarChar(50) NOT NULL,
      to_account_id                BigInt NOT NULL,
      to_holder_name               NVarChar(200) NULL,
      to_holder_name1              NVarChar(50) NULL,
      to_holder_name2              NVarChar(50) NULL,
      to_holder_name3              NVarChar(50) NULL,
      to_holder_name4              NVarChar(50) NULL,
      to_holder_address_01         NVarChar(50) NULL,
      to_holder_address_02         NVarChar(50) NULL,
      to_holder_address_03         NVarChar(50) NULL,
      to_holder_ext_num            NVarChar(10) NULL,
      to_holder_city               NVarChar(50) NULL,
      to_holder_fed_entity         Int NULL,
      to_holder_zip                NVarChar(10) NULL,
      to_holder_address_country    Int NULL,
      to_holder_nationality        Int NULL,
      to_holder_birth_date         DateTime NULL,
      to_holder_occupation_id      Int NULL,
      to_scanned_Identity_card     varbinary(max) NULL
  CONSTRAINT PK_threshold_operations PRIMARY KEY CLUSTERED 
  (
	  to_threshold_id ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table [threshold_operations] already exists *****';
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'APO_PAYMENT_SUBTYPE' AND Object_ID = Object_ID(N'ACCOUNT_PAYMENT_ORDERS'))
BEGIN
  ALTER TABLE ACCOUNT_PAYMENT_ORDERS ADD APO_PAYMENT_SUBTYPE int NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'cashier_sessions') and name = 'cs_has_pinpad_operations')
BEGIN
  ALTER TABLE cashier_sessions ADD cs_has_pinpad_operations BigInt NULL;
END
GO

IF EXISTS(SELECT * FROM report_tool_config)
BEGIN
	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'rtc_html_header' AND TABLE_NAME = 'report_tool_config')
	BEGIN
		ALTER TABLE report_tool_config ADD rtc_html_header XML	  
	END

	IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'rtc_html_footer' AND TABLE_NAME = 'report_tool_config')
	BEGIN
		ALTER TABLE report_tool_config ADD rtc_html_footer XML
	END
END
GO



/**** RECORDS *****/
-- ALARMS
IF NOT EXISTS (SELECT 1 FROM ALARM_CATEGORIES WHERE alc_category_id = 59)
BEGIN
  
	 -- ALARM CATEGORIES                    
	INSERT INTO ALARM_CATEGORIES VALUES(59, 9,  5, 0, N'BankCard'  , '', 1)
	INSERT INTO ALARM_CATEGORIES VALUES(59, 10, 5, 0, N'Tarjeta Bancaria', '', 1)

	INSERT INTO ALARM_CATEGORIES VALUES(60, 9,  5, 0, N'NFC', '', 1)
	INSERT INTO ALARM_CATEGORIES VALUES(60, 10, 5, 0, N'NFC', '', 1)

	INSERT INTO ALARM_CATEGORIES VALUES(61, 9,  5, 0, N'Custom'  , '', 1)
	INSERT INTO ALARM_CATEGORIES VALUES(61, 10, 5, 0, N'Personalizada', '', 1)

	-- ALARM CATALOG
	INSERT INTO ALARM_CATALOG VALUES(5242912, 9, 0, N'Inserted', N'Inserted', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242912, 10, 0, N'Insertada', N'Insertada', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242913, 9, 0, N'Captured', N'Captured', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242913, 10, 0, N'Capturada', N'Capturada', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242914, 9, 0, N'Issued', N'Issued', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242914, 10, 0, N'Emitida', N'Emitida', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242915, 9, 0, N'Transaction started', N'Transaction started', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242915, 10, 0, N'Transacci�n iniciada', N'Transacci�n iniciada', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242916, 9, 0, N'Transaction success', N'Transaction success', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242916, 10, 0, N'Transacci�n completada', N'Transacci�n completada', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242917, 9, 0, N'Transaction failed', N'Transaction failed', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242917, 10, 0, N'Transacci�n err�nea', N'Transacci�n err�nea', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242918, 9, 0, N'Arrived', N'Arrived', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242918, 10, 0, N'Conectado', N'Conectado', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242919, 9, 0, N'Departed', N'Departed', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242919, 10, 0, N'Desconectado', N'Desconectado', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242920, 9, 0, N'Rejected', N'Rejected', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242920, 10, 0, N'Rechazado', N'Rechazado', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242921, 9, 0, N'Payment sequence error - Paid', N'Payment sequence error - Paid', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242921, 10, 0, N'Secuencia de pago err�nea - Pagado', N'Secuencia de pago err�nea - Pagado', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242928, 9, 0, N'Payment sequence error - Unpaid', N'Payment sequence error - Unpaid', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242928, 10, 0, N'Secuencia de pago err�nea - No pagado', N'Secuencia de pago err�nea - No pagado', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242929, 9, 0, N'Wrong kiosk', N'Wrong kiosk', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242929, 10, 0, N'Kiosko err�neo', N'Kiosko err�neo', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242930, 9, 0, N'Incorrect status', N'Incorrect status', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242930, 10, 0, N'Estado err�neo', N'Estado err�neo', 1)

	INSERT INTO ALARM_CATALOG VALUES(5244160, 9, 0, N'Custom', N'Custom', 1)
	INSERT INTO ALARM_CATALOG VALUES(5244160, 10, 0, N'Personalizada', N'Personalizada', 1)

	-- ALARM CATALOG PER CATEGORY
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242912,59, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242913,59, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242914,59, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242915,59, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242916,59, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242917,59, 0, GETDATE())

	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242918,60, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242919,60, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242920,60, 0, GETDATE())

	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242921,55, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242928,55, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242929,55, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242930,55, 0, GETDATE())

	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5244160,61, 0, GETDATE()) 

-- Updates 20170926
 UPDATE   ALARM_GROUPS 
    SET   ALG_NAME = N'Redemption Kiosk' 
  WHERE   ALG_ALARM_GROUP_ID = 5 
    AND   ALG_LANGUAGE_ID = 9
 
  UPDATE   ALARM_GROUPS 
     SET   ALG_NAME = N'Kiosco de Redenci�n' 
   WHERE   ALG_ALARM_GROUP_ID = 5 
     AND   ALG_LANGUAGE_ID = 10
 
-- ALARM CATEGORIES    
 
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'NFC Device'
     WHERE alc_category_id = 60 
     AND alc_language_id = 9
     
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Dispositivo NFC'
   WHERE   ALC_CATEGORY_ID = 60 
     AND   ALC_LANGUAGE_ID = 10
 
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'TITO Tickets'
   WHERE   ALC_CATEGORY_ID = 58 
     AND   ALC_LANGUAGE_ID = 9
     
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Player card'
   WHERE   ALC_CATEGORY_ID = 53 
     AND   ALC_LANGUAGE_ID = 9
     
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Tarjeta de jugador'
   WHERE   ALC_CATEGORY_ID = 53 
     AND   ALC_LANGUAGE_ID = 10
     
   UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Bill'
   WHERE   ALC_CATEGORY_ID = 54 
     AND   ALC_LANGUAGE_ID = 9
     
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'TITO Tickets'
   WHERE   ALC_CATEGORY_ID = 55 
     AND   ALC_LANGUAGE_ID = 9
     
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Tickets TITO'
   WHERE   ALC_CATEGORY_ID = 55 
     AND   ALC_LANGUAGE_ID = 10
 
  UPDATE   ALARM_CATEGORIES
     SET   ALC_NAME = N'Call attendant'
   WHERE   ALC_CATEGORY_ID = 56 
     AND   ALC_LANGUAGE_ID = 9
    
-- ALARM CATALOG
  
  UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Tarjeta bancaria Insertada'      
         , ALCG_DESCRIPTION =  N'Tarjeta bancaria Insertada'      
   WHERE   ALCG_ALARM_CODE = 5242912
     AND   ALCG_LANGUAGE_ID = 10
  
  UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Inserted Bankcard'    
         , ALCG_DESCRIPTION =  N'Inserted Bankcard' 
   WHERE   ALCG_ALARM_CODE = 5242912
     AND   ALCG_LANGUAGE_ID = 9
 
   UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Tarjeta bancaria Capturada'      
         , ALCG_DESCRIPTION = N'Tarjeta bancaria Capturada'  
   WHERE   ALCG_ALARM_CODE = 5242913
     AND   ALCG_LANGUAGE_ID = 10
  
  UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Captured BankCard'  
         , ALCG_DESCRIPTION = N'Captured BankCard' 
   WHERE   ALCG_ALARM_CODE = 5242913
     AND   ALCG_LANGUAGE_ID = 9
     
  UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Tarjeta bancaria Expulsada'   
         , ALCG_DESCRIPTION = N'Tarjeta bancaria Expulsada' 
   WHERE   ALCG_ALARM_CODE = 5242914
     AND   ALCG_LANGUAGE_ID = 10
  
  UPDATE   ALARM_CATALOG 
     SET   ALCG_NAME =  N'Released BankCard'
         , ALCG_DESCRIPTION = N'Released BankCard'
   WHERE   ALCG_ALARM_CODE = 5242914
     AND   ALCG_LANGUAGE_ID = 9
 
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Transaction successfull'
          , ALCG_DESCRIPTION =  N'Transaction successfull'
    WHERE   ALCG_ALARM_CODE = 5242916
      AND   ALCG_LANGUAGE_ID = 9
     
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'NFC Device Detected'
         ,  ALCG_DESCRIPTION =  N'NFC Device Detected'
    WHERE   ALCG_ALARM_CODE = 5242918
      AND   ALCG_LANGUAGE_ID = 9
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Dispositivo NFC Detectado'
         ,  ALCG_DESCRIPTION =   N'Dispositivo NFC Detectado'
    WHERE   ALCG_ALARM_CODE = 5242918
      AND   ALCG_LANGUAGE_ID = 10

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'NFC Device Removed'
         ,  ALCG_DESCRIPTION =  N'NFC Device Removed'
    WHERE   ALCG_ALARM_CODE = 5242919
      AND   ALCG_LANGUAGE_ID = 9
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Dispositivo NFC Extraido'
         ,  ALCG_DESCRIPTION =   N'Dispositivo NFC Extraido'
    WHERE   ALCG_ALARM_CODE = 5242919
      AND   ALCG_LANGUAGE_ID = 10
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'NFC Device Rejected'
         ,  ALCG_DESCRIPTION =  N'NFC Device Rejected'
    WHERE   ALCG_ALARM_CODE = 5242920
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Dispositivo NFC Rechazado'
         ,  ALCG_DESCRIPTION =   N'Dispositivo NFC Rechazado'
    WHERE   ALCG_ALARM_CODE = 5242920
      AND   ALCG_LANGUAGE_ID = 10      
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Unexpected Ticket'
         ,  ALCG_DESCRIPTION =   N'Unexpected Ticket'
    WHERE   ALCG_ALARM_CODE = 5242921
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket no esperado'
         ,  ALCG_DESCRIPTION =   N'Ticket no esperado'
    WHERE   ALCG_ALARM_CODE = 5242921
      AND   ALCG_LANGUAGE_ID = 10   
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Unexpected Ticket'
         ,  ALCG_DESCRIPTION =   N'Unexpected Ticket'
    WHERE   ALCG_ALARM_CODE = 5242928
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket no esperado'
         ,  ALCG_DESCRIPTION =   N'Ticket no esperado'
    WHERE   ALCG_ALARM_CODE = 5242928
      AND   ALCG_LANGUAGE_ID = 10
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Error printing Ticket'
         ,  ALCG_DESCRIPTION =   N'Error printing Ticket'
    WHERE   ALCG_ALARM_CODE = 5242929
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Error al imprimir Ticket'
         ,  ALCG_DESCRIPTION =  N'Error al imprimir Ticket'
    WHERE   ALCG_ALARM_CODE = 5242929
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Attendant card Rejected'
    WHERE   ALCG_ALARM_CODE = 5242885
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Tarjeta de operador Rechazada'
    WHERE   ALCG_ALARM_CODE = 5242885
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Attendant card Inserted'
    WHERE   ALCG_ALARM_CODE = 5242886
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Tarjeta de operador Insertada'
    WHERE   ALCG_ALARM_CODE = 5242886
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Attendant card Captured'
    WHERE   ALCG_ALARM_CODE = 5242887
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Tarjeta de operador Capturada'
    WHERE   ALCG_ALARM_CODE = 5242887
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Attendant card Released'
    WHERE   ALCG_ALARM_CODE = 5242888
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Tarjeta de operador Expulsada'
    WHERE   ALCG_ALARM_CODE = 5242888
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Bill Rejected'
    WHERE   ALCG_ALARM_CODE = 5242899
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Billete Rechazado'
    WHERE   ALCG_ALARM_CODE = 5242899
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Bill Inserted'
    WHERE   ALCG_ALARM_CODE = 5242900
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Billete Insertado'
    WHERE   ALCG_ALARM_CODE = 5242900
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Bill Stacked'
    WHERE   ALCG_ALARM_CODE = 5242901
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Billete Apilado'
    WHERE   ALCG_ALARM_CODE = 5242901
      AND   ALCG_LANGUAGE_ID = 10 

  UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Rejected'
    WHERE   ALCG_ALARM_CODE = 5242902
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Rechazado'
    WHERE   ALCG_ALARM_CODE = 5242902
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Inserted'
    WHERE   ALCG_ALARM_CODE = 5242903
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Insertado'
    WHERE   ALCG_ALARM_CODE = 5242903
      AND   ALCG_LANGUAGE_ID = 10 

   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Stacked'
    WHERE   ALCG_ALARM_CODE = 5242904
      AND   ALCG_LANGUAGE_ID = 9    
      
   UPDATE   ALARM_CATALOG 
      SET   ALCG_NAME =  N'Ticket Apilado'
    WHERE   ALCG_ALARM_CODE = 5242904
      AND   ALCG_LANGUAGE_ID = 10 
      
END
GO

IF NOT EXISTS (SELECT 1 FROM ALARM_CATEGORIES WHERE alc_category_id = 63)
BEGIN
  
	 -- ALARM CATEGORIES                    
	INSERT INTO ALARM_CATEGORIES VALUES(63, 9,  5, 0, N'Operation'  , '', 1)
	INSERT INTO ALARM_CATEGORIES VALUES(63, 10, 5, 0, N'Operaci�n', '', 1)

	-- ALARM CATALOG
	INSERT INTO ALARM_CATALOG VALUES(5242931, 9, 0, N'Tickets', N'Tickets', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242931, 10, 0, N'Tickets', N'Tickets', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242932, 9, 0, N'Payment', N'Payment', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242932, 10, 0, N'Pago', N'Pago', 1)

	INSERT INTO ALARM_CATALOG VALUES(5242933, 9, 0, N'Transaction', N'Transaction', 1)
	INSERT INTO ALARM_CATALOG VALUES(5242933, 10, 0, N'Transacci�n', N'Transacci�n', 1)
	
	-- ALARM CATALOG PER CATEGORY
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242931, 63, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242932, 63, 0, GETDATE())
	INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242933, 63, 0, GETDATE())
	
END
GO

IF EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'gaming_tables' AND COLUMN_NAME = 'gt_linked_cashier_id') 
BEGIN

	UPDATE gaming_tables
	SET gt_linked_cashier_id = gt_cashier_id
	WHERE gt_has_integrated_cashier = 1

END
GO

DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE IN (SELECT ALCC_ALARM_CODE 
                                                      FROM ALARM_CATALOG_PER_CATEGORY 
                                                     WHERE ALCC_CATEGORY IN (SELECT ALC_CATEGORY_ID 
                                                                               FROM ALARM_CATEGORIES 
                                                                              WHERE ALC_ALARM_GROUP_ID = 6))
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_CATEGORY IN (SELECT ALC_CATEGORY_ID 
                                                                 FROM ALARM_CATEGORIES 
                                                                WHERE ALC_ALARM_GROUP_ID = 6)
DELETE FROM ALARM_CATEGORIES WHERE ALC_ALARM_GROUP_ID = 6
DELETE FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 6
GO

-- ALARM GROUPS
IF NOT EXISTS (SELECT 1 FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 6)
BEGIN
  INSERT INTO ALARM_GROUPS VALUES(6, 9,  0, N'Gambling tables', '', 1)            
  INSERT INTO ALARM_GROUPS VALUES(6, 10, 0, N'Mesas de juego',  '', 1)
END
GO

-- ALARM CATEGORIES                    
IF NOT EXISTS (SELECT 1 FROM ALARM_CATEGORIES WHERE ALC_ALARM_GROUP_ID = 6 AND ALC_CATEGORY_ID = 62)
BEGIN
  INSERT INTO ALARM_CATEGORIES VALUES(62, 9,  6, 0, N'Operations',  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(62, 10, 6, 0, N'Operaciones', '', 1)
END
GO

-- ALARM CATALOG 262400
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262400)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262400,  9, 0, N'Bets by customer', N'Bets by customer', 1)
  INSERT ALARM_CATALOG VALUES ( 262400, 10, 0,  N'Apuestas por cliente', N'Apuestas por cliente', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262400
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262400 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262400, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262401
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262401)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262401,  9, 0, N'Gambling table with session loss', N'Gambling table with session loss', 1)
  INSERT ALARM_CATALOG VALUES ( 262401, 10, 0, N'Mesa con p�rdida por sesi�n', N'Mesa con p�rdida por sesi�n', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262401
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262401 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262401, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262402
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262402)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262402,  9, 0, N'Investments made with credit / debit card', N'Investments made with credit / debit card', 1)
  INSERT ALARM_CATALOG VALUES ( 262402, 10, 0, N'Inversiones realizadas con tarjeta cr�dito/d�bito', N'Inversiones realizadas con tarjeta cr�dito/d�bito', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262402
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262402 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262402, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262403
IF EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403)
BEGIN
  DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403
END
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262403,  9, 0, N'Player sells chips without purchasing them', N'Customer gets paid without a Drop (purchase)', 1)
  INSERT ALARM_CATALOG VALUES ( 262403, 10, 0, N'Cliente vende fichas sin comprarlas', N'Cliente realiza cobro sin contar con Drop (compra)', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262403
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262403 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262403, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262404
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262404)
BEGIN
  INSERT ALARM_CATALOG VALUES (262404,  9, 0, N'Variation of customer bet', N'Variation of customer bet per session in a given period', 1)
  INSERT ALARM_CATALOG VALUES (262404, 10, 0, N'Variaci�n de apuesta cliente', N'Variaci�n de apuesta cliente por sesi�n en determinado per�odo', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262404
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262404 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262404, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262405
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262405)
BEGIN
  INSERT ALARM_CATALOG VALUES (262405,  9, 0, N'Purchase time limit', N'Payment done by player under minutes ', 1)
  INSERT ALARM_CATALOG VALUES (262405, 10, 0, N'L�mite de tiempo para compras', N'Jugador: realiza cobro en un tiempo menor a minutos', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262405
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262405 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262405, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262407
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262407)
BEGIN
  INSERT ALARM_CATALOG VALUES (262407,  9, 0, N'Amount purchased in one operation', N'Purchase from : (1 single exhibition)', 1)
  INSERT ALARM_CATALOG VALUES (262407, 10, 0, N'Importe comprado en una operaci�n', N'Compras de : (1 sola exhibici�n)', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262407
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262407 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262407, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262408
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262408)
BEGIN
  INSERT ALARM_CATALOG VALUES (262408,  9, 0, N'Amount paid in one operation', N'Payment from : (1 single exhibition)', 1)
  INSERT ALARM_CATALOG VALUES (262408, 10, 0, N'Importe pagado en una operaci�n', N'Pagos de : (1 sola exhibici�n) ', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262408
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262408 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262408, 62, 0, GETDATE() )
END
GO

IF EXISTS(SELECT * FROM report_tool_config WHERE rtc_store_name = 'MontlyReportPhilippines')
BEGIN

	DECLARE @header XML	
	DECLARE @footer XML	
	DECLARE @filter XML	
		  
	SET @header = '
	<div style="text-align:center">
		<h2>Monthly Report</h2>
		<h2>[customerName] - [site]</h2>
		<h2>[pMonth] [pYear]</h2>
		<br />
	</div>
	'

	SET @footer = '
	<div>
		<br />
		<br />
		<table style="width:100%;font-family:Lucida Sans Unicode;font-size:7px;">
		<tr>
			<td style="width:45%;">Prepared by:</td>
			<td style="width:10%;" />
			<td style="text-align:center;width:45%;">Verified by:</td>
		</tr>
		<tr>
			<td />
			<td />
			<td />
		</tr>
		<tr>
			<td style="font-size:6px;">[userName]</td>
			<td />
			<td />
		</tr>
		<tr>
			<td style="border-top:1px solid #000000; text-align:center">Officer In Charge</td>
			<td />
			<td style="border-top:1px solid #000000" />
		</tr>
		<tr>
			<td>[customerName] - [site]</td>
			<td />
			<td style="text-align:center">(Signature over Printed Name)</td>
		</tr>
		</table>
	</div>
	'

	SET @filter = '
	<ReportToolDesignFilterDTO>
		<Variables>
		<VariableDTO>
			<Name>[site]</Name>
			<Type>GeneralParam</Type>
			<MetaDataOne>Site</MetaDataOne>
			<MetaDataTwo>Name</MetaDataTwo>
		</VariableDTO>
		<VariableDTO>
			<Name>[customerName]</Name>
			<Type>GeneralParam</Type>
			<MetaDataOne>Cashier</MetaDataOne>
			<MetaDataTwo>Split.A.CompanyName</MetaDataTwo>
		</VariableDTO>
		<VariableDTO>
			<Name>[userName]</Name>
			<Type>CurrentUser</Type>
			<MetaDataOne>UserName</MetaDataOne>
			<MetaDataTwo />
		</VariableDTO>
		<VariableDTO>
			<Name>[pMonth]</Name>
			<Type>Parameter</Type>
			<MetaDataOne>@pMonthFrom</MetaDataOne>
			<MetaDataTwo>MMMM</MetaDataTwo>
		</VariableDTO>
		<VariableDTO>
			<Name>[pYear]</Name>
			<Type>Parameter</Type>
			<MetaDataOne>@pMonthFrom</MetaDataOne>
			<MetaDataTwo>yyyy</MetaDataTwo>
		</VariableDTO>
		</Variables>
		<ShowPrint>true</ShowPrint>
		<FilterType>CustomFilters</FilterType>
		<FilterText>
			<LanguageResources>
				<NLS09>
					<Label>Months</Label>
				</NLS09>
				<NLS10>
					<Label>Meses</Label>
				</NLS10>
			</LanguageResources>
		</FilterText>
		<Filters>
		<ReportToolDesignFilter>
			<TypeControl>uc_date_picker</TypeControl>
			<TextControl>
			<LanguageResources>
				<NLS09>
				<Label>From</Label>
				</NLS09>
				<NLS10>
				<Label>Desde</Label>
				</NLS10>
			</LanguageResources>
			</TextControl>
			<ParameterStoredProcedure>
			<Name>@pMonthFrom</Name>
			<Type>DateTime</Type>
			</ParameterStoredProcedure>
			<Methods>
			<Method>
				<Name>SetFormat</Name>
				<Parameters>14,0</Parameters>
			</Method>
			</Methods>
			<Propertys>
			<Property>
				<Name>ShowUpDown</Name>
				<Value>True</Value>
			</Property>
			</Propertys>
			<Value>TodayOpening()-30</Value>
		</ReportToolDesignFilter>
		<ReportToolDesignFilter>
			<TypeControl>uc_date_picker</TypeControl>
			<TextControl>
			<LanguageResources>
				<NLS09>
				<Label>To</Label>
				</NLS09>
				<NLS10>
				<Label>Hasta</Label>
				</NLS10>
			</LanguageResources>
			</TextControl>
			<ParameterStoredProcedure>
			<Name>@pMonthTo</Name>
			<Type>DateTime</Type>
			</ParameterStoredProcedure>
			<Methods>
			<Method>
				<Name>SetFormat</Name>
				<Parameters>14,0</Parameters>
			</Method>
			</Methods>
			<Propertys>
			<Property>
				<Name>ShowUpDown</Name>
				<Value>True</Value>
			</Property>
			</Propertys>
			<Value>TodayOpening()</Value>
		</ReportToolDesignFilter>
		</Filters>
	</ReportToolDesignFilterDTO>
	'

   
	UPDATE report_tool_config
		SET
		 rtc_html_header   = @header   
		,rtc_html_footer   = @footer 
		,rtc_design_filter = @filter 
	WHERE rtc_store_name = 'MontlyReportPhilippines'
END
GO



/**** STORED PROCEDURES *****/
  -- CREATE PROCEURE EISTPLAYSESSIONS
  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ExistsPlaySession') AND type in ('P', 'PC'))
DROP PROCEDURE ExistsPlaySession
GO
/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR Exists Play Session

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    08-AUG-2016			RGR				Initial version

         
Parameters:
   -- @pAccountID:					AccountID.
   -- @pTerminalID :				TerminalID.
*/ 

CREATE PROCEDURE ExistsPlaySession
   @pAccountID   BIGINT,
   @pTerminalID  INTEGER
AS
BEGIN 
 SELECT   
   CAST(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END AS BIT) AS IsPlaySession
FROM
(SELECT   PS.PS_ACCOUNT_ID
        , PS.DURATION
        , PS.PLAYED_COUNT
        , PS.PLAYED_AMOUNT
        , CASE WHEN  PS.PLAYED_COUNT = 0 THEN NULL ELSE ROUND((PS.PLAYED_AMOUNT / PS.PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
   FROM
        ( SELECT   PS_ACCOUNT_ID
                 , SUM(DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE()))) AS DURATION
                 , SUM(PS_PLAYED_COUNT) AS PLAYED_COUNT
                 , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT
            FROM   PLAY_SESSIONS
           WHERE   PS_STARTED >= dbo.TodayOpening(0) 
        GROUP BY   PS_ACCOUNT_ID) AS PS) AS WD
        , TERMINALS
INNER JOIN PROVIDERS ON TE_PROV_ID = PV_ID
INNER JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
INNER JOIN AREAS ON BK_AREA_ID = AR_AREA_ID
INNER JOIN PLAY_SESSIONS PS1 ON TE_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
INNER JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID
LEFT JOIN  TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID 
WHERE   TE_CURRENT_PLAY_SESSION_ID IS NOT NULL
  AND   WD.PS_ACCOUNT_ID = AC_ACCOUNT_ID AND TE_TERMINAL_ID = @pTerminalID AND AC_ACCOUNT_ID = @pAccountID 
END
GO
GRANT EXECUTE ON ExistsPlaySession TO wggui WITH GRANT OPTION 



/******* TRIGGERS *******/
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_Alarms_Buffer_AGG]'))
BEGIN
	DROP TRIGGER Trigger_Alarms_Buffer_AGG
END
GO

CREATE TRIGGER Trigger_Alarms_Buffer_AGG on ALARMS
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @_alarm_category as int
	Declare @_date as datetime
	
	set @_alarm_category = 1
	set @_date = GETDATE()
	
	IF EXISTS (SELECT gp_key_value FROM   general_params WHERE   gp_group_key = 'AGG' AND   gp_subject_key = 'Buffering.Enabled' AND gp_key_value = '1')
	BEGIN	
	
		INSERT INTO   BUFFER_ALARMS
					( ba_id, ba_timestamp )
			 SELECT   al_alarm_id, @_date
			   FROM   INSERTED
			   WHERE  al_alarm_code IN (    SELECT    Distinct(ALCC_ALARM_CODE)
     										  FROM    alarm_categories  
										INNER JOIN    ALARM_CATALOG_PER_CATEGORY
        										on    ALCC_CATEGORY = ALC_CATEGORY_ID
											   and    alc_alarm_group_id = @_alarm_category  );
    END    
    SET NOCOUNT OFF;
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_TSM_Buffer_AGG]'))
BEGIN
	DROP TRIGGER Trigger_TSM_Buffer_AGG
END
GO

CREATE TRIGGER Trigger_TSM_Buffer_AGG on terminal_sas_meters
    AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @_date as datetime	
	set @_date = GETDATE()
	
	IF EXISTS (SELECT GP_KEY_VALUE FROM   GENERAL_PARAMS WHERE  GP_GROUP_KEY = 'AGG' AND   GP_SUBJECT_KEY = 'Buffering.Enabled' AND GP_KEY_VALUE = '1')
	BEGIN	

		UPDATE   BUFFER_TERMINAL_SAS_METERS
		   SET   BTSM_TIMESTAMP = @_date
		  FROM   BUFFER_TERMINAL_SAS_METERS B
	INNER JOIN   INSERTED 
			ON   B.BTSM_TERMINAL_ID   =   INSERTED.TSM_TERMINAL_ID 
		   AND   B.BTSM_METER_CODE    =   INSERTED.TSM_METER_CODE 
		   AND   B.BTSM_GAME_ID       =   INSERTED.TSM_GAME_ID 
		   AND   B.BTSM_DENOMINATION  =   INSERTED.TSM_DENOMINATION

		INSERT INTO   BUFFER_TERMINAL_SAS_METERS
					( BTSM_TERMINAL_ID
					, BTSM_METER_CODE
					, BTSM_GAME_ID
					, BTSM_DENOMINATION
					, BTSM_TIMESTAMP)
			 SELECT   TSM_TERMINAL_ID
					, TSM_METER_CODE
					, TSM_GAME_ID
					, TSM_DENOMINATION
					, @_date
			   FROM   INSERTED
	LEFT OUTER JOIN   BUFFER_TERMINAL_SAS_METERS B 
				 ON   B.BTSM_TERMINAL_ID  =  INSERTED.TSM_TERMINAL_ID 
				AND   B.BTSM_METER_CODE   =  INSERTED.TSM_METER_CODE 
				AND   B.BTSM_GAME_ID      =  INSERTED.TSM_GAME_ID 
				AND   B.BTSM_DENOMINATION =  INSERTED.TSM_DENOMINATION
			  WHERE   B.BTSM_TERMINAL_ID   IS   NULL
				AND   B.BTSM_METER_CODE    IS   NULL
				AND   B.BTSM_GAME_ID       IS   NULL
				AND   B.BTSM_DENOMINATION  IS   NULL			   
	END    
	
	SET NOCOUNT OFF;
END
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_TSMH_Buffer_AGG]'))
DROP TRIGGER [dbo].[Trigger_TSMH_Buffer_AGG]
GO

CREATE TRIGGER [dbo].[Trigger_TSMH_Buffer_AGG] on [dbo].[terminal_sas_meters_history]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @_date as datetime
	set @_date = GETDATE()
	
	IF EXISTS (SELECT gp_key_value FROM   general_params WHERE   gp_group_key = 'AGG' AND   gp_subject_key = 'Buffering.Enabled' AND gp_key_value = '1')
	BEGIN	
		
		INSERT INTO   BUFFER_TERMINAL_SAS_METERS_HISTORY
					( BTSMH_TERMINAL_ID
					, BTSMH_METER_CODE
					, BTSMH_GAME_ID
					, BTSMH_DENOMINATION
					, BTSMH_TYPE
					, BTSMH_DATETIME
					, BTSMH_TIMESTAMP )
			 SELECT   TSMH_TERMINAL_ID
					, TSMH_METER_CODE
					, TSMH_GAME_ID
					, TSMH_DENOMINATION
					, TSMH_TYPE
					, TSMH_DATETIME
					, @_date
			   FROM   INSERTED;
    END    
    SET NOCOUNT OFF;
    
END
GO