/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 316;

SET @New_ReleaseId = 317;
SET @New_ScriptName = N'UpdateTo_18.317.039.sql';
SET @New_Description = N'Update SP CustomersPlaying;';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomersPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CustomersPlaying]
GO

CREATE PROCEDURE [dbo].[CustomersPlaying]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- TERMINALES
SELECT  TE_TERMINAL_ID
			 , TE_PROV_ID
			 , ISNULL(PV_NAME,'') AS PV_NAME
			 , ISNULL(TE_NAME,'') AS TE_NAME
			 , ISNULL(TE_FLOOR_ID,'') AS TE_FLOOR_ID
			 , ISNULL(BK_AREA_ID, -1) AS BK_AREA_ID --'AREA'
			 , ISNULL(AR_NAME,'') AS AR_NAME
			 , ISNULL(TE_BANK_ID, -1) AS TE_BANK_ID --'ISLA'
			 , ISNULL(BK_NAME,'') AS BK_NAME
			 , ISNULL(TE_POSITION, -1) AS TE_POSITION --'POSICION'
			   -- JUGADORES
			 , AC_ACCOUNT_ID
			 , ISNULL(AC_HOLDER_NAME,'') AS AC_HOLDER_NAME
			 , ISNULL(AC_TRACK_DATA,'') AS AC_TRACK_DATA
			 , AC_HOLDER_LEVEL AS AC_HOLDER_LEVEL
       , (SELECT   GP_KEY_VALUE     
            FROM   GENERAL_PARAMS   
           WHERE   GP_GROUP_KEY   = 'PlayerTracking'  
             AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' 
            ) AS AC_LEVEL             		
       , AC_HOLDER_IS_VIP	 
			   -- SESION DE JUEGO
			 , PS1.PS_PLAY_SESSION_ID
			 , PS1.PS_STARTED
			 , DATEDIFF(SECOND, PS1.PS_STARTED, ISNULL(PS1.PS_FINISHED, GETDATE())) AS PS1PS_DURATION
			 , PS1.PS_PLAYED_COUNT
			 , PS1.PS_PLAYED_AMOUNT
			 , CASE WHEN  PS1.PS_PLAYED_COUNT = 0 THEN ''
			        ELSE ROUND((PS1.PS_PLAYED_AMOUNT / PS1.PS_PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
			   -- CLIENTE JORNADA
			 , WD.DURATION AS WD_DURATION
			 , WD.PLAYED_COUNT AS WD_PLAYED_COUNT
			 , WD.PLAYED_AMOUNT AS WD_PLAYED_AMOUNT
			 , WD.PS_AVERAGE_BET AS WD_AVERAGE_BET
			 , AC_TYPE
             , AC_CREATED
             , AC_HOLDER_BIRTH_DATE            
             , AC_HOLDER_WEDDING_DATE
             , AC_USER_TYPE -- GMV 27-10-2015 52755 - HighRollers
             , PS1.PS_FINISHED -- GMV 27-10-2015 52755 - HighRollers
             , ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS -- GMV 27-10-2015 52755 - HighRollers
             , WD.TE_ISO_CODE
FROM
(SELECT   PS.PS_ACCOUNT_ID
        , PS.DURATION
        , PS.PLAYED_COUNT
        , PS.PLAYED_AMOUNT
        , PS.TE_ISO_CODE
        , CASE WHEN  PS.PLAYED_COUNT = 0 THEN NULL
               ELSE ROUND((PS.PLAYED_AMOUNT / PS.PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
   FROM
  (SELECT   PS_ACCOUNT_ID
          , SUM(DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE()))) AS DURATION
          , SUM(PS_PLAYED_COUNT) AS PLAYED_COUNT
          , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT
          , TE_ISO_CODE 
     FROM PLAY_SESSIONS
     INNER JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
    WHERE PS_STARTED >= dbo.TodayOpening(0) 
    GROUP BY PS_ACCOUNT_ID,TE_ISO_CODE) AS PS ) AS WD
, 
TERMINALS
INNER JOIN PROVIDERS ON TE_PROV_ID = PV_ID
INNER JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
INNER JOIN AREAS ON BK_AREA_ID = AR_AREA_ID
INNER JOIN PLAY_SESSIONS PS1 ON TE_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
INNER JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID
LEFT JOIN  TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID 
WHERE   TE_CURRENT_PLAY_SESSION_ID IS NOT NULL
  AND   WD.PS_ACCOUNT_ID = AC_ACCOUNT_ID
  AND   WD.TE_ISO_CODE = TERMINALS.TE_ISO_CODE


END

GO

GRANT EXECUTE ON [dbo].[CustomersPlaying] TO [wggui] WITH GRANT OPTION

GO