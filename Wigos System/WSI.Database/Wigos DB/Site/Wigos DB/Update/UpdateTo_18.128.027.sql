/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 127;

SET @New_ReleaseId = 128;
SET @New_ScriptName = N'UpdateTo_18.128.027.sql';
SET @New_Description = N'Update Stored procedures ReportDrawTickets and elp01_sp_TransactionInterface';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/

  /****** Object:  StoredProcedure [dbo].[elp01_sp_TransactionInterface]    Script Date: 05/14/2013 11:51:55 ******/
  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: elp01_sp_TransactionInterface.sql
  --
  --   DESCRIPTION: elp01_sp_TransactionInterface
  --
  --        AUTHOR: Jordi Vera
  --
  -- CREATION DATE: 14-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 14-MAY-2013 JVV     First release.  
  -------------------------------------------------------------------------------- 
    
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[elp01_sp_TransactionInterface]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[elp01_sp_TransactionInterface]
GO
 CREATE PROCEDURE [dbo].[elp01_sp_TransactionInterface]
        @pElpPlaySessionId  BIGINT
       ,@pElpAccountId			BIGINT
       ,@pElpTerminalId			INT    
       ,@pElpStarted				DATETIME  
       ,@pElpFinished				DATETIME  
       ,@pElpPlayed					MONEY  
       ,@pElpWon						MONEY 
       ,@pElpPlayedCount		INT 
       ,@pElpCashin					MONEY
       ,@pElpCashout				MONEY
       ,@pKindof_ticket			INT
	     ,@pGameCode					INT

  AS
BEGIN          
		DECLARE @ticketNumber		AS BIGINT
		DECLARE @sequenceNumber AS BIGINT		
		DECLARE @teSerialnumber AS NVARCHAR(50)
		DECLARE @teFloorId			AS NVARCHAR(20)
		DECLARE @teBankId				AS INT
		DECLARE @teProvId				AS INT
		DECLARE @bkAreaId				AS INT
		DECLARE @SiteId 				AS INT
		DECLARE @AcUserType			AS INT
    		 		
		 		
  		/********** EXTERNAL DATA **********/
  	 --General params
  	 SELECT  @SiteId = CAST(GP_KEY_VALUE AS INT) 
		  FROM   GENERAL_PARAMS 
		 WHERE   GP_GROUP_KEY   = 'Site' 
		   AND   GP_SUBJECT_KEY = 'Identifier'
		   AND   ISNUMERIC(GP_KEY_VALUE) = 1	
		--Accounts 
		SELECT	 @AcUserType = AC_USER_TYPE
		 FROM		 ACCOUNTS 
		WHERE		 AC_ACCOUNT_ID = @pElpAccountId;			
		--Calculate TicketNumber / SequenceNumber
		SET @ticketNumber = @pElpPlaySessionId;	 		
		SET @sequenceNumber = @ticketNumber;		
		--Terminals
		SELECT @teSerialnumber = TE_SERIAL_NUMBER
					,@teFloorId = TE_FLOOR_ID
					,@teBankId = TE_BANK_ID
					,@teProvId = TE_PROV_ID
			FROM TERMINALS
		WHERE  TE_TERMINAL_ID = @pElpTerminalId;
		--Banks
		SELECT @bkAreaId = BK_AREA_ID 
		  FROM BANKS
		WHERE BK_BANK_ID = @teBankId;
		
				 
		/********** EXTERNAL DATA **********/
		
        IF (@teProvId IS NOT NULL AND ISNULL(@AcUserType,0) = 1) 
        BEGIN         
          INSERT INTO elp01_play_sessions
           ([eps_ticket_number]
           ,[eps_account_id]
           ,[eps_slot_serial_number]
           ,[eps_slot_house_number]
           ,[eps_venue_code]
           ,[eps_area_code]
           ,[eps_bank_code]
           ,[eps_vendor_code]
           ,[eps_game_code]
           ,[eps_start_time]
           ,[eps_end_time]
           ,[eps_bet_amount]
           ,[eps_paid_amount]
           ,[eps_games_played]
           ,[eps_initial_amount]
           ,[eps_aditional_amount]
           ,[eps_final_amount]
           ,[eps_bet_comb_code]
           ,[eps_kindof_ticket]
           ,[eps_sequence_number]
           ,[eps_cupon_number]
           ,[eps_date_updated]
           ,[eps_date_inserted])
     VALUES
           ( @ticketNumber
           , @pElpAccountId  
           , @teSerialnumber
           , ISNULL(@teFloorId,' ')
           , @SiteId
           , @bkAreaId
           , @teBankId
           , @teProvId		
           , @pGameCode	
           , @pElpStarted
           , @pElpFinished	
           , @pElpPlayed
           , @pElpWon
           , @pElpPlayedCount
           , @pElpCashin
           , 0 --AditionalAmount
           , @pElpCashout
           , NULL --BetComboCode 
           , @pKindof_ticket
           , @sequenceNumber
           , NULL --CuponNumber
           , GETDATE()
           , GETDATE()
            )                 
            
       END      
 END --elp01_sp_TransactionInterface
GO
 
ALTER PROCEDURE [dbo].[ReportDrawTickets]
  @pDrawId      BIGINT        = NULL,
  @pDrawName    NVARCHAR(50)  = NULL,
  @pSqlAccount  NVARCHAR(MAX) = NULL,
  @pTicketFrom  DATETIME      = NULL,
  @pTicketTo    DATETIME      = NULL
AS
BEGIN
  SET NOCOUNT ON;

  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_HOLDER_LEVEL INT)
  IF @pSqlAccount IS NOT NULL
  BEGIN
    INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_HOLDER_LEVEL FROM ACCOUNTS ' + @pSqlAccount)
    IF @@ROWCOUNT > 500
      ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
  END

  IF @pDrawId IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_DRAW_ID  = @pDrawId
       AND   DT_CREATED >= @pTicketFrom
       AND   DT_CREATED  < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pDrawName IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by draw name
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_ID))
     INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
     INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DR_NAME    LIKE '%' + @pDrawName + '%' ESCAPE '\'
       AND   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED    < @pTicketTo
       AND   ( (@pSqlAccount     IS NULL) OR (DT_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pSqlAccount IS NOT NULL 
  BEGIN
    IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
    IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
    --
    -- Filtering by account
    --
    SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
           , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
           , COUNT(1) NUM_TICKETS
           , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
           , MIN(DT_CREATED) FIRST_TICKET
           , MAX(DT_CREATED) LAST_TICKET
      FROM   DRAW_TICKETS WITH(INDEX(IX_DRAW_TICKET_ACCOUNT_CREATED))
     INNER   JOIN DRAWS          ON DR_ID         = DT_DRAW_ID
     INNER   JOIN #ACCOUNTS_TEMP ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
     WHERE   DT_CREATED   >= @pTicketFrom
       AND   DT_CREATED   <  @pTicketTo
     GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
     ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

    DROP TABLE #ACCOUNTS_TEMP

    RETURN
  END

  IF @pTicketFrom IS NULL AND @pTicketTo IS NULL
    RAISERROR ('ReportDrawTickets - DateRange not defined!', 20, 0) WITH LOG

    
  IF @pTicketFrom IS NULL SET @pTicketFrom = '2007-01-01T00:00:00'
  IF @pTicketTo   IS NULL SET @pTicketTo   = GETDATE ()
  --
  -- For a period
  --
  SELECT   AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME
         , DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
         , COUNT(1) NUM_TICKETS
         , SUM(DT_LAST_NUMBER - DT_FIRST_NUMBER + 1) NUM_NUMBERS
         , MIN(DT_CREATED) FIRST_TICKET
         , MAX(DT_CREATED) LAST_TICKET
    FROM   DRAW_TICKETS WITH (INDEX(IX_DRAW_TICKET_CREATED))
   INNER   JOIN DRAWS    ON DR_ID         = DT_DRAW_ID
   INNER   JOIN ACCOUNTS ON AC_ACCOUNT_ID = DT_ACCOUNT_ID
   WHERE   DT_CREATED   >= @pTicketFrom
     AND   DT_CREATED   <  @pTicketTo
   GROUP   BY AC_HOLDER_LEVEL, AC_ACCOUNT_ID, AC_HOLDER_NAME, DT_DRAW_ID, DR_NAME, DR_CREDIT_TYPE
   ORDER   BY AC_HOLDER_LEVEL DESC, AC_ACCOUNT_ID ASC, DR_NAME ASC

  DROP TABLE #ACCOUNTS_TEMP

END -- ReportDrawTickets
GO
 

/****** TRIGGERS ******/

/****** RECORDS ******/

