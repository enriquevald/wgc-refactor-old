/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 282;

SET @New_ReleaseId = 283;
SET @New_ScriptName = N'UpdateTo_18.283.038.sql';
SET @New_Description = N'Trigger for chile/peru;Update GP: StartTrading';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

-- METERS INTO GROUP (Played & Won)

--Meter Code = 0x1D (29)
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 29 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 29)
 
--Meter Code = 0x20 (32)
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 32 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 32)

--Meter Code = 0x23 (35)
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 35 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 35)

--Meter Code = 0x25 (37)
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 37 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 37)

--Meter Code = 0xA2 (162) -In-house restricted transfers to gaming machine (cents)-
IF NOT EXISTS(SELECT  smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 162 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 162)

--Meter Code = 0xA3 (163) -In-house transfers to gaming machine that included restricted amounts (quantity)-
IF NOT EXISTS(SELECT  smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 163 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 163)

--Meter Code = 0xBA (186) -In-house restricted transfers to host (cents)-
IF NOT EXISTS(SELECT  smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 186 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 186)

--Meter Code = 0xBB (187) -In-house transfers to host that included restricted amounts (quantity)-
IF NOT EXISTS(SELECT  smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_meter_code = 187 AND smcg_group_id = 10004)
  INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10004, 187)

GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='OperationsSchedule' AND GP_SUBJECT_KEY = 'DisableMachine.StartTrading')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'DisableMachine.StartTrading', '15')
ELSE
   UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '15' WHERE GP_GROUP_KEY = 'OperationsSchedule' AND GP_SUBJECT_KEY = 'DisableMachine.StartTrading'
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='OperationsSchedule' AND GP_SUBJECT_KEY = 'DisableMachine.EndTrading')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'DisableMachine.EndTrading', '-15')
ELSE
   UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '-15' WHERE GP_GROUP_KEY ='OperationsSchedule' AND GP_SUBJECT_KEY = 'DisableMachine.EndTrading'
GO

UPDATE  TERMINAL_SAS_METERS 
   SET  TSM_METER_MAX_VALUE = 100000000
 WHERE  TSM_METER_MAX_VALUE = 1000000
   AND  TSM_METER_CODE IN (0, 1, 2, 4096)
GO

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + 
            '  AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 20
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsValidRegistrationCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsValidRegistrationCode]
GO

CREATE FUNCTION IsValidRegistrationCode 
(
	-- Add the parameters for the function here
	@pTerminalId       INT
  , @pStatus           INT
  , @pRegistrationCode NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	
	DECLARE @_value AS INT;
	DECLARE @_count AS INT;
	
	IF ( @pStatus <> 0 )					  RETURN 1; -- Check only 'Active Terminals'
	IF ( @pRegistrationCode IS NULL )		  RETURN 1; -- Allow NULL
	IF ( ISNUMERIC (@pRegistrationCode) = 0 ) RETURN 0; -- Don't allow NonNumericValues
	
	-- Convert to Integer
	SET @_value = CAST (@pRegistrationCode AS INT);
	
	-- Check range value
	IF (@_value <= 0 OR @_value > 99999999)   RETURN 0; -- Out of range
	
	
	SELECT   @_count = COUNT(1) 
	  FROM   TERMINALS 
	 WHERE   TE_TERMINAL_ID <> @pTerminalId
     AND   TE_STATUS       = 0
     AND   TE_REGISTRATION_CODE = @pRegistrationCode;
	   
	IF ( @_count > 0 ) RETURN 0; -- Duplicated ID   
	
	RETURN 1;

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsValidNUC]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsValidNUC]
GO

CREATE FUNCTION IsValidNUC 
(
	-- Add the parameters for the function here
	@pTerminalId       INT
  , @pStatus           INT
  , @pRegistrationCode NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	
	DECLARE @_count AS INT;
	
	IF ( @pStatus <> 0 )					  RETURN 1; -- Check only 'Active Terminals'
	IF ( @pRegistrationCode IS NULL )		  RETURN 1; -- Allow NULL
	
	SELECT   @_count = COUNT(1) 
	  FROM   TERMINALS 
	 WHERE   TE_TERMINAL_ID <> @pTerminalId
     AND   TE_STATUS            = 0
	   AND   TE_REGISTRATION_CODE = @pRegistrationCode;
	   
	IF ( @_count > 0 ) RETURN 0; -- Duplicated ID   
	
	RETURN 1;

END
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TriggerValidateRegistrationCode]'))
DROP TRIGGER [dbo].[TriggerValidateRegistrationCode]
GO

CREATE TRIGGER dbo.TriggerValidateRegistrationCode 
   ON  dbo.TERMINALS 
   AFTER INSERT,UPDATE
AS 
BEGIN
	DECLARE @_wxp_enabled       as INT
	DECLARE @_coljuegos_enabled as INT
	DECLARE @_terminal_id       as INT
	DECLARE @_status            as INT
	DECLARE @_result            as INT
	DECLARE @_registration_code as NVARCHAR(50)
	DECLARE @_raise             as BIT
	DECLARE @_err_msg           as NVARCHAR(MAX)
	
	SET NOCOUNT ON;
	
	IF ( NOT UPDATE (TE_STATUS) AND NOT UPDATE (TE_REGISTRATION_CODE) ) RETURN;

	SELECT @_wxp_enabled = CAST (GP_KEY_VALUE AS INT)
	  FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY    = 'ExternalProtocol'
	   AND GP_SUBJECT_KEY  = 'Enabled'
	   AND ISNUMERIC (GP_KEY_VALUE) = 1
	   
  SELECT @_coljuegos_enabled = CAST (GP_KEY_VALUE AS INT)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY    = 'Interface.Coljuegos'
     AND GP_SUBJECT_KEY  = 'Enabled'
     AND ISNUMERIC (GP_KEY_VALUE) = 1
	   
	IF ( @_wxp_enabled <> 1 AND @_coljuegos_enabled <> 1) RETURN;   

    SET @_raise = 0;
    
    DECLARE _terminals CURSOR FOR SELECT TE_TERMINAL_ID, TE_STATUS, TE_REGISTRATION_CODE  FROM INSERTED
    OPEN _terminals

	WHILE (1=1)
	BEGIN
    FETCH NEXT FROM _terminals INTO @_terminal_id, @_status, @_registration_code
	  IF ( @@Fetch_Status <> 0 ) BREAK;

      IF @_wxp_enabled = 1
	      set @_result = dbo.IsValidRegistrationCode(@_terminal_id, @_status, @_registration_code)
      ELSE
	      set @_result = dbo.IsValidNUC(@_terminal_id, @_status, @_registration_code)
      
      IF @_result = 0
      BEGIN
		    SET @_raise  = 1;
		    SET @_err_msg = 'Registration Code Not Valid, T:' + CAST(@_terminal_id as NVARCHAR) + ', NewCode:' + @_registration_code;
		    BREAK;
      END
  END
    
	CLOSE _terminals
	DEALLOCATE _terminals

	IF @_raise = 1 
	BEGIN
		RAISERROR(@_err_msg, 25, 0) WITH LOG;
	END
	
    RETURN
END
GO

-- DOMINICAN REPUBLIC STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'DO')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'DO'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '071'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Identidad
END
GO

-- DOMINICAN REPUBLIC REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'DO' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Azua','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bahoruco','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barahona','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dajab�n','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Distrito Nacional','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Duarte','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El Seibo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El�as Pi�a','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Espaillat','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hato Mayor','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hermanas Mirabal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Independencia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Altagracia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Romana','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Vega','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mar�a Trinidad S�nchez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monse�or Nouel','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Cristi','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pedernales','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Peravia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puerto Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Saman�','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Crist�bal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos� de Ocoa','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Pedro de Macor�s','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('S�nchez Ram�rez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago Rodr�guez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santo Domingo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valverde','DO')
END
GO

-- DOMINICAN REPUBLIC OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'DO' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'DO')
END
GO

-- DOMINICAN REPUBLIC IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'DO' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (70,1,100,'Otro','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (71,1,1,'C�dula de Identidad','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (72,1,2,'Pasaporte','DO')
END
GO

-- TRINIDAD AND TOBAGO STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'TT')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Region' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'TT'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '081'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Drivers License
END
GO

--TRINIDAD AND TOBAGO REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'TT' ))
BEGIN
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Port of Spain','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Fernando','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chaguanas Borough','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arima Borough','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Point Fortin','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Couva-Tabaquite-Talparo','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Diego Martin','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Penal-Debe','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Princes Town','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rio Claro-Mayaro','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan-Laventille','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sangre Grande','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Siparia','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tunapuna-Piarco','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tobago','TT')
END
GO

-- TRINIDAD AND TOBAGO OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'TT' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'TT')
END
GO

-- TRINIDAD AND TOBAGO IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'TT' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (80,1,100,'Other','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (81,1,1,'Driver''s license','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (82,1,2,'Passport','TT')
END
GO

-- COSTA RICA STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'CR')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'CR'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '101'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Identidad
END
GO

-- COSTA RICA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'CR' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos�','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Alajuela','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cartago','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Heredia','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lim�n','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guanacaste','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puntarenas','CR')
END
GO

-- COSTA RICA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CR' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'CR')
END
GO

-- COSTA RICA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'CR' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (100,1,100,'Otro','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (101,1,1,'C�dula de Identidad','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (102,1,2,'Pasaporte','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (103,1,3,'Licencia de conducir','CR')
END
GO

-- URUGUAYAN STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'UY')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Departamento' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'UY'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '111'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Identidad
END
GO

-- URUGUAYAN REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'UY' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Artigas','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Canelones','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cerro Largo','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Colonia','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Durazno','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Flores','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Florida','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lavalleja','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Maldonado','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Montevideo','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Paysand�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('R�o Negro','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rivera','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rocha','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salto','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Soriano','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tacuaremb�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Treinta y Tres','UY')

END
GO

-- URUGUAYAN OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'UY' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'UY')
END
GO

-- URUGUAYAN IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'UY' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (110,1,100,'Otro','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (111,1,1,'CI','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (112,1,2,'CPF','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (113,1,3,'DNI','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (114,1,4,'Licencia C�vica','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (115,1,5,'Licencia de Conductor','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (116,1,6,'Pasaporte','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (117,1,7,'RG','UY')
END
GO
