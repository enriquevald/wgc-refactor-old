/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 229;

SET @New_ReleaseId = 230;
SET @New_ScriptName = N'UpdateTo_18.230.037.sql';
SET @New_Description = N'Chages for Alarms categories and gt player tracking';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gt_playertracking_movements]') and name = 'gtpm_seat_id')
BEGIN 
      ALTER TABLE  [dbo].[gt_playertracking_movements]
      ALTER COLUMN [gtpm_seat_id] BigInt NULL;
END
   
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gt_playertracking_movements]') and name = 'gtpm_play_session_id')
BEGIN 
      ALTER TABLE  [dbo].[gt_playertracking_movements]
      ALTER COLUMN [gtpm_play_session_id] BigInt NULL;
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gt_seats]') and name = 'gts_current_play_session_id')
BEGIN
      ALTER TABLE dbo.gt_seats ADD gts_current_play_session_id BIGINT  NULL
END
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='Enabled' AND gp_key_value = '1')
BEGIN
  update   gaming_tables 
     SET   gt_table_speed_selected = 2 -- normal
   where   gt_table_speed_selected IS NULL
     and   gt_num_seats > 0 
END 
GO

/******* INDEXES *******/

/******* RECORDS *******/

INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (40, 10, 3, 0, N'Operaciones', N'', 1)
update alarm_categories set alc_name = 'Control de acceso' where alc_category_id = 23 AND alc_language_id = 10
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (40,  9, 3, 0, N'Operations', N'', 1)
update alarm_categories set alc_name = 'Access control' where alc_category_id = 23 AND alc_language_id = 9
update alarm_categories set alc_alarm_group_id = 3 where alc_category_id = 23 
update alarm_catalog_per_category SET alcc_category = 40 where alcc_alarm_code in (262146, 262147, 262149, 262150, 262151, 393247)
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ProgresiveReports.sql
-- 
--   DESCRIPTION: Progresive reports reports procedures and functions
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-------------------------------------------------------------------------------- 

--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and denomination
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @Nrows AS int                              
 DECLARE @index AS int                              
 DECLARE @q AS table(i int)                       

 SET @pClosingTime = @pClosingTime * -1

 SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @Nrows                           
 BEGIN	                                          
   INSERT INTO @q VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT I INTO #RP_TEMPORARY FROM @q 
 
 SET @Sql = CAST('
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT DATEADD(DAY, I, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATE)) AS ORDER_DATE FROM #RP_TEMPORARY ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) AS PS_FINISHED 
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) 
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                   , SUM(CASE HP_TYPE WHEN 10 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                   , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_TO < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
     ' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '
      AS varchar(max))

EXECUTE (@Sql)

DROP TABLE #RP_TEMPORARY

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @Nrows AS int                              
   DECLARE @index AS int                              
   DECLARE @q AS table(i int)                       
   DECLARE @Columns VARCHAR(MAX)
   DECLARE @ColumnChk varchar(max)

   SET @pClosingTime = @pClosingTime * -1

   SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @Nrows                           
   BEGIN	                                          
     INSERT INTO @q VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT I INTO #TempTable_Days FROM @q 

  SELECT @Columns =  COALESCE(@Columns + '[' + CAST(CUD_DENOMINATION AS NVARCHAR(20)) + '],', '')
    FROM (SELECT DISTINCT CUD_DENOMINATION FROM CURRENCY_DENOMINATIONS 
                 UNION  
          SELECT DISTINCT TM_AMOUNT FROM TERMINAL_MONEY 
           WHERE TM_REPORTED >= @pFromDt
             AND TM_REPORTED <  @pToDt
  ) AS DTM
  ORDER BY CUD_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL or ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
     SELECT   TE_PROVIDER_ID 
            , TE_TERMINAL_ID 
            , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
            , DATEADD(HOUR, ' + CAST(@pClosingTime * -1 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE
            , DATEADD(HOUR, ' + CAST((@pClosingTime * -1) + 24 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE_FIN
            , TICKET_IN_COUNT
            , TI_IN_AMOUNT_RE
            , TI_IN_AMOUNT_NO_RE
            , (TI_IN_AMOUNT_RE + TI_IN_AMOUNT_NO_RE) as TI_IN_AMOUNT
            , TICKET_OUT_COUNT
            , TI_OUT_AMOUNT_RE
            , TI_OUT_AMOUNT_NO_RE
            , (TI_OUT_AMOUNT_RE + TI_OUT_AMOUNT_NO_RE) as TI_OUT_AMOUNT
            , HAND_PAYS.MANUAL
            , HAND_PAYS.CREDIT_CANCEL
            , HAND_PAYS.JACKPOT_DE_SALA
            , HAND_PAYS.PROGRESIVES
            , HAND_PAYS.NO_PROGRESIVES
            , PROVISIONS.PROGRESIVE_PROVISIONS
            , 0 AS GAP_BILL
            , BILLS.*
       FROM   TERMINALS 
   LEFT JOIN ( SELECT   DATEADD(DAY, I, CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATE)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
   LEFT JOIN ( SELECT   TI_LAST_ACTION_TERMINAL_ID
                      , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_LAST_ACTION_DATETIME) AS DATE ) AS TI_LAST_ACTION_DATETIME
                      , COUNT(1) AS TICKET_IN_COUNT
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_IN_AMOUNT_RE
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_IN_AMOUNT_NO_RE
                 FROM   TICKETS 
                WHERE   TI_LAST_ACTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_LAST_ACTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                GROUP   BY TI_LAST_ACTION_TERMINAL_ID, CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_LAST_ACTION_DATETIME) AS DATE )
             ) TICKETS_IN ON TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID AND TI_LAST_ACTION_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                      , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) AS TI_CREATED_DATETIME
                      , COUNT(1) AS TICKET_OUT_COUNT
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
                 FROM   TICKETS 
                WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
                  AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                             --CASHABLE = 0,
                                                             --PROMO_REDEEM = 1,
                                                             --PROMO_NONREDEEM = 2,  // only playable
                                                             --HANDPAY = 3,
                                                             --JACKPOT = 4,
                                                             --OFFLINE = 5
                GROUP   BY TI_CREATED_TERMINAL_ID, CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) 
             ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN (SELECT   HP_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                     , SUM(CASE HP_TYPE WHEN 10 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                     , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                     , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
                FROM   HANDPAYS 
               WHERE   HP_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                 AND   HP_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               GROUP   BY HP_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                     ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                     , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
               FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
          LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                              AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                              AND PGP_HOUR_TO >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                              AND PGP_HOUR_TO <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               GROUP   BY PPT_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                     ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE 
   LEFT JOIN (SELECT *  
                FROM ( SELECT   TM_TERMINAL_ID
                              , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TM_REPORTED) AS DATE) AS TM_REPORTED
                              , TM_AMOUNT
                         FROM   TERMINAL_MONEY
                        where   TM_REPORTED >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                          AND   TM_REPORTED <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                     ) PIV
              PIVOT (  SUM(TM_AMOUNT) FOR TM_AMOUNT IN ('+ @Columns  + ')) AS Child
             )  BILLS ON TE_TERMINAL_ID = TM_TERMINAL_ID AND TM_REPORTED = DIA.ORDER_DATE 
   WHERE (  ' + @ColumnChk + '
         OR TICKET_IN_COUNT IS NOT NULL
         OR TI_IN_AMOUNT_RE IS NOT NULL
         OR TI_IN_AMOUNT_NO_RE IS NOT NULL
         OR TICKET_OUT_COUNT IS NOT NULL
         OR TI_OUT_AMOUNT_RE IS NOT NULL
         OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
         OR HAND_PAYS.MANUAL IS NOT NULL
         OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
         OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
         OR HAND_PAYS.PROGRESIVES IS NOT NULL
         OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
         OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL
          )' + 
         CAST(@pTerminalWhere AS Varchar(max)) +
'  ORDER BY   TE_PROVIDER_ID 
            , TE_TERMINAL_ID 
            , TE_NAME 
            , ISNULL(TE_MULTI_DENOMINATION, ''--'')
            , ORDER_DATE '

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO
