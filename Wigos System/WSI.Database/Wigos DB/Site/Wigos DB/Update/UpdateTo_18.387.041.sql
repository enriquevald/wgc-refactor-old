/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 386;

SET @New_ReleaseId = 387;
SET @New_ScriptName = N'UpdateTo_18.387.041.sql';
SET @New_Description = N'zsp_AccountInfo; GT_Cashier_Movements_And_Summary_By_Session'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/
-- ADD COLUMN ac_provision IN accounts
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'accounts' AND COLUMN_NAME = 'ac_provision')
BEGIN
  ALTER TABLE dbo.accounts ADD ac_provision money NOT NULL CONSTRAINT DF_accounts_ac_provision DEFAULT 0
END
GO 

/******* RECORDS *******/

/******* PROCEDURES *******/

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'zsp_AccountInfo')
DROP PROCEDURE zsp_AccountInfo
GO

create PROCEDURE [dbo].[zsp_AccountInfo]
    @VendorId        varchar(16),
      @TrackData       varchar(50)

WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  declare @AccountNumber              bigint ,
  @Name                       varchar(255) ,
  @FirstSurname               varchar(255) ,
  @SecondSurname              varchar(255) ,
  @Gender                     varchar(255) ,
  @BirthDate                  varchar(255) ,
  @Email                      varchar(255) ,
  @AuxEmail                   varchar(255) ,
  @MobileNumber               varchar(255) ,
  @FixedPhone                 varchar(255) ,
  @ClientTypeCode             varchar(255) ,
  @ClientType                 varchar(255) ,
  @FiscalCode                 varchar(255) ,
  @Nationality                varchar(255) ,
  @Street                     varchar(255) ,
  @HouseNumber                varchar(255) ,
  @Colony                     varchar(255) ,
  @State                      varchar(255) ,
  @City                       varchar(255) ,
  @DocumentType               varchar(255) ,
  @DocumentNumber             varchar(255) ,
  @LevelCode                  varchar(255) ,
  @Level                      varchar(255) ,
  @Points                     varchar(255) ,
  @Document2Type              varchar(255) ,
  @Document2Number            varchar(255) ,
  @Document3Type              varchar(255) ,
  @Document3Number            varchar(255) 

  



  SET @_try       = 0
  SET @_max_tries = 6        -- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Ok'
    SET @error_text   = ''
    BEGIN TRY
      SET @_try = @_try + 1

        DECLARE @_account_id BIGINT

      SET @_account_id = 0
      IF @TrackData IS NOT NULL  
      BEGIN 
        SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
                       )
      END
        IF @_account_id > 0
        begin
            SELECT @status_code  = 0,
            
            @AccountNumber = isnull(ac_account_id ,0),
                     @Name = isnull(ac_holder_name3 ,''),
                     @FirstSurname = isnull(ac_holder_name1 ,''),
                     @SecondSurname = isnull(ac_holder_name2,''),
                     @Gender = isnull(ac_holder_gender ,''),
                     @BirthDate = isnull(ac_holder_birth_date,''),
                     @Email = isnull(ac_holder_email_01 ,''),
                     @AuxEmail = isnull(ac_holder_email_02 ,''),
                     @MobileNumber = isnull(ac_holder_phone_number_01 ,''),
                     @FixedPhone = isnull(ac_holder_phone_number_02 ,''),
                     @ClientTypeCode = 'F' ,
                     @ClientType = 'FISICA',
                     @FiscalCode = isnull(oc_code,'') ,
                     @Nationality = isnull(co_name,''),
                     @Street = isnull(ac_holder_address_01 ,''),
                     @HouseNumber = isnull(ac_holder_ext_num,''),
                     @Colony = isnull(ac_holder_address_02 ,''),
                     @State = isnull(fs_name ,''),
                     @City = isnull(ac_holder_city ,''),
                     @DocumentType = isnull(identification_types.idt_name,'') ,
                     @DocumentNumber = isnull(ac_holder_id,''),
                     @LevelCode = isnull(ac_holder_level,''),
                     @Level = isnull((select top 1 gp_key_value from general_params where gp_group_key='PlayerTracking' and gp_subject_key='Level0'+ cast(ac_holder_level as varchar(2)) +'.Name'),''),
                     @Points = isnull(ac_points,''),
                     @Document2Type = isnull(idt2.idt_name,'') ,
                     @Document2Number = isnull(ac_holder_id2,''),
                     @Document3Type = isnull(idt3.idt_name,'') ,
                     @Document3Number = isnull(ac_holder_id3,'')

              FROM   accounts
              LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
              LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
              LEFT JOIN identification_types on ac_holder_id_type = identification_types.idt_id
              LEFT JOIN identification_types idt2 on ac_holder_id2_type = idt2.idt_id
              LEFT JOIN identification_types idt3 on ac_holder_id3_type = idt3.idt_id
              LEFT JOIN occupations on oc_id = ac_holder_occupation_id
            WHERE   ac_account_id = @_account_id

        end

      SET @_completed = 1

    END TRY
    BEGIN CATCH
    
      ROLLBACK TRANSACTION
      set @AccountNumber =0
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData 
                    +';VenbdorId='      + @VendorId
  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text



  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_GetAccountInfo', @AccountNumber, 0, 0, 0,
                                            0, @status_code, 0, 1,
                                            @input, @output

  COMMIT TRANSACTION
      SELECT 
      isnull(      @status_code      ,1)     AS StatusCode,
      isnull(      @status_text      ,'')      AS StatusText,
      isnull(      @AccountNumber    ,0)      as  AccountNumber     ,
      isnull(      @Name             ,'')      as  Name             , 
      isnull(      @FirstSurname     ,'')      as  FirstSurname     , 
      isnull(      @SecondSurname    ,'')      as  SecondSurname    , 
      isnull(      @Gender           ,'')      as  Gender           , 
      isnull(      @BirthDate        ,'')      as  BirthDate        , 
      isnull(      @Email            ,'')      as  Email            , 
      isnull(      @AuxEmail         ,'')      as  AuxEmail         , 
      isnull(      @MobileNumber     ,'')      as  MobileNumber     , 
      isnull(      @FixedPhone       ,'')      as  FixedPhone       , 
      isnull(      @ClientTypeCode   ,'')      as  ClientTypeCode   , 
      isnull(      @ClientType       ,'')      as  ClientType       , 
      isnull(      @FiscalCode       ,'')      as  FiscalCode       , 
      isnull(      @Nationality      ,'')      as  Nationality      , 
      isnull(      @Street           ,'')      as  Street           , 
      isnull(      @HouseNumber      ,'')      as  HouseNumber      , 
      isnull(      @Colony           ,'')      as  Colony           , 
      isnull(      @State            ,'')      as  [State]            , 
      isnull(      @City             ,'')      as  City             , 
      isnull(      @DocumentType     ,'')      as  DocumentType     , 
      isnull(      @DocumentNumber   ,'')      as  DocumentNumber   ,
      isnull(      @LevelCode        ,'')      as  LevelCode   ,
      isnull(      @Level            ,'')      as  [Level]   ,
      isnull(      @Points           ,'')      as  Points,
      isnull(      @Document2Type    ,'')      as  Document2Type     , 
      isnull(      @Document2Number  ,'')      as  Document2Number   ,
      isnull(      @Document3Type    ,'')      as  Document3Type     , 
      isnull(      @Document3Number  ,'')      as  Document3Number   

      

END -- zsp_SessionStart


go

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_AccountInfo')
       DROP PROCEDURE [S2S_AccountInfo]
GO
CREATE PROCEDURE [dbo].[S2S_AccountInfo]
  @VendorId        varchar(16),
  @Trackdata       varchar(24)
AS
BEGIN
  
  EXECUTE zsp_AccountInfo  @VendorId,@Trackdata
    
END

GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [3GS] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Cashier_Movements_And_Summary_By_Session]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
GO

CREATE PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
(
  @pGtSessionId                  BigInt,

  @pCageChipColor                VARCHAR(3),-- X02
  @pOpeningCash                  INT,  
  @pCageCloseSession             INT,
  @pCageFillerIn                 INT,
  @pCageFillerOut                INT,
  @pChipsSale                    INT,
  @pChipsPurchase                INT,
  @pChipsSaleDevolutionForTito   INT,
  @pChipsSaleWithCashIn          INT,
  @pChipsSaleRegisterTotal       INT,
  @pFillerInClosingStock         INT, 
  @pFillerOutClosingStock        INT,  
  @pReopenCashier                INT,
  
  @pOwnSalesAmount       Money,
  @pExternalSalesAmount  Money,
  @pCollectedAmount      Money,
  @pIsIntegratedCashier  Bit,

  @pDrop                 Money        OUTPUT
)
AS
BEGIN
  DECLARE @Columns           AS VARCHAR(MAX)
  DECLARE @NationalCurrency  AS VARCHAR(5)
           
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
          
      SELECT   CS_SESSION_ID
             , MAX(CM_DATE)                    AS DATE_MOV
             , CM_TYPE                         AS TYPE_MOV
             , ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS ID_MOV_22
             , REPLICATE('0', 50 - LEN(CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)) 
               + REPLICATE('0', 50 - LEN(CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)) AS ID_MOV
             , SUM(CM_SUB_AMOUNT)              AS SUB_AMOUNT
             , SUM(CM_ADD_AMOUNT)              AS ADD_AMOUNT
             , SUM(CM_ADD_AMOUNT) - SUM(CM_SUB_AMOUNT) AS CALCULATED_AMOUNT
             ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency
                WHEN @pCageChipColor THEN ''
                ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE
             ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE
             ,  0 OPENER_MOV
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTSC_INITIAL_CHIPS_AMOUNT
        INTO   #TABLE_MOVEMENTS
        FROM   GAMING_TABLES_SESSIONS
  INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
  INNER JOIN   CASHIER_MOVEMENTS AS CM WITH(INDEX(IX_CM_SESSION_ID)) ON CM_SESSION_ID = CS_SESSION_ID
   LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
  INNER JOIN   GUI_USERS         ON CS_USER_ID    = GU_USER_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON  GTS_GAMING_TABLE_SESSION_ID        = GTSC_GAMING_TABLE_SESSION_ID
                                                  AND CASE ISNULL(CM_CURRENCY_ISO_CODE,'') 
                                                      WHEN '' THEN @NationalCurrency
                                                      WHEN @pCageChipColor THEN ''
                                                      ELSE CM.CM_CURRENCY_ISO_CODE END   = GTSC_ISO_CODE
                                                  AND CM.CM_CAGE_CURRENCY_TYPE           = GTSC_TYPE
       WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
         AND   CM_TYPE IN (@pOpeningCash,
                           --- @pFillerInClosingStock,
                           @pCageFillerIn,
                           @pCageFillerOut,
                           @pCageCloseSession,
                           @pFillerOutClosingStock,
                           @pChipsSale,
                           @pChipsPurchase,
                           @pChipsSaleDevolutionForTito,
                           @pChipsSaleWithCashIn,
                           @pChipsSaleRegisterTotal, 
                           @pReopenCashier)
    GROUP BY   CS_SESSION_ID
             , CM_TYPE
             , CCMR.CGM_MOVEMENT_ID
             , REPLICATE('0', 50 - LEN(cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)))) + cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)) 
               + REPLICATE('0', 50 - LEN(cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50)))) + cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50))
             , CM.CM_CURRENCY_ISO_CODE
             , CM.CM_CAGE_CURRENCY_TYPE
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTSC_INITIAL_CHIPS_AMOUNT
    ORDER BY   MAX(CM_DATE) DESC
          
  DECLARE @cursor_id_mov AS nvarchar(255)

  DECLARE _cursor CURSOR FOR SELECT MIN(ID_MOV) 
                               FROM #TABLE_MOVEMENTS 
                              WHERE TYPE_MOV IN (@pCageFillerIn , @pFillerInClosingStock) AND GTSC_INITIAL_CHIPS_AMOUNT > 0
                              GROUP BY CS_SESSION_ID
  
  OPEN _cursor  
  FETCH NEXT FROM _cursor INTO @cursor_id_mov
  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    UPDATE #TABLE_MOVEMENTS
       SET OPENER_MOV = 1
     WHERE ID_MOV = @cursor_id_mov

    FETCH NEXT FROM _cursor INTO @cursor_id_mov 
  END   
  CLOSE _cursor;  
  DEALLOCATE _cursor;  

  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')

  SELECT @Columns = COALESCE(@Columns + ',', '') +  '[' + CURRENCY_TYPE + ']'                             
    FROM   (SELECT   DISTINCT ISNULL(CE_CURRENCY_ORDER, -1) CE_CURRENCY_ORDER
                   , CASE WHEN CURRENCY_TYPE > 1000 
                          THEN 0
                          ELSE 1 END AS ORDER_2
                   , CASE WHEN CURRENCY_ISO_CODE = '' THEN 'X02' ELSE CURRENCY_ISO_CODE END 
                   + ' ' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_TYPE      
              FROM   #TABLE_MOVEMENTS 
              LEFT   JOIN CURRENCY_EXCHANGE ON CURRENCY_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
             WHERE   CURRENCY_ISO_CODE IS NOT NULL
           ) AS COLS
  ORDER   BY CE_CURRENCY_ORDER ASC
        , ORDER_2 ASC
        , CURRENCY_TYPE ASC

  SET @Columns = ISNULL(@Columns,COALESCE('[' + @NationalCurrency + ']',''))                   

  SELECT * FROM   #TABLE_MOVEMENTS   

   EXEC ('-- INITIAL AMOUNT
        SELECT   ''INITIAL_AMOUNT''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV = 1
                  UNION
                 SELECT   0 AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV <> 1
               ) AS T1
        PIVOT (
                SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
              ) AS PVT
        UNION   ALL
         -- TOTAL FILL IN
        SELECT   ''TOTAL_FILL_IN''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END 
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV <> 1
                  UNION
                 SELECT   0 AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV = 1
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
         UNION   ALL
          -- TOTAL WITHDRAW
        SELECT   ''TOTAL_WITHDRAW'' AS NAME_MOV
               , *
          FROM (  SELECT   SUB_AMOUNT AS AMOUNT
                         , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                           + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                    FROM   #TABLE_MOVEMENTS
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
   ')
  
   DROP TABLE #TABLE_MOVEMENTS      

  SELECT @pDrop = dbo.GT_Calculate_DROP(@pOwnSalesAmount, @pExternalSalesAmount, @pCollectedAmount, @pIsIntegratedCashier) 
 
END
GO
  
GRANT EXECUTE ON [dbo].[GT_Cashier_Movements_And_Summary_By_Session] TO [WGGUI] WITH GRANT OPTION
GO


/******* TRIGGERS *******/
