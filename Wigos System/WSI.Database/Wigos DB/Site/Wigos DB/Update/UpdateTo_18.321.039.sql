/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 320;

SET @New_ReleaseId = 321;
SET @New_ScriptName = N'UpdateTo_18.321.039.sql';
SET @New_Description = N'CountR/AFIP';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'Enabled')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'Enabled', '0')
END

GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'FrequencyToGetPendingTasks')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'FrequencyToGetPendingTasks', '5')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ClosingTime')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ClosingTime', '0')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ClosingTimeMinutes')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ClosingTimeMinutes', '0')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'TimeOffsetMinutesToSendMeters')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'TimeOffsetMinutesToSendMeters', '180')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ServerAddress')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ServerAddress', 'http://localhost/JAZALOCALWS/Service.asmx')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ServerAddress2')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ServerAddress2', 'http://localhost/JAZALOCALWS/Service.asmx')
END

GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'EnableMessageTrace')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'EnableMessageTrace', '0')
END
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Enabled', '0')
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Log.Mode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Log.Mode', '0')
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Offline.Interval')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Offline.Interval', '30')
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Server.Version')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Server.Version', '1')
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Server.ProtocolVersion')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Server.ProtocolVersion', '1')
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Server.Port')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Server.Port', '2600')  
GO  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Payment.TimeOut')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Payment.TimeOut', '60')  
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY = 'OutputPath')
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Witholding.Document','OutputPath','')
GO

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS]') AND NAME = 'tsm_sas_accounting_denom')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS] ADD tsm_sas_accounting_denom MONEY NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINALS]') AND NAME = 'te_sas_accounting_denom')
	ALTER TABLE [dbo].[TERMINALS] ADD te_sas_accounting_denom MONEY NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS_HISTORY]') AND NAME = 'tsmh_sas_accounting_denom')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS_HISTORY] ADD tsmh_sas_accounting_denom MONEY NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS_HISTORY]') AND NAME = 'tsmh_created_datetime')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS_HISTORY] ADD tsmh_created_datetime DATETIME NULL DEFAULT GetDate()
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS_HISTORY]') AND NAME = 'tsmh_group_id')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS_HISTORY] ADD tsmh_group_id BIGINT NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS_HISTORY]') AND NAME = 'tsmh_meter_origin')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS_HISTORY] ADD tsmh_meter_origin INT NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_SAS_METERS_HISTORY]') AND NAME = 'tsmh_meter_max_value')
	ALTER TABLE [dbo].[TERMINAL_SAS_METERS_HISTORY] ADD tsmh_meter_max_value BIGINT NULL
GO

DECLARE @_currency_ISO_code AS VARCHAR(3)

SELECT @_currency_ISO_code = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'
IF (@_currency_ISO_code = 'ARS' AND NOT EXISTS (SELECT SMCG_METER_CODE FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002 AND SMCG_METER_CODE = 74))
   INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID,SMCG_METER_CODE) VALUES (10002,74) -- Bill of 500 
GO

declare @catalogId as int

IF NOT EXISTS (SELECT * FROM catalogs WHERE cat_system_type = 4)
BEGIN
  INSERT INTO catalogs (cat_type,cat_name,cat_description,cat_enabled,cat_system_type) VALUES (1,'Mesas AFIP','Tipos de Mesas AFIP',1,4)
  set @catalogId = (SELECT cat_id  FROM catalogs WHERE  cat_system_type = 4)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId ,'Ruleta','Ruleta',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId ,'Naipes','Naipes',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId ,'Dados','Dados',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId ,'Torneo','Torneo',1)

END

--Añadir columna en edición de mesas de juego
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[GAMING_TABLES_TYPES]') AND NAME = 'gtt_report_type_id')
BEGIN
  ALTER TABLE gaming_tables_types
  ADD gtt_report_type_id INTEGER NULL 
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.countr') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.countr(
  cr_countr_id int IDENTITY(1,1) NOT NULL,
	cr_code int NOT NULL,
	cr_name nvarchar(50) NOT NULL,
	cr_description nvarchar(250) NULL,
	cr_provider nvarchar(50) NULL,
	cr_area_id int NULL,
	cr_bank_id int NULL,
	cr_floor_id nvarchar(20) NULL,
	cr_max_payment money NULL,
	cr_min_payment money NULL,
	cr_current_isocode nvarchar(3) NULL,
	cr_enabled bit NOT NULL,
	cr_created datetime NOT NULL,
	cr_last_modified datetime NOT NULL,
	cr_retirement_date datetime NULL,
	cr_retirement_user_id bigint NULL,
	cr_ip_address nvarchar(20) NULL,
	cr_status int NULL,
	cr_last_connection datetime NULL,
	cr_show_log bit NOT NULL,
	cr_create_ticket bit NOT NULL,
CONSTRAINT [PK_cr_countr_id] PRIMARY KEY CLUSTERED 
(
  cr_countr_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
--TABLE COUNTR_SESSIONS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.countr_sessions') AND type in (N'U'))
BEGIN

CREATE TABLE dbo.countr_sessions(
	crs_session_id bigint IDENTITY(1,1) NOT NULL,
	crs_countr_id int NOT NULL,
	crs_cashier_session_id bigint NOT NULL,
	crs_status int NOT NULL,
	crs_initial_amount money NULL,
	crs_final_amount money NULL,
	crs_collected_amount money NULL,
	crs_openning_date datetime NOT NULL,
	crs_closed_date datetime NULL,
	crs_withdrawals_amount money NULL,
	crs_deposits_amount money NULL	
  CONSTRAINT [PK_crs_session_id] PRIMARY KEY CLUSTERED 
  (
    [crs_session_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (select 1 from INFORMATION_SCHEMA.columns where table_name = 'COUNTR_SESSIONS' and column_name = 'crs_ticket_amount')
BEGIN
  alter table COUNTR_SESSIONS add crs_ticket_amount money NULL 
END

GO
--TABLE COUNTR_TRANSACTION
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.countr_transaction') AND type in (N'U'))
BEGIN
 CREATE TABLE [dbo].[countr_transaction](
	[crt_transaction_id] [bigint] IDENTITY(1,1) NOT NULL,
	[crt_paid_ticket_id] [bigint] NULL,
	[crt_operation_id] [bigint] NULL,
	[crt_paid_amount] [money] NULL,
	[crt_new_ticket_id] [bigint] NULL,
	[crt_new_ticket_amount] [money] NULL,
	[crt_cash_amount] [money] NULL,
	[crt_countr_id] [int] NULL,
	[crt_cashier_session_id] [bigint] NULL,
	[crt_gaming_day] [datetime] NOT NULL,
	[crt_status] [int] NOT NULL,
	[crt_last_modified] datetime NOT NULL
 CONSTRAINT [PK_countr_transaction] PRIMARY KEY CLUSTERED 
(
	[crt_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
--TABLE COUNTR_LOG
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.countr_log') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.countr_log(
  crl_id bigint NOT NULL IDENTITY (1, 1),
  crl_code int NOT NULL,
  crl_request nvarchar(250) NULL,
  crl_response nvarchar(250) NULL,
  crl_elapsed_time int NOT NULL,
  crl_datetime datetime NOT NULL,
CONSTRAINT [PK_crl_id] PRIMARY KEY CLUSTERED 
(
  crl_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.major_prizes_to_generate') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[major_prizes_to_generate]
  (
  [mpg_operation_id] [bigint] NOT NULL,
  ) ON [PRIMARY]

  ALTER TABLE [dbo].[major_prizes_to_generate] ADD CONSTRAINT [PK_major_prizes_to_generate] PRIMARY KEY CLUSTERED  ([mpg_operation_id]) ON [PRIMARY]
  ALTER TABLE [dbo].[major_prizes_to_generate] ADD CONSTRAINT [FK_major_prizes_to_generate_account_major_prizes] FOREIGN KEY ([mpg_operation_id]) REFERENCES [dbo].[account_major_prizes] ([amp_operation_id])
END
GO

--TABLE CASHIER_TERMINALS
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE TABLE_NAME = 'cashier_terminals' AND COLUMN_NAME = 'ct_countr_id')
BEGIN
  ALTER TABLE CASHIER_TERMINALS 
    ADD CT_COUNTR_ID INT 
END
GO
-- TABLE GAMEGATEWAY_LOG
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_log]') and name = 'gl_terminal_id')
  ALTER TABLE [dbo].[gamegateway_log] 
	  ADD [gl_terminal_id] int NULL;
GO

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gaming_tables_connected')
BEGIN
	CREATE TABLE [dbo].[gaming_tables_connected](
		[gmc_gamingtable_id] [int] NOT NULL,
		[gmc_gaming_day] [int] NOT NULL,
		[gmc_enabled] [bit] NOT NULL,
		[gmc_used] [bit] NOT NULL,
	 CONSTRAINT [PK_Gaming_Tables_Connected] PRIMARY KEY CLUSTERED 
	(
		[gmc_gamingtable_id] ASC,
		[gmc_gaming_day] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[gaming_tables_connected] ADD  CONSTRAINT [DF_Gaming_Tables_Connected_gmc_gaming_day]  DEFAULT ((0)) FOR [gmc_gaming_day]
	ALTER TABLE [dbo].[gaming_tables_connected] ADD  CONSTRAINT [DF_Gaming_Tables_Connected_gmc_enabled]  DEFAULT ((0)) FOR [gmc_enabled]
	ALTER TABLE [dbo].[gaming_tables_connected] ADD  CONSTRAINT [DF_Gaming_Tables_Connected_gmc_used]  DEFAULT ((0)) FOR [gmc_used]
END


GO

/******* INDEXES *******/

/******* RECORDS *******/
IF NOT EXISTS(SELECT SEQ_ID FROM SEQUENCES WHERE SEQ_ID = 101)
    INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (101, 1);
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393268 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393268, 10, 0, N'Denominación contable cambiada', N'Denominación contable cambiada', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393268 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393268,  9, 0, N'Accounting denomination changed', N'Accounting denomination changed', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393268 AND [alcc_category] = 18) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393268, 18, 0, GETDATE() )
END
GO

IF NOT EXISTS (SELECT   1 FROM GUI_USERS WHERE GU_USER_TYPE = 6 )
BEGIN
DECLARE @user_id AS INT

SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
  FROM   GUI_USERS  

INSERT INTO GUI_USERS ( GU_USER_ID, GU_PROFILE_ID, GU_USERNAME, GU_ENABLED, GU_PASSWORD, GU_NOT_VALID_BEFORE, GU_NOT_VALID_AFTER, GU_LAST_CHANGED, GU_PASSWORD_EXP, GU_PWD_CHG_REQ, GU_FULL_NAME, GU_LOGIN_FAILURES, GU_USER_TYPE)
     VALUES           ( @user_id, 0, 'SYS-Redemption', 1, CAST('0000' AS BINARY(40)), GETDATE() - 3, NULL, GETDATE(), NULL, 0, 'SYS-Redemption', 0, 6)
END
GO

DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'AR')
BEGIN
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'AR'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '160'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- DNI
END
GO

-- ARGENTINA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'AR' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'AR')
END
GO

-- ARGENTINA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'AR' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (160,1,100,'DNI','AR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (161,1,1,'Pasaporte','AR')
END
GO

-- ARGENTINA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'AR' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Buenos Aires','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Catamarca','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Córdoba','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Corrientes','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chaco','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chubut','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Entre Ríos','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Formosa','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jujuy','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Pampa','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Rioja','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mendoza','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Misiones','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Neuquén','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Río Negro','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salta','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Luis','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Cruz','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Fe','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago del Estero','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tierra del Fuego, Antártida e islas del Atl. Sur','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tucumán','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ciudad Autónoma de Buenos Aires','AR');
END
GO

DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'NI')
BEGIN
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Departamentos' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'NI'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '170'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Cédula de identidad
END
GO

-- NICARAGUA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'NI' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'NI')
END
GO

-- NICARAGUA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'NI' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (170,1,100,'Cédula de identidad','NI')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (171,1,1,'Pasaporte','NI')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (172,1,2,'Licencia de conducir','NI')
END
GO

-- NICARAGUA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'NI' ))
BEGIN
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Boaco','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Carazo','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chinandega','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chontales','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Estelí','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Granada','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jinotega','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('León','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Madriz','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Managua','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Masaya','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Matagalpa','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Nueva Segovia','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rivas','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Río San Juan','NI');
END
GO

IF NOT EXISTS (SELECT 1 FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 5)
BEGIN
  -- ALARM GROUPS
  INSERT INTO ALARM_GROUPS VALUES( 5, 9,  0, N'Redeem Kiosk',        '', 1)            
  INSERT INTO ALARM_GROUPS VALUES( 5, 10, 0, N'Kiosko de Redención', '', 1)
                 
  -- ALARM CATEGORIES                    
  INSERT INTO ALARM_CATEGORIES VALUES(51, 9,  5, 0, N'Door'  , '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(51, 10, 5, 0, N'Puerta', '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(52, 9, 5,  0, N'Operator card'      , '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(52, 10, 5, 0, N'Tarjeta de operador', '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(53, 9, 5,  0, N'Customer card'     ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(53, 10, 5, 0, N'Tarjeta de cliente',  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(54, 9, 5,  0, N'Banknote' ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(54, 10, 5, 0, N'Billete'  ,  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(55, 9, 5,  0, N'Ticket',  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(55, 10, 5, 0, N'Ticket',  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(56, 9, 5,  0, N'Call operator'   ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(56, 10, 5, 0, N'Llamada operador',  '', 1)

  -- ALARM CATALOG
  INSERT INTO ALARM_CATALOG VALUES(5242881, 9, 0, N'Main door closed', N'Main door closed', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242882, 9, 0, N'Main door opened', N'Main door opened', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242883, 9, 0, N'Head door closed', N'Head door closed', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242884, 9, 0, N'Head door opened', N'Head door opened', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242885, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242886, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242887, 9, 0, N'Captured', N'Captured', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242888, 9, 0, N'Issued', N'Issued', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242889, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242896, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242897, 9, 0, N'Captured', N'Captured', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242898, 9, 0, N'Issued', N'Issued', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242899, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242900, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242901, 9, 0, N'Stacked', N'Stacked', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242902, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242903, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242904, 9, 0, N'Stacked', N'Stacked', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242905, 9, 0, N'Call operator', N'Call operator', 1)

  INSERT INTO ALARM_CATALOG VALUES(5242881, 10, 0, N'Puerta principal cerrada', N'Puerta principal cerrada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242882, 10, 0, N'Puerta principal abierta', N'Puerta principal abierta', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242883, 10, 0, N'Puerta cabecera cerrada', N'Puerta cabecera cerrada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242884, 10, 0, N'Puerta cabecera abierta', N'Puerta cabecera abierta', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242885, 10, 0, N'Rechazada', N'Rechazada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242886, 10, 0, N'Insertada', N'Insertada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242887, 10, 0, N'Capturada', N'Capturada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242888, 10, 0, N'Emitida', N'Emitida', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242889, 10, 0, N'Rechazada', N'Rechazada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242896, 10, 0, N'Insertada', N'Insertada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242897, 10, 0, N'Capturada', N'Capturada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242898, 10, 0, N'Emitida', N'Emitida', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242899, 10, 0, N'Rechazado', N'Rechazado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242900, 10, 0, N'Insertado', N'Insertado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242901, 10, 0, N'Apilado', N'Apilado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242902, 10, 0, N'Rechazado', N'Rechazado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242903, 10, 0, N'Insertado', N'Insertado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242904, 10, 0, N'Apilado', N'Apilado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242905, 10, 0, N'Llamada operador', N'Llamada operador', 1)

  -- ALARM CATALOG PER CATEGORY
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242881,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242882,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242883,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242884,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242885,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242886,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242887,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242888,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242889,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242896,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242897,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242898,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242899,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242900,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242901,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242902,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242903,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242904,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242905,56, 0, GETDATE())
END
GO

/******* PROCEDURES *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MBAndCashierSessionsCashMonitor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
GO
CREATE PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
AS
BEGIN  

DECLARE @IsCountR AS BIT;
DECLARE @UserType AS SMALLINT;

SET @UserType = 0

  SELECT @IsCountR = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY   = 'CountR'
     AND GP_SUBJECT_KEY = 'Enabled'
  
  IF @IsCountR = 1
  BEGIN
    SET @UserType = 6
  END 
  SELECT        '0'                            AS ROW_TYPE                   
   	           , ISNULL(GU_USER_ID, '')        AS ROW_ID                   
               , ISNULL(GU_USERNAME, '')       AS USR_NAME                    
               , ISNULL(GU_FULL_NAME, '')      AS USR_FULL_NAME                   
               , ISNULL(GU_EMPLOYEE_CODE, '')  AS EMPLOYEE_CODE                   
               , ISNULL(CS_BALANCE, '')        AS CASHIER_BALANCE                   
               , 0                             AS MB_CASH      
               , 0                             AS MB_RECHARGE_LIMIT
               , 0                             AS MB_NUM_RECHARGES
               , ''                            AS MB_LAST_ACTIVITY
               , CT_NAME                       AS CASHIER_NAME
               , 0                             AS MB_RECHARGE
          FROM   GUI_USERS                            
     LEFT JOIN   CASHIER_SESSIONS                     
            ON   CS_USER_ID = GU_USER_ID     
  
     LEFT JOIN   CASHIER_TERMINALS                     
            ON   CS_CASHIER_ID = CT_CASHIER_ID
  
         WHERE   CS_STATUS = 0
           AND	 GU_USER_TYPE IN (0, @UserType)

         UNION                                        

        SELECT   '1'                               			                AS ROW_TYPE  
               , ISNULL(MB_ACCOUNT_ID, '')				                      AS ROW_ID    
               , ISNULL('MB-' + CAST(MB_ACCOUNT_ID AS NVARCHAR), '')  	AS USR_NAME         
               , ISNULL(MB_HOLDER_NAME, '')				                      AS USR_FULL_NAME    
               , ISNULL(MB_EMPLOYEE_CODE, '')				                    AS EMPLOYEE_CODE    
               , 0         						                                  AS CASHIER_BALANCE  
               , ISNULL(MB_PENDING_CASH, 0)				                      AS MB_CASH      
               , ISNULL(MB_BALANCE, 0)					                        AS MB_RECHARGE_LIMIT
               , ISNULL(MB_ACTUAL_NUMBER_OF_RECHARGES, 0)			          AS MB_NUM_RECHARGES
               , ISNULL(MB_LAST_ACTIVITY, '')				                    AS MB_LAST_ACTIVITY
               , CT_NAME                                              	AS CASHIER_NAME
               , MB_CASH_IN                                             AS MB_RECHARGE
 
          FROM  MOBILE_BANKS                           
     LEFT JOIN  CASHIER_SESSIONS                     
            ON	CS_SESSION_ID = MB_CASHIER_SESSION_ID 
     LEFT JOIN  CASHIER_TERMINALS                     
            ON  CS_CASHIER_ID = CT_CASHIER_ID
         WHERE  CS_STATUS = 0
         AND	MB_ACCOUNT_TYPE IN ( 1 , 0 ) 
END
GO

GRANT EXECUTE ON [dbo].[MBAndCashierSessionsCashMonitor] TO [wggui] WITH GRANT OPTION;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetTerminalLastPreviousMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GetTerminalLastPreviousMeters]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GetTerminalLastPreviousMeters]
(
    @pTerminalId                NVARCHAR(50)
  , @pTerminalAFIP              NVARCHAR(4000)
  , @pMaxDateTime               DATETIME
)
AS
BEGIN
  --Meter Types CONSTANTS
  DECLARE @TYPE_DAILY           INT;
  DECLARE @TYPE_FIRST_TIME      INT;

  --Meter Codes CONSTANTS
  DECLARE @METER_COIN_IN        INT;
  DECLARE @METER_COIN_OUT       INT;
  DECLARE @METER_JACKPOTS       INT;
  DECLARE @METER_GAMES_PLAYED   INT;

  --Meter Types
  SET @TYPE_DAILY = 1           --Daily
  SET @TYPE_FIRST_TIME = 12     --First Time

  --Meter Codes
  SET @METER_COIN_IN = 0        --Coin In
  SET @METER_COIN_OUT = 1       --Coin Out
  SET @METER_JACKPOTS = 2       --Jackpots
  SET @METER_GAMES_PLAYED = 5   --Games Played

  -- Get last previous meters
  SELECT @pTerminalId                         as TERMINALID
       , @pTerminalAFIP                       as REGISTRATION_CODE
       , MIN(th.TSMH_SAS_ACCOUNTING_DENOM)    as SAS_ACCOUNTING_DENOM
       , th.TSMH_DATETIME                     as METER_DATE
       , th.TSMH_METER_CODE                   as METER_CODE
       , @TYPE_DAILY                          as METER_TYPE   --@@todo: verify is correct
       , th.TSMH_METER_FIN_VALUE              as INI_VALUE
       , th.TSMH_METER_FIN_VALUE              as FIN_VALUE
       , NULL                                 as GROUP_ID
       , NULL                                 as METER_ORIGIN
       , NULL                                 as METER_MAX_VALUE
       , MIN(th.TSMH_DATETIME)                as INTERVAL_FROM
       , MAX(th.TSMH_DATETIME)                as INTERVAL_TO
  FROM TERMINAL_SAS_METERS_HISTORY AS th
  INNER JOIN
          (
          SELECT hi.TSMH_TERMINAL_ID    as TERMINAL_ID
               , hi.TSMH_METER_CODE     as METER_CODE
               , MAX(hi.TSMH_DATETIME)  as MAX_DATE
           FROM TERMINAL_SAS_METERS_HISTORY AS hi
          WHERE hi.TSMH_TYPE IN (
                                    @TYPE_DAILY, @TYPE_FIRST_TIME   --We don't need Rollover, Denom change or Service RAM Clear
                                )
            AND hi.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED)
            --Get date of last record previous to @pMaxDateTime
            AND hi.TSMH_DATETIME <= @pMaxDateTime
            --Terminal to filter
            AND hi.TSMH_TERMINAL_ID = @pTerminalId
          GROUP BY hi.TSMH_TERMINAL_ID, hi.TSMH_METER_CODE
          ) mm  on th.TSMH_TERMINAL_ID = mm.TERMINAL_ID
               AND th.TSMH_METER_CODE  = mm.METER_CODE
               AND th.TSMH_DATETIME    = mm.MAX_DATE
  GROUP BY th.TSMH_DATETIME
         , th.TSMH_METER_CODE
         , th.TSMH_METER_FIN_VALUE
  ;
  --@@todo: has to be deleted??
/*
      WHERE th.TSMH_TYPE IN (
                                @TYPE_DAILY, @TYPE_FIRST_TIME
                            )
        AND th.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED)
        --Get date of last record previous to @pMaxDateTime
        AND th.TSMH_DATETIME <= @pMaxDateTime
        --Terminal to filter
        AND th.TSMH_TERMINAL_ID = @pTerminalId
*/
END
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetTerminalLastPreviousMeters] TO [WGGUI] WITH GRANT OPTION
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetTerminalMetersHistory]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
(
    @pStartDateTime           DATETIME
  , @pEndDateTime             DATETIME
  , @pPendingType             INT
  , @pTerminalsAFIP           NVARCHAR(4000)
  , @pTerminalStatusActive    INT
  , @pTerminalType            NVARCHAR(4000)
)
AS
BEGIN
  --Pending Types CONSTANTS
  DECLARE @PENDING_TYPE_REQUEST                   INT;

  --Meter Types CONSTANTS
  DECLARE @TYPE_DAILY                             INT;

  DECLARE @TYPE_ROLLOVER                          INT;
  DECLARE @TYPE_SAS_ACCOUNT_DENOM_CHANGE          INT;
  DECLARE @TYPE_SERVICE_RAM_CLEAR                 INT;

  DECLARE @TYPE_GROUP_SERVICE_RAM_CLEAR           INT;
  DECLARE @TYPE_GROUP_ROLLOVER                    INT;
  DECLARE @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE    INT;

  --Meter Codes CONSTANTS
  DECLARE @METER_COIN_IN                          INT;
  DECLARE @METER_COIN_OUT                         INT;
  DECLARE @METER_JACKPOTS                         INT;
  DECLARE @METER_GAMES_PLAYED                     INT;

  --Internal variables
  DECLARE @LAST_TERMINAL_METER_DATE               DATETIME;
  DECLARE @INTERVAL_FROM                          DATETIME;
  DECLARE @INTERVAL_TO                            DATETIME;

  DECLARE @TERMINAL_ID                            INT;
  DECLARE @REGISTRATION_CODE                      NVARCHAR(50);

  --Pending Types
  SET @PENDING_TYPE_REQUEST = 4;                  --Request

  --Meter Types
  SET @TYPE_DAILY = 1                             --Daily

  SET @TYPE_ROLLOVER = 11                         --Rollover
  SET @TYPE_SAS_ACCOUNT_DENOM_CHANGE = 14         --Denom change
  SET @TYPE_SERVICE_RAM_CLEAR = 15                --Service RAM Clear

  SET @TYPE_GROUP_ROLLOVER = 110                  --Group Rollover
  SET @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE = 140  --Group Denom change
  SET @TYPE_GROUP_SERVICE_RAM_CLEAR = 150         --Group Service RAM Clear

  --Meter Codes
  SET @METER_COIN_IN = 0                          --Coin In
  SET @METER_COIN_OUT = 1                         --Coin Out
  SET @METER_JACKPOTS = 2                         --Jackpots
  SET @METER_GAMES_PLAYED = 5                     --Games Played

  --Temporary table for terminals connected
  CREATE TABLE #TEMP_TERMINALS
  (
    TERMINAL_ID         INT
  , REGISTRATION_CODE   NVARCHAR(50)
  );

  --Temporary table for meters
  CREATE TABLE #TEMP_METERS
  (
    TERMINAL_ID           INT
  , REGISTRATION_CODE     NVARCHAR(50)
  , SAS_ACCOUNTING_DENOM  MONEY
  , METER_DATE            DATETIME
  , METER_CODE            INT
  , METER_TYPE            INT
  , INI_VALUE             BIGINT
  , FIN_VALUE             BIGINT
  , GROUP_ID              BIGINT
  , METER_ORIGIN          INT
  , METER_MAX_VALUE       BIGINT
  , INTERVAL_FROM         DATETIME
  , INTERVAL_TO           DATETIME
  );

  SET @INTERVAL_FROM = @pStartDateTime;
  SET @INTERVAL_TO   = @pEndDateTime;

  IF (@pPendingType = @PENDING_TYPE_REQUEST)
  BEGIN
    --Request type
    --Get Terminal ID
    SET @TERMINAL_ID = ( SELECT  MIN(te.TE_TERMINAL_ID)
                           FROM  TERMINALS te
                          WHERE  (te.TE_REGISTRATION_CODE = @pTerminalsAFIP)
                            -- Only show terminal with the terminal type
                            AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                                                FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))
                        );

    --Search terminal last meter date previous
    INSERT INTO #TEMP_METERS
           EXEC SP_AFIP_GetTerminalLastPreviousMeters
                     @TERMINAL_ID
                   , @pTerminalsAFIP
                   , @pEndDateTime
                   ;
  END --@PENDING_TYPE_REQUEST
  ELSE
  BEGIN
    --Terminals Type
    --Get terminals connected
    INSERT INTO #TEMP_TERMINALS
    (
      TERMINAL_ID, REGISTRATION_CODE
    )
    SELECT  te.TE_TERMINAL_ID
          , te.TE_REGISTRATION_CODE
      FROM  TERMINALS te
        --Was the terminal active?
     WHERE  EXISTS ( SELECT   1
                       FROM   TERMINALS_CONNECTED tc
                      WHERE   tc.TC_DATE >= @INTERVAL_FROM
                        AND   tc.TC_DATE <  @INTERVAL_TO
                        AND   tc.TC_MASTER_ID = te.TE_MASTER_ID
                        AND   tc.TC_STATUS = @pTerminalStatusActive
                   )
       --Are there any terminal to filter with?
       AND  ((ISNULL(@pTerminalsAFIP, '') = '') OR (te.TE_REGISTRATION_CODE IN (SELECT tp.SST_VALUE
                                                                                FROM SplitStringIntoTable(@pTerminalsAFIP, ',', 1) AS tp)))
       -- Only show terminal with the terminal type
       AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                           FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))

    --Get meters for the terminals connected
    INSERT INTO #TEMP_METERS
    (
      TERMINAL_ID
    , REGISTRATION_CODE
    , SAS_ACCOUNTING_DENOM
    , METER_DATE
    , METER_CODE
    , METER_TYPE
    , INI_VALUE
    , FIN_VALUE
    , GROUP_ID
    , METER_ORIGIN
    , METER_MAX_VALUE
    , INTERVAL_FROM
    , INTERVAL_TO
    )
    SELECT  te.TERMINAL_ID
          , te.REGISTRATION_CODE
          , th.TSMH_SAS_ACCOUNTING_DENOM
          , th.TSMH_DATETIME
          , th.TSMH_METER_CODE
          , th.TSMH_TYPE
          , ISNULL(th.TSMH_METER_INI_VALUE, 0)
          , ISNULL(th.TSMH_METER_FIN_VALUE, 0)
          , th.TSMH_GROUP_ID
          , th.TSMH_METER_ORIGIN
          , th.TSMH_METER_MAX_VALUE
          , @INTERVAL_FROM
          , @INTERVAL_TO
     FROM TERMINAL_SAS_METERS_HISTORY AS th
    INNER JOIN #TEMP_TERMINALS AS te ON th.TSMH_TERMINAL_ID = te.TERMINAL_ID
    WHERE th.TSMH_DATETIME >= @INTERVAL_FROM
      AND th.TSMH_DATETIME <  @INTERVAL_TO
      AND th.TSMH_TYPE IN (
                             @TYPE_DAILY,
                             @TYPE_ROLLOVER, @TYPE_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_SERVICE_RAM_CLEAR,
                             @TYPE_GROUP_ROLLOVER, @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_GROUP_SERVICE_RAM_CLEAR
                            )
      AND th.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED);

    -- Get terminals connected with no meters between the dates
    DECLARE curTerminalsWithNoMeters CURSOR FOR
      SELECT te.TERMINAL_ID
           , te.REGISTRATION_CODE
      FROM #TEMP_TERMINALS te
      WHERE NOT EXISTS (SELECT *
                        FROM #TEMP_METERS tm
                        WHERE tm.TERMINAL_ID = te.TERMINAL_ID);

    --Open cursor
    OPEN curTerminalsWithNoMeters

    FETCH NEXT FROM curTerminalsWithNoMeters
      INTO @TERMINAL_ID
         , @REGISTRATION_CODE;

    --Loop the cursor
    WHILE @@FETCH_STATUS = 0
    BEGIN
      -- For each terminal connected with no meters between the dates:
      --  * Search terminal last meter date previous
      INSERT INTO #TEMP_METERS
             EXEC SP_AFIP_GetTerminalLastPreviousMeters
                       @TERMINAL_ID
                     , @REGISTRATION_CODE
                     , @pEndDateTime
                     ;
      --Get next record
      FETCH NEXT FROM curTerminalsWithNoMeters
      INTO @TERMINAL_ID
         , @REGISTRATION_CODE;
    END --end while CURSOR

    --Free memory
    CLOSE curTerminalsWithNoMeters
    DEALLOCATE curTerminalsWithNoMeters
  END

  --Return meters data
  SELECT *
  FROM #TEMP_METERS
  ORDER BY TERMINAL_ID, METER_DATE, METER_TYPE, METER_CODE;

  --Drop temporary tables
  DROP TABLE #TEMP_TERMINALS;

  DROP TABLE #TEMP_METERS;
END

GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetTerminalMetersHistory] TO [WGGUI] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
GO


CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
(
  @pStarDateTime DATETIME
 )
AS
BEGIN
--Get the current iso code in general params
DECLARE @_Currency_ISO_Code AS VARCHAR(5);
DECLARE @_Default_Cashier_Afip_Type AS INT;

SET @_Default_Cashier_Afip_Type = 99;

SET @_Currency_ISO_Code = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' and GP_SUBJECT_KEY = 'CurrencyISOCode')

--Get the total sum of the GAMING TABLES
SELECT TABLE_ID
     , GTABLE_NAME
     , GTABLE_TYPE_ID
     , GTABLE_TYPE_DESC
     , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
     , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
     , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
     , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
     , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
     , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
     , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
     , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
     , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
     , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
     , CS_OPENING_DATE
     , CS_CLOSING_DATE
FROM
(
    --Get partial sum
    SELECT CM_TYPE
         , CM_INITIAL_BALANCE
         , CM_GAMING_TABLE_SESSION_ID
         , CM_CURRENCY_ISO_CODE
         , GT_GAMING_TABLE_ID AS TABLE_ID
         , GT_NAME AS GTABLE_NAME
         , GTT_REPORT_TYPE_ID AS GTABLE_TYPE_ID
         , GTT_NAME AS GTABLE_TYPE_DESC
         , CASE WHEN CM_TYPE = 0 THEN cm_initial_balance ELSE 0 END AS OPENING_CASH
         , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(cm_final_balance), 0) ELSE 0 END AS CLOSING_CASH
         , GTS_INITIAL_CHIPS_AMOUNT AS OPENING_CHIPS
         , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
         , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
         , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
         , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
         , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
         , CASE WHEN CM_TYPE in (300, 302, 307, 311) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS SALE_CHIPS_TOTAL
         , CASE WHEN CM_TYPE in (301) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS BUY_CHIPS_TOTAL
         , CS_OPENING_DATE, CS_CLOSING_DATE
    FROM  GAMING_TABLES_TYPES
    INNER JOIN GAMING_TABLES            ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
    INNER JOIN GAMING_TABLES_SESSIONS   ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
    INNER JOIN CASHIER_MOVEMENTS        ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
    INNER JOIN CASHIER_SESSIONS         ON CM_SESSION_ID = CS_SESSION_ID
    WHERE CM_DATE >= @pStarDateTime
      AND CM_DATE < DATEADD(day, 1, @pStarDateTime)
      AND CM_TYPE IN (0, 201, 202, 203, 300, 301, 302, 307, 311)
    GROUP BY CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE,
          GT_GAMING_TABLE_ID, GT_NAME, GTT_REPORT_TYPE_ID, GTT_NAME, GTS_GAMING_TABLE_SESSION_ID, CS_OPENING_DATE,
          CS_CLOSING_DATE, GTS_CASHIER_SESSION_ID, GTS_INITIAL_CHIPS_AMOUNT
 ) AS GT
GROUP BY  TABLE_ID, GTABLE_NAME, GTABLE_TYPE_ID, GTABLE_TYPE_DESC, CS_OPENING_DATE, CS_CLOSING_DATE

UNION ALL

--Get the total sum of the CASHIERS
SELECT TABLE_ID
     , GTABLE_NAME
     , @_Default_Cashier_Afip_Type
     , TIPO_MESA
     , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
     , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
     , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
     , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
     , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
     , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
     , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
     , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
     , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
     , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
     , CS_OPENING_DATE
     , CS_CLOSING_DATE
FROM
(
    --Select partial sum
    SELECT CM_TYPE
         , CM_INITIAL_BALANCE
         , CM_GAMING_TABLE_SESSION_ID
         , CM_CURRENCY_ISO_CODE
         , CS_USER_ID AS TABLE_ID
         , GU_FULL_NAME AS GTABLE_NAME
         , 'OTROS' AS TIPO_MESA
         , CASE WHEN CM_TYPE = 0 THEN cm_initial_balance ELSE 0 END AS OPENING_CASH
         , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(cm_final_balance), 0) ELSE 0 END AS CLOSING_CASH
         , 0.00 AS OPENING_CHIPS
         , CASE WHEN CM_TYPE = 201 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
         , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
         , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
         , CASE WHEN CM_TYPE = 203 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
         , CASE WHEN CM_TYPE = 202 AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
         , 0.00 AS SALE_CHIPS_TOTAL
         , 0.00 AS BUY_CHIPS_TOTAL
         , CS_OPENING_DATE, CS_CLOSING_DATE
    FROM CASHIER_MOVEMENTS
    INNER JOIN  CASHIER_SESSIONS  ON CM_SESSION_ID = CS_SESSION_ID
    INNER JOIN GUI_USERS          ON CS_USER_ID    = GU_USER_ID
    WHERE CM_DATE >= @pStarDateTime
      AND CM_DATE < DATEADD(day, 1, @pStarDateTime)
      AND CM_TYPE IN (0, 201, 202, 203, 300, 301, 302, 307, 311)
    GROUP BY cs_session_id, CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE, CS_USER_ID, GU_FULL_NAME,
          CS_OPENING_DATE, CS_CLOSING_DATE
) AS GT
GROUP BY TABLE_ID, GTABLE_NAME, TIPO_MESA, CS_OPENING_DATE, CS_CLOSING_DATE

END

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES] TO [WGGUI] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES_TYPE]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
(
  @pStarDateTime DATETIME
)
AS
BEGIN
  CREATE TABLE #GamingTable
  (
       TABLE_ID          INT
     , TABLE_NAME        VARCHAR(250)
     , ID_TABLE_TYPE     INT
     , DESC_TABLE_TYPE   VARCHAR(250)
     , OPENING_CASH      MONEY
     , CLOSING_CASH      MONEY
     , OPENING_CHIPS     MONEY
     , CLOSING_CHIPS     MONEY
     , CASH_OUT_TOTAL    MONEY
     , CASH_IN_TOTAL     MONEY
     , CHIPS_OUT_TOTAL   MONEY
     , CHIPS_IN_TOTAL    MONEY
     , CHIP_SALES_TOTAL  MONEY
     , CHIPS_BUY_TOTAL   MONEY
     , OPENING_DATE      DATETIME
     , CLOSING_DATE      DATETIME
  )

  INSERT  #GamingTable EXEC SP_AFIP_GAMING_TABLES @pStarDateTime

  SELECT ID_TABLE_TYPE
       , DESC_TABLE_TYPE
       , COUNT(ID_TABLE_TYPE) AS TABLE_COUNT
       , SUM(OPENING_CASH) AS OPENING_CASH
       , SUM(CLOSING_CASH) AS CLOSING_CASH
       , SUM(OPENING_CHIPS) AS OPENING_CHIPS
       , SUM(CLOSING_CHIPS) AS CLOSING_CHIPS
       , SUM(CASH_OUT_TOTAL) AS CASH_OUT_TOTAL
       , SUM(CASH_IN_TOTAL) AS CASH_IN_TOTAL
       , SUM(CHIPS_OUT_TOTAL) AS CHIPS_OUT_TOTAL
       , SUM(CHIPS_IN_TOTAL) AS CHIPS_IN_TOTAL
       , SUM(CHIP_SALES_TOTAL) AS CHIP_SALES_TOTAL
       , SUM(CHIPS_BUY_TOTAL) AS CHIPS_BUY_TOTAL
  FROM #GamingTable
  GROUP BY ID_TABLE_TYPE, DESC_TABLE_TYPE

  DROP TABLE #GamingTable
END

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES_TYPE] TO [WGGUI] WITH GRANT OPTION
GO
/****** OBJECT:  STOREDPROCEDURE [DBO].[sp_GetTablesActivity]    SCRIPT DATE: 05/17/2016 16:29:19 ******/
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_GetTablesActivity]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_GetTablesActivity]
GO

/****** OBJECT:  STOREDPROCEDURE [DBO].[sp_GetTablesActivity]    SCRIPT DATE: 05/17/2016 16:29:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DBO].[sp_GetTablesActivity]
@pDate AS DATETIME
AS
BEGIN

DECLARE @Count INT
DECLARE @Cols VARCHAR(MAX)
DECLARE @ColsVal VARCHAR(MAX)
DECLARE @ColsValU VARCHAR(MAX)
DECLARE @ColsEnabled VARCHAR(MAX)
DECLARE @ColsUsed VARCHAR(MAX)

SELECT @Count = 0
SELECT @Cols = ''
SELECT @ColsVal = ''
SELECT @ColsValU = ''
SELECT @ColsEnabled = ''
SELECT @ColsUsed = ''

WHILE @Count < DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,@pDate),0)))
BEGIN
   SELECT @Count = @Count + 1
   
   IF @Count <> 1
    BEGIN
		SELECT @Cols = @Cols + ','
		SELECT @ColsVal = @ColsVal + ','
		SELECT @ColsValU = @ColsValU + ','
		SELECT @ColsEnabled = @ColsEnabled + ','
		SELECT @ColsUsed = @ColsUsed + ','
	END
	
   SELECT @Cols = @Cols + '[' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsVal = @ColsVal + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As D' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsValU = @ColsValU + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As DU' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsEnabled = @ColsEnabled + '[D' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsUsed = @ColsUsed + '[DU' + CONVERT(VARCHAR,@Count) + ']'
    
END

DECLARE @Query NVARCHAR(MAX)
SET @Query = '           
			
			DECLARE @DATE AS DATETIME
			SELECT @DATE = ''' + CONVERT(VARCHAR(10),@pDate,112) + '''

			SELECT 
				A.GMC_GAMINGTABLE_ID T_ID,
				A.GT_NAME T_NAME,
				A.GT_TYPE T_TYPE,
				A.GMC_MONTH T_MONTH,
				A.T_DAYS,
				' + @ColsEnabled + ',
				A.T_DAYSUSE,
				' + @ColsUsed + '
			FROM 
			(
				SELECT 
					GMC_GAMINGTABLE_ID , 
					GT_TYPE,
					GT_NAME,
					CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
					T_DAYS,
					T_DAYSUSE,
					' + @ColsVal + '
				FROM 
				(
					SELECT 
						GMC.GMC_GAMINGTABLE_ID, 
						MAX(GTT.GTT_NAME) GT_TYPE,
						MAX(GT.GT_NAME) GT_NAME,
						YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
						MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
						DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
						CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_ENABLED = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
						CONVERT(INT,GMC.GMC_USED) GMC_USED,
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_USED  = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
						FROM GAMING_TABLES_CONNECTED AS GMC
							INNER JOIN GAMING_TABLES GT
								ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
							INNER JOIN GAMING_TABLES_TYPES GTT
								ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
						WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
						GROUP BY GMC.GMC_GAMINGTABLE_ID , YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
								,GMC.GMC_ENABLED, GMC.GMC_USED
				) AS SourceTable
				PIVOT
				(
					MAX(GMC_ENABLED) FOR GMN_DAY IN (' + @Cols + ')
				) AS PivotTable
			) A 
			INNER JOIN
			( 
				SELECT 
						GMC_GAMINGTABLE_ID , 
						GT_TYPE,
						GT_NAME,
						CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
						T_DAYS,
						T_DAYSUSE,
						' + @ColsValU + '
					FROM 
					(
						SELECT 
							GMC.GMC_GAMINGTABLE_ID, 
							MAX(GTT.GTT_NAME) GT_TYPE,
							MAX(GT.GT_NAME) GT_NAME,
							YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
							MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
							DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
							CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_ENABLED = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
							CONVERT(INT,GMC.GMC_USED) GMC_USED,
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_USED  = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
							FROM GAMING_TABLES_CONNECTED AS GMC
								INNER JOIN GAMING_TABLES GT
									ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
								INNER JOIN GAMING_TABLES_TYPES GTT
									ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
							WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
							GROUP BY GMC.GMC_GAMINGTABLE_ID , YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
									,GMC.GMC_ENABLED, GMC.GMC_USED
					) AS SourceTable
					PIVOT
					(
						MAX(GMC_USED) FOR GMN_DAY IN (' + @Cols + ')
					) AS PivotTable
			) B
			ON B.gmc_gamingtable_id = A.gmc_gamingtable_id
			AND B.GMC_MONTH = A.GMC_MONTH

            '     

EXEC SP_EXECUTESQL @Query

END

GO

GRANT EXECUTE ON [sp_GetTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetTablesActivityFormatted]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetTablesActivityFormatted]
GO

CREATE PROCEDURE [dbo].[sp_GetTablesActivityFormatted]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @Date AS DATETIME
DECLARE @Count INT
DECLARE @ColsVal VARCHAR(MAX)
DECLARE @ColsValU VARCHAR(MAX)
DECLARE @ColsEnabled VARCHAR(MAX)
DECLARE @ColsUsed VARCHAR(MAX)
DECLARE @ColsEnabledSum VARCHAR(MAX)
DECLARE @ColsUsedSum VARCHAR(MAX)

SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(4),@pYear) + '-' + CONVERT(VARCHAR(2),@pMonth) + '-' +  '1')
SELECT @Count = 0
SELECT @ColsVal = ''
SELECT @ColsValU = ''
SELECT @ColsEnabled = ''
SELECT @ColsUsed = ''
SELECT @ColsEnabledSum = ''
SELECT @ColsUsedSum = ''

WHILE @Count < DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,@Date),0)))
BEGIN

   SELECT @Count = @Count + 1
   IF @Count <> 1
    BEGIN
		SELECT @ColsVal = @ColsVal + ','
		SELECT @ColsValU = @ColsValU + ','
		SELECT @ColsEnabled = @ColsEnabled + ','
		SELECT @ColsUsed = @ColsUsed + ','
		SELECT @ColsEnabledSum = @ColsEnabledSum + ','
		SELECT @ColsUsedSum = @ColsUsedSum + ','
	END
	
   SELECT @ColsVal = @ColsVal + ' T_D' + CONVERT(VARCHAR,@Count) + ' INT Default 0'
   SELECT @ColsValU = @ColsValU + ' T_DU' + CONVERT(VARCHAR,@Count) + ' INT Default 0'
   SELECT @ColsEnabled = @ColsEnabled + 'T_D' + CONVERT(VARCHAR,@Count) + ' ''' + CONVERT(VARCHAR,@Count) +  ''''
   SELECT @ColsUsed = @ColsUsed + 'T_DU' + CONVERT(VARCHAR,@Count) + ' ''' + CONVERT(VARCHAR,@Count) +  ' '''
   SELECT @ColsEnabledSum = @ColsEnabledSum + 'ISNULL(SUM(T_D' + CONVERT(VARCHAR,@Count) + '),0) ' + ''
   SELECT @ColsUsedSum = @ColsUsedSum + 'ISNULL(SUM(T_DU' + CONVERT(VARCHAR,@Count) + '),0) ' +  ''
END

DECLARE @Query NVARCHAR(MAX)
SET @Query = '  

		IF OBJECT_ID(''tempdb..#TableMetricsHeader'') IS NOT NULL
			DROP TABLE #TableMetricsHeader

			CREATE TABLE #TableMetricsHeader
			(
				t_Id		INT,
				t_Name		VARCHAR(50),
				t_Type		VARCHAR(50),
				t_Month		INT Default 0,
				t_Days		INT Default 0,
				' + @ColsVal + ',
				t_DaysUse	INT Default 0,
				' + @ColsValU + '
			)

			INSERT INTO #TableMetricsHeader
			EXEC [dbo].[sp_GetTablesActivity] ''' + CONVERT(VARCHAR(10),@Date,112) + '''

			SELECT		t_Type ''TIPO'',
						t_Name ''MESA'',
						t_Month	''MES'',
						t_Days ''DIAS HABILITADA'',
						' + @ColsEnabled + ',	   
						t_DaysUse ''DIAS USADO'',
						' + @ColsUsed + '	   
			FROM #TableMetricsHeader
			UNION ALL
			SELECT	''TOTAL'',						
						''' + cast(@pYear as nvarchar(4)) + right('00'+cast( @pMonth as nvarchar(2)), 2) +''',						
						ISNULL(SUM(t_Month),0),
						ISNULL(SUM(t_Days),0),
						' + @ColsEnabledSum + ',	   
						ISNULL(SUM(t_DaysUse),0),
						' + @ColsUsedSum + '	   
			FROM #TableMetricsHeader

	'

EXEC SP_EXECUTESQL @Query

END


GRANT EXECUTE ON [dbo].[sp_GetTablesActivityFormatted] TO [WGGUI] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO

CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
  DECLARE @SelectPartToExecute INT -- 1:HP,  2:CM,  3:Both
      
  SET @_PAID = 32768
  
  SET @SelectPartToExecute = 3 

  IF @pHpMovements IS NOT NULL AND @pCmMovements IS NULL
  BEGIN
     SET @SelectPartToExecute = 2
  END
  ELSE IF @pHpMovements IS NULL AND @pCmMovements IS NOT NULL
  BEGIN
     SET @SelectPartToExecute = 1
  END
     
  
  SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
' + CHAR(10) 

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 2
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , HP_TYPE                   AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   HANDPAYS          ON HP_OPERATION_ID  = AO_OPERATION_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
--------      + CASE WHEN @pWithTaxes = 1 THEN ' AND HP_TAX_AMOUNT > 0' ELSE ' AND HP_TAX_AMOUNT = 0' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + CHAR(10) 
END


IF @SelectPartToExecute = 3
BEGIN
  SET @_SQL = @_SQL + ' UNION ' + CHAR(10)
END

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 1
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           - TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID     AS BEFORE_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
----     + CASE WHEN @pWithTaxes = 1 THEN ' AND CM_ADD_AMOUNT > 0' ELSE ' AND CM_ADD_AMOUNT = 0' END + CHAR(10) 
END
 
SET @_SQL = @_SQL + ' ORDER   BY DATETIME DESC ' + CHAR(10) 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL  
   
END  

GO
GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO



IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @nRows AS INT                              
 DECLARE @index AS INT                              
 DECLARE @tDays AS TABLE(NumDay INT)                       
 DECLARE @p2007Opening AS VARCHAR(MAX)

 SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
 SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
 
 SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @nRows                           
 BEGIN	                                          
   INSERT INTO @tDays VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT NumDay INTO #TempTable_Days FROM @tDays 
 
 SET @Sql = '
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PS_FINISHED
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID  
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_FROM < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '

EXECUTE (@Sql)

DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDualCurrencyAmount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetDualCurrencyAmount]
GO

CREATE FUNCTION [dbo].[GetDualCurrencyAmount]
(@pAmount  MONEY,
 @pAmt0    MONEY,
 @pPreferAmt INT)
RETURNS MONEY
AS
BEGIN

  DECLARE @Amount MONEY

  SET @Amount = @pAmount;
      
  IF (@pAmt0 IS NOT NULL AND @pPreferAmt = 1)
  BEGIN
     SET @Amount = @pAmt0;
  END
     
  RETURN @Amount
  
END -- GetDualCurrencyAmount
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetDualCurrencyAmount] TO [wggui] WITH GRANT OPTION
GO

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS JACKPOT_DE_SALA  '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
  SET @Sql =  @Sql +
  '                 , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 1
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO

IF OBJECT_ID (N'dbo.GetHandpaysBalanceReport', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GetHandpaysBalanceReport                 
GO    
CREATE PROCEDURE [dbo].[GetHandpaysBalanceReport]
( @pFromDt            DATETIME   
  ,@pToDt             DATETIME    
  ,@pClosingTime      INTEGER
  ,@pTerminalWhere    NVARCHAR(MAX)
  ,@pWithOutActivity  BIT
  ,@pOnlyUnbalance    BIT
)
AS
BEGIN
  DECLARE @index        AS INT
  DECLARE @nRows        AS INT
  DECLARE @p2007Opening AS DATETIME
  
  DECLARE @c2007Opening AS CHAR(20)
  DECLARE @cFromDt      AS CHAR(20)
  DECLARE @cToDt        AS CHAR(20)
  DECLARE @query        AS VARCHAR(MAX)
  DECLARE @apos         AS CHAR(1)
  DECLARE @query_day    AS VARCHAR(MAX)
  DECLARE @query_where  AS VARCHAR(MAX)
  
  SET @index = 0   
  SET @query_day = ''
  SET @query_where = ''
  
  SET @p2007Opening  = CAST('2007-01-01 00:00:00' AS DATETIME)
  SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
  
  SET @c2007Opening = CONVERT(CHAR(20),@p2007Opening,120)
  SET @cFromDt = CONVERT(CHAR(20),@pFromDt,120)
  SET @cToDt = CONVERT(CHAR(20),@pToDt,120)
  
  SET @apos = CHAR(39)
  SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt) + 1 
  
  
  WHILE @index < @nRows                           
  BEGIN
    SET @query_day = @query_day + ' UNION SELECT ' + LTRIM(RTRIM(CAST(@index AS CHAR(5)))) + ' AS ORDER_DATE '                                         
    SET @index = @index + 1                       
  END

  SET @query_day = ' SELECT DATEADD(DAY, ORDER_DATE, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)) AS ORDER_DATE 
                     FROM (' + SUBSTRING(@query_day,8, LEN(@query_day)) + ') TABLE_DAYS '
           
           
  IF @pWithOutActivity = 1
    BEGIN
      SET @query_where = ' AND (BALANCE_HANDPAYS.SYSTEM_HANDPAYS_TOTAL <> 0 OR BALANCE_HANDPAYS.COUNTERS_HANDPAYS_TOTAL <> 0) '
    END          
         
  IF @pOnlyUnbalance = 1
    BEGIN
      SET @query_where = @query_where + ' AND BALANCE_HANDPAYS.DIFFERENCE <> 0 '
    END
    
    
  IF @query_where <> ''
    BEGIN
      SET @query_where = ' WHERE ' + SUBSTRING(@query_where, 6, LEN(@query_where))
    END 


  SET @query = ' SELECT * FROM (
                                SELECT  TE_PROVIDER_ID, 
                                        TE_TERMINAL_ID, 
                                        TE_NAME, 
                                        ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') AS DENOMINATION, 
                                        
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) AS SYSTEM_CREDIT_CANCEL, 
                                        ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_HANDPAYS_TOTAL,

                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) AS COUNTERS_CREDIT_CANCEL,
                                        ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_JACKPOTS,
                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_HANDPAYS_TOTAL,
                                        
                                        (ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0)) -
                                        (ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0)) AS DIFFERENCE
                                        
                                FROM   TERMINALS 
                                LEFT JOIN (' + LTRIM(RTRIM(@query_day)) + '
                                          ) DIA ON ORDER_DATE <= GETDATE() 
                                LEFT JOIN (SELECT TC_TERMINAL_ID, 
                                                  DATEADD(HOUR, 10, TC_DATE) AS TC_DATE 
                                           FROM   TERMINALS_CONNECTED 
                                           WHERE  TC_DATE >= DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_DATE < DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cToDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_STATUS = 0 
                                                  AND TC_CONNECTED = 1
                                          ) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT HP_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS HP_DATETIME, 
                                                  SUM(CASE WHEN HP_TYPE IN (0, 1000) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32)) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN 0 
                                                                     ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) - 
                                                                          ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS CREDIT_CANCEL, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32) AND HP_PROGRESSIVE_ID IS NOT NULL) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS PROGRESIVES, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL < 1 OR HP_LEVEL > 32 OR HP_LEVEL IS NULL)) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                           ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32 AND HP_PROGRESSIVE_ID IS NULL)) 
                                                           THEN CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0)
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS NO_PROGRESIVES 
                                           FROM HANDPAYS WITH (INDEX (IX_HP_STATUS_CHANGED)) 
                                           WHERE  HP_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND HP_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                                  AND HP_TYPE <> 20 
                                          GROUP BY HP_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT TSMH_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS TSMH_DATE, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 2 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_JACKPOT_CREDITS, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 3 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS 
                                           FROM   TERMINAL_SAS_METERS_HISTORY 
                                           WHERE  TSMH_TYPE = 1 
                                                  AND TSMH_METER_CODE IN (0, 1, 2, 3 ) 
                                                  AND TSMH_METER_INCREMENT > 0 
                                                  AND TSMH_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND TSMH_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                           GROUP BY TSMH_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE 
                                WHERE (HAND_PAYS.CREDIT_CANCEL IS NOT NULL 
                                        OR HAND_PAYS.PROGRESIVES IS NOT NULL 
                                        OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL 
                                        OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL 
                                        OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL 
                                        OR TC_DATE IS NOT NULL ) ' + LTRIM(RTRIM(@pTerminalWhere)) + '
                                GROUP BY  TE_PROVIDER_ID, 
                                          TE_TERMINAL_ID, 
                                          TE_NAME, 
                                          ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') 
                               )  BALANCE_HANDPAYS ' + @query_where + '                                                                
                 ORDER BY BALANCE_HANDPAYS.TE_PROVIDER_ID, 
                          BALANCE_HANDPAYS.TE_TERMINAL_ID, 
                          BALANCE_HANDPAYS.TE_NAME, 
                          BALANCE_HANDPAYS.DENOMINATION '

  EXECUTE (@query)                          
END 
GO
-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetHandpaysBalanceReport] TO [wggui] WITH GRANT OPTION
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

	DECLARE @CurrentDay AS DATETIME
	SELECT @CurrentDay = [dbo].[TodayOpening] (1)

	SET NOCOUNT ON;

	IF NOT EXISTS (
	SELECT GMC_GAMING_DAY 
			FROM GAMING_TABLES_CONNECTED GTC 
			WHERE GTC.GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
	)
	BEGIN
		-- INSERT
		
		INSERT INTO GAMING_TABLES_CONNECTED
		SELECT 
		S.GT_GAMING_TABLE_ID, 
		S.GT_DATE, 
		SUM(S.GTS_ENABLED) GTS_ENABLED, 
		SUM(S.GTS_CASHIER_SESSION_ID) GTS_CASHIER_SESSION_ID
		FROM
		(
			SELECT
			GT.GT_GAMING_TABLE_ID, 
			CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) GT_DATE,
			CASE WHEN GTS.GTS_CASHIER_SESSION_ID IS NOT NULL OR GT.GT_ENABLED = 1 THEN 1 ELSE 0 END GTS_ENABLED,
			CASE WHEN GTS.GTS_CASHIER_SESSION_ID IS NOT NULL THEN 1 ELSE 0 END GTS_CASHIER_SESSION_ID
			FROM GAMING_TABLES GT
				LEFT JOIN(SELECT GTS_GAMING_TABLE_ID, MIN(GTS.GTS_CASHIER_SESSION_ID) AS GTS_CASHIER_SESSION_ID
									FROM GAMING_TABLES_SESSIONS GTS
										INNER JOIN CASHIER_SESSIONS CS
											ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
											AND (
											       (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
											    OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
											    OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
											    OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
											     )
									GROUP BY GTS_GAMING_TABLE_ID) AS GTS
						ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
		)S
		GROUP BY S.GT_GAMING_TABLE_ID, S.GT_DATE

	END
	ELSE
	BEGIN
		--UPDATE
		
		UPDATE GAMING_TABLES_CONNECTED
		SET GMC_ENABLED = S1.GTS_ENABLED,
			GMC_USED = S1.GTS_CASHIER_SESSION_ID
		FROM 
		(
			SELECT 
			S.GT_GAMING_TABLE_ID, 
			S.GT_DATE, 
			SUM(S.GTS_ENABLED) GTS_ENABLED, 
			SUM(S.GTS_CASHIER_SESSION_ID) GTS_CASHIER_SESSION_ID 
			FROM
			(
				SELECT
				GT.GT_GAMING_TABLE_ID, 
				CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) GT_DATE,
				CASE WHEN GTS.GTS_CASHIER_SESSION_ID IS NOT NULL OR GT.GT_ENABLED = 1 THEN 1 ELSE 0 END gts_enabled,
				CASE WHEN GTS.GTS_CASHIER_SESSION_ID IS NOT NULL THEN 1 ELSE 0 END GTS_CASHIER_SESSION_ID
				FROM GAMING_TABLES GT
					LEFT JOIN(SELECT GTS_GAMING_TABLE_ID, MIN(GTS.GTS_CASHIER_SESSION_ID) AS GTS_CASHIER_SESSION_ID
									FROM GAMING_TABLES_SESSIONS GTS
										INNER JOIN CASHIER_SESSIONS CS
											ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
											AND (
											       (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
											    OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
											    OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
											    OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
											     )
									GROUP BY GTS_GAMING_TABLE_ID) AS GTS
						ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
			)S
			GROUP BY S.GT_GAMING_TABLE_ID, S.GT_DATE
		) S1
		WHERE S1.GT_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID
		AND CONVERT(VARCHAR(10),S1.GT_GAMING_TABLE_ID) + '-' + CONVERT(VARCHAR(10),S1.GT_DATE,112) = 
			CONVERT(VARCHAR(10),GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID) + '-' + CONVERT(VARCHAR(10),GAMING_TABLES_CONNECTED.GMC_GAMING_DAY,112)

	END

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
	@pGamingDayMode BIT
    
AS 
BEGIN
  --GUI USERS  
  SELECT   GUI_USERS.GU_USER_ID     as  USERID   
      , GU_USER_TYPE                as  TYPE_USER  
      , GUI_USERS.GU_USERNAME       as  USERNAME   
      , (CASE 
          WHEN GU_USER_TYPE = 0
            THEN GUI_USERS.GU_FULL_NAME 
            ELSE CT_NAME 
         END)                       as  FULLNAME   
      , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE   
      , X.CM_CURRENCY_ISO_CODE      as  CURRENCY   
      , X.FALTANTE                  as  FALTANTE   
      , X.SOBRANTE                  as  SOBRANTE   
      ,(X.SOBRANTE - X.FALTANTE)    as  DIFERENCIA  
      , X.CS_CASHIER_ID             as  CASHIERID
      , CT_NAME                     as  CASHIERNAME
      FROM  
      (          
      SELECT   CS_USER_ID  
             , CM_CURRENCY_ISO_CODE  
             , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE  
             , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE               
             , CS_CASHIER_ID
        FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))  
  INNER JOIN   CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID      
       WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END >= @pDateFrom    
         AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END < @pDateTo  
         AND CM_TYPE IN ( 160  -- CASH_CLOSING_SHORT  
                        , 161)  -- CASH_CLOSING_OVER  
   GROUP BY CS_USER_ID  
        , CM_CURRENCY_ISO_CODE
        , CS_CASHIER_ID  
    ) X  
           
  INNER JOIN  GUI_USERS  
          ON  GU_USER_ID = X.CS_USER_ID
         AND (X.FALTANTE <>0  OR X.SOBRANTE<>0)  
         AND (GU_USER_TYPE=0  OR GU_USER_TYPE=6)
   LEFT JOIN  CASHIER_TERMINALS
          ON  CT_CASHIER_ID = X.CS_CASHIER_ID

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
           , 0                                                as  CASHIERID
           , ''                                               as  CASHIERNAME
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE)) 
      INNER JOIN   cashier_sessions 
              ON   MB1.mbm_cashier_session_id = cs_session_id          
          
           WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END >= @pDateFrom   
             AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END < @pDateTo   
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB2.MBM_DATETIME END < @pDateTo 
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
            AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END

GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION

GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'CloseSessionInNegative')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'CloseSessionInNegative', '0')
GO
--------JOB

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'jb_GamingTablesActivity')
EXEC msdb.dbo.sp_delete_job @job_name = N'jb_GamingTablesActivity' , @delete_unused_schedule=1
GO

BEGIN TRANSACTION

DECLARE @LOGIN VARCHAR(50)
DECLARE @DB VARCHAR(50)

SET @LOGIN = N'sa'
SET @DB = N'wgdb_000'

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 05/06/2016 10:43:45 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'jb_GamingTablesActivity', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Task created to execute sp_GenerateTablesActivity in order to populate gaming_tables_connected table', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=@LOGIN, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [jb_step_mesas_execution]    Script Date: 05/06/2016 10:43:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'jb_step_tables_execution', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.sp_GenerateTablesActivity', 
		@database_name=@DB, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'jb_tables_schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160506, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

-- Field Creation
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_tito_host_id')
  ALTER TABLE [dbo].[terminals] ADD te_tito_host_id INT NOT NULL DEFAULT 0;
GO

-- Field Update
DECLARE @_is_tito AS INT
SELECT @_is_tito = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND  GP_SUBJECT_KEY = 'TITOMode'

UPDATE TERMINALS 
   SET TE_TITO_HOST_ID = 0

IF (@_is_tito = 1)
UPDATE TERMINALS 
   SET TE_TITO_HOST_ID = CAST(TE_BASE_NAME AS INT) % 10000
 WHERE TE_TITO_HOST_ID = 0 
   AND TE_BASE_NAME IS NOT NULL
   AND ISNUMERIC (TE_BASE_NAME) = 1

IF (@_is_tito = 1)
UPDATE TERMINALS 
   SET TE_TITO_HOST_ID = CAST(TE_FLOOR_ID AS INT) % 10000
 WHERE TE_TITO_HOST_ID = 0 
   AND TE_FLOOR_ID IS NOT NULL
   AND ISNUMERIC (TE_FLOOR_ID) = 1 
GO