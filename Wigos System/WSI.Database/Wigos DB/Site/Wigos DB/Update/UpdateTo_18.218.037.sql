/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 217;

SET @New_ReleaseId = 218;
SET @New_ScriptName = N'UpdateTo_18.218.037.sql';
SET @New_Description = N'New features: Card replacement - Player tracking GT';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/**** CARD REPLACEMENT ****/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_card_replacement_count')
  ALTER TABLE dbo.accounts ADD ac_card_replacement_count int NULL
GO

/**** PLAYER TRACKING GT ****/

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_playertracking_session')
BEGIN
  CREATE TABLE [dbo].[gt_playertracking_session](
	  [gtpt_playertracking_session_id] [bigint] NOT NULL,
	  [gtpt_user_id] [bigint] NOT NULL,
	  [gtpt_gaming_table_id] [int] NOT NULL,
	  [gtpt_started] [datetime] NOT NULL,
	  [gtpt_finished] [datetime] NULL,
	  [gtpt_status] [int] NULL,
   CONSTRAINT [PK_gt_playertracking_session] PRIMARY KEY CLUSTERED 
  (
	  [gtpt_playertracking_session_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table gt_playertracking_session already exists *****';
GO

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_play_sessions')
BEGIN
  CREATE TABLE [dbo].[gt_play_sessions](
	  [gtps_play_session_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [gtps_gaming_table_id] [int] NOT NULL,
	  [gtps_started] [datetime] NOT NULL,
	  [gtps_finished] [datetime] NULL,
	  [gtps_account_id] [bigint] NOT NULL,
	  [gtps_seat_id] [int] NOT NULL,
	  [gtps_status] [int] NOT NULL,
	  [gtps_plays_count] [int] NULL,
	  [gtps_played_amount] [money] NOT NULL,
	  [gtps_played_max] [money] NOT NULL,
	  [gtps_played_min] [money] NOT NULL,
	  [gtps_played_avarage] [money] NOT NULL,
	  [gtps_won_amount] [money] NOT NULL,
	  [gtps_avarage_won] [money] NOT NULL,
	  [gtps_start_walk] [datetime] NULL,
	  [gtps_walk] [bigint] NULL,
	  [gtps_player_skill] [int] NULL,
	  [gtps_player_speed] [int] NULL,
	  [gtps_points] [money] NULL,
	  [gtps_computed_points] [money] NULL,
	  [gtps_finished_user_id] [int] NULL,
	  [gtps_updated_user_id] [int] NULL,
	  [gtps_game_time] [bigint] NULL,
	  [gtps_timestamp] [timestamp] NOT NULL,
   CONSTRAINT [PK_gt_play_sessions] PRIMARY KEY CLUSTERED 
  (
	  [gtps_play_session_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table gt_play_sessions already exists *****';
GO

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_seats')
BEGIN
  CREATE TABLE [dbo].[gt_seats](
	  [gts_seat_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [gts_gaming_table_id] [int] NOT NULL,
	  [gts_enabled] [bit] NOT NULL,
	  [gts_terminal_id] [int] NOT NULL,
	  [gts_account_id] [bigint] NOT NULL,
	  [gts_enabled_date] [datetime] NULL,
	  [gts_disabled_date] [datetime] NULL,
	  [gts_position] [int] NULL,
   CONSTRAINT [PK_gt_seats] PRIMARY KEY CLUSTERED 
  (
	  [gts_seat_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table gt_seats already exists *****';
GO

IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_play_movements')
BEGIN
  CREATE TABLE [dbo].[gt_play_movements](
	  [gtpm_play_movement_id] [bigint] NOT NULL,
	  [gtps_pt_session_id] [bigint] NULL,
	  [gtpm_play_session_id] [bigint] NOT NULL,
	  [gtpm_account_id] [bigint] NOT NULL,
	  [gtpm_gaming_table_id] [bigint] NOT NULL,
	  [gtpm_played_amount] [money] NOT NULL,
	  [gtpm_won_amount] [money] NOT NULL,
   CONSTRAINT [PK_gt_play_movements] PRIMARY KEY CLUSTERED 
  (
	  [gtpm_play_movement_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table gt_play_movements already exists *****';
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_playertracking_movements]') AND type in (N'U'))
  DROP TABLE [dbo].[gt_playertracking_movements]
GO

CREATE TABLE [dbo].[gt_playertracking_movements](
	[gtpm_movement_id] [bigint] NOT NULL,
	[gtpm_type] [int] NOT NULL,
	[gtpm_datetime] [datetime] NOT NULL,
	[gtpm_user_id] [int] NOT NULL,
	[gtpm_cashier_id] [bigint] NOT NULL,
	[gtpm_gaming_table_id] [int] NOT NULL,
	[gtpm_seat_id] [int] NOT NULL,
	[gtpm_play_session_id] [bigint] NOT NULL,
	[gtpm_terminal_id] [bigint] NULL,
	[gtpm_account_id] [bigint] NULL,
	[gtpm_old_value] [money] NULL,
	[gtpm_value] [money] NOT NULL,
	[gtpm_nls_desc] [nvarchar](200) NULL,
	[gtpm_acct_movement_id] [bigint] NULL,
 CONSTRAINT [PK_gt_playertracking_movements] PRIMARY KEY CLUSTERED 
(
	[gtpm_movement_id] ASC,
	[gtpm_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_gt_playertracking_movements_gtpm_datetime] ON [dbo].[gt_playertracking_movements] 
(
	[gtpm_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

/***** ALTER GAMING_TABLES *****/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_created')
  ALTER TABLE dbo.gaming_tables ADD [gt_created] [datetime] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_pt_session_id')
  ALTER TABLE dbo.gaming_tables ADD [gt_pt_session_id] [bigint] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_table_speed')
  ALTER TABLE dbo.gaming_tables ADD [gt_table_speed] [bigint] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_num_seats')
  ALTER TABLE dbo.gaming_tables ADD [gt_num_seats] [bigint] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_bet_min')
  ALTER TABLE dbo.gaming_tables ADD [gt_bet_min] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_bet_max')
  ALTER TABLE dbo.gaming_tables ADD [gt_bet_max] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_design_xml')
  ALTER TABLE dbo.gaming_tables ADD [gt_design_xml] [xml] NULL
GO

ALTER TABLE dbo.gaming_tables ALTER COLUMN  [gt_num_seats] [int] NULL
GO

/******* INDEXES *******/

/**** PLAYER TRACKING GT ****/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_finished')
DROP INDEX [IX_gtps_account_id_finished] ON [dbo].[gt_play_sessions] WITH ( ONLINE = OFF )
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_finished')
CREATE NONCLUSTERED INDEX [IX_gtps_account_id_finished] ON [dbo].[gt_play_sessions] 
(
	[gtps_finished] ASC,
	[gtps_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_started')
DROP INDEX [IX_gtps_account_id_started] ON [dbo].[gt_play_sessions] WITH ( ONLINE = OFF )
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_started')
CREATE NONCLUSTERED INDEX [IX_gtps_account_id_started] ON [dbo].[gt_play_sessions] 
(
	[gtps_started] ASC,
	[gtps_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_finished_status')
DROP INDEX [IX_gtps_finished_status] ON [dbo].[gt_play_sessions] WITH ( ONLINE = OFF )
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_finished_status')
CREATE NONCLUSTERED INDEX [IX_gtps_finished_status] ON [dbo].[gt_play_sessions] 
(
	[gtps_finished] ASC,
	[gtps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_started_status')
DROP INDEX [IX_gtps_started_status] ON [dbo].[gt_play_sessions] WITH ( ONLINE = OFF )
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_started_status')
CREATE NONCLUSTERED INDEX [IX_gtps_started_status] ON [dbo].[gt_play_sessions] 
(
	[gtps_started] ASC,
	[gtps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_timestamp')
DROP INDEX [IX_gtps_timestamp] ON [dbo].[gt_play_sessions] WITH ( ONLINE = OFF )
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_timestamp')
CREATE NONCLUSTERED INDEX [IX_gtps_timestamp] ON [dbo].[gt_play_sessions] 
(
	[gtps_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

/**** CARD REPLACEMENT ****/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'PersonalCardReplacement.ChargeAfterTimes')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'PersonalCardReplacement.ChargeAfterTimes', '0')
GO

/**** PLAYER TRACKING GT ****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY ='Seats.MaxAllowedSeats')
  INSERT INTO [dbo].[GENERAL_PARAMS]([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
                         VALUES      ('GamingTables', 'Seats.MaxAllowedSeats', '10')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='Enabled')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTable.PlayerTracking','Enabled'     ,'0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='DefaultTimeOfAHand')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTable.PlayerTracking','DefaultTimeOfAHand'     ,'300')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='PlayerTrackingSaveAutomatic')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTable.PlayerTracking','PlayerTrackingSaveAutomatic'     ,'1')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='ShowConfirmationExitMsg')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTable.PlayerTracking','ShowConfirmationExitMsg'     ,'1')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='ShowConfirmationChangePlayerMsg')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTable.PlayerTracking','ShowConfirmationChangePlayerMsg'     ,'1')
GO

/***** PLAYER TRACKING GT - INSERT TERMINAL_TYPES *****/
IF NOT EXISTS (SELECT * FROM TERMINAL_TYPES WHERE TT_TYPE = 107)
BEGIN
  INSERT INTO   TERMINAL_TYPES  ( TT_TYPE, TT_NAME)
						 VALUES ( 107    , 'GAMING TABLE SEAT')
END
GO

/*** CHIPS SALES WITH RECHARGE ***/
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTables' AND GP_SUBJECT_KEY='Cashier.IntegratedChipAndCreditOperations')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY                ,GP_SUBJECT_KEY,GP_KEY_VALUE) 
						 VALUES ('GamingTables','Cashier.IntegratedChipAndCreditOperations'     ,'0')
GO

/******* STORED PROCEDURES *******/
