/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 303;

SET @New_ReleaseId = 304;
SET @New_ScriptName = N'UpdateTo_18.304.039.sql';
SET @New_Description = N'Meters tables for Smartfloor';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[h_meters_definition]') and type in (N'u'))
  drop table [dbo].[h_meters_definition]
go

create table [dbo].[h_meters_definition](
	[hmd_meter_id] [int] not null,
	[hmd_meter_item] [int] not null,
	[hmd_meter_type] [int] not null,
	[hmd_description] [nvarchar](250) null,
	[hmd_meter_mask] [int] not null,
	[hmd_meter_enabled] [bit] not null,
	[hmd_meter_last_update] [int] not null,
	[hmd_meter_device] [int] not null,
	[hmd_visible] [bit] not null,
	[hmd_stored_name] [nvarchar](200) null,
	[hmd_meter_name] [nvarchar](100) null
) on [primary]
go

if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[h_pvh]') and type in (N'u'))
  drop table [dbo].[h_pvh]
go

create table [dbo].[h_pvh](
	[pvh_account_id] [bigint] not null,
	[pvh_date] [int] not null,
	[pvh_weekday] [tinyint] not null,
	[pvh_visit] [tinyint] null,
	[pvh_check_in] [datetime] null,
	[pvh_check_out] [datetime] null,
	[pvh_room_time] [int] null,
	[pvh_game_time] [int] null,
	[pvh_total_played_count] [int] null,
	[pvh_played_won_count] [int] null,
	[pvh_total_played] [money] null,
	[pvh_total_bet_avg] [money] null,
	[pvh_jackpots_won] [money] null,
	[pvh_total_won] [money] null,
	[pvh_re_played] [money] null,
	[pvh_nr_played] [money] null,
	[pvh_theorical_won] [money] null,
	[pvh_money_in] [money] null,
	[pvh_money_in_tax] [money] null,
	[pvh_money_out] [money] null,
	[pvh_re_won] [money] null,
	[pvh_nr_won] [money] null,
	[pvh_devolution] [money] null,
	[pvh_prize] [money] null,
	[pvh_tax1] [money] null,
	[pvh_tax2] [money] null,
	[pvh_tax3] [money] null,
	[pvh_money_in_count] [int] null,
	[pvh_money_out_count] [int] null,
	[pvh_age] [tinyint] null,
	[pvh_registered_num_days] [smallint] null,
	[pvh_level] [int] null,
	[pvh_points_balance] [bigint] null,
	[pvh_points_manual] [bigint] null,
	[pvh_points_won] [bigint] null,
	[pvh_points_redeemed] [bigint] null,
	[pvh_points_expired] [bigint] null,
 constraint [pk_h_pvh] primary key clustered 
(
	[pvh_account_id] asc,
	[pvh_date] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

go

/******* INDEXES *******/

/******* RECORDS *******/

INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (12, 0, 2, N'PLAYED', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PLAYED')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (1, 0, 2, N'BET AVG', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'BET_AVG')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 0, 3, N'OCC. GENDER', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', NULL)
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 1, 3, N'MALE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_MALE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 2, 3, N'FEMALE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_FEMALE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 3, 3, N'UNKNOWN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_UNKNOW')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 0, 3, N'OCC. PLAYER TYPE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', NULL)
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 1, 3, N'ANONYMOUSS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'ANONYMOUSS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 2, 3, N'REGISTERED_LEVEL_SILVER', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_SILVER')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 3, 3, N'REGISTERED_LEVEL_GOLD', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_GOLD')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (4, 0, 3, N'OCCUPANCY', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'OCCUPANCY')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (5, 0, 2, N'COIN IN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'COIN_IN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (6, 0, 3, N'NEW CLIENTS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'NEW_CLIENTS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (7, 0, 3, N'HIGH ROLLERS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'HIGH_ROLLERS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 0, 3, N'PROMOTIONS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (9, 0, 2, N'NET WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'NET_WIN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (10, 0, 2, N'THEORETICAL WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'THEORETICAL_WIN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (11, 0, 2, N'LOSS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'LOSS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (13, 0, 2, N'PRIZE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PRIZE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (14, 0, 2, N'PLAYED COUNT', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PLAYED_COUNT')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (15, 0, 2, N'DENOMINATION', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'DENOMINATION')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (16, 0, 2, N'MACHINE OCUPATTION', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'MACHINE_OCUPATTION')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 1, 3, N'PROMOTIONS_TOTAL', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS_TOTAL')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 2, 3, N'PROMOTIONS_GRANTED', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS_GRANTED')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 4, 3, N'REGISTERED_LEVEL_PLATINUM', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_PLATINUM')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 5, 3, N'REGISTERED_LEVEL_WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_WIN')
GO

/******* PROCEDURES *******/


