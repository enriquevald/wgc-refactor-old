/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 246;

SET @New_ReleaseId = 247;
SET @New_ScriptName = N'UpdateTo_18.247.037.sql';
SET @New_Description = N'SP modified ReportShortfallExcess.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGrouped_Read.sql
-- 
--   DESCRIPTION: Read historied cashier movements per working day
-- 
--        AUTHOR: JPJ
-- 
-- CREATION DATE: 02-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-DEC-2014 JPJ    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_Report_Per_Day]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Tax_Report_Per_Day]
GO

CREATE PROCEDURE [dbo].[Tax_Report_Per_Day]        
                 @pDateFrom DATETIME
               , @pDateTo   DATETIME       
  AS
BEGIN 

  DECLARE @CARD_DEPOSIT_IN                      INT
  DECLARE @CARD_REPLACEMENT                     INT
  DECLARE @CASH_IN_SPLIT2                       INT
  DECLARE @MB_CASH_IN_SPLIT2                    INT
  DECLARE @NA_CASH_IN_SPLIT2                    INT
  DECLARE @_DAY_VAR                             DATETIME 

  SET @CARD_DEPOSIT_IN                        =   9
  SET @CARD_REPLACEMENT                       =  28
  SET @CASH_IN_SPLIT2                         =  34
  SET @MB_CASH_IN_SPLIT2                      =  35
  SET @NA_CASH_IN_SPLIT2                      =  55

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TEMP_TABLE') AND type in (N'U'))
  BEGIN                                
    DROP TABLE #TEMP_TABLE  
  END       

  CREATE  TABLE #TEMP_TABLE ( TODAY   DATETIME PRIMARY KEY ) 
  
  IF @pDateTo IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @pDateTo = CAST(GETDATE() AS DATETIME)
  END

  -- TEMP TABLE IS FILLED WITH THE RANGE OF DATES
  SET @_DAY_VAR = @pDateFrom

  WHILE @_DAY_VAR < @pDateTo 
  BEGIN 
     INSERT INTO   #TEMP_TABLE (Today) VALUES (@_DAY_VAR)
     SET @_DAY_VAR =  DATEADD(Day,1,@_DAY_VAR)
  END 
  
  ;
  -- CASHIER MOVEMENTS FILTERED BY DATE AND MOVEMENT TYPES
  WITH MovementsPerWorkingday  AS 
   (
     SELECT   dbo.Opening(0, CM_DATE) 'WorkingDate'
            , CM_TYPE
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
      WHERE   (CM_DATE >= @pDateFrom AND (CM_DATE < @pDateTo))
              AND  CM_SUB_TYPE = 0
              AND  CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                @CASH_IN_SPLIT2, 
                                @NA_CASH_IN_SPLIT2,
                                @CARD_DEPOSIT_IN,
                                @CARD_REPLACEMENT )
  )

  SELECT   TimeRange.Today WorkingDate
         , ISNULL(((GrossCard - TaxCard) + (GrossCompanyB - TaxCompanyB)),0) BaseTotal
         , ISNULL((TaxCard + TaxCompanyB),0)       TaxTotal
         , ISNULL((GrossCard + GrossCompanyB),0)   GrossTotal
         , ISNULL((GrossCard - TaxCard),0)         BaseCard
         , ISNULL(TaxCard,0)                       TaxCard
         , ISNULL(GrossCard,0)                     GrossCard
         , ISNULL((GrossCompanyB - TaxCompanyB),0) BaseCompanyB         
         , ISNULL(TaxCompanyB,0)                   TaxCompanyB
         , ISNULL(GrossCompanyB,0)                 GrossCompanyB
  FROM (
           SELECT  WorkingDate
           ,
                   SUM(ISNULL(CASE WHEN CM_TYPE IN ( @CARD_DEPOSIT_IN,
                                                      @CARD_REPLACEMENT )  THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @CARD_DEPOSIT_IN,
                                                      @CARD_REPLACEMENT )  THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCompanyB'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCompanyB'
           FROM MovementsPerWorkingday 
           GROUP BY WorkingDate
         ) MOVEMENTS RIGHT JOIN #TEMP_TABLE TimeRange on TimeRange.Today = MOVEMENTS.WorkingDate
   ORDER BY TimeRange.Today ASC       
   
   DROP TABLE #TEMP_TABLE
END
GO

GRANT EXECUTE ON [dbo].[Tax_Report_Per_Day] TO [wggui] WITH GRANT OPTION
GO
