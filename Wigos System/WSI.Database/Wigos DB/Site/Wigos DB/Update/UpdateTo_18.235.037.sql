/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 234;

SET @New_ReleaseId = 235;
SET @New_ScriptName = N'UpdateTo_18.235.037.sql';
SET @New_Description = N'Added regional data, new GPs for progressives, new GPs for specific cashier voucher footer.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_progressive_paid_meter') 
   ALTER TABLE dbo.terminals DROP CONSTRAINT DF_terminals_te_progressive_paid_meter
   ALTER TABLE dbo.terminals DROP COLUMN te_progressive_paid_meter
GO

/******* INDEXES *******/

/******* RECORDS *******/

-- GENERAL PARAM
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'State.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','State.Name','')

-- PANAM� PROVINCES
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bocas del Toro','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cocl�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Col�n','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chiriqu�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dari�n','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Herrera','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Los Santos','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam� Oeste','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Veraguas','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guna Yala','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ember�-Wounaan','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ng�be-Bugl�','PA')

-- PANAM� OCCUPATIONS
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'PA')

-- PANAM� IDENTIFICATION_TYPES
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (30,1,100,'Otro','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (31,1,1,'Pasaporte','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (32,1,2,'C�dula de identidad','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (33,1,3,'Licencia de conducir','PA')

GO

-- PROGRESSIVES
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY = 'Provision.InputMode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Progressives','Provision.InputMode','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY = 'Provision.DistributionMode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Progressives','Provision.DistributionMode','2')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY = 'Provision.GoesToCage')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Progressives','Provision.GoesToCage','1')
GO

-- SPECIFIC CASHIER VOUCHER FOOTER
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'CashOpening.Footer') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','CashOpening.Footer','')

IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'CashClosing.Footer') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','CashClosing.Footer','')
           
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'MobileBank.SalesLimitChange.Footer') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','MobileBank.SalesLimitChange.Footer','')
           
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'MobileBank.Deposit.Footer') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','MobileBank.Deposit.Footer','')
           
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'MobileBank.Closing.Footer') 
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','MobileBank.Closing.Footer','')
GO

/******* STORED PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateSystemMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageUpdateSystemMeters]
GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
        @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                     FROM CAGE_MOVEMENTS
                                   WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                      AND CGM_TYPE IN (0, 1, 4, 5)
                               ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                FROM CAGE_MOVEMENTS
                               WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                 AND CGM_TYPE IN (100, 101, 103, 104)
                          ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                               WHERE CGS_CLOSE_DATETIME IS NULL
                          ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                             ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
            -- FEDERAL TAX --
  
            SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (142, 6) 
             AND CM_SESSION_ID = @pCashierSessionId

            SET @Tax1 = ISNULL(@Tax1, 0)
            
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax1,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 3,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
    
     
            -- STATE TAX --
   
            SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
             WHERE CM_TYPE IN (146, 14) 
               AND CM_SESSION_ID = @pCashierSessionId 
          
          SET @Tax2 = ISNULL(@Tax2, 0)
         
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax2,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 2,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
          
            ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    


            SELECT @CardRefundable = GP_KEY_VALUE 
              FROM GENERAL_PARAMS  
             WHERE GP_GROUP_KEY = 'Cashier'                             
               AND GP_SUBJECT_KEY = 'CardRefundable'                    

            IF ISNULL(@CardRefundable, 0) = 1                                                                        
            BEGIN                                                                                                   
                  SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 9 THEN CM_ADD_AMOUNT   
                                                              ELSE -1 * CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
               WHERE CM_TYPE IN (9, 10)                                             
                 AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                                                       FROM CAGE_MOVEMENTS                                                                     
                                                    WHERE CGM_CAGE_SESSION_ID = @pCashierSessionId)
                                                    
                  SET @CardDeposit = ISNULL(@CardDeposit, 0)
            
                  EXEC [dbo].[CageUpdateMeter]
                        @pValueIn = @CardDeposit,
                        @pValueOut = 0,
                        @pSourceTagetId = 0,
                        @pConceptId = 5,
                        @pIsoCode = @NationalCurrency,
                        @pOperation = 1,
                        @pSessionId = @CageSessionId,
                        @pSessiongetMode = 0
                     
            END                                                                                                     

      END                                      

END -- CageUpdateSystemMeters


GO

GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO
