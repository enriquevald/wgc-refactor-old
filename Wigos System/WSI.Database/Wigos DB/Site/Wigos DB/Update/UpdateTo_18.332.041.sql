/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 331;

SET @New_ReleaseId = 332;
SET @New_ScriptName = N'UpdateTo_18.332.041.sql';
SET @New_Description = N'ExternalPaymentAndSaleValidation';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='ExternalPaymentAndSaleValidation' AND GP_SUBJECT_KEY = 'TimeOutInMilliseconds')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalPaymentAndSaleValidation', 'TimeOutInMilliseconds', 10000)
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data_Player_Tracking]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
(
    @pBaseType            INTEGER
  , @pTimeInterval        INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pOnlyActivity        INTEGER
  , @pOrderBy             INTEGER
  , @pValidTypes          VARCHAR(4096)
  , @pSelectedCurrency    NVARCHAR(3)
)
AS
BEGIN
-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
   DECLARE @_SELECTED_CURRENCY AS  NVARCHAR(3)

----------------------------------------------------------------------------------------------------------------

-- Initialization --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','
SET @_SELECTED_CURRENCY =  @pSelectedCurrency

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 0, CAST(@_DATE_FROM AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END

END

-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         , SUM(X.BUY_IN) AS BUY_IN
         , SUM(X.CHIPS_IN) AS CHIPS_IN
         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
              THEN 0
              ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100)
           END AS HOLD
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
              THEN 0
              ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
           END AS PAYOUT
         , SUM(X.NETWIN) AS NETWIN
         , X.ISO_CODE

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
            -- CORE QUERY
            SELECT   CASE
                        WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                            DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                        WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                     END AS CM_DATE_ONLY
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER     -- GET THE BASE TYPE IDENTIFIER
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME                                         -- GET THE BASE TYPE IDENTIFIER NAME
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE                            -- TYPE
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME                                       -- TYPE NAME
                   , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN
                   , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
                   , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
                   , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
                   , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
                   , SUM(GTPS_NETWIN) AS NETWIN
                   , GTPS_ISO_CODE AS ISO_CODE
                   , GT_THEORIC_HOLD  AS THEORIC_HOLD
                   , CS_OPENING_DATE AS OPEN_HOUR
                   , CS_CLOSING_DATE AS CLOSE_HOUR
                   , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
              FROM   GT_PLAY_SESSIONS
             INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
             INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
             INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
             INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
               AND   CS_OPENING_DATE >= @pDateFrom
               AND   CS_OPENING_DATE < @pDateTo
             WHERE   CS_STATUS = 1
              AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
              AND    GTPS_ISO_CODE IN (@pSelectedCurrency)
             GROUP   BY   GTPS_GAMING_TABLE_ID
                   , GT_NAME
                   , CS_OPENING_DATE
                   , CS_CLOSING_DATE
                   , GTT_GAMING_TABLE_TYPE_ID
                   , GTT_NAME
                   , GT_THEORIC_HOLD
                   , GTPS_ISO_CODE
             -- END CORE QUERY
        ) AS X
 GROUP BY  X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , X.ISO_CODE

  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
               DT.TABLE_IDENT_NAME AS TABLE_NAME,
               DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
               DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
               ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
               ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
               ISNULL(ZZ.HOLD, 0) AS HOLD,
               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
               ISNULL(ZZ.NETWIN, 0) AS NETWIN,
               ISO_CODE,
               ZZ.OPEN_HOUR,
               ZZ.CLOSE_HOUR,
               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
               DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
               ON (
                  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                )
         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC,
               CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
               DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
              ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.HOLD, 0) AS HOLD,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
              ISNULL(ZZ.NETWIN, 0) AS NETWIN,
              ISO_CODE,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME
        FROM  @_DAYS_AND_TABLES DT
  INNER JOIN  #GT_TEMPORARY_REPORT_DATA ZZ
              ON(
                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
              )
              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
              DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION

GO


 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
 GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(3)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 0, CAST(@_DATE_FROM AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN)  TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP) / SUM(X.S_DROP) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP(ISNULL(GTS_OWN_SALES_AMOUNT      , 0)
                                                         , ISNULL(GTS_EXTERNAL_SALES_AMOUNT , 0)
                                                         , ISNULL(GTS_COLLECTED_AMOUNT      , 0)
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT       , 0))
                                                         , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT  , 0))
                                                         , SUM(ISNULL(GTSC_COLLECTED_AMOUNT       , 0))
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_DROP

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                      ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             END AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_TIPS                    , 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT    , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                , 0)))
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     ISNULL(GTSC_TIPS, 0)
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

 GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION