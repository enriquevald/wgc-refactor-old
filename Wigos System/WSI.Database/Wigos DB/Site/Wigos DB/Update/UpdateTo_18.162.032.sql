/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 161;

SET @New_ReleaseId = 162;
SET @New_ScriptName = N'UpdateTo_18.162.032.sql';
SET @New_Description = N'Gp: Cage and PSAClient.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/***** TABLES *****/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].draw_tickets') and name = 'dt_awarded_day')
      ALTER TABLE [dbo].draw_tickets ADD dt_awarded_day [datetime] NULL;
GO

UPDATE draw_tickets 
	SET dt_awarded_day = dbo.Opening(0, dt_created);
GO

ALTER TABLE [dbo].draw_tickets  alter column
	dt_awarded_day [datetime] NOT NULL;
GO

/**** TRIGGERS  ****/
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountMovementsSynchronize.sql
-- 
--   DESCRIPTION: Procedures for trigger ACCOUNT_MOVEMENTS_SYNCHRONIZE and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 25-FEB-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 25-FEB-2013 JML    First release.
-- 02-DIC-2013 jml    Add movements 68 & 72
-------------------------------------------------------------------------------
--
--   NOTES :
--
-- Movements related with points:
--     
--  36, 'PointsAwarded'
--  37, 'PointsToGiftRequest'
--  38, 'PointsToNotRedeemable'
--  39, 'PointsGiftDelivery'
--  40, 'PointsExpired'
--  41, 'PointsToDrawTicketPrint'
--  42, 'PointsGiftServices'
--  43, 'HolderLevelChanged'
--  46, 'PointsToRedeemable'
--  50, 'CardAdjustment'
--  60, 'PromotionPoint'
--  61, 'CancelPromotionPoint'
--  62, 'ManualHolderLevelChanged'
--  66, 'CancelGiftInstance' 
--  67, 'PointsStatusChanged' 
--  68, 'ManuallyAddedPointsForLevel'
--  71, 'ImportedPointsOnlyForRedeem'
--  72, 'ImportedPointsForLevel'
-- 101, 'MultiSiteCurrentLocalPoints'
--
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_SiteToMultiSite_Points]') AND type in (N'TR'))
DROP TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
  
    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
          WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 46, 50, 60, 61, 62, 66, 67, 68, 71, 72, 101 ) 
   
    SET NOCOUNT OFF

  END -- [Trigger_SiteToMultiSite_Points]
GO
 
--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0
END
GO
 
/**** RECORDS ****/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Constancias.ReportarCURP')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Constancias.ReportarCURP', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR1')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Salidas.ISR1', '1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR2')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Salidas.ISR2', '1');
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'CageAllowTotalImbalance')
   BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'CageAllowTotalImbalance', '0')
   END
ELSE
   BEGIN
      UPDATE GENERAL_PARAMS SET  GP_KEY_VALUE = '0' WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'CageAllowTotalImbalance'
   END
GO



IF NOT EXISTS ( SELECT * FROM dbo.currency_exchange WHERE ce_currency_iso_code = 'X01' AND ce_type = 0)
BEGIN
  INSERT INTO currency_exchange
             ([ce_type]
             ,[ce_currency_iso_code]
             ,[ce_description]
             ,[ce_change]
             ,[ce_num_decimals]
             ,[ce_variable_commission]
             ,[ce_fixed_commission]
             ,[ce_variable_nr2]
             ,[ce_fixed_nr2]
             ,[ce_status])
       VALUES
             (0,'X01','Fichas',1,2,0,0,0,0,1)
END
GO

DELETE FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = -1
GO

DECLARE @pEnabled AS BIT
SET @pEnabled = 0

UPDATE CAGE_AMOUNTS SET CAA_DENOMINATION = -100 WHERE CAA_DENOMINATION = -1

UPDATE CAGE_MOVEMENT_DETAILS  SET  CMD_QUANTITY = -100  WHERE CMD_QUANTITY = -1

IF (EXISTS (SELECT caa_allowed FROM cage_amounts WHERE caa_iso_code = 'MXN' AND caa_allowed = 1))
BEGIN
  SET @pEnabled = 1
END

--MXN
IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'MXN' AND caa_denomination = -1)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('MXN' , -1 , @pEnabled)
ELSE
    SELECT '***** value MXN, -1 already exists *****';

IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'MXN' AND caa_denomination = -2)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('MXN' , -2 , @pEnabled)
ELSE
    SELECT '***** value MXN, -2 already exists *****';
    
--USD
SET @pEnabled = 0
IF (EXISTS (SELECT caa_allowed FROM cage_amounts WHERE caa_iso_code = 'USD' AND caa_allowed = 1))
BEGIN
  SET @pEnabled = 1
END

IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'USD' AND caa_denomination = -1)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('USD' , -1 , @pEnabled)
ELSE
    SELECT '***** value USD, -1 already exists *****';

IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'USD' AND caa_denomination = -2)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('USD' , -2 , @pEnabled)
ELSE
    SELECT '***** value USD, -2 already exists *****';
 
    
--PEN
SET @pEnabled = 0
IF (EXISTS (SELECT caa_allowed FROM cage_amounts WHERE caa_iso_code = 'PEN' AND caa_allowed = 1))
BEGIN
  SET @pEnabled = 1
END
IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'PEN' AND caa_denomination = -1)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('PEN' , -1 , @pEnabled)
ELSE
    SELECT '***** value PEN, -1 already exists *****';   

IF NOT EXISTS ( SELECT * FROM dbo.cage_amounts WHERE caa_iso_code = 'PEN' AND caa_denomination = -2)
    INSERT INTO cage_amounts (caa_iso_code , caa_denomination , caa_allowed) VALUES ('PEN' , -2 , @pEnabled)
ELSE
    SELECT '***** value PEN, -2 already exists *****';   
GO


