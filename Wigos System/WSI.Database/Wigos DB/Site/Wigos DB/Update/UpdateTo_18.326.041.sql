/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 325;

SET @New_ReleaseId = 326;
SET @New_ScriptName = N'UpdateTo_18.326.041.sql';
SET @New_Description = N'New buckets 8 and 9';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'Cashier.TaxCustody' 
				AND GP_SUBJECT_KEY = 'Voucher.ShowTaxRechargeVoucher'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('Cashier.TaxCustody', 'Voucher.ShowTaxRechargeVoucher', '0')
END
GO

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'Cashier.Voucher' 
				AND GP_SUBJECT_KEY = 'PaymentMethod.BankCard.HideVoucherRecharge'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('Cashier.Voucher', 'PaymentMethod.BankCard.HideVoucherRecharge', '0')
END
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

DECLARE @bu_name_bucket_8 NVARCHAR(100)
DECLARE @bu_name_bucket_9 NVARCHAR(100)

if exists(select 1 from GENERAL_PARAMS where gp_group_key='WigosGUI' and gp_subject_key = 'Language' and gp_key_value = 'es') BEGIN
            set @bu_name_bucket_8 = 'Puntos de canje - Generados'
            set @bu_name_bucket_9 = 'Puntos de nivel - Discrecionales'

end else 
BEGIN
            set @bu_name_bucket_8 = 'Redemption points - Generated'
            set @bu_name_bucket_9 = 'Ranking level points - Discretionary'
end


IF NOT EXISTS (SELECT 1 FROM BUCKETS WHERE bu_bucket_id = 8)
	INSERT INTO dbo.buckets ([bu_bucket_id], [bu_name], [bu_enabled], [bu_system_type], [bu_bucket_type], [bu_visible_flags], [bu_order_on_reports], 
	                         [bu_expiration_days], [bu_expiration_date], [bu_level_flags], [bu_k_factor], [BU_MASTER_SEQUENCE_ID])   
	VALUES ( 8,  @bu_name_bucket_8, 1, 0, 8,  '4294967291', 1, 0, Null, 1, 0,  '1')


IF NOT EXISTS (SELECT 1 FROM BUCKETS WHERE bu_bucket_id = 9)
	INSERT INTO dbo.buckets ([bu_bucket_id], [bu_name], [bu_enabled], [bu_system_type], [bu_bucket_type], [bu_visible_flags], [bu_order_on_reports], 
	                         [bu_expiration_days], [bu_expiration_date], [bu_level_flags], [bu_k_factor], [BU_MASTER_SEQUENCE_ID])   
	VALUES ( 9,  @bu_name_bucket_9, 1, 0, 9,  '4294967291', 1, 0, Null, 1, 0,  '1')
GO


  SET NOCOUNT ON
  DECLARE @study_period                                    AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  DECLARE @AccountId                          BIGINT
  DECLARE @PointsGeneratedForLevel_8          MONEY 
  DECLARE @PointsDiscretionalForLevel_9       MONEY 
  DECLARE @PointsBucket2                      MONEY

  DECLARE @points_awarded                     AS INT
  DECLARE @manually_added_points_for_level    AS INT
  DECLARE @imported_points_for_level          AS INT
  DECLARE @imported_points_history            AS INT
  DECLARE @EndSession_RankingLevelPoints             AS INT
  DECLARE @Expired_RankingLevelPoints                AS INT
  DECLARE @Manual_Add_RankingLevelPoints                 AS INT
  DECLARE @Manual_Sub_RankingLevelPoints                 AS INT
  DECLARE @Manual_Set_RankingLevelPoints                 AS INT

  -- Points for level
    -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
     -- Discretional
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel
  SET @imported_points_history               = 73
  --ImportedPointsHistory
  SET @EndSession_RankingLevelPoints              = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints                 = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints              = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints              = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints              = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @study_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));


  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)

  SET @date_from = DATEADD(DAY, -1*(@study_period-1), @today_opening);

  DELETE FROM CUSTOMER_BUCKET WHERE cbu_bucket_id in(8,9)

-- bucket 8
    INSERT INTO CUSTOMER_BUCKET(cbu_customer_id,cbu_bucket_id,cbu_value,cbu_updated)

                        SELECT   am_account_id
                                   ,8
                                   ,SUM(ISNULL(AM_ADD_AMOUNT,0)-ISNULL(AM_SUB_AMOUNT,0) )
                                   ,GETDATE()
                              
                        FROM Account_Movements
                        WHERE AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                        and am_datetime >= @date_from 
                        and am_datetime < @today_opening
                        GROUP BY am_account_id
    
-- bucket 9
    INSERT INTO CUSTOMER_BUCKET(cbu_customer_id,cbu_bucket_id,cbu_value,cbu_updated)

                        SELECT   am_account_id
                                   ,9
                                   ,SUM(ISNULL(AM_ADD_AMOUNT,0)-ISNULL(AM_SUB_AMOUNT,0)) 
                                   ,GETDATE()
                              
                        FROM Account_Movements
                        WHERE AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, 
                                                           @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                        and am_datetime >= @date_from 
                        and am_datetime < @today_opening
                        GROUP BY am_account_id




-- BUCKET 2 <> BUCKET 8 + 9
-- b. if bucket_2 <> bucket_8 + bucket_9     then update bucket_2 = bucket_8 + bucket_9

    UPDATE 
      CUSTOMER_BUCKET SET CBU_VALUE = CBU8.VALUE
        FROM CUSTOMER_BUCKET CB 
        INNER JOIN 
        (
                SELECT   CB2.CBU_CUSTOMER_ID
                        ,( CB2.CBU_VALUE - CB9.CBU_VALUE) VALUE  
                  FROM  CUSTOMER_BUCKET CB2
            INNER JOIN  CUSTOMER_BUCKET CB8 ON CB2.CBU_CUSTOMER_ID = CB8.CBU_CUSTOMER_ID
            INNER JOIN  CUSTOMER_BUCKET CB9 ON CB2.CBU_CUSTOMER_ID = CB9.CBU_CUSTOMER_ID
                 WHERE  CB2.CBU_BUCKET_ID = 2
                        AND CB8.CBU_BUCKET_ID = 8
                        AND CB9.CBU_BUCKET_ID = 9
                        AND CB8.CBU_VALUE <> CB2.CBU_VALUE - CB9.CBU_VALUE 
        ) CBU8
        ON CBU8.CBU_CUSTOMER_ID = CB.CBU_CUSTOMER_ID
        WHERE CB.CBU_BUCKET_ID = 8


-- a. if bucket_2 == null                    then INSERT bucket_2       -->  (bucket_2 = bucket_8 + bucket_9)
-- NO EXISTE BUCKET 2 
    INSERT INTO CUSTOMER_BUCKET (cbu_bucket_id,cbu_customer_id,cbu_updated,cbu_value) 
            SELECT    2
                    , CB8.CBU_CUSTOMER_ID, GETDATE()
                    , (CB8.CBU_VALUE + CB9.CBU_VALUE) VALUE
              FROM  CUSTOMER_BUCKET CB8 
        INNER JOIN  CUSTOMER_BUCKET CB9 ON CB8.CBU_CUSTOMER_ID = CB9.CBU_CUSTOMER_ID
         LEFT JOIN  CUSTOMER_BUCKET CB2 ON CB8.CBU_CUSTOMER_ID = CB2.CBU_CUSTOMER_ID AND CB2.CBU_BUCKET_ID = 2
             WHERE  CB8.CBU_BUCKET_ID = 8
                    AND CB9.CBU_BUCKET_ID = 9
                    AND CB2.CBU_CUSTOMER_ID IS NULL


GO



DECLARE @CurrencyISOCode AS NVARCHAR(3)
DECLARE @RowCount AS INT
   
SELECT @CurrencyISOCode = GP_KEY_VALUE  FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'
   
SELECT @RowCount = gtb_id  FROM GAMING_TABLE_BET
   
if (@RowCount IS NULL)
   
BEGIN

  INSERT INTO GAMING_TABLE_BET
            (GTB_GAMING_TABLE_ID
            ,GTB_ISO_CODE,GTB_MIN_BET 
            ,GTB_MAX_BET,GTB_STATUS)
    SELECT   GT_GAMING_TABLE_ID
            ,@CurrencyISOCode 
            ,ISNULL(GT_BET_MIN,0)
            ,ISNULL(GT_BET_MAX,0)
            ,0 
      FROM  GAMING_TABLES
    
END
GO

/******* PROCEDURES *******/

IF EXISTS( SELECT 1 FROM sys.objects WHERE name = 'GetAccountPointsCache')
  DROP PROCEDURE [dbo].[GetAccountPointsCache] 
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache] @pAccountId bigint
AS
BEGIN

  --EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  DECLARE @BucketNR Int 
  DECLARE @BucketRE Int
  DECLARE @BucketPuntosCanjeGenerados Int
  DECLARE @BucketPuntosCanjeDiscrecionales Int

    
  SET @BucketPuntosCanje = 1
  SET @BucketPuntosCanjeGenerados = 8
  SET @BucketPuntosCanjeDiscrecionales = 9
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs función Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
		     , ISNULL(AC_POINTS.CBU_VALUE                      ,0) AS  AC_POINTS                 
		     , ISNULL(AM_POINTS_GENERATED.CBU_VALUE      	  ,0) AS  AM_POINTS_GENERATED       
		     , ISNULL(AM_POINTS_DISCRETIONARIES.CBU_VALUE	  ,0) AS  AM_POINTS_DISCRETIONARIES 
    FROM   ACCOUNTS 
      LEFT JOIN CUSTOMER_BUCKET AC_POINTS ON AC_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_POINTS.CBU_BUCKET_ID = 1 /* REDEMPTIONPOINTS */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_GENERATED ON AM_POINTS_GENERATED.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_GENERATED.CBU_BUCKET_ID = 8 /* RANKINGLEVELPOINTS_GENERATED */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_DISCRETIONARIES ON AM_POINTS_DISCRETIONARIES.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_DISCRETIONARIES.CBU_BUCKET_ID = 9 /* RANKINGLEVELPOINTS_DISCRETIONAL */
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
END

GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO