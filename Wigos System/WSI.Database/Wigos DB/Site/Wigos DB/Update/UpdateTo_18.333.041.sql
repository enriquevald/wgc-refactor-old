/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 332;

SET @New_ReleaseId = 333;
SET @New_ScriptName = N'UpdateTo_18.333.041.sql';
SET @New_Description = N'Pin Pad Provider';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.Name'
           ,'');
           
           
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.MerchandId')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.MerchandId'
           ,'');
   
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.User')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.User'
           ,'');
           
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.Password')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.Password'
           ,'');
           
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.URL')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.URL'
           ,'');
           
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Provider.001.PinPadId')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PinPad'
           ,'Provider.001.PinPadId'
           ,'');


/**** WITHDRAWAL *****/
UPDATE GENERAL_PARAMS SET gp_key_value = '' WHERE GP_GROUP_KEY = 'Cashier.Withdrawal' AND GP_SUBJECT_KEY = 'BankCardRechargeVSCash.Pct'

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

DECLARE @bu_name_bucket_8 NVARCHAR(100)
DECLARE @bu_name_bucket_9 NVARCHAR(100)

IF EXISTS(SELECT 1 FROM GENERAL_PARAMS where GP_GROUP_KEY='WigosGUI' and GP_SUBJECT_KEY = 'Language' and GP_KEY_VALUE = 'es') 
  BEGIN
              SET @bu_name_bucket_8 = 'Puntos de nivel - Generados'
              SET @bu_name_bucket_9 = 'Puntos de nivel - Discrecionales'

  END 
ELSE 
  BEGIN
              SET @bu_name_bucket_8 = 'Ranking level points - Generated'
              SET @bu_name_bucket_9 = 'Ranking level points - Discretionary'
  END


UPDATE buckets SET bu_name = @bu_name_bucket_8 WHERE bu_bucket_id = 8
UPDATE buckets SET bu_name = @bu_name_bucket_9 WHERE bu_bucket_id = 9

GO

DECLARE @bucket_enabled BIT

SELECT @bucket_enabled = bu_enabled 
  FROM BUCKETS 
 WHERE BU_BUCKET_ID = 2 -- Puntos de nivel

SET @bucket_enabled = ISNULL(@bucket_enabled, 0)

UPDATE buckets set bu_enabled = @bucket_enabled where bu_bucket_id in (8,9)

GO

/******* PROCEDURES *******/
